/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.map.view.TextMarkerMapView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * GoogleFragmentMapView.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 8:58:20 AM Aug 5, 2014
 */
public class GoogleFragmentMapView
		extends BaseFragmentMapView
		implements
		OnMapLoadedCallback,
		OnMarkerDragListener,
		OnMarkerClickListener, OnMapClickListener
		 {

	class ListenerFromView{
		BaseFragment view;
		OnEventMapControlListener listener;
		int action;
		Object data;
	}

	private InfoWindowAdapter adapter;
	private HashMap<String, ListenerFromView> hash;
	List<Marker> listPointRoute = null;
	Polyline polyRoute = null;
	
	public GoogleFragmentMapView(
			GlobalBaseActivity parent, InfoWindowAdapter adapter) {
		super(parent);
		this.adapter = adapter;
	}

	private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000 ;
//	final LatLng center = new LatLng(10.784829, 106.68307);
	final LatLng center = new LatLng(16.2872609, 107.1993245);

	public MapWrapperLayout bgMapView;
	// mapview cua google map
	//public MapView mapView;
	// google map
	public GoogleMap googleMap;
	// map tu id cua marker sang
	private Marker markerMyPosition;
	private int widthMap = 0;
	private int heightMap = 0;
	private Marker lastOpenedMarker = null;
	TextMarkerMapView markerMapview;
	// Listener
	OnInfoWindowElemTouchListener visitListener;
	OnInfoWindowElemTouchListener oderListener;
	OnInfoWindowElemTouchListener closeListener;
	//view cua infowindow
	View infoWindowView = null;
	MapView mapView;
	OfflineMapManager offlineMapManager;

	@Override
	public ViewGroup initMap(Bundle savedInstanceState) {
		bgMapView = new MapWrapperLayout(parent);
		bgMapView.setBackgroundColor(Color.rgb(237, 234, 226));
		bgMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

//		LayoutInflater inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//		inflater.inflate(R.layout.layout_map_view, bgMapView);
//
//		mapFragment = ((MapFragment) parent.getFragmentManager().findFragmentById(R.id.map));
//		googleMap = mapFragment.getMap();

		try {
			//truoc khi su dung cac factory thi phai truyen vao App context de initialize
			MapsInitializer.initialize(GlobalInfo.getInstance().getAppContext());
        } catch (Exception e) {
        }

		rlMainMapView = new RelativeLayout(parent);
		rlMainMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		rlMainMapView.addView(bgMapView);

		//add map
		mapView = new MapView(parent);
		mapView.onCreate(savedInstanceState);
		mapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		bgMapView.addView(mapView);
		googleMap = mapView.getMap();

		markerMapview = new TextMarkerMapView(parent, R.drawable.icon_map, "", "", -1, -1);

		if (googleMap == null) {
			return rlMainMapView;
		}

		googleMap.setOnMapLoadedCallback(this);
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		googleMap.getUiSettings().setMyLocationButtonEnabled(false);
		googleMap.setMyLocationEnabled(false);
		googleMap.setOnMarkerDragListener(this);
		googleMap.setOnMapClickListener(this);

		hash= new HashMap<String, GoogleFragmentMapView.ListenerFromView>();

		if (isViewMyPositionControl) {
			initMyPositionControl();
		}
		initZoomControl();
		moveMapTo(center, CustomFragmentMapView.ZOOM_LEVEL_GGMAP_DEFAULT);

		bgMapView.init(googleMap, getPixelsFromDp(this, 39 + 20));

		googleMap.setOnMarkerClickListener(this);
		googleMap.setInfoWindowAdapter(this.adapter);

		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			private ListenerFromView listenerFromView;
//			private ViewGroup view1;
			@Override
			public View getInfoWindow(Marker marker) {
				if(hash != null){
				 listenerFromView = hash.get(marker.getId());
					if(listenerFromView != null){
						infoWindowView = listenerFromView.listener.onEventMap(listenerFromView.action, null, listenerFromView.data);
					}
				}
				if (infoWindowView == null) {
					return null;
				}

				lastOpenedMarker = marker;

				bgMapView.setMarkerWithInfoWindow(marker, infoWindowView);

				return infoWindowView;
			}

			@Override
			public View getInfoContents(Marker marker) {
				// TODO Auto-generated method stub
				return null;
			}
		});

		//add offline future
		offlineMapManager = new OfflineMapManager(googleMap, parent, rlMainMapView);

		return rlMainMapView;
	}

	/**
	 * Khoi tao control zoom
	 *
	 * @author: dungdq3
	 * @since: 9:07:04 AM Aug 5, 2014
	 * @return: void
	 * @throws:
	 */
	@Override
	public void initZoomControl() {
		View zoomControls = null;
		zoomControls = mapView.findViewById(0x1);

		if (zoomControls != null
				&& zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
			// ZoomControl is inside of RelativeLayout
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls
					.getLayoutParams();

			// Align it to - parent top|left
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			// Update margins, set to 10dp
			final int margin = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 10, parent.getResources()
							.getDisplayMetrics());
			params.setMargins(margin, margin, margin, margin);
		}
	}

	@Override
	public void drawMarkerMyPosition() {
		// TODO Auto-generated method stub
		if (markerMyPosition != null) {
			markerMyPosition.remove();
		}

		if (GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() > 0
				&& GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude() > 0) {
			markerMyPosition = drawMarker(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(),
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude(), R.drawable.icon_location);
		}

		mapView.invalidate();
//		if (mapFragment.getView() != null) {
//			mapFragment.getView().invalidate();
//		}
	}

	@Override
	public void moveToCenterMyPosition() {
		if (googleMap == null) {
			return;
		}

		if (GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() > 0
				&& GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude() > 0) {
			LatLng myLocation = new LatLng(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(),
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude());
			CameraUpdate cu = CameraUpdateFactory.newLatLng(myLocation);
			googleMap.animateCamera(cu);
		}
	}

	/**
	 * Chuyen toa do tu dp -> pixel
	 *
	 * @author: dungdq3
	 * @since: 9:01:46 AM Aug 5, 2014
	 * @return: int
	 * @throws:
	 * @param fragmentMapView
	 * @param dp
	 * @return
	 */
	public static int getPixelsFromDp(BaseFragmentMapView fragmentMapView, double dp) {
		final double scale = fragmentMapView.parent.getResources().getDisplayMetrics().density;
		return (int) (dp * scale + 0.5f);
	}

	public int getPaddingFitbounds() {
		return 60;
	}

	/*
	 * @Override public void fitBounds(ArrayList<LatLng> arrayPosition) { //
	 * TODO Auto-generated method stub if (arrayPosition != null &&
	 * !arrayPosition.isEmpty()) { LatLngBounds.Builder builder = new
	 * LatLngBounds.Builder(); for (LatLng latLng : arrayPosition) {
	 * builder.include(latLng); } LatLngBounds bounds = builder.build();
	 * //CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
	 * if (widthMap <= 0 || heightMap <= 0) { widthMap = mapView.getWidth();
	 * heightMap = mapView.getHeight(); } if (widthMap > 0 && heightMap > 0) {
	 * CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, widthMap,
	 * heightMap, getPaddingFitbounds()); mapView.getMap().animateCamera(cu); }
	 * } }
	 */

	/**
	 * Di chuyen ban don den 1 toa do
	 *
	 * @author: dungdq3
	 * @since: 9:36:54 AM Aug 5, 2014
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	public void moveMapTo(double lat, double lng) {
		moveMapTo(new LatLng(lat, lng));
	}

	/**
	 * Di chuyen ban don den 1 toa do
	 *
	 * @author: dungdq3
	 * @since: 9:37:00 AM Aug 5, 2014
	 * @return: void
	 * @throws:
	 * @param latlng
	 */
	public void moveMapTo(LatLng latlng) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latlng).zoom(CustomFragmentMapView.ZOOM_LEVEL_GOOGLE_NORMAL).build();

		if (googleMap != null) {
			googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		}
	}

	public void moveMapTo(LatLng latlng, int zoomLevel) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latlng).zoom(zoomLevel).build();

		if (googleMap != null) {
			googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		}
	}

	/**
	 * covert view sang bitmap
	 *
	 * @author cuonglt3
	 * @param context
	 * @param view
	 * @return
	 */
	public static Bitmap createDrawableFromView(Context context, Object object) {
		// VTLog.e(true, "[Quang]", "start" + new Date());
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		View view = (View) object;
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
		// VTLog.e(true, "[Quang]", "end" + new Date());
		return bitmap;
	}

	public void clearAllCurrentMarker() {
		if (googleMap != null) {
			googleMap.clear();
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		// Check if there is an open info window
		if (lastOpenedMarker != null) {
			// Close the info window
			lastOpenedMarker.hideInfoWindow();

			// Is the marker the same marker that was already open
			if (lastOpenedMarker.equals(marker)) {
				// Nullify the lastOpened object
				lastOpenedMarker = null;
				// Return so that the info window isn't opened again
				return true;
			}
		}

		// Re-assign the last opened such that we can close it later
		lastOpenedMarker = marker;

		// Open the info window for the marker
		marker.showInfoWindow();

		// Event was handled by our code do not launch default behaviour.
		moveCameraAfterShowInfowindow(marker, 80);
		return true;
	}

	/**
	 * move camera map xuong 1 doan sau khi show infowindow
	 *
	 * @author: dungdq3
	 * @since: 9:49:49 AM Aug 5, 2014
	 * @return: void
	 * @throws:
	 * @param marker
	 * @param i
	 */
	private void moveCameraAfterShowInfowindow(Marker marker, final int pixel) {
		if (googleMap != null) {
			Projection projection = googleMap.getProjection();
			LatLng markerLocation = marker.getPosition();
			Point screenPosition = projection.toScreenLocation(markerLocation);
			screenPosition.y -= pixel;
			LatLng latlng = projection.fromScreenLocation(screenPosition);
			CameraUpdate cu = CameraUpdateFactory.newLatLng(latlng);
			googleMap.animateCamera(cu);
		}
	}

	/**
	 * add 1 marker vao map
	 *
	 * @author cuonglt3
	 * @param drawable
	 * @param lat
	 * @param lng
	 * @param seqInPlan
	 * @return
	 */
	public Marker addMarkerToMap(int drawable, double lat, double lng,
			String seqInPlan, String name, int colorSeq, int colorName) {
		// view cho marker, voi imageview va textview
		TextMarkerMapView markerIcon = new TextMarkerMapView(parent, drawable,
				seqInPlan, name, colorName, colorName);
		return addMarkerToMap(markerIcon, lat, lng);
	}

	public Marker addMarkerToMap(View view, double lat, double lng) {
		Marker marker = null;
		if (googleMap != null) {
			marker = googleMap.addMarker(new MarkerOptions()
			.position(new LatLng(lat, lng))
			.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(parent, view)))
					);
		}
		return marker;
	}

	@Override
	public void onMarkerDrag(Marker arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMarkerDragStart(Marker marker) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMapClick(LatLng point) {
		// TODO Auto-generated method stub
		com.viettel.maps.base.LatLng latlng = new com.viettel.maps.base.LatLng(point.latitude, point.longitude);
		if(listener!=null){
			listener.onSingleTap(latlng, null);
		}
//		onSingleTap(latlng, null);
	}

	@Override
	public void drawMarkerMyPosition(double lat, double lng) {
		// TODO Auto-generated method stub
		if (markerMyPosition != null) {
			markerMyPosition.remove();
		}

		if (lat > 0 && lng > 0) {
			markerMyPosition = drawMarker(lat, lng, R.drawable.icon_location);
		}

		mapView.invalidate();
//		if (mapFragment.getView() != null) {
//			mapFragment.getView().invalidate();
//		}
	}

	@Override
	public void fitBounds(ArrayList<com.viettel.maps.base.LatLng> arrayPosition) {
		if (googleMap == null) {
			return;
		}

		ArrayList<LatLng> listLatLng = new ArrayList<LatLng>();
		for (com.viettel.maps.base.LatLng latLng : arrayPosition) {
			LatLng latlg = new LatLng(latLng.getLatitude(), latLng.getLongitude());
			listLatLng.add(latlg);
		}
		if (listLatLng != null && !listLatLng.isEmpty()) {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			for (LatLng latLng : listLatLng) {
				builder.include(latLng);
			}
			LatLngBounds bounds = builder.build();
			//CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
			if (widthMap <= 0 || heightMap <= 0) {
				widthMap = bgMapView.getWidth();
				heightMap = bgMapView.getHeight();
			}
			if (widthMap > 0 && heightMap > 0) {
				CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
						widthMap, heightMap, getPaddingFitbounds());
				if (googleMap != null) {
					googleMap.animateCamera(cu);
				}
			}
		}
	}

	@Override
	public void initMyPositionControl() {
		// TODO Auto-generated method stub
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(parent);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		llMyPosition.setLayoutParams(lp);
		llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ImageView ivMyPosition = new ImageView(parent);
		ivMyPosition.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivMyPosition.setImageResource(R.drawable.icon_location_2);
		ivMyPosition.setBackgroundResource(R.drawable.custom_button_with_border);
		ivMyPosition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(listener != null)
					listener.onClickMarkerMyPositionEvent();
			}
		});
		llMyPosition.addView(ivMyPosition);
		rlMainMapView.addView(llMyPosition);
	}

	@Override
	public void onMapLoaded() {
		mapView.invalidate();
//		if (mapFragment.getView() != null) {
//			mapFragment.getView().invalidate();
//		}
	}

	/**
	 * draw marker
	 *
	 * @author: dungdq3
	 * @since: 10:06:42 AM Aug 6, 2014
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	private Marker drawMarker(double lat, double lng, int resource) {
		if (googleMap != null) {
			return googleMap.addMarker(new MarkerOptions()
			.position(new LatLng(lat, lng))
			.icon(BitmapDescriptorFactory.fromResource(resource)).anchor(0.5f, 0.5f));
		} else {
			return null;
		}
	}

	@Override
	public String requestGetCusLocation(double lat, double lng) {
		// TODO Auto-generated method stub
		List<Address> listAddress = null;
		String address = Constants.STR_BLANK;
		Geocoder coder = new Geocoder(parent, Locale.getDefault());
		try {
			listAddress = coder.getFromLocation(lat, lng, 1);
			address = listAddress.get(0).getAddressLine(0);
		} catch (IOException e) {
			MyLog.e("Google map", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}

		return address;
	}

	@Override
	public void showMarkerInMap(double lat, double lng, View view, MarkerPosition position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void invalidate() {
		mapView.invalidate();
//		if (mapFragment.getView() != null) {
//			mapFragment.getView().invalidate();
//		}
	}

	/**
	 * @author cuonglt3
	 * add marker voi view va lat, lng
	 *
	 */
	@Override
	public Object addMarker(double lat, double lng, int drawable, MarkerAnchor markerAnchor, String seq, String numVisitFact, int colorSeq, int colorNumVisit) {

		markerMapview.setIconMarker(drawable);
		markerMapview.setTextMarker(seq);
		markerMapview.setNameMarker(numVisitFact);
		Marker marker1 = null;

		if (googleMap != null) {
			marker1 = googleMap.addMarker(new MarkerOptions().position(
					new LatLng(lat, lng)).icon(
							BitmapDescriptorFactory.fromBitmap(createDrawableFromView(
									parent, markerMapview))));
			setAnchorMarker(marker1, markerAnchor);
		}
		return marker1;
	}

	@Override
	public void clearAllMarkerOnMap() {
		if (googleMap != null) {
			googleMap.clear();
		}
	}

	/**
	 * @author cuonglt3
	 * su kien khi click vao marker, truyen param vao hashmap
	 *
	 */
	@Override
	public void setMarkerOnClickListener(Object marker,
			OnEventMapControlListener listener, int action, Object userData) {
		if(hash != null){
			ListenerFromView a = hash.get(((Marker)marker).getId());
			if(a == null){
				a = new ListenerFromView();
				a.action = action;
				a.data = userData;
				a.listener = listener;

				hash.put(((Marker)marker).getId(), a);
			}
		}
	}

	@Override
	public Object getMapView() {
		return googleMap;
	}

	@Override
	public View showPopupWhenClickMarker(final View view, OverlayViewOptions opts) {
		return view;
	}

	@Override
	public void clearPopup() {
		// TODO Auto-generated method stub
        if (lastOpenedMarker != null) {
            // Close the info window
            lastOpenedMarker.hideInfoWindow();

            // Is the marker the same marker that was already open
            if (lastOpenedMarker.equals(marker)) {
                // Nullify the lastOpened object
                lastOpenedMarker = null;
                // Return so that the info window isn't opened again
            }
        }
	}
	/**
	 * Tao listener khi click len infowindow (pop up)
	 * @author: cuonglt3
	 * @since: 9:20:32 AM Aug 11, 2014
	 * @return: OnInfoWindowElemTouchListener
	 * @throws:
	 * @param v
	 * @param listener
	 * @param acion
	 * @return
	 */
	public OnInfoWindowElemTouchListener createWindowInfoListener(final View v, final OnEventMapControlListener listener,
			final int acion) {
		MyLog.d("[Quang]", "Set up listener.");
		OnInfoWindowElemTouchListener infoListener = new OnInfoWindowElemTouchListener(v) {
			@Override
			protected void onClickConfirmed(View view, Marker marker) {
				MyLog.d("[Quang]", "Nhan su kien click popup map.");
				if(listener != null && marker != null){
					listener.onEventMap(acion, view, marker);
				}else{
					MyLog.d("[Quang]", "Click tren popup marker nhung listener hoac marker bi null");
				}
			}
		};

		return infoListener;
	}

	@Override
	public void getAdressFromLatLng(GetAddressListener listener,
			com.viettel.maps.base.LatLng latlng) {
		double lat = latlng.getLatitude();
		double lng = latlng.getLongitude();
		new AsyncGetAddress(listener, parent, "").execute(""+ lat, "" +lng);
	}

	@Override
	public void onResume() {
		mapView.onResume();
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(GlobalInfo.getInstance().getActivityContext());
		if (status != ConnectionResult.SUCCESS) {
			GooglePlayServicesUtil.showErrorDialogFragment(status, parent, CONNECTION_FAILURE_RESOLUTION_REQUEST);
		}
	}

	@Override
	public void onPause() {
		mapView.onPause();
	}

	@Override
	public void onDestroy() {
		if (mapView != null) {
			mapView.onDestroy();
		}
	}

	@Override
	public void onLowMemory() {
		mapView.onLowMemory();
	}

	@Override
	public void setZoomBorder() {
		CameraPosition cameraPosition = new CameraPosition.Builder().zoom(4).build();

		if (googleMap != null) {
			googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		}
	}

	@Override
	public View addMarker(View view, OverlayViewOptions opts) {
		googleMap.addMarker(new MarkerOptions()
		.position(new LatLng(opts.getPosition().getLatitude(),  opts.getPosition().getLongitude()))
		.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(parent, view))).anchor((float)0.5,(float)-0.5)
				);
		return view;
	}

	@Override
	public void drawRoute(RouteInfo routeInfo) {
		listPointRoute = new ArrayList<Marker>();
		PolylineOptions polyOption = new PolylineOptions().width(routeInfo.width)
				.color(routeInfo.colorRoute);
		List<MarkerOptions> lstMarkerOptions = new ArrayList<MarkerOptions>();
		
		for (int i = 0, size = routeInfo.listNode.size(); i < size; i++) {
			RouteNodeInfo node = routeInfo.listNode.get(i);
			LatLng latLng = new LatLng(node.lat, node.lng);
			//add path route
			polyOption.add(latLng);
			MarkerOptions options = new MarkerOptions()
					.position(latLng)
					.title(node.title)
					.snippet(node.description)
					.anchor(0.5f, 1f)
					.icon(BitmapDescriptorFactory.fromResource(node.drawableNode));
			lstMarkerOptions.add(options);
		}
		
		polyRoute = googleMap.addPolyline(polyOption);
		
		for (MarkerOptions markerOptions : lstMarkerOptions) {
			Marker markerAdd = googleMap.addMarker(markerOptions);
			//add point to list
			listPointRoute.add(markerAdd);
		}
	}

	@Override
	public void hideRoute() {
		if (polyRoute != null) {
			polyRoute.setVisible(false);
		}
		
		if (listPointRoute != null) {
			for (Marker marker : listPointRoute) {
				marker.setVisible(false);
			}
		}
	}

	@Override
	public void showRoute() {
		if (polyRoute != null) {
			polyRoute.setVisible(true);
		}
		
		if (listPointRoute != null) {
			for (Marker marker : listPointRoute) {
				marker.setVisible(true);
			}
		}
	}

	@Override
	public boolean isHaveRouteData() {
		boolean res = false;
		if (polyRoute != null && listPointRoute != null) {
			res  = true;
		}
		return res;
	}

	private void setAnchorMarker(Marker marker, MarkerAnchor anchorConfig){
		//anchorU 	u-coordinate of the anchor, as a ratio of the image width (in the range [0, 1])
		//anchorV 	v-coordinate of the anchor, as a ratio of the image height (in the range [0, 1]) 
		if (marker != null) {
			float anchorU = 0.5f;
			float anchorV = 0.5f;
			switch (anchorConfig) {
			case CENTER:
				anchorU = 0.5f;
				anchorV = 0.5f;
				break;
			case BOTTOM_CENTER:
				anchorU = 0.5f;
				anchorV = 1f;
				break;
			case BOTTOM_LEFT:
				anchorU = 0f;
				anchorV = 1f;
				break;

			default:
				anchorU = 0.5f;
				anchorV = 0.5f;
				break;
			}
			marker.setAnchor(anchorU, anchorV);
		}
	}
	
}
