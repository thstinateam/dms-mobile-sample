/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.map.view.MarkerMapView;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;
import com.ths.map.view.TextMarkerMapView;
import com.viettel.maps.MapView;
import com.viettel.maps.base.LatLng;
import com.viettel.maps.base.LatLngBounds;
import com.viettel.maps.controls.BaseControl;
import com.viettel.maps.controls.MapTypeControl;
import com.viettel.maps.controls.ZoomControl;
import com.viettel.maps.layers.MarkerLayer;
import com.viettel.maps.objects.Marker;
import com.viettel.maps.objects.MarkerOptions;
import com.viettel.maps.objects.Polyline;
import com.viettel.maps.objects.PolylineOptions;
import com.viettel.maps.services.GeoService;
import com.viettel.maps.services.GeoServiceResult;
import com.viettel.maps.services.ServiceStatus;

/**
 * ViettelFragmentMapView.java
 * @author: dungdq3
 * @version: 1.0
 * @since:  10:27:11 AM Aug 5, 2014
 */
public class ViettelFragmentMapView
		extends BaseFragmentMapView implements com.viettel.maps.MapView.MapEventListener {
//	final LatLng center = new LatLng(10.784829, 106.68307);
	final LatLng center = new LatLng(16.2872609,107.1993245);

	public LinearLayout layoutZoomControl;
	// background map view
	public View bgMapView;
	public MapView mapView;
	public ZoomControl zoomControl;
	MapTypeControl mapTypeControl;
	OverlayViewLayer myPosLayer;
	private OverlayViewOptions markerCustomerOpts;
	private OverlayViewLayer markerCustomerLayer;
	//marker
	private MarkerMapView markerCustomer;
	// layer popup on map
	private OverlayViewLayer popupLayer;
	// layer customer on map
	private OverlayViewLayer customerLayer;
	Polyline polyRoute = null;
	MarkerLayer routeLayer = null;

	/**
	 * @param parent
	 */
	public ViettelFragmentMapView(GlobalBaseActivity parent) {
		super(parent);
	}

	@SuppressWarnings("unchecked")
	@Override
	public ViewGroup initMap(Bundle savedInstanceState) {
		bgMapView = new View(parent);
		bgMapView.setBackgroundColor(Color.rgb(237, 234, 226));
		bgMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		rlMainMapView = new RelativeLayout(parent);
		rlMainMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		rlMainMapView.addView(bgMapView);

		mapView = new MapView(parent);
		mapView.setMapKey(GlobalInfo.getVIETTEL_MAP_KEY());
		mapView.setZoomControlEnabled(true);
		mapView.setMapTypeControlEnabled(false);
		mapView.moveTo(center);
		mapView.setZoom(CustomFragmentMapView.ZOOM_LEVEL_VTMAP_DEFAULT);
		mapView.setMapEventListener(this);
		rlMainMapView.addView(mapView);

		zoomControl = (ZoomControl) mapView.getControl(BaseControl.TYPE_CONTROL_ZOOM);
		if (zoomControl != null) {
			mapView.removeView(zoomControl);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			zoomControl.setLayoutParams(lp);
			zoomControl.setGravity(Gravity.LEFT | Gravity.BOTTOM);
			rlMainMapView.addView(zoomControl);
		}

		if (isViewMyPositionControl) {
			initMyPositionControl();
		}

		markerCustomerOpts = new OverlayViewOptions();
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
		markerCustomer = new MarkerMapView(parent, R.drawable.icon_flag);
		markerCustomer.setVisibility(View.GONE);
//		for (OverlayViewLayer layer : listOverlay) {
//			layer = new OverlayViewLayer();
//			mapView.addLayer(layer);
//		}
		routeLayer = new MarkerLayer();
		mapView.addLayer(routeLayer);
		markerCustomerLayer = new OverlayViewLayer();
		mapView.addLayer(markerCustomerLayer);
		markerCustomerLayer.addItemObj(markerCustomer, markerCustomerOpts);
		myPosLayer = new OverlayViewLayer();
		mapView.addLayer(myPosLayer);
		customerLayer = new OverlayViewLayer();
		mapView.addLayer(customerLayer);
		popupLayer = new OverlayViewLayer();
		mapView.addLayer(popupLayer);
		
		return rlMainMapView;
	}

	/**
	 * Ve button hien thi vi tri cua toi
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	@Override
	public void initMyPositionControl() {
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(mapView.getContext());
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		llMyPosition.setLayoutParams(lp);
		llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ImageView ivMyPosition = new ImageView(parent);
		ivMyPosition.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivMyPosition.setImageResource(R.drawable.icon_location_2);
		ivMyPosition.setBackgroundResource(R.drawable.custom_button_with_border);
		ivMyPosition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if(listener != null)
					listener.onClickMarkerMyPositionEvent();
			}
		});
		llMyPosition.addView(ivMyPosition);
		rlMainMapView.addView(llMyPosition);
	}

	@Override
	public void initZoomControl() {
		// TODO Auto-generated method stub

	}

	@Override
	public void drawMarkerMyPosition() {
		// TODO Auto-generated method stub
		clearMapLayer(myPosLayer);
		if (GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() > 0
				&& GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude() > 0) {

			drawMarker(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(),
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude(),
					R.drawable.icon_location);

		}
	}

	@Override
	public void moveToCenterMyPosition() {
		// TODO Auto-generated method stub

	}

	@Override
	public void moveMapTo(double lat, double lng) {
		mapView.setZoom(CustomFragmentMapView.ZOOM_LEVEL_VTMAP_NORMAL);
		mapView.moveTo(new LatLng(lat, lng));
	}

	@Override
	public void drawMarkerMyPosition(double lat, double lng) {
		// TODO Auto-generated method stub
		clearMapLayer(myPosLayer);
		if (lat > 0f && lng > 0f) {
			drawMarker(lat, lng, R.drawable.icon_location);
		}

	}

	private void drawMarker(double lat, double lng, int icon){
//		LatLng myLocation = new LatLng(lat, lng);
//		mapView.moveTo(myLocation);
		MarkerMapView maker = new MarkerMapView(parent, icon);
		OverlayViewOptions opts = new OverlayViewOptions();
		opts.position(new com.viettel.maps.base.LatLng(lat, lng));
		opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
		myPosLayer.addItemObj(maker, opts);
		mapView.invalidate();
	}

	/**
	 * Xoa layer map
	 *
	 * @author: dungdq3
	 * @since: 9:59:08 AM Aug 6, 2014
	 * @return: void
	 * @throws:
	 * @param myPosLayer2
	 */
	public void clearMapLayer(OverlayViewLayer layer) {
		// TODO Auto-generated method stub
		if (layer != null) {
			int size = layer.getMapObjectTotal();
			for (int i = 0; i < size; i++) {
				OverlayViewItemObj obj = (OverlayViewItemObj) layer.getMapObject(i);
				mapView.removeView(obj.getView());
			}
			layer.clear();
		}
	}

	@Override
	public void clearAllCurrentMarker() {
		// TODO Auto-generated method stub
	}

	@Override
	public void fitBounds(ArrayList<LatLng> arrayPosition) {
		// TODO Auto-generated method stub
		LatLngBounds bound = null;
		for (int i = 0; i < arrayPosition.size(); i++) {
			//duongdt fix bug fit bound multi time
			com.viettel.maps.base.LatLng latlng = arrayPosition.get(i).clone();
			bound = getBound(bound, latlng);
		}

		mapView.fitBounds(bound, true);

	}

	/**
	 * @author: cuonglt3
	 * @since: 8:16:10 AM Aug 8, 2014
	 * @return: LatLngBounds
	 * @throws:
	 * @param bound
	 * @param latlng
	 * @return
	 */
	private LatLngBounds getBound(LatLngBounds bound, LatLng vtLatLng) {
		// TODO Auto-generated method stub
		if (bound == null) {
			bound = new LatLngBounds();
			bound.setSouthWest(vtLatLng);
			bound.setNorthEast(vtLatLng);
		} else {
			int lat = vtLatLng.getLatitudeE6();
			int lng = vtLatLng.getLongitudeE6();
			if (bound.getSouthWest().getLatitudeE6() > lat)
				bound.getSouthWest().setLatitudeE6(lat);
			if (bound.getNorthEast().getLatitudeE6() < lat)
				bound.getNorthEast().setLatitudeE6(lat);
			if (bound.getSouthWest().getLongitudeE6() > lng)
				bound.getSouthWest().setLongitudeE6(lng);
			if (bound.getNorthEast().getLongitudeE6() < lng)
				bound.getNorthEast().setLongitudeE6(lng);
		}
		return bound;
	}

	@Override
	public void onBoundChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCenterChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onDoubleTap(LatLng latlng, Point point) {
		if(listener != null){
			listener.onDoubleTap(latlng, point);
		}
		return false;
	}

	@Override
	public boolean onLongTap(LatLng latlng, Point point) {
		if(listener != null){
			listener.onLongTap(latlng, point);
		}
		return false;
	}

	@Override
	public boolean onSingleTap(LatLng latlng, Point point) {
		if(listener != null){
			listener.onSingleTap(latlng, point);
		}
		return false;
	}

	@Override
	public String requestGetCusLocation(final double lat, final double lng) {
		// TODO Auto-generated method stub
		GeoService geoService = new GeoService();
		LatLng latLng = new LatLng(lat, lng);
		String address = Constants.STR_BLANK;
		GeoServiceResult result = geoService.getAddress(latLng);
		if(result.getStatus() == ServiceStatus.OK){
			address = result.getItem(0).getAddress();
		} else {
			parent.showDialog(StringUtil.getString(R.string.ACCOUNT));
		}
		return address;
	}

	@Override
	public void showMarkerInMap(double lat, double lng, View view, MarkerPosition position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void invalidate() {
		// TODO Auto-generated method stub
		mapView.invalidate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void addMapLayer(OverlayViewLayer layer) {
		// TODO Auto-generated method stub
		super.addMapLayer(layer);
		mapView.addLayer(layer);
	}

	@Override
	public Object addMarker(double lat, double lng, int drawable, MarkerAnchor markerAnchor, String seq, String numVisitFact, int colorSeq, int colorNumVisited) {
		TextMarkerMapView marker = new TextMarkerMapView(parent, drawable, seq, numVisitFact, colorSeq, colorNumVisited);
		OverlayViewOptions opts = new OverlayViewOptions();
		opts.position(new com.viettel.maps.base.LatLng(lat, lng));
		int drawMode = getFrawModeFromMarkerAnchor(markerAnchor);
		opts.drawMode(drawMode);
		customerLayer.addItemObj(marker, opts);
		mapView.invalidate();
		return marker;
	}

	@Override
	public void onMapLoaded() {

	}

	@Override
	public void clearAllMarkerOnMap() {
		// TODO Auto-generated method stub
		clearMapLayer(myPosLayer);
		clearMapLayer(customerLayer);
		if (polyRoute != null) {
			mapView.removeVector(polyRoute);
			polyRoute = null;
		}

		if (routeLayer != null) {
			routeLayer.clear();
		}
	}

	@Override
	public void setMarkerOnClickListener(Object marker,
			OnEventMapControlListener listener, int action, Object userData) {
		// TODO Auto-generated method stub
		TextMarkerMapView textMarker = (TextMarkerMapView) marker;
		textMarker.setListener1(listener, action, userData);
	}

	@Override
	public Object getMapView() {
		// TODO Auto-generated method stub
		return mapView;
	}

	/**
	 * clear pop up hien dang show neu co, sau do show pop up moi
	 *@author cuonglt3
	 *
	 */
	@Override
	public View showPopupWhenClickMarker(View view, OverlayViewOptions opts) {
		// TODO Auto-generated method stub
//		if (popupLayer != null) {
//			int size = popupLayer.getMapObjectTotal();
//			for (int i = 0; i < size; i++) {
//				OverlayViewItemObj obj = (OverlayViewItemObj) popupLayer.getMapObject(i);
//				mapView.removeView(obj.getView());
//			}
//		}
		clearMapLayer(popupLayer);
		popupLayer.addItemObj(view, opts);
		return view;
	}

	@Override
	public void clearPopup() {
		// TODO Auto-generated method stub
		clearMapLayer(popupLayer);
	}

	@Override
	public void getAdressFromLatLng(final GetAddressListener listener, final LatLng latlng) {
		// TODO Auto-generated method stub
		GeoService geoService = new GeoService();
		geoService.getAddress(latlng, new GeoService.GeoServiceListener() {
			public void onGeoServicePreProcess(GeoService arg0) {
				// Su kien tra ve khi bat dau goi service
				// Co the hien thi hop thoai cho o fullDate
				if (listener != null)
					listener.showProgressLoading(true);
			}

			public void onGeoServiceCompleted(GeoServiceResult arg0) {
				// Su kien tra ve khi hoan thanh
				ServiceStatus status = arg0.getStatus();
				if (status == ServiceStatus.OK) {
					if (latlng.getLatitude() > 0
							&& latlng.getLongitude() > 0) {
						String address = arg0.getItem(0).getAddress();
						// Thuc hien xu ly o fullDate
						if (listener != null) {
							listener.setAddress(address);
							listener.showProgressLoading(false);
						}
					}
				} else {
					// Co loi xay ra
					// Xac dinh loi dua vao gia tri cua bien status
					if (listener != null)
						listener.showProgressLoading(false);
				}
			}
		});

	}

	@Override
	public void onResume() {
	}

	@Override
	public void onPause() {
	}

	@Override
	public void onDestroy() {
	}

	@Override
	public void onLowMemory() {
	}

	@Override
	public void setZoomBorder() {
		mapView.setZoom(4);
	}

	@Override
	public View addMarker(View view, OverlayViewOptions opts) {
		// TODO Auto-generated method stub
		customerLayer.addItemObj(view, opts);
		mapView.invalidate();
		return view;
	}

	@Override
	public void drawRoute(RouteInfo routeInfo) {
		PolylineOptions polyOption = new PolylineOptions().strokeWidth(routeInfo.width)
				.strokeColor(routeInfo.colorRoute);
		List<MarkerOptions> lstMarkerOptions = new ArrayList<MarkerOptions>();

		for (int i = 0, size = routeInfo.listNode.size(); i < size; i++) {
			RouteNodeInfo node = routeInfo.listNode.get(i);
			LatLng latLng = new LatLng(node.lat, node.lng);

			Bitmap bMap = BitmapFactory.decodeResource(GlobalInfo.getInstance().getResources(), node.drawableNode);
			//add path route
			polyOption.add(latLng);

			MarkerOptions options = new MarkerOptions().position(latLng)
					.title(node.title)
					.description(node.description)
					.icon(bMap);
			lstMarkerOptions.add(options);
		}

		polyRoute = mapView.addPolyline(polyOption);
		for (MarkerOptions markerOptions : lstMarkerOptions) {
			routeLayer.addMarker(markerOptions);
		}
	}

	@Override
	public void hideRoute() {
		if (polyRoute != null) {
			polyRoute.setVisible(false);
		}

		if (routeLayer != null) {
			for (int i = 0, size = routeLayer.getMapObjectTotal(); i < size; i++) {
				Marker marker = routeLayer.getMapObject(i);
				marker.setVisible(false);
			}
		}
	}

	@Override
	public void showRoute() {
		if (polyRoute != null) {
			polyRoute.setVisible(true);
		}
		if(routeLayer != null){
			for (int i = 0, size = routeLayer.getMapObjectTotal(); i < size; i++) {
				Marker marker = routeLayer.getMapObject(i);
				marker.setVisible(true);
			}
		}

	}

	@Override
	public boolean isHaveRouteData() {
		boolean res = false;
		if (polyRoute != null) {
			res  = true;
		}
		return res;
	}

	private int getFrawModeFromMarkerAnchor(MarkerAnchor anchorConfig){
		int drawMode = OverlayViewItemObj.DRAW_CENTER;
		switch (anchorConfig) {
		case CENTER:
			drawMode = OverlayViewItemObj.DRAW_CENTER;
			break;
		case BOTTOM_CENTER:
			drawMode = OverlayViewItemObj.DRAW_BOTTOM_CENTER;
			break;
		case BOTTOM_LEFT:
			drawMode = OverlayViewItemObj.DRAW_BOTTOM_LEFT;
			break;
			
		default:
			drawMode = OverlayViewItemObj.DRAW_CENTER;
			break;
		}
		return drawMode;
	}
}
