/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

/**
 * GetAddressListener.java
 * @author: quangvt1
 * @version: 1.0 
 * @since:  09:17:47 24-11-2014
 */
public interface GetAddressListener {
	public void showProgressLoading(boolean isShow);
	public void setAddress(String address);
}
