package com.ths.map;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.TileOverlay;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

public class OfflineMapManager implements OnClickListener {

	Button btOnline;
	Button btOffline;
	private long countCacheDownload = 0;
	private int countErrorCacheDownload = 0;
	private long countSuccessCacheDownload = 0;
	GlobalBaseActivity parent;
	GoogleMap googleMap;
	Button btCache;
	ViewGroup layoutPanelDownload;
	
	public OfflineMapManager(GoogleMap googleMap, GlobalBaseActivity parent, ViewGroup rlMainMapView) {
		this.googleMap = googleMap;
		this.parent = parent;
		LayoutInflater inflater = (LayoutInflater) parent
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutPanelDownload = (ViewGroup) inflater.inflate(
				R.layout.layout_map_panel, null);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.CENTER_VERTICAL,
				RelativeLayout.TRUE);
		layoutPanelDownload.setLayoutParams(layoutParams);

		rlMainMapView.addView(layoutPanelDownload);

		btOnline = (Button) layoutPanelDownload.findViewById(R.id.btOnline);
		btOnline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onlineMode();
			}
		});

		btOffline = (Button) layoutPanelDownload.findViewById(R.id.btOffline);
		btOffline.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialogListOfflineData();
			}
		});

		btCache = (Button) layoutPanelDownload
				.findViewById(R.id.btDownloadData);
		btCache.setOnClickListener(this);

		// turn on online mode
		onlineMode();
	}

	public ImageLoadingListener cacheDownloadListener = new ImageLoadingListener() {

		@Override
		public void onLoadingStarted(String url, View v) {

		}

		@Override
		public void onLoadingFailed(String url, View v, FailReason failReason) {
			countErrorCacheDownload++;
			updateDownloadCacheDialog();
		}

		@Override
		public void onLoadingComplete(String url, View arg1, Bitmap bitMap) {
			countSuccessCacheDownload++;
			updateDownloadCacheDialog();
		}

		@Override
		public void onLoadingCancelled(String url, View arg1) {
			countErrorCacheDownload++;
			updateDownloadCacheDialog();
		}
	};
	// current overlay current
	private TileOverlay tileOverlayCurrent;

	private void removeTileOverlay() {
		if (tileOverlayCurrent != null) {
			try {
				tileOverlayCurrent.remove();
			} catch (Exception e) {
			}
		}
	}

	protected void updateDownloadCacheDialog() {
		long totalDownloaded = countErrorCacheDownload
				+ countSuccessCacheDownload;
		if (countCacheDownload > 0) {
			int percent = (int) ((double) totalDownloaded / countCacheDownload * 100);
			parent.updateProgressPercentDialog(percent);
			if (percent >= 100) {
				parent.closeProgressPercentDialog();
				// show thong tin
				parent.showDialog(parent.getString(
						R.string.TEXT_DOWNLOAD_DATA_MAP_RESULT,
						countSuccessCacheDownload, countErrorCacheDownload,
						countCacheDownload));
			}
		}
	}

	/**
	 * tai du lieu cache ban do
	 */
	protected void downloadCacheNow(int detailLevel, String nameData) {
		boolean isNetwork = GlobalUtil.checkNetworkAccess();
		// kiem tra mang truoc khi request tai du lieu
		if (isNetwork) {
			if (googleMap != null) {

				String currentDateWithFormat = "";
				try {
					currentDateWithFormat = DateUtils
							.getCurrentDateTimeWithFormat(DateUtils.DATE_TIME_FORMAT_VN);
				} catch (Exception e) {
				}
				String nameFinal = currentDateWithFormat + " " + nameData;

				float zoom = googleMap.getCameraPosition().zoom;
				MyLog.d("downloadCacheNow",
						"zoom: " + googleMap.getCameraPosition().zoom);
				// add download LatLng
				LatLng centerPoint = googleMap.getCameraPosition().target;
				GlobalInfo.getInstance().addSaveMapData(
						new MapTileUtil.MapDataInfo(nameFinal, centerPoint.latitude,
								centerPoint.longitude, zoom));

				// download map
				LatLngBounds bounds = googleMap.getProjection()
						.getVisibleRegion().latLngBounds;
				DownloadCacheAsync downloadCacheAsync = new DownloadCacheAsync(
						bounds.northeast, bounds.southwest, zoom, detailLevel);
				downloadCacheAsync.execute();
			}
		} else {
			parent.showDialog(parent
					.getString(R.string.TEXT_ENABLE_NETWORK_DOWNLOAD_DATA_MAP));
		}
	}

	void showDialogListOfflineData() {
		List<MapTileUtil.MapDataInfo> listMapData = GlobalInfo.getInstance()
				.getListSaveMapData();
		if (listMapData == null || listMapData.isEmpty()) {
			parent.showDialog(parent
					.getString(R.string.TEXT_LIST_DOWNLOAD_DATA_MAP_EMPTY));
		} else {
			List<String> lstName = new ArrayList<String>();
			for (int i = 0, size = listMapData.size(); i < size; i++) {
				lstName.add(listMapData.get(i).toString());
			}
			String[] items = new String[lstName.size()];
			lstName.toArray(items);

			AlertDialog.Builder builder = new AlertDialog.Builder(parent);
			builder.setTitle(parent
					.getString(R.string.TEXT_LIST_DOWNLOAD_DATA_MAP));
			builder.setItems(items, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int orderData) {
					offineMode(orderData);
				}
			});
			builder.create().show();
		}
	}

	int getTimeMinutes(int second) {
		// 1 tam anh khoang 2s
		int hours = second / 3600;
		int minutes = (second % 3600) / 60;
		int seconds = second % 60;

		int miutesTotal = (hours * 60) + minutes + (seconds > 0 ? 1 : 0);
		return miutesTotal;
	}

	void showDialogListLevelDownload() {
		List<String> lstName = new ArrayList<String>();
		int secondDownload = 32;
		for (int i = 1; i <= MapTileUtil.COUNT_ZOOM_DOWNLOAD; i++) {
			// set name level
			String nameLevel = parent.getString(R.string.TEXT_LIST_ZOOM_LEVEL,
					i, getTimeMinutes(secondDownload));
			lstName.add(nameLevel);
			// thoi gian nhan doi (gan dung)
			secondDownload *= 2;
		}

		String[] items = new String[lstName.size()];
		lstName.toArray(items);

		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		builder.setTitle(parent
				.getString(R.string.TEXT_LIST_ZOOM_LEVEL_DOWNLOAD_DATA_MAP));
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int orderData) {
				downloadCacheNow(orderData + 1, nameDataCurrent);
			}
		});
		builder.create().show();
	}

	class DownloadCacheAsync extends AsyncTask<Void, Void, Void> {

		LatLng northeast;
		LatLng southwest;
		float zoom;
		int detailLevel;
		private List<MapTileUtil.TileInfo> lstDownload;

		public DownloadCacheAsync(LatLng northeast, LatLng southwest,
				float zoom, int detailLevel) {
			this.northeast = northeast;
			this.southwest = southwest;
			this.zoom = zoom;
			this.detailLevel = detailLevel;
		}

		@Override
		protected void onPreExecute() {
			countCacheDownload = 0;
			countErrorCacheDownload = 0;
			countSuccessCacheDownload = 0;
			parent.showProgressPercentDialog(
					parent.getString(R.string.TEXT_DOWNLOADING_DATA_MAP), false);
			parent.updateProgressPercentDialog(0);
		}

		@Override
		protected Void doInBackground(Void[] params) {
			// update download count
			lstDownload = MapTileUtil.getListDownloadCache(this.northeast,
					this.southwest, (int) this.zoom, this.detailLevel);
			return null;
		};

		@Override
		protected void onPostExecute(Void result) {
			if (lstDownload != null && !lstDownload.isEmpty()) {
				countCacheDownload = lstDownload.size();
				try {
					MapTileUtil.downloadCache(lstDownload,
							cacheDownloadListener);
				} catch (Exception e) {
					parent.closeProgressPercentDialog();
					parent.showDialog(parent
							.getString(R.string.TEXT_DOWNLOAD_DATA_MAP_RESULT_FAIL));
				}
			} else {
				parent.closeProgressPercentDialog();
			}
		}
	};

	private String nameDataCurrent = "";

	void showDialogInputNameDataOffline() {
		final EditText input = new EditText(parent);
		input.setSingleLine(true);
		input.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		final AlertDialog alertDialog = new AlertDialog.Builder(parent)
				.setTitle(
						parent.getString(R.string.TEXT_INPUT_NAME_DOWNLOAD_DATA_MAP))
				.setView(input)
				.setPositiveButton(parent.getString(R.string.TEXT_AGREE),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								GlobalUtil.forceHideKeyboardInput(
										(Activity) GlobalInfo.getInstance()
												.getActivityContext(), input);
								Editable nameDataInput = input.getText();
								nameDataCurrent = nameDataInput.toString();
								showDialogListLevelDownload();
							}
						})
				.setNegativeButton(parent.getString(R.string.TEXT_DENY),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								GlobalUtil.forceHideKeyboardInput(
										(Activity) GlobalInfo.getInstance()
												.getActivityContext(), input);
							}
						}).show();
	}

	/**
	 * @author: duongdt3
	 * @param orderData
	 * @since: 09:02:30 14 May 2015
	 * @return: void
	 * @throws:
	 */
	protected void offineMode(int orderData) {

		List<MapTileUtil.MapDataInfo> listMapData = GlobalInfo.getInstance()
				.getListSaveMapData();
		if (listMapData != null && !listMapData.isEmpty()
				&& listMapData.size() > orderData) {
			MapTileUtil.MapDataInfo data = listMapData.get(orderData);
			removeTileOverlay();
			try {
				if (googleMap != null) {
					if (btOnline != null) {
						btOffline
								.setBackgroundResource(R.drawable.icon_offline_active);
					}
					if (btOnline != null) {
						btOnline.setBackgroundResource(R.drawable.icon_online);
					}
					googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
					tileOverlayCurrent = googleMap
							.addTileOverlay(new CachingUrlTileProvider(256, 256) {
								@Override
								public String getTileUrl(int x, int y, int z) {
									// return
									// String.format("https://a.tile.openstreetmap.org/%3$s/%1$s/%2$s.png",
									// x, y, z);
									return MapTileUtil.getUrlTileGoogleMap(x,
											y, z);
								}
							}.createTileOverlayOptions());

					// move to save LatLng
					moveMapTo(new LatLng(data.lat, data.lng), data.zoom);
				}
			} catch (Exception e) {
				MyLog.e("offineMode", "offineMode fail", e);
			}
		}

	}

	/**
	 * @author: duongdt3
	 * @since: 09:02:28 14 May 2015
	 * @return: void
	 * @throws:
	 */
	protected void onlineMode() {
		removeTileOverlay();
		if (googleMap != null) {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			if (btOnline != null) {
				btOffline.setBackgroundResource(R.drawable.icon_offline);
			}
			if (btOnline != null) {
				btOnline.setBackgroundResource(R.drawable.icon_online_active);
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		if (v == btCache) {
			boolean isNetwork = GlobalUtil.checkNetworkAccess();
			// kiem tra mang truoc khi request tai du lieu
			if (isNetwork) {
				if (googleMap != null) {
					int zoom = (int) googleMap.getCameraPosition().zoom;
					if (zoom < MapTileUtil.MIN_ZOOM_DOWNLOAD) {
						int zoomDis = MapTileUtil.MIN_ZOOM_DOWNLOAD - zoom;
						parent.showDialog(parent.getString(
								R.string.TEXT_ZOOM_TO_DOWNLOAD_DATA_MAP,
								zoomDis));
					} else {
						showDialogInputNameDataOffline();
					}
				}
			} else {
				parent.showDialog(parent
						.getString(R.string.TEXT_ENABLE_NETWORK_DOWNLOAD_DATA_MAP));
			}
		}
	}
	
	public void moveMapTo(LatLng latlng, float zoomLevel) {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latlng).zoom(zoomLevel).build();
		
		if (googleMap != null) {
			googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		}
	}
	
	public void hide() {
		layoutPanelDownload.setVisibility(View.GONE);
	}
	
	public void show() {
		layoutPanelDownload.setVisibility(View.VISIBLE);
	}
}
