
/*******************************************************************************
 * Copyright 2014 Maurice WohlkAJnig
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.ths.map;


import java.io.ByteArrayOutputStream;
import java.io.File;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.TileProvider;
import com.ths.dmscore.util.MyLog;
import com.ths.universalimageloader.MapImageLoader;

/**
 * <p>Google Maps Android V2 tile overlay provider for cached URL tiles. Caches in memory and/or on disk.</p>
 * <p>Uses the famous <a href="https://github.com/nostra13/Android-Universal-Image-Loader/">Android Universal Image Loader</a> library for downloading and caching.</p>
 * <p>Usage example:<pre>
googleMap.addTileOverlay(new CachingUrlTileProvider(this, 256, 256) {
    @Override
    public String getTileUrl(int x, int y, int z) {
        return String.format("https://a.tile.openstreetmap.org/%3$s/%1$s/%2$s.png",x,y,z);
    }
}.createTileOverlayOptions());
 * </pre></p>
 */
public abstract class CachingUrlTileProvider implements TileProvider {

    private final int mTileWidth;
    private final int mTileHeight;

    public CachingUrlTileProvider(int mTileWidth, int mTileHeight) {
        this.mTileWidth = mTileWidth;
        this.mTileHeight = mTileHeight;
    }

    @Override
    public Tile getTile(int x, int y, int z) {
        byte[] tileImage = getTileImage(x, y, z);
        if (tileImage != null) {
        	//de anh nguyen goc 256 x 256
            return new Tile(mTileWidth, mTileHeight, tileImage);
        }
        return NO_TILE;
    }

    /**
     * Synchronously loads the requested Tile image either from cache or from the web.</p>
     * Background threading/pooling is done by the google maps api so we can do it all synchronously.
     *
     * @param x x coordinate of the tile
     * @param y y coordinate of the tile
     * @param z the zoom level
     * @return byte data of the image or <i>null</i> if the image could not be loaded.
     */
    private byte[] getTileImage(int x, int y, int z) {
        String tileUrl = getTileUrl(x, y, z);
        MyLog.d("getTileImage", "tileUrl" + tileUrl);
        try {
        	//chi lay nhung file co cache de hien thi, khong
        	File file = MapImageLoader.getInstance().getDiscCache().get(tileUrl);
	        if (file != null && file.exists()) {
	        	Bitmap bitmap = MapImageLoader.getInstance().loadImageSync(tileUrl);
	        	if (bitmap == null) {
	        		MyLog.d("getTileImage", "bitmap null");
	        		return null;
	        	}
	        	MyLog.d("getTileImage", "bitmap not null");
	        	ByteArrayOutputStream stream = new ByteArrayOutputStream();
	        	bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
	        	return stream.toByteArray();
	        }
        } catch (Exception e) {
        }
        return null;
    }

    public TileOverlayOptions createTileOverlayOptions() {
        TileOverlayOptions tileOverlayOptions = new TileOverlayOptions().tileProvider(this);
        tileOverlayOptions.fadeIn(true);
        return tileOverlayOptions;
    }

    /**
     * Return the url to your tiles. For example:
     * <pre>
		public String getTileUrl(int x, int y, int z) {
		     return String.format("https://a.tile.openstreetmap.org/%3$s/%1$s/%2$s.png",x,y,z);
		}
     </pre>
     * See <a href="http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames">http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames</a> for more details
     *
     * @param x x coordinate of the tile
     * @param y y coordinate of the tile
     * @param z the zoom level
     * @return the url to the tile specified by the parameters
     */
    public abstract String getTileUrl(int x, int y, int z);
}