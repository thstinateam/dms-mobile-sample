/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.viettel.maps.base.LatLng;


/**
 * BaseFragmentMapView.java
 * @author: dungdq3
 * @version: 1.0
 * @since:  10:14:49 AM Aug 5, 2014
 */
public abstract class BaseFragmentMapView {
	protected GlobalBaseActivity parent;
	protected MapEventListener listener;
	protected RelativeLayout rlMainMapView;
	protected boolean isViewMyPositionControl = true;
	Object marker = null;

	public enum MarkerPosition{
		TOP_LEFT,
	}

	public interface MapEventListener {
		public boolean onSingleTap(LatLng latlng, Point point);
		public boolean onLongTap(LatLng latlng, Point point);
		public boolean onDoubleTap(LatLng latlng, Point point);
		public void onClickMarkerMyPositionEvent();
	}

	public void setMapEventListener(MapEventListener listener){
		this.listener = listener;
	}

	public BaseFragmentMapView(GlobalBaseActivity parent){
		this.parent = parent;
	}

	public abstract ViewGroup initMap(Bundle savedInstanceState);
	public abstract void initZoomControl();
	public abstract void setZoomBorder();
	public abstract void initMyPositionControl();
	public abstract void drawMarkerMyPosition();
	public abstract void moveToCenterMyPosition();
	public abstract void moveMapTo(double lat, double lng);
	public abstract void drawMarkerMyPosition(double lat, double lng);
	public abstract void fitBounds(ArrayList<LatLng> arrayPosition);
	public abstract void clearAllCurrentMarker();
	public abstract void showMarkerInMap(double lat, double lng, View view, MarkerPosition position);
	public abstract String requestGetCusLocation(final double lat, final double lng);
	public abstract void invalidate();

	public void clearMapLayer(OverlayViewLayer layer) {
	}
	public void addMapLayer(OverlayViewLayer layer) {
	}

	/**
	 * @author: cuonglt3
	 * @param lng
	 * @param lat
	 * @param seq
	 * @param drawable
	 * @param numVisitFact
	 * @since: 9:57:00 AM Aug 7, 2014
	 * @return: Object
	 * @throws:
	 * @return
	 */
	public abstract Object addMarker(double lat, double lng, int drawable, MarkerAnchor markerArchor, String seq, String numVisitFact, int colorSeq, int colorNumVisit);

	/**
	 * @author: cuonglt3
	 * @since: 11:09:19 AM Aug 7, 2014
	 * @return: void
	 * @throws:
	 */
	public abstract void onMapLoaded();

	/**
	 * @author: cuonglt3
	 * @since: 8:29:29 AM Aug 8, 2014
	 * @return: void
	 * @throws:
	 */
	public abstract void clearAllMarkerOnMap();
	public abstract void setMarkerOnClickListener(Object Marker, OnEventMapControlListener listener, int action, Object userData);

	/**
	 * @author: cuonglt3
	 * @since: 9:20:32 AM Aug 8, 2014
	 * @return: View
	 * @throws:
	 * @return
	 */
	public abstract Object getMapView();

	/**
	 * @author: cuonglt3
	 * @since: 9:36:37 AM Aug 8, 2014
	 * @return: void
	 * @throws:
	 * @param data
	 */
	public abstract View showPopupWhenClickMarker(View view, OverlayViewOptions opts);

	/**
	 * @author: cuonglt3
	 * @since: 11:15:32 AM Aug 8, 2014
	 * @return: void
	 * @throws:
	 */
	public abstract void clearPopup();
	public abstract void getAdressFromLatLng(GetAddressListener listener, LatLng latlng);

	public abstract void onResume();
	public abstract void onPause();
	public abstract void onDestroy();
	public abstract void onLowMemory();
	public abstract View addMarker(View view, OverlayViewOptions opts);
	public abstract void drawRoute(RouteInfo routeInfo);
	public abstract void hideRoute();
	public abstract void showRoute();
	public abstract boolean isHaveRouteData();
	
	public static class RouteNodeInfo{
		public double lat;
		public double lng;
		public int drawableNode;
		public String title;
		public String description;
	}
	
	public static class RouteInfo{
		public List<RouteNodeInfo> listNode = new ArrayList<BaseFragmentMapView.RouteNodeInfo>();
		public int colorRoute;
		public int width;
	}
	
	public static enum MarkerAnchor{
		BOTTOM_CENTER,
		CENTER,
		BOTTOM_LEFT;
	}
}
