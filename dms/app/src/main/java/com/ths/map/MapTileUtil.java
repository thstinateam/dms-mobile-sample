package com.ths.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.ths.dmscore.util.StringUtil;
import com.ths.universalimageloader.MapImageLoader;

public class MapTileUtil {

	public static final int MAX_ZOOM_DOWNLOAD = 20;
	public static final int COUNT_ZOOM_DOWNLOAD = 4;
	public static final int MIN_ZOOM_DOWNLOAD = 11;

	public static class TileInfo{
		public int x;
		public int y;
		public int zoom;
		
		public TileInfo(int x, int y, int zoom) {
			this.x = x;
			this.y = y;
			this.zoom = zoom;
		}
		
		@Override
		public String toString() {
			return String.format("x: %d y:%d z:%d", x, y, zoom);
		}
	}
	
	public static class MapDataInfo implements Serializable{
		private static final long serialVersionUID = 1L;
		public double lat;
		public double lng;
		public float zoom;
		public String name;
		
		public MapDataInfo(String name, double lat, double lng, float zoom) {
			this.name = name;
			this.lat = lat;
			this.lng = lng;
			this.zoom = zoom;
		}
		
		@Override
		public String toString() {
			String str = "Unknown data";
			if (!StringUtil.isNullOrEmpty(name)) {
				str = String.valueOf(name);
			}
			return str;
		}
	}
	
	
	public static TileInfo getTileNumber(final double lat, final double lon,
			final int zoom) {
		int xtile = (int) Math.floor((lon + 180) / 360 * (1 << zoom));
		int ytile = (int) Math
				.floor((1 - Math.log(Math.tan(Math.toRadians(lat)) + 1
						/ Math.cos(Math.toRadians(lat)))
						/ Math.PI)
						/ 2 * (1 << zoom));
		if (xtile < 0){
			xtile = 0;
		}
		if (xtile >= (1 << zoom)){
			xtile = ((1 << zoom) - 1);
		}
		if (ytile < 0){
			ytile = 0;
		}
		if (ytile >= (1 << zoom)){
			ytile = ((1 << zoom) - 1);
		}
		return new TileInfo(xtile, ytile, zoom);
	}

	static double tile2lon(int x, int z) {
		return x / Math.pow(2.0, z) * 360.0 - 180;
	}

	static double tile2lat(int y, int z) {
		double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, z);
		return Math.toDegrees(Math.atan(Math.sinh(n)));
	}

	public static List<TileInfo> getListDownloadCache(LatLng p1, LatLng p2, int zoom, int levelZoom){
		zoom = zoom - 1;
		//vi zoom download bat dau tu 0
		List<TileInfo> lstDownloadCache = new ArrayList<TileInfo>();
		int countZoom = Math.min(levelZoom, COUNT_ZOOM_DOWNLOAD);
		//lay zoom hien tai + so level duoc zoom
		int maxZoom = Math.min(zoom + countZoom, MAX_ZOOM_DOWNLOAD);
		boolean isFirst = true;
		while (zoom <= maxZoom) {
			TileInfo tileInfo1 = MapTileUtil.getTileNumber(p1.latitude, p1.longitude, zoom);
			TileInfo tileInfo2 = MapTileUtil.getTileNumber(p2.latitude, p2.longitude, zoom);
			//neu la vung ngoai cung thi load them vong ben ngoai +- 1
			int minX = Math.min(tileInfo1.x, tileInfo2.x) - (isFirst ? 1 : 0);  
			int maxX = Math.max(tileInfo1.x, tileInfo2.x) + (isFirst ? 1 : 0);  
			int minY = Math.min(tileInfo1.y, tileInfo2.y) - (isFirst ? 1 : 0);  
			int maxY = Math.max(tileInfo1.y, tileInfo2.y) + (isFirst ? 1 : 0);
			
			for (int i = minX; i <= maxX; i++) {
				for (int j = minY; j <= maxY; j++) {
					TileInfo tileInfoNew = new TileInfo(i, j, zoom);
					lstDownloadCache.add(tileInfoNew);
				}
			}
			zoom ++;
			isFirst = false;
		}
		return lstDownloadCache;
	}
	
    public static void downloadCache(List<TileInfo> lstDownloadCache, ImageLoadingListener listener){
    	if (lstDownloadCache != null) {
    		for (TileInfo tileInfo : lstDownloadCache) {
    			String urlTileGoogleMap = MapTileUtil.getUrlTileGoogleMap(tileInfo.x, tileInfo.y, tileInfo.zoom);
    			MapImageLoader.getInstance().loadImage(urlTileGoogleMap, listener);
    		}
		}
    }
    
/*    public static int downloadCache(LatLng p1, LatLng p2, int zoom){
    	int countFail = 0;
    	int maxZoom = 20;
    	while (zoom <= maxZoom) {
    		TileInfo tileInfo1 = MapTileUtil.getTileNumber(p1.latitude, p1.longitude, zoom);
    		TileInfo tileInfo2 = MapTileUtil.getTileNumber(p2.latitude, p2.longitude, zoom);
    		int minX = Math.min(tileInfo1.x, tileInfo2.x);  
    		int maxX = Math.max(tileInfo1.x, tileInfo2.x);  
    		int minY = Math.min(tileInfo1.y, tileInfo2.y);  
    		int maxY = Math.max(tileInfo1.y, tileInfo2.y);
    		
    		for (int i = minX; i <= maxX; i++) {
    			for (int j = minY; j <= maxY; j++) {
    				String urlTileGoogleMap = MapTileUtil.getUrlTileGoogleMap(i, j, zoom);
    				Bitmap bitmap = MapImageLoader.getInstance().loadImageSync(urlTileGoogleMap);
    				if (bitmap == null) {
    					MyLog.d("downloadCache", "bitmap fail " + urlTileGoogleMap);
    					countFail ++;
    				} else{
    					MyLog.d("downloadCache", "bitmap success " + urlTileGoogleMap);
    				}
    			}
    		}
    		zoom ++;
    	}
    	return countFail;
    }
*/    
    public static String getUrlTileGoogleMap(int x, int y, int z){
    	return String.format("http://mt3.google.com/vt/lyrs=m&x=%1$s&y=%2$s&z=%3$s&s=Ga", x, y, z);
    }
}
