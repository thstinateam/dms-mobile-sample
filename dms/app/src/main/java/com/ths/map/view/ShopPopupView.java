package com.ths.map.view;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class ShopPopupView extends LinearLayout implements OnClickListener, View.OnTouchListener {
	public static final int POP_UP_CLOSE = 2001;

	TextView tvShopName;
	TextView tvShopCode;
	TextView tvShopAddress;
	ImageView ivDelete; // dong poupup
	OnEventControlListener lis;
	public int widthPopup;

	public ShopPopupView(Context context, AttributeSet a) {
		super(context, a);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public ShopPopupView(Context context, OnEventControlListener lis) {
		super(context);
		// TODO Auto-generated constructor stub
		this.lis = lis;
		setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.layout_shop_on_map, this);
		tvShopName = (TextView) v.findViewById(R.id.tvShopName);
		tvShopCode = (TextView) v.findViewById(R.id.tvShopCode);
		tvShopAddress = (TextView) v.findViewById(R.id.tvShopAddress);
		ivDelete = (ImageView) v.findViewById(R.id.ivDelete);
		ivDelete.setOnClickListener(this);
		setOnTouchListener(this);
		widthPopup = getWidthOfPopup();
	}
	
	public void setInfo(String shopName, String shopCode, String shopAddress){
		tvShopName.setText(shopName);
		tvShopCode.setText(StringUtil.getString(R.string.TEXT_CODE)+": " + shopCode);
		tvShopAddress.setText(StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS)+": " + shopAddress);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthPopup, heightMeasureSpec);
	}

	public int getWidthOfPopup() {
		Rect bounds = new Rect();
		Paint textPaint = tvShopAddress.getPaint();
		textPaint.getTextBounds(tvShopAddress.getText().toString(), 0, tvShopAddress.getText().toString().length(), bounds);
		int width = bounds.width();
		return width + 40;
	}

	@Override
	public void onClick(View v) {
		if (v == ivDelete) {
			lis.onEvent(POP_UP_CLOSE, ivDelete, null);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean onTouch = false;
		if (v == this
				&& event.getAction() == MotionEvent.ACTION_DOWN) {
			int x = (int)event.getX();
			int y = (int)event.getY();
			if (touchOnButtonClose(x, y)) {
				lis.onEvent(POP_UP_CLOSE, ivDelete, null);
			}
		}
		return onTouch;
	}
	
	/** Kiem tra toa do co nam trong button Close khong
	 * @author: trungnt61
	 * @since: 09:59:08 13-08-2014
	 * @return: boolean
	 * @throws:  
	 * @param x
	 * @param y
	 * @return
	 */
	boolean touchOnButtonClose(int x, int y) {
		int btnHeight = GlobalUtil.dip2Pixel((int)(40)) + GlobalUtil.dip2Pixel(20) ;
		int popUpWidth = this.getWidth();
		//int popupHeight = this.getHeight();
		int btnTop = 0;
		int btnWidth = GlobalUtil.dip2Pixel((int)(40)) + GlobalUtil.dip2Pixel(20);
		int btnLeft = popUpWidth - btnWidth;
		
		if (x>=btnLeft 
				&& x <= (btnWidth + btnLeft)
				&& y >= btnTop
				&& y <= (btnTop + btnHeight)) {
			return true;
		}
		
		return false;
	}
}
