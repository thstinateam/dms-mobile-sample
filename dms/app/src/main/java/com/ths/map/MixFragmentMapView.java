/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.map.view.MarkerMapView;
import com.ths.map.view.TextMarkerMapView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;
import com.viettel.maps.MapView;
import com.viettel.maps.MapView.MapEventListener;
import com.viettel.maps.base.LatLng;
import com.viettel.maps.base.LatLngBounds;
import com.viettel.maps.controls.BaseControl;
import com.viettel.maps.controls.ZoomControl;

/**
 * class bao gom googlemap va viettel map
 *
 * @author: dungdq3
 * @version: 1.0
 * @since:  10:55:50 AM Feb 25, 2015
 */
public class MixFragmentMapView extends RelativeLayout implements OnMapClickListener, MapEventListener, OnMarkerClickListener {

	private final GlobalBaseActivity con;

	private MapView mapView;

	private OverlayViewOptions markerCustomerOpts;
	private MarkerMapView markerCustomer;
	private OverlayViewLayer markerCustomerLayer;
	//cau hinh su dung google map hay vtmap
	protected boolean isUsingGoogleMap;
	//Google map
	GoogleMap googleMap;
	//map Fragment cua Google map
	protected static volatile MapFragment mapFragment;
	//Marker vi tri cua toi
	private Marker markerGoogle;
	//flag marker vi tri khach hang
	private Marker flagMarker;
	private double cusLat = -1;
	private double cusLng = -1;
	private ArrayList<Marker> listMarker;
//	private InfoWindowAdapter adapter;
	private HashMap<String, ListenerFromView> hash;
	private View infoWindowView = null;
	private Marker lastOpenedMarker = null;

	class ListenerFromView{
		BaseFragment view;
		OnEventMapControlListener listener;
		int action;
		Object data;
	}

	public MixFragmentMapView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		con = (GlobalBaseActivity) context;
		initView();
	}

	public MixFragmentMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		con = (GlobalBaseActivity) context;
		initView();
	}

	public MixFragmentMapView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		con = (GlobalBaseActivity) context;
		initView();
	}

	/**
	 * init view
	 *
	 * @author: dungdq3
	 * @since: 9:51:19 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 */
	private void initView() {
		// TODO Auto-generated method stub
		listMarker = new ArrayList<Marker>();
		if(GlobalInfo.getInstance().getMapTypeIsUse() == 2){
			isUsingGoogleMap = true;
		}
		if (isUsingGoogleMap) {
			initGoogleMap();
		} else {
			initVTMap();
		}

		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (isUsingGoogleMap) {
			drawMyLocation(lat, lng);
		} else{
			LinearLayout llMyPosition;
			llMyPosition = new LinearLayout(mapView.getContext());
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5));
			llMyPosition.setLayoutParams(lp);
			llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
			llMyPosition.setOrientation(LinearLayout.HORIZONTAL);
			ImageView ivMyPosition = new ImageView(con);
			ivMyPosition.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5));
			ivMyPosition.setImageResource(R.drawable.icon_location_2);
			ivMyPosition
					.setBackgroundResource(R.drawable.custom_button_with_border);
			ivMyPosition.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					moveToCenterMyPosition();
				}
			});
			llMyPosition.addView(ivMyPosition);
			addView(llMyPosition);
			drawMyLocation(lat, lng);
		}
	}

	/**
	 * init VT map
	 * @author: dungdq3
	 * @since: 08:07:47 09-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initVTMap() {
		mapView = new MapView(con);
		mapView.setMapKey(GlobalInfo.getVIETTEL_MAP_KEY());
		mapView.setZoomControlEnabled(true);
		mapView.setMapTypeControlEnabled(false);
		mapView.setMapEventListener(this);
		addView(mapView);
		markerCustomerOpts = new OverlayViewOptions();
		markerCustomer = new MarkerMapView(con, R.drawable.icon_flag);
		markerCustomer.setVisibility(View.GONE);
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
		markerCustomerLayer = new OverlayViewLayer();
		mapView.addLayer(markerCustomerLayer);
		markerCustomerLayer.addItemObj(markerCustomer, markerCustomerOpts);
		ZoomControl zoomControl = (ZoomControl) mapView
				.getControl(BaseControl.TYPE_CONTROL_ZOOM);
		if (zoomControl != null) {
			mapView.removeView(zoomControl);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			zoomControl.setLayoutParams(lp);
			zoomControl.setGravity(Gravity.LEFT | Gravity.BOTTOM);
			addView(zoomControl);
		}
	}

	/**
	 * init Google map
	 * @author: hoanpd1
	 * @since: 08:09:45 09-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initGoogleMap() {
		final MapWrapperLayout bgMapView = new MapWrapperLayout(con);
//		bgMapView.setBackgroundColor(ImageUtil.getColor(R.color.TRANSPARENT));
		bgMapView.setBackgroundColor(Color.rgb(237, 234, 226));
		bgMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =  inflater.inflate(R.layout.layout_map_view, bgMapView);
		hash= new HashMap<String, ListenerFromView>();
		mapFragment = ((MapFragment) con.getFragmentManager().findFragmentById(R.id.ggMap));
		googleMap = mapFragment.getMap();
		addView(bgMapView);

		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		googleMap.getUiSettings().setMyLocationButtonEnabled(false);
		googleMap.setMyLocationEnabled(false);
//		googleMap.setInfoWindowAdapter(this.adapter);
		googleMap.setOnMarkerClickListener(this);

		googleMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			private ListenerFromView listenerFromView;
//			private ViewGroup view1;
			@Override
			public View getInfoWindow(Marker marker) {
				if(hash != null){
				 listenerFromView = hash.get(marker.getId());
					if(listenerFromView != null){
						infoWindowView = listenerFromView.listener.onEventMap(listenerFromView.action, infoWindowView, listenerFromView.data);
					}
				}
				if (infoWindowView == null) {
					return null;
				}

				lastOpenedMarker = marker;

				bgMapView.setMarkerWithInfoWindow(marker, infoWindowView);

				return infoWindowView;
			}

			@Override
			public View getInfoContents(Marker marker) {
				// TODO Auto-generated method stub
				return null;
			}
		});
		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new com.google.android.gms.maps.model.LatLng(lat, lng)).zoom(12f).build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new com.google.android.gms.maps.model.LatLng(lat, lng), 12f));
		googleMap.setOnMapClickListener(this);

//		googleMap.setOnMapLoadedCallback(this);

		initZoomControl();

		initMyPositionControl();


	}

	/**
	 * add control my position cho Google map
	 * @author: cuonglt3
	 * @since: 08:10:00 09-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initMyPositionControl() {
		// TODO Auto-generated method stub
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(con);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		llMyPosition.setLayoutParams(lp);
		llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ImageView ivMyPosition = new ImageView(con);
		ivMyPosition.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivMyPosition.setImageResource(R.drawable.icon_location_2);
		ivMyPosition.setBackgroundResource(R.drawable.custom_button_with_border);
		ivMyPosition.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				drawMyLocation();
				moveToCenterMyPosition();
			}
		});
		llMyPosition.addView(ivMyPosition);
		addView(llMyPosition);
	}

	/**
	 * move zoom control Google map qua ben trai
	 * @author: hoanpd1
	 * @since: 08:10:14 09-03-2015
	 * @return: void
	 * @throws:
	 */
	public void initZoomControl() {
		View zoomControls = null;
		zoomControls = mapFragment.getView().findViewById(0x1);

		if (zoomControls != null
				&& zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
			// ZoomControl is inside of RelativeLayout
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls
					.getLayoutParams();

			// Align it to - parent top|left
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			// Update margins, set to 10dp
			final int margin = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 10, con.getResources()
							.getDisplayMetrics());
			params.setMargins(margin, margin, margin, margin);
		}
	}

	/**
	 * ve vi tri cua toi
	 *
	 * @author: dungdq3
	 * @since: 5:01:56 PM Jan 13, 2015
	 * @return: void
	 * @throws:
	 */
	public void drawMyLocation(double lat, double lng) {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			if (markerGoogle == null) {
				markerGoogle = drawOneMarkerGoogleMap(lat, lng, R.drawable.icon_location);
			}
		} else {
			LatLng latLng = new LatLng(lat, lng);
			// Move mapview to location image
			mapView.moveTo(latLng);
			// Add marker
			MarkerMapView maker = new MarkerMapView(con,
					R.drawable.icon_location);
			OverlayViewOptions opts = new OverlayViewOptions();
			opts.position(latLng);
			opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
			OverlayViewLayer myPosLayer = new OverlayViewLayer();
			// mapView.removeAllViews();
			mapView.addLayer(myPosLayer);
			myPosLayer.addItemObj(maker, opts);
		}
	}

	/**
	 * Ve maker voi toa do va icon truyen vao
	 * @author: hoanpd1
	 * @since: 18:07:21 10-03-2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	public void drawOneMarkerOnMap(double lat, double lng, int resource) {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			drawOneMarkerGoogleMap(lat, lng, resource);
		} else {
			LatLng latLng = new LatLng(lat, lng);
			// Add marker
			MarkerMapView maker = new MarkerMapView(con, resource);
			OverlayViewOptions opts = new OverlayViewOptions();
			opts.position(latLng);
			opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
			OverlayViewLayer myPosLayer = new OverlayViewLayer();
			// mapView.removeAllViews();
			// Move mapview to location image
			//mapView.moveTo(latLng);
			mapView.addLayer(myPosLayer);
			myPosLayer.addItemObj(maker, opts);
		}
	}

	@Override
	public void onBoundChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCenterChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onDoubleTap(LatLng arg0, Point arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onLongTap(LatLng arg0, Point arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onSingleTap(LatLng latLng, Point p) {
		// TODO Auto-generated method stub
		showLocationFlag(latLng.getLatitude(), latLng.getLongitude(), false);
		return true;
	}

	/**
	 * show market flag
	 *
	 * @author: dungdq3
	 * @since: 5:32:57 PM Jan 12, 2015
	 * @return: void
	 * @throws:
	 */
	public void showLocationFlag(double lat, double lng, boolean view) {
		cusLat = lat;
		cusLng = lng;
		if (isUsingGoogleMap) {
			if (flagMarker == null) {
				flagMarker = drawOneMarkerGoogleMap(lat, lng, R.drawable.icon_flag);
				flagMarker.setAnchor(0f, 1f);
			} else {
				flagMarker.setPosition(new com.google.android.gms.maps.model.LatLng(lat, lng));
			}
			if(view) {
				googleMap.setOnMapClickListener(null);
			}
		}else{
			// TODO Auto-generated method stub
			markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
			LatLng latLng = new LatLng(lat, lng);
			markerCustomerOpts.position(latLng);
			//chi hien khi co vi tri
			if (lat > 0 && lng > 0) {
				//hien thi marker cua khach hang
				markerCustomer.setVisibility(View.VISIBLE);

			} else {
				//khong co vi tri thi an
				markerCustomer.setVisibility(View.GONE);
			}
			if(view) {
				mapView.setMapEventListener(null);
			}
			mapView.invalidate();
		}
	}

	/**
	 * di toi vi tri cua toi
	 *
	 * @author: dungdq3
	 * @since: 5:08:54 PM Jan 13, 2015
	 * @return: void
	 * @throws:
	 */
	private void moveToCenterMyPosition() {
		// TODO Auto-generated method stub
		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (lat > 0 && lng > 0) {
			if (isUsingGoogleMap) {
				com.google.android.gms.maps.model.LatLng myLocation1 = new com.google.android.gms.maps.model.LatLng(lat,
						lng);
				CameraUpdate cu = CameraUpdateFactory.newLatLng(myLocation1);
				//ve lai marker mylocation
				markerGoogle.setPosition(myLocation1);
				googleMap.animateCamera(cu);
			}else{
				LatLng myLocation = new LatLng(lat, lng);
				mapView.moveTo(myLocation);
				mapView.invalidate();
			}
		}
	}

	/**
	 * move to position
	 *
	 * @author: dungdq3
	 * @since: 10:01:25 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	public void moveToMyPosition(double lat, double lng) {
		if (lat > 0 && lng > 0) {
			if (isUsingGoogleMap) {
				com.google.android.gms.maps.model.LatLng myLocation1 = new com.google.android.gms.maps.model.LatLng(lat,
						lng);
				CameraUpdate cu = CameraUpdateFactory.newLatLng(myLocation1);
				//ve lai marker mylocation
				markerGoogle.setPosition(myLocation1);
				googleMap.animateCamera(cu);
			}else{
				LatLng myLocation = new LatLng(lat, lng);
				mapView.moveTo(myLocation);
				mapView.invalidate();
			}
		}
	}

	/**
	 * add new control
	 *
	 * @author: dungdq3
	 * @since: 10:02:47 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param v
	 */
	public void addControl(View v){
		addView(v);
	}

	@Override
	public void onMapClick(com.google.android.gms.maps.model.LatLng point) {
		// TODO Auto-generated method stub
		showLocationFlag(point.latitude, point.longitude, false);
	}

	/**
	 * xoa layer viettel map
	 *
	 * @author: dungdq3
	 * @since: 9:44:58 AM Mar 6, 2015
	 * @return: void
	 * @throws:
	 * @param layer
	 */
	public void clearMapLayerViettel(OverlayViewLayer layer){
		if (layer != null) {
			int size =  layer.getMapObjectTotal();
			for (int i = 0; i < size; i++) {
				OverlayViewItemObj obj = (OverlayViewItemObj) layer.getMapObject(i);
				mapView.removeView(obj.getView());
			}
			layer.clear();
		}
	}

	/**
	 * fit bound for zoom
	 *
	 * @author: dungdq3
	 * @since: 10:07:52 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param arrayPosition
	 */
	public void fitBoundsViettelMap(ArrayList<LatLng> arrayPosition) {
		if (!isUsingGoogleMap && arrayPosition != null && arrayPosition.size() > 0) {
			LatLngBounds bound = null;
			for (com.viettel.maps.base.LatLng latlng : arrayPosition) {
				bound = getBound(bound, latlng.clone());
			}

			mapView.fitBounds(bound, true);
		}
	}

	/**
	 * get bound cho map
	 *
	 * @author: dungdq3
	 * @since: 10:07:42 AM Feb 25, 2015
	 * @return: LatLngBounds
	 * @throws:
	 * @param bound
	 * @param latLng
	 * @return
	 */
	private LatLngBounds getBound(LatLngBounds bound, LatLng latLng) {
		if (bound == null) {
			bound = new LatLngBounds();
			bound.setSouthWest(latLng);
			bound.setNorthEast(latLng);
		} else {
			int lat = latLng.getLatitudeE6();
			int lng = latLng.getLongitudeE6();
			if (bound.getSouthWest().getLatitudeE6() > lat)
				bound.getSouthWest().setLatitudeE6(lat);
			if (bound.getNorthEast().getLatitudeE6() < lat)
				bound.getNorthEast().setLatitudeE6(lat);
			if (bound.getSouthWest().getLongitudeE6() > lng)
				bound.getSouthWest().setLongitudeE6(lng);
			if (bound.getNorthEast().getLongitudeE6() < lng)
				bound.getNorthEast().setLongitudeE6(lng);
		}
		return bound;
	}

	/**
	 * fit bound google map
	 *
	 * @author: dungdq3
	 * @since: 10:32:59 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param arrayPosition
	 */
	public void fitBoundGoogleMap(ArrayList<com.google.android.gms.maps.model.LatLng> arrayPosition){
		if (isUsingGoogleMap && arrayPosition != null
				&& arrayPosition.size() > 0) {
			com.google.android.gms.maps.model.LatLngBounds.Builder builder = new com.google.android.gms.maps.model.LatLngBounds.Builder();
			for (com.google.android.gms.maps.model.LatLng latLng : arrayPosition) {
				builder.include(latLng);
			}
			com.google.android.gms.maps.model.LatLngBounds bounds = builder
					.build();
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
					bounds, 16);
			googleMap.animateCamera(cameraUpdate);
		}
//		googleMap.moveCamera(cameraUpdate);
	}


	/**
	 * ve danh sach cac marker tuong ung location
	 *
	 * @author: dungdq3
	 * @since: 10:55:51 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param arrayPosition
	 * @param resource
	 */
	public void drawMarker(ArrayList<com.google.android.gms.maps.model.LatLng> arrayPosition, int resource){
		if (googleMap != null) {
			for (com.google.android.gms.maps.model.LatLng latLng : arrayPosition) {
				addMarkerIntoList(latLng.latitude, latLng.longitude, resource);
			}
		}
	}

	/**
	 * ve 1 marker
	 *
	 * @author: dungdq3
	 * @since: 2:13:01 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 * @param resource
	 */
	public Marker drawOneMarkerGoogleMap(double lat, double lng, int resource){
		return googleMap.addMarker(new MarkerOptions().position(
				new com.google.android.gms.maps.model.LatLng(lat, lng)).icon(
				BitmapDescriptorFactory.fromResource(resource)));
	}

	/**
	 * clear marker google map
	 *
	 * @author: dungdq3
	 * @since: 10:55:35 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param arrayPosition
	 */
	public void clearMarker(ArrayList<com.google.android.gms.maps.model.LatLng> arrayPosition) {
		for (com.google.android.gms.maps.model.LatLng latLng : arrayPosition) {
			clearOneMarker(latLng.latitude, latLng.longitude);
		}
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		// TODO Auto-generated method stub
		// Check if there is an open info window
		boolean click = false;
		if (!marker.equals(markerGoogle)) {
			if (lastOpenedMarker != null) {
				// Close the info window
				lastOpenedMarker.hideInfoWindow();

				// Is the marker the same marker that was already open
				if (lastOpenedMarker.equals(marker)) {
					// Nullify the lastOpened object
					lastOpenedMarker = null;
					// Return so that the info window isn't opened again
					click  = true;
					return click;
				}
			}

			// Re-assign the last opened such that we can close it later
			lastOpenedMarker = marker;

			// Open the info window for the marker
			marker.showInfoWindow();

			// Event was handled by our code do not launch default behaviour.
			moveCameraAfterShowInfowindow(marker, 80);
			click = true;
		} else {
			clearPopUp();
		}
		return true;
	}

	/**
	 * move camera xuong
	 *
	 * @author: dungdq3
	 * @since: 11:07:55 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param marker
	 * @param pixel
	 */
	public void moveCameraAfterShowInfowindow(Marker marker, final int pixel) {
		if (googleMap != null) {
			Projection projection = googleMap.getProjection();
			com.google.android.gms.maps.model.LatLng markerLocation = marker.getPosition();
			Point screenPosition = projection.toScreenLocation(markerLocation);
			screenPosition.y -= pixel;
			com.google.android.gms.maps.model.LatLng latlng = projection.fromScreenLocation(screenPosition);
			CameraUpdate cu = CameraUpdateFactory.newLatLng(latlng);
			googleMap.animateCamera(cu);
		}
	}

	/**
	 * add layer for viettel map
	 *
	 * @author: dungdq3
	 * @since: 11:09:29 AM Feb 25, 2015
	 * @return: void
	 * @throws:
	 * @param layer
	 */
	public void addLayer(OverlayViewLayer layer){
		if(mapView != null && !isUsingGoogleMap)
			mapView.addLayer(layer);
	}

	/**
	 * set textmarker in view
	 *
	 * @author: dungdq3
	 * @since: 1:41:53 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 * @param drawable
	 * @param lat
	 * @param lng
	 * @param seqInPlan
	 * @param name
	 * @param colorSeq
	 * @param colorName
	 */
	public Marker setTextMarkerMapView(int drawable, double lat, double lng,
			String seqInPlan, String name, int colorSeq, int colorName){
		TextMarkerMapView markerIcon = new TextMarkerMapView(con, drawable,
				seqInPlan, name, colorSeq, colorName);
		Marker mk = addMarkerToMap(markerIcon, lat, lng);
		listMarker.add(mk);
		return mk;
	}

	/**
	 * set textmarker in view
	 * @author: hoanpd1
	 * @since: 18:03:45 06-03-2015
	 * @return: Marker
	 * @throws:
	 * @param lat
	 * @param lng
	 * @param name
	 * @param colorName
	 * @param drawable
	 * @return
	 */
	public Marker setTextMarkerMapView(double lat, double lng, String name, int colorName, int drawable){
		TextMarkerMapView markerIcon = new TextMarkerMapView(con, drawable, name, colorName);
		Marker mk = addMarkerToMap(markerIcon, lat, lng);
		listMarker.add(mk);
		return mk;
	}

	/**
	 * add marker into map
	 *
	 * @author: dungdq3
	 * @since: 1:41:39 PM Feb 26, 2015
	 * @return: Marker
	 * @throws:
	 * @param view
	 * @param lat
	 * @param lng
	 * @return
	 */
	private Marker addMarkerToMap(View view, double lat, double lng) {
		Marker marker = null;
		if (googleMap != null) {
			marker = googleMap.addMarker(new MarkerOptions()
			.position(new com.google.android.gms.maps.model.LatLng(lat, lng))
			.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(con, view)))
					);
		}
		return marker;
	}

	/**
	 * tao drawable from view
	 *
	 * @author: dungdq3
	 * @since: 1:41:27 PM Feb 26, 2015
	 * @return: Bitmap
	 * @throws:
	 * @param context
	 * @param object
	 * @return
	 */
	private Bitmap createDrawableFromView(Context context, Object object) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		View view = (View) object;
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);
		return bitmap;
	}

	/**
	 * fit bound for google map
	 *
	 * @author: dungdq3
	 * @since: 1:41:11 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 * @param arrayPosition
	 */
	public void fitBound(ArrayList<LatLng> arrayPosition) {
		if(arrayPosition != null && arrayPosition.size() > 0) {
			if (!isUsingGoogleMap){ // viettel map
				fitBoundsViettelMap(arrayPosition);
			} else {
				ArrayList<com.google.android.gms.maps.model.LatLng> arrayPositionGG = new ArrayList<com.google.android.gms.maps.model.LatLng>();
				for(LatLng latLng : arrayPosition) {
					com.google.android.gms.maps.model.LatLng latLngGG = new com.google.android.gms.maps.model.LatLng(latLng.getLatitude(), latLng.getLongitude());
					arrayPositionGG.add(latLngGG);
				}
				fitBoundGoogleMap(arrayPositionGG);
			}
		}
	}

	/**
	 * repaint lai view
	 *
	 * @author: dungdq3
	 * @since: 1:40:59 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 */
	public void repaint(){
		initView();
	}

	/**
	 * xoa tat ca marker
	 *
	 * @author: dungdq3
	 * @since: 1:53:32 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 */
	public void clearAllMarker(){
		for(Marker mk : listMarker){
			mk.remove();
		}
	}

	/**
	 * tra ve loai map dang su dung
	 *
	 * @author: dungdq3
	 * @since: 1:48:53 PM Feb 26, 2015
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	public boolean getIsUsingGoogleMap() {
		return isUsingGoogleMap;
	}

	/**
	 * xoa 1 marker in gg map
	 *
	 * @author: dungdq3
	 * @since: 2:08:33 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	public void clearOneMarker(double lat, double lng){
		com.google.android.gms.maps.model.LatLng latLngGG = new com.google.android.gms.maps.model.LatLng(lat, lng);
		for(Marker mk : listMarker) {
			if(mk.getPosition() == latLngGG) {
				mk.remove();
			}
		}
	}

	/**
	 * add marker into list
	 *
	 * @author: dungdq3
	 * @since: 2:19:15 PM Feb 26, 2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 * @param resource
	 */
	public Marker addMarkerIntoList(double lat, double lng, int resource) {
		Marker mk = drawOneMarkerGoogleMap(lat, lng, resource);
		listMarker.add(mk);
		return mk;
	}

	public void setMarkerOnClickListener(Object marker,
			OnEventMapControlListener listener, int action, Object userData) {
		if(hash != null){
			ListenerFromView a = hash.get(((Marker)marker).getId());
			if(a == null){
				a = new ListenerFromView();
				a.action = action;
				a.data = userData;
				a.listener = listener;

				hash.put(((Marker)marker).getId(), a);
			}
		}
	}

	/**
	 * clear popup
	 *
	 * @author: dungdq3
	 * @since: 10:03:12 AM Feb 27, 2015
	 * @return: void
	 * @throws:
	 */
	public void clearPopUp() {
		// TODO Auto-generated method stub
		if (lastOpenedMarker != null) {
			// Close the info window
			lastOpenedMarker.hideInfoWindow();
			lastOpenedMarker = null;
		}
	}

	public OnInfoWindowElemTouchListener createWindowInfoListener(final View v, final OnEventMapControlListener listener,
			final int acion) {
		MyLog.d("[Quang]", "Set up listener.");
		OnInfoWindowElemTouchListener infoListener = new OnInfoWindowElemTouchListener(v) {
			@Override
			protected void onClickConfirmed(View view, Marker marker) {
				MyLog.d("[Quang]", "Nhan su kien click popup map.");
				if(listener != null && marker != null){
					listener.onEventMap(acion, view, marker);
				}else{
					MyLog.d("[Quang]", "Click tren popup marker nhung listener hoac marker bi null");
				}
			}
		};

		return infoListener;
	}

	/**
	 * co cho phep click vao ban do hay ko?
	 *
	 * @author: dungdq3
	 * @since: 8:09:46 PM Feb 27, 2015
	 * @return: void
	 * @throws:
	 * @param isShowFlag
	 */
	public void setMapEvent(boolean isShowFlag){
		if(!isShowFlag) {
			if(isUsingGoogleMap) {
				googleMap.setOnMapClickListener(null);
			} else {
				mapView.setMapEventListener(null);
			}
		} else {
			if(isUsingGoogleMap) {
				googleMap.setOnMapClickListener(this);
			} else {
				mapView.setMapEventListener(this);
			}
		}
	}

	public void onResume() {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			if (mapFragment != null)
				mapFragment.onResume();
		}

	}

	public void onDestroy() {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			 if (mapFragment != null)
				 con.getFragmentManager().beginTransaction().remove(mapFragment).commit();
		}

	}

	public void onLowMemory() {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			if (mapFragment != null)
				mapFragment.onLowMemory();
		}

	}

	public void onPause() {
		// TODO Auto-generated method stub
		if (isUsingGoogleMap) {
			if (mapFragment != null)
				mapFragment.onPause();
		}

	}

}
