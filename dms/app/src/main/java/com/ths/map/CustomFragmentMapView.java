/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/

package com.ths.map;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.ths.dmscore.dto.GPSInfo;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.control.AnimationImageView;
import com.ths.dmscore.view.control.AnimationLinearLayout;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;
import com.viettel.maps.base.LatLng;
import com.viettel.maps.controls.MapTypeControl;

/**
 * Lop base ban do tren fragment
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class CustomFragmentMapView extends BaseFragment implements BaseFragmentMapView.MapEventListener {
	public LinearLayout layoutZoomControl;
	// background map view
	public Object bgMapView;
	public RelativeLayout rlMainMapView;
	public static final int ZOOM_LEVEL_VTMAP_DEFAULT = 4;
	public static final int ZOOM_LEVEL_GGMAP_DEFAULT = 5;
	public static final int ZOOM_LEVEL_VTMAP_NORMAL = 16;
	public static final int ZOOM_LEVEL_GOOGLE_NORMAL = 17;

	// hien thi button dinh vi vi tri cua toi
	protected boolean isViewMyPositionControl = true;
	MapTypeControl mapTypeControl;
	OverlayViewLayer myPosLayer;
	public Map<String, CustomerListItem> mappingCustomer;

	public BaseFragmentMapView mapHelper;
	AnimationLinearLayout llRoutingInfo;
	InfoWindowAdapter adapter;
	private AnimationImageView ivShowRoutingInfo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker marker) {
				return null;
			}

			@Override
			public View getInfoContents(Marker marker) {
				return null;
			}
		};
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		boolean isUsingGoogleMap = false;
		if (GlobalInfo.getInstance().getMapTypeIsUse() == 2) {
			isUsingGoogleMap = true;
		}
		if (isUsingGoogleMap) {
			mapHelper = new GoogleFragmentMapView(parent, adapter);
		} else {
			mapHelper = new ViettelFragmentMapView(parent);
		}
		rlMainMapView = (RelativeLayout) mapHelper.initMap(savedInstanceState);
		bgMapView = mapHelper.getMapView();
		mapHelper.setMapEventListener(this);

		//layout routing info
		initRoutingInfoLayout();

		return super.onCreateView(inflater, rlMainMapView, savedInstanceState);
	}

	/**
	 * init routing info layout
	 *
	 * @author: duongdt3
	 * @return: void
	 */
	private void initRoutingInfoLayout() {
		llRoutingInfo = new AnimationLinearLayout(parent);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
		lp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		lp.bottomMargin = GlobalUtil.dip2Pixel(5);
		llRoutingInfo.setLayoutParams(lp);
		llRoutingInfo.setOrientation(LinearLayout.HORIZONTAL);
		llRoutingInfo.setVisibility(View.GONE);
		rlMainMapView.addView(llRoutingInfo);

		ivShowRoutingInfo = new AnimationImageView(parent);
		ivShowRoutingInfo.setBackgroundResource(R.drawable.bg_white_rounded);
		ivShowRoutingInfo.setVisibility(View.GONE);
		ivShowRoutingInfo.setScaleType(ScaleType.FIT_CENTER);
		RelativeLayout.LayoutParams lpImageInfo = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lpImageInfo.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		lpImageInfo.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		lpImageInfo.width = GlobalUtil.dip2Pixel(48);
		lpImageInfo.height = GlobalUtil.dip2Pixel(48);
		lpImageInfo.leftMargin = GlobalUtil.dip2Pixel(65);
		lpImageInfo.bottomMargin = GlobalUtil.dip2Pixel(5);
		ivShowRoutingInfo.setLayoutParams(lpImageInfo);
		ivShowRoutingInfo.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivShowRoutingInfo.setImageResource(R.drawable.icon_info);
		ivShowRoutingInfo.initAnimationFade();
		ivShowRoutingInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ivShowRoutingInfo.setVisibility(View.GONE);
				llRoutingInfo.setVisibility(View.VISIBLE);
			}
		});
		rlMainMapView.addView(ivShowRoutingInfo);
	}

	/**
	 * Ve marker vi tri cua toi hien tai
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	protected void drawMarkerMyPosition() {
		GPSInfo gps = GlobalInfo.getInstance().getProfile().getMyGPSInfo();
		if (gps != null && gps.getLatitude() > 0 && gps.getLongtitude() > 0) {
			drawMarkerMyPosition(gps.getLatitude(), gps.getLongtitude());
		}
	}

	/**
	 * Di chuyen ban do toi vi tri dang dung
	 *
	 * @author: BANGHN
	 */
	protected void moveToCenterMyPosition() {
		GPSInfo gps = GlobalInfo.getInstance().getProfile().getMyGPSInfo();
		if (gps != null && gps.getLatitude() > 0 && gps.getLongtitude() > 0) {
			mapHelper.moveMapTo(gps.getLatitude(), gps.getLongtitude());
		}
	}

	protected void moveTo(LatLng latlng) {
		if (latlng != null && latlng.getLatitude() > 0 && latlng.getLongitude() > 0) {
			mapHelper.moveMapTo(latlng.getLatitude(), latlng.getLongitude());
		}
	}

	/**
	 * Ve marker vi tri cua toi hien tai
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	protected void drawMarkerMyPosition(double lat, double lng) {
		mapHelper.drawMarkerMyPosition(lat, lng);
	}

	/**
	 * Xu ly fit overlay
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public void fitBounds(ArrayList<LatLng> arrayPosition) {
		mapHelper.fitBounds(arrayPosition);
	}

	/**
	 * Xu ly khi click len button vi tri cua toi
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public void onClickMarkerMyPositionEvent() {
		drawMarkerMyPosition();
		moveToCenterMyPosition();
	}

	@Override
	public boolean onSingleTap(LatLng latlng, Point point) {
		MyLog.d("GIS", "onSingleTapUp : " + latlng.toString());
//		mapHelper.onSingleTap(latlng, point);
		return false;
	}

	@Override
	public boolean onLongTap(LatLng latlng, Point point) {
		return false;
	}

	@Override
	public boolean onDoubleTap(LatLng latlng, Point point) {
		MyLog.d("GIS", "onSingleTapUp : " + latlng.toString());
		return false;
	}
	/**
	 * Add marker len map
	 * @author: cuonglt3
	 * @since: 8:03:43 AM Aug 8, 2014
	 * @return: Object
	 * @throws:
	 * @param lat
	 * @param lng
	 * @param drawable
	 * @param seq
	 * @param numVisitFact
	 * @param colorSeq
	 * @param colorNumVisit
	 * @return
	 */
	public Object addMarkerToMap(double lat, double lng, int drawable, BaseFragmentMapView.MarkerAnchor markerArchor,
			String seq, String numVisitFact, int colorSeq, int colorNumVisit) {
		Object marker = mapHelper.addMarker(lat, lng, drawable, markerArchor, seq,
				numVisitFact, colorSeq, colorNumVisit);
		return marker;
	}

	public void OnMapLoaded(){
		mapHelper.onMapLoaded();
	}

	/**
	 * @author: cuonglt3
	 * @since: 8:28:49 AM Aug 8, 2014
	 * @return: void
	 * @throws:
	 */
	public void clearAllMarkerOnMap() {
		mapHelper.clearAllMarkerOnMap();
	}

	public void setMarkerOnClickListener(Object Marker, OnEventMapControlListener listener, int action, Object userData){
		mapHelper.setMarkerOnClickListener(Marker, listener, action, userData);
	}

	public void showPopupWhenClickMarker(View view, OverlayViewOptions opts){
		mapHelper.showPopupWhenClickMarker(view, opts);
	}

	public void setZoomBorder() {
		mapHelper.setZoomBorder();
	}

	/**
	 * @author: cuonglt3
	 * @since: 11:15:10 AM Aug 8, 2014
	 * @return: void
	 * @throws:
	 */
	public void clearPopup() {
		// TODO Auto-generated method stub
		mapHelper.clearPopup();

	}
	public void getAdressFromLatLng(GetAddressListener listener, LatLng latLng){
		mapHelper.getAdressFromLatLng(listener, latLng);
	}

	@Override
	public void onResume() {
		super.onResume();
		mapHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mapHelper.onPause();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		mapHelper.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mapHelper.onLowMemory();
	}

	public void moveTo(double lat, double lng) {
		// TODO Auto-generated method stub
		moveTo(new LatLng(lat, lng));
	}

	public void drawRoute(BaseFragmentMapView.RouteInfo routeInfo){
		mapHelper.drawRoute(routeInfo);
	}

	public void hideRoute(){
		mapHelper.hideRoute();
	}

	public void showRoute(){
		mapHelper.showRoute();
	}

	public boolean isHaveRouteData(){
		return mapHelper.isHaveRouteData();
	}

	public void addTextMarker(View view, OverlayViewOptions opts){
		mapHelper.addMarker(view,opts);
	}

	/**
	 * display Help Layout
	 * @author: duongdt3
	 * @since: 09:27:25 9 Jan 2014
	 * @return: void
	 * @throws:
	 * @param header
	 */
	public void showRoutingInfoLayout(){
		if (llRoutingInfo != null && llRoutingInfo.getChildCount() == 0) {
			llRoutingInfo.initAnimationFade();
			LayoutInflater inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup layoutView = (ViewGroup)inflater.inflate(R.layout.layout_help_routing, llRoutingInfo, false);
			ImageView iv = (ImageView) layoutView.findViewById(R.id.btClose);
			iv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (llRoutingInfo != null) {
						llRoutingInfo.setVisibility(View.GONE);
						ivShowRoutingInfo.setVisibility(View.VISIBLE);
					}
				}
			});
			//add view
			llRoutingInfo.addView(layoutView);
			//show header
			llRoutingInfo.setVisibility(View.VISIBLE);
		}
	}
}
