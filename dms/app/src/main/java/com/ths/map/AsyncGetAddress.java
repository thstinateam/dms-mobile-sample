/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.map;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.os.AsyncTask;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.main.GlobalBaseActivity;

/**
 * AsyncGetAddress.java
 * @author: dungdq3
 * @version: 1.0 
 * @since:  10:52:01 AM Aug 7, 2014
 */
public class AsyncGetAddress extends
		AsyncTask<String, JSONObject, String> {

	private String result;
	private final GlobalBaseActivity parent;
	private final GetAddressListener listener;
	
	public AsyncGetAddress(GetAddressListener listener, GlobalBaseActivity parent, String result){
		this.result = result;
		this.parent = parent;
		this.listener = listener;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if (listener != null)
			listener.showProgressLoading(true);
	}
	
	@Override
	protected String doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		String uri = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+ arg0[0]+","+arg0[1] +"&sensor=true";
		HttpGet httpGet = new HttpGet(uri);
	    HttpClient client = new DefaultHttpClient();
	    HttpResponse response;
	    StringBuilder stringBuilder = new StringBuilder();

	    try {
	        response = client.execute(httpGet);
	        HttpEntity entity = response.getEntity();
	        InputStream stream = entity.getContent();
	        InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
	        BufferedReader buffReader = new BufferedReader(reader);
	        int b;
	        while ((b = buffReader.read()) != -1) {
	            stringBuilder.append((char) b);
	        }
	    } catch (ClientProtocolException e) {
	    } catch (IOException e) {
	    }

		if (!StringUtil.isNullOrEmpty(stringBuilder.toString())) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject = new JSONObject(stringBuilder.toString());
			} catch (JSONException e) {
			}
			publishProgress(jsonObject);
		}
		return result;
	}
	
	@Override
	protected void onProgressUpdate(JSONObject... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
		JSONObject jsonObj = values[0];


	    try {
	        String status = jsonObj.getString("status").toString();

	        if(status.equalsIgnoreCase("OK")){
	            JSONArray results = jsonObj.getJSONArray("results");
	            int i = 0;
	            do{

	                JSONObject r = results.getJSONObject(i);
	                JSONArray typesArray = r.getJSONArray("types");
	                String types = typesArray.getString(0);

	                if(types.equalsIgnoreCase("street_address")){
	                    result = r.getString("formatted_address");
	                }

	                if(result!=null){
	                    i = results.length();
	                }

	                i++;
	            }while(i<results.length());
	            listener.setAddress(result);
	        }

	    } catch (JSONException e) {
	    	MyLog.e("Get Address from Google service", VNMTraceUnexceptionLog.getReportFromThrowable(e));
	    }
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (listener != null)
			listener.showProgressLoading(false);
	}

}
