package com.ths.map.view;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

/**
 * Popup thong tin khach hang tren ban do
 * 
 * @author banghn
 * @since 1.0
 */
public class CustomerPopupView extends LinearLayout implements OnClickListener, View.OnTouchListener {
	public static final int POP_UP_CLOSE = 1001;
	public static final int GOTO_INFO = 1002;
	public static final int GOTO_ORDER = 1003;

	public TextView tvName; // tv display name
	public TextView tvAddress; // tv About
	public RelativeLayout llRoot; // Update UI for popup
	public int widthPopup;
	public ImageView ivDelete; // dong poupup
	private TextView tvPage; // khach hang thu ?/tong
	private TextView tvCellPhone; // di dong
	private TextView tvStablePhone;// co dinh
	public TextView tvInfo;// thong tin chi tiet
	public TextView tvOrder;// dat hang
	public Object userData;
	public boolean isNVGS = false;// la nhan vien giam sat
	public OnEventControlListener lis;

	public CustomerPopupView(Context context, String info, boolean isNVGS) {
		super(context);
		this.isNVGS = isNVGS;
		loadControls();
	}

	/**
	 * Init control popup
	 * 
	 * @author banghn
	 */
	private void loadControls() {
		this.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.layout_user_on_map, this);
		llRoot = (RelativeLayout) v.findViewById(R.id.llRoot);
		tvName = (TextView) v.findViewById(R.id.tvName);
		ivDelete = (ImageView) v.findViewById(R.id.ivDelete);
		tvPage = (TextView) v.findViewById(R.id.tvPage);
		tvAddress = (TextView) v.findViewById(R.id.tvAddress);
		tvCellPhone = (TextView) v.findViewById(R.id.tvCellPhone);
		tvStablePhone = (TextView) v.findViewById(R.id.tvStablePhone);
//		tvInfo = (TextView) v.findViewById(R.id.tvInfo);
//		tvOrder = (TextView) v.findViewById(R.id.tvOrder);
		tvInfo = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvInfo, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_CTKH);
		tvOrder = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvOrder, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_GHETHAM);
		widthPopup = getWidthOfPopup();
		if (isNVGS) {// nvgs: an chuc nang dat hang
			tvOrder.setVisibility(View.GONE);
			tvInfo.setVisibility(View.GONE);
		}
		ivDelete.setOnClickListener(this);
		tvInfo.setOnClickListener(this);
		tvOrder.setOnClickListener(this);
		
		ivDelete.setOnTouchListener(this);
		tvInfo.setOnTouchListener(this);
		tvOrder.setOnTouchListener(this);

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// int mode = MeasureSpec.getMode(widthMeasureSpec);
		// int adjustedMaxWidth = (int) (MAX_WIDTH_DP * SCALE + 0.5f);
		// int adjustedWidth = Math.min(widthPopup, adjustedMaxWidth);
		// int adjustedWidthMeasureSpec =
		// MeasureSpec.makeMeasureSpec(adjustedWidth, mode);
		super.onMeasure(widthPopup, heightMeasureSpec);
	}

	public int getWidthOfPopup() {
		Rect bounds = new Rect();
		Paint textPaint = tvAddress.getPaint();
		textPaint.getTextBounds(tvAddress.getText().toString(), 0, tvAddress.getText().toString().length(), bounds);
		int width = bounds.width();
		return width + 40;
	}

	public void setListener(OnEventControlListener lis) {
		this.lis = lis;
	}

	/**
	 * Update thong tin popup
	 * 
	 * @author banghn
	 */
	public void updateInfo(CustomerListItem dto, int index, int isOrWithSelectedDay, int total, GlobalInfo.VistConfig visitConfig) {
		SpannableObject objCellPhone = new SpannableObject();
		objCellPhone.addSpan(StringUtil.getString(R.string.TEXT_MOBILE_PHONE_COLON) + ":" + Constants.STR_SPACE, ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.NORMAL);
		objCellPhone.addSpan(dto.aCustomer.getMobilephone(), ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);

		SpannableObject objStablePhone = new SpannableObject();
		objStablePhone.addSpan(StringUtil.getString(R.string.TEXT_FIXED) + ":" + Constants.STR_SPACE, ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.NORMAL);
		if (!StringUtil.isNullOrEmpty(dto.aCustomer.getPhone())) {
			objStablePhone.addSpan(dto.aCustomer.getPhone(),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
		} else if (StringUtil.isNullOrEmpty(dto.aCustomer.getPhone())) {
			objStablePhone.addSpan(Constants.STR_BLANK,
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
		}
		userData = index;
		tvName.setText(dto.aCustomer.getCustomerCode() + " - " + dto.aCustomer.getCustomerName());
		tvAddress.setText(StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS) + ": " + dto.aCustomer.getStreet());
		tvCellPhone.setText(objCellPhone.getSpan());
		tvStablePhone.setText(objStablePhone.getSpan());
		String str = "";
		if (isOrWithSelectedDay == 0) {
			if (StringUtil.isNullOrEmpty(dto.seqInDayPlan)) {
				str = "0";
			} else {
				str = dto.seqInDayPlan;
			}
			tvPage.setText(str + "/" + total);
		} else {
			tvPage.setText("!/" + total);
		}
		
		boolean isShowOrderIcon = false;
		if (isNVGS) {
			isShowOrderIcon = false;
		} else if (dto.canOrderException()) {
			isShowOrderIcon = true;
		} else {
			isShowOrderIcon = (visitConfig != null 
					&& visitConfig.isAllowVisit(dto.isTooFarShop, dto.isOr, dto.isVisit(), dto.aCustomer.isHaveLocation()));			
		}
		
		if (isShowOrderIcon) {
			PriUtils.getInstance().setStatus(tvOrder, PriUtils.ENABLE);
		} else{
			PriUtils.getInstance().setStatus(tvOrder, PriUtils.INVISIBLE);
		}
	}

	/**
	 * Da ghe tham khach hang chua
	 * 
	 * @author : BangHN since : 1.0
	 */
	public boolean isVisit(CustomerListItem item) {
		return item.visitStatus != VISIT_STATUS.NONE_VISIT;
	}

	@Override
	public void onClick(View v) {
		if (v == ivDelete) {
			lis.onEvent(POP_UP_CLOSE, ivDelete, null);
		} else if (v == tvInfo) {
			lis.onEvent(GOTO_INFO, tvInfo, userData);
		} else if (v == tvOrder) {
			lis.onEvent(GOTO_ORDER, tvOrder, userData);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		int x = (int) event.getX();
		int y = (int) event.getY();
		int action = event.getActionMasked();
		int button = -1;
		if (touchOnButtonClose(x, y)) {
			button = 0;
		} else if (touchOnButtonInfo(x, y)) {
			button = 1;
		} else if (touchOnButtonOrder(x, y)) {
			button = 2;
		}
		/*
		 * if (v == ivDelete) { lis.onEvent(POP_UP_CLOSE, ivDelete, null); }
		 * else if (v == tvInfo) { lis.onEvent(GOTO_INFO, tvInfo, userData); }
		 * else if (v == tvOrder) { lis.onEvent(GOTO_ORDER, tvOrder, userData);
		 * } else if (v == tvVisit) { lis.onEvent(VISIT_CUSTOMER, tvVisit,
		 * userData); }
		 */
		switch (button) {
		// Touch on CLOSE
		case 0:
			if (action == MotionEvent.ACTION_DOWN) {

			} else if (action == MotionEvent.ACTION_UP) {
				lis.onEvent(POP_UP_CLOSE, ivDelete, null);
			}
			break;
		// Touch on INFO
		case 1:
			if (action == MotionEvent.ACTION_DOWN) {

			} else if (action == MotionEvent.ACTION_UP) {
				lis.onEvent(GOTO_INFO, tvInfo, userData);
			}
			break;
		// Touch on ORDER
		case 2:
			if (action == MotionEvent.ACTION_DOWN) {
				
			} else if (action == MotionEvent.ACTION_UP) {
				lis.onEvent(GOTO_ORDER, tvOrder, userData);
			}
			break;
			// Touch on ORDER

		default:
			break;
		}

		return false;
	}
	
	/**
	 * Kiem tra toa do co nam trong vung touch hay ko?
	 * 
	 * @author: dungdq3
	 * @since: 9:17:29 AM Apr 3, 2015
	 * @return: boolean
	 * @throws:  
	 * @param x
	 * @param y
	 * @return
	 */
	boolean touchOnButtonInfo(int x, int y) {
		int btnHeight = 32 + GlobalUtil.dip2Pixel(10);
		int popUpWidth = this.getWidth();
		int popupHeight = this.getHeight();
		int btnTop = popupHeight - btnHeight;
		int btnLeft = 0;
		int btnWidth = popUpWidth / 2;

		if (x >= btnLeft && x <= (btnWidth + btnLeft) && y >= btnTop
				&& y <= (btnTop + btnHeight)) {
			return true;
		}

		return false;
	}

	/**
	 * Kiem tra toa do co nam trong vung touch hay ko?
	 * 
	 * @author: dungdq3
	 * @since: 9:17:29 AM Apr 3, 2015
	 * @return: boolean
	 * @throws:  
	 * @param x
	 * @param y
	 * @return
	 */
	boolean touchOnButtonOrder(int x, int y) {
		int btnHeight = 32 + GlobalUtil.dip2Pixel(10);
		int popUpWidth = this.getWidth();
		int popupHeight = this.getHeight();
		int btnTop = popupHeight - btnHeight;
		int btnLeft = popUpWidth / 2;
		int btnWidth = popUpWidth / 2;

		if (x >= btnLeft && x <= (btnWidth + btnLeft) && y >= btnTop
				&& y <= (btnTop + btnHeight)) {
			return true;
		}

		return false;
	}

	/**
	 * Kiem tra toa do co nam trong vung touch hay ko?
	 * 
	 * @author: dungdq3
	 * @since: 9:17:29 AM Apr 3, 2015
	 * @return: boolean
	 * @throws:  
	 * @param x
	 * @param y
	 * @return
	 */
	boolean touchOnButtonClose(int x, int y) {
		int btnHeight = GlobalUtil.dip2Pixel((int) (40))
				+ GlobalUtil.dip2Pixel(20);
		int popUpWidth = this.getWidth();
		// int popupHeight = this.getHeight();
		int btnTop = 0;
		int btnWidth = GlobalUtil.dip2Pixel((int) (40))
				+ GlobalUtil.dip2Pixel(20);
		int btnLeft = popUpWidth - btnWidth;

		if (x >= btnLeft && x <= (btnWidth + btnLeft) && y >= btnTop
				&& y <= (btnTop + btnHeight)) {
			return true;
		}

		return false;
	}
}
