package com.ths.map;

import java.util.ArrayList;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.AnimationImageView;
import com.ths.dmscore.view.control.AnimationLinearLayout;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.map.view.MarkerMapView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;
import com.viettel.maps.MapView;
import com.viettel.maps.MapView.MapEventListener;
import com.viettel.maps.base.LatLng;
import com.viettel.maps.base.LatLngBounds;
import com.viettel.maps.controls.BaseControl;
import com.viettel.maps.controls.MapTypeControl;
import com.viettel.maps.controls.ZoomControl;

/**
 * Lop base ban do tren fragment
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class FragmentMapView extends BaseFragment implements OnEventControlListener, MapEventListener {
	final LatLng center = new LatLng(10.784829, 106.68307);
	public LinearLayout layoutZoomControl;
	// background map view
	public View bgMapView;
	// view chinh chua ban do & cac marker, control
	public RelativeLayout rlMainMapView;

	// hien thi button dinh vi vi tri cua toi
	protected boolean isViewMyPositionControl = true;
	public MapView mapView;
	public ZoomControl zoomControl;
	// Marker myPosMarker;
	// MarkerLayer myPosMarkerLayer;
	MapTypeControl mapTypeControl;
	OverlayViewLayer myPosLayer;
	AnimationImageView ivHideShowHeader = null;
	AnimationLinearLayout llHeader;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) initMap(savedInstanceState);
		return super.onCreateView(inflater, view, savedInstanceState);
	}

	/**
	 * Khoi tao ban do Viettel Map
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	private ViewGroup initMap(Bundle savedInstanceState) {
		bgMapView = new View(parent);
		bgMapView.setBackgroundColor(Color.rgb(237, 234, 226));
		bgMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		rlMainMapView = new RelativeLayout(parent);
		rlMainMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		rlMainMapView.addView(bgMapView);

		mapView = new MapView(parent);
		// mapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
		// LayoutParams.MATCH_PARENT));
		// mapView.setMapKey("TTUDCNTT_KEY_TEST");
		mapView.setMapKey(GlobalInfo.getVIETTEL_MAP_KEY());
		mapView.setZoomControlEnabled(true);
		mapView.setMapTypeControlEnabled(false);
		mapView.moveTo(center);
		mapView.setMapEventListener(this);
		rlMainMapView.addView(mapView);

		zoomControl = (ZoomControl) mapView.getControl(BaseControl.TYPE_CONTROL_ZOOM);
		if (zoomControl != null) {
			mapView.removeView(zoomControl);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			zoomControl.setLayoutParams(lp);
			zoomControl.setGravity(Gravity.LEFT | Gravity.BOTTOM);
			rlMainMapView.addView(zoomControl);
		}

		if (isViewMyPositionControl) {
			initMyPositionControl();
		}
		llHeader = new AnimationLinearLayout(parent);
		llHeader.setId(R.id.table);

		llHeader.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
		llHeader.setOrientation(LinearLayout.HORIZONTAL);
		llHeader.setGravity(Gravity.CENTER | Gravity.TOP);
		llHeader.setVisibility(View.GONE);
		rlMainMapView.addView(llHeader);
		return rlMainMapView;
	}

	/**
	 * Ve button hien thi vi tri cua toi
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public void initMyPositionControl() {
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(mapView.getContext());
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		llMyPosition.setLayoutParams(lp);
		llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ImageView ivMyPosition = new ImageView(parent);
		ivMyPosition.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivMyPosition.setImageResource(R.drawable.icon_location_2);
		ivMyPosition.setBackgroundResource(R.drawable.custom_button_with_border);
		ivMyPosition.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickMarkerMyPositionEvent();
			}
		});
		llMyPosition.addView(ivMyPosition);
		rlMainMapView.addView(llMyPosition);
	}

	/**
	 * Ve marker vi tri cua toi hien tai
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	@SuppressWarnings("unchecked")
	protected void drawMarkerMyPosition() {
		clearMapLayer(myPosLayer);

		if (GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() > 0
				&& GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude() > 0) {

			// LatLng myLocation = new
			// LatLng(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(),
			// GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude());
			// mapView.moveTo(myLocation);

			MarkerMapView maker = new MarkerMapView(parent, R.drawable.icon_location);
			OverlayViewOptions opts = new OverlayViewOptions();
			opts.position(new com.viettel.maps.base.LatLng(GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude(), GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude()));
			opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
			myPosLayer = new OverlayViewLayer();
			mapView.addLayer(myPosLayer);
			myPosLayer.addItemObj(maker, opts);
		} else {
			parent.showDialog(StringUtil.getString(R.string.TEXT_ALERT_CANT_LOCATE_YOUR_POSITION));
		}
	}

	/**
	 * Di chuyen ban do toi vi tri dang dung
	 *
	 * @author: BANGHN
	 */
	protected void moveToCenterMyPosition() {
		if (GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() > 0
				&& GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude() > 0) {
			LatLng myLocation = new LatLng(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(),
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude());
			mapView.moveTo(myLocation);
			// mapView.invalidate();
		}
	}

	/**
	 * Ve marker vi tri cua toi hien tai
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	@SuppressWarnings("unchecked")
	protected void drawMarkerMyPosition(double lat, double lng) {
		clearMapLayer(myPosLayer);

		LatLng myLocation = new LatLng(lat, lng);
		mapView.moveTo(myLocation);
		MarkerMapView maker = new MarkerMapView(getActivity(), R.drawable.icon_location);
		OverlayViewOptions opts = new OverlayViewOptions();
		opts.position(new com.viettel.maps.base.LatLng(lat, lng));
		opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
		myPosLayer = new OverlayViewLayer();
		mapView.addLayer(myPosLayer);
		myPosLayer.addItemObj(maker, opts);
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public void clearMapLayer(OverlayViewLayer layer) {
		if (layer != null) {
			for (int i = 0; i < layer.getMapObjectTotal(); i++) {
				OverlayViewItemObj obj = (OverlayViewItemObj) layer.getMapObject(i);
				mapView.removeView(obj.getView());
			}
			layer.clear();
		}
	}

	/**
	 * Xu ly fit overlay
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public void fitBounds(ArrayList<LatLng> arrayPosition) {
		LatLngBounds bound = null;
		for (int i = 0; i < arrayPosition.size(); i++) {
			com.viettel.maps.base.LatLng latlng = arrayPosition.get(i).clone();
			bound = getBound(bound, latlng);
		}

		mapView.fitBounds(bound, true);
	}

	/**
	 * Xu ly fit overlay cho item
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public LatLngBounds getBound(LatLngBounds bound, LatLng latLng) {
		if (bound == null) {
			bound = new LatLngBounds();
			bound.setSouthWest(latLng);
			bound.setNorthEast(latLng);
		} else {
			int lat = latLng.getLatitudeE6();
			int lng = latLng.getLongitudeE6();
			if (bound.getSouthWest().getLatitudeE6() > lat)
				bound.getSouthWest().setLatitudeE6(lat);
			if (bound.getNorthEast().getLatitudeE6() < lat)
				bound.getNorthEast().setLatitudeE6(lat);
			if (bound.getSouthWest().getLongitudeE6() > lng)
				bound.getSouthWest().setLongitudeE6(lng);
			if (bound.getNorthEast().getLongitudeE6() < lng)
				bound.getNorthEast().setLongitudeE6(lng);
		}
		return bound;
	}

	/**
	 * Xu ly khi click len button vi tri cua toi
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public void onClickMarkerMyPositionEvent() {
		drawMarkerMyPosition();
		moveToCenterMyPosition();
	}

	@Override
	public boolean onSingleTap(LatLng latlng, Point point) {
		MyLog.d("GIS", "onSingleTapUp : " + latlng.toString());
		return false;
	}

	@Override
	public boolean onLongTap(LatLng latlng, Point point) {
		return false;
	}

	@Override
	public boolean onDoubleTap(LatLng latlng, Point point) {
		MyLog.d("GIS", "onSingleTapUp : " + latlng.toString());
		return false;
	}

	@Override
	public void onCenterChanged() {
	}

	@Override
	public void onBoundChanged() {
	}

	/**
	 * Set header for mapview
	 * @author: duongdt3
	 * @since: 09:27:25 9 Jan 2014
	 * @return: void
	 * @throws:
	 * @param header
	 */
	protected void setHeaderView(View header){
		llHeader.addView(header);
		//add control hide header
		initHideHeaderControl();

		ivHideShowHeader.setVisibility(View.GONE);
		//hien header len
		llHeader.setVisibility(View.VISIBLE);
		ivHideShowHeader.setImageResource(R.drawable.icon_go_up);
		ivHideShowHeader.setVisibility(View.VISIBLE);
//		mapView.invalidate();
	}

	/**
	 * Them control an header tren map
	 * @author: duongdt3
	 * @since: 09:15:56 9 Jan 2014
	 * @return: void
	 * @throws:
	 */
	public void initHideHeaderControl() {
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(mapView.getContext());
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		////noi voi llHeader
		lp.addRule(RelativeLayout.ALIGN_BOTTOM, llHeader.getId());
		lp.setMargins(0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), 0);
		llMyPosition.setLayoutParams(lp);
		//llMyPosition.setGravity(Gravity.RIGHT | Gravity.TOP);
		llMyPosition.setGravity(Gravity.RIGHT);

		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ivHideShowHeader = new AnimationImageView(parent);
		ivHideShowHeader.setLayoutParams(new LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivHideShowHeader.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivHideShowHeader.setImageResource(R.drawable.icon_go_down);
		//ivHideShowHeader.setBackgroundResource(R.drawable.custom_button_with_border);
		ivHideShowHeader.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				onClickMarkerHideHeaderEvent();
			}
		});
		llMyPosition.addView(ivHideShowHeader);
		rlMainMapView.addView(llMyPosition);
	}

	private void onClickMarkerHideHeaderEvent() {
		//an hien header
		if (llHeader.getVisibility() == View.VISIBLE) {
			ivHideShowHeader.setVisibility(View.GONE);
			llHeader.setVisibility(View.GONE);
			ivHideShowHeader.setImageResource(R.drawable.icon_go_down);
			ivHideShowHeader.setBackgroundResource(R.drawable.custom_button_with_border);
			ivHideShowHeader.setVisibility(View.VISIBLE);
		}else if (llHeader.getVisibility() == View.GONE){
			ivHideShowHeader.setVisibility(View.GONE);
			llHeader.setVisibility(View.VISIBLE);
			ivHideShowHeader.setImageResource(R.drawable.icon_go_up);
			ivHideShowHeader.setBackgroundDrawable(null);
			ivHideShowHeader.setVisibility(View.VISIBLE);
		}
	}

}
