package com.ths.map.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;


/**
 * Marker hien thi icon tren ban do
 * 
 * @author : BangHN since : 1.0 version : 1.0
 */
public class TextMarkerMapView extends LinearLayout {
	public RelativeLayout llRoot; // layout root
	public int widthPopup;// chieu rong popup
	private ImageView ivMaker;// icon marker
	private TextView tvIndex;// text hien thi
	TextView tvName;//ten nv
	Context mContext;// context
	OnEventControlListener lis;// lister su kien
	Object userData;// data user dinh nghia
	int action;// action
	int colorSeq = -1;
	int colorName = -1;
	private OnEventMapControlListener lis1;

	public TextMarkerMapView(Context context, int icon, String seq, String name, int colorSeq, int colorName) {
		super(context);
		this.colorSeq = colorSeq;
		this.colorName = colorName;
		initMarker();
		setIconMarker(icon);
		setTextMarker(seq); 
		setNameMarker(name); 
	}
	
	public TextMarkerMapView(Context context, int icon, String seq) {
		super(context);
		initMarker();
		setIconMarker(icon);
		setTextMarker(seq);
	}
	
	public TextMarkerMapView(Context context, int icon, String name, int colorName) {
		super(context);
		this.colorName = colorName;
		this.colorSeq = colorName;
		initMarker();
		setIconMarker(icon);
		setNameMarker(name);
	}

	public void setNameMarker(String name) {
		tvName.setText(name);
		if (colorName > 0) {
			tvName.setTextColor(getResources().getColor(colorName));
		} else {
			tvName.setTextColor(getResources().getColor(R.color.RED));
		}
		if (StringUtil.isNullOrEmpty(name)) {
			tvName.setVisibility(View.GONE);
		}else{
			tvName.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * chi set onclicklistener cho cac marker duoc setListener, tranh truong hop onclick ra null
	 * @author: cuonglt3
	 * @since: 2:05:09 PM Aug 8, 2014
	 * @return: void
	 * @throws:  
	 * @param listener
	 * @param action
	 * @param userData
	 */
	public void setListener(OnEventControlListener listener, final int action, final Object userData) {
		this.lis = listener;
		this.action = action;
		this.userData = userData;
		tvIndex.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lis.onEvent(action, TextMarkerMapView.this, userData);
			}
		});
		ivMaker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lis.onEvent(action, TextMarkerMapView.this, userData);
			}
		});
	}
	
	/**
	 * chi set onclicklistener cho cac marker duoc setListener, tranh truong hop onclick ra null
	 * @author: cuonglt3
	 * @since: 2:05:09 PM Aug 8, 2014
	 * @return: void
	 * @throws:  
	 * @param listener
	 * @param action
	 * @param userData
	 */
	public void setListener1(OnEventMapControlListener listener, final int action, final Object userData) {
		this.lis1 = listener;
		this.action = action;
		this.userData = userData;
		tvIndex.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lis1.onEventMap(action, TextMarkerMapView.this, userData);
			}
		});
		ivMaker.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				lis1.onEventMap(action, TextMarkerMapView.this, userData);
			}
		});
	}

	/**
	 * init popup
	 * 
	 * @author : BangHN since : 1.0
	 */
	private void initMarker() {
		this.setLayoutParams(new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.layout_text_marker_on_map, this);
		llRoot = (RelativeLayout) v.findViewById(R.id.llRoot);
		ivMaker = (ImageView) v.findViewById(R.id.ivMaker);
		tvIndex = (TextView) v.findViewById(R.id.tvIndex);
		tvName = (TextView) v.findViewById(R.id.tvName);
		BitmapDrawable bd = (BitmapDrawable) this.getResources().getDrawable(R.drawable.icon_circle_green);
		int width = bd.getBitmap().getWidth();
		widthPopup = GlobalUtil.dip2Pixel(width);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		int mode = MeasureSpec.getMode(widthMeasureSpec);
//		int adjustedWidthMeasureSpec = MeasureSpec.makeMeasureSpec(widthPopup, mode);
		super.onMeasure(widthPopup, heightMeasureSpec);
	}

	public void setIconMarker(int drawable) {
		ivMaker.setImageResource(drawable);
	}

	public void setTextMarker(String text) {
		tvIndex.setText(text);
		if (colorSeq > 0) {
			tvIndex.setTextColor(getResources().getColor(colorSeq));
		}
	}

	public void setColorTextName(int color){
		colorName = color;
		if (colorName > 0) {
			tvName.setTextColor(getResources().getColor(colorName));
		}else{
			tvName.setTextColor(getResources().getColor(R.color.BLACK)); 
		}
	}
	
	public void setColorSeq(int color){
		colorSeq = color;
		if (colorSeq > 0) {
			tvIndex.setTextColor(getResources().getColor(colorSeq));
		}else{
			tvIndex.setTextColor(getResources().getColor(R.color.WHITE)); 
		}
	}
	
	public void setFontSize(float size) {
		tvIndex.setTextSize(size);
	}
	
	public void setProperties(int icon, String seq, String name, int colorSeq, int colorName){
		setIconMarker(icon);
		setTextMarker(seq);
		setNameMarker(name);
		setColorSeq(colorSeq);
		setColorTextName(colorName);
	}
}
