package com.ths.map;

import android.view.View;

import com.viettel.maps.layers.MapLayer;

/**
 * Mo ta muc dich cua class
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */

@SuppressWarnings("rawtypes")
public class OverlayViewLayer extends MapLayer {
	private static int zMarkerLayerIndexCounter = 300;
	protected int offsetX;
	protected int offsetY;
	private OverlayViewItemObj obj;

	@SuppressWarnings("unchecked")
	public OverlayViewLayer() {
		offsetX = 0;
		offsetY = 0;
		mZIndex = getzMarkerLayerIndexCounter();
		setzMarkerLayerIndexCounter(getzMarkerLayerIndexCounter() + 1);
		zCustomLayerIndexCounter--;
	}

	@SuppressWarnings("unchecked")
	public OverlayViewItemObj addItemObj(View view, OverlayViewOptions opts) {
		if (opts == null) {
			return null;
		} else {
			obj = new OverlayViewItemObj(view, opts);
			add(obj);
			getMapView().addView(view);
			return obj;
		}
	}

	public OverlayViewItemObj getItemObj() {
		return obj;
	}

	public static int getzMarkerLayerIndexCounter() {
		return zMarkerLayerIndexCounter;
	}

	public static void setzMarkerLayerIndexCounter(int zMarkerLayerIndexCounter) {
		OverlayViewLayer.zMarkerLayerIndexCounter = zMarkerLayerIndexCounter;
	}
}
