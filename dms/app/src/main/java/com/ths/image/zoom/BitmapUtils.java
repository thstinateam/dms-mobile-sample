package com.ths.image.zoom;



import android.graphics.Bitmap;

public class BitmapUtils {
	
	public static Bitmap resizeBitmap( Bitmap input, int destWidth, int destHeight )
	{
		int srcWidth = input.getWidth();
		int srcHeight = input.getHeight();
		boolean needsResize = false;
		double p;
		if ( srcWidth > destWidth || srcHeight > destHeight ) {
			needsResize = true;
			if ( srcWidth > srcHeight && srcWidth > destWidth ) {
				p = (double)destWidth / (double)srcWidth;
				destHeight = (int)( srcHeight * p );
			} else {
				p = (double)destHeight / (double)srcHeight;
				destWidth = (int)( srcWidth * p );
			}
		} else {
			destWidth = srcWidth;
			destHeight = srcHeight;
		}
		if ( needsResize ) {
			Bitmap output = Bitmap.createScaledBitmap( input, destWidth, destHeight, true );
			return output;
		} else {
			return input;
		}
	}
}
