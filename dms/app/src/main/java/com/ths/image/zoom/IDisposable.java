package com.ths.image.zoom;

public interface IDisposable {
	void dispose();
}
