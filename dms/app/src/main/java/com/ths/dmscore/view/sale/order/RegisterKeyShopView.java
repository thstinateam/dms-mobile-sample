package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.dto.view.ValueItemDTO;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.main.AbstractAlertDialog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Popup dang ki keyshop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RegisterKeyShopView extends AbstractAlertDialog implements OnClickListener, OnItemSelectedListener {

	private int indexKeyShop = -1;
	Spinner spKeyShop;
	private int indexCycle = -1;
	Spinner spCycle;
	private DMSTableView tbKeyShopLevel;
	// listtener
	private RegisterKeyShopViewDTO dtoKeyShopDTO = new RegisterKeyShopViewDTO();
	// luu lai cac gia tri khi click sang tim kiem
	ArrayList<RegisterKeyShopItemDTO> lstItemClone = new ArrayList<RegisterKeyShopItemDTO>();
	private Button btUpdate;// button cap nhat
	public final static int MAX_LENGHT_MULTIPLIER = 6;// gioi han length boi so nhap
	TextView tvStatus;

	public RegisterKeyShopView(Context context, BaseFragment listener, String title) {
		super(context, listener, title);
		View viewLayout = setViewLayout(
				R.layout.layout_register_keyshop_view);
		tbKeyShopLevel = (DMSTableView) viewLayout.findViewById(R.id.tbKeyShopLevel);
		tbKeyShopLevel.addHeader(new RegisterKeyShopRow(parent));
		spKeyShop =  (Spinner) viewLayout.findViewById(R.id.spKeyShop);
		spCycle =  (Spinner) viewLayout.findViewById(R.id.spCycle);
//		btSearch =  (Button) viewLayout.findViewById(R.id.btSearch);
//		btSearch.setOnClickListener(this);
		btUpdate =  (Button) viewLayout.findViewById(R.id.btUpdate);
		btUpdate.setOnClickListener(this);
		tvStatus =  (TextView) viewLayout.findViewById(R.id.tvStatus);
	}

	 /**
	 * render layout
	 * @author: Tuanlt11
	 * @param isFirst
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(RegisterKeyShopViewDTO dto, boolean isFirst){
		this.dtoKeyShopDTO = dto;
		if (isFirst) {
			resetValue();
			initDataKeyShop();
		}
		//neu truong hop cycleId > 0, tuc la chon combobox cycle de loai lai du lieu neu ko khoi tao lai combbox cycle nua
		if(indexCycle >= 0)
			initDataCycle(false);
		else{
			initDataCycle(true);
		}
		tbKeyShopLevel.clearAllData();
		boolean isEdit = true;
		int status = 0;
		for(int i = 0, size = dto.lstItem.size() ; i < size; i++){
			RegisterKeyShopRow row = new RegisterKeyShopRow(parent);
			isEdit = dto.lstItem.get(i).isEdit;
			status = dto.lstItem.get(i).ksCustomerItem.status;
			row.renderLayout(dto.lstItem.get(i),isEdit);
			tbKeyShopLevel.addRow(row);
		}
		GlobalUtil.setEnableButton(btUpdate, isEdit);
		setStatusKeyShop(status);
	}

	 /**
	 * Xoa nhung du lieu khi khoi tao lai popup
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void resetValue(){
		lstItemClone.clear();
		indexCycle = -1;
	}

	 /**
	 * Khoi tao du lieu combobox keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void initDataKeyShop(){
		int size = dtoKeyShopDTO.lstKeyShop.size();
		String[] strAdap = new String[size];
		//add text all neu truong hop co hon 2 nganh hang
		int i = 0;
		for (KeyShopDTO item : dtoKeyShopDTO.lstKeyShop) {
			strAdap[i] = item.name;
			i++;
		}
//		SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, strAdap);
        ArrayAdapter adapterNPP = new ArrayAdapter(parent, R.layout.simple_spinner_item, strAdap);
		spKeyShop.setAdapter(adapterNPP);
		indexKeyShop = 0;
		spKeyShop.setSelection(0);
		spKeyShop.setOnItemSelectedListener(this);
	}

	 /**
	 * Khoi tao du lieu cycle
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void initDataCycle(boolean isLoadAgain){
		if(isLoadAgain){
			int size = dtoKeyShopDTO.lstCycle.size();
			String[] strAdap = new String[size];
			//add text all neu truong hop co hon 2 nganh hang
			int i = 0;
			for (ValueItemDTO item : dtoKeyShopDTO.lstCycle) {
				long cycleIdTemp =  Long.parseLong(item.id);
				strAdap[i] =  String.format("%02d",Integer.valueOf(item.value)) + "/" + DateUtils.getYear(DateUtils.parseDateFromString(item.name, DateUtils.FORMAT_YEAR));
				if(cycleIdTemp == dtoKeyShopDTO.cycleIdNow ){
					indexCycle = i;
				}
				i++;
			}
//			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, strAdap);
			ArrayAdapter adapterNPP = new ArrayAdapter(parent, R.layout.simple_spinner_item, strAdap);
			spCycle.setAdapter(adapterNPP);
			// lay chu ki hien tai
			spCycle.setSelection(indexCycle);
			spCycle.setOnItemSelectedListener(this);
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
//		if(arg0 == btSearch){
//			getLevelOfKeyShopRegisted();
//			Bundle b = new Bundle();
//			b.putLong(IntentConstants.INTENT_KEYSHOP_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).ksId);
//			b.putString(IntentConstants.INTENT_CYCLE_ID, dtoKeyShopDTO.lstCycle.get(spCycle.getSelectedItemPosition()).id);
//			listener.onEvent(ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP, null,b );
//		} else
		if (arg0 == btUpdate) {
			getLevelOfKeyShopRegisted();
			if(lstItemClone.size() > 0){
				Bundle b = new Bundle();
				b.putString(IntentConstants.INTENT_CYCLE_ID, dtoKeyShopDTO.lstCycle.get(spCycle.getSelectedItemPosition()).id);
				b.putSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP, lstItemClone);
				b.putLong(IntentConstants.INTENT_KEYSHOP_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).ksId);
				b.putLong(IntentConstants.INTENT_FROM_CYCLE_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).fromCycleId);
				b.putLong(IntentConstants.INTENT_TO_CYCLE_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).toCycleId);
				listener.onEvent(ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP, null, b );
//				dismiss();
			}else{
				parent.showDialog(StringUtil.getString(R.string.TEXT_NOT_YET_REGISTER_KEYSHOP));
			}
		} else
			super.onClick(arg0);
	}

	 /**
	 * Lay muc keyshop da dang ki
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getLevelOfKeyShopRegisted(){
		int allStatusZero = 0;
		for(int i = 0, size = tbKeyShopLevel.getListChildRow().size() ; i < size; i++){
			RegisterKeyShopRow row = (RegisterKeyShopRow)tbKeyShopLevel.getListChildRow().get(i);
			RegisterKeyShopItemDTO temp = (RegisterKeyShopItemDTO)dtoKeyShopDTO.lstItem.get(i);
			int multiplier = StringUtil.isNullOrEmpty(row.edMultiple.getText()
					.toString()) ? 0 : Integer.parseInt(row.edMultiple
					.getText().toString());
			temp.ksCustomerLevelItem.multiplier = multiplier;
			if (multiplier > 0) {
				temp.ksCustomerLevelItem.status = KSCustomerDTO.STATE_ACTIVE;
			} else {
				if (temp.ksCustomerLevelItem.ksCustomerLevelId > 0) {
					temp.ksCustomerLevelItem.status = KSCustomerDTO.STATE_DELETE;
				}
				allStatusZero++;
			}
			if(temp.ksCustomerLevelItem.ksCustomerLevelId > 0 || multiplier > 0 )
				lstItemClone.add(temp);
//			if (!checkExistKeyShopRegistered(row)) {
//				if(temp.ksCustomerLevelItem.ksCustomerLevelId > 0 || multiplier > 0 )
//					lstItemClone.add(temp);
//			} else {
//				lstItemClone.remove(i);
//			}

			if(i == size - 1){
				// tat ca muc keyshop dang ky deu dua ve 0
				if(allStatusZero == size){
					updateStatusKeyShopCustomer(KSCustomerDTO.STATE_DELETE);
				}else{
					updateStatusKeyShopCustomer(KSCustomerDTO.STATE_NEW);
				}
			}
		}
	}

	 /**
	 * update trang thai cua keyshop customer
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void updateStatusKeyShopCustomer(int status){
		for(int i = 0, size = dtoKeyShopDTO.lstItem.size() ; i < size; i++){
			RegisterKeyShopItemDTO temp = (RegisterKeyShopItemDTO)dtoKeyShopDTO.lstItem.get(i);
			temp.ksCustomerItem.status = status;
		}
	}

//	 /**
//	 * kiem tra keyshop dang ki da ton tai hay chua
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: boolean
//	 * @throws:
//	*/
//	private boolean checkExistKeyShopRegistered(RegisterKeyShopRow row){
//		boolean result = false;
//		for (RegisterKeyShopItemDTO item : lstItemClone) {
//			// tranh cac truong hop bang 0
//			if (item.ksCustomerLevelItem.ksCustomerLevelId == row.dto.ksCustomerLevelItem.ksCustomerLevelId
//					&& item.ksCustomerItem.ksCustomerId == row.dto.ksCustomerItem.ksCustomerId
//					&& row.dto.ksCustomerLevelItem.ksCustomerLevelId > 0
//					&& row.dto.ksCustomerItem.ksCustomerId > 0) {
//				result = true;
//				break;
//			}
//		}
//		return result;
//
//	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		if (arg0 == spKeyShop) {
			if (indexKeyShop != arg2) {
				indexKeyShop = arg2;
				indexCycle = -1;
				lstItemClone.clear();
				getLevelOfKeyShop();
			}

		} else {
			if (indexCycle != arg2) {
				indexCycle = arg2;
				lstItemClone.clear();
				getLevelOfKeyShop();
			}
		}
	}

	 /**
	 * Lay cac muc cua keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getLevelOfKeyShop(){
//		getLevelOfKeyShopRegisted();
		Bundle b = new Bundle();
		b.putLong(IntentConstants.INTENT_KEYSHOP_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).ksId);
		b.putString(IntentConstants.INTENT_CYCLE_ID, dtoKeyShopDTO.lstCycle.get(spCycle.getSelectedItemPosition()).id);
		b.putLong(IntentConstants.INTENT_FROM_CYCLE_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).fromCycleId);
		b.putLong(IntentConstants.INTENT_TO_CYCLE_ID, dtoKeyShopDTO.lstKeyShop.get(spKeyShop.getSelectedItemPosition()).toCycleId);
		listener.onEvent(ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP, null,b );
	}

	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemSelectedListener#onNothingSelected(android.widget.AdapterView)
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	 /**
	 * set trang thai cua keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void setStatusKeyShop(int status){
		if(status == KSCustomerDTO.STATE_DELETE || status == KSCustomerDTO.STATE_PENDING){
			tvStatus.setText(StringUtil.getString(R.string.TEXT_STATUS_NOT_YET_REGISTER));
		}else if(status == KSCustomerDTO.STATE_ACTIVE){
			tvStatus.setText(StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_TBHV_DONE));
		}else if(status == KSCustomerDTO.STATE_NEW){
			tvStatus.setText(StringUtil.getString(R.string.TEXT_NOT_YET_APPROVE));
		}else if(status == KSCustomerDTO.STATE_REJECT){
			tvStatus.setText(StringUtil.getString(R.string.TEXT_DENY));
		}

	}


}
