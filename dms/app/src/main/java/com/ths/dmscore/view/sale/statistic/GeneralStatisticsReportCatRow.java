/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.ReportInfoDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

/**
 * Row bao cao theo nganh hang
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class GeneralStatisticsReportCatRow extends DMSTableRow {
	TextView tvContent;
	TextView tvAmount;
	TextView tvAmountPlan;
	TextView tvAmountProgress;
	TextView tvQuantity;
	TextView tvQuantityPlan;
	TextView tvQuantityProgress;

	public GeneralStatisticsReportCatRow(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_general_statistics_cat_row, GlobalInfo
				.getInstance().isSysShowPrice() ? null : new int[] {R.id.tvAmount,R.id.tvAmountProgress,R.id.tvAmountPlan});
		tvContent = (TextView) findViewById(R.id.tvContent);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvAmountPlan = (TextView) findViewById(R.id.tvAmountPlan);
		tvAmountProgress = (TextView) findViewById(R.id.tvAmountProgress);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
		tvQuantityPlan = (TextView) findViewById(R.id.tvQuantityPlan);
		tvQuantityProgress = (TextView) findViewById(R.id.tvQuantityProgress);
		setListener(listen);
	}

	 /**
	 * render Data
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderData(ReportInfoDTO item, double percentFinished){
		Double amountDone = 0.0;
		long quantityDone = 0;
		double amountProgress = 0;
		double quantityProgress = 0;
		if (GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED) {
			amountDone = item.amountApproved;
			quantityDone = item.quantityApproved;
		} else if (GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			amountDone = item.amountPending;
			quantityDone = item.quantityPending;
		} else {
			amountDone = item.amount;
			quantityDone = item.quantity;
		}
		amountProgress = StringUtil.calPercentUsingRound(item.amountPlan,
				amountDone);
		quantityProgress = StringUtil.calPercentUsingRound(
				item.quantityPlan, quantityDone);

		tvContent.setText(item.content);
		display(tvAmount, amountDone);
		display(tvAmountPlan, item.amountPlan);
		displayPercent(tvAmountProgress, amountProgress);
		if (amountProgress < percentFinished) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvAmountProgress);
		}
		display(tvQuantity, quantityDone);
		display(tvQuantityPlan, item.quantityPlan);
		displayPercent(tvQuantityProgress, quantityProgress);
		if (quantityProgress < percentFinished) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvQuantityProgress);
		}
	}
	 /**
	 * render nganh hang
	 * @author: Tuanlt11
	 * @param position
	 * @param item
	 * @param percentFinished
	 * @return: void
	 * @throws:
	*/
	public void renderLayout( ReportInfoDTO item, double percentFinished) {
		if (item.reportType == ReportInfoDTO.REPORT_DETAIL) {
			renderData(item, percentFinished);
		} else {
			// display math sale
//			String mathSale = context.getString(R.string.TEXT_MATH_SALE_2)
//					+ " ";
//			mathSale += String.valueOf(item.startSale);
			// showRowSum(mathSale,tvContent, tvQuota);
			// display cusomter do not have amount
			String titleCustomerDoNotAmount = context
					.getString(R.string.TEXT_TITLE_CUSOTMER_DO_NOT_AMOUNT)
					+ " ";
			String str = String.valueOf(item.numCustomerDoNotAmount) + "/"
					+ String.valueOf(item.numCustomerVisitPlanInMonth);

			// update textView number item

			SpannableObject obj = new SpannableObject();
			obj.addSpan(titleCustomerDoNotAmount,
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			obj.addSpan(str, ImageUtil.getColor(R.color.COLOR_LINK),
					Typeface.BOLD, item);

			// init clickable
			// obj.setTextView(tvProgress);

			// tvProgress.setText(sText);
			showRowSum(obj.getSpan(), true, tvContent, tvAmount, tvAmountPlan, tvAmountProgress,
					tvQuantity, tvQuantityPlan,tvQuantityProgress);
			PriUtils.getInstance().setOnClickListener(tvQuantityProgress, this);
		}
	}

	 /**
	 * render dong tong
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void renderSum(ReportInfoDTO item, double percentFinished){
		renderData(item, percentFinished);
		tvContent.setText(StringUtil.getString(R.string.TEXT_TOTAL));
		setTextColor(ImageUtil.getColor(R.color.BLACK), tvContent, tvAmount,
				tvAmountPlan, tvAmountProgress, tvQuantity, tvQuantityPlan,
				tvQuantityProgress);
		setTypeFace(Typeface.BOLD, tvContent, tvAmount,
				tvAmountPlan, tvAmountProgress, tvQuantity, tvQuantityPlan,
				tvQuantityProgress);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvQuantityProgress:
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_CUSTOMER_LIST_DO_NOT_AMOUNT_IN_MONTH, v, null);
			break;

		default:
			super.onClick(v);
			break;
		}
	}

}
