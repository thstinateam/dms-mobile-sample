/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.me.photo;

import java.io.Serializable;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Thong tin hinh anh
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class ImageDTO implements Serializable{
	public int id = 0;
	public String smallLink = "";
	public String fullLink = "";
	public String commentId="";
	public String userId="";
	public String parentType="";
	public ImageDTO(){
	}
	
	public ImageDTO(int id, String smallLink, String fullLink){
		this.id = id;
		this.smallLink = smallLink;
		this.fullLink = fullLink;
	}
	
	public void parseJson(JSONObject jsonObj){
		try{
			this.id = jsonObj.getInt("id");
			this.smallLink = jsonObj.getString("smallUrl");
			this.fullLink = jsonObj.getString("bigUrl");
			if (!StringUtil.isNullOrEmpty(this.fullLink)){
				//this.fullLink = ServerPath.IMAGE_PRODUCT_VNM + this.fullLink;
				this.fullLink = GlobalInfo.getInstance().getServerImageProductVNM() + this.fullLink;
			}
			this.parentType = jsonObj.getString("parentType");
		}
		catch (Exception e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}
	
}
