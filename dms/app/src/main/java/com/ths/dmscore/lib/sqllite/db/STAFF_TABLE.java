/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.dto.view.AttendanceDTO;
import com.ths.dmscore.dto.view.GsnppRouteSupervisionDTO;
import com.ths.dmscore.dto.view.ReportInfoDTO;
import com.ths.dmscore.dto.view.ReportKPISupervisorItemDTO;
import com.ths.dmscore.dto.view.ShopAndStaffDTO;
import com.ths.dmscore.dto.view.TBHVCustomerListDTO;
import com.ths.dmscore.dto.view.TBHVRouteSupervisionDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.GeneralStatisticsInfoViewDTO;
import com.ths.dmscore.dto.view.GsnppRouteSupervisionItem;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO.StaffItemDTO;
import com.ths.dmscore.dto.view.ListStaffDTO;
import com.ths.dmscore.dto.view.ReportKPIItemDTO;
import com.ths.dmscore.dto.view.ReportKPISupervisorViewDTO;
import com.ths.dmscore.dto.view.ReportKPIViewDTO;
import com.ths.dmscore.dto.view.StaffFeedbackDTO;
import com.ths.dmscore.dto.view.TBHVVisitCustomerNotificationDTO;
import com.ths.dmscore.dto.view.TBHVVisitCustomerNotificationDTO.TBHVVisitCustomerNotificationItem;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

/**
 * Thong tin nhan vien
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class STAFF_TABLE extends ABSTRACT_TABLE {
	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// ma nhan vien
	public static final String STAFF_CODE = "STAFF_CODE";
	// ten
	public static final String STAFF_NAME = "STAFF_NAME";
	// dia chi
	public static final String ADDRESS = "ADDRESS";
	// duong
	public static final String STREET = "STREET";
	// nuoc
	public static final String COUNTRY = "COUNTRY";
	// sdt ban
	public static final String PHONE = "PHONE";
	// so di dong
	public static final String MOBILE_PHONE = "MOBILEPHONE";
	// email
	public static final String MAIL = "MAIL";
	// gioi tinh: 1 nam, 0 nu
	public static final String SEX = "SEX";
	// ngay vao lam
	public static final String START_DATE = "START_DATE";
	// trinh do
	public static final String EDUCATION_ID = "EDUCATION_ID";
	// vi tri
	public static final String POSITION_ID = "POSITION_ID";
	// truong nay chua dung
	public static final String CAT_ID = "CAT_ID";
	// id nhan vien quan ly
	public static final String STAFF_OWNER_ID = "STAFF_OWNER_ID";
	// ngay sinh
	public static final String BIRTHDAY = "BIRTHDAY";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// ma vung
	public static final String AREA_CODE = "AREA_CODE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_TIME = "CREATE_TIME";
	// ngay cap nhat
	public static final String UPDATE_TIME = "UPDATE_TIME";
	// loai kenh
	public static final String CHANNEL_TYPE_ID = "CHANNEL_TYPE_ID";
	// 1: hoat dong, 0: ngung hoat dong
	public static final String STATUS = "STATUS";
	// mat khau
	public static final String PASSWORD = "PASSWORD";
	// so lan dang nhap sai
	public static final String NUMBER_LOGIN_FAIL = "NUMBER_LOGIN_FAIL";
	// trang thai khoa: 0: binh thuong, 1: khoa
	public static final String LOCKSTATUS = "LOCKSTATUS";
	// so tien ke hoach ngay
	public static final String PLAN = "PLAN";
	// ngay cap nhat ke hoach
	public static final String UPDATE_PLAN = "UPDATE_PLAN";
	// do hang cuoi cung duoc duyet
	public static final String LAST_APPROVE_ORDER = "LAST_APPROVE_ORDER";
	// don hang cuoi cung da dat
	public static final String LAST_ORDER = "LAST_ORDER";

	public static final String STAFF_TABLE = "STAFF";

	public STAFF_TABLE() {
		this.tableName = STAFF_TABLE;
		this.columns = new String[] { STAFF_ID, STAFF_CODE, STAFF_NAME,
				ADDRESS, STREET, COUNTRY, PHONE, MOBILE_PHONE, MAIL, SEX,
				START_DATE, EDUCATION_ID, POSITION_ID, CAT_ID, STAFF_OWNER_ID,
				BIRTHDAY, SHOP_ID, AREA_CODE, CREATE_USER, UPDATE_USER,
				CREATE_TIME, UPDATE_TIME, CHANNEL_TYPE_ID, STATUS, PASSWORD,
				NUMBER_LOGIN_FAIL, LOCKSTATUS, PLAN, UPDATE_PLAN,
				LAST_APPROVE_ORDER, LAST_ORDER, SYN_STATE };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public STAFF_TABLE(SQLiteDatabase mDB) {
		this.tableName = STAFF_TABLE;
		this.columns = new String[] { STAFF_ID, STAFF_CODE, STAFF_NAME,
				ADDRESS, STREET, COUNTRY, PHONE, MOBILE_PHONE, MAIL, SEX,
				START_DATE, EDUCATION_ID, POSITION_ID, CAT_ID, STAFF_OWNER_ID,
				BIRTHDAY, SHOP_ID, AREA_CODE, CREATE_USER, UPDATE_USER,
				CREATE_TIME, UPDATE_TIME, CHANNEL_TYPE_ID, STATUS, PASSWORD,
				NUMBER_LOGIN_FAIL, LOCKSTATUS, PLAN, UPDATE_PLAN,
				LAST_APPROVE_ORDER, LAST_ORDER, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 *
	 * get report info dayInOrder
	 *
	 * @param staffId
	 * @param shopId
	 * @return
	 * @return: GeneralStatisticsInfoViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 15, 2013
	 */
	public GeneralStatisticsInfoViewDTO getReportGeneralStatisticsDate(
			String staffId, String shopId) throws Exception {
		GeneralStatisticsInfoViewDTO result = new GeneralStatisticsInfoViewDTO();
		String dateNow = DateUtils.now();
		ArrayList<String> listParams = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT NAME, ");
		sqlQuery.append("       PLAN, ");
		sqlQuery.append("       CODE, ");
		if(GlobalInfo.getInstance().getSysCalUnapproved() == 2) {
			// lay cac doanh so da duyet + chua duyet
			sqlQuery.append("       ACTUALLY, ");
		} else if (GlobalInfo.getInstance().getSysCalUnapproved() == 1) {
			// lay cac doanh so da duyet
			sqlQuery.append("       ACTUALLY_APPROVED as ACTUALLY, ");
		}
		sqlQuery.append("       ACTUALLY_APPROVED ");
		sqlQuery.append("FROM   RPT_SALE_SUMMARY ");
		sqlQuery.append("WHERE  OBJECT_TYPE = 1 ");
		sqlQuery.append("       AND SHOP_ID = ? ");
		listParams.add(shopId);
		sqlQuery.append("       AND OBJECT_ID = ? ");
		listParams.add(staffId);
		sqlQuery.append("       AND STATUS = 1 ");
		sqlQuery.append("       AND TYPE = 1 ");
		sqlQuery.append("       AND VIEW_TYPE = 2 ");
		sqlQuery.append("       AND substr(SALE_DATE, 1, 10) = substr(?, 1, 10) ");
		listParams.add(dateNow);
		sqlQuery.append("ORDER  BY ORDINAL");

		Cursor c = null;
		try {
			c = rawQueries(sqlQuery.toString(), listParams);
			if (c.moveToFirst()) {
				do {
					ReportInfoDTO reportDto = new ReportInfoDTO();
					reportDto.initWithCursorReportByDate(c);
					result.listReportDate.add(reportDto);
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}

		return result;
	}

	/**
	 *
	 * get report info flow month
	 *
	 * @param staffId
	 * @param shopId
	 * @return
	 * @return: GeneralStatisticsInfoViewDTO
	 * @throws:
	 * @author: TRUNGHQM
	 * @throws Exception 
	 * @date: 25/1/2014
	 */
	public List<ReportInfoDTO> getReportGeneralStatisticsMonth(String staffId, String shopId) throws Exception {
		List<ReportInfoDTO> result = new ArrayList<ReportInfoDTO>();
		ArrayList<String> paramsList = new ArrayList<String>();
		EXCEPTION_DAY_TABLE exTable = new EXCEPTION_DAY_TABLE(mDB);
		long cycleId = exTable.getCycleId(new Date(), 0);
//		String dateNow = DateUtils.now();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT pi.PRODUCT_INFO_NAME, ");
		sqlQuery.append("       rpt.AMOUNT_PLAN, ");
		sqlQuery.append("       rpt.AMOUNT, ");
		sqlQuery.append("       rpt.AMOUNT_APPROVED AMOUNT_APPROVED, ");
		sqlQuery.append("       rpt.AMOUNT_PENDING AMOUNT_PENDING, ");
		sqlQuery.append("       rpt.AMOUNT_PLAN AMOUNT_PLAN, ");
		sqlQuery.append("       rpt.QUANTITY, ");
		sqlQuery.append("       rpt.QUANTITY_APPROVED QUANTITY_APPROVED, ");
		sqlQuery.append("       rpt.QUANTITY_PENDING QUANTITY_PENDING, ");
		sqlQuery.append("       rpt.QUANTITY_PLAN QUANTITY_PLAN, ");
		sqlQuery.append("       pi.PRODUCT_INFO_CODE ");
		sqlQuery.append("FROM   product_info pi ");
		sqlQuery.append(" 		left join rpt_staff_category rpt ");
		sqlQuery.append(" 			on pi.product_info_id =  rpt.product_info_id ");
		sqlQuery.append(" 		join cycle cy ");
		sqlQuery.append(" 			on rpt.cycle_id = cy.cycle_id AND rpt.cycle_id = ? ");
		paramsList.add(String.valueOf(cycleId));
//		sqlQuery.append("WHERE  pi.type = 1 ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND rpt.shop_id = ? ");
		paramsList.add(shopId);
		sqlQuery.append("       AND rpt.object_id = ? ");
		paramsList.add(staffId);
		sqlQuery.append("       AND rpt.STATUS = 1 ");
		sqlQuery.append("       AND substr(rpt.create_date, 1, 10) >= substr(cy.begin_date, 1, 10) ");
//		sqlQuery.append("       AND DATE(rpt.create_date) >= DATE(?, 'START OF MONTH') ");
		sqlQuery.append("       group by rpt.PRODUCT_INFO_ID ");
		sqlQuery.append("       order by pi.PRODUCT_INFO_NAME ");

		Cursor c = null;

		try {
			c = rawQueries(sqlQuery.toString(), paramsList);
			if (c.moveToFirst()) {
				do {
					ReportInfoDTO reportDto = new ReportInfoDTO();
					reportDto.initWithCursorReportDate(c);
					// // neu dong tong thi lay them thong tin khach hang chua
					// psds
					// // trong thang
					// if (reportDto.reportType == ReportInfoDTO.REPORT_TOTAL) {
					// reportDto.numCustomerVisitPlanInMonth = this
					// .getNumCustomerVisitPlanInMonth(shopId, staffId);
					// reportDto.numCustomerDoNotAmount = this
					// .getNumCustomerDoNotAmountInMonth(shopId,
					// staffId);
					// }
					result.add(reportDto);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}

		return result;
	}

	/**
	 * Lay thong tin cham cong
	 *
	 * @author: DungNX
	 * @param staffId
	 *            , shopPosition
	 * @return
	 */
	private List<AttendanceDTO> getAttendanceInfo(String staffId,
												  LatLng shopPosition, String shopId) {
		List<AttendanceDTO> result = new ArrayList<AttendanceDTO>();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		StringBuilder var1 = new StringBuilder();
		var1.append("SELECT ST.staff_id, ");
		var1.append("       ST.staff_code, ");
		var1.append("       ST.STAFF_NAME, ");
		var1.append("       SPL.lat, ");
		var1.append("       SPL.lng, ");
		var1.append("       Strftime('%Y-%m-%d %H:%M', SPL.create_date) CREATE_DATE ");
		var1.append("FROM   staff ST ");
		var1.append("       LEFT JOIN staff_position_log SPL ");
		var1.append("              ON ST.staff_id = SPL.staff_id ");
		var1.append("WHERE  ST.staff_id = ? ");
		var1.append("       AND substr(SPL.create_date, 1, 10) = substr(?, 1, 10) ");
		var1.append("ORDER  BY ST.staff_code, ");
		var1.append("          ST.STAFF_NAME, ");
		var1.append("          SPL.create_date ");

		String[] params = { staffId, date_now };

		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						AttendanceDTO atDTO = new AttendanceDTO();
						atDTO.initWithCursor(c, shopPosition, shopId );
						double cusDistance = -1;
						if (atDTO.position1.lat > 0 && atDTO.position1.lng > 0
								&& shopPosition.lat > 0 && shopPosition.lng > 0) {
							cusDistance = GlobalUtil.getDistanceBetween(
									atDTO.position1, shopPosition);
							atDTO.distance1 = cusDistance;
						}
						result.add(atDTO);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return result;
	}

	/**
	 * Lay tong thoi gian ghe tham
	 *
	 * @author: DungNX
	 * @param staffId
	 * @param shopId
	 * @return
	 */
	private int getTimeVisited(String staffId, String shopId) {
		int time = 0;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT staff_id, ");
		var1.append("       Sum(visit_time) AS VISIT_TIME ");
		var1.append("FROM   (SELECT al.staff_id                       AS Staff_id, ");
		var1.append("               ( Strftime('%H', al.end_time) * 3600 ) + ( ");
		var1.append("               ( Strftime('%M', al.end_time) * 60 ) + ");
		var1.append("               Strftime('%S', al.end_time) ");
		var1.append("               - ");
		var1.append("                                                          ( ");
		var1.append("               Strftime('%H', al.start_time) * 3600 ) - ");
		var1.append("                                                        ( ");
		var1.append("               Strftime('%M', al.start_time) * 60 ) ");
		var1.append("               - Strftime('%S', al.start_time) ) AS VISIT_TIME ");
		var1.append("        FROM   action_log al, ");
		var1.append("               customer ct ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND al.customer_id = ct.customer_id ");
		var1.append("               AND al.STAFF_ID = ? ");
		var1.append("               AND al.SHOP_ID = ? ");
		var1.append("               AND ct.status = 1 ");
		var1.append("               AND ct.shop_id = ? ");
		var1.append("               AND al.is_or = 0 ");
		var1.append("               AND ( ( al.object_type = 0 ");
		var1.append("                       AND al.end_time IS NOT NULL ) ");
		var1.append("                      OR al.object_type = 1 ) ");
		var1.append("               AND Date(al.start_time) = Date(?) ");
		var1.append("               AND ( al.end_time IS NULL ");
		var1.append("                      OR Date(al.end_time) >= Date(?) )) ");
		var1.append("GROUP  BY staff_id ");

		String[] params = { staffId, shopId, shopId, date_now, date_now };
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						time = CursorUtil.getInt(c, "VISIT_TIME");
						time = time / 60;
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return time;
	}

	/**
	 *
	 * gia tri 2a : Lay doanh so ke hoach thang cua NVBH
	 *
	 * @author: TRUNGHQM
	 * @param isReport
	 *            = true => Lay doanh so thang
	 * @param staff_id
	 * @return
	 * @return: List<ReportInfoDTO>
	 * @throws Exception 
	 * @throws:
	 */
	public List<ReportInfoDTO> getNumAmountPlanOfNVBHInMonth(String staffId, String shopId,
			boolean isReport) throws Exception {
		List<ReportInfoDTO> result = new ArrayList<ReportInfoDTO>();
		ArrayList<String> listParams = new ArrayList<String>();
		EXCEPTION_DAY_TABLE exTable = new EXCEPTION_DAY_TABLE(mDB);
		long cycleId = exTable.getCycleId(new Date(), 0);
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT NAME, ");
		sqlQuery.append("       PLAN, ");
		sqlQuery.append("       CODE, ");
		if(GlobalInfo.getInstance().getSysCalUnapproved() == 2) {
			// lay cac doanh so hoac san luong da duyet
			sqlQuery.append("       ACTUALLY, ");
		} else if (GlobalInfo.getInstance().getSysCalUnapproved() == 1) {
			// lay theo tong chua duyet + da duyet.
			sqlQuery.append("       ACTUALLY_APPROVED as ACTUALLY, ");
		}
		sqlQuery.append("       ACTUALLY_APPROVED ");
		sqlQuery.append("FROM   RPT_SALE_SUMMARY ");
		sqlQuery.append("WHERE  OBJECT_TYPE = 2 ");
		sqlQuery.append("       AND SHOP_ID = ? ");
		listParams.add(shopId);
		sqlQuery.append("       AND OBJECT_ID = ? ");
		listParams.add(staffId);
		sqlQuery.append("       AND STATUS = 1 ");
		if (isReport) {
			sqlQuery.append("   AND (CODE = 'DS' OR CODE = 'SL') ");
		} else {
			sqlQuery.append("   AND CODE = 'SKU' ");
		}
		sqlQuery.append("       AND TYPE = 2 ");
		sqlQuery.append("       AND VIEW_TYPE = 2 ");
		sqlQuery.append("       AND CYCLE_ID = ? ");
		listParams.add(String.valueOf(cycleId));
		sqlQuery.append("ORDER  BY ORDINAL");

		Cursor c = null;
		try {
			c = rawQueries(sqlQuery.toString(), listParams);
			if (c.moveToFirst()) {
				do {
					ReportInfoDTO reportDto = new ReportInfoDTO();
					reportDto.initWithCursorReportByDate(c);
					result.add(reportDto);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}

		return result;
	}

	/**
	 *
	 * get general statistics info view
	 *
	 * @author: TRUNGHQM
	 * @param ext
	 * @return
	 * @return: GeneralStatisticsInfoViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public GeneralStatisticsInfoViewDTO getGeneralStatisticsInfo(
			String staffId, String shopId, LatLng shopPosition) throws Exception {
		GeneralStatisticsInfoViewDTO result = new GeneralStatisticsInfoViewDTO();

		/** -------- start report time -------- */
		result.attendanceDTO = getAttendanceInfo(staffId, shopPosition, shopId);
		result.time = getTimeVisited(staffId, shopId);
		/** -------- end report time -------- */

		/** -------- start report dayInOrder -------- */
		/**
		 * lay thong tin bao cao doanh so theo ngay
		 */

		// cap nhat thong tin bao cao theo ngay - so
		result.listReportDate = this.getReportGeneralStatisticsDate(staffId,
				shopId).listReportDate;
		/**
		 * lay thong tin diem ban hang cua nhan vien ban hang theo ngay
		 */
		// cap nhat thong tin bao cao theo thang - so
		// doanh so, SL theo thang
		result.listReportMonth.addAll(getNumAmountPlanOfNVBHInMonth(staffId, shopId,
				true));
		// Nganh hang theo thang
		result.listReportCat.addAll(this.getReportGeneralStatisticsMonth(
				staffId, shopId));
		// SKU/Don hang
		result.listReportMonth.addAll(this.getNumAmountPlanOfNVBHInMonth(
				staffId, shopId, false));
		ReportInfoDTO diemThang = new ReportInfoDTO();
		diemThang.reportType = ReportInfoDTO.REPORT_TOTAL;
		diemThang.startSale = getMarkMonth(staffId, shopId);
		diemThang.numCustomerVisitPlanInMonth = this
					.getNumCustomerVisitPlanInMonth(shopId, staffId);
		diemThang.numCustomerDoNotAmount = this
					.getNumCustomerDoNotAmountInMonth(shopId, staffId);
		result.listReportCat.add(diemThang);
		/** -------- end report month ------ */
		// number fullDate plan sale in month
		EXCEPTION_DAY_TABLE excep = new EXCEPTION_DAY_TABLE(mDB);
		result.numberDayPlan = excep.getPlanWorkingDaysOfMonth(new Date(), shopId);
		// number fullDate sold in month
		result.numberDaySold = excep.getCurrentWorkingDaysOfMonth(shopId);
//		result.numberDaySold = new EXCEPTION_DAY_TABLE(mDB).getWorkingDaysOfMonthRecursive(shopId);
		result.progressSold = StringUtil.calPercentUsingRound(result.numberDayPlan, result.numberDaySold);

		return result;
	}

	public double getMarkMonth(String staff_id, String shopId) throws Exception {
		double DBH_MONTH = 0;
		String dateNow = DateUtils.now();
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		EXCEPTION_DAY_TABLE exTable = new EXCEPTION_DAY_TABLE(mDB);
		long cycleId = exTable.getCycleId(new Date(), 0);
		var1.append("	SELECT	");
		if(GlobalInfo.getInstance().getSysCalUnapproved() == 2) {
			// lay cac doanh so hoac san luong da duyet
			var1.append("	    ROUND (AVG (actually),	");
			var1.append("	    2) ACTUALLY, ");
		} else if (GlobalInfo.getInstance().getSysCalUnapproved() == 1) {
			// lay theo tong chua duyet + da duyet.
			var1.append("	    ROUND (AVG (ACTUALLY_APPROVED),	");
			var1.append("	    2) ACTUALLY, ");
		}
		var1.append("	    ROUND (AVG (actually),	");
		var1.append("	    2) ACTUALLY ");
		var1.append("	FROM	");
		var1.append("	    rpt_sale_summary	");
		var1.append("	WHERE	");
		var1.append("	    1 = 1    ");
		var1.append("	    AND CYCLE_ID         = ?	");
		paramsObject.add(String.valueOf(cycleId));
		var1.append("	    AND status         = 1	");
		var1.append("	    AND object_id      = ?	");
		paramsObject.add(staff_id);
		var1.append("	    AND shop_id    = ?	");
		paramsObject.add(shopId);
		var1.append("	    AND object_type    = 2	");
		var1.append("	    AND code           = 'DBH';	");

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), paramsObject);
			if (c != null) {

				if (c.moveToFirst()) {
					DBH_MONTH = CursorUtil.getDouble(c, "ACTUALLY");
				}
			}
		}finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					MyLog.w("",	 e2.toString());
				}
			}
		}
		return DBH_MONTH;
	}

	/**
	 *
	 * lay diem ban hang cua NVBH trong thang chua cong voi diem ban hang trong
	 * ngay hien tai
	 *
	 * @author: TRUNGHQM
	 * @param staff_id
	 * @return
	 * @return: float
	 * @throws:
	 */
	public float getSoldMathOfNVBHInMonth(String staffId, String shopId) {
		float DBH_MONTH = 0;
		ArrayList<String> listParams = new ArrayList<String>();
		StringBuffer request = new StringBuffer();
		request.append("SELECT ACTUALLY ");
		request.append("FROM   rpt_sale_summary ");
		request.append("WHERE  object_type = 2 ");
		request.append("       AND shop_id = ? ");
		listParams.add(shopId);
		request.append("       AND object_id = ? ");
		listParams.add(staffId);
		request.append("       AND code = 'DBH' ");
		request.append("       AND type = 2 ");
		request.append("       AND view_type = 2 ");
		request.append("       AND status = 1 ");
		request.append("       AND Date(sale_date) >= (SELECT Date('NOW','localtime', 'START OF MONTH', '0 MONTH', ");
		request.append("                                      '0 DAY')) ");
		request.append("       AND Date(sale_date) < (SELECT Date('NOW', 'localtime')) ");

		Cursor c = null;
		try {
			c = rawQueries(request.toString(), listParams);
			if (c != null) {

				if (c.moveToFirst()) {
					DBH_MONTH = CursorUtil.getFloat(c, "ACTUALLY");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					MyLog.w("",	 e2.toString());
				}
			}
		}
		return DBH_MONTH;
	}

	/**
	 *
	 * lay diem ban hang cua NVBH trong thang chua cong voi diem ban hang trong
	 * ngay hien tai
	 *
	 * @author: TRUNGHQM
	 * @param staff_id
	 * @return
	 * @return: float
	 * @throws:
	 */
	public float getSoldMathOfNVBHInDay(String staffId, String shopId) {
		float DBH_MONTH = 0;
		ArrayList<String> listParams = new ArrayList<String>();
		StringBuffer request = new StringBuffer();
		request.append("SELECT ACTUALLY ");
		request.append("FROM   rpt_sale_summary ");
		request.append("WHERE  object_type = 2 ");
		request.append("       AND shop_id = ? ");
		listParams.add(shopId);
		request.append("       AND object_id = ? ");
		listParams.add(staffId);
		request.append("       AND code = 'DBH' ");
		request.append("       AND type = 1 ");
		request.append("       AND view_type = 2 ");
		request.append("       AND status = 1 ");
		request.append("       AND Date(sale_date) = Date('NOW', 'LOCALTIME')");

		Cursor c = null;
		try {
			c = rawQueries(request.toString(), listParams);
			if (c != null) {

				if (c.moveToFirst()) {
					DBH_MONTH = CursorUtil.getFloat(c, "ACTUALLY");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					MyLog.w("",	 e2.toString());
				}
			}
		}
		return DBH_MONTH;
	}

	/**
	 *
	 * lay danh sach dem so thu trong thang
	 *
	 * @author: HieuNH
	 * @param toDate
	 * @return
	 * @return: int[]
	 * @throws:
	 */
	public int[] countNumberWeekDayInMonth() {
		String fromDate = "", toDate = "";
		int kq[] = new int[7];
		kq[0] = 0;
		kq[1] = 0;
		kq[2] = 0;
		kq[3] = 0;
		kq[4] = 0;
		kq[5] = 0;
		kq[6] = 0;

		int currentWeekday = -1;
		// lay thu cua ngay hien tai
		String strWeekdayCurrent = "SELECT strftime('%w', (select dayInOrder('now','localtime','start of month'))) CURRENT_WEEKDAY, dayInOrder('now','localtime','start of month') FROM_DATE, dayInOrder('now','localtime','start of month','+1 month','-1 fullDate') TO_DATE";
		Cursor c = null;
		try {
			c = rawQuery(strWeekdayCurrent, null);
			if (c != null) {
				if (c.moveToFirst()) {
					if (c.getColumnIndex("CURRENT_WEEKDAY") >= 0) {
						currentWeekday = CursorUtil.getInt(c, "CURRENT_WEEKDAY");
						fromDate = CursorUtil.getString(c, "FROM_DATE");
						toDate = CursorUtil.getString(c, "TO_DATE");
					} else {
						currentWeekday = -1;
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				MyLog.e("countNumberWeekDayInMonth", e2);
			}
		}

		int numberDate = 0;
		// so ngay tu ngay hien tai den ngay bat ki
		String requestNumberDate = "SELECT (julianday(dayInOrder('";
		requestNumberDate += toDate + "','localtime')) - julianday(dayInOrder('"
				+ fromDate + "','localtime'))) as NUMDATE";
		try {
			c = rawQuery(requestNumberDate, null);

			if (c != null) {

				if (c.moveToFirst()) {
					if (c.getColumnIndex("NUMDATE") >= 0) {
						numberDate = c.getInt(c.getColumnIndex("NUMDATE"));
					} else {
						numberDate = -1;
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				MyLog.e("countNumberWeekDayInMonth", e2);
			}
		}

		// dem so ngay trong tuan cua thang
		// + chua toi 1 tuan
		if (numberDate == 0) { // chi con 1 ngay hien tai
			kq[currentWeekday] = 1;
		} else if (numberDate > 0) { // con duoc tren 1 ngay
			numberDate++; // them 1 ngay : do la ngay 'toDate'
			int day = 1;
			do {
				kq[currentWeekday] = kq[currentWeekday] + 1;
				currentWeekday++;
				if (currentWeekday > 6) {
					currentWeekday = 0;
				}
				day++;
			} while (day <= numberDate);
		}
		return kq;
	}

	/**
	 *
	 * lay danh sach cac ngay con lai trong tuan giua 2 ngay bat ki
	 *
	 * @author: HaiTC3
	 * @param toDate
	 * @return
	 * @return: int[]
	 * @throws:
	 */
	public int[] countNumberWeekDayBetweenDate(String fromDate, String toDate) {
		int kq[] = new int[7];
		kq[0] = 0;
		kq[1] = 0;
		kq[2] = 0;
		kq[3] = 0;
		kq[4] = 0;
		kq[5] = 0;
		kq[6] = 0;

		int currentWeekday = -1;
		// lay thu cua ngay hien tai
		String strWeekdayCurrent = "SELECT strftime('%w','" + fromDate
				+ "') AS CURRENTWEEKDAY";
		Cursor c = null;
		try {
			c = rawQuery(strWeekdayCurrent, null);

			if (c != null) {

				if (c.moveToFirst()) {
					currentWeekday = CursorUtil.getInt(c, "CURRENTWEEKDAY", -1);
				}
			}
		} catch (Exception e) {
			MyLog.e("countNumberWeekDayBetweenDate", e);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				MyLog.e("countNumberWeekDayBetweenDate", e2);
			}
		}

		int numberDate = 0;
		// so ngay tu ngay hien tai den ngay bat ki
		String requestNumberDate = "SELECT (julianday(dayInOrder('";
		requestNumberDate += toDate + "')) - julianday(dayInOrder('" + fromDate
				+ "'))) as NUMDATE";
		try {
			c = rawQuery(requestNumberDate, null);

			if (c != null) {

				if (c.moveToFirst()) {
					if (c.getColumnIndex("NUMDATE") >= 0) {
						numberDate = c.getInt(c.getColumnIndex("NUMDATE"));
					} else {
						numberDate = -1;
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("countNumberWeekDayBetweenDate", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}

		// dem so ngay trong tuan cua thang
		// + chua toi 1 tuan
		if (numberDate == 0) { // chi con 1 ngay hien tai
			kq[currentWeekday] = 1;
		} else if (numberDate > 0) { // con duoc tren 1 ngay
			numberDate++; // them 1 ngay : do la ngay 'toDate'
			do {
				kq[currentWeekday] = kq[currentWeekday] + 1;
				currentWeekday++;
				if (currentWeekday > 6) {
					currentWeekday = 0;
				}
				numberDate--;
			} while (numberDate > 0);
		}
		return kq;
	}

	/**
	 *
	 * lay so ngay ban hang trong thang cua NVBH
	 *
	 * @author: HaiTC3
	 * @param shop_id
	 * @param staff_id
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getNumberSaleDayInMonth(String shop_id, String staff_id) {
		int numDay = 0;
		String request1 = "select value from ap_param ";
		Cursor c = null;
		try {
			c = rawQuery(request1, null);

			if (c != null) {

				if (c.moveToFirst()) {
					numDay = CursorUtil.getInt(c, "VALUE");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				MyLog.e("getNumberSaleDayInMonth", e2);
			}
		}
		return numDay;
	}

	/**
	 *
	 * giam sat lo trinh gsnpp tai
	 *
	 * @author: TampQ
	 * @param sortInfo
	 * @param shop_id
	 * @param staff_id
	 * @return
	 * @return: int
	 * @throws Exception
	 * @throws:
	 */
	public GsnppRouteSupervisionDTO getGsnppRouteSupervision(int staffId,
															 String today, String shopId, DMSSortInfo sortInfo) throws Exception {
		GsnppRouteSupervisionDTO dto = new GsnppRouteSupervisionDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String listStaffId = getListStaffOfSupervisor(staffId + "", shopId);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT STAFFLIST.STAFF_ID                                       AS STAFF_ID, ");
		var1.append("       STAFFLIST.STAFF_CODE                                     AS STAFF_CODE, ");
		var1.append("       STAFFLIST.NAME                                           AS NAME, ");
		var1.append("       STAFFLIST.SHOP_ID                                        AS SHOP_ID, ");
		var1.append("       STAFFLIST.MOBILE                                         AS MOBILE, ");
		var1.append("       STAFFLIST.LAT                                            AS LAT, ");
		var1.append("       STAFFLIST.LNG                                            AS LNG, ");
		var1.append("       VP.NUM_TOTAL_CUS                                         AS NUM_TOTAL_CUS ");
		var1.append("       , ");
		var1.append("       VISITED.NUM_VISITED                                      AS ");
		var1.append("       NUM_VISITED, ");
		var1.append("       VISITED.TOTAL_VISITED                                    AS ");
		var1.append("       TOTAL_VISITED, ");
		var1.append("       VISITED.NUM_LESTTHAN_2MIN                                AS ");
		var1.append("       NUM_LESTTHAN_2MIN, ");
		var1.append("       VISITED.NUM_MORETHAN_30MIN                               AS ");
		var1.append("       NUM_MORETHAN_30MIN, ");
		var1.append("       ( NUM_VISITED - NUM_MORETHAN_30MIN - NUM_LESTTHAN_2MIN ) AS NUM_ON_TIME, ");
		var1.append("       VISITED.NUM_RIGHT_PLAN                                   AS ");
		var1.append("       NUM_RIGHT_PLAN, ");
		var1.append("       VISITED.NUM_WRONG_PLAN                                   AS ");
		var1.append("       NUM_WRONG_PLAN, ");
		var1.append("       VISITED.LESSTHAN_2_MIN_LIST                              AS ");
		var1.append("       LESSTHAN_2_MIN_LIST, ");
		var1.append("       VISITED.MORETHAN_30_MIN_LIST                             AS ");
		var1.append("       MORETHAN_30_MIN_LIST,   ");
		var1.append("       VISIT_TIME   ");
		var1.append("FROM   (SELECT ST.STAFF_ID    AS STAFF_ID, ");
		var1.append("               ST.STAFF_CODE  AS STAFF_CODE, ");
		var1.append("               ST.STAFF_NAME  AS NAME, ");
		var1.append("               ST.SHOP_ID     AS SHOP_ID, ");
		var1.append("               ST.MOBILEPHONE AS MOBILE, ");
		var1.append("               AAC.LAT        AS LAT, ");
		var1.append("               AAC.LNG        AS LNG ");
		var1.append("        FROM   (SELECT * ");
		var1.append("                FROM   STAFF S ");

		//[Quang] sua cach lay danh sach NV thuoc quan ly GS
		var1.append("                WHERE  S.STAFF_ID in ( " + listStaffId + " ) ");
		//[Quang] sua cach lay danh sach NV thuoc quan ly GS

		var1.append("                	    AND S.STATUS = 1 ");
		var1.append("                ) ST ");
		var1.append("               LEFT JOIN (SELECT AL.*, ");
		var1.append("                                 MAX(AL.END_TIME) AS LASTENDVISITTIME ");
		var1.append("                          FROM   ACTION_LOG AL ");
		var1.append("                          WHERE  substr(START_TIME, 1, 10) = substr(?, 1, 10) ");
		params.add(date_now);
		var1.append("                          GROUP  BY STAFF_ID) AAC ");
		var1.append("                      ON ST.STAFF_ID = AAC.STAFF_ID ");
		var1.append("        WHERE  ST.STATUS = 1) STAFFLIST ");
		var1.append("       LEFT JOIN (SELECT VP.STAFF_ID AS STAFF_ID, ");
		var1.append("                         COUNT(*) AS NUM_TOTAL_CUS ");
		var1.append("                  FROM   VISIT_PLAN VP, ");
		var1.append("                         ROUTING RT, CUSTOMER CT, ");
		var1.append("                         (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                                 ROUTING_ID, ");
		var1.append("                                 CUSTOMER_ID, ");
		var1.append("                                 MONDAY, ");
		var1.append("                                 TUESDAY, ");
		var1.append("                                 WEDNESDAY, ");
		var1.append("                                 THURSDAY, ");
		var1.append("                                 FRIDAY, ");
		var1.append("                                 SATURDAY, ");
		var1.append("                                 SUNDAY, ");
		var1.append("                                 STATUS, ");
		var1.append("                                 SEQ2, ");
		var1.append("                                 SEQ3, ");
		var1.append("                                 SEQ4, ");
		var1.append("                                 SEQ5, ");
		var1.append("                                 SEQ6, ");
		var1.append("                                 SEQ7, ");
		var1.append("                                 SEQ8 ");
		var1.append("                          FROM   ROUTING_CUSTOMER");
		var1.append("                WHERE (DATE(END_DATE) >= dayInOrder(?) OR DATE(END_DATE) IS NULL) and ");
		params.add(date_now);
		var1.append("                substr(START_DATE, 1, 10) <= substr(?, 1, 10) and ");
		params.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		params.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		params.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		params.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		params.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		params.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		params.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		params.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		params.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		params.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		params.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		params.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		params.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");

		var1.append("  )RTC ");
		var1.append("                  WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("                         AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("                         AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("                         AND substr(VP.FROM_DATE, 1, 10) <= substr(?, 1, 10) ");
		params.add(date_now);
		var1.append("                         AND (VP.TO_DATE IS NULL OR DATE(VP.TO_DATE) >= DATE(?)) ");
		params.add(date_now);
		var1.append("                         AND VP.STATUS = 1 ");
		var1.append("                         AND RT.STATUS = 1 ");
		var1.append("                         AND RTC.STATUS = 1 ");
		var1.append("                         AND CT.STATUS = 1 ");
		var1.append("                         AND VP.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("                         AND RT.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("                         AND CT.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("                         AND RTC." + today + " = 1 ");
		var1.append("                  GROUP  BY STAFF_ID) VP ");
		var1.append("              ON STAFFLIST.STAFF_ID = VP.STAFF_ID ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         COUNT(CASE WHEN IS_OR = 0 THEN STAFF_ID END)       AS NUM_VISITED, ");
		var1.append("                         COUNT(STAFF_ID)       AS TOTAL_VISITED, ");
		var1.append("                         COUNT(CASE ");
		var1.append("                                 WHEN (IS_OR = 0 AND VISIT_TIME < 120) THEN 1 ");
		var1.append("                               END)            AS NUM_LESTTHAN_2MIN, ");
		var1.append("                         COUNT(CASE ");
		var1.append("                                 WHEN (IS_OR = 0 AND VISIT_TIME > 1800) THEN 1 ");
		var1.append("                               END)            AS NUM_MORETHAN_30MIN, ");
		var1.append("                         COUNT(CASE ");
		var1.append("                                 WHEN IS_OR = 0 THEN 1 ");
		var1.append("                               END)            AS NUM_RIGHT_PLAN, ");
		var1.append("                         COUNT(CASE ");
		var1.append("                                 WHEN IS_OR = 1 THEN 1 ");
		var1.append("                               END)            AS NUM_WRONG_PLAN, ");
		var1.append("                         GROUP_CONCAT(( CASE ");
		var1.append("                                          WHEN (IS_OR = 0 AND VISIT_TIME < 120) THEN CUSTOMER_ID ");
		var1.append("                                        END )) AS LESSTHAN_2_MIN_LIST, ");
		var1.append("                         GROUP_CONCAT(( CASE ");
		var1.append("                                          WHEN (IS_OR = 0 AND VISIT_TIME > 1800) THEN CUSTOMER_ID ");
		var1.append("                                        END )) AS MORETHAN_30_MIN_LIST, ");
		var1.append("                         Sum(VISIT_TIME) as VISIT_TIME ");
		var1.append("                  FROM   (SELECT AL.STAFF_ID                       AS STAFF_ID, ");
		var1.append("                                 AL.CUSTOMER_ID                    AS ");
		var1.append("                                 CUSTOMER_ID, ");
		var1.append("                                 AL.OBJECT_ID                      AS OBJECT_ID, ");
		var1.append("                                 AL.OBJECT_TYPE                    AS ");
		var1.append("                                 OBJECT_TYPE, ");
		var1.append("                                 AL.IS_OR                          AS IS_OR, ");
		var1.append("                                 AL.START_TIME                     AS START_TIME ");
		var1.append("                                 , ");
		var1.append("                                 AL.END_TIME ");
		var1.append("                                 AS END_TIME, ");
		var1.append("                                 ( STRFTIME('%H', AL.END_TIME) * 3600 ) + ( ");
		var1.append("                                 ( STRFTIME('%M', AL.END_TIME) * 60 ) + ");
		var1.append("                                 STRFTIME('%S', AL.END_TIME) ");
		var1.append("                                 - ");
		var1.append("                                                                            ( ");
		var1.append("                                 STRFTIME('%H', AL.START_TIME) * 3600 ) - ");
		var1.append("                                                                          ( ");
		var1.append("                                 STRFTIME('%M', AL.START_TIME) * 60 ) ");
		var1.append("                                 - STRFTIME('%S', AL.START_TIME) ) AS VISIT_TIME ");
		var1.append("                          FROM   ACTION_LOG AL, ");
		var1.append("                                 CUSTOMER CT ");
		var1.append("                          WHERE  1 = 1 ");
		var1.append("                                 AND AL.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("                                 AND AL.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("                                 AND CT.STATUS = 1 ");
		var1.append("                                 AND ( ( AL.OBJECT_TYPE = 0 ");
		var1.append("                                         AND AL.END_TIME IS NOT NULL ) ");
		var1.append("                                        OR AL.OBJECT_TYPE = 1 ) ");
		var1.append("                                 AND substr(AL.START_TIME, 1, 10) = ");
		var1.append("                                     substr(?, 1, 10) ");
		params.add(date_now);
		var1.append("                                 AND ( AL.END_TIME IS NULL ");
		var1.append("                                        OR DATE(AL.END_TIME) >= ");
		var1.append("                                           DATE(?) )) ");
		params.add(date_now);
		var1.append("                  GROUP  BY STAFF_ID) VISITED ");
		var1.append("              ON STAFFLIST.STAFF_ID = VISITED.STAFF_ID ");

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY STAFF_CODE ASC, NAME ASC ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "STAFF_CODE")
				.addMapper(SortActionConstants.NAME, "NAME")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		var1.append(orderByStr);

		Cursor c = rawQueries(var1.toString(), params);
		try {
			if (c != null) {

				if (c.moveToFirst()) {
					do {
						GsnppRouteSupervisionItem item = new GsnppRouteSupervisionItem();
						item.initDataFromCursor(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		return dto;
	}

	/**
	 * lay so luong khach hang can vieng tham trong thang hien tai
	 * @author: hoanpd1
	 * @since: 09:08:42 10-04-2015
	 * @return: int
	 * @throws:
	 * @param shop_id
	 * @param staff_id
	 * @return
	 */
	public int getNumCustomerVisitPlanInMonth(String shop_id, String staff_id) {
		int numCustomer = 0;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();
		sql.append(" SELECT Count(DISTINCT RTC.CUSTOMER_ID) AS NUM_CUSTOMER ");
		sql.append(" FROM VISIT_PLAN VP, ");
		sql.append("      ROUTING RT, ");
		sql.append("      (SELECT routing_id, ");
		sql.append("              customer_id ");
		sql.append("       FROM routing_customer ");
		sql.append("       WHERE status = 1 ");
		sql.append("             AND (monday ");
		sql.append("                  OR tuesday ");
		sql.append("                  OR wednesday ");
		sql.append("                  OR thursday ");
		sql.append("                  OR friday ");
		sql.append("                  OR saturday ");
		sql.append("                  OR sunday) ");
		sql.append("             AND substr(START_DATE, 1, 10) <= ? ");
		params.add(date_now);
		sql.append("             AND Ifnull (substr(END_DATE,1,10) >= ? , 1) ");
		params.add(date_now);
		sql.append("       ) RTC           , ");
		sql.append("       customer cus ");
		sql.append(" WHERE VP.STAFF_ID = ? ");
		params.add(staff_id);
		sql.append("       AND VP.SHOP_ID = ? ");
		params.add(shop_id);
		sql.append("       AND substr(VP.FROM_DATE, 1, 10) <= ? ");
		params.add(date_now);
		sql.append("       AND Ifnull (substr(VP.TO_DATE,1,10) >= ?, 1) ");
		params.add(date_now);
		sql.append("       AND VP.STATUS = 1 ");
		sql.append("       AND VP.ROUTING_ID = RT.ROUTING_ID ");
		sql.append("       AND RT.SHOP_ID = ? ");
		params.add(shop_id);
		sql.append("       AND RT.STATUS = 1 ");
		sql.append("       AND RT.ROUTING_ID = RTC.ROUTING_ID ");
		sql.append("       AND RTC.customer_id = cus.customer_id ");
		sql.append("       AND cus.status = 1");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {

				if (c.moveToFirst()) {
					numCustomer = CursorUtil.getInt(c, "NUM_CUSTOMER");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					MyLog.w("",	 e2.toString());
				}
			}
		}
		return numCustomer;
	}

	/**
	 * lay so khach hang chua phat sinh doanh so trong thang hien tai
	 * @author: hoanpd1
	 * @since: 09:08:25 10-04-2015
	 * @return: int
	 * @throws:
	 * @param shop_id
	 * @param staff_id
	 * @return
	 * @throws Exception
	 */
	public int getNumCustomerDoNotAmountInMonth(String shop_id, String staff_id) throws Exception{
		int numCustomer = 0;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();
		sql.append(" SELECT Count(DISTINCT RTC.customer_id) NUM_CUSTOMER ");
		sql.append(" FROM visit_plan VP, ");
		sql.append("      routing RT, ");
		sql.append("      (SELECT * ");
		sql.append("       FROM routing_customer ");
		sql.append("       WHERE 1 = 1 ");
		sql.append("             AND  substr(START_DATE, 1, 10)<= ? ");
		params.add(date_now);
		sql.append("             AND (end_date IS NULL ");
		sql.append("                 OR end_date = '' ");
		sql.append("                 OR substr(end_date,1,10) >= ? ) ");
		params.add(date_now);
		sql.append("             AND status = 1 ");
		sql.append("             AND (monday ");
		sql.append("                  OR tuesday ");
		sql.append("                  OR wednesday ");
		sql.append("                  OR thursday ");
		sql.append("                  OR friday ");
		sql.append("                  OR saturday ");
		sql.append("                  OR sunday) ");
		sql.append("      ) RTC , ");
		sql.append("      customer cus");
		sql.append(" LEFT JOIN (SELECT SC.customer_id        CUSTOMER_ID, ");
		sql.append("                   SC.last_order         LAST_ORDER, ");
		sql.append("                   SC.last_approve_order LAST_APPROVE_ORDER ");
		sql.append("            FROM staff_customer SC ");
		sql.append("            WHERE 1 = 1 ");
		sql.append("                  AND  sc.shop_id = ? ");
		params.add(shop_id);
		sql.append("                  AND SC.staff_id = ? ");
		params.add(staff_id);
		sql.append("            ) STC ON STC.customer_id = RTC.customer_id ");
		sql.append(" WHERE VP.staff_id = ? ");
		params.add(staff_id);
		sql.append("       AND VP.shop_id = ? ");
		params.add(shop_id);
		sql.append("       AND substr(VP.from_date, 1, 10) <= ? ");
		params.add(date_now);
		sql.append("       AND Ifnull (substr(VP.to_date,1,10) >= ?, 1) ");
		params.add(date_now);
		sql.append("       AND VP.status = 1 ");
		sql.append("       AND VP.routing_id = RT.routing_id ");
		sql.append("       AND RT.shop_id = ? ");
		params.add(shop_id);
		sql.append("       AND RT.status = 1 ");
		sql.append("       AND RT.routing_id = RTC.routing_id ");
		sql.append("       AND Ifnull (substr(STC.last_order,1,10) < ? , 1) ");
		params.add(startOfMonth);
		sql.append("       AND Ifnull (substr(STC.last_approve_order,1,10) < substr( ? , 1, 10) , 1) ");
		params.add(startOfMonth);
		sql.append("       AND RTC.customer_id = cus.customer_id ");
		sql.append("       AND cus.status = 1");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					numCustomer = CursorUtil.getInt(c, "NUM_CUSTOMER");
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					MyLog.w("",	 e2.toString());
				}
			}
		}
		return numCustomer;
	}

	/**
	 *
	 * lay combobox nvbh cho man hinh theo doi khac phuc
	 *
	 * @author: ThanhNN8
	 * @param
	 * @return
	 * @return: ComboboxFollowProblemDTO
	 * @throws Exception
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getStaffCodeComboboxProblemNVBHOfSuperVisor() throws Exception {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		String staffOwnerId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+ "";
		String strListStaffId = getListStaffOfSupervisor(staffOwnerId, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());

		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder.append("SELECT STAFF_NAME, STAFF_ID FROM STAFF");
		stringbuilder.append(" WHERE STAFF_ID in ( " + strListStaffId + " ) ");
		stringbuilder
				.append(" and staff_type_id in (select channel_type_id from channel_type where type = 2 and status = 1 and object_type in (1,2)) ");
		stringbuilder.append(" AND STATUS = 1 ");
		stringbuilder.append(" AND SHOP_ID = ? ");
		stringbuilder.append(" ORDER BY STAFF_NAME");
		String requestGetFollowProblemList = stringbuilder.toString();
		Cursor c = null;
		String[] params = new String[] {
				GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId() + "" };

		try {
			c = rawQuery(requestGetFollowProblemList, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
						comboboxNVBHDTO.value = CursorUtil.getString(c, "STAFF_ID");
						comboboxNVBHDTO.name = CursorUtil.getString(c, "STAFF_NAME");
						comboboxNVBHDTO.fullName = CursorUtil.getString(c, STAFF_NAME);
						result.add(comboboxNVBHDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			// return null;
			result = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * Lay ds GSNPP cua
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopOwnerID
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getStaffCodeComboboxProblemSuperVisorOfTBHV(
			String shopIdVung) throws Exception {
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursiveReverse(shopIdVung);
		String strListShop = TextUtils.join(",", listShopId);
		StringBuffer stringbuilder = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();

		stringbuilder.append("	SELECT DISTINCT GSNPP.staff_code || '-'  || GSNPP.staff_name AS STAFF_CODE, GSNPP.staff_id STAFF_ID, GS_SHOP_ID 	");
		stringbuilder.append("	FROM	");
		stringbuilder.append("	    ( SELECT	");
		stringbuilder.append("	        GS.STAFF_ID          AS STAFF_ID,	");
		stringbuilder.append("	        GS.STAFF_CODE        AS STAFF_CODE,	");
		stringbuilder.append("	        GS.STAFF_NAME        AS STAFF_NAME,	");
		stringbuilder.append("	        GS.shop_id           AS GS_SHOP_ID,	");
		stringbuilder.append("	        SH.SHOP_ID    AS NVBH_SHOP_ID,	");
		stringbuilder.append("	        SH.SHOP_CODE   AS NVBH_SHOP_CODE,	");
		stringbuilder.append("	        COUNT(S.STAFF_ID) AS NUM_NVBH	");
		stringbuilder.append("	    FROM	");
		stringbuilder.append("	        (SELECT	");
		stringbuilder.append("	            *	");
		stringbuilder.append("	        FROM	");
		stringbuilder.append("	            STAFF GS	");
		stringbuilder.append("	        WHERE	");
		stringbuilder.append("	            1=1	");
		stringbuilder.append("	            AND GS.STATUS = 1	");
		stringbuilder.append("	            AND GS.SHOP_ID = ?	");
		params.add(String.valueOf(shopIdVung));
		stringbuilder.append("	            AND GS.STAFF_ID in (	");
		stringbuilder.append(PriUtils.getInstance().getListSupervisorOfASM(String.valueOf(staffId)));
		stringbuilder.append("	            ) ) GS,	");
		stringbuilder.append("	        PARENT_STAFF_MAP PS,	");
		stringbuilder.append("	        STAFF S,	");
		stringbuilder.append("	        SHOP SH,	");
		stringbuilder.append("	        CHANNEL_TYPE CT	");
		stringbuilder.append("	    WHERE	");
		stringbuilder.append("	        PS.PARENT_STAFF_ID =  GS.STAFF_ID	");
		stringbuilder.append("	        AND S.STAFF_ID = PS.STAFF_ID	");
		stringbuilder.append("	        AND SH.SHOP_ID = S.SHOP_ID	");
		stringbuilder.append("	        AND S.SHOP_ID IN (" + strListShop + ")	");
		stringbuilder.append("	        AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID	");
		stringbuilder.append("	        AND CT.TYPE = 2	");
		stringbuilder.append("	        AND CT.OBJECT_TYPE IN (	");
		stringbuilder.append("	            1, 2	");
		stringbuilder.append("	        )	");
		stringbuilder.append("	        AND PS.STATUS = 1	");
		stringbuilder.append("	        AND CT.STATUS = 1	");
		stringbuilder.append("	        AND S.STATUS = 1	");
		stringbuilder.append("	        AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)	");
		stringbuilder.append("	    GROUP BY	");
		stringbuilder.append("	        GS.STAFF_ID,	");
		stringbuilder.append("	        SH.SHOP_ID	");
		stringbuilder.append("	    ORDER  BY	");
		stringbuilder.append("	        SH.SHOP_CODE,	");
		stringbuilder.append("	        GS.STAFF_NAME       ) GSNPP	");
		stringbuilder.append("	    ORDER  BY GSNPP.STAFF_CODE 	");

		String requestGetFollowProblemList = stringbuilder.toString();
		Cursor c = null;

		try {
			c = rawQueries(requestGetFollowProblemList, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
						comboboxNVBHDTO.value = CursorUtil.getString(c, "STAFF_ID");
						comboboxNVBHDTO.name = CursorUtil.getString(c, "STAFF_CODE");
						comboboxNVBHDTO.fullName = CursorUtil.getString(c, "GS_SHOP_ID");
						result.add(comboboxNVBHDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			// return null;
			result = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * lay combobox type problem cho man hinh theo doi khac phuc
	 *
	 * @author: HieuNH
	 * @param
	 * @return
	 * @return: ComboboxFollowProblemDTO
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getComboboxProblemTypeProblemOfSuperVisor() {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder
				.append("SELECT AP.AP_PARAM_CODE, AP.AP_PARAM_NAME FROM AP_PARAM AP");
		stringbuilder
				.append(" WHERE AP.TYPE LIKE 'FEEDBACK_TYPE' AND STATUS = 1 ORDER BY AP.AP_PARAM_NAME");
		String requestGetFollowProblemList = stringbuilder.toString();
		Cursor c = null;
		String[] params = new String[] {};

		try {
			c = rawQuery(requestGetFollowProblemList, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
						comboboxNVBHDTO.value = CursorUtil.getString(c, "AP_PARAM_CODE");
						comboboxNVBHDTO.name = CursorUtil.getString(c, "AP_PARAM_NAME");
						result.add(comboboxNVBHDTO);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * check db co phai cua user dang dang nhap hay kg?
	 *
	 * @author: HieuNH
	 * @param
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean checkUserDB(String staffCode, int roleType) {
		boolean result = false;
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder.append("SELECT STAFF_CODE, ROLE_TYPE FROM STAFF");
		stringbuilder
				.append(" WHERE STAFF_CODE = ? AND ROLE_TYPE = ? AND ROLE_TYPE = (SELECT MAX(ROLE_TYPE) FROM STAFF)");
		String requestCheckUserDB = stringbuilder.toString();
		Cursor c = null;
		String[] params = new String[] { staffCode, "" + roleType };

		try {
			c = rawQuery(requestCheckUserDB, params);
			if (c != null) {
				if (c.moveToFirst()) {
					result = true;
				}
			}
		} catch (Exception e) {
			// return null;
			result = false;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (!result) {
			mDB.close();
		}
		return result;
	}

	/**
	 *
	 * danh sach nhan vien giam sat sai tuyen
	 *
	 * @author: TamPQ
	 * @param
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public TBHVRouteSupervisionDTO getTbhvRouteSupervision(int staff_id,
														   String shopId, int day) throws Exception {
		String today = DateUtils.getToday();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		TBHVRouteSupervisionDTO dto = null;

		String staffIdASM = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";
		String listGs = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopId);
		String shopStr = TextUtils.join(",", shopList);

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT GS.staff_id          AS GS_STAFF_ID, ");
		var1.append("               GS.staff_code        AS NPP_STAFF_CODE, ");
		var1.append("               GS.staff_name        AS NPP_STAFF_NAME, ");
		var1.append("               SH.shop_id           AS NPP_SHOP_ID, ");
		var1.append("               SH.shop_code         AS NPP_SHOP_CODE, ");
		var1.append("               SH.shop_name         AS NPP_SHOP_NAME, ");
		var1.append("               Count(NVBH.staff_id) AS NUM_NVBH ");
		var1.append("        FROM   staff NVBH, ");
		var1.append("               channel_type CH, ");
		var1.append("               shop SH, ");
		var1.append("               staff GS ");
		var1.append("        WHERE  NVBH.staff_type_id = CH.channel_type_id ");
		var1.append("               AND CH.type = 2 ");
		var1.append("               AND CH.object_type IN ( 1, 2 ) ");
		var1.append("               AND NVBH.shop_id = SH.shop_id ");
		var1.append("               AND NVBH.status = 1 ");
		var1.append("               AND SH.status = 1 ");
		var1.append("               AND GS.status = 1 ");
		var1.append("               AND GS.staff_id IN ( ");
		var1.append(listGs);
		var1.append("               )");
		var1.append("               AND NVBH.shop_id IN ( " + shopStr + " ) ");
		var1.append("               AND NVBH.staff_id IN ");
		var1.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		var1.append("				AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
		var1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		var1.append("        GROUP  BY SH.shop_id, ");
		var1.append("                  GS.staff_id ");
		var1.append("        ORDER  BY SH.shop_code, ");
		var1.append("                  GS.staff_name) NPP_LIST ");
		var1.append("       JOIN (SELECT ST.staff_id                 AS NVBH_STAFF_ID, ");
		var1.append("                    ST.staff_code               AS NVBH_STAFF_CODE, ");
		var1.append("                    ST.staff_name               AS NVBH_STAFF_NAME, ");
		var1.append("                    GS.staff_id           AS NVBH_STAFF_OWNER_ID, ");
		var1.append("                    ST.shop_id                  AS NVBH_SHOP_ID_1, ");
		var1.append("                    TPD.training_plan_detail_id AS NVBH_TPD_ID, ");
		var1.append("                    TPD.training_plan_id        AS NVBH_TP_ID, ");
		var1.append("                    TPD.training_date           AS NVBH_TRAINING_DATE, ");
		var1.append("                    SHOP.SHOP_CODE        AS NVBH_SHOP_CODE, ");
		var1.append("                    SHOP.SHOP_NAME        AS NVBH_SHOP_NAME, ST.SHOP_ID ");
		var1.append("             FROM   training_plan_detail TPD, ");
		var1.append("                    training_plan TP, ");
		var1.append("                    staff GS, ");
		var1.append("                    staff ST, ");
		var1.append("                    channel_type CH, ");
		var1.append("                    SHOP ");
		var1.append("             WHERE  Date(TPD.training_date) = Date (?) ");
		var1.append("                    AND ST.SHOP_ID = SHOP.SHOP_ID ");
		var1.append("                    AND ST.staff_type_id = CH.channel_type_id ");
		var1.append("                    AND CH.object_type IN ( 1, 2 ) ");
		var1.append("                    AND CH.type = 2 ");
		var1.append("               	 AND GS.staff_id IN ( ");
		var1.append(listGs);
		var1.append("               	 )");
		var1.append("               AND ST.shop_id IN ( " + shopStr + " ) ");
		var1.append("               AND ST.staff_id IN ");
		var1.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		var1.append("				AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
		var1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		var1.append("                    AND ST.status = 1 ");
		var1.append("                    AND TP.training_plan_id = TPD.training_plan_id ");
		var1.append("                    AND TPD.status IN ( 0, 1 ) ");
		var1.append("                    AND TP.status = 1 ");
		var1.append("                    AND TPD.staff_id = ST.staff_id)NVBH_TPD ");
		var1.append("         ON NPP_LIST.GS_STAFF_ID = NVBH_TPD.nvbh_staff_owner_id AND NPP_LIST.NPP_SHOP_ID = NVBH_TPD.SHOP_ID ");
		var1.append("       LEFT JOIN (SELECT AL_STAFF.staff_id                         AS ");
		var1.append("                         NPP_STAFF_ID, ");
		var1.append("                         Group_concat(AL_STAFF.customer_id)        AS ");
		var1.append("                         NPP_VISITED_ORDER ");
		var1.append("                              , ");
		var1.append("       Group_concat(AL_STAFF.lat)                AS NPP_VISITED_LAT, ");
		var1.append("       Group_concat(AL_STAFF.lng)                AS NPP_VISITED_LNG, ");
		var1.append("       Group_concat(AL_STAFF.start_time)         AS NPP_VISITED_START_TIME, ");
		var1.append("       Group_concat(AL_STAFF.customer_code_name) AS ");
		var1.append("       NPP_VISITED_CUSTOMER_CODE_NAME, ");
		var1.append("       Group_concat(ifnull(AL_STAFF.customer_address,''),'seperator')   AS ");
		var1.append("       NPP_VISITED_CUSTOMER_ADDRESS, ");
		var1.append("       MAX(AL_STAFF.LAST_VISITED_TIME)           AS LAST_VISITED_TIME, ");
		var1.append("       AL_STAFF.CUSTOMER_ID                      AS CUSTOMER_ID, ");
		var1.append("       CUSTOMER.CUSTOMER_NAME                    AS CUSTOMER_NAME, ");
		var1.append("       SUBSTR(CUSTOMER.CUSTOMER_CODE, 1, 3)      AS CUSTOMER_CODE, ");
		var1.append("       AL_STAFF.CUSTOMER_ADDRESS                    AS CUSTOMER_ADDRESS ");
		var1.append("       FROM   (SELECT action_log.*, ");
		var1.append("               ( Substr(customer.customer_code, 1, 3) ");
		var1.append("                 || ' - ' ");
		var1.append("                 || customer.customer_name ) AS CUSTOMER_CODE_NAME, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN customer.address IS NOT NULL THEN customer.address ");
		var1.append("                   ELSE ( customer.housenumber ");
		var1.append("                          || ' - ' ");
		var1.append("                          || customer.street ) ");
		var1.append("                 END )                       AS CUSTOMER_ADDRESS, ");
		var1.append("               Min(start_time)               AS VISITED_TIME, ");
		var1.append("               Max(start_time)               AS LAST_VISITED_TIME ");
		var1.append("        FROM   action_log, ");
		var1.append("               customer ");
		var1.append("        WHERE  Date(start_time) = Date(?) ");
		var1.append("               AND customer.customer_id = action_log.customer_id ");
		var1.append("               AND action_log.object_type in (0,1) ");
		var1.append("        GROUP  BY action_log.staff_id, ");
		var1.append("                  action_log.customer_id ");
		var1.append("        ORDER  BY visited_time ASC) AL_STAFF, ");
		var1.append("       staff, ");
		var1.append("       channel_type CH, ");
		var1.append("       customer ");
		var1.append("       WHERE  AL_STAFF.staff_id = staff.staff_id ");
		var1.append("       AND staff.staff_type_id = CH.channel_type_id ");
		var1.append("       AND CH.object_type = 5 ");
		var1.append("       AND CH.type = 2 ");
		var1.append("       AND customer.customer_id = AL_STAFF.customer_id ");
		var1.append("       AND customer.status = 1 ");
		var1.append("       GROUP  BY AL_STAFF.staff_id)NPP_AL ");
		var1.append("              ON NPP_LIST.GS_STAFF_ID = NPP_AL.npp_staff_id ");
		var1.append("       LEFT JOIN ");
		var1.append("       (SELECT staff_id               AS NVBH_STAFF_ID_1, ");
		var1.append("               Count(RTC.customer_id) AS NUM_CUS_PLAN, ");
		var1.append("                         GROUP_CONCAT(SEQ" + day
				+ ")        AS NVBH_DAY_SEQ, ");
		var1.append("                         GROUP_CONCAT(RTC.CUSTOMER_ID) AS NVBH_CUSTOMER_PLAN ");
		var1.append("        FROM   visit_plan VP, ");
		var1.append("               routing RT, ");
		var1.append("               customer CT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       START_DATE, ");
		var1.append("                       END_DATE, ");
		var1.append("                       STATUS, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                 WHERE (DATE(END_DATE) >= DATE(?) OR DATE(END_DATE) IS NULL) ");
		var1.append("                        AND DATE(START_DATE) <= DATE(?) ");
		var1.append("                        AND ((WEEK1 IS NULL ");
		var1.append("                              AND WEEK2 IS NULL ");
		var1.append("                              AND WEEK3 IS NULL ");
		var1.append("                              AND WEEK4 IS NULL) ");
		var1.append("                              OR (((cast((julianday(?) - julianday(start_date)) / 7 AS integer)  ");
		var1.append("                              + (CASE WHEN 1 AND ( cast((CASE WHEN strftime('%w', ?) = '0' ");
		var1.append("                                      THEN 7 ELSE strftime('%w', ?)END ) AS integer)  ");
		var1.append("                              < cast((CASE WHEN strftime('%w', start_date) = '0'  ");
		var1.append("                                           THEN 7 ELSE strftime('%w', start_date) END) AS integer) )  ");
		var1.append("                                           THEN 1 ELSE 0 END)) % 4 + 1)=1 AND week1=1) ");
		var1.append("                              OR (((cast((julianday(Date(?)) - julianday(start_date)) / 7 AS integer)  ");
		var1.append("                              + (CASE WHEN 1 AND ( cast((CASE WHEN strftime('%w', ?) = '0' ");
		var1.append("                                      THEN 7 ELSE strftime('%w', ?)END ) AS integer)  ");
		var1.append("                              < cast((CASE WHEN strftime('%w', start_date) = '0'  ");
		var1.append("                                           THEN 7 ELSE strftime('%w', start_date) END) AS integer) )  ");
		var1.append("                                           THEN 1 ELSE 0 END)) % 4 + 1)=2 AND week2=1) ");
		var1.append("                              OR (((cast((julianday(?) - julianday(start_date)) / 7 AS integer)  ");
		var1.append("                              +(CASE WHEN 1 AND ( cast((CASE WHEN strftime('%w', ?) = '0'  ");
		var1.append("                                     THEN 7 ELSE strftime('%w', ?)END ) AS integer)  ");
		var1.append("                              < cast((CASE WHEN strftime('%w', start_date) = '0'  ");
		var1.append("                                           THEN 7 ELSE strftime('%w', start_date) END) AS integer) )  ");
		var1.append("                                           THEN 1 ELSE 0 END)) % 4 + 1)=3 AND week3=1) ");
		var1.append("                              OR (((cast((julianday(?) - julianday(start_date)) / 7 AS integer)  ");
		var1.append("                              +(CASE WHEN 1 AND ( cast((CASE WHEN strftime('%w', ?) = '0'  ");
		var1.append("                                     THEN 7 ELSE strftime('%w', ?)END ) AS integer)  ");
		var1.append("                              < cast((CASE WHEN strftime('%w', start_date) = '0'  ");
		var1.append("                                           THEN 7 ELSE strftime('%w', start_date) END) AS integer) )  ");
		var1.append("                                           THEN 1 ELSE 0 END)) % 4 + 1)=4 AND week4=1) )  ");

		var1.append( "ORDER BY SEQ");
		var1.append( day + " ASC) RTC ");
		var1.append("                  WHERE  VP.routing_id = RT.routing_id ");
		var1.append("                         AND RTC.routing_id = RT.routing_id ");
		var1.append("                         AND RTC.customer_id = CT.customer_id ");
		var1.append("                         AND Date(VP.from_date) <= Date(?) ");
		var1.append("                         AND ( VP.to_date IS NULL ");
		var1.append("                                OR Date(VP.to_date) >= Date(?) ) ");
		var1.append("                         AND Date(RTC.start_date) <= Date('now', 'localtime') ");
		var1.append("                         AND ( RTC.end_date IS NULL ");
		var1.append("                                OR Date(RTC.end_date) >= Date('now', 'localtime') ) ");
		var1.append("                         AND VP.status = 1 ");
		var1.append("                         AND RT.status = 1 ");
		var1.append("                         AND RTC.status = 1 ");
		var1.append("                         AND RTC.").append(today)
				.append(" = 1 ");
		var1.append("                         AND CT.status = 1 ");
		var1.append("                  GROUP  BY VP.staff_id ORDER BY VP.STAFF_ID) PLAN ");
		var1.append("              ON PLAN.nvbh_staff_id_1 = NVBH_TPD.nvbh_staff_id ");
		var1.append("       LEFT JOIN (SELECT AL_STAFF.staff_id                         AS ");
		var1.append("                         NVBH_STAFF_IDS, ");
		var1.append("                         Group_concat(AL_STAFF.customer_id)        AS ");
		var1.append("              NVBH_VISITED_ORDER, ");
		var1.append("                         Group_concat(AL_STAFF.lat)                AS ");
		var1.append("                         NVBH_VISITED_LAT, ");
		var1.append("                         Group_concat(AL_STAFF.lng)                AS ");
		var1.append("                         NVBH_VISITED_LNG, ");
		var1.append("                         Group_concat(AL_STAFF.start_time)         AS ");
		var1.append("              NVBH_VISITED_START_TIME, ");
		var1.append("                         GROUP_CONCAT(AL_STAFF.IS_OR)              AS ");
		var1.append("              NVBH_VISITED_IS_OR, ");
		var1.append("                         Group_concat(AL_STAFF.end_time)           AS ");
		var1.append("              NVBH_VISITED_END_TIME, ");
		var1.append("                         Group_concat(AL_STAFF.customer_code_name) AS ");
		var1.append("                        NVBH_VISITED_CUSTOMER_CODE_NAME, ");
		var1.append("                         Group_concat(ifnull(AL_STAFF.customer_address,''),'seperator')   AS ");
		var1.append("                        NVBH_VISITED_CUSTOMER_ADDRESS ");
		var1.append("                  FROM   (SELECT action_log.*, ");
		var1.append("                                 ( Substr(customer.customer_code, 1, 3) ");
		var1.append("                                   || ' - ' ");
		var1.append("                                   || customer.customer_name ) AS ");
		var1.append("                                 CUSTOMER_CODE_NAME, ");
		var1.append("                                 ( CASE ");
		var1.append("                                     WHEN customer.address IS NOT NULL THEN ");
		var1.append("                                     customer.address ");
		var1.append("                                     ELSE ( ifnull(customer.housenumber, '') ");
		var1.append("                                            || ' ' ");
		var1.append("                                            || ifnull(customer.street,'')) ");
		var1.append("                                   END )                       AS ");
		var1.append("                                 CUSTOMER_ADDRESS, ");
		var1.append("                                 Min(start_time)               AS VISITED_TIME ");
		var1.append("                          FROM   action_log, ");
		var1.append("                                 customer ");
		var1.append("                          WHERE  Date(start_time) = Date(?) ");
		var1.append("                                 AND action_log.customer_id = ");
		var1.append("                                     customer.customer_id ");
		var1.append("                                 AND object_type IN ( 0, 1 ) ");
		var1.append("                                 AND is_or = 0 ");
		var1.append("                          GROUP  BY action_log.staff_id, ");
		var1.append("                                    action_log.customer_id ");
		var1.append("                          ORDER  BY start_time ASC) AL_STAFF, ");
		var1.append("                         staff, ");
		var1.append("                         channel_type CH ");
		var1.append("                  WHERE  AL_STAFF.staff_id = staff.staff_id ");
		var1.append("                         AND staff.staff_type_id = CH.channel_type_id ");
		var1.append("                         AND CH.object_type IN ( 1, 2 ) ");
		var1.append("                         AND CH.type = 2 ");
		var1.append("                  GROUP  BY AL_STAFF.staff_id) NVBH_AL ");
		var1.append("              ON NVBH_AL.nvbh_staff_ids = NVBH_TPD.nvbh_staff_id ");
		var1.append("ORDER  BY npp_shop_code ASC, ");
		var1.append("          npp_shop_name ASC, ");
		var1.append("          npp_staff_name ASC, ");
		var1.append("          npp_staff_code ");

		String param[] = { date_now, date_now, date_now, date_now, date_now,
				date_now, date_now, date_now, date_now, date_now, date_now,
				date_now, date_now, date_now, date_now, date_now, date_now,
				date_now, date_now };
		Cursor c = rawQuery(var1.toString(), param);
		if (c != null) {
			try {
				dto = new TBHVRouteSupervisionDTO();
				if (c.moveToFirst()) {
					do {
						TBHVRouteSupervisionDTO.THBVRouteSupervisionItem item = dto
								.newTHBVRouteSupervisionItem();
						item.initDataFromCursor(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			} finally {
				if (c != null) {
					c.close();
				}
			}
		}

		return dto;
	}

	/**
	 * Lay ds vi tri NVBH cua NVGS
	 *
	 * @author: TruongHN
	 * @param staffId
	 * @return: SaleRoadMapSupervisorDTO
	 * @throws Exception
	 * @throws:
	 */
	public GsnppRouteSupervisionDTO getListPositionSalePersons(int staffId,
			String shopId) throws Exception {
		GsnppRouteSupervisionDTO dto = null;

		String listStaffId = getListStaffOfSupervisor(staffId + "", shopId);
		String dateNow = DateUtils.now();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT staff.staff_id                           AS STAFF_ID, ");
		var1.append("       staff.staff_code                         AS STAFF_CODE, ");
		var1.append("       staff.staff_name                         AS STAFF_NAME, ");
		var1.append("       staff.shop_id                            AS SHOP_ID, ");
		var1.append("       staff.mobilephone                        AS MOBILEPHONE, ");
		var1.append("       staff.phone                              AS PHONE, ");
		var1.append("       staffPosLog.lat                          AS LAT, ");
		var1.append("       staffPosLog.lng                          AS LNG, ");
		var1.append("       ACTIONLOGSTAFF.customer_id               AS CUSTOMER_ID, ");
		var1.append("       ACTIONLOGSTAFF.cus_name                  AS CUS_NAME, ");
		var1.append("       ACTIONLOGSTAFF.start_time                AS START_TIME, ");
		var1.append("       ACTIONLOGSTAFF.end_time                  AS END_TIME, ");
		var1.append("       Strftime('%H:%M', staffPosLog.date_time) AS DATE_TIME ");
		var1.append("FROM   (SELECT * ");
		var1.append("        FROM   staff s ");
		var1.append("        , staff_type st ");
		var1.append("        WHERE  s.staff_id in ( " + listStaffId + " ) ");
		var1.append("               AND s.status = 1 ");
		var1.append("               AND st.status = 1 ");
		var1.append("               AND st.staff_type_id = s.staff_type_id ");
//		var1.append("               AND st.specific_type = ? ");
		var1.append("        ORDER  BY staff_name) staff ");
		var1.append("       LEFT JOIN (SELECT staff_position_log_id, ");
		var1.append("                         staff_id, ");
		var1.append("                         Max(create_date) AS DATE_TIME, ");
		var1.append("                         lat, ");
		var1.append("                         lng ");
		var1.append("                  FROM   staff_position_log ");
		var1.append("                  WHERE  substr(create_date, 1, 10) = substr(?, 1, 10) ");
		var1.append("                  GROUP  BY staff_id) staffPosLog ");
		var1.append("              ON staff.staff_id = staffPosLog.staff_id ");
		var1.append("       LEFT JOIN (SELECT staff.staff_id                AS STAFF_ID_1, ");
		var1.append("                         customer.customer_id, ");
//		var1.append("                         ( Substr(customer.customer_code, 1, 3) ");
//		var1.append("                           || '-' ");
//		var1.append("                           || customer.customer_name ) AS CUS_NAME, ");
		var1.append("                         (customer.short_code || '-' || customer.customer_name) AS CUS_NAME, ");
		var1.append("                         Max(action_log.start_time)    AS START_TIME, ");
		var1.append("                         action_log.end_time           AS END_TIME ");
		var1.append("                  FROM   action_log, ");
		var1.append("                         staff, ");
		var1.append("                         customer ");
		var1.append("                  WHERE  action_log.staff_id = staff.staff_id ");
		var1.append("                         AND substr(action_log.start_time, 1, 10) = ");
		var1.append("                             substr(?, 1, 10) ");
		var1.append("                         AND staff.status = 1 ");
		var1.append("                         AND action_log.object_type IN ( 0, 1 ) ");
		var1.append("                         AND customer.customer_id = action_log.customer_id ");
		var1.append("                  GROUP  BY action_log.staff_id) ACTIONLOGSTAFF ");
		var1.append("              ON staff.staff_id = ACTIONLOGSTAFF.staff_id_1 ");

		String[] params = { dateNow, dateNow };
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				dto = new GsnppRouteSupervisionDTO();
				if (c.moveToFirst()) {
					do {
						GsnppRouteSupervisionItem item = new GsnppRouteSupervisionItem();
						item.initListPostSalePersons(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.i("getListPositionSalePersons", ex.getMessage());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * HieuNH lay danh sach NVBH thuoc quyen quan ly cua NVGS trong chuc nang
	 * danh sach khach hang chua PSDS
	 *
	 * @param shopId
	 * @param staffOwnerId
	 *            : id NVGS
	 * @return
	 * @throws Exception
	 */
	public ListStaffDTO getListNVBHNotPSDS(int shopId, String staffOwnerId) throws Exception {
		ListStaffDTO dto = new ListStaffDTO();
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("SELECT DISTINCT tb1.nvbh     STAFF_NAME, ");
		sqlQuery.append("                tb1.staff_id STAFF_ID, ");
		sqlQuery.append("                tb1.manv     STAFF_CODE ");
		sqlQuery.append("FROM   (SELECT s.staff_id, ");
		sqlQuery.append("               s.staff_code MaNV, ");
		sqlQuery.append("               s.staff_name NVBH, ");
		sqlQuery.append("               a.customer_id ");
		sqlQuery.append("        FROM   (SELECT * ");
		sqlQuery.append("                FROM   routing_customer ");
		sqlQuery.append("                WHERE  ");
		sqlQuery.append(" ( julianday( dayInOrder('now', 'localtime', 'weekday 0', '-6 fullDate')) - julianday( dayInOrder(START_DATE, 'weekday 0', '-6 fullDate'))) >= 0");
		sqlQuery.append("                       AND status = 1) a, ");
		sqlQuery.append("               visit_plan b, ");
		sqlQuery.append("               routing c, ");
		sqlQuery.append("               staff s, ");
		sqlQuery.append("               shop sh, ");
		sqlQuery.append("               customer cu ");
		sqlQuery.append("        WHERE  1 = 1 ");
		sqlQuery.append("               AND b.routing_id = a.routing_id ");
		sqlQuery.append("               AND c.routing_id = a.routing_id ");
		sqlQuery.append("               AND b.staff_id = s.staff_id ");
		sqlQuery.append("               AND a.customer_id = cu.customer_id ");
		sqlQuery.append("               AND s.shop_id = sh.shop_id ");
		sqlQuery.append("               AND sh.status = 1 ");
		sqlQuery.append("               AND a.status = 1 ");
		sqlQuery.append("               AND b.status = 1 ");
		sqlQuery.append("               AND s.status = 1 ");
		sqlQuery.append("               AND c.status = 1 ");
		sqlQuery.append("               AND cu.status = 1 ");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			sqlQuery.append("               AND cu.shop_id = ? ");
			params.add("" + shopId);
		}
		// thanhnn add kiem tra phai la nvbh hay ko
		sqlQuery.append("               AND s.staff_type_id in (select channel_type_id from channel_type where status = 1 and type = 2 and object_type in(2,1)) ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) >= Date(b.from_date) ");
		sqlQuery.append("                     AND ( b.to_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) <= ");
		sqlQuery.append("                               Date(b.to_date) ) ) ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) >= Date(a.start_date) ");
		sqlQuery.append("                     AND ( a.end_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) <= ");
		sqlQuery.append("                               Date(a.end_date) ) ) ");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			sqlQuery.append(" AND s.shop_id = ?");
			params.add("" + shopId);
		}

		if (!StringUtil.isNullOrEmpty("" + staffOwnerId)) {
			String listStaff = getListStaffOfSupervisor(staffOwnerId, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			sqlQuery.append(" AND s.staff_id in ( ");
			sqlQuery.append(listStaff);
			sqlQuery.append(" )");
		}
		sqlQuery.append("   ) tb1    LEFT JOIN staff_customer tb2 ");
		sqlQuery.append("              ON ( tb1.staff_id = tb2.staff_id ");
		sqlQuery.append("                   AND tb1.customer_id = tb2.customer_id AND tb2.shop_id = ?) ");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			params.add("" + shopId);
		}
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND ( Date(tb2.last_approve_order) IS NULL ");
		sqlQuery.append("              OR tb2.last_approve_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_approve_order) < (SELECT ");
		sqlQuery.append("                 Date('now', 'localtime','start of month' ");
		sqlQuery.append("                 )) ) ");
		sqlQuery.append("       AND ( tb2.last_order IS NULL ");
		sqlQuery.append("              OR tb2.last_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_order) < (SELECT Date('now', 'localtime')) ) ");
		sqlQuery.append("ORDER  BY staff_name ");

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				if (c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.parrseStaffFromCursor(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			// c = null;
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * HieuNH lay danh sach NVBH thuoc quyen quan ly cua NVGS trong chuc nang
	 * danh sach khach hang chua PSDS
	 *
	 * @param shopId
	 * @param staffOwnerId
	 *            : id NVGS
	 * @return
	 * @throws Exception
	 */
	public ListStaffDTO getListNVBH(int shopId, String staffOwnerId) throws Exception {
		ListStaffDTO dto = new ListStaffDTO();
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("SELECT DISTINCT tb1.nvbh     STAFF_NAME, ");
		sqlQuery.append("                tb1.staff_id STAFF_ID, ");
		sqlQuery.append("                tb1.manv     STAFF_CODE ");
		sqlQuery.append("FROM   (SELECT s.staff_id, ");
		sqlQuery.append("               s.staff_code MaNV, ");
		sqlQuery.append("               s.staff_name NVBH, ");
		sqlQuery.append("               a.customer_id ");
		sqlQuery.append("        FROM   (SELECT * ");
		sqlQuery.append("                FROM   routing_customer ");
		sqlQuery.append("                WHERE  ");

		sqlQuery.append(" ( julianday( dayInOrder('now', 'localtime', 'weekday 0', '-6 fullDate')) - julianday( dayInOrder(START_DATE, 'weekday 0', '-6 fullDate'))) >= 0 ");
		sqlQuery.append("                       AND status = 1) a, ");
		sqlQuery.append("               visit_plan b, ");
		sqlQuery.append("               routing c, ");
		sqlQuery.append("               staff s, ");
		sqlQuery.append("               shop sh, ");
		sqlQuery.append("               customer cu ");
		sqlQuery.append("        WHERE  1 = 1 ");
		sqlQuery.append("               AND b.routing_id = a.routing_id ");
		sqlQuery.append("               AND c.routing_id = a.routing_id ");
		sqlQuery.append("               AND b.staff_id = s.staff_id ");
		sqlQuery.append("               AND a.customer_id = cu.customer_id ");
		sqlQuery.append("               AND s.shop_id = sh.shop_id ");
		sqlQuery.append("               AND sh.status = 1 ");
		sqlQuery.append("               AND a.status = 1 ");
		sqlQuery.append("               AND s.status = 1 ");
		sqlQuery.append("               AND c.status = 1 ");
		sqlQuery.append("               AND cu.status = 1 ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) > Date(b.from_date) ");
		sqlQuery.append("                     AND ( b.to_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) < ");
		sqlQuery.append("                               Date(b.to_date) ) ) ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) > Date(a.start_date) ");
		sqlQuery.append("                     AND ( a.end_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) < ");
		sqlQuery.append("                               Date(a.end_date) ) ) ");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			sqlQuery.append(" AND s.shop_id = ?");
			params.add("" + shopId);
		}

		if (!StringUtil.isNullOrEmpty("" + staffOwnerId)) {
			sqlQuery.append(" AND s.staff_id IN ( " + getListStaffOfSupervisor(String.valueOf(staffOwnerId), shopId + "") +" ) ");
		}
		sqlQuery.append("   ) tb1    LEFT JOIN staff_customer tb2 ");
		sqlQuery.append("              ON ( tb1.staff_id = tb2.staff_id ");
		sqlQuery.append("                   AND tb1.customer_id = tb2.customer_id AND tb2.shop_id = ?) ");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			params.add("" + shopId);
		}
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND ( Date(tb2.last_approve_order) IS NULL ");
		sqlQuery.append("              OR tb2.last_approve_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_approve_order) < (SELECT ");
		sqlQuery.append("                 Date('now', 'localtime','start of month' ");
		sqlQuery.append("                 )) ) ");
		sqlQuery.append("       AND ( tb2.last_order IS NULL ");
		sqlQuery.append("              OR tb2.last_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_order) < (SELECT Date('now', 'localtime')) ) ");
		sqlQuery.append("ORDER  BY staff_name ");

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				if (c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.parrseStaffFromCursor(c);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * Lay ds Kh cua 1 GSNPP
	 *
	 * @param staffIdGS
	 * @param stockCode
	 * @param customerName
	 * @param page
	 * @param isGetNumRow
	 * @return
	 */
	public TBHVCustomerListDTO getCustomerListForPostFeedback(Bundle data) throws Exception {
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String staffOwnerId = "";
		if (data.containsKey(IntentConstants.INTENT_STAFF_OWNER_ID)) {
			staffOwnerId = data
					.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		}
		String staff = "";
		if (data.containsKey(IntentConstants.INTENT_STAFF_ID)) {
			staff = data.getString(IntentConstants.INTENT_STAFF_ID);
		}
		String shopId = "";
		if (data.containsKey(IntentConstants.INTENT_SHOP_ID)) {
			shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		}
		String shopIdOwner = "";
		if (data.containsKey(IntentConstants.INTENT_SHOP_ID_OWNER)) {
			shopIdOwner = data.getString(IntentConstants.INTENT_SHOP_ID_OWNER);
		}
		String customerCode = data
				.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String customerName = data
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		boolean isGetNumRow = data
				.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();

		var1.append("SELECT distinct ct.customer_id                 AS CUSTOMER_ID, ");
		var1.append("       ct.short_code AS CUSTOMER_CODE, ");
		var1.append("       ct.SHOP_ID AS SHOP_ID, ");
		var1.append("       ct.customer_name               AS CUSTOMER_NAME, ");
		var1.append("       CT.NAME_TEXT  AS  CUSTOMER_NAME_ADDRESS_TEXT, ");
		var1.append("               ( (CASE WHEN CT.HOUSENUMBER IS NOT NULL THEN CT.HOUSENUMBER ELSE '' END) ");
		var1.append("                 || (CASE WHEN CT.HOUSENUMBER IS NOT NULL THEN ' ' ELSE '' END) ");
		var1.append("                 || (CASE WHEN CT.STREET IS NOT NULL THEN CT.STREET ELSE '' END) )        ADDRESS_ ");
		var1.append("FROM   visit_plan vp, ");
		var1.append("       routing rt, ");
		var1.append("       routing_customer rtc, ");
		var1.append("       customer ct, ");
		var1.append("       staff s ");
		var1.append("WHERE  vp.routing_id = rt.routing_id ");
		var1.append("       AND s.staff_id = vp.staff_id ");
		var1.append("       AND rtc.routing_id = rt.routing_id ");
		if (!StringUtil.isNullOrEmpty(shopId)) {
//			var1.append("       AND s.shop_id = ? ");
//			param.add(shopId);
//			totalParam.add(shopId);
			var1.append("       AND vp.shop_id = ? ");
			param.add(shopId);
			totalParam.add(shopId);
		}

		if (!StringUtil.isNullOrEmpty(shopIdOwner)) {
			SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
			ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopIdOwner);
			String shopStr = TextUtils.join(",", shopList);
			var1.append("       AND vp.shop_id IN ( " + shopStr + ") ");
			var1.append("       AND s.shop_id IN ( " + shopStr + ") ");
		}
		var1.append("       AND rtc.customer_id = ct.customer_id ");
		var1.append("       AND Date(vp.from_date) <= Date('now') ");
		var1.append("       AND ( Date(vp.to_date) >= Date('now') ");
		var1.append("              OR Date(vp.to_date) IS NULL ) ");
		var1.append("       AND Date(rtc.start_date) <= Date('now') ");
		var1.append("       AND ( Date(rtc.end_date) >= Date('now') ");
		var1.append("              OR Date(rtc.end_date) IS NULL ) ");
		if (!StringUtil.isNullOrEmpty(staff)) {
			var1.append("       AND vp.staff_id = ? ");
			param.add(staff);
			totalParam.add(staff);
		}
		if (!StringUtil.isNullOrEmpty(staffOwnerId)) {
			var1.append("	AND s.STAFF_ID IN (SELECT PS.STAFF_ID	FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			var1.append("	    					AND PS.PARENT_STAFF_ID = ? AND PS.STATUS = 1 ");
			var1.append("	    					AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			param.add(staffOwnerId);
			totalParam.add(staffOwnerId);
		}
		var1.append("       AND vp.status = 1 ");
		var1.append("       AND rt.status = 1 ");
		var1.append("       AND rtc.status = 1 ");
		var1.append("       AND ct.status = 1 ");

		if (!StringUtil.isNullOrEmpty(customerCode)) {
			customerCode = StringUtil.escapeSqlString(customerCode);
			var1.append("	and upper(SHORT_CODE) like upper(?) escape '^' ");
			param.add("%" + customerCode + "%");
			totalParam.add("%" + customerCode + "%");
		}
		if (!StringUtil.isNullOrEmpty(customerName)) {
			customerName = StringUtil
					.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			customerName = DatabaseUtils.sqlEscapeString("%" + customerName
					+ "%");
			var1.append("	and upper(CUSTOMER_NAME_ADDRESS_TEXT) like upper(");
			var1.append(customerName);
			var1.append(") escape '^' ");
		}

		var1.append("	ORDER  BY CT.short_code");
		if (page > 0) {
			// get count
			if (isGetNumRow) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ var1 + ")");
			}
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor c = null;
		Cursor cTotalRow = null;
		TBHVCustomerListDTO dto = new TBHVCustomerListDTO();
		List<CustomerDTO> listCustomer = new ArrayList<CustomerDTO>();
		try {
			// SQLUtils sqlUtil = SQLUtils.getInstance();

			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						CustomerDTO item = new CustomerDTO();
						item.parseCustomerInfo(c);

						listCustomer.add(item);
					} while (c.moveToNext());
				}
				dto.listCustomer = listCustomer;
			}
			if (page > 0 && isGetNumRow) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 * lay vi tri giam sat
	 *
	 * @author: TamPQ
	 * @param staffID
	 * @param staffIdGS
	 * @param today
	 * @return
	 * @return: TBHVRouteSupervisionDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public TBHVRouteSupervisionDTO getGsnppPosition(String shopId, String staffID) throws Exception {
		TBHVRouteSupervisionDTO dto = null;
		String date_now = DateUtils.now();

		String listGs = getListStaffManagerOfChildShop(staffID,shopId);
		ArrayList<String> listShopID = getListShopChild(shopId);
		String res = TextUtils.join(",", listShopID);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT STAFF.*, ");
		var1.append("       STAFFPOSLOG.LAT                         AS LAT, ");
		var1.append("       STAFFPOSLOG.LNG                         AS LNG, ");
		var1.append("       STRFTIME('%H:%M', STAFFPOSLOG.DATETIME) AS DATE_TIME ");
		var1.append("FROM   (");
		var1.append("SELECT GS.STAFF_ID          AS STAFF_ID, ");
		var1.append("       GS.STAFF_CODE        AS STAFF_CODE, ");
		var1.append("       GS.MOBILEPHONE       AS MOBILEPHONE, ");
		var1.append("       GS.PHONE        	 AS PHONE, ");
		var1.append("       GS.STAFF_NAME        AS STAFF_NAME, ");
		var1.append("       SH.SHOP_ID			 AS SHOP_ID, ");
		var1.append("       SH.SHOP_CODE		 AS SHOP_CODE ");
		var1.append("FROM   ");
		var1.append("       ORG_TEMP OT, ");
		var1.append("       STAFF_TYPE ST, ");
		var1.append("       SHOP SH, ");
		var1.append("       STAFF GS ");
		var1.append(" where 1=1 ");
		var1.append("       AND OT.SHOP_ID = SH.SHOP_ID ");
		var1.append("       AND OT.STAFF_ID = GS.STAFF_ID ");
		var1.append("       AND GS.STATUS = 1 ");
		var1.append("       AND SH.STATUS = 1 ");
		var1.append("       AND ST.STATUS = 1 ");
		var1.append("       AND SH.SHOP_ID IN ( ");
		var1.append(res);
		var1.append("               ) ");
//		var1.append("       AND ST.SPECIFIC_TYPE IN (?, ?, ?) ");
//		params.add(String.valueOf(UserDTO.TYPE_STAFF));
//		params.add(String.valueOf(UserDTO.TYPE_SUPERVISOR));
//		params.add(String.valueOf(UserDTO.TYPE_MANAGER));
		var1.append("       AND ST.STAFF_TYPE_ID = GS.STAFF_TYPE_ID ");
		var1.append("       AND GS.STAFF_ID IN( ");
		var1.append(listGs);
		var1.append("       )");
		var1.append("GROUP  BY  ");
		var1.append("          GS.STAFF_ID, ");
		var1.append("          SH.SHOP_ID ");
		var1.append("ORDER  BY SH.SHOP_CODE, ");
		var1.append("          GS.STAFF_NAME ");
		var1.append(") STAFF ");
		// lay vi tri cuoi cung cua gs.
		var1.append("       LEFT JOIN (SELECT STAFF_POSITION_LOG_ID, ");
		var1.append("                         STAFF_ID, ");
		var1.append("                         MAX(CREATE_DATE) AS DATETIME, ");
		var1.append("                         LAT, ");
		var1.append("                         LNG ");
		var1.append("                  FROM   STAFF_POSITION_LOG ");
		var1.append("                  WHERE  substr(CREATE_DATE, 1, 10) >= substr(?, 1, 10) ");
		params.add(date_now);
		var1.append("                  GROUP  BY STAFF_ID) STAFFPOSLOG ");
		var1.append("              ON STAFF.STAFF_ID = STAFFPOSLOG.STAFF_ID ");
		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				dto = new TBHVRouteSupervisionDTO();
				if (c.moveToFirst()) {
					do {
						TBHVRouteSupervisionDTO.THBVRouteSupervisionItem item = dto
								.newTHBVRouteSupervisionItem();
						item.initItem(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.e("getGsnppPosition", "fail", ex);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param staffId
	 * @return
	 * @return: SaleRoadMapSupervisorDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public GsnppRouteSupervisionDTO getNvbhPosition(String staffId,
			String shopId) throws Exception {
		GsnppRouteSupervisionDTO dto = new GsnppRouteSupervisionDTO();
		String date_now = DateUtils.now();
		ArrayList<String> param = new ArrayList<String>();
//		String listStaff = PriUtils.getInstance().getListStaffOfSupervisor(staffId, shopId);
		String listStaff = getListStaffOfSupervisor(staffId, shopId);

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT STAFF.staff_id                           AS STAFF_ID, ");
		var1.append("               STAFF.staff_code                         AS STAFF_CODE, ");
		var1.append("               STAFF.staff_name                         AS STAFF_NAME, ");
		var1.append("               STAFF.shop_id                            AS SHOP_ID, ");
		var1.append("               STAFF.mobilephone                        AS MOBILEPHONE, ");
		var1.append("               STAFF.phone                              AS PHONE, ");
		var1.append("               STAFFPOSLOG.lat                          AS LAT, ");
		var1.append("               STAFFPOSLOG.lng                          AS LNG, ");
		var1.append("               Strftime('%H:%M', STAFFPOSLOG.date_time) AS DATE_TIME ");
		var1.append("        FROM   (SELECT * ");
		var1.append("                FROM   staff s ");
		var1.append("                		, staff_type st ");
		var1.append("                WHERE  staff_id IN ( ");
		var1.append(listStaff);
		var1.append("                ) ");
		var1.append("                       AND s.status = 1 ");
		var1.append("                       AND s.shop_id = ? ");
		param.add(shopId);
		var1.append("                       AND s.staff_type_id = st.staff_type_id ");
		var1.append("                       AND st.specific_type = ? ");
		param.add(String.valueOf(UserDTO.TYPE_STAFF));
		var1.append("                       AND st.status = 1 ");
		var1.append("                ORDER  BY staff_name) STAFF ");
		var1.append("               LEFT JOIN (SELECT staff_position_log_id, ");
		var1.append("                                 staff_id, ");
		var1.append("                                 Max(create_date) AS DATE_TIME, ");
		var1.append("                                 lat, ");
		var1.append("                                 lng ");
		var1.append("                          FROM   staff_position_log ");
		var1.append("                          WHERE  substr(create_date, 1, 10) >= substr(? ");
		param.add(date_now);
		var1.append("                                                     , 1, 10) ");
		var1.append("                          GROUP  BY staff_id) STAFFPOSLOG ");
		var1.append("                      ON STAFF.staff_id = STAFFPOSLOG.staff_id) POSSTAFF ");
		var1.append("       LEFT JOIN (SELECT staff.staff_id                AS STAFF_ID_1, ");
		var1.append("                         customer.customer_id, ");
		var1.append("                         ( customer.short_code ");
		var1.append("                           || '-' ");
		var1.append("                           || customer.customer_name ) AS CUS_NAME, ");
		var1.append("                         Max(action_log.start_time)    AS START_TIME, ");
		var1.append("                         action_log.end_time           AS END_TIME ");
		var1.append("                  FROM   action_log, ");
		var1.append("                         staff, ");
//		var1.append("                         channel_type ct, ");
		var1.append("                         customer ");
		var1.append("                  WHERE  action_log.staff_id = staff.staff_id ");
		var1.append("                         AND substr(action_log.start_time, 1, 10) = substr( ");
		var1.append("                             ?, 1, 10) ");
		param.add(date_now);
		var1.append("                         AND staff.status = 1 ");
		var1.append("                       AND customer.shop_id = ? ");
		param.add(shopId);
		// kiem tra loai nhan vien la presales || valsales
//		var1.append("                         AND staff.staff_type_id = ct.channel_type_id ");
//		var1.append("                         AND ct.object_type IN ( 1, 2 ) ");
//		var1.append("                         AND ct.type = 2 ");

		var1.append("                         AND action_log.object_type IN ( 0, 1 ) ");
		var1.append("                         AND customer.customer_id = action_log.customer_id ");
		var1.append("                  GROUP  BY action_log.staff_id) ACTIONLOGSTAFF ");
		var1.append("              ON POSSTAFF.staff_id = ACTIONLOGSTAFF.staff_id_1 ");
//		var1.append("	left join (select count(staff_id) from staff where status = 1 and) ");
		var1.append("       LEFT JOIN (SELECT TPD.staff_id AS STAFF_ID_2, ");
		var1.append("                         TPD.TRAINING_DATE ");
		var1.append("                  FROM   training_plan TP, ");
		var1.append("                         training_plan_detail TPD ");
		var1.append("                  WHERE  TP.staff_id = ? ");
		param.add("" + staffId);
		var1.append("                         AND TP.training_plan_id = TPD.training_plan_id ");
		var1.append("                         AND TP.status = 1 ");
		var1.append("                         AND TPD.status IN ( 0, 1 ) ");
		var1.append("                         AND substr(training_date, 1, 10) = substr(?, 1, 10)) ");
		param.add(date_now);
		var1.append("                 TPD ");
		var1.append("              ON POSSTAFF.staff_id = TPD.staff_id_2 ");

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						GsnppRouteSupervisionItem item = new GsnppRouteSupervisionItem();
						item.initListPostSalePersons(c);
						item.haveStaffChild = !StringUtil.isNullOrEmpty(getListStaffOfSupervisor(String.valueOf(item.aStaff.staffId), shopId));
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.e("getNvbhPosition", "fail", ex);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPositionSalePersons", e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * K tra da co log ghe tham trong ngay hay chua cua 1 KH
	 *
	 * @author: TamPQ
	 * @param actionLog
	 * @return
	 * @return: booleanvoid
	 * @throws:
	 */
	public boolean alreadyHaveVisitLog(ActionLogDTO actionLog) {
		boolean alreadyHaveVisitLog = false;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   ACTION_LOG ");
		var1.append("WHERE  STAFF_ID = ? ");
		var1.append("       AND CUSTOMER_ID = ? ");
		var1.append("       AND SHOP_ID = ? ");
		var1.append("       AND OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("       AND DATE(START_TIME) = DATE(?) ");

		String[] params = { String.valueOf(actionLog.staffId),
				"" + actionLog.aCustomer.customerId, "" + actionLog.shopId, date_now };
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				if(c.moveToNext()) {
					alreadyHaveVisitLog = true;
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return alreadyHaveVisitLog;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param list
	 * @return
	 * @return: ArrayList<StaffItem>void
	 * @throws:
	 */
	public ListStaffDTO getPositionOfGsnppAndNvbh(String[] list) {
		ListStaffDTO staffList = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		var1.append("SELECT STAFF_ID, LAT, LNG, ");
		var1.append("       MAX(CREATE_DATE)  ");
		var1.append("FROM   STAFF_POSITION_LOG ");
		if (list != null && list.length == 2) {
			var1.append("WHERE  (STAFF_ID = ? ");
			var1.append("        OR STAFF_ID = ?) ");
			params.add(list[0]);
			params.add(list[1]);
		} else if (list != null && list.length == 1) {
			var1.append("WHERE  STAFF_ID = ? ");
			params.add(list[0]);
		}
		var1.append("		AND DATE(CREATE_DATE) = DATE(?) ");
		params.add(date_now);
		var1.append("GROUP  BY STAFF_ID ");

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				staffList = new ListStaffDTO();
				if (c.moveToFirst()) {
					do {
						staffList.addItem(c);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.e("getPositionOfGsnppAndNvbh", "fail", ex);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return staffList;
	}

	/**
	 * Lay ds canh bao ghe tham di tuyen
	 *
	 * @author: TamPQ
	 * @param staffIdGS
	 * @return
	 * @return: TBHVVisitCustomerNotificationDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public TBHVVisitCustomerNotificationDTO getVisitCusNotification(
			String shopId) throws Exception {
		String day = DateUtils.getToday();
		TBHVVisitCustomerNotificationDTO dto = new TBHVVisitCustomerNotificationDTO();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		Cursor c = null;
		String staffIdASM = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";
		String listGs = SQLUtils.getInstance().getListStaffChild(staffIdASM,shopId);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopId);
		String shopStr = TextUtils.join(",", shopList);

		StringBuffer shopParamSql = new StringBuffer();
		shopParamSql
				.append("SELECT * FROM SHOP_PARAM WHERE SHOP_ID = ? AND TYPE LIKE '%DT%' AND STATUS = 1");
		Cursor cScore = null;
		try {
			cScore = rawQuery(shopParamSql.toString(),
					new String[] { String.valueOf(shopId) });
			if (cScore != null) {
				if (cScore.moveToFirst()) {
					do {
						if (dto != null) {
							ShopParamDTO item = new ShopParamDTO();
							item.initObjectWithCursor(cScore);
							dto.listParam.put(item.type, item);
						}
					} while (cScore.moveToNext());
				}
			}
		} finally {
			try {
				if (cScore != null) {
					cScore.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		String DT_START = "";
		String DT_MIDDLE = "";
		String DT_END = "";
		if (dto.listParam.size() > 0) {
			DT_START = dto.listParam.get("DT_START").value;
			DT_MIDDLE = dto.listParam.get("DT_MIDDLE").value;
			DT_END = dto.listParam.get("DT_END").value;
		}

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT GS.STAFF_ID          AS GS_STAFF_ID, ");
		var1.append("               GS.STAFF_CODE        AS GS_STAFF_CODE, ");
		var1.append("               GS.STAFF_NAME        AS GS_STAFF_NAME, ");
		var1.append("               SH.SHOP_ID           AS NVBH_SHOP_ID, ");
		var1.append("               SH.SHOP_CODE         AS NVBH_SHOP_CODE, ");
		var1.append("               COUNT(NVBH.STAFF_ID) AS NUM_NVBH ");
		var1.append("        FROM   STAFF NVBH, ");
		var1.append("               CHANNEL_TYPE CH, ");
		var1.append("               SHOP SH, ");
		var1.append("               STAFF GS, ");
		var1.append("               CHANNEL_TYPE GS_CH ");
		var1.append("        WHERE  NVBH.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID ");
		var1.append("               AND CH.TYPE = 2 ");
		var1.append("               AND CH.OBJECT_TYPE IN ( 1, 2 ) ");
		var1.append("               AND NVBH.SHOP_ID = SH.SHOP_ID ");
		var1.append("               AND NVBH.STATUS = 1 ");
		var1.append("               AND SH.STATUS = 1 ");
		var1.append("               AND NVBH.SHOP_ID IN ( " + shopStr + " ) ");
		var1.append("               AND GS.STAFF_ID IN ( ");
		var1.append(listGs);
		var1.append("               ) ");
		var1.append("               AND NVBH.STAFF_ID IN ");
		var1.append("					(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		var1.append("						AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
		var1.append("						AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		var1.append("               AND GS.STAFF_TYPE_ID = GS_CH.CHANNEL_TYPE_ID ");
		var1.append("               AND GS_CH.TYPE = 2 ");
		var1.append("               AND GS_CH.OBJECT_TYPE = 5 ");
		var1.append("               AND GS.STATUS = 1 ");
		var1.append("        GROUP  BY SH.SHOP_ID, ");
		var1.append("                  GS.STAFF_ID ");
		var1.append("        ORDER  BY SH.SHOP_CODE, ");
		var1.append("                  GS.STAFF_NAME) NPP_LIST ");
		var1.append("       LEFT JOIN (SELECT NPP_STAFF_ID                AS NPP_STAFF_ID_2, ");
		var1.append("                         NVBH_SHOP_CODE              AS NVBH_SHOP_CODE_1, ");
		var1.append("                         COUNT(CASE ");
		var1.append("                                 WHEN ( NUM0930 > 0 ");
		var1.append("                                         OR TOTAL_CUS_PLAN IS NULL ");
		var1.append("                                         OR TOTAL_CUS_PLAN = 0 ) THEN 1 ");
		var1.append("                               END)                  AS NUM930NUM, ");
		var1.append("                         GROUP_CONCAT(NVBH_STAFF_ID) AS STAFF_LIST, ");
		var1.append("                         GROUP_CONCAT(CASE ");
		var1.append("                                        WHEN TOTAL_CUS_PLAN IS NULL THEN 0 ");
		var1.append("                                        ELSE TOTAL_CUS_PLAN ");
		var1.append("                                      END)           AS TOTAL_CUS_PLAN_LIST, ");
		var1.append("                         GROUP_CONCAT(NUM1200)       AS NUM1200_LIST, ");
		var1.append("                         GROUP_CONCAT(NUM1600)       AS NUM1600_LIST, ");
		var1.append("                         GROUP_CONCAT(NUM_NOW)       AS NUM_NOW_LIST ");
		var1.append("                  FROM   (SELECT NPP_STAFF_ID, ");
		var1.append("                                 NVBH_SHOP_CODE, ");
		var1.append("                                 NVBH_STAFF_ID, ");
		var1.append("                                 TOTAL_CUS_PLAN, ");
		var1.append("                                 COUNT(CASE ");
		var1.append("                                         WHEN TIME(STRFTIME('%H:%M', START_TIME) ");
		var1.append("                                              ) <= ");
		var1.append("                                              TIME( ");
		var1.append("                                              STRFTIME('%H:%M', '"
				+ DT_START + "')) ");
		var1.append("                                       THEN 1 ");
		var1.append("                                       END) AS NUM0930, ");
		var1.append("                                 COUNT(CASE ");
		var1.append("                                         WHEN TIME(STRFTIME('%H:%M', START_TIME) ");
		var1.append("                                              ) <= ");
		var1.append("                                              TIME( ");
		var1.append("                                              STRFTIME('%H:%M', '"
				+ DT_MIDDLE + "')) ");
		var1.append("                                       THEN 1 ");
		var1.append("                                       END) AS NUM1200, ");
		var1.append("                                 COUNT(CASE ");
		var1.append("                                         WHEN TIME(STRFTIME('%H:%M', START_TIME) ");
		var1.append("                                              ) <= ");
		var1.append("                                              TIME( ");
		var1.append("                                              STRFTIME('%H:%M', '"
				+ DT_END + "')) ");
		var1.append("                                       THEN 1 ");
		var1.append("                                       END) AS NUM1600, ");
		var1.append("                                 COUNT(CASE ");
		var1.append("                                         WHEN TIME(STRFTIME('%H:%M', START_TIME) ");
		var1.append("                                              ) <= ");
		var1.append("                                              TIME( ");
		var1.append("                                              STRFTIME('%H:%M', '"
				+ date_now + "')) ");
		var1.append("                                       THEN 1 ");
		var1.append("                                       END) AS NUM_NOW ");
		var1.append("                          FROM   (SELECT NVBH.*, ");
		var1.append("                                         NVBH_AL.START_TIME  AS START_TIME, ");
		var1.append("                                         PLAN.TOTAL_CUS_PLAN AS TOTAL_CUS_PLAN ");
		var1.append("                                  FROM   (SELECT ST.STAFF_ID       AS ");
		var1.append("                                                 NVBH_STAFF_ID, ");
		var1.append("                                                 GS.STAFF_ID AS ");
		var1.append("                                                 NPP_STAFF_ID, ");
		var1.append("                                                 SHOP.SHOP_CODE    AS ");
		var1.append("                                                 NVBH_SHOP_CODE ");
		var1.append("                                          FROM   STAFF GS, ");
		var1.append("                                                 STAFF ST, ");
		var1.append("                                                 CHANNEL_TYPE CH, ");
		var1.append("                                                 SHOP ");
		var1.append("                                          WHERE  GS.STATUS = 1 ");
		var1.append("                                                 AND ST.STATUS = 1 ");
		var1.append("                                                 AND GS.STAFF_ID IN ( ");
		var1.append(listGs);
		var1.append("                                                 )");
		var1.append("                                                 AND ST.STAFF_ID IN ");
		var1.append("														(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		var1.append("															AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
		var1.append("															AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		var1.append("                                                 AND ST.STAFF_TYPE_ID = ");
		var1.append("                                                     CH.CHANNEL_TYPE_ID ");
		var1.append("                                                 AND CH.TYPE = 2 ");
		var1.append("                                                 AND CH.OBJECT_TYPE IN ( 1, 2 ) ");
		var1.append("                                                 AND ST.SHOP_ID IN ");
		var1.append("                                                     ( "
				+ shopStr + " ) ");
		var1.append("                                                 AND SHOP.SHOP_ID = ST.SHOP_ID ");
		var1.append("                                                 AND SHOP.STATUS = 1) ");
		var1.append("                                         NVBH ");
		var1.append("                                         LEFT JOIN (SELECT ");
		var1.append("                                         STAFF_ID AS NVBH_AL_STAFF_ID ");
		var1.append("                                         , ");
		var1.append("       START_TIME ");
		var1.append("       FROM   ACTION_LOG ");
		var1.append("       WHERE  STAFF_ID = STAFF_ID ");
		var1.append("       AND OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("       AND IS_OR = 0 ");
		var1.append("       AND DATE(START_TIME) = DATE(?)) ");
		param.add(date_now);
		var1.append("       NVBH_AL ");
		var1.append("       ON NVBH.NVBH_STAFF_ID = NVBH_AL.NVBH_AL_STAFF_ID ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID               AS STAFF_ID_1, ");
		var1.append("       COUNT(RTC.CUSTOMER_ID) AS ");
		var1.append("       TOTAL_CUS_PLAN ");
		var1.append("       FROM   VISIT_PLAN VP, ");
		var1.append("       ROUTING RT, ");
		var1.append("       (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("               ROUTING_ID, ");
		var1.append("               CUSTOMER_ID, ");
		var1.append("               START_DATE, ");
		var1.append("               END_DATE, ");
		var1.append("               STATUS, ");
		var1.append("               MONDAY, ");
		var1.append("               TUESDAY, ");
		var1.append("               WEDNESDAY, ");
		var1.append("               THURSDAY, ");
		var1.append("               FRIDAY, ");
		var1.append("               SATURDAY, ");
		var1.append("               SUNDAY, ");
		var1.append("               SEQ2, ");
		var1.append("               SEQ3, ");
		var1.append("               SEQ4, ");
		var1.append("               SEQ5, ");
		var1.append("               SEQ6, ");
		var1.append("               SEQ7, ");
		var1.append("               SEQ8 ");
		var1.append("        FROM   ROUTING_CUSTOMER");
		var1.append("                WHERE (DATE(END_DATE) >= DATE(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= DATE(?) and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when (cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when (cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when (cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when (cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");

		var1.append(") RTC, ");
		var1.append("       CUSTOMER CT ");
		var1.append("       WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("       AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("       AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("       AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("       AND ( VP.TO_DATE IS NULL ");
		var1.append("              OR DATE(VP.TO_DATE) >= DATE(?) ) ");
		param.add(date_now);
		var1.append("       AND DATE(RTC.START_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("       AND ( RTC.END_DATE IS NULL ");
		var1.append("              OR DATE(RTC.END_DATE) >= DATE(?) ) ");
		param.add(date_now);
		var1.append("       AND VP.STATUS = 1 ");
		var1.append("       AND RT.STATUS = 1 ");
		var1.append("       AND CT.STATUS = 1 ");
		var1.append("       AND RTC.STATUS = 1 ");
		var1.append("       AND RTC." + day + " = 1 ");
		var1.append("       GROUP  BY VP.STAFF_ID) PLAN ");
		var1.append("       ON NVBH.NVBH_STAFF_ID = PLAN.STAFF_ID_1) AL_NVBH ");
		var1.append("       GROUP  BY NVBH_STAFF_ID) ");
		var1.append("       GROUP  BY NPP_STAFF_ID, ");
		var1.append("       NVBH_SHOP_CODE) COUNT_NVBH ");
		var1.append("              ON ( NPP_LIST.GS_STAFF_ID = COUNT_NVBH.NPP_STAFF_ID_2 ");
		var1.append("                   AND NPP_LIST.NVBH_SHOP_CODE = COUNT_NVBH.NVBH_SHOP_CODE_1 ) ");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						TBHVVisitCustomerNotificationItem item = dto
								.newTBHVVisitCustomerNotificationItem();
						item.initData(c);
						dto.arrList.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return dto;
	}

	/**
	 * Lay danh sach cham cong cua GS
	 * @author: hoanpd1
	 * @since: 15:30:51 12-03-2015
	 * @return: List<AttendanceDTO>
	 * @throws:
	 * @param ext
	 * @param shopPosition
	 * @return
	 * @return: ListProductDTO
	 * @throws Exception
	 * @throws:
	 */
	public List<AttendanceDTO> getSupervisorAttendanceList(Bundle ext,
			LatLng shopPosition) throws Exception {
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) ext.getSerializable(IntentConstants.INTENT_SORT_DATA);

		List<AttendanceDTO> list = new ArrayList<AttendanceDTO>();
		List<AttendanceDTO> supList = new ArrayList<AttendanceDTO>();
		List<AttendanceDTO> resultList = new ArrayList<AttendanceDTO>();

		ArrayList<String> params = new ArrayList<String>();
//		String listStaffId = PriUtils.getInstance().getListStaffOfSupervisor(staffId, shopId);
		String listStaffId = getListStaffOfSupervisor(staffId, shopId);
//		StringBuffer  sql = new StringBuffer();
//		sql.append("  SELECT ST.STAFF_ID, ");
//		sql.append("         ST.STAFF_CODE, ");
//		sql.append("         ST.STAFF_NAME, ");
//		sql.append("         SPLTEMP.LAT, ");
//		sql.append("         SPLTEMP.LNG, ");
//		sql.append("         strftime('%Y-%m-%d %H:%M',SPLTEMP.CREATE_DATE) CREATE_DATE ");
//		sql.append("  FROM STAFF ST ");
//		sql.append("  LEFT JOIN (SELECT * ");
//		sql.append("             FROM STAFF_POSITION_LOG SPL ");
//		sql.append("             WHERE SPL.STAFF_ID IN (SELECT STAFF_ID ");
//		sql.append("                                    FROM STAFF ");
//		sql.append("                                    WHERE STATUS = 1 ");
//		sql.append("                                          AND  ((STAFF_ID in ( " + listStaffId +" ) ");
//		sql.append("                                                 AND SHOP_ID = ? ) ");
//		params.add(shopId);
////		sql.append("                                               OR staff_id = ? ) ");
////		params.add(staffId);
//		sql.append("                                    )) ");
//		sql.append("                   AND DATE(SPL.CREATE_DATE) = DATE('now', 'localtime') ");
//		sql.append("            ) SPLTEMP ");
//		sql.append("              ON ST.STAFF_ID = SPLTEMP.STAFF_ID ");
//		sql.append("  WHERE ST.STAFF_ID IN (SELECT STAFF_ID ");
//		sql.append("                        FROM STAFF ");
//		sql.append("                        WHERE STATUS = 1 ");
//		sql.append("                              AND ((STAFF_ID in ( " + listStaffId +" ) ");
//		sql.append("                                      AND SHOP_ID = ? ) ");
//		params.add(shopId);
////		sql.append("                                 OR staff_id = ? ) ");
////		params.add(staffId);
//		sql.append("                        )) ");
//		sql.append("  ORDER BY ST.STAFF_CODE, ");
//		sql.append("           ST.STAFF_NAME, ");
//		sql.append("           SPLTEMP.CREATE_DATE");


		StringBuffer  sql = new StringBuffer();
		sql.append(" SELECT  ST.STAFF_ID AS STAFF_ID, ");
		sql.append("        ST.STAFF_CODE AS STAFF_CODE, ");
		sql.append("        ST.STAFF_NAME AS STAFF_NAME, ");
		sql.append("        SPLTEMP.LAT AS LAT, ");
		sql.append("        SPLTEMP.LNG AS LNG, ");
		sql.append("        strftime('%Y-%m-%d %H:%M',SPLTEMP.CREATE_DATE) AS CREATE_DATE ");
		sql.append(" FROM (SELECT s.staff_id, ");
		sql.append("             s.staff_code, ");
		sql.append("             s.staff_name ");
		sql.append("      FROM staff s, ");
		sql.append("           staff_type st ");
		sql.append("      WHERE s.staff_id in  (" + listStaffId + " ) ");
		sql.append("            AND S.SHOP_ID = ? ");
		params.add(shopId);
		sql.append("            AND s.staff_type_id = st.staff_type_id ");
		sql.append("            AND st.status = 1 ");
//		sql.append("            AND st.specific_type = ? ");
//		params.add(String.valueOf(UserDTO.TYPE_STAFF));
		sql.append("            AND s.status = 1) ST ");
		sql.append(" LEFT JOIN STAFF_POSITION_LOG SPLTEMP ");
		sql.append("     ON (ST.STAFF_ID = SPLTEMP.STAFF_ID ");
		sql.append("        AND DATE(SPLTEMP.CREATE_DATE) = DATE('now', 'localtime') ");
		sql.append("        AND SPLTEMP.SHOP_ID = ?) ");
		params.add(shopId);
		sql.append(" WHERE 1=1 ");

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY ST.STAFF_CODE, ST.STAFF_NAME, SPLTEMP.CREATE_DATE ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "ST.STAFF_CODE")
				.addMapper(SortActionConstants.NAME, "ST.STAFF_NAME")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		sql.append(orderByStr);

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						AttendanceDTO atDTO = new AttendanceDTO();
						atDTO.initWithCursor(c, shopPosition, shopId);
						// add giam sat len dau tien
						if (atDTO.staffId == Integer.parseInt(staffId)) {
							supList.add(atDTO);
						} else {
							list.add(atDTO);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		resultList.addAll(supList);
		resultList.addAll(list);
		return resultList;
	}

	/**
	 * Lay ds gs cung voi shop tuong ung
	 *
	 * @author: TamPQ
	 * @param staffIdGS
	 * @return
	 * @return: TBHVVisitCustomerNotificationDTOvoid
	 * @throws:
	 */
	public List<ShopSupervisorDTO> getTBHListSupervisorShopManaged(String shopId) throws Exception {
		List<ShopSupervisorDTO> list = new ArrayList<ShopSupervisorDTO>();
		Cursor c = null;

		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopId);
		String shopStr = TextUtils.join(",", shopList);

//		StringBuffer var1 = new StringBuffer();
//		var1.append(" SELECT GS.STAFF_ID          AS GS_STAFF_ID, ");
//		var1.append("       GS.STAFF_CODE        AS GS_STAFF_CODE, ");
//		var1.append("       GS.STAFF_NAME        AS GS_STAFF_NAME, ");
//		var1.append("       SH.SHOP_ID			 AS NVBH_SHOP_ID, ");
//		var1.append("       SH.SHOP_CODE		 AS NVBH_SHOP_CODE, ");
//		var1.append("       COUNT(NVBH.STAFF_ID) AS NUM_NVBH ");
//		var1.append(" FROM   STAFF NVBH, ");
//		var1.append("       CHANNEL_TYPE CH, ");
//		var1.append("       SHOP SH, ");
//		var1.append("       STAFF GS ");
//		var1.append(" WHERE  NVBH.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID ");
//		var1.append("       AND CH.TYPE = 2 ");
//		var1.append("       AND CH.OBJECT_TYPE IN ( 1, 2 ) ");
//		var1.append("       AND NVBH.SHOP_ID = SH.SHOP_ID ");
//		var1.append("       AND NVBH.STATUS = 1 ");
//		var1.append("       AND SH.STATUS = 1 ");
//		var1.append("       AND NVBH.SHOP_ID IN ( " + shopStr + " ) ");
//		var1.append("       AND NVBH.STAFF_ID IN ");
//		var1.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
//		var1.append("				AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
//		var1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
//		var1.append("       AND GS.STATUS = 1 ");
//		var1.append(" GROUP  BY SH.SHOP_ID, ");
//		var1.append("          GS.STAFF_ID ");
//		var1.append(" ORDER  BY SH.SHOP_CODE, ");
//		var1.append("          GS.STAFF_NAME ");

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT GS.STAFF_ID            AS GS_STAFF_ID, ");
		var1.append("       GS.STAFF_CODE          AS GS_STAFF_CODE, ");
		var1.append("       GS.STAFF_NAME          AS GS_STAFF_NAME, ");
		var1.append("       NVBH.SHOP_ID           AS NVBH_SHOP_ID, ");
		var1.append("       NVBH.SHOP_CODE         AS NVBH_SHOP_CODE, ");
		var1.append("       COUNT(NVBH.STAFF_ID)   AS NUM_NVBH ");
		var1.append("FROM STAFF GS, ");
		var1.append("     (select st.staff_id         as STAFF_ID , ");
		var1.append("             sh.shop_id          as SHOP_ID, ");
		var1.append("             sh.shop_code        as SHOP_CODE, ");
		var1.append("             sh.shop_name        as SHOP_NAME, ");
		var1.append("             psm.parent_staff_id as PARENT_STAFF_ID ");
		var1.append("      from staff st, ");
		var1.append("           channel_type ct, ");
		var1.append("           shop sh, ");
		var1.append("           (select psm.staff_id, ");
		var1.append("                   parent_staff_id ");
		var1.append("            from parent_staff_map psm ");
		var1.append("            where 1=1 ");
		var1.append("                  and psm.STATUS = 1 ");
		var1.append("                  and not exists (select * ");
		var1.append("                                  from exception_user_access ");
		var1.append("                                  where staff_id = psm.staff_id ");
		var1.append("                                        and parent_staff_id = psm.parent_staff_id ");
		var1.append("                                        and status = 1) ");
		var1.append("            ) psm ");
		var1.append("      where 1=1 ");
		var1.append("            and st.staff_type_id= ct.channel_type_id ");
		var1.append("            and st.shop_id = sh.shop_id ");
		var1.append("            and ct.type = 2 ");
		var1.append("            and ct.object_type in (1,2) ");
		var1.append("            and sh.status = 1 ");
		var1.append("            and st.status =1 ");
//		var1.append("            and st.shop_id = 91 ");
		var1.append("            and st.shop_id in ( " + shopStr + " ) ");
		var1.append("            and st.staff_id = psm.staff_id ");
		var1.append("      )NVBH ");
		var1.append(" WHERE 1=1 ");
		var1.append("       AND GS.STATUS = 1 ");
		var1.append("       AND GS.STAFF_ID = NVBH.PARENT_STAFF_ID ");
		var1.append(" GROUP BY NVBH.SHOP_ID, ");
		var1.append("          GS.STAFF_ID ");
		var1.append(" ORDER BY NVBH.SHOP_CODE, ");
		var1.append("          GS.STAFF_NAME");


		try {
			c = rawQuery(var1.toString(), null);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ShopSupervisorDTO item = new ShopSupervisorDTO();
						item.initWithCursor(c);
						list.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return list;
	}

	/**
	 * Lay action_log ghe tham trong ngay voi end_time = null
	 *
	 * @author: TamPQ
	 * @return
	 * @return: ArrayList<ActionLogDTO>void
	 * @throws:
	 */
	public ArrayList<ActionLogDTO> getVisitActionLogWithEndTimeIsNull(int staffId, long cusId, String shopId) {
		ArrayList<ActionLogDTO> listActLog = new ArrayList<ActionLogDTO>();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append("SELECT * ");
		var1.append("FROM   ACTION_LOG ");
		var1.append("WHERE  STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("       AND CUSTOMER_ID != ? ");
		param.add("" + cusId);
		var1.append("       AND SHOP_ID = ? ");
		param.add(shopId);
		var1.append("       AND OBJECT_TYPE = 0 ");
		var1.append("       AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		var1.append("       AND END_TIME IS NULL ");

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ActionLogDTO item = new ActionLogDTO();
						item.initDataFromCursor(c);
						listActLog.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listActLog;
	}

	/**
	 *
	 * lay combobox type problem cho man hinh theo doi khac phuc
	 *
	 * @author: HieuNH
	 * @param
	 * @return
	 * @return: ComboboxFollowProblemDTO
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getComboboxProblemTypeProblem(
			boolean isGSNPP) {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder
				.append("SELECT AP.AP_PARAM_CODE, AP.AP_PARAM_NAME FROM AP_PARAM AP");
		if (isGSNPP) {
			stringbuilder
					.append(" WHERE (AP.TYPE LIKE 'FEEDBACK_TYPE_NVBH' OR AP.TYPE LIKE 'FEEDBACK_TYPE_GSNPP') AND STATUS = 1");
		} else {// tbhv
			stringbuilder
					.append(" WHERE (AP.TYPE LIKE 'FEEDBACK_TYPE_TBHV') AND STATUS = 1");
		}
		String requestGetFollowProblemList = stringbuilder.toString();
		Cursor c = null;
		String[] params = new String[] {};

		try {
			c = rawQuery(requestGetFollowProblemList, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						if (c.getColumnIndex("AP_PARAM_CODE") >= 0) {
							DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
							comboboxNVBHDTO.value = CursorUtil.getString(c, "AP_PARAM_CODE");
							comboboxNVBHDTO.name = CursorUtil.getString(c, "AP_PARAM_NAME");
							result.add(comboboxNVBHDTO);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			// return null;
			result = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 * lay danh sach nvbh cua gsnpp
	 *
	 * @author thanhnguyen
	 * @param shopId
	 * @param staffOwnerId
	 * @return
	 * @throws Exception
	 */
	public ListStaffDTO getListNVBHForGSNPP(int shopId, String staffOwnerId,
			boolean orderByCode) throws Exception {
		ListStaffDTO dto = new ListStaffDTO();
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();

		sqlQuery.append("SELECT DISTINCT s.staff_name     STAFF_NAME, ");
		sqlQuery.append("                s.staff_id 	  STAFF_ID, ");
		sqlQuery.append("                s.staff_code     STAFF_CODE ");
		sqlQuery.append("FROM   staff s ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append(" AND s.staff_type_id in (select channel_type_id from channel_type where type = 2 and status = 1 and object_type in (1,2)) ");
		sqlQuery.append(" AND s.status = 1");
		if (!StringUtil.isNullOrEmpty("" + shopId)) {
			sqlQuery.append(" AND s.shop_id = ? ");
			params.add("" + shopId);
		}

		if (!StringUtil.isNullOrEmpty("" + staffOwnerId)) {
			sqlQuery.append(" AND s.staff_id IN ( " + getListStaffOfSupervisor(String.valueOf(staffOwnerId), shopId + "") +" ) ");

		}
		if (orderByCode) {
			sqlQuery.append(" ORDER  BY s.staff_code, s.staff_name ");
		} else {
			sqlQuery.append(" ORDER  BY s.staff_name ");
		}

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				if (c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.parrseStaffFromCursor(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			// c = null;
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * Lay ds nhan vien cua GS & GS
	 *
	 * @author: TamPQ
	 * @return
	 * @return: ArrayList<StaffChoosenDTO>
	 * @throws Exception
	 * @throws:
	 */

	public ArrayList<StaffChoosenDTO> getSupervisorSearchImageListStaff(
			Bundle data) throws Exception {
		ArrayList<StaffChoosenDTO> result = new ArrayList<StaffChoosenDTO>();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append("SELECT STAFF_ID, STAFF_CODE, STAFF_NAME AS STAFF_NAME, OBJECT_TYPE AS ROLE_TYPE from staff st join channel_type ct"
				+ " on st.staff_type_id = ct.channel_type_id where 1 = 1 and st.status = 1 and ct.status = 1 and ct.type = 2");
		var1.append(" and st.shop_id = ? and st.staff_id IN ( " + getListStaffOfSupervisor(String.valueOf(staffId), shopId) +" ) ");
		param.add(shopId);
		var1.append(" ORDER BY STAFF_CODE ");

		StaffChoosenDTO allStaff = new StaffChoosenDTO();
		allStaff.staffName = StringUtil.getString(R.string.TEXT_ALL);
		allStaff.roleType = 7;
		result.add(allStaff);

		StaffChoosenDTO supStaff = new StaffChoosenDTO();
		supStaff.staffCode = GlobalInfo.getInstance().getProfile()
				.getUserData().getUserCode();
		supStaff.staffName = GlobalInfo.getInstance().getProfile()
				.getUserData().getDisplayName();
		supStaff.roleType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
		supStaff.statffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		result.add(supStaff);

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						StaffChoosenDTO staff = new StaffChoosenDTO();
						staff.initDataFromCursor(c);
						result.add(staff);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * Lay ds nhan vien cua GS & GS
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: ArrayList<StaffChoosenDTO>
	 * @throws:
	 */

	public ArrayList<StaffChoosenDTO> getTBHVSearchImageListStaff(Bundle data) throws Exception {
		ArrayList<StaffChoosenDTO> result = new ArrayList<StaffChoosenDTO>();
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		String staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";
		String listGs = PriUtils.getInstance().getListSupervisorOfASM(staffId);
		// StringBuffer supSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		// supSql.append("SELECT STAFF_ID, STAFF_CODE, STAFF_NAME, OBJECT_TYPE AS ROLE_TYPE from staff st join channel_type on channel_type_id = st.staff_type_id where 1 = 1 and st.status = 1 ");
		// supSql.append(" and channel_type.object_type = 2 and st.shop_id in (select shop_id from shop where parent_shop_id = ?)");
		// param.add(shopId);
		// supSql.append(" ORDER BY STAFF_CODE ");

		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursiveReverse(shopId);
		String strListShop = TextUtils.join(",", listShopId);

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT sh.shop_id     AS NPP_SHOP_ID, ");
		var1.append("       sh.shop_name   NPP_SHOP_NAME, ");
		var1.append("       sh.shop_code   NPP_SHOP_CODE, ");
		var1.append("       st1.staff_id   AS STAFF_ID, ");
		var1.append("       st1.staff_name STAFF_NAME, ");
		var1.append("       st1.staff_code STAFF_CODE, ");
		var1.append("       5 ROLE_TYPE ");
		var1.append("FROM   shop sh, ");
		var1.append("       staff st, ");
		var1.append("       staff st1 ");
		var1.append("WHERE  sh.parent_shop_id IN ( ");
		var1.append(strListShop + " ) ");
		var1.append("       AND st1.staff_id IN (");
		var1.append(listGs);
		var1.append("       )");
		var1.append("       AND st.staff_id IN ");
		var1.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		var1.append("				AND PS.PARENT_STAFF_ID = st1.staff_id AND PS.STATUS = 1 ");
		var1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		var1.append("       AND st.status = 1 ");
		var1.append("       AND st1.status = 1 ");
		var1.append("       AND st.[shop_id] = sh.[shop_id] ");
		var1.append("GROUP  BY  ");
		var1.append("          st1.staff_id ");

		StringBuffer staffSql = new StringBuffer();
		ArrayList<String> staffParam = new ArrayList<String>();
		staffSql.append("SELECT st.STAFF_ID STAFF_ID, st.STAFF_CODE STAFF_CODE, st.STAFF_NAME STAFF_NAME, 1 AS ROLE_TYPE, gs.STAFF_ID STAFF_OWNER_ID FROM staff gs, staff st where 1 = 1 and gs.staff_id in (" + listGs + ") and gs.status = 1 and st.status = 1 ");
		staffSql.append(" and st.staff_id in ");
		staffSql.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		staffSql.append("				AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
		staffSql.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		staffSql.append(" and st.shop_id in (select shop_id from shop where parent_shop_id = ?)");
		staffParam.add(shopId);
		staffSql.append(" ORDER BY STAFF_OWNER_ID, STAFF_CODE ");

		StaffChoosenDTO allStaff = new StaffChoosenDTO();
		allStaff.staffName = StringUtil.getString(R.string.TEXT_ALL);
		allStaff.roleType = 7;
		result.add(allStaff);

		StaffChoosenDTO supStaff = new StaffChoosenDTO();
		supStaff.statffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		supStaff.staffCode = GlobalInfo.getInstance().getProfile()
				.getUserData().getUserCode();
		supStaff.staffName = GlobalInfo.getInstance().getProfile()
				.getUserData().getDisplayName();
		supStaff.roleType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
		result.add(supStaff);

		Cursor c = null;
		Cursor c2 = null;
		ArrayList<StaffChoosenDTO> listSupervisor = new ArrayList<StaffChoosenDTO>();
		ArrayList<StaffChoosenDTO> listStaff = new ArrayList<StaffChoosenDTO>();
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						StaffChoosenDTO staff = new StaffChoosenDTO();
						staff.initDataFromCursor(c);
						listSupervisor.add(staff);
					} while (c.moveToNext());
				}
			}

			c2 = rawQueries(staffSql.toString(), staffParam);
			if (c2 != null) {
				if (c2.moveToFirst()) {
					do {
						StaffChoosenDTO staff = new StaffChoosenDTO();
						staff.initDataFromCursor(c2);
						listStaff.add(staff);
					} while (c2.moveToNext());
				}
			}

			for (StaffChoosenDTO supDto : listSupervisor) {
				result.add(supDto);
				for (StaffChoosenDTO staffDto : listStaff) {
					if (supDto.statffId == staffDto.staffOwnerId) {
						result.add(staffDto);
					}
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (c2 != null) {
					c2.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	 /**
	 * Lay loai nv
	 * @author: Tuanlt11
	 * @param staffId
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int getStaffType(String staffId) throws Exception{
		Cursor c = null;
		int staffType = UserDTO.TYPE_STAFF;
		ArrayList<String> paramsObject = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT st.SPECIFIC_TYPE ");
		varname1.append("FROM   staff s, ");
		varname1.append("       staff_type st ");
		varname1.append("WHERE  st.status = 1 ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND s.staff_type_id = st.staff_type_id ");
		varname1.append("       AND s.staff_id = ?;");
		paramsObject.add(staffId);
		try {
			c = rawQueries(varname1.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					staffType = CursorUtil.getInt(c, "SPECIFIC_TYPE");
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return staffType;
	}

	 /**
	 * Lay ds staff con, ko phan biet nv,gs hay mot loai nhan vien nao do
	 * @author: Tuanlt11
	 * @param staffId
	 * @return
	 * @return: String
	 * @throws:
	*/
	public String getListStaffChild(String staffId, String shopId) throws Exception {
		ArrayList<String> listStaffId = null;
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer  var1 = new StringBuffer();

			var1.append("	    SELECT	");
			var1.append("	        distinct st.staff_id AS STAFF_ID	");
			var1.append("	    FROM	");
			var1.append("	        staff st	");
			var1.append("	    LEFT JOIN	");
			var1.append("	        parent_staff_map psm	");
			var1.append("	            ON st.staff_id = psm.staff_id	");
			var1.append("	            AND psm.status = 1	");
			var1.append("	            AND psm.parent_staff_id = ?	");
			params.add(""+staffId);
			var1.append("	            AND NOT EXISTS (	");
			var1.append("	                SELECT	");
			var1.append("	                    *	");
			var1.append("	            FROM	");
			var1.append("	                exception_user_access eua	");
			var1.append("	            WHERE	");
			var1.append("	                eua.staff_id = st.staff_id	");
			var1.append("	                AND eua.parent_staff_id = psm.parent_staff_id	");
			var1.append("	                AND eua.status = 1	");
			var1.append("	        )	");
			var1.append("	    WHERE	1= 1 ");
			var1.append("	        AND st.status = 1	");
			var1.append("	        AND (CASE when psm.staff_id is not null THEN st.staff_id  = psm.staff_id ELSE 0 END) ");
//			var1.append("	    UNION	");
//			var1.append("	    SELECT	");
//			var1.append("	        distinct st.staff_id AS STAFF_ID 	");
//			var1.append("	    FROM	");
//			var1.append("	        staff st	");
////			var1.append("	    JOIN	");
////			var1.append("	        (	");
////			var1.append("	            SELECT	");
////			var1.append("	                sh.shop_id	");
////			var1.append("	            FROM	");
////			var1.append("	                shop sh	");
////			var1.append("	            WHERE	");
////			var1.append("	                1 = 1	");
////			var1.append("	                AND sh.status = 1	");
////			var1.append("	                AND sh.parent_shop_id = ?	");
////			params.add(""+shopId);
////			var1.append("	        ) SH	");
////			var1.append("	            ON st.shop_id = sh.shop_id	");
//			var1.append("	    LEFT JOIN	");
//			var1.append("	        (	SELECT ");
//			var1.append("	                distinct oa.shop_id	");
//			var1.append("	            FROM	");
//			var1.append("	                role_user ru,	");
//			var1.append("	                role_permission_map rpm,	");
//			var1.append("	                org_access oa	");
//			var1.append("	            WHERE	");
//			var1.append("	                1 = 1	");
//			var1.append("	                AND oa.status = 1	");
//			var1.append("	                AND rpm.status = 1	");
//			var1.append("	                AND ru.status = 1	");
//			var1.append("	                AND ru.role_id = rpm.role_id	");
//			var1.append("	                AND ru.user_id = ?	");
//			params.add(""+staffId);
//			var1.append("	                AND rpm.permission_id = oa.permission_id	");
//			var1.append("	        ) OA	");
//			var1.append("	            ON OA.shop_id = st.shop_id	");
//			var1.append("	    WHERE	1= 1 ");
//			var1.append("	        AND st.status = 1	");
//			var1.append("	        AND (CASE when OA.shop_id is null THEN st.shop_id = ? ");
//			params.add(""+shopId);
//			var1.append("	       		 ELSE st.shop_id = OA.shop_id AND oa.shop_id = ?	  ");
//			params.add(""+shopId);
////			var1.append("	        AND ST.shop_id  IN	");
////			var1.append("	        (select shop_id from shop where parent_shop_id = ?)	");
////			params.add(shopId);
//			var1.append("	       	AND st.shop_id not in  (	");
//			var1.append("	            SELECT	");
//			var1.append("	                distinct stt.shop_id	");
//			var1.append("	            FROM	");
//			var1.append("	                staff stt	");
//			var1.append("	            WHERE	");
//			var1.append("	                1 = 1	");
//			var1.append("	                AND stt.staff_id = st.staff_id   	");
//			var1.append("	                AND stt.status = 1	");
//			var1.append("	                AND                   exists (	");
//			var1.append("	                    SELECT	");
//			var1.append("	                        1	");
//			var1.append("	                    FROM	");
//			var1.append("	                        parent_staff_map psm	");
//			var1.append("	                    WHERE	");
//			var1.append("	                        psm.parent_staff_id = ?	");
//			params.add(""+staffId);
//			var1.append("	                        AND psm.staff_id = stt.staff_id	and psm.status = 1");
//			var1.append("	                        AND NOT EXISTS (	");
//			var1.append("	                            SELECT	");
//			var1.append("	                                *	");
//			var1.append("	                            FROM	");
//			var1.append("	                                exception_user_access eua	");
//			var1.append("	                            WHERE	");
//			var1.append("	                                eua.staff_id = stt.staff_id	");
//			var1.append("	                                AND eua.parent_staff_id = psm.parent_staff_id	");
//			var1.append("	                                AND eua.status = 1	");
//			var1.append("	                        )	");
//			var1.append("	                    )	");
//			var1.append("	            )	");
//			var1.append("	       										END) ");

			c = rawQueries(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					listStaffId = new ArrayList<String>();
					do {
						String temp = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(temp);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		String result = "";
		if(listStaffId != null){
			result = TextUtils.join(",", listStaffId);
		}

		return result;
	}



	 /**
	 * Lay ds ghe tham di tuyen
	 * @author: Tuanlt11
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: TBHVVisitCustomerNotificationDTO
	 * @throws:
	*/
	public TBHVVisitCustomerNotificationDTO getVisitCustomerNotification(String staffId, String shopId)
			throws Exception {
		TBHVVisitCustomerNotificationDTO dto = new TBHVVisitCustomerNotificationDTO();
		Cursor c = null;
		// lay ds nhung nhan vien o duoi staffId truyen vao mot cap
		String listGs = getListStaffManagerOfChildShop(staffId, shopId);
		ArrayList<String> listShopID = getListShopChild(shopId);
		String res = TextUtils.join(",", listShopID);

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT GS.STAFF_ID          AS GS_STAFF_ID, ");
		var1.append("               GS.STAFF_CODE        AS GS_STAFF_CODE, ");
		var1.append("               GS.STAFF_NAME        AS GS_STAFF_NAME, ");
		var1.append("               SH.SHOP_ID           AS NVBH_SHOP_ID, ");
		var1.append("               SH.SHOP_CODE         AS NVBH_SHOP_CODE ");
		var1.append("        FROM   ORG_TEMP OT, ");
		var1.append("               SHOP SH, ");
		var1.append("               STAFF_TYPE ST, ");
		var1.append("               STAFF GS ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND SH.STATUS = 1 ");
		var1.append("           	AND OT.SHOP_ID = SH.SHOP_ID ");
		var1.append("           	AND OT.STAFF_ID = GS.STAFF_ID ");
		var1.append("       		AND ST.STATUS = 1 ");
//		var1.append("       		AND ST.SPECIFIC_TYPE IN (?, ?, ?) ");
//		param.add(String.valueOf(UserDTO.TYPE_STAFF));
//		param.add(String.valueOf(UserDTO.TYPE_SUPERVISOR));
//		param.add(String.valueOf(UserDTO.TYPE_MANAGER));
		var1.append("       		AND ST.STAFF_TYPE_ID = GS.STAFF_TYPE_ID ");
		var1.append("               AND GS.STAFF_ID IN ( ");
		var1.append(listGs);
		var1.append("               ) ");
		var1.append("               AND SH.SHOP_ID IN ( ");
		var1.append(res);
		var1.append("               ) ");

		//-- ket thuc lay nhan vien con
		var1.append("               AND GS.STATUS = 1 ");
		var1.append("        GROUP  BY SH.SHOP_ID, ");
		var1.append("                  GS.STAFF_ID ");
		var1.append("        ORDER  BY SH.SHOP_CODE, ");
		var1.append("                  GS.STAFF_NAME) NPP_LIST ");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						TBHVVisitCustomerNotificationItem item = dto
								.newTBHVVisitCustomerNotificationItem();
						item.initData(c);
						dto.arrList.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return dto;
	}

	/**
	 * Lay danh sach quan li don vi con cua don vi truyen vao
	 *
	 * @author: dungdq3
	 * @since: 6:38:06 PM Mar 9, 2015
	 * @return: String
	 * @throws:
	 * @param staffOwnerId: staff cha
	 * @param parentShopId: shop cha truyen vao
	 * @return
	 * @throws Exception
	 */
	public String getListStaffManagerOfChildShop(String staffOwnerId, String parentShopId) throws Exception {
		StringBuilder result = new StringBuilder();
		ArrayList<String> listShopChild = getListShopChild(parentShopId);
		if (listShopChild != null && listShopChild.size() > 0) {
//			ArrayList<String> listStaff = getListStaffInParentStaffMap(
//					staffOwnerId, shopId);
//			if (listStaff == null || listStaff.size() == 0) {
//				listStaff = getListStaffInShop(shopId, staffOwnerId);
//			}
//			String res1 = TextUtils.join(",", listStaff);
//
//			if (!StringUtil.isNullOrEmpty(res1)) {
//				if (StringUtil.isNullOrEmpty(result.toString())) {
//					result.append(res1);
//				} else {
//					result.append(",");
//					result.append(res1);
//				}
//			}

			for (String str : listShopChild) {
				String res = Constants.STR_BLANK;
				res = getListStaffManagerOfShop(staffOwnerId, str);
				if (!StringUtil.isNullOrEmpty(res)) {
					if (StringUtil.isNullOrEmpty(result.toString())) {
						result.append(res);
					} else {
						result.append(",");
						result.append(res);
					}
				}
//				params.clear();
//				ArrayList<String> listStaffId = new ArrayList<String>();
//				try {
//					StringBuffer  varname1 = new StringBuffer();
//					varname1.append("SELECT DISTINCT ru.user_id AS STAFF_ID ");
//					varname1.append("FROM   role_user ru, ");
//					varname1.append("       role_permission_map rpm, ");
//					varname1.append("       org_access oa ");
//					varname1.append("WHERE  1 = 1 ");
//					varname1.append("       AND oa.status = 1 ");
//					varname1.append("       AND rpm.status = 1 ");
//					varname1.append("       AND ru.status = 1 ");
//					varname1.append("       AND ru.role_id = rpm.role_id ");
//					varname1.append("       AND rpm.permission_id = oa.permission_id ");
//					varname1.append("       AND oa.shop_id = ? ");
//					params.add(str);
//					varname1.append("       AND NOT EXISTS (SELECT 1 ");
//					varname1.append("                       FROM   organization_except oe ");
//					varname1.append("                       WHERE  oe.org_id = (SELECT organization_id ");
//					varname1.append("                                           FROM   staff ");
//					varname1.append("                                           WHERE  staff_id = ?) ");
//					params.add(staffOwnerId);
//					varname1.append("                              AND org_except_id = (SELECT organization_id ");
//					varname1.append("                                                   FROM   staff ");
//					varname1.append("                                                   WHERE  staff_id = ru.user_id) ");
//					varname1.append("                      			AND oe.status = 1 ");
//					varname1.append("                      )");
//
//					c = rawQueries(varname1.toString(), params);
//					if (c != null) {
//						if (c.moveToFirst()) {
//							do {
//								String temp = StringUtil.getStringFromCursor(c, STAFF_ID);
//								listStaffId.add(temp);
//							} while (c.moveToNext());
//						}
//					}
//				} finally {
//					try {
//						if (c != null) {
//							c.close();
//						}
//					} catch (Exception e) {
//					}
//				}
//				String res = Constants.STR_BLANK;
//				res = TextUtils.join(",", listStaffId);
//				if (listStaffId == null || listStaffId.size() <= 0) {
//					ArrayList<String> listStaffID = getListStaffInParentStaffMap(
//							staffOwnerId, str);
//					if (listStaffID == null || listStaffID.size() == 0) {
//						listStaffID = getListStaffInShop(str, staffOwnerId);
//					}
//					res = TextUtils.join(",", listStaffID);
//				}
//
//				if (!StringUtil.isNullOrEmpty(res)) {
//					if (StringUtil.isNullOrEmpty(result.toString())) {
//						result.append(res);
//					} else {
//						result.append(",");
//						result.append(res);
//					}
//				}
			}
		} else {
			result.append(getListStaffOfSupervisor(staffOwnerId, parentShopId));
		}

		return result.toString();
	}

	/**
	 * Lay danh sach nhan vien con duoi quyen quan ly nhan vien cha
	 * KO quan tam loai nv
	 *
	 * @author: dungdq3
	 * @since: 9:29:21 AM Mar 5, 2015
	 * @return: String
	 * @throws:
	 * @param staffOwnerId
	 * @param shopId
	 * @throws Exception
	 */
	public String getListStaffOfSupervisor(String staffOwnerId, String shopId) throws Exception {
		SHOP_TABLE sh = new SHOP_TABLE(mDB);
		String shopType = sh.getShopType(shopId);
		String result = Constants.STR_BLANK;
		if (StringUtil.isNullOrEmpty(shopType)) { // ko phai npp
//			ArrayList<String> listShopChild = getListShopChild(shopId);
			ArrayList<String> listStaff = new ArrayList<String>();
//			for(String shop : listShopChild) {
				// duyet qua tung shop con, lay danh sach nhan vien tuong ung
				ArrayList<String> listStaffTemp = getListStaffInParentStaffMap(
						staffOwnerId, shopId);
				if (listStaffTemp == null || listStaffTemp.size() == 0) {
					listStaffTemp = getListStaffInShop(shopId, staffOwnerId);
				}
				if(listStaffTemp != null) {
					listStaff.addAll(listStaffTemp);
				}
//			}
			if (listStaff != null && listStaff.size() > 0) {
				result = TextUtils.join(",", listStaff);
			}
		} else if (!StringUtil.isNullOrEmpty(shopType)) {
			ArrayList<String> listStaff = getListStaffInParentStaffMap(
					staffOwnerId, shopId);
			if (listStaff == null || listStaff.size() == 0) {
				listStaff = getListStaffInShop(shopId, staffOwnerId);
			}
			int staffType = getStaffType(staffOwnerId);
			if (staffType == UserDTO.TYPE_STAFF) {
				if (listStaff == null)
					listStaff = new ArrayList<String>();
				listStaff.add(staffOwnerId);
			}
			if (listStaff != null && listStaff.size() > 0) {
				result = TextUtils.join(",", listStaff);
			}
		}
		return result;
	}

	/**
	 * lay danh sach nhan vien trong parent staff map
	 *
	 * @author: dungdq3
	 * @since: 3:02:32 PM Mar 10, 2015
	 * @return: String
	 * @throws:
	 * @param parentStaffID
	 * @return
	 */
	public ArrayList<String> getListStaffInParentStaffMap(String parentStaffID, String shopID) {
		ArrayList<String> listStaffId = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer  varname1 = new StringBuffer();
			varname1.append(" SELECT distinct (st.staff_id) ");
			varname1.append(" FROM   parent_staff_map psm, ");
			varname1.append("       staff st ");
			varname1.append(" WHERE  psm.status = 1 ");
			varname1.append("       AND psm.parent_staff_id = ? ");
			params.add(parentStaffID);
			varname1.append("       AND st.status = 1 ");
			varname1.append("       AND st.staff_id = psm.staff_id ");
			varname1.append(" AND ");
			varname1.append(" st.shop_id = ? ");
			params.add(shopID);
			varname1.append(" AND ");
			varname1.append(" NOT EXISTS ");
			varname1.append(" ( ");
			varname1.append("       SELECT * ");
			varname1.append("       FROM   exception_user_access eua ");
			varname1.append("       WHERE  eua.status = 1 ");
			varname1.append("       AND    eua.[staff_id] = st.staff_id ");
			varname1.append("       AND    eua.parent_staff_id = psm.parent_staff_id)");

			varname1.append(" AND NOT EXISTS (SELECT 1 ");
			varname1.append("                FROM   organization_except oe ");
			varname1.append("                WHERE  oe.org_id = (SELECT organization_id ");
			varname1.append("                                    FROM   staff s1 ");
			varname1.append("                                    WHERE  s1.staff_id = ?) ");
			params.add(parentStaffID);
			varname1.append("                              AND org_except_id = st.organization_id ");
			varname1.append("                      			AND oe.status = 1 ");
			varname1.append("                      )");

			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String temp = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(temp);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return listStaffId;
	}

	/**
	 * lay danh sach nv theo shop truyen vao
	 *
	 * @author: dungdq3
	 * @since: 3:09:35 PM Mar 10, 2015
	 * @return: String
	 * @throws:
	 * @param shopID
	 * @return
	 */
	public ArrayList<String> getListStaffInShop(String shopID, String parentStaff) {
		ArrayList<String> listStaffId = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer  varname1 = new StringBuffer();
			varname1.append("SELECT distinct (st.STAFF_ID) ");
			varname1.append("FROM   staff st ");
			varname1.append("WHERE  st.status = 1 ");
			varname1.append("       AND st.shop_id = ? ");
			params.add(shopID);
			varname1.append("       AND st.staff_id <> ? ");
			params.add(parentStaff);
			varname1.append(" and NOT EXISTS ");
			varname1.append("( ");
			varname1.append("       SELECT * ");
			varname1.append("       FROM   exception_user_access eua ");
			varname1.append("       WHERE  eua.status = 1 ");
			varname1.append("       AND    eua.[staff_id] = st.staff_id ");
			varname1.append("       AND    eua.parent_staff_id = ?)");
			params.add(parentStaff);

			varname1.append(" AND NOT EXISTS (SELECT 1 ");
			varname1.append("                FROM   organization_except oe ");
			varname1.append("                WHERE  oe.org_id = (SELECT organization_id ");
			varname1.append("                                    FROM   staff s1 ");
			varname1.append("                                    WHERE  s1.staff_id = ?) ");
			params.add(parentStaff);
			varname1.append("                              AND org_except_id = st.organization_id  ");
			varname1.append("                      			AND oe.status = 1 ");
			varname1.append("                      )");

			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String temp = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(temp);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return listStaffId;
	}

	/**
	 * Lay danh sach shop va nv
	 * @author: hoanpd1
	 * @since: 18:35:27 12-03-2015
	 * @return: TBHVAttendanceDTO
	 * @throws:
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public ShopAndStaffDTO getListShopAndStaff(Bundle bundle) throws Exception {
		ShopAndStaffDTO dto = new ShopAndStaffDTO();
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		// lay ds nhung nhan vien o duoi staffId truyen vao mot cap
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		String listGs = staffTable.getListStaffOfSupervisor(staffId, shopId);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		// lay shop con
		ArrayList<String> shopListChild = shopTable.getShopRecursiveReverse(shopId);
		String shopChildStr = TextUtils.join(",", shopListChild);
		StringBuffer  sql = new StringBuffer();
		sql.append(" SELECT * ");
		sql.append(" FROM (SELECT S.STAFF_ID           AS STAFF_ID, ");
		sql.append("              S.STAFF_CODE         AS STAFF_CODE, ");
		sql.append("              S.STAFF_NAME         AS STAFF_NAME, ");
		sql.append("              SH.SHOP_ID           AS SHOP_ID, ");
		sql.append("              SH.SHOP_CODE         AS SHOP_CODE, ");
		sql.append("              SH.SHOP_NAME         AS SHOP_NAME, ");
		sql.append("              SH.LAT               AS SHOP_LAT, ");
		sql.append("              SH.LNG               AS SHOP_LNG, ");
		sql.append("              GS_CH.OBJECT_TYPE    AS GS_OBJECT_TYPE ");
		sql.append("       FROM STAFF S, ");
		sql.append("            SHOP SH, ");
		sql.append("            CHANNEL_TYPE GS_CH ");
		sql.append("       WHERE S.STAFF_TYPE_ID = GS_CH.CHANNEL_TYPE_ID ");
		sql.append("             AND GS_CH.TYPE = 2 ");
		sql.append("             AND S.STATUS = 1 ");
		sql.append("             AND SH.STATUS = 1 ");
		sql.append("             AND SH.SHOP_ID IN (" + shopChildStr + ") ");
		sql.append("             AND S.STAFF_ID IN ( "+ listGs + ") ");
		sql.append("       GROUP BY SH.SHOP_ID, ");
		sql.append("                S.STAFF_ID ");
		sql.append("       ORDER  BY SH.SHOP_CODE, ");
		sql.append("                 S.STAFF_NAME) NPP_LIST");
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), null);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ShopAndStaffDTO.ShopAndStaffItem item = dto.newShopAndStaffItem();
						item.initObjectWithCursor(c);
						dto.listShopStaff.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}

		return dto;
	}

	/**
	 * lay danh sach shop con
	 *
	 * @author: dungdq3
	 * @since: 8:12:40 AM Mar 17, 2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @param shopID
	 * @return
	 */
	public ArrayList<String> getListShopChild(String shopID) {
		ArrayList<String> listStaffId = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		String roleID = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();
		Cursor c = null;
		try {
			StringBuffer  varname1 = new StringBuffer();
			varname1.append("SELECT s.shop_id SHOP_ID ");
			varname1.append("FROM   shop s ");
			varname1.append("WHERE  s.status = 1 ");
			varname1.append("       AND s.parent_shop_id = ? ");
			params.add(shopID);
			varname1.append("       AND NOT EXISTS (SELECT 1 ");
			varname1.append("                       FROM   org_access_except oae, ");
			varname1.append("                              role_permission_map rpm ");
			varname1.append("                       WHERE  1 = 1 ");
			varname1.append("                              AND oae.status = 1 ");
			varname1.append("                              AND rpm.status = 1 ");
			varname1.append("                              AND rpm.role_id = ? ");
			varname1.append("                              AND oae.shop_id = s.shop_id ");
			varname1.append("                              AND rpm.permission_id = oae.permission_id)");
			params.add(roleID);

			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String temp = CursorUtil.getString(c, "SHOP_ID");
						listStaffId.add(temp);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return listStaffId;
	}

	/**
	 *
	 * @author: hoanpd1
	 * @since: 16:45:53 14-04-2015
	 * @return: ArrayList<StaffItemDTO>
	 * @throws:
	 * @param shopId
	 * @param staffOwnerId
	 * @param listStaffId
	 * @param isSatffOwnerId : co lay ca staff Owner id hay ko
	 * @return
	 * @throws Exception
	 */
	public ArrayList<StaffItemDTO> getListStaff(String shopId, String staffOwnerId, String listStaffId, boolean isSatffOwnerId) throws Exception{
		ArrayList<StaffItemDTO> listStaff = new ArrayList<StaffItemDTO>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		StringBuffer varname1 = new StringBuffer();
		varname1.append(" SELECT st.staff_id            AS STAFF_ID, ");
		varname1.append("     	 st.staff_code			AS STAFF_CODE, ");
		varname1.append("     	 st.staff_name			AS STAFF_NAME, ");
		varname1.append("     	 s.specific_type        AS SPECIFIC_TYPE");
		varname1.append(" FROM   staff st, STAFF_TYPE S  ");
		varname1.append(" WHERE  st.status = 1 ");
		varname1.append("       AND st.staff_type_id = s.staff_type_id ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND st.staff_id in ("+ listStaffId +") ");
//		if(!isSatffOwnerId){
//			varname1.append("       AND st.staff_id <> ? ");
//			params.add(staffOwnerId);
//			varname1.append("       AND s.SPECIFIC_TYPE = ? ");
//			params.add(String.valueOf(UserDTO.TYPE_STAFF));
//			varname1.append("       AND st.shop_id = ? ");
//			params.add(shopId);
//		}
		varname1.append(" ORDER BY  SPECIFIC_TYPE DESC, STAFF_CODE, STAFF_NAME ");
		try {
    		c = rawQueries(varname1.toString(), params);
    		if (c != null) {
    			if (c.moveToFirst()) {
    				do {
    					StaffItemDTO item = new ListComboboxShopStaffDTO().newStaffItem();
    					item.initObjectWithCursor(c);
    					listStaff.add(item);
    				} while (c.moveToNext());
    			}
    		}
		} finally {
        	try {
        		if (c != null) {
        			c.close();
        		}
        	} catch (Exception e) {
        	}
        }

        return listStaff;
	}

	 /**
	 * Lay ds shop con cua quan li
	 * @author: Tuanlt11
	 * @param staffOwnerId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	*/
	public String getListStaffManagerOfShop(String staffOwnerId, String shopId) throws Exception {
		ArrayList<String> params = new ArrayList<String>();
		StringBuilder result = new StringBuilder();
		Cursor c = null;
		ArrayList<String> listStaffId = new ArrayList<String>();
		try {
			StringBuffer varname1 = new StringBuffer();
			varname1.append("SELECT DISTINCT ru.user_id AS STAFF_ID ");
			varname1.append("FROM   role_user ru, ");
			varname1.append("       role_permission_map rpm, ");
			varname1.append("       org_access oa ");
			varname1.append("WHERE  1 = 1 ");
			varname1.append("       AND oa.status = 1 ");
			varname1.append("       AND rpm.status = 1 ");
			varname1.append("       AND ru.status = 1 ");
			varname1.append("       AND ru.role_id = rpm.role_id ");
//			varname1.append("       AND ru.role_id = ? ");
//			params.add(GlobalInfo.getInstance().getProfile().getUserData().roleId);
			varname1.append("       AND rpm.permission_id = oa.permission_id ");
			varname1.append("       AND oa.shop_id = ? ");
			params.add(shopId);
			varname1.append("       AND NOT EXISTS (SELECT 1 ");
			varname1.append("                       FROM   organization_except oe ");
			varname1.append("                       WHERE  oe.org_id = (SELECT organization_id ");
			varname1.append("                                           FROM   staff ");
			varname1.append("                                           WHERE  staff_id = ?) ");
			params.add(staffOwnerId);
			varname1.append("                              AND org_except_id = (SELECT organization_id ");
			varname1.append("                                                   FROM   staff ");
			varname1.append("                                                   WHERE  staff_id = ru.user_id) ");
			varname1.append("                      			AND oe.status = 1 ");
			varname1.append("                      )");

			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String temp = CursorUtil.getString(c,
								STAFF_ID);
						listStaffId.add(temp);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		String res = Constants.STR_BLANK;
		res = TextUtils.join(",", listStaffId);
		if (listStaffId == null || listStaffId.size() <= 0) {
			ArrayList<String> listStaffID = getListStaffInParentStaffMap(
					staffOwnerId, shopId);
			if (listStaffID == null || listStaffID.size() == 0) {
				listStaffID = getListStaffInShop(shopId, staffOwnerId);
			}
			res = TextUtils.join(",", listStaffID);
		}

		if (!StringUtil.isNullOrEmpty(res)) {
			if (StringUtil.isNullOrEmpty(result.toString())) {
				result.append(res);
			} else {
				result.append(",");
				result.append(res);
			}
		}

		return result.toString();
	}

	/**
	 * Danh sach id nhan vien BH
	 * @author: hoanpd1
	 * @since: 10:54:26 10-04-2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @return
	 */
	public String getListStaffSaleId (String shopId, String staffOwnerId) throws Exception{
		String listStaffSaleId = Constants.STR_BLANK;
		String listStaff = getListStaffOfSupervisor(staffOwnerId, shopId);
		ArrayList<String> listStaffId = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		StringBuffer varname1 = new StringBuffer();
		varname1.append(" SELECT st.staff_id            AS STAFF_ID ");
		varname1.append(" FROM   staff st, STAFF_TYPE S  ");
		varname1.append(" WHERE  st.status = 1 ");
		varname1.append("       AND st.staff_type_id = s.staff_type_id ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND st.staff_id in ("+ listStaff +") ");
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			varname1.append("       AND st.staff_id = ? ");
			params.add(staffOwnerId);
		}else {
			varname1.append("       AND st.staff_id <> ? ");
			params.add(staffOwnerId);
		}
		varname1.append("       AND st.shop_id = ? ");
		params.add(shopId);
		varname1.append("       AND s.SPECIFIC_TYPE = ? ");
		params.add(String.valueOf(UserDTO.TYPE_STAFF));
		varname1.append(" ORDER BY  STAFF_CODE, STAFF_NAME ");
		try {
    		c = rawQueries(varname1.toString(), params);
    		if (c != null) {
    			if (c.moveToFirst()) {
    				do {
    					String item = CursorUtil.getString(c, "STAFF_ID");
    					listStaffId.add(item);
    				} while (c.moveToNext());
    			}
    		}
		} finally {
        	try {
        		if (c != null) {
        			c.close();
        		}
        	} catch (Exception e) {
        	}
        }
		if (listStaffId != null && listStaffId.size() > 0) {
			listStaffSaleId = TextUtils.join(",", listStaffId);
		}
		return listStaffSaleId;
	}

	 /**
	 * Lay ds staff cua ds van de
	 * @author: Tuanlt11
	 * @param staffOwnerId
	 * @param shopId
	 * @return
	 * @return: String
	 * @throws:
	*/
	public List<DisplayProgrameItemDTO> getListStaffProblem(Bundle bundle) throws Exception {
		// TODO Auto-generated method stub
		List<DisplayProgrameItemDTO> lstStaffItem = new ArrayList<DisplayProgrameItemDTO>();
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		StringBuffer lstStaff = new StringBuffer();
//		int index = 0;
		for (String shopItem : lstShopReverse) {
			String idStaff = getListStaffOfSupervisor(staffId, shopItem);
			if (!StringUtil.isNullOrEmpty(lstStaff.toString().trim()) && !StringUtil.isNullOrEmpty(idStaff)) {
				lstStaff.append(",");
			}
			lstStaff.append(idStaff);
//			index++;
		}
		if (!StringUtil.isNullOrEmpty(lstStaff.toString().trim())) {
			lstStaff.append(",");
		}
		lstStaff.append(staffId);

		StringBuffer varname1 = new StringBuffer();
		varname1.append(" SELECT st.staff_id            AS STAFF_ID, ");
		varname1.append(" 		 st.staff_name            AS STAFF_NAME, ");
		varname1.append(" 		 st.STAFF_CODE            AS STAFF_CODE ");
		varname1.append(" FROM   staff st ");
		varname1.append(" WHERE  st.status = 1 ");
		varname1.append("       AND st.staff_id in ("+ lstStaff +") ");
		varname1.append(" ORDER BY  STAFF_CODE, STAFF_NAME ");
		Cursor c = null;
		try {
    		c = rawQueries(varname1.toString(), null);
    		if (c != null) {
    			if (c.moveToFirst()) {
    				do {
						DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
						comboboxNVBHDTO.value = CursorUtil.getString(c, STAFF_ID) ;
						comboboxNVBHDTO.name = CursorUtil.getString(c, STAFF_NAME) ;
						comboboxNVBHDTO.fullName = CursorUtil.getString(c,
								STAFF_CODE)
								+ Constants.STR_SUBTRACTION
								+ CursorUtil.getString(c, STAFF_NAME);
						lstStaffItem.add(comboboxNVBHDTO);
    				} while (c.moveToNext());
    			}
    		}
		} finally {
        	try {
        		if (c != null) {
        			c.close();
        		}
        	} catch (Exception e) {
        	}
        }
		return lstStaffItem;
	}


	/**
	 * Lay ds staff cua ds van de
	 *
	 * @author: Tuanlt11
	 * @param staffOwnerId
	 * @param shopId
	 * @return
	 * @return: String
	 * @throws:
	 */
	public ArrayList<StaffFeedbackDTO> getListStaffFeedback(Bundle bundle)
			throws Exception {
		// TODO Auto-generated method stub
		ArrayList<StaffFeedbackDTO> lstStaffItem = new ArrayList<StaffFeedbackDTO>();
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffCode = bundle.getString(IntentConstants.INTENT_STAFF_CODE);
		String orgId = bundle.getString(IntentConstants.INTENT_ORG_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> lstShopReverse = shopTB
				.getShopRecursiveReverse(shopId);
		StringBuffer lstStaff = new StringBuffer();
		for (String shopItem : lstShopReverse) {
			String idStaff = getListStaffOfSupervisor(staffId, shopItem);
			if (!StringUtil.isNullOrEmpty(lstStaff.toString().trim())
					&& !StringUtil.isNullOrEmpty(idStaff)) {
				lstStaff.append(",");
			}
			lstStaff.append(idStaff);
		}
		// add chinh no
		if ( !StringUtil.isNullOrEmpty(lstStaff.toString().trim())) {
			lstStaff.append(",");
		}
		lstStaff.append(staffId);

		StringBuffer varname1 = new StringBuffer();
		varname1.append(" SELECT st.staff_id           AS STAFF_ID, ");
		varname1.append(" 		 st.staff_name         AS STAFF_NAME, ");
		varname1.append(" 		 st.STAFF_CODE         AS STAFF_CODE, ");
		varname1.append(" 		 sh.SHOP_CODE          AS SHOP_CODE, ");
		varname1.append(" 		 sh.SHOP_NAME          AS SHOP_NAME, ");
		varname1.append(" 		 sh.SHOP_ID            AS SHOP_ID, ");
//		varname1.append(" 		 org.node_type_name    AS STAFF_TYPE, ");
		varname1.append(" 		 stt.NAME    AS STAFF_TYPE, ");
		varname1.append(" 		 stt.specific_type    AS SPECIFIC_TYPE ");
		varname1.append(" FROM   staff st, organization org, shop sh, staff_type stt ");
		varname1.append(" WHERE  st.status = 1 ");
		varname1.append("       AND st.staff_id in (" + lstStaff + ") ");
		varname1.append("       AND sh.shop_id = st.shop_id ");
		varname1.append("       AND st.staff_type_id = stt.staff_type_id ");
		varname1.append("       AND st.organization_id  = org.organization_id and st.organization_id is not null ");
		if (!StringUtil.isNullOrEmpty(staffCode)) {
			staffCode = StringUtil.getEngStringFromUnicodeString(staffCode);
			varname1.append("	and (upper(st.NAME_TEXT) like upper(?) escape '^' OR upper(st.STAFF_CODE) like upper(?) escape '^' )");
			param.add("%" + staffCode + "%");
			param.add("%" + staffCode + "%");
		}
		if (!StringUtil.isNullOrEmpty(orgId)) {
			varname1.append("	and org.organization_id = ?");
			param.add(orgId);
		}
		varname1.append(" ORDER BY  STAFF_CODE, STAFF_NAME ");
		Cursor c = null;
		try {
			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						StaffFeedbackDTO dto = new StaffFeedbackDTO();
						dto.initFromCursor(c);
						lstStaffItem.add(dto);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstStaffItem;
	}

	 /**
	 * Lay tat ca loai nhan vien tu shop chon tro xuong
	 * @author: Tuanlt11
	 * @param bundle
	 * @return
	 * @throws Exception
	 * @return: ArrayList<StaffFeedbackDTO>
	 * @throws:
	*/
	public ArrayList<DisplayProgrameItemDTO> getListTypeStaffFeedback(Bundle bundle)
			throws Exception {
		// TODO Auto-generated method stub
		ArrayList<DisplayProgrameItemDTO> lstStaffType = new ArrayList<DisplayProgrameItemDTO>();
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		StringBuffer lstStaff = new StringBuffer();
		for (String shopItem : lstShopReverse) {
			String idStaff = getListStaffOfSupervisor(staffId, shopItem);
			if ( !StringUtil.isNullOrEmpty(lstStaff.toString().trim())
					&& !StringUtil.isNullOrEmpty(idStaff)) {
				lstStaff.append(",");
			}
			lstStaff.append(idStaff);
		}
		if ( !StringUtil.isNullOrEmpty(lstStaff.toString().trim())) {
			lstStaff.append(",");
		}
		lstStaff.append(staffId);

		StringBuffer varname1 = new StringBuffer();
		varname1.append(" SELECT distinct org.organization_id            AS ORGANIZATION_ID, ");
//		varname1.append(" 		 org.node_type_name            AS NODE_TYPE_NAME ");
		varname1.append(" 		 sty.name            AS NODE_TYPE_NAME ");
		varname1.append(" FROM   staff st, organization org, staff_type sty ");
		varname1.append(" WHERE  st.status = 1 ");
		varname1.append("       AND st.staff_id in (" + lstStaff + ") ");
		varname1.append("       AND st.organization_id  = org.organization_id and st.organization_id is not null ");
		varname1.append("       AND org.node_type_id  = sty.staff_type_id ");
		varname1.append(" ORDER BY  STAFF_CODE, STAFF_NAME ");
		Cursor c = null;
		try {
			c = rawQueries(varname1.toString(), null);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO comboboxNVBHDTO = new DisplayProgrameItemDTO();
						comboboxNVBHDTO.value = CursorUtil.getString(c, "ORGANIZATION_ID") ;
						comboboxNVBHDTO.name = CursorUtil.getString(c, "NODE_TYPE_NAME") ;
						comboboxNVBHDTO.fullName = CursorUtil.getString(c, "NODE_TYPE_NAME") ;
						if(!StringUtil.isNullOrEmpty(comboboxNVBHDTO.name))
							lstStaffType.add(comboboxNVBHDTO);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstStaffType;
	}

	private AttendanceDTO getAttendance(String staffId,
			LatLng shopPosition, String shopId) {
		AttendanceDTO result = new AttendanceDTO();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		StringBuilder var1 = new StringBuilder();
		var1.append("SELECT ST.staff_id, ");
		var1.append("       ST.staff_code, ");
		var1.append("       ST.STAFF_NAME, ");
		var1.append("       SPL.lat, ");
		var1.append("       SPL.lng, ");
		var1.append("       Strftime('%Y-%m-%d %H:%M', SPL.create_date) CREATE_DATE ");
		var1.append("FROM   staff ST ");
		var1.append("       LEFT JOIN staff_position_log SPL ");
		var1.append("              ON ST.staff_id = SPL.staff_id ");
		var1.append("WHERE  ST.staff_id = ? ");
		var1.append("       AND substr(SPL.create_date, 1, 10) = substr(?, 1, 10) ");
//		var1.append("       AND DATE(SPL.create_date) = DATE('now', 'localtime') ");
		var1.append("ORDER  BY ST.staff_code, ");
		var1.append("          ST.STAFF_NAME, ");
		var1.append("          SPL.create_date ");
//		var1.append("          limit 1 ");

		String[] params = { staffId, date_now };
//		String[] params = { staffId };

		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						result = new AttendanceDTO();
						result.initWithCursor(c, shopPosition, shopId );
						double cusDistance = -1;
						if (result.position1.lat > 0 && result.position1.lng > 0
								&& shopPosition.lat > 0 && shopPosition.lng > 0) {
							cusDistance = GlobalUtil.getDistanceBetween(
									result.position1, shopPosition);
							result.distance1 = cusDistance;
						}
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return result;
	}
	ArrayList<String> shopIdArray = new ArrayList<String>();
	/**
	 * Danh sach customer cua nhan vien
	 * @author: yennth16
	 * @since: 17:28:54 20-07-2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @param parentShopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getListCustomerId(String shopId, String staffId) throws Exception {
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT * ");
		var1.append("FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");
		var1.append("       DISTINCT kscm.CUSTOMER_ID      CUSTOMER_ID_3 ");
		var1.append("from ks ks ");
		var1.append("JOIN   ks_customer kscm ");
		var1.append("         ON kscm.ks_id = ks.ks_id ");
		var1.append("            AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("            AND kscm.status = 1 ");
		var1.append("       , ks_shop_map ksm ");
		var1.append("where 1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String customerId = Constants.STR_BLANK + CursorUtil.getLong(c, "CUSTOMER_ID");
						shopIdArray.add(customerId);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return shopIdArray;
	}


	 /**
	 * Lay bao cao KPI cua NV
	 * @author: Tuanlt11
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: ReportKPIViewDTO
	 * @throws:
	*/
	public ReportKPIViewDTO getReportKPISale(Bundle b) throws Exception {
		ReportKPIViewDTO dto = new ReportKPIViewDTO();
		Cursor c = null;
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String dateNow = DateUtils.now();
		EXCEPTION_DAY_TABLE edt = new EXCEPTION_DAY_TABLE(mDB);
		long cycleId = edt.getCycleId(new Date(), 0);
		try {
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    kpi.criteria CRITERIA,	");
			sqlObject.append("	    rkd.plan VALUE_DAY_PLAN,	");
			sqlObject.append("	    (CASE WHEN kpi.route_type = 1 THEN rkd.DONE_IR WHEN kpi.route_type = 2 THEN rkd.DONE_OR ELSE rkd.DONE END) VALUE_DAY_DONE,	");
			sqlObject.append("	    rkc.plan VALUE_CYCLE_PLAN,	");
			sqlObject.append("	    (CASE WHEN kpi.route_type = 1 THEN rkc.DONE_IR WHEN kpi.route_type = 2 THEN rkc.DONE_OR ELSE rkc.DONE END) VALUE_CYCLE_DONE,	");
			sqlObject.append("	    kpi.data_type DATA_TYPE	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    kpi	");
			sqlObject.append("	LEFT JOIN	");
			sqlObject.append("	    rpt_kpi_day rkd	");
			sqlObject.append("	        ON kpi.kpi_id = rkd.kpi_id	");
			sqlObject.append("	        AND rkd.staff_id = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	        AND rkd.shop_id = ?	");
			paramsObject.add(shopId);
			sqlObject.append("	        AND Date(rkd.fullDate) = Date(?)	");
			paramsObject.add(dateNow);
			sqlObject.append("	LEFT JOIN	");
			sqlObject.append("	    rpt_kpi_cycle rkc	");
			sqlObject.append("	        ON rkc.kpi_id = kpi.kpi_id	");
			sqlObject.append("	        AND rkc.staff_id = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	        AND rkc.shop_id = ?	");
			paramsObject.add(shopId);
			sqlObject.append("	        AND rkc.cycle_id = ?	");
			paramsObject.add(""+cycleId);
			sqlObject.append("	WHERE	");
			sqlObject.append("	    kpi.status = 1	");
			sqlObject.append("	and  Date(kpi.begin_date) <= dayInOrder(?)	");
			paramsObject.add(dateNow);
			sqlObject.append("	and  (Date(kpi.end_date) >= dayInOrder(?) or kpi.end_date is null )	");
			paramsObject.add(dateNow);
			sqlObject.append("	and  (rkd.rpt_kpi_day_id is not null or rkc.rpt_kpi_cycle_id is not null)	");
			sqlObject.append("	order by   kpi.ORDER_INDEX, kpi.CRITERIA	");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c.moveToFirst()) {
				do {
					ReportKPIItemDTO item = new ReportKPIItemDTO();
					item.parseDataFromCursor(c);
					dto.lstReportKPI.add(item);
				} while (c.moveToNext());
			}

			dto.monthSalePlan = edt.getPlanWorkingDaysOfMonth(new Date(), shopId);
			dto.soldSalePlan = edt.getCurrentWorkingDaysOfMonth(shopId);
			dto.perSalePlan = StringUtil.calPercentUsingRound(dto.monthSalePlan, dto.soldSalePlan);
			// return dto;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Lay bao cao KPI cua GS
	 * @author: Tuanlt11
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: ReportKPIViewDTO
	 * @throws:
	*/
	public ReportKPISupervisorViewDTO getReportKPISupervisor(Bundle b) throws Exception {
		ReportKPISupervisorViewDTO dto = new ReportKPISupervisorViewDTO();
		Cursor c = null;
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String lstStaffId = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		String dateNow = DateUtils.now();
		EXCEPTION_DAY_TABLE edt = new EXCEPTION_DAY_TABLE(mDB);
		long cycleId = edt.getCycleId(new Date(), 0);
		// ngay hoac luy ke
		boolean isDay = b.getBoolean(IntentConstants.INTENT_DAY);
		try {
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    kpi.criteria CRITERIA,	");
			// du lieu ngay
			if(isDay){
				sqlObject.append("	    rkd.plan VALUE_DAY_PLAN,	");
				sqlObject.append("	    (CASE WHEN kpi.route_type = 1 THEN rkd.DONE_IR WHEN kpi.route_type = 2 THEN rkd.DONE_OR ELSE rkd.DONE END) VALUE_DAY_DONE,	");
			}else{
				sqlObject.append("	    rkc.plan VALUE_CYCLE_PLAN,	");
				sqlObject.append("	    (CASE WHEN kpi.route_type = 1 THEN rkc.DONE_IR WHEN kpi.route_type = 2 THEN rkc.DONE_OR ELSE rkc.DONE END) VALUE_CYCLE_DONE,	");
			}
			sqlObject.append("	    kpi.data_type DATA_TYPE,	");
			sqlObject.append("	    st.staff_name STAFF_NAME,	");
			sqlObject.append("	    st.staff_code STAFF_CODE,	");
			sqlObject.append("	    st.staff_id STAFF_ID,	");
			sqlObject.append("	    kpi.kpi_id KPI_ID	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    kpi, staff st	");
			if (isDay) {
				sqlObject.append("	JOIN	");
				sqlObject.append("	    rpt_kpi_day rkd	");
				sqlObject.append("	        ON kpi.kpi_id = rkd.kpi_id	");
				sqlObject.append("	        AND rkd.staff_id In (	");
				sqlObject.append(lstStaffId);
				sqlObject.append("	        )	");
				sqlObject.append("	        AND rkd.shop_id = ?	");
				paramsObject.add(shopId);
				sqlObject.append("	        AND Date(rkd.fullDate) = Date(?)	");
				paramsObject.add(dateNow);
				sqlObject.append("	        AND st.staff_id = rkd.staff_id	");
			} else {
				sqlObject.append("	JOIN	");
				sqlObject.append("	    rpt_kpi_cycle rkc	");
				sqlObject.append("	        ON rkc.kpi_id = kpi.kpi_id	");
				sqlObject.append("	        AND rkc.staff_id In (	");
				sqlObject.append(lstStaffId);
				sqlObject.append("	        )	");
				sqlObject.append("	        AND rkc.shop_id = ?	");
				paramsObject.add(shopId);
				sqlObject.append("	        AND rkc.cycle_id = ?	");
				paramsObject.add("" + cycleId);
				sqlObject.append("	        AND st.staff_id = rkc.staff_id	");
			}
			sqlObject.append("	WHERE	");
			sqlObject.append("	    kpi.status = 1	");
			sqlObject.append("	and  Date(kpi.begin_date) <= dayInOrder(?)	");
			paramsObject.add(dateNow);
			sqlObject.append("	and  (Date(kpi.end_date) >= dayInOrder(?) or kpi.end_date is null )	");
			paramsObject.add(dateNow);
//			sqlObject.append("	and  (rkd.rpt_kpi_day_id is not null or rkc.rpt_kpi_cycle_id is not null)	");
			sqlObject.append("	order by   kpi.ORDER_INDEX, kpi.CRITERIA	");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c.moveToFirst()) {
				do {
					long staffIdTemp = CursorUtil.getLong(c, "STAFF_ID");
					boolean isExist = false;
					int index = 0;
					ReportKPISupervisorItemDTO item = new ReportKPISupervisorItemDTO();
					item.parseDataFromCursor(c);
					for (ReportKPISupervisorItemDTO temp : dto.lstReportKPISuperVisorItem) {
						if (temp.staffId == staffIdTemp) {
							isExist = true;
							break;
						}
						index++;
					}
					// truong hop dto chua add nhan vien vao
					if(!isExist)
						dto.lstReportKPISuperVisorItem.add(item);
					else{
						// nhan vien da ton tai trong mang dto
						item = dto.lstReportKPISuperVisorItem.get(index);
					}
					ReportKPIItemDTO itemSale = new ReportKPIItemDTO();
					itemSale.parseDataFromCursor(c);
					isExist = false;
					for (ReportKPIItemDTO temp : dto.lstHeader) {
						if (itemSale.kpiId == temp.kpiId) {
							isExist = true;
							break;
						}
					}
					// add header
					if(!isExist)
						dto.lstHeader.add(itemSale);
					// add noi dung
					item.lstReportKPI.add(itemSale);
				} while (c.moveToNext());
			}

			dto.monthSalePlan = edt.getPlanWorkingDaysOfMonth(new Date(), shopId);
			dto.soldSalePlan = edt.getCurrentWorkingDaysOfMonth(shopId);
			dto.perSalePlan = StringUtil.calPercentUsingRound(dto.monthSalePlan,dto.soldSalePlan);
			// return dto;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}


}
