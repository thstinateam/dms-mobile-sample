package com.ths.dmscore.view.sale.customer;

import java.util.Vector;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Danh sach gop y
 *
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerFeedBackListView extends BaseFragment implements VinamilkTableListener, OnEventControlListener {

	Button btFeedBack;// btFeedBack
	TextView tvCusCode;// tvCusCode
	TextView tvCusName;// tvCusName
	DMSTableView tbCusFeedBack;// tbCusFeedBack
	private CustomerDTO customer;
	private CustomerFeedBackDto dto = new CustomerFeedBackDto();

	private static final int MENU_MAP = 12;
	private static final int MENU_IMAGE = 13;
	private static final int MENU_INFO_DETAIL = 14;

	public static final int ACTION_OK = 0;
	public static final int ACTION_CANCEL = 1;
	public static final int ACTION_GET_CUSTOMER_LIST = 3;
	public static final int ACTION_CHOOSE_CUSTOMER = 4;
	private boolean isFromGS = false;

	// customer list table
	int[] CUSTOMER_LIST_TABLE_WIDTHS = { 45, 413, 120, 120, 120, 60, 60 };
	String[] CUSTOMER_LIST_TABLE_TITLES = { StringUtil.getString(R.string.TEXT_STT),
			StringUtil.getString(R.string.CONTENT), StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE),
			StringUtil.getString(R.string.TEXT_REMIND_DATE), StringUtil.getString(R.string.TEXT_DONE_DATE), " ", " " };

	public static CustomerFeedBackListView newInstance(Bundle b) {
		CustomerFeedBackListView f = new CustomerFeedBackListView();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		parent = (GlobalBaseActivity) getActivity();
		customer = (CustomerDTO) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER);
		isFromGS =  getArguments().getBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_customer_feed_back_list_fragment, container,
				false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_FEEDBACK));
		if(isFromGS){
			parent.setTitleName(StringUtil.getString(R.string.TEXT_FEEDBACK));
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_GOPY);
		} else {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_GIAOVANDE);
		}

		// init
		btFeedBack = (Button) PriUtils.getInstance().findViewById(v, R.id.btFeedBack, PriHashMap.PriControl.NVBH_GOPY_THEMGOPY);
		PriUtils.getInstance().setOnClickListener(btFeedBack, this);
		if(isFromGS){
			PriUtils.getInstance().setStatus(btFeedBack, PriUtils.INVISIBLE);
		}else{
			PriUtils.getInstance().setStatus(btFeedBack, PriUtils.ENABLE);
		}
		tvCusCode = (TextView) v.findViewById(R.id.tvCusCode);
		tvCusName = (TextView) v.findViewById(R.id.tvCusName);
		tbCusFeedBack = (DMSTableView) v.findViewById(R.id.tbCusFeedBack);
		tbCusFeedBack.setListener(this);
		initHeaderTable();

		if (customer != null) {
			tvCusCode.setText(customer.customerCode);
			tvCusName.setText(customer.customerName);
		}
		getCustomerFeedBack(1, 1);

		return v;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param i
	 * @param j
	 * @return: voidvoid
	 * @throws:
	 */
	private void getCustomerFeedBack(int page, int isGetTotalPage) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_STATE, "");// tat ca
		bundle.putString(IntentConstants.INTENT_TYPE, "");
//		bundle.putString(IntentConstants.INTENT_STAFF_ID,
//				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().id));
		if(isFromGS){
			bundle.putString(IntentConstants.INTENT_STAFF_ID, "");
		}else{
			bundle.putString(IntentConstants.INTENT_STAFF_ID,
					String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		}
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_DONE_DATE, "");
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);

		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.GET_LIST_CUS_FEED_BACK;
		e.sender = this;
		e.viewData = bundle;
		SaleController.getInstance().handleViewEvent(e);

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_CUS_FEED_BACK:
			CustomerFeedBackDto temDto = (CustomerFeedBackDto) (modelEvent.getModelData());
			dto.arrItem = temDto.arrItem;
			if (dto.currentPage <= 0) {
				dto.totalFeedBack = temDto.totalFeedBack;
				dto.typeList = temDto.typeList;
				// dto.gsnppTypeList = temDto.gsnppTypeList;
			}
			renderLayout();
			break;
		case ActionEventConstant.UPDATE_FEEDBACK:
			parent.showDialog(StringUtil.getString(R.string.TEXT_UPDATE_FEEDBACK_SUCC));
			dto.currentPage = -1;
			getCustomerFeedBack(1, 1);
			break;
		case ActionEventConstant.DELETE_FEEDBACK:
			parent.showDialog(StringUtil.getString(R.string.TEXT_DELETE_FEEDBACK_SUCC));
			dto.currentPage = -1;
			getCustomerFeedBack(1, 1);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);

	}

	/**
	 * renderLayout
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void renderLayout() {
		tbCusFeedBack.clearAllData();
		// paging
		if (dto.currentPage <= 0) {
			tbCusFeedBack.setTotalSize(dto.totalFeedBack,1);
		} else {
			tbCusFeedBack.getPagingControl().setCurrentPage(dto.currentPage);
		}
		if (dto.arrItem.size() > 0) {
			int pos = 1 + Constants.NUM_ITEM_PER_PAGE * (tbCusFeedBack.getPagingControl().getCurrentPage() - 1);
			for (int i = 0, s = dto.arrItem.size(); i < s; i++) {
				CustomerFeedBackRow row = new CustomerFeedBackRow(parent, this);
				row.renderLayout(pos, dto.arrItem.get(i), dto);
				pos++;
			}

		} else {
			tbCusFeedBack.showNoContentRow();
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbCusFeedBack) {
			dto.currentPage = tbCusFeedBack.getPagingControl().getCurrentPage();
			getCustomerFeedBack(dto.currentPage, 0);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
		switch (action) {
		case ActionEventConstant.UPDATE_FEEDBACK:
			CheckBox cb = (CheckBox) control;
			if (cb.isChecked()) {
				cb.setEnabled(false);
				FeedBackDTO item = (FeedBackDTO) data;
				item.setDoneDate(DateUtils.now());
				item.setUpdateDate(DateUtils.now());
				item.setUserUpdate(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
				item.setStatus(FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE);
				updateFeedbackRow(item);
			}
			break;
		case ActionEventConstant.DELETE_FEEDBACK:
			control.setEnabled(false);
			FeedBackDTO item = (FeedBackDTO) data;
			Vector<Object> vt = new Vector<Object>();
			vt.add(item);
			vt.add(control);
			GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this, parent,
					StringUtil.getString(R.string.TEXT_QUESTION_DELETE), StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					ACTION_OK, StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, vt, false, false);
			break;

		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btFeedBack) {
			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
			b.putInt(IntentConstants.INTENT_FROM, PostFeedbackView.FROM_FEEDBACK_LIST_VIEW);
			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.POST_FEEDBACK;
			e.sender = this;
			e.viewData = b;
			SaleController.getInstance().handleSwitchFragment(e);
		} else {
			super.onClick(v);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case MENU_INFO_DETAIL:
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerInfo();
			break;
		case MENU_MAP:
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerLocation();
			break;
		case MENU_IMAGE:
			GlobalUtil.popBackStack(this.getActivity());
			gotoListAlbumUser(customer);
			break;
		case ACTION_OK:
			Vector<Object> vt;
			View v;
			vt = (Vector<Object>) data;
			FeedBackDTO item = (FeedBackDTO) vt.elementAt(0);
			v = (View) vt.elementAt(1);
			item.setStatus(0);
			item.setUpdateDate(DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW));
			item.setUserUpdate(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
			parent.showLoadingDialog();
			ActionEvent e = new ActionEvent();
			e.sender = this;
			e.action = ActionEventConstant.DELETE_FEEDBACK;
			e.viewData = item;
			e.userData = v;
			e.isNeedCheckTimeServer = false;
			SaleController.getInstance().handleViewEvent(e);
			break;
		case ACTION_CANCEL:
			vt = (Vector<Object>) data;
			v = (View) vt.elementAt(1);
			v.setEnabled(true);
			break;

		default:
			break;
		}
		super.onEvent(eventType, control, data);
	}

	/**
	 * di toi man hinh cap nhat vi tri khach hang
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoCustomerLocation() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		e.viewData = bundle;
		e.action = ActionEventConstant.GOTO_CUSTOMER_LOCATION;
		UserController.getInstance().handleSwitchFragment(e);
	}

	/**
	 *
	 * Qua man hinh chi tiet co ban khach hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param
	 * @return: void
	 * @throws:
	 */
	public void gotoCustomerInfo() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, String.valueOf(customer.customerId));
		bunde.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		e.viewData = bunde;
		e.action = ActionEventConstant.GO_TO_CUSTOMER_INFO;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * updateFeedbackRow
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void updateFeedbackRow(FeedBackDTO item) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.action = ActionEventConstant.UPDATE_FEEDBACK;
		e.viewData = item;
		e.isNeedCheckTimeServer = false;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * Toi man hinh ds album cua kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customer
	 * @return: void
	 * @throws:
	 */

	private void gotoListAlbumUser(CustomerDTO customer) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = bundle;
		e.action = ActionEventConstant.GO_TO_LIST_ALBUM_USER;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// cau request du lieu man hinh
				// isUpdateData = true;
				// resetAllValue();
				 getCustomerFeedBack(1, 1);
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	 /**
	 * Khoi tao header cho table
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void initHeaderTable(){
		CustomerFeedBackRow header = new CustomerFeedBackRow(parent, this);
		tbCusFeedBack.addHeader(header);
	}
}
