/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * DTO kiem ke
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:27:41 08-12-2014
 */
public class EquipPeriodDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	// id dot kiem ke
	public long equipPeriodId;
	// ma dot kiem ke
	public String code;
	// ten dot kiem ke
	public String name;
	// ngay bat dau
	public String fromDate;
	// ngay ket thuc
	public String toDate;
	// status inventory
	public boolean status;

	// danh sach thiet bi can duoc kiem ke trong dot
	public ArrayList<EquipInventoryDTO> listEquipInventory = new ArrayList<EquipInventoryDTO>();

	public EquipPeriodDTO() {
		equipPeriodId = 0;
		code = "";
		name = "";
		fromDate = "";
		toDate = "";
		status = false;
	}

	/**
	 * init du lieu dot kiem ke
	 * 
	 * @author: hoanpd1
	 * @since: 09:59:15 16-12-2014
	 * @return: void
	 * @throws:
	 * @param c
	 * @throws Exception
	 */
	public void initPeriodInventoryInfo(Cursor c) throws Exception {
		equipPeriodId = CursorUtil.getInt(c, "ID_RECORD_INVEN");
		code = CursorUtil.getString(c, "CODE_RECORD_INVEN");
		name = CursorUtil.getString(c, "NAME_RECORD_INVEN");
		fromDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "FROM_DATE"));
		toDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "TO_DATE"));
	}

}
