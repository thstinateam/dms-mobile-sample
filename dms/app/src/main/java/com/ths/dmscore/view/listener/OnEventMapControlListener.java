/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/

package com.ths.dmscore.view.listener;

import android.view.View;


/**
 * Listener
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public interface OnEventMapControlListener{
	View onEventMap( int eventType, View control, Object data); 
}
