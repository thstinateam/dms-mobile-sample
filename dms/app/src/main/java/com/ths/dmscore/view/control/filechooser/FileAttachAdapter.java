/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ths.dmscore.dto.view.filechooser.FileAttachEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;

public class FileAttachAdapter extends ArrayAdapter<FileInfo> {
        List<FileInfo> objects;
        FileAttachEvent fileAttachEvent;
        boolean isCanRemoveFile;

        public FileAttachAdapter(Context context, List<FileInfo> objects, FileAttachEvent fileAttachEvent, boolean isCanRemoveFile) {
            super(context, -1, objects);
            this.objects = objects;
            this.fileAttachEvent = fileAttachEvent;
            this.isCanRemoveFile = isCanRemoveFile;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FileAttachViewHolder viewHolder;
            if (convertView == null){
                viewHolder = new FileAttachViewHolder(getContext(), parent, this.fileAttachEvent, isCanRemoveFile);
                convertView = viewHolder.view;
                convertView.setTag(viewHolder);
            } else{
                viewHolder = (FileAttachViewHolder) convertView.getTag();
            }
            viewHolder.render(objects.get(position), position);
            return convertView;
        }

		public void setIsRemoveFile(boolean isCanRemoveFile) {
			this.isCanRemoveFile = isCanRemoveFile;
		}
    }