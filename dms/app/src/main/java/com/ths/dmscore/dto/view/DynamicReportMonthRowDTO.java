package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.RptKPIInMonthDTO;

/**
 * dto theo thang cho man hinh bao cao
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class DynamicReportMonthRowDTO extends AbstractDynamicReportRowDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	// mang ds cac column ngang
	public ArrayList<RptKPIInMonthDTO> lstColumn;
	
	public DynamicReportMonthRowDTO() {
		super();
		lstColumn = new ArrayList<RptKPIInMonthDTO>();
	}

	@Override
	public Object clone() {
		// TODO Auto-generated method stub
		ArrayList<RptKPIInMonthDTO> lstColumnClone = new ArrayList<RptKPIInMonthDTO>();
		for (RptKPIInMonthDTO month : lstColumn) {
			lstColumnClone.add(month.clone());
		}
		return lstColumnClone;
	}
}
