package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dms.R;

public class VisitDTO {
	public int totalList;	
	public String totalofTotal;
	
	public ArrayList<VisitItem> arrList;
	
	public VisitDTO(){
		arrList = new ArrayList<VisitDTO.VisitItem>();		
	}
	
	public void addItem(Cursor c){
		VisitItem item = new VisitItem();		
		try {
//			item.thoiGian = StringUtil.dateToString(StringUtil.stringToDate(c.getString(c, "THOI_GIAN")), "yyyy-MM-dd HH:mm:ss"), "dd/MM/yyyy HH:mm");
			item.thoiGian = CursorUtil.getString(c, "THOI_GIAN");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		item.trangThai = CursorUtil.getString(c, "TRANG_THAI");	
		if("0".equals(item.trangThai)){
			item.trangThai = StringUtil.getString(R.string.TEXT_VISIT_NOT_HAVE_ORDER);
		}else if("1".equals(item.trangThai)){
			item.trangThai = StringUtil.getString(R.string.TEXT_VISIT_CLOSED_DOOR);
		}
		arrList.add(item);
	}
	
	public class VisitItem{
		public String stt;
		public String thoiGian;	
		public String trangThai;		
		
		public VisitItem(){
			stt = "0/0";
			thoiGian = "3/7";
			trangThai = "0/0";					
		}
	}
}
