package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.ShopDTO;

/**
 * DTO cua man hinh bao cao chuong trinh trung bay
 * @author hieunq1
 *
 */
public class SupervisorReportDisplayProgressDTO {
	//danh sach item
	public ArrayList<SupervisorReportDisplayProgressItem> arrList;
	//danh sach level code
	public ArrayList<String> arrLevelCode;
	//tong ket qua
	public SupervisorReportDisplayProgressItem totalItem;
	public double standardProgress;
	public ArrayList<ShopDTO> lstShop;
	
	public SupervisorReportDisplayProgressDTO(){
		arrList = new ArrayList<SupervisorReportDisplayProgressItem>();
		arrLevelCode = new ArrayList<String>();
		totalItem = new SupervisorReportDisplayProgressItem();
	}
	
	/**
	 * add item
	 * @param c
	 */
	public void addItem(Cursor c) {
		arrList.add(new SupervisorReportDisplayProgressItem(c));
	}

}
