package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cho man hinh bao cao KPI
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPIViewDTO {
	public ArrayList<ReportKPIItemDTO> lstReportKPI = new ArrayList<ReportKPIItemDTO>();
	// ngay lam viec trong thang
	public int monthSalePlan;
	// ngay da lam viec trong thang
	public int soldSalePlan;
	// percent ngay lam viec trong thang
	public double perSalePlan;

}
