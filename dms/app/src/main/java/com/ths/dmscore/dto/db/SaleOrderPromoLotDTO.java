package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_PROMO_LOT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_LOT_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Chi tiet km cua sale_order_promo_log
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class SaleOrderPromoLotDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -6277057207552893358L;
	// id
	public long saleOrderPromoLotId;
	public long saleOrderId;
	public long saleOrderDetailId;
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public int programType;
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public String programCode;
	public String programName;
	// % chiet khau
	public double discountPercent;
	// So tien chiet khau
	public double discountAmount;
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public int isOwner;
	public long staffId;
	//su dung cho po
	public long poCustomerPromoLotId;
	public long poDetailId;
	public long poId;
	public long poCustomerLotId;
	public String programTypeCode;
	public String shopId;
	public String orderDate;
	public long saleOrderLotId;

	public SaleOrderPromoLotDTO() {
		super(TableType.SALE_ORDER_PROMO_LOT_TABLE);
	}

	public SaleOrderPromoLotDTO(SaleOrderPromoDetailDTO item, long saleOrderPromoLotId, long poCustomerPromoLotId, long saleOrderLotId, long poCustomerLotId) {
		this.saleOrderPromoLotId = saleOrderPromoLotId;
		saleOrderId = item.saleOrderId;
		saleOrderDetailId = item.saleOrderDetailId;
		programType = item.programType;
		programCode = item.programCode;
		programName = item.programName;
		discountAmount = item.discountAmount;
		discountPercent = item.discountPercent;
		isOwner = item.isOwner;
		staffId = item.staffId;
		this.poCustomerPromoLotId = poCustomerPromoLotId;
		this.poDetailId = item.poDetailId;
		this.poId = item.poId;
		this.programTypeCode = item.programTypeCode;
		this.shopId = item.shopId;
		this.orderDate = item.orderDate;
		this.saleOrderLotId = saleOrderLotId;
		this.poCustomerLotId = poCustomerLotId;
	}
    /**
	 * gen Json cho sale order promo lot
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateJsonSaleOrderPromoLot() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_PROMO_LOT_TABLE.TABLE_NAME);
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_PROMO_LOT_ID, saleOrderPromoLotId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_ID, this.saleOrderId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_DETAIL_ID, this.saleOrderDetailId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.PROGRAM_CODE, this.programCode, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.PROGRAM_TYPE, this.programType, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.PROGRAME_TYPE_CODE, this.programTypeCode, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.IS_OWNER, this.isOwner, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_LOT_ID, this.saleOrderLotId, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * gen Json cho poCustomer promo lot
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateJsonPOCustomerPromoLot() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_PROMO_LOT_TABLE.TABLE_NAME);
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PO_CUSTOMER_PROMO_LOT_ID, poCustomerPromoLotId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PO_CUSTOMER_ID, this.poId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PO_CUSTOMER_DETAIL_ID, this.poDetailId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PROGRAM_CODE, this.programCode, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PROGRAM_TYPE, this.programType, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PROGRAME_TYPE_CODE, this.programTypeCode, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.IS_OWNER, this.isOwner, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_LOT_TABLE.PO_CUSTOMER_LOT_ID, this.poCustomerLotId, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	 /**
	 * parse du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		saleOrderPromoLotId = CursorUtil.getLong(c, SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_PROMO_LOT_ID);
		saleOrderId = CursorUtil.getLong(c, SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_ID);
		saleOrderDetailId = CursorUtil.getLong(c, SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_DETAIL_ID);
		shopId = CursorUtil.getString(c, SALE_ORDER_PROMO_LOT_TABLE.SHOP_ID);
		staffId = CursorUtil.getLong(c, SALE_ORDER_PROMO_LOT_TABLE.STAFF_ID);
		orderDate = CursorUtil.getString(c, SALE_ORDER_PROMO_LOT_TABLE.ORDER_DATE);
		programCode = CursorUtil.getString(c, SALE_ORDER_PROMO_LOT_TABLE.PROGRAM_CODE);
		programType = CursorUtil.getInt(c, SALE_ORDER_PROMO_LOT_TABLE.PROGRAM_TYPE);
		programTypeCode = CursorUtil.getString(c, SALE_ORDER_PROMO_LOT_TABLE.PROGRAME_TYPE_CODE);
		discountPercent = CursorUtil.getDouble(c, SALE_ORDER_PROMO_LOT_TABLE.DISCOUNT_PERCENT);
		discountAmount = CursorUtil.getDouble(c, SALE_ORDER_PROMO_LOT_TABLE.DISCOUNT_AMOUNT);
		saleOrderLotId = CursorUtil.getLong(c, SALE_ORDER_PROMO_LOT_TABLE.SALE_ORDER_LOT_ID);
		isOwner = CursorUtil.getInt(c, SALE_ORDER_PROMO_LOT_TABLE.IS_OWNER);
	}

}
