/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.view.DisplayProgrameModel;
import com.ths.dmscore.dto.view.TBHVDisplayProgrameModel;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;

/**
 * DISPLAY_PROGRAME - Chuong trinh trung bay
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class DISPLAY_PROGRAME_TABLE extends ABSTRACT_TABLE {
	// id cua bang
	public static final String DISPLAY_PROGRAM_ID = "DISPLAY_PROGRAM_ID";
	// ma CTTB
	public static final String DISPLAY_PROGRAM_CODE = "DISPLAY_PROGRAM_CODE";
	// ten CTTB
	public static final String DISPLAY_PROGRAM_NAME = "DISPLAY_PROGRAM_NAME";
	// ma ngan CTTB
	public static final String DP_SHORT_CODE = "DP_SHORT_CODE";
	// ten ngan CTTB
	public static final String DP_SHORT_NAME = "DP_SHORT_NAME";
	// trang thai
	public static final String STATUS = "STATUS";
	// tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// den ngay
	public static final String TO_DATE = "TO_DATE";
	// truong nay chua dung
	public static final String OBJTARGET = "OBJTARGET";
	// quan he: 1: OR, 0 : AND
	public static final String RELATION = "RELATION";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay update
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// ma nganh hang
	public static final String CAT = "CAT";
	// % ti le fino
	public static final String PERCENT_FINO = "PERCENT_FINO";
	// loai CT
	public static final String TYPE = "TYPE";

	public static final String DESCRIPTION = "DESCRIPTION";

	private static final String TABLE_DISPLAY_PROGRAME = "DISPLAY_PROGRAME";

	public DISPLAY_PROGRAME_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_DISPLAY_PROGRAME;
		this.columns = new String[] { DISPLAY_PROGRAM_ID, DISPLAY_PROGRAM_CODE,
				DISPLAY_PROGRAM_NAME, STATUS, FROM_DATE, TO_DATE, OBJTARGET,
				RELATION, CREATE_DATE, UPDATE_DATE, CREATE_USER, UPDATE_USER,
				CAT, PERCENT_FINO, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((DisplayProgrameDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(DisplayProgrameDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		DisplayProgrameDTO disDTO = (DisplayProgrameDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.displayProgrameCode };
		return update(value, DISPLAY_PROGRAM_CODE + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(DISPLAY_PROGRAM_CODE + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		DisplayProgrameDTO disDTO = (DisplayProgrameDTO) dto;
		String[] params = { "" + disDTO.displayProgrameCode };
		return delete(DISPLAY_PROGRAM_CODE + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayProgrameDTO
	 * @throws:
	 */
	public DisplayProgrameDTO getRowById(String id) {
		DisplayProgrameDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			String queryStr = "SELECT * FROM DISPLAY_PROGRAME WHERE DISPLAY_PROGRAME_CODE like ?";
			c = rawQuery(queryStr, params);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initLogDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
			c = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	private DisplayProgrameDTO initLogDTOFromCursor(Cursor c) {
		DisplayProgrameDTO dpProDTO = new DisplayProgrameDTO();
		dpProDTO.displayProgrameId = (CursorUtil.getInt(c, DISPLAY_PROGRAM_ID));
		dpProDTO.displayProgrameCode = (CursorUtil.getString(c, DISPLAY_PROGRAM_CODE));
		dpProDTO.displayProgrameName = (CursorUtil.getString(c, DISPLAY_PROGRAM_NAME));
		dpProDTO.status = (CursorUtil.getInt(c, STATUS));
		dpProDTO.fromDate = (CursorUtil.getString(c, FROM_DATE));
		dpProDTO.toDate = (CursorUtil.getString(c, TO_DATE));
		dpProDTO.objTarget = (CursorUtil.getFloat(c, OBJTARGET));

		dpProDTO.relation = (CursorUtil.getInt(c, RELATION));
		dpProDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dpProDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dpProDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		dpProDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dpProDTO.cat = (CursorUtil.getString(c, CAT));
		dpProDTO.percentFino = (CursorUtil.getFloat(c, PERCENT_FINO));

		return dpProDTO;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayProgrameDTO>
	 * @throws:
	 */
	public Vector<DisplayProgrameDTO> getAllRow() {
		Vector<DisplayProgrameDTO> v = new Vector<DisplayProgrameDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		}
		if (c != null) {
			DisplayProgrameDTO DisplayProgrameDTO;
			if (c.moveToFirst()) {
				do {
					DisplayProgrameDTO = initLogDTOFromCursor(c);
					v.addElement(DisplayProgrameDTO);
				} while (c.moveToNext());
			}
		}
		try {
			if (c != null) {
				c.close();
			}
		} catch (Exception e2) {
			// TODO: handle exception
		}
		return v;

	}

	private ContentValues initDataRow(DisplayProgrameDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISPLAY_PROGRAM_ID, dto.displayProgrameId);
		editedValues.put(DISPLAY_PROGRAM_CODE, dto.displayProgrameCode);
		editedValues.put(DISPLAY_PROGRAM_NAME, dto.displayProgrameName);
		editedValues.put(STATUS, dto.status);
		editedValues.put(FROM_DATE, dto.fromDate);
		editedValues.put(TO_DATE, dto.toDate);
		editedValues.put(RELATION, dto.relation);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CAT, dto.cat);
		editedValues.put(PERCENT_FINO, dto.percentFino);

		return editedValues;
	}

	/**
	 * Lay ds chuong trinh trung bay cua NVBH
	 *
	 * @author: ThuatTQ
	 * @param ext
	 * @return: List<SaleOrderDetailDTO>
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayPrograme(Bundle data) throws Exception{

		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}

		String displayType = data
				.getString(IntentConstants.INTENT_DISPLAY_TYPE);
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String displayDepart = data
				.getString(IntentConstants.INTENT_DISPLAY_DEPART);
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);

		boolean isHasDepart = !StringUtil.isNullOrEmpty(displayDepart);
		boolean isHasType = !StringUtil.isNullOrEmpty(displayType);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);

		ArrayList<String> params = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT DISTINCT D.display_program_code            DISPLAY_PROGRAM_CODE, ");
		var1.append("                D.display_program_name            DISPLAY_PROGRAM_NAME, ");
		var1.append("                Strftime('%d/%m/%Y', D.from_date) AS FROM_DATE, ");
		var1.append("                Strftime('%d/%m/%Y', D.to_date)   AS TO_DATE, ");
		var1.append("                ap.description                    DESCRIPTION, ");
		var1.append("                DSTM.quantity_received            SOSUAT ");
		var1.append("FROM   display_program D ");
		var1.append("       JOIN ap_param ap ");
		var1.append("         ON ap.value = d.type ");
		var1.append("       JOIN display_shop_map DSM ");
		var1.append("         ON DSM.display_program_id = D.display_program_id ");
		var1.append("            AND DSM.status = 1 ");
		var1.append("            AND DSM.shop_id in (").append(idShopList)
				.append(")");
		var1.append("       JOIN display_staff_map DSTM ");
		var1.append("              ON D.display_program_id = DSTM.display_program_id ");
		var1.append("                 AND DSTM.status = 1 ");
		var1.append("                 AND Date(DSTM.month, 'start of month') = Date('now', 'localtime', ");
		var1.append("                                         'start of month') ");
		var1.append("                 AND DSTM.staff_id = ? ");
		params.add(String.valueOf(staffId));
		var1.append("                 AND DSTM.shop_id = ? ");
		params.add(shopId);
		var1.append("       LEFT JOIN display_product_group DDG ");
		var1.append("              ON DDG.display_program_id = D.display_program_id ");
		var1.append("                 AND DDG.type IN ( 1, 2, 3, 4 ) ");
		var1.append("                 AND DDG.status = 1 ");
		var1.append("       LEFT JOIN display_product_group_dtl DDGD ");
		var1.append("              ON DDGD.display_product_group_id = DDG.display_product_group_id ");
		var1.append("                 AND DDGD.status = 1 ");
		var1.append("       LEFT JOIN product p ");
		var1.append("              ON DDGD.product_id = p.product_id ");
		var1.append("                 AND p.status = 1 ");
		var1.append("       LEFT JOIN product_info PI ");
		var1.append("              ON PI.product_info_id = P.cat_id ");
		var1.append("              AND PI.type = 1 ");
		var1.append("              AND PI.status = 1 ");
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND D.status = 1 ");
		var1.append("       AND Ifnull(Date(D.to_date) >= Date('now', 'localtime'), 1) ");
		var1.append("       AND ap.status = 1 ");
		var1.append("       AND ap.ap_param_code = 'DISPLAY_PROGR_TYPE' ");

		if (isHasType) {
			var1.append("	and d.type = ? ");
			params.add(displayType);
		}

		if (isHasDepart) {
			var1.append("	and pi.product_info_code like ( ? ) ");
			params.add("%" + displayDepart + "%");
		}

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ var1.toString() + ") ";
//		var1.append(" ORDER BY Date(FROM_DATE) DESC, Date(TO_DATE), DISPLAY_PROGRAM_CODE, DISPLAY_PROGRAM_NAME");
		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY Date(FROM_DATE) DESC, Date(TO_DATE), DISPLAY_PROGRAM_CODE, DISPLAY_PROGRAM_NAME ";

		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "DISPLAY_PROGRAM_CODE")
		.addMapper(SortActionConstants.NAME, "DISPLAY_PROGRAM_NAME")
		.addMapper(SortActionConstants.TYPE, "SOSUAT")
		.defaultOrderString(defaultOrderByStr)
		.build(sortInfo);
		//add order string
		var1.append(orderByStr);

		String queryGetListProductForOrder = var1.toString() + " " + ext;

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		modelData.setModelData(list);
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQueries(getCountProductList,params);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQueries(queryGetListProductForOrder,params);

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this
								.initPromotionProgrameObjectFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 *
	 * Lay ds chuong trinh bay cua GSNPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: DisplayProgrameModel
	 * @throws:
	 */
	public DisplayProgrameModel getListSuperDisplayPrograme(Bundle data) throws Exception{
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String displayDepart = data.getString(IntentConstants.INTENT_DISPLAY_DEPART);
		String displayType = data.getString(IntentConstants.INTENT_DISPLAY_TYPE);
		boolean isHasDepart = !StringUtil.isNullOrEmpty(displayDepart);
		boolean isHasType = !StringUtil.isNullOrEmpty(displayType);
		boolean checkPagging = data.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT DISTINCT D.display_program_code ");
		var1.append("                DISPLAY_PROGRAM_CODE, ");
		var1.append("                D.display_program_name ");
		var1.append("                DISPLAY_PROGRAM_NAME, ");
		var1.append("                Strftime('%d/%m/%Y', D.from_date)                      AS ");
		var1.append("                FROM_DATE, ");
		var1.append("                Strftime('%d/%m/%Y', D.to_date)                        AS ");
		var1.append("                TO_DATE, ");
		var1.append("                ap.description ");
		var1.append("                DESCRIPTION, ");
		var1.append("                (SELECT Count(*) ");
		var1.append("                 FROM   rpt_display_program ");
		var1.append("                 WHERE  1 = 1 ");
//		var1.append("                        AND shop_id =?  ");
//		params.add(shopId);
		var1.append("                        AND shop_id IN ( ")
				.append(idShopList).append(" )");
		var1.append("                        AND status = 1 ");
		var1.append("                        AND display_program_id = d.display_program_id ");
		var1.append("                        AND Date(month, 'start of month') = ");
		var1.append("                            Date('now', 'localtime', 'start of month') ");
		var1.append("                        AND result = 0)                                KD, ");
		var1.append("                (SELECT Count(*) ");
		var1.append("                 FROM   rpt_display_program ");
		var1.append("                 WHERE  1 = 1 ");
//		var1.append("                        AND shop_id = ? ");
//		params.add(shopId);
		var1.append("                        AND shop_id IN ( ")
				.append(idShopList).append(" )");
		var1.append("                        AND status = 1 ");
		var1.append("                        AND display_program_id = d.display_program_id ");
		var1.append("                        AND Date(month, 'start of month') = ");
		var1.append("                            Date('now', 'localtime', 'start of month'))TG ");
		var1.append("FROM   display_program D ");
		var1.append("       JOIN ap_param ap ");
		var1.append("         ON ap.value = d.type ");
		var1.append("       JOIN display_shop_map DSM ");
		var1.append("         ON DSM.display_program_id = D.display_program_id ");
		var1.append("            AND DSM.status = 1 ");
//		var1.append("            AND DSM.shop_id = ? ");
//		params.add(shopId);
		var1.append("            AND DSM.shop_id IN ( ").append(idShopList)
				.append(" )");
		var1.append("       LEFT JOIN display_product_group DDG ");
		var1.append("              ON DDG.display_program_id = D.display_program_id ");
		var1.append("                 AND DDG.type IN ( 1, 2, 3, 4 ) ");
		var1.append("                 AND DDG.status = 1 ");
		var1.append("       LEFT JOIN display_product_group_dtl DDGD ");
		var1.append("              ON DDGD.display_product_group_id = DDG.display_product_group_id ");
		var1.append("                 AND DDGD.status = 1 ");
		var1.append("       LEFT JOIN product p ");
		var1.append("              ON DDGD.product_id = p.product_id ");
		var1.append("                 AND p.status = 1 ");
		var1.append("       LEFT JOIN product_info pi ");
		var1.append("              ON p.cat_id = pi.product_info_id ");
		var1.append("                 AND pi.status = 1 ");
		var1.append("                 AND pi.type = 1 ");
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND D.status = 1 ");
		var1.append("       AND Ifnull(Date(D.to_date) >= Date('now', 'localtime'), 1) ");
		var1.append("       AND ap.status = 1 ");
		var1.append("       AND ap.ap_param_code = 'DISPLAY_PROGR_TYPE' ");

		if (isHasType) {
			var1.append("	and d.type = ? ");
			params.add(displayType);
		}

		if (isHasDepart) {
			var1.append("	and pi.product_info_code like ( ? ) ");
			params.add("%" + displayDepart + "%");
		}

		var1.append("GROUP  BY D.display_program_code, ");
		var1.append("          D.display_program_name, ");
		var1.append("          D.from_date, ");
		var1.append("          D.to_date, ");
		var1.append("          ap.description ");

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ var1.toString() + ") ";
		//default order by
		StringBuffer defaultOrderByStr = new StringBuffer();
		defaultOrderByStr.append("ORDER  BY d.from_date DESC, ");
		defaultOrderByStr.append("          d.to_date ASC, ");
		defaultOrderByStr.append("          display_program_code ");

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "display_program_code")
				.addMapper(SortActionConstants.NAME, "display_program_name")
				.addMapper(SortActionConstants.FROM_DATE, "d.from_date")
				.addMapper(SortActionConstants.TO_DATE, "d.to_date")
				.addMapper(SortActionConstants.TYPE, "ap.description")
				.defaultOrderString(defaultOrderByStr.toString())
				.build(sortInfo);
				//add order string
		var1.append(orderByStr);

		String queryGetListProductForOrder = var1.toString() + " " + ext;

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		modelData.setModelData(list);
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQueries(getCountProductList,params);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQueries(queryGetListProductForOrder,params);

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this
								.initPromotionProgrameObjectForSuperFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 *
	 * Lay ds chuong trinh trung bay cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: TBHVDisplayProgrameModel
	 * @throws:
	 */
	public TBHVDisplayProgrameModel getListTBHVDisplayPrograme(Bundle data) throws Exception{
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String displayDepart = data
				.getString(IntentConstants.INTENT_DISPLAY_DEPART);
		boolean isHasDepart = !StringUtil.isNullOrEmpty(displayDepart);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);

		ArrayList<String> params = new ArrayList<String>();

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT display_program_code, ");
		var1.append("       display_program_name, ");
		var1.append("       Strftime('%d/%m/%Y',from_date) FROM_DATE, ");
		var1.append("       Strftime('%d/%m/%Y',to_date) TO_DATE, ");
		var1.append("       Group_concat(DISTINCT product_info_code) CAT ");
		var1.append("FROM   (SELECT DISTINCT D.display_program_code            DISPLAY_PROGRAM_CODE, ");
		var1.append("                        D.display_program_name            DISPLAY_PROGRAM_NAME, ");
		var1.append("                        D.from_date AS FROM_DATE, ");
		var1.append("                        D.to_date AS TO_DATE, ");
		var1.append("                        D.from_date                       AS FROM_DATE_SORT, ");
		var1.append("                        D.to_date                         AS TO_DATE_SORT, ");
		var1.append("                        PI.product_info_code ");
		var1.append("        FROM   display_program D ");
		var1.append("               JOIN display_shop_map DSM ");
		var1.append("                 ON DSM.display_program_id = D.display_program_id ");
		var1.append("                    AND DSM.status = 1 ");
		var1.append("               and DSM.shop_id in (  ");
		var1.append(idShopList + " )");
		var1.append("               LEFT JOIN display_product_group DDG ");
		var1.append("                      ON DDG.display_program_id = D.display_program_id ");
		var1.append("                         AND DDG.type IN ( 1, 2, 3 ) ");
		var1.append("                         AND DDG.status = 1 ");
		var1.append("               LEFT JOIN display_product_group_dtl DDGD ");
		var1.append("                      ON DDGD.display_product_group_id = ");
		var1.append("                         DDG.display_product_group_id ");
		var1.append("                         AND DDGD.status = 1 ");
		var1.append("               LEFT JOIN product pr ");
		var1.append("                      ON DDGD.product_id = pr.product_id ");
		var1.append("                         AND pr.status = 1 ");
		var1.append("               LEFT JOIN product_info PI ");
		var1.append("                      ON PR.cat_id = PI.product_info_id ");
		var1.append("                      AND PI.status = 1 ");
		var1.append("                      AND PI.type = 1 ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND D.status = 1 ");
		var1.append("               AND Ifnull(Date(D.to_date) >= Date('now', 'localtime'), 1) ");

		if (isHasDepart) {
			var1.append(" AND PI.product_info_code LIKE (?)");
			params.add("" + displayDepart );
		}

		var1.append("       ) ");
		var1.append("GROUP  BY display_program_code, ");
		var1.append("          display_program_name, ");
		var1.append("          from_date, ");
		var1.append("          to_date ");
		var1.append("        ORDER  BY datetime(from_date) desc, ");
		var1.append("                  datetime(to_date),  ");
		var1.append("       			display_program_code, ");
		var1.append("       			display_program_name ");

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ var1.toString() + ") ";

		String queryGetListProductForOrder = var1.toString() + " " + ext;

		TBHVDisplayProgrameModel modelData = new TBHVDisplayProgrameModel();
		List<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		modelData.setModelData(list);
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList,
						params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(queryGetListProductForOrder,
					params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this
								.initPromotionProgrameObjectForSuperFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 * set gia tri cho displayProgramDTO danh cho GSNPP
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: DisplayProgrameDTO
	 * @throws:
	 */
	private DisplayProgrameDTO initPromotionProgrameObjectForSuperFromGetStatement(
			Cursor c) {
		DisplayProgrameDTO dto = new DisplayProgrameDTO();

		dto.displayProgrameId = CursorUtil.getLong(c, DISPLAY_PROGRAM_ID);
		dto.displayProgrameCode = CursorUtil.getString(c, DISPLAY_PROGRAM_CODE);
		dto.displayProgrameName = CursorUtil.getString(c, DISPLAY_PROGRAM_NAME);
		dto.fromDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, FROM_DATE));
		dto.toDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, TO_DATE));
		dto.quantity = CursorUtil.getInt(c, "TG");
		dto.countCustomerNotComplete = CursorUtil.getInt(c, "KD");
		dto.displayProgrameType = CursorUtil.getString(c, DESCRIPTION);
		dto.cat = CursorUtil.getString(c, CAT);
		return dto;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: SoaN
	 * @param c
	 * @return
	 * @return: DisplayProgrameDTO
	 * @throws:
	 */

	private DisplayProgrameDTO initPromotionProgrameObjectFromGetStatement(
			Cursor c) {
		DisplayProgrameDTO dto = new DisplayProgrameDTO();
		dto.displayProgrameId = CursorUtil.getLong(c, DISPLAY_PROGRAM_ID);
		dto.displayProgrameCode = CursorUtil.getString(c, DISPLAY_PROGRAM_CODE);
		dto.displayProgrameName = CursorUtil.getString(c, DISPLAY_PROGRAM_NAME);
		dto.fromDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, FROM_DATE));
		dto.toDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, TO_DATE));
		dto.quantity = CursorUtil.getInt(c, "SOSUAT");
		dto.displayProgrameType = CursorUtil.getString(c, DESCRIPTION);

		return dto;
	}

	/**
	 * Lay so chuong trinh trung bay dang chay
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: int
	 * @throws:
	 */

	public int getNumDisplayProgrameRunning() {
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder
				.append("SELECT DP.DISPLAY_PROGRAME_ID, DP.DISPLAY_PROGRAME_CODE, DP.DISPLAY_PROGRAME_NAME, StrfTime('%d/%m/%Y', DP.FROM_DATE) as FROM_DATE, StrfTime('%d/%m/%Y', DP.TO_DATE) as TO_DATE, GROUP_CONCAT(distinct DP.CAT) as CAT FROM DISPLAY_PROGRAME DP");
		stringbuilder.append(" WHERE DP.STATUS=1 ");
		stringbuilder
				.append(" AND dayInOrder(DP.FROM_DATE) <= (SELECT dayInOrder('now','localtime')) AND ifnull(dayInOrder(DP.TO_DATE) >= (SELECT dayInOrder('now','localtime')),1)");
		stringbuilder
				.append(" GROUP BY DP.DISPLAY_PROGRAME_CODE, DP.DISPLAY_PROGRAME_NAME");

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ stringbuilder.toString() + ") ";

		Cursor cTmp = null;
		int total = 0;
		try {
			// get total row first
			cTmp = rawQuery(getCountProductList, null);

			if (cTmp != null) {
				cTmp.moveToFirst();
				total = cTmp.getInt(0);
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return total;
	}

	/**
	 *
	 * Lay ds CTTB cho chuc nang hinh cua NVBH & cua KH cua NV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: DisplayProgrameModel
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayProgrameWhenTakePhoto(Bundle data) throws Exception {
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		StringBuilder strbuilder = new StringBuilder();
		strbuilder.append("select ");
		strbuilder.append("       DISTINCT ks.KS_ID      DISPLAY_PROGRAM_ID, ");
		strbuilder.append("                ks.KS_CODE    DISPLAY_PROGRAM_CODE, ");
		strbuilder.append("                ks.NAME       DISPLAY_PROGRAM_NAME ");
		strbuilder.append("from ks ks ");
		if (!StringUtil.isNullOrEmpty(customerId)) {
			strbuilder.append("JOIN   ks_customer kscm ");
			strbuilder.append("         ON kscm.ks_id = ks.ks_id ");
			strbuilder.append("            AND kscm.SHOP_ID = ? ");
			params.add(shopId);
			strbuilder.append("            AND kscm.customer_id = ? ");
			params.add(customerId);
			strbuilder.append("            AND kscm.cycle_id = ? ");
			params.add(""+cycleId);
			strbuilder.append("            AND kscm.status = 1 ");
		}
		strbuilder.append("       , ks_shop_map ksm ");
		strbuilder.append("where 1 = 1 ");
		strbuilder.append("      and ks.status = 1 ");
		strbuilder.append("      and  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		strbuilder.append("      and  ? <= ks.TO_CYCLE_ID");
		params.add(""+cycleId);
		strbuilder.append("      and ksm.status = 1 ");
		strbuilder.append("      and ks.KS_ID = ksm.KS_ID ");
		strbuilder.append(" 	   and ksm.SHOP_ID IN ");
		strbuilder.append("       (").append(idShopList).append(")");


		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ strbuilder.toString() + ") ";
		strbuilder.append(" ORDER BY ks.KS_CODE ");

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList,
						params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(strbuilder.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = new DisplayProgrameDTO();
						orderJoinTableDTO.initDisplayProgrameObject(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 * Lay ds CTTB cho chuc nang hinh cua NVBH & cua KH cua NV
	 *
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @return: DisplayProgrameModel
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayProgrameImage(Bundle data) throws Exception {
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		StringBuilder sql = new StringBuilder();
		sql.append("select ");
		sql.append("       DISTINCT ks.KS_ID      DISPLAY_PROGRAM_ID, ");
		sql.append("                ks.KS_CODE    DISPLAY_PROGRAM_CODE, ");
		sql.append("                ks.NAME       DISPLAY_PROGRAM_NAME ");
		sql.append("from ks ks ");
		if (!StringUtil.isNullOrEmpty(customerId)) {
			sql.append("JOIN   ks_customer kscm ");
			sql.append("         ON kscm.ks_id = ks.ks_id ");
			sql.append("            AND kscm.SHOP_ID = ? ");
			params.add(shopId);
			sql.append("            AND kscm.customer_id = ? ");
			params.add(customerId);
			sql.append("            AND kscm.cycle_id = ? ");
			params.add(""+cycleId);
			sql.append("            AND kscm.status = 1 ");
		}
		sql.append("       , ks_shop_map ksm ");
		sql.append("where 1 = 1 ");
		sql.append("      and ks.status = 1 ");
		sql.append("      and  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		sql.append("      and  ? <= ks.TO_CYCLE_ID");
		params.add(""+cycleId);
		sql.append("      and ksm.status = 1 ");
		sql.append("      and ks.KS_ID = ksm.KS_ID ");
		sql.append(" 	   and ksm.SHOP_ID IN ");
		sql.append("       (").append(idShopList).append(")");

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ sql.toString() + ") ";
		sql.append(" ORDER BY ks.KS_CODE");

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList,
						params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(sql.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = new DisplayProgrameDTO();
						orderJoinTableDTO.initDisplayProgrameObject(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 * Lay ds cttb cua TBHV
	 *
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @return: STDisPlayProgramModel
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayProgrameImageTBHV(Bundle data) throws Exception {
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> params = new ArrayList<String>();

		StringBuilder strbuilder = new StringBuilder();
		strbuilder
				.append("SELECT DISTINCT sdp.display_program_id   DISPLAY_PROGRAM_ID, ");
		strbuilder
				.append("                sdp.display_program_code DISPLAY_PROGRAM_CODE, ");
		strbuilder
				.append("                sdp.display_program_name DISPLAY_PROGRAM_NAME ");
		strbuilder.append("FROM   display_program sdp ");
		strbuilder.append("       JOIN display_shop_map sdshm ");
		strbuilder.append("         ON sdp.status = 1 ");
		strbuilder.append("            AND sdshm.status = 1 ");
		strbuilder
				.append("            AND sdshm.display_program_id = sdp.display_program_id ");

		if (!StringUtil.isNullOrEmpty(shopId)) {
			strbuilder
					.append("            AND sdshm.shop_id in (select shop_id from shop where parent_shop_id = ?) ");
			params.add("" + shopId);
		}
		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ strbuilder.toString() + ") ";
		strbuilder.append(" ORDER BY sdp.display_program_code");

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList,
						params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(strbuilder.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					DisplayProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = new DisplayProgrameDTO();
						orderJoinTableDTO.initDisplayProgrameObject(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

}
