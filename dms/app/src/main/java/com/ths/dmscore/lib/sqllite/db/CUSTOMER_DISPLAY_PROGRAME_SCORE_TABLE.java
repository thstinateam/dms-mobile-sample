/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.CustomerDisplayProgrameScoreDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;

/**
 * Mo ta muc dich cua lop (interface)
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_DISPLAY_PROGRAME_SCORE_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String ID = "ID";
	// ma khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// ma CTTB
	public static final String DISPLAY_PROGRAME_ID = "DISPLAY_PROGRAME_ID";

	// ma NVBH
	public static final String STAFF_ID = "STAFF_ID";
	// loai doi tuong cham trung bay
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// ID cua doi tuong cham trung bay
	public static final String OBJECT_ID = "OBJECT_ID";
	// diem cham
	public static final String RESULT = "RESULT";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String TABLE_NAME = "CUSTOMER_DISPLAY_PROGR_SCORE";

	public CUSTOMER_DISPLAY_PROGRAME_SCORE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { ID, CUSTOMER_ID, DISPLAY_PROGRAME_ID,
				STAFF_ID, OBJECT_TYPE, OBJECT_ID, RESULT, CREATE_DATE,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		ContentValues value = initDataRow((CustomerDisplayProgrameScoreDTO) dto);
		return insert(null, value);
	}

	public long insert(CustomerDisplayProgrameScoreDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		CustomerDisplayProgrameScoreDTO dtoDisplay = (CustomerDisplayProgrameScoreDTO) dto;
		ContentValues value = initDataRow(dtoDisplay);
		String[] params = { String.valueOf(dtoDisplay.ID) };
		return update(value, ID + " = ?", params);
	}

	public int delete(String code) {
		String[] params = { code };
		return delete(ID + " = ?", params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		CustomerDisplayProgrameScoreDTO dtoDisplay = (CustomerDisplayProgrameScoreDTO) dto;
		String[] params = { "" + dtoDisplay.ID };
		return delete(ID + " = ?", params);
	}

	/**
	 * 
	 * GET ROW BY ID
	 * 
	 * @author: HaiTC3
	 * @param id
	 * @return
	 * @return: CustomerDisplayProgrameScoreDTO
	 * @throws:
	 */
	public CustomerDisplayProgrameScoreDTO getRowById(String id) {
		CustomerDisplayProgrameScoreDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	/**
	 * 
	 * init object data with cursor
	 * 
	 * @author: HaiTC3
	 * @param c
	 * @return
	 * @return: CustomerDisplayProgrameScoreDTO
	 * @throws:
	 */
	private CustomerDisplayProgrameScoreDTO initDTOFromCursor(Cursor c) {
		CustomerDisplayProgrameScoreDTO dto = new CustomerDisplayProgrameScoreDTO();
		dto.ID = (CursorUtil.getInt(c, ID));
		dto.customerId = (CursorUtil.getLong(c, CUSTOMER_ID));
		dto.displayProgrameId = (CursorUtil.getInt(c, DISPLAY_PROGRAME_ID));
		dto.staffId = (CursorUtil.getInt(c, STAFF_ID));
		dto.objectType = (CursorUtil.getInt(c, OBJECT_TYPE));
		dto.objectId = (CursorUtil.getInt(c, OBJECT_ID));

		dto.result = (CursorUtil.getInt(c, RESULT));
		dto.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dto.synState = (CursorUtil.getInt(c, SYN_STATE));
		return dto;
	}

	/**
	 * 
	 * init content values with object dto
	 * 
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDataRow(CustomerDisplayProgrameScoreDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(ID, dto.ID);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(DISPLAY_PROGRAME_ID, dto.displayProgrameId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(OBJECT_TYPE, dto.objectType);
		editedValues.put(OBJECT_ID, dto.objectId);
		editedValues.put(RESULT, dto.result);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(SYN_STATE, dto.synState);
		return editedValues;
	}

	/**
	 * 
	 * check customer display programe score exist DB
	 * 
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public int existDisplayProgrameScore(CustomerDisplayProgrameScoreDTO dto) {
		int index = -1;
		String queryCheckExist = "SELECT * FROM CUSTOMER_DISPLAY_PROGR_SCORE WHERE CUSTOMER_ID = ";
		queryCheckExist += String.valueOf(dto.customerId);
		queryCheckExist += " and DISPLAY_PROGRAME_ID = "
				+ String.valueOf(dto.displayProgrameId);
		queryCheckExist += " and STAFF_ID = " + String.valueOf(dto.staffId);
		queryCheckExist += " and OBJECT_TYPE = "
				+ String.valueOf(dto.objectType);
		queryCheckExist += " and OBJECT_ID = " + String.valueOf(dto.objectId);

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ queryCheckExist + ") ";
		String[] params = new String[] {};

		Cursor cTmp = null;
		Cursor c = null;
		try {
			// get total row first
			cTmp = rawQuery(getCountProductList, params);
			int total = 0;
			if (cTmp != null) {
				cTmp.moveToFirst();
				total = cTmp.getInt(0);
				if (total <= 0) {
					index = -1;
				}
			}
			if (total > 0) {
				c = rawQuery(queryCheckExist, params);

				if (c != null) {

					if (c.moveToFirst()) {
						do {
							index = CursorUtil.getInt(c, "ID");
						} while (c.moveToNext());
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
			if (cTmp != null) {
				cTmp.close();
			}
		}
		return index;
	}
}
