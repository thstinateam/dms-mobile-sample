/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.CUSTOMER_ATTRIBUTE_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_ATTRIBUTE_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * CustomerAttributeViewDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:00:17 PM Jan 27, 2015
 */
public class CustomerAttributeViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static final int TEXT = 1;
	public static final int NUMBER = 2;
	public static final int DATE = 3;
	public static final int SELECT = 4;
	public static final int MULTISELECT = 5;
	public static final int LOCATION = 6;
	
	public long customerAttributeID;
	public int displayOrder;
	public String code;
	public String description;
	public int type;
	public String dataLength;
	public int minValue;
	public int maxValue;
	public String name;
	public int isEnumeration;
	public int isSearch;
	public int mandatory;

	public int enumID;
	public String enumValue;
	public long attributeDetailID;
	
	public ArrayList<CustomerAttributeEnumViewDTO> listEnum;
	
	public CustomerAttributeViewDTO() {
		// TODO Auto-generated constructor stub
		listEnum = new ArrayList<CustomerAttributeEnumViewDTO>();
	}
	
	public void initFromCurSor(Cursor c){
		customerAttributeID = CursorUtil.getLong(c, CUSTOMER_ATTRIBUTE_TABLE.CUSTOMER_ATTRIBUTE_ID);
		displayOrder = CursorUtil.getInt(c, "DISPLAY_ORDER");
		dataLength = CursorUtil.getString(c, "DATA_LENGTH");
		minValue = CursorUtil.getInt(c, "MIN_VALUE");
		maxValue = CursorUtil.getInt(c, "MAX_VALUE");
		isEnumeration = CursorUtil.getInt(c, "IS_ENUMERATION");
		isSearch = CursorUtil.getInt(c, "IS_SEARCH");
		mandatory = CursorUtil.getInt(c, "MANDATORY");
		type = CursorUtil.getInt(c, "TYPE");
		
		code = CursorUtil.getString(c, "CODE");
		description = CursorUtil.getString(c, "DESCRIPTION");
		name = CursorUtil.getString(c, "NAME");
		
		enumID = CursorUtil.getInt(c, "DETAIL_ENUM_ID");
		enumValue = CursorUtil.getString(c, "DETAIL_VALUE");
		attributeDetailID = CursorUtil.getLong(c, CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_DETAIL_ID);
	}
	
	public void addAttributeEnum(CustomerAttributeEnumViewDTO attEnum) {
		listEnum.add(attEnum);
	}
	
	public ArrayList<CustomerAttributeEnumViewDTO> getListEnum(){
		return listEnum;
	}
	
	public CustomerAttributeEnumViewDTO[] toArray(){
		CustomerAttributeEnumViewDTO[] arr = new CustomerAttributeEnumViewDTO[listEnum.size()];
		int i = 0;
		for(CustomerAttributeEnumViewDTO dto : listEnum){
			arr[i] = dto;
			i++;
		}
		return arr;
	}
	
	public String[] toArrayString(){
		String[] arr = new String[listEnum.size()];
		int i = 0;
		for(CustomerAttributeEnumViewDTO dto : listEnum){
			arr[i] = dto.value;
			i++;
		}
		return arr;
	}

}
