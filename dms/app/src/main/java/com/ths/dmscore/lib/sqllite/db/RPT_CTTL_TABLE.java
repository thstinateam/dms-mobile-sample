package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Chua thong tin tien trinh km tich luy
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_CTTL_TABLE extends ABSTRACT_TABLE {
	public static final String RPT_CTTL_ID = "RPT_CTTL_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	public static final String PROMOTION_FROM_DATE = "PROMOTION_FROM_DATE";
	public static final String PROMOTION_TO_DATE = "PROMOTION_TO_DATE";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String TOTAL_QUANTITY = "TOTAL_QUANTITY";
	public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
	public static final String TOTAL_QUANTITY_PAY_PROMOTION = "TOTAL_QUANTITY_PAY_PROMOTION";
	public static final String TOTAL_AMOUNT_PAY_PROMOTION = "TOTAL_AMOUNT_PAY_PROMOTION";
	public static final String TOTAL_PROMOTION = "TOTAL_PROMOTION";
	public static final String LAST_DATE_PROMOTION = "LAST_DATE_PROMOTION";
	public static final String BALANCE_PROMOTION = "BALANCE_PROMOTION";

	public static final String TABLE_NAME = "RPT_CTTL";

	public RPT_CTTL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { RPT_CTTL_ID, CREATE_DATE,
				PROMOTION_PROGRAM_ID, PROMOTION_FROM_DATE, PROMOTION_TO_DATE,
				SHOP_ID, CUSTOMER_ID, TOTAL_QUANTITY, TOTAL_AMOUNT,
				TOTAL_QUANTITY_PAY_PROMOTION, TOTAL_AMOUNT_PAY_PROMOTION,
				TOTAL_PROMOTION, LAST_DATE_PROMOTION, BALANCE_PROMOTION,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

}