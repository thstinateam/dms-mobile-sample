package com.ths.dmscore.util;

import java.math.BigDecimal;
import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dms.R;

public class NetworkUtil {

	// Loai mang
	public static final String DMS_NETWORK_NOT_CONNECTED = "Not Connected";
	public static final String DMS_NETWORK_TYPE_WIFI = "Wifi";
	public static final String DMS_NETWORK_TYPE_GPRS = "GPRS";
	public static final String DMS_NETWORK_TYPE_CDMA = "CDMA(2G)";
	public static final String DMS_NETWORK_TYPE_IDEN = "IDEN(2G)";
	public static final String DMS_NETWORK_TYPE_EDGE = "EDGE(2G)";
	public static final String DMS_NETWORK_TYPE_1xRTT = "1xRTT(2G)";
	public static final String DMS_NETWORK_TYPE_UMTS = "UMTS(3G)";
	public static final String DMS_NETWORK_TYPE_EVDO_0 = "EVDO_0(3G)";
	public static final String DMS_NETWORK_TYPE_EVDO_A = "EVDO_A(3G)";
	public static final String DMS_NETWORK_TYPE_HSDPA = "HSDPA(3G)";
	public static final String DMS_NETWORK_TYPE_HSPA = "HSPA(3G)";
	public static final String DMS_NETWORK_TYPE_HSUPA = "HSUPA(3G)";
	public static final String DMS_NETWORK_TYPE_EVDO_B = "EVDO_B(3G)";
	public static final String DMS_NETWORK_TYPE_EHRPD = "EHRPD(3G)";
	public static final String DMS_NETWORK_TYPE_HSPAP = "HSPAP(4G)";
	public static final String DMS_NETWORK_TYPE_LTE = "LTE(4G)";
	public static final String DMS_NETWORK_TYPE_UNKNOWN = "Unknown";
	public static final String DMS_NETWORK_TYPE_3G = "3G";
	public static final String DMS_NETWORK_TYPE_3G_UNKNOWN = "(3G)";

	// Toc do
	public static final int DMS_NETWORK_WEAK = 0;
	public static final int DMS_NETWORK_STRONG = 1;
	public static final int DMS_NETWORK_VERY_STRONG = 2;

	// Neu build tren API 13 thi khong can dung bien nay
	public final int NETWORK_TYPE_HSPAP = 15;

	int MIN_RSSI = -100;
	int MAX_RSSI = -55;
	int levels = 101;

	private int levelSignalMobile = 0;
	private PhoneStateListener listener;

	// Su dung inner class ket hop singleton
	public static class NetworkHolder {
		public static final NetworkUtil INSTANCE = new NetworkUtil();
	}

	private NetworkUtil() {
		listener = new MyPhoneStateListener();
	}

	public void startListenerSignal() {
		TelephonyManager telManager = getTelephonyManager();
		telManager.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
	}

	public void stopListenerSignal() {
		TelephonyManager telManager = getTelephonyManager();
		telManager.listen(listener, PhoneStateListener.LISTEN_NONE);
		levelSignalMobile = DMS_NETWORK_WEAK;
	}

	public static NetworkUtil me() {
		return NetworkHolder.INSTANCE;
	}

	public void updateLevelSignalMobile(int level) {
		levelSignalMobile = level;
	}

	/**
	 * Tra ve doi tuong quan ly thong tin ve telephony
	 * 
	 * @author: quangvt1
	 * @since: 08:30:17 12-11-2014
	 * @return: TelephonyManager
	 * @throws:
	 * @return
	 */
	public TelephonyManager getTelephonyManager() {
		Context mContext = GlobalInfo.getInstance().getAppContext();
		TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
		return tm;
	}

	/**
	 * Tra ve doi tuong quan ly thong tin mang cua may
	 * 
	 * @author: quangvt1
	 * @since: 08:30:17 12-11-2014
	 * @return: ConnectivityManager
	 * @throws:
	 * @return
	 */
	public ConnectivityManager getNetworkManager() {
		Context mContext = GlobalInfo.getInstance().getAppContext();
		ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm;
	}

	/**
	 * Tra ve doi tuong quan ly thong tin wifi
	 * 
	 * @author: quangvt1
	 * @since: 08:30:17 12-11-2014
	 * @return: WifiManager
	 * @throws:
	 * @return
	 */
	public WifiManager getWifiManager() {
		Context mContext = GlobalInfo.getInstance().getAppContext();
		WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		return wifi;
	}

	/**
	 * Get the network info
	 * 
	 * @author: quangvt1
	 * @since: 09:18:09 12-11-2014
	 * @return: NetworkInfo
	 * @throws:
	 * @param context
	 * @return
	 */
	public NetworkInfo getNetworkInfo() {
		ConnectivityManager cm = getNetworkManager();
		return cm.getActiveNetworkInfo();
	}

	/**
	 * Kiem tra co mang khong
	 * 
	 * @author: quangvt1
	 * @since: 08:28:00 12-11-2014
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	public boolean checkNetworkAccess() {
		boolean res = false;
		ConnectivityManager cm = getNetworkManager();
		NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if ((mobile != null && mobile.isConnectedOrConnecting())
				|| (wifi != null && wifi.isConnectedOrConnecting())) {
			MyLog.i("Test Network Access", "Network available!");
			res = true;
		} else {
			MyLog.i("Test Network Access", "No network available!");
			res = false;
		}

		return res;
	}

	/**
	 * Tra ve loai mang dang connect
	 * 
	 * @author: quangvt1
	 * @since: 09:19:31 12-11-2014
	 * @return: String
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public DMSNetworkInfo getCurrentTypeConnect() throws Exception {
		DMSNetworkInfo dmsInfo = null;
		NetworkInfo info = getNetworkInfo();
		if (checkNetworkAccess() && info != null) {
			int type = info.getType();
			int subType = info.getSubtype();
			dmsInfo = getTypeConnect(type, subType);
		}

		if (dmsInfo == null) {
			dmsInfo = new DMSNetworkInfo();
			dmsInfo.type = DMS_NETWORK_NOT_CONNECTED;
			dmsInfo.value = DMS_NETWORK_WEAK;
		}

		return dmsInfo;
	}

	/**
	 * Tra ve loai mang dang connect
	 * 
	 * @author: quangvt1
	 * @since: 09:19:53 12-11-2014
	 * @return: String
	 * @throws:
	 * @param type
	 * @param subType
	 * @return
	 */
	private DMSNetworkInfo getTypeConnect(int type, int subType)
			throws Exception {
		DMSNetworkInfo info = new DMSNetworkInfo();
		if (type == ConnectivityManager.TYPE_WIFI) {
			Context mContext = GlobalInfo.getInstance().getAppContext();
			WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = wifi.getConnectionInfo();
			info.type = DMS_NETWORK_TYPE_WIFI;

			if (wifiInfo != null) {
				info.value = getSignalStrengthWifi(wifiInfo);
			} else {
				info.value = DMS_NETWORK_WEAK;
			}

		} else if (type == ConnectivityManager.TYPE_MOBILE) {
			switch (subType) {
			case TelephonyManager.NETWORK_TYPE_GPRS: // ~ 100 kbps
				info.type = DMS_NETWORK_TYPE_GPRS;
				break;
			case TelephonyManager.NETWORK_TYPE_CDMA: // ~ 14-64 kbps
				info.type = DMS_NETWORK_TYPE_CDMA;
				break;
			case TelephonyManager.NETWORK_TYPE_IDEN: // ~ 25 kbps
				info.type = DMS_NETWORK_TYPE_IDEN;
				break;
			case TelephonyManager.NETWORK_TYPE_EDGE: // ~ 50-100 kbps
				info.type = DMS_NETWORK_TYPE_EDGE;
				break;
			case TelephonyManager.NETWORK_TYPE_1xRTT: // ~ 50-100 kbps
				info.type = DMS_NETWORK_TYPE_1xRTT;
				break;
			case TelephonyManager.NETWORK_TYPE_UMTS: // ~ 400-7000 kbps
				info.type = DMS_NETWORK_TYPE_UMTS;
				break;
			case TelephonyManager.NETWORK_TYPE_EVDO_0: // ~ 400-1000 kbps
				info.type = DMS_NETWORK_TYPE_EVDO_0;
				break;
			case TelephonyManager.NETWORK_TYPE_EVDO_A: // ~ 600-1400 kbps
				info.type = DMS_NETWORK_TYPE_EVDO_A;
				break;
			case TelephonyManager.NETWORK_TYPE_HSDPA: // ~ 2-14 Mbps
				info.type = DMS_NETWORK_TYPE_HSDPA;
				break;
			case TelephonyManager.NETWORK_TYPE_HSPA: // ~ 700-1700 kbps
				info.type = DMS_NETWORK_TYPE_HSPA;
				break;
			case TelephonyManager.NETWORK_TYPE_HSUPA: // ~ 1-23 Mbps
				info.type = DMS_NETWORK_TYPE_HSUPA;
				break;
			case TelephonyManager.NETWORK_TYPE_EVDO_B: // ~ 5 Mbps
				info.type = DMS_NETWORK_TYPE_EVDO_B;
				break;
			case TelephonyManager.NETWORK_TYPE_EHRPD: // ~ 1-2 Mbps
				info.type = DMS_NETWORK_TYPE_EHRPD;
				break;
			// Sau nay neu build API >= 13 thi mo ra
			// case TelephonyManager.NETWORK_TYPE_HSPAP:
			case NETWORK_TYPE_HSPAP: // ~ 10-20 Mbps
				info.type = DMS_NETWORK_TYPE_HSPAP;
				break;
			case TelephonyManager.NETWORK_TYPE_LTE: // ~ 10+ Mbps
				info.type = DMS_NETWORK_TYPE_LTE;
				break;
			// Unknown
			case TelephonyManager.NETWORK_TYPE_UNKNOWN:
				info.type = DMS_NETWORK_TYPE_3G;
				break;
			default:
				info.type = DMS_NETWORK_TYPE_3G_UNKNOWN;
			}

			info.value = levelSignalMobile;
		} else {
			info.type = DMS_NETWORK_TYPE_UNKNOWN;
			info.value = DMS_NETWORK_WEAK;
		}

		return info;
	}

	/**
	 * @author: quangvt1
	 * @since: 11:33:59 22-11-2014
	 * @return: int
	 * @throws:
	 * @param wifiInfo
	 * @return
	 */
	private int getSignalStrengthWifi(WifiInfo wifiInfo) {
		int strength = DMS_NETWORK_WEAK;
		List<ScanResult> results = getWifiManager().getScanResults();
		if (results != null) {
			for (ScanResult result : results) {
				if (result != null && result.BSSID != null
						&& result.BSSID.equals(wifiInfo.getBSSID())) {
					int level = calculateSignalLevel(wifiInfo.getRssi(), result.level);
					level = level * 100 / result.level;
					strength = getSignalStrength(level);
					break;
				}
			}
		}
		return strength;
	}

	public class DMSNetworkInfo {
		public String type; // "WIFI" or "MOBILE"
		public int value; // 0 - yeu, 1 - manh, 2 - rat manh
	}

	public int calculateSignalLevel(int rssi, int numLevels) {
		int level = 0;
		if (rssi <= MIN_RSSI) {
			level = 0;
		} else if (rssi >= MAX_RSSI) {
			level = numLevels - 1;
		} else {
			double inputRange = (MAX_RSSI - MIN_RSSI);
			double outputRange = (numLevels - 1);
//			if (inputRange != 0) {			
			if (BigDecimal.valueOf(inputRange).compareTo(BigDecimal.ZERO) != 0) {
				level = (int) ((rssi - MIN_RSSI) * outputRange / inputRange);
			}
		}

		return level;
	}

	public int getSignalStrength(int level) {
		int strength = 0;
		if (level > 75) {
			strength = DMS_NETWORK_VERY_STRONG; // Rat manh
		} else if (level > 50) {
			strength = DMS_NETWORK_STRONG; // Manh
		} else {
			strength = DMS_NETWORK_WEAK; // Yeu
		}

		return strength;
	}

	private class MyPhoneStateListener extends PhoneStateListener {

		public int singalStenths = 0;

		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			singalStenths = signalStrength.getGsmSignalStrength();

			if (singalStenths > 30) {
				updateLevelSignalMobile(DMS_NETWORK_VERY_STRONG);
			} else if (singalStenths > 20 && singalStenths < 30) {
				updateLevelSignalMobile(DMS_NETWORK_STRONG);
			} else if (singalStenths < 20) {
				updateLevelSignalMobile(DMS_NETWORK_WEAK);
			}
		}
	};

	public boolean isDisconnectOrUnknown(String networkType) {
		return DMS_NETWORK_NOT_CONNECTED.equalsIgnoreCase(networkType)
				|| DMS_NETWORK_TYPE_UNKNOWN.equalsIgnoreCase(networkType);
	}

	/**
	 * @author: quangvt1
	 * @since: 08:50:19 25-11-2014
	 * @return: String
	 * @throws:
	 * @param networkType
	 * @return
	 */
	public String getSpeed(int networkSpeed) {
		String speed = "";
		switch (networkSpeed) {
		case DMS_NETWORK_VERY_STRONG:
			speed = StringUtil.getString(R.string.TEXT_NETWORK_SPEED_VERY_STRONG);
			break;
		case DMS_NETWORK_STRONG:
			speed = StringUtil.getString(R.string.TEXT_NETWORK_SPEED_STRONG);
			break;
		default:
			speed = StringUtil.getString(R.string.TEXT_NETWORK_SPEED_WEAK);
			break;
		}

		return speed;
	}
}