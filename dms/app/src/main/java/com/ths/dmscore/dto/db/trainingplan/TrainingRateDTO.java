package com.ths.dmscore.dto.db.trainingplan;

/**
 * 
 * TraningRateDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:43:18 17-11-2014
 */
public class TrainingRateDTO {
	private int trainingRateId;
	private int subStaffId;
	private String trainingMonth;
	private String code;
	private String shortName;
	private String fullName;
	private int orderNumber;
	private String createUser;
	private String updateUser;
	private String createDate;
	private String updateDate;

	public int getTraningRateId() {
		return trainingRateId;
	}

	public void setTraningRateId(int trainingRateId) {
		this.trainingRateId = trainingRateId;
	}

	public int getSubStaffId() {
		return subStaffId;
	}

	public void setSubStaffId(int subStaffId) {
		this.subStaffId = subStaffId;
	}

	public String getTrainingMonth() {
		return trainingMonth;
	}

	public void setTrainingMonth(String trainingMonth) {
		this.trainingMonth = trainingMonth;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}