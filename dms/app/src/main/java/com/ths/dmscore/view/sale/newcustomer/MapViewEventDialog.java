/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.AbstractAlertDialog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.map.MapWrapperLayout;
import com.ths.map.OfflineMapManager;
import com.ths.map.OverlayViewItemObj;
import com.ths.map.OverlayViewLayer;
import com.ths.map.OverlayViewOptions;
import com.ths.map.view.MarkerMapView;
import com.ths.dms.R;
import com.viettel.maps.MapView;
import com.viettel.maps.MapView.MapEventListener;
import com.viettel.maps.base.LatLng;
import com.viettel.maps.controls.BaseControl;
import com.viettel.maps.controls.ZoomControl;

/**
 * MapViewEventDialog.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since:  9:21:40 AM Jan 12, 2015
 */
public class MapViewEventDialog extends AbstractAlertDialog implements MapEventListener,
	View.OnClickListener, OnMapClickListener, OnMapLoadedCallback {

	private final int ACTION_CLOSE_MAP_DIALOG;

	private RelativeLayout rlMap;
	private Button btOK;

	private MapView mapView;

	private OverlayViewOptions markerCustomerOpts;
	private MarkerMapView markerCustomer;
	private OverlayViewLayer markerCustomerLayer;
	//cau hinh su dung google map hay vtmap
	private boolean isUsingGoogleMap;
	//Google map
	GoogleMap googleMap;
	private volatile MapFragment mapFragment;
	//map Fragment cua Google map
	//Marker vi tri cua toi
	private Marker markerGoogle;
	//flag marker vi tri khach hang
	private Marker flagMarker;
	double cusLat = -1;
	double cusLng = -1;

	private Button btHuy;
	View viewHeader;
	private Button btUseMyPosition;

	private View llViewInfo;

	private View llEditInfo;

	private MarkerMapView makerMyLocation;
	OverlayViewOptions optsMyPos;
	public MapViewEventDialog(Context context, BaseFragment base,
			CharSequence title, int actionCloseMapView) {
		super(context, base, title);
		// TODO Auto-generated constructor stub
		View v = setViewLayout(R.layout.layout_map_view_event_dialog, GlobalUtil.getInstance().getDPIScreen() - 50);
		setCancelable(false);
		setCanceledOnTouchOutside(false);
		ACTION_CLOSE_MAP_DIALOG = actionCloseMapView;
		rlMap = (RelativeLayout) v.findViewById(R.id.rlMap);
		if(GlobalInfo.getInstance().getMapTypeIsUse() == 2){
			isUsingGoogleMap = true;
		}
		if (isUsingGoogleMap) {
			initGoogleMap();
		} else {
			initVTMap();
		}

		LayoutInflater inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		viewHeader =  inflater.inflate(R.layout.layout_map_header_view_event_dialog, rlMap);
		llViewInfo = viewHeader.findViewById(R.id.llInfoView);
		llEditInfo = viewHeader.findViewById(R.id.llInfoEdit);
		
		btUseMyPosition = (Button) viewHeader.findViewById(R.id.btUseMyPosition);
		btUseMyPosition.setOnClickListener(this);
		btOK = (Button) viewHeader.findViewById(R.id.btOK);
		btOK.setOnClickListener(this);
		btHuy = (Button) viewHeader.findViewById(R.id.btHuy);
		btHuy.setOnClickListener(this);
	}

	private void initVTMap() {
		mapView = new MapView(parent);
		mapView.setMapKey(GlobalInfo.getVIETTEL_MAP_KEY());
		mapView.setZoomControlEnabled(true);
		mapView.setMapTypeControlEnabled(false);
		mapView.setMapEventListener(this);
		rlMap.addView(mapView);
		markerCustomerOpts = new OverlayViewOptions();
		markerCustomer = new MarkerMapView(parent, R.drawable.icon_flag);
		markerCustomer.setVisibility(View.GONE);
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
		markerCustomerLayer = new OverlayViewLayer();
		mapView.addLayer(markerCustomerLayer);
		markerCustomerLayer.addItemObj(markerCustomer, markerCustomerOpts);
		ZoomControl zoomControl = (ZoomControl) mapView
				.getControl(BaseControl.TYPE_CONTROL_ZOOM);
		if (zoomControl != null) {
			mapView.removeView(zoomControl);
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.MATCH_PARENT,
					RelativeLayout.LayoutParams.MATCH_PARENT);
			zoomControl.setLayoutParams(lp);
			zoomControl.setGravity(Gravity.LEFT | Gravity.BOTTOM);
			rlMap.addView(zoomControl);
		}
	}

	private void initGoogleMap() {
		MapWrapperLayout bgMapView = new MapWrapperLayout(parent);
		bgMapView.setBackgroundColor(Color.rgb(237, 234, 226));
		bgMapView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		LayoutInflater inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view =  inflater.inflate(R.layout.layout_map_view, bgMapView);
		RelativeLayout mainView = (RelativeLayout) view.findViewById(R.id.RelativeLayout1);
		
		mapFragment = ((MapFragment) parent.getFragmentManager().findFragmentById(R.id.ggMap));
		googleMap = mapFragment.getMap();
		rlMap.addView(bgMapView);
		
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		googleMap.getUiSettings().setMyLocationButtonEnabled(false);
		googleMap.setMyLocationEnabled(false);
		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new com.google.android.gms.maps.model.LatLng(lat, lng)).zoom(17).build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		googleMap.setOnMapClickListener(this);
		
//		googleMap.setOnMapLoadedCallback(this);
		
		initZoomControl();
		
		initMyPositionControl();
		
		//add offline map
		OfflineMapManager offlineMapManager = new OfflineMapManager(googleMap, parent, mainView);
	}

	/** add control my position cho Google map
	 * @author cuonglt3
	 *
	 */
	private void initMyPositionControl() {
		// TODO Auto-generated method stub
		LinearLayout llMyPosition;
		llMyPosition = new LinearLayout(parent);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		llMyPosition.setLayoutParams(lp);
		llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
		llMyPosition.setOrientation(LinearLayout.VERTICAL);

		ImageView ivMyPosition = new ImageView(parent);
		ivMyPosition.setLayoutParams(new LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT));
		ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		ivMyPosition.setImageResource(R.drawable.icon_location_2);
		ivMyPosition.setBackgroundResource(R.drawable.custom_button_with_border);
		ivMyPosition.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				drawMyLocation();
				moveToCenterMyPosition();
			}
		});
		llMyPosition.addView(ivMyPosition);
		rlMap.addView(llMyPosition);
	}

	/** move zoom control Google map qua ben trai
	 * @author cuonglt3
	 *
	 */
	public void initZoomControl() {
		View zoomControls = null;
		zoomControls = mapFragment.getView().findViewById(0x1);

		if (zoomControls != null
				&& zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
			// ZoomControl is inside of RelativeLayout
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls
					.getLayoutParams();

			// Align it to - parent top|left
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
			// Update margins, set to 10dp
			final int margin = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 10, parent.getResources()
							.getDisplayMetrics());
			params.setMargins(margin, margin, margin, margin);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (isUsingGoogleMap) {
			drawMyLocation();
		} else{
			LinearLayout llMyPosition;
			llMyPosition = new LinearLayout(mapView.getContext());
			RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			lp.setMargins(0, 0, GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5));
			llMyPosition.setLayoutParams(lp);
			llMyPosition.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
			llMyPosition.setOrientation(LinearLayout.HORIZONTAL);
			ImageView ivMyPosition = new ImageView(parent);
			ivMyPosition.setLayoutParams(new LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			ivMyPosition.setPadding(GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5));
			ivMyPosition.setImageResource(R.drawable.icon_location_2);
			ivMyPosition
					.setBackgroundResource(R.drawable.custom_button_with_border);
			ivMyPosition.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					moveToCenterMyPosition();
				}
			});
			llMyPosition.addView(ivMyPosition);
			rlMap.addView(llMyPosition);
			drawMyLocation();
		}
	}

	@Override
	public void onShow(DialogInterface dialog) {
		// TODO Auto-generated method stub
		super.onShow(dialog);
		if (isUsingGoogleMap) {
			WindowManager.LayoutParams params = getWindow().getAttributes();
		    params.dimAmount = 0;
		    getWindow().setAttributes(params);
		}
	}

	/**
	 * ve vi tri cua toi
	 *
	 * @author: dungdq3
	 * @since: 5:01:56 PM Jan 13, 2015
	 * @return: void
	 * @throws:
	 */
	private void drawMyLocation() {
		// TODO Auto-generated method stub
		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (isUsingGoogleMap) {
			if (markerGoogle == null) {
				markerGoogle = googleMap.addMarker(new MarkerOptions()
						.position(new com.google.android.gms.maps.model.LatLng(lat, lng))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.icon_location)));
				markerGoogle.setAnchor(0.5f, 0.5f);
				if (lat > 0 && lng > 0) {
					markerGoogle.setVisible(true);
				} else{
					markerGoogle.setVisible(false);
				}
			} else{
				markerGoogle.setPosition(new com.google.android.gms.maps.model.LatLng(lat, lng));
				if (lat > 0 && lng > 0) {
					markerGoogle.setVisible(true);
				} else{
					markerGoogle.setVisible(false);
				}
			}
		} else {
			LatLng latLng = new LatLng(lat, lng);
			// Move mapview to location image
			mapView.moveTo(latLng);
			if (makerMyLocation == null) {
				// Add marker
				makerMyLocation = new MarkerMapView(parent,
						R.drawable.icon_location);
				optsMyPos = new OverlayViewOptions();
				optsMyPos.position(latLng);
				optsMyPos.drawMode(OverlayViewItemObj.DRAW_CENTER);
				OverlayViewLayer myPosLayer = new OverlayViewLayer();
				mapView.addLayer(myPosLayer);
				// mapView.removeAllViews();
				myPosLayer.addItemObj(makerMyLocation, optsMyPos);
			}
			if (optsMyPos != null) {
				optsMyPos.position(latLng);
			}
			
			if (makerMyLocation != null) {
				if (lat > 0 && lng > 0) {
					makerMyLocation.setVisibility(View.VISIBLE);
				} else{
					makerMyLocation.setVisibility(View.INVISIBLE);
				}
			}
			mapView.invalidate();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean touch = super.onTouch(v, event);
		if(touch) {
			setEventFromAlert(ACTION_CLOSE_MAP_DIALOG, v, null);
		}
		return touch;
	}

	@Override
	public void onBoundChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCenterChanged() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onDoubleTap(LatLng arg0, Point arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onLongTap(LatLng arg0, Point arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onSingleTap(LatLng latLng, Point p) {
		// TODO Auto-generated method stub
		showLocationFlag(latLng.getLatitude(), latLng.getLongitude(), false);
		return true;
	}

	/**
	 *
	 *
	 * @author: dungdq3
	 * @since: 5:32:57 PM Jan 12, 2015
	 * @return: void
	 * @throws:
	 */
	public void showLocationFlag(double lat, double lng, boolean view) {
		cusLat = lat;
		cusLng = lng;
		hideHeaderPosition(view);
		if (isUsingGoogleMap) {
			if (flagMarker == null) {
				flagMarker = googleMap.addMarker(new MarkerOptions()
					.position(new com.google.android.gms.maps.model.LatLng(lat, lng))
					.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_flag)));
				flagMarker.setAnchor(0f, 1f);
				if (lat > 0 && lng > 0) {
					flagMarker.setVisible(true);
				} else{
					flagMarker.setVisible(false);
				}
			} else {
				if (lat > 0 && lng > 0) {
					flagMarker.setPosition(new com.google.android.gms.maps.model.LatLng(lat, lng));
					flagMarker.setVisible(true);
				} else{
					flagMarker.setVisible(false);
				}
			}
			if(view) {
				googleMap.setOnMapClickListener(null);
			} else{
				googleMap.setOnMapClickListener(this);
			}
		}else{
			// TODO Auto-generated method stub
			markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
			LatLng latLng = new LatLng(lat, lng);
			markerCustomerOpts.position(latLng);
			//chi hien khi co vi tri
			if (lat > 0 && lng > 0) {
				//hien thi marker cua khach hang
				markerCustomer.setVisibility(View.VISIBLE);

			} else {
				//khong co vi tri thi an
				markerCustomer.setVisibility(View.GONE);
			}
			if(view) {
				mapView.setMapEventListener(null);
			} else{
				mapView.setMapEventListener(this);
			}
			mapView.invalidate();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == btOK) {
			double[] arrayLatLng = {cusLat, cusLng};
			setEventFromAlert(ACTION_CLOSE_MAP_DIALOG, null, arrayLatLng);
			this.dismiss();
		} else if (v == btHuy){
			double[] arrayLatLng = {-1, -1};
			setEventFromAlert(ACTION_CLOSE_MAP_DIALOG, null, arrayLatLng);
			this.dismiss();
		} else if(v == btUseMyPosition){
			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			if (lat > 0 && lng > 0) {
				showLocationFlag(lat, lng, false);
				moveToCenterMyPosition();
			}
		} else{
			super.onClick(v);
		}
	}

	/**
	 * di toi vi tri cua toi
	 *
	 * @author: dungdq3
	 * @since: 5:08:54 PM Jan 13, 2015
	 * @return: void
	 * @throws:
	 */
	private void moveToCenterMyPosition() {
		// TODO Auto-generated method stub
		double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (lat > 0 && lng > 0) {
			if (isUsingGoogleMap) {
				com.google.android.gms.maps.model.LatLng myLocation1 = new com.google.android.gms.maps.model.LatLng(lat,
						lng);
				CameraUpdate cu = CameraUpdateFactory.newLatLng(myLocation1);
				//ve lai marker mylocation
				markerGoogle.setPosition(myLocation1);
				markerGoogle.setVisible(true);
				googleMap.animateCamera(cu);
			} else {
				LatLng myLocation = new LatLng(lat, lng);
				mapView.moveTo(myLocation);
				mapView.invalidate();
			}
		}
	}

	@Override
	public void onMapClick(com.google.android.gms.maps.model.LatLng point) {
		// TODO Auto-generated method stub
		showLocationFlag(point.latitude, point.longitude, false);
	}

	@Override
	public void onMapLoaded() {
		// TODO Auto-generated method stub
//		mapFragment.getView().invalidate();
	}

	private void hideHeaderPosition(boolean isHide){
		if (llViewInfo != null && llEditInfo != null) {
			if(isHide){
				llViewInfo.setVisibility(View.VISIBLE);
				llEditInfo.setVisibility(View.GONE);
			} else{
				llViewInfo.setVisibility(View.GONE);
				llEditInfo.setVisibility(View.VISIBLE);
			}
		}
	}
	
	public void updateNewGPSPosition(){
		drawMyLocation();
	}
}
