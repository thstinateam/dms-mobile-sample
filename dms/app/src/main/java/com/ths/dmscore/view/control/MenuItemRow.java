/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

/**
 * MenuItemRow.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  9:23:11 AM Mar 16, 2015
 */
public class MenuItemRow extends LinearLayout {

	OnEventControlListener listener;
	Context mContext;
	LinearLayout llRow;
	public TextView tvText;
	public ImageView ivMenuIcon;
	public ImageView ivExpand;
	
	public MenuItemRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initMenu();
	}

	public MenuItemRow(Context context, View row) {
		super(context);
		mContext = context;
		initMenu(row);
	}

	private void initMenu() {
		LayoutInflater vi = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = vi.inflate(R.layout.layout_fragment_menu_item, this, false);
		
		tvText = (TextView) view.findViewById(R.id.tvText);
		llRow = (LinearLayout) view.findViewById(R.id.llRow);
		// Neu la item con thi se bi null
		ivExpand = (ImageView) view.findViewById(R.id.ivExpand);
		ivMenuIcon = (ImageView) view.findViewById(R.id.ivMenuIcon);
	}
	
	private void initMenu(View row) {
		llRow = (LinearLayout) row.findViewById(R.id.llRow);
		tvText = (TextView) row.findViewById(R.id.tvText);
		// Neu la item con thi se bi null
		ivExpand = (ImageView) row.findViewById(R.id.ivExpand);
		ivMenuIcon = (ImageView) row.findViewById(R.id.ivMenuIcon);
	}

	public void populateFrom(MenuItemDTO item) {
		if(item != null){
			tvText.setText(item.getTextMenu());
			ivMenuIcon.setImageResource(item.getIconMenu());
/*			if (item.getIconMenu() != 0) {
				tvText.setCompoundDrawablesWithIntrinsicBounds(item.getIconMenu(), 0, 0, 0);
			} else {
				tvText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_app, 0, 0, 0);
			}*/
			
			if(!item.isChild){
				if(item.getItems() != null && item.getItems().size() > 0){
					if(ivExpand != null){
						ivExpand.setVisibility(VISIBLE); 
					}
				}else{
					if(ivExpand != null){
						ivExpand.setVisibility(INVISIBLE);
					}
				}
			} 
			
			if (item.isSelected) {
				if (item.isChild) {
					llRow.setBackgroundColor(ImageUtil
							.getColor(R.color.TRANSPARENT));
					if (ivExpand != null) {
						ivExpand.setImageResource(R.drawable.ic_navigation_next_item);
						ivExpand.setVisibility(VISIBLE);
					}
				} else {
					if (item.isRefresh) {
						item.isExpand = item.isExpand ? false : true;
						item.isRefresh = false;
					}
					if (ivExpand != null) {
						if (item.isExpand) {
							ivExpand.setImageResource(R.drawable.ic_navigation_collapse);
						} else {
							ivExpand.setImageResource(R.drawable.ic_navigation_expand);
						}
					}
					llRow.setSelected(true);
				}
			} else {
				if (item.isChild) {
					llRow.setBackgroundColor(ImageUtil.getColor(R.color.TRANSPARENT));
					if(ivExpand != null){
						ivExpand.setVisibility(INVISIBLE);
					}
				} else{
					item.isExpand = false;
					if (ivExpand != null) {
						ivExpand.setImageResource(R.drawable.ic_navigation_expand);
					}
				}
			}
		}
	}
	
	public void setBackground(int res){
		if(llRow != null)
			llRow.setBackgroundResource(res);
	}

}
