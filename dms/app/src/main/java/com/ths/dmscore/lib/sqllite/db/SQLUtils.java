/**
 * Copyright 2011 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.BuyInfoLevel;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.CustomerStockHistoryDTO;
import com.ths.dmscore.dto.db.DisplayProgrameLvDTO;
import com.ths.dmscore.dto.db.EquipLostMobileRecDTO;
import com.ths.dmscore.dto.db.KSCusProductRewardDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.PromotionStaffMapDTO;
import com.ths.dmscore.dto.db.RptCttlDetailPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.db.ToDoTaskDTO;
import com.ths.dmscore.dto.view.*;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateResultDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.FromViewConstants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.BigDecimalRound;
import com.ths.dmscore.dto.NotifyDataDTO;
import com.ths.dmscore.dto.NotifyOrderDTO;
import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO.TableType;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.db.DebitDetailDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.EquipAttachFileDTO;
import com.ths.dmscore.dto.db.EquipmentFormHistoryDTO;
import com.ths.dmscore.dto.db.EquipmentHistoryDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedBackDetailDTO;
import com.ths.dmscore.dto.db.FeedBackTBHVDTO;
import com.ths.dmscore.dto.db.FeedbackStaffCustomerDTO;
import com.ths.dmscore.dto.db.FeedbackStaffDTO;
import com.ths.dmscore.dto.db.GSNPPTrainingPlanDTO;
import com.ths.dmscore.dto.db.GroupLevelDTO;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.db.KeyShopListDTOView;
import com.ths.dmscore.dto.db.ListProductDTO;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.MediaLogDTO;
import com.ths.dmscore.dto.db.POCustomerPromoMapDTO;
import com.ths.dmscore.dto.db.PayReceivedDTO;
import com.ths.dmscore.dto.db.PaymentDetailDTO;
import com.ths.dmscore.dto.db.PriceTempDTO;
import com.ths.dmscore.dto.db.ProductDTO;
import com.ths.dmscore.dto.db.PromotionCustomerMapDTO;
import com.ths.dmscore.dto.db.PromotionProductConvDtlDTO;
import com.ths.dmscore.dto.db.PromotionProductConvertDTO;
import com.ths.dmscore.dto.db.PromotionShopMapDTO;
import com.ths.dmscore.dto.db.QuantityRevicevedDTO;
import com.ths.dmscore.dto.db.ReportTemplateCriterionDTO;
import com.ths.dmscore.dto.db.RoutingCustomerDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderLotDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoLotDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.db.SalePromoMapDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.db.ShopLockDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.db.StaffPositionLogDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;
import com.ths.dmscore.dto.db.SynTabledataLogDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.db.TakePhotoEquipmentDTO;
import com.ths.dmscore.dto.db.TrainingShopManagerResultDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingIssueDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingPlanDTO;
import com.ths.dmscore.dto.map.GeomDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.AreaItem;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO.StaffItemDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSubTrainingPlanDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSupTrainingIssueDTO;
import com.ths.dmscore.dto.view.trainingplan.ListTrainingRateDTO;
import com.ths.dmscore.dto.view.trainingplan.TBHVDayTrainingSupervisionDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateDetailResultlDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.MeasuringTime;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriDto;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.LoginView;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;
import com.ths.dms.R;

/**
 * Cac ham truy xuat contact trong SQLLite
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressLint("UseSparseArrays")
public class SQLUtils {
	private static final String LOG_TAG = "SQLUtils";
	private static volatile SQLiteDatabase mDB;
	private static volatile SQLUtils instance = null;
	public boolean isProcessingTrans = false;

	private static final Object lockObject = new Object();

	public static SQLUtils getInstance() {
		if (instance == null) {
			synchronized (lockObject) {
				if (instance == null) {
					instance = new SQLUtils();
				}
			}
		}
		if (mDB == null || !mDB.isOpen()) {
			synchronized (lockObject) {
				if (mDB == null || !mDB.isOpen()) {
					try {
						SharedPreferences sharedPreferences = GlobalInfo
								.getInstance().getDmsPrivateSharePreference();
						String roleId = sharedPreferences.getString(
								LoginView.VNM_ROLE_ID, "");
						String shopId = sharedPreferences.getString(
								LoginView.VNM_SHOP_ID, "");
						SQLiteDatabaseHook hook = new SQLiteDatabaseHook() {
							@Override
							public void preKey(SQLiteDatabase db) {
								db.rawExecSQL(String.format(
										"PRAGMA key = '%s'",
										Constants.CIPHER_KEY));
								db.rawExecSQL("PRAGMA cipher = 'rc4'");
							}

							@Override
							public void postKey(SQLiteDatabase db) {
								db.rawExecSQL(String.format(
										"PRAGMA key = '%s'",
										Constants.CIPHER_KEY));
								db.rawExecSQL("PRAGMA cipher = 'rc4'");
							}
						};
						mDB = SQLiteDatabase.openDatabase(
								ExternalStorage.getFileDBPath(
										GlobalInfo.getInstance()
												.getAppContext())
										.getAbsolutePath()
										+ "/"
										+ roleId
										+ "_"
										+ shopId
										+ "_"
										+ Constants.DATABASE_NAME,
								Constants.CIPHER_KEY, null,
								SQLiteDatabase.OPEN_READWRITE, hook);
						// mDB.execSQL("PRAGMA journal_mode = OFF");
						// mDB.execSQL("PRAGMA page_size = 5120");
						// mDB.execSQL("analyze;");
					} catch (Exception e) {
						MyLog.e("getInstance", "fail", e);
					} finally {
					}
				}
			}
		}
		return instance;
	}

	public SQLiteDatabase getmDB() {
		return mDB;
	}

	// ------------PRIVATE------------
	/*
	 * cai dat cay phan nhanh thuc thi cac nghiep vu DB cho table
	 */
	/**
	 * Insert dto (value of row) to table
	 *
	 * @author: TruongHN
	 * @return: long
	 * @throws:
	 */
	private long insertDTO(AbstractTableDTO tableDTO) {
		long res = -1;
		if (AbstractTableDTO.TableType.CUSTOMER.equals(tableDTO.getType())) {
			CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER.equals(tableDTO
				.getType())) {
			SALE_ORDER_TABLE table = new SALE_ORDER_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER_DETAIL.equals(tableDTO
				.getType())) {
			SALES_ORDER_DETAIL_TABLE table = new SALES_ORDER_DETAIL_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.PRODUCT
				.equals(tableDTO.getType())) {
			PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(tableDTO
				.getType())) {
			FEED_BACK_TABLE table = new FEED_BACK_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.FEEDBACK_DETAIL_TABLE
				.equals(tableDTO.getType())) {
			FEEDBACK_DETAIL_TABLE table = new FEEDBACK_DETAIL_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.LOG.equals(tableDTO.getType())) {
			LOG_TABLE table = new LOG_TABLE(mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.TRAINING_SHOP_MANAGER_RESULT_TABLE
				.equals(tableDTO.getType())) {
			TRAINING_SHOP_MANAGER_RESULT_TABLE table = new TRAINING_SHOP_MANAGER_RESULT_TABLE(
					mDB);
			res = table.insert(tableDTO);
		} else if (AbstractTableDTO.TableType.CUSTOMER_STOCK_HISTORY_TABLE
				.equals(tableDTO.getType())) {
			CUSTOMER_STOCK_HISTORY_TABLE table = new CUSTOMER_STOCK_HISTORY_TABLE(
					mDB);
			res = table.insert(tableDTO);
		}
		return res;
	}

	/**
	 * Update dto (value of row) to table
	 *
	 * @author: TruongHN
	 * @param tableDTO
	 * @return: long (-1 neu that bai)
	 * @throws:
	 */
	private long updateDTO(AbstractTableDTO tableDTO) {
		long res = -1;
		if (AbstractTableDTO.TableType.CUSTOMER.equals(tableDTO.getType())) {
			CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER.equals(tableDTO
				.getType())) {
			SALE_ORDER_TABLE table = new SALE_ORDER_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER_DETAIL.equals(tableDTO
				.getType())) {
			SALES_ORDER_DETAIL_TABLE table = new SALES_ORDER_DETAIL_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.PRODUCT
				.equals(tableDTO.getType())) {
			PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.CUSTOMER_DISPLAY_PROGRAME_SCORE
				.equals(tableDTO.getType())) {
			CUSTOMER_DISPLAY_PROGRAME_SCORE_TABLE table = new CUSTOMER_DISPLAY_PROGRAME_SCORE_TABLE(
					mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(tableDTO
				.getType())) {
			FEED_BACK_TABLE table = new FEED_BACK_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.TRAINING_RESULT_TABLE
				.equals(tableDTO.getType())) {
			TRAINING_RESULT_TABLE table = new TRAINING_RESULT_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.LOG.equals(tableDTO.getType())) {
			LOG_TABLE table = new LOG_TABLE(mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.EQUIP_STATISTIC_REC_DTL_TABLE
				.equals(tableDTO.getType())) {
			EQUIP_STATISTIC_REC_DTL_TABLE table = new EQUIP_STATISTIC_REC_DTL_TABLE(
					mDB);
			res = table.update(tableDTO);
		} else if (AbstractTableDTO.TableType.EQUIPMENT_TABLE.equals(tableDTO
				.getType())) {
			EQUIPMENT_TABLE table = new EQUIPMENT_TABLE(mDB);
			res = table.updateLostMobile(tableDTO);
		}
		return res;
	}

	/**
	 * Delete 1 DTO
	 *
	 * @author: TruongHN
	 * @param tableDTO
	 * @return: long (-1 neu that bai)
	 * @throws:
	 */
	private long deleteDTO(AbstractTableDTO tableDTO) {
		long res = -1;
		if (AbstractTableDTO.TableType.CUSTOMER.equals(tableDTO.getType())) {
			CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER.equals(tableDTO
				.getType())) {
			SALE_ORDER_TABLE table = new SALE_ORDER_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.SALE_ORDER_DETAIL.equals(tableDTO
				.getType())) {
			SALES_ORDER_DETAIL_TABLE table = new SALES_ORDER_DETAIL_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.PRODUCT
				.equals(tableDTO.getType())) {
			PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.TRAINING_RESULT_TABLE
				.equals(tableDTO.getType())) {
			TRAINING_RESULT_TABLE table = new TRAINING_RESULT_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(tableDTO
				.getType())) {
			FEED_BACK_TABLE table = new FEED_BACK_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.FEEDBACK_DETAIL_TABLE
				.equals(tableDTO.getType())) {
			FEEDBACK_DETAIL_TABLE table = new FEEDBACK_DETAIL_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.LOG.equals(tableDTO.getType())) {
			LOG_TABLE table = new LOG_TABLE(mDB);
			res = table.delete(tableDTO);
		} else if (AbstractTableDTO.TableType.TRAINING_SHOP_MANAGER_RESULT_TABLE
				.equals(tableDTO.getType())) {
			TRAINING_SHOP_MANAGER_RESULT_TABLE table = new TRAINING_SHOP_MANAGER_RESULT_TABLE(
					mDB);
			res = table.delete(tableDTO);
		}
		return res;
	}

	/**
	 * Lay doanh so theo nganh hang cua KH trong 3 thang gan nhat + thang hien
	 * tai
	 *
	 * @author: BANGHN
	 * @param shopOwnerID
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public CustomerCatAmountPopupViewDTO getCustomerCatAmountInNearly3Month(
			Bundle data) throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.getCustomerCatAmountInNearly3Month(data);
	}

	/**
	 * Lay album cua gsnpp
	 *
	 * @author: Tuanlt11
	 * @param bundle
	 * @return
	 * @return: ImageSearchViewDTO
	 * @throws Exception
	 * @throws:
	 */
//	public ImageSearchViewDTO getListAlbumGSNPP(Bundle bundle) throws Exception {
//		ImageSearchViewDTO dto = new ImageSearchViewDTO();
//		boolean isAll = bundle.getBoolean(IntentConstants.INTENT_IS_ALL, false);
//		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
//
//		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
//		DISPLAY_PROGRAME_TABLE programeTable = new DISPLAY_PROGRAME_TABLE(mDB);
//		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
//		dto = custommerTable.getImageSearchListGSNPP(bundle);
//		if (isAll) {
//			DisplayProgrameModel listDisplay = new DisplayProgrameModel();
//			listDisplay = programeTable.getListDisplayProgrameImage(bundle);
//
//			if (listDisplay != null & listDisplay.getModelData() != null) {
//				dto.listPrograme.addAll(listDisplay.getModelData());
//			}
//		}
//		dto.listShop.add/*All(listShop);
//		ArrayList<StaffChoosenDTO> listStaff = staffTable
//				.getSupervisorSearchImageListStaff(bundle);
//		dto.listStaff = listStaff;*/
//
//		return dto;
//	}

	public ImageSearchViewDTO getListAlbumGSNPP(Bundle bundle) throws Exception {
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		return custommerTable.getImageSearchListGSNPP(bundle);
	}


	/**
	 * Thuc thi mot mot hanh dong insert, update, delete mot doi tuong vao bang
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long (-1 neu that bai)
	 * @throws:
	 */
	private long execute(AbstractTableDTO dto) {
		long res = -1;
		if (AbstractTableDTO.TableAction.INSERT.equals(dto.getAction())) {
			res = insertDTO(dto);
		}
		if (AbstractTableDTO.TableAction.UPDATE.equals(dto.getAction())) {
			res = updateDTO(dto);
		}
		if (AbstractTableDTO.TableAction.DELETE.equals(dto.getAction())) {
			res = deleteDTO(dto);
		}
		return res;
	}

	// ------------PUBLIC-------------//
	/**
	 * Insert dto (value of row) to table
	 *
	 * @author: BangHN
	 * @param tableDTO
	 * @return: void
	 * @throws:
	 */
	public synchronized void insert(AbstractTableDTO tableDTO) {
		if (mDB != null && tableDTO != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				insertDTO(tableDTO);
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					try {
						mDB.endTransaction();
					} catch (Exception e) {
						ServerLogger.sendLog("SQLUtils.insert",
								VNMTraceUnexceptionLog
										.getReportFromThrowable(e), true,
								TabletActionLogDTO.LOG_EXCEPTION);
					}
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
	}

	/**
	 * Insert dto (value of row) to table
	 *
	 * @author : BangHN since : 11:52:29 AM
	 */
	public synchronized void update(AbstractTableDTO tableDTO) {
		if (mDB != null && tableDTO != null && mDB.isOpen() && !mDB.isReadOnly()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				updateDTO(tableDTO);
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
	}

	/**
	 * Delelte 1 doi tuong
	 *
	 * @author: TruongHN
	 * @param tableDTO
	 * @return: long (-1 neu that bai)
	 * @throws:
	 */
	public synchronized long delete(AbstractTableDTO tableDTO) {
		long res = -1;
		if (mDB != null && tableDTO != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				res = deleteDTO(tableDTO);
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
		return res;
	}

	/**
	 * lay bao cao chi tiet doanh so ngay cua KH
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public SupervisorReportStaffSaleViewDTO getSupervisorCatAmountReport(
			Bundle data) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		SupervisorReportStaffSaleViewDTO result = table
				.getSupervisorCatAmountReport(data);
		return result;
	}

	/**
	 * Thuc thi mot mot hanh dong insert, update, delete mot doi tuong vao bang
	 *
	 * @author: BangHN
	 * @param dto
	 * @return: long (-1 neu that bai)
	 * @throws:
	 */
	public synchronized long executeDTO(AbstractTableDTO dto) {
		long res = -1;
		if (mDB != null && dto != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				res = execute(dto);
				mDB.setTransactionSuccessful();

			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
		return res;
	}

	/**
	 * Thuc thi mot mot hanh dong insert, update, delete mot doi tuong vao bang
	 *
	 * @author: TruongHN
	 * @param listDTO
	 * @return: void
	 * @throws:
	 */
	public synchronized void executeListDTO(ArrayList<AbstractTableDTO> listDTO) {
		if (mDB != null && listDTO != null && mDB.isOpen() && !mDB.isReadOnly()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				for (AbstractTableDTO dto : listDTO) {
					execute(dto);
				}
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
	}

	/**
	 * Thuc hien 1 cau lenh sql tong quat
	 *
	 * @author: TruongHN
	 * @param sqlQuery
	 * @param params
	 * @return
	 * @return: Cursor
	 * @throws:
	 */
	public Cursor rawQuery(String sqlQuery, String[] params) {
		Cursor cursor = null;
		if (mDB != null && mDB.isOpen()) {
			cursor = mDB.rawQuery(sqlQuery, params);
		}
		return cursor;
	}

	/**
	 * Danh sach hinh anh cua TBHV
	 *
	 * @author: TruongHN
	 * @param shopId
	 * @param numPerPage
	 * @param offset
	 * @return: TBHVListImageDTOView
	 * @throws:
	 */
	public TBHVListImageDTOView getListImageOfTBHV(String shopId,
												   int numPerPage, int offset) throws Exception {
		TBHVListImageDTOView listData = null;
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		listData = shopTable.getListImageOfTBHV(shopId, numPerPage, offset);
		return listData;
	}

	/**
	 * Xoa du lieu tu cau lenh sql
	 *
	 * @author: TruongHN
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 * @return: int
	 * @throws:
	 */
	public int delete(String tableName, String whereClause, String[] whereArgs) {
		int delete = -1;
		if (mDB != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				delete = mDB.delete(tableName, whereClause, whereArgs);
				MyLog.i(LOG_TAG, "delete: " + String.valueOf(delete));
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
		return delete;
	}

	/**
	 * Xoa du lieu tu cau lenh sql, roi xoa luon bang con tham chieu
	 *
	 * @author: PhucNT
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 * @return: int
	 * @throws:
	 */
	private boolean deleteCascade(String tableName, String whereClause,
			String[] whereArgs, String[] childTables) {
		boolean result = true;

		int delete = mDB.delete(tableName, whereClause, whereArgs);

		MyLog.i(LOG_TAG, "delete bang cha" + String.valueOf(delete));
		if (delete > 0) {
			for (int i = 0; i < childTables.length; i++) {
				String tbChild = childTables[i];
				int delete2 = mDB.delete(tbChild, whereClause, whereArgs);
				MyLog.i(LOG_TAG, "Delete bang con" + String.valueOf(delete2));

				if (delete2 <= 0) {
					result = false;
					break;
				}
			}
		} else {
			result = false;
		}

		return result;
	}

	/**
	 * Update table tu cau lenh sql
	 *
	 * @author: TruongHN
	 * @param tableName
	 * @param values
	 * @param whereClause
	 * @param whereArgs
	 * @return: int
	 * @throws:
	 */
	public int update(String tableName, ContentValues values,
			String whereClause, String[] whereArgs) {
		int upda = -1;
		if (mDB != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				upda = mDB.update(tableName, values, whereClause, whereArgs);
				MyLog.i(LOG_TAG, "Truong, update - " + String.valueOf(upda));
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}
		return upda;
	}

	/**
	 * Insert thong tin tu cau lenh sql
	 *
	 * @author: TruongHN
	 * @param nameTable
	 * @param nullColumnHack
	 * @param values
	 * @return: long
	 * @throws:
	 */
	public long insert(String nameTable, String nullColumnHack,
			ContentValues values) {
		long insert = -1;
		if (mDB != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				insert = mDB.insert(nameTable, nullColumnHack, values);
				MyLog.i(LOG_TAG, "Truong, insert - " + String.valueOf(insert));
				mDB.setTransactionSuccessful();
			} finally {
				if (mDB != null && mDB.inTransaction()) {
					mDB.endTransaction();
				}
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}

		}
		return insert;
	}

	/**
	 * Close DB
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void closeDB() {
		try {
			if (mDB != null) {
				if (mDB.inTransaction()) {
					mDB.endTransaction();
				}
				if (mDB.isOpen()) {
					mDB.close();
				}
				mDB = null;
			}
		} catch (Exception e) {
			MyLog.e("closeDB", e);
		}
	}

	/**
	 * ------------------------------------------------------------------------
	 * ------------- CAI DAT CAC FUNCTION PHUC VU VIEC LAY DU LIEU TU NGHIEP VU
	 * -- ----------------------------------------------------------------------
	 * -- -----------
	 */

	/**
	 * Lay thong tin customer trong : CUSTOMER & CUSTOMER_TYPE
	 *
	 * @author banghn
	 */
	public CustomerInfoDTO getCustomerInfo(String customerId, String shopId,
			int sysCalUnApproved, int sysCurrencyDivide) throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		return cusTable.getCustomerInfo(customerId, shopId, sysCalUnApproved, sysCurrencyDivide);
	}

	/**
	 * lay thong tin cua khach hang
	 *
	 * @author banghn
	 * @param ap_param_id
	 *            cua khach hang since : 1.0
	 * @throws Exception
	 */
	public CustomerDTO getCustomerById(String customerId) throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		CustomerDTO customerDTO = cusTable.getCustomerById(customerId);
		return customerDTO;
	}

	/**
	 * Lay thong tin album user cua cttb
	 *
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @return: ListAlbumUserDTO
	 * @throws:
	 */
	public ListAlbumUserDTO getAlbumUserInfoCTTB(Bundle data) throws Exception {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		ACTION_LOG_TABLE actionLogTable = new ACTION_LOG_TABLE(mDB);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		// request lay danh sach hinh anh cua user
		ListAlbumUserDTO albumInfo = null;
		albumInfo = mediaItemTable.getAlbumUserInfo(customerId, shopId);
		albumInfo.visitType = SQLUtils.getInstance().checkStateCutomerVisit(
				customerId, staffId, shopId);

		albumInfo.monthSeq = actionLogTable.getMonthSeqVisitCustomer(data);
		if (albumInfo.visitType == ListAlbumUserDTO.TYPE_IS_OR) {
			if (albumInfo.monthSeq == 0) {
				albumInfo.monthSeq = 1;
			}
		} else if (albumInfo.visitType == ListAlbumUserDTO.TYPE_IS_VISITED) {
			albumInfo.monthSeq += 1;
		}
		// request combobox
		boolean checkRequestCombobox = data.getBoolean(
				IntentConstants.INTENT_CHECK_COMBOBOX, true);
		if (checkRequestCombobox) {
			DISPLAY_PROGRAME_TABLE programeTable = new DISPLAY_PROGRAME_TABLE(
					mDB);
			DisplayProgrameModel listDisplay = programeTable
					.getListDisplayProgrameImage(data);
			if (listDisplay != null && listDisplay.getModelData() != null) {
				List<DisplayProgrameDTO> listDPDTO = listDisplay.getModelData();
				ArrayList<DisplayProgrameItemDTO> listDPItemDTO = new ArrayList<DisplayProgrameItemDTO>();
				for (int i = 0, size = listDPDTO.size(); i < size; i++) {
					DisplayProgrameDTO displayProgrameDTO = listDPDTO.get(i);
					DisplayProgrameItemDTO displayProgrameItemDTO = new DisplayProgrameItemDTO();
					displayProgrameItemDTO.name = displayProgrameDTO.displayProgrameName;
					displayProgrameItemDTO.fullName = displayProgrameDTO.displayProgrameCode;
					displayProgrameItemDTO.value = displayProgrameDTO.displayProgrameCode;
					listDPItemDTO.add(displayProgrameItemDTO);
				}
				albumInfo.setListDisplayPrograme(listDPItemDTO);
			} else {
				albumInfo.setListDisplayPrograme(null);
			}
			DisplayProgrameModel listPhotoDPPrograme = programeTable
					.getListDisplayProgrameWhenTakePhoto(data);
			if (listPhotoDPPrograme != null
					&& listPhotoDPPrograme.getModelData() != null) {
				albumInfo
						.setListPhotoDPrograme((ArrayList<DisplayProgrameDTO>) listPhotoDPPrograme
								.getModelData());
			}
		}

		// request hinh anh cho CTTB
		ArrayList<AlbumDTO> arrayListPrograme = mediaItemTable
				.getAlbumProgramInfoCTTB("", customerId, "", "", shopId,
						staffId);
		if (arrayListPrograme != null && arrayListPrograme.size() > 0) {
			albumInfo.setListProgrameAlbum(arrayListPrograme);
		} else {
			albumInfo.setListProgrameAlbum(null);
		}

		if (albumInfo != null) {
			SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
			albumInfo.shopDistance = shopTable.getShopDistance(data
					.getString(IntentConstants.INTENT_SHOP_ID));
		}

		return albumInfo;
	}

	/**
	 * get init cac textview de thuc hien autoComplete
	 *
	 * @author: HieuNH
	 * @param ext
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public AutoCompleteFindProductSaleOrderDetailViewDTO getInitListProductAddToOrderView(
			Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);

		AutoCompleteFindProductSaleOrderDetailViewDTO vt = saleOrderDetailTable
				.getInitListProductAddToOrderView(ext);
		return vt;
	}

	/**
	 * get list product to add order list
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public ListFindProductSaleOrderDetailViewDTO getListProductAddToOrderView(
			Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);

		ListFindProductSaleOrderDetailViewDTO vt = saleOrderDetailTable
				.getListProductAddToOrderView(ext);
		return vt;
	}

	/**
	 * Lay doanh so sku khach hang mua trong thang
	 *
	 * @author: BANGHN
	 * @param inheritShopId
	 * @param customerId
	 * @return
	 */
	public ArrayList<CustomerSaleSKUDTO> requestGetCustomerSaleSKUInMonth(Bundle data) throws Exception{
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.getCustomerSaleSKUInMonth(data);
	}

	/**
	 *
	 * get list display programe product
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public List<DisplayPresentProductInfo> getListDisplayProgramProduct(
			Bundle ext) {
		// sale order detail - lay ds order detail
		DISPLAY_CUSTOMER_MAP_TABLE customerDisplayPrograme = new DISPLAY_CUSTOMER_MAP_TABLE(
				mDB);

		List<DisplayPresentProductInfo> vt = new ArrayList<DisplayPresentProductInfo>();
		vt = customerDisplayPrograme.getListDisplayProgramProduct(ext);
		return vt;
	}

	/**
	 *
	 * lay thong tin cac chuong trinh trung bay va d/s san pham de cham trung
	 * bay
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: VoteDisplayPresentProductViewDTO
	 * @throws:
	 */
	public VoteDisplayPresentProductViewDTO getVoteDisplayProgrameProductViewData(
			Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		CUSTOMER_DISPLAY_PROGRAME_TABLE customerDisplayPrograme = new CUSTOMER_DISPLAY_PROGRAME_TABLE(
				mDB);

		VoteDisplayPresentProductViewDTO vt = new VoteDisplayPresentProductViewDTO();
		vt = customerDisplayPrograme.getVoteDisplayPresentProductView(ext);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		vt.shopDistance = shopTable.getShopDistance(ext
				.getString(IntentConstants.INTENT_SHOP_ID));
		ACTION_LOG_TABLE actionLogTable = new ACTION_LOG_TABLE(mDB);
		vt.monthSeq = actionLogTable.getMonthSeqVisitCustomer(ext) + 1; // +1:
																		// lan
																		// ghe
																		// tham
																		// nay
		MEDIA_ITEM_TABLE mediaTable = new MEDIA_ITEM_TABLE(mDB);
		if (vt.listDisplayProgrameInfo != null
				&& vt.listDisplayProgrameInfo.size() > 0) {
			// lay hinh anh theo tung chuong trinh chung bay
			String displayProgrameId = vt.listDisplayProgrameInfo.get(0).displayProgrameID;
			String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
			String customerId = ext
					.getString(IntentConstants.INTENT_CUSTOMER_ID);
			int objectType = ext.getInt(IntentConstants.INTENT_OBJECT_TYPE);
			String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);
			vt.listImageDisplay = mediaTable.getListImageCTTB(customerId,
					objectType, displayProgrameId, shopId, staffId);
		}

		return vt;

	}

	/**
	 *
	 * get list vote display product
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: List<DisplayPresentProductInfo>
	 * @throws:
	 */
	public VoteDisplayPresentProductViewDTO getListVoteDisplayProduct(Bundle ext)
			throws Exception {
		// sale order detail - lay ds order detail
		CUSTOMER_DISPLAY_PROGRAME_TABLE customerDisplayPrograme = new CUSTOMER_DISPLAY_PROGRAME_TABLE(
				mDB);
		MEDIA_ITEM_TABLE mediaTable = new MEDIA_ITEM_TABLE(mDB);
		VoteDisplayPresentProductViewDTO vt = new VoteDisplayPresentProductViewDTO();
		vt = customerDisplayPrograme.getListVoteDisplayProduct(ext);

		String displayProgrameId = ext
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = ext.getString(IntentConstants.INTENT_CUSTOMER_ID);
		int objectType = ext.getInt(IntentConstants.INTENT_OBJECT_TYPE);
		boolean isGetTotal = ext
				.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);
		String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);

		if (isGetTotal) {
			vt.listImageDisplay = mediaTable.getListImageCTTB(customerId,
					objectType, displayProgrameId, shopId, staffId);
		}
		return vt;
	}

	/**
	 *
	 * inset voted display programe product into DB
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int insertVoteDisplayProgrameProduct(
			ListCustomerDisplayProgrameScoreDTO listDisplayProductVoted) {
		int kq = 0;
		if (listDisplayProductVoted != null) {
			try {
				mDB.beginTransaction();
				boolean checkSuccess = true;
				for (int i = 0, size = listDisplayProductVoted.getListCusDTO()
						.size(); i < size; i++) {
					CustomerStockHistoryDTO displayProduct = listDisplayProductVoted
							.getListCusDTO().get(i);
					checkSuccess = this.insertDTO(displayProduct) >= 0 ? true
							: false;
					if (!checkSuccess) {
						break;
					}
				}

				// this.updateMaxIdForCustomerDisplayPrograme(listDisplayProductVoted.maxCustomerDisplayProgID);
				if (checkSuccess) {
					mDB.setTransactionSuccessful();
					kq = 1;
				}
			} catch (Exception e) {
				MyLog.e("insertVoteDisplayProgrameProduct", "fail", e);
				kq = 0;
			} finally {
				mDB.endTransaction();
			}
		}
		return kq;
	}

	/**
	 *
	 * update id for list customer display programe score
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ListCustomerDisplayProgrameScoreDTO
	 * @throws:
	 */
	public ListCustomerDisplayProgrameScoreDTO updateCustomerDisplayProgrameScoreDTO(
			ListCustomerDisplayProgrameScoreDTO listDisplayProductVoted) {
		TABLE_ID tableId = new TABLE_ID(mDB);
		if (listDisplayProductVoted != null) {
			try {
				long cdpsMaxId = tableId
						.getMaxIdTime(CUSTOMER_STOCK_HISTORY_TABLE.TABLE_NAME);
				for (int i = 0, size = listDisplayProductVoted.getListCusDTO()
						.size(); i < size; i++) {
					listDisplayProductVoted.getListCusDTO().get(i).customerStockHistoryId = cdpsMaxId;
					cdpsMaxId++;
				}
			} catch (Exception e) {
				MyLog.e("updateCustomerDisplayProgrameScoreDTO", "fail", e);
			}
		}
		return listDisplayProductVoted;
	}

	/**
	 * Lay ds chuong trinh KM
	 * @author: hoanpd1
	 * @since: 14:14:51 24-03-2015
	 * @return: PromotionProgrameModel
	 * @throws:
	 * @param inheritShopId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @throws Exception
	 */
	public PromotionProgrameModel getListPromotionPrograme(Bundle bundle) throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProgrameTable = new PROMOTION_PROGRAME_TABLE(mDB);
		return promotionProgrameTable.getListPromotionPrograme(bundle);
	}

	/**
	 * Lay ds chuong trinh KM cua GS
	 * @author: hoanpd1
	 * @since: 14:14:51 24-03-2015
	 * @return: PromotionProgrameModel
	 * @throws:
	 * @param inheritShopId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @throws Exception
	 */
	public PromotionProgrameModel getListSupervisorPromotionPrograme(Bundle bundle) throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProgrameTable = new PROMOTION_PROGRAME_TABLE(mDB);
		return promotionProgrameTable.getListSupervisorPromotionPrograme(bundle);
	}

	/**
	 *
	 * Lay ds chuong trinh KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: TBHVPromotionProgrameModel
	 * @throws:
	 */
	public TBHVPromotionProgrameDTO getTBHVListPromotionPrograme(String shopId,
																 String ext, boolean checkLoadMore) throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProgrameTable = new PROMOTION_PROGRAME_TABLE(
				mDB);

		return promotionProgrameTable.getListTBHVPromotionPrograme(shopId, ext,
				checkLoadMore);
	}

	/**
	 * getListDisplayProgrameItem
	 *
	 * @author: ThuatTQ
	 * @param ext
	 * @return: List<DisplayProgrameModel>
	 * @throws:
	 */
	public DisplayProgrameItemModel getListDisplayProgrameItem(Bundle ext) {

		DISPLAY_PROGRAME_PRODUCT_TABLE displayProductTable = new DISPLAY_PROGRAME_PRODUCT_TABLE(
				mDB);
		return displayProductTable.getListDisplayProgrameItem(ext);
	}

	/**
	 * lay bao cao chi tiet doanh so ngay cua KH
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public SupervisorReportStaffSaleViewDTO getTBHVTotalCatAmountReport(
			Bundle data) throws Exception{
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		SupervisorReportStaffSaleViewDTO result = table
				.getTBHVTotalCatAmountReport(data);
		return result;
	}

	/**
	 * getListDisplayProgrameItem
	 *
	 * @author: SoaN
	 * @param ext
	 * @return: List<DisplayProgrameModel>
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayPrograme(Bundle ext)
			throws Exception {
		DISPLAY_PROGRAME_TABLE promotionProgrameTable = new DISPLAY_PROGRAME_TABLE(
				mDB);
		return promotionProgrameTable.getListDisplayPrograme(ext);
	}

	/**
	 * Lay ds sale order de chinh sua
	 *
	 * @author: BangHN
	 * @param orderId
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @throws Exception
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ListSaleOrderDTO getSaleOrderForEdit(long orderId, long customerId, String shopId, String staffId) throws Exception {
		ListSaleOrderDTO list = new ListSaleOrderDTO();

		SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
		SaleOrderDTO saleOrderDTO = saleOrderTable.getSaleById(orderId);
		if (saleOrderDTO != null) {
			list.saleOrderDTO = saleOrderDTO;
			SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
					mDB);
			List<OrderDetailViewDTO> listProduct = orderDetailTable
					.getListProductForEdit(Long.toString(orderId),
							saleOrderDTO.orderType,customerId,shopId,staffId);

			list.listData.addAll(listProduct);
		}
		return list;
	}

	/**
	 * Lay ds sale order goc
	 *
	 * @author: DungNX
	 * @param poCustomerId
	 * @param staffId
	 * @param shopId
	 * @throws Exception
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ListSaleOrderDTO getPO(long poCustomerId, long customerId, String shopId, String staffId) throws Exception {
		ListSaleOrderDTO list = new ListSaleOrderDTO();

		PO_CUSTOMER_TABLE poCustomerTable = new PO_CUSTOMER_TABLE(mDB);
		SaleOrderDTO saleOrderDTO = poCustomerTable.getSaleById(poCustomerId);
		if (saleOrderDTO != null) {
			list.saleOrderDTO = saleOrderDTO;
			PO_CUSTOMER_DETAIL_TABLE poCustomerDetailTable = new PO_CUSTOMER_DETAIL_TABLE(
					mDB);

			List<OrderDetailViewDTO> listProduct = poCustomerDetailTable
					.getListProductForEdit(Long.toString(poCustomerId),
							saleOrderDTO.orderType,customerId,shopId,staffId);

			list.listData.addAll(listProduct);
		}

		return list;
	}

	/**
	 * Lay ds hinh anh cua CTTB
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public ArrayList<MediaItemDTO> getListImageCTTB(String customerId,
			int objectType, String displayProgrameId, String shopId,
			String staffId) throws Exception {
		MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
		return table.getListImageCTTB(customerId, objectType,
				displayProgrameId, shopId, staffId);
	}

	/**
	 * Lay thong tin KH dat hang
	 *
	 * @author: dungnt19
	 * @since: 15:33:34 21-02-2014
	 * @return: CustomerListItem
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 */
	public CustomerListItem getCustomerInfoVisit(String staffId, String shopId,
												 String customerId) {
		CustomerListItem dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			if (mDB != null) {
				dto = custommerTable.getCustomerInfoVisit(staffId, shopId,
						customerId);
			}

		} catch (Exception e) {
			MyLog.e("getCustomerInfoVisit", "fail", e);
			// ServerLogger.sendLog("getCustomerList", e.getMessage() + " - " +
			// e.toString(),
			// TabletActionLogDTO.LOG_EXCEPTION);
		} finally {
		}
		return dto;
	}

	/**
	 * Lay danh sach log trong 1 khoang thoi gian
	 *
	 * @author quangvt
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public StaffPositionDTO getListLog(String startTime) {
		STAFF_POSITION_LOG_TABLE table = new STAFF_POSITION_LOG_TABLE(mDB);
		return table.getListLog(startTime);
	}

	/**
	 *
	 * Lay ds san pham ban de chuyen len server
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 * @return
	 * @throws Exception
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ListSaleOrderDTO getSaleOrderForSend(long orderId) {
		ListSaleOrderDTO list = new ListSaleOrderDTO();

		SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(SQLUtils
				.getInstance().getmDB());

		try {
			SaleOrderDTO saleOrderDTO = saleOrderTable.getSaleById(orderId);
			if (saleOrderDTO != null) {
				list.saleOrderDTO = saleOrderDTO;

				SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
						mDB);

				List<OrderDetailViewDTO> listProduct = orderDetailTable
						.getListProductForSend(Long.toString(orderId));
				list.listData.addAll(listProduct);
			}
		} catch (Exception e) {
			MyLog.e("getSaleOrderForSend", "fail", e);
		}

		return list;
	}

	/**
	 * Get MaxId trong table khi truyen vao tableName & ten column
	 *
	 * @author: BangHN
	 * @param tableName
	 * @param columnIdName
	 * @throws Exception
	 * @return: int
	 * @throws:
	 */
	public int getMaxIdInTable(String tableName, String columnIdName)
			throws Exception {
		int maxId = -1;
		Cursor cursor = null;
		try {
			StringBuilder sqlState = new StringBuilder();
			sqlState.append("select ifnull(max(" + columnIdName + "), 0) as "
					+ columnIdName + " from " + tableName);
			cursor = rawQuery(sqlState.toString(), null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					maxId = CursorUtil.getInt(cursor, columnIdName);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return maxId;
	}

	/**
	 * Get log da sync cuoi cung
	 *
	 * @author: ThuatTQ
	 * @param staffId
	 * @throws Exception
	 * @return: SynTabledataLogDTO
	 * @throws:
	 */
	public SynTabledataLogDTO getLastSynLog(Integer staffId) throws Exception {
		SYN_TABLEDATA_LOG_TABLE synLog = new SYN_TABLEDATA_LOG_TABLE(mDB);
		return synLog.getLastSynLog(staffId);
	}

	/**
	 * get list product storage
	 *
	 * @author: HieuNH
	 * @return: ListProductDTO
	 * @throws:
	 */
	public ListProductDTO getProductList(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		int isGetTotalPage = ext.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		// chi gen bang temp lan dau
		if(isGetTotalPage == 1)
			genPriceTempForShop(ext);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTable.getShopRecursive(ext
				.getString(IntentConstants.INTENT_SHOP_ID));
		return productTable.getProductList(ext, shopIdArray);
	}

	/**
	 * get list product storage
	 *
	 * @author: HieuNH
	 * @return: ListProductDTO
	 * @throws:
	 */
	public ListProductDTO getSupervisorProductList(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		int isGetTotalPage = ext.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		// chi gen bang temp lan dau
		if(isGetTotalPage == 1)
			genPriceTempForShop(ext);
		ArrayList<String> shopIdArray = shopTable.getShopRecursive(ext
				.getString(IntentConstants.INTENT_SHOP_ID));
		return productTable.getSupervisorProductList(ext, shopIdArray);
	}

	/**
	 * bao cao chi tiet CTTB cua 1 nvbh voi 1 ct
	 *
	 * @author: TampQ
	 * @param dto
	 * @return
	 * @return: int
	 * @throws:
	 */
	public ReportDisplayProgressDetailCustomerDTO getReportDisplayProgressDetailCustomer(
			Bundle data) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getReportDisplayProgressDetailCustomer(data);
	}

	/**
	 *
	 * Lay ds sp cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param ext
	 * @return
	 * @return: ListProductDTO
	 * @throws Exception
	 * @throws:
	 */
	public ListProductDTO getTBHVListProductStorage(Bundle ext)
			throws Exception {
		// sale order detail - lay ds order detail
		PRODUCT_TABLE saleOrderDetailTable = new PRODUCT_TABLE(mDB);
		ListProductDTO dto = null;
		dto = saleOrderDetailTable.getTBHVProductList(ext);
		return dto;
	}

	/**
	 * get list nganh hang, nganh hang con
	 *
	 * @author: HieuNH
	 * @return: CategoryCodeDTO
	 * @throws:
	 */
	public CategoryCodeDTO getListCategoryCodeProduct() {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);

		try {
			CategoryCodeDTO vt = saleOrderDetailTable
					.getListCategoryCodeProduct();
			return vt;

		} catch (Exception e) {
			MyLog.e("getListCategoryCodeProduct", "fail", e);
			return null;
		}
	}

	/**
	 *
	 * get Remain Product
	 *
	 * @author: HieuNH
	 * @param ext
	 * @return
	 * @return: ListProductDTO
	 * @throws Exception
	 * @throws:
	 */
	public ListRemainProductDTO getRemainProduct(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);

		return saleOrderDetailTable.getRemainProduct(ext);
	}

	public Bundle getLastOrdinalRemainProduct(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(mDB);

		return saleOrderDetailTable.getLastOrdinalRemainProduct(ext);
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: SoaN
	 * @param ext
	 * @return
	 * @return: DisplayProgrameDTO
	 * @throws:
	 */

	public DisplayProgrameDTO getDisplayProgrameInfo(Bundle ext) {
		// sale order detail - lay ds order detail
		DISPLAY_PROGRAME_TABLE programe_TABLE = new DISPLAY_PROGRAME_TABLE(mDB);
		String code = ext.getString(IntentConstants.INTENT_PROMOTION_CODE);
		try {
			DisplayProgrameDTO vt = programe_TABLE.getRowById(code);
			return vt;

		} catch (Exception e) {
			MyLog.e("getDisplayProgrameInfo", "fail", e);
			return null;
		}
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: SoaN
	 * @param ext
	 * @return
	 * @return: DisplayProgrameLvDTO
	 * @throws:
	 */

	public List<DisplayProgrameLvDTO> getDisplayLvByProgrameCode(Bundle ext) {
		// sale order detail - lay ds order detail
		DISPLAY_PROGRAME_LEVEL_TABLE level_TABLE = new DISPLAY_PROGRAME_LEVEL_TABLE(
				mDB);
		String code = ext.getString(IntentConstants.INTENT_PROMOTION_CODE);
		try {
			return level_TABLE.getListRowById(code);
		} catch (Exception e) {
			MyLog.e("getDisplayLvByProgrameCode", "fail", e);
			return null;
		}
	}

	/**
	 * Thuc hien lay nhung don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 1:50:41 PM
	 */
	public int getCountLastSaleOrders(String customerId, String shopId,
			String staffId) {
		int count = 0;
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		try {
			count = orderTable.getCountLastSaleOrders(customerId, shopId,
					staffId);
		} catch (Exception e) {
			MyLog.e("getCountLastSaleOrders", "fail", e);
		}
		return count;
	}

	/**
	 *
	 * get promotion programe detail info
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws:
	 */
	public ArrayList<PromotionProgrameDTO> getPromotionProgrameDetail(Bundle ext)
			throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProgrameTable = new PROMOTION_PROGRAME_TABLE(
				mDB);
		ArrayList<PromotionProgrameDTO> vt = promotionProgrameTable
				.getPromotionProgrameDetail(ext);
		return vt;

	}

	/**
	 * Thuc hien lay nhung don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 1:50:41 PM
	 */
	public ArrayList<SaleOrderCustomerDTO> getLastSaleOrders(String customerId,
															 String shopId, int page, int numTop, int sysCurrencyDivide) throws Exception {
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		return orderTable.getLastSaleOrders(customerId, shopId, page, numTop, sysCurrencyDivide);
	}

	/**
	 * Lay chuong trinh khach hang dang tham gia
	 *
	 * @param customerId
	 *            : id khach hang
	 * @param shopId
	 * @return
	 */
	public ArrayList<CustomerProgrameDTO> getCustomerProgrames(
			String customerId, String shopId) {
		CUSTOMER_DISPLAY_PROGRAME_TABLE customerPrograme = new CUSTOMER_DISPLAY_PROGRAME_TABLE(
				mDB);
		return customerPrograme.getCustomerProgrames(customerId, shopId);
	}

	/**
	 * Tinh doanh so chuong trinh khach hang dang tham gia
	 *
	 * @author banghn
	 * @param programId
	 *            : chuong trinh tham gia
	 * @param isFino
	 *            co phai la fino hay ko
	 * @param hasFino
	 *            : chuong trinh nay co fino hay khong
	 * @return doanh so da dat
	 */
	public long caculateAmountAchievedOfPrograme(String customerId,
			String programId, boolean isFino, boolean hasFino) {
		CUSTOMER_DISPLAY_PROGRAME_TABLE customerPrograme = new CUSTOMER_DISPLAY_PROGRAME_TABLE(
				mDB);
		return customerPrograme.caculateAmountAchievedOfPrograme(customerId, ""
				+ programId, isFino, hasFino);
	}

	/**
	 * Insert order have detail into table sale_order & sales_order_detail
	 *
	 * @author: TruongHN
	 * @param dto
	 * @throws Exception
	 * @return: SaleOrderDataResult
	 * @throws:
	 */
	@SuppressLint("UseSparseArrays")
	public SaleOrderDataResult createOrder(OrderViewDTO dto) throws Exception {
		SaleOrderDataResult result = new SaleOrderDataResult();
		long saleOrderId = -1;
		long saleOrderDetailId = -1;
		long poId = -1;
		long poDetailId = -1;
		String createUser = dto.orderInfo.createUser;
		boolean insertSuccess = false;
		//cap nhat la don cuoi cung cap nhat last_order, staff customer, customer...
		dto.isFinalOrder = 1;
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId());

		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		SALES_ORDER_DETAIL_TABLE detailTable = new SALES_ORDER_DETAIL_TABLE(mDB);
		PO_CUSTOMER_TABLE poCustomerTable = new PO_CUSTOMER_TABLE(mDB);
		PO_CUSTOMER_DETAIL_TABLE poCustomerDetailTable = new PO_CUSTOMER_DETAIL_TABLE(
				mDB);
		SALE_ORDER_LOT_TABLE saleOrderLotTable = new SALE_ORDER_LOT_TABLE(mDB);
		PO_CUSTOMER_LOT_TABLE poLotTable = new PO_CUSTOMER_LOT_TABLE(mDB);
		SALE_ORDER_PROMO_DETAIL_TABLE promoDetailTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
				mDB);
		PO_CUSTOMER_PROMO_DETAIL_TABLE poPromoDetailTable = new PO_CUSTOMER_PROMO_DETAIL_TABLE(
				mDB);
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		STAFF_CUSTOMER_TABLE staffCusTable = new STAFF_CUSTOMER_TABLE(mDB);
		ROUTING_CUSTOMER_TABLE routingCusTable = new ROUTING_CUSTOMER_TABLE(mDB);
		RPT_CTTL_PAY_TABLE rptCttlPayTable = new RPT_CTTL_PAY_TABLE(mDB);
		RPT_CTTL_DETAIL_PAY_TABLE rptCttlDetailPayTable = new RPT_CTTL_DETAIL_PAY_TABLE(
				mDB);
		SALE_PROMO_MAP_TABLE promoMapTable = new SALE_PROMO_MAP_TABLE(
				mDB);
		PO_CUSTOMER_PROMO_MAP_TABLE poCustomerPromoMapTable = new PO_CUSTOMER_PROMO_MAP_TABLE(
				mDB);
		long routingCustomerId = routingCusTable
				.getRoutingCustomerIdOfCustomer(staffId, shopId,
						dto.customer.customerId);
		dto.routingCusDTO.routingCustomerId = routingCustomerId;
		SALE_ORDER_PROMO_LOT_TABLE saleOrderPromoLotTable = new SALE_ORDER_PROMO_LOT_TABLE(mDB);
		PO_CUSTOMER_PROMO_LOT_TABLE poCustomerPromoLotTable = new PO_CUSTOMER_PROMO_LOT_TABLE(mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);

		try {
			mDB.beginTransaction();
			// cap nhat id order
			saleOrderId = tableId.getMaxIdTime(SALE_ORDER_TABLE.TABLE_NAME);
			poId = tableId.getMaxIdTime(PO_CUSTOMER_TABLE.TABLE_NAME);
			long debitDetailId = tableId.getMaxIdTime(DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);
			long debitId = tableId.getMaxIdTime(DEBIT_TABLE.TABLE_NAME);
			result.orderId = saleOrderId;
			result.poId = poId;

			saleOrderDetailId = tableId
					.getMaxIdTime(SALES_ORDER_DETAIL_TABLE.TABLE_NAME);
			poDetailId = tableId.getMaxIdTime(PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);

			// lay max promotion detail id
			long saleOrderPromoDetailId = tableId
					.getMaxIdTime(SALE_ORDER_PROMO_DETAIL_TABLE.TABLE_NAME);
			long poPromoDetailId = tableId
					.getMaxIdTime(PO_CUSTOMER_PROMO_DETAIL_TABLE.TABLE_NAME);

			long saleOrderLotId = tableId
					.getMaxIdTime(SALE_ORDER_LOT_TABLE.TABLE_NAME);
			long poLotId = tableId.getMaxIdTime(PO_CUSTOMER_LOT_TABLE.TABLE_NAME);

			// max id bang rpt pay
			long rptCttlPayId = tableId.getMaxIdTime(RPT_CTTL_PAY_TABLE.TABLE_NAME);
			long rptCttlDetailPayId = tableId
					.getMaxIdTime(RPT_CTTL_DETAIL_PAY_TABLE.TABLE_NAME);
			// lay max promo_map id
			long salePromoMapId = tableId
								.getMaxIdTime(SALE_PROMO_MAP_TABLE.TABLE_NAME);
			// lay max promo_map id
			long poCustomerPromoMapId = tableId
					.getMaxIdTime(PO_CUSTOMER_PROMO_MAP_TABLE.TABLE_NAME);
			// max Id sale promot lot
			long saleOrderPromoLotId = tableId
					.getMaxIdTime(SALE_ORDER_PROMO_LOT_TABLE.TABLE_NAME);
			// max Id sale promot lot
			long poCustomerPromoLotId = tableId
					.getMaxIdTime(PO_CUSTOMER_PROMO_LOT_TABLE.TABLE_NAME);
			// insert sale order
			dto.orderInfo.saleOrderId = result.orderId;
			dto.orderInfo.poId = result.poId;
			dto.orderInfo.fromPOCustomerId = result.poId;
			// cap nhat cycleId hien tai
			dto.orderInfo.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
			// cap nhat order number
			// order number = chu cai dau tien (P, W) + 4 chu cuoi staff_id + 5
			// so cuoi id don hang
			String strOrderId = String.valueOf(result.orderId);
			StringBuilder orderNumber = new StringBuilder();
			String userId = ""
					+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

			if (dto.orderInfo.orderType
					.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
				if (dto.orderInfo.isVisitPlan == 1) {
					// trong tuyen
					orderNumber.append("V");
				} else {
					// ngoai tuyen
					orderNumber.append("E");
				}
			} else if (dto.orderInfo.orderType
					.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
				if (dto.orderInfo.isVisitPlan == 1) {
					// trong tuyen
					orderNumber.append("T");
				} else {
					// ngoai tuyen
					orderNumber.append("L");
				}
				// vansale: orderDate = ngay chot
				//khong dung ngay chot nua
				//dto.orderInfo.orderDate = lockDate;
			}

			if (userId.length() >= 5) {
				orderNumber.append(userId.substring(userId.length() - 5));
			} else {
				// 975
				String zero = "00000";
				int lengId = userId.length();
				orderNumber.append(zero.substring(lengId) + userId);
			}

			if (strOrderId.length() >= 8) {
				orderNumber
						.append(strOrderId.substring(strOrderId.length() - 8));
			} else {
				String zero = "00000000";
				int lengId = strOrderId.length();
				orderNumber.append(zero.substring(lengId) + strOrderId);
			}
			dto.orderInfo.orderNumber = orderNumber.toString();
			dto.orderInfo.refOrderNumber = dto.orderInfo.orderNumber;

			// them danh sach luu sale_order_lot
			dto.listSaleOrderLot = new ArrayList<SaleOrderLotDTO>();

			// them danh sach luu sale_order_promo_lot
			dto.listSaleOrderPromoLot = new ArrayList<SaleOrderPromoLotDTO>();

			if (StringUtil.isNullOrEmpty(dto.orderInfo.shopCode)) {
				dto.orderInfo.shopCode = GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopCode();
			}
			dto.orderInfo.approvedStep = 0;

			// Reset danh sp
			dto.listProduct = new ArrayList<OrderDetailViewDTO>();

			// Ds sp ban
			for (OrderDetailViewDTO product : dto.listBuyOrders) {
				if (product.orderDetailDTO.quantity > 0) {
					dto.listProduct.add(product);
				}
			}

			// KM sp
			dto.listProduct.addAll(dto.listPromotionOrders);

			// KM don hang - loai sp
			for (OrderDetailViewDTO promotion : dto.listPromotionOrder) {
				if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
					dto.listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			// KM tich luy
			for (OrderDetailViewDTO promotion : dto.listPromotionAccumulation) {
				// KM duoc sp
				if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
					dto.listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			// KM mo moi - loai sp
			for (OrderDetailViewDTO promotion : dto.listPromotionNewOpen) {
				if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
					dto.listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			// KM keyshop
			for (OrderDetailViewDTO promotion : dto.listPromotionKeyShop) {
				if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
					dto.listProduct.add(promotion);
				}
			}

			// Tong so dong trong sale order detail
			dto.orderInfo.totalDetail = dto.listProduct.size();
			// co tham gia keyshop hay ko
			dto.orderInfo.isRewardKS = dto.isKeyShop;
			if (orderTable.insert(dto.orderInfo) > 0
					&& poCustomerTable.insert(dto.orderInfo) > 0) {
				// insert sale order detail
				STOCK_TOTAL_TABLE stockTable = new STOCK_TOTAL_TABLE(mDB);
				// boolean hasOrderDetail = false;
				boolean insertOrderDetail = true;
				boolean insertPOOrderDetail = true;
				boolean insertSaleOrderLot = true;
				boolean increasePromotion = true;
				ArrayList<String> listPromotionCode = null;
				// mac dinh la thanh cong, khi gap 1 that bai -> insert that bai
				boolean insertArrayPromoDetail = true;
				// mac dinh la thanh cong, khi gap 1 that bai -> insert that bai
				boolean insertArrayPromoMap = true;
				boolean insertArrayPOCustomerPromoMap = true;

				for (OrderDetailViewDTO detailViewDTO : dto.listProduct) {
					// Luu cac mat hang ban
					createOrderDetailInfo(detailViewDTO, dto, result.orderId,
							saleOrderDetailId, result.poId, poDetailId,
							createUser, dto.orderInfo.updateUser, staffId,
							shopId);

					// Check insert success
					insertOrderDetail = detailTable
							.insert(detailViewDTO.orderDetailDTO) > 0 ? true
							: false;
					insertPOOrderDetail = poCustomerDetailTable
							.insert(detailViewDTO.orderDetailDTO) > 0 ? true
							: false;

					// them phan insert sale_order_lot
					if (insertOrderDetail && insertPOOrderDetail) {
						if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
								.equals(dto.orderInfo.orderType)) {
							SaleOrderLotDTO saleLotDto = new SaleOrderLotDTO(
									detailViewDTO);
							saleLotDto.saleOrderLotId = saleOrderLotId;
							saleLotDto.stockTotalId = detailViewDTO.stockId;
							saleLotDto.poLotId = poLotId;
							insertSaleOrderLot = saleOrderLotTable
									.insert(saleLotDto) > 0
									&& poLotTable.insert(saleLotDto) > 0 ? true
									: false;
							if (insertSaleOrderLot) {
								saleOrderLotId++;
								poLotId++;
								// them vao danh sach sale_order_lot
								dto.listSaleOrderLot.add(saleLotDto);
							}
						}
					}

					// insert them phan chi tiet khuyen mai
					if (insertOrderDetail && insertPOOrderDetail && insertSaleOrderLot
							&& detailViewDTO.listPromoDetail != null
							&& !detailViewDTO.listPromoDetail.isEmpty()) {
						for (SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
							if (promoDetail.discountAmount >= 0) {
								// cap nhat staff
								promoDetail.staffId = detailViewDTO.orderDetailDTO.staffId;
								promoDetail.shopId = detailViewDTO.orderDetailDTO.shopId
										+ "";
								promoDetail.orderDate = detailViewDTO.orderDetailDTO.orderDate;
								promoDetail.saleOrderDetailId = saleOrderDetailId;
								promoDetail.saleOrderPromoDetailId = saleOrderPromoDetailId;
								promoDetail.saleOrderId = saleOrderId;

								// dung cho po
								promoDetail.poId = poId;
								promoDetail.poDetailId = poDetailId;
								promoDetail.poPromoDetailId = poPromoDetailId;

								if (promoDetailTable.insert(promoDetail) > 0
										&& poPromoDetailTable
												.insert(promoDetail) > 0) {
									// neu insert thanh cong thi tang id promo
									// detail
									saleOrderPromoDetailId++;
									// neu insert thanh cong thi tang id po
									// promo detail
									poPromoDetailId++;
									if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
											.equals(dto.orderInfo.orderType)) {
										if(saleOrderPromoLotTable.insert(
												promoDetail, saleOrderLotId,
												saleOrderPromoLotId) > 0
										&& poCustomerPromoLotTable.insert(
												promoDetail, poLotId,
												poCustomerPromoLotId) > 0){
										// neu insert thanh cong thi tang id saleOrderPromoLot len 1
										saleOrderPromoLotId++;
										// insert poCustomerPomoLot thanh cong
										poCustomerPromoLotId++;
										dto.listSaleOrderPromoLot
												.add(new SaleOrderPromoLotDTO(promoDetail,saleOrderPromoLotId,poCustomerPromoLotId,saleOrderLotId,poLotId));
										}else{
											insertArrayPromoDetail = false;
										}
									}
								} else {
									insertArrayPromoDetail = false;
								}
							}
						}
					}
					// insert them phan thong tin sp dat hay ko dat
					if (insertOrderDetail && insertPOOrderDetail
							&& detailViewDTO.lstPromotionMap != null
							&& !detailViewDTO.lstPromotionMap.isEmpty()) {
						for (SalePromoMapDTO promoMap : detailViewDTO.lstPromotionMap) {
								promoMap.salePromoMapId = salePromoMapId;
								promoMap.staffId = detailViewDTO.orderDetailDTO.staffId;
								promoMap.saleOrderDetailId = saleOrderDetailId;
								promoMap.saleOrderId = saleOrderId;

								// dung cho po customer map
								promoMap.poCustomerId = poId;
								promoMap.poCustomerDetailId = poDetailId;

								if (promoMapTable.insert(promoMap) > 0) {
									salePromoMapId++;
								} else {
									insertArrayPromoMap = false;
								}
//							}
						}
					}

					// insert them phan thong tin sp dat hay ko dat cho po
					if (insertOrderDetail && insertPOOrderDetail
							&& detailViewDTO.lstPromotionMap != null
							&& !detailViewDTO.lstPromotionMap.isEmpty()) {
						for (SalePromoMapDTO promoMap : detailViewDTO.lstPromotionMap) {
								promoMap.poCustomerPromoMapId = poCustomerPromoMapId;
								POCustomerPromoMapDTO poCustomerMap = new POCustomerPromoMapDTO();
								poCustomerMap.poCustomerPromoMapId = poCustomerPromoMapId;
								poCustomerMap.poCustomerId = promoMap.poCustomerId;
								poCustomerMap.poCustomerDetailId = promoMap.poCustomerDetailId;
								poCustomerMap.programCode = promoMap.programCode;
								poCustomerMap.staffId = promoMap.staffId;
								poCustomerMap.status = promoMap.status;
								if (poCustomerPromoMapTable.insert(poCustomerMap) > 0) {
									poCustomerPromoMapId++;
								} else {
									insertArrayPOCustomerPromoMap = false;
								}
//							}
						}
					}

					// Update stock total if insert order success
					// + check promo detail array insert success
					if (insertOrderDetail && insertPOOrderDetail
							&& insertArrayPromoDetail) {
						saleOrderDetailId += 1;
						poDetailId += 1;
						// cap nhat ton kho tuong ung
						StockTotalDTO stockTotal = StockTotalDTO
								.createStockTotalInfo(dto,
										detailViewDTO.orderDetailDTO);

						if (dto.orderInfo.approved != -1) {
							/*
							 * if (dto.orderInfo.orderType
							 * .equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {//
							 * presale stockTable
							 * .descreaseStockTotalPresale(stockTotal); } else
							 */if (dto.orderInfo.orderType
									.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
								stockTable
										.descreaseStockTotalVansale(stockTotal);
							}
						}

					} else {
						break;
					}

					// add id vao ds insert xuong db server
					result.listOrderDetailId
							.add(detailViewDTO.orderDetailDTO.salesOrderDetailId);
				}

				boolean insertRptCttlPay = true;
				boolean insertRptCttlDetailPay = true;
				if (insertOrderDetail && insertPOOrderDetail) {
					for (OrderDetailViewDTO promotion : dto.listPromotionAccumulation) {
						// insert cttl pay cua KM tich luy
						double proDisAmount = promotion.orderDetailDTO.getDiscountAmount();
						if (promotion.rptCttlPay != null
								&& (proDisAmount > 0 || (BigDecimal.valueOf(proDisAmount).compareTo(BigDecimal.ZERO) == 0 && promotion.remainDiscount == 0))) {
//								&& (proDisAmount > 0 || (proDisAmount == 0 && promotion.remainDiscount == 0))) {
							promotion.rptCttlPay.rptCttlPayId = rptCttlPayId;
							promotion.rptCttlPay.saleOrderId = saleOrderId;
							// Lam tron 2 chu so thap phan truoc khi luu so
							// luong
							promotion.rptCttlPay.totalQuantityPayPromotion = StringUtil
									.roundDoubleDown(
											"#.##",
											promotion.rptCttlPay.totalQuantityPayPromotion);
							// Don van sale thi approve = 1
							if (dto.orderInfo.orderType
									.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
								promotion.rptCttlPay.approved = 1;
							}

							insertRptCttlPay = rptCttlPayTable
									.insert(promotion.rptCttlPay) > 0 ? true
									: false;
							if (insertRptCttlPay) {
								for (RptCttlDetailPayDTO payCttlPayDetail : promotion.rptCttlPay.rptCttlDetailList) {
									if (payCttlPayDetail.quantityPromotion > 0
											|| payCttlPayDetail.amountPromotion > 0) {
										payCttlPayDetail.rptCttlDetailPayId = rptCttlDetailPayId;
										payCttlPayDetail.rptCttlPayId = rptCttlPayId;
										payCttlPayDetail.quantityPromotion = StringUtil
												.roundDoubleDown(
														"#.##",
														payCttlPayDetail.quantityPromotion);
										insertRptCttlDetailPay = rptCttlDetailPayTable
												.insert(payCttlPayDetail) > 0 ? true
												: false;

										if (insertRptCttlDetailPay) {
											rptCttlDetailPayId++;
										} else {
											break;
										}
									}
								}
								rptCttlPayId++;
							} else {
								break;
							}
						}
					}
				}

				long insertQuantityReceivedID = 0;
				long insertQuantityReceivedPOID = 0;
				// Luu so suat
				boolean insertQuantityReceived = true;
				boolean insertMapDelta = true;
				SALE_ORDER_PROMOTION_TABLE quantityReceivedTable = new SALE_ORDER_PROMOTION_TABLE(
						mDB);
				PO_PROMOTION_TABLE quantityReceivedPoTable = new PO_PROMOTION_TABLE(
						mDB);
				if (insertOrderDetail && insertPOOrderDetail
						&& insertSaleOrderLot && insertArrayPromoDetail
						&& increasePromotion && insertRptCttlPay
						&& insertRptCttlDetailPay
						&& dto.productQuantityReceivedList.size() > 0) {
					long quantityReceivedId = tableId
							.getMaxIdTime(SALE_ORDER_PROMOTION_TABLE.TABLE_NAME);
					long quantityReceivedPoId = tableId
							.getMaxIdTime(PO_PROMOTION_TABLE.TABLE_NAME);
					// Check lai quantity receive sau khi sua
					dto.checkQuantityReceived();
					for (SaleOrderPromotionDTO quantityReceived : dto.productQuantityReceivedList) {
						quantityReceived.saleOrderPromotionId = quantityReceivedId;
						quantityReceived.poPromotionId = quantityReceivedPoId;
						quantityReceived.saleOrderId = dto.orderInfo.saleOrderId;
						quantityReceived.poId = dto.orderInfo.poId;
						quantityReceived.shopId = shopId;
						quantityReceived.staffId = staffId;
						quantityReceived.orderDate = dto.orderInfo.orderDate;
						quantityReceived.createDate = dto.orderInfo.orderDate;
						quantityReceived.updateDate = dto.orderInfo.updateDate;
						quantityReceived.updateUser = dto.orderInfo.updateUser;
						quantityReceived.createUser = createUser;
						//Chi app dung cho presale
						if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
							quantityReceived.promotionDetail = quantityReceived.generatePromotionDetail();
						}
						insertQuantityReceivedID = quantityReceivedTable
								.insert(quantityReceived);
						insertQuantityReceivedPOID = quantityReceivedPoTable
								.insert(quantityReceived);
						insertQuantityReceived = insertQuantityReceivedID > 0
								&& insertQuantityReceivedPOID > 0 ? true
								: false;

						if (insertQuantityReceived) {
							quantityReceivedId++;
							quantityReceivedPoId++;
						} else {
							break;
						}
					}
				}

				boolean isVaildIncreasePromoQuanReceived = true;
				// ***
				// tang so xuat KM vansale, neu la don vansale, khong phai don
				// hang tam
				// ***
//				if (dto.orderInfo.orderType
//						.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
//						&& dto.orderInfo.approved != -1) {
//					HashMap<Long, QuantityRevicevedDTO> mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();
//					// nhap chung nhieu group cua 1 chuong trinh lai
//					for (SaleOrderPromotionDTO quantityReceived : dto.productQuantityReceivedList) {
//						QuantityRevicevedDTO quanDto = mapQuantityReviceved
//								.get(quantityReceived.promotionProgramId);
//						if (quanDto == null) {
//							quanDto = new QuantityRevicevedDTO(
//									quantityReceived.promotionProgramId,
//									quantityReceived.promotionProgramCode);
//							mapQuantityReviceved.put(
//									quantityReceived.promotionProgramId,
//									quanDto);
//						}
//
//						// cong them so suat
//						quanDto.setQuantityReviceved(quanDto
//								.getQuantityReviceved() + quantityReceived.quantityReceived);
//						// luu thong tin so tien/ so luong nhan duoc cua mot CTKM
//						quanDto.setNumReceived(quanDto.getNumReceived()
//								+ quantityReceived.numReceived);
//						quanDto.setAmountReceived(quanDto.getAmountReceived()
//								+ quantityReceived.amountReceived);
//					}
//
//					PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(mDB);
//					PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
//					PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(
//							mDB);
//					if (listPromotionCode == null) {
//						listPromotionCode = new ArrayList<String>();
//					}
//					// qua tung chuong trinh, check so luong so suat
//					for (QuantityRevicevedDTO quanDto : mapQuantityReviceved
//							.values()) {
//
//						PromotionShopMapDTO promotionShopMapDTO = psmTable
//								.getPromotionShopMapByPromotionProgramId(
//										quanDto.getProId(),
//										GlobalInfo.getInstance().getProfile()
//												.getUserData().getInheritShopId());
//
//						int quantityConfigRemain = -1;
//						// so luong nhan con lai
//						int numConfigRemain = -1;
//						// so tien nhan con lai
//						double amountConfigRemain = -1;
//						if (promotionShopMapDTO != null) {
//							quanDto.setShopMapID(promotionShopMapDTO.promotionShopMapId);
//							// truong hop co map voi shop
//
//							// promotionShopMapDTO.quantityReceived +
//							// quanDto.quantityReviceved <
//							// promotionShopMapDTO.quantityMax
//							// lay promotion_customer_map
//							PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable
//									.getPromotionShopMapByPromotionShopMapID(
//											dto.orderInfo.customerId,
//											dto.orderInfo.shopId,
//											promotionShopMapDTO.promotionShopMapId);
//							PromotionStaffMapDTO promotionStaffMapDTO = pstmTable
//									.getPromotionStaffMapByPromotionShopMapID(
//											dto.orderInfo.staffId,
//											dto.orderInfo.shopId,
//											promotionShopMapDTO.promotionShopMapId);
//
//							// khong cau hinh so suat nhan vien + khach hang
//							if (promotionStaffMapDTO == null
//									&& promotionCustomerMapDTO == null) {
//								// thi su dung so suat cua NPP
//								quantityConfigRemain = promotionShopMapDTO.quantityMax
//										- promotionShopMapDTO.quantityReceived;
//								// thi su dung so suat cua NPP
//								numConfigRemain = promotionShopMapDTO.numMax
//										- promotionShopMapDTO.numReceived;
//								// thi su dung so suat cua NPP
//								amountConfigRemain = promotionShopMapDTO.amountMax
//										- promotionShopMapDTO.amountReceived;
//							} else
//							// co khai bao staff map
//							if (promotionStaffMapDTO != null) {
//								quanDto.setStaffMapId(promotionStaffMapDTO.promotionStaffMapId);
//								// thi su dung so suat cua nv
//								quantityConfigRemain = promotionStaffMapDTO.quantityMax
//										- promotionStaffMapDTO.quantityReceived;
//								// thi su dung so suat cua NPP
//								numConfigRemain = promotionStaffMapDTO.numMax
//										- promotionStaffMapDTO.numReceived;
//								// thi su dung so suat cua NPP
//								amountConfigRemain = promotionStaffMapDTO.amountMax
//										- promotionStaffMapDTO.amountReceived;
//
//								// co khai bao customer map
//								if (promotionCustomerMapDTO != null) {
//									quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
//									// neu so suat con lai cua kh nho hon cua nv
//									// thi cap nhat lai so suat ton
//									int quantityTemp = promotionCustomerMapDTO.quantityMax
//											- promotionCustomerMapDTO.quantityReceived;
//									int numTemp = promotionCustomerMapDTO.numMax
//											- promotionCustomerMapDTO.numReceived;
//									double amountTemp = promotionCustomerMapDTO.amountMax
//											- promotionCustomerMapDTO.amountReceived;
//
//									if (quantityTemp < quantityConfigRemain) {
//										quantityConfigRemain = quantityTemp;
//									}
//									if (numTemp < numConfigRemain) {
//										numConfigRemain = numTemp;
//									}
//									if (amountTemp < amountConfigRemain) {
//										amountConfigRemain = amountTemp;
//									}
//								} else {
//									// khong khai bao customer map
//								}
//							} else {
//								quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
//								// co khai bao so suat kh
//								// thi su dung so suat cua kh
//								quantityConfigRemain = promotionCustomerMapDTO.quantityMax
//										- promotionCustomerMapDTO.quantityReceived;
//								numConfigRemain = promotionCustomerMapDTO.numMax
//										- promotionCustomerMapDTO.numReceived;
//								amountConfigRemain = promotionCustomerMapDTO.amountMax
//										- promotionCustomerMapDTO.amountReceived;
//							}
//
//							// neu min so suat con lai khong du thi them vao
//							// danh sach cac chuong trinh thieu so suat
//							if (quantityConfigRemain < quanDto.getQuantityReviceved()
//									|| numConfigRemain < quanDto.getNumReceived()
//									|| amountConfigRemain < quanDto.getAmountReceived()) {
//								listPromotionCode.add(quanDto.getProCode());
//								isVaildIncreasePromoQuanReceived = false;
//							}
//						} else {
//							// neu khong co so suat npp thi them vao danh sach
//							// cac chuong trinh thieu so suat
//							listPromotionCode.add(quanDto.getProCode());
//							isVaildIncreasePromoQuanReceived = false;
//						}
//					}
//
//					// neu cac promotion deu du so suat thi tien hanh tru
//					if (isVaildIncreasePromoQuanReceived) {
//						// tru so suat tung chuong trinh
//						for (QuantityRevicevedDTO quanDto : mapQuantityReviceved
//								.values()) {
//							// tang so xuat shop
//							if (quanDto.getShopMapID() > 0) {
//								isVaildIncreasePromoQuanReceived = psmTable
//										.increaseQuantityRecevie(
//												quanDto.getShopMapID(),
//												quanDto.getQuantityReviceved(), quanDto.getNumReceived(),quanDto.getAmountReceived());
//							}
//
//							if (isVaildIncreasePromoQuanReceived
//									&& quanDto.getStaffMapId() > 0) {
//								isVaildIncreasePromoQuanReceived = pcmTable
//										.increaseStaffQuantityRecevie(
//												quanDto.getStaffMapId(),
//												quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
//							}
//
//							if (isVaildIncreasePromoQuanReceived
//									&& quanDto.getCustomerMapId() > 0) {
//								isVaildIncreasePromoQuanReceived = pcmTable
//										.increaseQuantityRecevie(
//												quanDto.getCustomerMapId(),
//												quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
//							}
//
//							// neu fail thi break
//							if (!isVaildIncreasePromoQuanReceived) {
//								break;
//							}
//						}
//					}
//
//					// cap nhat list so suat phai tru
//					dto.listIncreasePromoQuantityReceived = mapQuantityReviceved
//							.values();
//
//				} else
				if (dto.orderInfo.approved != -1) {
					PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(mDB);
//					PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
					PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(mDB);

					PROMOTION_MAP_DELTA_TABLE pmdTable = new PROMOTION_MAP_DELTA_TABLE(mDB);
					long promotionMapDeltaId = tableId.getMaxIdTime(PROMOTION_MAP_DELTA_TABLE.TABLE_NAME);
					//Insert vao bang promotion_map_delta
					long insertPromotionMapDeltaId = 0;
					for(QuantityRevicevedDTO quanDto : dto.mapQuantityReviceved.values()) {
						quanDto.setPromotionMapDeltaId(promotionMapDeltaId);
						quanDto.setFromObjectId(dto.orderInfo.saleOrderId);
						quanDto.setOrderAction(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_ACTION_INSERT);
						quanDto.setSource(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_SOURCE_MOBILE);
						quanDto.setShopId(shopId);
						quanDto.setStaffId(staffId);
						quanDto.setCustomerId(dto.orderInfo.customerId);
						quanDto.setCreateDate(dto.orderInfo.orderDate);
						quanDto.setCreateUser(createUser);
						insertPromotionMapDeltaId = pmdTable.insert(quanDto);

						insertMapDelta = insertPromotionMapDeltaId > 0 ? true: false;

						if (insertMapDelta) {
							promotionMapDeltaId++;
						} else {
							break;
						}

						// tang so xuat shop
						if (quanDto.getShopMapID() > 0) {
							isVaildIncreasePromoQuanReceived = psmTable.increaseQuantityRecevieTotal(
											quanDto.getShopMapID(), quanDto.getQuantityReviceved(), quanDto.getNumReceived(),quanDto.getAmountReceived());
						}

						if (isVaildIncreasePromoQuanReceived
								&& quanDto.getStaffMapId() > 0) {
							isVaildIncreasePromoQuanReceived = pcmTable.increaseStaffQuantityRecevieTotal(
											quanDto.getStaffMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
						}

						if (isVaildIncreasePromoQuanReceived
								&& quanDto.getCustomerMapId() > 0) {
							isVaildIncreasePromoQuanReceived = pcmTable.increaseQuantityRecevieTotal(
											quanDto.getCustomerMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
						}

						// neu fail thi break
						if (!isVaildIncreasePromoQuanReceived) {
							break;
						}
					}

					// cap nhat list so suat phai tru
					dto.listIncreasePromoQuantityReceived = dto.mapQuantityReviceved.values();
				}
				result.listPromotionCode = listPromotionCode;

				if (insertOrderDetail && insertPOOrderDetail
						&& insertSaleOrderLot && increasePromotion
						&& insertQuantityReceived && insertMapDelta
						&& isVaildIncreasePromoQuanReceived && insertRptCttlPay
						&& insertRptCttlDetailPay) {
					// don hang co phat sinh doanh so thi moi cap nhat last
					// order
					boolean updateStaff = true;
					boolean insertUpdateStaffCus = true;
					boolean updateRoutingCus = true;

//					if (dto.orderInfo.getAmount() > 0) {
					if (BigDecimal.valueOf(dto.orderInfo.getAmount()).compareTo(BigDecimal.ZERO) > 0) {
						// cap nhat customer
						dto.customer.setLastOrder(dto.orderInfo.orderDate);
						updateStaff = cusTable.updateLastOrder(dto.customer) > 0 ? true
								: false;

						// cap nhat staff_customer
						dto.staffCusDto.customerId = dto.customer.customerId;
						dto.staffCusDto.staffId = staffId;
						dto.staffCusDto.shopId = shopId + "";
						dto.staffCusDto.lastOrder = dto.orderInfo.orderDate;
						insertUpdateStaffCus = staffCusTable
								.insertOrUpdate(dto.staffCusDto) > 0 ? true
								: false;

						// Cap nhat routing_customer
						dto.routingCusDTO.lastOrder = dto.orderInfo.orderDate;
						updateRoutingCus = routingCusTable
								.updateLastOrder(dto.routingCusDTO) > 0 ? true
								: false;
					}

					boolean increaseDebit = true;
					boolean insertDebitDetail = true;
					if (dto.orderInfo.orderType
							.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
							&& dto.orderInfo.approved != -1) {// vansale
						CustomerDTO cus = cusTable.getCustomerById(String
								.valueOf(dto.customer.customerId));

						DEBIT_TABLE debitTable = new DEBIT_TABLE(mDB);
						dto.debitDto.id = debitId++;
						dto.debitDto.objectID = String
								.valueOf(dto.orderInfo.customerId);
						dto.debitDto.objectType = "3";
						dto.debitDto.totalAmount = dto.orderInfo.getTotal();
						dto.debitDto.totalPay = 0;
						dto.debitDto.totalDebit = dto.orderInfo.getTotal();
						dto.debitDto.maxDebitAmount = cus.getMaxDebitAmount();
						dto.debitDto.maxDebitDate = cus.getMaxDebitDate();
						dto.debitDto.totalDiscount = 0;

						dto.debitIdExist = debitTable.checkDebitExist(String
								.valueOf(dto.orderInfo.customerId));
						if (dto.debitIdExist > 0) {
							dto.debitDto.id = dto.debitIdExist;
							increaseDebit = debitTable
									.increaseDebit(dto.debitDto);
						} else {
							increaseDebit = debitTable.insert(dto.debitDto) > 0 ? true
									: false;
						}

						// Insert vao bang debit_detail
						DEBIT_DETAIL_TEMP_TABLE debitDetailTable = new DEBIT_DETAIL_TEMP_TABLE(
								mDB);
						dto.debitDetailDto.debitDetailID = debitDetailId++;
						dto.debitDetailDto.fromObjectID = dto.orderInfo.saleOrderId;
						dto.debitDetailDto.amount = dto.orderInfo.getAmount();
						dto.debitDetailDto.discount = dto.orderInfo.getDiscount();
						dto.debitDetailDto.total = dto.orderInfo.getTotal();
						dto.debitDetailDto.totalPay = 0;
						dto.debitDetailDto.remain = dto.debitDetailDto.total;
						dto.debitDetailDto.debitID = dto.debitDto.id;
						dto.debitDetailDto.createUser = dto.orderInfo.createUser;
						dto.debitDetailDto.createDate = dto.orderInfo.orderDate;
						dto.debitDetailDto.debitDate = dto.orderInfo.orderDate;
						dto.debitDetailDto.type = 4;
						dto.debitDetailDto.fromObjectNumber = dto.orderInfo.orderNumber;
						dto.debitDetailDto.customerId = dto.orderInfo.customerId;
						dto.debitDetailDto.shopId = dto.orderInfo.shopId;
						dto.debitDetailDto.staffID = dto.orderInfo.staffId;
						dto.debitDetailDto.orderType = dto.orderInfo.orderType;
						dto.debitDetailDto.saleDate = dto.orderInfo.orderDate;

						insertDebitDetail = debitDetailTable
								.insert(dto.debitDetailDto) > 0 ? true : false;
					}

					if (updateStaff && insertUpdateStaffCus && updateRoutingCus
							&& increaseDebit && insertDebitDetail
							&& insertArrayPromoDetail && insertArrayPromoMap && insertArrayPOCustomerPromoMap) {
						insertSuccess = true;
						mDB.setTransactionSuccessful();
					}
				}
			}
		} catch (Exception e) {
			insertSuccess = false;
			ServerLogger.sendLog(e.getMessage(), "createOrder", false,
					TabletActionLogDTO.LOG_EXCEPTION);
		} finally {
			mDB.endTransaction();
			result.isCreateSqlLiteSuccess = insertSuccess;
		}

		return result;
	}

	/**
	 * huy don hang
	 * @author: duongdt3
	 * @time: 5:06:34 PM Nov 3, 2015
	 * @param dto
	 * @param actionLogDTO
	 * @return
	 */
	public boolean cancelSaleOrder(SaleOrderViewDTO dto, ActionLogDTO actionLogDTO) {
		boolean result = false;

		try {
			mDB.beginTransaction();
			CUSTOMER_TABLE customer = new CUSTOMER_TABLE(mDB);
			SALE_ORDER_TABLE soTable = new SALE_ORDER_TABLE(mDB);
			PO_CUSTOMER_TABLE poTable = new PO_CUSTOMER_TABLE(mDB);
			STAFF_CUSTOMER_TABLE staffCusTable = new STAFF_CUSTOMER_TABLE(mDB);
			ROUTING_CUSTOMER_TABLE routingCusTable = new ROUTING_CUSTOMER_TABLE(mDB);
			int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId());
			boolean isDeleted = (soTable.updateOrderCancel(dto.saleOrder.saleOrderId) > 0)
					&& (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE.equals(dto.saleOrder.orderType)?true: poTable.updateOrderCancel(dto.saleOrder.saleOrderId) > 0);

			if (isDeleted) {
				// Update log of sale order delete
				LOG_TABLE logTable = new LOG_TABLE(mDB);
				logTable.updateState(String.valueOf(dto.saleOrder.saleOrderId),
						Integer.parseInt(LogDTO.STATE_ORDER_DELETED));

				if (dto.isFinalOrder == 1) {
					// delete record cuoi cung thi cap nhat last_order
					CustomerDTO cus = new CustomerDTO();
					cus.customerId = dto.getCustomerId();
					cus.setLastOrder(dto.lastOrder);
					// updateCustomer = customer.updateLastOrder(cus) > 0 ? true
					// : false;
					customer.updateLastOrder(cus);

					// cap nhat last order trong staff customer
					StaffCustomerDTO staffDto = new StaffCustomerDTO();
					staffDto.customerId = dto.getCustomerId();
					staffDto.staffId = staffId;
					staffDto.shopId = shopId + "";
					staffDto.lastOrder = dto.lastOrder;
					staffCusTable.updateLastOrder(staffDto);

					// cap nhat routing customer
					long routingCusId = routingCusTable
							.getRoutingCustomerIdOfCustomer(staffId, shopId,
									dto.getCustomerId());
					RoutingCustomerDTO routingCusDTO = new RoutingCustomerDTO();
					routingCusDTO.routingCustomerId = routingCusId;
					routingCusDTO.lastOrder = dto.lastOrder;
					routingCusTable.updateLastOrder(routingCusDTO);
				}

				// Delete action log
//				ACTION_LOG_TABLE action_log = new ACTION_LOG_TABLE(mDB);
//				action_log.deleteActionLogWhenDeleteOrder(actionLogDTO);

				mDB.setTransactionSuccessful();
				result = true;
			}

		} catch (Exception ex) {
			result = false;
		} finally {
			if (mDB != null && mDB.inTransaction()) {
				mDB.endTransaction();
			}
		}
		return result;
	}

	/**
	 * xoa don hang tam
	 * @author: duongdt3
	 * @time: 5:06:41 PM Nov 3, 2015
	 * @param dto
	 * @param listSaleOrderDetail
	 * @param actionLogDTO
	 * @return
	 */
	public boolean deleteDraftSaleOrder(SaleOrderViewDTO dto, List<SaleOrderDetailDTO> listSaleOrderDetail, ActionLogDTO actionLogDTO) {
		boolean result = false;

		try {

			mDB.beginTransaction();
			long poID = -1;
			CUSTOMER_TABLE customer = new CUSTOMER_TABLE(mDB);
			PO_CUSTOMER_TABLE poTable = new PO_CUSTOMER_TABLE(mDB);
			STAFF_CUSTOMER_TABLE staffCusTable = new STAFF_CUSTOMER_TABLE(mDB);
			ROUTING_CUSTOMER_TABLE routingCusTable = new ROUTING_CUSTOMER_TABLE(
					mDB);
			poID = poTable.getPOIDFromSaleOrderID(dto.saleOrder.saleOrderId);
			dto.saleOrder.poId = poID;
			int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId());
			boolean isDeleted = deleteOrderNotCommit(dto);

			// xoa cac promo detail cua don hang
			SALE_ORDER_PROMO_DETAIL_TABLE promoTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
					mDB);
			boolean isDeletedPromoDetail = promoTable
					.deleteAllPromoDetailOfOrder(dto.saleOrder.saleOrderId) >= 0;

			// xoa cac promo detail cua PO
			PO_CUSTOMER_PROMO_DETAIL_TABLE poPromoTable = new PO_CUSTOMER_PROMO_DETAIL_TABLE(
					mDB);
			boolean isDeletedPoPromoDetail = poPromoTable
					.deleteAllPromoDetailOfPO(dto.saleOrder.poId) >= 0;

			// xoa cac promo cua don hang
			SALE_ORDER_PROMOTION_TABLE salePromotionTable = new SALE_ORDER_PROMOTION_TABLE(
					mDB);
			boolean isDeletedSalePromotion = salePromotionTable
					.deleteAllDetailOfOrder(dto.saleOrder.saleOrderId) >= 0;

			// xoa cac promo detail cua po
			PO_PROMOTION_TABLE poPromotionTable = new PO_PROMOTION_TABLE(mDB);
			boolean isDeletedPoPromotion = poPromotionTable
					.deleteAllDetailOfPO(dto.saleOrder.poId) >= 0;
			// xoa cac CTKM dat hay ko dat lien quan toi don hang
			SALE_PROMO_MAP_TABLE promoMapTable = new SALE_PROMO_MAP_TABLE(
							mDB);
			// xoa cac CTKM lien quan toi sale_order_detail
			boolean isDeletePromoMapSuccess = promoMapTable
							.deleteAllPromoMapOfOrder(dto.saleOrder.saleOrderId) >= 0 ? true
									: false;
			// xoa cac CTKM dat hay ko dat lien quan toi PO
			PO_CUSTOMER_PROMO_MAP_TABLE poCustomerPromoMapTable = new PO_CUSTOMER_PROMO_MAP_TABLE(
									mDB);
			boolean isDeletePOPromoMapSuccess = poCustomerPromoMapTable
					.deleteAllPromoMapOfPO(dto.saleOrder.poId) >= 0 ? true
					: false;

			// Xoa tra tich luy
			RPT_CTTL_PAY_TABLE rptCttlPayTable = new RPT_CTTL_PAY_TABLE(mDB);
			dto.listRptCttPay = rptCttlPayTable
					.deleteAllRptCttlPayOfOrder(dto.saleOrder.saleOrderId);

			boolean isDeletedOrderLotPromotion = true;
			boolean isDeletedPoLotPromotion = true;

			// xoa sale_order_lot + po_lot khi xoa don Vansale
			if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
					.equals(dto.saleOrder.orderType)) {
				SALE_ORDER_LOT_TABLE saleOrderLotTable = new SALE_ORDER_LOT_TABLE(
						mDB);
				PO_CUSTOMER_LOT_TABLE poLotTable = new PO_CUSTOMER_LOT_TABLE(
						mDB);
				isDeletedOrderLotPromotion = saleOrderLotTable
						.deleteAllDetailOfOrder(dto.saleOrder.saleOrderId) > 0;
				isDeletedPoLotPromotion = poLotTable
						.deleteAllDetailOfPo(dto.saleOrder.poId) > 0;
			}

			if (isDeleted && isDeletedPromoDetail && isDeletedPoPromoDetail
					&& isDeletedSalePromotion && isDeletedPoPromotion
					&& isDeletedOrderLotPromotion && isDeletedPoLotPromotion && isDeletePromoMapSuccess && isDeletePOPromoMapSuccess) {
				// Update log of sale order delete
				LOG_TABLE logTable = new LOG_TABLE(mDB);
				logTable.updateState(String.valueOf(dto.saleOrder.saleOrderId),
						Integer.parseInt(LogDTO.STATE_ORDER_DELETED));

				if (dto.isFinalOrder == 1) {
					// delete record cuoi cung thi cap nhat last_order
					CustomerDTO cus = new CustomerDTO();
					cus.customerId = dto.getCustomerId();
					cus.setLastOrder(dto.lastOrder);
					// updateCustomer = customer.updateLastOrder(cus) > 0 ? true
					// : false;
					customer.updateLastOrder(cus);

					// cap nhat last order trong staff customer
					StaffCustomerDTO staffDto = new StaffCustomerDTO();
					staffDto.customerId = dto.getCustomerId();
					staffDto.staffId = staffId;
					staffDto.shopId = shopId + "";
					staffDto.lastOrder = dto.lastOrder;
					staffCusTable.updateLastOrder(staffDto);

					// cap nhat routing customer
					long routingCusId = routingCusTable
							.getRoutingCustomerIdOfCustomer(staffId, shopId,
									dto.getCustomerId());
					RoutingCustomerDTO routingCusDTO = new RoutingCustomerDTO();
					routingCusDTO.routingCustomerId = routingCusId;
					routingCusDTO.lastOrder = dto.lastOrder;
					routingCusTable.updateLastOrder(routingCusDTO);
				}

				// Delete action log
				ACTION_LOG_TABLE action_log = new ACTION_LOG_TABLE(mDB);
				action_log.deleteActionLogWhenDeleteOrder(actionLogDTO);

				// if (updateCustomer && updateStaffCus) {
				mDB.setTransactionSuccessful();
				result = true;
				// }
			}

		} catch (Exception ex) {
			result = false;
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	public ActionLogDTO checkCreateOrderFromActionLog(String staffId,
			String customerId, String shopId) {
		ACTION_LOG_TABLE table = new ACTION_LOG_TABLE(mDB);
		ActionLogDTO dto = table.checkCreateOrderFromActionLog(staffId,
				customerId, shopId);
		return dto;
	}

	/**
	 * Lay thong tin don hang luu tam
	 *
	 * @author: TruongHN
	 * @return: NotifyOrderDTO
	 * @throws:
	 */
	public String getOrderDraft(String staffId, String customerId) {
		String orderId = "";
		try {
			if (mDB != null && mDB.isOpen()) {
				// lay so luong don hang tra ve trong ngay tu NPP
				SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
				orderId = orderTable.getOrderDaftInday(staffId, customerId);
			}
		} catch (Exception e) {
			MyLog.e("getOrderDraft", "fail", e);
		}
		return orderId;
	}

	/**
	 * update oder have detail not commit server
	 *
	 * @author: PhucNT
	 * @param dto
	 * @throws Exception
	 * @return: boolean
	 * @throws: note:
	 */
	private boolean deleteOrderNotCommit(SaleOrderViewDTO dto) throws Exception {
		boolean result = false;
		boolean resultPO = true;
		try {
			String[] params = { "" + dto.getSaleOrderId() };
			String[] paramsPO = { "" + dto.saleOrder.poId };
			String[] childTables = { "SALE_ORDER_DETAIL" };
			result = SQLUtils.getInstance().deleteCascade("SALE_ORDER",
					"SALE_ORDER_ID = ? ", params, childTables);
			if (dto.saleOrder.poId > 0
			// && dto.saleOrder.orderType
			// .equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)
			) {
				// xoa trong PO
				resultPO = SQLUtils.getInstance().deleteCascade(
						PO_CUSTOMER_TABLE.TABLE_NAME, "PO_CUSTOMER_ID = ? ",
						paramsPO,
						new String[] { PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME });
			}
		} catch (Exception e) {
			result = false;
			ServerLogger.sendLog(
					e.getMessage() + " saleorderid: " + dto.getSaleOrderId(),
					"deleteOrderNotCommit", false,
					TabletActionLogDTO.LOG_EXCEPTION);
		}
		return (result && resultPO);
	}

	/**
	 * Cap nhat sua don hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	 */
	public JSONArray generateSqlUpdateOrder(OrderViewDTO dto,
			List<SaleOrderDetailDTO> listSaleOrderDetail) throws Exception {
		long saleOrderId = dto.orderInfo.saleOrderId;
		// Contain sql
		JSONArray listSql = new JSONArray();

		try {
			// Generate sql to server
			listSql.put(dto.orderInfo.generateEditOrderSql());

			// Luu cac chi tiet don hang moi (san pham ban)
			SaleOrderDetailDTO saleDetailE = new SaleOrderDetailDTO();
			saleDetailE.salesOrderId = dto.orderInfo.saleOrderId;

			// Generate sql to server
			listSql.put(saleDetailE.generateDeleteOrderSql(saleOrderId));

			SaleOrderPromoDetailDTO promoDetailE = new SaleOrderPromoDetailDTO();
			listSql.put(promoDetailE.generateDeleteByOrderSql(saleOrderId));

			SaleOrderPromotionDTO salePromotion = new SaleOrderPromotionDTO();
			listSql.put(salePromotion.generateDeleteByOrderSql(saleOrderId));

			// delete cac CTKM dat hay ko dat lien quan toi don hang
			SalePromoMapDTO salePromoMap = new SalePromoMapDTO();
			listSql.put(salePromoMap.generateDeletePromoMapByOrderSql(saleOrderId));

			// Xoa tra thuong KM tich luy
			for (RptCttlPayDTO rptPayDto : dto.listRptCttPay) {
				listSql.put(rptPayDto.generateDeleteRptCttlPay());
				listSql.put(rptPayDto.generateDeleteRptCttlDetailPay());
			}

			// Luu san pham & tang giam ton kho
			for (OrderDetailViewDTO detailViewDTO : dto.listProduct) {
				// Generate sql send to server
				listSql.put(detailViewDTO.orderDetailDTO
						.generateCreateSqlForUpdate());

				// insert them phan chi tiet khuyen mai
				if (detailViewDTO.listPromoDetail != null
						&& !detailViewDTO.listPromoDetail.isEmpty()) {
					for (SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
						if (promoDetail.discountAmount >= 0) {
							listSql.put(promoDetail
									.generateJsonSaleOrderPromoDetail());
						}
					}
				}

				//insert them phan chi tiet khuyen mai
				if (detailViewDTO.lstPromotionMap != null && !detailViewDTO.lstPromotionMap.isEmpty()) {
					for (SalePromoMapDTO promoDetail : detailViewDTO.lstPromotionMap) {
							listSql.put(promoDetail.generateInsertSql());
					}
				}

			}

			// tien KM tich luy
			// for (SaleOrderPromoDetailDTO promoDetail : dto.listPromoDetail) {
			// listSql.put(promoDetail.generateJsonSaleOrderPromoDetail());
			// }

			// Tra KM tich luy
			for (OrderDetailViewDTO promotion : dto.listPromotionAccumulation) {
				double proDisAmount = promotion.orderDetailDTO.getDiscountAmount();
				if (promotion.rptCttlPay != null
					&& (proDisAmount > 0 || (BigDecimal.valueOf(proDisAmount).compareTo(BigDecimal.ZERO) == 0 && promotion.remainDiscount == 0))) {
//					&& (proDisAmount > 0 || (proDisAmount == 0 && promotion.remainDiscount == 0))) {
					listSql.put(promotion.rptCttlPay.generateInsertRptCttlPay());

					for (RptCttlDetailPayDTO payCttlPayDetail : promotion.rptCttlPay.rptCttlDetailList) {
						if (payCttlPayDetail.quantityPromotion > 0
								|| payCttlPayDetail.amountPromotion > 0) {
							listSql.put(payCttlPayDetail
									.generateJsonRptCttlDetailPay());
						}
					}
				}
			}

			// tao json so suat
			if (dto.productQuantityReceivedList != null) {
				for (SaleOrderPromotionDTO salePromotionInsert : dto.productQuantityReceivedList) {
					listSql.put(salePromotionInsert
							.generateJsonInsertSaleOrderPromotion());
				}
			}

			// Tang ton kho //Khong cap nhat ton kho nua -> de thuc hien chuc
			// nang hien thi mau` ton kho
			// Chi co presale moi sua duoc don hang
			/*
			 * for (StockTotalDTO stockTotalDTO : listStockTotal) { // Generate
			 * sql send to server
			 * listSql.put(stockTotalDTO.generateDescreaseSqlPresale()); }
			 */

			if (dto.isFinalOrder == 1) {
				// cap nhat ban customer
				listSql.put(dto.customer.generateUpdateFromOrderSql());

				// cap nhat staff_customer
				listSql.put(dto.staffCusDto
						.generateInsertOrUpdateFromOrderSql());

				// cap nhat routing customer
				listSql.put(dto.routingCusDTO.generateUpdateFromOrderSql());
			}

			if (dto.orderInfo.orderType
					.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
				// insert or update cong no
				// neu chua co no cua KH thi insert 1 debit cho KH moi
				if (dto.debitIdExist <= 0) {
					listSql.put(dto.debitDto.generateInsertFromOrderSql());
				}
				// Insert chi tiet cong no
				listSql.put(dto.debitDetailDto
						.generateInsertOrUpdateDebitDetail());
			}

			// thong nhat chi cap nhat so suat luc in don vansale tren web
//			if (dto.orderInfo.orderType
//					.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
//				// tang so xuat cho don hang vansale
//				if (dto.listIncreasePromoQuantityReceived != null) {
//					for (QuantityRevicevedDTO quanDto : dto.listIncreasePromoQuantityReceived) {
//						quanDto.generateInscreasePromotionSqlVansale(listSql);
//					}
//				}
//			}
//			if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
//				// tang so xuat cho don hang vansale
//			}
			if (dto.listIncreasePromoQuantityReceived != null) {
				for (QuantityRevicevedDTO quanDto : dto.listIncreasePromoQuantityReceived) {
					quanDto.generateInsertPromotionMapDelta(listSql);
				}
			}

		} catch (Exception e) {
		} finally {
		}

		return listSql;
	}

	/**
	 * Cap nhat sua don hang - clone PO_CUSTOMER
	 *
	 * @author: DungNX3
	 * @param dto
	 * @param listSql
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	 */
	public JSONArray generateSqlUpdatePOCustomer(OrderViewDTO dto,
			List<SaleOrderDetailDTO> listSaleOrderDetail, JSONArray listSql)
			throws Exception {

		try {
			// Generate sql to server
			listSql.put(dto.orderInfo.generateEditPOSql());
			// Luu cac chi tiet don hang moi (san pham ban)
			SaleOrderDetailDTO saleDetailE = new SaleOrderDetailDTO();
			// Generate sql to server
			listSql.put(saleDetailE
					.generateDeletePODetailSql(dto.orderInfo.poId));

			SaleOrderPromoDetailDTO promoDetailE = new SaleOrderPromoDetailDTO();
			listSql.put(promoDetailE.generateDeleteByPOSql(dto.orderInfo.poId));

			SaleOrderPromotionDTO salePromotion = new SaleOrderPromotionDTO();
			listSql.put(salePromotion.generateDeleteByPoSql(dto.orderInfo.poId));

			// delete cac CTKM dat hay ko dat lien quan toi PO customer
			POCustomerPromoMapDTO poCustomerPromoMap = new POCustomerPromoMapDTO();
			listSql.put(poCustomerPromoMap.generateDeletePromoMapByPOSql(dto.orderInfo.poId));

			// Luu san pham
			for (OrderDetailViewDTO detailViewDTO : dto.listProduct) {
				// Generate sql send to server
				listSql.put(detailViewDTO.orderDetailDTO
						.generateCreatePOSqlForUpdate());

				// insert them phan chi tiet khuyen mai
				if (detailViewDTO.listPromoDetail != null
						&& !detailViewDTO.listPromoDetail.isEmpty()) {
					for (SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
						if (promoDetail.discountAmount > 0) {
							listSql.put(promoDetail
									.generateJsonPOCustomerPromoDetail());
						}
					}
				}
				//insert them phan chi tiet khuyen mai cho PO
				if (detailViewDTO.lstPromotionMap != null && !detailViewDTO.lstPromotionMap.isEmpty()) {
					for (SalePromoMapDTO promoDetail : detailViewDTO.lstPromotionMap) {
							listSql.put(promoDetail.generateInsertPOCustomerPromoMapSql());
					}
				}
			}

			// tao json so suat
			if (dto.productQuantityReceivedList != null) {
				for (SaleOrderPromotionDTO salePromotionInsert : dto.productQuantityReceivedList) {
					listSql.put(salePromotionInsert
							.generateJsonInsertPoPromotion());
				}
			}
		} catch (Exception e) {
		} finally {
		}

		return listSql;
	}

	/**
	 *
	 * Update info for order detail to insert db
	 *
	 * @author: Nguyen Thanh Dung
	 * @param detailViewDTO
	 * @param dto
	 * @param saleOrderId
	 * @param saleOrderDetailId
	 * @param poDetailId
	 * @param poDetailId
	 * @param createUser
	 * @param updateUser
	 * @param staffId
	 * @param shopId
	 * @return: void
	 * @throws:
	 */
	private void createOrderDetailInfo(OrderDetailViewDTO detailViewDTO,
			OrderViewDTO dto, long saleOrderId, long saleOrderDetailId,
			long poId, long poDetailId, String createUser, String updateUser,
			int staffId, int shopId) {
		detailViewDTO.orderDetailDTO.salesOrderId = saleOrderId;
		detailViewDTO.orderDetailDTO.poId = poId;
		detailViewDTO.orderDetailDTO.poDetailId = poDetailId;
		detailViewDTO.orderDetailDTO.salesOrderDetailId = saleOrderDetailId;
		detailViewDTO.orderDetailDTO.createUser = createUser;
		detailViewDTO.orderDetailDTO.updateUser = updateUser;
		detailViewDTO.orderDetailDTO.createDate = dto.orderInfo.createDate;
		//detailViewDTO.orderDetailDTO.orderDate = dto.orderInfo.createDate;
		// cap nhat lai thoi gian edit dong hang
		detailViewDTO.orderDetailDTO.orderDate = dto.orderInfo.orderDate;
		detailViewDTO.orderDetailDTO.updateDate = dto.orderInfo.updateDate;
		// detailViewDTO.orderDetailDTO.isFreeItem = 1;
		detailViewDTO.orderDetailDTO
				.setType(AbstractTableDTO.TableType.SALE_ORDER_DETAIL);
		detailViewDTO.orderDetailDTO.shopId = shopId;
		detailViewDTO.orderDetailDTO.staffId = staffId;

	}

	/**
	 * Generata sql sua don hang gui server
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	 */
	public SaleOrderDataResult updateOrder(OrderViewDTO dto,
			List<SaleOrderDetailDTO> listSaleOrderDetail) throws Exception {
		SaleOrderDataResult result = new SaleOrderDataResult();
		long saleOrderId = dto.orderInfo.saleOrderId;
		long saleOrderDetailId = -1;
		long poId = -1;
		long poDetailId = -1;
		long saleOrderPromoDetailId = -1;
		boolean insertSuccess = false;
		String createUser = dto.orderInfo.createUser;
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId());

		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);
		SALE_ORDER_PROMO_DETAIL_TABLE promoDetailTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
				mDB);
		SALE_ORDER_LOT_TABLE saleOrderLotTable = new SALE_ORDER_LOT_TABLE(mDB);
		PO_CUSTOMER_LOT_TABLE poLotTable = new PO_CUSTOMER_LOT_TABLE(mDB);
		PO_CUSTOMER_PROMO_DETAIL_TABLE poPromoDetailTable = new PO_CUSTOMER_PROMO_DETAIL_TABLE(
				mDB);
		PO_CUSTOMER_TABLE poCustomerTable = new PO_CUSTOMER_TABLE(mDB);
		PO_CUSTOMER_DETAIL_TABLE poCustomerDetailTable = new PO_CUSTOMER_DETAIL_TABLE(
				mDB);
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		STAFF_CUSTOMER_TABLE staffCusTable = new STAFF_CUSTOMER_TABLE(mDB);
		ROUTING_CUSTOMER_TABLE routingCusTable = new ROUTING_CUSTOMER_TABLE(mDB);
		RPT_CTTL_PAY_TABLE rptCttlPayTable = new RPT_CTTL_PAY_TABLE(mDB);
		RPT_CTTL_DETAIL_PAY_TABLE rptCttlDetailPayTable = new RPT_CTTL_DETAIL_PAY_TABLE(
				mDB);
		SALE_PROMO_MAP_TABLE promoMapTable = new SALE_PROMO_MAP_TABLE(
				mDB);
		PO_CUSTOMER_PROMO_MAP_TABLE poCustomerPromoMapTable = new PO_CUSTOMER_PROMO_MAP_TABLE(
				mDB);
		long routingCustomerId = routingCusTable
				.getRoutingCustomerIdOfCustomer(staffId, shopId,
						dto.customer.customerId);
		dto.routingCusDTO.routingCustomerId = routingCustomerId;
		TABLE_ID tableId = new TABLE_ID(mDB);
		SALE_ORDER_PROMOTION_TABLE quantityReceivedTable = new SALE_ORDER_PROMOTION_TABLE(
				mDB);
		PO_PROMOTION_TABLE quantityReceivedPoTable = new PO_PROMOTION_TABLE(mDB);
		SALE_ORDER_PROMO_LOT_TABLE saleOrderPromoLotTable = new SALE_ORDER_PROMO_LOT_TABLE(mDB);
		PO_CUSTOMER_PROMO_LOT_TABLE poCustomerPromoLotTable = new PO_CUSTOMER_PROMO_LOT_TABLE(mDB);
		poId = poCustomerTable.getPOIDFromSaleOrderID(saleOrderId);
		dto.orderInfo.poId = poId;
		// max id bang rpt pay
		long rptCttlPayId = tableId.getMaxIdTime(RPT_CTTL_PAY_TABLE.TABLE_NAME);
		long rptCttlDetailPayId = tableId
				.getMaxIdTime(RPT_CTTL_DETAIL_PAY_TABLE.TABLE_NAME);
		// sinh lai order number khi chinh sua don hang
		// neu doi tu don hang nay sang don hang khac
		String strOrderId = String.valueOf(saleOrderId);
		StringBuilder orderNumber = new StringBuilder();
		String userId = ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		// add them cycleId
		dto.orderInfo.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);

		if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			if (dto.orderInfo.isVisitPlan == 1) {
				// trong tuyen
				orderNumber.append("V");
			} else {
				// ngoai tuyen
				orderNumber.append("E");
			}
		} else if (dto.orderInfo.orderType
				.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			if (dto.orderInfo.isVisitPlan == 1) {
				// trong tuyen
				orderNumber.append("T");
			} else {
				// ngoai tuyen
				orderNumber.append("L");
			}
		}

		if (userId.length() >= 5) {
			orderNumber.append(userId.substring(userId.length() - 5));
		} else {
			// 975
			String zero = "00000";
			int lengId = userId.length();
			orderNumber.append(zero.substring(lengId) + userId);
		}

		if (strOrderId.length() >= 8) {
			orderNumber.append(strOrderId.substring(strOrderId.length() - 8));
		} else {
			String zero = "00000000";
			int lengId = strOrderId.length();
			orderNumber.append(zero.substring(lengId) + strOrderId);
		}

		dto.orderInfo.orderNumber = orderNumber.toString();

		if (StringUtil.isNullOrEmpty(dto.orderInfo.shopCode)) {
			dto.orderInfo.shopCode = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopCode();
		}
		dto.orderInfo.approvedStep = 0;

		// Reset danh sp
		dto.listProduct = new ArrayList<OrderDetailViewDTO>();

		// Ds sp ban
		for (OrderDetailViewDTO product : dto.listBuyOrders) {
			if (product.orderDetailDTO.quantity > 0) {
				dto.listProduct.add(product);
			}
		}

		// KM sp
		dto.listProduct.addAll(dto.listPromotionOrders);

		// KM don hang - loai sp
		for (OrderDetailViewDTO promotion : dto.listPromotionOrder) {
			if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				dto.listProduct.addAll(promotion.listPromotionForPromo21);
			}
		}

		// KM tich luy
		for (OrderDetailViewDTO promotion : dto.listPromotionAccumulation) {
			// KM duoc sp
			if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
				dto.listProduct.addAll(promotion.listPromotionForPromo21);
				// } else {
				// for (SaleOrderPromoDetailDTO promoDetail :
				// promotion.listPromoDetail) {
				// if(promoDetail.discountAmount > 0) {
				// dto.listPromoDetail.add(promoDetail);
				// }
				// }
			}
		}

		// KM mo moi - loai sp
		for (OrderDetailViewDTO promotion : dto.listPromotionNewOpen) {
			if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
				dto.listProduct.addAll(promotion.listPromotionForPromo21);
			}
		}

		// KM keyshop
		for (OrderDetailViewDTO promotion : dto.listPromotionKeyShop) {
			if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
				dto.listProduct.add(promotion);
			}
		}

		// Tong so dong trong sale order detail
		dto.orderInfo.totalDetail = dto.listProduct.size();
		// co tham gia keyshop hay ko
		dto.orderInfo.isRewardKS = dto.isKeyShop;
		// cap nhat lai thoi gian
		dto.orderInfo.accountDate = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		dto.orderInfo.orderDate = DateUtils.now();
		try {
			mDB.beginTransaction();
			boolean updatePOSucess = dto.orderInfo.poId > 0 ? poCustomerTable
					.update(dto.orderInfo) > 0 : true;
			// Update sale order to sale order table
			if (orderTable.update(dto.orderInfo) > 0 && updatePOSucess) {
				// Xoa cac order detail cu~
				boolean deleteDetailSuccess = orderDetailTable
						.deleteAllDetailOfOrder(saleOrderId) > 0 ? true : false;

				boolean deletePromoDetailSuccess = promoDetailTable
						.deleteAllPromoDetailOfOrder(saleOrderId) >= 0 ? true
						: false;
				boolean deletePoPromoDetailSuccess = poPromoDetailTable
						.deleteAllPromoDetailOfPO(poId) >= 0 ? true : false;

				// xoa PO_CUSTOMER_DETAIL
				boolean deletePODetailSuccess = dto.orderInfo.poId > 0 ? (poCustomerDetailTable
						.deleteAllDetailOfOrder(dto.orderInfo.poId) > 0 ? true
						: false) : true;

				// xoa cac sale_order_promotion, so suat
				boolean deleteSaleOrderPromotionSuccess = quantityReceivedTable
						.deleteAllDetailOfOrder(saleOrderId) >= 0 ? true
						: false;
				boolean deletePOPromotionSuccess = quantityReceivedPoTable
						.deleteAllDetailOfPO(dto.orderInfo.poId) >= 0 ? true
						: false;
				// xoa cac CTKM lien quan toi sale_order_detail
				boolean deletePromoMapSuccess = promoMapTable
								.deleteAllPromoMapOfOrder(saleOrderId) >= 0 ? true
										: false;
				// xoa cac CTKM lien quan toi po
				boolean deletePOPromoMapSuccess = poCustomerPromoMapTable
						.deleteAllPromoMapOfPO(dto.orderInfo.poId) >= 0 ? true
						: false;

				// xoa saleOrderLotTable vansale
				boolean deleteSaleOrderLot = true;
				boolean isLotExist = saleOrderLotTable.isLotExist(saleOrderId);
				boolean deleteSaleOrderPromoLot = true; // xoa promoLot
				if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
						.equals(dto.orderInfo.orderType)) {
					dto.listSaleOrderLot = new ArrayList<SaleOrderLotDTO>();
					dto.listSaleOrderPromoLot = new ArrayList<SaleOrderPromoLotDTO>();
					if (isLotExist) {
						deleteSaleOrderPromoLot = saleOrderPromoLotTable.deleteAllDetailOfOrder(saleOrderId) > 0 ? true : false;
						if (deleteSaleOrderPromoLot) {
							deleteSaleOrderPromoLot = poCustomerPromoLotTable
									.deleteAllDetailOfOrder(dto.orderInfo.poId) > 0 ? true
									: false;
						}
						deleteSaleOrderLot = saleOrderLotTable
								.deleteAllDetailOfOrder(saleOrderId) > 0 ? true
								: false;
						if (deleteSaleOrderLot) {
							deleteSaleOrderLot = poLotTable
									.deleteAllDetailOfPo(dto.orderInfo.poId) > 0 ? true
									: false;
						}
					}
				}
				// Xoa tra tich luy
				dto.listRptCttPay = rptCttlPayTable
						.deleteAllRptCttlPayOfOrder(saleOrderId);
				// xoa detail cua order + po thanh cong
				// xoa promo detail cua don hang + po thanh cong
				// xoa so suat cua don hang thanh cong
				if ((deleteDetailSuccess && deletePODetailSuccess
						&& deletePromoDetailSuccess
						&& deletePoPromoDetailSuccess
						&& deleteSaleOrderPromotionSuccess
						&& deletePOPromotionSuccess && deleteSaleOrderLot && deletePromoMapSuccess && deletePOPromoMapSuccess && deleteSaleOrderPromoLot)
						|| (deleteDetailSuccess == false
								&& deletePODetailSuccess == false && dto.orderInfo.approved == -1)
						|| (deleteDetailSuccess == false
								&& deletePODetailSuccess == false && dto.orderInfo.approved == 0)) {
					// Get max id
					saleOrderDetailId = tableId
							.getMaxIdTime(SALES_ORDER_DETAIL_TABLE.TABLE_NAME);
					poDetailId = tableId
							.getMaxIdTime(PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);
					// them id promo detail
					saleOrderPromoDetailId = tableId
							.getMaxIdTime(SALE_ORDER_PROMO_DETAIL_TABLE.TABLE_NAME);
					long poPromoDetailId = tableId
							.getMaxIdTime(PO_CUSTOMER_PROMO_DETAIL_TABLE.TABLE_NAME);

					long saleOrderLotId = tableId
							.getMaxIdTime(SALE_ORDER_LOT_TABLE.TABLE_NAME);
					long poLotId = tableId
							.getMaxIdTime(PO_CUSTOMER_LOT_TABLE.TABLE_NAME);
					// lay max promo_map id
					long salePromoMapId = tableId
										.getMaxIdTime(SALE_PROMO_MAP_TABLE.TABLE_NAME);
					// lay max promo_map id
					long poCustomerPromoMapId = tableId
							.getMaxIdTime(PO_CUSTOMER_PROMO_MAP_TABLE.TABLE_NAME);
					// max Id sale promot lot
					long saleOrderPromoLotId = tableId
							.getMaxIdTime(SALE_ORDER_PROMO_LOT_TABLE.TABLE_NAME);
					// max Id sale promot lot
					long poCustomerPromoLotId = tableId
							.getMaxIdTime(PO_CUSTOMER_PROMO_LOT_TABLE.TABLE_NAME);
					long debitDetailId = tableId.getMaxIdTime(DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);
					long debitId = tableId.getMaxIdTime(DEBIT_TABLE.TABLE_NAME);
					// Luu san pham & tang giam ton kho
					// List<StockTotalDTO> listStockTotal = new
					// ArrayList<StockTotalDTO>();
					// boolean hasOrderDetail = false;
					boolean insertOrderDetail = true;
					boolean insertPOOrderDetail = true;
					boolean increasePromotion = true;
					boolean insertSaleOrderLot = true;
					// mac dinh la thanh cong, khi gap 1 that bai ->
					// insert that bai
					boolean insertArrayPromoDetail = true;
					boolean insertArrayPromoMap = true;
					boolean insertArrayPOCustomerPromoMap = true;
					ArrayList<String> listPromotionCode = null;
					for (OrderDetailViewDTO detailViewDTO : dto.listProduct) {
						createOrderDetailInfo(detailViewDTO, dto, saleOrderId,
								saleOrderDetailId, dto.orderInfo.poId,
								poDetailId, createUser,
								dto.orderInfo.updateUser, staffId, shopId);

						// Check insert success
						insertOrderDetail = orderDetailTable
								.insert(detailViewDTO.orderDetailDTO) > 0 ? true
								: false;

						insertPOOrderDetail = dto.orderInfo.poId > 0 ? (poCustomerDetailTable
								.insert(detailViewDTO.orderDetailDTO) > 0 ? true
								: false)
								: true;

						// them phan insert sale_order_lot
						if (insertOrderDetail && insertPOOrderDetail) {
							if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
									.equals(dto.orderInfo.orderType)) {
								SaleOrderLotDTO saleLotDto = new SaleOrderLotDTO(
										detailViewDTO);
								saleLotDto.saleOrderLotId = saleOrderLotId;
								saleLotDto.poLotId = poLotId;
								insertSaleOrderLot = saleOrderLotTable
										.insert(saleLotDto) > 0
										&& poLotTable.insert(saleLotDto) > 0 ? true
										: false;
								if (insertSaleOrderLot) {
									saleOrderLotId++;
									poLotId++;
									// them vao danh sach sale_order_lot
									dto.listSaleOrderLot.add(saleLotDto);
								}
							}
						}

						// insert them phan chi tiet khuyen mai
						if (detailViewDTO.listPromoDetail != null
								&& !detailViewDTO.listPromoDetail.isEmpty()) {
							for (SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
								if (promoDetail.discountAmount >= 0) {
									// cap nhat staff
									promoDetail.staffId = detailViewDTO.orderDetailDTO.staffId;
									promoDetail.shopId = detailViewDTO.orderDetailDTO.shopId
											+ "";
									promoDetail.orderDate = detailViewDTO.orderDetailDTO.orderDate;
									promoDetail.saleOrderDetailId = saleOrderDetailId;
									promoDetail.saleOrderPromoDetailId = saleOrderPromoDetailId;
									promoDetail.saleOrderId = saleOrderId;

									// dung cho po
									promoDetail.poId = poId;
									promoDetail.poDetailId = poDetailId;
									promoDetail.poPromoDetailId = poPromoDetailId;

									if (promoDetailTable.insert(promoDetail) > 0
											&& poPromoDetailTable
													.insert(promoDetail) > 0) {
										// neu insert thanh cong thi tang id promo
										// detail
										saleOrderPromoDetailId++;
										// neu insert thanh cong thi tang id po
										// promo detail
										poPromoDetailId++;
										if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE
												.equals(dto.orderInfo.orderType)) {
											if(saleOrderPromoLotTable.insert(
													promoDetail, saleOrderLotId,
													saleOrderPromoLotId) > 0
											&& poCustomerPromoLotTable.insert(
													promoDetail, poLotId,
													poCustomerPromoLotId) > 0){
											// neu insert thanh cong thi tang id saleOrderPromoLot len 1
											saleOrderPromoLotId++;
											// insert poCustomerPomoLot thanh cong
											poCustomerPromoLotId++;
											dto.listSaleOrderPromoLot
													.add(new SaleOrderPromoLotDTO(
															promoDetail,
															saleOrderPromoLotId,
															poCustomerPromoLotId,
															saleOrderLotId,
															poLotId));
											}else{
												insertArrayPromoDetail = false;
											}
										}

									} else {
										insertArrayPromoDetail = false;
									}
								}
							}
						}

						// insert them phan thong tin sp dat hay ko dat
						if (insertOrderDetail && insertPOOrderDetail
								&& detailViewDTO.lstPromotionMap != null
								&& !detailViewDTO.lstPromotionMap.isEmpty()) {
							for (SalePromoMapDTO promoMap : detailViewDTO.lstPromotionMap) {
									promoMap.salePromoMapId = salePromoMapId;
									promoMap.staffId = detailViewDTO.orderDetailDTO.staffId;
									promoMap.saleOrderDetailId = saleOrderDetailId;
									promoMap.saleOrderId = saleOrderId;

									// dung cho po customer map
									promoMap.poCustomerId = poId;
									promoMap.poCustomerDetailId = poDetailId;
									if (promoMapTable.insert(promoMap) > 0) {
										salePromoMapId++;
									} else {
										insertArrayPromoMap = false;
									}
//								}
							}
						}

						// insert them phan thong tin sp dat hay ko dat cho po
						if (insertOrderDetail && insertPOOrderDetail
								&& detailViewDTO.lstPromotionMap != null
								&& !detailViewDTO.lstPromotionMap.isEmpty()) {
							for (SalePromoMapDTO promoMap : detailViewDTO.lstPromotionMap) {
									promoMap.poCustomerPromoMapId = poCustomerPromoMapId;
									POCustomerPromoMapDTO poCustomerMap = new POCustomerPromoMapDTO();
									poCustomerMap.poCustomerPromoMapId = poCustomerPromoMapId;
									poCustomerMap.poCustomerId = promoMap.poCustomerId;
									poCustomerMap.poCustomerDetailId = promoMap.poCustomerDetailId;
									poCustomerMap.programCode = promoMap.programCode;
									poCustomerMap.staffId = promoMap.staffId;
									poCustomerMap.status = promoMap.status;
									if (poCustomerPromoMapTable.insert(poCustomerMap) > 0) {
										poCustomerPromoMapId++;
									} else {
										insertArrayPOCustomerPromoMap = false;
									}
//								}
							}
						}

						if (insertOrderDetail && insertPOOrderDetail
								&& insertArrayPromoDetail && insertArrayPromoMap && insertArrayPOCustomerPromoMap) {
							saleOrderDetailId += 1;
							poDetailId += 1;
						} else {
							break;
						}
					}

					// KM tich luy
					boolean insertRptCttlPay = true;
					boolean insertRptCttlDetailPay = true;
					if (insertOrderDetail && insertPOOrderDetail) {
						for (OrderDetailViewDTO promotion : dto.listPromotionAccumulation) {
							// insert cttl pay cua KM tich luy
							double proDisAmount = promotion.orderDetailDTO.getDiscountAmount();
							if (promotion.rptCttlPay != null
									&& (proDisAmount > 0 || (BigDecimal.valueOf(proDisAmount).compareTo(BigDecimal.ZERO) == 0 && promotion.remainDiscount == 0))) {
//									&& (proDisAmount > 0 || (proDisAmount == 0 && promotion.remainDiscount == 0))) {
								promotion.rptCttlPay.rptCttlPayId = rptCttlPayId;
								promotion.rptCttlPay.saleOrderId = saleOrderId;
								promotion.rptCttlPay.totalQuantityPayPromotion = StringUtil
										.roundDoubleDown(
												"#.##",
												promotion.rptCttlPay.totalQuantityPayPromotion);
								insertRptCttlPay = rptCttlPayTable
										.insert(promotion.rptCttlPay) > 0 ? true
										: false;
								if (insertRptCttlPay) {
									for (RptCttlDetailPayDTO payCttlPayDetail : promotion.rptCttlPay.rptCttlDetailList) {
										if (payCttlPayDetail.quantityPromotion > 0
												|| payCttlPayDetail.amountPromotion > 0) {
											payCttlPayDetail.rptCttlDetailPayId = rptCttlDetailPayId;
											payCttlPayDetail.rptCttlPayId = rptCttlPayId;
											payCttlPayDetail.quantityPromotion = StringUtil
													.roundDoubleDown(
															"#.##",
															payCttlPayDetail.quantityPromotion);
											insertRptCttlDetailPay = rptCttlDetailPayTable
													.insert(payCttlPayDetail) > 0 ? true
													: false;

											if (insertRptCttlDetailPay) {
												rptCttlDetailPayId++;
											} else {
												break;
											}
										}
									}
									rptCttlPayId++;
								} else {
									break;
								}
							}
						}
					}

					// Luu so suat
					boolean insertQuantityReceived = true;
					if (insertOrderDetail && insertPOOrderDetail
							&& insertSaleOrderLot && insertArrayPromoDetail
							&& increasePromotion && insertRptCttlPay
							&& insertRptCttlDetailPay
							&& dto.productQuantityReceivedList.size() > 0) {
						long quantityReceivedId = tableId
								.getMaxIdTime(SALE_ORDER_PROMOTION_TABLE.TABLE_NAME);
						long quantityReceivedPoId = tableId
								.getMaxIdTime(PO_PROMOTION_TABLE.TABLE_NAME);
						// Check lai quantity receive sau khi sua
						dto.checkQuantityReceived();
						for (SaleOrderPromotionDTO quantityReceived : dto.productQuantityReceivedList) {
							quantityReceived.saleOrderPromotionId = quantityReceivedId;
							quantityReceived.poPromotionId = quantityReceivedPoId;
							// sale id
							quantityReceived.saleOrderId = dto.orderInfo.saleOrderId;
							// po id
							quantityReceived.poId = dto.orderInfo.poId;
							quantityReceived.shopId = shopId;
							quantityReceived.staffId = staffId;
							quantityReceived.orderDate = dto.orderInfo.orderDate;
							quantityReceived.createDate = dto.orderInfo.orderDate;
							quantityReceived.updateDate = dto.orderInfo.updateDate;
							quantityReceived.updateUser = dto.orderInfo.updateUser;
							quantityReceived.createUser = createUser;
							//Chi app dung cho presale
							if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
								quantityReceived.promotionDetail = quantityReceived.generatePromotionDetail();
							}

							insertQuantityReceived = quantityReceivedTable
									.insert(quantityReceived) > 0
									&& quantityReceivedPoTable
											.insert(quantityReceived) > 0 ? true
									: false;

							if (insertQuantityReceived) {
								quantityReceivedId++;
								quantityReceivedPoId++;
							} else {
								break;
							}
						}
					}

					boolean isVaildIncreasePromoQuanReceived = true;
					// ***
					// tang so xuat KM vansale, neu la don vansale, khong phai
					// don hang tam
					// ***
					boolean insertMapDelta = true;
//					if (dto.orderInfo.orderType
//							.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
//							&& dto.orderInfo.approved != -1) {
//						HashMap<Long, QuantityRevicevedDTO> mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();
//						// nhap chung nhieu group cua 1 chuong trinh lai
//						for (SaleOrderPromotionDTO quantityReceived : dto.productQuantityReceivedList) {
//							QuantityRevicevedDTO quanDto = mapQuantityReviceved
//									.get(quantityReceived.promotionProgramId);
//							if (quanDto == null) {
//								quanDto = new QuantityRevicevedDTO(
//										quantityReceived.promotionProgramId,
//										quantityReceived.promotionProgramCode);
//								mapQuantityReviceved.put(
//										quantityReceived.promotionProgramId,
//										quanDto);
//							}
//
//							// cong them so suat
//							quanDto.setQuantityReviceved(quanDto
//									.getQuantityReviceved() + quantityReceived.quantityReceived);
//							// luu thong tin so tien/ so luong nhan duoc cua mot CTKM
//							quanDto.setNumReceived(quanDto.getNumReceived()
//									+ quantityReceived.numReceived);
//							quanDto.setAmountReceived(quanDto
//									.getAmountReceived() + quantityReceived.amountReceived);
//						}
//
//						PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(
//								mDB);
//						PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
//						PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(
//								mDB);
//						// qua tung chuong trinh, check so luong so suat
//						for (QuantityRevicevedDTO quanDto : mapQuantityReviceved
//								.values()) {
//							if (listPromotionCode == null) {
//								listPromotionCode = new ArrayList<String>();
//							}
//
//							PromotionShopMapDTO promotionShopMapDTO = psmTable
//									.getPromotionShopMapByPromotionProgramId(
//											quanDto.getProId(), GlobalInfo
//													.getInstance().getProfile()
//													.getUserData().getInheritShopId());
//
//							int quantityConfigRemain = -1;
//							int numConfigRemain = -1;
//							double amountConfigRemain = -1;
//							if (promotionShopMapDTO != null) {
//								quanDto.setShopMapID(promotionShopMapDTO.promotionShopMapId);
//								// truong hop co map voi shop
//
//								// promotionShopMapDTO.quantityReceived +
//								// quanDto.quantityReviceved <
//								// promotionShopMapDTO.quantityMax
//								// lay promotion_customer_map
//								PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable
//										.getPromotionShopMapByPromotionShopMapID(
//												dto.orderInfo.customerId,
//												dto.orderInfo.shopId,
//												promotionShopMapDTO.promotionShopMapId);
//								PromotionStaffMapDTO promotionStaffMapDTO = pstmTable
//										.getPromotionStaffMapByPromotionShopMapID(
//												dto.orderInfo.staffId,
//												dto.orderInfo.shopId,
//												promotionShopMapDTO.promotionShopMapId);
//
//								// khong cau hinh so suat nhan vien + khach hang
//								if (promotionStaffMapDTO == null
//										&& promotionCustomerMapDTO == null) {
//									// thi su dung so suat cua NPP
//									quantityConfigRemain = promotionShopMapDTO.quantityMax
//											- promotionShopMapDTO.quantityReceived;
//									numConfigRemain = promotionShopMapDTO.numMax
//											- promotionShopMapDTO.numReceived;
//									amountConfigRemain = promotionShopMapDTO.amountMax
//											- promotionShopMapDTO.amountReceived;
//								} else
//								// co khai bao staff map
//								if (promotionStaffMapDTO != null) {
//									quanDto.setStaffMapId(promotionStaffMapDTO.promotionStaffMapId);
//									// thi su dung so suat cua nv
//									quantityConfigRemain = promotionStaffMapDTO.quantityMax
//											- promotionStaffMapDTO.quantityReceived;
//									numConfigRemain = promotionStaffMapDTO.numMax
//											- promotionStaffMapDTO.numReceived;
//									amountConfigRemain = promotionStaffMapDTO.amountMax
//											- promotionStaffMapDTO.amountReceived;
//
//									// co khai bao customer map
//									if (promotionCustomerMapDTO != null) {
//										quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
//										// neu so suat con lai cua kh nho hon
//										// cua nv thi cap nhat lai so suat ton
//										int quantityTemp = promotionCustomerMapDTO.quantityMax
//												- promotionCustomerMapDTO.quantityReceived;
//										int numTemp  = promotionCustomerMapDTO.numMax
//												- promotionCustomerMapDTO.numReceived;
//										double amountTemp = promotionCustomerMapDTO.amountMax
//												- promotionCustomerMapDTO.amountReceived;
//										if (quantityTemp < quantityConfigRemain) {
//											quantityConfigRemain = quantityTemp;
//										}
//										if (numTemp < numConfigRemain) {
//											numConfigRemain = numTemp;
//										}
//										if (amountTemp < amountConfigRemain) {
//											amountConfigRemain = amountTemp;
//										}
//									}
//								} else {
//									quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
//									// co khai bao so suat kh
//									// thi su dung so suat cua kh
//									quantityConfigRemain = promotionCustomerMapDTO.quantityMax
//											- promotionCustomerMapDTO.quantityReceived;
//									numConfigRemain = promotionCustomerMapDTO.numMax
//											- promotionCustomerMapDTO.numReceived;
//									amountConfigRemain = promotionCustomerMapDTO.amountMax
//											- promotionCustomerMapDTO.amountReceived;
//								}
//
//								// neu min so suat con lai khong du thi them vao
//								// danh sach cac chuong trinh thieu so suat
//								if (quantityConfigRemain < quanDto.getQuantityReviceved()
//										|| numConfigRemain < quanDto.getNumReceived()
//										|| amountConfigRemain < quanDto.getAmountReceived()) {
//									listPromotionCode.add(quanDto.getProCode());
//									isVaildIncreasePromoQuanReceived = false;
//								}
//							} else {
//								// neu khong co so suat npp thi them vao danh
//								// sach cac chuong trinh thieu so suat
//								listPromotionCode.add(quanDto.getProCode());
//								isVaildIncreasePromoQuanReceived = false;
//							}
//						}
//
//						// neu cac promotion deu du so suat thi tien hanh tru
//						if (isVaildIncreasePromoQuanReceived) {
//							// tru so suat tung chuong trinh
//							for (QuantityRevicevedDTO quanDto : mapQuantityReviceved
//									.values()) {
//								// tang so xuat shop
//								if (quanDto.getShopMapID() > 0) {
//									isVaildIncreasePromoQuanReceived = psmTable
//											.increaseQuantityRecevie(
//													quanDto.getShopMapID(),
//													quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
//								}
//
//								if (isVaildIncreasePromoQuanReceived
//										&& quanDto.getStaffMapId() > 0) {
//									isVaildIncreasePromoQuanReceived = pcmTable
//											.increaseStaffQuantityRecevie(
//													quanDto.getStaffMapId(),
//													quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
//								}
//
//								if (isVaildIncreasePromoQuanReceived
//										&& quanDto.getCustomerMapId() > 0) {
//									isVaildIncreasePromoQuanReceived = pcmTable
//											.increaseQuantityRecevie(
//													quanDto.getCustomerMapId(),
//													quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
//								}
//
//								// neu fail thi break
//								if (!isVaildIncreasePromoQuanReceived) {
//									break;
//								}
//							}
//						}
//
//						// cap nhat list so suat phai tru
//						dto.listIncreasePromoQuantityReceived = mapQuantityReviceved
//								.values();
//
//					} else
					if (dto.orderInfo.approved != -1) {
						PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(mDB);
						PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
						PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(mDB);

						PROMOTION_MAP_DELTA_TABLE pmdTable = new PROMOTION_MAP_DELTA_TABLE(mDB);
						long promotionMapDeltaId = tableId.getMaxIdTime(PROMOTION_MAP_DELTA_TABLE.TABLE_NAME);
						//Insert vao bang promotion_map_delta
						long insertPromotionMapDeltaId = 0;
						for(QuantityRevicevedDTO quanDto : dto.mapQuantityReviceved.values()) {
							quanDto.setPromotionMapDeltaId(promotionMapDeltaId);
							quanDto.setFromObjectId(dto.orderInfo.saleOrderId);
							quanDto.setOrderAction(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_ACTION_UPDATE);
							quanDto.setSource(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_SOURCE_MOBILE);
							quanDto.setShopId(shopId);
							quanDto.setStaffId(staffId);
							quanDto.setCustomerId(dto.orderInfo.customerId);
							quanDto.setCreateDate(dto.orderInfo.orderDate);
							quanDto.setCreateUser(createUser);
							insertPromotionMapDeltaId = pmdTable.insert(quanDto);

							insertMapDelta = insertPromotionMapDeltaId > 0 ? true: false;

							if (insertMapDelta) {
								promotionMapDeltaId++;
							} else {
								break;
							}

							// tang so xuat shop
							if (quanDto.getShopMapID() > 0) {
								isVaildIncreasePromoQuanReceived = psmTable.increaseQuantityRecevieTotal(
												quanDto.getShopMapID(), quanDto.getQuantityReviceved(), quanDto.getNumReceived(),quanDto.getAmountReceived());
							}

							if (isVaildIncreasePromoQuanReceived
									&& quanDto.getStaffMapId() > 0) {
								isVaildIncreasePromoQuanReceived = pcmTable.increaseStaffQuantityRecevieTotal(
												quanDto.getStaffMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
							}

							if (isVaildIncreasePromoQuanReceived
									&& quanDto.getCustomerMapId() > 0) {
								isVaildIncreasePromoQuanReceived = pcmTable.increaseQuantityRecevieTotal(
												quanDto.getCustomerMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
							}

							// neu fail thi break
							if (!isVaildIncreasePromoQuanReceived) {
								break;
							}
						}

						// cap nhat list so suat phai tru
						dto.listIncreasePromoQuantityReceived = dto.mapQuantityReviceved.values();
					}
					result.listPromotionCode = listPromotionCode;

					if (insertOrderDetail && insertPOOrderDetail
							&& increasePromotion && insertSaleOrderLot && insertMapDelta
							&& insertArrayPromoDetail && insertQuantityReceived
							&& isVaildIncreasePromoQuanReceived
							&& insertRptCttlPay && insertRptCttlDetailPay) {
						// Khong cap nhat ton kho nua -> de thuc hien chuc nang
						// hien thi
						// STOCK_TOTAL_TABLE stockTotalTable = new
						// STOCK_TOTAL_TABLE(mDB);
						// Tang ton kho
						// for (StockTotalDTO stockTotalDTO :
						// listStockTotalDelete) {
						// stockTotalTable.increaseStockTotal2(stockTotalDTO);
						// }

						// Chi co presale moi sua don hang duoc
						/*
						 * if(dto.orderInfo.approved != -1) { for (StockTotalDTO
						 * stockTotalDTO : listStockTotal) { stockTotalTable
						 * .descreaseStockTotalPresale(stockTotalDTO); } }
						 */

						String lastOrder = "";
						if (dto.isFinalOrder == 1) {
							if (dto.orderInfo.getAmount() > 0) {
								lastOrder = dto.orderInfo.orderDate;
							} else {
								lastOrder = dto.lastOrder;
							}
						}

						boolean updateStaff = true;
						boolean insertUpdateStaffCus = true;
						boolean updateRoutingCus = true;

						if (dto.isFinalOrder == 1) {
							// cap nhat customer
							dto.customer.setLastOrder(lastOrder);
							updateStaff = cusTable
									.updateLastOrder(dto.customer) > 0 ? true
									: false;

							// cap nhat staff_customer
							dto.staffCusDto.customerId = dto.customer.customerId;
							dto.staffCusDto.staffId = staffId;
							dto.staffCusDto.shopId = String.valueOf(shopId);
							dto.staffCusDto.lastOrder = lastOrder;
							insertUpdateStaffCus = staffCusTable
									.insertOrUpdate(dto.staffCusDto) > 0 ? true
									: false;

							// cap nhat routing customer
							dto.routingCusDTO.lastOrder = lastOrder;
							updateRoutingCus = routingCusTable
									.updateLastOrder(dto.routingCusDTO) > 0 ? true
									: false;
						}

						boolean increaseDebit = true;
						boolean insertDebitDetail = true;
						if (dto.orderInfo.orderType
								.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
								&& dto.orderInfo.approved != -1) {// vansale
							CustomerDTO cus = cusTable.getCustomerById(String
									.valueOf(dto.customer.customerId));

							DEBIT_TABLE debitTable = new DEBIT_TABLE(mDB);
							dto.debitDto.id = debitId++;
							dto.debitDto.objectID = String
									.valueOf(dto.orderInfo.customerId);
							dto.debitDto.objectType = "3";
							dto.debitDto.totalAmount = dto.orderInfo.getTotal();
							dto.debitDto.totalPay = 0;
							dto.debitDto.totalDebit = dto.orderInfo.getTotal();
							dto.debitDto.maxDebitAmount = cus
									.getMaxDebitAmount();
							dto.debitDto.maxDebitDate = cus.getMaxDebitDate();
							dto.debitDto.totalDiscount = 0;

							dto.debitIdExist = debitTable
									.checkDebitExist(String
											.valueOf(dto.orderInfo.customerId));
							if (dto.debitIdExist > 0) {
								dto.debitDto.id = dto.debitIdExist;
								increaseDebit = debitTable
										.increaseDebit(dto.debitDto);
							} else {
								increaseDebit = debitTable.insert(dto.debitDto) > 0 ? true
										: false;
							}

							// Insert vao bang debit_detail
							DEBIT_DETAIL_TEMP_TABLE debitDetailTable = new DEBIT_DETAIL_TEMP_TABLE(
									mDB);
							dto.debitDetailDto.debitDetailID = debitDetailId++;
							dto.debitDetailDto.fromObjectID = dto.orderInfo.saleOrderId;
							dto.debitDetailDto.amount = dto.orderInfo.getAmount();
							dto.debitDetailDto.discount = dto.orderInfo.getDiscount();
							dto.debitDetailDto.total = dto.orderInfo.getTotal();
							dto.debitDetailDto.totalPay = 0;
							dto.debitDetailDto.remain = dto.debitDetailDto.total;
							dto.debitDetailDto.debitID = dto.debitDto.id;
							dto.debitDetailDto.createUser = dto.orderInfo.createUser;
							dto.debitDetailDto.createDate = dto.orderInfo.orderDate;
							dto.debitDetailDto.debitDate = dto.orderInfo.orderDate;
							dto.debitDetailDto.type = 4;
							dto.debitDetailDto.fromObjectNumber = dto.orderInfo.orderNumber;
							dto.debitDetailDto.customerId = dto.orderInfo.customerId;
							dto.debitDetailDto.shopId = dto.orderInfo.shopId;
							dto.debitDetailDto.staffID = dto.orderInfo.staffId;
							dto.debitDetailDto.orderType = dto.orderInfo.orderType;
							dto.debitDetailDto.saleDate = dto.orderInfo.orderDate;

							insertDebitDetail = debitDetailTable
									.insert(dto.debitDetailDto) > 0 ? true
									: false;
						}

						if (updateStaff && insertUpdateStaffCus
								&& updateRoutingCus && increaseDebit
								&& insertDebitDetail && insertArrayPromoDetail && insertArrayPromoMap && insertArrayPOCustomerPromoMap) {
							mDB.setTransactionSuccessful();
							insertSuccess = true;
						}
					}
				}
			}
		} catch (Exception e) {
			insertSuccess = false;
			ServerLogger.sendLog(e.getMessage(), "updateOrder", false,
					TabletActionLogDTO.LOG_EXCEPTION);
		} finally {
			mDB.endTransaction();
			result.isCreateSqlLiteSuccess = insertSuccess;
		}

		return result;
	}

	/**
	 * Lay ds hinh anh cua tbhv
	 *
	 * @author: Tuanlt11
	 * @param bundle
	 * @return
	 * @return: ImageSearchViewDTO
	 * @throws:
	 */
	public ImageSearchViewDTO getListAlbumTBHV(Bundle bundle) {
		ImageSearchViewDTO dto = new ImageSearchViewDTO();
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		DISPLAY_PROGRAME_TABLE programeTable = new DISPLAY_PROGRAME_TABLE(mDB);
		try {
			boolean isAll = bundle.getBoolean(IntentConstants.INTENT_IS_ALL,
					false);
			dto = custommerTable.getImageSearchListTBHV(bundle);
			if (isAll) {
				DisplayProgrameModel listDisplay = new DisplayProgrameModel();
				listDisplay = programeTable
						.getListDisplayProgrameImageTBHV(bundle);
				if (listDisplay != null && listDisplay.getModelData() != null) {
					dto.listPrograme.addAll(listDisplay.getModelData());
				}
				ArrayList<StaffChoosenDTO> listStaff = staffTable
						.getTBHVSearchImageListStaff(bundle);
				dto.listStaff = listStaff;
			}

		} catch (Exception e) {
		}
		return dto;
	}

	/**
	 * Lay ds san pham khuyen mai tu san pham ban
	 *
	 * @author: TruongHN
	 * @param orderView
	 * @return: ArrayList<OrderDetailViewDTO>
	 * @throws:
	 */

	public SortedMap<Long, List<OrderDetailViewDTO>> calculatePromotionProducts2(
			OrderViewDTO orderView) throws Exception {
		// Table for request
		PRODUCT_TABLE productDAO = new PRODUCT_TABLE(mDB);
		PROMOTION_PROGRAME_TABLE promotionProTable = new PROMOTION_PROGRAME_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);

		// reset list dskm sai
		orderView.lstWrongProgrameId.clear();
		GlobalInfo.getInstance().getLstWrongProgrameId().clear();

//		MyLog.e("calculatePromotionProducts2 - ", "1");
		//Lay thong gia moi, ton kho cua sp ban
		ArrayList<String> productIdList = new ArrayList<String>();
		ArrayList<OrderDetailViewDTO> productList = new ArrayList<OrderDetailViewDTO>();
		SortedMap<Long, OrderDetailViewDTO> sortListProductSale = new TreeMap<Long, OrderDetailViewDTO>();
		for (OrderDetailViewDTO product : orderView.listBuyOrders) {
			productIdList.add(String.valueOf(product.orderDetailDTO.productId));
			productList.add(product);
		}

		List<OrderDetailViewDTO> productInfoList = productDAO
				.getListProductStockByID(TextUtils.join(",", productIdList),
						orderView.orderInfo.orderType, orderView.customer.customerId);

		orderView.orderInfo.setAmount(0);
		for (OrderDetailViewDTO product : productList) {
			for(OrderDetailViewDTO productPromotionInfo : productInfoList) {
				if(product.orderDetailDTO.productId == productPromotionInfo.orderDetailDTO.productId) {
					updatePromotionProductInfo(product, productPromotionInfo);
					product.orderDetailDTO.setAmount(product.orderDetailDTO.calculateAmountProduct());
					orderView.orderInfo.addAmount(product.orderDetailDTO.getAmount());
				}
			}
		}

//		MyLog.e("calculatePromotionProducts2 - ", "2");

		// ds khuyen mai tra ve
		SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut = new TreeMap<Long, List<OrderDetailViewDTO>>();
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN_MONEY), new ArrayList<OrderDetailViewDTO>());
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY), new ArrayList<OrderDetailViewDTO>());

		// Sap xep cac san pham ban theo product id
		for (int i = 0, size = orderView.listBuyOrders.size(); i < size; i++) {
			OrderDetailViewDTO detail = orderView.listBuyOrders.get(i);
			//Reset sp chua co KM
			detail.orderDetailDTO.hasPromotion = false;

			OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
			SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
			orderDetail.orderDetailDTO = orderDTO;

			orderDTO.quantity = detail.orderDetailDTO.quantity;
			orderDTO.quantityPackage = detail.orderDetailDTO.quantityPackage;
			orderDTO.quantityRetail = detail.orderDetailDTO.quantityRetail;
			orderDTO.productId = detail.orderDetailDTO.productId;
			orderDTO.price = detail.orderDetailDTO.price;
			orderDTO.packagePrice = detail.orderDetailDTO.packagePrice;
			orderDTO.setAmount(detail.orderDetailDTO.getAmount());
			orderDTO.programeCode = detail.orderDetailDTO.programeCode;
			// gan lai convfact
			orderDTO.convfact = detail.orderDetailDTO.convfact;
			orderDetail.convfact = detail.convfact;

			// ko luu percent, chi luu so tien tong duoc chiet khau vai co the 1 sp dat nhieu CTKM
			detail.orderDetailDTO.discountPercentage = 0;
			detail.orderDetailDTO.setDiscountAmount(0);
			//xoa het phan tu trong mang promo detail sau khi tinh khuyen mai lai
			if (detail.listPromoDetail == null) {
				detail.listPromoDetail = new ArrayList<SaleOrderPromoDetailDTO>();
			} else{
				detail.listPromoDetail.clear();
			}
			//xoa het phan tu trong mang promo map sau khi tinh khuyen mai lai
			if (detail.lstPromotionMap == null) {
				detail.lstPromotionMap = new ArrayList<SalePromoMapDTO>();
			} else{
				detail.lstPromotionMap.clear();
			}
			sortListProductSale.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
		}

		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		Long keyList = Long.valueOf(100);

		// lay thong tin KM, loai KM
		String orderType = orderView.orderInfo.orderType;
		String customerId = orderView.customer.getCustomerId();
		String customerTypeId = String.valueOf(orderView.customer.getCustomerTypeId());

		ArrayList<String> shopIdArray = shopTable.getShopRecursive(shopId);
//		ArrayList<String> shopIdArray = shopTable.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);

//		MyLog.e("calculatePromotionProducts2 - ", "3");
		ArrayList<LongSparseArray<ArrayList<PromotionProgrameDTO>>> lstPromoObject =  promotionProTable
				.getArrPromotionObjectWithCheckInvalid(TextUtils.join(",", productIdList),
						idShopList, customerId, customerTypeId, staffId, orderType);
		LongSparseArray<ArrayList<PromotionProgrameDTO>> promotionArray = lstPromoObject.get(0);
//		MyLog.e("calculatePromotionProducts2 - ", "4");
		// mang CTKM clone de ko bi remove sau khi xu li SP thuoc mot CTKM
		LongSparseArray<ArrayList<PromotionProgrameDTO>> promotionArrayClone = lstPromoObject.get(1);

		// mang chua CTKM tuong ung voi sp de ko tinh lai KM cho sp da thuoc 1 CTKM da tinh roi
		HashMap<Integer, ArrayList<Long>> hmProgrameProduct = new HashMap<Integer,  ArrayList<Long>>();
		boolean isExistPromotion = false; // co ton tai CTKM theo tung sp hay ko
		// Tinh KM cho tung sp
		while (sortListProductSale.size() > 0) {
			// Reset price & quantiy
			for (OrderDetailViewDTO sortDetail : sortListProductSale.values()) {
				for (int i = 0, size = orderView.listBuyOrders.size(); i < size; i++) {
					OrderDetailViewDTO detail = orderView.listBuyOrders.get(i);
					if (sortDetail.orderDetailDTO.productId == detail.orderDetailDTO.productId) {
						sortDetail.orderDetailDTO.quantity = detail.orderDetailDTO.quantity;
						sortDetail.orderDetailDTO.quantityPackage = detail.orderDetailDTO.quantityPackage;
						sortDetail.orderDetailDTO.quantityRetail = detail.orderDetailDTO.quantityRetail;
						sortDetail.orderDetailDTO.price = detail.orderDetailDTO.price;
						sortDetail.orderDetailDTO.packagePrice = detail.orderDetailDTO.packagePrice;
						sortDetail.orderDetailDTO.setAmount(detail.orderDetailDTO.getAmount());
						break;
					}
				}
			}

			//Lay ra 1 sp
			Long key = sortListProductSale.firstKey();
//			String productId = key.toString();
			ArrayList<PromotionProgrameDTO> lstPromotionProgram = promotionArray.get(key);
//			PromotionProgrameDTO promotionProgram = promotionProTable
//					.getPromotionObjectWithCheckInvalid(productId,
//							idShopList, customerId, customerTypeId, staffId);
			if (lstPromotionProgram != null && lstPromotionProgram.size() > 0) {
				isExistPromotion = true; // ton tai CTKM
				for (int j = 0, s = lstPromotionProgram.size(); j < s; j++) {
					PromotionProgrameDTO promotionProgram = lstPromotionProgram
							.get(j);
					if (hmProgrameProduct
							.containsKey(promotionProgram.getPROMOTION_PROGRAM_ID())) {
						// list sp da duoc tinh KM
						ArrayList<Long> lstProductIdKM = hmProgrameProduct
								.get(promotionProgram.getPROMOTION_PROGRAM_ID());
						for (int i = 0, size = lstProductIdKM.size(); i < size; i++) {
							if (lstProductIdKM.get(i) == key.longValue()) {
								lstPromotionProgram.remove(j); // xoa mang CTKM
																// cua sp
								s--;
								j--;
								break;
							}
						}

					}
				}
			}else{
				//Phuc vu muc dich sau do KM bi off ma luc truoc lai co
				for (OrderDetailViewDTO product : orderView.listBuyOrders) {
					if (product.orderDetailDTO.productId == key.longValue() && product.orderDetailDTO.programeTypeCode != null) {
						// tam thoi ko xet truong hop zv23
						if(!isExistPromotion){// neu ko ton tai CTKM nao thi reset tat ca gia tri KM dua tu man hinh dat hang sang
							product.orderDetailDTO.programeType = -1;
							product.orderDetailDTO.programeCode = null;
							product.orderDetailDTO.programeTypeCode = null;
							break;
						}
					}
				}

				sortListProductSale.remove(key);//Khong xoa key o fullDate nua ma xoa trong ham tinh KM
				isExistPromotion = false;
				if(lstPromotionProgram == null)
					lstPromotionProgram = new ArrayList<PromotionProgrameDTO>();// tao ra mang rong de ko bi crash
			}
			for(PromotionProgrameDTO promotionProgram : lstPromotionProgram ){
				// MyLog.e("calculatePromotionProducts2 - begin ", "4" + index);
				if (promotionProgram != null && promotionProgram.isInvalid()) {
					//validate chuong trinh khuyen mai dung hay khong?
					if (!promotionProTable.validatePromotionPrograme(
							promotionProgram.getPROMOTION_PROGRAM_ID(),
							promotionProgram.getPROMOTION_PROGRAM_CODE(),
							promotionProgram.getMD5_VALID_CODE())) {
						orderView.lstWrongProgrameId.add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
						GlobalInfo.getInstance().getLstWrongProgrameId().add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
					}

					boolean isPromotionOpen = false;
					//Kiem tra xem KM mo moi co dat dk mo moi de tinh KM hay ko?
					if(promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
						//isPromotionOpen = promotionProTable.isPromotionNewOpen(promotionProgram.PROMOTION_PROGRAM_ID, customerId, idShopList, orderView);
					}
					//KM thuong hoac ZV24 (mo moi) & co mo moi
					if(!promotionProgram.getTYPE().equals(CalPromotions.ZV24) || (promotionProgram.getTYPE().equals(CalPromotions.ZV24) && isPromotionOpen)) {
						SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPutTemp = CalPromotions.calcPromotion(orderView, promotionProgram,
								sortListProductSale, keyList, mDB, GroupLevelDTO.PROMOTION_PRODUCT, promotionArray,hmProgrameProduct);

						ArrayList<OrderDetailViewDTO> listPromotionProduct = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
						//ArrayList<OrderDetailViewDTO> listPromotionProductOpen = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));
						ArrayList<OrderDetailViewDTO> listPromotion = (ArrayList<OrderDetailViewDTO>) sortListOutPutTemp.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
						if(listPromotion.size() > 0) {
							if(!promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
								listPromotionProduct.addAll(listPromotion);
							}
							//Ly do comment: isPromotionOpen luon = false nen ko vao case nay
							/* else if(promotionProgram.TYPE.equals(CalPromotions.ZV24) && isPromotionOpen) {
								listPromotionProductOpen.addAll(listPromotion);
							}*/
							//Ds sp doi
							if (sortListOutPutTemp.size() > OrderViewDTO.INDEX_PROMOTION_PRODUCT) {
								Iterator<Long> it = sortListOutPutTemp.keySet().iterator();
								it.next();// KM sp

								while (it.hasNext()) {
									keyList++;
									Long md = it.next();
									List<OrderDetailViewDTO> listProductChange = sortListOutPutTemp.get(md);

									//Neu la KM ZV24
									if(promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
										for(OrderDetailViewDTO product : listProductChange) {
											product.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;
										}
									}

									sortListOutPut.put(keyList, listProductChange);
								}
							}
						}

						//Check co du lieu thi moi set lai key list
						if (sortListOutPut.size() > 0) {
							if(sortListOutPut.lastKey() > keyList) {
								keyList = sortListOutPut.lastKey();
							}
						}
					} else {
						sortListProductSale.remove(key);//Khong xoa key o fullDate nua ma xoa trong ham tinh KM
					}
				} else if(promotionProgram == null || promotionProgram.getPROMOTION_PROGRAM_ID() <= 0) {
					//Phuc vu muc dich sau do KM bi off ma luc truoc lai co
					for (OrderDetailViewDTO product : orderView.listBuyOrders) {
						if (product.orderDetailDTO.productId == key.longValue() && product.orderDetailDTO.programeTypeCode != null
								&& !product.orderDetailDTO.programeTypeCode.equals(CalPromotions.ZV23)) {
							product.orderDetailDTO.programeType = -1;
							product.orderDetailDTO.programeCode = null;
							product.orderDetailDTO.programeTypeCode = null;
							break;
						}
					}

					sortListProductSale.remove(key);//Khong xoa key o fullDate nua ma xoa trong ham tinh KM
				}
			}

//			MyLog.e("calculatePromotionProducts2 - end ", "4" + index);
//			sortListProductSale.remove(key);//Khong xoa key o fullDate nua ma xoa trong ham tinh KM
		}

//		MyLog.e("calculatePromotionProducts2 - ", "5");
		// Tinh KM cho don hang
		calPromotionForOrder2(orderView, sortListOutPut, sortListProductSale,
				idShopList, keyList, customerId, customerTypeId, staffId, mDB, idShopList, orderType);
		if (sortListOutPut.size() > 0) {
			if(sortListOutPut.lastKey() > keyList) {
				keyList = sortListOutPut.lastKey();
			}
		}
//		MyLog.e("calculatePromotionProducts2 - ", "6");

		//Tinh KM tich luy
//		calAccumulationPromotion(orderView, sortListOutPut, sortListProductSale,
//				idShopList, keyList, customerId, customerTypeId, staffId, mDB);
		if (sortListOutPut.size() > 0) {
			if(sortListOutPut.lastKey() > keyList) {
				keyList = sortListOutPut.lastKey();
			}
		}
//		MyLog.e("calculatePromotionProducts2 - ", "7");

		//Gom KM mo moi
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN), groupNewOpenPromotion(sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN))));

		// Tra thuong keyshop
		if(orderView.isKeyShop)
			calPromotionKeyShopForOrder(orderView, sortListOutPut, shopId, customerId, mDB);
//		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP), groupKeyShopPromotion(sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP))));

		//------------------------------KM SAN PHAM------------------
		//Ds thong tin san pham
		// Lay thong tin cho ds sp KM hien thi
		ArrayList<OrderDetailViewDTO> listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
		ArrayList<OrderDetailViewDTO> listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();

		//Clear ds
		productIdList.clear();
		productList.clear();
		if (listProductPromotionsale != null && listProductPromotionsale.size() > 0) {
			for (OrderDetailViewDTO promotionProduct : listProductPromotionsale) {
				//Neu co san pham KM thi moi gan lai
				if (promotionProduct.productPromoId > 0) {
					promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
					// Chi lay sp KM co so luong KM > 0
					if (promotionProduct.orderDetailDTO.quantity > 0) {
						productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
						productList.add(promotionProduct);
					} else {
						continue;
					}
				} else {
					promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
					promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
				}

				// Sap xep theo programe code
				int i = 0;
				int size = listProductSalePromotion.size();
				for (i = 0; i < size; i++) {
					OrderDetailViewDTO promotionTemp = listProductSalePromotion.get(i);
					// Lon hon
					if (promotionProduct.orderDetailDTO.programeCode
							.compareToIgnoreCase(promotionTemp.orderDetailDTO.programeCode) < 0) {
						break;
					}
				}

				listProductSalePromotion.add(i, promotionProduct);
			}
		}

		// Ds san pham KM sp
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT), listProductSalePromotion);

		//------------------------------KM DON HANG------------------
		//DS Km don hang
		listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER));
		listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();
		if (listProductPromotionsale != null && listProductPromotionsale.size() > 0) {
			for (OrderDetailViewDTO promotion : listProductPromotionsale) {
				//Neu co san pham KM thi moi gan lai
				if (promotion.listPromotionForPromo21 != null
						&& promotion.listPromotionForPromo21.size() > 0) {
					for (OrderDetailViewDTO promotionProduct : promotion.listPromotionForPromo21) {
						//Neu co san pham KM thi moi gan lai
						if (promotionProduct.productPromoId > 0) {
							promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
							// Chi lay sp KM co so luong KM > 0
							if (promotionProduct.orderDetailDTO.quantity > 0) {
								productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
								productList.add(promotionProduct);
							} else {
								continue;
							}
						} else {
							promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
							promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
						}
					}
				} else {
					promotion.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
					promotion.quantityEdit = (int)promotion.orderDetailDTO.quantity;
				}

				// Sap xep theo programe code
				int i = 0;
				int size = listProductSalePromotion.size();
				for (i = 0; i < size; i++) {
					OrderDetailViewDTO promotionTemp = listProductSalePromotion.get(i);
					// Lon hon
					if (promotion.orderDetailDTO.programeCode
							.compareToIgnoreCase(promotionTemp.orderDetailDTO.programeCode) < 0) {
						break;
					}
				}

				listProductSalePromotion.add(i, promotion);
			}
		}

		// Ds san pham KM sp
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER), listProductSalePromotion);

		//------------------------------KM TICH LUY------------------
		listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION));
		listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();
		//Ds sp KM tich luy
		if (listProductPromotionsale != null && listProductPromotionsale.size() > 0) {
			for (OrderDetailViewDTO promotion : listProductPromotionsale) {
				//Neu co san pham KM thi moi gan lai
				if (promotion.listPromotionForPromo21 != null
						&& promotion.listPromotionForPromo21.size() > 0) {
					for (OrderDetailViewDTO promotionProduct : promotion.listPromotionForPromo21) {
						//Neu co san pham KM thi moi gan lai
						if (promotionProduct.productPromoId > 0) {
							promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
							// Chi lay sp KM co so luong KM > 0
							if (promotionProduct.orderDetailDTO.quantity > 0) {
								productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
								productList.add(promotionProduct);
							} else {
								continue;
							}
						} else {
							promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
							promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
						}
					}
				} else {
					promotion.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
					promotion.quantityEdit = (int)promotion.orderDetailDTO.quantity;
				}

				// Sap xep theo programe code
				int i = 0;
				int size = listProductSalePromotion.size();
				for (i = 0; i < size; i++) {
					OrderDetailViewDTO promotionTemp = listProductSalePromotion.get(i);
					// Lon hon
					if (promotion.orderDetailDTO.programeCode
							.compareToIgnoreCase(promotionTemp.orderDetailDTO.programeCode) < 0) {
						break;
					}
				}

				listProductSalePromotion.add(i, promotion);
			}
		}

		// Ds san pham KM tich luy
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION), listProductSalePromotion);

		//------------------------------KM MO MOI------------------
		listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));
		listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();
		//Ds sp KM tich luy
		if (listProductPromotionsale != null && listProductPromotionsale.size() > 0) {
			for (OrderDetailViewDTO promotion : listProductPromotionsale) {
				//Neu co san pham KM thi moi gan lai
				if (promotion.listPromotionForPromo21 != null
						&& promotion.listPromotionForPromo21.size() > 0) {
					for (OrderDetailViewDTO promotionProduct : promotion.listPromotionForPromo21) {
						//Neu co san pham KM thi moi gan lai
						if (promotionProduct.productPromoId > 0) {
							promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
							// Chi lay sp KM co so luong KM > 0
							if (promotionProduct.orderDetailDTO.quantity > 0) {
								productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
								productList.add(promotionProduct);
							} else {
								continue;
							}
						} else {
							promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
							promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
						}
					}
				} else {
					promotion.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
					promotion.quantityEdit = (int)promotion.orderDetailDTO.quantity;
				}

				// Sap xep theo programe code
				int i = 0;
				int size = listProductSalePromotion.size();
				for (i = 0; i < size; i++) {
					OrderDetailViewDTO promotionTemp = listProductSalePromotion.get(i);
					// Lon hon
					if (promotion.orderDetailDTO.programeCode
							.compareToIgnoreCase(promotionTemp.orderDetailDTO.programeCode) < 0) {
						break;
					}
				}

				listProductSalePromotion.add(i, promotion);
			}
		}

		// Ds san pham KM tich luy
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN), listProductSalePromotion);

		//------------------------------TRA THUONG KEYSHOP ------------------
		listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP));
		listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();
		if (listProductPromotionsale != null
				&& listProductPromotionsale.size() > 0) {
			for (OrderDetailViewDTO promotion : listProductPromotionsale) {
				if (promotion.orderDetailDTO.quantity > 0) {
					productIdList.add(String
							.valueOf(promotion.orderDetailDTO.productId));
					productList.add(promotion);
				} else {
					continue;
				}
			}
		}

		// Ds san pham tra thuong key shop
//		MyLog.e("calculatePromotionProducts2 - ", "9");

		// Ds san pham ko co trong ton kho or ko co trong kho cua sp hien thi
		// 10: ds hien thi loi,
//		sortListOutPut.put(Long.valueOf(10), listProductSalePromotionMissing);

		// Ds sp ko co trong ton kho or ko co trong kho cua sp doi
//		listProductSalePromotionMissing = new ArrayList<OrderDetailViewDTO>();

		if (sortListOutPut.size() > OrderViewDTO.NUM_PROMOTION_TYPE) {
			Iterator<Long> it = sortListOutPut.keySet().iterator();
			for(int i = 0; i < OrderViewDTO.NUM_PROMOTION_TYPE; i++) {
				it.next();
			}

			while (it.hasNext()) {
				Long md = it.next();
				List<OrderDetailViewDTO> listProductChange = sortListOutPut
						.get(md);
				listProductSalePromotion = new ArrayList<OrderDetailViewDTO>();

				for (OrderDetailViewDTO promotionProduct : listProductChange) {
					// Neu co san pham KM thi moi gan lai
					if (promotionProduct.productPromoId > 0) {
						promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
						// Chi lay sp KM co so luong KM > 0
						if (promotionProduct.orderDetailDTO.quantity > 0) {
							productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
							productList.add(promotionProduct);
						} else {
							continue;
						}
					} else {
						promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
						promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
					}

					listProductSalePromotion.add(promotionProduct);
				}

				sortListOutPut.put(md, listProductSalePromotion);
			}

			// Ds san pham ko co trong ton kho or ko co trong kho cua sp doi hang
			// 11: ds doi hang loi
//			sortListOutPut.put(Long.valueOf(11), listProductSalePromotionMissing);
		}

		// Get information for product of zv21
		if (orderView.listPromotionForOrderChange != null
				&& orderView.listPromotionForOrderChange.size() > 0) {
			for (OrderDetailViewDTO promotion : orderView.listPromotionForOrderChange) {
				if (promotion.listPromotionForPromo21 != null
						&& promotion.listPromotionForPromo21.size() > 0) {
					for (OrderDetailViewDTO promotionProduct : promotion.listPromotionForPromo21) {
						// Neu co san pham KM thi moi gan lai
						if (promotionProduct.productPromoId > 0) {
							promotionProduct.orderDetailDTO.productId = (int) promotionProduct.productPromoId;
							// Chi lay sp KM co so luong KM > 0
							if (promotionProduct.orderDetailDTO.quantity > 0) {
								productIdList.add(String.valueOf(promotionProduct.orderDetailDTO.productId));
								productList.add(promotionProduct);
							} else {
								continue;
							}
						} else {
							promotionProduct.productName = StringUtil.getString(R.string.TEXT_KM_CK_GET);
							promotionProduct.quantityEdit = (int)promotionProduct.orderDetailDTO.quantity;
						}
					}
				}
			}
		}


		//cap nhat so luong sp KM quantityPackage, quantityRetail
		for (OrderDetailViewDTO productPromotion : productList) {
			if (productPromotion != null && productPromotion.orderDetailDTO != null) {
				productPromotion.orderDetailDTO.quantityRetail = (long) productPromotion.orderDetailDTO.quantity;
				productPromotion.orderDetailDTO.quantityPackage = 0;
			}
		}

		// Lay ton kho cho sp ban tuong ung voi loai don hang
		for (OrderDetailViewDTO product : orderView.listBuyOrders) {
			productIdList.add(String.valueOf(product.orderDetailDTO.productId));
			productList.add(product);
		}

		// Lay ton kho cho sp KM KHONG PHAI TU DONG tuong ung voi loai don hang
		for (OrderDetailViewDTO product : orderView.listPromotionOrders) {
			if (product.orderDetailDTO.programeType != PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
				productIdList.add(String.valueOf(product.orderDetailDTO.productId));
				productList.add(product);
			}
		}

//		MyLog.e("calculatePromotionProducts2 - ", "10");
		//Lay thong tin ton kho cua sp ban & sp KM tay
		productInfoList = productDAO
				.getListProductStockByID(TextUtils.join(",", productIdList),
						orderView.orderInfo.orderType, orderView.customer.customerId);

		for (OrderDetailViewDTO product : productList) {
			for(OrderDetailViewDTO productPromotionInfo : productInfoList) {
				if(product.orderDetailDTO.productId == productPromotionInfo.orderDetailDTO.productId) {
					updatePromotionProductInfo(product, productPromotionInfo);
					break;
				}
			}
		}

//		MyLog.e("calculatePromotionProducts2 - ", "11");
		orderView.promotionArrayClone = promotionArrayClone;

		//Lay ds KM tien % cua KM sp
		ArrayList<OrderDetailViewDTO> listPromotionMoney = new ArrayList<OrderDetailViewDTO>();
		listProductSalePromotion = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
		for (OrderDetailViewDTO promotion : listProductSalePromotion) {
			//Xu ly cho cac KM % cua sp
			if (promotion.type == OrderDetailViewDTO.FREE_PERCENT ||
					//Moi xu ly cho KM mua Amount tang Amount: ZV05, ZV11, ZV17
					promotion.type == OrderDetailViewDTO.FREE_PRICE) {
				listPromotionMoney.add(promotion);
			}
		}

		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY), listPromotionMoney);
		listProductSalePromotion.removeAll(listPromotionMoney);

		//Lay ds KM tien % cua KM mo moi
		listPromotionMoney = new ArrayList<OrderDetailViewDTO>();
		listProductSalePromotion = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));
		for (OrderDetailViewDTO promotion : listProductSalePromotion) {
			//Xu ly cho cac KM % cua sp
			if (promotion.type == OrderDetailViewDTO.FREE_PERCENT ||
					//Moi xu ly cho KM mua Amount tang Amount: ZV05, ZV11, ZV17
					promotion.type == OrderDetailViewDTO.FREE_PRICE) {
				listPromotionMoney.add(promotion);
			}
		}

		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN_MONEY), listPromotionMoney);
		listProductSalePromotion.removeAll(listPromotionMoney);

		orderView.shareAmountPromotionForBuyProduct(sortListOutPut);
//		orderView.checkQuantityReceivedFull();
//------------------------
//		//Tinh % discount, discount amount cho sp ban tu KM sp
//		ArrayList<ArrayList<OrderDetailViewDTO>> listListPromotion = new ArrayList<ArrayList<OrderDetailViewDTO>>();
//		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT)));
//		//KM mo moi
//		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN)));
//		// km keyshop
////		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP)));
////
////		//Reset gia tri
//		for (OrderDetailViewDTO product : orderView.listBuyOrders) {
//			product.orderDetailDTO.discountPercentage = 0;
//			product.orderDetailDTO.setDiscountAmount(0);
//			//xoa het phan tu trong mang promo detail sau khi tinh khuyen mai lai
//			if (product.listPromoDetail == null) {
//				product.listPromoDetail = new ArrayList<SaleOrderPromoDetailDTO>();
//			} else{
//				product.listPromoDetail.clear();
//			}
//			//xoa het phan tu trong mang promo map sau khi tinh khuyen mai lai
//			if (product.lstPromotionMap == null) {
//				product.lstPromotionMap = new ArrayList<SalePromoMapDTO>();
//			} else{
//				product.lstPromotionMap.clear();
//			}
//		}
//
//		for (ArrayList<OrderDetailViewDTO> listPromotionProduct: listListPromotion) {
//			ArrayList<OrderDetailViewDTO> listPercentPromotion = new ArrayList<OrderDetailViewDTO>();
//			for (OrderDetailViewDTO promotion : listPromotionProduct) {
//				//Xu ly cho cac KM % cua sp
//				if (promotion.type == OrderDetailViewDTO.FREE_PERCENT ||
//						//Moi xu ly cho KM mua Amount tang Amount: ZV05, ZV11, ZV17
//						promotion.type == OrderDetailViewDTO.FREE_PRICE) {
//					//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
//					double totalDiscount = 0;
//					double fixDiscountAmount = promotion.orderDetailDTO.getDiscountAmount();
//					//KM sp
//					int numProduct = 0;
////					if(promotion.listBuyProduct.size() > 0) {
//					if (promotion.listBuyProduct != null
//							&& promotion.listBuyProduct.size() > 0) {
//						for (int i = orderView.listBuyOrders.size() - 1; i >= 0; i--) {
//							OrderDetailViewDTO product = orderView.listBuyOrders
//									.get(i);
//							for (GroupLevelDetailDTO levelDetail : promotion.listBuyProduct) {
//								if (product.orderDetailDTO.productId == levelDetail.productId) {
//									numProduct++;
//									// Sp tham CTKM dat duoc KM
//									product.orderDetailDTO.hasPromotion = true;
//									// Lay lai gia tri sau khi tinh KM (phong
//									// hop luc dau ko dat KM nhung sua don hang
//									// duoc KM)
//									product.orderDetailDTO.programeType = promotion.orderDetailDTO.programeType;
//									product.orderDetailDTO.programeCode = promotion.orderDetailDTO.programeCode;
//									product.orderDetailDTO.programeTypeCode = promotion.orderDetailDTO.programeTypeCode;
//									// product.orderDetailDTO.discountPercentage
//									// +=
//									// promotion.orderDetailDTO.discountPercentage;
//									double amount = 0;
//									if (promotion.isBundle) {
//										amount = levelDetail.usedAmount;
//									} else {
//										amount = product.orderDetailDTO.getAmount();
//									}
//									BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
////									double discountAmount = discountPercent.multiply(new BigDecimal(amount)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).doubleValue();
//									double discountAmount = discountPercent.multiply(new BigDecimal(amount)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
//
//									// Tinh toan de lam tron, tong so tien <=
//									// tong tien KM
//									if (discountAmount > fixDiscountAmount) {
//										discountAmount = fixDiscountAmount;
//									}
//
//									if (totalDiscount + discountAmount > fixDiscountAmount) {
//										discountAmount = fixDiscountAmount
//												- totalDiscount;
//									} else {
//										if (numProduct == promotion.listBuyProduct
//												.size()) {
//											discountAmount = fixDiscountAmount
//													- totalDiscount;
//										}
//									}
//									totalDiscount += discountAmount;
//
//									product.orderDetailDTO.addDiscountAmount(discountAmount);
//									// orderView.orderInfo.discount +=
//									// discountAmount;
//									listPercentPromotion.add(promotion);
//
//									// promo detail sau khi tinh khuyen mai lai
//									SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
//									promoDetail.updateData(promotion,
//											discountAmount);
//									// neu so luong san pham quy dinh == 1 -> la
//									// LINE, isOwner = 1, con lai la isOwner = 0
//									promoDetail.isOwner = (promotion.numBuyProductInGroup == 1) ? 1
//											: 0;
//									// KM tu dong
//									product.listPromoDetail.add(promoDetail);
//
//									// luu thong tin CTKM dat
//									boolean isExist = false;
//									for (SalePromoMapDTO temp : product.lstPromotionMap) {
//										if (temp.programCode
//												.equals(promotion.orderDetailDTO.programeCode)) {
//											isExist = true;
//											break;
//										}
//									}
//									if (!isExist) {
//										// luu thong tin CTKM dat
//										SalePromoMapDTO proMap = new SalePromoMapDTO();
//										proMap.updateData(promotion);
//										proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
//										product.lstPromotionMap.add(proMap);
//									}
//									break;
//								}
//							}
//						}
//					} else if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY || promotion.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
//						// neu la sp KM mo moi thuoc dang don hang
//						// thi phan bo deu ra cho cac sp nhu KM don hang
//						for (int i = orderView.listBuyOrders.size() - 1; i >= 0; i--) {
//							OrderDetailViewDTO product = orderView.listBuyOrders
//									.get(i);
//							numProduct++;
//							// product.orderDetailDTO.discountPercentage +=
//							// promotion.orderDetailDTO.discountPercentage;
////							BigDecimal discountPercent = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
////							double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).doubleValue();
//							BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
//							double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
//							// Tinh toan de lam tron, tong so tien <= tong tien
//							// KM
//							if (discountAmount > fixDiscountAmount) {
//								discountAmount = fixDiscountAmount;
//							}
//
//							if (totalDiscount + discountAmount > fixDiscountAmount) {
//								discountAmount = fixDiscountAmount
//										- totalDiscount;
//							} else {
//								if (numProduct == orderView.listBuyOrders
//										.size()) {
//									discountAmount = fixDiscountAmount
//											- totalDiscount;
//								}
//							}
//							totalDiscount += discountAmount;
//							product.orderDetailDTO.addDiscountAmount(discountAmount);
//							// orderView.orderInfo.discount += discountAmount;
//
//							// promo detail sau khi tinh khuyen mai lai
//							SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
//							promoDetail.updateData(promotion, discountAmount);
//							// km don hang isOwner = 2
//							promoDetail.isOwner = 2;
//							product.listPromoDetail.add(promoDetail);
//						}
//					}
//					//KM sp
//				} else {
//					//Cap nhat nhung sp ban dat duoc KM de luu programCode
//					for(GroupLevelDetailDTO levelDetail: promotion.listBuyProduct) {
//						for (OrderDetailViewDTO product : orderView.listBuyOrders) {
//							if(product.orderDetailDTO.productId == levelDetail.productId) {
//								//Sp tham CTKM dat duoc KM
//								product.orderDetailDTO.hasPromotion = true;
//								//Lay lai gia tri sau khi tinh KM (phong hop luc dau ko dat KM nhung sua don hang duoc KM)
//								product.orderDetailDTO.programeType = promotion.orderDetailDTO.programeType;
//								product.orderDetailDTO.programeCode = promotion.orderDetailDTO.programeCode;
//								product.orderDetailDTO.programeTypeCode = promotion.orderDetailDTO.programeTypeCode;
//								boolean isExist = false;
//								for (SalePromoMapDTO promoDetail : product.lstPromotionMap) {
//									if (promoDetail.programCode
//											.equals(promotion.orderDetailDTO.programeCode)) {
//										isExist = true;
//										break;
//									}
//								}
//								if (!isExist) {
//									// luu thong tin CTKM dat
//									SalePromoMapDTO proMap = new SalePromoMapDTO();
//									proMap.updateData(promotion);
//									proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
//									product.lstPromotionMap.add(proMap);
//								}
//							}
//						}
//					}
//				}
////			}
//			}
//			// luu thong tin nhung CTKM ko dat cho sp
//			for (OrderDetailViewDTO product : orderView.listBuyOrders) {
////				if(product.listPromoDetail.size() == 0){
//					ArrayList<PromotionProgrameDTO> lstPromotionProgram = promotionArrayClone.get(product.orderDetailDTO.productId);
//					if(lstPromotionProgram != null && lstPromotionProgram.size() > 0 ){
//						for(PromotionProgrameDTO promotionProgram : lstPromotionProgram ){
//							boolean isAchive = false;
//							for(SalePromoMapDTO promoDetail: product.lstPromotionMap){
//							// promo detail sau khi tinh khuyen mai lai
////							SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
////							promoDetail.updateData(promotionProgram);
////							// km don hang isOwner = 2
////							promoDetail.isOwner = CalPromotions.getTypePromtion(promotionProgram.TYPE);
////							product.listPromoDetail.add(promoDetail);
//							if(promoDetail.programCode.equals(promotionProgram.PROMOTION_PROGRAM_CODE)){
//									isAchive = true;
//									break;
//								}
//							}
//							if(!isAchive){
//								// luu thong tin CTKM ko dat
//								SalePromoMapDTO proMap = new SalePromoMapDTO();
//								proMap.saleOrderDetailId = product.orderDetailDTO.salesOrderDetailId;
//								proMap.programCode = promotionProgram.PROMOTION_PROGRAM_CODE;
//								proMap.status = SalePromoMapDTO.TYPE_NOT_ACHIVE;
//								product.lstPromotionMap.add(proMap);
//							}
//						}
//
//					}
////				}
//			}
//
////			MyLog.e("calculatePromotionProducts2 - ", "12");
//			//Xoa sp KM phan tram
//			listPromotionProduct.removeAll(listPercentPromotion);
//		}
//
//		//Cap nhat % discount, discount cua KM don hang
//		ArrayList<OrderDetailViewDTO> listPromotionOrder = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER));
//		for (OrderDetailViewDTO promotion : listPromotionOrder) {
//			if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER) {
//				//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
//				double totalDiscount = 0;
//				double fixDiscountAmount = promotion.orderDetailDTO.getDiscountAmount();
//				//KM sp
//				int numProduct = 0;
//				for (int i = orderView.listBuyOrders.size() - 1; i >= 0; i--) {
//					OrderDetailViewDTO product = orderView.listBuyOrders.get(i);
//					numProduct++;
////					product.orderDetailDTO.discountPercentage += promotion.orderDetailDTO.discountPercentage;
////					BigDecimal discountPercent = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
////					double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).doubleValue();
//					BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
//					double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
//
////					long discountAmount = Math.round((product.orderDetailDTO.amount
////							* (int) (promotion.orderDetailDTO.discountPercentage * CalPromotions.ROUND_PERCENT)) / (double)CalPromotions.ROUND_NO_PERCENT);
//					//Tinh toan de lam tron, tong so tien <= tong tien KM
//					if(discountAmount > fixDiscountAmount) {
//						discountAmount = fixDiscountAmount;
//					}
//
//					if(totalDiscount + discountAmount > fixDiscountAmount) {
//						discountAmount = fixDiscountAmount - totalDiscount;
//					} else {
//						if(numProduct == orderView.listBuyOrders.size()) {
//							discountAmount = fixDiscountAmount - totalDiscount;
//						}
//					}
//					totalDiscount += discountAmount;
//					product.orderDetailDTO.addDiscountAmount(discountAmount);
////					orderView.orderInfo.discount += discountAmount;
//
//					//promo detail sau khi tinh khuyen mai lai
//					SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
//					promoDetail.updateData(promotion, discountAmount);
//					//km don hang isOwner = 2
//					promoDetail.isOwner = 2;
//					product.listPromoDetail.add(promoDetail);
//
//					// luu thong tin CTKM dat
//					// SalePromoMapDTO proMap = new SalePromoMapDTO();
//					// proMap.updateData(promotion);
//					// proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
//					// product.lstPromotionMap.add(proMap);
//				}
//			}
//		}
//

//------------------------

		// luu thong tin nhung CTKM ko dat cho don hang
//		for (OrderDetailViewDTO product : orderView.listBuyOrders) {
//			// if(product.listPromoDetail.size() == 0){
//			for (OrderDetailViewDTO promotion : listPromotionOrder) {
//				boolean isAchive = false;
//				for (SalePromoMapDTO promoDetail : product.lstPromotionMap) {
//					if (promoDetail.programCode
//							.equals(promotion.orderDetailDTO.programeCode)) {
//						isAchive = true;
//						break;
//					}
//				}
//				if (!isAchive) {
//					// luu thong tin CTKM ko dat
//					SalePromoMapDTO proMap = new SalePromoMapDTO();
//					proMap.saleOrderDetailId = product.orderDetailDTO.salesOrderDetailId;
//					proMap.programCode = promotion.orderDetailDTO.programeCode;
//					proMap.status = SalePromoMapDTO.TYPE_NOT_ACHIVE;
//					product.lstPromotionMap.add(proMap);
//				}
//			}
//
//		}

		//Cap nhat % discount, discount cua KM tich luy
//		ArrayList<OrderDetailViewDTO> listPromotionAccumulation = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION));
//		for (OrderDetailViewDTO promotion : listPromotionAccumulation) {
//			if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY) {
//				//promo detail sau khi tinh khuyen mai lai
//				SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
//				promoDetail.updateData(promotion, promotion.orderDetailDTO.maxAmountFree);
//				promotion.listPromoDetail.add(promoDetail);
//			}
//		}

		return sortListOutPut;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 * @param list
	 * @return
	 */
	private ArrayList<OrderDetailViewDTO> groupNewOpenPromotion(
			List<OrderDetailViewDTO> listNewOpenProduct) {
		ArrayList<OrderDetailViewDTO> listPromotionProduct = new ArrayList<OrderDetailViewDTO>();
		// Gom nhom KM mo moi
		if (listNewOpenProduct != null && listNewOpenProduct.size() > 0) {
			// Gom nhom cac KM sp nhu KM ZV 21
			ArrayList<OrderDetailViewDTO> listGroupDetail = new ArrayList<OrderDetailViewDTO>();
			for (OrderDetailViewDTO detail : listNewOpenProduct) {
				boolean isExist = false;
				for (OrderDetailViewDTO groupDetail : listGroupDetail) {
					if (detail.productPromoId > 0
							&& detail.orderDetailDTO.productGroupId == groupDetail.orderDetailDTO.productGroupId
							&& detail.orderDetailDTO.groupLevelId == groupDetail.orderDetailDTO.groupLevelId) {
						// Cap nhat loai KM
						detail.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;

						groupDetail.listPromotionForPromo21.add(detail);
						isExist = true;
						break;
					}
				}

				if (!isExist) {
					// KM duoc sp
					if (detail.productPromoId > 0) {
						// Cap nhat loai KM
						detail.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;

						OrderDetailViewDTO detailView = new OrderDetailViewDTO();
						SaleOrderDetailDTO productSaleDTO = new SaleOrderDetailDTO();
						detailView.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT;
						detailView.orderDetailDTO = productSaleDTO;

						productSaleDTO.programeCode = detail.orderDetailDTO.programeCode;
						productSaleDTO.programeName = detail.orderDetailDTO.programeName;
						productSaleDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
						productSaleDTO.programeTypeCode = detail.orderDetailDTO.programeTypeCode;
						productSaleDTO.isFreeItem = 1; // mat hang khuyen mai
						// Dung cho viec kiem tra sua so luong = 0 -> so suat =
						// 0
						productSaleDTO.productGroupId = detail.orderDetailDTO.productGroupId;
						productSaleDTO.groupLevelId = detail.orderDetailDTO.groupLevelId;
						detailView.listBuyProduct = detail.listBuyProduct;

						detailView.isEdited = detail.isEdited;
						detailView.oldIsEdited = detail.oldIsEdited;
						detailView.isHasQuantityReceived = detail.isHasQuantityReceived;
						detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
						detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);
						detailView.listPromotionForPromo21.add(detail);

						listGroupDetail.add(detailView);
					} else {
						// KM duoc tien, %
						if (detail.listPromotionForPromo21.isEmpty()) {
							detail.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY;
							// KM duoc sp
						} else {
							// Cap nhat loai KM
							detail.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT;
							detail.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;

							for (OrderDetailViewDTO product : detail.listPromotionForPromo21) {
								product.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;
							}
						}

						listGroupDetail.add(detail);
					}
				}
			}

			listPromotionProduct.addAll(listGroupDetail);
		}

		return listPromotionProduct;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<OrderDetailViewDTO>
	 * @throws:
	 * @param promotionProgram
	 * @param listPromotionProduct
	 * @param accumulationOrder
	 * @param listAccuProduct
	 * @return
	 */
	public ArrayList<OrderDetailViewDTO> groupAccumulationPromotion(
			ArrayList<OrderDetailViewDTO> listAccuProduct, int payingOrder) {
		ArrayList<OrderDetailViewDTO> listGroupDetail = new ArrayList<OrderDetailViewDTO>();
		for (OrderDetailViewDTO detail : listAccuProduct) {
			boolean isExist = false;
			for (OrderDetailViewDTO groupDetail : listGroupDetail) {
				if (detail.productPromoId > 0
						&& detail.orderDetailDTO.productGroupId == groupDetail.orderDetailDTO.productGroupId
						&& detail.orderDetailDTO.groupLevelId == groupDetail.orderDetailDTO.groupLevelId) {
					// Cap nhat loai KM
					detail.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
					detail.orderDetailDTO.payingOrder = payingOrder;

					groupDetail.listPromotionForPromo21.add(detail);
					isExist = true;
					break;
				}
			}

			if (!isExist) {
				// KM duoc sp
				if (detail.productPromoId > 0) {
					// Cap nhat loai KM
					detail.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
					detail.orderDetailDTO.payingOrder = payingOrder;

					OrderDetailViewDTO detailView = new OrderDetailViewDTO();
					SaleOrderDetailDTO productSaleDTO = new SaleOrderDetailDTO();
					detailView.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT;
					detailView.orderDetailDTO = productSaleDTO;

					productSaleDTO.programeCode = detail.orderDetailDTO.programeCode;
					productSaleDTO.programeName = detail.orderDetailDTO.programeName;
					productSaleDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
					productSaleDTO.programeTypeCode = detail.orderDetailDTO.programeTypeCode;
					productSaleDTO.isFreeItem = 1; // mat hang khuyen mai
					// Dung cho viec kiem tra sua so luong = 0 -> so suat = 0
					productSaleDTO.productGroupId = detail.orderDetailDTO.productGroupId;
					productSaleDTO.groupLevelId = detail.orderDetailDTO.groupLevelId;
					productSaleDTO.payingOrder = payingOrder;

					detailView.isEdited = detail.isEdited;
					detailView.oldIsEdited = detail.oldIsEdited;
					detailView.isHasQuantityReceived = detail.isHasQuantityReceived;
					detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
					detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);
					detailView.listPromotionForPromo21.add(detail);

					listGroupDetail.add(detailView);
				} else {
					// KM duoc tien, %
					if (detail.listPromotionForPromo21.isEmpty()) {
						detail.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
						// KM tich luy dang KM don hang
					} else {
						// Cap nhat loai KM
						detail.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT;
						detail.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
						detail.orderDetailDTO.payingOrder = payingOrder;

						for (OrderDetailViewDTO product : detail.listPromotionForPromo21) {
							product.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
							product.orderDetailDTO.payingOrder = payingOrder;
						}
					}

					listGroupDetail.add(detail);
				}
			}
		}

		return listGroupDetail;
	}

	/**
	 * Tinh Km tich luy
	 *
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param orderView
	 * @param sortListOutPut
	 * @param sortListProductSale
	 * @param listProductPromotionsale
	 * @param idShopList
	 * @param keyList
	 * @param customerId
	 * @param customerTypeId
	 * @param staffId
	 * @param mDB2
	 * @throws Exception
	 */
	@SuppressLint("DefaultLocale")
	private void calAccumulationPromotion(OrderViewDTO orderView,
			SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut,
			SortedMap<Long, OrderDetailViewDTO> sortListProductSale,
			String idShopList, Long keyList, String customerId,
			String customerTypeId, String staffId, SQLiteDatabase mDB, String orderType) throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProTable = new PROMOTION_PROGRAME_TABLE(mDB);

		ArrayList<OrderDetailViewDTO> listAccuPromotionProduct = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION));
		// B2: tinh KM cho don hang
		ArrayList<PromotionProgrameDTO> listPromotionOrder = promotionProTable.getListAccumulationPromotion(idShopList, customerId, staffId, customerTypeId, orderType);
		for (PromotionProgrameDTO promotionProgram : listPromotionOrder) {
			// lay ds CTKM tuong ung
//			boolean checkProgrameValid = promotionProTable
//					.checkPromotionInValid(
//							String.valueOf(promotionProgram.PROMOTION_PROGRAM_ID),
//							idShopList, customerId, customerTypeId, staffId, false);

			if (promotionProgram != null && promotionProgram.isInvalid()) {
				//validate chuong trinh khuyen mai dung hay khong?
				if (!promotionProTable.validatePromotionPrograme(
						promotionProgram.getPROMOTION_PROGRAM_ID(),
						promotionProgram.getPROMOTION_PROGRAM_CODE(),
						promotionProgram.getMD5_VALID_CODE())) {
					orderView.lstWrongProgrameId.add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
					GlobalInfo.getInstance().getLstWrongProgrameId().add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
				}

				ArrayList<OrderDetailViewDTO> listPromotionProduct = new ArrayList<OrderDetailViewDTO>();
				// kiem tra CTKM hop le
				OrderViewDTO accumulationOrder = promotionProTable.getAccumulationOrder(promotionProgram.getPROMOTION_PROGRAM_ID(), customerId, idShopList, orderView.orderInfo.saleOrderId);
				ArrayList<PromotionProductConvertDTO> listProductConvertGroup = promotionProTable.getListProductConvertGroup(promotionProgram.getPROMOTION_PROGRAM_ID());

				//Ds sp goc ko co trong bang tich luy
				ArrayList<OrderDetailViewDTO> listSourceProductNotExist = new ArrayList<OrderDetailViewDTO>();
				//Ds nhom quy doi
				for(PromotionProductConvertDTO convertGroup: listProductConvertGroup) {
					//Ds sp quy doi trong nhom
					for(PromotionProductConvDtlDTO convertProductInfo: convertGroup.listConvertProduct) {
						//tim sp quy doi da tich luy
						for(OrderDetailViewDTO buyConvertProduct: accumulationOrder.listBuyOrders) {
							if(convertProductInfo.productId == buyConvertProduct.orderDetailDTO.productId) {
								boolean isExist = false;
								for(OrderDetailViewDTO product : accumulationOrder.listBuyOrders) {
									//Tim thay sp goc trong sp ban
									if(convertGroup.sourceProduct.productId == product.orderDetailDTO.productId) {
										isExist = true;
										if(buyConvertProduct.exchangeQuantity() > 0) {
//											BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.exchangeQuantity()));
//											quantity = quantity.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor), 2, RoundingMode.DOWN);
//											quantity = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity())));
											BigDecimalRound quantity = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.exchangeQuantity()));
											quantity = quantity.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor));
											quantity = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity())));
											product.orderDetailDTO.quantity = quantity.toDouble();

//											BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.getAmount()));
//											amount = amount.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor), 0, RoundingMode.DOWN);
//											amount = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.getAmount())));
											BigDecimalRound amount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.getAmount()));
											amount = amount.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor));
											amount = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.getAmount())));
											product.orderDetailDTO.setAmount(amount.toDouble());

//											product.orderDetailDTO.quantity = StringUtil.roundDoubleDown("#.##", buyConvertProduct.orderDetailDTO.quantity * convertGroup.sourceProduct.factor / convertProductInfo.factor);
//											product.orderDetailDTO.amount += (long)(buyConvertProduct.orderDetailDTO.amount * convertGroup.sourceProduct.factor / convertProductInfo.factor);
										}
										break;
									}
								}

								//Ko tim thay sp goc thi tao ra 1 sp goc
								if(!isExist) {
									OrderDetailViewDTO detailView = new OrderDetailViewDTO();
									SaleOrderDetailDTO orderDetail = new SaleOrderDetailDTO();
									detailView.orderDetailDTO = orderDetail;

									orderDetail.productId = (int)convertGroup.sourceProduct.productId;

//									BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.exchangeQuantity()));
//									quantity = quantity.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor), 2, RoundingMode.DOWN);
//									quantity = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTOC.quantity)));
									BigDecimalRound quantity = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.exchangeQuantity()));
									quantity = quantity.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor));
									orderDetail.quantity = quantity.toDouble();

//									BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.getAmount()));
//									amount = amount.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor), 0, RoundingMode.DOWN);
//									amount = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amount)));
									BigDecimalRound amount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.getAmount()));
									amount = amount.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor));
									orderDetail.setAmount(amount.toDouble());


//									orderDetail.quantity = StringUtil.roundDoubleDown("#.##", buyConvertProduct.orderDetailDTO.quantity * convertGroup.sourceProduct.factor / convertProductInfo.factor);
//									orderDetail.amount = (long)(buyConvertProduct.orderDetailDTO.amount * convertGroup.sourceProduct.factor / convertProductInfo.factor);

									accumulationOrder.listBuyOrders.add(detailView);
									listSourceProductNotExist.add(detailView);
								}

								break;
							}
						}
					}
				}

				//Tinh gia tien cua 1 sp
				for(OrderDetailViewDTO orderDetail: accumulationOrder.listBuyOrders) {
					if(orderDetail.orderDetailDTO.priceBegin > 0) {
						orderDetail.orderDetailDTO.price = orderDetail.orderDetailDTO.priceBegin;
					} else {
						if(orderDetail.exchangeQuantity() > 0) {
//							BigDecimal price = new BigDecimal(StringUtil.decimalFormatSymbols("#", orderDetail.orderDetailDTO.getAmount()));
//							price = price.divide(new BigDecimal(orderDetail.exchangeQuantity()), 0, RoundingMode.DOWN);
							BigDecimalRound price = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", orderDetail.orderDetailDTO.getAmount()));
							price = price.divide(new BigDecimal(orderDetail.exchangeQuantity()));

							orderDetail.orderDetailDTO.price = price.toDouble();
						} else {
							orderDetail.orderDetailDTO.quantity = 0;
							orderDetail.orderDetailDTO.setAmount(0);
						}
					}

				}

				accumulationOrder.isCalAccuPromotion = true;
				int multiple = 0;
				//Loai KM mua check so luong & so tien
				int promotionValueType = 0;
				do {
					//CTKM don hang
					SortedMap<Long, List<OrderDetailViewDTO>> results = new TreeMap<Long, List<OrderDetailViewDTO>>();

					ArrayList<OrderDetailViewDTO> listAccuProduct = new ArrayList<OrderDetailViewDTO>();
					if(accumulationOrder.listBuyOrders.size() == 0) {
						results = CalPromotions.calcPromotion(accumulationOrder,
								promotionProgram, sortListProductSale,
								keyList, mDB, GroupLevelDTO.PROMOTION_ORDER, null,null);
					} else {
						SortedMap<Long, OrderDetailViewDTO> sortListProductSaleTemp = new TreeMap<Long, OrderDetailViewDTO>();
						for (OrderDetailViewDTO product : accumulationOrder.listBuyOrders) {
							//Reset sp chua co KM
							product.orderDetailDTO.hasPromotion = false;

							OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
							SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
							orderDetail.orderDetailDTO = orderDTO;

							orderDTO.quantity = product.orderDetailDTO.quantity;
							orderDTO.quantityPackage = product.orderDetailDTO.quantityPackage;
							orderDTO.quantityRetail = product.orderDetailDTO.quantityRetail;
							orderDTO.productId = product.orderDetailDTO.productId;
							orderDTO.price = product.orderDetailDTO.price;
							orderDTO.packagePrice = product.orderDetailDTO.packagePrice;
							orderDTO.setAmount(product.orderDetailDTO.getAmount());
							orderDTO.programeCode = product.orderDetailDTO.programeCode;

							sortListProductSaleTemp.put(Long.valueOf(product.orderDetailDTO.productId), orderDetail);
						}
						results = CalPromotions.calcPromotion(accumulationOrder, promotionProgram,
								sortListProductSaleTemp, keyList, mDB, GroupLevelDTO.PROMOTION_PRODUCT, null,null);
					}
					listAccuProduct = (ArrayList<OrderDetailViewDTO>) results.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
					//Lay boi so lan dau
					if(multiple == 0) {
						multiple = accumulationOrder.multiple;
					}

					if(listAccuProduct != null && listAccuProduct.size() > 0) {
						//Gom nhom cac KM sp nhu KM ZV 21
						ArrayList<OrderDetailViewDTO> listGroupDetail = groupAccumulationPromotion(listAccuProduct, accumulationOrder.multipleUsing);
						//Thuc ra chi co 1 nhom ma thoi
						OrderDetailViewDTO groupDetail = listGroupDetail.get(0);

						//Gom hay tach cac lan dat
//						boolean isNeedSeperate = true;
//						if(groupDetail.type == OrderDetailViewDTO.FREE_PRICE || groupDetail.type == OrderDetailViewDTO.FREE_PERCENT) {
//							if(accumulationOrder.multipleUsing > 1) {
//								isNeedSeperate = false;
//							} else {
//								isNeedSeperate = true;
//							}
//						} else {
//							isNeedSeperate = true;
//						}

//						if(isNeedSeperate) {
							listPromotionProduct.add(groupDetail);
//						} else {
//							groupDetail = listPromotionProduct.get(listPromotionProduct.size() - 1);
//							groupDetail.orderDetailDTO.discountAmount += listGroupDetail.get(0).orderDetailDTO.discountAmount;
//							groupDetail.orderDetailDTO.maxAmountFree += listGroupDetail.get(0).orderDetailDTO.maxAmountFree;
//						}

						//Lay ra du lieu tra rpt cttl pay
						RptCttlPayDTO rptCttlPay = groupDetail.rptCttlPay;

						//So suat KM: KM tich luy ko co so suat
//						orderView.productQuantityReceivedList.addAll(accumulationOrder.productQuantityReceivedList);
//						orderView.productQuantityReceivedList.addAll(accumulationOrder.orderQuantityReceivedList);

						//Tra thuong KM tich luy
						if(rptCttlPay == null) {
							rptCttlPay = new RptCttlPayDTO();
							rptCttlPay.createDate = DateUtils.now();
							rptCttlPay.promotionProgramId = promotionProgram.getPROMOTION_PROGRAM_ID();
							rptCttlPay.promotionFromDate = promotionProgram.getFROM_DATE();
							rptCttlPay.promotionToDate = promotionProgram.getTO_DATE();
							rptCttlPay.shopId = Integer.parseInt(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
							rptCttlPay.customerId = Integer.parseInt(customerId);

							rptCttlPay.promotion = 1;
							rptCttlPay.approved = 0;
							rptCttlPay.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

							groupDetail.rptCttlPay = rptCttlPay;
						}

						for(BuyInfoLevel buyInfo: accumulationOrder.buyInfoLevelArray) {
							BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", rptCttlPay.totalQuantityPayPromotion));
							quantity = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyInfo.usedQuantity)));
							rptCttlPay.totalQuantityPayPromotion = quantity.doubleValue();

							BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", rptCttlPay.totalAmountPayPromotion));
							amount = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#",  buyInfo.usedAmount)));
							rptCttlPay.totalAmountPayPromotion = amount.longValue();

//							rptCttlPay.totalQuantityPayPromotion += buyInfo.usedQuantity;
//							rptCttlPay.totalAmountPayPromotion += buyInfo.usedAmount;
						}

						HashMap<Long, RptCttlDetailPayDTO> productPayList = new HashMap<Long, RptCttlDetailPayDTO>();
						for(RptCttlDetailPayDTO payDetail : rptCttlPay.rptCttlDetailList) {
							productPayList.put(Long.valueOf(payDetail.productId), payDetail);
						}
						//Phan bo so luong sp tich luy da su dung de tinh KM
						SortedMap<Long, OrderDetailViewDTO> sortListProductSaleTemp = new TreeMap<Long, OrderDetailViewDTO>();
						for (OrderDetailViewDTO product : accumulationOrder.listBuyOrders) {
							sortListProductSaleTemp.put(Long.valueOf(product.orderDetailDTO.productId), product);
						}
						//Duyet qua co cau
						for(GroupLevelDTO subLevel: accumulationOrder.groupLevel.subLevelArray) {
							int index = accumulationOrder.groupLevel.subLevelArray.indexOf(subLevel);
							BuyInfoLevel buyInfo = accumulationOrder.buyInfoLevelArray.get(index);
							boolean newRound = true;
							//Khi nao tru het co cau thi dung lai
							do {
								for(GroupLevelDetailDTO levelDetail : subLevel.levelDetailArray) {
									promotionValueType = levelDetail.valueType;
									Long key = Long.valueOf(levelDetail.productId);
									OrderDetailViewDTO p = sortListProductSaleTemp.get(key);

									//San pham yeu cau ban thi phai thoa man so luong/ so tien qui dinh
									if(p != null && ((p.orderDetailDTO.getAmount() - p.orderDetailDTO.amountUsed > 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) ||
											(p.orderDetailDTO.quantity - p.orderDetailDTO.quantityUsed > 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY))) {
										boolean isHasPay = true;
										RptCttlDetailPayDTO payDetail = productPayList.get(Long.valueOf(levelDetail.productId));
										if(payDetail == null) {
											isHasPay = false;
											payDetail = new RptCttlDetailPayDTO();
											payDetail.productId = levelDetail.productId;
											payDetail.staffId = rptCttlPay.staffId;
											payDetail.shopId = rptCttlPay.shopId;
											payDetail.createDate = DateUtils.now();
											productPayList.put(Long.valueOf(payDetail.productId), payDetail);
										}
										//Lay boi so nho nhat cua tung san pham
										//Sp bat buoc
										double payValue = 0;
										double requireValue = levelDetail.value;

										if(levelDetail.isRequired == 1) {
											//Chua tra tinh so luong tra
											//Neu ko co khai bao so luong
//											if(requireValue == 0) {
											if(BigDecimal.valueOf(requireValue).compareTo(BigDecimal.ZERO) == 0) {
												if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
													requireValue = 1;
												} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
													BigDecimalRound price = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountBegin));
													price = price.divide(new BigDecimal(p.orderDetailDTO.quantityBegin));
													requireValue = price.toLong();
//													requireValue = StringUtil.roundDoubleUp("#", p.orderDetailDTO.amountBegin / p.orderDetailDTO.quantityBegin);
												}
											}
											//Neu chua tra hoac vong moi
											if(!isHasPay || newRound) {
//												payValue = requireValue;
												if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
													BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.exchangeQuantity()));
													quantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed)));

													payValue = (quantity.doubleValue() < buyInfo.usedQuantity) ? quantity.doubleValue() : buyInfo.usedQuantity;
													payValue = (requireValue > 0 && requireValue < payValue) ? requireValue : payValue;

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payDetail.quantityPromotion));
													payDetail.quantityPromotion = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyInfo.usedQuantity));
													buyInfo.usedQuantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed));
													p.orderDetailDTO.quantityUsed = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();
												} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
													BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.getAmount()));
													amount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed)));

													payValue = (amount.longValue() < buyInfo.usedAmount) ? amount.longValue() : buyInfo.usedAmount;
													payValue = (requireValue > 0 && requireValue < payValue) ? requireValue : payValue;

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", payDetail.amountPromotion));
													payDetail.amountPromotion = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", buyInfo.usedAmount));
													buyInfo.usedAmount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed));
													p.orderDetailDTO.amountUsed = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();
												}
											} else {
												if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
													BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.exchangeQuantity()));
													quantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed)));

													payValue = (quantity.doubleValue() < buyInfo.usedQuantity) ? quantity.doubleValue() : buyInfo.usedQuantity;
													payValue = (requireValue > 0 && requireValue < payValue) ? requireValue : payValue;

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payDetail.quantityPromotion));
													payDetail.quantityPromotion = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyInfo.usedQuantity));
													buyInfo.usedQuantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();;

													quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed));
													p.orderDetailDTO.quantityUsed = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();
												} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
													BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.getAmount()));
													amount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed)));

													payValue = (amount.longValue() < buyInfo.usedAmount) ? amount.longValue() : buyInfo.usedAmount;
													payValue = (requireValue > 0 && requireValue < payValue) ? requireValue : payValue;

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", payDetail.amountPromotion));
													payDetail.amountPromotion = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", buyInfo.usedAmount));
													buyInfo.usedAmount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

													amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed));
													p.orderDetailDTO.amountUsed = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();
												}
											}
										//Sp khong bat buoc
										} else {
											if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
												BigDecimal quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.exchangeQuantity()));
												quantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed)));

												payValue = (quantity.doubleValue() < buyInfo.usedQuantity) ? quantity.doubleValue() : buyInfo.usedQuantity;

												quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payDetail.quantityPromotion));
												payDetail.quantityPromotion = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();

												quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyInfo.usedQuantity));
												buyInfo.usedQuantity = quantity.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();;

												quantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.orderDetailDTO.quantityUsed));
												p.orderDetailDTO.quantityUsed = quantity.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payValue))).doubleValue();

											} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
												BigDecimal amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.getAmount()));
												amount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed)));

												payValue = (amount.longValue() < buyInfo.usedAmount) ? amount.longValue() : buyInfo.usedAmount;

												amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", payDetail.amountPromotion));
												payDetail.amountPromotion = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

												amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", buyInfo.usedAmount));
												buyInfo.usedAmount = amount.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();

												amount = new BigDecimal(StringUtil.decimalFormatSymbols("#", p.orderDetailDTO.amountUsed));
												p.orderDetailDTO.amountUsed = amount.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", payValue))).longValue();
											}
										}
									} else {
										if(levelDetail.isRequired == 1) {
											//Neu chua tra hoac vong moi
											if(newRound) {
												groupDetail.notGetThisPromotion = true;
											}
										}
									}

									//Neu da tra het thi khong tinh tiep nua
									if((buyInfo.usedQuantity <= 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY)
											|| (buyInfo.usedAmount <= 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT)) {
										break;
									}
								}
								//Het 1 vong duyet co cau phan bo
								newRound = false;
							} while ((buyInfo.usedQuantity > 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY)
									|| (buyInfo.usedAmount > 0 && promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT));
						}

						//Cap nhat lai ds phan bo chi tiet
						rptCttlPay.rptCttlDetailList.clear();
						rptCttlPay.rptCttlDetailList.addAll(productPayList.values());
					} else {
						//Neu lan KM truoc chua tra het
						if(accumulationOrder.orderInfo.getDiscount() > 0) {
							//Tao sp KM
							OrderDetailViewDTO promotion = new OrderDetailViewDTO();
							SaleOrderDetailDTO productSaleDTO = new SaleOrderDetailDTO();
							promotion.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
							promotion.orderDetailDTO = productSaleDTO;

							productSaleDTO.programeCode = promotionProgram.getPROMOTION_PROGRAM_CODE();
							productSaleDTO.programeName = promotionProgram.getPROMOTION_PROGRAM_NAME();
							productSaleDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
							productSaleDTO.programeTypeCode = promotionProgram.getTYPE();
							productSaleDTO.isFreeItem = 1; // mat hang khuyen mai

							promotion.isEdited = promotionProgram.getIsEdited();
							promotion.oldIsEdited = promotionProgram.getIsEdited();
							promotion.isHasQuantityReceived = promotionProgram.isHasQuantityReceived();
							productSaleDTO.setDiscountAmount(accumulationOrder.orderInfo.getDiscount());
							productSaleDTO.maxAmountFree = accumulationOrder.orderInfo.getDiscount();

							promotion.type = OrderDetailViewDTO.FREE_PRICE;
							promotion.typeName = StringUtil.getString(R.string.TEXT_MONEY_PROMOTION_NAME);

							listPromotionProduct.add(promotion);

							//Tra thuong KM tich luy
							RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();
							rptCttlPay.createDate = DateUtils.now();
							rptCttlPay.promotionProgramId = promotionProgram.getPROMOTION_PROGRAM_ID();
							rptCttlPay.promotionFromDate = promotionProgram.getFROM_DATE();
							rptCttlPay.promotionToDate = promotionProgram.getTO_DATE();
							rptCttlPay.shopId = Integer.parseInt(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
							rptCttlPay.customerId = Integer.parseInt(customerId);
							rptCttlPay.promotion = 0;
							rptCttlPay.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
							promotion.rptCttlPay = rptCttlPay;

							//Tra tien dot nay luon roi thi reset discount
							accumulationOrder.orderInfo.setDiscount(0);
						}
					}


					if (results.size() > OrderViewDTO.INDEX_PROMOTION_PRODUCT) {
						Iterator<Long> it = results.keySet().iterator();
						it.next();// KM sp

						while (it.hasNext()) {
							keyList++;
							Long md = it.next();
							List<OrderDetailViewDTO> listProductChange = results.get(md);

							for(OrderDetailViewDTO changeProduct : listProductChange) {
								changeProduct.keyList = keyList;
								// KM duoc sp
								if(changeProduct.productPromoId > 0) {
									//Cap nhat loai KM
									changeProduct.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
									changeProduct.orderDetailDTO.payingOrder = accumulationOrder.multipleUsing;
								}
							}

							sortListOutPut.put(keyList, listProductChange);
						}
					}
				} while (multiple > accumulationOrder.multipleUsing);

				//Thuc hien tinh lai quy doi
				OrderViewDTO orderAccu = promotionProTable.getAccumulationOrder(promotionProgram.getPROMOTION_PROGRAM_ID(), customerId, idShopList, orderView.orderInfo.saleOrderId);
				//Tao ds cac sp goc ko co trong bang tich luy vao trong don tich luy
				for(OrderDetailViewDTO sourceProduct : listSourceProductNotExist) {
					sourceProduct.orderDetailDTO.quantity = 0;
					sourceProduct.orderDetailDTO.quantityRetail = 0;
					sourceProduct.orderDetailDTO.quantityPackage = 0;
					sourceProduct.orderDetailDTO.setAmount(0);
					sourceProduct.orderDetailDTO.quantityUsed = 0;
					sourceProduct.orderDetailDTO.amountUsed = 0;
				}
				orderAccu.listBuyOrders.addAll(listSourceProductNotExist);

				//Ds cac lan dat
				for(OrderDetailViewDTO promotion: listPromotionProduct) {
					//Co tra tich luy
					if(promotion.rptCttlPay != null) {
						promotion.rptCttlPay.promotionValueType = promotionValueType;
						//Ds cac tru tich luy
						ArrayList<RptCttlDetailPayDTO> listDetailPayConvert = new ArrayList<RptCttlDetailPayDTO>();
						//Neu co phan bo thi reset lai de tinh cho dung tong cac thang con
						if(!promotion.rptCttlPay.rptCttlDetailList.isEmpty()) {
							promotion.rptCttlPay.totalQuantityPayPromotion = 0;
							promotion.rptCttlPay.totalAmountPayPromotion = 0;
						}
						for(RptCttlDetailPayDTO payDetailSource : promotion.rptCttlPay.rptCttlDetailList) {
							for(OrderDetailViewDTO product: orderAccu.listBuyOrders) {
								if(payDetailSource.productId == product.orderDetailDTO.productId) {
									if(promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
										//So luong su dung < so luong tich luy
										if(payDetailSource.quantityPromotion <= product.exchangeQuantity() - product.orderDetailDTO.quantityUsed) {
											BigDecimalRound quantityPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", payDetailSource.quantityPromotion));
											BigDecimal amountPromotion = quantityPromotion
													.multiply(new BigDecimal(product.orderDetailDTO.getAmount()))
													.divide(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity()))).getValue();

											payDetailSource.amountPromotion = amountPromotion.longValue();

											product.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed))).toDouble();
											product.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed))).longValue();

											promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).toDouble();
											promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).longValue();
										} else {
											//So luong or so tien toi da cua san pham goc
											BigDecimalRound quantityPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity()));
											quantityPromotion = quantityPromotion.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed)));

											BigDecimal amountPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.getAmount()));
											amountPromotion = amountPromotion.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed)));

											//So luong sp goc con du
											double remainQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", payDetailSource.quantityPromotion)))
																		.subtract(quantityPromotion.getValue()).doubleValue();
//											double remainQuantity = payDetailSource.quantityPromotion - quantityMax;

											//Tinh lai tong so luong va tong tien ung voi sp goc
											payDetailSource.quantityPromotion = quantityPromotion.toDouble();
											payDetailSource.amountPromotion = amountPromotion.longValue();

											//So luong da su dung
											product.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed))).toDouble();
											product.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed))).longValue();

											//Tang so luong sp quy doi
											promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).toDouble();
											promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).longValue();

											//Ds nhom quy doi
											for(PromotionProductConvertDTO convertGroup: listProductConvertGroup) {
												if(convertGroup.sourceProduct.productId == product.orderDetailDTO.productId) {
													//Ds sp quy doi trong nhom
													for(PromotionProductConvDtlDTO convertProductInfo: convertGroup.listConvertProduct) {
														//tim sp quy doi da tich luy
														for(OrderDetailViewDTO buyConvertProduct: orderAccu.listBuyOrders) {
															if(convertProductInfo.productId == buyConvertProduct.orderDetailDTO.productId) {
																if(buyConvertProduct.orderDetailDTO.quantity - buyConvertProduct.orderDetailDTO.quantityUsed > 0) {
																	//Quy doi so luong sp goc ra so luong sp quy doi
//																	BigDecimal quantityConvert = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", remainQuantity));
//																	quantityConvert = quantityConvert.multiply(new BigDecimal(convertProductInfo.factor));
//																	quantityConvert = quantityConvert.divide(new BigDecimal(convertGroup.sourceProduct.factor), 2, RoundingMode.DOWN);
																	BigDecimalRound quantityConvert = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", remainQuantity));
																	quantityConvert = quantityConvert.multiply(new BigDecimal(convertProductInfo.factor));
																	quantityConvert = quantityConvert.divide(new BigDecimal(convertGroup.sourceProduct.factor));

																	//So luong sp quy doi su dung
																	quantityPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##",
																			quantityConvert.toDouble() <= buyConvertProduct.exchangeQuantity() ? quantityConvert.toDouble() : buyConvertProduct.exchangeQuantity()));
																	//Amount sp quy doi su dung
																	amountPromotion = quantityPromotion
																			.multiply(new BigDecimal(buyConvertProduct.orderDetailDTO.amountBegin))
																			.divide(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.orderDetailDTO.quantityBegin))).getValue();

																	//Pay detail sp qui doi
																	RptCttlDetailPayDTO payDetailConvert = new RptCttlDetailPayDTO();
																	payDetailConvert.productId = buyConvertProduct.orderDetailDTO.productId;
																	payDetailConvert.staffId = promotion.rptCttlPay.staffId;
																	payDetailConvert.shopId = promotion.rptCttlPay.shopId;
																	payDetailConvert.createDate = DateUtils.now();
																	payDetailConvert.quantityPromotion = quantityPromotion.toDouble();
																	payDetailConvert.amountPromotion = amountPromotion.longValue();
																	payDetailConvert.isConvertProduct = true;
																	payDetailConvert.amountBegin = buyConvertProduct.orderDetailDTO.amountBegin;
																	payDetailConvert.quantityBegin = buyConvertProduct.orderDetailDTO.quantityBegin;
																	listDetailPayConvert.add(payDetailConvert);

																	//Thong tin sp quy doi da su dung
																	buyConvertProduct.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.orderDetailDTO.quantityUsed))).toDouble();
																	buyConvertProduct.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.amountUsed))).longValue();

																	//Tang so luong bang pay voi thong tin sp quy doi
																	promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).toDouble();
																	promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).longValue();

																	//Thong tin sp quy doi con lai
																	quantityConvert = quantityConvert.subtract(quantityPromotion.getValue());

																	//Quy doi nguoc lai sp goc
																	remainQuantity = quantityConvert.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor)).toDouble();
																}
																break;
															}
														}

														if(remainQuantity <= 0) {
															break;
														}
													}

													if(remainQuantity <= 0) {
														break;
													}
												}
											}
										}
									} else if(promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
										//So luong su dung < so luong tich luy
										if(payDetailSource.amountPromotion <= product.orderDetailDTO.getAmount() - product.orderDetailDTO.amountUsed) {
											BigDecimalRound amountPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", payDetailSource.amountPromotion));
											BigDecimal quantityPromotion = amountPromotion
													.multiply(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityBegin)))
													.divide(new BigDecimal(product.orderDetailDTO.amountBegin)).getValue();

											payDetailSource.quantityPromotion = quantityPromotion.doubleValue();

											product.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed))).doubleValue();
											product.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed))).toLong();

											promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).doubleValue();
											promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).toLong();
										} else {
											//So luong or so tien toi da cua san pham goc
											BigDecimal quantityPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity()));
											quantityPromotion = quantityPromotion.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed)));

											BigDecimalRound amountPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.getAmount()));
											amountPromotion = amountPromotion.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed)));

											//So luong sp goc con du
											long remainAmount = (new BigDecimal(StringUtil.decimalFormatSymbols("#", payDetailSource.amountPromotion)))
													.subtract(amountPromotion.getValue()).longValue();

											//Tinh lai tong so luong va tong tien ung voi sp goc
											payDetailSource.quantityPromotion = quantityPromotion.doubleValue();
											payDetailSource.amountPromotion = amountPromotion.toLong();

											//So luong da su dung
											product.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.orderDetailDTO.quantityUsed))).doubleValue();
											product.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", product.orderDetailDTO.amountUsed))).toLong();

											//Tang so luong sp quy doi
											promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).doubleValue();
											promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).toLong();


											//Ds nhom quy doi
											for(PromotionProductConvertDTO convertGroup: listProductConvertGroup) {
												if(convertGroup.sourceProduct.productId == product.orderDetailDTO.productId) {
													//Ds sp quy doi trong nhom
													for(PromotionProductConvDtlDTO convertProductInfo: convertGroup.listConvertProduct) {
														//tim sp quy doi da tich luy
														for(OrderDetailViewDTO buyConvertProduct: orderAccu.listBuyOrders) {
															if(convertProductInfo.productId == buyConvertProduct.orderDetailDTO.productId) {
																if(buyConvertProduct.orderDetailDTO.getAmount() - buyConvertProduct.orderDetailDTO.amountUsed > 0) {
																	//Quy doi so luong sp goc ra so luong sp quy doi
																	BigDecimalRound amountConvert = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", remainAmount));
																	amountConvert = amountConvert.multiply(new BigDecimal(convertProductInfo.factor));
//																	amountConvert = amountConvert.divide(new BigDecimal(convertGroup.sourceProduct.factor), 0, RoundingMode.DOWN);
																	amountConvert = amountConvert.divide(new BigDecimal(convertGroup.sourceProduct.factor));

																	//So luong sp quy doi su dung
																	amountPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#",
																			amountConvert.toDouble() <= buyConvertProduct.orderDetailDTO.getAmount() ? amountConvert.toDouble() : buyConvertProduct.orderDetailDTO.getAmount()));
																	//Amount sp quy doi su dung
																	quantityPromotion = amountPromotion.multiply(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.orderDetailDTO.quantityBegin)))
																			.divide(new BigDecimal(buyConvertProduct.orderDetailDTO.amountBegin)).getValue();

																	//Pay detail sp qui doi
																	RptCttlDetailPayDTO payDetailConvert = new RptCttlDetailPayDTO();
																	payDetailConvert.productId = buyConvertProduct.orderDetailDTO.productId;
																	payDetailConvert.staffId = promotion.rptCttlPay.staffId;
																	payDetailConvert.shopId = promotion.rptCttlPay.shopId;
																	payDetailConvert.createDate = DateUtils.now();
																	payDetailConvert.quantityPromotion = quantityPromotion.doubleValue();
																	payDetailConvert.amountPromotion = amountPromotion.toLong();
																	payDetailConvert.isConvertProduct = true;
																	payDetailConvert.amountBegin = buyConvertProduct.orderDetailDTO.amountBegin;
																	payDetailConvert.quantityBegin = buyConvertProduct.orderDetailDTO.quantityBegin;
																	listDetailPayConvert.add(payDetailConvert);

																	//Thong tin sp quy doi da su dung
																	buyConvertProduct.orderDetailDTO.quantityUsed = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", buyConvertProduct.orderDetailDTO.quantityUsed))).doubleValue();
																	buyConvertProduct.orderDetailDTO.amountUsed = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", buyConvertProduct.orderDetailDTO.amountUsed))).toLong();

																	//Tang so luong bang pay voi thong tin sp quy doi
																	promotion.rptCttlPay.totalQuantityPayPromotion = quantityPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", promotion.rptCttlPay.totalQuantityPayPromotion))).doubleValue();
																	promotion.rptCttlPay.totalAmountPayPromotion = amountPromotion.add(new BigDecimal(StringUtil.decimalFormatSymbols("#", promotion.rptCttlPay.totalAmountPayPromotion))).toLong();


																	//Thong tin sp quy doi con lai
																	amountConvert = amountConvert.subtract(amountPromotion.getValue());

																	//Quy doi nguoc lai sp goc
																	remainAmount = amountConvert.multiply(new BigDecimal(convertGroup.sourceProduct.factor)).divide(new BigDecimal(convertProductInfo.factor)).toLong();
																}
																break;
															}
														}

														if(remainAmount <= 0) {
															break;
														}
													}

													if(remainAmount <= 0) {
														break;
													}
												}
											}
										}
									}
								}
							}
						}
						promotion.rptCttlPay.rptCttlDetailList.addAll(listDetailPayConvert);
					}

					//Tinh tien, % tuong ung voi sp tich luy
					if(promotion.type == OrderDetailViewDTO.FREE_PRICE) {
						BigDecimalRound discountPercent = new BigDecimalRound(promotion.orderDetailDTO.getDiscountAmount());
						discountPercent = discountPercent.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT));
						discountPercent = discountPercent.divide(new BigDecimal(promotion.rptCttlPay.totalAmountPayPromotion));
						promotion.orderDetailDTO.discountPercentage = discountPercent.toDouble();

//						promotion.orderDetailDTO.discountPercentage = StringUtil
//								.roundDoubleDown("#.##",
//										Math.round((double) promotion.orderDetailDTO.discountAmount * CalPromotions.ROUND_NO_PERCENT
//												/ promotion.rptCttlPay.totalAmountPayPromotion) / (double) CalPromotions.ROUND_PERCENT);
					} else if(promotion.type == OrderDetailViewDTO.FREE_PERCENT) {
						BigDecimalRound discountAmount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
						discountAmount = discountAmount.multiply(new BigDecimal(promotion.rptCttlPay.totalAmountPayPromotion));
						discountAmount = discountAmount.divide(new BigDecimal(CalPromotions.ROUND_PERCENT));
						promotion.orderDetailDTO.setDiscountAmount(discountAmount.toDouble());

//						promotion.orderDetailDTO.discountAmount = (long) StringUtil
//								.roundDoubleDown(
//										"#",
//										promotion.orderDetailDTO.discountPercentage
//												* CalPromotions.ROUND_PERCENT
//												* promotion.rptCttlPay.totalAmountPayPromotion
//												/ CalPromotions.ROUND_NO_PERCENT);
						promotion.orderDetailDTO.maxAmountFree = promotion.orderDetailDTO.getDiscountAmount();
					}
				}

				//Them vao ds tra ve
				listAccuPromotionProduct.addAll(listPromotionProduct);
			}
		}
 	}

	/**
	 * Update thong tin cho 1 sp KM
	 *
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param promotionProduct
	 * @param productPromotionInfo
	 */
	private void updatePromotionProductInfo(
			OrderDetailViewDTO promotionProduct,
			OrderDetailViewDTO productPromotionInfo) {
		promotionProduct.productCode = productPromotionInfo.productCode;
		promotionProduct.productName = productPromotionInfo.productName;
		promotionProduct.convfact = productPromotionInfo.convfact;
		promotionProduct.stock = productPromotionInfo.stock;
		promotionProduct.stockActual = productPromotionInfo.stockActual;
		promotionProduct.stockId = productPromotionInfo.stockId;

		promotionProduct.orderDetailDTO.convfact = productPromotionInfo.convfact;
		promotionProduct.orderDetailDTO.catId = productPromotionInfo.orderDetailDTO.catId;
		promotionProduct.orderDetailDTO.vat = productPromotionInfo.orderDetailDTO.vat;
		promotionProduct.orderDetailDTO.priceId = productPromotionInfo.orderDetailDTO.priceId;
		//for check null price
		boolean isNullPrice = (promotionProduct.orderDetailDTO.priceId <= 0);

		//chi lay lai gia khi khong thay doi gia
		if(!promotionProduct.isModifyPrice){
			promotionProduct.orderDetailDTO.price = productPromotionInfo.orderDetailDTO.price;
			promotionProduct.orderDetailDTO.priceNotVat = productPromotionInfo.orderDetailDTO.priceNotVat;
			//check null price
			promotionProduct.orderDetailDTO.isPriceNull = isNullPrice;
		} else{
			//cap nhat lai price not vat
			double priceNotVat = (promotionProduct.orderDetailDTO.price
					/ (1 + (promotionProduct.orderDetailDTO.vat / 100)));
			promotionProduct.orderDetailDTO.priceNotVat = StringUtil.roundPromotion(priceNotVat);

		}
		//chi lay lai gia khi khong thay doi gia dong goi
		if(!promotionProduct.isModifyPackagePrice){
			promotionProduct.orderDetailDTO.packagePrice = productPromotionInfo.orderDetailDTO.packagePrice;
			promotionProduct.orderDetailDTO.packagePriceNotVat = productPromotionInfo.orderDetailDTO.packagePriceNotVat;
			//check null price
			promotionProduct.orderDetailDTO.isPackagePriceNull = isNullPrice;
		} else{
			//cap nhat lai package price not vat
			double packagePriceNotVat = (promotionProduct.orderDetailDTO.packagePrice
					/ (1 + (promotionProduct.orderDetailDTO.vat / 100)));
			promotionProduct.orderDetailDTO.packagePriceNotVat = StringUtil.roundPromotion(packagePriceNotVat);
		}
		promotionProduct.grossWeight = productPromotionInfo.grossWeight;
		BigDecimalRound totalWeight = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.###", promotionProduct.grossWeight));
		totalWeight = totalWeight.multiply(new BigDecimal(promotionProduct.exchangeQuantity())).setScale();
		promotionProduct.orderDetailDTO.totalWeight = totalWeight.toDouble();
	}

	/**
	 * Tinh KM cho don hang
	 *
	 * @author: TruongHN
	 * @param orderView
	 * @param sortListOutPut
	 * @param listProductPromotionsale
	 * @param shopIdList
	 * @param keyList
	 * @param promotionProTable
	 * @param proProgrameDetailTable
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	private void calPromotionForOrder2(OrderViewDTO orderView,
			SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut,
			SortedMap<Long, OrderDetailViewDTO> sortListProductSale,
			String shopIdList, Long keyList, String customerId,
			String customerTypeId, String staffId, SQLiteDatabase mDB,
			String idShopList, String orderType) throws Exception {
		PROMOTION_PROGRAME_TABLE promotionProTable = new PROMOTION_PROGRAME_TABLE(mDB);

		ArrayList<OrderDetailViewDTO> listProductPromotionsale = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER));
		// B2: tinh KM cho don hang
		ArrayList<PromotionProgrameDTO> listPromotionOrder = promotionProTable.getListPromotionForOrder2(shopIdList, customerId, staffId, customerTypeId, orderType);
		ArrayList<OrderDetailViewDTO> listPromotionForOrderChange = new ArrayList<OrderDetailViewDTO>();
//		ArrayList<OrderDetailViewDTO> listOrderProductPromotion = new ArrayList<OrderDetailViewDTO>();
		boolean hasOrderPromotion = false;
		for (int i = 0, size = listPromotionOrder.size(); i < size; i++) {
			// lay ds CTKM tuong ung
			PromotionProgrameDTO promotionProgram = listPromotionOrder.get(i);
			if (promotionProgram != null && promotionProgram.isInvalid()) {
				//validate chuong trinh khuyen mai dung hay khong?
				if (!promotionProTable.validatePromotionPrograme(
						promotionProgram.getPROMOTION_PROGRAM_ID(),
						promotionProgram.getPROMOTION_PROGRAM_CODE(),
						promotionProgram.getMD5_VALID_CODE())) {
					orderView.lstWrongProgrameId.add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
					GlobalInfo.getInstance().getLstWrongProgrameId().add((long)promotionProgram.getPROMOTION_PROGRAM_ID());
				}
				// kiem tra CTKM hop le

//				boolean checkProgrameValid = promotionProTable.checkPromotionInValid(
//								String.valueOf(promotionProgram.PROMOTION_PROGRAM_ID),
//								shopIdList, customerId, customerTypeId, staffId, true);
//				if (checkProgrameValid) {
					boolean isPromotionOpen = false;
					//Kiem tra xem KM mo moi co dat dk mo moi de tinh KM hay ko?
					if(promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
						isPromotionOpen = promotionProTable.isPromotionNewOpen(promotionProgram.getPROMOTION_PROGRAM_ID(), customerId, idShopList, orderView);
					}
					// KM thuong hoac ZV24 (mo moi) & co mo moi
					if (!promotionProgram.getTYPE().equals(CalPromotions.ZV24)
							|| (promotionProgram.getTYPE().equals(CalPromotions.ZV24)
							&& isPromotionOpen)) {

						SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPutTemp = CalPromotions
								.calcPromotion(orderView, promotionProgram,
										sortListProductSale, keyList, mDB,
										GroupLevelDTO.PROMOTION_ORDER, null,null);
						// Huong duoc KM cua 1 program -> co the huong duoc
						// nhieu group
						// Them vao ds doi hang
						ArrayList<OrderDetailViewDTO> listOrderProductPromotion = (ArrayList<OrderDetailViewDTO>) sortListOutPutTemp
								.get(Long
										.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT));
						ArrayList<OrderDetailViewDTO> listPromotionProductOpen = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));

						if (listOrderProductPromotion.size() > 0) {
							orderView.isHaveOneOrderPromotion = !hasOrderPromotion;
							if(!promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
								listPromotionForOrderChange.addAll(listOrderProductPromotion);
								if (!hasOrderPromotion) {
									hasOrderPromotion = true;
									listProductPromotionsale.addAll(listOrderProductPromotion);
									// Lay ds so suat cua KM don hang dau tien vao
									// ds so suat
									orderView.productQuantityReceivedList.addAll(orderView.orderQuantityReceivedList);
								}
							} else if(promotionProgram.getTYPE().equals(CalPromotions.ZV24) && isPromotionOpen) {
								listPromotionProductOpen.addAll(listOrderProductPromotion);
								if (orderView.orderQuantityReceivedList != null) {
									ArrayList<SaleOrderPromotionDTO> listRemove = new ArrayList<SaleOrderPromotionDTO>();
									for (SaleOrderPromotionDTO newPromo : orderView.orderQuantityReceivedList) {
										if (newPromo.promotionProgramCode
												.equals(promotionProgram.getPROMOTION_PROGRAM_CODE())
												&& !orderView.productQuantityReceivedList
														.contains(newPromo)) {
											// them chuong trinh mo moi cho don
											// hang vao list
											orderView.productQuantityReceivedList.add(newPromo);
											listRemove.add(newPromo);
										}
									}

									//Remove ds so suat ma dang luu ben ds so suat cua don hang
									orderView.orderQuantityReceivedList.removeAll(listRemove);
								}
							}


							// listOrderProductPromotion.clear();
							// Ds sp doi
							if (sortListOutPutTemp.size() > 1) {
								Iterator<Long> it = sortListOutPutTemp.keySet()
										.iterator();
								it.next();// KM sp

								while (it.hasNext()) {
									keyList++;
									Long md = it.next();
									List<OrderDetailViewDTO> listProductChange = sortListOutPutTemp
											.get(md);

									// for(OrderDetailViewDTO changeProduct :
									// listProductChange) {
									// changeProduct.keyList = keyList;
									// }
									//Neu la KM ZV24
									if(promotionProgram.getTYPE().equals(CalPromotions.ZV24)) {
										for(OrderDetailViewDTO product : listProductChange) {
											product.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;
										}
									}

									sortListOutPut.put(keyList, listProductChange);
								}
							}
						}
					}
//				}
			}
		}
		// add vao nhung CTKM don hang ma ko dat
//		for (PromotionProgrameDTO promotion : listPromotionOrder) {
//			boolean isAchive = false;
//			for (OrderDetailViewDTO productAchive : listProductPromotionsale) {
//				if (promotion.PROMOTION_PROGRAM_CODE
//						.equals(productAchive.orderDetailDTO.programeCode)) {
//					isAchive = true;
//					break;
//				}
//			}
//			if (!isAchive) {
//				// luu thong tin cho sp ko dat KM cho don hang
//				for (int i = 0, size = orderView.listBuyOrders.size(); i < size; i++) {
//					OrderDetailViewDTO temp = new OrderDetailViewDTO();
//					temp.orderDetailDTO = new SaleOrderDetailDTO();
//					temp.orderDetailDTO.programeCode = promotion.PROMOTION_PROGRAM_CODE;
//					listProductPromotionsale.add(temp);
//				}
//			}
//		}

//		if (listPromotionForOrderChange.size() > 0) {
//			// neu co doi CTKM cho don hang
//			listProductPromotionsale.add(listPromotionForOrderChange.get(0));
//		}

		if (listPromotionForOrderChange.size() > 0) {
			// neu co doi CTKM cho don hang
			orderView.listPromotionForOrderChange = listPromotionForOrderChange;
		}
	}

	/**
	 * Tinh KM cho don hang
	 *
	 * @author: TruongHN
	 * @param orderView
	 * @param sortListOutPut
	 * @param listProductPromotionsale
	 * @param shopIdList
	 * @param keyList
	 * @param promotionProTable
	 * @param proProgrameDetailTable
	 * @return: void
	 * @throws:
	 */
	// private void calPromotionForOrder(OrderViewDTO orderView,
	// SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut,
	// ArrayList<OrderDetailViewDTO> listProductPromotionsale,
	// String shopIdList, Long keyList,
	// PROMOTION_PROGRAME_TABLE promotionProTable,
	// PROMOTION_PROGRAME_DETAIL_TABLE proProgrameDetailTable,
	// String customerId, String customerTypeId, String staffId) {
	// long total = orderView.orderInfo.amount;
	//
	// // B2: tinh KM cho don hang
	// ArrayList<OrderDetailViewDTO> listPromotionForOrderChange = new
	// ArrayList<OrderDetailViewDTO>();
	// ArrayList<PromotionProgrameDTO> listPromotionOrder = promotionProTable
	// .getListPromotionForOrder(shopIdList, customerId, staffId);
	// for (int i = 0, size = listPromotionOrder.size(); i < size; i++) {
	// // lay ds CTKM tuong ung
	// PromotionProgrameDTO promotionProgram = listPromotionOrder.get(i);
	// if (promotionProgram != null) {
	// // kiem tra CTKM hop le
	// boolean checkProgrameValid = promotionProTable
	// .checkPromotionInValid(
	// String.valueOf(promotionProgram.PROMOTION_PROGRAM_ID),
	// shopIdList, customerId, customerTypeId, staffId, true);
	// if (checkProgrameValid) {
	// ArrayList<PromotionProDetailDTO> listPromotionDetail = new
	// ArrayList<PromotionProDetailDTO>();
	// // Ds cac chi tiet khuyen mai cua chuong trinh khuyen mai
	// listPromotionDetail = proProgrameDetailTable
	// .getPromotionDetailByPromotionId((int)
	// promotionProgram.PROMOTION_PROGRAM_ID);
	// if (listPromotionDetail.size() > 0) {
	// // tinh KM tuong ung
	// OrderDetailViewDTO promotionOrder = CalPromotions
	// .calcPromotionForOrder(total, promotionProgram,
	// listPromotionDetail,
	// listProductPromotionsale, keyList,
	// sortListOutPut);
	// if (promotionOrder != null) {
	// listPromotionForOrderChange.add(promotionOrder);
	// }
	// }
	// }
	// }
	// }
	//
	// if (listPromotionForOrderChange.size() > 0) {
	// // neu co doi CTKM cho don hang
	// listProductPromotionsale.add(listPromotionForOrderChange.get(0));
	// }
	//
	// if (listPromotionForOrderChange.size() > 0) {
	// // neu co doi CTKM cho don hang
	// orderView.listPromotionForOrderChange = listPromotionForOrderChange;
	// }
	// }

	/**
	 * Lay danh sach feedback can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:47:59 21-05-2015
	 * @return: void
	 * @throws:
	 * @param page
	 * @param isGetTotalPage
	 * @param sortInfo
	 */
	public CustomerFeedBackDto getFeedBackList(String staffId,
			String customerId, String type, String status, String doneDate,
			int page, int isGetTotalPage, DMSSortInfo sortInfo, BaseFragment sender) {
		CustomerFeedBackDto dto = null;
		FEED_BACK_TABLE feedback = new FEED_BACK_TABLE(mDB);
		dto = feedback.getFeedBackList(staffId, customerId, type, status,
				doneDate, page, isGetTotalPage, sortInfo, sender);
		for (FeedBackDTO follow : dto.arrItem) {
			follow.setLstAttach(feedback.getAttachInfoFeedback(follow.getFeedBackId()));
		}
		return dto;
	}

	/**
	 * postFeedBack
	 *
	 * @author: TamPQ
	 * @param String
	 * @param String
	 * @return: void
	 * @throws:
	 */
	public long postFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				FEED_BACK_TABLE feedbackTable = new FEED_BACK_TABLE(mDB);
				returnCode = feedbackTable.postFeedBack(dto);
				mDB.setTransactionSuccessful();
			} catch (Exception e) {
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * postFeedBack
	 *
	 * @author: TamPQ
	 * @param lstFeedbackStaffCustomerDTO
	 * @param lstFeedbackStaffDTO
	 * @param lstMediaAttach
	 * @param String
	 * @param String
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public boolean gsnppPostFeedBack(FeedBackDTO feedbackDTO,
			ArrayList<FeedbackStaffDTO> lstFeedbackStaffDTO,
			ArrayList<FeedbackStaffCustomerDTO> lstFeedbackStaffCustomerDTO, ArrayList<MediaItemDTO> lstMediaAttach) throws Exception {
		long returnCode = -1;
		boolean insertSuccess = true;
		if (feedbackDTO != null) {
			try {
				mDB.beginTransaction();
				TABLE_ID tableId = new TABLE_ID(mDB);
				long feedbackDtlId = tableId.getMaxIdTime(FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_TABLE);
				long fbackStaffCusId = tableId.getMaxIdTime(FEEDBACK_STAFF_CUSTOMER_TABLE.FEEDBACK_STAFF_CUSTOMER_TABLE);

				FEEDBACK_TABLE feedbackTable = new FEEDBACK_TABLE(mDB);
				FEEDBACK_STAFF_TABLE feedbackStaffTable = new FEEDBACK_STAFF_TABLE(mDB);
				FEEDBACK_STAFF_CUSTOMER_TABLE feedbackStaffCustomerTable = new FEEDBACK_STAFF_CUSTOMER_TABLE(mDB);
				MEDIA_ITEM_TABLE tableMediaItem = new MEDIA_ITEM_TABLE(mDB);
				long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
				returnCode = feedbackTable.gsnppPostFeedBack(feedbackDTO);
				if (returnCode == -1) {
					insertSuccess = false;
				}
				if (insertSuccess) {
					for (int i = 0, size = lstFeedbackStaffDTO.size(); i < size; i++) {
						FeedbackStaffDTO item = lstFeedbackStaffDTO.get(i);
						item.feedBackId = feedbackDTO.getFeedBackId();
						item.feedBackStaffId = feedbackDtlId++;
						returnCode = feedbackStaffTable.insertFeedbackStaff(item);
						if (returnCode == -1) {
							insertSuccess = false;
							break;
						}
					}
				}
				if (insertSuccess) {
					for (int i = 0, size = lstFeedbackStaffCustomerDTO.size(); i < size; i++) {
						FeedbackStaffCustomerDTO item = lstFeedbackStaffCustomerDTO.get(i);
						for(int j = 0, s = lstFeedbackStaffDTO.size(); j < s; j++){
							FeedbackStaffDTO temp = lstFeedbackStaffDTO.get(j);
							if(item.staffId == temp.staffId){
								item.feedBackStaffId = temp.feedBackStaffId;
								break;
							}
						}
						item.feedBackStaffCustomerId = fbackStaffCusId++;
						returnCode = feedbackStaffCustomerTable.insertFeedbackStaffCustomer(item);
						if (returnCode == -1) {
							insertSuccess = false;
							break;
						}
					}
				}
				if (insertSuccess) {
					long mediaId = tableId.getMaxIdTime(MEDIA_ITEM_TABLE.TABLE_MEDIA_ITEM);
					for (MediaItemDTO mediaDTO : lstMediaAttach) {
						mediaDTO.id = mediaId++;
						mediaDTO.objectId = feedbackDTO.getFeedBackId();
						mediaDTO.cycleId = cycleId;
						returnCode = tableMediaItem.insert(mediaDTO);
						if (returnCode == -1) {
							insertSuccess = false;
							break;
						}
					}
				}

				if (insertSuccess) {
					mDB.setTransactionSuccessful();
				}
			}  finally {
				mDB.endTransaction();
			}
		}
		return insertSuccess;
	}

	/**
	 * Lay action cho phep dat hang tu xa o ngay hien tai cau staff, customer
	 * tuong ung
	 *
	 * @author: BangHN
	 * @return
	 * @return: ActionLogDTO
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ActionLogDTO getActionLogExceptionOrderDate(String staffId,
			String customerId, String shopId) {
		ACTION_LOG_TABLE actionTable = new ACTION_LOG_TABLE(mDB);
		return actionTable.getActionLogExceptionOrderDate(staffId, customerId,
				shopId);
	}

	/**
	 * Insert mot action log to db local
	 *
	 * @author : BangHN since : 1.0
	 */
	public long insertActionLog(ActionLogDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				TABLE_ID tableId = new TABLE_ID(mDB);
				dto.id = tableId.getMaxIdTime(ACTION_LOG_TABLE.TABLE_NAME);
				ACTION_LOG_TABLE actionTable = new ACTION_LOG_TABLE(mDB);
				returnCode = actionTable.insert(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	// bao cao tien do CTTB chi tiet NVBH ngay //
	public ReportDisplayProgressDetailDayDTO getReportDisplayProgressDetailDay(
			Bundle data) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getProgReportProDispDetailSaleDTO(data);
	}

	/**
	 * delete actoin log khi remove exception order dayInOrder
	 *
	 * @author : BangHN since : 1.0
	 */
	public long deleteActionLogWhenRemoveExceptionOrderDate(ActionLogDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				ACTION_LOG_TABLE actionTable = new ACTION_LOG_TABLE(mDB);
				returnCode = actionTable
						.deleteActionLogWhenRemoveExceptionOrderDate(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	public long updateActionLogToDB(ActionLogDTO dto) {
		long returnCode = -1;
		ACTION_LOG_TABLE table = new ACTION_LOG_TABLE(mDB);
		try {
			mDB.beginTransaction();
			returnCode = table.updateVisited(dto);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateActionLogToDB", "fail", e);
		} finally {
			mDB.endTransaction();
		}
		return returnCode;
	}

	/**
	 *
	 * get general statistics report info
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: GeneralStatisticsInfoViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public GeneralStatisticsInfoViewDTO getGeneralStatisticsReportInfo(
			String staffId, String shopId) throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		AP_PARAM_TABLE apParamTable = new AP_PARAM_TABLE(mDB);
		LatLng shopPosition = shopTable.getPositionOfShop(shopId);
		GeneralStatisticsInfoViewDTO vt = staff.getGeneralStatisticsInfo(
				staffId, shopId, shopPosition);
		Bundle ext = new Bundle();
		ext.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		List<ApParamDTO> paramList = apParamTable
				.getListParamForTakeAttendanceSalePerson(ext);
		vt.listInfo = paramList;
		vt.shopPosition = shopPosition;
		return vt;
	}

	/**
	 *
	 * get list note for general statistics view
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param shopId
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO getListNoteForGeneralStaticsView(String staffId,
																String shopId) {
		FEED_BACK_TABLE feedBack = new FEED_BACK_TABLE(mDB);
		ListNoteInfoViewDTO listNote = null;
		try {
			listNote = feedBack.getListNote(staffId, shopId, " limit 0,2");
			return listNote;
		} catch (Exception e) {
			MyLog.e("getListNoteForGeneralStaticsView", "fail", e);
			return null;
		}
	}

	/**
	 *
	 * update note infor to database
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param shopId
	 * @param noteUpdate
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO updateNoteInfoToDB(String staffId,
			String shopId, NoteInfoDTO noteUpdate) {
		ListNoteInfoViewDTO listNote = new ListNoteInfoViewDTO();
		FEED_BACK_TABLE feedBack = new FEED_BACK_TABLE(mDB);
		try {
			mDB.beginTransaction();
			listNote = feedBack.updateNoteInfo(staffId, shopId, noteUpdate);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateNoteInfoToDB", "fail", e);
			listNote = null;
		} finally {
			mDB.endTransaction();
		}
		return listNote;
	}

	/**
	 *
	 * postNote
	 *
	 * @author: TamPQ
	 * @param ext
	 * @return
	 * @return: ToDoTaskDTO
	 * @throws:
	 */
	public long postNote(ToDoTaskDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			mDB.beginTransaction();
			try {
				TODO_TASK todoTaskTable = new TODO_TASK(mDB);
				returnCode = todoTaskTable.postNote(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * getCustomerList
	 *
	 * @author: TamPQ
	 * @param sortInfo
	 * @param ext
	 * @return
	 * @return: CustomerListDTO
	 * @throws:
	 */
	public CustomerListDTO getCustomerList(int staffId, int shopId,
										   String name, String code, String address, String visit_plan,
										   boolean isGetWrongPlan, int page, int isGetTotalPage, DMSSortInfo sortInfo) throws Exception {
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		CustomerListDTO dto = custommerTable.getCustomerList(staffId, shopId, name, code,
				visit_plan, isGetWrongPlan, page, isGetTotalPage, false,
				sortInfo);
		return dto;
	}

	/**
	 * Lay danh sach khach hang trong mot tuyen
	 *
	 * @author : BangHN since : 1.0
	 */
	public CustomerListDTO getCustomerListForRoute(int staffId, int shopId,
			String visitPlan, boolean isGetWrongPlan, boolean isFromRouteView) throws Exception {
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		CustomerListDTO dto = custommerTable.getCustomerList(staffId, shopId, null, null,
					visitPlan, isGetWrongPlan, -1, 1, isFromRouteView, null);
		return dto;
	}

	/**
	 * Lay danh sach khach hang trong mot tuyen cua ban do TBHV
	 *
	 * @author : BangHN since : 1.0
	 */
	public CustomerListDTO getTBHVCustomerInVisitPlan(int staffId, int shopId,
			String visitPlan, boolean isGetWrongPlan) {
		CustomerListDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			dto = custommerTable.getTBHVListCustomer(staffId, shopId, null,
					null, null, visitPlan, isGetWrongPlan, -1, 1);
		} catch (Exception e) {
		}
		return dto;
	}

	public long updateDoneDateFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			mDB.beginTransaction();
			try {
				FEEDBACK_STAFF_TABLE feedbackTable = new FEEDBACK_STAFF_TABLE(mDB);
				returnCode = feedbackTable.updateDoneDateFeedBack(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	public long deleteFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				FEED_BACK_TABLE feedbackTable = new FEED_BACK_TABLE(mDB);
				returnCode = feedbackTable.delete(dto);
				mDB.setTransactionSuccessful();
			} catch (Exception e) {
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	public long updateDeleteFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				FEED_BACK_TABLE feedbackTable = new FEED_BACK_TABLE(mDB);
				returnCode = feedbackTable.updateDeleteFeedBack(dto);
				mDB.setTransactionSuccessful();
			} catch (Exception e) {
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Cap nhat truong is send cua 1 order
	 *
	 * @author: PhucNT
	 * @param order
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean updateSentOrder(SaleOrderDTO order) {
		boolean result = false;
		try {
			mDB.beginTransaction();
			SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
			saleOrderTable.updateSentOrder(order);
			mDB.setTransactionSuccessful();
			result = true;
		} catch (Exception e) {
			result = false;
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	/**
	 * Lay ds san pham khuyen mai cua 1 don hang co san
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 *            : id cua don hang
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @return
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ListSaleOrderDTO getPromotionProductsForEdit(long orderId,
			String orderType, long customerId, String shopId, String staffId) throws Exception {
		ListSaleOrderDTO list = new ListSaleOrderDTO();

		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);
		List<OrderDetailViewDTO> listProduct = orderDetailTable
				.getPromotionProductsForEdit(Long.toString(orderId), orderType,customerId,shopId,staffId);

		list.listData.addAll(listProduct);
		return list;
	}

	/**
	 * Lay ds san pham khuyen mai cua 1 don hang co san
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 *            : id cua don hang
	 * @return
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ArrayList<SaleOrderPromotionDTO> getQuantityReceivedForEdit(
			long orderId) {
		ArrayList<SaleOrderPromotionDTO> listSaleOrderPromotionDTO = new ArrayList<SaleOrderPromotionDTO>();
		try {
			SALE_ORDER_PROMOTION_TABLE saleOrderPromotionTable = new SALE_ORDER_PROMOTION_TABLE(
					mDB);
			listSaleOrderPromotionDTO = saleOrderPromotionTable
					.getSaleOrderPromotionBySaleOrderID(orderId);
		} catch (Exception e) {
			MyLog.e("getQuantityReceivedForEdit", "fail", e);
		}

		return listSaleOrderPromotionDTO;
	}

	/**
	 * Lay ds san pham khuyen mai cua 1 don hang co san
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 *            : id cua don hang
	 * @return
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ArrayList<SaleOrderPromotionDTO> getPOQuantityReceived(
			long poCustomerId) {
		ArrayList<SaleOrderPromotionDTO> listSaleOrderPromotionDTO = new ArrayList<SaleOrderPromotionDTO>();
		try {
			PO_PROMOTION_TABLE saleOrderPromotionTable = new PO_PROMOTION_TABLE(
					mDB);
			listSaleOrderPromotionDTO = saleOrderPromotionTable
					.getPoCustomerPromotionByPoCustomerId(poCustomerId);
		} catch (Exception e) {
			MyLog.e("getPOQuantityReceived", "fail", e);
		}

		return listSaleOrderPromotionDTO;
	}

	/**
	 * Lay ds san pham khuyen mai cua 1 don hang PO
	 *
	 * @author: DungNX3
	 * @param poCustomerId
	 *            : id cua don hang
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @return
	 * @return: ListSaleOrderDTO
	 * @throws:
	 */
	public ListSaleOrderDTO getPromotionProductsPO(long poCustomerId,
			String orderType, long customerId, String shopId, String staffId) throws Exception {
		ListSaleOrderDTO list = new ListSaleOrderDTO();
		PO_CUSTOMER_DETAIL_TABLE poDetailTable = new PO_CUSTOMER_DETAIL_TABLE(
				mDB);
		List<OrderDetailViewDTO> listProduct = poDetailTable
				.getPromotionProductsPO(Long.toString(poCustomerId), orderType,customerId,shopId,staffId);
		list.listData.addAll(listProduct);

		return list;
	}

	/**
	 * Lay danh sach khach hang tham gia chuong trinh khuyen mai
	 *
	 * @author: ThanhNN8
	 * @param extPage
	 * @param displayProgrameCode
	 * @return
	 * @return: List<CustomerAttentProgrameDTO>
	 * @throws:
	 */
	public ListCustomerAttentProgrameDTO getListCustomerAttentPrograme(
			String extPage, String displayProgrameCode, long displayProgrameId,
			String customer_code, String customer_name, int staffId,
			boolean checkLoadMore) throws Exception{
		// TODO Auto-generated method stub

		CUSTOMER_DISPLAY_PROGRAME_TABLE customerDisplayTable = new CUSTOMER_DISPLAY_PROGRAME_TABLE(
				mDB);
		ListCustomerAttentProgrameDTO result = customerDisplayTable
				.getListCustomerAttentPrograme(extPage, displayProgrameCode,
						displayProgrameId, customer_code, customer_name,
						staffId, checkLoadMore);
		return result;
	}

	/**
	 * Lay ds log can de thuc hien retry
	 *
	 * @author: TruongHN
	 * @return: ArrayList<LogDTO>
	 * @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheck() {
		ArrayList<LogDTO> result = new ArrayList<LogDTO>();
		if (mDB != null && mDB.isOpen()) {
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			result = logTable.getLogNeedToCheck();
		}
		return result;
	}

	/**
	 * Lay ds log can de thuc hien retry su dung truoc khi sync du lieu tu
	 * server ve
	 *
	 * @author: TruongHN
	 * @return: ArrayList<LogDTO>
	 * @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckBeforeSync() {
		ArrayList<LogDTO> result = new ArrayList<LogDTO>();
		if (mDB != null && mDB.isOpen()) {
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			result = logTable.getLogNeedToCheckBeforeSync();
		}
		return result;
	}

	/**
	 * Lay ds log can de thuc hien retry su dung truoc khi sync du lieu tu
	 * server ve (khi login)
	 *
	 * @author: TruongHN
	 * @return: ArrayList<LogDTO>
	 * @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckForLogin() {
		ArrayList<LogDTO> result = new ArrayList<LogDTO>();
		if (mDB != null && mDB.isOpen()) {
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			result = logTable.getLogNeedToCheckForLogin();
		}
		return result;
	}

	/**
	 * Lay so factor default (dung cho cac truong hop generate id khi them moi)
	 *
	 * @author: TruongHN
	 * @return: long
	 * @throws:
	 */
	public long getFactorValue() {
		// TABLE_ID table = new TABLE_ID(mDB);
		// return table.getFactorValue();
		long factor = 0;
		AP_PARAM_TABLE apTable = new AP_PARAM_TABLE(mDB);
		ArrayList<ApParamDTO> result = apTable.getConstantsApp();

		for (int i = 0, size = result.size(); i < size; i++) {
			if ("FACTOR".equals(result.get(i).getApParamType())) {
				factor = Long.valueOf(result.get(i).getApParamCode());
				break;
			}
		}
		return factor;
	}

	/**
	 * Get nhung constant dinh nghia cho app "FACTOR", "TIME_TEST_ORDER",
	 * "RADIUS_OF_POSITION", "TIME_TRIG_POSITION"
	 *
	 * @author banghn
	 */
	public ArrayList<ApParamDTO> getAppDefineConstant() {
		AP_PARAM_TABLE apTable = new AP_PARAM_TABLE(mDB);
		ArrayList<ApParamDTO> result = apTable.getConstantsApp();
		return result;
	}

	/**
	 * lay param AllowResetLocation
	 *
	 * @author DungNX
	 */
	public ApParamDTO getAllowResetLocation(Bundle ext) {
		AP_PARAM_TABLE apTable = new AP_PARAM_TABLE(mDB);
		ApParamDTO result = apTable.getAllowResetLocation(ext);
		return result;
	}

	/**
	 * Lay so factor default (dung cho cac truong hop generate id khi them moi)
	 *
	 * @author: TruongHN
	 * @return: long
	 * @throws:
	 */
	public long getFactorValue2() {
		TABLE_ID table = new TABLE_ID(mDB);
		long res = 1;
		try {
			mDB.beginTransaction();
			res = table.getFactorValue();
			mDB.setTransactionSuccessful();
		} catch (Exception ex) {
		} finally {
			mDB.endTransaction();
		}

		return res;
	}

	/**
	 *
	 * lay danh sach chuong trinh lien quan toi san pham
	 *
	 * @author: HaiTC3
	 * @param orderType
	 * @param dto
	 * @return
	 * @return: List<ProgrameForProductDTO>
	 * @throws:
	 */
	public List<ProgrameForProductDTO> getListProgrameForProduct(
			String shop_id, String staff_id, String customer_id,
			String customerTypeId, String orderType) throws Exception {
		// sale order detail - lay ds order detail
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);
		List<ProgrameForProductDTO> vt = saleOrderDetailTable
				.getListProgrameForProduct(shop_id, staff_id, customer_id,
						customerTypeId, orderType);
		return vt;

	}

	/**
	 * update Number Remain Product
	 *
	 * @author: PhucNT
	 * @param ArrayList
	 *            <SaleOrderDTO>
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	private boolean updateNumberRemainProduct(
			ArrayList<CustomerStockHistoryDTO> order) {

		boolean checkSuccess = true;
		try {
			// mDB.beginTransaction();
			CUSTOMER_STOCK_HISTORY_TABLE cusStockHistory = new CUSTOMER_STOCK_HISTORY_TABLE(
					SQLUtils.getInstance().getmDB());
			for (int i = 0; i < order.size(); i++) {
				CustomerStockHistoryDTO s = order.get(i);
				checkSuccess = cusStockHistory.insertOrUpdate(s) >= 0 ? true
						: false;
				if (!checkSuccess) {
					break;
				}
			}
			// mDB.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			checkSuccess = false;
		} finally {
			// mDB.endTransaction();
		}

		return checkSuccess;
	}

	/**
	 * Lay ds param by type
	 *
	 * @author: TruongHN
	 * @param code
	 * @return: ArrayList<ApParamDTO>
	 * @throws:
	 */
	public ArrayList<ApParamDTO> getListParaPriorityByType(String code) {
		AP_PARAM_TABLE apTable = new AP_PARAM_TABLE(mDB);
		return apTable.getPriorityByType(code);
	}

	/**
	 * Tinh sku cua khach hang trong thang: So luong san pham khach hang mua
	 * trong thang
	 *
	 * @author : BangHN since : 1.0
	 */
	public int getSKUOfCustomerInMonth(String customerId, String shopId,
			int sysCalUnApproved) throws Exception {
		SALE_ORDER_TABLE saleOrder = new SALE_ORDER_TABLE(mDB);
		return saleOrder.getSKUOfCustomerInMonth(customerId, shopId,
				sysCalUnApproved);
	}

	/**
	 * getGsnppCustomerList
	 *
	 * @author: TrungHQM
	 * @param ext
	 * @return
	 * @return: SupervisorCustomerListDTO
	 * @throws:
	 */
	public SupervisorCustomerListDTO getGsnppCustomerList(int staffId,
														  String name, String code, String address, String visit_plan,
														  int page, int isGetTotalPage) {
		SupervisorCustomerListDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			if (mDB != null) {
				dto = custommerTable.getGsnppCustomerList(staffId, name, code,
						address, visit_plan, page, isGetTotalPage);
			}

		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 * So lan mua cua khach hang trong thang: So don hang trong thang
	 *
	 * @author : BangHN since : 1.0
	 */
	public int getNumberOrdersInMonth(String customerId, String shopId)
			throws Exception {
		SALE_ORDER_TABLE saleOrder = new SALE_ORDER_TABLE(mDB);
		return saleOrder.getNumberOrdersInMonth(customerId, shopId);
	}

	/**
	 * doanh so cua khach hang dat duoc trong thang
	 *
	 * @author : BangHN since : 1.0
	 */
	public ArrayList<Double> getAverageSalesInMonth(String customerId,
			String shopId, int sysCalUnApproved, int sysCurrencyDivide, CustomerInfoDTO customerInfoDTO) throws Exception {
		SALE_ORDER_TABLE saleOrder = new SALE_ORDER_TABLE(mDB);
		return saleOrder.getAverageSalesInMonth2(customerId, shopId,
				sysCalUnApproved, sysCurrencyDivide, customerInfoDTO);
	}

	/**
	 * Lay doanh so cua khach hang trong 3 thang gan fullDate
	 *
	 * @author : BangHN since : 1.0
	 * @param shopId
	 */
	public ArrayList<SaleInMonthDTO> getAverageSalesIn3MonthAgo(
			String customerId, String shopId, int sysCalUnApproved, int sysCurrencyDivide)
			throws Exception {
		SALE_ORDER_TABLE saleOrder = new SALE_ORDER_TABLE(mDB);
		return saleOrder.getAverageSalesIn3MonthAgo(customerId, shopId,
				sysCalUnApproved, sysCurrencyDivide);
	}

	/**
	 * Lay bai gioi thieu cua san pham
	 *
	 * @author: ThanhNN8
	 * @param productId
	 * @return
	 * @return: IntroduceProductDTO
	 * @throws:
	 */
	public IntroduceProductDTO getIntroduceProduct(String productId) {
		// TODO Auto-generated method stub
		try {
			MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
			PRODUCT_INTRODUCTION_TABLE introTable = new PRODUCT_INTRODUCTION_TABLE(
					mDB);
			IntroduceProductDTO result = new IntroduceProductDTO();
			// get html content for introduce
			String htmlContent = introTable.getContentIntroduction(productId);
			result.setHtmlContextIntroduce(htmlContent);
			// get all image for product
			List<MediaItemDTO> listMedia = mediaItemTable
					.getListMediaOfProduct(productId);
			if (listMedia != null) {
				result.setListMedia(listMedia);
			}
			return result;
		} catch (Exception e) {
			MyLog.e("getIntroduceProduct", "fail", e);
			return null;
		}
	}

	/**
	 * get List Sale Road map Supervisor
	 *
	 * @author: TamPQ
	 * @param sortInfo
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public GsnppRouteSupervisionDTO getGsnppRouteSupervision(int staffId,
			String today, String shopId, DMSSortInfo sortInfo) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		GsnppRouteSupervisionDTO dto = null;
		dto = staffTable.getGsnppRouteSupervision(staffId, today, shopId, sortInfo);
		return dto;
	}

	// bao cao tien do CTTB chi tiet NVBH ngay //
	public TBHVReportDisplayProgressDetailDayViewDTO getTBHVReportDisplayProgressDetailDayDTO(
			int staffId, String displayProgrameId, String displayProgrameCode,
			String displayProgrameLevel, int checkAll, int page) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getTBHVReportDisplayProgressDetailDayDTO(staffId,
				displayProgrameId, displayProgrameCode, displayProgrameLevel,
				checkAll, page);
	}

	/**
	 * update thong tin vi tri cua khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	public long updateCustomerLocation(CustomerDTO customer) {
		long update = -1;
		CUSTOMER_TABLE cusTab = new CUSTOMER_TABLE(mDB);
		try {
			mDB.beginTransaction();
			update = cusTab.updateLocation(customer);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateCustomerLocation", "fail", e);
		} finally {
			mDB.endTransaction();
		}
		return update;
	}

	/**
	 * Lay danh sach loai chuong trinh cho chuong trinh trung bay
	 *
	 * @author: ThanhNN8
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public ArrayList<DisplayProgrameItemDTO> getListTypeDisplayPrograme()
			throws Exception {
		AP_PARAM_TABLE apParamTable = new AP_PARAM_TABLE(mDB);
		return apParamTable.getListTypeDisplayPrograme();
	}

	/**
	 * Lay danh sach nganh hang cho chuong trinh trung bay
	 *
	 * @author: ThanhNN8
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public ArrayList<DisplayProgrameItemDTO> getListDepartDisplayPrograme()
			throws Exception {
		// TODO Auto-generated method stub
		PRODUCT_INFO_TABLE productInfoTable = new PRODUCT_INFO_TABLE(mDB);
		return productInfoTable.getListDepartDisplayPrograme();
	}

	public AccSaleProgReportDTO getAccSaleProgReport(int staffId, String shopId, int sysSaleRoute) throws Exception {
		RPT_STAFF_SALE_TABLE staffTable = new RPT_STAFF_SALE_TABLE(mDB);
		return staffTable.getAccSaleProgReportDTO(staffId, shopId, sysSaleRoute);
	}

	public SupervisorReportDisplayProgressDTO getDisProComProReport(Bundle data) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE table = new RPT_DISPLAY_PROGRAME_TABLE(mDB);
		return table.getDisProComProReportDTO(data);
	}

	public ProgressDateReportDTO getProgressDateReport(Bundle bundle) throws Exception{
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getProgressDateReport(bundle);
	}

	public ProgressDateDetailReportDTO getProgressDateDetailReport(Bundle bundle) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getProgressDateDetailReport(bundle);
	}

	 /**
	 * Lay bao cao ngay cua role quan li
	 * @author: Tuanlt11
	 * @param staffOwnerId
	 * @return
	 * @throws Exception
	 * @return: TBHVProgressDateReportDTO
	 * @throws:
	*/
	public TBHVProgressDateReportDTO getTBHVProgressDateReport(
			Bundle bundle) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getTBHVProgressDateReport(bundle);
	}

	public TBHVCustomerNotPSDSReportDTO getTBHVNotPSDSReport(String staffOwnerId) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getTBHVNotPSDSReport(staffOwnerId);
	}

	 /**
	 * Lay bao cao tien do ngay cua quan li
	 * @author: Tuanlt11
	 * @param shopId
	 * @param staffOwnerId
	 * @return
	 * @throws Exception
	 * @return: TBHVProgressDateReportDTO
	 * @throws:
	*/
	public TBHVProgressDateReportDTO getTBHVProgressDateDetailReport(
			String shopId, String staffOwnerId, int sysCurrencyDevide) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getTBHVProgressDateDetailReport(shopId, staffOwnerId,sysCurrencyDevide);
	}

	public VisitDTO getListVisit(int staffId, int customerId) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListVisit(staffId, customerId);
	}

	public ManagerEquipmentDTO getListEquipment(int staffId, String shopId)
			throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListEquipment(staffId, shopId);
	}

	public TBHVManagerEquipmentDTO getListEquipmentTBHV(int parentShopId) {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListEquipmentTBHV(parentShopId);
	}

	public TBHVManagerEquipmentDTO getTBHVListEquipment(String shopId) {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListEquipment(shopId);
	}

	// hoan
	public TBHVDisProComProgReportDTO getTBHVDisProComProReport(Bundle data)
			throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE table = new RPT_DISPLAY_PROGRAME_TABLE(mDB);
		return table.getTBHVDisProComProReportDTO(data);

	}

	public TBHVDisProComProgReportNPPDTO getTBHVDisProComProReportNPP(
			Bundle data) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE table = new RPT_DISPLAY_PROGRAME_TABLE(mDB);
		return table.getTBHVDisProComProReportNPP(data);

	}

	public CabinetStaffDTO getCabinetStaff(int shopId, int staffId, int isAll,
										   String page) {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getCabinetStaff(shopId, staffId, isAll, page);
	}

	public int getCountCabinetStaff(int shopId, int staffId, int isAll) {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getCountCabinetStaff(shopId, staffId, isAll);
	}

	public CustomerNotPsdsInMonthReportDTO getCustomerNotPsdsInMonthReport(
			int shopId, String staffOwnerId, String visit_plan, String staffId,
			int page, int sysCalUnApproved, String listStaff,
			DMSSortInfo sortInfo) throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.getCustomerNotPsdsInMonthReport(shopId, staffOwnerId,
				visit_plan, staffId, page, listStaff, sysCalUnApproved, sortInfo);
	}

	public int getCountCustomerNotPsdsInMonthReport(int shopId,
			String staffOwnerId, String visit_plan, String staffId)
			throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.getCountCustomerNotPsdsInMonthReport(shopId, staffOwnerId,
				visit_plan, staffId);
	}

	public ListStaffDTO getListNVBH(int shopId, String staffOwnerId)
			throws Exception {
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		return table.getListNVBH(shopId, staffOwnerId);
	}

	public ListStaffDTO getListNVBHNotPSDS(int shopId, String staffOwnerId)
			throws Exception {
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		return table.getListNVBHForGSNPP(shopId, staffOwnerId, false);
	}

	/**
	 * lay danh sach NVBH duoi quyen
	 *
	 * @author: hoanpd1
	 * @since: 10:49:58 05-03-2015
	 * @return: ListStaffDTO
	 * @throws:
	 * @param shopId
	 * @param staffOwnerId
	 * @param isHaveAll
	 * @return
	 * @throws Exception
	 */
	public ListStaffDTO getListNVBHOrderCode(int shopId, String staffOwnerId,
			boolean isHaveAll) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListNVBHOrderCode(shopId, staffOwnerId, isHaveAll);
	}

	/**
	 *
	 * bao cao tien do ban MHTT den ngay
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ProgressReportSalesFocusDTO
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ProgressReportSalesFocusDTO getProgReportSalesFocus(Bundle ext) throws Exception {
		RPT_STAFF_SALE_TABLE focusTable = new RPT_STAFF_SALE_TABLE();
		return focusTable.getProgReportSalesFocus(ext);
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int updateClientThumbnailUrl(MediaItemDTO dto) {
		// TODO Auto-generated method stub
		int result = 0;
		try {
			mDB.beginTransaction();
			MEDIA_ITEM_TABLE mediaTable = new MEDIA_ITEM_TABLE(mDB);
			result = mediaTable
					.updateClientThumbnailUrl(dto.sdCardPath, dto.id);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			result = -1;
			MyLog.e("updateClientThumbnailUrl", "fail", e);
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	public DisProComProgReportDTO getStaffDisProComProReportDTO(int staffId,
			int shopId, int proId) {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getStaffDisProComProReportDTO(staffId, shopId, proId);
	}

	// bao cao tien do CTTB chi tiet NVBH ngay //
	public ProgReportProDispDetailSaleDTO getProgReportProDispDetailSaleDTO(
			int staffId, String displayProgrameCode, int checkAll, String page) {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getProgReportProDispDetailSaleDTO(staffId,
				displayProgrameCode, checkAll, page);
	}

	/**
	 * ds huan luyen tich luy ngay
	 *
	 * @author: TampQ
	 * @param dto
	 * @return
	 * @return: int
	 * @throws:
	 */
	public GsnppTrainingResultAccReportDTO getAccTrainResultReportDTO(
			int staffId, String shopId) {
		return new TRAINING_PLAN_DETAIL_TABLE(mDB).getAccTrainResultReportDTO(
				staffId, shopId);
	}

	/**
	 * ds huan luyen ngay
	 *
	 * @author: TampQ
	 * @param dto
	 * @return
	 * @return: int
	 * @throws:
	 */

	public GSNPPTrainingResultDayReportDTO getGsnppTrainingResultDayReport(
			long staffTrainId, int shopId, int staffId) {
		return new RPT_SALE_RESULT_DETAIL_TABLE(mDB)
				.getGsnppTrainingResultDayReport(staffTrainId, shopId, staffId);
	}

	/**
	 * Danh sach hinh anh khach hang NVBH
	 *
	 * @author: HoanPD1
	 * @return: ImageListDTO
	 * @throws:
	 */
	public ImageListDTO getImageList(Bundle ext) throws Exception {
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_STAFF_ID,
				ext.getInt(IntentConstants.INTENT_STAFF_ID));
		bundle.putString(IntentConstants.INTENT_SHOP_ID,
				ext.getString(IntentConstants.INTENT_SHOP_ID));
		DISPLAY_SHOP_MAP_TABLE programeTable = new DISPLAY_SHOP_MAP_TABLE(mDB);
		DisplayProgrameModel listDisplay = new DisplayProgrameModel();
		if (ext.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE) > 0) {
			listDisplay = programeTable.getListDisplayProgrameImage(bundle);
		}
		ImageListDTO dto = custommerTable.getImageList(ext);
		if (listDisplay != null && listDisplay.getModelData() != null) {
			dto.listPrograme.addAll(listDisplay.getModelData());
		}
		return dto;
	}

	/**
	 * Insert mot media log to db local
	 *
	 * @author : ToanTT
	 */
	public long insertMediaLog(MediaLogDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				TABLE_ID tableId = new TABLE_ID(mDB);
				dto.id = tableId.getMaxIdTime(MEDIA_LOG_TABLE.TABLE_NAME);
				MEDIA_LOG_TABLE mediaLogTable = new MEDIA_LOG_TABLE(mDB);
				returnCode = mediaLogTable.insert(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * getSelectCustomerList
	 *
	 * @author: ToanTT
	 * @param ext
	 * @return
	 * @return: CustomerListDTO
	 * @throws:
	 */
	public CustomerListDTO getSelectCustomerList(String shopId, String staffId,
			double lat, double lng, int roleType) {
		CustomerListDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			if (mDB != null) {
				if (roleType == UserDTO.TYPE_STAFF) {
					dto = custommerTable.getSelectCustomerListNVBH(shopId,
							staffId, lat, lng);
				}
				if (roleType == UserDTO.TYPE_SUPERVISOR) {
					dto = custommerTable.getSelectCustomerListGSNPP(shopId,
							staffId, lat, lng);
				}
				if (roleType == UserDTO.TYPE_MANAGER) {
					dto = custommerTable.getSelectCustomerListTBHV(shopId,
							staffId, lat, lng);
				}
			}

		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 * update endtime media log to db local
	 *
	 * @author : ToanTT
	 */
	public long updateEndtimeMediaLog(MediaLogDTO dto) {
		long returnCode = -1;
		MEDIA_LOG_TABLE table = new MEDIA_LOG_TABLE(mDB);
		try {
			mDB.beginTransaction();
			returnCode = table.updateEndtime(dto);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateEndtimeMediaLog", "fail", e);
		} finally {
			mDB.endTransaction();
		}
		return returnCode;
	}

	/**
	 * Danh sach hinh anh khach hang cua GSNPP
	 * @author: hoanpd1
	 * @since: 15:53:48 13-04-2015
	 * @return: ImageListDTO
	 * @throws:
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public ImageListDTO getSupervisorImageList(Bundle bundle) throws Exception {
		// sale order detail - lay ds order detail
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		return custommerTable.getSupervisorImageList(bundle);
	}

	/**
	 * Danh sach hinh anh khach hang GSNPP
	 *
	 * @author: HoanPD1
	 * @return: ImageListDTO
	 * @throws:
	 */
	public ImageListDTO getImageListGSNPP(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		ImageListDTO dto = custommerTable.getImageList(ext);
		return dto;

	}

	// bao cao tien do CTTB chi tiet NVBH ngay //
	public TBHVProgReportProDispDetailSaleDTO getTBHVProgReportProDispDetailSaleDTO(
			int staffId, String displayProgrameCode,
			String displayProgrameLevel, int checkAll, int page) {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getTBHVProgReportProDispDetailSaleDTO(staffId,
				displayProgrameCode, displayProgrameLevel, checkAll, page);
	}

	/**
	 * Danh sach chuong trinh trung bay danh cho nhan vien giam sat
	 *
	 * @author: hoanpd1
	 * @since: 09:04:42 24-03-2015
	 * @return: DisplayProgrameModel
	 * @throws:
	 * @param ext
	 * @return
	 * @throws Exception
	 */
	public DisplayProgrameModel getListSuperVisorDisplayPrograme(Bundle ext)
			throws Exception {
		DISPLAY_PROGRAME_TABLE displayProgrameTable = new DISPLAY_PROGRAME_TABLE(mDB);
		return displayProgrameTable.getListSuperDisplayPrograme(ext);
	}

	/**
	 *
	 * Lay ds chuong tinh trung bay cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param ext
	 * @return
	 * @return: DisplayProgrameModel
	 * @throws:
	 */
	public TBHVDisplayProgrameModel getListTBHVDisplayPrograme(Bundle ext)
			throws Exception {

		DISPLAY_PROGRAME_TABLE displayProgrameTable = new DISPLAY_PROGRAME_TABLE(
				mDB);
		TBHVDisplayProgrameModel model = displayProgrameTable
				.getListTBHVDisplayPrograme(ext);
		return model;
	}

	/**
	 * Lay danh sach theo doi khac phuc cua GSNPP
	 *
	 * @author: ThanhNN8
	 * @return
	 * @return: FollowProblemDTO
	 * @throws Exception
	 * @throws:
	 */
	public FollowProblemDTO getListFollowProblem(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		FEED_BACK_TABLE feedBackTable = new FEED_BACK_TABLE(mDB);
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		FollowProblemDTO model = feedBackTable.getListProblemOfSuperVisor(data);
		for (FollowProblemItemDTO follow : model.list) {
			follow.lstAttach = feedBackTable.getAttachInfoFeedback(follow.feedBackId);
		}
		boolean checkCombobox = data.getBoolean(
				IntentConstants.INTENT_CHECK_COMBOBOX, false);
		if (checkCombobox) {
			ComboboxFollowProblemDTO comboboxDTO = new ComboboxFollowProblemDTO();
		/*	comboboxDTO.listNVBH = staffTable
					.getStaffCodeComboboxProblemNVBHOfSuperVisor();*/
			comboboxDTO.listNVBH = staffTable.getListStaffProblem(data);
			comboboxDTO.listStatus = feedBackTable
					.getComboboxProblemStatusOfSuperVisor();
			comboboxDTO.listTypeProblem = staffTable
					.getComboboxProblemTypeProblemOfSuperVisor();
			model.comboboxDTO = comboboxDTO;
		}
		return model;
	}

	/**
	 *
	 * Lay ds van de cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: FollowProblemDTO
	 * @throws:
	 */
	public TBHVFollowProblemDTO getTBHVListFollowProblem(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		TRAINING_SHOP_MANAGER_RESULT_TABLE feedBackTable = new TRAINING_SHOP_MANAGER_RESULT_TABLE(
				mDB);
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		TBHVFollowProblemDTO model = feedBackTable.getListProblemOfTBHV(data);

		boolean checkCombobox = data.getBoolean(
				IntentConstants.INTENT_CHECK_COMBOBOX, false);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		if (checkCombobox) {
			ComboboxFollowProblemDTO comboboxDTO = new ComboboxFollowProblemDTO();
			comboboxDTO.listNVBH = staffTable
					.getStaffCodeComboboxProblemSuperVisorOfTBHV(shopId);
			comboboxDTO.listStatus = feedBackTable
					.getComboboxProblemStatusOfTBHV();
			comboboxDTO.listTypeProblem = staffTable
					.getComboboxProblemTypeProblem(false);
			model.comboboxDTO = comboboxDTO;
		} else {
			model.comboboxDTO = null;
		}
		return model;
	}

	/**
	 * Lay danh sach combobox cho man hinh theo doi khac phuc
	 *
	 * @author: ThanhNN8
	 * @return
	 * @return: ComboboxFollowProblemDTO
	 * @throws Exception
	 * @throws:
	 */
	public ComboboxFollowProblemDTO getComboboxListFollowProblem()
			throws Exception {
		// TODO Auto-generated method stub
		FEED_BACK_TABLE feedBackTable = new FEED_BACK_TABLE(mDB);
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		ComboboxFollowProblemDTO model = new ComboboxFollowProblemDTO();
		model.listNVBH = staffTable
				.getStaffCodeComboboxProblemNVBHOfSuperVisor();
		model.listStatus = feedBackTable.getComboboxProblemStatusOfSuperVisor();
		return model;
	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: HieuNQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public GSNPPTrainingPlanDTO getGsnppTrainingPlan(int staffId, String shopId) {
		TRAINING_PLAN_DETAIL_TABLE table = new TRAINING_PLAN_DETAIL_TABLE(mDB);
		GSNPPTrainingPlanDTO dto = null;
		try {
			dto = table.getGsnppTrainingPlan(staffId, shopId);
		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 *
	 * lay thong tin danh gia cua NVBH
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ReviewsStaffViewDTO
	 * @throws:
	 */
	public ReviewsStaffViewDTO getReviewsInfoOfStaff(Bundle ext) {
		ReviewsStaffViewDTO reviewsData = new ReviewsStaffViewDTO();
		FEED_BACK_TABLE feedBackTable = new FEED_BACK_TABLE(mDB);
		try {
			reviewsData = feedBackTable.getReviewStaffView(ext);
		} catch (Exception e) {
			MyLog.e("getReviewsInfoOfStaff", "fail", e);
			return null;
		}
		return reviewsData;
	}

	/**
	 *
	 * cap nhat cac thong tin can bo xung cho training result va feedback (id va
	 * training_plan_detail_id)
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ReviewsStaffViewDTO
	 * @throws:
	 */
	public ReviewsStaffViewDTO updateIdForTrainingResult(String shopId,
			String supperStaffId, String staffId, ReviewsStaffViewDTO data) {
		TABLE_ID tableId = new TABLE_ID(mDB);

		if (data != null) {
			long feedBackMaxId = tableId
					.getMaxIdTime(FEED_BACK_TABLE.FEED_BACK_TABLE);
			long feedBackDetailMaxId = tableId
					.getMaxIdTime(FEEDBACK_DETAIL_TABLE.FEEDBACK_DETAIL_TABLE);
			try {
				// cap nhat thong tin feedback dto cho SKU vao bang FeedBack
				// table
				long feedbackSKUId = -1;
				if (data.feedBackSKU.feedBack != null) {
					if (data.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
						if (!StringUtil
								.isNullOrEmpty(data.feedBackSKU.feedBack.getContent())) {
							data.feedBackSKU.feedBack.setFeedBackId(feedBackMaxId);
							// data.feedBackSKU.feedBack.trainingPlanDetailId =
							// trainingPlanDetailId;
							feedbackSKUId = feedBackMaxId;
						}
					} else if (data.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_UPDATE) {
						data.feedBackSKU.feedBack.setUpdateDate(DateUtils.now());
						feedbackSKUId = data.feedBackSKU.feedBack.getFeedBackId();
					}
				}
				if (feedbackSKUId > 0) {
					// cap nhat thong tin sku (feedback detail)
					for (int i = 0, size = data.listSKU.size(); i < size; i++) {
						// tao moi training result
						if (data.listSKU.get(i).feedbackDetailId == -1
								&& data.listSKU.get(i).currentState == FeedBackDetailDTO.STATE_NEW_INSERT) {
							data.listSKU.get(i).feedbackDetailId = feedBackDetailMaxId;
							data.listSKU.get(i).feedbackId = feedbackSKUId;
							feedBackDetailMaxId++;
						}
					}
				}

				feedBackMaxId++;
				// cap nhat thong tin danh gia (feedback)
				for (int i = 0, size = data.listReviewsObject.size(); i < size; i++) {
					// tao moi record training result
					if (data.listReviewsObject.get(i).currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
						data.listReviewsObject.get(i).feedBack.setFeedBackId(feedBackMaxId);
						// data.listReviewsObject.get(i).feedBack.trainingPlanDetailId
						// = trainingPlanDetailId;
						feedBackMaxId++;
					}
				}
			} catch (Exception e) {
				MyLog.e("updateIdForTrainingResult", "fail", e);
			}
		}

		return data;
	}

	/**
	 *
	 * update maxid for training shop manager result
	 *
	 * @author: HaiTC3
	 * @param listTrainingReviews
	 * @return
	 * @return: List<TrainingShopManagerResultDTO>
	 * @throws:
	 */
	public List<FeedBackTBHVDTO> tbhvUpdateIdForTrainingResult(
			List<FeedBackTBHVDTO> listTrainingReviews) {
		TABLE_ID tableId = new TABLE_ID(mDB);
		long feedbackMaxId = tableId.getMaxIdTime(FEED_BACK_TABLE.FEED_BACK_TABLE);

		for (int i = 0, size = listTrainingReviews.size(); i < size; i++) {
			if (listTrainingReviews.get(i).currentState == FeedBackTBHVDTO.STATE_NEW_INSERT) {
				listTrainingReviews.get(i).feedBackBasic.setFeedBackId(feedbackMaxId++);
			}
		}
		return listTrainingReviews;
	}

	/**
	 * Kiem tra trang thai khach hang (ghe tham, ngoai tuyen, trong tuyen)
	 *
	 * @author: BANGHN
	 * @param shopId
	 * @return TRONG TUYEN GHE THAM: 1 TRONG TUYEN CHUA GHE THAM : 0 NGOAI TUYEN
	 *         : -1
	 */
	public int checkStateCutomerVisit(String customerId, String staffId,
			String shopId) {
		VISIT_PLAN_TABLE visitPlan = new VISIT_PLAN_TABLE(mDB);
		return visitPlan.checkStateCutomerVisit(customerId, staffId, shopId);
	}

	/**
	 * Lay thong tin album cua user
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param typeName
	 * @return
	 * @return: ListAlbumUserDTO
	 * @throws:
	 */
	public ListAlbumUserDTO getAlbumUserInfo(Bundle data) {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		// request lay danh sach hinh anh cua user
		ListAlbumUserDTO albumInfo = null;
		try {
			albumInfo = mediaItemTable.getAlbumUserInfo(customerId, shopId);
			albumInfo.visitType = SQLUtils.getInstance()
					.checkStateCutomerVisit(customerId, staffId, shopId);
			// request combobox
			boolean checkRequestCombobox = data.getBoolean(
					IntentConstants.INTENT_CHECK_COMBOBOX, true);
			if (checkRequestCombobox) {
				DISPLAY_PROGRAME_TABLE programeTable = new DISPLAY_PROGRAME_TABLE(
						mDB);
				DISPLAY_SHOP_MAP_TABLE programeShopMapTable = new DISPLAY_SHOP_MAP_TABLE(
						mDB);
				// DisplayProgrameModel getListDisplayProgrameImage(Bundle data)
				DisplayProgrameModel listDisplay = programeShopMapTable
						.getListDisplayProgrameImage(data);
				if (listDisplay != null && listDisplay.getModelData() != null) {
					List<DisplayProgrameDTO> listDPDTO = listDisplay
							.getModelData();
					ArrayList<DisplayProgrameItemDTO> listDPItemDTO = new ArrayList<DisplayProgrameItemDTO>();
					for (int i = 0, size = listDPDTO.size(); i < size; i++) {
						DisplayProgrameDTO displayProgrameDTO = listDPDTO
								.get(i);
						DisplayProgrameItemDTO displayProgrameItemDTO = new DisplayProgrameItemDTO();
						displayProgrameItemDTO.name = displayProgrameDTO.displayProgrameCode;
						displayProgrameItemDTO.fullName = displayProgrameDTO.displayProgrameCode;
						displayProgrameItemDTO.value = displayProgrameDTO.displayProgrameCode;
						listDPItemDTO.add(displayProgrameItemDTO);
					}
					albumInfo.setListDisplayPrograme(listDPItemDTO);
				} else {
					albumInfo.setListDisplayPrograme(null);
				}
				DisplayProgrameModel listPhotoDPPrograme = programeTable
						.getListDisplayProgrameWhenTakePhoto(data);
				if (listPhotoDPPrograme != null
						&& listPhotoDPPrograme.getModelData() != null) {
					albumInfo
							.setListPhotoDPrograme((ArrayList<DisplayProgrameDTO>) listPhotoDPPrograme
									.getModelData());
				}
			}

			// request hinh anh cho chuong trinh
			ArrayList<AlbumDTO> arrayListPrograme = mediaItemTable
					.getAlbumProgrameInfo("", customerId, "", "", shopId);
			if (arrayListPrograme != null && arrayListPrograme.size() > 0) {
				albumInfo.setListProgrameAlbum(arrayListPrograme);
			} else {
				albumInfo.setListProgrameAlbum(null);
			}

			if (albumInfo != null) {
				SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
				albumInfo.shopDistance = shopTable.getShopDistance(data
						.getString(IntentConstants.INTENT_SHOP_ID));
			}

			return albumInfo;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 *
	 * lay thong tin training plan detail id for NVBH
	 *
	 * @author: HaiTC3
	 * @param shop_id
	 * @param supperStaff_id
	 * @param staff_id
	 * @return
	 * @return: String
	 * @throws:
	 */
	public String getTrainingPlanDetailId(String shop_id,
			String supperStaff_id, String staff_id) {
		String trainingPlanDetailId = "";
		TRAINING_PLAN_DETAIL_TABLE trainingPlanDetail = new TRAINING_PLAN_DETAIL_TABLE(
				mDB);
		trainingPlanDetailId = trainingPlanDetail.getTrainingPlanDetailId(
				shop_id, supperStaff_id, staff_id);

		return trainingPlanDetailId;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @return
	 * @return: ArrayList<PhotoDTO>
	 * @throws:
	 */
	public ArrayList<PhotoDTO> getAlbumDetailUser(String customerId,
			String type, String numTop, String page, String shopId) {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		ArrayList<PhotoDTO> listPhoto = mediaItemTable.getAlbumDetailUser(
				customerId, type, numTop, page, shopId);
		return listPhoto;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @return
	 * @return: ArrayList<PhotoDTO>
	 * @throws:
	 */
	public ArrayList<PhotoDTO> getSupervisorAlbumDetailUser(String customerId,
			String type, String numTop, String page, String shopId) {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		ArrayList<PhotoDTO> listPhoto = mediaItemTable
				.getSupervisorAlbumDetailUser(customerId, type, numTop, page,
						shopId);
		return listPhoto;
	}

	/**
	 * get Staff Information
	 *
	 * @author: TamPQ
	 * @param shopId
	 * @param dto
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public StaffInfoDTO getStaffInformation(String staffId, int sysSaleRoute, int page,
											int isGetTotalPage, int isLoadSaleInMonth, String shopId) throws Exception {
		RPT_SALE_HISTORY rptSaleHistory = new RPT_SALE_HISTORY(mDB);
		StaffInfoDTO dto = null;
		dto = rptSaleHistory.getStaffInformation(staffId, sysSaleRoute, page, isGetTotalPage,
				isLoadSaleInMonth, shopId);
		return dto;
	}

	/**
	 * Cap nhat cac log co thoi gian khong hop le ve trang thai close
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	public void updateLogWithStateClose(ArrayList<LogDTO> listLog) {
		if (mDB != null && mDB.isOpen()) {
			try {
				mDB.beginTransaction();
				LOG_TABLE log = new LOG_TABLE(mDB);
				SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
				for (int i = 0, size = listLog.size(); i < size; i++) {
					LogDTO dto = (LogDTO) listLog.get(i);
					log.updateLogWithStateClose(dto);
					// neu la don hang thi cap nhat trang thai don hang la co
					// thoi gian khong hop le
					if (dto.tableType == LogDTO.TYPE_ORDER
							&& !StringUtil.isNullOrEmpty(dto.tableId)) {
						orderTable.updateState(dto.tableId,
								Integer.parseInt(LogDTO.STATE_INVALID_TIME));
					}
				}
				mDB.setTransactionSuccessful();
			} catch (Exception e) {
				MyLog.e("updateLogWithStateClose", "fail", e);
			} finally {
				mDB.endTransaction();
			}
		}
	}

	/**
	 *
	 * cap nhat thong tin danh gia NVBH toi DB Note: Nhung ham insert, delete,
	 * update da co transaction
	 *
	 * @author: HaiTC3
	 * @param reviewsInfoDTO
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int insertAndUpdateReviewsStaffDTO(ReviewsStaffViewDTO reviewsInfoDTO) {
		int kq = 0;
		try {
			mDB.beginTransaction();
			FEED_BACK_TABLE feedback = new FEED_BACK_TABLE(mDB);
			boolean checkSuccess = true;
			// insert feedback of SKU to DB
			if (reviewsInfoDTO.feedBackSKU != null) {
				if (reviewsInfoDTO.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
					// tao moi feedback SKU trong feedback
					checkSuccess = this
							.insertDTO(reviewsInfoDTO.feedBackSKU.feedBack) >= 0 ? true
							: false;
				} else if (reviewsInfoDTO.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_UPDATE) {
					// update feedback SKU trong feedback neu co it nhat 1 sku
					// trong feedback detail
					checkSuccess = feedback
							.updateContentFeedBack(reviewsInfoDTO.feedBackSKU.feedBack) > 0 ? true
							: false;
				} else if (reviewsInfoDTO.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_DELETED) {
					// bo xung xoa di feedback SKU trong feedback neu nhu ko con
					// SKU nao
					checkSuccess = this
							.deleteDTO(reviewsInfoDTO.feedBackSKU.feedBack) > 0 ? true
							: false;
				}
			}
			if (checkSuccess) {
				// insert list SKU to DB
				for (int i = 0, size = reviewsInfoDTO.listSKU.size(); i < size; i++) {
					FeedBackDetailDTO feedbackInfo = reviewsInfoDTO.listSKU
							.get(i);
					// chi them moi cac recode, nhung record da co thi khong cap
					// nhat gi ca
					if (feedbackInfo.currentState == FeedBackDetailDTO.STATE_NEW_INSERT) {
						checkSuccess = this.insertDTO(feedbackInfo) >= 0 ? true
								: false;
					} else if (feedbackInfo.currentState == FeedBackDetailDTO.STATE_DELETED) {
						// xoa record ra khoi bang feeback detail neu yeu cau
						// xoa
						// SKU nay
						checkSuccess = this.deleteDTO(feedbackInfo) > 0 ? true
								: false;
					}
				}
			}

			if (checkSuccess) {
				// insert reviews info to DB (3 loai feedback co type = 6-7-8)
				for (int i = 0, size = reviewsInfoDTO.listReviewsObject.size(); i < size; i++) {
					ReviewsObjectDTO reviewDTO = reviewsInfoDTO.listReviewsObject
							.get(i);
					// them moi cac record tao moi
					if (reviewDTO.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
						checkSuccess = this.insertDTO(reviewDTO.feedBack) >= 0 ? true
								: false;
						if (!checkSuccess) {
							break;
						}
					}
					// nhung record da co thi chi bo xung noi dung trong
					// feedback
					else if (reviewDTO.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_UPDATE) {
						checkSuccess = feedback
								.updateContentFeedBack(reviewDTO.feedBack) > 0 ? true
								: false;
						if (!checkSuccess) {
							break;
						}
					} else if (reviewDTO.currentStateOfFeedback == FeedBackDetailDTO.STATE_DELETED) {
						checkSuccess = this.deleteDTO(reviewDTO.feedBack) > 0 ? true
								: false;
						if (!checkSuccess) {
							break;
						}
					}
				}
			}
			if (checkSuccess) {
				kq = 1;
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			MyLog.e("insertAndUpdateReviewsStaffDTO", "fail", e);
			kq = 0;
		} finally {
			mDB.endTransaction();
		}
		return kq;
	}

	/**
	 *
	 * insert and update training reviews info to db local
	 *
	 * @author: HaiTC3
	 * @param listTrainingReviews
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int tbhvInsertAndUpdateReviewsStaffDTO(
			List<FeedBackTBHVDTO> listTrainingReviews) {
		int kq = 0;
		try {
			mDB.beginTransaction();
			boolean checkSuccess = true;
			FEED_BACK_TABLE tableTraining = new FEED_BACK_TABLE(mDB);
			for (int i = 0, size = listTrainingReviews.size(); i < size; i++) {
				FeedBackTBHVDTO feedBackInfo = listTrainingReviews.get(i);
				// them moi, delete, update record , nhung record khong thay doi
				// thi khong lam gi ca
				if (feedBackInfo.currentState == TrainingShopManagerResultDTO.STATE_NEW_INSERT) {
					checkSuccess = this.insertDTO(feedBackInfo.feedBackBasic) >= 0 ? true
							: false;
					if (!checkSuccess) {
						break;
					}
				} else if (feedBackInfo.currentState == TrainingShopManagerResultDTO.STATE_DELETED) {
					checkSuccess = this.deleteDTO(feedBackInfo.feedBackBasic) > 0 ? true
							: false;
					if (!checkSuccess) {
						break;
					}
				} else if (feedBackInfo.currentState == TrainingShopManagerResultDTO.STATE_NEW_UPDATE) {
					checkSuccess = tableTraining
							.update(feedBackInfo.feedBackBasic) > 0 ? true
							: false;
					if (!checkSuccess) {
						break;
					}
				}
			}
			if (checkSuccess) {
				kq = 1;
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			MyLog.e("tbhvInsertAndUpdateReviewsStaffDTO", "fail", e);
			kq = 0;
		} finally {
			mDB.endTransaction();
		}
		return kq;
	}

	/**
	 *
	 * cap nhat maxId vao bang table_id
	 *
	 * @author: HaiTC3
	 * @param maxFeedbackId
	 * @param maxTrainingResultId
	 * @return: void
	 * @throws:
	 */
	public void updateMaxIdForFeedBackAndTrainingResult(long maxFeedbackId,
			long maxTrainingResultId) {
		try {
			mDB.beginTransaction();
			TABLE_ID tableId = new TABLE_ID(mDB);
			tableId.updateMaxId(FEED_BACK_TABLE.FEED_BACK_TABLE, maxFeedbackId);
			tableId.updateMaxId(TRAINING_RESULT_TABLE.TABLE_NAME,
					maxTrainingResultId);
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateMaxIdForFeedBackAndTrainingResult", "fail", e);
		} finally {
			mDB.endTransaction();
		}
	}

	public ActionLogDTO checkVisitFromActionLog(int staffId, long shopId) {
		ACTION_LOG_TABLE table = new ACTION_LOG_TABLE(mDB);
		ActionLogDTO dto = table.checkVisitFromActionLog(staffId, shopId);
		return dto;
	}

	/**
	 *
	 * cap nhat van de cua nhan vien ban hang (thuc hien boi GSNPP)
	 *
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateGSNPPFollowProblemDone(FollowProblemItemDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			mDB.beginTransaction();
			try {
				FEEDBACK_STAFF_TABLE feedbackTable = new FEEDBACK_STAFF_TABLE(mDB);
				returnCode = feedbackTable.updateGSNPPFollowProblemDone(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Cap nhat van de cua GSNPP (thuc hien boi TBHV)
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				FEED_BACK_TABLE feedbackTable = new FEED_BACK_TABLE(mDB);
				returnCode = feedbackTable.updateTBHVFollowProblemDone(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * HieuNH delete van de cua TBHV
	 *
	 * @param dto
	 * @return
	 */
	public long deleteTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				FEED_BACK_TABLE feedbackTable = new FEED_BACK_TABLE(mDB);
				returnCode = feedbackTable.deleteTBHVFollowProblemDone(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 *
	 * lay d/s san pham de them vao danh gia NVBH
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public ListFindProductSaleOrderDetailViewDTO getListProductToAddReviewsStaff(
			Bundle ext) {
		SALES_ORDER_DETAIL_TABLE saleOrderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				mDB);
		try {
			ListFindProductSaleOrderDetailViewDTO vt = saleOrderDetailTable
					.getListProductToAddReviewsStaff(ext);
			return vt;

		} catch (Exception e) {
			MyLog.e("getListProductToAddReviewsStaff", "fail", e);
			return null;
		}
	}

	/**
	 * lay thong tin bao cao cham cong ngay
	 *
	 * @author: hoanpd1
	 * @since: 10:45:34 24-02-2015
	 * @return: TBHVAttendanceDTO
	 * @param ext
	 * @throws Exception
	 */
	public TBHVAttendanceDTO getTBHVAttendance(Bundle ext) throws Exception {
		STAFF_POSITION_LOG_TABLE staffPosition = new STAFF_POSITION_LOG_TABLE(
				mDB);
		return staffPosition.getTBHVAttendance(ext);
	}

	/**
	 * Request upload photo
	 *
	 * @author: PhucNT
	 * @param mediaDTO
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public long insertMediaItem(MediaItemDTO mediaDTO) {
		// TODO Auto-generated method stub
		long result = -1;
		try {
			mDB.beginTransaction();
			// <HaiTC> - update use new resolution get maxid
			// TABLE_ID tableId = new TABLE_ID(SQLUtils.getInstance().getmDB());
			// // test
			// long offValue =
			// GlobalInfo.getInstance().getProfile().getUserData().id
			// * GlobalInfo.getInstance().getFactorDefault();
			// // cap nhat id order
			// long cshId = tableId
			// .getMaxIdFromTableName(MEDIA_ITEM_TABLE.TABLE_MEDIA_ITEM) + 1;
			// MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
			// mediaDTO.id = offValue + cshId;
			// result = mediaDTO.id;
			// result = table.insert(mediaDTO);
			//
			// tableId.updateMaxId(MEDIA_ITEM_TABLE.TABLE_MEDIA_ITEM, cshId);

			TABLE_ID tableId = new TABLE_ID(SQLUtils.getInstance().getmDB());
			// cap nhat id order
			long cshId = tableId.getMaxIdTime(MEDIA_ITEM_TABLE.TABLE_MEDIA_ITEM);
			MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
			mediaDTO.id = cshId;
			mediaDTO.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
			result = mediaDTO.id;
			result = table.insert(mediaDTO);
			// </HaiTC> - end

			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = -1;
			ServerLogger.sendLog(e.getMessage(),
					TabletActionLogDTO.LOG_EXCEPTION);
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	/**
	 * cap nhat du lieu o db local
	 *
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	public ArrayList<CustomerStockHistoryDTO> saveRemainProduct(
			ArrayList<RemainProductViewDTO> listRemain) {
		// TODO Auto-generated method stub
		MeasuringTime.getStartTimeParser();
		ArrayList<CustomerStockHistoryDTO> listCSTDTO = new ArrayList<CustomerStockHistoryDTO>();
		try {
			mDB.beginTransaction();
			TABLE_ID tableId = new TABLE_ID(SQLUtils.getInstance().getmDB());
			long cshId = tableId.getMaxIdTime(CUSTOMER_STOCK_HISTORY_TABLE.TABLE_NAME);

			for (int i = 0; i < listRemain.size(); i++) {
				CustomerStockHistoryDTO cus = new CustomerStockHistoryDTO();
				cus.convertFromRemainProductDTO(listRemain.get(i));
				cus.customerStockHistoryId = cshId ++;
				cus.createDate = DateUtils.now();
				listCSTDTO.add(cus);
			}
			boolean checkSuccess = false;
			checkSuccess = updateNumberRemainProduct(listCSTDTO);
			if (checkSuccess) {
				// truong hop insert/update thanh cong
				mDB.setTransactionSuccessful();
			} else {
				//
				listCSTDTO = new ArrayList<CustomerStockHistoryDTO>();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			listCSTDTO = null;
		} finally {
			mDB.endTransaction();
		}
		MeasuringTime.getEndTimeParser();
		MyLog.i("INSERT", "Insert voi thuong: " + MeasuringTime.getTimeParser());
		return listCSTDTO;

	}

	/**
	 * lay thong tin chi tiet san pham
	 *
	 * @author: ThanhNN8
	 * @param viewData
	 * @return
	 * @return: ProductDTO
	 * @throws:
	 */
	public ProductDTO getProductInfoDetail(Bundle viewData) {
		// TODO Auto-generated method stub
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		try {
			String productId = viewData
					.getString(IntentConstants.INTENT_PRODUCT_ID);
			ProductDTO dto = productTable.getProductInfoDetail(productId);
			return dto;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/**
	 * Request upload photo
	 *
	 * @author: PhucNT
	 * @param mediaDTO
	 * @return
	 * @return: long
	 * @throws:
	 */
	public int updateNewLinkPhoto(MediaItemDTO mediaDTO) {
		// TODO Auto-generated method stub
		int result = -1;
		if (mDB != null && mDB.isOpen()) {
			try {
				mDB.beginTransaction();

				MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
				result = table.updateNewLink(mediaDTO);

				mDB.setTransactionSuccessful();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				result = -1;
			} finally {
				mDB.endTransaction();
			}
		}
		return result;
	}

	public WrongPlanCustomerDTO getWrongCustomerList(String staffId,
													 String shopId) throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		WrongPlanCustomerDTO dto = table.getWrongCustomerList(staffId, shopId);
		return dto;
	}

	/**
	 * generate sql delete order
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: JSONArray
	 * @throws:
	 */

	public JSONArray generateSqlDeleteOrder(SaleOrderViewDTO dto, ActionLogDTO actionLogDTO) {
		// tao cau sql de xoa sale order tren server
		JSONArray listSql = new JSONArray();
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		try {
			listSql = dto.generateDeleteSaleOrderSql();

			if (dto.isFinalOrder == 1) {
				// delete record cuoi cung thi cap nhat last_order
				CustomerDTO cus = new CustomerDTO();
				cus.customerId = dto.getCustomerId();
				// cus.setLastOrder(null);
				String formatDate = DateUtils
						.convertDateOneFromFormatToAnotherFormat(dto.lastOrder,
								DateUtils.defaultDateFormat_3,
								DateUtils.DATE_FORMAT_NOW);
				listSql.put(cus.generateUpdateLastOrder(formatDate));

				// cap nhat staff customer
				StaffCustomerDTO staffDto = new StaffCustomerDTO();
				staffDto.customerId = dto.customer.customerId;
				staffDto.staffId = staffId;
				staffDto.shopId = "" + shopId;
				staffDto.lastOrder = dto.lastOrder;
				listSql.put(staffDto.generateUpdateFromOrderSql());

				// Cap nhat routing customer
				ROUTING_CUSTOMER_TABLE routingCusTable = new ROUTING_CUSTOMER_TABLE(
						mDB);
				long routingCusId = routingCusTable
						.getRoutingCustomerIdOfCustomer(staffId, shopId,
								dto.customer.customerId);
				RoutingCustomerDTO routingCusDTO = new RoutingCustomerDTO();
				routingCusDTO.routingCustomerId = routingCusId;
				routingCusDTO.lastOrder = dto.lastOrder;
				routingCusTable.updateLastOrder(routingCusDTO);
			}

			//listSql.put(actionLogDTO.generateDeleteActionWhenDeleteOrder());
		} catch (Exception e) {
			MyLog.e("generateSqlDeleteOrder", "fail", e);
		}

		return listSql;
	}

	/**
	 * generate sql delete order - clone PO_CUSTOMER
	 *
	 * @author: DungNX3
	 * @param dto
	 * @param listSql2
	 * @return
	 * @return: JSONArray
	 * @throws:
	 */
	public JSONArray generateSqlDeletePOCustomer(SaleOrderViewDTO dto,
			List<SaleOrderDetailDTO> listSaleOrderDetail,
			ActionLogDTO actionLogDTO, JSONArray listSql) {
		// tao cau sql de xoa PO_CUSTOMER tren server
		try {
			dto.generateDeletePOCustomerSql(listSql);
		} catch (Exception e) {
			MyLog.e("generateSqlDeletePOCustomer", "fail", e);
		}

		return listSql;
	}

	/**
	 * Xoa nhung log cu
	 *
	 * @author banghn
	 * @return
	 */
	public long deleteOldLogTable() {
		long returnCode = 1;
		if (mDB != null && mDB.isOpen() && !mDB.isOpen()) {
			try {
				// delete log table truoc 15 ngay
				LOG_TABLE logTable = new LOG_TABLE(mDB);
				if (logTable.deleteOldLog() < 0) {
					returnCode = -1;
				}
				// delete action log truoc 3 ngay
				ACTION_LOG_TABLE actionLog = new ACTION_LOG_TABLE(mDB);
				if (actionLog.deleteOldActionLog() < 0) {
					returnCode = -1;
				}

				// delete sale_in_month (giu lai 3 thang)
//				String startOfMonth = DateUtils
//						.getFirstDateOfNumberPreviousMonthWithFormat(
//								DateUtils.DATE_FORMAT_DATE, -3);
				String starfOfCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -3);
				String[] params = { starfOfCycle };
				StringBuffer sql = new StringBuffer();
				sql.append("DELETE FROM rpt_sale_in_month  ");
				sql.append("WHERE  cycle_id in (select cy.cycle_id from cycle cy where Date(?) > Date(cy.begin_date)) ");
				mDB.execSQL(sql.toString(), params);

				// xoa nhung feeback sai
//				sql = new StringBuffer();
//				sql.append("DELETE FROM FEEDBACK ");
//				sql.append("WHERE  CREATE_USER_ID <= 0 ");
//				mDB.execSQL(sql.toString());

				// delete position-log
				STAFF_POSITION_LOG_TABLE positionLog = new STAFF_POSITION_LOG_TABLE(
						mDB);
				returnCode = positionLog.deleteOldPositionLog();

				// delete SO & SOD
				SALES_ORDER_DETAIL_TABLE sod = new SALES_ORDER_DETAIL_TABLE(mDB);
				if (sod.deleteOldSaleOrderDetail() < 0) {
					returnCode = -1;
				}
				// SALE_ORDER_TABLE so = new SALE_ORDER_TABLE(mDB);
				SALE_ORDER_TABLE so = new SALE_ORDER_TABLE(mDB);
				if (so.deleteOldSaleOrder() < 0) {
					returnCode = -1;
				}

				// xoa du lieu media cu hon 2 thang, giu lai hinh anh san pham
				MEDIA_ITEM_TABLE mediaItem = new MEDIA_ITEM_TABLE(mDB);
				returnCode = mediaItem.deleteOldMediaItem();
			} catch (Exception e) {
				MyLog.e("Delete Old Log",VNMTraceUnexceptionLog.getReportFromThrowable(e));
				returnCode = -1;
			}
		}
		return returnCode;
	}

	/**
	 * Lay ds don hang co trong log
	 *
	 * @author: TruongHN
	 * @return: ArrayList<String>
	 * @throws:
	 */
	public ArrayList<LogDTO> getOrderInLog() {
		ArrayList<LogDTO> listOrder = new ArrayList<LogDTO>();
		try {
			if (mDB != null && mDB.isOpen()) {
				// lay ds don hang co trong log
				LOG_TABLE logTable = new LOG_TABLE(mDB);
				listOrder = logTable.getOrderInLog();
			}
		} catch (Exception e) {
			MyLog.e("getOrderInLog", "fail", e);
		}
		return listOrder;
	}

	public SupervisorReportDisplayProgressStaffDTO getStaffDisProComProReportDTO(
			Bundle data) throws Exception {
		RPT_DISPLAY_PROGRAME_TABLE rptTable = new RPT_DISPLAY_PROGRAME_TABLE(
				mDB);
		return rptTable.getStaffDisProComProReportDTO(data);
	}

	/**
	 * Update log va sale order
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void updateLogWithOrder(LogDTO log, String saleOrderId) {
		if (mDB != null && log != null && mDB.isOpen() && !mDB.isReadOnly()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				// update log
				updateDTO(log);
				// update saleOrder
				SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
				saleOrderTable.updateState(saleOrderId,
						Integer.parseInt(log.state));
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}

	}

	/**
	 * Update trang thai approve cua don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void updateStateApprovedOfOrder(LogDTO logDTO, int approved) {
		if (mDB != null && mDB.isOpen()) {
			try {
				isProcessingTrans = true;
				mDB.beginTransaction();
				// update saleOrder
				SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
				SaleOrderDTO orderDTO = new SaleOrderDTO();
				orderDTO.saleOrderId = Long.parseLong(logDTO.tableId);
				orderDTO.approved = approved;// Don hang tai mtb cap nhat ve
												// trang thai huy approve = 3
				saleOrderTable.updateStateApprovedOfOrder(orderDTO);

				// cap nhat debit

				// cap nhat lai stock khi don huy
				// lay saleorderdetail
				STOCK_TOTAL_TABLE stockTotalTable = new STOCK_TOTAL_TABLE(mDB);
				DEBIT_TABLE debitTable = new DEBIT_TABLE(mDB);
				DEBIT_DETAIL_TABLE debitDetailTable = new DEBIT_DETAIL_TABLE(
						mDB);

				JSONObject query = new JSONObject(logDTO.value);
				JSONObject params = query.getJSONObject("params");
				JSONArray listSql = new JSONArray(params.getString("listSql"));
				for (int i = 0; i < listSql.length(); i++) {
					JSONObject orderDetail = listSql.getJSONObject(i);
					if (orderDetail.getString("tableName").equals(
							"SALE_ORDER_DETAIL")) {
						// neu la orderdetail thi cap nhat stock
						JSONArray listParam = new JSONArray(
								orderDetail.getString("listParam"));
						StockTotalDTO stockTotal = StockTotalDTO
								.createStockTotalInfoFromJson(listParam);
						stockTotalTable.increaseStockTotal2(stockTotal);
					}
					if (orderDetail.getString("tableName").equals("DEBIT")) {
						// neu la DEBIT thi cap nhat DEBIT
						JSONArray listParam = new JSONArray(
								orderDetail.getString("listParam"));
						DebitDTO dibitDTO = DebitDTO
								.createDebitFromJson(listParam);
						debitTable.increaseDebit(dibitDTO);
					}
					if (orderDetail.getString("tableName").equals(
							"DEBIT_DETAIL")) {
						// neu la DEBIT_DETAIL thi xoa
						JSONArray listParam = new JSONArray(
								orderDetail.getString("listParam"));
						String debitDetailID = null;
						for (int j = 0; j < listParam.length(); j++) {
							JSONObject object = listParam.getJSONObject(j);
							if (object.getString("column").equals(
									"DEBIT_DETAIL_ID")) {
								debitDetailID = object.getString("value");
								break;
							}
						}
						if (!StringUtil.isNullOrEmpty(debitDetailID)) {
							debitDetailTable
									.deleteByDebitDetailID(debitDetailID);
						}
					}
				}

				mDB.setTransactionSuccessful();
			} catch (Exception e) {
				MyLog.e("updateStateApprovedOfOrder", "fail", e);
			} finally {
				mDB.endTransaction();
				isProcessingTrans = false;
			}
			if (GlobalInfo.getInstance().isExitApp()) {
				closeDB();
			}
		}

	}

	/**
	 * Lay ds don hang chua send thanh cong
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public NoSuccessSaleOrderDto getNoSuccessOrderList(ArrayList<String> idList) {
		NoSuccessSaleOrderDto dto = null;
		if (mDB != null && mDB.isOpen()) {
			try {
				SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
				dto = saleOrderTable.getNoSuccessOrderList(idList);
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * Lay lich su mua hang cua nhan vien ban hang ban cho khach hang
	 *
	 * @author: HieuNH
	 * @param staffCode
	 *            , customerCode
	 * @return: void
	 * @throws:
	 */
	public Vector<HistoryItemDTO> getListHistory(String staffId,
			String customerId, String productId, String shopId) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListHistory(staffId, customerId, productId, shopId);
	}

	/**
	 * Lay danh sach NPP
	 *
	 * @author: HieuNH
	 * @param parentShopId
	 * @return: void
	 * @throws:
	 */
	public ArrayList<ShopDTO> getListNPP(String shopId) throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListNPP(shopId);
	}

	/**
	 * Lay danh sach NVGS report dayInOrder
	 *
	 * @author: HieuNH
	 * @param inheritShopId
	 * @return: void
	 * @throws:
	 */
	public ListStaffDTO getListNVGSOfTBHVReportDate(Bundle bundle)
			throws Exception {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListNVGSOfTBHVReportDate(bundle);
	}

	/**
	 * Lay danh sach NVGS
	 *
	 * @author: HieuNH
	 * @param shopId
	 * @return: void
	 * @throws:
	 */
	public ListStaffDTO getListNVGSOfTBHVReportPSDS(String shopId) {
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		return table.getListNVGSOfTBHVReportPSDS(shopId);
	}

	/**
	 * kiem tra db co phai cua user dang dang nhap hay kg
	 *
	 * @author: TamPQ
	 * @param staffCode
	 *            , customerCode
	 * @return: void
	 * @throws:
	 */
	public boolean checkUserDB(String staffCode, int roleType) {
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		return table.checkUserDB(staffCode, roleType);
	}

	/**
	 *
	 * get report progress in month of TBHV
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return: ReportProgressMonthViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public ReportProgressMonthViewDTO getReportProgressInMonthTBHV(Bundle ext) throws Exception {
		ReportProgressMonthViewDTO reviewsData = new ReportProgressMonthViewDTO();
		RPT_STAFF_SALE_TABLE reportStaffSale = new RPT_STAFF_SALE_TABLE(mDB);
		reviewsData = reportStaffSale.getReportProgressInMonthOfTBHV(ext);
		return reviewsData;
	}

	/**
	 *
	 * get report progress in month of TBHV in detail NPP screen
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ReportProgressMonthViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public ReportProgressMonthViewDTO getReportProgressInMonthTBHVDetailNPP(
			Bundle ext) throws Exception {
		ReportProgressMonthViewDTO reviewsData = new ReportProgressMonthViewDTO();
		RPT_STAFF_SALE_TABLE reportStaffSale = new RPT_STAFF_SALE_TABLE(mDB);
		reviewsData = reportStaffSale
				.getReportProgressInMonthOfTBHVDetailNPP(ext);
		return reviewsData;
	}

	/**
	 *
	 * get report progress TBHV sales focus info
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: ReportProgressMonthViewDTO
	 * @throws:
	 */
	public TBHVProgressReportSalesFocusViewDTO getReportProgressTBHVSalesFocusInfo(
			Bundle ext) {
		TBHVProgressReportSalesFocusViewDTO reviewsData = new TBHVProgressReportSalesFocusViewDTO();
		RPT_STAFF_SALE_TABLE reportStaffSale = new RPT_STAFF_SALE_TABLE(mDB);
		try {
			reviewsData = reportStaffSale.getReportProgressSalesFocusInfo(ext);
		} catch (Exception e) {
			MyLog.e("getReportProgressTBHVSalesFocusInfo", "fail", e);
			return null;
		}
		return reviewsData;
	}

	/**
	 *
	 * get report progess tbhv sales focus detail info
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: TBHVProgressReportSalesFocusViewDTO
	 * @throws:
	 */
	public TBHVProgressReportSalesFocusViewDTO getReportProgressTBHVSalesFocusDetailInfo(
			Bundle ext) {
		TBHVProgressReportSalesFocusViewDTO reviewsData = new TBHVProgressReportSalesFocusViewDTO();
		RPT_STAFF_SALE_TABLE reportStaffSale = new RPT_STAFF_SALE_TABLE(mDB);
		try {
			reviewsData = reportStaffSale
					.getReportProgressSalesFocusDetailInfo(ext);
		} catch (Exception e) {
			MyLog.e("getReportProgressTBHVSalesFocusDetailInfo", "fail", e);
			return null;
		}
		return reviewsData;
	}

	/**
	 *
	 *
	 * @author: HaiTC3
	 * @param ext
	 * @return
	 * @return: List<TrainingShopManagerResultDTO>
	 * @throws:
	 */
	public List<FeedBackTBHVDTO> getTrainingReviewsGSNPPOfTBHVInfo(Bundle ext) {
		List<FeedBackTBHVDTO> listTrainingReviewsTBHV = new ArrayList<FeedBackTBHVDTO>();
		FEED_BACK_TABLE trainingReviewsTable = new FEED_BACK_TABLE(mDB);
		try {
			listTrainingReviewsTBHV = trainingReviewsTable
					.getListReviewsOfTBHV(ext);
		} catch (Exception e) {
			MyLog.e("getTrainingReviewsGSNPPOfTBHVInfo", "fail", e);
			return null;
		}
		return listTrainingReviewsTBHV;
	}

	/**
	 * getTBHVListRouteSupervision
	 *
	 * @author: TamPQ
	 * @return: TBHVRouteSupervisionDTO
	 * @throws:
	 */
	public TBHVRouteSupervisionDTO getTBHVListRouteSupervision(int staff_id,
															   String shopId, int day) throws Exception {
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		return table.getTbhvRouteSupervision(staff_id, shopId, day);
	}

	/**
	 * Get danh sach khach hang cua nhan vien ban hang trong tuyen
	 *
	 * @author banghn
	 * @param sortInfo
	 * @return
	 */
	public CustomerListDTO requestGetCustomerSaleList(String ownerId,
			String shopId, long staffId, String code, String nameAddress,
			String visitPlan, int page, String listStaff, DMSSortInfo sortInfo) throws Exception {
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		return custommerTable.requestGetCustomerSaleList(ownerId, shopId,
				staffId, code, nameAddress, visitPlan, page, listStaff, sortInfo);
	}

	/**
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @return: TBHVGsnppTrainingPlanDTO
	 * @throws Exception
	 * @throws NumberFormatException
	 * @throws:
	 */
	public TBHVTrainingPlanDTO getTbhvTrainingPlan(int staffId, int shopId)
			throws NumberFormatException, Exception {
		TRAINING_PLAN_DETAIL_TABLE table = new TRAINING_PLAN_DETAIL_TABLE(mDB);
		return table.getTbhvTrainingPlan(staffId, shopId);
	}

	/**
	 * Lay danh sach lich su cap nhat vi tri khach hang
	 *
	 * @author banghn
	 * @param customerId
	 * @return
	 */
	public ArrayList<CustomerUpdateLocationDTO> getCustomerHistoryUpdateLocation(
			String customerId) {
		ArrayList<CustomerUpdateLocationDTO> listHistory = null;
		CUSTOMER_POSITION_LOG_TABLE logPosition = new CUSTOMER_POSITION_LOG_TABLE(
				mDB);
		try {
			listHistory = logPosition
					.getCustomerHistoryUpdateLocation(customerId);
		} catch (Exception e) {
		} finally {
		}
		return listHistory;
	}

	/**
	 * Lay ds vi tri NVBH cua NVGS
	 *
	 * @author: TruongHN
	 * @param dto
	 * @throws Exception
	 */
	public GsnppRouteSupervisionDTO getListPositionSalePersons(int staffId,
			String shopId) throws Exception {
		GsnppRouteSupervisionDTO dto = null;
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		dto = staffTable.getListPositionSalePersons(staffId, shopId);
		dto.listShop = SQLUtils.getInstance().requestGetListCombobox(shopId);
		return dto;
	}

	/**
	 * getDayTrainingSupervision
	 *
	 * @author: TamPQ
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 */
	public TBHVTrainingPlanDayResultReportDTO getDayTrainingSupervision(
			int trainDetailId, int shopId) {
		return new RPT_SALE_RESULT_DETAIL_TABLE(mDB).getDayTrainingSupervision(
				trainDetailId, shopId);
	}

	/**
	 * getHistoryPlanTraining
	 *
	 * @author: TamPQ
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 */
	public TBHVTrainingPlanHistoryAccDTO getPlanTrainingHistoryAcc(int staffId,
																   int gsnppStaffId, String shopId, boolean getListStaff) throws Exception {
		return new TRAINING_PLAN_DETAIL_TABLE(mDB).getPlanTrainingHistoryAcc(
				staffId, gsnppStaffId, shopId, getListStaff);
	}

	/**
	 * Lay ds don hang loi: bao gom don hang chua goi, don hang loi va don hang
	 * tra ve
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public NoSuccessSaleOrderDto getAllOrderFail() {
		NoSuccessSaleOrderDto dto = null;
		if (mDB != null && mDB.isOpen()) {
			try {
				SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
				dto = saleOrderTable.getAllOrderFail(GlobalInfo.getInstance()
						.getProfile().getUserData().getInheritId(), GlobalInfo
						.getInstance().getProfile().getUserData().getInheritShopId());
			} catch (Exception ex) {
				// mDB.endTransaction();
			}
		}
		return dto;
	}

	/**
	 * Lay thong tin don hang can canh bao
	 *
	 * @author: TruongHN
	 * @return: NotifyOrderDTO
	 * @throws:
	 */
	public NotifyOrderDTO getOrderNeedNotify() {
		NotifyOrderDTO notifyDTO = new NotifyOrderDTO();
		if (mDB != null && mDB.isOpen()) {
			// lay ds don hang co trong log
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			notifyDTO.listOrderInLog = logTable.getOrderInLog();
			// lay so luong don hang tra ve trong ngay tu NPP
			SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
			notifyDTO.numOrderReturnNPP = orderTable
					.getOrderReturnNPPInDay(GlobalInfo.getInstance()
							.getProfile().getUserData().getInheritId());
		}
		return notifyDTO;
	}

	/**
	 * update ExeptionOrderDate
	 *
	 * @author: TamPQ
	 * @param dto
	 * @return
	 * @return: longvoid
	 * @throws:
	 */
	public long updateExceptionOrderDate(StaffCustomerDTO dto) {
		long returnCode = -1;
		if (dto != null) {
			mDB.beginTransaction();
			try {
				STAFF_CUSTOMER_TABLE staffCusTable = new STAFF_CUSTOMER_TABLE(
						mDB);
				returnCode = staffCusTable.insertOrUpdate(dto);
				mDB.setTransactionSuccessful();
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 *
	 * get list industry product and list product
	 *
	 * @param ext
	 * @return
	 * @return: SaleStatisticsProductInDayInfoViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Oct 20, 2012
	 */
	public SaleStatisticsProductInDayInfoViewDTO getListIndustryProductAndListProduct(Bundle ext)
			throws Exception {
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		return productTable.getListIndustryProductAndListProduct(ext);
	}

	/**
	 *
	 * get list product pre sale sold
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SaleProductInfoDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Oct 20, 2012
	 */
	public ArrayList<SaleProductInfoDTO> getListProductPreSaleSold(Bundle ext) throws Exception {
		ArrayList<SaleProductInfoDTO> result = new ArrayList<SaleProductInfoDTO>();
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		result = productTable.getListProductPreSaleSold(ext);
		return result;
	}

	/**
	 *
	 * get list product van sale sold
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SaleProductInfoDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 17, 2013
	 */
	public ArrayList<SaleProductInfoDTO> getListProductVanSaleSold(Bundle ext) throws Exception{
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		return productTable.getListProductVanSaleSold(ext);
	}

	public ArrayList<String> getSaleStatisticsAccumulateDay(String shopId,
			String staffId, String productCode, String productName,
			String industry, String page) throws Exception {
		PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
		return table.getSaleStatisticsAccumulateDay(shopId, staffId,
				productCode, productName, industry, page);
	}

	public SaleStatisticsAccumulateDayDTO getSaleStatisticsAccumulateDayListProduct(Bundle data) throws Exception {
		PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
		return table.getSaleStatisticsAccumulateDayListProduct(data);
	}

	public int getCountSaleStatisticsAccumulateDayListProduct(String shopId,
			String staffId, String productCode, String productName,
			String industry) throws Exception {
		PRODUCT_TABLE table = new PRODUCT_TABLE(mDB);
		return table.getCountSaleStatisticsAccumulateDayListProduct(shopId,
				staffId, productCode, productName, industry);
	}

	/**
	 * Lay thong tin so luong ctkm & cttb dang chay
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopId
	 * @return
	 * @return: Bundle
	 * @throws:
	 */

	public Bundle getBusinessSupportProgrameInfo(int shopId) {
		Bundle dto = new Bundle();
		PROMOTION_PROGRAME_TABLE promotionTable = new PROMOTION_PROGRAME_TABLE(
				mDB);
		int numPromotionPrograme = promotionTable
				.getNumPromotionProgrameRunning(String.valueOf(shopId));

		DISPLAY_PROGRAME_TABLE displayTable = new DISPLAY_PROGRAME_TABLE(mDB);
		int numDisplayPrograme = displayTable.getNumDisplayProgrameRunning();

		dto.putInt("CTKM", numPromotionPrograme);
		dto.putInt("CTTB", numDisplayPrograme);

		return dto;
	}

	/**
	 *
	 * get list track and fix problem of gsnpp
	 *
	 * @param ext
	 * @return
	 * @return: SuperviorTrackAndFixProblemOfGSNPPViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public SuperviorTrackAndFixProblemOfGSNPPViewDTO getListTrackAndFixProblemOfGSNPP(
			Bundle ext) {
		SuperviorTrackAndFixProblemOfGSNPPViewDTO reviewsData = new SuperviorTrackAndFixProblemOfGSNPPViewDTO();
		// AP_PARAM_TABLE appParam = new AP_PARAM_TABLE(mDB);
		FEED_BACK_TABLE feedBack = new FEED_BACK_TABLE(mDB);
		try {
			boolean isGetTotalitem = ext
					.getBoolean(IntentConstants.INTENT_IS_ALL);
			SuperviorTrackAndFixProblemOfGSNPPViewDTO reviewsDataTMP = feedBack
					.getListTrackAndFixProblemOfGSNPP(ext);
			reviewsData.listProblemsOfGSNPP = reviewsDataTMP.listProblemsOfGSNPP;
			if (isGetTotalitem) {
				reviewsData.totalItem = reviewsDataTMP.totalItem;
			}

		} catch (Exception e) {
			MyLog.e("getListTrackAndFixProblemOfGSNPP", "fail", e);
			return null;
		}
		return reviewsData;
	}

	/**
	 *
	 * get list time define for header
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<ActionLogDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 21, 2013
	 */
	public ArrayList<ShopParamDTO> getListDefineHeaderTable(Bundle ext) {
		ArrayList<ShopParamDTO> listDefineTimeHeader = new ArrayList<ShopParamDTO>();
		SHOP_PARAM_TABLE shopPram = new SHOP_PARAM_TABLE(mDB);
		try {
			listDefineTimeHeader = shopPram.getListTimeHeader(ext);
		} catch (Exception e) {
			MyLog.e("getListDefineHeaderTable", "fail", e);
			return null;
		}
		return listDefineTimeHeader;
	}

	/**
	 *
	 * lay d/s lich su bao cao cua NVBH da ghe tham khach hang trong ngay
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<ReportNVBHVisitCustomerDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 21, 2013
	 */
	public ArrayList<ReportNVBHVisitCustomerDTO> getListReportVisitCustomerInDay(
			Bundle ext) throws Exception {
		ArrayList<ReportNVBHVisitCustomerDTO> listReportNVBH = new ArrayList<ReportNVBHVisitCustomerDTO>();
		ACTION_LOG_TABLE actionLog = new ACTION_LOG_TABLE(mDB);
		listReportNVBH = actionLog.getListReportNVBHInDay(ext);
		return listReportNVBH;
	}

	/**
	 *
	 * request update feedback status
	 *
	 * @param ext
	 * @return
	 * @return: int
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public int requestUpdateFeedbackStatus(String feedBackId,
			String feedBackStatus, String doneDate) {
		FEED_BACK_TABLE feedBack = new FEED_BACK_TABLE(mDB);
		int result = 0;
		try {
			mDB.beginTransaction();
			feedBack.updateFeedBackStatus(feedBackStatus, doneDate, feedBackId);
			result = 1;
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("requestUpdateFeedbackStatus", "fail", e);
			result = 0;
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	/**
	 * Lay thong tin GSNNP & loai van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param nvbhShopId
	 * @return
	 * @return: TBHVAddRequirementViewDTO
	 * @throws:
	 */
	public TBHVCustomerListDTO getCustomerListForPostFeedback(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		TBHVCustomerListDTO dto = staffTable.getCustomerListForPostFeedback(ext);
		return dto;
	}

	/**
	 * Lay thong tin GSNNP & loai van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopId
	 * @return
	 * @return: TBHVAddRequirementViewDTO
	 * @throws:
	 */
	public TBHVAddRequirementViewDTO getAddRequirementInfo(int shopId) throws Exception {
		// TODO Auto-generated method stub
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		AP_PARAM_TABLE apParamTable = new AP_PARAM_TABLE(mDB);
		TBHVAddRequirementViewDTO model = new TBHVAddRequirementViewDTO();

		model.listNVBH = staffTable
				.getStaffCodeComboboxProblemSuperVisorOfTBHV(String
						.valueOf(shopId));
		model.listTypeProblem = apParamTable.getListTBHVProblemType();
		// List<CustomerDTO> customerList =
		// staffTable.getListCustomerOfSupervisor(5);
		return model;
	}

	/**
	 * Luu thong tin feedback (problem) do TBHV tao ra
	 *
	 * @author: Nguyen Thanh Dung
	 * @param nvbhShopId
	 * @return
	 * @return: TBHVAddRequirementViewDTO
	 * @throws:
	 */
	public boolean createTBHVFeedback(FeedBackDTO feedBack) {
		// long feedBackId = -1;
		long insert = -1;
		boolean insertSuccess = true;

		FEED_BACK_TABLE orderTable = new FEED_BACK_TABLE(mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);
		try {
			mDB.beginTransaction();
			// cap nhat id order
			// feedBackId = tableId.getMaxId(FEED_BACK_TABLE.FEED_BACK_TABLE);
			// feedBack.feedBackId = feedBackId;
			long feedBackId = tableId.getMaxIdTime(FEEDBACK_TABLE.FEEDBACK_TABLE);
			for (int i = 0; i < feedBack.getArrStaffId().size(); i++) {
				feedBack.setFeedBackId(feedBackId++);
				feedBack.getArrFeedBackId().add(String.valueOf(feedBack.getFeedBackId()));
				feedBack.setStaffId(feedBack.getArrStaffId().get(i));
				if (!StringUtil
						.isNullOrEmpty(feedBack.getCustomerShopIdForRequest())) {
					feedBack.setShopId(feedBack.getCustomerShopIdForRequest());
				} else {
					feedBack.setShopId(feedBack.getArrStaffShopId().get(i));
				}
				insert = orderTable.insert(feedBack);
				if (insert == -1) {
					insertSuccess = false;
					break;
				}
			}

			if (insertSuccess) {
				// insertSuccess = true;
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			insertSuccess = false;
		} finally {
			mDB.endTransaction();
		}

		return insertSuccess;
	}

	/**
	 * get Gsnpp Position
	 *
	 * @author: TamPQ
	 * @param staffIdGS
	 * @param today
	 * @return
	 * @return: TBHVRouteSupervisionDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public TBHVRouteSupervisionDTO getGsnppPosition(String shopId,
			String staffID) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		TBHVRouteSupervisionDTO dto = null;
		dto = staffTable.getGsnppPosition(shopId, staffID);
		return dto;
	}

	/**
	 * get Nvbh Position
	 *
	 * @author: TamPQ
	 * @param staffId
	 * @return
	 * @return: SaleRoadMapSupervisorDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public GsnppRouteSupervisionDTO getNvbhPosition(String staffId,
			String shopId) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		GsnppRouteSupervisionDTO dto = staffTable.getNvbhPosition(staffId, shopId);
		return dto;
	}

	/**
	 * Lay DS KH < 2 phut
	 *
	 * @author: TamPQ
	 * @param lessThan2MinList
	 * @return
	 * @return: LessThan2MinsDTOvoid
	 * @throws:
	 */
	public GsnppLessThan2MinsDTO requestGetLessThan2Mins(String shopId,
														 int staffId, String lessThan2MinList) {
		GsnppLessThan2MinsDTO dto = null;

		if (mDB != null && mDB.isOpen()) {
			try {
				CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
				dto = table.requestGetLessThan2Mins(shopId, staffId,
						lessThan2MinList);
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 *
	 * get forcus product infor view
	 *
	 * @param data
	 * @return
	 * @return: NVBHReportForcusProductInfoViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public NVBHReportForcusProductInfoViewDTO getForcusProductInfoView(
			Bundle data) throws Exception {
		PRODUCT_TABLE productTable = new PRODUCT_TABLE(mDB);
		NVBHReportForcusProductInfoViewDTO vt = productTable
				.getForcusProductInfoView(data);
		return vt;
	}

	/**
	 * K tra da co log ghe tham trong ngay hay chua cua 1 KH
	 *
	 * @author: TamPQ
	 * @param actionLog
	 * @return
	 * @return: booleanvoid
	 * @throws:
	 */
	public boolean alreadyHaveVisitLog(ActionLogDTO actionLog) {
		boolean alreadyHaveVisitLog = false;
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		try {
			alreadyHaveVisitLog = staffTable.alreadyHaveVisitLog(actionLog);
		} catch (Exception e) {
		}
		return alreadyHaveVisitLog;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param list
	 * @return
	 * @return: ArrayList<StaffItem>void
	 * @throws:
	 */
	public ListStaffDTO getPositionOfGsnppAndNvbh(String[] list) {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		ListStaffDTO staffList = staffTable.getPositionOfGsnppAndNvbh(list);
		return staffList;
	}

	/**
	 * Lay danh sach cac loai van de can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:33:37 21-05-2015
	 * @return: void
	 * @throws:
	 */
	public Vector<ApParamDTO> getListTypeProblemNVBHGSNPP() {
		AP_PARAM_TABLE apParamTable = new AP_PARAM_TABLE(mDB);
		return apParamTable.getListTypeProblemNVBHGSNPP();
	}

	/**
	 * chi tiet cong no
	 *
	 * @author: TamPQ
	 * @param cusId
	 * @return
	 * @return: CustomerListDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public CustomerDebitDetailDTO requestDebitDetail(long debitId) throws Exception {
		DEBIT_DETAIL_TEMP_TABLE debit = new DEBIT_DETAIL_TEMP_TABLE(mDB);
		CustomerDebitDetailDTO debitDetail = debit.requestDebitDetail(debitId);
		return debitDetail;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param from
	 * @return
	 * @return: ArrayList<String>void
	 * @throws:
	 */
	public ArrayList<ApParamDTO> requestGetTypeFeedback(int from) {
		AP_PARAM_TABLE table = new AP_PARAM_TABLE(mDB);
		ArrayList<ApParamDTO> list = table.requestGetTypeFeedback(from);
		return list;
	}

	/**
	 * Request get danh sach cong no khach hang
	 *
	 * @author: BangHN
	 * @param cusCode
	 * @param cusNameAdd
	 * @return
	 * @return: ArrayList<CustomerDebtDTO>
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */

	public ArrayList<CustomerDebtDTO> requestGetCustomerDebt(String cusCode,
			String cusNameAdd) {
		DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
		ArrayList<CustomerDebtDTO> listData = null;
		try {
			listData = debit.getCustomerDebt(cusCode, cusNameAdd);
		} catch (Exception e) {
			MyLog.e("requestGetCustomerDebt", "fail", e);
		}
		return listData;
	}

	/**
	 * Lay ds canh bao ghe tham di tuyen
	 *
	 * @author: TamPQ
	 * @param staffIdGS
	 * @return
	 * @return: TBHVVisitCustomerNotificationDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public TBHVVisitCustomerNotificationDTO getVisitCusNotification(
			String staffId,String shopId) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		// TBHVVisitCustomerNotificationDTO dto = staffTable
		// .getVisitCusNotification(shopId);
		TBHVVisitCustomerNotificationDTO dto = staffTable
				.getVisitCustomerNotification(staffId, shopId);
		return dto;
	}

	/**
	 * get list product storage
	 *
	 * @author: HieuNH
	 * @return: ListProductDTO
	 * @throws Exception
	 * @throws:
	 */
	public GSNPPTakeAttendaceDTO getSupervisorAttendanceList(Bundle ext) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		SHOP_PARAM_TABLE apParamTable = new SHOP_PARAM_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		GSNPPTakeAttendaceDTO dto = new GSNPPTakeAttendaceDTO();
		List<ShopParamDTO> paramList = apParamTable
				.getListParamForTakeAttendance(ext);
		dto.listInfo = paramList;
		LatLng shopPosition = shopTable.getPositionOfShop(ext
				.getString(IntentConstants.INTENT_SHOP_ID));
		dto.shopPosition = shopPosition;
		List<AttendanceDTO> attendanceList = staffTable
				.getSupervisorAttendanceList(ext, shopPosition);
		dto.listStaff = attendanceList;
		return dto;
	}

	/**
	 * Lay thoi gian deffine cac tham so thoi gian cham cong
	 *
	 * @author banghn
	 * @param ext
	 *            : bundle chua shop id
	 * @return: cac tham so define
	 */
	public List<ShopParamDTO> getAttendaceTimeDefine(Bundle ext) throws Exception {
		SHOP_PARAM_TABLE apParamTable = new SHOP_PARAM_TABLE(mDB);
		List<ShopParamDTO>	paramList = apParamTable.getListParamForTakeAttendance(ext);
		return paramList;
	}

	/**
	 * Lay danh sach cham chi tiet cua role quan ly
	 *
	 * @author: hoanpd1
	 * @since: 10:44:36 12-03-2015
	 * @return: GSNPPTakeAttendaceDTO
	 * @throws:
	 * @param ext
	 * @return
	 */
	public GSNPPTakeAttendaceDTO getTBHVListAttendance(Bundle ext) {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		SHOP_PARAM_TABLE apParamTable = new SHOP_PARAM_TABLE(mDB);
		GSNPPTakeAttendaceDTO dto = new GSNPPTakeAttendaceDTO();
		try {
			List<ShopParamDTO> paramList = apParamTable
					.getListParamForTakeAttendance(ext);
			dto.listInfo = paramList;
			LatLng shopPosition = shopTable.getPositionOfShop(ext
					.getString(IntentConstants.INTENT_SHOP_ID));
			dto.shopPosition = shopPosition;
			List<AttendanceDTO> attendanceList = staffTable
					.getSupervisorAttendanceList(ext, shopPosition);
			dto.listStaff = attendanceList;
		} catch (Exception e) {
			MyLog.e("getTBHVListAttendance", "fail", e);
		} finally {
		}
		return dto;
	}

	/**
	 * Tao phieu thu
	 *
	 * @author: TamPQ
	 * @param cusDebitDetailDto
	 * @return
	 * @return: booleanvoid
	 * @throws:
	 */
	// public boolean createPayReceived(CusDebitDetailDTO cusDebitDetailDto) {
	// PAY_RECEIVED_TABLE payReceivedTable = new PAY_RECEIVED_TABLE(mDB);
	// PAYMENT_DETAIL_TABLE payDetailTable = new PAYMENT_DETAIL_TABLE(mDB);
	// TABLE_ID tableId = new TABLE_ID(mDB);
	// try {
	// mDB.beginTransaction();
	// // generate PAY_RECEIVED_NUMBER
	// // PAY_RECEIVED_NUMBER = chu cai dau tien (CT) + 5 chu cuoi staff_id
	// // +
	// // so cuoi cua pay_received_Id
	// long payReceivedID = tableId
	// .getMaxId(PAY_RECEIVED_TABLE.TABLE_NAME);
	// String strpayReceivedID = String.valueOf(payReceivedID);
	// String userId = ""
	// + GlobalInfo.getInstance().getProfile().getUserData().id;
	// StringBuilder payReceivedNumber = new StringBuilder();
	// payReceivedNumber.append("CT");
	// if (userId.length() >= 5) {
	// payReceivedNumber.append(userId.substring(userId.length() - 5));
	// } else {
	// String zero = "00000";
	// int lengId = userId.length();
	// payReceivedNumber.append(zero.substring(lengId) + userId);
	// }
	//
	// if (strpayReceivedID.length() >= 8) {
	// payReceivedNumber.append(strpayReceivedID
	// .substring(strpayReceivedID.length() - 8));
	// } else {
	// String zero = "00000000";
	// int lengId = strpayReceivedID.length();
	// payReceivedNumber.append(zero.substring(lengId)
	// + strpayReceivedID);
	// }
	//
	// // update DEBIT
	// DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
	// long debitSucc = debit.updateDebt(cusDebitDetailDto);
	// // send to server
	// if (debitSucc <= 0) {
	// return false;
	// }
	//
	// // insert vao PayReceived
	// PayReceivedDTO dto = new PayReceivedDTO();
	// dto.type = 0;
	// dto.payReceivedID = payReceivedID;
	// dto.amount = cusDebitDetailDto.payNowTotal;
	// dto.customerId = cusDebitDetailDto.customerId;
	// dto.paymentType = PayReceivedDTO.PAYMENT_TYPE_CASH;
	// dto.shopId = GlobalInfo.getInstance().getProfile().getUserData().shopId;
	// dto.receiptType = 0;
	// dto.payReceivedNumber = payReceivedNumber.toString();
	// dto.createDate = DateUtils.now();
	// dto.createUser = ""
	// + GlobalInfo.getInstance().getProfile().getUserData().userName;
	// cusDebitDetailDto.payReceivedDto = dto;
	// long paySuccess = payReceivedTable.insert(dto);
	// // send to server
	// if (paySuccess == -1) {
	// return false;
	// }
	//
	// long tempPaymentDetailID = tableId
	// .getMaxId(PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL);
	// cusDebitDetailDto.arrPaymentDetailDto = new
	// ArrayList<PaymentDetailDTO>();
	// for (int i = 0; i < cusDebitDetailDto.arrList.size(); i++) {
	// if (cusDebitDetailDto.arrList.get(i).isWouldPay) {
	// // update DEBIT_DETAIL
	// DEBIT_DETAIL_TABLE debitDetail = new DEBIT_DETAIL_TABLE(mDB);
	// long detaiSucc = debitDetail
	// .updateDebt(cusDebitDetailDto.arrList.get(i));
	// // send to server
	// if (detaiSucc <= 0) {
	// return false;
	// }
	//
	// // insert PAYMENT_DETAIL
	// PaymentDetailDTO paymentDetailDto = new PaymentDetailDTO();
	// paymentDetailDto.paymentDetailID = tempPaymentDetailID;
	// paymentDetailDto.payReceivedID = payReceivedID;
	// paymentDetailDto.discount = cusDebitDetailDto.arrList.get(i).discount;
	// paymentDetailDto.debitDetailID = cusDebitDetailDto.arrList
	// .get(i).debitDetailId;
	// // paymentDetailDto.amount = cusDebitDetailDto.arrList.get(i).paidAmount;
	// paymentDetailDto.amount =
	// cusDebitDetailDto.arrList.get(i).paidAmountItemDetail;
	// paymentDetailDto.paymentType = "1";
	// paymentDetailDto.status = 1;
	// // ********************* ngay chot
	// paymentDetailDto.payDate = DateUtils.now();
	// paymentDetailDto.createUser = ""
	// + GlobalInfo.getInstance().getProfile()
	// .getUserData().userName;
	// paymentDetailDto.createDate = DateUtils.now();
	// long paymentSucc = payDetailTable.insert(paymentDetailDto);
	// tempPaymentDetailID = paymentSucc + 1;
	// cusDebitDetailDto.arrPaymentDetailDto.add(paymentDetailDto);
	//
	// // send to server
	// if (paymentSucc <= 0) {
	// return false;
	// }
	// } else {
	// cusDebitDetailDto.arrPaymentDetailDto
	// .add(new PaymentDetailDTO());
	// }
	// }
	// mDB.setTransactionSuccessful();
	// } catch (Exception e) {
	// try {
	// throw e;
	// } catch (Exception e1) {
	// }
	// } finally {
	// mDB.endTransaction();
	// }
	//
	// return true;
	// }

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param staffTypeId
	 * @return
	 * @return: Stringvoid
	 * @throws:
	 */
	public String requestStaffType(int staffTypeId) {
		AP_PARAM_TABLE apParamTable = new AP_PARAM_TABLE(mDB);
		String staffType = null;
		try {
			staffType = apParamTable.requestStaffType(staffTypeId);
		} catch (Exception e) {
			MyLog.e("requestStaffType", "fail", e);
		} finally {
		}
		return staffType;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param inheritId
	 * @param customerId
	 * @return
	 * @return: ArrayList<ActionLogDTO>void
	 * @throws:
	 */
	public ArrayList<ActionLogDTO> getVisitActionLogWithEndTimeIsNull(
			int staffId, long cusId, String shopId) {
		ArrayList<ActionLogDTO> listActLog = null;
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		try {
			listActLog = staffTable.getVisitActionLogWithEndTimeIsNull(staffId,
					cusId, shopId);
		} catch (Exception e) {
		}
		return listActLog;
	}

	/**
	 *
	 * update list ActionLog To DB
	 *
	 * @author: TamPQ
	 * @param ext
	 * @return
	 * @return:
	 * @throws:
	 */
	public long updateEndtimeListActionLogToDB(
			ArrayList<ActionLogDTO> listActLog) {
		long returnCode = -1;
		ACTION_LOG_TABLE table = new ACTION_LOG_TABLE(mDB);
		try {
			mDB.beginTransaction();
			for (int i = 0; i < listActLog.size(); i++) {
				returnCode = table.updateVisitEndtime(listActLog.get(i));
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("updateEndtimeListActionLogToDB", "fail", e);
		} finally {
			mDB.endTransaction();
		}
		return returnCode;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param parentStaffId
	 * @param month
	 * @param isGetTotalPage
	 * @param page
	 * @return
	 * @return: CustomerListDTOvoid
	 * @throws:
	 */
	public CustomerListDTO requestGetCusNoPSDS(int rptSaleHisId,
			boolean isGetTotalPage, int page) {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.requestGetCusNoPSDS(rptSaleHisId, isGetTotalPage, page);
	}

	/**
	 * lay danh sach hinh anh cua chuong trinh
	 *
	 * @author thanhnn
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param lastMediaId
	 * @param lastCreatedTime
	 * @return
	 */
	public ArrayList<PhotoDTO> getAlbumDetailPrograme(String customerId,
			String type, String numTop, String page, String programeCode,
			String fromDate, String toDate, String shopId) throws Exception {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		ArrayList<PhotoDTO> listPhoto = mediaItemTable.getAlbumDetailPrograme(
				customerId, type, numTop, page, programeCode, fromDate, toDate,
				shopId);
		return listPhoto;
	}

	/**
	 * lay danh sach hinh anh cua chuong trinh (gsnpp)
	 *
	 * @author thanhnn
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param lastMediaId
	 * @param lastCreatedTime
	 * @return
	 */
	public ArrayList<PhotoDTO> getSuperVisorAlbumDetailPrograme(
			String customerId, String type, String numTop, String page,
			String programeCode, String fromDate, String toDate) {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		ArrayList<PhotoDTO> listPhoto = mediaItemTable
				.getSuperVisorAlbumDetailPrograme(customerId, type, numTop,
						page, programeCode, fromDate, toDate);
		return listPhoto;
	}

	/**
	 * lay danh sach album theo chuong trinh cua khach hang
	 *
	 * @author thanhnn
	 * @param data
	 * @return
	 */
	public ArrayList<AlbumDTO> getAlbumProgrameInfo(Bundle data) throws Exception {
		MEDIA_ITEM_TABLE mediaItemTable = new MEDIA_ITEM_TABLE(mDB);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String programeCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		ArrayList<AlbumDTO> arrayListAlbum = mediaItemTable
				.getAlbumProgramInfoCTTB(programeCode, customerId, fromDate,
						toDate, shopId, staffId);
		return arrayListAlbum;

	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param staffId
	 * @return
	 * @return: StaffDTOvoid
	 * @throws Exception
	 * @throws:
	 */
	public ArrayList<GeomDTO> getPosition(int staffId) throws Exception {
		STAFF_POSITION_LOG_TABLE table = new STAFF_POSITION_LOG_TABLE(mDB);
		return table.getPosition(staffId);
	}

	/**
	 *
	 * lay danh sach tim kiem hinh anh nvbh
	 *
	 * @author: YenNTH
	 * @param bundle
	 * @return
	 * @return: ImageListDTO
	 * @throws:
	 */
	public ImageSearchViewDTO getListAlbum(Bundle bundle) throws Exception {
		ImageSearchViewDTO dto = new ImageSearchViewDTO();
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		DISPLAY_PROGRAME_TABLE programeTable = new DISPLAY_PROGRAME_TABLE(mDB);
		dto = custommerTable.getImageSearchList(bundle);
		boolean isAll = bundle.getBoolean(IntentConstants.INTENT_IS_ALL);
		if (isAll) {
			DisplayProgrameModel listDisplay = new DisplayProgrameModel();
			listDisplay = programeTable.getListDisplayProgrameImage(bundle);
			if (listDisplay != null && listDisplay.getModelData() != null) {
				dto.listPrograme.addAll(listDisplay.getModelData());
			}
		}

		return dto;
	}

	/**
	 * lay bao cao chi tiet doanh so ngay cua KH
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ReportStaffSaleDetailViewDTO getCatAmountDetailReport(Bundle data) throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		ReportStaffSaleDetailViewDTO result = table
				.getCatAmountDetailReport(data);
		return result;
	}

	public ProblemsFeedBackDTO getListProblemsFeedback(int staffId, long cusId,
			int page) {
		FEEDBACK_TABLE table = new FEEDBACK_TABLE(mDB);
		ProblemsFeedBackDTO dto = table.getListProblemsFeedback(staffId, cusId,
				page);
		return dto;
	}

	/**
	 * Lay danh sach khach hang da ghe tham
	 *
	 * @author: BANGHN
	 * @param staffId
	 * @return
	 */
	public CustomerVisitedViewDTO getCustomerVisited(int staffId, String shopId) throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		CustomerVisitedViewDTO dto = table.getCustomerVisited(staffId, shopId);
		return dto;
	}

	/**
	 *
	 * danh sach cong van NVBH, GSNPP
	 *
	 * @author: YenNTH
	 * @param parentStaffId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: PromotionProgrameModel
	 * @throws:
	 */
	public OfficeDocumentListDTO getListDocument(int type, String fromDate,
												 String toDate, String ext, boolean checkLoadMore, String shopId) {

		OFFICE_DOCUMENT_TABLE officeDocumentTable = new OFFICE_DOCUMENT_TABLE(
				mDB);
		return officeDocumentTable.getListOfficeDocument(type, fromDate,
				toDate, ext, checkLoadMore, shopId);

	}

	/**
	 *
	 * chi tiet cong van
	 *
	 * @author: YenNTH
	 * @param staffId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: PromotionProgrameModel
	 * @throws:
	 */
	public MediaItemDTO getListDocumentDetail(long staffId) {

		MEDIA_ITEM_TABLE promotionProgrameTable = new MEDIA_ITEM_TABLE(mDB);
		return promotionProgrameTable.getImageOfficeDocument(staffId);

	}

	/**
	 *
	 * danh sach cong van TBHV
	 *
	 * @author: YenNTH
	 * @param parentStaffId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: PromotionProgrameModel
	 * @throws:
	 */
	public OfficeDocumentListDTO getListDocumentTBHV(int type, String fromDate,
			String toDate, String ext, boolean checkLoadMore, String shopId) {
		OFFICE_DOCUMENT_TABLE officeDocumentTable = new OFFICE_DOCUMENT_TABLE(
				mDB);
		return officeDocumentTable.getListOfficeDocumentTBHV(type, fromDate,
				toDate, ext, checkLoadMore, shopId);
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @param: Tham so cua ham
	 * @return: Ket qua tra ve
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ArrayList<ApParamDTO> getListApparam(Bundle data) {
		// TODO Auto-generated method stub
		ArrayList<ApParamDTO> arrApparam = new ArrayList<ApParamDTO>();
		AP_PARAM_TABLE apparamTable = new AP_PARAM_TABLE(mDB);
		arrApparam = apparamTable.getApparam();
		return arrApparam;
	}

	/**
	 * shop param cho phep xoa vi tri
	 *
	 * @author: DungNX
	 * @param: Bundle
	 * @return: ApParamDTO
	 * @throws:
	 */
	public ShopParamDTO getShopParamAllowClearPosition(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		SHOP_PARAM_TABLE shopparamTable = new SHOP_PARAM_TABLE(mDB);
		ShopParamDTO shopparam  = shopparamTable.getShopParamAllowClearPosition(data);
		return shopparam;
	}

	/**
	 * getCustomerListVisited
	 *
	 * @author: DungNX
	 * @param ext
	 * @return
	 * @return: CustomerListDTO
	 * @throws Exception
	 * @throws:
	 */
	public CustomerVisitedViewDTO getCustomerListVisited(String shopId, int staffId, int page,
			int isGetTotalPage) throws Exception {
		CustomerVisitedViewDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		if (mDB != null) {
			dto = custommerTable.getCustomerVisitedPaging(shopId, staffId,
					page, isGetTotalPage);
		}
		return dto;
	}

	/**
	 * danh sach loai document
	 *
	 * @author: DungNX
	 * @param bundle
	 * @return
	 * @return: ActiveSKUDetailDTO
	 * @throws:
	 */
	public ArrayList<ApParamDTO> getListDocumentType() {
		AP_PARAM_TABLE table = new AP_PARAM_TABLE(mDB);
		return table.getListDocumentType();
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @param cusDebitDetailDto
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	// public boolean createPaymentVansale(OrderViewDTO orderDTO) {
	// PAY_RECEIVED_TABLE payReceivedTable = new PAY_RECEIVED_TABLE(mDB);
	// PAYMENT_DETAIL_TABLE payDetailTable = new PAYMENT_DETAIL_TABLE(mDB);
	// TABLE_ID tableId = new TABLE_ID(mDB);
	// try {
	// mDB.beginTransaction();
	// // generate PAY_RECEIVED_NUMBER
	// // PAY_RECEIVED_NUMBER = chu cai dau tien (CT) + 5 chu cuoi staff_id
	// // +
	// // so cuoi cua pay_received_Id
	// long payReceivedID = tableId
	// .getMaxId(PAY_RECEIVED_TABLE.TABLE_PAY_RECEIVED);
	// String strpayReceivedID = String.valueOf(payReceivedID);
	// String userId = ""
	// + GlobalInfo.getInstance().getProfile().getUserData().id;
	// StringBuilder payReceivedNumber = new StringBuilder();
	// payReceivedNumber.append("CT");
	// if (userId.length() >= 5) {
	// payReceivedNumber.append(userId.substring(userId.length() - 5));
	// } else {
	// String zero = "00000";
	// int lengId = userId.length();
	// payReceivedNumber.append(zero.substring(lengId) + userId);
	// }
	//
	// if (strpayReceivedID.length() >= 8) {
	// payReceivedNumber.append(strpayReceivedID
	// .substring(strpayReceivedID.length() - 8));
	// } else {
	// String zero = "00000000";
	// int lengId = strpayReceivedID.length();
	// payReceivedNumber.append(zero.substring(lengId)
	// + strpayReceivedID);
	// }
	//
	// // update DEBIT
	// DebitDTO debitDTO = new DebitDTO();
	// debitDTO.totalPay = orderDTO.paymentMoney;
	// debitDTO.totalDebit = -(orderDTO.paymentMoney + orderDTO.discount);
	// debitDTO.totalDiscount = orderDTO.discount;
	// DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
	// debitDTO.objectID = orderDTO.debitDto.objectID;
	// debitDTO.objectType = orderDTO.debitDto.objectType;
	// debitDTO.id = debit.checkDebitExist(String
	// .valueOf(orderDTO.orderInfo.customerId));
	// boolean insertOrUpdate = false;
	// if (debitDTO.id > 0) {
	// insertOrUpdate = debit.increaseDebit(debitDTO);
	// } else {
	// debitDTO.id = tableId
	// .getMaxId(DEBIT_TABLE.TABLE_NAME);
	// insertOrUpdate = debit.insert(debitDTO) > 0 ? true : false;
	// }
	// // long debitSucc = debit.updateDebt(orderDTO.debitDto);
	// // send to server
	// if (!insertOrUpdate) {
	// return false;
	// }
	// orderDTO.debitDto = debitDTO;
	// // insert vao PayReceived
	// PayReceivedDTO dto = new PayReceivedDTO();
	// dto.payReceivedNumber = payReceivedNumber.toString();
	// dto.amount = (long) orderDTO.paymentMoney;
	// dto.paymentType = PayReceivedDTO.PAYMENT_TYPE_CASH;
	// dto.customerId = orderDTO.customer.customerId;
	// dto.shopId = GlobalInfo.getInstance().getProfile().getUserData().shopId;
	// dto.receiptType = 0;
	// dto.type = 0;
	// dto.payReceivedID = payReceivedID;
	// dto.createDate = DateUtils.now();
	// dto.createUser = ""
	// + GlobalInfo.getInstance().getProfile().getUserData().userName;
	// // cusDebitDetailDto.payReceivedDto = dto;
	// orderDTO.payReceivedDTO = dto;
	// long paySuccess = payReceivedTable.insert(dto);
	// // send to server
	// if (paySuccess == -1) {
	// return false;
	// }
	//
	// // update DEBIT_DETAIL
	// orderDTO.debitDetailDto.totalPay += orderDTO.paymentMoney;
	// orderDTO.debitDetailDto.remain -= (orderDTO.paymentMoney +
	// orderDTO.discount);
	// orderDTO.debitDetailDto.discountAmount += orderDTO.discount;
	// DEBIT_DETAIL_TABLE debitDetail = new DEBIT_DETAIL_TABLE(mDB);
	// long detaiSucc = debitDetail.updateDebt(orderDTO.debitDetailDto);
	// // send to server
	// if (detaiSucc <= 0) {
	// return false;
	// }
	//
	// // insert PAYMENT_DETAIL
	// long tempPaymentDetailID = tableId
	// .getMaxId(PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL);
	// PaymentDetailDTO paymentDetailDto = new PaymentDetailDTO();
	// paymentDetailDto.paymentDetailID = tempPaymentDetailID;
	// paymentDetailDto.payReceivedID = payReceivedID;
	// paymentDetailDto.amount = (long) orderDTO.paymentMoney;
	// paymentDetailDto.paymentType = "1";
	// paymentDetailDto.status = 1;
	// // ngay chot
	// ShopLockDTO shopLock = GlobalInfo.getInstance().getShopLockDto();
	// String lockDate = "";
	// if (shopLock != null) {
	// lockDate = shopLock.lockDate + " "
	// + DateUtils.formatNow(DateUtils.DATE_TIME_FORMAT_TIME);
	// }
	// paymentDetailDto.payDate = lockDate;
	// paymentDetailDto.discount = (long) orderDTO.discount;
	// paymentDetailDto.createUser = ""
	// + GlobalInfo.getInstance().getProfile().getUserData().id;
	// paymentDetailDto.createDate = DateUtils.now();
	// orderDTO.paymentDetailDto = paymentDetailDto;
	// long paymentSucc = payDetailTable.insert(paymentDetailDto);
	//
	// // send to server
	// if (paymentSucc <= 0) {
	// return false;
	// }
	// mDB.setTransactionSuccessful();
	// } catch (Exception e) {
	// try {
	// throw e;
	// } catch (Exception e1) {
	// }
	// } finally {
	// mDB.endTransaction();
	// }
	//
	// return true;
	// }

	/**
	 * get vanlock
	 *
	 * @author: DungNX
	 * @param: Bundle
	 * @return: ApParamDTO
	 * @throws Exception
	 * @throws:
	 */
	public LockDateDTO getLockDate(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		STOCK_LOCK_TABLE stockLockTable = new STOCK_LOCK_TABLE(mDB);
		data.putString(IntentConstants.INTENT_CREATE_DATE, DateUtils.now());
		LockDateDTO stockLock = stockLockTable.getLockDate(data);
		return stockLock;
	}

	/**
	 * get shoplock
	 *
	 * @author: DungNX
	 * @param: Bundle
	 * @return: ApParamDTO
	 * @throws:
	 */
	public ShopLockDTO getShopLock(Bundle data) {
		// TODO Auto-generated method stub
		SHOP_LOCK_TABLE shopLockTable = new SHOP_LOCK_TABLE(mDB);
		ShopLockDTO shopLockDto = shopLockTable.getShopLock(GlobalInfo
				.getInstance().getProfile().getUserData().getInheritShopId());
		return shopLockDto;
	}

	/**
	 * tao don tra
	 *
	 * @author: DungNX
	 * @param item
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	 */
	public boolean createReturnOrder(SaleOrderViewDTO item,
			OrderViewDTO orderDTO) throws Exception {
		boolean insertSuccess = true;

		long saleOrderId = -1;
		long saleOrderDetailId = -1;
		long shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData()
				.getInheritShopId());
		long staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		String createUser = orderDTO.orderInfo.createUser;
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		SALES_ORDER_DETAIL_TABLE detailTable = new SALES_ORDER_DETAIL_TABLE(mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);
		DEBIT_TABLE debitTable = new DEBIT_TABLE(mDB);
		DEBIT_DETAIL_TEMP_TABLE debitDetailTable = new DEBIT_DETAIL_TEMP_TABLE(
				mDB);

		try {
			mDB.beginTransaction();
			saleOrderId = tableId.getMaxIdTime(SALE_ORDER_TABLE.TABLE_NAME);
			saleOrderDetailId = tableId
					.getMaxIdTime(SALES_ORDER_DETAIL_TABLE.TABLE_NAME);
			// tao don tra
			String strOrderId = String.valueOf(saleOrderId);
			StringBuilder orderNumber = new StringBuilder();
			String userId = ""
					+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

			// tiep dau ngu don tra la CO
			orderNumber.append("CO");
			if (userId.length() >= 5) {
				orderNumber.append(userId.substring(userId.length() - 5));
			} else {
				String zero = "00000";
				int lengId = userId.length();
				orderNumber.append(zero.substring(lengId) + userId);
			}

			if (strOrderId.length() >= 8) {
				orderNumber
						.append(strOrderId.substring(strOrderId.length() - 8));
			} else {
				String zero = "00000000";
				int lengId = strOrderId.length();
				orderNumber.append(zero.substring(lengId) + strOrderId);
			}

			SaleOrderDTO saleOrder = item.saleOrder.cloneThis();
			saleOrder.saleOrderId = saleOrderId;
			saleOrder.orderNumber = orderNumber.toString();
			saleOrder.refOrderNumber = saleOrder.orderNumber;
			saleOrder.orderSource = 2;
			saleOrder.fromSaleOrderId = item.saleOrder.saleOrderId;
			saleOrder.type = 2; // don tra
			saleOrder.orderType = SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE; // vansale
			saleOrder.approved = 0;
			saleOrder.approvedStep = 0;
			saleOrder.synState = 0;
			saleOrder.approvedVan = 1;
			String dateNow = DateUtils.now();
			saleOrder.orderDate = dateNow;
			saleOrder.createDate = dateNow;
			saleOrder.deliveryDate = dateNow;
			saleOrder.quantity = item.saleOrder.quantity;
			saleOrder.setDiscount(item.saleOrder.getDiscount());
			// luu cycleId
			saleOrder.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
			// tinh lai tien, discount, total ... cua don hang do ko tinh tien keyshop
			reCalReturnOrder(item, orderDTO);
			// ko tra thuong keyshop
			saleOrder.isRewardKS = false;
			if (saleOrder != null && orderTable.insert(saleOrder) > 0) {
				orderDTO.orderInfo = saleOrder;
				// cap nhat don goc
				// SALE_ORDER.TYPE cua don hang goc = 0 da tra

				item.saleOrder.type = 0;
				long updateSaleOrder = -1;
				updateSaleOrder = orderTable
						.updateReturnedOrder(item.saleOrder);

				orderDTO.orderTemp = item.saleOrder;

				// thuc hien lay detail va luu detail cua don tra
				List<SaleOrderDetailDTO> listOrderDetail = detailTable
						.getAllDetailOfSaleOrder(saleOrder.fromSaleOrderId);
				// cap nhat lai discount tat ca detail;
				updateDiscountOrderDetail(listOrderDetail, orderDTO.listBuyOrders);
				// reset lai cac mang luu tru
				orderDTO.listBuyOrders = new ArrayList<OrderDetailViewDTO>();
				orderDTO.listPromotionOrders = new ArrayList<OrderDetailViewDTO>();
				boolean insertOrderDetail = false;
				boolean increaseStockTotal = false;
				boolean insertSaleOrderLotSuccess = true;
				boolean insertSaleOrderPromoDetailSuccess = true;
				boolean insertSaleOrderPromotionSuccess = true;
				boolean isVaildDecreasePromoQuanReceived = true;
				boolean insertOrUpdateDebit = false;
				boolean insertDebitDetail = false;
				boolean insertSalePromoMapSuccess = true;
				boolean insertSaleOrderPromoLotSuccess = true;
				boolean insertMapDelta = true;
				boolean isVaildIncreasePromoQuanReceived = true;

				// tuong ung 1 SaleOrderDetailId don goc map voi 1
				// SaleOrderDetailId don tra
				HashMap<Long, SaleOrderDetailDTO> saleOrderDetailMap = new HashMap<Long, SaleOrderDetailDTO>();
				if (updateSaleOrder > 0) {
					STOCK_TOTAL_TABLE stockTable = new STOCK_TOTAL_TABLE(mDB);
					for (SaleOrderDetailDTO tempDetail : listOrderDetail) {
						// ko tinh keyshop
						if(PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != tempDetail.programeType){
							OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
							orderDetail.orderDetailDTO = tempDetail;
							if (tempDetail.isFreeItem == 0) {
								orderDTO.listBuyOrders.add(orderDetail);
							} else {
								orderDTO.listPromotionOrders.add(orderDetail);
							}

							saleOrderDetailMap.put(tempDetail.salesOrderDetailId,
									tempDetail);
							tempDetail.salesOrderDetailId = saleOrderDetailId;
							tempDetail.salesOrderId = saleOrder.saleOrderId;
							tempDetail.createDate = dateNow;
							tempDetail.orderDate = dateNow;
							insertOrderDetail = detailTable.insert(tempDetail) > 0 ? true
									: false;
							if (insertOrderDetail) {
								saleOrderDetailId += 1;
								// tang ton kho thuc te, dap ung
								StockTotalDTO stockTotal = StockTotalDTO
										.createStockTotalInfoReturnOrder(tempDetail);
								if (saleOrder.approved != -1) {
									increaseStockTotal = stockTable
											.increaseStockTotal2(stockTotal);
								}
							} else {
								break;
							}
						}
					}
				}

				if (insertOrderDetail && increaseStockTotal) {
					// lay thong tin SaleOrderLot cua don hang
					SALE_ORDER_LOT_TABLE saleOrderLotTable = new SALE_ORDER_LOT_TABLE(
							mDB);
					List<SaleOrderLotDTO> listSaleOrderLotDTO = saleOrderLotTable
							.getSaleOrderPromoDetailBySaleOrderID(item.saleOrder.saleOrderId);
					List<SaleOrderLotDTO> lstSaleOrderLotRemove = new ArrayList<SaleOrderLotDTO>();
					// luu SaleOrderLot cho don tra
					if (listSaleOrderLotDTO != null
							&& listSaleOrderLotDTO.size() > 0) {
						long saleOrderLotId = tableId
								.getMaxIdTime(SALE_ORDER_LOT_TABLE.TABLE_NAME);
						for (SaleOrderLotDTO saleOrderLotDTO : listSaleOrderLotDTO) {
							// lay newSaleOrderDetailId tu hash map da luu tren
							SaleOrderDetailDTO newSaleOrderDetailId = saleOrderDetailMap
									.get(saleOrderLotDTO.saleOrderDetailId);
							if (PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != newSaleOrderDetailId.programeType) {
								saleOrderLotDTO.saleOrderId = saleOrder.saleOrderId;
								saleOrderLotDTO.saleOrderDetailId = newSaleOrderDetailId.salesOrderDetailId;
								saleOrderLotDTO.discountAmount = newSaleOrderDetailId
										.getDiscountAmount();
								saleOrderLotDTO.saleOrderLotId = saleOrderLotId;
								saleOrderLotDTO.createDate = saleOrder.orderDate;
								// luu cho don tra
								insertSaleOrderLotSuccess = saleOrderLotTable
										.insert(saleOrderLotDTO) > 0 ? true
										: false;
								saleOrderLotId++;
								if (!insertSaleOrderLotSuccess) {
									break;
								}
							}else{
								lstSaleOrderLotRemove.add(saleOrderLotDTO);
							}
						}
					}
					listSaleOrderLotDTO.removeAll(lstSaleOrderLotRemove);
					orderDTO.listSaleOrderLot = (ArrayList<SaleOrderLotDTO>) listSaleOrderLotDTO;
				}

				if (insertSaleOrderLotSuccess) {
					// lay thong tin sale order promo detail
					SALE_ORDER_PROMO_DETAIL_TABLE saleOrderPromoDetailTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
							mDB);
					ArrayList<SaleOrderPromoDetailDTO> listSaleOrderPromoDetailDTO = saleOrderPromoDetailTable
							.getSaleOrderPromoDetailBySaleOrderID(item.saleOrder.saleOrderId);
					ArrayList<SaleOrderPromoDetailDTO> lstSaleOrderPromoDetailRemove = new ArrayList<SaleOrderPromoDetailDTO>();
					// luu sale order promo detail vao cho don tra
					if (listSaleOrderPromoDetailDTO != null
							&& listSaleOrderPromoDetailDTO.size() > 0) {
						// cap nhat lai saleOrderId cua sale order promo detail
						long saleOrderPromoDetailId = tableId
								.getMaxIdTime(SALE_ORDER_PROMO_DETAIL_TABLE.TABLE_NAME);
						for (SaleOrderPromoDetailDTO saleOrderPromoDetailDTO : listSaleOrderPromoDetailDTO) {
							// ko luu thong tin keyshop
							if(PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != saleOrderPromoDetailDTO.programType){
								saleOrderPromoDetailDTO.saleOrderId = saleOrder.saleOrderId;
								saleOrderPromoDetailDTO.orderDate = saleOrder.orderDate;
								// lay newSaleOrderDetailId tu hash map da luu
								// tren
								SaleOrderDetailDTO newSaleOrderDetailId = saleOrderDetailMap
										.get(saleOrderPromoDetailDTO.saleOrderDetailId);
								saleOrderPromoDetailDTO.saleOrderDetailId = newSaleOrderDetailId.salesOrderDetailId;
								saleOrderPromoDetailDTO.saleOrderPromoDetailId = saleOrderPromoDetailId;
								// luu xuong cho don tra
								insertSaleOrderPromoDetailSuccess = saleOrderPromoDetailTable
										.insert(saleOrderPromoDetailDTO) > 0 ? true
										: false;
								saleOrderPromoDetailId++;
								if (!insertSaleOrderPromoDetailSuccess) {
									break;
								}
							}else{
								//add vao mang remove nhung lot lien quan toi keyshop
								lstSaleOrderPromoDetailRemove.add(saleOrderPromoDetailDTO);
							}
						}
					}
					listSaleOrderPromoDetailDTO.removeAll(lstSaleOrderPromoDetailRemove);
					orderDTO.listSaleOrderPromoDetailDTO = listSaleOrderPromoDetailDTO;

					// lay thong tin sale order promo lot
					SALE_ORDER_PROMO_LOT_TABLE saleOrderPromoLot = new SALE_ORDER_PROMO_LOT_TABLE(mDB);
					ArrayList<SaleOrderPromoLotDTO> listSaleOrderPromoLotDTO = saleOrderPromoLot
							.getSaleOrderPromoLotBySaleOrderID(item.saleOrder.saleOrderId);
					List<SaleOrderPromoLotDTO> lstSaleOrderPromoLotRemove = new ArrayList<SaleOrderPromoLotDTO>();
					// luu sale order promo detail vao cho don tra
					if (listSaleOrderPromoLotDTO.size() > 0) {
						// cap nhat lai saleOrderId cua sale order promo detail
						long saleOrderPromoLotId = tableId
								.getMaxIdTime(SALE_ORDER_PROMO_LOT_TABLE.TABLE_NAME);
						for (SaleOrderPromoLotDTO saleOrderPromoLotDTO : listSaleOrderPromoLotDTO) {
								// ko luu thong tin keyshop
							if (PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != saleOrderPromoLotDTO.programType) {
								saleOrderPromoLotDTO.saleOrderId = saleOrder.saleOrderId;
								saleOrderPromoLotDTO.orderDate = saleOrder.orderDate;
								// lay newSaleOrderDetailId tu hash map da luu
								// tren
								SaleOrderDetailDTO newSaleOrderDetailId = saleOrderDetailMap
										.get(saleOrderPromoLotDTO.saleOrderDetailId);
								saleOrderPromoLotDTO.saleOrderDetailId = newSaleOrderDetailId.salesOrderDetailId;
								saleOrderPromoLotDTO.saleOrderPromoLotId = saleOrderPromoLotId;
								// luu xuong cho don tra
								insertSaleOrderPromoLotSuccess = saleOrderPromoLot
										.insert(saleOrderPromoLotDTO) > 0 ? true
										: false;
								saleOrderPromoLotId++;
								if (!insertSaleOrderPromoLotSuccess) {
									break;
								}
							} else {
								lstSaleOrderPromoLotRemove.add(saleOrderPromoLotDTO);
							}
						}
					}
					listSaleOrderPromoLotDTO.removeAll(lstSaleOrderPromoLotRemove);
					orderDTO.listSaleOrderPromoLot = listSaleOrderPromoLotDTO;

					// lay thong tin sale order promo map
					SALE_PROMO_MAP_TABLE saleOrderPromoMapTable = new SALE_PROMO_MAP_TABLE(
							mDB);
					ArrayList<SalePromoMapDTO> listSaleOrderPromoMaplDTO = saleOrderPromoMapTable.getSalePromoMap(item.saleOrder.saleOrderId);
					ArrayList<SalePromoMapDTO> lstSalePromoMapRemove = new ArrayList<SalePromoMapDTO>();
					// luu sale order promo map vao cho don tra
					if (listSaleOrderPromoMaplDTO != null
							&& listSaleOrderPromoMaplDTO.size() > 0) {
						// cap nhat lai saleOrderId cua sale promo map
						long salePromoMapId = tableId
								.getMaxIdTime(SALE_PROMO_MAP_TABLE.TABLE_NAME);
						for (SalePromoMapDTO saleOrderPromoMapDTO : listSaleOrderPromoMaplDTO) {
							// ko luu thong tin keyshop
							// lay newSaleOrderDetailId tu hash map da luu tren
							SaleOrderDetailDTO newSaleOrderDetailId = saleOrderDetailMap
									.get(saleOrderPromoMapDTO.saleOrderDetailId);
							if(PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != newSaleOrderDetailId.programeType){
								saleOrderPromoMapDTO.saleOrderId = saleOrder.saleOrderId;
								saleOrderPromoMapDTO.saleOrderDetailId = newSaleOrderDetailId.salesOrderDetailId;
								saleOrderPromoMapDTO.salePromoMapId= salePromoMapId;
								// luu xuong cho don tra
								insertSalePromoMapSuccess = saleOrderPromoMapTable
										.insert(saleOrderPromoMapDTO) > 0 ? true
												: false;
										salePromoMapId++;
								if (!insertSalePromoMapSuccess) {
									break;
								}
							}else{
								lstSalePromoMapRemove.add(saleOrderPromoMapDTO);
							}
						}
					}
					listSaleOrderPromoMaplDTO.removeAll(lstSalePromoMapRemove);
					orderDTO.listSalePromomapDTO = listSaleOrderPromoMaplDTO;
				}

				ArrayList<SaleOrderPromotionDTO> listSaleOrderPromotionDTO;
				if (insertSaleOrderPromoDetailSuccess && insertSalePromoMapSuccess && insertSaleOrderPromoLotSuccess) {
					// lay thong tin saleOrderPromotion cua don hang goc
					SALE_ORDER_PROMOTION_TABLE saleOrderPromotionTable = new SALE_ORDER_PROMOTION_TABLE(
							mDB);
					listSaleOrderPromotionDTO = saleOrderPromotionTable
							.getSaleOrderPromotionBySaleOrderID(item.saleOrder.saleOrderId);
					// luu thong tin SaleOrderPromotion cho don tra
					if (listSaleOrderPromotionDTO != null
							&& listSaleOrderPromotionDTO.size() > 0) {
						long saleOrderPromotionId = tableId.getMaxIdTime(SALE_ORDER_PROMOTION_TABLE.TABLE_NAME);
						for (SaleOrderPromotionDTO saleOrderPromotionDTO : listSaleOrderPromotionDTO) {
							saleOrderPromotionDTO.saleOrderId = saleOrder.saleOrderId;
							saleOrderPromotionDTO.saleOrderPromotionId = saleOrderPromotionId;
							saleOrderPromotionDTO.orderDate = saleOrder.orderDate;
							saleOrderPromotionDTO.createDate = saleOrder.orderDate;
							// luu xuong cho don tra
							insertSaleOrderPromotionSuccess = saleOrderPromotionTable
									.insert(saleOrderPromotionDTO) > 0 ? true
									: false;
							saleOrderPromotionId++;
							if (!insertSaleOrderPromotionSuccess) {
								break;
							}
						}
					}
					orderDTO.listSaleOrderPromotionDTO = listSaleOrderPromotionDTO;
					// ----- giam so suat
					if (insertSaleOrderPromoDetailSuccess
							&& insertSaleOrderPromotionSuccess
							&& listSaleOrderPromotionDTO != null && insertSalePromoMapSuccess) {
//						HashMap<Long, QuantityRevicevedDTO> mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();
//						// nhap chung nhieu group cua 1 chuong trinh lai
//						for (SaleOrderPromotionDTO quantityReceived : listSaleOrderPromotionDTO) {
//							QuantityRevicevedDTO quanDto = mapQuantityReviceved
//									.get(quantityReceived.promotionProgramId);
//							if (quanDto == null) {
//								quanDto = new QuantityRevicevedDTO(
//										quantityReceived.promotionProgramId,
//										quantityReceived.promotionProgramCode);
//								mapQuantityReviceved.put(
//										quantityReceived.promotionProgramId,
//										quanDto);
//							}
//							// cong them so suat
//							quanDto.setQuantityReviceved(quanDto
//									.getQuantityReviceved() + quantityReceived.quantityReceived);
//						}
//						PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(
//								mDB);
//						PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
//						PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(
//								mDB);
//
//						// tru so suat
//						// tru so suat tung chuong trinh
//						for (QuantityRevicevedDTO quanDto : mapQuantityReviceved
//								.values()) {
//							PromotionShopMapDTO promotionShopMapDTO = psmTable
//									.getPromotionShopMapByPromotionProgramId(
//											quanDto.getProId(), GlobalInfo
//											.getInstance()
//											.getProfile()
//											.getUserData().getInheritShopId());
//							if (promotionShopMapDTO != null) {
//								quanDto.setShopMapID(promotionShopMapDTO.promotionShopMapId);
//
//							}
//							PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable
//									.getPromotionShopMapByPromotionShopMapID(
//											orderDTO.orderInfo.customerId,
//											orderDTO.orderInfo.shopId,
//											promotionShopMapDTO.promotionShopMapId);
//							if (promotionCustomerMapDTO != null) {
//								quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
//							}
//							PromotionStaffMapDTO promotionStaffMapDTO = pstmTable
//									.getPromotionStaffMapByPromotionShopMapID(
//											orderDTO.orderInfo.staffId,
//											orderDTO.orderInfo.shopId,
//											promotionShopMapDTO.promotionShopMapId);
//							if (promotionStaffMapDTO != null) {
//								quanDto.setStaffMapId(promotionStaffMapDTO.promotionStaffMapId);
//							}
//
//							// giam so xuat shop
//							if (quanDto.getShopMapID() > 0) {
//								isVaildDecreasePromoQuanReceived = psmTable
//										.decreaseQuantityRecevie(
//												quanDto.getShopMapID(),
//												quanDto.getQuantityReviceved());
//							}
//							if (isVaildDecreasePromoQuanReceived
//									&& quanDto.getStaffMapId() > 0) {
//								isVaildDecreasePromoQuanReceived = pcmTable
//										.decreaseStaffQuantityRecevie(
//												quanDto.getStaffMapId(),
//												quanDto.getQuantityReviceved());
//							}
//							if (isVaildDecreasePromoQuanReceived
//									&& quanDto.getCustomerMapId() > 0) {
//								isVaildDecreasePromoQuanReceived = pcmTable
//										.decreaseQuantityRecevie(
//												quanDto.getCustomerMapId(),
//												quanDto.getQuantityReviceved());
//							}
//							// neu fail thi break
//							if (!isVaildDecreasePromoQuanReceived) {
//								break;
//							}
//						}
//						orderDTO.listIncreasePromoQuantityReceived = mapQuantityReviceved
//								.values();
						//get quantity received

						if (orderDTO.orderInfo.approved != -1) {
							// xu li thong tin so suat nhan duoc cua don hang
							orderDTO.productQuantityReceivedList = SQLUtils.getInstance().getQuantityReceivedForEdit(orderDTO.orderInfo.saleOrderId);
							for (SaleOrderPromotionDTO quantityReceived : orderDTO.productQuantityReceivedList) {
								//So suat tong
								QuantityRevicevedDTO quanDto =  orderDTO.mapQuantityReviceved.get(quantityReceived.promotionProgramId);
								if (quanDto == null) {
									quanDto = new QuantityRevicevedDTO(quantityReceived.promotionProgramId, quantityReceived.promotionProgramCode);
									quanDto.setProName(quantityReceived.promotionProgramName);
									 orderDTO.mapQuantityReviceved.put(quantityReceived.promotionProgramId, quanDto);
								}

								//Reset gia tri khi thay doi so luong do doi sp tuy chon
								quantityReceived.quantityReceived = quantityReceived.quantityReceivedMax;
								quantityReceived.numReceived = quantityReceived.numReceivedMax;
								quantityReceived.amountReceived = quantityReceived.amountReceivedMax;

								// cong them so suat
								quanDto.setQuantityReviceved(quanDto.getQuantityReviceved()
										+ quantityReceived.quantityReceived);
								quanDto.setQuantityRevicevedMax(quanDto.getQuantityReviceved());
								// luu thong tin so tien/ so luong nhan duoc cua mot CTKM
								quanDto.setNumReceived(quanDto.getNumReceived() + quantityReceived.numReceived);
								quanDto.setAmountReceived(quanDto.getAmountReceived()
										+ quantityReceived.amountReceived);
							}
							PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(mDB);
							PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(mDB);
							PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(mDB);

							PROMOTION_MAP_DELTA_TABLE pmdTable = new PROMOTION_MAP_DELTA_TABLE(mDB);
							long promotionMapDeltaId = tableId.getMaxIdTime(PROMOTION_MAP_DELTA_TABLE.TABLE_NAME);
							//Insert vao bang promotion_map_delta
							long insertPromotionMapDeltaId = 0;

							for(QuantityRevicevedDTO quanDto : orderDTO.mapQuantityReviceved.values()) {
								// xu li gan cac thong tin
								PromotionShopMapDTO promotionShopMapDTO = psmTable.getPromotionShopMapByPromotionProgramId(
										quanDto.getProId(), GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
								if(promotionShopMapDTO != null){
									quanDto.setShopMapID(promotionShopMapDTO.promotionShopMapId);
									PromotionStaffMapDTO promotionStaffMapDTO = pstmTable.getPromotionStaffMapByPromotionShopMapID(
											orderDTO.orderInfo.staffId, orderDTO.orderInfo.shopId, promotionShopMapDTO.promotionShopMapId);
									if(promotionStaffMapDTO != null){
										quanDto.setStaffMapId(promotionStaffMapDTO.promotionStaffMapId);
									}
									PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable.getPromotionShopMapByPromotionShopMapID(
											orderDTO.orderInfo.customerId, orderDTO.orderInfo.shopId, promotionShopMapDTO.promotionShopMapId);
									if(promotionCustomerMapDTO != null){
										quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);
									}

								}
								quanDto.setPromotionMapDeltaId(promotionMapDeltaId);
								quanDto.setFromObjectId(orderDTO.orderInfo.saleOrderId);
								quanDto.setOrderAction(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_ACTION_INSERT);
								quanDto.setSource(QuantityRevicevedDTO.PROMOTION_MAP_DELTA_SOURCE_MOBILE);
								quanDto.setShopId(shopId);
								quanDto.setStaffId(staffId);
								quanDto.setCustomerId(orderDTO.orderInfo.customerId);
								quanDto.setCreateDate(orderDTO.orderInfo.orderDate);
								quanDto.setCreateUser(createUser);
								// do la don tra nen cap nhat vao bang delta so am
								quanDto.setQuantityReviceved(-quanDto.getQuantityReviceved());
								quanDto.setAmountReceived(-quanDto.getAmountReceived());
								quanDto.setNumReceived(-quanDto.getNumReceived());
								insertPromotionMapDeltaId = pmdTable.insert(quanDto);

								insertMapDelta = insertPromotionMapDeltaId > 0 ? true: false;

								if (insertMapDelta) {
									promotionMapDeltaId++;
								} else {
									break;
								}

								// tang so xuat shop
								if (quanDto.getShopMapID() > 0) {
									isVaildIncreasePromoQuanReceived = psmTable.increaseQuantityRecevieTotal(
													quanDto.getShopMapID(), quanDto.getQuantityReviceved(), quanDto.getNumReceived(),quanDto.getAmountReceived());
								}

								if (isVaildIncreasePromoQuanReceived
										&& quanDto.getStaffMapId() > 0) {
									isVaildIncreasePromoQuanReceived = pcmTable.increaseQuantityRecevieTotal(
													quanDto.getStaffMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
								}

								if (isVaildIncreasePromoQuanReceived
										&& quanDto.getCustomerMapId() > 0) {
									isVaildIncreasePromoQuanReceived = pcmTable.increaseQuantityRecevieTotal(
													quanDto.getCustomerMapId(), quanDto.getQuantityReviceved(),quanDto.getNumReceived(),quanDto.getAmountReceived());
								}

								// neu fail thi break
								if (!isVaildIncreasePromoQuanReceived) {
									break;
								}
							}

							// cap nhat list so suat phai tru
							orderDTO.listIncreasePromoQuantityReceived = orderDTO.mapQuantityReviceved.values();
						}
					}
					// ----- giam so suat
				}

				// if (insertSaleOrderPromotionSuccess &&
				// isVaildDecreasePromoQuanReceived) {
				// RPT_CTTL_PAY_TABLE rptCttlPayTable = new
				// RPT_CTTL_PAY_TABLE(mDB);
				// ArrayList<RptCttlPayDTO> listRptCttlPay = new
				// ArrayList<RptCttlPayDTO>();
				// listRptCttlPay =
				// rptCttlPayTable.getRptCttlPayBySaleOrderId(item.saleOrder.saleOrderId);
				//
				// long rptCttlPayId =
				// tableId.getMaxId(RPT_CTTL_PAY_TABLE.TABLE_NAME);
				// for(RptCttlPayDTO cttlPay : listRptCttlPay) {
				// cttlPay.saleOrderId = saleOrder.saleOrderId;
				// cttlPay.rptCttlPayId = rptCttlPayId
				// }
				// }

				boolean insertRptCttlPay = true;
				boolean insertRptCttlDetailPay = true;
				if (insertSaleOrderPromotionSuccess
						&& isVaildDecreasePromoQuanReceived) {
					RPT_CTTL_PAY_TABLE rptCttlPayTable = new RPT_CTTL_PAY_TABLE(
							mDB);
					RPT_CTTL_DETAIL_PAY_TABLE rptCttlDetailPayTable = new RPT_CTTL_DETAIL_PAY_TABLE(
							mDB);
					orderDTO.listRptCttPay = rptCttlPayTable
							.getRptCttlPayBySaleOrderId(item.saleOrder.saleOrderId);
					long rptCttlPayId = tableId
							.getMaxIdTime(RPT_CTTL_PAY_TABLE.TABLE_NAME);
					long rptCttlDetailPayId = tableId
							.getMaxIdTime(RPT_CTTL_DETAIL_PAY_TABLE.TABLE_NAME);
					// insert cttl pay cua KM tich luy
					for (RptCttlPayDTO cttlPay : orderDTO.listRptCttPay) {
						cttlPay.rptCttlPayId = rptCttlPayId;
						cttlPay.saleOrderId = saleOrderId;
						cttlPay.totalQuantityPayPromotion = -cttlPay.totalQuantityPayPromotion;
						cttlPay.totalAmountPayPromotion = -cttlPay.totalAmountPayPromotion;
						cttlPay.createDate = dateNow;
						insertRptCttlPay = rptCttlPayTable.insert(cttlPay) > 0 ? true
								: false;
						if (insertRptCttlPay) {
							for (RptCttlDetailPayDTO payCttlPayDetail : cttlPay.rptCttlDetailList) {
								payCttlPayDetail.rptCttlDetailPayId = rptCttlDetailPayId;
								payCttlPayDetail.rptCttlPayId = rptCttlPayId;
								payCttlPayDetail.quantityPromotion = -payCttlPayDetail.quantityPromotion;
								payCttlPayDetail.amountPromotion = -payCttlPayDetail.amountPromotion;
								payCttlPayDetail.createDate = dateNow;

								insertRptCttlDetailPay = rptCttlDetailPayTable
										.insert(payCttlPayDetail) > 0 ? true
										: false;

								if (insertRptCttlDetailPay) {
									rptCttlDetailPayId++;
								} else {
									break;
								}
							}
							rptCttlPayId++;
						} else {
							break;
						}
					}
				}

				// ghi no
				if (insertRptCttlPay && insertRptCttlDetailPay) {
					// cap nhat no cua NPP voi KH
					DebitDTO debitDto = new DebitDTO();
					debitDto.objectID = String
							.valueOf(item.customer.customerId);
					debitDto.objectType = "3";

					// Insert vao bang debit_detail
					DebitDetailDTO debitDetailDto = new DebitDetailDTO();
					debitDetailDto.debitDetailID = tableId
							.getMaxIdTime(DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);
					debitDetailDto.fromObjectID = saleOrder.saleOrderId;
					debitDetailDto.shopId = saleOrder.shopId;
					debitDetailDto.customerId = saleOrder.customerId;
					debitDetailDto.createDate = dateNow;
					debitDetailDto.debitDate = item.saleOrder.orderDate;
					debitDetailDto.amount = saleOrder.getAmount() * (-1);
					debitDetailDto.discount = saleOrder.getDiscount() * (-1);
					debitDetailDto.total = saleOrder.getTotal() * (-1);
					debitDetailDto.totalPay = 0;
					debitDetailDto.remain = debitDetailDto.total;
					debitDetailDto.createUser = saleOrder.createUser;
					debitDetailDto.type = 5; // giam no do KH tra hang
					debitDetailDto.fromObjectNumber = saleOrder.orderNumber;
					debitDetailDto.staffID = saleOrder.staffId;
					debitDetailDto.orderType = saleOrder.orderType;
					debitDetailDto.saleDate = item.saleOrder.orderDate;
					debitDetailDto.returnDate = saleOrder.orderDate;

					debitDto.totalAmount = debitDetailDto.total;
					debitDto.totalDebit = debitDetailDto.total;
					// debitDto.maxDebitAmount =
					// item.customer.getMaxDebitAmount();
					// debitDto.maxDebitDate = item.customer.getMaxDebitDate();
					debitDto.id = debitTable.checkDebitExist(String
							.valueOf(saleOrder.customerId));
					// debitDto.totalDiscount = 0;

					debitDetailDto.debitID = debitDto.id;
					insertDebitDetail = debitDetailTable.insert(debitDetailDto) > 0 ? true
							: false;
					orderDTO.debitDto = debitDto;
					orderDTO.debitDetailDto = debitDetailDto;

					if (debitDto.id > 0) {
						orderDTO.debitIdExist = debitDto.id;
						insertOrUpdateDebit = debitTable
								.increaseDebit(debitDto);
					} else {
						debitDto.id = tableId.getMaxIdTime(DEBIT_TABLE.TABLE_NAME);
						insertOrUpdateDebit = debitTable.insert(debitDto) > 0 ? true
								: false;
					}
				}

				if (insertOrderDetail && increaseStockTotal
						&& insertSaleOrderLotSuccess
						&& insertSaleOrderPromoDetailSuccess
						&& insertSaleOrderPromotionSuccess
						&& isVaildDecreasePromoQuanReceived
						&& insertOrUpdateDebit && insertDebitDetail && insertSalePromoMapSuccess && insertSaleOrderPromoLotSuccess) {
					insertSuccess = true;
				} else {
					insertSuccess = false;
				}
			} else {
				insertSuccess = false;
			}

			if (insertSuccess) {
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			insertSuccess = false;
			throw e;
		} finally {
			mDB.endTransaction();
		}

		return insertSuccess;
	}

	/**
	 * cap nhat trang thai chot ngay
	 *
	 * @author: ThangNV31
	 * @param dto
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public long insertStockState(LockDateDTO dto) throws Exception {
		long returnCode = -1;
		if (dto != null) {
			try {
				dto.createDate = DateUtils.now();
				dto.shopId = Long.parseLong(GlobalInfo.getInstance()
						.getProfile().getUserData().getInheritShopId());
				dto.staffId = GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId();
				Bundle data = new Bundle();
				data.putString(IntentConstants.INTENT_CREATE_DATE, ""
						+ dto.createDate);
				data.putString(IntentConstants.INTENT_SHOP_ID, "" + dto.shopId);
				data.putString(IntentConstants.INTENT_STAFF_ID, ""
						+ dto.staffId);

				mDB.beginTransaction();
				STOCK_LOCK_TABLE table = new STOCK_LOCK_TABLE(mDB);
				LockDateDTO exist = table.getLockDate(data);
				if (exist.stockLockID > 0) {
					dto.stockLockID = exist.stockLockID;
					dto.updateDate = DateUtils.now();
					returnCode = table.updateStockState(dto);
				} else {
					returnCode = table.insert(dto);
				}
				mDB.setTransactionSuccessful();
			} catch (Exception e) {
				throw e;
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * tao don thanh toan
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: boolean
	 * @throws:
	 * @param cusDebitDetailDto
	 * @return
	 */
	public boolean createPayReceived(CustomerDebitDetailDTO cusDebitDetailDto) {
		PAY_RECEIVED_TEMP_TABLE payReceivedTempTable = new PAY_RECEIVED_TEMP_TABLE(
				mDB);
		PAYMENT_DETAIL_TEMP_TABLE payDetailTempTable = new PAYMENT_DETAIL_TEMP_TABLE(
				mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);
		try {
			mDB.beginTransaction();
			// generate PAY_RECEIVED_NUMBER
			// PAY_RECEIVED_NUMBER = chu cai dau tien (CT) + 5 chu cuoi staff_id
			// +
			// so cuoi cua pay_received_Id
			// long payReceivedID = tableId
			// .getMaxId(PAY_RECEIVED_TABLE.TABLE_NAME);
			long payReceivedtempID = tableId
					.getMaxIdTime(PAY_RECEIVED_TEMP_TABLE.TABLE_NAME);
			// payReceivedtempID =
			// payReceivedID>payReceivedtempID?payReceivedID:payReceivedtempID;
			String strpayReceivedID = String.valueOf(payReceivedtempID);
			String userId = ""
					+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			StringBuilder payReceivedNumber = new StringBuilder();
			payReceivedNumber.append("CT");
			if (userId.length() >= 5) {
				payReceivedNumber.append(userId.substring(userId.length() - 5));
			} else {
				String zero = "00000";
				int lengId = userId.length();
				payReceivedNumber.append(zero.substring(lengId) + userId);
			}

			if (strpayReceivedID.length() >= 8) {
				payReceivedNumber.append(strpayReceivedID
						.substring(strpayReceivedID.length() - 8));
			} else {
				String zero = "00000000";
				int lengId = strpayReceivedID.length();
				payReceivedNumber.append(zero.substring(lengId)
						+ strpayReceivedID);
			}

			// update DEBIT
			DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
			long debitSucc = debit.updateDebt(cusDebitDetailDto.debitDTO);
			// send to server
			if (debitSucc <= 0) {
				return false;
			}

			cusDebitDetailDto.currentPayReceivedDTO.payReceivedNumber = payReceivedNumber
					.toString();
			cusDebitDetailDto.currentPayReceivedDTO.createDate = DateUtils
					.now();
			cusDebitDetailDto.currentPayReceivedDTO.createUser = GlobalInfo
					.getInstance().getProfile().getUserData().getUserName();
			cusDebitDetailDto.currentPayReceivedDTO.staffId = GlobalInfo
					.getInstance().getProfile().getUserData().getInheritId();
			cusDebitDetailDto.currentPayReceivedDTO.payReceivedID = payReceivedtempID;
			cusDebitDetailDto.currentPayReceivedDTO.paymentStatus = 1;
			cusDebitDetailDto.currentPayReceivedDTO.payerName = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserName();
			cusDebitDetailDto.currentPayReceivedDTO.payerAddress = GlobalInfo.getInstance().getProfile()
					.getUserData().getAddress();

			long paySuccess = payReceivedTempTable
					.insert(cusDebitDetailDto.currentPayReceivedDTO);
			// send to server
			if (paySuccess == -1) {
				return false;
			}

			// long PaymentDetailID = tableId
			// .getMaxId(PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL);
			long tempPaymentDetailID = tableId.getMaxIdTime(PAYMENT_DETAIL_TEMP_TABLE.TABLE_PAYMENT_TEMP_DETAIL);
			// tempPaymentDetailID = (PaymentDetailID > tempPaymentDetailID ?
			// PaymentDetailID : tempPaymentDetailID);
			for (int i = 0; i < cusDebitDetailDto.arrList.size(); i++) {

				CustomerDebitDetailItem customerDebitDetailItem = cusDebitDetailDto.arrList
						.get(i);

				if (customerDebitDetailItem.isWouldPay) {
					DEBIT_DETAIL_TEMP_TABLE debitDetail = new DEBIT_DETAIL_TEMP_TABLE(
							mDB);
					long detaiSucc = debitDetail
							.updateDebt(customerDebitDetailItem.debitDetailDTO);
					if (detaiSucc <= 0) {
						return false;
					}

					customerDebitDetailItem.currentPaymentDetailDTO.paymentDetailID = tempPaymentDetailID;
					customerDebitDetailItem.currentPaymentDetailDTO.payReceivedID = payReceivedtempID;
					customerDebitDetailItem.currentPaymentDetailDTO.paymentType = "1";
					customerDebitDetailItem.currentPaymentDetailDTO.status = 1;
					customerDebitDetailItem.currentPaymentDetailDTO.payDate = DateUtils
							.now();
					customerDebitDetailItem.currentPaymentDetailDTO.createUser = ""
							+ GlobalInfo.getInstance().getProfile()
									.getUserData().getUserName();
					customerDebitDetailItem.currentPaymentDetailDTO.staffId = GlobalInfo
							.getInstance().getProfile().getUserData().getInheritId();
					customerDebitDetailItem.currentPaymentDetailDTO.createDate = DateUtils
							.now();
					customerDebitDetailItem.currentPaymentDetailDTO.saleOrderId = customerDebitDetailItem.debitDetailDTO.fromObjectID;
					customerDebitDetailItem.currentPaymentDetailDTO.paymentStatus = 1;
					long paymentSucc = payDetailTempTable
							.insert(customerDebitDetailItem.currentPaymentDetailDTO);
					tempPaymentDetailID = paymentSucc + 1;
					// send to server
					if (paymentSucc <= 0) {
						return false;
					}
				}
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
			}
		} finally {
			mDB.endTransaction();
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @param e
	 * @return
	 * @return: DebitDTO
	 * @throws:
	 */
	public DebitDTO getCustomerDebit(ActionEvent e) {
		DebitDTO result = null;
		CUSTOMER_TABLE customerTable = new CUSTOMER_TABLE(mDB);
		DEBIT_TABLE debitTable = new DEBIT_TABLE(mDB);
		Bundle b = (Bundle) e.viewData;
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		CustomerDTO cusDTO = (CustomerDTO) e.userData;
		try {
			customerTable.setDebitInfoCustomer(cusDTO);
			result = debitTable.getDebitByCustomerID(cusDTO.customerId);
			result.maxDebitDays = debitTable.getMaxDaysDebitCustomer(
					cusDTO.customerId, shopId);
		} catch (Exception ex) {
			result = null;
		}

		return result;
	}



	public ArrayList<String> getListShopPrivilege() {
		ArrayList<String> result = null;
		Cursor c = null;
		try {
			String staffId = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritId());

			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    oa.SHOP_ID	AS SHOP_ID");
			sqlObject.append("	FROM	");
			sqlObject.append("	    role_user ru,	");
			sqlObject.append("	    role_permission_map rpm,	");
			sqlObject.append("	    ORG_ACCESS oa	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	    1=1	");
			sqlObject.append("	    AND ru.USER_ID = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	    AND oa.STATUS = 1	");
			sqlObject.append("	    AND rpm.STATUS = 1	");
			sqlObject.append("	    AND ru.STATUS = 1	");
			sqlObject.append("	    AND ru.ROLE_ID = rpm.ROLE_ID	");
			sqlObject.append("	    AND rpm.PERMISSION_ID = oa.PERMISSION_ID	");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					result = new ArrayList<String>();
					do {
						String shopId = CursorUtil.getString(c, "SHOP_ID");
						result.add(shopId);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getListShopPrivilege", e.getMessage());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	public Cursor rawQueries(String sqlQuery, ArrayList<String> params) {
		String[] strParams = null;
		if (params != null) {
			strParams = new String[params.size()];
			for (int i = 0, s = params.size(); i < s; i++) {
				strParams[i] = params.get(i);
			}
		}
		return mDB.rawQuery(sqlQuery, strParams);
	}

	/**
	 * Lay danh sach id nhan vien thuoc quan li cua giam sat
	 * @author: hoanpd1
	 * @since: 16:52:44 14-04-2015
	 * @return: String
	 * @throws:
	 * @param staffOwnerId
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public String getListStaffOfSupervisor(String staffOwnerId, String shopId)
			throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		return staff.getListStaffOfSupervisor(staffOwnerId, shopId);
	}

	/**
	 * get list Supervisor of ASM
	 *
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 09:40:01 11 Aug 2014
	 * @return: String
	 * @throws:
	 * @param staffIdASM
	 * @return
	 */
	public String getListSupervisorOfASM(String staffIdASM) {
		ArrayList<String> listStaffId = null;
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT S.STAFF_ID AS STAFF_ID ");
			sql.append("FROM   PARENT_STAFF_MAP PS, ");
			sql.append("       STAFF S, ");
			sql.append("       CHANNEL_TYPE CT ");
			sql.append("WHERE  PS.PARENT_STAFF_ID = ? ");
			params.add(staffIdASM);
			sql.append("       AND S.STAFF_ID = PS.STAFF_ID ");
			sql.append("       AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID ");
			sql.append("       AND CT.TYPE = 2 "); // Loai Nhan vien
			sql.append("       AND CT.OBJECT_TYPE IN ( ? ) "); // Loai GSNPP
			params.add(String.valueOf(UserDTO.TYPE_SUPERVISOR));
			sql.append("       AND PS.STATUS = 1 ");
			sql.append("       AND CT.STATUS = 1 ");
			// chuyen don vi GS xuong NPP
			// sql.append("       AND s.shop_id = ? ");
			// params.add(GlobalInfo.getInstance().getProfile().getUserData().shopId);
			sql.append("       AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1) ");

			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					listStaffId = new ArrayList<String>();
					do {
						String staffId = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(staffId);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getListSupervisorOfASM", e.getMessage());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		String result = "";
		if (listStaffId != null) {
			result = TextUtils.join(",", listStaffId);
		}

		return result;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @param bundle
	 * @return
	 * @return: CustomerListItem
	 * @throws Exception
	 * @throws:
	 */
	public CustomerListItem getCustomerInfoVisitTraining(Bundle bundle)
			throws Exception {
		CustomerListItem dto = null;
		try {
			if (mDB != null) {
				dto = new RPT_SALE_RESULT_DETAIL_TABLE(mDB)
						.getGsnppTrainingResultDayReport(bundle);
			}
		} catch (Exception e) {
			MyLog.e("getCustomerInfoVisitTraining", "fail", e);
			throw e;
		} finally {
		}
		return dto;
	}

	/**
	 * lay thong tin shop
	 *
	 * @author: DungNX
	 * @param shopId
	 * @return
	 * @return: ShopDTO
	 * @throws Exception
	 * @throws:
	 */
	public ShopDTO getShopInfo(String shopId) throws Exception {
		ShopDTO dto = null;
		try {
			if (mDB != null) {
				SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
				dto = shopTable.getShopInfo(shopId);
			}
		} catch (Exception e) {
			MyLog.e("getShopInfo", "fail", e);
			throw e;
		} finally {
		}
		return dto;
	}

	/**
	 * lay thong tin shop
	 *
	 * @author: DungNX
	 * @param shopOwnerID
	 * @return
	 * @return: ShopDTO
	 * @throws Exception
	 * @throws:
	 */
	public ArrayList<ShopDTO> getListShopInfo(String listShopId)
			throws Exception {
		ArrayList<ShopDTO> dto = null;
		try {
			if (mDB != null) {
				SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
				dto = shopTable.getListShopInfo(listShopId);
			}
		} catch (Exception e) {
			throw e;
		} finally {
		}
		return dto;
	}

	/**
	 * cap nhat trang thai don hang, lay trang thai tu synstate cua log_table
	 *
	 * @param notifyDTO
	 */
	public void updateSaleOrderSynstate(NotifyOrderDTO notifyDTO)
			throws Exception {
		SALE_ORDER_TABLE saleOrderTable = new SALE_ORDER_TABLE(mDB);
		for (int i = 0; i < notifyDTO.listOrderInLog.size(); i++) {
			try {
				saleOrderTable
						.updateState(notifyDTO.listOrderInLog.get(i).tableId,
								Integer.parseInt(notifyDTO.listOrderInLog
										.get(i).state));
			} catch (Exception e) {
				MyLog.e("updateSaleOrderSynstate", "fail", e);
			}
		}
	}

	/**
	 * danh sach don hang tra lai
	 *
	 * @author cuonglt3
	 * @param debitId
	 * @return
	 */
	public CustomerDebitDetailDTO requestDebitDetailReturn(long debitId) {
		DEBIT_DETAIL_TEMP_TABLE debit = new DEBIT_DETAIL_TEMP_TABLE(mDB);
		CustomerDebitDetailDTO debitDetail = debit
				.requestDebitDetailReturn(debitId);
		return debitDetail;
	}

	/**
	 * tao phieu chi
	 *
	 * @author cuonglt3
	 * @param cusDebitDetailDto
	 * @return
	 */
	public boolean saveAndSendPaymentDetail(
			CustomerDebitDetailDTO cusDebitDetailDto) {
		PAY_RECEIVED_TEMP_TABLE payReceivedTempTable = new PAY_RECEIVED_TEMP_TABLE(
				mDB);
		PAYMENT_DETAIL_TEMP_TABLE payDetailTable = new PAYMENT_DETAIL_TEMP_TABLE(
				mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);
		try {
			mDB.beginTransaction();
			// generate PAY_RECEIVED_NUMBER
			// PAY_RECEIVED_NUMBER = chu cai dau tien (CT) + 5 chu cuoi staff_id
			// +
			// so cuoi cua pay_received_Id
			long payReceivedtempID = tableId.getMaxIdTime(PAY_RECEIVED_TEMP_TABLE.TABLE_NAME);
			String userId = ""
					+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			StringBuilder payReceivedNumber = new StringBuilder();
			payReceivedNumber.append("C");
			int newPayReceivedId = payReceivedTempTable
					.getNumPayReceivedCreateInDay();
			String dateNow = DateUtils
					.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE_PAY_RECEIVED);
			payReceivedNumber.append("-" + dateNow);
			payReceivedNumber.append("-" + newPayReceivedId);

			// update DEBIT
			DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
			long debitSucc = debit.updateDebt(cusDebitDetailDto.debitDTO);
			// send to server
			if (debitSucc <= 0) {
				return false;
			}

			cusDebitDetailDto.currentPayReceivedDTO.payReceivedNumber = payReceivedNumber
					.toString();
			cusDebitDetailDto.currentPayReceivedDTO.createDate = DateUtils
					.now();
			cusDebitDetailDto.currentPayReceivedDTO.createUser = GlobalInfo
					.getInstance().getProfile().getUserData().getUserName();
			cusDebitDetailDto.currentPayReceivedDTO.payReceivedID = payReceivedtempID;
			cusDebitDetailDto.currentPayReceivedDTO.staffId = GlobalInfo
					.getInstance().getProfile().getUserData().getInheritId();
			cusDebitDetailDto.currentPayReceivedDTO.paymentStatus = 1;
			cusDebitDetailDto.currentPayReceivedDTO.payerName = GlobalInfo.getInstance().getProfile().getUserData().getUserName();
			cusDebitDetailDto.currentPayReceivedDTO.payerAddress = GlobalInfo.getInstance().getProfile().getUserData().getAddress();
			long paySuccess = payReceivedTempTable
					.insert(cusDebitDetailDto.currentPayReceivedDTO);
			// send to server
			if (paySuccess == -1) {
				return false;
			}

			long tempPaymentDetailID = tableId.getMaxIdTime(PAYMENT_DETAIL_TEMP_TABLE.TABLE_PAYMENT_TEMP_DETAIL);
			for (int i = 0; i < cusDebitDetailDto.arrList.size(); i++) {

				CustomerDebitDetailItem customerDebitDetailItem = cusDebitDetailDto.arrList
						.get(i);
				double remain = customerDebitDetailItem.debitDetailDTO.remain;
				if (customerDebitDetailItem.isCheck) {
					customerDebitDetailItem.debitDetailDTO.remain = 0;
					DEBIT_DETAIL_TEMP_TABLE debitDetail = new DEBIT_DETAIL_TEMP_TABLE(
							mDB);
					long detaiSucc = debitDetail
							.updateDebt(customerDebitDetailItem.debitDetailDTO);
					if (detaiSucc <= 0) {
						return false;
					}

					customerDebitDetailItem.currentPaymentDetailDTO.paymentDetailID = tempPaymentDetailID;
					cusDebitDetailDto.arrList.get(i).currentPaymentDetailDTO.paymentDetailID = tempPaymentDetailID;
					customerDebitDetailItem.currentPaymentDetailDTO.payReceivedID = payReceivedtempID;
					customerDebitDetailItem.currentPaymentDetailDTO.paymentType = "0";
					customerDebitDetailItem.currentPaymentDetailDTO.status = 1;
					customerDebitDetailItem.currentPaymentDetailDTO.staffId = GlobalInfo
							.getInstance().getProfile().getUserData().getInheritId();
					customerDebitDetailItem.currentPaymentDetailDTO.payDate = DateUtils
							.now();
					customerDebitDetailItem.currentPaymentDetailDTO.createUser = ""
							+ GlobalInfo.getInstance().getProfile()
									.getUserData().getUserName();
					customerDebitDetailItem.currentPaymentDetailDTO.createDate = DateUtils
							.now();
					customerDebitDetailItem.currentPaymentDetailDTO.saleOrderId = customerDebitDetailItem.debitDetailDTO.fromObjectID;
					customerDebitDetailItem.currentPaymentDetailDTO.amount = remain;
					customerDebitDetailItem.currentPaymentDetailDTO.discount = customerDebitDetailItem.debitDetailDTO.discountAmount;
					customerDebitDetailItem.currentPaymentDetailDTO.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
					customerDebitDetailItem.currentPaymentDetailDTO.paymentStatus = 1;
					long paymentSucc = payDetailTable
							.insert(customerDebitDetailItem.currentPaymentDetailDTO);
					tempPaymentDetailID += 1;
					// send to server
					if (paymentSucc <= 0) {
						return false;
					}
				}
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
			}
		} finally {
			mDB.endTransaction();
		}

		return true;
	}

	/**
	 * lay danh sach thanh toan bi tu choi
	 *
	 * @author cuonglt3
	 * @param debitId
	 * @return
	 * @throws Exception
	 */
	public CustomerDebitDetailDTO requestPaymentRefused(long debitId) throws Exception {
		DEBIT_DETAIL_TEMP_TABLE debit = new DEBIT_DETAIL_TEMP_TABLE(mDB);
		CustomerDebitDetailDTO debitDetail = debit
				.getPaymentDetailRefused(debitId);
		return debitDetail;
	}

	/**
	 * cap nhat debit, debit_detail va payment_detail khi xoa phieu tu choi
	 *
	 * @param dto2
	 * @param dto
	 * @return
	 */
	public boolean deletePayment(CustomerDebitDetailDTO dto,
			CustomerDebitDetailItem item) {
		// TODO Auto-generated method stub
		// DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
		// long debitSucc = debit.updateDebt(dto.debitDTO);
		// if (debitSucc == -1) {
		// return false;
		// }
		// DEBIT_DETAIL_TABLE debitDetail = new DEBIT_DETAIL_TABLE(mDB);
		// long detaiSucc = debitDetail
		// .updateDebt(item.debitDetailDTO);
		// if (detaiSucc <= 0) {
		// return false;
		// }
		// cap nhat status payment detail de khong view len lan nua
		PAYMENT_DETAIL_TEMP_TABLE pTable = new PAYMENT_DETAIL_TEMP_TABLE(mDB);
		int paymentSuccess = pTable.updateStatus(
				item.currentPaymentDetailDTO.paymentDetailID, -1);
		if (paymentSuccess == -1) {
			return false;
		}
		return true;
	}

	/**
	 * @author: DungNX
	 * @param orderDTO
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	 */
	public boolean createPaymentVansale(OrderViewDTO orderDTO) throws Exception {
		PAY_RECEIVED_TEMP_TABLE payReceivedTable = new PAY_RECEIVED_TEMP_TABLE(
				mDB);
		PAYMENT_DETAIL_TEMP_TABLE payDetailTable = new PAYMENT_DETAIL_TEMP_TABLE(
				mDB);
		TABLE_ID tableId = new TABLE_ID(mDB);
		long debitId = tableId.getMaxIdTime(DEBIT_TABLE.TABLE_NAME);
		long tempPaymentDetailID = tableId.getMaxIdTime(PAYMENT_DETAIL_TEMP_TABLE.TABLE_PAYMENT_DETAIL_TEMP);
		long payReceivedID = tableId.getMaxIdTime(PAY_RECEIVED_TEMP_TABLE.TABLE_NAME);
		boolean isPaymentSuccess = false;
		try {
			mDB.beginTransaction();
			// generate PAY_RECEIVED_NUMBER
			// PAY_RECEIVED_NUMBER = chu cai dau tien (CT) + 5 chu cuoi staff_id
			// +
			// so cuoi cua pay_received_Id
			// long payReceivedID =
			// tableId.getMaxId(PAY_RECEIVED_TABLE.TABLE_NAME);
			// payReceivedID = payReceivedID > payReceivedIDTemp ? payReceivedID
			// : payReceivedIDTemp;
			String strpayReceivedID = String.valueOf(payReceivedID);
			String userId = ""
					+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			StringBuilder payReceivedNumber = new StringBuilder();
			payReceivedNumber.append("CT");
			if (userId.length() >= 5) {
				payReceivedNumber.append(userId.substring(userId.length() - 5));
			} else {
				String zero = "00000";
				int lengId = userId.length();
				payReceivedNumber.append(zero.substring(lengId) + userId);
			}

			if (strpayReceivedID.length() >= 8) {
				payReceivedNumber.append(strpayReceivedID
						.substring(strpayReceivedID.length() - 8));
			} else {
				String zero = "00000000";
				int lengId = strpayReceivedID.length();
				payReceivedNumber.append(zero.substring(lengId)
						+ strpayReceivedID);
			}

			// update DEBIT
			DebitDTO debitDTO = new DebitDTO();
			debitDTO.totalPay = orderDTO.paymentMoney;
			debitDTO.totalDebit = -(orderDTO.paymentMoney + orderDTO.discount);
			debitDTO.totalDiscount = orderDTO.discount;
			DEBIT_TABLE debit = new DEBIT_TABLE(mDB);
			debitDTO.objectID = orderDTO.debitDto.objectID;
			debitDTO.objectType = orderDTO.debitDto.objectType;
			debitDTO.id = debit.checkDebitExist(String
					.valueOf(orderDTO.orderInfo.customerId));
			boolean insertOrUpdateDebit = false;
			if (debitDTO.id > 0) {
				orderDTO.debitIdExist = debitDTO.id;
				insertOrUpdateDebit = debit.increaseDebit(debitDTO);
			} else {
				debitDTO.id = debitId++;
				insertOrUpdateDebit = debit.insert(debitDTO) > 0 ? true : false;
			}

			boolean insertPayReceived = false;
			if (insertOrUpdateDebit) {
				orderDTO.debitDto = debitDTO;
				// insert vao PayReceived
				PayReceivedDTO dto = new PayReceivedDTO();
				dto.payReceivedNumber = payReceivedNumber.toString();
				dto.amount = (long) orderDTO.paymentMoney;
				dto.paymentType = PayReceivedDTO.PAYMENT_TYPE_CASH;
				//dto.customerId = orderDTO.customer.customerId;
				dto.shopId = GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId();
				dto.staffId = GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId();
				dto.receiptType = 0;
				dto.type = 0;
				dto.payReceivedID = payReceivedID;
				dto.paymentStatus = 1;
				dto.createDate = DateUtils.now();
				dto.createUser = ""
						+ GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
				dto.payerName = GlobalInfo.getInstance().getProfile().getUserData().getUserName();
				dto.payerAddress = GlobalInfo.getInstance().getProfile().getUserData().getAddress();
				// cusDebitDetailDto.payReceivedDto = dto;
				orderDTO.payReceivedDTO = dto;
				insertPayReceived = payReceivedTable.insert(dto) > 0 ? true
						: false;
			}

			boolean updateDebitDetail = false;
			// update DEBIT_DETAIL
			if (insertPayReceived) {
				orderDTO.debitDetailDto.totalPay += orderDTO.paymentMoney;
				orderDTO.debitDetailDto.remain -= (orderDTO.paymentMoney + orderDTO.discount);
				orderDTO.debitDetailDto.discountAmount += orderDTO.discount;
				DEBIT_DETAIL_TEMP_TABLE debitDetail = new DEBIT_DETAIL_TEMP_TABLE(
						mDB);
				updateDebitDetail = debitDetail
						.updateDebt(orderDTO.debitDetailDto) > 0 ? true : false;
			}

			boolean insertPaymentDetail = false;
			// insert PAYMENT_DETAIL
			if (updateDebitDetail) {
				// long tempPaymentDetailID =
				// tableId.getMaxId(PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL);
				// tempPaymentDetailID = tempPaymentDetailID >
				// tempPaymentDetailIDTemp ? tempPaymentDetailID :
				// tempPaymentDetailIDTemp;
				PaymentDetailDTO paymentDetailDto = new PaymentDetailDTO();
				paymentDetailDto.paymentDetailID = tempPaymentDetailID ++;
				paymentDetailDto.payReceivedID = payReceivedID;
				paymentDetailDto.amount = (long) orderDTO.paymentMoney;
				paymentDetailDto.paymentType = "1";
				paymentDetailDto.status = 1;
				String payDate = orderDTO.orderInfo.orderDate;
				paymentDetailDto.payDate = payDate;
				paymentDetailDto.discount = (long) orderDTO.discount;
				paymentDetailDto.createUser = ""
						+ GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
				paymentDetailDto.createDate = DateUtils.now();
				paymentDetailDto.staffId = GlobalInfo.getInstance()
						.getProfile().getUserData().getInheritId();
				paymentDetailDto.saleOrderId = orderDTO.orderInfo.saleOrderId;
				paymentDetailDto.paymentStatus = 1;
				paymentDetailDto.customerId = orderDTO.orderInfo.customerId;
				paymentDetailDto.shopId = orderDTO.orderInfo.shopId;
				orderDTO.paymentDetailDto = paymentDetailDto;
				insertPaymentDetail = payDetailTable.insert(paymentDetailDto) > 0 ? true
						: false;
			}

			if (insertOrUpdateDebit && insertPayReceived && updateDebitDetail
					&& insertPaymentDetail) {
				isPaymentSuccess = true;
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			throw e;
		} finally {
			mDB.endTransaction();
		}

		return isPaymentSuccess;
	}

	/**
	 * Chay Script DB
	 *
	 * @author: ThangNV31
	 * @param listScript
	 * @return boolean
	 */
	public boolean executeDBScripts(List<String> listScript) {
		if (listScript == null || listScript.size() < 1) {
			return true;
		}
		boolean execSuccessfully = true;
		try {
			mDB.beginTransaction();
			for (String script : listScript) {
				mDB.execSQL(script);
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			execSuccessfully = false;
		} finally {
			if (mDB != null) {
				mDB.endTransaction();
			}
		}
		return execSuccessfully;
	}

	/**
	 * Lay ds log thuc hien chot ngay
	 *
	 * @author: ThangNV31
	 * @return: ArrayList<LogDTO>
	 * @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckBeforeDateLockSync() {
		ArrayList<LogDTO> result = new ArrayList<LogDTO>();
		if (mDB != null && mDB.isOpen()) {
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			result = logTable.getLogNeedToCheckBeforeDateLockSync();
		}
		return result;
	}

	/**
	 * Kiem tra xem duoc phep thuc hien chot ngay hay chua
	 *
	 * @author: ThangNV31
	 * @return: boolean
	 * @throws:
	 */
	public boolean checkDBLogForDateLock() {
		boolean result = false;
		if (mDB != null && mDB.isOpen()) {
			LOG_TABLE logTable = new LOG_TABLE(mDB);
			result = logTable.checkDBLogForDateLock();
		}
		return result;
	}

	/**
	 * lay type don hang tam
	 *
	 * @author: DungNX
	 * @param saleOrderId
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	 */
	public String getOrderDraftType(String saleOrderId) throws Exception {
		String orderId = "";
		try {
			if (mDB != null && mDB.isOpen()) {
				SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
				orderId = orderTable.getOrderDaftType(saleOrderId);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw e;
		}
		return orderId;
	}

	/**
	 * Lay thong tin tien tich luy cho don hang
	 *
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<SaleOrderPromoDetailDTO>
	 * @throws:
	 * @param orderId
	 * @return
	 */

	public ArrayList<SaleOrderPromoDetailDTO> getSaleOrderPromoDetailAccumulaction(
			long orderId) {
		ArrayList<SaleOrderPromoDetailDTO> listSaleOrderPromotionDTO = new ArrayList<SaleOrderPromoDetailDTO>();
		try {
			SALE_ORDER_PROMO_DETAIL_TABLE saleOrderPromotionTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
					mDB);
			listSaleOrderPromotionDTO = saleOrderPromotionTable
					.getSaleOrderPromoDetailAccumulation(orderId);
		} catch (Exception e) {
			MyLog.e("", e.toString());
		}

		return listSaleOrderPromotionDTO;
	}

	/**
	 * Lay thong tin tien cho don hang
	 *
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<SaleOrderPromoDetailDTO>
	 * @throws:
	 * @param poCustomerId
	 * @return
	 */

	public ArrayList<SaleOrderPromoDetailDTO> getPoCustomerPromoDetailAccumulaction(
			long poCustomerId) {
		ArrayList<SaleOrderPromoDetailDTO> listSaleOrderPromotionDTO = new ArrayList<SaleOrderPromoDetailDTO>();
		try {
			SALE_ORDER_PROMO_DETAIL_TABLE saleOrderPromotionTable = new SALE_ORDER_PROMO_DETAIL_TABLE(
					mDB);
			listSaleOrderPromotionDTO = saleOrderPromotionTable
					.getPoCustomerPromoDetailAccumulation(poCustomerId);
		} catch (Exception e) {
			MyLog.e("", e.toString());
		}

		return listSaleOrderPromotionDTO;
	}

	/**
	 * lay StockTransDate cua don DP moi nhat
	 *
	 * @author: DungNX
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	 */
	public String getStockTransDate(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		String stockTransDate = null;
		STOCK_TRANS_LOT_TABLE stockTransTable = new STOCK_TRANS_LOT_TABLE(mDB);
		stockTransDate = stockTransTable.getStockTransDate(data);
		return stockTransDate;
	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: HieuNQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public GSNPPTrainingPlanDTO getPlanTrainResultReportDTO(int staffId) {
		TRAINING_PLAN_DETAIL_TABLE table = new TRAINING_PLAN_DETAIL_TABLE(mDB);
		GSNPPTrainingPlanDTO dto = null;
		try {
			dto = table.getPlanTrainResultReportDTO(staffId);
		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 * Danh sach khach hang cua NVBH trong ngay training cua TBHV
	 *
	 * @author: hoanpd1
	 * @since: 16:42:03 18-11-2014
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public TBHVDayTrainingSupervisionDTO getListCustomerTraingStaff(Bundle data) {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getListCustomerTraingStaff(data);
	}

	/**
	 * Get lich huan luyen nhan vien cua TBHV
	 *
	 * @author: hoanpd1
	 * @since: 14:27:50 17-11-2014
	 * @return: SupTraningPlanDTO
	 * @throws:
	 * @param parentStaffId
	 * @param shopOwnerID
	 * @return
	 * @throws Exception
	 */
	public ListSubTrainingPlanDTO getListTrainingHistory(Bundle data)
			throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getListTrainingHistory(data);
	}

	/**
	 * lay danh sach cac tieu chi
	 *
	 * @author: dungdq3
	 * @since: 11:31:17 AM Nov 17, 2014
	 * @return: TrainingRateDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ListTrainingRateDTO getListCriteria(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		ListTrainingRateDTO trainingRateDTO = null;
		try {
			if (mDB != null) {
				TRAINING_RATE_TABLE rate = new TRAINING_RATE_TABLE(mDB);
				trainingRateDTO = rate.getListCriteria(data);
			}
		} catch (Exception e) {
			throw e;
		}
		return trainingRateDTO;
	}

	/**
	 * lay danh sach cac tieu chi
	 *
	 * @author: dungdq3
	 * @since: 9:40:30 AM Nov 17, 2014
	 * @return: TBHVCustomerListDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public TrainingRateResultDTO getListStandard(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		TrainingRateResultDTO adDTO = null;
		try {
			if (mDB != null) {
				TRAINING_RATE_TABLE rate = new TRAINING_RATE_TABLE(mDB);
				adDTO = rate.getListStandard(data);
			}
		} catch (Exception e) {
			throw e;
		}
		return adDTO;
	}

	/**
	 * lay danh sach van de
	 *
	 * @author: dungdq3
	 * @since: 8:55:51 AM Nov 19, 2014
	 * @return: ListSupTrainingIssueDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ListSupTrainingIssueDTO getListIssue(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		SUP_TRAINING_ISSUE_TABLE issue = new SUP_TRAINING_ISSUE_TABLE(mDB);
		return issue.getListIssue(data);
	}

	/**
	 *
	 *
	 * @author: dungdq3
	 * @since: 8:45:03 AM Nov 20, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 */
	public long updateIssue(SupTrainingIssueDTO dto) {
		// TODO Auto-generated method stub
		SUP_TRAINING_ISSUE_TABLE issueTable = new SUP_TRAINING_ISSUE_TABLE(mDB);
		return issueTable.update(dto);
	}

	/**
	 * xoa can thuc hien
	 *
	 * @author: dungdq3
	 * @since: 5:19:15 PM Nov 19, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 */
	public long deleteIssue(SupTrainingIssueDTO dto) {
		// TODO Auto-generated method stub
		SUP_TRAINING_ISSUE_TABLE issueTable = new SUP_TRAINING_ISSUE_TABLE(mDB);
		return issueTable.delete(dto);
	}

	/**
	 *
	 *
	 * @author: dungdq3
	 * @param b
	 * @since: 3:41:23 PM Nov 19, 2014
	 * @return: long
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public long sendResultTraining(SupTrainingPlanDTO trainingPlan)
			throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		try {
			mDB.beginTransaction();
			SUP_TRAINING_PLAN_TABLE plan = new SUP_TRAINING_PLAN_TABLE(mDB);
			ContentValues cv = new ContentValues();
			cv.put(SUP_TRAINING_PLAN_TABLE.IS_SEND, 1);
			cv.put(SUP_TRAINING_PLAN_TABLE.SEND_DATE,
					trainingPlan.getSendDate());
			cv.put(SUP_TRAINING_PLAN_TABLE.UPDATE_DATE,
					trainingPlan.getUpdateDate());
			cv.put(SUP_TRAINING_PLAN_TABLE.UPDATE_USER,
					trainingPlan.getUpdateUser());
			String[] param = { String.valueOf(trainingPlan
					.getSupTraningPlanId()) };
			returnCode = plan.update(cv,
					SUP_TRAINING_PLAN_TABLE.SUP_TRAINING_PLAN_ID + " = ? ",
					param);
			/*
			 * if(returnCode > 0){ LOG_TABLE log = new LOG_TABLE(mDB);
			 * returnCode = log.updateState(); }
			 */
			if (returnCode > -1)
				mDB.setTransactionSuccessful();
		} catch (Exception e) {
			throw e;
		} finally {
			mDB.endTransaction();
		}
		return returnCode;
	}

	/**
	 * Get lich huan luyen nhan vien cua TBHV
	 *
	 * @author: hoanpd1
	 * @since: 14:27:50 17-11-2014
	 * @return: SupTraningPlanDTO
	 * @throws:
	 * @param parentStaffId
	 * @param shopOwnerID
	 * @return
	 */
	public ListSubTrainingPlanDTO getStaffTrainingPlan(Bundle data) {
		SUP_TRAINING_PLAN_TABLE table = new SUP_TRAINING_PLAN_TABLE(mDB);
		return table.getStaffTrainingPlan(data);
	}

	/**
	 * luu issue
	 *
	 * @author: dungdq3
	 * @since: 9:58:15 AM Nov 19, 2014
	 * @return: long
	 * @throws:
	 * @param issue
	 * @return
	 * @throws Exception
	 */
	public long saveIssue(SupTrainingIssueDTO issue) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		if (issue != null) {
			try {
				mDB.beginTransaction();
				SUP_TRAINING_ISSUE_TABLE issueTable = new SUP_TRAINING_ISSUE_TABLE(
						mDB);
				TABLE_ID tableId = new TABLE_ID(mDB);
				long supTrainIssueId = tableId.getMaxIdTime(SUP_TRAINING_ISSUE_TABLE.TABLE_NAME);
				issue.setSupTrainingIssueID(supTrainIssueId);
				returnCode = issueTable.saveIssue(issue);
				if (returnCode > 0)
					mDB.setTransactionSuccessful();
			} catch (Exception e) {
				throw e;
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * Get lich huan luyen nhan vien cua GSNPP
	 *
	 * @author: hoanpd1
	 * @since: 09:57:24 21-11-2014
	 * @return: ListSubTrainingPlanDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ListSubTrainingPlanDTO getListTrainingHistoryGSNPP(Bundle data)
			throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getListTrainingHistoryGSNPP(data);
	}

	/**
	 * luu danh sach tieu chuan
	 *
	 * @author: dungdq3
	 * @since: 5:02:47 PM Nov 17, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long saveListStandard(TrainingRateResultDTO dto) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		if (dto != null) {
			try {
				mDB.beginTransaction();
				SUP_TRAINING_CUSTOMER_TABLE supCus = new SUP_TRAINING_CUSTOMER_TABLE(
						mDB);
				returnCode = supCus.saveTrainingCustomer(dto);
				// insert danh gia tieu chi thanh cong
				if (returnCode > 0) {
					TRAINING_RATE_DETAIL_TABLE detail = new TRAINING_RATE_DETAIL_TABLE(
							mDB);
					SUP_TRAINING_RESULT_TABLE supResult = new SUP_TRAINING_RESULT_TABLE(
							mDB);
					TABLE_ID tableId = new TABLE_ID(mDB);
					long trainingRateDtlId = tableId.getMaxIdTime(TRAINING_RATE_DETAIL_TABLE.TABLE_NAME);
					long subTrainResId = tableId.getMaxIdTime(SUP_TRAINING_RESULT_TABLE.TABLE_NAME);
					for (TrainingRateDetailResultlDTO rateResult : dto
							.getListStandard()) {
						// tieu chuan chua luu va do nhan vien tao moi
						rateResult.setSubTrainingCustomerID(dto
								.getSupTrainingCustomerID());
						rateResult.generateCode();
						rateResult.setTrainingRateDetailID(trainingRateDtlId++);
						rateResult.setSubTrainingResultID(subTrainResId++);
						if (rateResult.getDefaultRate() == 0) {
							if (!StringUtil.isNullOrEmpty(rateResult
									.getFullStandardName())) {
								returnCode = detail.saveStandard(rateResult);
								// luu tieu chuan thanh cong thi se luu tiep
								// danh gia
								if (returnCode > 0) {
									returnCode = supResult
											.saveTrainingResult(rateResult);
									// luu danh gia tieu chuan ko duoc thi
									// rollback
									if (returnCode < 1) {
										break;
									}
								} else if (returnCode == -1) {
									break;
								}
							}
						} else if (rateResult.getDefaultRate() == 1) { // tieu
																		// chuan
																		// mac
																		// dinh
							returnCode = supResult
									.saveTrainingResult(rateResult);
							// luu danh gia tieu chuan ko duoc thi rollback
							if (returnCode < 1) {
								break;
							}
						}
					}
				}
				if (returnCode > 0)
					mDB.setTransactionSuccessful();
			} catch (Exception e) {
				throw e;
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Xoa cac tieu chuan
	 *
	 * @author: dungdq3
	 * @since: 5:06:04 PM Dec 5, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long deleteListStandard(TrainingRateDetailResultlDTO rateResult)
			throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		if (rateResult != null) {
			try {
				mDB.beginTransaction();
				TRAINING_RATE_DETAIL_TABLE detail = new TRAINING_RATE_DETAIL_TABLE(
						mDB);
				SUP_TRAINING_RESULT_TABLE result = new SUP_TRAINING_RESULT_TABLE(
						mDB);
				// tieu chuan chua luu va do nhan vien tao moi
				returnCode = detail.delete(rateResult);
				if (returnCode > 0) {
					returnCode = result.delete(rateResult);
				}
				if (returnCode > 0)
					mDB.setTransactionSuccessful();
			} catch (Exception e) {
				throw e;
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * @author: dungdq3
	 * @since: 3:36:09 PM Dec 10, 2014
	 * @return: ActionLogDTO
	 * @throws:
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	public ActionLogDTO checkVisitFromActionLogAndPrize(int staffId)
			throws Exception {
		// TODO Auto-generated method stub
//		ACTION_LOG_TABLE table = new ACTION_LOG_TABLE(mDB);
//		ActionLogDTO dto = table.checkVisitFromActionLogAndPrize(staffId);
		// tam thoi dong khuc tren
		ActionLogDTO dto = new ActionLogDTO();
		return dto;
	}

	/**
	 * Luu thiet bi bao mat
	 *
	 * @author: hoanpd1
	 * @since: 18:35:06 18-12-2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long requestSaveReportLostDevice(Bundle bundle) throws Exception {
		EquipLostMobileRecDTO equipLostMobileRec = (EquipLostMobileRecDTO) bundle
				.getSerializable(IntentConstants.INTENT_EQUIP_LOST_MOBILE_REC_DATA);
		EquipmentFormHistoryDTO equipFromHistory = (EquipmentFormHistoryDTO) bundle
				.getSerializable(IntentConstants.INTENT_EQUIP_FORM_HISTORY);
		EquipmentHistoryDTO equipHistory = (EquipmentHistoryDTO) bundle
				.getSerializable(IntentConstants.INTENT_EQUIP_HISTORY);
		long returnCode = -1;
		if (equipLostMobileRec != null && equipFromHistory != null
				&& equipHistory != null) {
			try {
				EQUIP_LOST_MOBILE_REC_TABLE tbLostMobile = new EQUIP_LOST_MOBILE_REC_TABLE(mDB);
				EQUIP_FORM_HISTORY_TABLE tbEquipHistoryFrom = new EQUIP_FORM_HISTORY_TABLE(mDB);
				EQUIP_HISTORY_TABLE tbEquipHistory = new EQUIP_HISTORY_TABLE(mDB);
				EQUIP_STATISTIC_REC_DTL_TABLE table = new EQUIP_STATISTIC_REC_DTL_TABLE(mDB);
				TABLE_ID tableId = new TABLE_ID(mDB);
				long maxId = tableId.getMaxIdTime(EQUIP_STATISTIC_REC_DTL_TABLE.TABLE_NAME);
				mDB.beginTransaction();
				returnCode = tbLostMobile.insertLostDevice(equipLostMobileRec);
				if (returnCode > 0) {
					equipFromHistory.recordId = returnCode;
					equipHistory.recordId = returnCode;
					returnCode = tbEquipHistoryFrom
							.insertEquipHistoryFrom(equipFromHistory);
				}
				if (returnCode > 0) {
					returnCode = tbEquipHistory
							.insertEquipHistory(equipHistory);
				}

				if (returnCode > 0) {
					equipHistory.setType(TableType.EQUIPMENT_TABLE);
					returnCode = this.updateDTO(equipHistory);
				}

				int fromView = bundle.getInt(IntentConstants.INTENT_FROM_VIEW);
				if(fromView == FromViewConstants.FROM_INVENTORY_EQUIPMENT_LIST) {
					EquipInventoryDTO itemInventory = (EquipInventoryDTO) bundle.getSerializable(IntentConstants.INTENT_ITEM_REPORT_LOST_DEVICE);
					itemInventory.idInvenDetail = maxId;

					returnCode = table.insert(itemInventory);
				}

				if (returnCode > 0) {
					mDB.setTransactionSuccessful();
				}
			} catch (Exception e) {
				throw e;
			} finally {
				mDB.endTransaction();
			}
		}
		return returnCode;
	}

	/**
	 * insert hinh anh kiem ke thiet bi
	 *
	 * @author: hoanpd1
	 * @since: 20:21:02 19-12-2014
	 * @return: long
	 * @throws:
	 * @param mediaDTO
	 * @return
	 */
	public long insertMediaInventory(EquipAttachFileDTO mediaDTO) {
		// TODO Auto-generated method stub
		long result = -1;
		try {
			mDB.beginTransaction();
			TABLE_ID tableId = new TABLE_ID(SQLUtils.getInstance().getmDB());
			// cap nhat id order
			long maxId = tableId.getMaxIdTime(EQUIP_ATTACH_FILE_TABLE.TABLE_NAME);
			EQUIP_ATTACH_FILE_TABLE table = new EQUIP_ATTACH_FILE_TABLE(mDB);
			mediaDTO.idEquipAttachFile = maxId;
			result = table.insert(mediaDTO);
			if (result > 0) {
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = -1;
			ServerLogger.sendLog(e.getMessage(),
					TabletActionLogDTO.LOG_EXCEPTION);
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	/**
	 * Danh sach khach hang dang quan ly thiet bi
	 *
	 * @author: hoanpd1
	 * @since: 15:26:03 25-12-2014
	 * @return: CustomerListDTO
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param name
	 * @param code
	 * @param address
	 * @param visit_plan
	 * @param isGetWrongPlan
	 * @param page
	 * @param isGetTotalPage
	 * @return
	 */
	public CustomerListDTO getListCustomerManageEquipment(String ownerId, String staffId,
			String shopId, String name, String code, String address,
			String visit_plan, boolean isGetWrongPlan, int page, String listStaff,
			int isGetTotalPage) {
		CustomerListDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			dto = custommerTable.getListCustomerManageEquipment(ownerId, staffId,
					shopId, name, code, visit_plan, isGetWrongPlan, page, listStaff,
					isGetTotalPage);
		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 * Lay danh sach quan ly thiet bi
	 *
	 * @author: hoanpd1
	 * @since: 15:31:19 27-11-2014
	 * @return: ReportLostEquipmentDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public EquipStatisticRecordDTO getReportLostDevice(Bundle data) {
		EQUIPMENT_TABLE table = new EQUIPMENT_TABLE(mDB);
		EquipStatisticRecordDTO inventory = null;
		try {
			inventory = table.getReportLostDevice(data);
		} catch (Exception e) {
			ServerLogger.sendLog(
					"method getReportLostDevice(Danh sach thiet) error: ",
					e.toString(), TabletActionLogDTO.LOG_CLIENT);
		}
		return inventory;
	}

	/**
	 * Lay danh sach cac dot kiem ke thiet bi
	 *
	 * @author: hoanpd1
	 * @since: 11:16:57 03-12-2014
	 * @return: ReportLostEquipmentDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public ListEquipInventoryRecordsDTO getListPeriodInventory(Bundle data) {
		EQUIP_STATISTIC_RECORD_TABLE table = new EQUIP_STATISTIC_RECORD_TABLE(
				mDB);
		ListEquipInventoryRecordsDTO inventory = null;
		try {
			inventory = table.getListPeriodInventory(data);
			SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
			inventory.shopDistance = shopTable.getShopDistance(data
					.getString(IntentConstants.INTENT_SHOP_ID));
		} catch (Exception e) {
			ServerLogger
					.sendLog(
							"method getListPeriodInventory(Danh sach ky Kiem ke) error: ",
							e.toString(), TabletActionLogDTO.LOG_CLIENT);
		}
		return inventory;
	}

	/**
	 * Lay danh sach thiet bi can kiem ke
	 *
	 * @author: hoanpd1
	 * @since: 11:17:11 03-12-2014
	 * @return: ReportLostEquipmentDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public EquipStatisticRecordDTO getListInventoryDevice(Bundle data) {
		EQUIPMENT_TABLE table = new EQUIPMENT_TABLE(mDB);
		EquipStatisticRecordDTO inventory = null;
		try {
			inventory = table.getListInventoryDevice(data);
		} catch (Exception e) {
			ServerLogger
					.sendLog(
							"method getListInventoryDevice(Danh sach thiet bi can kiem ke) error: ",
							e.toString(), TabletActionLogDTO.LOG_CLIENT);
		}
		return inventory;
	}

	/**
	 * Cap nhat Kiem ke thiet bi
	 *
	 * @author: hoanpd1
	 * @since: 11:40:52 17-12-2014
	 * @return: int
	 * @throws:
	 * @param listInventory
	 * @param staffId
	 * @param staffCode
	 * @return
	 */
	public int insertInventoryDevice(Bundle bundle) {
		String staffID = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String staffCode = bundle.getString(IntentConstants.INTENT_STAFF_CODE);
		String shopID = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		long customerId = bundle.getLong(IntentConstants.INTENT_CUSTOMER_ID);
		EquipStatisticRecordDTO listInventory = (EquipStatisticRecordDTO) bundle.getSerializable(IntentConstants.INTENT_LIST_INVENTORY_DEVICE);

		int kq = 0;
		if (listInventory != null) {
			try {
				mDB.beginTransaction();
				EQUIP_STATISTIC_REC_DTL_TABLE table = new EQUIP_STATISTIC_REC_DTL_TABLE(mDB);
//				EQUIP_SHELF_TOTAL_TABLE shelfTotalTable = new EQUIP_SHELF_TOTAL_TABLE(mDB);
				TABLE_ID tableId = new TABLE_ID(mDB);
				long maxId = tableId.getMaxIdTime(EQUIP_STATISTIC_REC_DTL_TABLE.TABLE_NAME);
				boolean checkSuccess = true;
				String dateNow = DateUtils.now();
				for (EquipInventoryDTO itemInventory : listInventory.listEquipInventory) {
					itemInventory.setType(TableType.EQUIP_STATISTIC_REC_DTL_TABLE);
					itemInventory.idInvenDetail = maxId;
					maxId++;
					itemInventory.setShopId(shopID);
					itemInventory.setStaffId(staffID);
					itemInventory.objectStockId = customerId;
					if (itemInventory.checkStatus) {
						itemInventory.status = 2;
					} else {
						itemInventory.status = 3;
					}
					
					itemInventory.createDate = dateNow;
					itemInventory.createUser = staffCode;
					itemInventory.statisticDate = dateNow;
					itemInventory.statisticTime = itemInventory.preTimes + 1;
					//CT cho KH
					itemInventory.objectStockType = 2;
					
					//Kiem ke truyen thong chu ko phai lay tu cot TYPE cua bang EQUIP_STATISTIC_RECORD
					itemInventory.setObjectType(1);
					
					itemInventory.objectId = itemInventory.idEquip;
					
					checkSuccess = table.insert(itemInventory) >= 0 ? true : false;
					if (!checkSuccess) {
						break;
					}
						
//						if (itemInventory.getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_GROUP_EQUIPMENT
//							|| itemInventory.getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_EQUIPMENT) {
//							// kiem ke thiet bi
//							itemInventory.quantity = 1;
//							if (itemInventory.checkStatus) {
//								itemInventory.quantityActually = 1;
//								itemInventory.status = 2;
//							} else {
//								itemInventory.quantityActually = 0;
//								itemInventory.status = 3;
//							}
//							itemInventory.objectId = itemInventory.idEquip;
//						} else if (itemInventory.getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_U_KE) {
//							// kiem ke u ke
//							// lay tu EQUIP_SHELF_TOTAL
//							itemInventory.quantity = shelfTotalTable.getShelfQuantity(shopID, customerId, itemInventory.idUke);
//							itemInventory.status = 1;
//							itemInventory.objectId = itemInventory.idUke;
//						}
//						itemInventory.createDate = dateNow;
//						itemInventory.createUser = staffCode;
//						itemInventory.statisticDate = dateNow;
//						
//						// tuong ung 1 KH, 1 thiet bi se dem so lan
//						if(listInventory.getEquipStatisticRecordType() == 2) {
//							itemInventory.statisticTime = itemInventory.preTimes + 1;
//						} else {
//							itemInventory.statisticTime = -1;
//						}
//						if(listInventory.getEquipStatisticRecordType() == 1) {
//							itemInventory.dateInWeek = DateUtils.getCurrentLine();
//						} else {
//							itemInventory.dateInWeek = null;
//						}
//						itemInventory.objectStockType = 2;
//						checkSuccess = table.insert(itemInventory) >= 0 ? true : false;
//						if (!checkSuccess) {
//							break;
//						}
				}
				if (checkSuccess) {
					mDB.setTransactionSuccessful();
					kq = 1;
				}
			} catch (Exception e) {
				MyLog.i("insertInventoryDevice", e.getMessage());
				kq = 0;
			} finally {
				mDB.endTransaction();
			}
		}
		return kq;
	}

	/**
	 * Lay danh sach id nhan vien thuoc quan li cua giam sat bao gom ca nhan
	 * vien nghi/chuyen shop trong thang
	 *
	 * @author: cuonglt3
	 * @param shopId
	 * @since: 14:51:58 07-08-2014
	 * @return: Dang: id1, id2, ..
	 * @throws:
	 * @return
	 */
	public String getListStaffOfSupervisorIncludeHistory(String staffOwnerId,
			String shopId) {
		ArrayList<String> listStaffId = null;
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT S.STAFF_ID AS STAFF_ID ");
			sql.append("FROM   PARENT_STAFF_MAP PS, ");
			sql.append("       (SELECT ST.staff_id, ");
			sql.append("               ST.shop_id, ");
			sql.append("               ST.staff_type_id ");
			sql.append("        FROM   staff ST ");
			sql.append("        UNION ");
			sql.append("        SELECT SH.staff_id, ");
			sql.append("               SH.shop_id, ");
			sql.append("               SH.staff_type_id ");
			sql.append("        FROM   staff_history SH WHERE DATE(SH.FROM_DATE) >= DATE('NOW', 'LOCALTIME', 'START OF MONTH')) S, ");
			sql.append("       CHANNEL_TYPE CT ");
			sql.append("WHERE  PS.PARENT_STAFF_ID IN ( " + staffOwnerId + " ) ");
			sql.append("       AND S.STAFF_ID = PS.STAFF_ID ");
			sql.append("       AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID ");
			sql.append("       AND CT.TYPE = 2 "); // Nhan vien
			sql.append("       AND CT.OBJECT_TYPE IN ( 1, 2 ) "); // VanSale
																	// hoac
																	// PreSale
			sql.append("       AND PS.STATUS = 1 ");
			sql.append("       AND CT.STATUS = 1 ");
			sql.append("       AND s.shop_id = ? ");
			params.add(shopId);
			sql.append("       AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1) ");
			sql.append("       GROUP BY S.STAFF_ID ");

			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					listStaffId = new ArrayList<String>();
					do {
						String staffId = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(staffId);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getListStaffOfSupervisor", e.getMessage());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		String result = "";
		if (listStaffId != null) {
			result = TextUtils.join(",", listStaffId);
		}

		return result;
	}

	/**
	 * get list Supervisor of ASM
	 *
	 * @author: cuonglt3
	 * @param staffIdASM
	 * @return
	 */
	public String getListSupervisorOfASMIncludeHistory(String staffIdASM) {
		ArrayList<String> listStaffId = null;
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT S.STAFF_ID AS STAFF_ID ");
			sql.append("FROM   PARENT_STAFF_MAP PS, ");
			sql.append("       (SELECT S.staff_id, ");
			sql.append("               S.shop_id, ");
			sql.append("               S.staff_type_id ");
			sql.append("        FROM   staff S ");
			sql.append("        UNION ");
			sql.append("        SELECT SH.staff_id, ");
			sql.append("               SH.shop_id, ");
			sql.append("               SH.staff_type_id ");
			sql.append("        FROM   staff_history SH WHERE DATE(SH.FROM_DATE) >= DATE('NOW', 'LOCALTIME', 'START OF MONTH')) S, ");
			sql.append("       CHANNEL_TYPE CT ");
			sql.append("WHERE  PS.PARENT_STAFF_ID = ? ");
			params.add(staffIdASM);
			sql.append("       AND S.STAFF_ID = PS.STAFF_ID ");
			sql.append("       AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID ");
			sql.append("       AND CT.TYPE = 2 "); // Loai Nhan vien
			sql.append("       AND CT.OBJECT_TYPE IN ( ? ) "); // Loai GSNPP
			params.add(String.valueOf(UserDTO.TYPE_SUPERVISOR));
			sql.append("       AND PS.STATUS = 1 ");
			sql.append("       AND CT.STATUS = 1 ");
			sql.append("       AND s.shop_id = ? ");
			params.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			sql.append("       AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1) ");
			sql.append("       GROUP BY S.STAFF_ID ");

			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					listStaffId = new ArrayList<String>();
					do {
						String staffId = CursorUtil.getString(c, "STAFF_ID");
						listStaffId.add(staffId);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getListSupervisorOfASM", e.getMessage());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		String result = "";
		if (listStaffId != null) {
			result = TextUtils.join(",", listStaffId);
		}

		return result;
	}

	/**
	 * @author: DungNX
	 * @param ext
	 * @return
	 * @return: List<EquipInventoryDTO>
	 * @throws Exception
	 * @throws:
	 */
	public List<EquipInventoryDTO> getEquipmentLostReportError(Bundle ext)
			throws Exception {
		// sale order detail - lay ds order detail
		EQUIPMENT_TABLE equipment = new EQUIPMENT_TABLE(mDB);
		List<EquipInventoryDTO> result = null;
		result = equipment.getEquipmentLostReportError();
		return result;
	}

	/**
	 * @author: DungNX
	 * @param equipLostMobileID
	 * @return: void
	 * @throws:
	 */
	public void updateEquipLostMobileStatus(String equipLostMobileID) {
		try {
			EQUIP_LOST_MOBILE_REC_TABLE equipLostMobile = new EQUIP_LOST_MOBILE_REC_TABLE(
					mDB);
			equipLostMobile.updateRejectStatus(equipLostMobileID);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * lay danh sach KPI dong
	 *
	 * @author: dungdq3
	 * @since: 10:12:53 AM Oct 22, 2014
	 * @return: StockLockDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ListDynamicKPIDTO getListDynamicKPI(Bundle b) throws Exception {
		REPORT_TEMPLATE_TABLE report = new REPORT_TEMPLATE_TABLE(mDB);
		return report.getListDynamicKPI(b);
	}

	/**
	 * Lay so ngay lam viec
	 *
	 * @author: dungdq3
	 * @since: 5:08:12 PM Oct 22, 2014
	 * @return: String[]
	 * @throws:
	 * @param b
	 * @return:
	 * @throws Exception
	 */
	public ChooseParametersBeforeExportReportDTO getWorkingDay(Bundle b) throws Exception {
		ChooseParametersBeforeExportReportDTO data = new ChooseParametersBeforeExportReportDTO();
		String shopID = b.getString(IntentConstants.INTENT_SHOP_ID,Constants.STR_BLANK);
		EXCEPTION_DAY_TABLE excep = new EXCEPTION_DAY_TABLE(mDB);
		// ngay lam viec thuc te
		data.numDaySoldPlan = excep.getCurrentWorkingDaysOfMonth(shopID);

		// ngay lam viec ke hoach
		data.numDaySalePlan = excep.getPlanWorkingDaysOfMonth(new Date(), shopID);

		if (data.numDaySalePlan == 0) {
			data.numDaySalePlan = 1; // cho = 1 de xem nhung hoan thanh 100% ngay lam
								// viec
		}
		data.progessSold = StringUtil.calPercentUsingRound(data.numDaySalePlan, data.numDaySoldPlan);

		return data;
	}

	/**
	 * get parameter before export report
	 *
	 * @author: dungdq3
	 * @since: 9:14:14 AM Oct 27, 2014
	 * @return: ReportTemplateDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ChooseParametersDTO getParameters(Bundle b) throws Exception {
		REPORT_TEMPLATE_TABLE report = new REPORT_TEMPLATE_TABLE(mDB);
		return report.getParameters(b);
	}

	/**
	 * Lay du lieu cho man hinh report chon row
	 *
	 * @author: Tuanlt11
	 * @param viewData
	 * @return
	 * @return: CustomerListDTO
	 * @throws:
	 */
	public Object getDataReportChooseRow(Bundle viewData) throws Exception {
		int typeId = (Integer) viewData
				.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE);
		if (typeId == ReportTemplateCriterionDTO.UNIT) {
			return getDataUnitChooseRow(viewData);
		} else if (typeId == ReportTemplateCriterionDTO.STAFF) {
			return getDataStaffChooseRow(viewData);
		} else if (typeId == ReportTemplateCriterionDTO.SUPERVISOR) {
			return getDataStaffChooseRow(viewData);
		} else if (typeId == ReportTemplateCriterionDTO.CUSTOMER) {
			return getDataCustomerChooseRow(viewData);
		} else
			// return table.getDataReportChooseRow(viewData);

			return null;
	}

	/**
	 * Lay du lieu bao cao dong chon nhan vien cho row
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public DynamicCustomerListDTO getDataCustomerChooseRow(Bundle viewData)
			throws Exception {
		CUSTOMER_TABLE table = new CUSTOMER_TABLE(mDB);
		return table.getDataCustomerReportChooseRow(viewData);
	}

	/**
	 * Lay du lieu bao cao dong chon nhan vien cho row
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public ReportRowStaffDialogDTO getDataStaffChooseRow(Bundle viewData)
			throws Exception {
		REPORT_TEMPLATE_OBJECT_TABLE table = new REPORT_TEMPLATE_OBJECT_TABLE(
				mDB);
		return table.getDataStaffReportChooseRow(viewData);
	}

	/**
	 * Lay du lieu bao cao dong chon khach hang cho row
	 *
	 * @author: Tuanlt11
	 * @param viewData
	 * @return
	 * @throws Exception
	 * @return: CustomerListDTO
	 * @throws:
	 */
	public ReportRowUnitDialogDTO getDataUnitChooseRow(Bundle viewData)
			throws Exception {
		REPORT_TEMPLATE_OBJECT_TABLE table = new REPORT_TEMPLATE_OBJECT_TABLE(
				mDB);
		return table.getDataUnitReportChooseRow(viewData);
	}

	/**
	 * lay danh sach san pham cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:01:39 PM Oct 28, 2014
	 * @return: ChooseParametersDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ListReportColumnProductDTO getProductListForReport(Bundle b) {
		REPORT_TEMPLATE_OBJECT_TABLE obj = new REPORT_TEMPLATE_OBJECT_TABLE(mDB);
		return obj.getProductListForReport(b);
	}

	/**
	 * Lay danh sach Nganh hang cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:39:35 PM Oct 30, 2014
	 * @return: ListReportColumnCatDTO
	 * @throws:
	 * @param b
	 * @return
	 */
	public ListReportColumnCatDTO getCategoryForReport(Bundle b) {
		// TODO Auto-generated method stub
		REPORT_TEMPLATE_OBJECT_TABLE obj = new REPORT_TEMPLATE_OBJECT_TABLE(mDB);
		return obj.getCategoryForReport(b);
	}

	/**
	 * Lay bao cao dong
	 *
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: ListReportColumnCatDTO
	 * @throws:
	 */
	public Object getDynamicReport(Bundle b) {
		// TODO Auto-generated method stub
		Object dto = null;
		int typeRow = b
				.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE, 0);
		int typeColumn = b.getInt(
				IntentConstants.INTENT_TYPE_REPORT_COLUMN_CHOOSE, 0);
		int typeSubRow = b.getInt(
				IntentConstants.INTENT_TYPE_REPORT_SUB_ROW_CHOOSE, 0);
		// loai object de lay bao cao theo bang rpt_kpi
		int objectTypeRowID = typeRow;
		if (typeRow == ReportTemplateCriterionDTO.UNIT
				|| typeRow == ReportTemplateCriterionDTO.MANAGER) {
			objectTypeRowID = typeSubRow;
			// objectTypeRowID = ReportTemplateCriterionDTO.SUPERVISOR;
		} else {
			objectTypeRowID = typeRow;
		}

		if (typeColumn == ReportTemplateCriterionDTO.WEEK) {
			RPT_KPI_IN_WEEK_TABLE rpt = new RPT_KPI_IN_WEEK_TABLE(mDB);
			dto = rpt.getDynamicReportByWeek(objectTypeRowID, b);
		} else if (typeColumn == ReportTemplateCriterionDTO.MONTH) {
			RPT_KPI_IN_MONTH_TABLE rpt = new RPT_KPI_IN_MONTH_TABLE(mDB);
			dto = rpt.getDynamicReportByMonth(objectTypeRowID, b);
		} else if (typeColumn == ReportTemplateCriterionDTO.QUARTER) {
			RPT_KPI_IN_QUARTER_TABLE rpt = new RPT_KPI_IN_QUARTER_TABLE(mDB);
			dto = rpt.getDynamicReportByQuarter(objectTypeRowID, b);
		} else if (typeColumn == ReportTemplateCriterionDTO.YEAR) {
			// do nam phai tong hop theo quarter nen cap nhat lai loai cot
			RPT_KPI_IN_QUARTER_TABLE rpt = new RPT_KPI_IN_QUARTER_TABLE(mDB);
			dto = rpt.getDynamicReportByYear(objectTypeRowID, b);
		} else if (typeColumn == ReportTemplateCriterionDTO.DATE) {
			RPT_KPI_IN_DATE_TABLE rpt = new RPT_KPI_IN_DATE_TABLE(mDB);
			dto = rpt.getDynamicReportByTypeDate(objectTypeRowID, b);
		} else {
			RPT_KPI_IN_DATE_TABLE rpt = new RPT_KPI_IN_DATE_TABLE(mDB);
			dto = rpt.getDynamicReportByDate(objectTypeRowID, b);
		}
		return dto;
	}

	/**
	 * @author: dungdq3
	 * @param sortInfo
	 * @since: 10:25:19 AM Sep 16, 2014
	 * @return: NewCustomerListDTO
	 * @param:
	 * @throws Exception
	 * @throws:
	 */
	public NewCustomerListDTO getListNewCustomer(String staffId, String shopId,
			String customerName, int numItemInPage,
			int currentIndexSpinnerState, int currentPage, DMSSortInfo sortInfo) throws Exception {
		// TODO Auto-generated method stub
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		return cusTable.getListNewCustomer(staffId, shopId, customerName,
				numItemInPage, currentIndexSpinnerState, currentPage, sortInfo);
	}

	public CreateCustomerInfoDTO getCreateCustomerInfo(String staffId,
													   String shopId, String customerId) throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		AP_PARAM_TABLE apTable = new AP_PARAM_TABLE(mDB);
		CreateCustomerInfoDTO dto = new CreateCustomerInfoDTO();
		dto.listCusType = cusTable.getListCustomerType();
		dto.listTypePosition = apTable.getSalePositionTypeParams();

		//add empty ap sale position
		ApParamDTO emptyApSalePosition = new ApParamDTO();
		emptyApSalePosition.setApParamId(0);
		emptyApSalePosition.setApParamName("");
		dto.listTypePosition.add(0, emptyApSalePosition);

		long idDistrict = 0;
		long idProvine = 0;
		long idPrecinct = 0;
		long idSalePosition = 0;

		if (!StringUtil.isNullOrEmpty(customerId)) {
			dto.cusInfo = cusTable.getCustomerByIdForCreateCustomer(customerId);
			if (dto.cusInfo != null) {
				// co thong tin khach hang
				idPrecinct = dto.cusInfo.areaId;
				// lay thong tin huyen, tinh cua xa hien tai
				// tim 2 id nay tu xa hien tai
				idDistrict = cusTable.getParrentAreaId(idPrecinct);
				idProvine = cusTable.getParrentAreaId(idDistrict);

				dto.orderSource = dto.cusInfo.orderSource;

				// tim id, vi tri hien tai cua C2
				dto.curentIdType = dto.cusInfo.getCustomerTypeId();
				int currentIndexType = -1;
				for (int i = 0, size = dto.listCusType.size(); i < size; i++) {
					if (dto.curentIdType == dto.listCusType.get(i).typeId) {
						currentIndexType = i;
						break;
					}
				}
				dto.currentIndexType = currentIndexType;
				idSalePosition = dto.cusInfo.typePostitionId;
				dto.curentIdTypePosition = idSalePosition;
			}
		}

		if (idSalePosition <= 0) {
			if (dto.listTypePosition.size() > 0) {
				idSalePosition = dto.listTypePosition.get(0).getApParamId();
				dto.currentIndexTypePosition = 0;
			}
		} else {
			for (int i = 0, size = dto.listTypePosition.size(); i < size; i++) {
				if (idSalePosition == dto.listTypePosition.get(i).getApParamId()) {
					dto.currentIndexTypePosition = i;
					break;
				}
			}
		}

		// lay danh sach Tinh
		dto.listProvine = cusTable.getListArea(
				CUSTOMER_TABLE.AREA_TYPE_PROVINE, 0);
		// lay danh sach huyen cua tinh index 0, hoac index X (tu id lay ra
		// index) neu co
		// chua lay ra duoc id tinh
		if (idProvine <= 0) {
			// se lay id dau tien
			if (dto.listProvine.size() > 0) {
				idProvine = dto.listProvine.get(0).areaId;
				dto.currentIndexProvince = 0;
			}
		} else {
			for (int i = 0, size = dto.listProvine.size(); i < size; i++) {
				if (idProvine == dto.listProvine.get(i).areaId) {
					dto.currentIndexProvince = i;
					break;
				}
			}
		}
		dto.listDistrict = cusTable.getListArea(
				CUSTOMER_TABLE.AREA_TYPE_DISTRICT, idProvine);
		// lay danh sach phuong xa cua huyen index 0, hoac index X (tu id lay ra
		// index) neu co
		// chua lay ra duoc id huyen
		if (idDistrict <= 0) {
			// se lay id dau tien
			if (dto.listDistrict.size() > 0) {
				idDistrict = dto.listDistrict.get(0).areaId;
				dto.currentIndexDistrict = 0;
			}
		} else {
			for (int i = 0, size = dto.listDistrict.size(); i < size; i++) {
				if (idDistrict == dto.listDistrict.get(i).areaId) {
					dto.currentIndexDistrict = i;
					break;
				}
			}
		}
		dto.listPrecinct = cusTable.getListArea(
				CUSTOMER_TABLE.AREA_TYPE_PRECINCT, idDistrict);
		if (idPrecinct <= 0) {
			// se lay id dau tien
			if (dto.listPrecinct.size() > 0) {
				idPrecinct = dto.listPrecinct.get(0).areaId;
				dto.currentIndexPrecinct = 0;
			}
		} else {
			for (int i = 0, size = dto.listPrecinct.size(); i < size; i++) {
				if (idPrecinct == dto.listPrecinct.get(i).areaId) {
					dto.currentIndexPrecinct = i;
					break;
				}
			}
		}
		// luu cac id 3 dia ban
		dto.curentIdDistrict = idDistrict;
		dto.curentIdPrecinct = idPrecinct;
		dto.curentIdProvine = idProvine;


		return dto;
	}

	/**
	 * lay kh = id tao
	 *
	 * @author: dungdq3
	 * @since: 4:05:56 PM Jan 26, 2015
	 * @return: CustomerDTO
	 * @throws:
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public CustomerDTO getCustomerByIdForCreateCustomer(String customerId)
			throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		return cusTable.getCustomerByIdForCreateCustomer(customerId);
	}

	public long getParrentAreaId(long idDistrict) throws Exception {
		int result = 0;
		Cursor c = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append(" SELECT a.PARENT_AREA_ID ID ");
		var1.append(" FROM AREA a ");
		var1.append(" WHERE 1=1 ");
		var1.append(" AND AREA_ID = ? ");
		param.add(String.valueOf(idDistrict));

		String sql = var1.toString();
		try {
			c = rawQueries(sql, param);

			if (c != null) {
				if (c.moveToFirst()) {
					result = (int) CursorUtil.getLong(c, "ID");
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * them moi kh
	 *
	 * @author: dungdq3
	 * @since: 5:48:26 PM Jan 26, 2015
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long insertCustomer(CustomerDTO dto) throws Exception {
		long success = 0;
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		CUSTOMER_ATTRIBUTE_DETAIL_TABLE detailTable = new CUSTOMER_ATTRIBUTE_DETAIL_TABLE(
				mDB);
		try {
			mDB.beginTransaction();
			TABLE_ID idTable = new TABLE_ID(mDB);
			long cusAtDtlId = idTable.getMaxIdTime(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.TABLE_NAME);
			success = cusTable.insertCustomer(dto);
			if (success > 0) { // insert kh thanh cong
				for (CustomerAttributeDetailViewDTO detail : dto.listCusAttrDetail) {
					detail.detailID = cusAtDtlId++;
					long succ = detailTable.insert(detail, success);
					if (succ <= 0) {
						success = succ;
						break;
					}
				}
			}
			if (success > 0) {
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (mDB != null && mDB.inTransaction())
				mDB.endTransaction();
		}
		return success;
	}

	/**
	 * Xoa KH
	 *
	 * @author: dungdq3
	 * @since: 1:44:38 PM Sep 22, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return:
	 */
	public long deleteCustomer(CustomerDTO dto) throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		return cusTable.deleteCustomer(dto);
	}

	/**
	 * lay danh sach xa
	 *
	 * @author: dungdq3
	 * @since: 9:44:34 AM Jan 27, 2015
	 * @return: ArrayList<AreaItem>
	 * @throws:
	 * @param parentAreaId
	 * @param typeArea
	 * @return
	 * @throws Exception
	 */
	public ArrayList<AreaItem> getCreateCustomerAreaInfo(long parentAreaId,
			int typeArea) throws Exception {
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		ArrayList<AreaItem> dto = new ArrayList<AreaItem>();
		dto = cusTable.getListArea(typeArea, parentAreaId);
		return dto;
	}

	/**
	 * cap nhat kh
	 *
	 * @author: dungdq3
	 * @since: 11:20:25 AM Jan 27, 2015
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long updateCustomer(CustomerDTO dto) throws Exception {

		long success = 0;
		CUSTOMER_TABLE cusTable = new CUSTOMER_TABLE(mDB);
		CUSTOMER_ATTRIBUTE_DETAIL_TABLE detailTable = new CUSTOMER_ATTRIBUTE_DETAIL_TABLE(
				mDB);
		TABLE_ID idTable = new TABLE_ID(mDB);
		try {
			mDB.beginTransaction();
			success = cusTable.updateCustomer(dto);
			long cusAtDtlId = idTable.getMaxIdTime(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.TABLE_NAME);
			if (success > 0) {
				//delete detail before re insert
				detailTable.delete(dto.customerId);

				for (CustomerAttributeDetailViewDTO detail : dto.listCusAttrDetail) {
					detail.detailID = cusAtDtlId++;
					long succ = detailTable.insert(detail, dto.customerId);
					if (succ <= 0) {
						success = succ;
						break;
					}
				}
			}
//			if (success > 0) { // insert kh thanh cong
//				for (CustomerAttributeDetailViewDTO detail : dto.listCusAttrDetail) {
//					long succ = detailTable.update(detail, dto.getCustomerId());
//					if (succ <= 0) {
//						success = succ;
//						break;
//					}
//				}
//			}
			if (success > 0) {
				mDB.setTransactionSuccessful();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (mDB != null && mDB.inTransaction())
				mDB.endTransaction();
		}
		return success;
	}

	/**
	 * lay thuoc tinh dong
	 *
	 * @author: dungdq3
	 * @since: 4:19:13 PM Jan 27, 2015
	 * @return: Object
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 */
	public ListCustomerAttributeViewDTO getExtraCustomerInfo(String staffId,
															 String shopId, String customerId) {
		// TODO Auto-generated method stub
		CUSTOMER_ATTRIBUTE_TABLE ca = new CUSTOMER_ATTRIBUTE_TABLE(mDB);
		return ca.getExtraCustomerInfo(staffId, shopId, customerId);
	}

	/**
	 * Lay ds shop con cua mot shop truyen vao
	 *
	 * @author: Tuanlt11
	 * @return
	 * @throws Exception
	 * @return: ArrayList<String>
	 * @throws:
	 */
	public ArrayList<String> getListShopChild(String shopId) throws Exception {
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		return shopTB.getShopRecursiveReverse(shopId);
	}

	/**
	 * Lay loai nhan vien cua user
	 *
	 * @author: Tuanlt11
	 * @param staffId
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getStaffType(String staffId) throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		return staff.getStaffType(staffId);
	}

	/**
	 * Lay ds staff con
	 *
	 * @author: Tuanlt11
	 * @param staffId
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	 */
	public String getListStaffChild(String staffId, String shopId)
			throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		return staff.getListStaffChild(staffId, shopId);
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: Tuanlt11
	 * @param parentStaffId
	 * @param shopId
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	 */
	public String getListStaffSupervisor(String staffOwnerId, String shopId)
			throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		return staff.getListStaffManagerOfChildShop(staffOwnerId, shopId);
	}

	/**
	 * Lay danh sach shop va nhan vien tuong ung
	 *
	 * @author: hoanpd1
	 * @since: 10:45:34 24-02-2015
	 * @return: TBHVAttendanceDTO
	 * @throws:
	 * @param ext
	 * @return
	 * @throws Exception
	 */
	public ShopAndStaffDTO getListShopAndStaff(Bundle bundle) throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		return staff.getListShopAndStaff(bundle);
	}

	/**
	 * lay data tu org access
	 *
	 * @author: dungdq3
	 * @param b
	 * @since: 4:26:14 PM Mar 18, 2015
	 * @return: ListOrgTempDTO
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public ListOrgTempDTO getDataFromOrgAcces(Bundle b) throws Exception {
		// TODO Auto-generated method stub
		String staffOwnerId = b.getString(IntentConstants.INTENT_STAFF_ID,
				Constants.STR_BLANK);
		String shopID = b.getString(IntentConstants.INTENT_SHOP_ID,
				Constants.STR_BLANK);

		ListOrgTempDTO listOrgTemp = null;
		ORG_TEMP_TABLE tempTable = new ORG_TEMP_TABLE(mDB);
		try {
			mDB.beginTransaction();
			// xoa toan bo du lieu trong bang
			tempTable.clearTable();
			// lay du lieu tu DB
			listOrgTemp = tempTable.getDataFromOrgAcces(staffOwnerId, shopID);
			if (listOrgTemp != null && listOrgTemp.getSize() > 0) {
				// insert vao table
				long success = tempTable.insertIntoTable(listOrgTemp);
				// neu insert tat ca thanh cong
				if (success > 0)
					mDB.setTransactionSuccessful();
			}
		} finally {
			if (mDB != null && mDB.inTransaction())
				mDB.endTransaction();
		}
		return listOrgTemp;
	}

	/**
	 * Danh sach shop
	 *
	 * @author: hoanpd1
	 * @since: 17:25:36 23-03-2015
	 * @return: Object
	 * @throws:
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ShopDTO> requestGetListCombobox(String shopId)
			throws Exception {
		SHOP_TABLE shop = new SHOP_TABLE(mDB);
		return shop.getListShopNPPRecursive(shopId);
	}

	/**
	 * Danh sach NV
	 * @author: hoanpd1
	 * @since: 10:55:48 26-03-2015
	 * @return: ListStaffDTO
	 * @throws:
	 * @param shopId
	 * @param staffOwnerId
	 * @param isHaveAll
	 * @return
	 * @throws Exception
	 */
	public ArrayList<StaffItemDTO> getListStaffInfo(String shopId, String staffOwnerId, String listStaffId, boolean isSatffOwnerId) throws Exception {
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		return table.getListStaff(shopId, staffOwnerId, listStaffId, isSatffOwnerId);
	}

	/**
	 * Lay loai don vi
	 *
	 * @author: dungdq3
	 * @since: 10:11:35 AM Mar 26, 2015
	 * @return: String
	 * @throws:
	 * @param shopID
	 * @return
	 * @throws Exception
	 */
	public String getShopType(String shopID) throws Exception {
		SHOP_TABLE shop = new SHOP_TABLE(mDB);
		return shop.getShopType(shopID);
	}

	/**
	 * get Shop Params
	 * @author: duongdt3
	 * @since: 15:05:27 4 Apr 2015
	 * @return: ArrayList<ShopParamDTO>
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ShopParamDTO> getShopParams(Bundle data) throws Exception {
		ArrayList<ShopParamDTO> result = new ArrayList<ShopParamDTO>();
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String[] arrParams = data.getStringArray(IntentConstants.INTENT_PARAMS);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		SHOP_PARAM_TABLE sParamTable = new SHOP_PARAM_TABLE(mDB);

		ArrayList<String> lstShopId = shopTB.getShopRecursive(shopId);
		List<ShopParamDTO> params = sParamTable.getShopParams(lstShopId, arrParams);
		if (params != null && !params.isEmpty()) {
			result.addAll(params);
		}
		return result;
	}

	/**
	 * Lay danh sach don hang
	 *
	 * @author: dungdq3
	 * @since: 3:01:44 PM Apr 6, 2015
	 * @return: ListOrderMngDTO
	 * @throws:
	 * @param from_date
	 * @param to_date
	 * @param customer_code
	 * @param customer_name
	 * @param typeRoute
	 * @param status
	 * @param billCategory
	 * @param staffId
	 * @param shop_id
	 * @param bApproved
	 * @param page
	 * @param customerId
	 * @param isGetTotalPage
	 * @param getListLog
	 * @param sortInfo
	 * @param listStaff
	 * @throws Exception
	 */
	public ListOrderMngDTO getSalesOrderInDate(String from_date,
											   String to_date, String customer_code, String customer_name,
											   String typeRoute, String status, String billCategory,
											   String staffId, String shop_id, boolean bApproved, String page,
											   String customerId, boolean isGetTotalPage, boolean getListLog,
											   DMSSortInfo sortInfo, String listStaff) throws Exception {
		// TODO Auto-generated method stub
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);

		ListOrderMngDTO listSaleOrderView = null;
		listSaleOrderView = orderTable.getSalesOrderInDate(from_date, to_date,
				customer_code, customer_name, typeRoute, status, billCategory,
				staffId, shop_id, bApproved, page, customerId, isGetTotalPage, sortInfo, listStaff);

		int count = orderTable.getTotalSalesOrderNotSend(staffId, shop_id);

		NotifyOrderDTO notifyOrder = null;
		if (getListLog) {
			notifyOrder = getOrderNeedNotify();
		}
		if (listSaleOrderView != null) {
			listSaleOrderView.totalIsSend = count;
			listSaleOrderView.notifyDTO = notifyOrder;

		}
		return listSaleOrderView;
	}

	/**
	 * lay chi tiet don hang
	 *
	 * @author: dungdq3
	 * @since: 5:06:11 PM Apr 6, 2015
	 * @return: List<SaleOrderDetailDTO>
	 * @param saleOrderId
	 * @throws Exception
	 */
	public List<SaleOrderDetailDTO> getAllDetailOfSaleOrder(long saleOrderId) throws Exception {
		// TODO Auto-generated method stub
		List<SaleOrderDetailDTO> listSaleOrderDetail = new ArrayList<SaleOrderDetailDTO>();
		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(mDB);
		listSaleOrderDetail = orderDetailTable.getAllDetailOfSaleOrder(saleOrderId);
		return listSaleOrderDetail;
	}

	/**
	 * lay don hang cuoi
	 *
	 * @author: dungdq3
	 * @since: 5:09:19 PM Apr 6, 2015
	 * @return: String
	 * @throws:
	 * @param dto
	 * @return
	 */
	public String getPreviousLastOrderDate(SaleOrderViewDTO dto) {
		// TODO Auto-generated method stub
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(mDB);
		return orderTable.getPreviousLastOrderDate(dto);
	}

	/**
	 *
	 * Lay bao cao theo nganh hang cho role NVBH
	 * @author: hoanpd1
	 * @since: 17:24:45 07-04-2015
	 * @return: ListCustomerAttributeViewDTO
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 */
	public SupervisorReportStaffSaleViewDTO getReportCategory(Bundle bundle) throws Exception{
		// TODO Auto-generated method stub
		RPT_STAFF_SALE_TABLE table = new RPT_STAFF_SALE_TABLE(mDB);
		SupervisorReportStaffSaleViewDTO result = table.getReportCategory(bundle);
		return result;
	}


	 /**
	 * Gen bang gia lay theo nhieu buoc
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ListOrgTempDTO
	 * @throws:
	*/
	public void genPriceTemp(Bundle b) throws Exception {
		ArrayList<PriceTempDTO> listPriceTemp = new ArrayList<PriceTempDTO>();
		PRICE_TEMP_TABLE tempTable = new PRICE_TEMP_TABLE(mDB);
		try {
			mDB.beginTransaction();
			// xoa toan bo du lieu trong bang
			tempTable.clearTable();
			// lay du lieu tu DB
			listPriceTemp = tempTable.getPriceCustomerByLevel(b);
			if (listPriceTemp.size() > 0) {
				// insert vao table
				long success = tempTable.insertIntoTable(listPriceTemp);
				// neu insert tat ca thanh cong
				if (success > 0){
					mDB.setTransactionSuccessful();
				}
			}
		} finally {
			if (mDB != null && mDB.inTransaction()){
				mDB.endTransaction();
			}
		}
	}

	/**
	 * gen price temp for shop
	 *
	 * @author: dungdq3
	 * @since: 9:18:43 AM Apr 10, 2015
	 * @return: void
	 * @throws:
	 * @param b
	 * @throws Exception
	 */
	public void genPriceTempForShop(Bundle b) throws Exception {
		ArrayList<PriceTempDTO> listPriceTemp = new ArrayList<PriceTempDTO>();
		PRICE_TEMP_TABLE tempTable = new PRICE_TEMP_TABLE(mDB);
		try {
			mDB.beginTransaction();
			// xoa toan bo du lieu trong bang
			tempTable.clearTable();
			// lay du lieu tu DB
			listPriceTemp = tempTable.getPriceShopByLevel(b);
			if (listPriceTemp.size() > 0) {
				// insert vao table
				long success = tempTable.insertIntoTable(listPriceTemp);
				// neu insert tat ca thanh cong
				if (success > 0)
					mDB.setTransactionSuccessful();
			}
		} finally {
			if (mDB != null && mDB.inTransaction())
				mDB.endTransaction();
		}
	}

	/**
	 *
	 * Lay bao cao theo nganh hang cho role NVBH
	 * @author: hoanpd1
	 * @since: 17:24:45 07-04-2015
	 * @return: ListCustomerAttributeViewDTO
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 */
	public ArrayList<DisplayProgrameDTO> getListDisplayProgrameImageOfSupervisor(String shopId) throws Exception {
		DISPLAY_SHOP_MAP_TABLE table = new DISPLAY_SHOP_MAP_TABLE(mDB);
		return table.getListDisplayProgrameImageOfSupervisor(shopId);
	}


	 /**
	 * add them so luong ngay nghi neu ngay nghi roi vao tu serverDate tro ve truoc sysMaxDayReturn
	 * @author: Tuanlt11
	 * @param serverDate
	 * @param sysMaxdayReturn
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
	public int addCurrentWorkingDaysOfMonth(Date serverDate, int sysMaxdayReturn ) throws Exception{
		EXCEPTION_DAY_TABLE table = new EXCEPTION_DAY_TABLE(mDB);
		return table.addCurrentWorkingDaysOfMonth(serverDate, sysMaxdayReturn);
	}

	/**
	 * Lay ds shop con cua mot shop truyen vao
	 *
	 * @author: Tuanlt11
	 * @return
	 * @throws Exception
	 * @return: ArrayList<String>
	 * @throws:
	 */
	public ArrayList<ShopProblemDTO> getListShopProblem(Bundle bundle) throws Exception {
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String shopCode = bundle.getString(IntentConstants.INTENT_SHOP_CODE);
		String shopName = bundle.getString(IntentConstants.INTENT_SHOP_NAME);
		return shopTB.getListShopProblem(shopId, shopCode, shopName, shopId,1);
	}

	/**
	 * Lay thong tin GSNNP & loai van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param nvbhShopId
	 * @return
	 * @return: TBHVAddRequirementViewDTO
	 * @throws:
	 */
	public CustomerListFeedbackDTO getCustomerListFeedback(Bundle ext) throws Exception {
		// sale order detail - lay ds order detail
		FEED_BACK_TABLE table = new FEED_BACK_TABLE(mDB);
		CustomerListFeedbackDTO dto = table.getCustomerListFeedback(ext);
		return dto;
	}

	 /**
	 * Lay ds nhan vien, shop tao van de
	 * @author: Tuanlt11
	 * @param ext
	 * @return
	 * @throws Exception
	 * @return: TBHVCustomerListDTO
	 * @throws:
	*/
	public StaffListFeedbackDTO getListStaffFeedback(Bundle ext) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		StaffListFeedbackDTO dto = new StaffListFeedbackDTO();
		dto.lstStaff = staffTable.getListStaffFeedback(ext);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		String shopParentId = ext.getString(IntentConstants.INTENT_PARENT_SHOP_ID);
		String shopCode = ext.getString(IntentConstants.INTENT_SHOP_CODE);
		String shopName = ext.getString(IntentConstants.INTENT_SHOP_NAME);
		dto.lstShop = shopTB.getListShopProblem(shopParentId, shopCode, shopName, shopParentId,1);
		//lay ds loai nhan vien
		dto.lstTypeStaff = staffTable.getListTypeStaffFeedback(ext);
		return dto;
	}

	/**
	 * lay trang thai cua form
	 *
	 * @author: DungNX
	 * @param formCode
	 * @return
	 * @return: int
	 * @throws Exception
	 * @throws:
	*/
	public int getFormStatus(String formCode) throws Exception {
		PERMISSION_TABLE permissionTable = new PERMISSION_TABLE(mDB);
		int status = permissionTable.getFormStatus(formCode);
		return status;
	}

	/**
	 * kiem tra role cua user dang nhap co full quyen khong
	 *
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	*/
	public boolean getIsFullPrivilege() throws Exception {
		PERMISSION_TABLE permissionTable = new PERMISSION_TABLE(mDB);
		return permissionTable.getIsFullPrivilege();
	}

	/**
	 * lay thong tin quyen
	 *
	 * @author: DungNX
	 * @param formCode
	 * @return
	 * @return: HashMap<Integer,PriDto>
	 * @throws Exception
	 * @throws:
	 */
	public HashMap<String, PriDto> getAppPrivilege(String formCode) throws Exception {
		PERMISSION_TABLE permissionTable = new PERMISSION_TABLE(mDB);
		HashMap<String, PriDto> priHashMap = permissionTable.getAppPrivilege(formCode);
		return priHashMap;
	}
	/**
	 * getNumCycle
	 * @author: yennth16
	 * @since: 16:19:50 09-06-2015
	 * @return: int
	 * @throws:
	 * @param num
	 * @return
	 * @throws Exception
	 */
	public int getNumCycle(int num) throws Exception {
		EXCEPTION_DAY_TABLE exceptionDay = new EXCEPTION_DAY_TABLE(mDB);
		return exceptionDay.getNumCycle(new Date(), num);
	}

	/**
	 * Danh sach nganh hang con
	 * @author: yennth16
	 * @param productInfoCode
	 * @since: 10:31:53 19-06-2015
	 * @return: List<DisplayProgrameItemDTO>
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DisplayProgrameItemDTO> getListSubcat(String productInfoCode)
			throws Exception {
		PRODUCT_INFO_TABLE productInfoTable = new PRODUCT_INFO_TABLE(mDB);
		return productInfoTable.getListSubcat(productInfoCode);
	}

	/**
	 * Lay ds key shop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: VoteKeyShopViewDTO
	 * @throws:
	*/
	public TakePhotoEquipmentViewDTO getListEquipmentTakePhoto(Bundle b) throws Exception {
		TakePhotoEquipmentViewDTO dto = new TakePhotoEquipmentViewDTO();
		dto.shopDistance = new SHOP_TABLE(mDB).getShopDistance(b.getString(IntentConstants.INTENT_SHOP_ID));
		EQUIPMENT_TABLE table = new EQUIPMENT_TABLE(mDB);
		dto.lstKeyShop = table.getListEquipment(b);
		// neu co ds key shop moi lay hinh anh len
		if (dto.lstKeyShop.size() > 0) {
			MEDIA_ITEM_TABLE tb = new MEDIA_ITEM_TABLE(mDB);
			b.putLong(IntentConstants.INTENT_EQUIP_ID, dto.lstKeyShop.get(0).equipId);
			dto.image = tb.getListImageOfEquipment(b);
		}
		return dto;
	}

	 /**
	 * Lay ds hinh anh cua thiet bi
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public TakePhotoEquipmentImageViewDTO getListImageOfEquipment(Bundle b)throws Exception {
		MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
		return table.getListImageOfEquipment(b);
	}

	/**
	 * Validate hinh anh cua thiet bi
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public ArrayList<TakePhotoEquipmentDTO> validateListImageOfEquipment(Bundle b)
			throws Exception {
		MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
		return table.validateListImageOfEquipment(b);
	}

	 /**
	 * Lay ds key shop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: VoteKeyShopViewDTO
	 * @throws:
	*/
	public VoteKeyShopViewDTO getListKeyShop(Bundle b) throws Exception {
		VoteKeyShopViewDTO dto = new VoteKeyShopViewDTO();
		dto.shopDistance = new SHOP_TABLE(mDB).getShopDistance(b
				.getString(IntentConstants.INTENT_SHOP_ID));
		KEYSHOP_TABLE table = new KEYSHOP_TABLE(mDB);
		dto.lstKeyShop = table.getListKeyShop(b);
		// neu co ds key shop moi lay hinh anh len
		if (dto.lstKeyShop.size() > 0) {
			MEDIA_ITEM_TABLE tb = new MEDIA_ITEM_TABLE(mDB);
			b.putLong(IntentConstants.INTENT_KEYSHOP_ID, dto.lstKeyShop.get(0).ksId);
			dto.image = tb.getListImageOfKeyShop(b);
		}
		return dto;
	}

	 /**
	 * Lay ds hinh anh cua key shop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public VoteKeyShopImageViewDTO getListImageOfKeyShop(Bundle b)
			throws Exception {
		MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
		return table.getListImageOfKeyShop(b);
	}

	/**
	 * Validate hinh anh cua keyshop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public ArrayList<KeyShopDTO> validateListImageOfKeyShop(Bundle b)
			throws Exception {
		MEDIA_ITEM_TABLE table = new MEDIA_ITEM_TABLE(mDB);
		return table.validateListImageOfKeyShop(b);
	}

	/**
	 * Lay ds key shop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: VoteKeyShopViewDTO
	 * @throws:
	*/
	public RegisterKeyShopViewDTO getAllListKeyShop(Bundle b) throws Exception {
		RegisterKeyShopViewDTO dto = new RegisterKeyShopViewDTO();
		KEYSHOP_TABLE table = new KEYSHOP_TABLE(mDB);
		dto.lstKeyShop = table.getAllListKeyShop(b);
//
//		for(int i = -2; i <= 1; i++){
//			ValueItemDTO value = ex.getCycleInfo(new Date(), i);
//			if(!StringUtil.isNullOrEmpty(value.id))
//				dto.lstCycle.add(value);
//		}
		if(dto.lstKeyShop.size() > 0){
			EXCEPTION_DAY_TABLE ex = new EXCEPTION_DAY_TABLE(mDB);
			dto.lstCycle = ex.getCycleInfo(dto.lstKeyShop.get(0).fromCycleId, dto.lstKeyShop.get(0).toCycleId);
			dto.cycleIdNow = ex.getCycleId(new Date(), 0);
			// lay du lieu cho keyshop dau tien
			b.putLong(IntentConstants.INTENT_KEYSHOP_ID, dto.lstKeyShop.get(0).ksId);
			RegisterKeyShopViewDTO temp = getLevelOfKeyShop(b);
			dto.lstItem = temp.lstItem;
		}

		return dto;
	}

	 /**
	 * Lay cac muc cua keyshop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<RegisterKeyShopItemDTO>
	 * @throws:
	*/
	public RegisterKeyShopViewDTO getLevelOfKeyShop(Bundle b) throws Exception {
		KEYSHOP_TABLE table = new KEYSHOP_TABLE(mDB);
		RegisterKeyShopViewDTO dto = new RegisterKeyShopViewDTO();
		long fromCycleId = b.getLong(IntentConstants.INTENT_FROM_CYCLE_ID);
		long toCycleId = b.getLong(IntentConstants.INTENT_TO_CYCLE_ID);
		EXCEPTION_DAY_TABLE ex = new EXCEPTION_DAY_TABLE(mDB);
		dto.lstCycle = ex.getCycleInfo(fromCycleId, toCycleId);
		dto.lstItem = table.getLevelOfKeyShop(b);
		return dto;
	}

	 /**
	 * update dang ki keyshop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public long updateRegisterKeyShop(Bundle b) {
		long result = -1;
		try {
			mDB.beginTransaction();
			TABLE_ID tableId = new TABLE_ID(mDB);
			long ksCusId = tableId.getMaxIdTime(KS_CUSTOMER_TABLE.TABLE_NAME);
			long ksCusLevelId = tableId.getMaxIdTime(KS_CUSTOMER_LEVEL_TABLE.TABLE_NAME);
			KS_CUSTOMER_TABLE kscTable = new KS_CUSTOMER_TABLE(mDB);
			@SuppressWarnings("unchecked")
			ArrayList<RegisterKeyShopItemDTO> lstItemClone = (ArrayList<RegisterKeyShopItemDTO>)b.getSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP);
			long customerId = Long.parseLong(b.getString(IntentConstants.INTENT_CUSTOMER_ID));
			long shopId = Long.parseLong(b.getString(IntentConstants.INTENT_SHOP_ID));
			long staffId = Long.parseLong(b.getString(IntentConstants.INTENT_STAFF_ID));
			long cycleId = Long.parseLong(b.getString(IntentConstants.INTENT_CYCLE_ID));
			RegisterKeyShopItemDTO ksCustomer = lstItemClone.get(0);
			if (ksCustomer.ksCustomerItem.ksCustomerId > 0) {
				ksCustomer.ksCustomerItem.updateDate = DateUtils.now();
				ksCustomer.ksCustomerItem.updateUser = GlobalInfo.getInstance()
						.getProfile().getUserData().getUserCode();
				// temp.ksCustomerItem.status = 1;
				ksCustomer.ksCustomerItem.shopId = shopId;
				result = kscTable.update(ksCustomer);
			} else {
				ksCustomer.ksCustomerItem.ksCustomerId = ksCusId ++;
				ksCustomer.ksCustomerItem.cycleId = cycleId;
				ksCustomer.ksCustomerItem.createDate = DateUtils.now();
				ksCustomer.ksCustomerItem.customerId = customerId;
				ksCustomer.ksCustomerItem.shopId = shopId;
				ksCustomer.ksCustomerItem.createUserId = staffId;
				ksCustomer.ksCustomerItem.createUser = GlobalInfo.getInstance()
						.getProfile().getUserData().getUserCode();
				// temp.ksCustomerItem.status = KSCustomerDTO.STATE_NEW;
				result = kscTable.insert(ksCustomer);
			}
			for(RegisterKeyShopItemDTO temp: lstItemClone){
//			RegisterKeyShopItemDTO temp = lstItemClone.get(0);
				if (result > 0) {
					KS_CUSTOMER_LEVEL_TABLE kslTable = new KS_CUSTOMER_LEVEL_TABLE(mDB);
//					for (RegisterKeyShopItemDTO item : lstItemClone) {
						if (temp.ksCustomerLevelItem.ksCustomerLevelId > 0) {
							temp.ksCustomerLevelItem.updateDate = DateUtils.now();
							temp.ksCustomerLevelItem.updateUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
							temp.ksCustomerLevelItem.shopId = shopId;
							result = kslTable.update(temp);
						} else {
							temp.ksCustomerLevelItem.ksCustomerLevelId = ksCusLevelId++;
//							temp.ksCustomerLevelItem.ksLevelId = ksCustomer.ksLevelItem.ksLevelId;
							temp.ksCustomerLevelItem.ksCustomerId = ksCustomer.ksCustomerItem.ksCustomerId;
							temp.ksCustomerLevelItem.createUserId = staffId;
							temp.ksCustomerLevelItem.createDate = DateUtils.now();
							temp.ksCustomerLevelItem.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
							temp.ksCustomerLevelItem.ksId = ksCustomer.ksCustomerItem.ksId;
							temp.ksCustomerLevelItem.shopId = shopId;
//							temp.ksCustomerLevelItem.status = 1;
							result = kslTable.insert(temp);
						}
						if(result < 0)
							break;
					}
			}
			if(result > 0)
				mDB.setTransactionSuccessful();
		} finally {
			mDB.endTransaction();
		}
		return result;
	}
	/**
	 *
	 * @author: yennth16
	 * @since: 11:51:00 10-07-2015
	 * @return: KeyShopListDTOView
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public KeyShopListDTOView getKeyShopList(Bundle data) throws Exception {
		KEYSHOP_TABLE keyShop = new KEYSHOP_TABLE(mDB);
		return keyShop.getKeyShopList(data);
	}

	/**
	 * Lay danh sach chu ky hien tai + 2 chu ky trc
	 * @author: yennth16
	 * @since: 14:34:30 20-07-2015
	 * @return: ArrayList<ValueItemDTO>
	 * @throws:
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ValueItemDTO> getListCycleOfSupervisor() throws Exception {
		EXCEPTION_DAY_TABLE table = new EXCEPTION_DAY_TABLE(mDB);
		long toCycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		long fromCycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), -2);
		return table.getCycleInfoReport(fromCycleId, toCycleId);
	}

	/**
	 * Chu ky hien tai
	 * @author: yennth16
	 * @since: 15:56:41 20-07-2015
	 * @return: String
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public String currentCycle() throws Exception{
		return String.valueOf(new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0));
	}

	/**
	 * Lay danh sach bao cao keyshop theo nvbh
	 * @author: yennth16
	 * @since: 16:31:12 20-07-2015
	 * @return: KeyShopListDTOView
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ReportKeyshopStaffDTO getReportKeyShopStaff(Bundle data) throws Exception {
		KEYSHOP_TABLE keyShop = new KEYSHOP_TABLE(mDB);
		return keyShop.getReportKeyShopStaff(data);
	}

	/**
	 * Lay danh sach bao cao keyshop theo nvbh
	 * @author: yennth16
	 * @since: 16:31:12 20-07-2015
	 * @return: KeyShopListDTOView
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ReportKeyshopDTO getListReportKeyShopStaff(Bundle data) throws Exception {
		ReportKeyshopDTO result = new ReportKeyshopDTO();
		KEYSHOP_TABLE keyShop = new KEYSHOP_TABLE(mDB);
		result = keyShop.getListReportKeyShopStaff(data);
//		result.total = keyShop.getTotalReportKeyShopStaff(data);
		return result;
	}

	/**
	 * Lay danh sach CLB thuoc 3 chu ky: chu ky hien tai + 2 chu ky truoc
	 * @author: yennth16
	 * @since: 10:00:03 23-07-2015
	 * @return: ArrayList<DisplayProgrameDTO>
	 * @throws:
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DisplayProgrameDTO> getListDisplayProgrameThreeCycle(String shopId) throws Exception {
		DISPLAY_SHOP_MAP_TABLE table = new DISPLAY_SHOP_MAP_TABLE(mDB);
		return table.getListDisplayProgrameThreeCycle(shopId);
	}

	 /**
	 * Tra thuong keyshop cho don hang
	 * @author: Tuanlt11
	 * @param orderView
	 * @param sortListOutPut
	 * @param sortListProductSale
	 * @param idShopList
	 * @param customerId
	 * @param mDB
	 * @throws Exception
	 * @return: void
	 * @throws:
	*/
	private void calPromotionKeyShopForOrder(OrderViewDTO orderView,
			SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut,
			String shopId, String customerId,
			SQLiteDatabase mDB) throws Exception {
		KEYSHOP_TABLE keyShopTable = new KEYSHOP_TABLE(mDB);
		double totalAmountFix = 0;
		// Sap xep cac san pham ban theo product id
		for (int i = 0, size = orderView.listBuyOrders.size(); i < size; i++) {
			OrderDetailViewDTO detail = orderView.listBuyOrders.get(i);
			totalAmountFix += detail.orderDetailDTO.getAmount();
		}

		ArrayList<OrderDetailViewDTO> lstKeyShopPromotionProduct = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP));
		// ds san pham
		ArrayList<OrderDetailViewDTO> lstKeyShopProduct = new ArrayList<OrderDetailViewDTO>();
		// ds tien
		ArrayList<OrderDetailViewDTO> lstKeyShopMoney = new ArrayList<OrderDetailViewDTO>();
		// B2: tinh KM cho don hang
		ArrayList<KeyShopOrderDTO> lstPromotionKeyShopOrder = keyShopTable.getListKeyShopByCustomer(customerId, shopId);
		for (KeyShopOrderDTO promotionProgram : lstPromotionKeyShopOrder) {
//			ArrayList<OrderDetailViewDTO> listPromotionProduct = new ArrayList<OrderDetailViewDTO>();
			// km tien
			OrderDetailViewDTO promotion = new OrderDetailViewDTO();
			SaleOrderDetailDTO productSaleDTO = new SaleOrderDetailDTO();
			promotion.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY;
			promotion.orderDetailDTO = productSaleDTO;

			productSaleDTO.programeId = promotionProgram.ksId;
			productSaleDTO.programeCode = promotionProgram.ksCode;
			productSaleDTO.programeName = promotionProgram.name;
			productSaleDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP;
			productSaleDTO.programeTypeCode = CalPromotions.KS;
			productSaleDTO.setDiscountAmount(StringUtil.calRemain(promotionProgram.keyshopItem.totalRewardMoney, promotionProgram.keyshopItem.totalRewardMoneyDone));
			productSaleDTO.maxAmountFree = StringUtil.calRemain(promotionProgram.keyshopItem.totalRewardMoney, promotionProgram.keyshopItem.totalRewardMoneyDone);
			productSaleDTO.productId = 0;
			productSaleDTO.price = 0;
			promotion.isEdited = 0;// tien luon duoc edit
			promotion.type = OrderDetailViewDTO.FREE_PRICE;
			promotion.typeName = StringUtil.getString(R.string.TEXT_MONEY_PROMOTION_NAME);
			promotion.oldDiscountAmount = productSaleDTO.getDiscountAmount();
			promotion.keyshopRewardMoney = promotionProgram.keyshopItem.totalRewardMoney;
			promotion.keyshopRewardMoneyDone = promotionProgram.keyshopItem.totalRewardMoneyDone;
			// luu % chiet khau cho tung sp
//			if (totalAmountFix != 0) {
			if (BigDecimal.valueOf(totalAmountFix).compareTo(BigDecimal.ZERO) != 0) {
				BigDecimalRound discountAmount = new BigDecimalRound(productSaleDTO.getDiscountAmount());
				productSaleDTO.discountPercentage = discountAmount.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT)).divide(new BigDecimal(totalAmountFix)).toDouble();
			} else{
				productSaleDTO.discountPercentage = 0;
			}

			lstKeyShopMoney.add(promotion);

			// km san pham
			for (KSCusProductRewardDTO item : promotionProgram.lstKSProduct) {
				// Cap nhat loai KM
				OrderDetailViewDTO detailView = new OrderDetailViewDTO();
				detailView.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT;
				// khuyen mai sp
				SaleOrderDetailDTO productSaleProduct = new SaleOrderDetailDTO();
				detailView.orderDetailDTO = productSaleProduct;

				productSaleProduct.programeId = promotionProgram.ksId;
				productSaleProduct.programeCode = promotionProgram.ksCode;
				productSaleProduct.programeName = promotionProgram.name;
				productSaleProduct.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP;
				productSaleProduct.programeTypeCode = CalPromotions.KS;
				productSaleProduct.isFreeItem = 1; // mat hang khuyen mai
				productSaleProduct.price = 0; // mat hang khuyen mai
				productSaleProduct.productId = item.productId;
				productSaleProduct.setDiscountAmount(0);// amountPromo = 0;
				int itemPay = (int)item.productNum >item.productNumDone?(int)item.productNum - item.productNumDone :0;
				productSaleProduct.quantity = itemPay;
				productSaleProduct.maxQuantityFree = itemPay;
				productSaleProduct.maxQuantityFreeCK = itemPay;
				detailView.isEdited = 0;
				detailView.isHasQuantityReceived = false;
				detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
				detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);
				detailView.productPromoId = item.productId;
				detailView.productCode = item.productCode;
				detailView.productName = item.productName;
				detailView.keyshopProductNum = item.productNum;
				detailView.keyshopProductNumDone = item.productNumDone;
//				promotion.listPromotionForPromo21.add(detailView);
				// truong hop sp tra keyshop > 0 thi moi hien thi
				if(productSaleProduct.quantity > 0)
					lstKeyShopProduct.add(detailView);
			}

		}
		lstKeyShopPromotionProduct.addAll(lstKeyShopProduct);
		lstKeyShopPromotionProduct.addAll(lstKeyShopMoney);
		// Lay ds so suat cua KM don hang dau tien vao
		// ds so suat
//		orderView.productQuantityReceivedList.addAll(orderView.orderQuantityReceivedList);
 	}

	 /**
	 * Lay thong tin CT tra thuong keyshop
	 * @author: Tuanlt11
	 * @param ext
	 * @return
	 * @throws Exception
	 * @return: KeyShopItemDTO
	 * @throws:
	*/
	public KeyShopItemDTO getPromotionProgrameDetailKeyShop(Bundle ext)
			throws Exception {
		KEYSHOP_TABLE promotionProgrameTable = new KEYSHOP_TABLE(
				mDB);
		KeyShopItemDTO vt = promotionProgrameTable
				.getPromotionProgrameDetailKeyShop(ext);
		return vt;

	}

	 /**
	 * Lay bao cao KPI cua NV
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: AccSaleProgReportDTO
	 * @throws:
	*/
	public ReportKPIViewDTO getReportKPISale(Bundle b) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		return staffTable.getReportKPISale(b);
	}

	/**
	 * Lay bao cao KPI cua GS
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ReportKPISupervisorViewDTO
	 * @throws:
	*/
	public ReportKPISupervisorViewDTO getReportKPISuperVisor(Bundle b) throws Exception {
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		return staffTable.getReportKPISupervisor(b);
	}

	 /**
	 * Kiem tra keyshop cua kh
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	*/
	public boolean checkKeyShopCustomer(Bundle b) throws Exception {
		KEYSHOP_TABLE table = new KEYSHOP_TABLE(mDB);
		return table.checkKeyShopCustomer(b);
	}

	public NotifyDataDTO getNotifyData() {
		NotifyDataDTO dto = new NotifyDataDTO();

		LOG_TABLE logTable = new LOG_TABLE(mDB);
		int num = logTable.getNotifySyncData();
		dto.numDataNeedSync = num;

		STAFF_POSITION_LOG_TABLE posTable = new STAFF_POSITION_LOG_TABLE(mDB);
		StaffPositionLogDTO result = posTable.getLastPosition();
		dto.lastTimeHaveLoc = result != null ? result.createDate : null;

		return dto;
	}

	 /**
	 * Lay so tien tra thuong cua keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void getMoneyRewardByCustomer(OrderDetailViewDTO orderJoinTableDTO,long ksId, long customerId) throws Exception{
		KEYSHOP_TABLE ksTable = new KEYSHOP_TABLE(mDB);
		ksTable.getMoneyRewardByCustomer(orderJoinTableDTO, ksId, customerId);
	}

	 /**
	 *  Tinh toan lai tien, discount, total, weight ... cua don hang tra sau khi loai bo keyshop
	 * @author: Tuanlt11
	 * @param item
	 * @return: void
	 * @throws:
	*/
	private void reCalReturnOrder(SaleOrderViewDTO item, OrderViewDTO result)
			throws Exception {
		long orderId = item.saleOrder.saleOrderId;
		long customerId = item.customer.customerId;
		String shopId = item.saleOrder.shopId + "";
		String staffId = "" + item.saleOrder.staffId;
		ListSaleOrderDTO listChosenProduct = getSaleOrderForEdit(orderId,
				customerId, shopId, staffId);
		result.orderInfo = listChosenProduct.saleOrderDTO;
		result.listBuyOrders = listChosenProduct.listData;

		// Get ds promotion from sqlite
		ListSaleOrderDTO listPromotionProduct = SQLUtils
				.getInstance()
				.getPromotionProductsForEdit(item.saleOrder.saleOrderId,
						result.orderInfo.orderType, customerId, shopId, staffId);
		result.listPromotionOrders = listPromotionProduct.listData;

		int totalDetail = 0;
		// tinh lai discount khi da tru keyshop
		for (OrderDetailViewDTO detailViewDTO : result.listBuyOrders) {
			for (int i = 0, size = detailViewDTO.listPromoDetail.size(); i < size; i++) {
				SaleOrderPromoDetailDTO promotion = detailViewDTO.listPromoDetail
						.get(i);
				if(PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP == promotion.programType){
					detailViewDTO.orderDetailDTO.setDiscountAmount(detailViewDTO.orderDetailDTO.getDiscountAmount() - promotion.discountAmount);
					result.orderInfo.setDiscount(result.orderInfo.getDiscount() - promotion.discountAmount);
				}
			}
			result.numSKU += detailViewDTO.orderDetailDTO.quantity;
			BigDecimal totalWeightOrder = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", result.orderInfo.totalWeight));
			totalWeightOrder = totalWeightOrder.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", detailViewDTO.orderDetailDTO.totalWeight)));
			result.orderInfo.totalWeight = totalWeightOrder.doubleValue();
			totalDetail++;
		}

		// tinh total sku, total weigth
		for (OrderDetailViewDTO detailViewDTO : result.listPromotionOrders) {
			if(PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP != detailViewDTO.orderDetailDTO.programeType){
				result.numSKUPromotion += detailViewDTO.orderDetailDTO.quantity;
				BigDecimalRound promotionTotalWeight = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.###", result.promotionTotalWeight));
				promotionTotalWeight = promotionTotalWeight.add(new BigDecimal(detailViewDTO.orderDetailDTO.totalWeight)).setScale();
				result.promotionTotalWeight = promotionTotalWeight.toDouble();
				totalDetail++;
			}
		}
		result.orderInfo.quantity = result.numSKU;
		result.orderInfo.totalWeight += result.promotionTotalWeight;
		result.orderInfo.totalDetail = totalDetail;
	}

	 /**
	 * Cap nhat discount cho don hang
	 * @author: Tuanlt11
	 * @param orderDetail
	 * @param lstBuyProducts
	 * @return: void
	 * @throws:
	*/
	private void updateDiscountOrderDetail(List<SaleOrderDetailDTO> lstOrderDetail,ArrayList<OrderDetailViewDTO> lstBuyProducts) {
		for (OrderDetailViewDTO detailViewDTO : lstBuyProducts) {
			for (SaleOrderDetailDTO orderDetail : lstOrderDetail) {
				if (orderDetail.salesOrderDetailId == detailViewDTO.orderDetailDTO.salesOrderDetailId) {
					orderDetail.setDiscountAmount(detailViewDTO.orderDetailDTO
							.getDiscountAmount());
					break;
				}
			}
		}

	}

	/**
	 * Chay Script DB
	 *
	 * @author: ThangNV31
	 * @param listScript
	 * @return boolean
	 */
	public boolean executeDBScripts(String[] listScript, String[] params) {
		if (listScript == null || listScript.length < 1) {
			return true;
		}
		boolean execSuccessfully = true;
		try {
			mDB.beginTransaction();
			for (String script : listScript) {
				mDB.execSQL(script, params);
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			execSuccessfully = false;
		} finally {
			if (mDB != null) {
				mDB.endTransaction();
			}
		}
		return execSuccessfully;
	}

	public Object getReportCustomerSale(Bundle viewInfo) throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getReportCustomerSale(viewInfo);
	}

	 /**
	 * Lay ds ct httm cua khach hang
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ReportKeyshopStaffDTO
	 * @throws:
	*/
	public ReportKeyshopStaffDTO getReportListCustomer(Bundle data) throws Exception {
		KEYSHOP_TABLE keyShop = new KEYSHOP_TABLE(mDB);
		return keyShop.getReportListCustomer(data);
	}

	/**
	 * get report ASO for NVBH
	 * @author: duongdt3
	 * @time: 2:28:55 PM Oct 20, 2015
	 * @param viewInfo
	 * @return
	 * @throws Exception
	*/
	public NvbhReportAsoViewDTO getReportNvbhAso(Bundle viewInfo) throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getReportNvbhAso(viewInfo);
	}

	/**
	 * get aso report for sup
	 * @author: duongdt3
	 * @time: 6:56:12 AM Oct 23, 2015
	 * @param viewInfo
	 * @return
	 * @throws Exception
	*/
	public SupReportAsoViewDTO getReportSupAso(Bundle viewInfo) throws Exception {
		RPT_STAFF_SALE_DETAIL_TABLE table = new RPT_STAFF_SALE_DETAIL_TABLE(mDB);
		return table.getReportSupAso(viewInfo);
	}

	/**
	 * Lay danh sach cac dot kiem ke thiet bi
	 *
	 * @author: dungnt19
	 * @since: 11:16:57 03-12-2014
	 * @return: ReportLostEquipmentDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public CustomerListDTO getListInventoryEquipment(Bundle data) {
		EQUIP_STATISTIC_RECORD_TABLE table = new EQUIP_STATISTIC_RECORD_TABLE(mDB);
		CustomerListDTO inventory = null;
		try {
			inventory = table.getListInventoryEquipment(data);
			SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
			inventory.setDistance(shopTable.getShopDistance(data.getString(IntentConstants.INTENT_SHOP_ID)));
		} catch (Exception e) {
			ServerLogger
					.sendLog(
							"method getListPeriodInventory(Danh sach ky Kiem ke) error: ",
							e.toString(), TabletActionLogDTO.LOG_CLIENT);
		}
		return inventory;
	}

	/**
	 * danh sach khach hang dang quan ly thiet bi
	 * @author dungnt19
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public CustomerListDTO getListCustomerEventoryEquipment(Bundle b) {
		CustomerListDTO dto = null;
		CUSTOMER_TABLE custommerTable = new CUSTOMER_TABLE(mDB);
		try {
			dto = custommerTable.getListCustomerInventoryEquipment(b);
		} catch (Exception e) {
		} finally {
		}
		return dto;
	}

	/**
	 * Lay danh sach thiet bi can kiem ke
	 *
	 * @author: dungnt19
	 * @return: ReportLostEquipmentDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public EquipStatisticRecordDTO getSupervisorListInventoryDevice(Bundle data) {
		EQUIPMENT_TABLE table = new EQUIPMENT_TABLE(mDB);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		EquipStatisticRecordDTO inventory = null;
		try {
			inventory = table.getSupervisorListInventoryDevice(data);
			inventory.shopDistance = shopTable.getShopDistance(data.getString(IntentConstants.INTENT_SHOP_ID));
		} catch (Exception e) {
			ServerLogger.sendLog("method getSupervisorListInventoryDevice(Danh sach thiet bi can kiem ke) error: ", e.toString(), TabletActionLogDTO.LOG_CLIENT);
		}
		return inventory;
		
		
	}

	/**
	 * Lay thong tin man hinh chup hinh thiet bi kiem ke
	 * @author: dungnt19
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: VoteKeyShopViewDTO
	 * @throws:
	*/
	public TakePhotoEquipmentViewDTO getPhotoInventoryEquipmentInfo(Bundle b) throws Exception {
		TakePhotoEquipmentViewDTO dto = new TakePhotoEquipmentViewDTO();
		dto.shopDistance = new SHOP_TABLE(mDB).getShopDistance(b.getString(IntentConstants.INTENT_SHOP_ID));
		return dto;
	}

	/**
	 * Lay ds hinh anh cua thiet bi kiem  ke
	 * @author: dungnt19
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public TakePhotoEquipmentImageViewDTO getListImageOfInventoryEquipment(Bundle b)throws Exception {
		EQUIP_ATTACH_FILE_TABLE table = new EQUIP_ATTACH_FILE_TABLE(mDB);
		return table.getListImageOfEquipment(b);
	}
}
