/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.VisitPlanDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * Thong tin lo trinh ban hang
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class VISIT_PLAN_TABLE extends ABSTRACT_TABLE {
	// ID bang
	public static final String VISIT_PLAN_ID = "VISIT_PLAN_ID";
	// ID tuyen
	public static final String ROUTING_ID = "ROUTING_ID";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// ID NVBH
	public static final String STAFF_ID = "STAFF_ID";
	// tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// den ngay
	public static final String TO_DATE = "TO_DATE";
	// 0: ngung hoat dong, 1: hoat dong
	public static final String STATUS = "STATUS";
	// ngay tao
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_VISIT_PLAN = "VISIT_PLAN";

	public VISIT_PLAN_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_VISIT_PLAN;
		this.columns = new String[] { VISIT_PLAN_ID, ROUTING_ID, SHOP_ID, STAFF_ID, FROM_DATE, TO_DATE, STATUS,
				UPDATE_DATE, CREATE_DATE, CREATE_USER, UPDATE_USER, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((VisitPlanDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(VisitPlanDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		VisitPlanDTO disDTO = (VisitPlanDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.visitPlanId };
		return update(value, VISIT_PLAN_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * 
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(VISIT_PLAN_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		VisitPlanDTO cusDTO = (VisitPlanDTO) dto;
		String[] params = { String.valueOf(cusDTO.visitPlanId) };
		return delete(VISIT_PLAN_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * 
	 * @author: TruongHN
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public VisitPlanDTO getRowById(String id) {
		VisitPlanDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(VISIT_PLAN_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}


	private VisitPlanDTO initLogDTOFromCursor(Cursor c) {
		VisitPlanDTO vsPlanDTO = new VisitPlanDTO();
		return vsPlanDTO;
	}

	/**
	 * 
	 * lay tat ca cac dong cua CSDL
	 * 
	 * @author: HieuNH
	 * @return
	 * @return: Vector<VisitPlanDTO>
	 * @throws:
	 */
	public Vector<VisitPlanDTO> getAllRow() {
		Vector<VisitPlanDTO> v = new Vector<VisitPlanDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				VisitPlanDTO VisitPlanDTO;
				if (c.moveToFirst()) {
					do {
						VisitPlanDTO = initLogDTOFromCursor(c);
						v.addElement(VisitPlanDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(VisitPlanDTO dto) {
		ContentValues editedValues = new ContentValues();
		return editedValues;
	}

	
	/**
	 * Kiem tra trang thai khach hang (ghe tham, ngoai tuyen, trong tuyen)
	 * @author: BANGHN
	 * @param shopId 
	 * @return 
	 * TRONG TUYEN GHE THAM: 1
	 * TRONG TUYEN CHUA GHE THAM : 0
	 * NGOAI TUYEN : -1
	 */
	public int checkStateCutomerVisit(String customerId, String staffId, String shopId){
		int state = -1;
		
		ArrayList<String> param = new ArrayList<String>();
//		StringBuffer  sql = new StringBuffer();
//		sql.append("SELECT VP.visit_plan_id, ");
//		sql.append("       AL.object_type, ");
//		sql.append("       is_or, ");
//		sql.append("       ( CASE ");
//		sql.append("           WHEN VP.monday = 1 THEN 'T2' ");
//		sql.append("           ELSE '' ");
//		sql.append("         END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.tuesday = 1 THEN ',T3' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.wednesday = 1 THEN ',T4' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.thursday = 1 THEN ',T5' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.friday = 1 THEN ',T6' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.saturday = 1 THEN ',T7' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) ");
//		sql.append("       || ( CASE ");
//		sql.append("              WHEN VP.sunday = 1 THEN ',CN' ");
//		sql.append("              ELSE '' ");
//		sql.append("            END ) AS TUYEN ");
//		sql.append("FROM   visit_plan VP ");
//		sql.append("       LEFT JOIN action_log AL ");
//		sql.append("              ON ( VP.staff_id = AL.staff_id ");
//		sql.append("                   AND AL.customer_id = ?  ");
//		sql.append("                   AND Ifnull(Date(AL.start_time) >= Date('now', 'localtime'), 0))");
//		param.add("" + customerId);
//		
//		sql.append("WHERE  1 = 1 ");
//		//sql.append("       AND VP.active = 1 ");
//		sql.append("       AND tuyen LIKE ? ");
//		param.add("%" + DateUtils.getCurrentLine() + "%");
//		
//		sql.append("       AND VP.staff_id = ? ");
//		param.add(staffId);
//		
//		//sql.append("       AND Ifnull(Date(VP.start_date) <= Date('now', 'localtime'), 0) ");
//		//sql.append("       AND Ifnull(Date(VP.end_date) >= Date('now', 'localtime'), 1) ");
//		sql.append("       AND VP.customer_id = ? ");
//		param.add(customerId);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT VP.visit_plan_id, ");
		var1.append("       AL.object_type, ");
		var1.append("       is_or, ");
		var1.append("       ( CASE ");
		var1.append("           WHEN RC.monday = 1 THEN 'T2' ");
		var1.append("           ELSE '' ");
		var1.append("         END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.tuesday = 1 THEN ',T3' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.wednesday = 1 THEN ',T4' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.thursday = 1 THEN ',T5' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.friday = 1 THEN ',T6' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.saturday = 1 THEN ',T7' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) ");
		var1.append("       || ( CASE ");
		var1.append("              WHEN RC.sunday = 1 THEN ',CN' ");
		var1.append("              ELSE '' ");
		var1.append("            END ) AS TUYEN ");
		var1.append("FROM   visit_plan VP ");
		var1.append("       LEFT JOIN routing_customer RC ");
		var1.append("              ON RC.routing_id = VP.routing_id ");
		var1.append("       LEFT JOIN action_log AL ");
		var1.append("              ON ( VP.staff_id = AL.staff_id ");
		var1.append("                   AND AL.customer_id = ? ");
		var1.append("                   AND AL.shop_id = ? ");
		param.add("" + customerId);
		param.add(shopId);
		var1.append("                   AND Ifnull(Date(AL.start_time) >= ?, 0 ");
		var1.append("                       ) ) ");
		param.add(dateNow);
		
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND tuyen LIKE ?  ");
		param.add("%" + DateUtils.getCurrentLine() + "%");
		var1.append("       AND VP.staff_id = ? ");
		param.add(staffId);
		var1.append("       AND RC.customer_id = ?");
		param.add(customerId);
		var1.append("       AND vp.shop_id = ?");
		param.add(shopId);

		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), param);

			if (c != null && c.moveToFirst()) {
				String objectType = CursorUtil.getString(c, "OBJECT_TYPE");
				if (!StringUtil.isNullOrEmpty(objectType)) {
					// trong tuyen da ghe tham
					state = 1;
				} else {
					// trong tuyen chua ghe tham
					state = 0;
				}
			}
		} catch (Exception e) {
			MyLog.e("checkStateCutomerVisit", "fail", e);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}
		return state;
	}
}
