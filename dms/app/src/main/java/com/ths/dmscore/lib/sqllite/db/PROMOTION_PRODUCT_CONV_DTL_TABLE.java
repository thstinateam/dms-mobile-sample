package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Chua thong tin tien trinh km tich luy
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class PROMOTION_PRODUCT_CONV_DTL_TABLE extends ABSTRACT_TABLE {
	public static final String PROMOTION_PRODUCT_CONVERT_ID = "PROMOTION_PRODUCT_CONVERT_ID";
	public static final String PROMOTION_PRODUCT_CONV_DTL_ID = "PROMOTION_PRODUCT_CONV_DTL_ID";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String IS_SOURCE_PRODUCT = "IS_SOURCE_PRODUCT";
	public static final String FACTOR = "FACTOR";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String STATUS = "STATUS";

	public static final String TABLE_NAME = "PROMOTION_PRODUCT_CONVERT";

	public PROMOTION_PRODUCT_CONV_DTL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PROMOTION_PRODUCT_CONVERT_ID, PROMOTION_PRODUCT_CONV_DTL_ID,
				PRODUCT_ID, IS_SOURCE_PRODUCT, FACTOR, CREATE_DATE, CREATE_USER,
				UPDATE_DATE, UPDATE_USER, STATUS, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

}