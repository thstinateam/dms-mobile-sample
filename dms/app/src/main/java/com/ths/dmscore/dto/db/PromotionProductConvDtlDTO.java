package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PROMOTION_PRODUCT_CONV_DTL_TABLE;
import com.ths.dmscore.util.CursorUtil;

public class PromotionProductConvDtlDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public long promotionProductConvDtlId;
	public long promotionProductConvertId;
	public long productId;
	public int isSourceProduct;
	public double factor;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public int status;

	/** Khoi tao tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		promotionProductConvDtlId = CursorUtil.getLong(c, PROMOTION_PRODUCT_CONV_DTL_TABLE.PROMOTION_PRODUCT_CONV_DTL_ID);
		promotionProductConvertId = CursorUtil.getLong(c, PROMOTION_PRODUCT_CONV_DTL_TABLE.PROMOTION_PRODUCT_CONVERT_ID);
		productId = CursorUtil.getLong(c, PROMOTION_PRODUCT_CONV_DTL_TABLE.PRODUCT_ID);
		isSourceProduct = CursorUtil.getInt(c, PROMOTION_PRODUCT_CONV_DTL_TABLE.IS_SOURCE_PRODUCT);
		factor = CursorUtil.getLong(c, PROMOTION_PRODUCT_CONV_DTL_TABLE.FACTOR);
	}
}