/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * progress report sales focus view for tbhv
 * 
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class TBHVProgressReportSalesFocusViewDTO implements Serializable{
	private static final long serialVersionUID = -4608777736056082246L;
	// so ngay ke hoach trong thang
	public int numPlanDate;
	// so ngay da ban trong thang
	public int numPlanDateDone;
	// tien do ban hang
	public double progressSales;
	// list report info
	public ArrayList<ReportSalesFocusEmployeeInfo> listFocusInfoRow = new ArrayList<ReportSalesFocusEmployeeInfo>();
	// row info report total
	public ReportSalesFocusEmployeeInfo objectReportTotal = new ReportSalesFocusEmployeeInfo();
	public ArrayList<String> arrMMTTText;

	public TBHVProgressReportSalesFocusViewDTO() {
		numPlanDate = 0;
		numPlanDateDone = 0;
		progressSales = 0;
		listFocusInfoRow = new ArrayList<ReportSalesFocusEmployeeInfo>();
		objectReportTotal = new ReportSalesFocusEmployeeInfo();
		arrMMTTText = new ArrayList<String>();
	}
	
}
