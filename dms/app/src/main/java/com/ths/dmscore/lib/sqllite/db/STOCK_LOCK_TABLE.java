package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.LockDateDTO;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class STOCK_LOCK_TABLE extends ABSTRACT_TABLE {

	// id cua table
	public static final String STOCK_LOCK_ID = "STOCK_LOCK_ID";
	// id cua shop khai bao
	public static final String VAN_LOCK = "VAN_LOCK";
	// type param
	public static final String STAFF_ID = "STAFF_ID";
	// code param
	public static final String SHOP_ID = "SHOP_ID";
	// type param
	public static final String CREATE_DATE = "CREATE_DATE";
	// code param
	public static final String CREATE_USER = "CREATE_USER";
	// type param
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// code param
	public static final String UPDATE_USER = "UPDATE_USER";
	// id cua shop khai bao
	public static final String SYN_STATE = "SYN_STATE";

	// table name
	public static final String TABLE_NAME = "STOCK_LOCK";

	public STOCK_LOCK_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { STOCK_LOCK_ID, VAN_LOCK, STAFF_ID,
				SHOP_ID, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	protected long insert(LockDateDTO dto) {
		TABLE_ID tableId = new TABLE_ID(mDB);
		long stockLockId = tableId.getMaxIdTime(STOCK_LOCK_TABLE.TABLE_NAME);
		dto.stockLockID = stockLockId;
		return insert(null, initDataRow(dto));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		LockDateDTO stDto = (LockDateDTO) dto;
		return update(initDataRow(stDto), STOCK_LOCK_ID + " = ?", new String[] { "" + stDto.stockLockID });
	}

	protected long updateStockState(LockDateDTO dto) {
		return update(initUpdateStockState(dto), STAFF_ID + " = ? AND " + SHOP_ID + " = ? ", new String[] { "" + dto.staffId, "" + dto.shopId });
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * khoi tao gia tri cho Row
	 * @author: ThangNV31
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	*/
	private ContentValues initDataRow(LockDateDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STOCK_LOCK_ID, dto.stockLockID);
		editedValues.put(VAN_LOCK, dto.vanLock);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		if(!StringUtil.isNullOrEmpty(dto.createDate)){
			editedValues.put(CREATE_DATE, dto.createDate);
		}
		if(!StringUtil.isNullOrEmpty(dto.createUser)){
			editedValues.put(CREATE_USER, dto.createUser);
		}
		if(!StringUtil.isNullOrEmpty(dto.updateDate)){
			editedValues.put(UPDATE_DATE, dto.updateDate);
		}
		if(!StringUtil.isNullOrEmpty(dto.updateUser)){
			editedValues.put(UPDATE_USER, dto.updateUser);
		}

		return editedValues;
	}

	/**
	 * khoi tao gia tri cho Row
	 * @author: ThangNV31
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	*/
	private ContentValues initUpdateStockState(LockDateDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(VAN_LOCK, dto.vanLock);
		if(!StringUtil.isNullOrEmpty(dto.updateDate)){
			editedValues.put(UPDATE_DATE, dto.updateDate);
		}
		if(!StringUtil.isNullOrEmpty(dto.updateUser)){
			editedValues.put(UPDATE_USER, dto.updateUser);
		}
		return editedValues;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param data
	 * @return
	 * @return: StockLockDTO
	 * @throws Exception
	 * @throws:
	*/
	public LockDateDTO getLockDate(Bundle data) throws Exception{
		LockDateDTO stockLock = new LockDateDTO();

		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String createDate = data.getString(IntentConstants.INTENT_CREATE_DATE);

		StringBuilder sqlRequest = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sqlRequest.append("SELECT * ");
		sqlRequest.append("FROM   STOCK_LOCK sl ");
		sqlRequest.append("WHERE  1 = 1 ");
		sqlRequest.append("       AND sl.staff_id = ? ");
		params.add(staffId);
		sqlRequest.append("       AND sl.shop_id = ? ");
		params.add(shopId);
//		sqlRequest.append("       AND Substr(sl.CREATE_DATE, 0, 11) = Substr(?, 0, 11) ");
//		params.add(createDate);

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					stockLock.initFromCursor(c);
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return stockLock;
	}
}
