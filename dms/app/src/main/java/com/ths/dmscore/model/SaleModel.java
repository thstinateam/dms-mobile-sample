/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerCatLevelDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.CustomerStockHistoryDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.DisplayProgrameLvDTO;
import com.ths.dmscore.dto.db.EquipAttachFileDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.MediaLogDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.db.TakePhotoEquipmentDTO;
import com.ths.dmscore.dto.db.ToDoTaskDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.AutoCompleteFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.CategoryCodeDTO;
import com.ths.dmscore.dto.view.ChooseParametersBeforeExportReportDTO;
import com.ths.dmscore.dto.view.ChooseParametersDTO;
import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerCatAmountPopupViewDTO;
import com.ths.dmscore.dto.view.CustomerDebitDetailDTO;
import com.ths.dmscore.dto.view.CustomerDebitDetailItem;
import com.ths.dmscore.dto.view.CustomerInfoDTO;
import com.ths.dmscore.dto.view.CustomerProgrameDTO;
import com.ths.dmscore.dto.view.DisplayPresentProductInfo;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.dto.view.EquipStatisticRecordDTO;
import com.ths.dmscore.dto.view.GeneralStatisticsInfoViewDTO;
import com.ths.dmscore.dto.view.ImageListDTO;
import com.ths.dmscore.dto.view.ListAlbumUserDTO;
import com.ths.dmscore.dto.view.ListCustomerAttentProgrameDTO;
import com.ths.dmscore.dto.view.ListDynamicKPIDTO;
import com.ths.dmscore.dto.view.ListFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.NVBHReportForcusProductInfoViewDTO;
import com.ths.dmscore.dto.view.NoteInfoDTO;
import com.ths.dmscore.dto.view.OfficeDocumentListDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.dto.view.PhotoThumbnailListDto;
import com.ths.dmscore.dto.view.ProgrameForProductDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopViewDTO;
import com.ths.dmscore.dto.view.RemainProductViewDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailCustomerDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailDayDTO;
import com.ths.dmscore.dto.view.SaleOrderDataResult;
import com.ths.dmscore.dto.view.SaleProductInfoDTO;
import com.ths.dmscore.dto.view.SaleStatisticsAccumulateDayDTO;
import com.ths.dmscore.dto.view.SaleStatisticsProductInDayInfoViewDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentImageViewDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentViewDTO;
import com.ths.dmscore.dto.view.VoteDisplayPresentProductViewDTO;
import com.ths.dmscore.dto.view.VoteKeyShopViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALES_ORDER_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.CustomerPositionLogDTO;
import com.ths.dmscore.dto.db.ListProductDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.ShopLockDTO;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.map.GeomDTO;
import com.ths.dmscore.dto.view.ChartStaffDTO;
import com.ths.dmscore.dto.view.ComboboxDisplayProgrameDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemModel;
import com.ths.dmscore.dto.view.DisplayProgrameModel;
import com.ths.dmscore.dto.view.DisplayProgrameViewDTO;
import com.ths.dmscore.dto.view.ImageSearchViewDTO;
import com.ths.dmscore.dto.view.IntroduceProductDTO;
import com.ths.dmscore.dto.view.ListCustomerDisplayProgrameScoreDTO;
import com.ths.dmscore.dto.view.ListEquipInventoryRecordsDTO;
import com.ths.dmscore.dto.view.ListNoteInfoViewDTO;
import com.ths.dmscore.dto.view.ListOrderMngDTO;
import com.ths.dmscore.dto.view.ListRemainProductDTO;
import com.ths.dmscore.dto.view.ListReportColumnCatDTO;
import com.ths.dmscore.dto.view.ListReportColumnProductDTO;
import com.ths.dmscore.dto.view.ListSaleOrderDTO;
import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.dto.view.PromotionProgrameModel;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.dto.view.SaleInMonthDTO;
import com.ths.dmscore.dto.view.SaleOrderCustomerDTO;
import com.ths.dmscore.dto.view.SaleOrderViewDTO;
import com.ths.dmscore.dto.view.VoteKeyShopImageViewDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_CATEGORY_LEVEL_TABLE;
import com.ths.dmscore.lib.sqllite.db.CalPromotions;
import com.ths.dmscore.lib.sqllite.db.DEBIT_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;
import com.ths.dms.R;

/**
 * Tang xu ly model cua NVBH
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class SaleModel extends AbstractModelService {
	protected static volatile SaleModel instance;

	protected SaleModel() {
	}

	public static SaleModel getInstance() {
		if (instance == null) {
			instance = new SaleModel();
		}
		return instance;
	}

	public void onReceiveData(HTTPMessage mes) {
		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(mes.getDataText());
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		// DMD check null or empty
		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SaleController.getInstance().handleErrorModelEvent(model);
			return;
		}
		switch (mes.getAction()) {
		case ActionEventConstant.ACTION_INSERT_LOCK_DATE: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					SaleController.getInstance().handleModelEvent(model);
				} else {
					model.setModelMessage(result.getString("errorMessage"));
					SaleController.getInstance().handleErrorModelEvent(model);
				}
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SaleController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
//		case ActionEventConstant.ACTION_SEND_REQUEST_DATE_LOCK_TO_SERVER: {
//			JSONObject json;
//			try {
//				json = new JSONObject((String) mes.getDataText());
//				JSONObject result = json.getJSONObject("result");
//				int errCode = result.getInt("errorCode");
//				model.setModelCode(errCode);
//				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
//					SaleController.getInstance().handleModelEvent(model);
//				} else {
//					model.setModelMessage(result.getString("errorMessage"));
//					SaleController.getInstance().handleErrorModelEvent(model);
//				}
//			} catch (Exception e) {
//				model.setModelCode(ErrorConstants.ERROR_COMMON);
//				SaleController.getInstance().handleErrorModelEvent(model);
//			}
//			break;
//		}
		case ActionEventConstant.UPDATE_CUSTOMER_LOATION:
				JSONObject json;
				try {
					json = new JSONObject((String) mes.getDataText());
					JSONObject result = json.getJSONObject("result");
					int errCode = result.getInt("errorCode");
					model.setModelCode(errCode);
					if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
						SaleController.getInstance().handleModelEvent(model);
					} else {
						model.setModelMessage(result.getString("errorMessage"));
						SaleController.getInstance().handleErrorModelEvent(model);
					}
				} catch (Exception e) {
					model.setModelCode(ErrorConstants.ERROR_COMMON);
					SaleController.getInstance().handleErrorModelEvent(model);
				}
				break;
        case ActionEventConstant.GET_CHART_STAFF_DAY:
            try {
                json = new JSONObject((String) mes.getDataText());
                String errCode = json.getString("status");
                if(!StringUtil.isNullOrEmpty(errCode) && errCode.equals("success")){
                    model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

                    JSONArray data = json.getJSONArray("data");
					ArrayList<ChartStaffDTO> list = new ArrayList<>();
                    for(int i=0, s=data.length();i<s; i++){
                        ChartStaffDTO item = new ChartStaffDTO();
                        item.parseData(data.getJSONObject(i));
						list.add(item);

                    }
                    model.setModelData(list);
                    SaleController.getInstance().handleModelEvent(model);
                }else{
                    model.setModelMessage(json.getString("errorMessage"));
                    SaleController.getInstance().handleErrorModelEvent(model);
                }

            }catch (Exception e) {
                model.setModelCode(ErrorConstants.ERROR_COMMON);
                SaleController.getInstance().handleErrorModelEvent(model);
            }
            break;
		case ActionEventConstant.REQUEST_MONTH_SALES_REVENUE:
			try {
				json = new JSONObject((String) mes.getDataText());
				String errCode = json.getString("status");
				if(!StringUtil.isNullOrEmpty(errCode) && errCode.equals("success")){
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

					JSONArray data = json.getJSONArray("data");
					ArrayList<ChartStaffDTO> list = new ArrayList<>();
					for(int i=0, s=data.length();i<s; i++){
						ChartStaffDTO item = new ChartStaffDTO();
						item.parseData(data.getJSONObject(i));
						list.add(item);

					}
					model.setModelData(list);
					SaleController.getInstance().handleModelEvent(model);
				}else{
					model.setModelMessage(json.getString("errorMessage"));
					SaleController.getInstance().handleErrorModelEvent(model);
				}

			}catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SaleController.getInstance().handleErrorModelEvent(model);
			}
			break;
		default:
			int errCodeDefault = ErrorConstants.ERROR_COMMON;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				errCodeDefault = result.getInt("errorCode");
				model.setModelCode(errCodeDefault);
				model.setModelData(actionEvent.userData);
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			} finally {
				// thanh cong, that bai khong gui tra kq ve view nua
				if (errCodeDefault == ErrorConstants.ERROR_CODE_SUCCESS) {
					// TH mac dinh la request create/update/delete du lieu
					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
					// SaleController.getInstance().handleModelEvent(model);
				} else if (errCodeDefault == ErrorConstants.ERROR_UNIQUE_CONTRAINTS) {
					// request loi trung khoa -- khong thuc hien goi lai len
					// server nua
					// SaleController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_UNIQUE_CONTRAINTS);
				} else {
					// ghi log loi len server
					updateLog(actionEvent, LogDTO.STATE_FAIL);
					// SaleController.getInstance().handleErrorModelEvent(model);
				}
			}
		}
	}

	public void onReceiveError(HTTPResponse response) {
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(response.getDataText());
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (GlobalUtil.checkActionSave(actionEvent.action)
				&& actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO log = (LogDTO) actionEvent.logData;
			if (LogDTO.STATE_NONE.equals(log.state)) {
				updateLog(actionEvent, LogDTO.STATE_NEW);
			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			SaleController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelMessage(response.getErrMessage());
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	@Override
	public Object requestHandleModelData(ActionEvent e) throws Exception {
		Object data = null;
		switch (e.action) {
		// case ActionEventConstant.ACTION_SEND_REQUEST_DATE_LOCK_TO_SERVER:
		// sendRequestDateLockToServer(e);
		// break;
		case ActionEventConstant.ACTION_CHECK_DB_LOG_FOR_DATE_LOCK:
			data = checkDBLogForDateLock(e);
			break;
		case ActionEventConstant.ACTION_INSERT_LOCK_DATE:
			data = insertStockState(e);
			break;
		case ActionEventConstant.SAVE_DEBIT_AND_GEN_PAYMENT:
			data = saveAndSendPaymentDetail(e);
			break;
		case ActionEventConstant.DELETE_PAYMENT_REFUSED:
			data = deletePaymentDetail(e);
			break;
		case ActionEventConstant.ACTION_GET_LOCK_DATE_STATE:
			data = getStockState(e);
			break;
		case ActionEventConstant.CHECK_CREATE_ORDER_FROM_ACTION_LOG:
			data = checkCreateOrderFromActionLog(e);
			break;
		case ActionEventConstant.GET_ORDER_DRAFT:
			data = requestGetOrderDraft(e);
			break;
		case ActionEventConstant.GET_IMAGE_LIST_SEARCH:
			data = getListAlbum(e);
			break;
		case ActionEventConstant.ACTION_GO_TO_VIEW_CUSTOMER_DISPLAY_PROGRESS: {
			data = getReportDisplayProgressDetailCustomer(e);
			break;
		}
		case ActionEventConstant.GET_CUSTOMER_SALE_SKU: {
			data = getCustomerSaleSKUInMonth(e);
			break;
		}
		case ActionEventConstant.ACTION_LOAD_LIST_IMAGE_CTTB: {
			data = getListImageCTTB(e);
			break;
		}
		case ActionEventConstant.REVIEW_SHOWROOM_TOOL_LIST:
			data = getReviewShowroom(e);
			break;
		case ActionEventConstant.REVIEW_SHOWROOM_GOODS_LIST:
			data = getReviewShowroom_GoodList(e);
			break;
		case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY: {
			data = getReportDisplayProgressDetailDay(e);
			break;
		}
		case ActionEventConstant.DELETE_DRAFT_ORDER:
			data = deleteDraftSaleOrder(e);
			break;
		case ActionEventConstant.PAID_PROMOTION_LIST:
			data = getPaidPromotionList(e);
			break;
		case ActionEventConstant.PAY_DEBT:
			data = payDebt(e);
			break;
		case ActionEventConstant.PAYMENT_VANSALE: {
			data = paymentVansale(e);
			break;
		}
		case ActionEventConstant.GET_LAST_SALE_ORDERS:
			data = getLastSaleOrders(e);
			break;
		case ActionEventConstant.GET_SALE_LEVEL_CUSTOMER:
			data = getSaleLevelCustomer(e);
			break;
		case ActionEventConstant.GET_REMAIN_PRODUCT:
			data = getRemainProduct(e);
			break;
		case ActionEventConstant.GET_LAST_ORDINAL_REMAIN_PRODUCT:
			data = getLastOrdinalRemainProduct(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_CAT_AMOUNT_IN_NEARLY_3_MONTH: {
			data = getCustomerCatAmountInNearly3Month(e);
			break;
		}
		case ActionEventConstant.GET_PRODUCT_LIST:
			data = getProductList(e);
			break;
		case ActionEventConstant.GET_LIST_DISPLAY_PROGRAM_PRODUCT:
			data = requestGetListDisplayProgramProduct(e);
			break;
		case ActionEventConstant.GET_VOTE_DISPLAY_PROGRAME_VIEW: {
			data = requestGetVoteDisplayProgrameView(e);
			break;
		}
		case ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT: {
			data = getCustomerInfoVisit(e);
			break;
		}
		case ActionEventConstant.GET_LIST_VOTE_DISPLAY_PRODUCT:
			data = requestGetListVoteDisplayProduct(e);
			break;
		case ActionEventConstant.SAVE_VOTE_DISPLAY_PRESENT_PRODUCT:
			data = requestUpdateVoteProductDisplay(e);
			break;
		case ActionEventConstant.GET_LIST_CATEGORY_CODE_PRODUCT:
			data = getListCategoryCodeProduct(e);
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			data = requestGetListProductAddToOrderView(e);
			break;
		case ActionEventConstant.GET_INIT_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			data = requestGetInitListProductAddToOrderView(e);
			break;
		case ActionEventConstant.GET_LIST_CUSTOMER_ATTENT_PROGRAME:
			data = getListCustomerAttentPrograme(e);
			break;
		case ActionEventConstant.GET_LIST_PROGRAME_FOR_PRODUCT:
			data = getListProgrameForProduct(e);
			break;
		case ActionEventConstant.GET_INTRODUCE_PRODUCT:
			data = getIntroduceProduct(e);
			break;
		case ActionEventConstant.GET_LIST_COMBOBOX_DISPLAY_PROGRAME:
			data = getListComboboxDisplayPrograme(e);
			break;
		case ActionEventConstant.GET_LIST_SUB_CAT:
			data = getListSubcat(e);
			break;
		case ActionEventConstant.UPDATE_CLIENT_THUMNAIL_URL:
			data = updateClientThumbnailUrl(e);
			break;
		case ActionEventConstant.GO_TO_LIST_ALBUM_USER:
			data = getListAlbumUser(e);
			break;
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_USER:
			data = getAlbumDetailUser(e);
			break;
		case ActionEventConstant.NO_SUCCESS_ORDER_LIST:
			data = getNoSuccessOrderList(e);
			break;
		case ActionEventConstant.ACTION_GET_ALL_ORDER_FAIL:
			data = requestGetAllOrderFail(e);
			break;
		case ActionEventConstant.GET_LIST_INDUSTRY_PRODUCT_AND_LIST_PRODUCT:
			data = requestGetListIndustryProductAndListProduct(e);
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_PRE_SALE_SOLD:
			data = requestListProductPreSaleSold(e);
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_VAN_SALE_SOLD:
			data = requestListProductVanSaleSold(e);
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_SALE_STATISTICS_ACCUMULATE_DAY:
			data = requestSaleStatisticsAccumulateDay(e);
			break;
		case ActionEventConstant.GET_COUNT_SALE_STATISTICS_ACCUMULATE_DAY_LIST_PRODUCT:
			data = requestCountSaleStatisticsAccumulateDayListProduct(e);
			break;
		case ActionEventConstant.GET_SALE_STATISTICS_ACCUMULATE_DAY_LIST_PRODUCT:
			data = requestSaleStatisticsAccumulateDayListProduct(e);
			break;
		case ActionEventConstant.TRANSFER_LIST_ORDER:
			data = requestTranferListOrder2(e);
			break;
		case ActionEventConstant.CREATE_NEW_ORDER:
			data = requestCreateOrder(e);
			break;
		case ActionEventConstant.EDIT_AND_SEND_ORDER:
			data = requestEditAndSendOrder(e);
			break;
		case ActionEventConstant.GET_LIST_PROMOTION_PROGRAME:
			data = requestGetListPromotionPrograme(e);
			break;
		case ActionEventConstant.GET_LIST_DISPLAY_PROGRAM:
			data = requestGetListDisplayPrograme(e);
			break;
		case ActionEventConstant.GET_DISPLAY_PROGRAME_ITEM:
			data = requestGetDisplayProgrameItem(e);
			break;
		case ActionEventConstant.GET_DISPLAY_PROGRAME_INFO:
			data = requestGetDisplayProgrameInfo(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_LIST:
			data = getListCustomer(e);
			break;
		case ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG:
			data = checkVisitFromActionLog(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_LIST_FOR_ROUTE:
			data = getCustomerListForRoute(e);
			break;
		case ActionEventConstant.TBHV_CUSTOMER_LIST_FOR_SUPERVISION:
			data = getTBHVCustomerInVisitPlan(e);
			break;
		case ActionEventConstant.GET_SALE_ORDER:
			data = getListSalesOrderInDate(e);
			break;
		case ActionEventConstant.GET_ORDER_FOR_EDIT:
			data = getOrderForEdit(e);
			break;
		case ActionEventConstant.GET_PO:
			data = getPO(e);
			break;
		case ActionEventConstant.NOTE_LIST_VIEW:
		case ActionEventConstant.GET_LIST_CUS_FEED_BACK:
			data = getListCusFeedBack(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_BASE_INFO:
			data = getCustomerBaseInfo(e);
			break;
		case ActionEventConstant.GET_GENERAL_STATISTICS_REPORT_INFO:
			data = getGeneralStatisticsReportInfo(e);
			break;
		case ActionEventConstant.UPDATE_NOTE_LIST_GENERAL_STATICTICS_VIEW:
			data = getListNoteForGeneralStatisticsView(e);
			break;
		case ActionEventConstant.UPDATE_NOTE_STATUS:
			data = requestGetUpdateNoteStatusToDB(e);
			break;
		case ActionEventConstant.GET_PROMOTION_PRODUCT_FROM_SALE_PRODUCT:
			data = getPromotionProducts(e);
			break;
		case ActionEventConstant.DELETE_SALE_ORDER:
			data = cancelSaleOrder(e);
			break;
		case ActionEventConstant.POST_FEEDBACK:
			data = postFeedBack(e);
			break;
		case ActionEventConstant.UPDATE_FEEDBACK:
			data = updateDoneDateFeedBack(e);
			break;
		case ActionEventConstant.DELETE_FEEDBACK:
			data = deleteFeedBack(e);
			break;
		case ActionEventConstant.START_INSERT_ACTION_LOG:
			data = inserVisitActionLogToDB(e);
			break;
		case ActionEventConstant.INSERT_ACTION_LOG:
			data = inserActionLogToDB(e);
			break;
		case ActionEventConstant.DELETE_ACTION_LOG:
			data = deleteActionLogToDB(e);
			break;
		case ActionEventConstant.UPDATE_ACTION_LOG:
			data = updateActionLogToDB(e);
			break;
		case ActionEventConstant.SAVE_NUMBER_REMAIN_PRODUCT:
			data = requestSaveRemainProduct(e);
			break;
		case ActionEventConstant.GET_COMMON_DATA_ORDER:
			data = getCommonDataInOrder(e);
			break;
		case ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL:
			data = getPromotionDetail(e);
			break;
		case ActionEventConstant.UPDATE_CUSTOMER_LOATION:
			data = requestUpdateCustomerLocation(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_IMAGE_LIST:
			data = getCusImageList(e);
			break;
		case ActionEventConstant.GET_CUS_DEBIT_RETURN_DETAIL:
			data = requestDebitDetailReturn(e);
			break;
		case ActionEventConstant.GET_CUS_PAYMENT_RECEIVED_REFUSED:
			data = requestPaymentRefused(e);
			break;
		case ActionEventConstant.INSERT_MEDIA_ITEM:
			data = requestInsertMediaItem(e);
			break;
		case ActionEventConstant.GO_TO_PRODUCT_INFO_DETAIL:
			data = requestGetProductInfoDetail(e);
			break;
		case ActionEventConstant.GET_ORDER_IN_LOG:
			data = requestGetOrderInLog(e);
			break;
		case ActionEventConstant.ACTION_GET_CUSTOMER_SALE_LIST:
			data = requestGetCustomerSaleList(e);
			break;
		case ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE:
			data = requestUpdateExceptionOrderDate(e);
			break;
		case ActionEventConstant.GET_LIST_TYPE_PROBLEM_NVBH_GSNPP:
			data = getListTypeProblemNVBHGSNPP(e);
			break;
		case ActionEventConstant.GO_TO_IMAGE_LIST_GSNPP:
			data = getCusImageListGSNPP(e);
			break;
		case ActionEventConstant.ACTION_GET_CUSTOMER_DEBT:
			data = requestGetCustomerDebt(e);
			break;
		case ActionEventConstant.GET_CUS_DEBIT_DETAIL:
			data = requestDebitDetail(e);
			break;
		case ActionEventConstant.GET_TYPE_FEEDBACK:
			data = requestGetTypeFeedback(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_NO_PSDS:
			data = requestGetCusNoPSDS(e);
			break;
		case ActionEventConstant.GO_TO_LIST_ALBUM_PROGRAME:
			data = gotoListAlbumPrograme(e);
			break;
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME:
			data = getAlbumDetailPrograme(e);
			break;
		case ActionEventConstant.ACTION_LOAD_LIST_SELECT_CUSTOMER: {
			data = getListSelectCustomer(e);
			break;
		}
		case ActionEventConstant.INSERT_MEDIA_LOG: {
			data = insertMediaLog(e);
			break;
		}
		case ActionEventConstant.UPDATE_ENDTIME_MEDIA_LOG: {
			data = updateEndtimeMediaLog(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_REPORT_STAFF_SALE_DETAIL: {
			data = getCatAmountReport(e);
			break;
		}
		case ActionEventConstant.ACTION_LOAD_LIST_PPROBLEMS_FEEDBACK: {
			data = requestGetListProblemsFeedBack(e);
			break;
		}
		case ActionEventConstant.GET_LIST_DOCUMENT: {
			data = requestGetListDocument(e);
			break;
		}
		case ActionEventConstant.GET_LIST_CUSTOMER_VISITED: {
			data = getListCustomerVisited(e);
			break;
		}
		case ActionEventConstant.ACTION_CREATE_RETURN_ORDER: {
			data = createReturnOrder(e);
			break;
		}
		case ActionEventConstant.GET_SHOP_INFO: {
			data = getShopInfo(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_CUSTOMER_DEBIT: {
			data = getCustomerDebit(e);
			break;
		}
		case ActionEventConstant.GET_ORDER_DRAFT_TYPE: {
			data = requestGetOrderDraftType(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_LIST_PERIOD_INVENTORY: {
			data = getListPeriodInventory(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_INVENTORY_DEVICE: {
			data = getListInventoryDevice(e);
			break;
		}
		case ActionEventConstant.ACTION_SAVE_INVENTORY_DEVICE: {
			data = requestUpdateInventoryDevice(e);
			break;
		}
		case ActionEventConstant.INSERT_MEDIA_INVENTORY_DEVICE:
			data = requestInsertMediaInventory(e);
			break;
		case ActionEventConstant.GET_LIST_DYNAMIC_KPI:
			data = getListDynamicKPI(e);
			break;
		case ActionEventConstant.GET_LIST_PARAMETERS:
			data = getParameters(e);
			break;
		case ActionEventConstant.GET_DATA_CHOOSE_ROW:
			data = getDataReportChooseRow(e);
			break;
		case ActionEventConstant.GET_PRODUCT_LIST_FOR_REPORT:
			data = getProductListForReport(e);
			break;
		case ActionEventConstant.GET_CATEGORY_FOR_REPORT:
			data = getCategoryForReport(e);
			break;
		case ActionEventConstant.GET_DYNAMIC_REPORT:
			data = getDynamicReport(e);
			break;
		case ActionEventConstant.GET_WORKING_DAY:
			data = getWorkingDay(e);
			break;
		case ActionEventConstant.ACTION_READ_SHOP_LOCK:
			data = readShopLock(e);
			break;
		case ActionEventConstant.GET_LIST_NEW_CUSTOMER:{
			data=getListNewCustomer(e);
			break;
		}
		case ActionEventConstant.ACTION_DELETE_CUSTOMER:{
			data=deleteCustomerInfo(e);
			break;
		}
		case ActionEventConstant.GET_CREATE_CUSTOMER_INFO:
			data = getCreateCustomerInfo(e);
			break;
		case ActionEventConstant.ACTION_CREATE_CUSTOMER:
			data = insertCustomer(e);
			break;
		case ActionEventConstant.ACTION_UPDATE_CUSTOMER:
			data = updateCustomerInfo(e);
			break;
		case ActionEventConstant.GET_CREATE_CUSTOMER_AREA_INFO:
			data = getCreateCustomerAreaInfo(e);
			break;
		case ActionEventConstant.GET_EXTRA_CUSTOMER_INFO:
			data = getExtraCustomerInfo(e);
			break;
		case ActionEventConstant.GET_REPORT_CATEGORY:
			data = getReportCategory(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_BY_ID: {
			data = getCustomerById(e);
			break;
		}case ActionEventConstant.NVBH_GET_REPORT_FORCUS_PRODUCT_INFO_VIEW: {
			data = requestGetForcusProductInfo(e);
			break;
		}case ActionEventConstant.ACTION_GET_LIST_EQUIPMENT:
			data = getListEquipmentTakePhoto(e);
			break;
		case ActionEventConstant.ACTION_GET_LIST_IMAGE_EQUIPMENT:
			data = getListImageOfEquipment(e);
			break;
		case ActionEventConstant.ACTION_CHECK_LIST_IMAGE_EQUIPMENT:
			data = validateListImageOfEquipment(e);
			break;
		case ActionEventConstant.ACTION_GET_LIST_KEY_SHOP: 
			data = getListKeyShop(e);
			break;
		case ActionEventConstant.ACTION_GET_LIST_IMAGE_KEY_SHOP:
			data = getListImageOfKeyShop(e);
			break;
		case ActionEventConstant.ACTION_CHECK_LIST_IMAGE_KEY_SHOP:
			data = validateListImageOfKeyShop(e);
			break;
		case ActionEventConstant.ACTION_GET_ALL_LIST_KEYSHOP:
			data = getAllListKeyShop(e);
			break;
		case ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP:
			data = getLevelOfKeyShop(e);
			break;
		case ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP:
			data = updateRegisterKeyShop(e);
			break;
		case ActionEventConstant.GET_PROMOTION_DETAIL_KEYSHOP:
			data = getPromotionDetailKeyShop(e);
			break;
		case ActionEventConstant.GET_REPORT_KPI_SALE:
			data = getReportKPISale(e);
			break;
		case ActionEventConstant.ACTION_CHECK_KEYSHOP_CUSTOMER:
			data = checkKeyShopCustomer(e);
			break;
		case ActionEventConstant.NVBH_GET_REPORT_CUSTOMER_SALE:
			data = getReportCustomerSale(e);
			break;
		case ActionEventConstant.ACTION_GET_REPORT_CUSTOMER:
			data = getReportListCustomer(e);
			break;
		case ActionEventConstant.NVBH_GET_REPORT_ASO:
			data = getReportNvbhAso(e);
            break;
        case ActionEventConstant.GET_CHART_STAFF_DAY:
            data = requestChartStaffDay(e);
			break;
		case ActionEventConstant.REQUEST_MONTH_SALES_REVENUE:
			data = requestMonthSalesRevenue(e);
			break;
		default:// test
			break;
		}
		return data;
	}

	private ModelEvent requestMonthSalesRevenue(ActionEvent actionEvent) {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		Vector info = (Vector) actionEvent.viewData;
		re = sendHttpRequestChart("webresources/chartStaffMonth", info,
				actionEvent, NetworkTimeout.getCONNECT_TIME_OUT_LOGIN(), NetworkTimeout.getREAD_TIME_OUT_LOGIN(), true);
		return new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	private ModelEvent requestChartStaffDay(ActionEvent actionEvent) throws Exception{
        HTTPRequest re = null;
        actionEvent.type = ActionEvent.TYPE_ONLINE;
        Vector info = (Vector) actionEvent.viewData;
        re = sendHttpRequestChart("webresources/chartStaffDay", info,
                actionEvent, NetworkTimeout.getCONNECT_TIME_OUT_LOGIN(), NetworkTimeout.getREAD_TIME_OUT_LOGIN(), true);
        return new ModelEvent(actionEvent, re,
                ErrorConstants.ERROR_CODE_SUCCESS, "");
    }

    /**
	 * get report ASO for NVBH
	 * @author: duongdt3
	 * @time: 2:28:07 PM Oct 20, 2015
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getReportNvbhAso(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportNvbhAso(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	public ModelEvent getReviewShowroom(ActionEvent e){
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		return model;
	}

	public ModelEvent getReviewShowroom_GoodList(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		return model;
	}

	public ModelEvent getPaidPromotionList(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		return model;
	}

	/**
	 * Request gach no
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: HTTPRequest
	 * @throws:
	 * @param e
	 * @return
	 */
	public ModelEvent payDebt(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		CustomerDebitDetailDTO cusDebitDetailDto = (CustomerDebitDetailDTO) e.viewData;
		boolean result = SQLUtils.getInstance().createPayReceived(cusDebitDetailDto);
		if (result) {
			JSONArray listSql = cusDebitDetailDto.generatePayDebtSql();
			sendPayDebtSqlToServer(e, listSql);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("DEBIT ID: " + cusDebitDetailDto.debitDTO.id,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage("Gach no ko thanh cong");
		}
		return model;
	}

	/**
	 * Lay ds san pham
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getProductList(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ListProductDTO dto = SQLUtils.getInstance().getProductList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds ma code cua sp
	 *
	 * @author:
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListCategoryCodeProduct(ActionEvent e) {
		CategoryCodeDTO dto = SQLUtils.getInstance().getListCategoryCodeProduct();
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 11:29:09 AM
	 */
	@SuppressWarnings({ "unchecked" })
	public ModelEvent getLastSaleOrders(ActionEvent e) throws Exception {
		ArrayList<SaleOrderCustomerDTO> lastOrderCustomer = null;
		Vector<Object> data = (Vector<Object>) e.viewData;
		String customerId = null;
		String page = null, numTop = null;
		if (data.lastIndexOf(IntentConstants.INTENT_CUSTOMER_ID) >= 0) {
			customerId = data.get(
					data.lastIndexOf(IntentConstants.INTENT_CUSTOMER_ID) + 1)
					.toString();
		}
		if (data.lastIndexOf(IntentConstants.INTENT_PAGE) >= 0) {
			page = data.get(data.lastIndexOf(IntentConstants.INTENT_PAGE) + 1)
					.toString();
		}
		if (data.lastIndexOf(IntentConstants.INTENT_NUMTOP) >= 0) {
			numTop = data.get(
					data.lastIndexOf(IntentConstants.INTENT_NUMTOP) + 1)
					.toString();
		}
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		lastOrderCustomer = SQLUtils.getInstance().getLastSaleOrders(
				customerId, shopId, Integer.parseInt(page),
				Integer.parseInt(numTop),1);
		return new ModelEvent(e, lastOrderCustomer, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin doanh so khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent getSaleLevelCustomer(ActionEvent e) throws Exception {
		Vector<Object> data = (Vector<Object>) e.viewData;
		String customerId = null;
		if (data.lastIndexOf(IntentConstants.INTENT_CUSTOMER_ID) >= 0) {
			customerId = data.get(
					data.lastIndexOf(IntentConstants.INTENT_CUSTOMER_ID) + 1)
					.toString();
		}
		ArrayList<CustomerCatLevelDTO> lastOrderCustomer = new ArrayList<CustomerCatLevelDTO>();
		CUSTOMER_CATEGORY_LEVEL_TABLE cusCat = new CUSTOMER_CATEGORY_LEVEL_TABLE(
				SQLUtils.getInstance().getmDB());

		lastOrderCustomer = cusCat.getListCustomerCatLevel(customerId);
		return new ModelEvent(e, lastOrderCustomer, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds kiem ton
	 *
	 * @author: HieuNH
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getRemainProduct(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		ListRemainProductDTO result = SQLUtils.getInstance().getRemainProduct(data);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	/**
	 * Lay ds kiem ton
	 *
	 * @author: HieuNH
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getLastOrdinalRemainProduct(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		Bundle result = SQLUtils.getInstance().getLastOrdinalRemainProduct(data);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Xoa mot don hang
	 *
	 * @author: PhucNT
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent deleteDraftSaleOrder(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		String draftOrderId = bundle.getString(IntentConstants.INTENT_ORDER_ID);
		SaleOrderViewDTO dto = new SaleOrderViewDTO();
		dto.saleOrder.saleOrderId = Long.valueOf(draftOrderId);
		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				SQLUtils.getInstance().getmDB());
		List<SaleOrderDetailDTO> listSaleOrderDetail = new ArrayList<SaleOrderDetailDTO>();
		listSaleOrderDetail = orderDetailTable
				.getAllDetailOfSaleOrder(dto.saleOrder.saleOrderId);

		ActionLogDTO actionLogDTO = new ActionLogDTO();
		actionLogDTO.objectType = "4";
		actionLogDTO.routingId = dto.saleOrder.routingId;
		// actionLogDTO.objectId = String.valueOf(dto.saleOrder.saleOrderId);
		// gp thay doi luu objectId: saleOrderId -> POCustomerId
		actionLogDTO.objectId = String.valueOf(dto.saleOrder.fromPOCustomerId);
		actionLogDTO.staffId = GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId();
		actionLogDTO.shopId = Long.valueOf(GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());

		// Check order is last order of customer
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(SQLUtils
				.getInstance().getmDB());
		SaleOrderDTO saleOrder = orderTable
				.getSaleById(dto.saleOrder.saleOrderId);
		boolean result = true;
		if (saleOrder != null) {
			dto.saleOrder = saleOrder;
			dto.lastOrder = orderTable.getPreviousLastOrderDate(dto);
			if (!StringUtil.isNullOrEmpty(dto.lastOrder)
					&& dto.lastOrder.equals(dto.saleOrder.orderDate)) {
				dto.isFinalOrder = 0;
			} else {
				dto.isFinalOrder = 1;
			}
			result = SQLUtils.getInstance().deleteDraftSaleOrder(dto, listSaleOrderDetail, actionLogDTO);
		}
		if (result) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_SUCCESS_DELETE_ORDER));
		} else {
			ServerLogger.sendLog("Xoa don hang " + dto.getSaleOrderId()
					+ " khong thanh cong.", TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}

		return model;
	}
	
	public boolean deleteDraftSaleOrder(String draftOrderId) throws Exception {
		SaleOrderViewDTO dto = new SaleOrderViewDTO();
		dto.saleOrder.saleOrderId = Long.valueOf(draftOrderId);
		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				SQLUtils.getInstance().getmDB());
		List<SaleOrderDetailDTO> listSaleOrderDetail = new ArrayList<SaleOrderDetailDTO>();
		listSaleOrderDetail = orderDetailTable
				.getAllDetailOfSaleOrder(dto.saleOrder.saleOrderId);

		ActionLogDTO actionLogDTO = new ActionLogDTO();
		actionLogDTO.objectType = "4";
		actionLogDTO.routingId = dto.saleOrder.routingId;
		// actionLogDTO.objectId = String.valueOf(dto.saleOrder.saleOrderId);
		// gp thay doi luu objectId: saleOrderId -> POCustomerId
		actionLogDTO.objectId = String.valueOf(dto.saleOrder.fromPOCustomerId);
		actionLogDTO.staffId = GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId();
		actionLogDTO.shopId = Long.valueOf(GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());

		// Check order is last order of customer
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(SQLUtils
				.getInstance().getmDB());
		SaleOrderDTO saleOrder = orderTable
				.getSaleById(dto.saleOrder.saleOrderId);
		boolean result = true;
		if (saleOrder != null) {
			dto.saleOrder = saleOrder;
			dto.lastOrder = orderTable.getPreviousLastOrderDate(dto);
			if (!StringUtil.isNullOrEmpty(dto.lastOrder)
					&& dto.lastOrder.equals(dto.saleOrder.orderDate)) {
				dto.isFinalOrder = 0;
			} else {
				dto.isFinalOrder = 1;
			}
			result = SQLUtils.getInstance().deleteDraftSaleOrder(dto, listSaleOrderDetail, actionLogDTO);
		}

		return result;
	}

	/**
	 *
	 * get list display program product with customerId
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent requestGetListDisplayProgramProduct(ActionEvent event) throws Exception {
		Bundle data = (Bundle) event.viewData;
		List<DisplayPresentProductInfo> dto = SQLUtils.getInstance().getListDisplayProgramProduct(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay doanh so theo nganh trong 3 thang gan nhat
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 */
	public ModelEvent getCustomerCatAmountInNearly3Month(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		CustomerCatAmountPopupViewDTO dto = SQLUtils.getInstance().getCustomerCatAmountInNearly3Month(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 *
	 * lay thong tin cho man hinh cham trung bay hien dien mat hang: lay d/s
	 * chuong trinh trung bay & d/s 10 san pham dau tien cho chuong trinh trung
	 * bay dau tien
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetVoteDisplayProgrameView(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		VoteDisplayPresentProductViewDTO dto = SQLUtils.getInstance()
				.getVoteDisplayProgrameProductViewData(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay don hang luu tam
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent requestGetOrderDraft(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = bundle
				.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String dto = SQLUtils.getInstance().getOrderDraft(staffId, customerId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list vote display product
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListVoteDisplayProduct(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		VoteDisplayPresentProductViewDTO dto = SQLUtils.getInstance().getListVoteDisplayProduct(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * update vote number product display and update to server
	 *
	 * @author: HaiTC3
	 * @param actionEvent
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent requestUpdateVoteProductDisplay(ActionEvent actionEvent)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		int result = 0;
		Bundle data = (Bundle) actionEvent.viewData;
		ListCustomerDisplayProgrameScoreDTO listDisplayProductVoted = (ListCustomerDisplayProgrameScoreDTO) data
				.getSerializable(IntentConstants.INTENT_LIST_DISPLAY_PROGRAM_PRODUCT);

		listDisplayProductVoted = SQLUtils.getInstance()
				.updateCustomerDisplayProgrameScoreDTO(listDisplayProductVoted);

		result = SQLUtils.getInstance().insertVoteDisplayProgrameProduct(
				listDisplayProductVoted);
		if (result == 1) {
			JSONArray listSql = listDisplayProductVoted
					.generateNewVoteDisplayList();
			genJsonSendToServer(actionEvent, listSql);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			ServerLogger
					.sendLog(
							"method requestUpdateVoteProductDisplay error: ",
							"sql insert vote display programe product to db local error or update maxId error",
							TabletActionLogDTO.LOG_CLIENT);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}


	/**
	 *
	 * khoi tao cac textview de thuc hien autoComplete
	 *
	 * @author: HieuNH6
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetInitListProductAddToOrderView(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		String productInfoCode = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		AutoCompleteFindProductSaleOrderDetailViewDTO dto = SQLUtils.getInstance().getInitListProductAddToOrderView(data);
		dto.lstCat = SQLUtils.getInstance().getListDepartDisplayPrograme();
		dto.lstSubcat = SQLUtils.getInstance().getListSubcat(productInfoCode);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * request get list product
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListProductAddToOrderView(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		ListFindProductSaleOrderDetailViewDTO dto = SQLUtils.getInstance().getListProductAddToOrderView(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds hinh anh CTTB
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListImageCTTB(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		String objectId = data.getString(IntentConstants.INTENT_OBJECT_ID);
		int objectType = data.getInt(IntentConstants.INTENT_OBJECT_TYPE);
		String displayProgrameId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		ArrayList<MediaItemDTO> dto = SQLUtils.getInstance().getListImageCTTB(objectId, objectType,
				displayProgrameId, shopId, staffId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Kiem tra don hang da tao trong action log chua
	 *
	 * @author:
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent checkCreateOrderFromActionLog(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = viewInfo
				.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		ActionLogDTO dto = SQLUtils.getInstance().checkCreateOrderFromActionLog(staffId,
				customerId,shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 *
	 * lay danh sach tim kiem hinh anh
	 *
	 * @author: YenNTH
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListAlbum(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ImageSearchViewDTO dto = SQLUtils.getInstance().getListAlbum(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach khach hang tham gia chuong trinh trung bay
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListCustomerAttentPrograme(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		Bundle data = (Bundle) e.viewData;
		String extPage = data.getString(IntentConstants.INTENT_PAGE);
		String displayProgrameCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String customerCode = data
				.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String customerName = data
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		long displayProgrameId = data
				.getLong(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		ListCustomerAttentProgrameDTO dto = SQLUtils.getInstance().getListCustomerAttentPrograme(extPage,
				displayProgrameCode, displayProgrameId, customerCode,
				customerName, staffId, checkPagging);
		boolean repareReset = data.getBoolean(IntentConstants.INTENT_CHECK_PAGGING);
		if (!repareReset) {
			e.tag = 11;
		}
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list programe for product
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListProgrameForProduct(ActionEvent event) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		String customer_id = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String customerTypeId = data
				.getString(IntentConstants.INTENT_CUSTOMER_TYPE_ID);
		String shop_id = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staff_id = data.getString(IntentConstants.INTENT_STAFF_ID);
		String orderType = data.getString(IntentConstants.INTENT_ORDER_TYPE);
		List<ProgrameForProductDTO> listPrograme = new ArrayList<ProgrameForProductDTO>();
		listPrograme = SQLUtils.getInstance().getListProgrameForProduct(
				shop_id, staff_id, customer_id, customerTypeId, orderType);
		return new ModelEvent(event, listPrograme, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Lay bao cao tien do CTTB
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 */
	public ModelEvent getReportDisplayProgressDetailDay(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		ReportDisplayProgressDetailDayDTO dto = SQLUtils.getInstance().getReportDisplayProgressDetailDay(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin gioi thieu san pham
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getIntroduceProduct(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) event.viewData;
		String productId = data.getString(IntentConstants.INTENT_PRODUCT_ID);
		IntroduceProductDTO dto =  SQLUtils.getInstance().getIntroduceProduct(productId);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * Lay danh sach loai CT va danh sach nganh hang
	 *
	 * @author: ThanhNN8
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListComboboxDisplayPrograme(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		String productInfoCode = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		ComboboxDisplayProgrameDTO result = new ComboboxDisplayProgrameDTO();
		result.listDepartPrograme = SQLUtils.getInstance().getListDepartDisplayPrograme();
		result.listSubcat = SQLUtils.getInstance().getListSubcat(productInfoCode);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay bao cao tien do CTTB
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 */
	public ModelEvent getReportDisplayProgressDetailCustomer(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		ReportDisplayProgressDetailCustomerDTO dto =  SQLUtils.getInstance().getReportDisplayProgressDetailCustomer(
				data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent updateClientThumbnailUrl(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) event.viewData;
		MediaItemDTO dto = (MediaItemDTO) data.get(IntentConstants.INTENT_DATA);
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		int result =  SQLUtils.getInstance().updateClientThumbnailUrl(dto);
		if(result > 0){
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		model.setModelData(result);
		return model;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListAlbumUser(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ListAlbumUserDTO dto =  SQLUtils.getInstance().getAlbumUserInfoCTTB(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds hinh anh cua album
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getAlbumDetailUser(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String type = String.valueOf(data
				.getInt(IntentConstants.INTENT_ALBUM_TYPE));
		String numTop = String.valueOf(data
				.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE));
		String page = String.valueOf(data.getInt(IntentConstants.INTENT_PAGE));
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		PhotoThumbnailListDto result = null;
		result = new PhotoThumbnailListDto();
		ArrayList<PhotoDTO> listPhoto = SQLUtils.getInstance()
				.getAlbumDetailUser(customerId, type, numTop, page, shopId);
		result.getAlbumInfo().getListPhoto().addAll(listPhoto);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds don hang chua send thanh cong
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getNoSuccessOrderList(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ArrayList<String> listOrderId = data
				.getStringArrayList(IntentConstants.INTENT_ARRAY_STRING_LIST);
		NoSuccessSaleOrderDto dto =  SQLUtils.getInstance().getNoSuccessOrderList(listOrderId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay tat ca don hang chua chuyen thanh cong: bao gom chua goi len server,
	 * don hang loi bi tra ve....
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetAllOrderFail(ActionEvent e) throws Exception {
		NoSuccessSaleOrderDto dto =  SQLUtils.getInstance().getAllOrderFail();
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * request get list industry product and list product for the first industry
	 * in list
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 20, 2012
	 */
	public ModelEvent requestGetListIndustryProductAndListProduct(ActionEvent e) throws Exception {
		e.isNeedCheckTimeServer = false;
		SaleStatisticsProductInDayInfoViewDTO dto = SQLUtils.getInstance().getListIndustryProductAndListProduct((Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list product sold
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 20, 2012
	 */
	public ModelEvent requestListProductPreSaleSold(ActionEvent e) throws Exception {
		ArrayList<SaleProductInfoDTO> dto =  SQLUtils.getInstance().getListProductPreSaleSold(
				(Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list product van sale sold
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 17, 2013
	 */
	public ModelEvent requestListProductVanSaleSold(ActionEvent e) throws Exception {
		e.isNeedCheckTimeServer = false;
		ArrayList<SaleProductInfoDTO> dto =  SQLUtils.getInstance().getListProductVanSaleSold(
				(Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach san pham trong man hinh don tong luy ke ngay
	 *
	 * @author: HieuNH6
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestSaleStatisticsAccumulateDay(ActionEvent e)
			throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;

		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		String productCode = viewInfo
				.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = viewInfo
				.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String industry = viewInfo.getString(IntentConstants.INTENT_INDUSTRY);
		String page = viewInfo.getString(IntentConstants.INTENT_PAGE);
		ArrayList<String> dto =  SQLUtils.getInstance().getSaleStatisticsAccumulateDay(shopId,
				staffId, productCode, productName, industry, page);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach san pham trong man hinh don tong luy ke ngay
	 *
	 * @author: HieuNH6
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestSaleStatisticsAccumulateDayListProduct(ActionEvent e)
			throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;

		SaleStatisticsAccumulateDayDTO dto =  SQLUtils.getInstance()
				.getSaleStatisticsAccumulateDayListProduct(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay count danh sach san pham trong man hinh don tong luy ke ngay
	 *
	 * @author: HieuNH6
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestCountSaleStatisticsAccumulateDayListProduct(
			ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;

		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		String productCode = viewInfo
				.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = viewInfo
				.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String industry = viewInfo.getString(IntentConstants.INTENT_INDUSTRY);
		Object dto = SQLUtils.getInstance()
				.getCountSaleStatisticsAccumulateDayListProduct(shopId,
						staffId, productCode, productName, industry);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Request tao don hang
	 *
	 * @author: TruongHN
	 * @param actionEvent
	 * @return: HTTPRequest
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent requestCreateOrder(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		// insert to sql Lite & request to server
		OrderViewDTO order = (OrderViewDTO) actionEvent.viewData;
		SaleOrderDataResult result = SQLUtils.getInstance().createOrder(order);
		if (result.isCreateSqlLiteSuccess) {
			// Khong phai don hang tam thi chuyen len server
			// Luc tao 1 don hang moi thi approved :
			// = 0 : doi voi presales
			// = 1 : doi voi vansales
			if (order.orderInfo.approved == 0 || order.orderInfo.approved == 1) {
				JSONArray listSql = order.generateNewOrderSql();
				order.generateNewPOCustomerSql(listSql);
				// don valsale khong can check thoi gian
				if (order.orderInfo.orderType
						.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
					actionEvent.isNeedCheckTimeServer = false;
				}
				sendCreateOrderSqlToServer(actionEvent, listSql,
						order.orderInfo.saleOrderId);
			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

		} else {
			ServerLogger.sendLog("order id: " + order.orderInfo.saleOrderId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);

			model.setModelCode(ErrorConstants.ERROR_COMMON);
			if (result.listPromotionCode != null
					&& !result.listPromotionCode.isEmpty()) {
				model.setModelMessage(StringUtil.getString(R.string.TEXT_PROGRAM_NOT_ENOUGHT)
						+ result.listPromotionCode.toString());
			} else {
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			}
		}
		return model;
	}

	/**
	 * Generate sql tao don hang gui server
	 *
	 * @author: Nguyen Thanh Dung
	 * @param actionEvent
	 * @param order
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	private void sendCreateOrderSqlToServer(ActionEvent actionEvent,
			JSONArray listSql, long orderId) {
		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		// para.add(IntentConstants.INTENT_LIST_DECLARE);
		// para.add(sqlPara.get(0));
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_LOG_TYPE);
		para.add(SALE_ORDER_TABLE.TABLE_NAME);
//		para.add(IntentConstants.INTENT_STAFF_ID_PARA);
//		para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());
		sendHttpRequestOffline("queryController/executeSql", para, actionEvent,
				LogDTO.TYPE_ORDER, String.valueOf(orderId),
				SALE_ORDER_TABLE.TABLE_NAME);
	}

	/**
	 * Generate sql tao don hang gui server - clone cho PO_CUSTOMER, PO_CUSTOMER_DETAIL
	 *
	 * @author: DungNX3
	 * @param actionEvent
	 * @param order
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	@SuppressWarnings("unused")
	private void sendPOCustomerSqlToServer(ActionEvent actionEvent,
			JSONArray listSql, long orderId) {

		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		// para.add(IntentConstants.INTENT_LIST_DECLARE);
		// para.add(sqlPara.get(0));
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
//		para.add(IntentConstants.INTENT_STAFF_ID_PARA);
//		para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());
		sendHttpRequestOffline("queryController/executeSql", para, actionEvent,
				LogDTO.TYPE_ORDER, String.valueOf(orderId),
				PO_CUSTOMER_TABLE.TABLE_NAME);
	}

	/**
	 * chuyen don hang
	 *
	 * @author: PhucNT
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestTranferListOrder2(ActionEvent actionEvent) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);

		SaleOrderViewDTO order = (SaleOrderViewDTO) actionEvent.viewData;
		// Update is_send field
		// order.saleOrder.isSend = 1;
		boolean updateSuccess = SQLUtils.getInstance().updateSentOrder(
				order.saleOrder);

		long orderId = order.saleOrder.saleOrderId;

		if (updateSuccess) {
			if (order.saleOrder.synState == 2) {// order has exist on server
				JSONArray listSql = new JSONArray();
				listSql.put(order.saleOrder.generateUpdateSentOrderSql());
				sendCreateOrderSqlToServer(actionEvent, listSql, orderId);
			} else {
				OrderViewDTO result = new OrderViewDTO();
				result.listBuyOrders = new ArrayList<OrderDetailViewDTO>();
				result.listPromotionOrders = new ArrayList<OrderDetailViewDTO>();

				ListSaleOrderDTO listChosenProduct = SQLUtils.getInstance()
						.getSaleOrderForSend(orderId);
				result.orderInfo = listChosenProduct.saleOrderDTO;

				for (OrderDetailViewDTO detail : listChosenProduct.listData) {
					if (detail.orderDetailDTO.isFreeItem == 0) {
						result.listBuyOrders.add(detail);
					} else {
						result.listPromotionOrders.add(detail);
					}
				}

				// result.listBuyOrders = listChosenProduct.listData;
				//
				// // Get ds promotion from sqlite
				// ListSaleOrderDTO listPromotionProduct =
				// SQLUtils.getInstance().getPromotionProductsForSend(orderId);
				// result.listPromotionOrders =
				// listPromotionProduct.listData;

				result.orderInfo.updateDate = order.saleOrder.updateDate;// DateUtils.now();
				result.orderInfo.updateUser = order.saleOrder.updateUser;// GlobalInfo.getInstance().getProfile().getUserData().userCode;
				// result.orderInfo.importCode =
				// order.saleOrder.importCode;// ""

				JSONArray listSql = result.generateNewOrderSql();
				sendCreateOrderSqlToServer(actionEvent, listSql, orderId);
			}

			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		} else {
			ServerLogger.sendLog("Chuyen don hang " + order.getSaleOrderId()
					+ " khong thanh cong ", TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}

		return model;
	}

	/**
	 * TODO Sua order tren local va goi len server.
	 *
	 * @author: Nguyen Thanh Dung
	 * @param :
	 * @return : HTTPRequest
	 * @throws :
	 * @time : 2:20:42 PM
	 */
	public ModelEvent requestEditAndSendOrder(ActionEvent actionEvent)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		// insert to sql Lite & request to server
		OrderViewDTO order = (OrderViewDTO) actionEvent.viewData;
		List<SaleOrderDetailDTO> listSaleOrderDetail = new ArrayList<SaleOrderDetailDTO>();
		SALES_ORDER_DETAIL_TABLE orderDetailTable = new SALES_ORDER_DETAIL_TABLE(
				SQLUtils.getInstance().getmDB());
		SALE_ORDER_TABLE orderTable = new SALE_ORDER_TABLE(SQLUtils
				.getInstance().getmDB());
		listSaleOrderDetail = orderDetailTable
				.getAllDetailOfSaleOrder(order.orderInfo.saleOrderId);

		// Save syn state
		int synState = order.orderInfo.synState;
		order.orderInfo.synState = 0; // reset -> 0 to show status
		// Get last order khi cap nhat don
		order.lastOrder = orderTable.getPreviousLastOrderDate(order);
		if (!StringUtil.isNullOrEmpty(order.lastOrder)
				&& order.lastOrder.equals(order.orderInfo.orderDate)) {
			order.isFinalOrder = 0;
		} else {
			order.isFinalOrder = 1;
		}
		SaleOrderDataResult result = SQLUtils.getInstance().updateOrder(order,
				listSaleOrderDetail);
		order.orderInfo.synState = synState;

		if (result.isCreateSqlLiteSuccess) {
			// synstate = 2 => update bill
			if (order.orderInfo.synState == 2) {// order has exist on server
				JSONArray listSql = SQLUtils.getInstance()
						.generateSqlUpdateOrder(order, listSaleOrderDetail);
				// PO
				if (order.orderInfo.poId > 0) {
					SQLUtils.getInstance().generateSqlUpdatePOCustomer(order,
							listSaleOrderDetail, listSql);
				}
				// don valsale khong can check thoi gian
				if (order.orderInfo.orderType
						.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
					actionEvent.isNeedCheckTimeServer = false;
				}
				sendCreateOrderSqlToServer(actionEvent, listSql,
						order.orderInfo.saleOrderId);

				// sendCreateOrderSqlToServer(actionEvent, listPOSql,
				// order.orderInfo.saleOrderId);
			} else {
				// approved = 0 => bill create => create order sql
				// approved = 1 -> vansale -> tao don
				if (order.orderInfo.approved == 0
						|| (order.orderInfo.approved == 1 && order.orderInfo.orderType
								.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE))) {
					JSONArray listSql = order.generateNewOrderSql();
					// tao don PO
					order.generateNewPOCustomerSql(listSql);
					sendCreateOrderSqlToServer(actionEvent, listSql,
							order.orderInfo.saleOrderId);
					// sendPOCustomerSqlToServer(actionEvent, listPOSql,
					// order.orderInfo.saleOrderId);
				}
			}

			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("order id: " + order.orderInfo.saleOrderId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);

			model.setModelCode(ErrorConstants.ERROR_COMMON);
			if (result.listPromotionCode != null) {
				model.setModelMessage(StringUtil.getString(R.string.TEXT_PROGRAM_NOT_ENOUGHT)
						+ result.listPromotionCode.toString());
			} else {
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			}
		}
		return model;
	}

	/**
	 * Lay ds chuong trinh khuyen mai
	 *
	 * @author: SoaN
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListPromotionPrograme(ActionEvent event) throws Exception {
		Bundle bundle = (Bundle) event.viewData;
		PromotionProgrameModel result = SQLUtils.getInstance().getListPromotionPrograme(bundle);
		boolean checkLoadMore = bundle.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		if (checkLoadMore == false) {
			event.tag = 11;
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds chuong trinh khuyen mai
	 *
	 * @author: SoaN
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListDisplayPrograme(ActionEvent event)
			throws Exception {
		Bundle ext = (Bundle) event.viewData;
		boolean checkRequestCombobox = ext.getBoolean(
				IntentConstants.INTENT_CHECK_COMBOBOX, false);
		boolean repareReset = ext.getBoolean(IntentConstants.INTENT_CHECK_PAGGING);
		DisplayProgrameModel result = null;
		result = SQLUtils.getInstance().getListDisplayPrograme(ext);
		if (checkRequestCombobox) {
			ComboboxDisplayProgrameDTO comboboxModel = new ComboboxDisplayProgrameDTO();
			comboboxModel.listDepartPrograme = SQLUtils.getInstance()
					.getListDepartDisplayPrograme();
			comboboxModel.listTypePrograme = SQLUtils.getInstance()
					.getListTypeDisplayPrograme();
			result.setComboboxDTO(comboboxModel);
		} else {
			result.setComboboxDTO(null);
		}
		if (!repareReset) {
			event.tag = 11;
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS,
				"");
	}

	/**
	 * Lay ds san pham chuong trinh trung bay
	 *
	 * @author: SoaN
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetDisplayProgrameItem(ActionEvent event) throws Exception {
		Bundle ext = (Bundle) event.viewData;
		DisplayProgrameItemModel result = SQLUtils.getInstance().getListDisplayProgrameItem(ext);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	/**
	 * Lay thong tin chuong trinh trung bay
	 *
	 * @author: SoaN
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetDisplayProgrameInfo(ActionEvent event) throws Exception {
		Bundle ext = (Bundle) event.viewData;

		DisplayProgrameDTO programeDTO = null;
		List<DisplayProgrameLvDTO> lvDTO = null;
		DisplayProgrameViewDTO viewDTO = null;
		programeDTO = SQLUtils.getInstance().getDisplayProgrameInfo(ext);
		lvDTO = SQLUtils.getInstance().getDisplayLvByProgrameCode(ext);
		//
		viewDTO = new DisplayProgrameViewDTO();
		viewDTO.setCode(programeDTO.displayProgrameCode);
		viewDTO.setName(programeDTO.displayProgrameName);
		viewDTO.setStartDate(programeDTO.fromDate);
		viewDTO.setEndDate(programeDTO.toDate);
		viewDTO.setDisplayProgLevel(lvDTO);
		return new ModelEvent(event, viewDTO, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	/**
	 * getListCustomer
	 *
	 * @author: TamPQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getListCustomer(ActionEvent actionEvent) throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = bundle.getInt(IntentConstants.INTENT_SHOP_ID);
		String code = bundle.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String name = bundle.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		// String address = bundle
		// .getString(IntentConstants.INTENT_CUSTOMER_ADDRESS);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		String visit_plan = bundle.getString(IntentConstants.INTENT_VISIT_PLAN);
		String isGetWrongPlan = bundle
				.getString(IntentConstants.INTENT_GET_WRONG_PLAN);
		boolean getWrongPlan = false;
		if (isGetWrongPlan.equals("1")) {
			getWrongPlan = true;
		}
		int isGetTotalPage = bundle
				.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);
		Object dto = SQLUtils.getInstance().getCustomerList(staffId, shopId, name,
				code, null, visit_plan, getWrongPlan, page, isGetTotalPage, sortInfo);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	 /**
	 * Kiem tra da ghe tham hay chua
	 * @author:
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent checkVisitFromActionLog(ActionEvent e) throws Exception {
		@SuppressWarnings("rawtypes")
		Vector viewInfo = (Vector) e.viewData;

		int staffId = Integer.parseInt(viewInfo.get(
				viewInfo.lastIndexOf(IntentConstants.INTENT_STAFF_ID) + 1)
				.toString());
		long shopId = Integer.parseInt(viewInfo.get(
				viewInfo.lastIndexOf(IntentConstants.INTENT_SHOP_ID) + 1)
				.toString());
		ActionLogDTO dto = SQLUtils.getInstance().checkVisitFromActionLog(
				staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * get danh sach khach hang trong tuyen
	 *
	 * @author : BangHN since : 1.0
	 */
	public ModelEvent getCustomerListForRoute(ActionEvent actionEvent)
			throws Exception {
		Bundle b = (Bundle) actionEvent.viewData;
		int staffId = Integer.parseInt(b
				.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(b
				.getString(IntentConstants.INTENT_SHOP_ID));
		String visitPlan = b.getString(IntentConstants.INTENT_VISIT_PLAN);
		String isGetWrongPlan = b
				.getString(IntentConstants.INTENT_GET_WRONG_PLAN);

		boolean isFromRouteView = b
				.getBoolean(IntentConstants.INTENT_IS_FROM_ROUTE_VIEW);
		boolean getWrongPlan = false;
		if (isGetWrongPlan.equals("1")) {
			getWrongPlan = true;
		}
		CustomerListDTO dto = SQLUtils.getInstance().getCustomerListForRoute(
				staffId, shopId, visitPlan, getWrongPlan, isFromRouteView);
//		GeomDTO nvbh = SQLUtils.getInstance().getPosition(staffId);
		ArrayList<GeomDTO> nvbh = SQLUtils.getInstance().getPosition(staffId);
		List<Integer> removeIds = new ArrayList<Integer>();
		if (nvbh != null) {
			long timePre = !nvbh.isEmpty() ? DateUtils.getNumMilisecond(nvbh.get(0).createDate, DateUtils.DATE_FORMAT_HOUR_MINUTE) : 0;
			//khong duyet qua diem dau, diem cuoi
			for (int i = 1, size = nvbh.size(); i < size - 1; i++) {
				GeomDTO pos = nvbh.get(i);
				long time = DateUtils.getNumMilisecond(pos.createDate, DateUtils.DATE_FORMAT_HOUR_MINUTE);

				//ban kinh chinh xac nho hon quy dinh || tgian vi tri qua gan nhau
				if (pos.accuracy > GlobalInfo.getInstance().getRadiusDrawRouting()
						|| (pos.type == 1 && time - timePre < GlobalInfo.getInstance().getDistanceTimeDrawRouting())) {
					//insert head list
					removeIds.add(0, i);
				} else{
					//time pre is now time
					timePre = time;
				}
			}

			//remove list position too close
			for (Integer index : removeIds) {
				nvbh.remove(index.intValue());
			}
		}

		ArrayList<Object> data = new ArrayList<Object>();
		data.add(dto);
		data.add(nvbh);
		return new ModelEvent(actionEvent, data, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * get danh sach khach hang trong tuyen cho ban do tbhv
	 *
	 * @author : TamPQ since : 1.0
	 */
	public ModelEvent getTBHVCustomerInVisitPlan(ActionEvent actionEvent)
			throws Exception {

		Bundle b = (Bundle) actionEvent.viewData;

		int staffId = Integer.parseInt(b
				.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(b
				.getString(IntentConstants.INTENT_SHOP_ID));
		String visitPlan = b.getString(IntentConstants.INTENT_VISIT_PLAN);
		String isGetWrongPlan = b
				.getString(IntentConstants.INTENT_GET_WRONG_PLAN);

		boolean getWrongPlan = false;
		if (isGetWrongPlan.equals("1")) {
			getWrongPlan = true;
		}
		Object dto = SQLUtils.getInstance().getTBHVCustomerInVisitPlan(staffId,
				shopId, visitPlan, getWrongPlan);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	/**
	 * get danh sach don hang trong ngay
	 *
	 * @author PhucNT
	 * @param ae
	 */
	public ModelEvent getListSalesOrderInDate(ActionEvent ae) throws Exception {
		ListOrderMngDTO listSaleOrderView = null;
		Bundle viewInfo = (Bundle) ae.viewData;
		String from_date = "", page = "", to_date = "", customer_code = "", customer_name = "", typeRoute = "", status = "", billCategory = "", staffId = "", shop_id = "";
		boolean bApproved = true;
		String customerId = "";
		boolean isGetTotalPage = false;
		boolean getListLog = false;
		DMSSortInfo sortInfo = null;
		String listStaff = Constants.STR_BLANK;

		if (viewInfo != null) {
			if (viewInfo.containsKey(IntentConstants.INTENT_LIST_STAFF)) {
				listStaff = viewInfo.getString(IntentConstants.INTENT_LIST_STAFF);
			}

			if (viewInfo.containsKey(IntentConstants.INTENT_FIND_ORDER_STAFFID)) {
				staffId = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_STAFFID);
			}

			if(viewInfo.containsKey(IntentConstants.INTENT_SORT_DATA)) {
				sortInfo = (DMSSortInfo) viewInfo.getSerializable(IntentConstants.INTENT_SORT_DATA);
			}

			if (viewInfo.containsKey(IntentConstants.INTENT_FIND_ORDER_SHOP_ID)) {
				shop_id = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_SHOP_ID);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_FROM_DATE)) {
				from_date = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
			}

			if (viewInfo.containsKey(IntentConstants.INTENT_FIND_ORDER_TO_DATE)) {
				to_date = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_CODE)) {
				customer_code = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_CODE);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_NAME)) {
				customer_name = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_NAME);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_TYPEROUTE)) {
				typeRoute = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_TYPEROUTE);
			}

			if (viewInfo.containsKey(IntentConstants.INTENT_FIND_ORDER_STATUS)) {
				status = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_STATUS);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_BILL_CATEGORY)) {
				billCategory = viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_BILL_CATEGORY);
			}

			if (viewInfo
					.containsKey(IntentConstants.INTENT_FIND_ORDER_BAPPROVED)) {
				if ("1".equals(viewInfo
						.getString(IntentConstants.INTENT_FIND_ORDER_BAPPROVED))) {
					bApproved = true;
				} else if ("0"
						.equals(viewInfo
								.getString(IntentConstants.INTENT_FIND_ORDER_BAPPROVED))) {
					bApproved = false;
				}
			}

			getListLog = viewInfo.getBoolean(IntentConstants.INTENT_GET_LIST_LOG);

			page = viewInfo.getString(IntentConstants.INTENT_PAGE);

			customerId = viewInfo.getString(IntentConstants.INTENT_CUSTOMER_ID);
			isGetTotalPage = viewInfo
					.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		}

		listSaleOrderView = SQLUtils.getInstance().getSalesOrderInDate(from_date, to_date,
				customer_code, customer_name, typeRoute, status, billCategory,
				staffId, shop_id, bApproved, page, customerId, isGetTotalPage, getListLog, sortInfo, listStaff);

		return new ModelEvent(ae, listSaleOrderView, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin & chi tiet don hang de view len khi xem & sua
	 *
	 * @author: Nguyen Thanh Dung
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getOrderForEdit(ActionEvent event) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle v = (Bundle) event.viewData;
		long orderId ;

		OrderViewDTO result = new OrderViewDTO();


		if (v.getString(IntentConstants.INTENT_ORDER) != null) {
			orderId = Long.parseLong(v
					.getString(IntentConstants.INTENT_ORDER));
		} else {
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			return model;
		}
		long customerId = v.getLong(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = v.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = v.getString(IntentConstants.INTENT_STAFF_ID);
		boolean isNeedGenPrice = v.getBoolean(IntentConstants.INTENT_IS_NEED_GEN_PRICE);
		if (isNeedGenPrice) {
			int customerTypeId = v.getInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID);
			
			Bundle dataGenPrice = new Bundle();
			dataGenPrice.putString(IntentConstants.INTENT_CUSTOMER_ID, String.valueOf(customerId));
			dataGenPrice.putString(IntentConstants.INTENT_SHOP_ID, String.valueOf(shopId));
			dataGenPrice.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
			
			// gen bang gia
			SQLUtils.getInstance().genPriceTemp(dataGenPrice);
		}
		
		// get ds from sqllite
		ListSaleOrderDTO listChosenProduct = SQLUtils.getInstance().getSaleOrderForEdit(orderId,customerId,shopId,staffId);
		result.orderInfo = listChosenProduct.saleOrderDTO;
		result.listBuyOrders = listChosenProduct.listData;

		// Get ds promotion from sqlite
		ListSaleOrderDTO listPromotionProduct = SQLUtils.getInstance()
				.getPromotionProductsForEdit(orderId,
						result.orderInfo.orderType,customerId,shopId,staffId);
		result.listPromotionOrders = listPromotionProduct.listData;

		//get quantity received
		ArrayList<SaleOrderPromotionDTO> quantityReceivedList = SQLUtils.getInstance().getQuantityReceivedForEdit(orderId);
		result.productQuantityReceivedList = quantityReceivedList;

		//get sale order promo detail cua KM tich luy
//			ArrayList<SaleOrderPromoDetailDTO> promoDetailList = SQLUtils.getInstance().getSaleOrderPromoDetailAccumulaction(orderId);
//			result.listPromoDetail = promoDetailList;

		//Cap nhat tien vao cac sp ban
		ArrayList<OrderDetailViewDTO> orderPromotionList = new ArrayList<OrderDetailViewDTO>();
		ArrayList<OrderDetailViewDTO> accumulationPromotionList = new ArrayList<OrderDetailViewDTO>();
		ArrayList<OrderDetailViewDTO> listPromotionNewOpenSaleOrder = new ArrayList<OrderDetailViewDTO>();
		ArrayList<OrderDetailViewDTO> orderPromotionKeyShop = new ArrayList<OrderDetailViewDTO>();
		for (OrderDetailViewDTO detailViewDTO: result.listBuyOrders) {
//				result.orderInfo.discount += detailViewDTO.orderDetailDTO.discountAmount;

			for(SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
				//KM don hang
				if(promoDetail.isOwner == 2) {
					boolean isExist = false;

					ArrayList<OrderDetailViewDTO> promotionListTemp = orderPromotionList;
					if (CalPromotions.ZV23.equals(promoDetail.programTypeCode)) {
						promotionListTemp = accumulationPromotionList;

					} else if (CalPromotions.ZV24.equals(promoDetail.programTypeCode)) {
						promotionListTemp = listPromotionNewOpenSaleOrder;
					}else if (CalPromotions.KS.equals(promoDetail.programTypeCode)) {
						promotionListTemp = orderPromotionKeyShop;
					}

					for (OrderDetailViewDTO orderPromotion: promotionListTemp) {
						if(promoDetail.programCode.equals(orderPromotion.orderDetailDTO.programeCode)
								&& promoDetail.productGroupId == orderPromotion.orderDetailDTO.productGroupId
								&& promoDetail.groupLevelId == orderPromotion.orderDetailDTO.groupLevelId
								) {
							orderPromotion.orderDetailDTO.addDiscountAmount(promoDetail.discountAmount);
							if (!CalPromotions.ZV23.equals(orderPromotion.orderDetailDTO.programeTypeCode)) {
								orderPromotion.orderDetailDTO.maxAmountFree = orderPromotion.orderDetailDTO.getDiscountAmount();
							}
							isExist = true;
							break;
						}
					}

					if(!isExist) {
						OrderDetailViewDTO promoDetailDTO = new OrderDetailViewDTO();
						promoDetailDTO.orderDetailDTO = new SaleOrderDetailDTO();
						// cap nhat tong so tien tra thuong keyshop bao gom so tien da tra va phai tra
						if(CalPromotions.KS.equals(promoDetail.programTypeCode))
							SQLUtils.getInstance().getMoneyRewardByCustomer(promoDetailDTO, promoDetail.programId, customerId);
//							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER;
						promoDetailDTO.orderDetailDTO.isFreeItem = 1;
						promoDetailDTO.orderDetailDTO.programeId = promoDetail.programId;
						promoDetailDTO.orderDetailDTO.programeCode = promoDetail.programCode;
						promoDetailDTO.orderDetailDTO.programeName = promoDetail.programName;
						promoDetailDTO.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
						promoDetailDTO.orderDetailDTO.programeTypeCode = promoDetail.programTypeCode;
						promoDetailDTO.orderDetailDTO.productGroupId = promoDetail.productGroupId;
						promoDetailDTO.orderDetailDTO.groupLevelId = promoDetail.groupLevelId;
						promoDetailDTO.orderDetailDTO.setDiscountAmount(promoDetail.discountAmount);
//							promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.discountAmount;
						promoDetailDTO.orderDetailDTO.discountPercentage = promoDetail.discountPercent;

						if(CalPromotions.ZV24.equals(promoDetailDTO.orderDetailDTO.programeTypeCode)){
							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY;
							promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.getDiscountAmount();
						}else if(CalPromotions.KS.equals(promoDetailDTO.orderDetailDTO.programeTypeCode)){// truong hop keyshop
							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY;
							promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.getDiscountAmount();
						} else if (!CalPromotions.ZV23.equals(promoDetailDTO.orderDetailDTO.programeTypeCode)) {
							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER;
							promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.getDiscountAmount();
//								orderPromotionList.add(promoDetailDTO);
						} else {
							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
							promoDetailDTO.orderDetailDTO.maxAmountFree = (long)promoDetail.maxAmountFree;

							//pay
							RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();
							promoDetailDTO.rptCttlPay = rptCttlPay;
//								accumulationPromotionList.add(promoDetailDTO);
						}

						promotionListTemp.add(promoDetailDTO);
					}
				}
			}
		}
		//KM don hang: tien, %
		result.listPromotionOrder.addAll(orderPromotionList);

		//KM tich luy
//			ArrayList<OrderDetailViewDTO> accumulationPromotionList = new ArrayList<OrderDetailViewDTO>();
//			for(SaleOrderPromoDetailDTO promoDetail : promoDetailList) {
//				if (CalPromotions.ZV23.equals(promoDetail.programTypeCode)) {
//					OrderDetailViewDTO promotion= new OrderDetailViewDTO();
//					promotion.orderDetailDTO = new SaleOrderDetailDTO();
//					promotion.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
//					promotion.orderDetailDTO.isFreeItem = 1;
//					promotion.orderDetailDTO.programeCode = promoDetail.programCode;
//					promotion.orderDetailDTO.programeName = promoDetail.programName;
//					promotion.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
//					promotion.orderDetailDTO.productGroupId = promoDetail.productGroupId;
//					promotion.orderDetailDTO.groupLevelId = promoDetail.groupLevelId;
//					promotion.orderDetailDTO.discountAmount = Math.round(promoDetail.discountAmount);
//					promotion.orderDetailDTO.maxAmountFree = Math.round(promoDetail.maxAmountFree);
//					promotion.orderDetailDTO.discountPercentage = promoDetail.discountPercent;
//
//					//promo detail
//					promotion.listPromoDetail.add(promoDetail);
//
//					//pay
//					RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();
//					promotion.rptCttlPay = rptCttlPay;
//
//					//Them sp KM vao ds sp tich luy
//					accumulationPromotionList.add(promotion);
//				}
//			}
		result.listPromotionAccumulation.addAll(accumulationPromotionList);

		List<OrderDetailViewDTO> listPromotionForPromo21 = new ArrayList<OrderDetailViewDTO>();
		List<OrderDetailViewDTO> listPromotionAccumulation = new ArrayList<OrderDetailViewDTO>();
		List<OrderDetailViewDTO> listPromotionNewOpen = new ArrayList<OrderDetailViewDTO>();
		List<OrderDetailViewDTO> listPromotionKeyShop = new ArrayList<OrderDetailViewDTO>();
		for (OrderDetailViewDTO detailViewDTO: result.listPromotionOrders) {
			//sp cua KM don hang (ZV21)
			if (CalPromotions.ZV21.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
				detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT;
				listPromotionForPromo21.add(detailViewDTO);
			}

			//Sp cua KM tich luy
			else if (CalPromotions.ZV23.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
				detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
				listPromotionAccumulation.add(detailViewDTO);
			}

			//Sp cua KM mo moi
			else if (CalPromotions.ZV24.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
				detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;
				listPromotionNewOpen.add(detailViewDTO);
			}
			//keyshop
			else if (CalPromotions.KS.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
				detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT;
				listPromotionKeyShop.add(detailViewDTO);
			}

			//Cap nhat promotion id cho sp tra thuong -> de co the tu tao so suat lai
			if(detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
					detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
					detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
					detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
					) {
				for (SaleOrderPromotionDTO quantityReceived : result.productQuantityReceivedList) {
					if(detailViewDTO.orderDetailDTO.programeCode.equals(quantityReceived.promotionProgramCode)) {
						detailViewDTO.orderDetailDTO.programeId = quantityReceived.promotionProgramId;

						break;
					}
				}
			}
		}

		//Gom nhom KM don hang: huong sp
		if (listPromotionForPromo21.size() > 0) {
			ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionForPromo21, OrderDetailViewDTO.PROMOTION_FOR_ZV21);

			for (OrderDetailViewDTO detailViewDTO : listPromotionForPromo21) {
				result.listPromotionOrders.remove(detailViewDTO);
			}

			result.listPromotionOrder.addAll(listPromotionZV21);
		}

		//Gom nhom KM tich luy: huong sp
		if (listPromotionAccumulation.size() > 0) {
			ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionAccumulation, OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT);

			for (OrderDetailViewDTO detailViewDTO : listPromotionAccumulation) {
				result.listPromotionOrders.remove(detailViewDTO);
			}

			result.listPromotionAccumulation.addAll(listPromotionZV21);
		}

		// gom nhom KM mo moi dang don hang
		if (listPromotionNewOpenSaleOrder.size() > 0) {
//				for (OrderDetailViewDTO detailViewDTO : listPromotionNewOpenSaleOrder) {
//					result.listPromotionOrders.remove(detailViewDTO);
//				}
			result.listPromotionNewOpen.addAll(listPromotionNewOpenSaleOrder);
		}

		//Gom nhom KM mo moi: huong sp
		if (listPromotionNewOpen.size() > 0) {
			ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionNewOpen, OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT);

			for (OrderDetailViewDTO detailViewDTO : listPromotionNewOpen) {
				result.listPromotionOrders.remove(detailViewDTO);
			}

			result.listPromotionNewOpen.addAll(listPromotionZV21);
		}

		// gom nhom sp keyshop
		result.listPromotionKeyShop.addAll(listPromotionKeyShop);
		// gom nhom tien km keyshop
		result.listPromotionKeyShop.addAll(orderPromotionKeyShop);
		for (OrderDetailViewDTO detailViewDTO : listPromotionKeyShop) {
			result.listPromotionOrders.remove(detailViewDTO);
		}


		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		model.setModelData(result);
		return model;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<OrderDetailViewDTO>
	 * @throws:
	 * @param listPromotionForPromo21
	 * @return
	 */
	public ArrayList<OrderDetailViewDTO> getListGroupedPromotion(List<OrderDetailViewDTO> listPromotionForPromo21, int promotionType) {
		ArrayList<OrderDetailViewDTO> listGroupedPromotion = new ArrayList<OrderDetailViewDTO>();

		for(OrderDetailViewDTO detail: listPromotionForPromo21) {
			boolean isExist = false;
			for(OrderDetailViewDTO groupDetail : listGroupedPromotion) {
				if (detail.orderDetailDTO.programeCode.equals(groupDetail.orderDetailDTO.programeCode)
						&& detail.orderDetailDTO.productGroupId == groupDetail.orderDetailDTO.productGroupId
						&& detail.orderDetailDTO.groupLevelId == groupDetail.orderDetailDTO.groupLevelId
						&& detail.orderDetailDTO.payingOrder == groupDetail.orderDetailDTO.payingOrder) {
					isExist = true;

					groupDetail.listPromotionForPromo21.add(detail);
				}
			}

			if(!isExist) {
				OrderDetailViewDTO orderDetailView = new OrderDetailViewDTO();
				SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
				orderDetailView.orderDetailDTO = orderDTO;
				orderDetailView.promotionType = promotionType;
				orderDetailView.orderDetailDTO.programeType = detail.orderDetailDTO.programeType;
				orderDetailView.orderDetailDTO.programeCode = detail.orderDetailDTO.programeCode;
				orderDetailView.orderDetailDTO.productGroupId = detail.orderDetailDTO.productGroupId;
				orderDetailView.orderDetailDTO.groupLevelId = detail.orderDetailDTO.groupLevelId;
				orderDetailView.orderDetailDTO.payingOrder = detail.orderDetailDTO.payingOrder;
				orderDetailView.orderDetailDTO.programeId = detail.orderDetailDTO.programeId;
				orderDetailView.listPromotionForPromo21.add(detail);
				listGroupedPromotion.add(orderDetailView);
			}
		}
		return listGroupedPromotion;
	}

	/**
	 * Lay danh sach feedback can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:47:59 21-05-2015
	 * @return: void
	 * @throws:
	 * @param page
	 * @param isGetTotalPage
	 */
	public ModelEvent getListCusFeedBack(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String type = bundle.getString(IntentConstants.INTENT_TYPE);
		String status = bundle.getString(IntentConstants.INTENT_STATE);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);

		String doneDate = bundle.getString(IntentConstants.INTENT_DONE_DATE);
		int isGetTotalPage = bundle.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);
		CustomerFeedBackDto dto = SQLUtils.getInstance().getFeedBackList(
				staffId, customerId, type, status, doneDate, page,
				isGetTotalPage, sortInfo, (BaseFragment) e.sender);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * load thong tin co ban cua khach hang
	 *
	 * @author : BangHN since : 8:25:08 AM
	 */
	public ModelEvent getCustomerBaseInfo(ActionEvent e) throws Exception {
		CustomerInfoDTO customerInfoDTO = new CustomerInfoDTO();
		Bundle b = (Bundle) e.viewData;
		String customerId = "";
		String numTop = "";
		String page = "";
		int sysCalUnApproved = 0;
		int sysCurrencyDivide = 1;
		if (b != null) {
			customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
			page = b.getString(IntentConstants.INTENT_PAGE);
			numTop = b.getString(IntentConstants.INTENT_NUMTOP);
			sysCalUnApproved = b.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
			sysCurrencyDivide = b.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE);
		}
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		// thong tin customer & customer type
		customerInfoDTO = SQLUtils.getInstance().getCustomerInfo(customerId,
				shopId, sysCalUnApproved, sysCurrencyDivide );
		if (customerInfoDTO == null || customerInfoDTO.getCustomer() == null) {
			// fixed bug truong hop customer ko co customerType
			customerInfoDTO = new CustomerInfoDTO();
			customerInfoDTO.setCustomer(SQLUtils.getInstance().getCustomerById(
					customerId));
			// lay so don hang trong thang
			int saleOrdersInMonth = SQLUtils.getInstance()
					.getNumberOrdersInMonth(customerId, shopId);
			customerInfoDTO.saleOrdersInMonth = saleOrdersInMonth;

			// so mat hang khac nhau ban trong ngay
			customerInfoDTO.sku = SQLUtils.getInstance()
					.getSKUOfCustomerInMonth(customerId, shopId, sysCalUnApproved);
			SQLUtils.getInstance().getAverageSalesInMonth(customerId, shopId, sysCalUnApproved, sysCurrencyDivide, customerInfoDTO);

		}

		// lay doanh so trong 3 thang gan fullDate
		ArrayList<SaleInMonthDTO> saleIn3Month = SQLUtils.getInstance()
				.getAverageSalesIn3MonthAgo(customerId, shopId, sysCalUnApproved, sysCurrencyDivide);
		Calendar cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		int size = saleIn3Month.size();

		int oneMonthAgo = (month - 0 + 12) % 12;
		if (oneMonthAgo == 0)
			oneMonthAgo = 12;
		int twoMonthAgo = (month - 1 + 12) % 12;
		if (twoMonthAgo == 0)
			twoMonthAgo = 12;
		int threeMonthAgo = (month - 2 + 12) % 12;
		if (threeMonthAgo == 0)
			threeMonthAgo = 12;

		for (int i = 0; i < size; i++) {
			if (saleIn3Month.get(i).month == threeMonthAgo) {
				customerInfoDTO.amountInThreeMonthAgo = saleIn3Month.get(i).amount;
			} else if (saleIn3Month.get(i).month == twoMonthAgo) {
				customerInfoDTO.amountInTwoMonthAgo = saleIn3Month.get(i).amount;
			} else if (saleIn3Month.get(i).month == oneMonthAgo) {
				customerInfoDTO.amountInOneMonthAgo = saleIn3Month.get(i).amount;
			}
		}
		//san luong
		for (int i = 0; i < size; i++) {
			if (saleIn3Month.get(i).month == threeMonthAgo) {
				customerInfoDTO.quantityInThreeMonthAgo = saleIn3Month.get(i).quantity;
			} else if (saleIn3Month.get(i).month == twoMonthAgo) {
				customerInfoDTO.quantityInTwoMonthAgo = saleIn3Month.get(i).quantity;
			} else if (saleIn3Month.get(i).month == oneMonthAgo) {
				customerInfoDTO.quantityInOneMonthAgo = saleIn3Month.get(i).quantity;
			}
		}

		// get nhung don hang gan fullDate
		customerInfoDTO.listOrderCustomer = SQLUtils.getInstance()
				.getLastSaleOrders(customerId, shopId, Integer.parseInt(page),
						Integer.parseInt(numTop), sysCurrencyDivide);

		// chuong trinh khach hang tham gia
//		ArrayList<CustomerProgrameDTO> listPro = SQLUtils.getInstance()
//				.getCustomerProgrames(customerId, shopId);
		ArrayList<CustomerProgrameDTO> listPro = new ArrayList<CustomerProgrameDTO>();
		customerInfoDTO.setListCustomerPrograme(listPro);
		customerInfoDTO.numInMonth = SQLUtils.getInstance().getNumCycle(0);
		customerInfoDTO.numInOneMonthAgo = SQLUtils.getInstance().getNumCycle(-1);
		customerInfoDTO.numInTwoMonthAgo = SQLUtils.getInstance().getNumCycle(-2);
		customerInfoDTO.numInThreeMonthAgo = SQLUtils.getInstance().getNumCycle(-3);
		return new ModelEvent(e, customerInfoDTO, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get general statistics report info (dayInOrder and month)
	 *
	 * @author: HaiTC3
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getGeneralStatisticsReportInfo(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		GeneralStatisticsInfoViewDTO dto = SQLUtils.getInstance().getGeneralStatisticsReportInfo(staffId,
				shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta chuc nang ham
	 *
	 * @author: BANGHN
	 * @param e
	 */
	public ModelEvent getCustomerSaleSKUInMonth(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().requestGetCustomerSaleSKUInMonth(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * get list note for general statistics view
	 *
	 * @author: HaiTC3
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListNoteForGeneralStatisticsView(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto =  SQLUtils.getInstance().getListNoteForGeneralStaticsView(staffId,
				shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * update note infor to database
	 *
	 * @author: HaiTC3
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetUpdateNoteStatusToDB(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		NoteInfoDTO noteUpdate = (NoteInfoDTO) data
				.getSerializable(IntentConstants.INTENT_NOTE_OBJECT);

		ListNoteInfoViewDTO noteListInfo = null;
		noteListInfo = SQLUtils.getInstance().updateNoteInfoToDB(staffId,
				shopId, noteUpdate);
		if (noteListInfo != null) {
			JSONObject sqlUpdate = noteUpdate.generateJsonUpdateFeedBack();
			JSONArray listSQL = new JSONArray();
			listSQL.put(sqlUpdate);
			genJsonSendToServer(e, listSQL);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}
		else{
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			model.setIsSendLog(false);
			ServerLogger.sendLog(
					"method requestGetUpdateNoteStatusToDB error: ",
					"save note to db local incorrect",
					TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(noteListInfo);
		return model;
	}

	/**
	 * Lay ds san pham khuyen mai tu san pham ban
	 *
	 * @author: TruongHN
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getPromotionProducts(ActionEvent actionEvent)
			throws Exception {
		// insert to sql Lite & request to server
		Bundle b = (Bundle) actionEvent.viewData;
		OrderViewDTO orderDTO = (OrderViewDTO) b.getSerializable(IntentConstants.INTENT_ORDER);
		Object dto =  SQLUtils.getInstance().calculatePromotionProducts2(orderDTO);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Xoa mot don hang
	 *
	 * @author: PhucNT
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent cancelSaleOrder(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		SaleOrderViewDTO dto = (SaleOrderViewDTO) e.viewData;
		boolean result = true;
		//don tam
		if (dto.saleOrder.approved == -1) {
			result = deleteDraftSaleOrder(String.valueOf(dto.saleOrder.saleOrderId));
		} else{
			//List<SaleOrderDetailDTO> listSaleOrderDetail = new ArrayList<SaleOrderDetailDTO>();
			//listSaleOrderDetail = SQLUtils.getInstance().getAllDetailOfSaleOrder(dto.saleOrder.saleOrderId);
	
			ActionLogDTO actionLogDTO = new ActionLogDTO();
			actionLogDTO.objectType = "4";
			// actionLogDTO.objectId = String.valueOf(dto.saleOrder.saleOrderId);
			// gp thay doi luu objectId: saleOrderId -> POCustomerId
			actionLogDTO.objectId = String.valueOf(dto.saleOrder.fromPOCustomerId);
			actionLogDTO.staffId = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId();
			actionLogDTO.shopId = Long.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritShopId());
	
			// Check order is last order of customer
	
			dto.lastOrder = SQLUtils.getInstance().getPreviousLastOrderDate(dto);
			Date currentDate = DateUtils.parseDateFromString(
					DateUtils.getCurrentDate(), DateUtils.defaultDateFormat);
			Date orderDate = DateUtils.parseDateFromString(
					dto.saleOrder.createDate, DateUtils.defaultDateFormat_2);
			// Khong cap nhat last order neu xoa don hang
			if (!StringUtil.isNullOrEmpty(dto.lastOrder)
					&& dto.lastOrder.equals(dto.saleOrder.createDate)
					|| (orderDate.compareTo(currentDate) < 0)) {
				dto.isFinalOrder = 0;
			} else {
				dto.isFinalOrder = 1;
			}
			// dto.isFinalOrder = orderTable.checkIsLastOrder(dto);
			result  = SQLUtils.getInstance().cancelSaleOrder(dto, actionLogDTO);
	
			if (result) {
				boolean allowSendToServer = true;
				if (dto.saleOrder != null
						&& (dto.saleOrder.orderType
								.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE) || dto.saleOrder.orderType
								.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE)
								&& dto.saleOrder.approved == 2)) {
					// khong gui yeu cau xoa don hang vansale/ tra vansale tu choi len server
					allowSendToServer = false;
				}
				
				if (dto.saleOrder.synState == 2 && allowSendToServer) {
					// order exist on server
					JSONArray listSql = SQLUtils.getInstance().generateSqlDeleteOrder(dto, actionLogDTO);
					// delete PO_CUSTOMER
	//				SQLUtils.getInstance().generateSqlDeletePOCustomer(dto, listSaleOrderDetail, actionLogDTO, listSql);
					genJsonSendToServer(e, listSql);
				} else if (allowSendToServer) {
					// send to server vi luon tao action log
					JSONArray listSql = new JSONArray();
					listSql.put(actionLogDTO.generateDeleteActionWhenDeleteOrder());
					genJsonSendToServer(e, listSql);
				}
			}
		}
		
		if (result) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_SUCCESS_DELETE_ORDER));
		} else {
			ServerLogger.sendLog("Xoa don hang " + dto.getSaleOrderId()
					+ " khong thanh cong.", TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * postNote
	 *
	 * @author: TamPQ
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent postNote(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		ToDoTaskDTO dto = (ToDoTaskDTO) e.viewData;
		dto.setType(AbstractTableDTO.TableType.TODO_TASK_TABLE);
		returnCode = SQLUtils.getInstance().postNote(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateTakeNoteSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

		}else{
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			ServerLogger.sendLog("Post Note khong thanh cong. Kiem tra lai du lieu.", TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(returnCode);
		return model;
	}

	/**
	 * postFeedBack
	 *
	 * @author: TamPQ
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent postFeedBack(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		Bundle bd = (Bundle) e.viewData;
		FeedBackDTO dto = (FeedBackDTO) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_DTO);
		dto.setType(AbstractTableDTO.TableType.FEEDBACK_TABLE);
		returnCode = SQLUtils.getInstance().postFeedBack(dto);

		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateFeedbackSql(dto.getRemindDate());
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			ServerLogger.sendLog("Luu feedback khong thanh cong. Kiem tra lai du lieu.", TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(returnCode);
		return model;
	}

	/**
	 * updateFeedBack
	 *
	 * @author: TamPQ
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent updateDoneDateFeedBack(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		Bundle b = (Bundle) e.viewData;
		FeedBackDTO dto = (FeedBackDTO) b.getSerializable(IntentConstants.INTENT_DTO);
		dto.setType(AbstractTableDTO.TableType.FEEDBACK_STAFF_TABLE);
		returnCode = SQLUtils.getInstance().updateDoneDateFeedBack(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateUpdateFeedbackSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			ServerLogger.sendLog("Update feedback khong thanh cong. Kiem tra lai du lieu.", TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(returnCode);
		return model;
	}


	/**
	 * Xoa feedback
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent deleteFeedBack(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		FeedBackDTO dto = (FeedBackDTO) e.viewData;
		dto.setType(AbstractTableDTO.TableType.FEEDBACK_TABLE);
		// returnCode = SQLUtils.getInstance().deleteFeedBack(dto);
		returnCode = SQLUtils.getInstance().updateDeleteFeedBack(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateDeleteFeedbackSql(dto);
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			ServerLogger.sendLog("delete feedback khong thanh cong. Kiem tra lai du lieu.",
					TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(returnCode);
		return new ModelEvent(e, returnCode, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	/**
	 * inset mot hanh dong xuong action_log (local & server db)
	 *
	 * @author : BangHN since : 1.0
	 */
	public ModelEvent inserActionLogToDB(ActionEvent e) throws Exception {
		e.isNeedCheckTimeServer = false;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		ActionLogDTO actionLog = (ActionLogDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
		actionLog.setType(AbstractTableDTO.TableType.ACTION_LOG);
		long insetLocal = SQLUtils.getInstance().insertActionLog(actionLog);
		if (insetLocal != -1) {// insert local thanh cong != -1
			JSONObject sqlPara = actionLog.generateActionLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("inserActionLogToDB - "
					+ actionLog.aCustomer.customerId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(insetLocal);
		return model;
	}

	/**
	 * delete mot hanh dong xuong action_log (local & server db)
	 *
	 * @author : BangHN since : 1.0
	 */
	public ModelEvent deleteActionLogToDB(ActionEvent e) throws Exception {
		e.isNeedCheckTimeServer = false;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		ActionLogDTO actionLog = (ActionLogDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
		actionLog.setType(AbstractTableDTO.TableType.ACTION_LOG);
		long insetLocal = SQLUtils.getInstance()
				.deleteActionLogWhenRemoveExceptionOrderDate(actionLog);
		if (insetLocal != -1) {// insert local thanh cong != -1
			JSONObject sqlPara = actionLog.generateDeleteActionLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			ServerLogger.sendLog("deleteActionLogToDB - "
					+ actionLog.aCustomer.customerId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * inser Visit ActionLog To DB
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent inserVisitActionLogToDB(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		ActionLogDTO actionLog = (ActionLogDTO)bundle.getSerializable(IntentConstants.INTENT_DATA);
		actionLog.setType(AbstractTableDTO.TableType.ACTION_LOG);
		boolean alreadyHaveVisitLog = SQLUtils.getInstance()
				.alreadyHaveVisitLog(actionLog);
		if (!alreadyHaveVisitLog) {
			MyLog.e("TamPQ", "vodddddddddddddddddddddddddddddd");
			model = inserActionLogToDB(e);
		} else {
			ServerLogger.sendLog("inserVisitActionLogToDB - "
					+ actionLog.aCustomer.customerId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}


	/**
	 * Lay thong KH ghe tham dat hang
	 * @author: dungnt19
	 * @since: 15:36:03 21-02-2014
	 * @return: void
	 * @throws:
	 * @param actionEvent
	 */
	public ModelEvent getCustomerInfoVisit(ActionEvent actionEvent)
			throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = bundle
				.getString(IntentConstants.INTENT_CUSTOMER_ID);
		Object dto =  SQLUtils.getInstance().getCustomerInfoVisit(staffId, shopId,
				customerId);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Cap nhat action log toi db
	 *
	 * @author:
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent updateActionLogToDB(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		ActionLogDTO actionLog = (ActionLogDTO) e.viewData;
		actionLog.setType(AbstractTableDTO.TableType.ACTION_LOG);
		// TamPQ: kiem tra neu co action_log ghe tham khac co end_time =
		// null thi ket thuc luon action_log do voi end_time = start_time +
		// 6 phut
		ArrayList<ActionLogDTO> listActLog = SQLUtils
				.getInstance()
				.getVisitActionLogWithEndTimeIsNull(
						GlobalInfo.getInstance().getProfile().getUserData().getInheritId(),
						actionLog.aCustomer.customerId,
						GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		if (listActLog.size() > 0) {
			for (int i = 0; i < listActLog.size(); i++) {
				if (listActLog.get(i).id != actionLog.id) {
					listActLog.get(i).endTime = DateUtils.plusTime(
							listActLog.get(i).startTime, 6);
				}
			}
			SQLUtils.getInstance().updateEndtimeListActionLogToDB(listActLog);
			JSONArray sqlArr = new ActionLogDTO()
					.generateUpdateEntimeListActionLogSql(listActLog);
			genJsonSendToServer(e, sqlArr);
		}

		long updateLocal = SQLUtils.getInstance()
				.updateActionLogToDB(actionLog);
		if (updateLocal > 0) {// insert local thanh cong
			JSONObject sqlPara = actionLog.generateUpdateActionLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(updateLocal);
		return model;
	}

	/**
	 * save nhung san pham ton kho
	 *
	 * @author: PhucNT
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent requestSaveRemainProduct(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		// insert to sql Lite & request to server
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle b = (Bundle)e.viewData;
		ArrayList<RemainProductViewDTO> listRemain = (ArrayList<RemainProductViewDTO>) b.getSerializable(IntentConstants.INTENT_DATA_LIST_PRODUCT_REMAIN);

		ArrayList<CustomerStockHistoryDTO> listCSTDTO = SQLUtils.getInstance()
				.saveRemainProduct(listRemain);

		// boolean insertSuccess = true;
		if (listCSTDTO != null) {
			// cap nhat lai id max cho bang table
			JSONArray sqlPara = generateSaveRemainProductSql(listCSTDTO);
			genJsonSendToServer(e, sqlPara);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			ServerLogger.sendLog(
					"Luu danh sach hang ton luu khong thanh cong",
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(listCSTDTO);
		return model;
	}

	private JSONArray generateSaveRemainProductSql(
			ArrayList<CustomerStockHistoryDTO> listOrder) {
		// TODO Auto-generated method stub

		JSONArray listSql = new JSONArray();
		CustomerStockHistoryDTO detail;
		for (int i = 0, size = listOrder.size(); i < size; i++) {
			detail = listOrder.get(i);
			// update sales order detail
			listSql.put(detail.generateUpdateRemainProductSql());
		}

		return listSql;
	}

	/**
	 * Lay ds muc do khan
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getCommonDataInOrder(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		// lay ds muc do khan
		ArrayList<ApParamDTO> listPara = SQLUtils.getInstance()
				.getListParaPriorityByType(
						bundle.getString(IntentConstants.INTENT_PRIORITY_CODE));

		ArrayList<Object> result = new ArrayList<Object>();
		result.add(listPara);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * get promotion detail
	 *
	 * @author: HaiTC3
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getPromotionDetail(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		Object dto =  SQLUtils.getInstance().getPromotionProgrameDetail(
				(Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Cap nhat lay vi tri KH
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent requestUpdateCustomerLocation(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		e.isNeedCheckTimeServer = true;
		Bundle bundle = (Bundle) e.viewData;
		CustomerDTO customerInfo = (CustomerDTO) bundle.getSerializable(IntentConstants.INTENT_CUSTOMER_ID);
		CustomerPositionLogDTO logPostion = (CustomerPositionLogDTO) bundle.getSerializable(IntentConstants.INTENT_CUSTOMER_POSITION_LOG);
		// cap nhat position moi trong customer_table
		long updateCustomer = SQLUtils.getInstance().updateCustomerLocation(
				customerInfo);
		// hien tai khong can insert record position log khi cap nhat vi tri
		// moi, cho dong bo ve
		if (updateCustomer > 0) {
			JSONObject sqlUpdateCustomerPosition = customerInfo
					.generateUpdateLocationSql();
			JSONObject sqlInsertPositionLog = logPostion
					.generateInsertCustomerPositionLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlUpdateCustomerPosition);
			jarr.put(sqlInsertPositionLog);
			genJsonSendToServer(e, jarr);
		}
		return new ModelEvent(e, updateCustomer, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * Danh sach hinh anh khach hang NVBH
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 * @date: Jan 8, 2013
	 */
	public ModelEvent getCusImageList(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ImageListDTO dto =  SQLUtils.getInstance().getImageList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * Danh sach hinh anh khach hang GSNPP
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 * @date: Jan 8, 2013
	 */
	public ModelEvent getCusImageListGSNPP(ActionEvent e) throws Exception {
		// @SuppressWarnings("rawtypes")
		Bundle data = (Bundle) e.viewData;
		ImageListDTO dto =  SQLUtils.getInstance().getImageList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * Request insert media item
	 *
	 * @author: PhucNT
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent requestInsertMediaItem(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		MediaItemDTO mediaDTO = (MediaItemDTO) e.viewData;
		long mediaId = SQLUtils.getInstance().insertMediaItem(mediaDTO);
		if (mediaId != -1) {
			JSONArray sqlPara = mediaDTO.generateInsertMediaItem();
			genJsonSendToServer(e, sqlPara);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("Insert mot hinh anh bi that bai "
					+ mediaDTO.id, TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(mediaId);
		return model;
	}

	/**
	 * Lay thong tin chi tiet cua san pham
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetProductInfoDetail(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		Object dto =  SQLUtils.getInstance().getProductInfoDetail((Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds order trong log table
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetOrderInLog(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Object dto =  SQLUtils.getInstance().getOrderNeedNotify();
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * request get customer sale list
	 * @author: hoanpd1
	 * @since: 11:09:47 05-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent requestGetCustomerSaleList(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		long staffId = bundle.getLong(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String code = bundle.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String nameAddress = bundle
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		String visitPlan = bundle.getString(IntentConstants.INTENT_VISIT_PLAN);
		String ownerId = bundle.getString(IntentConstants.INTENT_USER_ID);
		String listStaff = bundle.getString(IntentConstants.INTENT_LIST_STAFF);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);

		CustomerListDTO dto =  SQLUtils.getInstance().requestGetCustomerSaleList(ownerId,
				shopId, staffId, code, nameAddress, visitPlan, page, listStaff, sortInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * update ExceptionOrderDate ve null
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestUpdateExceptionOrderDate(ActionEvent e)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		Bundle b = (Bundle) e.viewData;
		StaffCustomerDTO dto = (StaffCustomerDTO) b.getSerializable(IntentConstants.INTENT_STAFF_DTO);
		dto.setType(AbstractTableDTO.TableType.VISIT_PLAN_TABLE);
		returnCode = SQLUtils.getInstance().updateExceptionOrderDate(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateUpdateExceptionOrderDate();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			ServerLogger.sendLog("Update exception_order_date ko thanh cong.",
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage("Update exception_order_date ko thanh cong.");
		}
		model.setModelData(returnCode);
		return model;

	}

	/**
	 * Lay danh sach cac loai van de can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:33:37 21-05-2015
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListTypeProblemNVBHGSNPP(ActionEvent e) throws Exception {
		Object dto = SQLUtils.getInstance().getListTypeProblemNVBHGSNPP();
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Request lay danh sach cong no khach hang
	 *
	 * @author: BangHN
	 * @param e
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ModelEvent requestGetCustomerDebt(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String cusNameAdd = data
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		Object dto =  SQLUtils.getInstance().requestGetCustomerDebt(cusCode,
				cusNameAdd);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Chi tiet cong no
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestDebitDetail(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		long debitId = bundle.getLong(IntentConstants.INTENT_DEBIT_ID);
		Object dto =  SQLUtils.getInstance().requestDebitDetail(debitId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**DANH SACH DON HANG TRA LAI
	 * @author cuonglt3
	 * @param e
	 */
	public ModelEvent requestDebitDetailReturn(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		Bundle bundle = (Bundle) e.viewData;
		long debitId = bundle.getLong(IntentConstants.INTENT_DEBIT_ID);
		CustomerDebitDetailDTO dto = SQLUtils.getInstance()
				.requestDebitDetailReturn(debitId);
		if (dto != null) {
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		return model;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestGetTypeFeedback(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		int from = bundle.getInt(IntentConstants.INTENT_FROM);
		Object dto =  SQLUtils.getInstance().requestGetTypeFeedback(from);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @param listSql
	 * @return
	 * @return: HTTPRequestvoid
	 * @throws:
	 */
	private HTTPRequest sendPayDebtSqlToServer(ActionEvent actionEvent,
			JSONArray listSql) {
		HTTPRequest re;

		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_LOG_TYPE);
		para.add(DEBIT_TABLE.TABLE_NAME);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());

		re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent);
		return re;
	}

	/**
	 * Thanh toan ngay luc tao don hang
	 *
	 * @author: ThangNV31
	 * @param actionEvent
	 * @param listSql
	 * @return
	 * @return: HTTPRequestvoid
	 * @throws:
	 */
	private HTTPRequest paymentAtCreateOrderTime(ActionEvent actionEvent, JSONArray listSql) {
		HTTPRequest re;

		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());

		re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent);
		return re;
	}

	/**
	 * Lay KH chua psds
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestGetCusNoPSDS(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		int rptSaleHisId = b.getInt(IntentConstants.INTENT_ID);
		boolean isGetTotalPage = b
				.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		Object dto =  SQLUtils.getInstance().requestGetCusNoPSDS(rptSaleHisId,
				isGetTotalPage, page);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * lay danh sach hinh anh chuong trinh cua khach hang
	 *
	 * @author ThanhNN
	 * @param e
	 */
	public ModelEvent gotoListAlbumPrograme(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getAlbumProgrameInfo(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getListSelectCustomer
	 *
	 * @author: ToanTT
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getListSelectCustomer(ActionEvent actionEvent)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		Bundle bundle = (Bundle) actionEvent.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		double lat = bundle.getDouble(IntentConstants.INTENT_POSITION_LAT);
		double lng = bundle.getDouble(IntentConstants.INTENT_POSITION_LONG);
		int roleType = bundle.getInt(IntentConstants.INTENT_ROLE_TYPE);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto =  SQLUtils.getInstance().getSelectCustomerList(shopId, staffId,
				lat, lng, roleType);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * insert hanh dong xem video vao media_log (local & server db)
	 *
	 * @author : ToanTT
	 */
	public ModelEvent insertMediaLog(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		MediaLogDTO mediaLog = (MediaLogDTO) e.viewData;
		mediaLog.setType(AbstractTableDTO.TableType.MEDIA_LOG_TABLE);
		long insetLocal = SQLUtils.getInstance().insertMediaLog(mediaLog);
		if (insetLocal != -1) {// insert local thanh cong != -1
			JSONObject sqlPara = mediaLog.generateMediaLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("inserActionLogToDB - " + mediaLog.customerId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(insetLocal);
		return model;

	}

	/**
	 * update endtime xem video vao media_log (local & server db)
	 *
	 * @author : ToanTT
	 */
	public ModelEvent updateEndtimeMediaLog(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		MediaLogDTO mediaLog = (MediaLogDTO) e.viewData;
		mediaLog.setType(AbstractTableDTO.TableType.MEDIA_LOG_TABLE);
		long updateLocal = SQLUtils.getInstance().updateEndtimeMediaLog(
				mediaLog);
		if (updateLocal != -1) {// update local thanh cong != -1
			JSONObject sqlPara = mediaLog.generateUpdateEndtimeMediaLogSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("inserActionLogToDB - " + mediaLog.customerId,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(updateLocal);
		return model;
	}

	/**
	 * Lay ds hinh anh cua chuong trinh
	 *
	 * @author: thanhnn
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public ModelEvent getAlbumDetailPrograme(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String type = String.valueOf(data
				.getInt(IntentConstants.INTENT_ALBUM_TYPE));
		String numTop = String.valueOf(data
				.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE));
		String page = String.valueOf(data.getInt(IntentConstants.INTENT_PAGE));
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String programeCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		PhotoThumbnailListDto result = null;
		result = new PhotoThumbnailListDto();
		ArrayList<PhotoDTO> listPhoto = SQLUtils.getInstance()
				.getAlbumDetailPrograme(customerId, type, numTop, page,
						programeCode, fromDate, toDate, shopId);
		result.getAlbumInfo().getListPhoto().addAll(listPhoto);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay bao cao chi tiet doanh so ngay cua KH
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getCatAmountReport(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getCatAmountDetailReport(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	/**
	 *
	 * Lay danh sach van de
	 *
	 * @author quangvt
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListProblemsFeedBack(ActionEvent e) throws Exception{
		Bundle data = (Bundle) e.viewData;
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		long cusId = data.getLong(IntentConstants.INTENT_CUSTOMER_ID);
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		Object dto =  SQLUtils.getInstance().getListProblemsFeedback(staffId, cusId,
				page);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListDocument(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		Bundle bundle = (Bundle) event.viewData;
		OfficeDocumentListDTO result = new OfficeDocumentListDTO();
		int type = bundle.getInt(IntentConstants.INTENT_DOCUMENT_TYPE);
		String fromDate = bundle
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = bundle
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String ext = bundle.getString(IntentConstants.INTENT_PAGE);
		boolean checkLoadMore = bundle.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		boolean isGetType = bundle.getBoolean(
				IntentConstants.INTENT_IS_GET_LIST_TYPE, false);
		result = SQLUtils.getInstance().getListDocument(type, fromDate, toDate,
				ext, checkLoadMore, shopId);
		if (isGetType) {
			result.listType = SQLUtils.getInstance().getListDocumentType();
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * chi tiet cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListDocumentDetail(ActionEvent event)
			throws Exception {
		// TODO Auto-generated method stub
		Bundle bundle = (Bundle) event.viewData;
		long officeDocumentId = bundle
				.getLong(IntentConstants.INTENT_OFFICE_DOCUMENT_ID);
		Object dto =  SQLUtils.getInstance().getListDocumentDetail(officeDocumentId);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getListCustomer
	 *
	 * @author: DungNX
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getListCustomerVisited(ActionEvent actionEvent)
			throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		int isGetTotalPage = bundle
				.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto =  SQLUtils.getInstance().getCustomerListVisited(shopId,staffId, page,
				isGetTotalPage);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin & chi tiet don hang goc
	 *
	 * @author: DungNX3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getPO(ActionEvent event) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle v = (Bundle) event.viewData;
		long poCustomerId;

		OrderViewDTO result = new OrderViewDTO();

		try {

			if (v.getString(IntentConstants.INTENT_ORDER) != null) {
				poCustomerId = Long.parseLong(v
						.getString(IntentConstants.INTENT_ORDER));
			} else {
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				return model;
			}
			long customerId = v.getLong(IntentConstants.INTENT_CUSTOMER_ID);
			String shopId = v.getString(IntentConstants.INTENT_SHOP_ID);
			String staffId = v.getString(IntentConstants.INTENT_STAFF_ID);

			// get ds from sqllite
			ListSaleOrderDTO listChosenProduct = SQLUtils.getInstance().getPO(poCustomerId,customerId,shopId,staffId);
			result.orderInfo = listChosenProduct.saleOrderDTO;
			result.listBuyOrders = listChosenProduct.listData;

			// Get ds promotion from sqlite
			ListSaleOrderDTO listPromotionProduct = SQLUtils.getInstance()
					.getPromotionProductsPO(poCustomerId,
							result.orderInfo.orderType,customerId,shopId,staffId);
			result.listPromotionOrders = listPromotionProduct.listData;

			//get quantity received
			ArrayList<SaleOrderPromotionDTO> quantityReceivedList = SQLUtils.getInstance().getPOQuantityReceived(poCustomerId);
			result.productQuantityReceivedList = quantityReceivedList;

			//get sale order promo detail cua KM tich luy
//			ArrayList<SaleOrderPromoDetailDTO> promoDetailList = SQLUtils.getInstance().getPoCustomerPromoDetailAccumulaction(poCustomerId);

			//update discount amount on product
			ArrayList<OrderDetailViewDTO> orderPromotionList = new ArrayList<OrderDetailViewDTO>();
			ArrayList<OrderDetailViewDTO> accumulationPromotionList = new ArrayList<OrderDetailViewDTO>();
			ArrayList<OrderDetailViewDTO> orderPromotionKeyShop = new ArrayList<OrderDetailViewDTO>();
			for (OrderDetailViewDTO detailViewDTO: result.listBuyOrders) {
//				result.orderInfo.discount += detailViewDTO.orderDetailDTO.discountAmount;

				for(SaleOrderPromoDetailDTO promoDetail : detailViewDTO.listPromoDetail) {
					//KM don hang
					if(promoDetail.isOwner == 2) {
						boolean isExist = false;

						ArrayList<OrderDetailViewDTO> promotionListTemp = orderPromotionList;
						if (CalPromotions.ZV23.equals(promoDetail.programTypeCode)) {
							promotionListTemp = accumulationPromotionList;

						}else if (CalPromotions.KS.equals(promoDetail.programTypeCode)) {
							promotionListTemp = orderPromotionKeyShop;
						}

						for (OrderDetailViewDTO orderPromotion: promotionListTemp) {
							if(promoDetail.programCode.equals(orderPromotion.orderDetailDTO.programeCode)
									&& promoDetail.productGroupId == orderPromotion.orderDetailDTO.productGroupId
									&& promoDetail.groupLevelId == orderPromotion.orderDetailDTO.groupLevelId
									) {
								orderPromotion.orderDetailDTO.addDiscountAmount(promoDetail.discountAmount);
								if (!CalPromotions.ZV23.equals(orderPromotion.orderDetailDTO.programeTypeCode)) {
									orderPromotion.orderDetailDTO.maxAmountFree = orderPromotion.orderDetailDTO.getDiscountAmount();
								}
								isExist = true;
								break;
							}
						}

						if(!isExist) {
							OrderDetailViewDTO promoDetailDTO = new OrderDetailViewDTO();
							promoDetailDTO.orderDetailDTO = new SaleOrderDetailDTO();
//							promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER;
							// cap nhat tong so tien tra thuong keyshop bao gom so tien da tra va phai tra
							if(CalPromotions.KS.equals(promoDetail.programTypeCode))
								SQLUtils.getInstance().getMoneyRewardByCustomer(promoDetailDTO, promoDetail.programId, customerId);
							promoDetailDTO.orderDetailDTO.isFreeItem = 1;
							promoDetailDTO.orderDetailDTO.programeId = promoDetail.programId;
							promoDetailDTO.orderDetailDTO.programeCode = promoDetail.programCode;
							promoDetailDTO.orderDetailDTO.programeName = promoDetail.programName;
							promoDetailDTO.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
							promoDetailDTO.orderDetailDTO.programeTypeCode = promoDetail.programTypeCode;
							promoDetailDTO.orderDetailDTO.productGroupId = promoDetail.productGroupId;
							promoDetailDTO.orderDetailDTO.groupLevelId = promoDetail.groupLevelId;
							promoDetailDTO.orderDetailDTO.setDiscountAmount(promoDetail.discountAmount);
//							promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.discountAmount;
							promoDetailDTO.orderDetailDTO.discountPercentage = promoDetail.discountPercent;

							if(CalPromotions.KS.equals(promoDetailDTO.orderDetailDTO.programeTypeCode)){// truong hop keyshop
								promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY;
								promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.getDiscountAmount();
							}else if (!CalPromotions.ZV23.equals(promoDetailDTO.orderDetailDTO.programeTypeCode)) {
								promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER;
								promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetailDTO.orderDetailDTO.getDiscountAmount();
//								orderPromotionList.add(promoDetailDTO);
							} else {
								promoDetailDTO.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
								promoDetailDTO.orderDetailDTO.maxAmountFree = promoDetail.maxAmountFree;

								//pay
								RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();
								promoDetailDTO.rptCttlPay = rptCttlPay;
//								accumulationPromotionList.add(promoDetailDTO);
							}

							promotionListTemp.add(promoDetailDTO);
						}
					}
				}
			}

			//KM don hang: tien, %
			result.listPromotionOrder.addAll(orderPromotionList);

			//KM tich luy
//			ArrayList<OrderDetailViewDTO> accumulationPromotionList = new ArrayList<OrderDetailViewDTO>();
//			for(SaleOrderPromoDetailDTO promoDetail : promoDetailList) {
//				if (CalPromotions.ZV23.equals(promoDetail.programTypeCode)) {
//					OrderDetailViewDTO promotion= new OrderDetailViewDTO();
//					promotion.orderDetailDTO = new SaleOrderDetailDTO();
//					promotion.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY;
//					promotion.orderDetailDTO.isFreeItem = 1;
//					promotion.orderDetailDTO.programeCode = promoDetail.programCode;
//					promotion.orderDetailDTO.programeName = promoDetail.programName;
//					promotion.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
//					promotion.orderDetailDTO.productGroupId = promoDetail.productGroupId;
//					promotion.orderDetailDTO.groupLevelId = promoDetail.groupLevelId;
//					promotion.orderDetailDTO.discountAmount = Math.round(promoDetail.discountAmount);
//					promotion.orderDetailDTO.maxAmountFree = Math.round(promoDetail.maxAmountFree);
//					promotion.orderDetailDTO.discountPercentage = promoDetail.discountPercent;
//
//					//promo detail
//					promotion.listPromoDetail.add(promoDetail);
//
//					//pay
//					RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();
//					promotion.rptCttlPay = rptCttlPay;
//
//					//Them sp KM vao ds sp tich luy
//					accumulationPromotionList.add(promotion);
//				}
//			}
			result.listPromotionAccumulation.addAll(accumulationPromotionList);

			List<OrderDetailViewDTO> listPromotionForPromo21 = new ArrayList<OrderDetailViewDTO>();
			List<OrderDetailViewDTO> listPromotionAccumulation = new ArrayList<OrderDetailViewDTO>();
			List<OrderDetailViewDTO> listPromotionNewOpen = new ArrayList<OrderDetailViewDTO>();
			List<OrderDetailViewDTO> listPromotionKeyShop = new ArrayList<OrderDetailViewDTO>();
			for (OrderDetailViewDTO detailViewDTO: result.listPromotionOrders) {
				//sp cua KM don hang (ZV21)
				if (CalPromotions.ZV21.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
					detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT;
					listPromotionForPromo21.add(detailViewDTO);
				}

				//Sp cua KM tich luy
				else if (CalPromotions.ZV23.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
					detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;
					listPromotionAccumulation.add(detailViewDTO);
				}

				//Sp cua KM mo moi
				else if (CalPromotions.ZV24.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
					detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT;
					listPromotionNewOpen.add(detailViewDTO);
				}//keyshop
				else if (CalPromotions.KS.equals(detailViewDTO.orderDetailDTO.programeTypeCode)) {
					detailViewDTO.promotionType = OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT;
					listPromotionKeyShop.add(detailViewDTO);
				}

				//Cap nhat promotion id cho sp tra thuong -> de co the tu tao so suat lai
				if(detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
						detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
						detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
						detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
						) {
					for (SaleOrderPromotionDTO quantityReceived : result.productQuantityReceivedList) {
						if(detailViewDTO.orderDetailDTO.programeCode.equals(quantityReceived.promotionProgramCode)) {
							detailViewDTO.orderDetailDTO.programeId = quantityReceived.promotionProgramId;

							break;
						}
					}
				}
			}

			//Gom nhom KM don hang: huong sp
			if (listPromotionForPromo21.size() > 0) {
				ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionForPromo21, OrderDetailViewDTO.PROMOTION_FOR_ZV21);

				for (OrderDetailViewDTO detailViewDTO : listPromotionForPromo21) {
					result.listPromotionOrders.remove(detailViewDTO);
				}

				result.listPromotionOrder.addAll(listPromotionZV21);
			}

			//Gom nhom KM tich luy: huong sp
			if (listPromotionAccumulation.size() > 0) {
				ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionAccumulation, OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT);

				for (OrderDetailViewDTO detailViewDTO : listPromotionAccumulation) {
					result.listPromotionOrders.remove(detailViewDTO);
				}

				result.listPromotionAccumulation.addAll(listPromotionZV21);
			}

			//Gom nhom KM mo moi: huong sp
			if (listPromotionNewOpen.size() > 0) {
				ArrayList<OrderDetailViewDTO> listPromotionZV21 = getListGroupedPromotion(listPromotionNewOpen, OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT);

				for (OrderDetailViewDTO detailViewDTO : listPromotionNewOpen) {
					result.listPromotionOrders.remove(detailViewDTO);
				}

				result.listPromotionNewOpen.addAll(listPromotionZV21);
			}
			// gom nhom sp keyshop
			result.listPromotionKeyShop.addAll(listPromotionKeyShop);
			// gom nhom tien km keyshop
			result.listPromotionKeyShop.addAll(orderPromotionKeyShop);
			for (OrderDetailViewDTO detailViewDTO : listPromotionKeyShop) {
				result.listPromotionOrders.remove(detailViewDTO);
			}
		} catch (Exception ex) {
			result = null;
		}
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		model.setModelData(result);
		return model;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param actionEvent
	 * @return: void
	 * @throws Exception
	 * @throws:
	*/
	public ModelEvent createReturnOrder(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		Bundle b = (Bundle) actionEvent.viewData;
		SaleOrderViewDTO item = (SaleOrderViewDTO) b.getSerializable(IntentConstants.INTENT_ORDER);

		// doi tuong tra ve tao json
		OrderViewDTO orderDTO = new OrderViewDTO();
		boolean success = SQLUtils.getInstance().createReturnOrder(item,
				orderDTO);
		if (success) {
			// neu success thi tao json gui len server
			JSONArray jarr = orderDTO.generateReturnOrderSql();
			// cap nhat don goc
			jarr.put(orderDTO.orderTemp.generateUpdateSentOrderSql());

			sendReturnOrderToServer(actionEvent, jarr,
					orderDTO.orderInfo.saleOrderId);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	private HTTPRequest sendReturnOrderToServer(ActionEvent actionEvent,
			JSONArray listSql, long orderId) {
		HTTPRequest re;

		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_LOG_TYPE);
		para.add(SALE_ORDER_TABLE.TABLE_NAME);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());
		re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent,
				LogDTO.TYPE_ORDER, String.valueOf(orderId),
				SALE_ORDER_TABLE.TABLE_NAME);
		return re;
	}

	/**
	 * Lay trang thai kho
	 * @author: ThangNV31
	 * @param actionEvent
	 * @return: void
	 * @throws:
	*/
	public ModelEvent getStockState(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		LockDateDTO stockLock = SQLUtils.getInstance().getLockDate(
				(Bundle) actionEvent.viewData);
//		String stockTransDate = SQLUtils.getInstance().getStockTransDate(
//				(Bundle) actionEvent.viewData);
		// tam thoi ko quan tam toi xuat kho
		stockLock.hasDP = true;
//		try {
//			// catch cho truong hop compare dayInOrder bi sai
////			if (DateUtils.compareDate(DateUtils.now(), stockTransDate) == 0) {
////				stockLock.hasDP = true;
////			}
//			// co xuat kho
//			if (!StringUtil.isNullOrEmpty(stockTransDate)) {
//				stockLock.hasDP = true;
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//			stockLock.hasDP = false;
//		}

		GlobalInfo.getInstance().setLockDateValsale(stockLock);
		return new ModelEvent(actionEvent, stockLock, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * cap nhat trang thai kho
	 * @author: ThangNV31
	 * @param actionEvent
	 * @return: void
	 * @throws:
	*/
	public ModelEvent insertStockState(ActionEvent actionEvent)
			throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		actionEvent.type = ActionEvent.TYPE_OFFLINE;
		LockDateDTO dto = (LockDateDTO) data
				.getSerializable(IntentConstants.INTENT_STOCK_DTO);
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		long returnCode = SQLUtils.getInstance().insertStockState(dto);
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateInsertLockDateVansale();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(actionEvent, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param e
	 * @return: void
	 * @throws:
	*/
	public ModelEvent getCustomerDebit(ActionEvent e) throws Exception {
		Object dto = SQLUtils.getInstance().getCustomerDebit(e);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 * lay thong tin shop
	 * @author: DungNX
	 * @param actionEvent
	 * @return: void
	 * @throws:
	*/
	public ModelEvent getShopInfo(ActionEvent actionEvent) throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getShopInfo(shopId);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**tao phieu chi
	 * @author cuonglt3
	 * @param e
	 */
	public ModelEvent saveAndSendPaymentDetail(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		CustomerDebitDetailDTO dto = (CustomerDebitDetailDTO) e.viewData;
		boolean isInsertSuccess = SQLUtils.getInstance().saveAndSendPaymentDetail(dto);
		if (isInsertSuccess) {
			JSONArray listSql = dto.generatePayDebtSqlReturn();
			sendPayDebtSqlToServer(e, listSql);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("DEBIT ID: " + dto.debitDTO.id,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage("tao phieu chi khong thanh cong");
		}
		return model;
	}

	/**xoa thanh toan bi tu choi
	 * @author cuonglt3
	 * @param e
	 */
	public ModelEvent deletePaymentDetail(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle b = (Bundle) e.viewData;
		CustomerDebitDetailItem item = (CustomerDebitDetailItem) b
				.getSerializable(IntentConstants.CUSTOMER_DEBIT_DETAIL_ITEM);
		CustomerDebitDetailDTO dto = (CustomerDebitDetailDTO) b
				.getSerializable(IntentConstants.CUSTOMER_DEBIT_DETAIL_DTO);
		boolean isUpdateSuccess = SQLUtils.getInstance().deletePayment(dto,
				item);
		if (isUpdateSuccess) {
			JSONArray listSql = new JSONArray();
			listSql.put(item.currentPaymentDetailDTO.upDateStatusDelete(item));
			updatePaymentDetailStatus(e, listSql);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage("xoa phieu thanh toan khong thanh cong");
		}
		return model;
	}

	/**gui json update status cua payment_detail_temp
	 * @param actionEvent
	 * @param listSql
	 * @return
	 */
	private HTTPRequest updatePaymentDetailStatus(ActionEvent actionEvent,
			JSONArray listSql) {
		HTTPRequest re;

		String logId = GlobalUtil.generateLogId();
		actionEvent.logData = logId;
		// send to server
		Vector<Object> para = new Vector<Object>();
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(listSql);
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(listSql.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());

		re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent);
		return re;
	}

	/**lay danh sach phieu thanh toan bi tu choi
	 * @param e
	 * @author cuonglt3
	 */
	public ModelEvent requestPaymentRefused(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		long debitId = bundle.getLong(IntentConstants.INTENT_DEBIT_ID);
		CustomerDebitDetailDTO dto = SQLUtils.getInstance()
				.requestPaymentRefused(debitId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/** thanh toan cong no
	 * @author: DungNX
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	*/
	public ModelEvent paymentVansale(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		// insert to sql Lite & request to server
		OrderViewDTO orderDTO = (OrderViewDTO) e.viewData;
		boolean result = SQLUtils.getInstance().createPaymentVansale(orderDTO);
		if (result) {
			JSONArray jarr = new JSONArray();

			if (orderDTO.debitIdExist <= 0) {
				jarr.put(orderDTO.debitDto.generateInsertFromOrderSql());
			}
			jarr.put(orderDTO.debitDetailDto
					.generateInsertOrUpdateDebitDetail());
			jarr.put(orderDTO.payReceivedDTO.generateInsertForPayDebt());
			jarr.put(orderDTO.paymentDetailDto.generateInsertForPayDebt());
			paymentAtCreateOrderTime(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * kiem tra xem duoc phep thuc hien chot ngay hay chua
	 *
	 * @author: ThangNV31
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent checkDBLogForDateLock(ActionEvent actionEvent) throws Exception{
		boolean isSuccess = false;
		isSuccess = SQLUtils.getInstance().checkDBLogForDateLock();
		return new ModelEvent(actionEvent, isSuccess,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

//	/**
//	 * send request chot ngay, thuc hien online
//	 *
//	 * @author: ThangNV31
//	 * @param actionEvent
//	 * @return: void
//	 * @throws:
//	 */
//	public void sendRequestDateLockToServer(ActionEvent actionEvent) {
//		ModelEvent model = new ModelEvent();
//		model.setActionEvent(actionEvent);
//		boolean isSuccess = true;
//		try {
//			Vector<String> vt = new Vector<String>();
//			vt.add("updateType");
//			vt.add("UPDATE_LAST_DATE_LOCK_DATE");
//			if (isSuccess) {
//				sendHttpRequest("synDataController/updateStaffSynDataStatus", vt, actionEvent);
//			} else {
//				throw new Exception("Chot ngay khong thanh cong");
//			}
//		} catch (Exception ex) {
//			model.setIsSendLog(true);
//			model.setModelCode(ErrorConstants.ERROR_COMMON);
//			model.setModelMessage(ex.getMessage());
//			SaleController.getInstance().handleErrorModelEvent(model);
//		}
//	}


	/**
	 * lay type don hang tam
	 * @author: DungNX
	 * @param e
	 * @return: void
	 * @throws:
	*/
	public ModelEvent requestGetOrderDraftType(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		String saleOrderId = bundle
				.getString(IntentConstants.INTENT_SALE_ORDER_ID);
		String notifyDTO = SQLUtils.getInstance()
				.getOrderDraftType(saleOrderId);
		return new ModelEvent(e, notifyDTO, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	/**
	 * Lay danh sach cac dot kiem ke thiet bi
	 * @author: hoanpd1
	 * @since: 11:15:32 03-12-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public ModelEvent getListPeriodInventory(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ListEquipInventoryRecordsDTO dto = SQLUtils.getInstance()
				.getListPeriodInventory(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay danh sach thiet bi can kiem ke trong dot
	 * @author: hoanpd1
	 * @since: 11:15:46 03-12-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public ModelEvent getListInventoryDevice(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		EquipStatisticRecordDTO dto = SQLUtils.getInstance()
				.getListInventoryDevice(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Cap nhat tr
	 * @author: hoanpd1
	 * @since: 11:58:55 17-12-2014
	 * @return: HTTPRequest
	 * @throws:
	 * @param e
	 * @return
	 */
	public ModelEvent requestUpdateInventoryDevice(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		Bundle bundle = (Bundle) e.viewData;
		EquipStatisticRecordDTO listPeriod = (EquipStatisticRecordDTO) bundle.getSerializable(IntentConstants.INTENT_LIST_INVENTORY_DEVICE);
		try {
			long success = SQLUtils.getInstance().insertInventoryDevice(bundle);

			if (success > 0) {
				JSONArray jarr = new JSONArray();
				for (EquipInventoryDTO productCompetitor : listPeriod.listEquipInventory) {
						JSONObject sqlPara = productCompetitor.generateInsertEquipLostMobileRec();
						jarr.put(sqlPara);
				}
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			} else {
				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.MESSAGE_NO_CUSTOMER_INFO));
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
		}

		return model;
	}

	/**
	 * Luu hinh anh cua kiem ke thiet bi
	 * @author: hoanpd1
	 * @since: 20:16:03 19-12-2014
	 * @return: HTTPRequest
	 * @throws:
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent requestInsertMediaInventory(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		// insert to sql Lite & request to server
		EquipAttachFileDTO equipAttachFileDTO = (EquipAttachFileDTO) e.userData;

		long equipAttachFileId = SQLUtils.getInstance().insertMediaInventory(
				equipAttachFileDTO);
		if (equipAttachFileId != -1) {
			String logId = GlobalUtil.generateLogId();
			e.logData = logId;
			// send to server
			Vector<String> data = (Vector<String>) e.viewData;
			data.add(IntentConstants.INTENT_EQUIP_ATTACH_FILE_ID);
			data.add(Long.toString(equipAttachFileId));
			data.add(IntentConstants.INTENT_LOG_ID);
			data.add(logId);
			String imgPath = data.get(
					data.lastIndexOf(IntentConstants.INTENT_TAKEN_PHOTO) + 1)
					.toString();

			if (StringUtil.isNullOrEmpty(imgPath)) {
			} else {
				httpMultiPartRequestInventory(data, imgPath, e);
			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("Insert mot hinh anh bi that bai "
					+ equipAttachFileDTO.idEquipAttachFile,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * lay danh sach KPI dong
	 *
	 * @author: dungdq3
	 * @since: 10:12:18 AM Oct 22, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 */
	private ModelEvent getListDynamicKPI(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		ListDynamicKPIDTO listDynamic = SQLUtils.getInstance().getListDynamicKPI(b);
		return new ModelEvent(e, listDynamic, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay cac tham so bao cao
	 *
	 * @author: dungdq3
	 * @since: 9:11:52 AM Oct 27, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 */
	private ModelEvent getParameters(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		ChooseParametersDTO report = SQLUtils.getInstance().getParameters(b);
		return new ModelEvent(e, report, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * get so ngay lam viec
	 *
	 * @author: dungdq3
	 * @since: 5:03:45 PM Oct 22, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 * @throws Exception
	 */
	private ModelEvent getWorkingDay(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		ChooseParametersBeforeExportReportDTO data = SQLUtils.getInstance().getWorkingDay(b);
		return new ModelEvent(e, data, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay du lieu chon cot trong man hinh bao cao dong
	 * @author: Tuanlt11
	 * @return
	 * @return: Object
	 * @throws:
	*/
	private ModelEvent getDataReportChooseRow(ActionEvent e) throws Exception {
		Object dto = SQLUtils.getInstance().getDataReportChooseRow((Bundle)e.viewData);

		// co the xu li truong hop customer list lay ve ko co du lieu hoa
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay report
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @return: Object
	 * @throws:
	*/
	private ModelEvent getDynamicReport(ActionEvent e) {
		// TODO Auto-generated method stub
		Bundle b = (Bundle) e.viewData;
		Object report = SQLUtils.getInstance().getDynamicReport(b);
		return new ModelEvent(e, report, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach Nganh hang cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:39:11 PM Oct 30, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 */
	private ModelEvent getCategoryForReport(ActionEvent e) {
		// TODO Auto-generated method stub
		Bundle b = (Bundle) e.viewData;
		ListReportColumnCatDTO report = SQLUtils.getInstance().getCategoryForReport(b);
		return new ModelEvent(e, report, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay danh sach san pham cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:00:56 PM Oct 28, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 */
	private ModelEvent getProductListForReport(ActionEvent e) {
		Bundle b = (Bundle) e.viewData;
		ListReportColumnProductDTO report = SQLUtils.getInstance().getProductListForReport(b);
		return new ModelEvent(e, report, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * read shop lock
	 *
	 * @author: dungdq3
	 * @since: 10:03:13 AM Oct 14, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 * @throws Exception
	 */
	private Object readShopLock(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ShopLockDTO shopLock = SQLUtils.getInstance().getShopLock(data);
		return new ModelEvent(e, shopLock, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach khach hang them moi
	 *
	 * @author: dungdq3
	 * @since: 10:21:37 AM Sep 16, 2014
	 * @return: void
	 * @param:
	 * @throws:
	 */
	public ModelEvent getListNewCustomer(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;

		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID, "");
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID, "");
		String customerName = data.getString(
				IntentConstants.INTENT_CUSTOMER_NAME, "");
		int numItemInPage = data.getInt(
				IntentConstants.INTENT_NUM_ITEM_IN_PAGE,
				Constants.NUM_ITEM_PER_PAGE);
		int currentIndexSpinnerState = data.getInt(
				IntentConstants.INTENT_STATE, 0);
		int currentPage = data.getInt(IntentConstants.INTENT_PAGE, -1);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		Object dto = SQLUtils.getInstance().getListNewCustomer(staffId, shopId,
				customerName, numItemInPage, currentIndexSpinnerState,
				currentPage, sortInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Xoa KH
	 *
	 * @author: dungdq3
	 * @since: 1:43:05 PM Sep 22, 2014
	 * @return: void
	 * @throws:
	 * @param e:
	 */
	public ModelEvent deleteCustomerInfo(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long returnCode = -1;
		Bundle bd = (Bundle) e.viewData;
		long customerId = bd.getLong(IntentConstants.INTENT_CUSTOMER_ID);
		CustomerDTO dto = SQLUtils.getInstance()
				.getCustomerByIdForCreateCustomer(String.valueOf(customerId));
		dto.setType(AbstractTableDTO.TableType.CUSTOMER);
		returnCode = SQLUtils.getInstance().deleteCustomer(dto);

		// send to server
		if (returnCode > 0) {
			JSONObject sqlPara = dto.generateDeleteCustomerSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			ServerLogger.sendLog("deleteCustomerInfo - " + dto.customerId,
					TabletActionLogDTO.LOG_CLIENT);
		}
		return model;
	}

	/**
	 * lay thong tin kh tao moi
	 *
	 * @author: dungdq3
	 * @since: 3:59:23 PM Jan 26, 2015
	 * @return: ModelEvent
	 * @param e
	 * @throws Exception
	 */
	public ModelEvent getCreateCustomerInfo(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;

		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID, "");
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID, "");
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID,
				"");
		Object dto = SQLUtils.getInstance().getCreateCustomerInfo(staffId, shopId,
				customerId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * insert new kh
	 *
	 * @author: dungdq3
	 * @since: 5:47:36 PM Jan 26, 2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent insertCustomer(ActionEvent e) throws Exception {
		long returnCode = -1;
		ModelEvent model = new ModelEvent(); model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		CustomerDTO dto = (CustomerDTO) bd
				.getSerializable(IntentConstants.INTENT_CUSTOMER);
		dto.setType(AbstractTableDTO.TableType.CUSTOMER);
		returnCode = SQLUtils.getInstance().insertCustomer(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateCreateCustomerSql();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			for(CustomerAttributeDetailViewDTO detail : dto.listCusAttrDetail){
				jarr.put(detail.generateJSON());
			}
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			ServerLogger.sendLog("insertCustomer - " + dto.customerId,
					TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(returnCode);
		return model;
	}

	/**
	 * lay danh sach xa
	 *
	 * @author: dungdq3
	 * @since: 9:43:56 AM Jan 27, 2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getCreateCustomerAreaInfo(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		int typeArea = data.getInt(IntentConstants.INTENT_AREA_TYPE);
		long parentAreaId = data
				.getLong(IntentConstants.INTENT_PARRENT_AREA_ID);
		Object dto = SQLUtils.getInstance().getCreateCustomerAreaInfo(parentAreaId,
				typeArea);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * cap nhat kh
	 *
	 * @author: dungdq3
	 * @since: 11:20:05 AM Jan 27, 2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent updateCustomerInfo(ActionEvent e) throws Exception {
		long returnCode = -1;
		Bundle bd = (Bundle) e.viewData;
		CustomerDTO dto = (CustomerDTO) bd
				.getSerializable(IntentConstants.INTENT_CUSTOMER);
		dto.setType(AbstractTableDTO.TableType.CUSTOMER);
		returnCode = SQLUtils.getInstance().updateCustomer(dto);
		// send to server
		if (returnCode > 0) {
			JSONObject sqlPara = dto.generateUpdateCustomerSql();
			JSONObject deleteDetailPara = dto.generateDeleteDetail();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			jarr.put(deleteDetailPara);
			for(CustomerAttributeDetailViewDTO detail : dto.listCusAttrDetail){
				jarr.put(detail.generateJSON());
			}
			genJsonSendToServer(e, jarr);
			return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
		} else {
			return new ModelEvent(e, dto, ErrorConstants.ERROR_COMMON, "");
		}

	}

	/**
	 * lay thuoc tinh dong cua kh
	 *
	 * @author: dungdq3
	 * @since: 4:18:13 PM Jan 27, 2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private Object getExtraCustomerInfo(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;

		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID, "");
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID, "");
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID,
				"");
		Object dto = SQLUtils.getInstance().getExtraCustomerInfo(staffId, shopId,
				customerId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	/**
	 * Lay bao cao theo nganh hang cho role NVBH
	 * @author: hoanpd1
	 * @since: 17:23:35 07-04-2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private Object getReportCategory(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportCategory(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay thong tin khach hang qua id
	 * @author: hoanpd1
	 * @since: 14:54:21 11-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getCustomerById(ActionEvent actionEvent)
			throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		Object dto = SQLUtils.getInstance().getCustomerById(customerId);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get forcus product info
	 *
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public ModelEvent requestGetForcusProductInfo(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		NVBHReportForcusProductInfoViewDTO result = null;
		result = SQLUtils.getInstance().getForcusProductInfoView(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Danh sach nganh hang con
	 * @author: yennth16
	 * @since: 10:41:33 19-06-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param event
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getListSubcat(ActionEvent event)
			throws Exception {
		Bundle data = (Bundle) event.viewData;
		String productInfoCode = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		ComboboxDisplayProgrameDTO result = new ComboboxDisplayProgrameDTO();
		result.listSubcat = SQLUtils.getInstance().getListSubcat(productInfoCode);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds thiet bi chup hinh
	 * @author: DungNT19
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getListEquipmentTakePhoto(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		TakePhotoEquipmentViewDTO result = SQLUtils.getInstance().getListEquipmentTakePhoto(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds hinh anh cua key shop
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getListImageOfEquipment(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		TakePhotoEquipmentImageViewDTO result = SQLUtils.getInstance().getListImageOfEquipment(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * validate so luong hinh anh cua keyshop
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent validateListImageOfEquipment(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		ArrayList<TakePhotoEquipmentDTO> result = SQLUtils.getInstance().validateListImageOfEquipment(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds key shop
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getListKeyShop(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		VoteKeyShopViewDTO result = SQLUtils.getInstance().getListKeyShop(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay ds hinh anh cua key shop
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getListImageOfKeyShop(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		VoteKeyShopImageViewDTO result = SQLUtils.getInstance().getListImageOfKeyShop(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * validate so luong hinh anh cua keyshop
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent validateListImageOfKeyShop(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		ArrayList<KeyShopDTO> result = SQLUtils.getInstance().validateListImageOfKeyShop(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds tat ca keyshop phan bo cho shop
	 *
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getAllListKeyShop(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		RegisterKeyShopViewDTO result = SQLUtils.getInstance().getAllListKeyShop(data);
		return new ModelEvent(actionEvent, result,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay cac muc cua key shop
	 *
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getLevelOfKeyShop(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		RegisterKeyShopViewDTO result = SQLUtils.getInstance().getLevelOfKeyShop(data);
		return new ModelEvent(actionEvent, result,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	 /**
	 * Update muc dang ki cua khach hang
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent updateRegisterKeyShop(ActionEvent e) throws Exception {
		long returnCode = 1;
		Bundle bd = (Bundle) e.viewData;
		returnCode = SQLUtils.getInstance().updateRegisterKeyShop(bd);
		// send to server
		if (returnCode > 0) {
			@SuppressWarnings("unchecked")
			ArrayList<RegisterKeyShopItemDTO> lstItemClone = (ArrayList<RegisterKeyShopItemDTO>)bd.getSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP);
			JSONArray jarr = new JSONArray();
			JSONObject sqlPara = lstItemClone.get(0).ksCustomerItem.generateSql();
			jarr.put(sqlPara);
			for(int i = 0, size = lstItemClone.size(); i < size; i++){
				RegisterKeyShopItemDTO temp = lstItemClone.get(i);
				jarr.put(temp.ksCustomerLevelItem.generateSql());
			}
			genJsonSendToServer(e, jarr);
			return new ModelEvent(e, returnCode, ErrorConstants.ERROR_CODE_SUCCESS, "");
		} else {
			return new ModelEvent(e, returnCode, ErrorConstants.ERROR_COMMON, "");
		}

	}

	 /**
	 * Lay thong tin CTKM
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getPromotionDetailKeyShop(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		Object dto =  SQLUtils.getInstance().getPromotionProgrameDetailKeyShop(
				(Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	 /**
	 * Lay bao cao kpi cua nhan vien
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getReportKPISale(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportKPISale(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * check keyshop cua kh
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent checkKeyShopCustomer(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().checkKeyShopCustomer(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	public ModelEvent getReportCustomerSale(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportCustomerSale(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay ds bao cao cthttm cua khach hang
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getReportListCustomer(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportListCustomer(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
}