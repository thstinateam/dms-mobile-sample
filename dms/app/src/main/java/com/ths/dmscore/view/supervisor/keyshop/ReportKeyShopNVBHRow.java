/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dms.R;

/**
 * ReportKeyShopNVBHRow.java
 * @author: yennth16
 * @version: 1.0
 * @since:  10:56:28 20-07-2015
 */
public class ReportKeyShopNVBHRow extends DMSTableRow implements OnClickListener {

	// stt
	TextView tvSTT;
	// cau lac bo
	TextView tvKeyshop;
	// khach hang
	TextView tvCustomerCode;
	TextView tvCustomerName;
	// dia chi
	TextView tvAddress;
	// muc dang ky
	TextView tvLevelRegister;
	// ds dang ky
	TextView tvAmountRegister;
	// ds thuc hien
	TextView tvAmountDone;
	// ket qua
	TextView tvResult;
	// sl dang ky
	TextView tvQuantityRegister;
	// sl thuc hien
	TextView tvQuantityDone;
	// tra thuong
	TextView tvAward;
	ReportKeyshopStaffItemDTO dto;

	// khoi tao control
	public ReportKeyShopNVBHRow(Context context) {
		super(context, R.layout.layout_report_key_shop_staff_row,1.5, GlobalInfo.getInstance()
				.isSysShowPrice() ? null : new int[] { R.id.tvAmountRegister, R.id.tvAmountDone });
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvSTT.setOnClickListener(this);
		tvKeyshop = (TextView) findViewById(R.id.tvKeyshop);
		tvKeyshop.setOnClickListener(this);
		tvCustomerCode = (TextView) findViewById(R.id.tvCustomerCode);
		tvCustomerCode.setOnClickListener(this);
		tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
		tvCustomerName.setOnClickListener(this);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
		tvAddress.setOnClickListener(this);
		tvLevelRegister = (TextView) findViewById(R.id.tvLevelRegister);
		tvLevelRegister.setOnClickListener(this);
		tvResult = (TextView) findViewById(R.id.tvResult);
		tvResult.setOnClickListener(this);
		tvAward = (TextView) findViewById(R.id.tvAward);
		tvAward.setOnClickListener(this);

		tvAmountRegister = (TextView) findViewById(R.id.tvAmountRegister);
		tvAmountDone = (TextView) findViewById(R.id.tvAmountDone);
		if(GlobalInfo.getInstance().isSysShowPrice()){
			tvAmountRegister.setOnClickListener(this);
			tvAmountDone.setOnClickListener(this);
		}
		tvQuantityRegister = (TextView) findViewById(R.id.tvQuantityRegister);
		tvQuantityRegister.setOnClickListener(this);
		tvQuantityDone = (TextView) findViewById(R.id.tvQuantityDone);
		tvQuantityDone.setOnClickListener(this);
	}

	// render layout
	public void renderLayout(int position, ReportKeyshopStaffItemDTO item) {
		this.dto = item;
		tvSTT.setText(Constants.STR_BLANK + position);
		tvKeyshop.setText(item.keyshop.ksCode);
		tvCustomerCode.setText(item.customerCode);
		tvCustomerName.setText(item.customerName);
		tvAddress.setText(item.address);
		tvLevelRegister.setText(item.levelRegister);
		if(item.result == ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_ATTAIN){
			tvResult.setText(StringUtil.getString(R.string.TEXT_NOT_ATTAIN));
		}else if(item.result == ReportKeyshopStaffItemDTO.TYPE_RESULT_ATTAIN){
			tvResult.setText(StringUtil.getString(R.string.ATTAIN));
		}else{
			tvResult.setText(Constants.STR_BLANK);
		}
		if(item.id>0){
			if(item.award == ReportKeyshopStaffItemDTO.TYPE_REWARD_TRANSFER){
				tvAward.setText(StringUtil.getString(R.string.TEXT_TRANSFER));
			}else if(item.award == ReportKeyshopStaffItemDTO.TYPE_RESULT_LOCK ){
				tvAward.setText(StringUtil.getString(R.string.TEXT_LOCK));
			}else if(item.award== ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_PAID){
				tvAward.setText(StringUtil.getString(R.string.TEXT_NOT_PAID));
			}else if(item.award== ReportKeyshopStaffItemDTO.TYPE_RESULT_PAID_PART){
				tvAward.setText(StringUtil.getString(R.string.TEXT_PAID_PART));
			}else if(item.award== ReportKeyshopStaffItemDTO.TYPE_RESULT_PAID){
				tvAward.setText(StringUtil.getString(R.string.TEXT_PAID));
			}else{
				tvAward.setText(StringUtil.getString(R.string.TEXT_KEY_SHOP_NOT_AWARD));
			}
		}else{
			tvAward.setText(Constants.STR_BLANK);
		}
		if(GlobalInfo.getInstance().isSysShowPrice()){
			display(tvAmountRegister, item.amountRegister);
			display(tvAmountDone, item.amountDone);
		}
		display(tvQuantityRegister, item.quantityRegister);
		display(tvQuantityDone, item.quantityDone);
	}

	@Override
	public void onClick(View arg0) {
		if (arg0 == tvKeyshop && listener != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH, arg0, dto);
		}else if((arg0 == tvCustomerCode || arg0 == tvCustomerName || arg0 == tvAddress || arg0 == tvLevelRegister
				|| arg0 == tvResult || arg0 == tvAward
				|| arg0 == tvAmountRegister || arg0 == tvAmountDone
				|| arg0 == tvQuantityRegister || arg0 == tvQuantityDone
				|| arg0 == tvSTT) && listener != null){
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_REPORT_KEY_SHOP_PAID, arg0, dto);
		}
	}
}
