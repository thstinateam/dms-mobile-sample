/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.PRODUCT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * thong tin san pham mat hang trong tam cua nhan vien ban hang
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class ForcusProductOfNVBHDTO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	// id of product
	public long productId;
	// ma mat hang
	public String productCode;
	// ten mat hang
	public String productName;
	// nganh hang
	public String productIndustry;
	// nganh hang
	public String productIndustryCode;
	// loai mat hang trong tam
	public String typeProductFocus;
	// doanh so ke hoach
	public double amountPlan;
	// doanh so thuc hien
	public double amount;
	public double amountApproved;
	public double amountPending;
	// doanh so con lai
	public double amountRemain;
	// tien do
	public double amountProgress;
	// san luong ke hoach
	public long quantityPlan;
	// san luong thuc hien
	public long quantity;
	public long quantityApproved;
	public long quantityPending;
	// san luong con lai
	public long quantityRemain;
	// tien do
	public double quantityProgress;

	/**
	 * init object
	 */
	public ForcusProductOfNVBHDTO() {
		productCode = Constants.STR_BLANK;
		productName = Constants.STR_BLANK;
		productIndustry = Constants.STR_BLANK;
		productIndustryCode = Constants.STR_BLANK;
		typeProductFocus = Constants.STR_BLANK;
		amountPlan = 0;
		amount = 0;
		amountRemain = 0;
		amountProgress = 0;
		productId = 0;
	}

	/**
	 *
	 * khoi tao doi tuong
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void initDateWithCursor(Cursor c) {
		// id san pham
		productId = CursorUtil.getLong(c, PRODUCT_TABLE.PRODUCT_ID);
		// code san pham
		productCode = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_CODE);
		// ten san pham
		productName = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_NAME);
		// nganh hang
		productIndustry = CursorUtil.getString(c, "PRODUCT_INFO_NAME");
		// nganh hang
		productIndustryCode = CursorUtil.getString(c, "PRODUCT_INFO_CODE");
		// loai mat hang trong tam
		typeProductFocus = CursorUtil.getString(c, "TYPE_PRODUCT_FOCUS");
		// doanh so ke hoach
		amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PLAN");
		// doanh so thuc hien
		amount = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT");
		amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_APPROVED");
		amountPending = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PENDING");
		// doanh so con lai
		amountRemain = StringUtil.calRemainUsingRound(amountPlan, amount);
		// tien do
		amountProgress = StringUtil.calPercentUsingRound(amountPlan, amount);
		// doanh so ke hoach
		quantityPlan = CursorUtil.getLong(c, "QUANTITY_PLAN");
		// doanh so thuc hien
		quantity = CursorUtil.getLong(c, "QUANTITY");
		quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
		quantityPending = CursorUtil.getLong(c, "QUANTITY_PENDING");
		// doanh so con lai
		quantityRemain = StringUtil.calRemain(quantityPlan, quantity);
		// tien do
		quantityProgress = StringUtil.calPercentUsingRound(quantityPlan, quantity);
	}
}
