/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.MediaLogDTO;

/**
 * Luu thong tin action chon kh khi xem video san pham
 *
 * @author: ToanTT
 * @version: 1.0
 * @since: 1.0
 */
public class MEDIA_LOG_TABLE extends ABSTRACT_TABLE {
	// id media log
	public static final String MEDIA_LOG_ID = "MEDIA_LOG_ID";
	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// id media item
	public static final String MEDIA_ID = "MEDIA_ID";
	// id product
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// thoi gian bat dau hanh dong
	public static final String START_TIME = "START_TIME";
	// thoi gian ket thuc mot hanh dong
	public static final String END_TIME = "END_TIME";
	// trong, ngoai tuyen
	public static final String IS_OR = "IS_OR";
	// chua, dang, da ghe tham
	public static final String VISIT_STATUS = "VISIT_STATUS";
	// toa do lat
	public static final String LAT = "LAT";
	// toa do lng
	public static final String LNG = "LNG";
	// k/c tu nhan vien den kh
	public static final String DISTANCE = "DISTANCE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// table name
	public static final String TABLE_NAME = "MEDIA_LOG";

	public MEDIA_LOG_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { MEDIA_LOG_ID, STAFF_ID, CUSTOMER_ID, MEDIA_ID, PRODUCT_ID, START_TIME, END_TIME,
				IS_OR, VISIT_STATUS, LAT, LNG, DISTANCE, CREATE_USER, CREATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: ToanTT
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((MediaLogDTO) dto);
		return insert(null, value);
	}

	/**
	 * init dong data media log
	 *
	 * @author : ToanTT
	 */
	private ContentValues initDataRow(MediaLogDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(MEDIA_LOG_ID, dto.id);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(MEDIA_ID, dto.mediaId);
		editedValues.put(PRODUCT_ID, dto.productId);
		editedValues.put(START_TIME, dto.startTime);
		editedValues.put(IS_OR, dto.isVisitPlan);
		editedValues.put(VISIT_STATUS, dto.visitStatus);
		editedValues.put(LAT, dto.lat);
		editedValues.put(LNG, dto.lng);
		editedValues.put(DISTANCE, dto.distance);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		return editedValues;
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: ToanTT
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(MEDIA_LOG_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		MediaLogDTO mediaLogDTO = (MediaLogDTO) dto;
		String[] params = { "" + mediaLogDTO.id };
		return delete(MEDIA_LOG_ID + " = ?", params);
	}

	/**
	 * Cap nhat media log
	 *
	 * @author: ToanTT
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public int update(MediaLogDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.id };
		return update(value, MEDIA_LOG_ID + " = ?", params);
	}

	public long update(AbstractTableDTO dto) {
		MediaLogDTO mediaLogDTO = (MediaLogDTO) dto;
		ContentValues value = initDataRow(mediaLogDTO);
		String[] params = { "" + mediaLogDTO.id };
		return update(value, MEDIA_LOG_ID + " = ?", params);
	}

	/**
	 * ToanTT
	 *
	 * @param dto
	 * @return
	 */
	public long updateEndtime(MediaLogDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(END_TIME, dto.endTime);
		String[] params = { "" + dto.id };
		return update(editedValues, MEDIA_LOG_ID + " = ?", params);
	}

}
