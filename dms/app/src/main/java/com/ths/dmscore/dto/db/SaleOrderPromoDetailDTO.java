package com.ths.dmscore.dto.db;

import java.math.BigDecimal;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_DETAIL_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_PROMO_DETAIL_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class SaleOrderPromoDetailDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1329010725605050699L;
	// id
	public long saleOrderPromoDetailId;
	public long saleOrderDetailId;
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public int programType;
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public String programCode;
	public String programName;
	// % chiet khau
	public double discountPercent;
	// So tien chiet khau
	public double discountAmount;
	public double maxAmountFree;
	//public double maxQuantityFreeCK;
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public int isOwner;
	public long staffId;
	public long saleOrderId;
	//su dung cho po
	public long poPromoDetailId;
	public long poDetailId;
	public long poId;
	public String programTypeCode;
	public String shopId;
	public String orderDate;
	//Hien thi so suat
	public long productGroupId;
	public long groupLevelId;
	public long programId;// id cua CTKM

	public SaleOrderPromoDetailDTO() {
		super(TableType.SALE_ORDER_PROMO_DETAIL_TABLE);
	}

	/**
	 * gen json promotion detail
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonSaleOrderPromoDetail() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_PROMO_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
			if(this.isOwner >=0)
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.IS_OWNER, this.isOwner, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.MAX_AMOUNT_FREE, this.maxAmountFree, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_CODE, this.programCode, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_TYPE, this.programType, null));
			if (!StringUtil.isNullOrEmpty(this.programTypeCode)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAME_TYPE_CODE, this.programTypeCode, null));
			}
			if(saleOrderDetailId > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_DETAIL_ID, this.saleOrderDetailId, null));
			}

			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_PROMO_DETAIL_ID, this.saleOrderPromoDetailId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_ID, this.saleOrderId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.PRODUCT_GROUP_ID, this.productGroupId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.GROUP_LEVEL_ID, this.groupLevelId, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * gen json po promotion detail
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonPOCustomerPromoDetail() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_PROMO_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
			if(isOwner >=0)
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.IS_OWNER, this.isOwner, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.MAX_AMOUNT_FREE, this.maxAmountFree, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PROGRAM_CODE, this.programCode, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PROGRAM_TYPE, this.programType, null));
			if (!StringUtil.isNullOrEmpty(this.programTypeCode)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PROGRAME_TYPE_CODE, this.programTypeCode, null));
			}
			if(poDetailId > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PO_CUSTOMER_DETAIL_ID, this.poDetailId, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PO_CUSTOMER_PROMO_DETAIL_ID, this.poPromoDetailId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PO_CUSTOMER_ID, this.poId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.PRODUCT_GROUP_ID, this.productGroupId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.GROUP_LEVEL_ID, this.groupLevelId, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	public JSONObject generateDeleteByPOSql(long poId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_PROMO_DETAIL_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_DETAIL_TABLE.PO_CUSTOMER_ID, String.valueOf(poId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	public JSONObject generateDeleteByOrderSql(long saleOrderId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_PROMO_DETAIL_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_ID, String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * Cap nhat thong tin vao sale order promotion detail
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param promotion
	 * @param discountAmount
	 * @param promoDetail
	 */
	public void updateData(OrderDetailViewDTO promotion, double discountAmount) {
		this.discountAmount = discountAmount;
		discountPercent = promotion.orderDetailDTO.discountPercentage;
		//% = 0 khi amount = 0
//		if(discountAmount == 0) {
		if(BigDecimal.valueOf(discountAmount).compareTo(BigDecimal.ZERO) == 0) {
			discountPercent = 0;
		}
		//neu so luong san pham quy dinh == 1 -> la LINE, isOwner = 1, con lai la isOwner = 2
//		isOwner = (promotion.numBuyProductInGroup == 1) ? 1 : 2;
		maxAmountFree = promotion.orderDetailDTO.maxAmountFree;
		programCode = promotion.orderDetailDTO.programeCode;
		programName = promotion.orderDetailDTO.programeName;
		productGroupId = promotion.orderDetailDTO.productGroupId;
		groupLevelId = promotion.orderDetailDTO.groupLevelId;
		programType = promotion.orderDetailDTO.programeType;
		programTypeCode = promotion.orderDetailDTO.programeTypeCode;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param c
	 */
	public void parseDataFromCursor(Cursor c) {
		discountPercent = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_PERCENT);
		discountAmount = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_AMOUNT);
		//neu so luong san pham quy dinh == 1 -> la LINE, isOwner = 1, con lai la isOwner = 2
		isOwner = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.IS_OWNER);
		maxAmountFree = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.MAX_AMOUNT_FREE);
		programCode = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_CODE);
		programName = programCode;
		//KM tu dong
		programType = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_TYPE);
		programTypeCode = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAME_TYPE_CODE);
		//Hien thi so suat
		productGroupId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.PRODUCT_GROUP_ID);
		groupLevelId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.GROUP_LEVEL_ID);
		programId = CursorUtil.getLong(c, "PROGRAM_ID");
	}

	/**
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		saleOrderPromoDetailId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_PROMO_DETAIL_ID, -1);
		saleOrderDetailId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_DETAIL_ID, -1);
		programType = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_TYPE, -1);
		programCode = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAM_CODE);
		discountPercent = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_PERCENT);
		discountAmount = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.DISCOUNT_AMOUNT);
		maxAmountFree = CursorUtil.getDouble(c, SALE_ORDER_PROMO_DETAIL_TABLE.MAX_AMOUNT_FREE);
		isOwner = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.IS_OWNER);
		staffId = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.STAFF_ID, -1);
		saleOrderId = CursorUtil.getInt(c, SALE_ORDER_PROMO_DETAIL_TABLE.SALE_ORDER_ID, -1);
		programTypeCode = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.PROGRAME_TYPE_CODE);
		shopId = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.SHOP_ID);
		orderDate = CursorUtil.getString(c, SALE_ORDER_PROMO_DETAIL_TABLE.ORDER_DATE);
		productGroupId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.PRODUCT_GROUP_ID, -1);
		groupLevelId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.GROUP_LEVEL_ID);
		productGroupId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.PRODUCT_GROUP_ID);
		groupLevelId = CursorUtil.getLong(c, SALE_ORDER_PROMO_DETAIL_TABLE.GROUP_LEVEL_ID);
	}

	 /**
	 * Cap nhat promotion detail cho sp ko dat KM
	 * @author: Tuanlt11
	 * @param promotion
	 * @param discountAmount
	 * @return: void
	 * @throws:
	*/
	public void updateData(PromotionProgrameDTO promotion) {
		this.discountAmount = 0;
		discountPercent = 0;
		//neu so luong san pham quy dinh == 1 -> la LINE, isOwner = 1, con lai la isOwner = 2
//		isOwner = (promotion.numBuyProductInGroup == 1) ? 1 : 2;
		maxAmountFree = 0;
		programCode = promotion.getPROMOTION_PROGRAM_CODE();
		programName = promotion.getPROMOTION_PROGRAM_NAME();
		productGroupId = 0;
		groupLevelId = 0;
		programType = 0;
		programTypeCode = promotion.getTYPE();
	}

}
