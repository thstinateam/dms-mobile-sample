/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.FEEDBACK_STAFF_CUSTOMER_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.GlobalUtil;

/**
 * FeedbackStaffCustomerDTO.java
 * @author: yennth16
 * @version: 1.0
 * @since:  10:06:46 21-05-2015
 */
public class FeedbackStaffCustomerDTO extends AbstractTableDTO {
	// noi dung field
	private static final long serialVersionUID = 1L;
	// id bang
	public long feedBackStaffCustomerId;
	// id feedback
	public long feedBackStaffId;
	// if staff
	public long staffId;
	// id khach hang
	public long customerId;
	// nguoi tao
	public String createUserId;
	// ngay tao
	public String createDate;
	// nguoi cap nhat
	public String updateUser;
	// ngay cap nhat
	public String updateDate;

	/**
	 * insert feeback staff
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateFeedbackStaffCustomerSql() {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME,
					FEEDBACK_STAFF_CUSTOMER_TABLE.FEEDBACK_STAFF_CUSTOMER_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.FEEDBACK_STAFF_CUSTOMER_ID, feedBackStaffCustomerId,
					null));
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.FEEDBACK_STAFF_ID, feedBackStaffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.CREATE_USER_ID, createUserId, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					FEEDBACK_STAFF_CUSTOMER_TABLE.CREATE_DATE, createDate, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return feedbackJson;
	}
}
