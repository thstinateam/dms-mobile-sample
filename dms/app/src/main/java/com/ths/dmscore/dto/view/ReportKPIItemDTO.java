package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * DTO cho tung row cho man hinh bao cao KPI
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPIItemDTO {
	public String criteria; // chi tieu
	public double valueDayPlan; // san luong / doanh so ngay ke hoach
	public double valueDayDone; //san luong / doanh so ngay thuc hien
	public double progressDay; // tien do ngay
	public double valueCyclePlan; // san luong / doanh so chu ky ke hoach
	public double valueCycleDone; // san luong / doanh so chu ky thuc hien
	public double progressCycle; // tien do chu ki
	// 1: so nguyen, 2 so thuc
	public static final int TYPE_INT = 1; // so nguyen
	public static final int TYPE_FLOAT = 2; // so thuc.
	public int dataType = TYPE_INT; // co cham phay hay ko cham phay.
	public long kpiId; // kpi id


	 /**
	 * parse du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void parseDataFromCursor(Cursor c) {
		criteria = CursorUtil.getString(c, "CRITERIA");
		dataType = CursorUtil.getInt(c, "DATA_TYPE");
		valueDayPlan = CursorUtil.getDouble(c, "VALUE_DAY_PLAN",1, GlobalInfo.getInstance().getSysNumRounding());
		valueDayDone = CursorUtil.getDouble(c, "VALUE_DAY_DONE",1,GlobalInfo.getInstance().getSysNumRounding());
		progressDay = StringUtil.calPercentUsingRound(valueDayPlan, valueDayDone);
		valueCyclePlan = CursorUtil.getDouble(c, "VALUE_CYCLE_PLAN",1,GlobalInfo.getInstance().getSysNumRounding());
		valueCycleDone = CursorUtil.getDouble(c, "VALUE_CYCLE_DONE",1,GlobalInfo.getInstance().getSysNumRounding());
		progressCycle = StringUtil.calPercentUsingRound(valueCyclePlan, valueCycleDone);
		kpiId = CursorUtil.getLong(c, "KPI_ID",1,GlobalInfo.getInstance().getSysNumRounding());

	}
}
