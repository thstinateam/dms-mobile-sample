package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.EQUIPMENT_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cho table thiet bi chup hinh
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class TakePhotoEquipmentDTO extends AbstractTableDTO {
	// noi dung field
	private static final long serialVersionUID = 1L;
	public long equipId;
	public String code;
	public long takenPhotoNum = 0;

	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c){
		equipId = CursorUtil.getLong(c, EQUIPMENT_TABLE.EQUIP_ID);
		code = CursorUtil.getString(c, EQUIPMENT_TABLE.CODE);
	}
}
