package com.ths.dmscore.util;

import java.util.HashMap;

/**
 * Map ten 2 Collumn khac nhau nhung cung kieu du lieu
 * 
 * @author: ThangNV31
 * @version: 1.0 
 */
public class CollumnMapping {
	private HashMap<String, String> map;

	public CollumnMapping() {
		this.map = new HashMap<String, String>();
	}

	/**
	 * tao mapping
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param sourceName
	 * @param destName
	 */
	public void map(String sourceName, String destName) {
		map.put(sourceName.trim().toUpperCase(), destName.trim().toUpperCase());
	}

	/**
	 * lay ten da mapping
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: String
	 * @throws:  
	 * @param name
	 * @return
	 */
	public String get(String name) {
		if(name == null){
			return null;
		}
		name = name.trim();
		if(name.equals("")){
			return "";
		}
		name = name.toUpperCase();
		Object retObj = map.get(name);
		if (retObj == null) {
			return name;
		}
		String retStr = ((String) retObj);
		if (retStr.equals("")) {
			return name;
		}
		return retStr;
	}
}
