/**
 * Copyright 2013 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 *  DTO chon nhan vien chup hinh
 *  @author: Tuanlt11
 *  @version: 1.0
 *  @since: 1.0
 */
public class StaffChoosenItemDTO {
	
		String menuText;
		boolean isSelected = false;
		public int staffId; 
		
		public StaffChoosenItemDTO(){
		}
		
		public StaffChoosenItemDTO(String staffName, boolean isSelected){
			menuText = staffName;
		}

		
		public void setTextMenu(String text){
			menuText = text;
		}
		
		public String getTextMenu(){
			return menuText;
		}
		public void setSelected(boolean selected){
			isSelected = selected;
		}
		
		public boolean isSelected(){
			return isSelected;
		}
		
		private ArrayList<StaffChoosenItemDTO> lstLevel1;

		public ArrayList<StaffChoosenItemDTO> getlstLevel1() {
			return lstLevel1;
		}

		public void setlstLevel1(ArrayList<StaffChoosenItemDTO> Items) {
			this.lstLevel1 = Items;
		}
		
		private ArrayList<StaffChoosenItemDTO> lstLevel2;

		public ArrayList<StaffChoosenItemDTO> getlstLevel2() {
			return lstLevel2;
		}

		public void setlstLevel2(ArrayList<StaffChoosenItemDTO> Items) {
			this.lstLevel2 = Items;
		}
		
		private ArrayList<StaffChoosenItemDTO> lstLevel3 = new ArrayList<StaffChoosenItemDTO>();

		public ArrayList<StaffChoosenItemDTO> getlstLevel3() {
			return lstLevel3;
		}

		public void setlstLevel3(ArrayList<StaffChoosenItemDTO> Items) {
			this.lstLevel3 = Items;
		}
		
		


}
