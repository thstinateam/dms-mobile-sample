package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.dto.db.KeyShopDTO;

/**
 * DTO cho man hinh dang ki keyshop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RegisterKeyShopViewDTO {
	// ds cau lac bo
	public ArrayList<KeyShopDTO> lstKeyShop = new ArrayList<KeyShopDTO>();
	// list chu ky
	public ArrayList<ValueItemDTO> lstCycle = new ArrayList<ValueItemDTO>();
	// list dang ki keyshop
	public ArrayList<RegisterKeyShopItemDTO> lstItem = new ArrayList<RegisterKeyShopItemDTO>();
	// cycle Id hien tai
	public long cycleIdNow;
}
