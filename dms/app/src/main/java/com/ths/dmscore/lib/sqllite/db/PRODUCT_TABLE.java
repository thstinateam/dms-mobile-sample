/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteStatement;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.view.NVBHReportForcusProductInfoViewDTO;
import com.ths.dmscore.dto.view.SaleProductInfoDTO;
import com.ths.dmscore.dto.view.SaleStatisticsAccumulateDayDTO;
import com.ths.dmscore.dto.view.SaleStatisticsProductInDayInfoViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.ListProductDTO;
import com.ths.dmscore.dto.db.ProductDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;
import com.ths.dmscore.dto.view.ForcusProductOfNVBHDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DMSCursor;
import com.ths.dmscore.util.DateUtils;

/**
 * Luu thong tin san pham
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class PRODUCT_TABLE extends ABSTRACT_TABLE {
	// id san pham
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// ten san pham
	public static final String PRODUCT_NAME = "PRODUCT_NAME";
	// trang thai 1: hoat dong, 0: ngung
	public static final String STATUS = "STATUS";
	// don vi tinh nho nhat (hop)
	public static final String UOM1 = "UOM1";
	// package (thung)
	public static final String UOM2 = "UOM2";
	// gia tri quy doi tu UOM2 --> UOM1
	public static final String CONVFACT = "CONVFACT";
	// ma nganh hang
	public static final String CATEGORY_CODE = "CATEGORY_CODE";
	public static final String SUBCATEGORY_ID = "SUBCATEGORY_ID";
	// thuoc tinh cua mat hang
	public static final String BRAND = "BRAND";
	// thuoc tinh cua mat hang
	public static final String FLAVOUR = "FLAVOUR";
	// ton kho an toan - chua dung
	public static final String SAFETYSTOCK = "SAFETYSTOCK";
	// hoa hong - chua dung
	public static final String COMMISSION = "COMMISSION";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// thuoc tinh cua mat hang
	public static final String VOLUMN = "VOLUMN";
	// thuoc tinh cua mat hang
	public static final String NET_WEIGHT = "NET_WEIGHT";
	// thuoc tinh cua mat hang
	public static final String GROSS_WEIGHT = "GROSS_WEIGHT";
	// thuoc tinh cua mat hang
	public static final String PACKING = "PACKING";
	public static final String PRODUCT_TYPE_ID = "PRODUCT_TYPE_ID";
	// ma mat hang
	public static final String PRODUCT_CODE = "PRODUCT_CODE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// loai san pham
	public static final String PRODUCT_TYPE = "PRODUCT_TYPE";
	// sub cat cua sang pham
	public static final String SUB_CAT = "SUB_CAT";
	public static final String CAT_ID = "CAT_ID";

	public static final String PRODUCT_NAME_TABLE = "PRODUCT";

	/**
	 * tao va mo mot CSDL
	 *
	 * @author: BangHN
	 * @return: SQLiteUtil
	 * @throws:
	 */
	public PRODUCT_TABLE() {
		this.tableName = PRODUCT_NAME_TABLE;
		this.columns = new String[] { PRODUCT_ID, PRODUCT_NAME, STATUS, UOM1, UOM2, CONVFACT, CATEGORY_CODE,
				SUBCATEGORY_ID, BRAND, FLAVOUR, SAFETYSTOCK, COMMISSION, CREATE_DATE, UPDATE_DATE, VOLUMN, NET_WEIGHT,
				GROSS_WEIGHT, PACKING, PRODUCT_TYPE_ID, PRODUCT_CODE, CREATE_USER, UPDATE_USER, PRODUCT_TYPE, SUB_CAT,CAT_ID,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public PRODUCT_TABLE(SQLiteDatabase mDB) {
		this.tableName = PRODUCT_NAME_TABLE;
		this.columns = new String[] { PRODUCT_ID, PRODUCT_NAME, STATUS, UOM1, UOM2, CONVFACT, CATEGORY_CODE,
				SUBCATEGORY_ID, BRAND, FLAVOUR, SAFETYSTOCK, COMMISSION, CREATE_DATE, UPDATE_DATE, VOLUMN, NET_WEIGHT,
				GROSS_WEIGHT, PACKING, PRODUCT_TYPE_ID, PRODUCT_CODE, CREATE_USER, UPDATE_USER, PRODUCT_TYPE, SUB_CAT };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/**
	 * xoa data
	 *
	 * @author: DoanDM
	 * @return: void
	 * @throws:
	 */
	public void clearTable() {
		execSQL(sqlDelete);
	}

	/**
	 * drop database trong data
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws:
	 */
	public void dropDatabase() {
		try {
			GlobalInfo.getInstance().getAppContext().deleteDatabase(Constants.DATABASE_NAME);
			MyLog.i("DBAdapter", "Drop database success.......");
		} catch (Exception e) {
			MyLog.i("DBAdapter", "Error drop database : " + e.toString());
		}
	}

	public long getCount() {
		SQLiteStatement statement = compileStatement(sqlGetCountQuerry);
		long count = statement.simpleQueryForLong();
		return count;
	}

	/**
	 * them 1 dong xuong CSDL
	 *
	 * @author: BangHN
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long insert(ProductDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((ProductDTO) dto);
		return insert(null, value);
	}

	/**
	 * thay doi 1 dong cua CSDL
	 *
	 * @author: BangHN
	 * @param inheritId
	 * @param dto
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int update(ProductDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.productId };
		return update(value, PRODUCT_ID + " = ?", params);
	}

	public long update(AbstractTableDTO dto) {
		ProductDTO pro = (ProductDTO) dto;
		ContentValues value = initDataRow(pro);
		String[] params = { "" + pro.productId };
		return update(value, PRODUCT_ID + " = ?", params);
	}

	/**
	 *
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: HieuNH
	 * @param id
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(PRODUCT_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		ProductDTO proDTO = (ProductDTO) dto;
		String[] params = { Integer.toString(proDTO.productId) };
		return delete(PRODUCT_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: ProductDTO
	 * @throws:
	 */
	public ProductDTO getProductById(String id) {
		ProductDTO ProductDTO = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(PRODUCT_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				ProductDTO = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return ProductDTO;
	}

	private ProductDTO initLogDTOFromCursor(Cursor c) {
		ProductDTO productDTO = new ProductDTO();
		productDTO.productId = (CursorUtil.getInt(c, PRODUCT_ID));
		productDTO.productName = (CursorUtil.getString(c, PRODUCT_NAME));
		productDTO.status = (CursorUtil.getInt(c, STATUS));
		productDTO.uom1 = (CursorUtil.getString(c, UOM1));
		productDTO.uom2 = (CursorUtil.getString(c, UOM2));
		productDTO.convfact = (CursorUtil.getInt(c, CONVFACT));
		productDTO.categoryCode = (CursorUtil.getString(c, CATEGORY_CODE));

		productDTO.subCategoryId = (CursorUtil.getString(c, SUBCATEGORY_ID));
		productDTO.brand = (CursorUtil.getString(c, BRAND));
		productDTO.flavour = (CursorUtil.getString(c, FLAVOUR));
		productDTO.safetyStock = (CursorUtil.getFloat(c, SAFETYSTOCK));
		productDTO.commission = (CursorUtil.getString(c, COMMISSION));
		productDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		productDTO.udpateDate = (CursorUtil.getString(c, UPDATE_DATE));
		productDTO.volumn = (CursorUtil.getFloat(c, VOLUMN));
		productDTO.netweight = (CursorUtil.getFloat(c, NET_WEIGHT));
		productDTO.grossWeight = (CursorUtil.getFloat(c, GROSS_WEIGHT));
		productDTO.packing = (CursorUtil.getString(c, PACKING));
		productDTO.productTypeId = (CursorUtil.getInt(c, PRODUCT_TYPE_ID));
		productDTO.productCode = (CursorUtil.getString(c, PRODUCT_CODE));
		productDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		productDTO.udpateUser = (CursorUtil.getString(c, UPDATE_USER));
		productDTO.productType = (CursorUtil.getString(c, PRODUCT_TYPE));
		productDTO.subCat = (CursorUtil.getString(c, SUB_CAT));
		productDTO.categoryCode = (CursorUtil.getString(c, SUB_CAT));

		return productDTO;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: BangHN
	 * @return
	 * @return: Vector<ProductDTO>
	 * @throws:
	 */
	public Vector<ProductDTO> getAllRow() {
		Vector<ProductDTO> v = new Vector<ProductDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				ProductDTO ProductDTO;
				if (c.moveToFirst()) {
					do {
						ProductDTO = initLogDTOFromCursor(c);
						v.addElement(ProductDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(ProductDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PRODUCT_ID, dto.productId);
		editedValues.put(PRODUCT_NAME, dto.productName);
		editedValues.put(STATUS, dto.status);
		editedValues.put(UOM1, dto.uom1);
		editedValues.put(UOM2, dto.uom2);
		editedValues.put(CONVFACT, dto.convfact);
		editedValues.put(CATEGORY_CODE, dto.categoryCode);
		editedValues.put(SUBCATEGORY_ID, dto.subCategoryId);
		editedValues.put(BRAND, dto.brand);
		editedValues.put(FLAVOUR, dto.flavour);
		editedValues.put(SAFETYSTOCK, dto.safetyStock);
		editedValues.put(COMMISSION, dto.commission);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.udpateDate);
		editedValues.put(VOLUMN, dto.volumn);
		editedValues.put(NET_WEIGHT, dto.netweight);
		editedValues.put(GROSS_WEIGHT, dto.grossWeight);
		editedValues.put(PACKING, dto.packing);
		editedValues.put(PRODUCT_TYPE_ID, dto.productTypeId);
		editedValues.put(PRODUCT_CODE, dto.productCode);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.udpateUser);
		editedValues.put(PRODUCT_TYPE, dto.productType);
		editedValues.put(SUB_CAT, dto.subCat);

		return editedValues;
	}

	/**
	 *
	 * Lay thong tin cua 1 san pham khuyen mai
	 *
	 * @author: Nguyen Thanh Dung
	 * @param productID
	 * @param customerId
	 * @param nvbhShopId
	 * @return
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */

	public OrderDetailViewDTO getProductStockByID(String productID, String orderType, int customerId) throws Exception{
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		String objectId = "0";
		int objectType = 1;
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}

		sql.append("SELECT p.PRODUCT_ID PRODUCT_ID, p.PRODUCT_CODE PRODUCT_CODE, p.GROSS_WEIGHT GROSS_WEIGHT, p.PRODUCT_NAME PRODUCT_NAME, p.CONVFACT CONVFACT, ");
		sql.append(" pr.PRICE PRICE, pr.PRICE_ID PRICE_ID, pr.VAT VAT, pr.PRICE_NOT_VAT PRICE_NOT_VAT, pr.PACKAGE_PRICE PACKAGE_PRICE,  pr.PACKAGE_PRICE_NOT_VAT PACKAGE_PRICE_NOT_VAT, ");
		sql.append(" st.STOCK_TOTAL_ID STOCK_TOTAL_ID, st.QUANTITY QUANTITY, st.AVAILABLE_QUANTITY AVAILABLE_QUANTITY, pi.PRODUCT_INFO_CODE PRODUCT_INFO_CODE ");
		sql.append(" FROM (select * from product where product_id = ? )p, ");
		params.add(productID);
		sql.append(" (select * from PRODUCT_INFO where 1 = 1 AND STATUS = 1 AND TYPE = 1) pi ");
		sql.append(" left outer join ( ");
		//begin doi cau lay ton kho (nhiu kho)
		sql.append("  SELECT MAX(st.STOCK_TOTAL_ID) STOCK_TOTAL_ID, SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) AVAILABLE_QUANTITY, st.product_id product_id ");
		sql.append("  FROM stock_total st ");
		sql.append("  LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("  WHERE 1=1 ");
		sql.append("    AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		params.add(shopId);
		sql.append("  	AND st.object_id = ? AND st.status = 1 ");
		params.add(objectId);
		sql.append("    AND st.object_type = ? ");
		params.add(String.valueOf(objectType));
		sql.append("  	AND st.product_id = ? ");
		params.add(productID);
		sql.append("  	group by st.product_id ");
		//end doi cau lay ton kho (nhiu kho)

		sql.append(" ) st on st.product_id = p.product_id ");
		sql.append(" left outer join (");

		//begin lay gia theo cach moi (nhiu gia)
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> arrShopId = shopTable.getShopRecursive(shopId);
		String strListShop = TextUtils.join(",", arrShopId);
		sql.append("	SELECT prRight.* FROM(	");
		sql.append("	SELECT	");
		sql.append("	    PRODUCT_ID pri_product_id,	");
		sql.append("	    IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(pr_kh_npp,	");
		sql.append("	    pr_kh_vung),	");
		sql.append("	    pr_kh_mien),	");
		sql.append("	    pr_kh_kenh),	");
		sql.append("	    pr_kh_vnm),	");
		sql.append("	    pr_kh_vnm_nhomdv),	");
		sql.append("	    pr_npp),	");
		sql.append("	    pr_vung),	");
		sql.append("	    pr_mien),	");
		sql.append("	    pr_kenh),	");
		sql.append("	    pr_vnm),	");
		sql.append("	    pr_vnm_nhomdv) price_id	");
		sql.append("	FROM	");
		sql.append("	    (SELECT	");
		sql.append("	        prc.price_id,	");
		sql.append("	        prc.PRODUCT_ID PRODUCT_ID,	");
		sql.append("	        prc.CUSTOMER_TYPE_ID,	");
		sql.append("	        prc.shop_id,	");
		sql.append("	        IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	        ''),	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id = ?	");
		params.add(shopId);
		sql.append("	            AND prc.shop_id IN                        (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                             (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 3))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = ''	");
		sql.append("	                THEN price_id	");
		sql.append("	            END) pr_kh_npp,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 2))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_vung,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 1))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_mien,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = '0'))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_kenh,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND IFNULL(prc.shop_id,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND PRC.SHOP_CHANNEL = SHOP.SHOP_CHANNEL                THEN price_id	");
		sql.append("	        END) pr_kh_vnm_nhomdv,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("				AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("				)	");
		sql.append("				AND prc.shop_id IN                  (	");
		sql.append("	    			SELECT	");
		sql.append("	        			shop_id	");
		sql.append("	    			FROM	");
		sql.append("	         			shop	");
		sql.append("	     			WHERE	");
		sql.append("	         			status = 1	");
		sql.append("	         			AND shop_type_id IN                       (	");
		sql.append("	             			SELECT	");
		sql.append("	                 			channel_type_id	");
		sql.append("	             			FROM	");
		sql.append("	                 			channel_type	");
		sql.append("	             			WHERE	");
		sql.append("	                 			TYPE = 1	");
		sql.append("	                 			AND object_type = 14	");
		sql.append("	     			)) THEN price_id	");
		sql.append("	        END) pr_kh_vnm,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (?)	");
		params.add(shopId);
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 3)) THEN price_id	");
		sql.append("	            END) pr_npp,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 2)) THEN price_id	");
		sql.append("	            END) pr_vung,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 1)) THEN price_id	");
		sql.append("	            END) pr_mien,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = '0')) THEN price_id	");
		sql.append("	            END) pr_kenh,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND PRC.SHOP_CHANNEL = SHOP.SHOP_CHANNEL	");
		sql.append("	            AND IFNULL(prc.shop_id,	");
		sql.append("	            '') = '' THEN price_id	");
		sql.append("	        END) pr_vnm_nhomdv,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("				AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("				)	");
		sql.append("				AND prc.shop_id IN                  (	");
		sql.append("	    			SELECT	");
		sql.append("	        			shop_id	");
		sql.append("	    			FROM	");
		sql.append("	        			shop	");
		sql.append("	    			WHERE	");
		sql.append("	        			status = 1	");
		sql.append("	        			AND shop_type_id IN                       (	");
		sql.append("	            			SELECT	");
		sql.append("	                			channel_type_id	");
		sql.append("	            			FROM	");
		sql.append("	                			channel_type	");
		sql.append("	            			WHERE	");
		sql.append("	                			TYPE = 1	");
		sql.append("	                			AND object_type = 14	");
		sql.append("	        			)	");
		sql.append("	    			) THEN price_id	");
		sql.append("	        END) pr_vnm	");
		sql.append("	    FROM	");
		sql.append("	        (SELECT	");
		sql.append("	            *	");
		sql.append("	        FROM	");
		sql.append("	            PRICE	");
		sql.append("	        WHERE	");
		sql.append("	            1 = 1	");
		sql.append("	            AND PRICE.STATUS = 1	");
		sql.append("	            AND Ifnull(DATE (PRICE.from_date) <= DATE (?), 0)	");
		params.add(dateNow);
		sql.append("	            AND Ifnull(DATE(PRICE.to_date) >= DATE (?), 1)) prc	");
		params.add(dateNow);
		sql.append("	    JOIN	");
		sql.append("	        (	");
		sql.append("	            SELECT	");
		sql.append("	                CHANNEL_TYPE_ID AS CUSTOMER_TYPE_ID	");
		sql.append("	            FROM	");
		sql.append("	                CUSTOMER	");
		sql.append("	            WHERE	");
		sql.append("	                CUSTOMER_ID = ?	");
		params.add(String.valueOf(customerId));
		sql.append("	        ) c	");
		sql.append("	            ON 1=1	");
		sql.append("	    JOIN	");
		sql.append("	        (	");
		sql.append("	            SELECT	");
		sql.append("	                SHOP_CHANNEL	");
		sql.append("	            FROM	");
		sql.append("	                SHOP	");
		sql.append("	            WHERE	");
		sql.append("	                SHOP_ID = ?	");
		params.add(shopId);
		sql.append("	        ) shop	");
		sql.append("	            ON 1=1	");
		sql.append("	    GROUP BY	");
		sql.append("	        prc.PRODUCT_ID	");
		sql.append("	)) pri_id, PRICE prRight	");
		sql.append("	WHERE 1=1	");
		sql.append("	and pri_id.price_id = prRight.price_id	");
		//end lay gia theo cach moi (nhiu gia)

		sql.append(" ) pr ");
		sql.append(" on pr.product_id = p.product_id  ");
		sql.append(" WHERE 1 = 1");
//		sql.append(" AND pi.STATUS = 1 ");
//		sql.append(" AND pi.TYPE = 1 ");
		sql.append(" AND pi.PRODUCT_INFO_ID = p.CAT_ID ");

		OrderDetailViewDTO productPromotion = null;
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					productPromotion = initPromotionProductInfo(c);
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return productPromotion;

	}

	/**
	 *
	 * Lay thong tin cua 1 san pham khuyen mai
	 *
	 * @author: Nguyen Thanh Dung
	 * @param productID
	 * @param customerId
	 * @param nvbhShopId
	 * @return
	 * @return: OrderDetailViewDTO
	 * @throws Exception
	 * @throws:
	 */

	public List<OrderDetailViewDTO> getListProductStockByID(String productID, String orderType, long customerId) throws Exception {
		ArrayList<OrderDetailViewDTO> result = new ArrayList<OrderDetailViewDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		String objectId = "0";
		int objectType = 1;
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}

		sql.append("SELECT p.PRODUCT_ID PRODUCT_ID, p.cat_id CAT_ID, p.PRODUCT_CODE PRODUCT_CODE, p.GROSS_WEIGHT GROSS_WEIGHT, p.PRODUCT_NAME PRODUCT_NAME, p.CONVFACT CONVFACT, ");
		sql.append(" pr.PRICE PRICE, pr.package_price as PACKAGE_PRICE, pr.PRICE_ID PRICE_ID, pr.VAT VAT, pr.PRICE_NOT_VAT PRICE_NOT_VAT, pr.PACKAGE_PRICE_NOT_VAT PACKAGE_PRICE_NOT_VAT, ");
		sql.append(" st.STOCK_TOTAL_ID STOCK_TOTAL_ID, st.QUANTITY QUANTITY, st.AVAILABLE_QUANTITY AVAILABLE_QUANTITY, pi.PRODUCT_INFO_CODE PRODUCT_INFO_CODE ");
		sql.append(" FROM (select * from product where product_id in ( ");
		sql.append(productID);
		sql.append(" )) p left join ");
		sql.append(" (select * from PRODUCT_INFO where 1 = 1 AND STATUS = 1 AND TYPE = 1) pi on pi.product_info_id = p.cat_id ");
		sql.append(" left outer join ( ");
		//begin doi cau lay ton kho (nhiu kho)
		sql.append("  SELECT MAX(st.STOCK_TOTAL_ID) STOCK_TOTAL_ID, SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) AVAILABLE_QUANTITY, st.product_id product_id ");
		sql.append("  FROM stock_total st ");
		sql.append("  LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("  WHERE 1=1 ");
		sql.append("    AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		params.add(shopId);
		sql.append("  	AND st.object_id = ? AND st.status = 1 ");
		params.add(objectId);
		sql.append("    AND st.object_type = ? ");
		params.add(String.valueOf(objectType));
		sql.append("  	AND st.product_id in ( ");
		sql.append(productID);
		sql.append("  	) group by st.product_id ");
		//end doi cau lay ton kho (nhiu kho)

		sql.append(" ) st on st.product_id = p.product_id ");
		sql.append(" left outer join (");

		sql.append("       select pt.product_id, p.price_id	");
		sql.append("       , p.price, p.package_price	");
		sql.append("       , p.vat, p.price_not_vat	");
		sql.append("       , p.package_price_not_vat	");
		sql.append("       from price_temp pt, price p	");
		sql.append("       where 1 = 1	");
		sql.append("       and p.price_id = pt.price_id	");

		sql.append(" ) pr ");
		sql.append(" on pr.product_id = p.product_id  ");
//		sql.append(" WHERE 1 = 1");
//		sql.append(" AND pi.STATUS = 1 ");
//		sql.append(" AND pi.TYPE = 1 ");
//		sql.append(" AND pi.PRODUCT_INFO_ID = p.CAT_ID ");

		OrderDetailViewDTO productPromotion = null;
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						productPromotion = initPromotionProductInfo(c);
						result.add(productPromotion);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
			throw e;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return result;

	}
	/**
	 *
	 * Tao mot doi tuong khuyen mai voi gia tri ton kho con lai
	 *
	 * @author: Nguyen Thanh Dung
	 * @param cursor
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	OrderDetailViewDTO initPromotionProductInfo(Cursor cursor) {
		OrderDetailViewDTO productPromotion = new OrderDetailViewDTO();
		SaleOrderDetailDTO productInfo = new SaleOrderDetailDTO();
		productPromotion.orderDetailDTO = productInfo;

		productPromotion.orderDetailDTO.productId = CursorUtil.getInt(cursor, PRODUCT_ID);
		productPromotion.productCode = CursorUtil.getString(cursor, PRODUCT_CODE);
		productPromotion.productName = CursorUtil.getString(cursor, PRODUCT_NAME);
		productPromotion.convfact = CursorUtil.getInt(cursor, CONVFACT);
		productPromotion.orderDetailDTO.catId = CursorUtil.getInt(cursor, CAT_ID);
		productPromotion.stockId = CursorUtil.getLong(cursor, STOCK_TOTAL_TABLE.STOCK_TOTAL_ID);
		productPromotion.stock = CursorUtil.getLong(cursor, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		productPromotion.stockActual = CursorUtil.getLong(cursor, STOCK_TOTAL_TABLE.QUANTITY);
		productPromotion.orderDetailDTO.price = CursorUtil.getDouble(cursor, PRICE_TABLE.PRICE);
		productPromotion.orderDetailDTO.packagePrice = CursorUtil.getDouble(cursor, PRICE_TABLE.PACKAGE_PRICE);
		productPromotion.orderDetailDTO.priceNotVat = CursorUtil.getDouble(cursor, PRICE_TABLE.PRICE_NOT_VAT);
		productPromotion.orderDetailDTO.packagePriceNotVat = CursorUtil.getDouble(cursor, PRICE_TABLE.PACKAGE_PRICE_NOT_VAT);
		productPromotion.orderDetailDTO.priceId = CursorUtil.getLong(cursor, PRICE_TABLE.PRICE_ID);
		productPromotion.grossWeight = CursorUtil.getDouble(cursor, PRODUCT_TABLE.GROSS_WEIGHT);
		productPromotion.orderDetailDTO.vat = CursorUtil.getDouble(cursor, PRICE_TABLE.VAT);

		return productPromotion;
	}

	/**
	 *
	 * lay thong tin chi tiet san pham
	 *
	 * @author: HaiTC3
	 * @param productId
	 * @return
	 * @return: ProductDTO
	 * @throws:
	 * @since: Feb 4, 2013
	 */
	public ProductDTO getProductInfoDetail(String productId) {
		ProductDTO dto = new ProductDTO();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT p.product_code, ");
		sqlObject.append("                p.product_name, ");
		sqlObject.append("                p.convfact, ");
		sqlObject.append("                pi.product_info_code         CATEGORY_CODE, ");
		sqlObject.append("                (SELECT ap.ap_param_name ");
		sqlObject.append("                 FROM   ap_param ap ");
		sqlObject.append("                 WHERE  p.uom2 = ap.ap_param_code ");
		sqlObject.append("                        AND ap.status = 1 ");
		sqlObject.append("                        AND ap.type = 'UOM') UOM2, ");
		sqlObject.append("                (SELECT ap.ap_param_name ");
		sqlObject.append("                 FROM   ap_param ap ");
		sqlObject.append("                 WHERE  p.uom1 = ap.ap_param_code ");
		sqlObject.append("                        AND ap.status = 1 ");
		sqlObject.append("                        AND ap.type = 'UOM') UOM1 ");
		sqlObject.append("FROM   product p, ");
		sqlObject.append("       product_info pi, ");
		sqlObject.append("       ap_param ap ");
		sqlObject.append("WHERE  p.product_id = ? ");
		sqlObject.append("       AND pi.status = 1 ");
		sqlObject.append("       AND ap.status = 1 ");
		sqlObject.append("       AND pi.type = 1 ");
		sqlObject.append("       AND pi.product_info_id = p.cat_id ");

		String[] param = new String[] { productId };
		Cursor c = null;
		try {
			c = rawQuery(sqlObject.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					dto.initDataWithCursor(c);
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return dto;
	}

	/**
	 * request lay danh sach san pham
	 * tu man hinh 05-01 nhan vien ban hang
	 * @author: HieuNH, BangHN
	 * @return: ListProductDTO
	 * @throws:
	 */
	public ListProductDTO getProductList(Bundle data, ArrayList<String> shopIdArray) throws Exception{
		ListProductDTO dto = null;
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String tomorrow = DateUtils.getTomorrowDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String page = data.getString(IntentConstants.INTENT_PAGE);
		String productCode = data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String idListString = TextUtils.join(",", shopIdArray);
		// String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String saleTypeCode = data.getString(IntentConstants.INTENT_SALE_TYPE_CODE);
		String productInfoCode = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		String productInfoCodeSubCat = data.getString(IntentConstants.INTENT_PRODUCT_SUB_CAT);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		int objectType;
		String objectId = null;
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
			// Vansale lay ton kho NPP
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}

		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		////sql try van
		StringBuffer  sql = new StringBuffer();
		sql.append("SELECT DISTINCT PD.product_id                AS PRODUCT_ID, ");
		sql.append("                PD.product_code              AS PRODUCT_CODE, ");
		sql.append("                PD.convfact                  AS CONVFACT, ");
		sql.append("                PD.product_name              AS PRODUCT_NAME, ");
		sql.append("                UOM2.ap_param_name    			UOM2,  ");
		sql.append("                uom1.ap_param_name    			UOM1, ");
		sql.append("                PD.name_text                 AS PRODUCT_NAME_TEXT, ");
		sql.append("                pi.category_code      		 AS CATEGORY_CODE, ");
		sql.append("                pis.sub_category_code        AS SUB_CATEGORY_CODE, ");
		sql.append("                PD.create_date               AS CREATE_DATE, ");
		sql.append("                PR.price_id                  AS PRICE_ID, ");
		sql.append("                PR.price                     AS PRICE, ");
		sql.append("                PR.PACKAGE_PRICE             AS PACKAGE_PRICE, ");
		sql.append("                PR.status                    AS PRICE_STATUS, ");
		sql.append("                ST.available_quantity        AS AVAILABLE_QUANTITY, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN fp.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )                      AS TT, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN pro.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )                      AS HAS_PROMOTION, ");

		sql.append(" ( CASE ");
		sql.append("                    WHEN image.object_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  end )               IMAGE, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN video.object_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  end )               VIDEO ");

		sql.append("FROM   (SELECT * ");
		sql.append("        FROM   product ");
		sql.append("        WHERE  1 = 1  ");
		//cho phep hien san pham cua tat ca nganh hang, khong phan biet nganh hang Z
		//sql.append("               AND product_info.product_info_code != 'Z' ");
		sql.append("               AND status = 1 ) PD ");
		if (!StringUtil.isNullOrEmpty(productInfoCode)) {
			sql.append(" JOIN ");
		} else {
			sql.append(" LEFT JOIN ");
		}
		sql.append(" (SELECT product_info_code AS category_code, ");
		sql.append("                         product_info_id ");
		sql.append("                  FROM   product_info ");
		sql.append("                  WHERE  1 = 1 ");
		sql.append("                         AND type = 1 ");
		if(!StringUtil.isNullOrEmpty(productInfoCode)) {
			sql.append("                         AND product_info_code like upper(?) ");
			param.add(productInfoCode);
		}
		sql.append("                         AND status = 1) pi ");
		sql.append("              ON pi.product_info_id = pd.cat_id ");

		if (!StringUtil.isNullOrEmpty(productInfoCodeSubCat)) {
			sql.append(" JOIN ");
		} else {
			sql.append(" LEFT JOIN ");
		}
		sql.append(" (SELECT product_info_code AS sub_category_code, ");
		sql.append("                         product_info_id ");
		sql.append("                  FROM   product_info ");
		sql.append("                  WHERE  1 = 1 ");
		sql.append("                         AND type = 2 ");
		if(!StringUtil.isNullOrEmpty(productInfoCodeSubCat)){
			sql.append("                         AND product_info_code like upper(?) ");
			param.add(productInfoCodeSubCat);
		}
		sql.append("                         AND status = 1) pis ");
		sql.append("              ON pis.product_info_id = pd.sub_cat_id ");


		sql.append("       LEFT JOIN(SELECT ap.ap_param_name, ");
		sql.append("                        ap_param_code ");
		sql.append("                 FROM   ap_param ap ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND ap.status = 1 ");
		sql.append("                        AND ap.type = 'UOM') uom2 ");
		sql.append("              ON pd.uom2 = uom2.ap_param_code ");
		sql.append("       LEFT JOIN(SELECT ap.ap_param_name, ");
		sql.append("                        ap_param_code ");
		sql.append("                 FROM   ap_param ap ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND ap.status = 1 ");
		sql.append("                        AND ap.type = 'UOM') uom1 ");
		sql.append("              ON pd.uom1 = uom1.ap_param_code ");
		sql.append("       LEFT JOIN(SELECT object_id ");
		sql.append("                 FROM   media_item mdi ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND mdi.object_type = 3 ");
		sql.append("                        AND mdi.media_type = 0 ");
		sql.append("                        AND mdi.status = 1) image ");
		sql.append("              ON image.[object_id] = pd.product_id ");
		sql.append("       LEFT JOIN (SELECT mm.object_id ");
		sql.append("                  FROM   media m, ");
		sql.append("                         media_map mm, ");
		sql.append("                         media_item mi ");
		sql.append("                  WHERE  m.status = 1 ");
		sql.append("                         AND mm.object_type = 1 ");
		sql.append("                         AND mm.status = 1 ");
		sql.append("                         AND mi.status = 1 ");
		sql.append("                         AND mi.object_type = 7 ");
		sql.append("                         AND mi.media_type = 1 ");
		sql.append("                         AND mm.media_id = m.media_id ");
		sql.append("                         AND m.media_item_id = mi.media_item_id) video ");
		sql.append("              ON video.[object_id] = pd.product_id ");
		sql.append("       LEFT JOIN (	");
		sql.append("       select pt.product_id, p.price_id	");
		sql.append("       , p.price, p.package_price	");
		sql.append("       , p.status	");
		sql.append("       from price_temp pt, price p	");
		sql.append("       where 1 = 1	");
		sql.append("       and p.price_id = pt.price_id	");

		//end lay gia theo cach moi (nhiu gia) - chi lay muc shop

		sql.append("                         ) PR ");
		sql.append("              ON PD.product_id = PR.product_id ");

		//begin doi cau lay ton kho (nhiu kho)
		sql.append("       LEFT JOIN (SELECT SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sql.append("                  FROM stock_total st ");
		sql.append("                  LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("                  WHERE 1=1 ");
		sql.append("    				AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		param.add(shopId);
		sql.append("                  	AND st.object_id = ? AND st.status = 1 ");
		param.add(objectId);
		sql.append("                  	AND st.object_type = ? ");
		param.add(String.valueOf(objectType));
		sql.append("                  group by st.product_id) ST ");
		//end doi cau lay ton kho (nhiu kho)

		sql.append("              ON PD.product_id = ST.product_id ");
		sql.append("       LEFT JOIN (SELECT DISTINCT fcmd.product_id, ");
		sql.append("                  ap.value, ");
		sql.append("                  'z'  z ");
		sql.append("                  FROM   focus_program fp, ");
		sql.append("                         focus_shop_map fsm, ");
		sql.append("                         focus_channel_map fcm, ");
		sql.append("                         focus_channel_map_product fcmd, ");
		sql.append("                         AP_PARAM AP  ");
		sql.append("                  WHERE  fp.focus_program_id = fsm.focus_program_id ");
		sql.append("                         AND fp.focus_program_id = fcm.focus_program_id ");
		sql.append("                         AND fcm.focus_channel_map_id = ");
		sql.append("                             fcmd.focus_channel_map_id ");
//		sql.append("                         AND fcm.sale_type_code = ? ");
//		param.add(saleTypeCode);
		sql.append("                         AND substr(fp.from_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("                         AND Ifnull(substr(fp.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("                         AND substr(fcm.from_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("                         AND Ifnull(substr(fcm.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("                         AND fp.status = 1 ");
		sql.append("                         AND fsm.status = 1 ");
		sql.append("                         AND fcm.status = 1 ");
		sql.append("                         AND AP.TYPE = 'FOCUS_PRODUCT_TYPE'  ");
		sql.append("                         AND AP.AP_PARAM_CODE = fcmd.TYPE   ");
//		sql.append("                         AND fsm.shop_id IN ( ");
//		sql.append(idListString);
//		sql.append(" ) ");
		sql.append(" ) AS FP ");
		sql.append("              ON pd.product_id = FP.product_id ");

//		sql.append("       LEFT JOIN (SELECT DISTINCT GLD.product_id product_id ");
//		sql.append("		FROM	");
//		sql.append("			promotion_program pp,	");
//		sql.append("			PRODUCT_GROUP PG,	");
//		sql.append("			GROUP_LEVEL GL,	");
//		sql.append("			GROUP_LEVEL_DETAIL GLD,	");
//		sql.append("			promotion_shop_map psm	");
//		sql.append("		WHERE	");
//		sql.append("			1 = 1	");
//		sql.append("			AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID	");
//		sql.append("			AND PG.PRODUCT_GROUP_ID = GL.PRODUCT_GROUP_ID	");
//		sql.append("			AND GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
//		sql.append("			AND PG.GROUP_TYPE = 1	");
//		sql.append("			AND GL.HAS_PRODUCT = 1	");
//		sql.append("			AND PP.STATUS = 1	");
//		sql.append("			AND PG.STATUS = 1	");
//		sql.append("			AND GL.STATUS = 1	");
//		sql.append("			AND GLD.STATUS = 1	");
//		sql.append("                         AND pp.promotion_program_id = psm.promotion_program_id ");
//		sql.append("                         AND Date(pp.from_date) <= Date(?) ");
//		param.add(dateNow);
//		sql.append("                         AND Ifnull(Date(pp.to_date) >= Date(?), 1) ");
//		param.add(dateNow);
//		sql.append("                         AND pp.status = 1 ");
//		sql.append("                         AND psm.status = 1 ");
//		sql.append("                         AND psm.shop_id IN ( ");
//		sql.append(idListString);
//		sql.append(")) AS pro ");

		sql.append("       LEFT JOIN (SELECT ");
		sql.append("                        DISTINCT GLD.product_id product_id ");
		sql.append("                    FROM ");
		sql.append("                        (SELECT PP.PROMOTION_PROGRAM_ID FROM promotion_program PP ");
		sql.append("						WHERE PP.STATUS = 1 ");
		sql.append("							AND substr(pp.from_date, 1, 10) < ? ");
		param.add(tomorrow);
		sql.append("							AND Ifnull(substr(pp.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("						) AS PP ");
		sql.append("                        JOIN ");
		sql.append("						(SELECT PG.PROMOTION_PROGRAM_ID, PG.PRODUCT_GROUP_ID FROM PRODUCT_GROUP PG ");
		sql.append("						WHERE PG.STATUS = 1 ");
		sql.append("							AND PG.GROUP_TYPE = 1 ");
		sql.append("						) AS PG ");
		sql.append("						ON PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID ");
		sql.append("						JOIN ");
		sql.append("                        (SELECT GL.PRODUCT_GROUP_ID, GL.GROUP_LEVEL_ID FROM GROUP_LEVEL GL ");
		sql.append("						WHERE GL.HAS_PRODUCT = 1 ");
		sql.append("							AND GL.STATUS = 1 ");
		sql.append("						) AS GL ");
		sql.append("						ON PG.PRODUCT_GROUP_ID = GL.PRODUCT_GROUP_ID ");
		sql.append("						JOIN ");
		sql.append("                        (SELECT GLD.GROUP_LEVEL_ID, GLD.product_id FROM GROUP_LEVEL_DETAIL GLD ");
		sql.append("						WHERE GLD.STATUS = 1 ");
		sql.append("						) AS GLD ");
		sql.append("						ON GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID ");
		sql.append("						JOIN ");
		sql.append("                        (SELECT PSM.promotion_program_id FROM promotion_shop_map PSM ");
		sql.append("						WHERE psm.status = 1 ");
		sql.append("							AND psm.shop_id IN ( ");
		sql.append(idListString);
		sql.append("							) ");
		sql.append("						) AS PSM ");
		sql.append("						ON PP.promotion_program_id = PSM.promotion_program_id ");
		sql.append("				) AS pro ");

//		sql.append("       LEFT JOIN (SELECT ");
//		sql.append("                        1 as product_id ");
//		sql.append("				) AS pro ");

		sql.append("              ON pd.product_id = pro.product_id ");
//		sql.append("              left join media_item  MDI  ");
//		sql.append("              on MDI.object_id = PD.product_id ");
//		sql.append("              AND MDI.object_type = 3 ");
//		sql.append("              AND MDI.media_type = 0	AND MDI.status = 1 ");
//		sql.append("      LEFT JOIN ( ");
//		sql.append("				SELECT mm.object_id, ");
//		sql.append("                         m.media_id ");
//		sql.append("                  FROM   media m, ");
//		sql.append("                         media_map mm, ");
//		sql.append("                         media_item mi ");
//		sql.append("                  WHERE  m.status = 1 ");
//		sql.append("                         AND mm.object_type = 1 ");
//		sql.append("                         AND mm.status = 1 ");
//		sql.append("                         AND mi.status = 1 ");
//		sql.append("                         AND mi.object_type = 7 ");
//		sql.append("                         AND mi.media_type = 1 ");
//		sql.append("                         AND mm.media_id = m.media_id ");
//		sql.append("                         AND m.media_item_id = mi.media_item_id ");
//		sql.append("       ) MDV  ON PD.product_id = MDV.object_id ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND substr(PD.create_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("       AND Ifnull(substr(PD.update_date, 1, 10) <= ?, 1) ");
		param.add(dateNow);

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			sql.append("	and upper(PRODUCT_CODE) like upper(");
			sql.append(productCode);
			sql.append(") escape '^' ");
			sql.append("	or upper(PRODUCT_NAME_TEXT) like upper(");
			sql.append(productCode);
			sql.append(") escape '^' ");
		}
//		if (!StringUtil.isNullOrEmpty(productName)) {
//			productName = StringUtil.getEngStringFromUnicodeString(productName);
//			productName = StringUtil.escapeSqlString(productName);
//			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
//			sql.append("	and upper(PRODUCT_NAME_TEXT) like upper(");
//			sql.append(productName);
//			sql.append(") escape '^' ");
//		}

		sql.append(" group by pd.product_id  ");
//		sql.append(" ORDER BY tt DESC, has_promotion DESC, product_code ASC ");

		String orderStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "product_code")
			.addMapper(SortActionConstants.NAME, "product_name")
			.addMapper(SortActionConstants.CAT, "CATEGORY_CODE")
			.addMapper(SortActionConstants.SUB_CAT, "sub_category_code")
			.addMapper(SortActionConstants.CONVFACT, "convfact")
			.addMapper(SortActionConstants.AVAILABLE_QUANTITY, "AVAILABLE_QUANTITY")
			.addMapper(SortActionConstants.PRICE, "price")
			.addMapper(SortActionConstants.PACKAGE_PRICE, "PACKAGE_PRICE")
			.addMapper(SortActionConstants.IMAGE_COUNT, "(IMAGE + (VIDEO * 2))")
			.defaultOrderString(" ORDER BY z desc, fp.value asc, PD.order_index, product_code ASC ")
			.build(sortInfo);
		//add order string
		sql.append(orderStr);

		totalPageSql.append("select COUNT(*) as TOTAL_ROW from (" + sql + ")");
		sql.append(page);

		Cursor c = null;
		Cursor c1 = null;
		try {
			c = rawQueries(sql.toString(), param);
			if (c != null) {
				if (dto == null) {
					dto = new ListProductDTO();
				}
				if (c.moveToFirst()) {
					do {
						ProductDTO productInfo = new ProductDTO();
						productInfo.initDataFromCursor(c);
						dto.producList.add(productInfo);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == 1) {
				c1 = rawQueries(totalPageSql.toString(), param);
				if (c1 != null) {
					c1.moveToFirst();
					if (dto != null) {
						dto.total = c1.getInt(0);
					}
				}

			}
		}finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e) {
				}
			}
			if (c1 != null) {
				try {
					c1.close();
				} catch (Exception e) {
				}
			}
		}

		return dto;
	}

	/**
	 * Lay danh sach san pham cua role GSNPP
	 * @author: HieuNH
	 * @param data
	 * @return
	 * @return: ListProductDTO
	 * @throws:
	 */
	public ListProductDTO getSupervisorProductList(Bundle data, ArrayList<String> shopIdArray) throws Exception{
		ListProductDTO dto = new ListProductDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String page = data.getString(IntentConstants.INTENT_PAGE);
		String productCode = data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String idListString = TextUtils.join(",", shopIdArray);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String productInfoCode = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		String productInfoCodeSubCat = data.getString(IntentConstants.INTENT_PRODUCT_SUB_CAT);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);

		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT PD.product_id         AS PRODUCT_ID, ");
		sql.append("       PD.product_code       AS PRODUCT_CODE, ");
		sql.append("       PD.convfact           AS CONVFACT, ");
		sql.append("       PD.product_name       AS PRODUCT_NAME, ");
		sql.append("       PD.name_text          AS NAME_TEXT, ");
		sql.append("       UOM2.ap_param_name    UOM2,  ");
		sql.append("       uom1.ap_param_name    UOM1, ");
		sql.append("       Pi.category_code  	AS CATEGORY_CODE, ");
		sql.append("       pis.sub_category_code        AS SUB_CATEGORY_CODE, ");
		sql.append("       PD.create_date        AS CREATE_DATE, ");
		sql.append("       PR.price_id           AS PRICE_ID, ");
		sql.append("       PR.price              AS PRICE, ");
		sql.append("       PR.package_price      AS PACKAGE_PRICE, ");
		sql.append("       PR.status             AS PRICE_STATUS, ");
		sql.append("       ST.available_quantity AS AVAILABLE_QUANTITY, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN fp.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )                      AS TT, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN pro.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )                      AS HAS_PROMOTION, ");

		sql.append(" ( CASE ");
		sql.append("                    WHEN image.object_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  end )               IMAGE, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN video.object_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  end )               VIDEO ");

		sql.append("FROM   (SELECT  ");
		sql.append("	product_id         AS PRODUCT_ID, ");
		sql.append("	product_code       AS PRODUCT_CODE, ");
		sql.append("	order_index        AS order_index, ");
		sql.append("	convfact           AS CONVFACT, ");
		sql.append("	product_name       AS PRODUCT_NAME, ");
		sql.append("	uom1               AS UOM1, ");
		sql.append("	uom2               AS UOM2, ");
		sql.append("	name_text          AS NAME_TEXT, ");
		sql.append("	cat_id        	   AS CAT_ID, ");
		sql.append("	sub_cat_id         AS SUB_CAT_ID, ");
		sql.append("	create_date        AS CREATE_DATE, ");
		sql.append("	update_date        AS UPDATE_DATE ");
		sql.append("        FROM   product ");
		sql.append("        WHERE  1=1 ");
		//cho phep hien san pham cua tat ca nganh hang, khong phan biet nganh hang Z
		//sql.append("               AND product_info.product_info_code != 'Z' ");
		sql.append("               AND product.status = 1) PD ");

		if (!StringUtil.isNullOrEmpty(productInfoCode)) {
			sql.append(" JOIN ");
		} else {
			sql.append(" LEFT JOIN ");
		}
		sql.append(" (SELECT product_info_code AS category_code, ");
		sql.append("                         product_info_id ");
		sql.append("                  FROM   product_info ");
		sql.append("                  WHERE  1 = 1 ");
		sql.append("                         AND type = 1 ");
		if(!StringUtil.isNullOrEmpty(productInfoCode)) {
			sql.append("                         AND product_info_code like upper(?) ");
			param.add(productInfoCode);
		}
		sql.append("                         AND status = 1) pi ");
		sql.append("              ON pi.product_info_id = pd.cat_id ");

		if (!StringUtil.isNullOrEmpty(productInfoCodeSubCat)) {
			sql.append(" JOIN ");
		} else {
			sql.append(" LEFT JOIN ");
		}
		sql.append(" (SELECT product_info_code AS sub_category_code, ");
		sql.append("                         product_info_id ");
		sql.append("                  FROM   product_info ");
		sql.append("                  WHERE  1 = 1 ");
		sql.append("                         AND type = 2 ");
		if(!StringUtil.isNullOrEmpty(productInfoCodeSubCat)){
			sql.append("                         AND product_info_code like upper(?) ");
			param.add(productInfoCodeSubCat);
		}
		sql.append("                         AND status = 1) pis ");
		sql.append("              ON pis.product_info_id = pd.sub_cat_id ");

		sql.append("       LEFT JOIN(SELECT ap.ap_param_name, ");
		sql.append("                        ap_param_code ");
		sql.append("                 FROM   ap_param ap ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND ap.status = 1 ");
		sql.append("                        AND ap.type = 'UOM') uom2 ");
		sql.append("              ON pd.uom2 = uom2.ap_param_code ");
		sql.append("       LEFT JOIN(SELECT ap.ap_param_name, ");
		sql.append("                        ap_param_code ");
		sql.append("                 FROM   ap_param ap ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND ap.status = 1 ");
		sql.append("                        AND ap.type = 'UOM') uom1 ");
		sql.append("              ON pd.uom1 = uom1.ap_param_code ");
		sql.append("       LEFT JOIN(SELECT object_id ");
		sql.append("                 FROM   media_item mdi ");
		sql.append("                 WHERE  1 = 1 ");
		sql.append("                        AND mdi.object_type = 3 ");
		sql.append("                        AND mdi.media_type = 0 ");
		sql.append("                        AND mdi.status = 1) image ");
		sql.append("              ON image.[object_id] = pd.product_id ");
		sql.append("       LEFT JOIN (SELECT mm.object_id ");
		sql.append("                  FROM   media m, ");
		sql.append("                         media_map mm, ");
		sql.append("                         media_item mi ");
		sql.append("                  WHERE  m.status = 1 ");
		sql.append("                         AND mm.object_type = 1 ");
		sql.append("                         AND mm.status = 1 ");
		sql.append("                         AND mi.status = 1 ");
		sql.append("                         AND mi.object_type = 7 ");
		sql.append("                         AND mi.media_type = 1 ");
		sql.append("                         AND mm.media_id = m.media_id ");
		sql.append("                         AND m.media_item_id = mi.media_item_id) video ");
		sql.append("              ON video.[object_id] = pd.product_id ");

		sql.append("       LEFT JOIN (	");
		sql.append("       select pt.product_id, p.price_id	");
		sql.append("       , p.price, p.package_price	");
		sql.append("       , p.status	");
		sql.append("       from price_temp pt, price p	");
		sql.append("       where 1 = 1	");
		sql.append("       and p.price_id = pt.price_id	");

		sql.append("                         ) PR ");
		sql.append("              ON PD.product_id = PR.product_id ");
		//end lay gia theo cach moi (nhiu gia) - chi lay muc shop

		// npp khi moi login vao chon
		sql.append("       LEFT JOIN ( ");
		//doi cau lay ton kho (nhiu kho)
		sql.append("       SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sql.append("       FROM stock_total st ");
		sql.append("       LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("       WHERE 1=1 ");
		sql.append("        	AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		param.add(shopId);
		sql.append("        	AND st.object_id = ? AND st.status = 1 ");
		param.add(shopId);
		sql.append("       		AND st.object_type = ? ");
		param.add(String.valueOf(StockTotalDTO.TYPE_SHOP));
		sql.append("       group by st.product_id ");
		sql.append("                         					) ST ");
		sql.append("              ON PD.product_id = ST.product_id ");
		// mat hang trong tam
		sql.append("       LEFT JOIN (SELECT DISTINCT fcmd.product_id , ");
		sql.append("                  ap.value, ");
		sql.append("                  'z'  z ");
		sql.append("                  FROM   focus_program fp, ");
		sql.append("                         focus_shop_map fsm, ");
		sql.append("                         focus_channel_map fcm, ");
		sql.append("                         focus_channel_map_product fcmd , ");
		sql.append("                         AP_PARAM AP  ");
		sql.append("                  WHERE  fp.focus_program_id = fsm.focus_program_id ");
		sql.append("                         AND fp.focus_program_id = fcm.focus_program_id ");
		sql.append("                         AND fcm.focus_channel_map_id = ");
		sql.append("                             fcmd.focus_channel_map_id ");
		sql.append("                         AND substr(fp.from_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("                         AND Ifnull(substr(fp.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("                         AND substr(fcm.from_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("                         AND Ifnull(substr(fcm.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("                         AND fp.status = 1 ");
		sql.append("                         AND fsm.status = 1 ");
		sql.append("                         AND fcm.status = 1 ");
		sql.append("                         AND AP.TYPE = 'FOCUS_PRODUCT_TYPE'  ");
		sql.append("                         AND AP.AP_PARAM_CODE = fcmd.TYPE   ");
		sql.append("                         AND fsm.shop_id IN ( ");
		sql.append(idListString);
		sql.append(")) AS FP ");
		sql.append("              ON pd.product_id = FP.product_id ");
		// Chuong trinh khuyen mai
		sql.append("       LEFT JOIN (SELECT DISTINCT GLD.product_id product_id ");
		sql.append("		FROM	");
		sql.append("			promotion_program pp	");
		sql.append("			,PRODUCT_GROUP PG,	");
		sql.append("			GROUP_LEVEL GLP,	");
		sql.append("			GROUP_LEVEL GLC,	");
		sql.append("			GROUP_LEVEL_DETAIL GLD,	");
		sql.append("       promotion_shop_map psm ");
		sql.append("		WHERE	");
		sql.append("			1 = 1	");
		sql.append("			AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID	");
		sql.append("			AND PG.PRODUCT_GROUP_ID = GLP.PRODUCT_GROUP_ID	");
		sql.append("			AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
		sql.append("			AND GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
		sql.append("			AND PG.GROUP_TYPE = 1	");
		sql.append("			AND GLP.HAS_PRODUCT = 1	");
		sql.append("			AND GLC.HAS_PRODUCT = 1	");
		sql.append("			AND PP.STATUS = 1	");
		sql.append("			AND PG.STATUS = 1	");
		sql.append("			AND GLP.STATUS = 1	");
		sql.append("			AND GLC.STATUS = 1	");
		sql.append("			AND GLD.STATUS = 1	");
		sql.append("                         AND pp.promotion_program_id = psm.promotion_program_id ");
		sql.append("                         AND substr(pp.from_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("                         AND Ifnull(substr(pp.to_date, 1, 10) >= ?, 1) ");
		param.add(dateNow);
		sql.append("                         AND pp.status = 1 ");
		sql.append("                         AND psm.status = 1 ");
		sql.append("                         AND psm.shop_id IN ( ");
		sql.append(idListString);
		sql.append(")) AS pro ");
		sql.append("              ON pd.product_id = pro.product_id ");
//		sql.append("              left join media_item  MDI  ");
//		sql.append("              on MDI.object_id = PD.product_id ");
//		sql.append("              AND MDI.object_type = 3 ");
//		sql.append("              AND MDI.media_type = 0	AND MDI.status = 1 ");
//		sql.append("      LEFT JOIN ( ");
//		sql.append("				SELECT mm.object_id, ");
//		sql.append("                         m.media_id ");
//		sql.append("                  FROM   media m, ");
//		sql.append("                         media_map mm, ");
//		sql.append("                         media_item mi ");
//		sql.append("                  WHERE  m.status = 1 ");
//		sql.append("                         AND mm.object_type = 1 ");
//		sql.append("                         AND mm.status = 1 ");
//		sql.append("                         AND mi.status = 1 ");
//		sql.append("                         AND mi.object_type = 7 ");
//		sql.append("                         AND mi.media_type = 1 ");
//		sql.append("                         AND mm.media_id = m.media_id ");
//		sql.append("                         AND m.media_item_id = mi.media_item_id ");
//		sql.append("       ) MDV  ON PD.product_id = MDV.object_id ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND substr(PD.create_date, 1, 10) <= ? ");
		param.add(dateNow);
		sql.append("       AND Ifnull(substr(PD.update_date, 1, 10) <= ?, 1) ");
		param.add(dateNow);

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			sql.append("	and upper(PRODUCT_CODE) like upper(");
			sql.append(productCode);
			sql.append(") escape '^' ");
			sql.append("	or upper(NAME_TEXT) like upper(");
			sql.append(productCode);
			sql.append(") escape '^' ");
		}

//		if (!StringUtil.isNullOrEmpty(productName)) {
//			productName = StringUtil.getEngStringFromUnicodeString(productName);
//			productName = StringUtil.escapeSqlString(productName);
//			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
//			sql.append("	and upper(NAME_TEXT) like upper(");
//			sql.append(productName);
//			sql.append(") escape '^' ");
//		}

		sql.append(" group by pd.product_id ");

		String orderStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "product_code")
				.addMapper(SortActionConstants.NAME, "product_name")
				.addMapper(SortActionConstants.CAT, "CATEGORY_CODE")
				.addMapper(SortActionConstants.SUB_CAT, "sub_category_code")
				.addMapper(SortActionConstants.CONVFACT, "convfact")
				.addMapper(SortActionConstants.AVAILABLE_QUANTITY,
						"AVAILABLE_QUANTITY")
				.addMapper(SortActionConstants.PRICE, "price")
				.addMapper(SortActionConstants.PACKAGE_PRICE, "PACKAGE_PRICE")
				.addMapper(SortActionConstants.IMAGE_COUNT, "(IMAGE + (VIDEO * 2))")
				.defaultOrderString(
						" ORDER BY z desc, fp.value asc, PD.order_index, product_code ASC ")
				.build(sortInfo);
		// add order string
		sql.append(orderStr);

		totalPageSql.append("select COUNT(*) as TOTAL_ROW from (" + sql + ")");

		sql.append(page);

		Cursor c = null;
		Cursor c1 = null;
		try {
			c = rawQueries(sql.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = new ListProductDTO();
					do {
						ProductDTO productInfo = new ProductDTO();
						productInfo.initDataFromCursor(c);
						dto.producList.add(productInfo);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == 1) {
				c1 = rawQueries(totalPageSql.toString(), param);
				if (c1 != null) {
					c1.moveToFirst();
					dto.total = c1.getInt(0);
				}

			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e) {
				}
			}
			if (c1 != null) {
				try {
					c1.close();
				} catch (Exception e) {
				}
			}
		}

		return dto;
	}

	/**
	 * Lay ds sp cua TBHV
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: ListProductDTO
	 * @throws:
	 */
	public ListProductDTO getTBHVProductList(Bundle data) {
		ListProductDTO dto = null;
		String page = data.getString(IntentConstants.INTENT_PAGE);
		String productCode = data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		//String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		// String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		String dateNow = DateUtils.now();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("SELECT PD.product_id        AS PRODUCT_ID, ");
		sqlRequest.append("       PD.product_code      AS PRODUCT_CODE, ");
		sqlRequest.append("       PD.convfact          AS CONVFACT, ");
		sqlRequest.append("       PD.UOM1              AS UOM1, ");
		sqlRequest.append("       PD.UOM2              AS UOM2, ");
		sqlRequest.append("       PD.product_name      AS PRODUCT_NAME, ");
		sqlRequest.append("       PI.product_info_code AS CATEGORY_CODE, ");
		sqlRequest.append("       PD.create_date       AS CREATE_DATE, ");
		sqlRequest.append("       PR.price_id          AS PRICE_ID, ");
		sqlRequest.append("       PR.price             AS PRICE, ");
		sqlRequest.append("       PR.status            AS PRICE_STATUS, ");

		sqlRequest.append("       (case WHEN exists (select media_item_id  ");
		sqlRequest.append("       	from media_item ");
		sqlRequest.append("       	where object_id = PD.product_id ");
		sqlRequest.append("       	and object_type = 3  ");
		sqlRequest.append("       	and media_type = 0 ");
		sqlRequest.append("       	and status = 1) THEN 1 ELSE 0 ");
		sqlRequest.append("       END) as IMAGE, ");
		sqlRequest.append("       (case WHEN exists (select m.MEDIA_ID ");
		sqlRequest.append("       	from media m, media_map mm, media_item mi  ");
		sqlRequest.append("       	where mm.media_id = m.media_id  ");
		sqlRequest.append("       	and m.media_item_id = mi.media_item_id ");
		sqlRequest.append("       	and m.status = 1 ");
		sqlRequest.append("       	and mm.object_type = 1 ");
		sqlRequest.append("       	and mm.object_id = PD.product_id  ");
		sqlRequest.append("       	and mm.status = 1 ");
		sqlRequest.append("       	and mi.status = 1 ");
		sqlRequest.append("       	and mi.object_type = 7 ");
		sqlRequest.append("       	and mi.media_type = 1 ) THEN 1 ELSE 0 ");
		sqlRequest.append("       END) as VIDEO ");

		sqlRequest.append("FROM   product PD ");
		sqlRequest.append("       LEFT JOIN (	");

		//begin lay gia theo cach moi (nhiu gia) - chi lay muc shop
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> arrShopId = null;
		try {
			arrShopId = shopTable.getShopRecursive(shopId);
		} catch (Exception e1) {
			MyLog.e("getTBHVProductList", "fail", e1);
		}
		String strListShop = TextUtils.join(",", arrShopId);
		sqlRequest.append("	SELECT prRight.* FROM(	");
		sqlRequest.append("	SELECT	");
		sqlRequest.append("	    IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(pr_npp,	");
		sqlRequest.append("	    pr_vung),	");
		sqlRequest.append("	    pr_mien),	");
		sqlRequest.append("	    pr_kenh),	");
		sqlRequest.append("	    pr_vnm),	");
		sqlRequest.append("	    pr_vnm_nhomdv) price_id	");
		sqlRequest.append("	FROM	");
		sqlRequest.append("	    (SELECT	");
		sqlRequest.append("	        prc.price_id,	");
		sqlRequest.append("	        prc.PRODUCT_ID PRODUCT_ID,	");
		sqlRequest.append("	        prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	        prc.shop_id,	");
		sqlRequest.append("	        IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	        ''),	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND prc.shop_id IN (?)	");
		param.add(shopId);
		sqlRequest.append("	            AND prc.shop_id IN                  (SELECT	");
		sqlRequest.append("	                shop_id	");
		sqlRequest.append("	            FROM	");
		sqlRequest.append("	                shop	");
		sqlRequest.append("	            WHERE	");
		sqlRequest.append("	                status = 1	");
		sqlRequest.append("	                AND shop_type_id IN                       (SELECT	");
		sqlRequest.append("	                    channel_type_id	");
		sqlRequest.append("	                FROM	");
		sqlRequest.append("	                    channel_type	");
		sqlRequest.append("	                WHERE	");
		sqlRequest.append("	                    TYPE = 1	");
		sqlRequest.append("	                    AND object_type = 3)) THEN price_id	");
		sqlRequest.append("	            END) pr_npp,	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND prc.shop_id IN (	");
		sqlRequest.append(strListShop + ")	");
		sqlRequest.append("	            AND prc.shop_id IN                  (SELECT	");
		sqlRequest.append("	                shop_id	");
		sqlRequest.append("	            FROM	");
		sqlRequest.append("	                shop	");
		sqlRequest.append("	            WHERE	");
		sqlRequest.append("	                status = 1	");
		sqlRequest.append("	                AND shop_type_id IN                       (SELECT	");
		sqlRequest.append("	                    channel_type_id	");
		sqlRequest.append("	                FROM	");
		sqlRequest.append("	                    channel_type	");
		sqlRequest.append("	                WHERE	");
		sqlRequest.append("	                    TYPE = 1	");
		sqlRequest.append("	                    AND object_type = 2)) THEN price_id	");
		sqlRequest.append("	            END) pr_vung,	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND prc.shop_id IN (	");
		sqlRequest.append(strListShop + ")	");
		sqlRequest.append("	            AND prc.shop_id IN                  (SELECT	");
		sqlRequest.append("	                shop_id	");
		sqlRequest.append("	            FROM	");
		sqlRequest.append("	                shop	");
		sqlRequest.append("	            WHERE	");
		sqlRequest.append("	                status = 1	");
		sqlRequest.append("	                AND shop_type_id IN                       (SELECT	");
		sqlRequest.append("	                    channel_type_id	");
		sqlRequest.append("	                FROM	");
		sqlRequest.append("	                    channel_type	");
		sqlRequest.append("	                WHERE	");
		sqlRequest.append("	                    TYPE = 1	");
		sqlRequest.append("	                    AND object_type = 1)) THEN price_id	");
		sqlRequest.append("	            END) pr_mien,	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND prc.shop_id IN (	");
		sqlRequest.append(strListShop + ")	");
		sqlRequest.append("	            AND prc.shop_id IN                  (SELECT	");
		sqlRequest.append("	                shop_id	");
		sqlRequest.append("	            FROM	");
		sqlRequest.append("	                shop	");
		sqlRequest.append("	            WHERE	");
		sqlRequest.append("	                status = 1	");
		sqlRequest.append("	                AND shop_type_id IN                       (SELECT	");
		sqlRequest.append("	                    channel_type_id	");
		sqlRequest.append("	                FROM	");
		sqlRequest.append("	                    channel_type	");
		sqlRequest.append("	                WHERE	");
		sqlRequest.append("	                    TYPE = 1	");
		sqlRequest.append("	                    AND object_type = '0')) THEN price_id	");
		sqlRequest.append("	            END) pr_kenh,	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') <> ''	");
		sqlRequest.append("	            AND PRC.SHOP_CHANNEL = SHOP.SHOP_CHANNEL	");
		sqlRequest.append("	            AND IFNULL(prc.shop_id,	");
		sqlRequest.append("	            '') = '' THEN price_id	");
		sqlRequest.append("	        END) pr_vnm_nhomdv,	");
		sqlRequest.append("	        MAX(CASE	");
		sqlRequest.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sqlRequest.append("	            '') = ''	");
		sqlRequest.append("				AND prc.shop_id IN (	");
		sqlRequest.append(strListShop);
		sqlRequest.append("				)	");
		sqlRequest.append("				AND prc.shop_id IN                  (	");
		sqlRequest.append("	    			SELECT	");
		sqlRequest.append("	        			shop_id	");
		sqlRequest.append("	    			FROM	");
		sqlRequest.append("	        			shop	");
		sqlRequest.append("	    			WHERE	");
		sqlRequest.append("	        			status = 1	");
		sqlRequest.append("	        			AND shop_type_id IN                       (	");
		sqlRequest.append("	            			SELECT	");
		sqlRequest.append("	                			channel_type_id	");
		sqlRequest.append("	            			FROM	");
		sqlRequest.append("	                			channel_type	");
		sqlRequest.append("	            			WHERE	");
		sqlRequest.append("	                			TYPE = 1	");
		sqlRequest.append("	                			AND object_type = 14	");
		sqlRequest.append("	        			)	");
		sqlRequest.append("	    			) THEN price_id	");
		sqlRequest.append("	        END) pr_vnm	");
		sqlRequest.append("	    FROM	");
		sqlRequest.append("	        (SELECT	");
		sqlRequest.append("	            *	");
		sqlRequest.append("	        FROM	");
		sqlRequest.append("	            PRICE	");
		sqlRequest.append("	        WHERE	");
		sqlRequest.append("	            1 = 1	");
		sqlRequest.append("	            AND PRICE.STATUS = 1	");
		sqlRequest.append("	            AND Ifnull(DATE (PRICE.from_date) <= DATE (?), 0)	");
		param.add(dateNow);
		sqlRequest.append("	            AND Ifnull(DATE(PRICE.to_date) >= DATE (?), 1)) prc	");
		param.add(dateNow);
		sqlRequest.append("	    JOIN	");
		sqlRequest.append("	        (	");
		sqlRequest.append("	            SELECT	");
		sqlRequest.append("	                SHOP_CHANNEL	");
		sqlRequest.append("	            FROM	");
		sqlRequest.append("	                SHOP	");
		sqlRequest.append("	            WHERE	");
		sqlRequest.append("	                SHOP_ID = ?	");
		param.add(shopId);
		sqlRequest.append("	        ) shop	");
		sqlRequest.append("	            ON 1=1	");
		sqlRequest.append("	    GROUP BY	");
		sqlRequest.append("	        prc.PRODUCT_ID	");
		sqlRequest.append("	)) pri_id, PRICE prRight	");
		sqlRequest.append("	WHERE 1=1	");
		sqlRequest.append("	and pri_id.price_id = prRight.price_id	");
		//end lay gia theo cach moi (nhiu gia) - chi lay muc shop

		sqlRequest.append("	) PR ON PD.PRODUCT_ID = PR.PRODUCT_ID ");
		sqlRequest.append("       JOIN product_info PI ");
		sqlRequest.append("         ON PI.product_info_id = PD.cat_id ");
		sqlRequest.append("WHERE  PD.status = 1 ");
		sqlRequest.append("       AND PI.status = 1 ");
		sqlRequest.append("       AND PR.status = 1 ");
		sqlRequest.append("       AND PI.type = 1 ");
		//cho phep hien san pham cua tat ca nganh hang, khong phan biet nganh hang Z
		//sqlRequest.append("       AND pi.product_info_code <> 'Z' ");
		sqlRequest.append("       AND Date(PR.from_date) <= Date('now', 'localtime') ");
		sqlRequest.append("       AND Ifnull(Date(PR.to_date) >= Date('now', 'localtime'), 1) ");

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			sqlRequest.append("	and upper(PD.product_code) like upper(");
			sqlRequest.append(productCode);
			sqlRequest.append(") escape '^' ");
		}

		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
			sqlRequest.append("	and upper(PD.name_text) like upper(");
			sqlRequest.append(productName);
			sqlRequest.append(") escape '^' ");
		}

		sqlRequest.append("ORDER  BY product_code ASC ");

		totalPageSql.append("select COUNT(*) as TOTAL_ROW from (" + sqlRequest + ")");

		sqlRequest.append(page);

		String[] paramStr = param.toArray(new String[param.size()]);
		Cursor c = null;
		try {
			c = rawQuery(sqlRequest.toString(), paramStr);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = new ListProductDTO();
					do {
						ProductDTO productInfo = new ProductDTO();
						productInfo.initDataFromCursor(c);
						dto.producList.add(productInfo);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == 1) {
				c = rawQuery(totalPageSql.toString(), paramStr);
				if (c != null) {
					c.moveToFirst();
					if (dto != null) {
						dto.total = c.getInt(0);
					}
				}

			}
		} catch (Exception e) {
			MyLog.e("getTBHVProductList", "fail", e);
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}

		return dto;
	}

	/**
	 * get list industry product and list product for the first industry
	 * @param ext
	 * @return
	 * @return: SaleStatisticsProductInDayInfoViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Oct 20, 2012
	 */
	public SaleStatisticsProductInDayInfoViewDTO getListIndustryProductAndListProduct(Bundle ext) throws Exception {
		SaleStatisticsProductInDayInfoViewDTO result = new SaleStatisticsProductInDayInfoViewDTO();
		result.listIndustry = this.getListIndustryProduct();
		int staffType = ext.getInt(IntentConstants.INTENT_STAFF_TYPE);
		if (result.listIndustry != null && result.listIndustry.size() > 0) {
			if (staffType == 1) {
				// don tong ngay presale
				result.listProduct = this.getListProductPreSaleSold(ext);
			} else if (staffType == 2) {
				// don tong ngay valsale
				result.listProduct = this.getListProductVanSaleSold(ext);
			}
		}
		return result;
	}

	/**
	 *
	 * get list product pre sale sold
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SaleProductInfoDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Oct 20, 2012
	 */
	public ArrayList<SaleProductInfoDTO> getListProductPreSaleSold(Bundle ext) throws Exception {
		ArrayList<SaleProductInfoDTO> result = new ArrayList<SaleProductInfoDTO>();
		String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		boolean isHasProductCode = false;
		boolean isHasProductName = false;
		boolean isHasIndustryProduct = false;
		String productCode = ext.getString(IntentConstants.INTENT_PRODUCT_CODE);
		isHasProductCode = !StringUtil.isNullOrEmpty(productCode);
		String productName = ext.getString(IntentConstants.INTENT_PRODUCT_NAME);
		isHasProductName = !StringUtil.isNullOrEmpty(productName);
		String industryProduct = ext.getString(IntentConstants.INTENT_INDUSTRY);
		DMSSortInfo sort = (DMSSortInfo) ext.getSerializable(IntentConstants.INTENT_SORT_DATA);
		isHasIndustryProduct = !StringUtil.isNullOrEmpty(industryProduct);

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			productCode = productCode.substring(1, productCode.length() - 1);
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
			productName = productName.substring(1, productName.length() - 1);
		}

		ArrayList<String> totalParams = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT p.product_id          PRODUCT_ID, ");
		varname1.append("       p.[product_info_id]   PRODUCT_INFO_ID, ");
		varname1.append("       p.[product_info_name] PRODUCT_INFO_NAME, ");
		varname1.append("       p.[product_info_code] CATEGORY_CODE, ");
		varname1.append("       p.[product_code]      PRODUCT_CODE, ");
		varname1.append("       p.[convfact]          CONVFACT, ");
		varname1.append("       p.[product_name]      PRODUCT_NAME, ");
		varname1.append("       so.amount_total       AMOUNT_TOTAL, ");
		varname1.append("       so.product_number     PRODUCT_NUMBER, ");
		varname1.append("       so.da_duyet           DA_DUYET, ");
		varname1.append("       so.tong_da_duyet      TONG_DA_DUYET, ");
		varname1.append("       so.da_tra             DA_TRA, ");
		varname1.append("       so.tong_da_tra        TONG_DA_TRA ");
		varname1.append("FROM   (SELECT p.product_id, ");
		varname1.append("               pi.[product_info_id], ");
		varname1.append("               pi.[product_info_name], ");
		varname1.append("               pi.[product_info_code], ");
		varname1.append("               p.[product_code], ");
		varname1.append("               p.[convfact], ");
		varname1.append("               p.[product_name], ");
		varname1.append("               p.[name_text], ");
		varname1.append("               p.ORDER_INDEX ");
		varname1.append("        FROM   product p, ");
		varname1.append("               product_info pi ");
		varname1.append("        WHERE  1 = 1 ");
		varname1.append("               AND p.[cat_id] = pi.[product_info_id] ");
		varname1.append("               AND p.status = 1 ");
		varname1.append("               AND pi.status = 1) p ");
		varname1.append("       JOIN (SELECT Sum(case when SO.type IN( 0, 1 ) AND SO.order_type LIKE 'IN' then sod.amount else 0 end)    AMOUNT_TOTAL, ");
		varname1.append("                    Sum (case when SO.type IN( 0, 1 ) AND SO.order_type LIKE 'IN' then sod.quantity else 0 end) PRODUCT_NUMBER, ");
		varname1.append("                    sod.product_id, ");
		varname1.append("                    Sum(CASE ");
		varname1.append("                          WHEN ( SO.type IN( 0, 1 ) ");
		varname1.append("                                 AND SO.approved in (1) ");
		varname1.append("                                 AND SO.order_type LIKE 'IN' ) THEN SOD.quantity ");
		varname1.append("                          ELSE 0 ");
		varname1.append("                        END)           DA_DUYET, ");
		varname1.append("                    Sum(CASE ");
		varname1.append("                          WHEN ( SO.type IN( 0, 1 ) ");
		varname1.append("                                 AND SO.approved in (1) ");
		varname1.append("                                 AND SO.order_type LIKE 'IN' ) THEN SOD.amount ");
		varname1.append("                          ELSE 0 ");
		varname1.append("                        END)           TONG_DA_DUYET, ");
		varname1.append("                    Sum(CASE ");
		varname1.append("                          WHEN ( SO.type = 2 ");
		varname1.append("                                 AND SO.order_type LIKE 'CM' AND SO.approved in (1) ) THEN SOD.quantity ");
		varname1.append("                          ELSE 0 ");
		varname1.append("                        END)           DA_TRA, ");
		varname1.append("                    Sum(CASE ");
		varname1.append("                          WHEN ( SO.type = 2 ");
		varname1.append("                                 AND SO.order_type LIKE 'CM' AND SO.approved in (1) ) THEN SOD.amount ");
		varname1.append("                          ELSE 0 ");
		varname1.append("                        END)           TONG_DA_TRA ");
		varname1.append("             FROM   sale_order so, ");
		varname1.append("                    sale_order_detail sod ");
		varname1.append("             WHERE  1 = 1 ");
		varname1.append("                    AND so.sale_order_id = sod.sale_order_id ");
		varname1.append("                    AND so.type IN ( 0, 1, 2 ) ");
		varname1.append("                    AND so.approved IN ( 0, 1) ");
		varname1.append("                    AND SO.order_type IN ( 'IN', 'CM' ) ");
		varname1.append("                    AND SOD.is_free_item = 0 ");
		varname1.append("                    AND SO.shop_id = ? ");
		totalParams.add(shopId);
		varname1.append("                    AND SO.staff_id = ? ");
		totalParams.add(staffId);
		varname1.append("                    AND Substr(SO.order_date, 1, 10) = ? ");
		totalParams.add(dateNow);
		varname1.append("             GROUP  BY sod.[product_id]) so ");
		varname1.append("         ON so.product_id = p.product_id ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND ( ( da_duyet + tong_da_duyet + da_tra + tong_da_tra ) > 0 ");
		varname1.append("              OR product_number IS NOT NULL ) ");

		if (isHasIndustryProduct) {
			varname1.append("       AND upper(product_info_code) = upper(?) ");
			totalParams.add(industryProduct);
		} else {
			varname1.append("       AND upper(product_info_code) != 'Z' ");
		}

		if (isHasProductCode) {
			varname1.append("       AND upper(product_code) LIKE upper(?) escape '^' ");
			totalParams.add(productCode);
		}
		if (isHasProductName) {
			varname1.append("       AND upper(name_text) LIKE upper(?) escape '^' ");
			totalParams.add(productName);
		}

		String orderStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "PRODUCT_CODE")
				.addMapper(SortActionConstants.NAME, "PRODUCT_NAME")
				.addMapper(SortActionConstants.CAT, "CATEGORY_CODE")
				.addMapper(SortActionConstants.CONVFACT, "CONVFACT")
//				.defaultOrderString(" order by product_code, product_name ")
				.defaultOrderString(" order by ORDER_INDEX, product_code, product_name ")
				.build(sort);
		// add order string
		varname1.append(orderStr);

		String[] params = new String[] {};
		params = totalParams.toArray(new String[totalParams.size()]);

		Cursor c = null;
		try {
			c = rawQuery(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleProductInfoDTO item = new SaleProductInfoDTO();
						item.initDataPreSaleFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return result;
	}

	/**
	 *
	 * get list product van sale sold
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SaleProductInfoDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 17, 2013
	 */
	public ArrayList<SaleProductInfoDTO> getListProductVanSaleSold(Bundle ext) throws Exception{
		ArrayList<SaleProductInfoDTO> result = new ArrayList<SaleProductInfoDTO>();
		String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
		boolean isHasProductCode = false;
		boolean isHasProductName = false;
		boolean isHasIndustryProduct = false;
		String productCode = ext.getString(IntentConstants.INTENT_PRODUCT_CODE);
		isHasProductCode = !StringUtil.isNullOrEmpty(productCode);
		String productName = ext.getString(IntentConstants.INTENT_PRODUCT_NAME);
		isHasProductName = !StringUtil.isNullOrEmpty(productName);
		String industryProduct = ext.getString(IntentConstants.INTENT_INDUSTRY);
		isHasIndustryProduct = !StringUtil.isNullOrEmpty(industryProduct);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		DMSSortInfo sort = (DMSSortInfo) ext.getSerializable(IntentConstants.INTENT_SORT_DATA);

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			productCode = productCode.substring(1, productCode.length() - 1);
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
			productName = productName.substring(1, productName.length() - 1);
		}

		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("SELECT TT.product_id                   PRODUCT_ID, ");
		sqlRequest.append("       TT.price                        PRICE, ");
		sqlRequest.append("       TT.product_code                 PRODUCT_CODE, ");
		sqlRequest.append("       TT.product_name                 PRODUCT_NAME, ");
		sqlRequest.append("       TT.product_name_text            PRODUCT_NAME_TEXT, ");
		sqlRequest.append("       TT.category_code                CATEGORY_CODE, ");
		sqlRequest.append("       TT.convfact                     CONVFACT, ");
		sqlRequest.append("       sum(TT.total_product_remain)         TOTAL_PRODUCT_REMAIN, ");
//		sqlRequest.append("       Sum(TT.total_product)           TOTAL_PRODUCT, ");
//		sqlRequest.append("       Sum(TT.total_amount)            TOTAL_AMOUNT, ");
		sqlRequest.append("       sum(TT.total_product_sold)      TOTAL_PRODUCT_SOLD, ");
		sqlRequest.append("       sum(TT.total_amount_sold)      TOTAL_TOTAL_AMOUNT_SOLD ");
//		sqlRequest.append("       TT.total_product_add_order TOTAL_PRODUCT_ADD_ORDER, ");
//		sqlRequest.append("       TT.total_amount_add_order  TOTAL_AMOUNT_ADD_ORDER, ");
//		sqlRequest.append("       DA_DUYET       DA_DUYET, ");
//		sqlRequest.append("       DA_TRA       DA_TRA ");
		sqlRequest.append("FROM   (SELECT sod.product_id         PRODUCT_ID, ");
		sqlRequest.append("               sod.price              PRICE, ");
		sqlRequest.append("               p.product_code         PRODUCT_CODE, ");
		sqlRequest.append("               p.product_name         PRODUCT_NAME, ");
		sqlRequest.append("               p.name_text    		 PRODUCT_NAME_TEXT, ");
		sqlRequest.append("               pi.product_info_code   CATEGORY_CODE, ");
		sqlRequest.append("               p.convfact             CONVFACT, ");
		sqlRequest.append("               p.ORDER_INDEX			 ORDER_INDEX, ");
		sqlRequest.append("               SO.order_type          ORDER_TYPE, ");
//		sqlRequest.append("               Sum(sod.quantity)      TOTAL_PRODUCT, ");
//		sqlRequest.append("               Sum( ");
//		sqlRequest.append("					case when so.order_type = 'SO' then sod.amount ");
//		sqlRequest.append("     				 when so.order_type = 'CO' then (sod.amount * (-1)) ");
//		sqlRequest.append("     				 else 0 end )        TOTAL_AMOUNT, ");
		sqlRequest.append("               STT.available_quantity TOTAL_PRODUCT_REMAIN, ");
//		sqlRequest.append("               SUM (CASE ");
//		sqlRequest.append("                 WHEN SO.order_type = 'IN' THEN (SOD.quantity) ");
//		sqlRequest.append("                 ELSE 0 ");
//		sqlRequest.append("               END )                    TOTAL_PRODUCT_ADD_ORDER, ");
//		sqlRequest.append("               SUM (CASE ");
//		sqlRequest.append("                 WHEN (SO.type IN( 0, 1 ) ");
//		sqlRequest.append("                       AND SO.approved = 1 ");
//		sqlRequest.append("                       AND SO.order_type LIKE 'IN') THEN (SOD.quantity) ");
//		sqlRequest.append("                 ELSE 0 ");
//		sqlRequest.append("               END )                   DA_DUYET, ");
//		sqlRequest.append("               SUM (CASE ");
//		sqlRequest.append("                 WHEN (SO.type = 2 ");
//		sqlRequest.append("                       AND SO.order_type LIKE 'CM' ) THEN (SOD.quantity) ");
//		sqlRequest.append("                 ELSE 0 ");
//		sqlRequest.append("               END )                    DA_TRA, ");
//		sqlRequest.append("               SUM (CASE ");
//		sqlRequest.append("                 WHEN SO.order_type = 'IN' THEN (SOD.amount) ");
//		sqlRequest.append("                 ELSE 0 ");
//		sqlRequest.append("               END )                    TOTAL_AMOUNT_ADD_ORDER, ");
		sqlRequest.append("               Sum (CASE ");
		sqlRequest.append("                 WHEN SO.order_type = 'SO' THEN (SOD.quantity) ");
		sqlRequest.append("                 ELSE (SOD.quantity)*(-1) ");
		sqlRequest.append("               END  )                  TOTAL_PRODUCT_SOLD, ");
		sqlRequest.append("               SUM (CASE ");
		sqlRequest.append("                 WHEN SO.order_type = 'SO' THEN (SOD.amount) ");
		sqlRequest.append("                 ELSE (SOD.amount)*(-1) ");
		sqlRequest.append("               END )                   TOTAL_AMOUNT_SOLD ");
		sqlRequest.append("        FROM   (SELECT SOD.sale_order_id ,  ");
		sqlRequest.append("                       SOD.product_id,       ");
		sqlRequest.append("                       SOD.quantity,      ");
		sqlRequest.append("                       SOD.amount, ");
		sqlRequest.append("                       SOD.price ");
		sqlRequest.append("                FROM   sale_order_detail SOD ");
		sqlRequest.append("                WHERE  SOD.shop_id = ? ");
		params.add(shopId);
		sqlRequest.append("                       AND SOD.staff_id = ? ");
		params.add(staffId);
		sqlRequest.append("                       AND SOD.IS_FREE_ITEM = 0 ");
		sqlRequest.append("                       AND substr(order_date,1,10) = ? ");
		params.add(dateNow);
		sqlRequest.append("                       AND SOD.price > 0) SOD ");
		sqlRequest.append("               INNER JOIN (SELECT SO.sale_order_id, ");
		sqlRequest.append("                                  SO.order_type, ");
		sqlRequest.append("                                  SO.type, ");
		sqlRequest.append("                                  SO.approved ");
		sqlRequest.append("                           FROM   sale_order SO ");
		sqlRequest.append("                           WHERE  SO.shop_id = ? ");
		params.add(shopId);
		sqlRequest.append("                                  AND SO.staff_id = ? ");
		params.add(staffId);
		sqlRequest.append("                                  AND substr(SO.order_date,1,10) = ? ");
		params.add(dateNow);
		sqlRequest.append("                                  AND ( SO.order_type = 'CO' ");
		sqlRequest.append("                                         OR SO.order_type = 'SO' ) ");
		sqlRequest.append("       							 AND so.approved in (0,1) ");
		sqlRequest.append("       							 AND so.syn_state in (0,2) ");
//		sqlRequest.append("       AND (CASE WHEN SO.APPROVED == 0 THEN substr(SO.ORDER_DATE,1,10) = ? WHEN SO.APPROVED == 1 THEN substr(SO.ORDER_DATE,1,10) <= ? END) ) SO ");
		sqlRequest.append("       ) SO ");
//		params.add(dateNow);
//		params.add(dateNow);
		sqlRequest.append("                       ON SOD.sale_order_id = SO.sale_order_id ");
		sqlRequest.append("               LEFT JOIN ( ");

		//begin doi cau lay ton kho (nhiu kho)
		sqlRequest.append("       SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sqlRequest.append("       FROM stock_total st ");
		sqlRequest.append("       LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sqlRequest.append("       WHERE 1=1 ");
		sqlRequest.append("    		AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		params.add(shopId);
		sqlRequest.append("        	AND st.object_id = ? AND st.status = 1 ");
		params.add(staffId);
		sqlRequest.append("       		AND st.object_type = ? ");
		params.add(String.valueOf(StockTotalDTO.TYPE_VANSALE));
		sqlRequest.append("       group by st.product_id ");
		//end doi cau lay ton kho (nhiu kho)

		sqlRequest.append("                          ) STT ");
		sqlRequest.append("                       ON STT.product_id = SOD.product_id, ");
		sqlRequest.append("               product_info PI, ");
		sqlRequest.append("               product P ");
		sqlRequest.append("        WHERE  1 = 1 ");
		sqlRequest.append("               AND PI.status = 1 ");
		sqlRequest.append("               AND PI.type = 1 ");
		sqlRequest.append("               AND PI.product_info_id = P.cat_id ");
		sqlRequest.append("               AND P.product_id = SOD.product_id ");
		sqlRequest.append("        GROUP  BY sod.product_id, ");
		sqlRequest.append("                  sod.price, ");
		sqlRequest.append("                  p.product_code, ");
		sqlRequest.append("                  p.product_name, ");
		sqlRequest.append("                  p.name_text, ");
		sqlRequest.append("                  pi.product_info_code, ");
		sqlRequest.append("                  p.convfact, ");
		sqlRequest.append("                  SO.order_type, ");
		sqlRequest.append("                  STT.available_quantity) TT where 1 = 1 ");
		if (isHasIndustryProduct) {
			sqlRequest.append("       AND upper(TT.category_code) = upper(?) ");
			params.add(industryProduct);
		}
//		else {
//			sqlRequest.append("       upper(TT.category_code) != 'Z' ");
//		}

		if (isHasProductCode) {
			sqlRequest.append("       AND upper(TT.product_code) LIKE upper(?) escape '^' ");
			params.add(productCode);
		}
		if (isHasProductName) {
			sqlRequest.append("       AND upper(TT.product_name_text) LIKE upper(?) escape '^' ");
			params.add(productName);
		}
		sqlRequest.append("GROUP  BY TT.product_id, ");
		sqlRequest.append("          TT.price, ");
		sqlRequest.append("          TT.product_code, ");
		sqlRequest.append("          TT.product_name, ");
		sqlRequest.append("          TT.product_name_text, ");
		sqlRequest.append("          TT.category_code, ");
		sqlRequest.append("          TT.convfact ");
//		sqlRequest.append("          TT.total_product_remain ");

		String orderStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "PRODUCT_CODE")
		.addMapper(SortActionConstants.NAME, "PRODUCT_NAME")
		.addMapper(SortActionConstants.CAT, "CATEGORY_CODE")
		.addMapper(SortActionConstants.CONVFACT, "CONVFACT")
//		.defaultOrderString(" order by product_code, product_name ")
		.defaultOrderString(" order by ORDER_INDEX, product_code, product_name ")
		.build(sort);
		// add order string
		sqlRequest.append(orderStr);

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleProductInfoDTO item = new SaleProductInfoDTO();
						item.initDataVanSaleFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return result;
	}

	/**
	 *
	 * get list industry for product
	 *
	 * @param staffIdGS
	 * @return
	 * @return: ArrayList<String>
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Oct 20, 2012
	 */
	public ArrayList<String> getListIndustryProduct() throws Exception {
		ArrayList<String> listIndustry = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("SELECT DISTINCT PRODUCT_INFO_CODE ");
		sqlRequest.append("FROM   PRODUCT_INFO ");
		sqlRequest.append("WHERE  1 = 1 ");
		sqlRequest.append("       AND STATUS = 1 ");
		sqlRequest.append("       AND TYPE = 1 ");
		sqlRequest.append("GROUP  BY PRODUCT_INFO_CODE ");
		sqlRequest.append("ORDER  BY PRODUCT_INFO_CODE ");

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						listIndustry.add(CursorUtil.getString(c, PRODUCT_INFO_TABLE.PRODUCT_INFO_CODE));
					} while (c.moveToNext());
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return listIndustry;
	}

	/**
	 *
	 * Mo ta ham
	 *
	 * @author: HieuNH6
	 * @return
	 * @return: SaleStatisticsAccumulateDayDTO
	 * @throws:
	 */
	public SaleStatisticsAccumulateDayDTO getSaleStatisticsAccumulateDayListProduct(Bundle data) throws Exception{
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String productCode = data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String industry = data.getString(IntentConstants.INTENT_INDUSTRY);
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		int sysCalUnApproved = GlobalInfo.getInstance().getSysCalUnapproved();
		int itemOnPage = Constants.NUM_ITEM_PER_PAGE;
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);

		SaleStatisticsAccumulateDayDTO listIndustry = new SaleStatisticsAccumulateDayDTO();
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
			productName = productName.substring(1, productName.length() - 1);
		}
		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			productCode = productCode.substring(1, productCode.length() - 1);
		}
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		ArrayList<String> totalParams = new ArrayList<String>();
		// totalParams.add(shopId);

		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("	SELECT *, (thucHien - keHoach) conLai, (thucHienSanLuong - keHoachSanLuong) conLaiSanLuong FROM ( ");
		sqlRequest.append("SELECT DISTINCT p.product_id                               product_id, ");
		sqlRequest.append("                p.product_code                              maHang, ");
		sqlRequest.append("                p.product_name                              tenMatHang, ");
		sqlRequest.append("                p.order_index                              order_index, ");
		sqlRequest.append("                pi.product_info_code                             nh, ");
		sqlRequest.append("                Sum(Ifnull (TTT.AMOUNT_PLAN, 0)) keHoach, ");
		sqlRequest.append("                Sum(Ifnull (TTT.AMOUNT, 0)) thucHien, ");
		sqlRequest.append("                Sum(Ifnull (TTT.AMOUNT_APPROVED, 0)) duyet, ");
		sqlRequest.append("                Sum(Ifnull (TTT.QUANTITY_PLAN, 0)) keHoachSanLuong, ");
		sqlRequest.append("                Sum(Ifnull (TTT.QUANTITY, 0)) thucHienSanLuong, ");
		sqlRequest.append("                Sum(Ifnull (TTT.QUANTITY_APPROVED, 0)) duyetSanLuong ");
		sqlRequest.append("FROM   product p JOIN product_info pi ON p.cat_id = pi.product_info_id");
		sqlRequest.append("       JOIN (SELECT DISTINCT rpt.product_id, ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlRequest.append(" 	      SUM(rpt.AMOUNT_APPROVED) AMOUNT, ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sqlRequest.append(" 	      SUM(rpt.AMOUNT_PENDING) AMOUNT, ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlRequest.append(" 	      SUM(rpt.AMOUNT) AMOUNT, ");
		}else {
			sqlRequest.append(" 	     SUM(rpt.AMOUNT) AMOUNT, ");
		}
		sqlRequest.append("              Sum(rpt.AMOUNT_PLAN) AMOUNT_PLAN, ");
		sqlRequest.append("                                        Sum(rpt.AMOUNT_APPROVED) AMOUNT_APPROVED, ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlRequest.append(" 	      SUM(rpt.QUANTITY_APPROVED) QUANTITY, ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sqlRequest.append(" 	      SUM(rpt.QUANTITY_PENDING) QUANTITY, ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlRequest.append(" 	      SUM(rpt.QUANTITY) QUANTITY, ");
		}else {
			sqlRequest.append(" 	     SUM(rpt.QUANTITY) QUANTITY, ");
		}
		sqlRequest.append("                                        Sum(rpt.QUANTITY_PLAN) QUANTITY_PLAN, ");
		sqlRequest.append("                                        Sum(rpt.QUANTITY_APPROVED) QUANTITY_APPROVED ");
		sqlRequest.append("                        FROM   rpt_sale_in_month rpt, cycle cy ");
		sqlRequest.append("                        WHERE  1 = 1 ");
		sqlRequest.append("                               AND rpt.object_id = ? ");
		totalParams.add(staffId);
		//loai 1 nhan vien
		sqlRequest.append("                               AND rpt.object_type = 1 ");
		sqlRequest.append("                               AND rpt.status = 1 ");
		sqlRequest.append("                               AND rpt.shop_id = ? ");
		totalParams.add(shopId);
//		sqlRequest.append("                               AND substr(month, 1, 7) = ? ");
//		totalParams.add(monthNow);
		sqlRequest.append("                               AND cy.cycle_id = rpt.cycle_id ");
		sqlRequest.append("                               AND cy.status = 1 ");
		sqlRequest.append("                               AND substr(cy.begin_date,1,10) <= substr(?,1,10) ");
		totalParams.add(dateNow);
		sqlRequest.append("                               AND substr(cy.end_date,1,10) >= substr(?,1,10) ");
		totalParams.add(dateNow);
		sqlRequest.append("                        GROUP  BY rpt.product_id) TTT ");
		sqlRequest.append("                    ON TTT.product_id = p.product_id ");
		sqlRequest.append("WHERE  1 = 1 ");
		sqlRequest.append("       AND p.status = 1 ");
		sqlRequest.append("       AND pi.status = 1 ");
		sqlRequest.append("       AND pi.type = 1 ");

		if (!StringUtil.isNullOrEmpty(industry)) {
			sqlRequest.append("       AND pi.product_info_code = ? ");
			totalParams.add(industry);
		} else {
//			sqlRequest.append("       AND upper(pi.product_info_code) != 'Z' ");
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			sqlRequest.append("       AND upper(p.name_text) LIKE upper(?) ");
			totalParams.add(productName);
		}
		if (!StringUtil.isNullOrEmpty(productCode)) {
			sqlRequest.append("       AND upper(p.product_code) LIKE upper(?) ");
			totalParams.add(productCode);
		}
		sqlRequest.append("	GROUP  BY p.product_id, ");
		sqlRequest.append("          p.product_code, ");
		sqlRequest.append("          p.product_name ");
		sqlRequest.append(" ) ");

		//get count sq1l
		String sqlCount = StringUtil.getCountSql(sqlRequest.toString(), "NUM_ROW");

		String orderStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "maHang")
			.addMapper(SortActionConstants.NAME, "tenMatHang")
			.addMapper(SortActionConstants.CAT, "nh")
			.addMapper(SortActionConstants.AMOUNT_PLAN, "keHoach")
			.addMapper(SortActionConstants.AMOUNT_DONE, "thucHien")
			.addMapper(SortActionConstants.AMOUNT_APPROVED, "duyet")
			.addMapper(SortActionConstants.AMOUNT_REMAIN, "conLai")
			.addMapper(SortActionConstants.QUANITY_PLAN, "keHoachSanLuong")
			.addMapper(SortActionConstants.QUANITY_DONE, "thucHienSanLuong")
			.addMapper(SortActionConstants.QUANITY_APPROVED, "duyetSanLuong")
			.addMapper(SortActionConstants.QUANITY_REMAIN, "conLaiSanLuong")
//			.defaultOrderString(" ORDER  BY nh, maHang, tenMatHang")
			.defaultOrderString(" ORDER  BY order_index, nh, maHang, tenMatHang")
			.build(sortInfo);
		//add order
		sqlRequest.append(orderStr);

		//add limit sql
		//sqlRequest.append(StringUtil.getPagingSql(itemOnPage, page));

		Cursor cTotal = null;
		try {
			cTotal = rawQueries(sqlCount, totalParams);
			if (cTotal != null) {
				if (cTotal.moveToFirst()) {
					int countRow = CursorUtil.getInt(cTotal, "NUM_ROW");
					listIndustry.totalRow = countRow;
				}
			}
		} finally {
			if (cTotal != null) {
				try {
					cTotal.close();
				} catch (Exception e2) {
				}
			}
		}

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), totalParams);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						listIndustry.addItem(c);
					} while (c.moveToNext());
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}
		listIndustry.beginDate = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(),0);
		listIndustry.endDate = new EXCEPTION_DAY_TABLE(mDB).getEndDayOfOffsetCycle(new Date(),0);
		listIndustry.numCycle = new EXCEPTION_DAY_TABLE(mDB).getNumCycle(new Date(),0);

		return listIndustry;
	}

	/**
	 *
	 * Mo ta ham
	 *
	 * @author: HieuNH6
	 * @return
	 * @return: SaleStatisticsAccumulateDayDTO
	 * @throws:
	 */
	public int getCountSaleStatisticsAccumulateDayListProduct(String shopId, String staffId, String productCode,
			String productName, String industry)  throws Exception {
		int countListIndustry = 0;
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = "%" + productName + "%";
		}
		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = "%" + productCode + "%";
		}
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ArrayList<String> totalParams = new ArrayList<String>();

		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("SELECT DISTINCT sp.product_id                               product_id, ");
		sqlRequest.append("                p.product_code                              maHang, ");
		sqlRequest.append("                p.product_name                              tenMatHang, ");
		sqlRequest.append("                pi.product_info_code                         nh, ");
		sqlRequest.append("                Sum(Ifnull (sp.amount, 0)) keHoach, ");
		sqlRequest.append("                Sum(Ifnull (TTT.amount, 0))                 thucHien ");
		sqlRequest.append("FROM   ");
		sqlRequest.append("       sale_plan sp, ");
		sqlRequest.append("       product p join product_info pi on p.cat_id = pi.product_info_id ");
		sqlRequest.append("       LEFT OUTER JOIN (SELECT DISTINCT product_id, ");
		sqlRequest.append("                                        Sum(amount) amount ");
		sqlRequest.append("                        FROM   rpt_sale_in_month ");
		sqlRequest.append("                        WHERE  1 = 1 ");
		sqlRequest.append("                               AND staff_id = ? ");
		totalParams.add(staffId);
		sqlRequest.append("                               AND status = 1 ");
		sqlRequest.append("                               AND shop_id = ? ");
		totalParams.add(shopId);
		sqlRequest.append("                               AND Strftime('%Y/%m', month) = ");
		sqlRequest.append("                                   Strftime('%Y/%m', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("                        GROUP  BY product_id) TTT ");
		sqlRequest.append("                    ON TTT.product_id = sp.product_id ");
		sqlRequest.append("WHERE  1 = 1 ");
		// sqlRequest.append("       AND sp.shop_id = ? ");
		sqlRequest.append("       AND sp.object_id = ? ");
		totalParams.add(staffId);
		sqlRequest.append("       AND sp.object_type = 2 ");
		sqlRequest.append("       AND sp.amount > 0 ");
		sqlRequest.append("       AND sp.type = 2 ");
		sqlRequest.append("       AND sp.month = ");
		sqlRequest.append("           Strftime('%m', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("       AND sp.year = ");
		sqlRequest.append("           Strftime('%Y', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("       AND p.status = 1 ");
		sqlRequest.append("       AND pi.status = 1 ");
		sqlRequest.append("       AND pi.type = 1 ");

		if (!StringUtil.isNullOrEmpty(industry)) {
			sqlRequest.append("       AND pi.product_info_code = ? ");
			totalParams.add(industry);
		} else {
			sqlRequest.append("       AND upper(pi.product_info_code) != 'Z' ");
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			sqlRequest.append("       AND upper(p.name_text) LIKE upper(?) ");
			totalParams.add(productName);
		}
		if (!StringUtil.isNullOrEmpty(productCode)) {
			sqlRequest.append("       AND upper(p.product_code) LIKE upper(?) ");
			totalParams.add(productCode);
		}

		sqlRequest.append("       AND p.product_id = sp.product_id ");
		sqlRequest.append("GROUP  BY p.product_id, ");
		sqlRequest.append("          product_code, ");
		sqlRequest.append("          product_name ");
		sqlRequest.append("UNION ALL ");

		sqlRequest.append("SELECT DISTINCT sim.product_id  product_id, ");
		sqlRequest.append("                p.product_code  maHang, ");
		sqlRequest.append("                p.product_name  tenMatHang, ");
		sqlRequest.append("                pi.product_info_code         nh, ");
		sqlRequest.append("                0               keHoach, ");
		sqlRequest.append("                Sum(sim.amount) thucHien ");
		sqlRequest.append("FROM   rpt_sale_in_month sim, ");
		sqlRequest.append("       product p join product_info pi on p.cat_id = pi.product_info_id ");
		sqlRequest.append("WHERE  1 = 1 ");
		sqlRequest.append("       AND sim.staff_id = ? ");
		totalParams.add(staffId);
		sqlRequest.append("       AND sim.status = 1 ");
		sqlRequest.append("       AND sim.shop_id = ? ");
		totalParams.add(shopId);
		sqlRequest.append("       AND Strftime('%Y/%m', sim.month) = Strftime('%Y/%m', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("       AND p.status = 1 ");
		sqlRequest.append("       AND pi.status = 1 ");
		sqlRequest.append("       AND pi.type = 1 ");
		sqlRequest.append("       AND p.product_id = sim.product_id ");
		if (!StringUtil.isNullOrEmpty(industry)) {
			sqlRequest.append("       AND pi.product_info_code = ? ");
			totalParams.add(industry);
		} else {
			sqlRequest.append("       AND upper(pi.product_info_code) != 'Z' ");
		}

		sqlRequest.append("       AND NOT EXISTS (SELECT DISTINCT product_id ");
		sqlRequest.append("                                  FROM   sale_plan sp ");
		sqlRequest.append("                                  WHERE  1 = 1 ");
		sqlRequest.append("                                         AND shop_id = ? ");
		totalParams.add(shopId);
		sqlRequest.append("                                         AND staff_id = ? ");
		totalParams.add(staffId);
		sqlRequest.append("       AND p.product_id = sp.product_id ");
		sqlRequest.append("                                         AND sp.month = ");
		sqlRequest.append("                                             Strftime( ");
		sqlRequest.append("                                             '%m', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("                                         AND sp.year = ");
		sqlRequest.append("                                             Strftime( ");
		sqlRequest.append("                                             '%Y', ?) ");
		// *
		totalParams.add(dateNow);
		sqlRequest.append("                                 ) ");
		if (!StringUtil.isNullOrEmpty(productName)) {
			sqlRequest.append("       AND upper(p.name_text) LIKE upper(?) ");
			totalParams.add(productName);
		}
		if (!StringUtil.isNullOrEmpty(productCode)) {
			sqlRequest.append("       AND upper(p.product_code) LIKE upper(?) ");
			totalParams.add(productCode);
		}
		sqlRequest.append("GROUP  BY p.product_id, ");
		sqlRequest.append("          product_code, ");
		sqlRequest.append("          product_name ");

		Cursor c = null;
		try {
			c = rawQuery(sqlRequest.toString(), totalParams.toArray(new String[totalParams.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					countListIndustry = c.getCount();
				}
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return countListIndustry;
	}

	/**
	 *
	 * Mo ta ham
	 *
	 * @author: HieuNH6
	 * @return
	 * @return: SaleStatisticsAccumulateDayDTO
	 * @throws Exception
	 * @throws:
	 */
	public ArrayList<String> getSaleStatisticsAccumulateDay(String shopId, String staffId, String productCode,
			String productName, String industry, String page) throws Exception {
		return getListIndustryProduct();
	}

	/**
	 *
	 * get forcus product info view
	 *
	 * @param data
	 * @return
	 * @return: NVBHReportForcusProductInfoViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Jan 2, 2013
	 */
	public NVBHReportForcusProductInfoViewDTO getForcusProductInfoView(
			Bundle data) throws Exception {
		NVBHReportForcusProductInfoViewDTO result = new NVBHReportForcusProductInfoViewDTO();
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		String strListShop = TextUtils.join(",", listShopId);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String saleTypeCode = data
				.getString(IntentConstants.INTENT_SALE_TYPE_CODE);
		ArrayList<String> totalParams = new ArrayList<String>();
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		DMSSortInfo sort = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT FCMP.PRODUCT_ID PRODUCT_ID, ");
		sqlQuery.append("       P.PRODUCT_CODE PRODUCT_CODE, ");
		sqlQuery.append("       P.PRODUCT_NAME PRODUCT_NAME, ");
		sqlQuery.append("       PI.PRODUCT_INFO_NAME PRODUCT_INFO_NAME, ");
		sqlQuery.append("       PI.PRODUCT_INFO_CODE PRODUCT_INFO_CODE, ");
		sqlQuery.append("       SOD.AMOUNT_PLAN  AMOUNT_PLAN, ");
		sqlQuery.append("       SOD.AMOUNT_APPROVED  AMOUNT_APPROVED, ");
		sqlQuery.append("       SOD.AMOUNT_PENDING  AMOUNT_PENDING, ");
		sqlQuery.append("       SOD.AMOUNT AMOUNT, ");
		sqlQuery.append("       SOD.QUANTITY_PLAN  QUANTITY_PLAN, ");
		sqlQuery.append("       SOD.QUANTITY_APPROVED  QUANTITY_APPROVED, ");
		sqlQuery.append("       SOD.QUANTITY_PENDING  QUANTITY_PENDING, ");
		sqlQuery.append("       SOD.QUANTITY QUANTITY, ");
		sqlQuery.append("       FCMP.TYPE TYPE_PRODUCT_FOCUS ");
		sqlQuery.append("FROM   FOCUS_CHANNEL_MAP FCM, ");
		sqlQuery.append("       FOCUS_SHOP_MAP FSM, ");
		sqlQuery.append("       FOCUS_PROGRAM FP, ");
		sqlQuery.append("       PRODUCT P, ");
		sqlQuery.append("       PRODUCT_INFO PI, ");
		sqlQuery.append("       AP_PARAM AP, ");
		sqlQuery.append("       FOCUS_CHANNEL_MAP_PRODUCT FCMP ");
		sqlQuery.append("       LEFT JOIN ( ");
		sqlQuery.append("SELECT  		 product_id  PRODUCT_ID, ");
		sqlQuery.append("                AMOUNT AMOUNT, ");
		sqlQuery.append("                AMOUNT_PLAN AMOUNT_PLAN, ");
		sqlQuery.append("                AMOUNT_APPROVED AMOUNT_APPROVED, ");
		sqlQuery.append("                AMOUNT_PENDING AMOUNT_PENDING, ");
		sqlQuery.append("                QUANTITY QUANTITY, ");
		sqlQuery.append("                QUANTITY_PLAN QUANTITY_PLAN, ");
		sqlQuery.append("                QUANTITY_APPROVED QUANTITY_APPROVED, ");
		sqlQuery.append("                QUANTITY_PENDING QUANTITY_PENDING ");
		sqlQuery.append("FROM   rpt_sale_in_month RSIM, cycle cy ");
		sqlQuery.append("WHERE  RSIM.shop_id = ? ");
		totalParams.add(shopId);
		sqlQuery.append("       AND RSIM.object_id = ? ");
		totalParams.add(staffId);
		sqlQuery.append("       AND RSIM.cycle_id = cy.cycle_id ");
		sqlQuery.append("       AND RSIM.cycle_id = ? ");
		totalParams.add(String.valueOf(cycleId));
//		sqlQuery.append("       AND substr(cy.begin_date, 1, 10) >= substr(?, 1, 10) ");
//		totalParams.add(dateNow);
//		sqlQuery.append("       AND substr(cy.end_date, 1, 10) <= substr(?, 1, 10) ");
//		totalParams.add(dateNow);
//		sqlQuery.append("       AND Strftime('%Y/%m', cy.begin_date) = ");
//		sqlQuery.append("           Strftime('%Y/%m', ?) ");
//		totalParams.add(startOfMonth);
		sqlQuery.append("       AND RSIM.status = 1 ");
		sqlQuery.append("GROUP  BY RSIM.product_id ");

		sqlQuery.append(") SOD ");
		sqlQuery.append("              ON SOD.PRODUCT_ID = FCMP.PRODUCT_ID ");
		sqlQuery.append("WHERE  FP.STATUS = 1 ");
		sqlQuery.append("       AND substr(FP.FROM_DATE, 1, 10) <= ? ");
		totalParams.add(dateNow);
		sqlQuery.append("       AND Ifnull (substr(FP.TO_DATE, 1, 10) >= ?, 1) ");
		totalParams.add(dateNow);
		sqlQuery.append("       AND FP.FOCUS_PROGRAM_ID = FCM.FOCUS_PROGRAM_ID ");
		sqlQuery.append("       AND FCM.SALE_TYPE_CODE = ? ");
		totalParams.add(saleTypeCode);
		sqlQuery.append("       AND FCM.STATUS = 1 ");
		// sqlQuery.append("       AND Date(FCM.FROM_DATE) <= Date('NOW', 'LOCALTIME') ");
		// sqlQuery.append("       AND Ifnull (Date(FCM.TO_DATE) >= Date('NOW', 'LOCALTIME'), 1) ");
		sqlQuery.append("       AND FP.FOCUS_PROGRAM_ID = FSM.FOCUS_PROGRAM_ID ");
		sqlQuery.append("       AND FSM.SHOP_ID IN ( ");
		sqlQuery.append(strListShop + ") ");
		sqlQuery.append("       AND FSM.STATUS = 1 ");
		sqlQuery.append("       AND FCMP.FOCUS_CHANNEL_MAP_ID = FCM.FOCUS_CHANNEL_MAP_ID ");
		sqlQuery.append("       AND P.PRODUCT_ID = FCMP.PRODUCT_ID ");
		sqlQuery.append("       AND P.STATUS = 1 ");
		sqlQuery.append("       AND P.CAT_ID = PI.PRODUCT_INFO_ID ");
		sqlQuery.append("       AND PI.STATUS = 1 ");
		sqlQuery.append("       AND PI.TYPE = 1 ");
		sqlQuery.append("       AND AP.TYPE = 'FOCUS_PRODUCT_TYPE' ");
		sqlQuery.append("       AND AP.AP_PARAM_CODE = FCMP.TYPE ");

//		String defaultOrderByStr = "ORDER BY  FCMP.TYPE ASC, P.ORDER_INDEX,P.PRODUCT_CODE, P.PRODUCT_NAME";
		String defaultOrderByStr = "ORDER BY  AP.VALUE ASC, P.ORDER_INDEX,P.PRODUCT_CODE, P.PRODUCT_NAME";
		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "PRODUCT_CODE")
		.addMapper(SortActionConstants.NAME, "PRODUCT_NAME")
		.addMapper(SortActionConstants.CAT, "PRODUCT_INFO_CODE")
		.addMapper(SortActionConstants.TYPE, "TYPE_PRODUCT_FOCUS")
		.defaultOrderString(defaultOrderByStr)
		.build(sort);

		//add order string
		sqlQuery.append(orderByStr);

		Cursor c = null;
		String[] params = new String[] {};
		params = totalParams.toArray(new String[totalParams.size()]);
		try {
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					ForcusProductOfNVBHDTO item = new ForcusProductOfNVBHDTO();
					item.initDateWithCursor(c);
					result.listForcusProduct.add(item);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		// get list type forcus product
		ArrayList<String> listForcusType = new ArrayList<String>();
		for (int i = 0, size = result.listForcusProduct.size(); i < size; i++) {
			ForcusProductOfNVBHDTO dto = result.listForcusProduct.get(i);
			String currentType = String.valueOf(dto.typeProductFocus);
			if (i == 0 || !this.isExistListType(listForcusType, currentType)) {
				listForcusType.add(currentType);
			}
		}

		for (int i = 0, size = listForcusType.size(); i < size; i++) {
			String type = listForcusType.get(i);
			ForcusProductOfNVBHDTO totalType = new ForcusProductOfNVBHDTO();
			// totalType.typeProductFocus = StringUtil
			// .getString(R.string.TEXT_HEADER_MENU_MHTT) + type;
			totalType.typeProductFocus = type;
			double amountPlan = 0;
			double amount = 0;
			double amountRemain = 0;
			double amountProgress = 0;
			long quantityPlan = 0;
			long quantity = 0;
			long quantityRemain = 0;
			double quantityProgress = 0;
			for (int j = 0, size2 = result.listForcusProduct.size(); j < size2; j++) {
				ForcusProductOfNVBHDTO dto = result.listForcusProduct.get(j);
				if (type.equals(dto.typeProductFocus)) {
					amountPlan += dto.amountPlan;
					amount += dto.amount;
					quantityPlan += dto.quantityPlan;
					quantity += dto.quantity;
				}
			}
			amountRemain = (amountPlan - amount) >= 0 ? (amountPlan - amount)
					: 0;
			amountRemain = StringUtil.calRemainUsingRound(amountPlan, amount);
			amountProgress = StringUtil.calPercentUsingRound(amountPlan, amount);
			quantityRemain = StringUtil.calRemain(quantityPlan, quantity);
			quantityProgress = StringUtil.calPercentUsingRound(quantityPlan, quantity);
			totalType.amountPlan = amountPlan;
			totalType.amount = amount;
			totalType.amountRemain = amountRemain;
			totalType.amountProgress = amountProgress;
			totalType.quantityPlan = quantityPlan;
			totalType.quantity = quantity;
			totalType.quantityRemain = quantityRemain;
			totalType.quantityProgress = quantityProgress;

			result.listTotalForcusProduct.add(totalType);
		}

		// number fullDate plan sale in month
		result.numberDayPlan = new EXCEPTION_DAY_TABLE(mDB)
				.getPlanWorkingDaysOfMonth(new Date(), shopId);
		// number fullDate sold in month
		result.numberDaySold = new EXCEPTION_DAY_TABLE(mDB)
				.getCurrentWorkingDaysOfMonth(shopId);
		result.progressSold = StringUtil.calPercentUsingRound(result.numberDayPlan, result.numberDaySold);

		return result;
	}

	/**
	 * check type forcus exist in list
	 *
	 * @param listType
	 * @param typeNeedCheck
	 * @return
	 * @return: boolean
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public boolean isExistListType(ArrayList<String> listType, String typeNeedCheck) {
		boolean kq = false;
		if (!StringUtil.isNullOrEmpty(typeNeedCheck)) {
			for (int i = 0, size = listType.size(); i < size; i++) {
				if (typeNeedCheck.equals(listType.get(i))) {
					kq = true;
					break;
				}
			}
		}
		return kq;
	}

	public ListProductDTO getProductListEx() throws Exception {
		int isGetTotalPage = 1;
		int currentPage = 1;
		int numItemOnPage = 20;

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();
		sql.append("select PRODUCT_ID, PRODUCT_CODE, PRODUCT_NAME from PRODUCT");

		final ListProductDTO dto = new ListProductDTO();
		String sqlStr = sql.toString();

		//lay tong so dong san pham
		if (isGetTotalPage == 1) {
			dto.total = cursorRunner.queryCount(sqlStr, param);
		}

		//lay ds san pham o trang currentPage, moi trang co numItemOnPage san pham
		cursorRunner.query(sqlStr, param, currentPage, numItemOnPage, new ICursorActionQuery() {

			@Override
			public void onMoveCursor(DMSCursor c) throws Exception {
				ProductDTO productInfo = new ProductDTO();
				productInfo.initDataFromCursor(c.cursor);
				dto.producList.add(productInfo);
			}
		});
		return dto;
	}
}
