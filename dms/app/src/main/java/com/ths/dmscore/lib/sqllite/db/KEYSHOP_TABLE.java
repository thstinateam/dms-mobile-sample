package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.KSCusProductRewardDTO;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.db.KeyShopListDTOView;
import com.ths.dmscore.dto.view.KeyShopOrderDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.dto.view.ReportKeyshopDTO;
import com.ths.dmscore.dto.view.ReportKeyshopItemDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;

/**
 * table cho key shop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KEYSHOP_TABLE extends ABSTRACT_TABLE {


	public static final String KS_ID = "KS_ID";
	public static final String KS_CODE = "KS_CODE";
	public static final String NAME = "NAME";
	public static final String FROM_CYCLE_ID = "FROM_CYCLE_ID";
	public static final String TO_CYCLE_ID = "TO_CYCLE_ID";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String MIN_PHOTO_NUM = "MIN_PHOTO_NUM";
	public static final String TABLE_NAME = "KS";

	public KEYSHOP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { KS_ID, KS_CODE, NAME,
				FROM_CYCLE_ID, TO_CYCLE_ID, STATUS, CREATE_DATE, CREATE_USER, UPDATE_DATE,
				UPDATE_USER };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	 /**
	 * Lay ds keyshop
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public ArrayList<KeyShopDTO> getListKeyShop(Bundle b) throws Exception{
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor c = null;
		ArrayList<KeyShopDTO> lstKeyShop = new ArrayList<KeyShopDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    KS.KS_CODE,	");
		sqlObject.append("	    KS.NAME,	");
		sqlObject.append("	    KS.MIN_PHOTO_NUM,	");
		sqlObject.append("	    KS.KS_ID	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS ,	");
		sqlObject.append("	    KS_CUSTOMER KSC,	");
		sqlObject.append("	    CYCLE CY	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    KSC.CUSTOMER_ID = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	    AND KSC.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSC.KS_ID = KS.KS_ID	");
		sqlObject.append("	    AND KSC.STATUS = ?	");
//		sqlObject.append("	    AND KSC.APPROVAL = ?	");
		paramsObject.add(""+ KSCustomerDTO.STATE_ACTIVE);
		sqlObject.append("	    AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		sqlObject.append("	    AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
		sqlObject.append("      AND CY.CYCLE_ID = KSC.CYCLE_ID ");
		sqlObject.append("      AND CY.STATUS = 1 ");
		sqlObject.append("      AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10) ");
		paramsObject.add(dateNow);
		sqlObject.append("      AND SUBSTR(CY.END_DATE,1,10) >= SUBSTR(?,1,10) ");
		paramsObject.add(dateNow);
		sqlObject.append("   ORDER BY NAME, KS_CODE ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KeyShopDTO item = new KeyShopDTO();
						item.initDataFromCursor(c);
						lstKeyShop.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;

	}

	/**
	 * Lay ds keyshop duoc phan bo cho shop
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	 */
	public ArrayList<KeyShopDTO> getAllListKeyShop(Bundle b) throws Exception {
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> lstShop = new SHOP_TABLE(mDB).getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		Cursor c = null;
		ArrayList<KeyShopDTO> lstKeyShop = new ArrayList<KeyShopDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    KS.KS_CODE,	");
		sqlObject.append("	    KS.NAME,	");
		sqlObject.append("	    KS.KS_ID,	");
		sqlObject.append("	    KS.FROM_CYCLE_ID,	");
		sqlObject.append("	    KS.TO_CYCLE_ID	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS ,	");
		sqlObject.append("	    KS_SHOP_MAP KSM,	");
		sqlObject.append("	    CYCLE CY	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND KSM.SHOP_ID IN (	");
		sqlObject.append(idShopList);
		sqlObject.append("      )");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSM.KS_ID = KS.KS_ID	");
		sqlObject.append("	    AND KSM.STATUS = 1	");
		sqlObject.append("	    AND CY.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		sqlObject.append("	    AND CY.CYCLE_ID <= KS.TO_CYCLE_ID	");
		sqlObject.append("      AND CY.STATUS = 1 ");
		sqlObject.append("      AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10) ");
		paramsObject.add(dateNow);
		sqlObject.append("      AND SUBSTR(CY.END_DATE,1,10) >= SUBSTR(?,1,10) ");
		paramsObject.add(dateNow);
		sqlObject.append("   ORDER BY NAME, KS_CODE ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KeyShopDTO item = new KeyShopDTO();
						item.initDataFromCursor(c);
						lstKeyShop.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;

	}

	/**
	 * Lay cac muc cua keyshop ma khach hang tham gia
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	 */
	public ArrayList<RegisterKeyShopItemDTO> getLevelOfKeyShop(Bundle b) throws Exception {
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		long ksID = b.getLong(IntentConstants.INTENT_KEYSHOP_ID);
		String cycleId = b.getString(IntentConstants.INTENT_CYCLE_ID);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor c = null;
		ArrayList<String> lstShop = new SHOP_TABLE(mDB).getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		ArrayList<RegisterKeyShopItemDTO> lstKeyShop = new ArrayList<RegisterKeyShopItemDTO>();
		// cycleId hien tai
		long cycleIdNow = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		boolean registerApprovedKeyshop = b.getInt(IntentConstants.INTENT_REGISTER_APPROVED_KEYSHOP)==1?true:false;

		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	     KS.NAME NAME,	");
		sqlObject.append("	     KS.AMOUNT AMOUNT,	");
		sqlObject.append("	     KS.QUANTITY QUANTITY,	");
		sqlObject.append("	     KS.KS_LEVEL_CODE KS_LEVEL_CODE,	");
		sqlObject.append("	     KS.KS_ID KS_ID,	");
		sqlObject.append("	     KS.SHOP_ID SHOP_ID,	");
		sqlObject.append("	     KS.KS_LEVEL_ID KS_LEVEL_ID,	");
		sqlObject.append("	     KSC.KS_CUSTOMER_ID KS_CUSTOMER_ID,	");
		sqlObject.append("	     KSC.STATUS STATUS,	");
		sqlObject.append("	     KCL.KS_CUSTOMER_LEVEL_ID KS_CUSTOMER_LEVEL_ID,	");
		sqlObject.append("	     KCL.MULTIPLIER MULTIPLIER,	");
		paramsObject.add("" + cycleIdNow);
		if(registerApprovedKeyshop){
			sqlObject.append("	     IFNULL(CASE WHEN KS.CYCLE_ID < ? or (ksc.cycle_id = ks.cycle_id and KSC.STATUS = ?) or (ksc.reward_type = ? or ksc.reward_type = ?) THEN 0 else 1 end 	");
			sqlObject.append("	     ,1) IS_EDIT	");
			paramsObject.add(""+KSCustomerDTO.STATE_NEW);
			paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_PART_PAY);
			paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_ALL_PAY);
		}else{
			sqlObject.append("	     IFNULL(CASE WHEN KS.CYCLE_ID < ? or (ksc.cycle_id = ks.cycle_id and (KSC.STATUS = ? or KSC.STATUS = ?)) or (ksc.reward_type = ? or ksc.reward_type = ?) THEN 0 else 1 end 	");
			sqlObject.append("	     ,1) IS_EDIT	");
			paramsObject.add(""+KSCustomerDTO.STATE_NEW);
			paramsObject.add(""+KSCustomerDTO.STATE_ACTIVE);
			paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_PART_PAY);
			paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_ALL_PAY);
		}
		sqlObject.append("	FROM	");
		sqlObject.append("	    (SELECT	");
		sqlObject.append("	        KSL.NAME,	");
		sqlObject.append("	        KSL.AMOUNT,	");
		sqlObject.append("	        KSL.QUANTITY,	");
		sqlObject.append("	        KSL.KS_LEVEL_CODE,	");
		sqlObject.append("	        KSL.KS_ID,	");
		sqlObject.append("	        KS.FROM_CYCLE_ID,	");
		sqlObject.append("	        KS.TO_CYCLE_ID,	");
		sqlObject.append("	        KSL.KS_LEVEL_ID,	");
		sqlObject.append("	        CY.CYCLE_ID,	");
		sqlObject.append("	        KSM.SHOP_ID	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        KS,	");
		sqlObject.append("	        KS_LEVEL KSL,	");
		sqlObject.append("	        KS_SHOP_MAP KSM,	");
		sqlObject.append("	        CYCLE CY	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        1                         = 1	");
		sqlObject.append("	        AND KSM.SHOP_ID                IN (	");
		sqlObject.append(idShopList);
		sqlObject.append("	        )	");
		sqlObject.append("	        AND KSL.STATUS                   = 1	");
		sqlObject.append("	        AND KS.STATUS                   = 1	");
		sqlObject.append("	        AND KSM.KS_ID                   = KS.KS_ID	");
		sqlObject.append("	        AND KSM.STATUS                  = 1	");
		sqlObject.append("	        AND KS.KS_ID                    = KSL.KS_ID	");
		sqlObject.append("	        AND KS.KS_ID                    = ?");
		paramsObject.add(""+ksID);
		sqlObject.append("	        AND CY.CYCLE_ID                >= KS.FROM_CYCLE_ID	");
		if (!StringUtil.isNullOrEmpty(cycleId)) {
			sqlObject.append("	        AND CY.CYCLE_ID                = ?	");
			paramsObject.add("" + cycleId);
		}else{
			sqlObject.append("	        AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10)	");
			paramsObject.add(dateNow);
			sqlObject.append("	        AND SUBSTR(CY.END_DATE,1,10)   >= SUBSTR(?,1,10) 	");
			paramsObject.add(dateNow);
		}
		sqlObject.append("	        AND CY.CYCLE_ID                <= KS.TO_CYCLE_ID	");
		sqlObject.append("	        AND CY.STATUS                   = 1	");
		sqlObject.append("	         ) KS	");
		sqlObject.append("	LEFT JOIN	");
		sqlObject.append("	    KS_CUSTOMER KSC	");
		sqlObject.append("	        ON KSC.KS_ID      = KS.KS_ID	");
//		sqlObject.append("	        AND KSC.STATUS    = 1	");
		sqlObject.append("	        AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		sqlObject.append("	        AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
		sqlObject.append("	        AND KSC.CYCLE_ID = KS.CYCLE_ID	");
		sqlObject.append("	        AND KSC.CUSTOMER_ID = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	        AND KSC.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	LEFT JOIN	");
		sqlObject.append("	    KS_CUSTOMER_LEVEL KCL	");
		sqlObject.append("	        ON KCL.KS_CUSTOMER_ID = KSC.KS_CUSTOMER_ID	");
		sqlObject.append("	        AND KCL.STATUS = 1	");
		sqlObject.append("	        AND KCL.KS_LEVEL_ID = KS.KS_LEVEL_ID	");
		sqlObject.append("	WHERE 1 = 1	");
//		sqlObject.append("  and case when ksc.ks_customer_id is not null then ksc.cycle_id = ks.cycle_id else 1 end ");
		sqlObject.append("   ORDER BY AMOUNT desc, QUANTITY desc,NAME ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						RegisterKeyShopItemDTO item = new RegisterKeyShopItemDTO();
						item.initDataFromCursor(c);
						lstKeyShop.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;

	}

	/**
	 * Danh sach cau lac bo role GSBH
	 * @author: yennth16
	 * @since: 13:57:29 10-07-2015
	 * @return: KeyShopListDTOView
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public KeyShopListDTOView getKeyShopList(Bundle b) throws Exception{
		KeyShopListDTOView listKeyshop = new KeyShopListDTOView();
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		Cursor c = null;
		ArrayList<String> lstShop = new SHOP_TABLE(mDB).getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		boolean checkPagging = b.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		DMSSortInfo sortInfo = (DMSSortInfo) b.getSerializable(IntentConstants.INTENT_SORT_DATA);
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sqlObject.append("SELECT ");
		sqlObject.append("    ks.KS_ID      						KS_ID, ");
		sqlObject.append("    ks.KS_CODE    						KS_CODE, ");
		sqlObject.append("    ks.NAME       						NAME, ");
		sqlObject.append("    ks.[DESCRIPTION] 						DESCRIPTION, ");
		sqlObject.append("    c.CYCLE_NAME  						FROM_CYCLE_ID, ");
		sqlObject.append("    c1.CYCLE_NAME    						TO_CYCLE_ID ");
		sqlObject.append("FROM ");
		sqlObject.append("( ");
		sqlObject.append("SELECT ");
		sqlObject.append("    DISTINCT ");
		sqlObject.append("    ks.KS_ID      KS_ID, ");
		sqlObject.append("    ks.KS_CODE    KS_CODE, ");
		sqlObject.append("    ks.NAME       NAME, ");
		sqlObject.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		sqlObject.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		sqlObject.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		sqlObject.append("FROM ");
		sqlObject.append("    ks ks        , ");
		sqlObject.append("    ks_shop_map ksm ");
		sqlObject.append("WHERE ");
		sqlObject.append("    1 = 1 ");
		sqlObject.append("    AND ks.status = 1 ");
		sqlObject.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		sqlObject.append("    AND  ? <= ks.TO_CYCLE_ID ");
		params.add(""+cycleId);
		sqlObject.append("    AND ksm.status = 1 ");
		sqlObject.append("    AND ks.KS_ID = ksm.KS_ID ");
		sqlObject.append(" 	   and ksm.SHOP_ID IN ");
		sqlObject.append("       (").append(idShopList).append(")");
		sqlObject.append(" ");
		sqlObject.append(")  ks ");
		sqlObject.append("left join ");
		sqlObject.append("     cycle c on c.[CYCLE_ID] = ks.FROM_CYCLE_ID ");
		sqlObject.append("left join ");
		sqlObject.append("     cycle c1 on c1.[CYCLE_ID] = ks.TO_CYCLE_ID ");


		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
				defaultOrderByStr.append(" ORDER BY ks.KS_CODE ");
		String orderByStr = new DMSSortQueryBuilder()
					.addMapper(SortActionConstants.CODE, " ks.KS_CODE ")
					.addMapper(SortActionConstants.NAME, " ks.NAME ")
					.addMapper(SortActionConstants.FROM_DATE, " c.CYCLE_NAME ")
					.addMapper(SortActionConstants.TO_DATE, " c1.CYCLE_NAME ")
					.defaultOrderString(defaultOrderByStr.toString())
					.build(sortInfo);

		//add order string
		sqlObject.append(orderByStr);

		Cursor cTmp = null;
		String getCountFollowProblemList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ sqlObject + ") ";
		try {
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KeyShopItemDTO item = new KeyShopItemDTO();
						item.initData(c);
						item.productInfo = getProductKS(item.ksId);
						item.ksLevelItem = getLevelKS(item.ksId);
						listKeyshop.arrItem.add(item);
					} while (c.moveToNext());
				}
			}
			if (!checkPagging) {
				cTmp = rawQuery(getCountFollowProblemList, params.toArray(new String[params.size()]));
				if (cTmp != null) {
					cTmp.moveToFirst();
					listKeyshop.total = cTmp.getInt(0);
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;

	}

	/**
	 * Lay danh sach muc chuong trinh
	 * @author: yennth16
	 * @since: 11:11:30 13-07-2015
	 * @return: KSLevelDTO
	 * @throws:
	 * @param ksId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<KSLevelDTO> getLevelKS(long ksId) throws Exception{
		ArrayList<KSLevelDTO> listKeyshop = new ArrayList<KSLevelDTO>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT ");
		varname1.append("       KSL.[KS_LEVEL_CODE]  KS_LEVEL_CODE, ");
		varname1.append("       KSL.[NAME]           NAME, ");
		varname1.append("       KSL.[AMOUNT]         AMOUNT, ");
		varname1.append("       KSL.[QUANTITY]       QUANTITY ");
		varname1.append("FROM KS_LEVEL KSL ");
		varname1.append("WHERE 1=1 ");
		varname1.append("      AND KSL.KS_ID = ? ");
		params.add(Constants.STR_BLANK + ksId);
		varname1.append("      AND KSL.STATUS = 1 ");
		varname1.append("ORDER BY ");
		varname1.append("    KSL.AMOUNT desc, KSL.KS_LEVEL_CODE");
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KSLevelDTO ksl = new KSLevelDTO();
						ksl.initDataFromCursor(c);
						listKeyshop.add(ksl);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;

	}

	/**
	 * Lay thong san pham theo key shop
	 * @author: yennth16
	 * @since: 11:11:30 13-07-2015
	 * @return: KSLevelDTO
	 * @throws:
	 * @param ksId
	 * @return
	 * @throws Exception
	 */
	public String getProductKS(long ksId) throws Exception{
		String productInfo = Constants.STR_BLANK;
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT ");
		varname1.append("       group_concat( P.PRODUCT_CODE || ' - ' ||	P.PRODUCT_NAME, ', ')     PRODUCT_INFO ");
		varname1.append("FROM [KS_PRODUCT] KSL, [PRODUCT]  P ");
		varname1.append("WHERE 1=1 ");
		varname1.append("      AND KSL.KS_ID = ? ");
		params.add(Constants.STR_BLANK + ksId);
		varname1.append("      AND KSL.[PRODUCT_ID] = P.[PRODUCT_ID] ");
		varname1.append("      AND KSL.STATUS = 1 ");
		varname1.append("      AND P.STATUS = 1 ");
		varname1.append("GROUP  BY KSL.KS_ID");
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						productInfo = CursorUtil.getString(c, "PRODUCT_INFO");
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return productInfo;

	}

	/**
	 * Lay danh sach bao cao keyshop nvbh
	 * @author: yennth16
	 * @since: 16:43:35 20-07-2015
	 * @return: KeyShopListDTOView
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public ReportKeyshopStaffDTO getReportKeyShopStaff(Bundle b) throws Exception{
		ReportKeyshopStaffDTO listKeyshop = new ReportKeyshopStaffDTO();
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String programId = b.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		int result = b.getInt(IntentConstants.INTENT_REPORT_RESULT);
		int award = b.getInt(IntentConstants.INTENT_REPORT_AWARD);
		String customer = b.getString(IntentConstants.INTENT_CUSTOMER);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		Cursor c = null;
		String cycleId = Constants.STR_BLANK;
		if(!StringUtil.isNullOrEmpty(b.getString(IntentConstants.INTENT_CYCLE_ID))){
			cycleId = b.getString(IntentConstants.INTENT_CYCLE_ID);
		}else{
			cycleId = String.valueOf(new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0));
		}
		boolean checkPagging = b.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		DMSSortInfo sortInfo = (DMSSortInfo) b.getSerializable(IntentConstants.INTENT_SORT_DATA);
		//String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		//String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("    * ");
		var1.append(" FROM   ");
		var1.append("        (SELECT   VP.STAFF_ID, ");
		var1.append("        		   VP.SHOP_ID, ");
		var1.append("        		   CT.CUSTOMER_ID, ");
		var1.append("        		   CT.SHORT_CODE CUSTOMER_CODE, ");
		var1.append("        		   CT.ADDRESS ADDRESS, ");
		var1.append("                  ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 	 || ' ' ");
		var1.append("                	 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("        		   	CT.CUSTOMER_NAME CUSTOMER_NAME ");
		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               ROUTING_CUSTOMER RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND (SUBSTR(RTC.END_DATE,1,10) >=SUBSTR(?,1,10) OR RTC.END_DATE IS NULL)  ");
		param.add(date_now);
		var1.append("               AND SUBSTR(RTC.START_DATE,1,10) <= SUBSTR(?,1,10)  ");
		param.add(date_now);
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		if (!StringUtil.isNullOrEmpty(customer)) {
			customer = StringUtil.getEngStringFromUnicodeString(customer);
			customer = StringUtil.escapeSqlString(customer);
			var1.append("	and ( upper(CT.CUSTOMER_CODE) like upper(?) ");
			param.add("%" + customer + "%");
			var1.append("	or upper(CT.SHORT_CODE) like upper(?) ");
			param.add("%" + customer + "%");
			var1.append("	or upper(CT.NAME_TEXT) like upper(?) ");
			param.add("%" + customer + "%");
			var1.append("	or upper(CT.CUSTOMER_NAME) like upper(?) ");
			param.add("%" + customer + "%");
			var1.append("	or upper(CT.ADDRESS) like upper(?)) ");
			param.add("%" + customer + "%");
		}
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		if(award < 0 && result <= 0){
			var1.append(" LEFT JOIN ");
		}else{
			var1.append(" JOIN ");
		}
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.RPT_KS_MOBILE_ID          RPT_KS_MOBILE_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID_3, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.RESULT                    RESULT, ");
		var1.append("            rpt_ks.REWARD_TYPE               REWARD_TYPE, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND rpt_ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		if(result > 0){
			var1.append("            AND rpt_ks.RESULT = ? ");
			param.add(""+result);
		}
		if(award == ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_ASSIGN){
			var1.append("            AND rpt_ks.REWARD_TYPE is null ");
		}else if(award >= 0 ){
			var1.append("            AND rpt_ks.REWARD_TYPE = ? ");
			param.add(""+ award);
		}
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID_3 = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("SELECT ");
		var1.append("    ks_c.KS_ID      							KS_ID, ");
		var1.append("    ks_c.KS_CODE    							KS_CODE, ");
		var1.append("    ks_c.NAME       							NAME, ");
		var1.append("    ks_c.[DESCRIPTION] 						DESCRIPTION, ");
		var1.append("    c.CYCLE_NAME  								FROM_CYCLE_ID, ");
		var1.append("    c1.CYCLE_NAME    							TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("( ");
		var1.append("SELECT ");
		var1.append("    DISTINCT ");
		var1.append("    ks.KS_ID      KS_ID, ");
		var1.append("    ks.KS_CODE    KS_CODE, ");
		var1.append("    ks.NAME       NAME, ");
		var1.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		var1.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		var1.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("    ks ks        , ");
		var1.append("    ks_shop_map ksm ");
		var1.append("WHERE ");
		var1.append("    1 = 1 ");
		var1.append("    AND ks.status = 1 ");
		var1.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND  ? <= ks.TO_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND ksm.status = 1 ");
		var1.append("    AND ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ");
		var1.append(")  ks_c ");
		var1.append("left join ");
		var1.append("     cycle c on c.[CYCLE_ID] = ks_c.FROM_CYCLE_ID ");
		var1.append("left join ");
		var1.append("     cycle c1 on c1.[CYCLE_ID] = ks_c.TO_CYCLE_ID ");
		var1.append("    ) KSINFO");
		var1.append("        ON KSINFO.KS_ID = KS.KS_ID");

		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
				defaultOrderByStr.append(" ORDER BY  KSINFO.KS_CODE, CUS.CUSTOMER_CODE ");
		String orderByStr = new DMSSortQueryBuilder()
					.addMapper(SortActionConstants.CODE_PROGRAM, "KSINFO.KS_CODE")
					.addMapper(SortActionConstants.NAME, "CUS.CUSTOMER_NAME")
					.addMapper(SortActionConstants.CODE, "CUS.CUSTOMER_CODE")
					.addMapper(SortActionConstants.ADDRESS, "CUS.ADDRESS")
					.addMapper(SortActionConstants.LEVEL, "REGISTERED_LEVEL")
					.addMapper(SortActionConstants.RESULT, "RESULT")
					.addMapper(SortActionConstants.REWARD, "REWARD_TYPE")
					.addMapper(SortActionConstants.AMOUNT_PLAN, "AMOUNT_TARGET")
					.addMapper(SortActionConstants.AMOUNT_DONE, "AMOUNT")
					.addMapper(SortActionConstants.QUANITY_PLAN, "QUANTITY_TARGET")
					.addMapper(SortActionConstants.QUANITY_DONE, "QUANTITY")
					.defaultOrderString(defaultOrderByStr.toString())
					.build(sortInfo);
		//add order string
		var1.append(orderByStr);

		Cursor cTmp = null;
		String getCountFollowProblemList = Constants.STR_BLANK;
		if (page > 0) {
			// get count
			getCountFollowProblemList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
					+ var1 + ") ";
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReportKeyshopStaffItemDTO item = new ReportKeyshopStaffItemDTO();
						item.initData(c);
						item.productInfo = getProductKS(item.keyshop.ksId);
						item.ksLevelItem = getLevelKS(item.keyshop.ksId);
						item.cycleName = new EXCEPTION_DAY_TABLE(mDB).getCycleName(cycleId);
						item.ksProductItem = getProductLevelKS(item.ksCustomerId);
						listKeyshop.listItem.add(item);
					} while (c.moveToNext());
				}
			}
			if (!checkPagging) {
				cTmp = rawQuery(getCountFollowProblemList, param.toArray(new String[param.size()]));
				if (cTmp != null) {
					cTmp.moveToFirst();
					listKeyshop.total = cTmp.getInt(0);
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;

	}

	/**
	 * Lay danh sach san pham
	 * @author: yennth16
	 * @since: 11:52:03 21-07-2015
	 * @return: ArrayList<KSLevelDTO>
	 * @throws:
	 * @param ksId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<KSLevelDTO> getProductLevelKS(long ksCustomerId) throws Exception{
		ArrayList<KSLevelDTO> listKeyshop = new ArrayList<KSLevelDTO>();
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT ");
		varname1.append("       P.PRODUCT_CODE  			PRODUCT_CODE, ");
		varname1.append("       P.PRODUCT_NAME           	PRODUCT_NAME, ");
		varname1.append("       P.CONVFACT           		CONVFACT, ");
		varname1.append("       KSL.PRODUCT_NUM         	PRODUCT_NUM, ");
		varname1.append("       KSL.PRODUCT_NUM_DONE       	PRODUCT_NUM_DONE ");
		varname1.append("FROM KS_CUS_PRODUCT_REWARD KSL, PRODUCT P ");
		varname1.append("WHERE 1=1 ");
		varname1.append("      AND KSL.KS_CUSTOMER_ID = ? ");
		params.add(Constants.STR_BLANK + ksCustomerId);
		varname1.append("      AND KSL.PRODUCT_ID = P.PRODUCT_ID ");
		varname1.append("      AND KSL.STATUS = 1 ");
		varname1.append("ORDER BY ");
		varname1.append("    P.PRODUCT_CODE");
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KSLevelDTO ksl = new KSLevelDTO();
						ksl.initDataProductFromCursor(c);
						listKeyshop.add(ksl);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;

	}

	/**
	 * getListReportKeyShopStaff
	 * @author: yennth16
	 * @since: 16:15:11 21-07-2015
	 * @return: ReportKeyshopStaffDTO
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public ReportKeyshopDTO getListReportKeyShopStaff(Bundle b) throws Exception{
		ReportKeyshopDTO listKeyshop = new ReportKeyshopDTO();
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String programId = b.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		Cursor c = null;
		String cycleId = Constants.STR_BLANK;
		if(!StringUtil.isNullOrEmpty(b.getString(IntentConstants.INTENT_CYCLE_ID))){
			cycleId = b.getString(IntentConstants.INTENT_CYCLE_ID);
		}else{
			cycleId = String.valueOf(new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0));
		}
		//String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		//String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		DMSSortInfo sortInfo = (DMSSortInfo) b.getSerializable(IntentConstants.INTENT_SORT_DATA);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("      SUM(RPT.QUANTITY_TARGET)           TOTAL_QUANTITY_TARGET, ");
		var1.append("      SUM(RPT.AMOUNT_TARGET)             TOTAL_AMOUNT_TARGET, ");
		var1.append("      SUM(RPT.QUANTITY)                  TOTAL_QUANTITY, ");
		var1.append("      SUM(RPT.AMOUNT)                    TOTAL_AMOUNT, ");
		var1.append("      COUNT(CUS.CUSTOMER_ID)         	  COUNT_CUSTOMER_REGISTER,     ");
		var1.append("      sum (CASE WHEN RESULT = ? THEN 1 else 0 END)         COUNT_CUSTOMER_DONE,        ");
//		var1.append("      (CASE WHEN RESULT = ? THEN COUNT( CUS.CUSTOMER_ID) END)         COUNT_CUSTOMER_DONE,       ");
		param.add(String.valueOf(Constants.STATUS_ARCHIVE)); // dem kh dat
		var1.append("      KS.KS_ID 						  KS_ID, ");
		var1.append("      KS.KS_CODE 						  KS_CODE, ");
		var1.append("      KS.NAME 						  	  NAME, ");
		var1.append("      ST.STAFF_CODE 					  STAFF_CODE, ");
		var1.append("      ST.STAFF_NAME 					  STAFF_NAME, ");
		var1.append("      CUS.SHOP_ID 						  SHOP_ID, ");
		var1.append("      ST.STAFF_ID 						  STAFF_ID ");
		var1.append(" FROM   ");
		var1.append("        (SELECT   VP.STAFF_ID, ");
		var1.append("        		   VP.SHOP_ID, ");
		var1.append("        		   RTC.CUSTOMER_ID ");
		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               ROUTING_CUSTOMER RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND (SUBSTR(RTC.END_DATE,1,10) >=SUBSTR(?,1,10) OR RTC.END_DATE IS NULL)  ");
		param.add(date_now);
		var1.append("               AND SUBSTR(RTC.START_DATE,1,10) <= SUBSTR(?,1,10)  ");
		param.add(date_now);
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("         ) CUS ");
		var1.append("	JOIN ");
		var1.append("	(");
		var1.append("		SELECT ");
		var1.append("     	 DISTINCT 	kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       			ks.KS_ID      		   				KS_ID, ");
		var1.append("       			ks.KS_CODE      					KS_CODE, ");
		var1.append("       			ks.NAME      						NAME, ");
		var1.append("       			ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       			kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       			kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       			kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");
		var1.append("		FROM 	KS ks ");
		var1.append("		JOIN   	KS_CUSTOMER kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       		, KS_SHOP_MAP ksm ");
		var1.append("				where 	1 = 1 ");
		var1.append("     					and ks.status = 1 ");
		var1.append("      					and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("     					and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      					and ksm.status = 1 ");
		var1.append("      					and ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   					and ksm.SHOP_ID IN ");
		var1.append("      					 (").append(idShopList).append(")");
		var1.append("      					) KS ");
		var1.append("	  			 ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID_3, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.RESULT                    RESULT, ");
		var1.append("            rpt_ks.REWARD_TYPE               REWARD_TYPE, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND rpt_ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID_3 = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("SELECT ");
		var1.append("    ks_c.KS_ID      							KS_ID, ");
		var1.append("    ks_c.KS_CODE    							KS_CODE, ");
		var1.append("    ks_c.NAME       							NAME, ");
		var1.append("    ks_c.[DESCRIPTION] 						DESCRIPTION, ");
		var1.append("    c.CYCLE_NAME  								FROM_CYCLE_ID, ");
		var1.append("    c1.CYCLE_NAME    							TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("( ");
		var1.append("SELECT ");
		var1.append("    DISTINCT ");
		var1.append("    ks.KS_ID      KS_ID, ");
		var1.append("    ks.KS_CODE    KS_CODE, ");
		var1.append("    ks.NAME       NAME, ");
		var1.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		var1.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		var1.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("    ks ks        , ");
		var1.append("    ks_shop_map ksm ");
		var1.append("WHERE ");
		var1.append("    1 = 1 ");
		var1.append("    AND ks.status = 1 ");
		var1.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND  ? <= ks.TO_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND ksm.status = 1 ");
		var1.append("    AND ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ");
		var1.append(")  ks_c ");
		var1.append("left join ");
		var1.append("     cycle c on c.[CYCLE_ID] = ks_c.FROM_CYCLE_ID ");
		var1.append("left join ");
		var1.append("     cycle c1 on c1.[CYCLE_ID] = ks_c.TO_CYCLE_ID ");
		var1.append("    ) KSINFO");
		var1.append("        ON KSINFO.KS_ID = KS.KS_ID");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            s.STAFF_CODE           STAFF_CODE, ");
		var1.append("            s.STAFF_NAME           STAFF_NAME, ");
		var1.append("            s.STAFF_ID             STAFF_ID ");
		var1.append("        FROM ");
		var1.append("            STAFF s ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		var1.append("    ) st ");
		var1.append("        ON st.STAFF_ID = CUS.STAFF_ID  ");
		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID,  KSINFO.KS_ID   ");
		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
				defaultOrderByStr.append(" ORDER BY STAFF_CODE, KS_CODE ");
		String orderByStr = new DMSSortQueryBuilder()
					.addMapper(SortActionConstants.STAFF, " STAFF_CODE ")
					.addMapper(SortActionConstants.NAME, " KS.KS_CODE ")
					.addMapper(SortActionConstants.AMOUNT_PLAN, " TOTAL_AMOUNT_TARGET ")
					.addMapper(SortActionConstants.AMOUNT_DONE, " TOTAL_AMOUNT ")
					.addMapper(SortActionConstants.QUANITY_PLAN, " TOTAL_QUANTITY_TARGET ")
					.addMapper(SortActionConstants.QUANITY_DONE, " TOTAL_QUANTITY ")
					.defaultOrderString(defaultOrderByStr.toString())
					.build(sortInfo);

		//add order string
		var1.append(orderByStr);


		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReportKeyshopItemDTO item = new ReportKeyshopItemDTO();
						item.initData(c);
						item.countCustomerRegister = CursorUtil.getInt(c, "COUNT_CUSTOMER_REGISTER");
						item.countDone = CursorUtil.getInt(c, "COUNT_CUSTOMER_DONE");
						item.percent = StringUtil.calPercentUsingRound(item.countCustomerRegister, item.countDone);
						listKeyshop.listItem.add(item);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;
	}


	/**
	 * Tinh tong report keyshop
	 * @author: yennth16
	 * @since: 15:20:31 22-07-2015
	 * @return: ReportKeyshopDTO
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public ReportKeyshopItemDTO getTotalReportKeyShopStaff(Bundle b) throws Exception{
		ReportKeyshopItemDTO total = new ReportKeyshopItemDTO();
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String programId = b.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		Cursor c = null;
		String cycleId = Constants.STR_BLANK;
		if(!StringUtil.isNullOrEmpty(b.getString(IntentConstants.INTENT_CYCLE_ID))){
			cycleId = b.getString(IntentConstants.INTENT_CYCLE_ID);
		}else{
			cycleId = String.valueOf(new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0));
		}
		String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("           ");
		var1.append("      SUM(RPT.QUANTITY_TARGET)           TOTAL_QUANTITY_TARGET, ");
		var1.append("      SUM(RPT.AMOUNT_TARGET)             TOTAL_AMOUNT_TARGET, ");
		var1.append("      SUM(RPT.QUANTITY)                  TOTAL_QUANTITY, ");
		var1.append("      SUM(RPT.AMOUNT)                    TOTAL_AMOUNT, ");
		var1.append("    * ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  			AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID_3, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.RESULT                    RESULT, ");
		var1.append("            rpt_ks.REWARD_TYPE               REWARD_TYPE, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND rpt_ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID_3 = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("SELECT ");
		var1.append("    ks_c.KS_ID      							KS_ID, ");
		var1.append("    ks_c.KS_CODE    							KS_CODE, ");
		var1.append("    ks_c.NAME       							NAME, ");
		var1.append("    ks_c.[DESCRIPTION] 						DESCRIPTION, ");
		var1.append("    c.CYCLE_NAME  								FROM_CYCLE_ID, ");
		var1.append("    c1.CYCLE_NAME    							TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("( ");
		var1.append("SELECT ");
		var1.append("    DISTINCT ");
		var1.append("    ks.KS_ID      KS_ID, ");
		var1.append("    ks.KS_CODE    KS_CODE, ");
		var1.append("    ks.NAME       NAME, ");
		var1.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		var1.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		var1.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("    ks ks        , ");
		var1.append("    ks_shop_map ksm ");
		var1.append("WHERE ");
		var1.append("    1 = 1 ");
		var1.append("    AND ks.status = 1 ");
		var1.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND  ? <= ks.TO_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND ksm.status = 1 ");
		var1.append("    AND ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ");
		var1.append(")  ks_c ");
		var1.append("left join ");
		var1.append("     cycle c on c.[CYCLE_ID] = ks_c.FROM_CYCLE_ID ");
		var1.append("left join ");
		var1.append("     cycle c1 on c1.[CYCLE_ID] = ks_c.TO_CYCLE_ID ");
		var1.append("    ) KSINFO");
		var1.append("        ON KSINFO.KS_ID = KS.KS_ID");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            s.STAFF_CODE           STAFF_CODE, ");
		var1.append("            s.STAFF_NAME           STAFF_NAME, ");
		var1.append("            s.STAFF_ID             STAFF_ID ");
		var1.append("        FROM ");
		var1.append("            STAFF s ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		var1.append("    ) st ");
		var1.append("        ON st.STAFF_ID = CUS.STAFF_ID  ");
		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID");
		var1.append(" ORDER BY STAFF_CODE, KS_CODE ");


		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReportKeyshopItemDTO item = new ReportKeyshopItemDTO();
						item.initData(c);
						item.countCustomerRegister = countCustomerRegisterTotal(shopId, staffId, programId, cycleId);
						item.countDone = countCustomerDoneTotal(shopId, staffId, programId, cycleId);
						item.percent = StringUtil.calPercentUsingRound(item.countCustomerRegister, item.countDone);
						total = item;
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return total;
	}


	/**
	 * Dem so khach hang dang ky keyshop GSBH
	 * @author: yennth16
	 * @since: 10:22:30 22-07-2015
	 * @return: int
	 * @throws:
	 * @param shopId
	 * @param staffId
	 * @param programId
	 * @param cycleId
	 * @return
	 * @throws Exception
	 */
	public int countCustomerRegister(String shopId, String staffId, String programId, String cycleId)
			throws Exception {
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(
				DateUtils.DATE_FORMAT_NOW, new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB
				.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("    count(*) 		NUM	 ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  			AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add("" + cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add("" + cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add("" + cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if (!StringUtil.isNullOrEmpty(programId)) {
			var1.append("    AND ks.KS_ID = ? ");
			param.add("" + programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID,  KS.KS_ID   ");
		int count = 0;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						count = CursorUtil.getInt(c, "NUM");
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return count;
	}

	/**
	 * Dem so khach hang dat
	 * @author: yennth16
	 * @since: 10:34:30 22-07-2015
	 * @return: int
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public int countCustomerDone(String shopId, String staffId, String programId, String cycleId) throws Exception{
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("    count(*)		NUM ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  			AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID_3, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.RESULT                    RESULT, ");
		var1.append("            rpt_ks.REWARD_TYPE               REWARD_TYPE, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		var1.append("    AND rpt_ks.RESULT = 6 ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND rpt_ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID_3 = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("SELECT ");
		var1.append("    ks_c.KS_ID      							KS_ID, ");
		var1.append("    ks_c.KS_CODE    							KS_CODE, ");
		var1.append("    ks_c.NAME       							NAME, ");
		var1.append("    ks_c.[DESCRIPTION] 						DESCRIPTION, ");
		var1.append("    c.CYCLE_NAME  								FROM_CYCLE_ID, ");
		var1.append("    c1.CYCLE_NAME    							TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("( ");
		var1.append("SELECT ");
		var1.append("    DISTINCT ");
		var1.append("    ks.KS_ID      KS_ID, ");
		var1.append("    ks.KS_CODE    KS_CODE, ");
		var1.append("    ks.NAME       NAME, ");
		var1.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		var1.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		var1.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("    ks ks        , ");
		var1.append("    ks_shop_map ksm ");
		var1.append("WHERE ");
		var1.append("    1 = 1 ");
		var1.append("    AND ks.status = 1 ");
		var1.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND  ? <= ks.TO_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND ksm.status = 1 ");
		var1.append("    AND ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ");
		var1.append(")  ks_c ");
		var1.append("left join ");
		var1.append("     cycle c on c.[CYCLE_ID] = ks_c.FROM_CYCLE_ID ");
		var1.append("left join ");
		var1.append("     cycle c1 on c1.[CYCLE_ID] = ks_c.TO_CYCLE_ID ");
		var1.append("    ) KSINFO");
		var1.append("        ON KSINFO.KS_ID = KS.KS_ID");

		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID,  KSINFO.KS_ID   ");
		int count = 0;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						count = CursorUtil.getInt(c, "NUM");
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return count;
	}


	/**
	 * Dem so khach hang dang ky keyshop GSBH
	 * @author: yennth16
	 * @since: 10:22:30 22-07-2015
	 * @return: int
	 * @throws:
	 * @param shopId
	 * @param staffId
	 * @param programId
	 * @param cycleId
	 * @return
	 * @throws Exception
	 */
	public int countCustomerRegisterTotal(String shopId, String staffId, String programId, String cycleId)
			throws Exception {
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(
				DateUtils.DATE_FORMAT_NOW, new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB
				.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("    count(*) 		NUM	 ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  			AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add("" + cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add("" + cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add("" + cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if (!StringUtil.isNullOrEmpty(programId)) {
			var1.append("    AND ks.KS_ID = ? ");
			param.add("" + programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID");
		int count = 0;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						count = CursorUtil.getInt(c, "NUM");
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return count;
	}

	/**
	 * Dem so khach hang dat
	 * @author: yennth16
	 * @since: 10:34:30 22-07-2015
	 * @return: int
	 * @throws:
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public int countCustomerDoneTotal(String shopId, String staffId, String programId, String cycleId) throws Exception{
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("    count(*)		NUM ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  			AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("               AND VP.STAFF_ID IN ");
		var1.append("       (").append(staffId).append(")");
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");

		var1.append("	JOIN ");
		var1.append("	(");

		var1.append("select ");

		var1.append("      DISTINCT kscm.CUSTOMER_ID      				CUSTOMER_ID_3, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");

		var1.append("	from 	ks ks ");
		var1.append("	JOIN   	ks_customer kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("       	, ks_shop_map ksm ");
		var1.append("	where 	1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("      ) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append(" JOIN ");
		var1.append("    ( ");
		var1.append("        SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID_3, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.RESULT                    RESULT, ");
		var1.append("            rpt_ks.REWARD_TYPE               REWARD_TYPE, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		var1.append("    AND rpt_ks.RESULT = 6 ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND rpt_ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID_3 = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");

		var1.append(" LEFT JOIN ");
		var1.append("    ( ");
		var1.append("SELECT ");
		var1.append("    ks_c.KS_ID      							KS_ID, ");
		var1.append("    ks_c.KS_CODE    							KS_CODE, ");
		var1.append("    ks_c.NAME       							NAME, ");
		var1.append("    ks_c.[DESCRIPTION] 						DESCRIPTION, ");
		var1.append("    c.CYCLE_NAME  								FROM_CYCLE_ID, ");
		var1.append("    c1.CYCLE_NAME    							TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("( ");
		var1.append("SELECT ");
		var1.append("    DISTINCT ");
		var1.append("    ks.KS_ID      KS_ID, ");
		var1.append("    ks.KS_CODE    KS_CODE, ");
		var1.append("    ks.NAME       NAME, ");
		var1.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		var1.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		var1.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		var1.append("FROM ");
		var1.append("    ks ks        , ");
		var1.append("    ks_shop_map ksm ");
		var1.append("WHERE ");
		var1.append("    1 = 1 ");
		var1.append("    AND ks.status = 1 ");
		var1.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND  ? <= ks.TO_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("    AND ksm.status = 1 ");
		var1.append("    AND ks.KS_ID = ksm.KS_ID ");
		if(!StringUtil.isNullOrEmpty(programId)){
			var1.append("    AND ks.KS_ID = ? ");
			param.add(""+ programId);
		}
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ");
		var1.append(")  ks_c ");
		var1.append("left join ");
		var1.append("     cycle c on c.[CYCLE_ID] = ks_c.FROM_CYCLE_ID ");
		var1.append("left join ");
		var1.append("     cycle c1 on c1.[CYCLE_ID] = ks_c.TO_CYCLE_ID ");
		var1.append("    ) KSINFO");
		var1.append("        ON KSINFO.KS_ID = KS.KS_ID");

		var1.append(" GROUP BY  CUS.STAFF_ID, CUS.SHOP_ID ");
		int count = 0;
		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						count = CursorUtil.getInt(c, "NUM");
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return count;
	}

	 /**
	 * Lay ds keyshop ma khach hang tham gia
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public ArrayList<KeyShopOrderDTO> getListKeyShopByCustomer(String customerId, String shopId) throws Exception {
		ArrayList<KeyShopOrderDTO> lstKeyShopOrder = new ArrayList<KeyShopOrderDTO>();
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    KS.KS_CODE,	");
		sqlObject.append("	    KS.NAME,	");
		sqlObject.append("	    KS.KS_ID,	");
		sqlObject.append("	    group_concat(KSC.KS_CUSTOMER_ID) KS_CUSTOMER_ID,	");
		sqlObject.append("	    sum(KSC.TOTAL_REWARD_MONEY) TOTAL_REWARD_MONEY,	");
		sqlObject.append("	    sum(KSC.TOTAL_REWARD_MONEY_DONE) TOTAL_REWARD_MONEY_DONE	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS ,	");
		sqlObject.append("	    KS_CUSTOMER KSC,	");
		sqlObject.append("	    CYCLE CY	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    KSC.CUSTOMER_ID = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	    AND KSC.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSC.KS_ID = KS.KS_ID	");
		sqlObject.append("	    AND KSC.STATUS = ?	");
		paramsObject.add(""+KSCustomerDTO.STATE_ACTIVE);
		sqlObject.append("	    AND (KSC.REWARD_TYPE = ?	");
		paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_NOT_PAY);
		sqlObject.append("	    OR KSC.REWARD_TYPE = ? )	");
		paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_PART_PAY);
		sqlObject.append("	    AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		sqlObject.append("	    AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
		sqlObject.append("      AND CY.CYCLE_ID = KSC.CYCLE_ID ");
		sqlObject.append("      AND CY.STATUS = 1 ");
//		sqlObject.append("      AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10) ");
//		paramsObject.add(dateNow);
//		sqlObject.append("      AND SUBSTR(CY.END_DATE,1,10) >= SUBSTR(?,1,10) ");
//		paramsObject.add(dateNow);
		sqlObject.append("   GROUP BY KS.KS_ID,KSC.CUSTOMER_ID ");
		sqlObject.append("   ORDER BY KS_CODE ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KeyShopOrderDTO item = new KeyShopOrderDTO();
						item.parseDataFromCursor(c);
						item.keyshopItem.parseDataFromCursor(c);
						String lstKsCustomerId = CursorUtil.getString(c,"KS_CUSTOMER_ID");
						item.lstKSProduct
								.addAll(getListProductRewardByCustomer(lstKsCustomerId,shopId));
						lstKeyShopOrder.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShopOrder;
	}


	 /**
	 * Lay ds san pham tra thuong cho khach hang tham gia
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: KeyShopOrderViewDTO
	 * @throws:
	*/
	public ArrayList<KSCusProductRewardDTO> getListProductRewardByCustomer(String lstKSCustomerId, String shopId)
			throws Exception {
		Cursor c = null;
		ArrayList<KSCusProductRewardDTO> lstKsCusProductRewardDTOs = new ArrayList<KSCusProductRewardDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    PR.PRODUCT_CODE, PR.PRODUCT_NAME,	");
		sqlObject.append("	    KCPR.PRODUCT_ID PRODUCT_ID,	");
		sqlObject.append("	    SUM(KCPR.PRODUCT_NUM) PRODUCT_NUM,	");
		sqlObject.append("	    SUM(KCPR.PRODUCT_NUM_DONE) PRODUCT_NUM_DONE,	");
		sqlObject.append("	    KS_ID KS_ID	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS_CUS_PRODUCT_REWARD KCPR, PRODUCT PR ");
		sqlObject.append("	WHERE	");
		sqlObject.append("		KCPR.KS_CUSTOMER_ID IN (	");
		sqlObject.append(lstKSCustomerId);
		sqlObject.append("		)	");
		sqlObject.append("		AND KCPR.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("		AND KCPR.status = 1	");
		sqlObject.append("		AND KCPR.PRODUCT_ID = PR.PRODUCT_ID	");
		sqlObject.append("  GROUP BY KS_ID, pr.product_id");
		sqlObject.append("	ORDER BY pr.order_index	");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						KSCusProductRewardDTO item = new KSCusProductRewardDTO();
						item.initDataFromCursor(c);
						lstKsCusProductRewardDTOs.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKsCusProductRewardDTOs;
	}

	/**
	 * Lay thong tin CT keyshop
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ArrayList<PromotionProgrameDTO>
	 * @throws:
	*/
	public KeyShopItemDTO getPromotionProgrameDetailKeyShop(Bundle data) throws Exception {
		String keyshopId = data
				.getString(IntentConstants.INTENT_PROMOTION_ID);
		KeyShopItemDTO item = new KeyShopItemDTO();
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> lstShop = new SHOP_TABLE(mDB).getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT ");
		sqlObject.append("    ks.KS_ID      						KS_ID, ");
		sqlObject.append("    ks.KS_CODE    						KS_CODE, ");
		sqlObject.append("    ks.NAME       						NAME, ");
		sqlObject.append("    ks.[DESCRIPTION] 						DESCRIPTION, ");
		sqlObject.append("    c.CYCLE_NAME  						FROM_CYCLE_ID, ");
		sqlObject.append("    c1.CYCLE_NAME    						TO_CYCLE_ID ");
		sqlObject.append("FROM ");
		sqlObject.append("( ");
		sqlObject.append("SELECT ");
		sqlObject.append("    DISTINCT ");
		sqlObject.append("    ks.KS_ID      KS_ID, ");
		sqlObject.append("    ks.KS_CODE    KS_CODE, ");
		sqlObject.append("    ks.NAME       NAME, ");
		sqlObject.append("    ks.[DESCRIPTION] DESCRIPTION, ");
		sqlObject.append("    ks.[FROM_CYCLE_ID]  FROM_CYCLE_ID, ");
		sqlObject.append("    ks.[TO_CYCLE_ID]    TO_CYCLE_ID ");
		sqlObject.append("FROM ");
		sqlObject.append("    ks ks        , ");
		sqlObject.append("    ks_shop_map ksm ");
		sqlObject.append("WHERE ");
		sqlObject.append("    1 = 1 ");
		sqlObject.append("    AND ks.status = 1 ");
		sqlObject.append("    AND  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		sqlObject.append("    AND  ? <= ks.TO_CYCLE_ID ");
		params.add(""+cycleId);
		sqlObject.append("    AND ksm.status = 1 ");
		sqlObject.append("    AND ks.KS_ID = ksm.KS_ID ");
		sqlObject.append("    AND ks.KS_ID = ? ");
		params.add(keyshopId);
		sqlObject.append(" 	   and ksm.SHOP_ID IN ");
		sqlObject.append("       (").append(idShopList).append(")");
		sqlObject.append(" ");
		sqlObject.append(")  ks ");
		sqlObject.append("left join ");
		sqlObject.append("     cycle c on c.[CYCLE_ID] = ks.FROM_CYCLE_ID ");
		sqlObject.append("left join ");
		sqlObject.append("     cycle c1 on c1.[CYCLE_ID] = ks.TO_CYCLE_ID ");


		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					item.initData(c);
					item.productInfo = getProductKS(item.ksId);
					item.ksLevelItem = getLevelKS(item.ksId);
				}
			}

		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return item;
	}

	 /**
	 * Kiem tra keyshop cua KH
	 * @author: Tuanlt11
	 * @param customerId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	*/
	public boolean checkKeyShopCustomer(Bundle b) throws Exception {
		boolean isExist = false;
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS ,	");
		sqlObject.append("	    KS_CUSTOMER KSC,	");
		sqlObject.append("	    CYCLE CY	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    KSC.CUSTOMER_ID = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	    AND KSC.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSC.KS_ID = KS.KS_ID	");
		sqlObject.append("	    AND KSC.STATUS = ?	");
		paramsObject.add(""+KSCustomerDTO.STATE_ACTIVE);
		sqlObject.append("	    AND (KSC.REWARD_TYPE = ?	");
		paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_NOT_PAY);
		sqlObject.append("	    OR KSC.REWARD_TYPE = ? )	");
		paramsObject.add(""+KSCustomerDTO.TYPE_REWARD_PART_PAY);
		sqlObject.append("	    AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		sqlObject.append("	    AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
		sqlObject.append("      AND CY.CYCLE_ID = KSC.CYCLE_ID ");
		sqlObject.append("      AND CY.STATUS = 1 ");
		sqlObject.append("   GROUP BY KS.KS_ID,KSC.CUSTOMER_ID ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					isExist = true;
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return isExist;
	}



	 /**
	 * Lay so luong san pham tra cho customer
	 * @author: Tuanlt11
	 * @param customerId
	 * @param lstKSCustomerId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: ArrayList<KSCusProductRewardDTO>
	 * @throws:
	*/
	public void getNumProductRewardByCustomer(OrderDetailViewDTO orderJoinTableDTO, long ksId, int productId, long customerId)
			throws Exception {
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    SUM(KCPR.PRODUCT_NUM) PRODUCT_NUM,	");
		sqlObject.append("	    SUM(KCPR.PRODUCT_NUM_DONE) PRODUCT_NUM_DONE	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS_CUS_PRODUCT_REWARD KCPR, KS ,KS_CUSTOMER KSC ");
		sqlObject.append("	WHERE	");
		sqlObject.append("		KS.KS_ID = KSC.KS_ID	");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("		and KCPR.KS_CUSTOMER_ID = KSC.KS_CUSTOMER_ID	");
		sqlObject.append("		and ksc.CUSTOMER_ID = ?	");
		paramsObject.add(""+customerId);
		sqlObject.append("		and KS.KS_ID = ?	");
		paramsObject.add(""+ksId);
		sqlObject.append("		AND KCPR.product_id = ?	");
		paramsObject.add(""+productId);
		sqlObject.append("		AND KSC.status = 1	");
		sqlObject.append("  GROUP BY KCPR.KS_ID, KCPR.product_id");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					orderJoinTableDTO.keyshopProductNum = CursorUtil.getLong(c, KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_NUM);
					orderJoinTableDTO.keyshopProductNumDone = CursorUtil.getLong(c, KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_NUM_DONE);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
	}


	 /**
	 * Lay so tien nhan duoc cua khach hang
	 * @author: Tuanlt11
	 * @param orderJoinTableDTO
	 * @param ksId
	 * @param customerId
	 * @param customerId
	 * @throws Exception
	 * @return: void
	 * @throws:
	*/
	public void getMoneyRewardByCustomer(OrderDetailViewDTO orderJoinTableDTO,long ksId, long customerId)
			throws Exception {
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    sum(KSC.TOTAL_REWARD_MONEY) TOTAL_REWARD_MONEY,	");
		sqlObject.append("	    sum(KSC.TOTAL_REWARD_MONEY_DONE) TOTAL_REWARD_MONEY_DONE	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    KS, KS_CUSTOMER KSC ");
		sqlObject.append("	WHERE	");
		sqlObject.append("		KS.KS_ID = KSC.KS_ID	");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSC.STATUS = ?	");
		paramsObject.add(""+KSCustomerDTO.STATE_ACTIVE);
		sqlObject.append("		and ksc.CUSTOMER_ID = ?	");
		paramsObject.add(""+customerId);
		sqlObject.append("		and KS.KS_ID = ?	");
		paramsObject.add(""+ksId);
		sqlObject.append("   GROUP BY KS.KS_ID, KSC.CUSTOMER_ID ");

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					orderJoinTableDTO.keyshopRewardMoney = CursorUtil.getDouble(c, KS_CUSTOMER_TABLE.TOTAL_REWARD_MONEY);
					orderJoinTableDTO.keyshopRewardMoneyDone = CursorUtil.getDouble(c, KS_CUSTOMER_TABLE.TOTAL_REWARD_MONEY_DONE);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
	}


	 /**
	 * Lay ds  bao cao ct httm cua khach hang
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @throws Exception
	 * @return: ReportKeyshopStaffDTO
	 * @throws:
	*/
	public ReportKeyshopStaffDTO getReportListCustomer(Bundle b) throws Exception{
		ReportKeyshopStaffDTO listKeyshop = new ReportKeyshopStaffDTO();
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		Cursor c = null;
		String cycleId = String.valueOf(new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0));
//		DMSSortInfo sortInfo = (DMSSortInfo) b.getSerializable(IntentConstants.INTENT_SORT_DATA);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append(" SELECT ");
		var1.append("       	ks.KS_ID      		   			KS_ID, ");
		var1.append("       	ks.KS_CODE      				KS_CODE, ");
		var1.append("       	ks.NAME      					NAME, ");
		var1.append("       	ks.DESCRIPTION      			DESCRIPTION, ");
		var1.append("       	ks.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       	ks.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       	ks.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE, ");
		var1.append("           RPT.RPT_KS_MOBILE_ID          	RPT_KS_MOBILE_ID, ");
		var1.append("           RPT.CUSTOMER_ID               	CUSTOMER_ID, ");
		var1.append("           RPT.QUANTITY_TARGET           	QUANTITY_TARGET, ");
		var1.append("           RPT.AMOUNT_TARGET             	AMOUNT_TARGET, ");
		var1.append("           RPT.QUANTITY                  	QUANTITY, ");
		var1.append("           RPT.AMOUNT                    	AMOUNT, ");
		var1.append("           RPT.REGISTERED_LEVEL          	REGISTERED_LEVEL ");
		var1.append(" FROM   CUSTOMER CUS ");
		var1.append("    JOIN ");
		var1.append("    ( SELECT ");
		var1.append("               kscm.CUSTOMER_ID      				CUSTOMER_ID, ");
		var1.append("       		ks.KS_ID      		   				KS_ID, ");
		var1.append("       		ks.KS_CODE      					KS_CODE, ");
		var1.append("       		ks.NAME      						NAME, ");
		var1.append("       		ks.DESCRIPTION      				DESCRIPTION, ");
		var1.append("       		kscm.KS_CUSTOMER_ID     			KS_CUSTOMER_ID, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY     		TOTAL_REWARD_MONEY, ");
		var1.append("       		kscm.TOTAL_REWARD_MONEY_DONE     	TOTAL_REWARD_MONEY_DONE ");
		var1.append("	   FROM 	KS ks ");
		var1.append("	     JOIN   KS_CUSTOMER kscm ");
		var1.append("         		ON 	kscm.ks_id = ks.ks_id ");
		var1.append("            		AND kscm.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("            		AND kscm.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("           	 	AND kscm.status = 1 ");
		var1.append("	   			    AND kscm.STATUS = ?	");
		param.add(""+KSCustomerDTO.STATE_ACTIVE);
		var1.append("       	   , KS_SHOP_MAP ksm ");
		var1.append("		WHERE 	1 = 1 ");
		var1.append("      			and ks.status = 1 ");
		var1.append("      			and  ? >= ks.FROM_CYCLE_ID ");
		param.add(""+cycleId);
		var1.append("      			and  ? <= ks.TO_CYCLE_ID");
		param.add(""+cycleId);
		var1.append("     		    and ksm.status = 1 ");
		var1.append("     		    and ks.KS_ID = ksm.KS_ID ");
		var1.append(" 	   			and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append("     ) KS ");
		var1.append("	      ON KS.CUSTOMER_ID = CUS.CUSTOMER_ID");
		var1.append("    JOIN (SELECT ");
		var1.append("            rpt_ks.KS_ID               	  KS_ID, ");
		var1.append("            rpt_ks.RPT_KS_MOBILE_ID          RPT_KS_MOBILE_ID, ");
		var1.append("            rpt_ks.CUSTOMER_ID               CUSTOMER_ID, ");
		var1.append("            rpt_ks.QUANTITY_TARGET           QUANTITY_TARGET, ");
		var1.append("            rpt_ks.AMOUNT_TARGET             AMOUNT_TARGET, ");
		var1.append("            rpt_ks.QUANTITY                  QUANTITY, ");
		var1.append("            rpt_ks.AMOUNT                    AMOUNT, ");
		var1.append("            rpt_ks.REGISTERED_LEVEL          REGISTERED_LEVEL ");
		var1.append("        FROM ");
		var1.append("            RPT_KS_MOBILE rpt_ks ");
		var1.append("        WHERE ");
		var1.append("            1 = 1 ");
		var1.append("            AND rpt_ks.shop_id = ? ");
		param.add(""+shopId);
		var1.append("            AND rpt_ks.cycle_id = ? ");
		param.add(""+cycleId);
		var1.append("    ) RPT ");
		var1.append("        ON RPT.CUSTOMER_ID = CUS.CUSTOMER_ID AND RPT.KS_ID = KS.KS_ID ");
		var1.append(" WHERE 1 = 1");
		var1.append(" 		AND CUS.customer_id = ?");
		param.add(""+customerId);
		var1.append(" ORDER BY  KS_CODE");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReportKeyshopStaffItemDTO item = new ReportKeyshopStaffItemDTO();
						item.initData(c);
						listKeyshop.listItem.add(item);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listKeyshop;

	}


}
