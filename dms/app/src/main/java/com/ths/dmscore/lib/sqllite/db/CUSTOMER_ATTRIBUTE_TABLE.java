/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.CustomerAttributeEnumViewDTO;
import com.ths.dmscore.dto.view.ListCustomerAttributeViewDTO;

/**
 * Luu thong tin khach hang
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_ATTRIBUTE_TABLE extends ABSTRACT_TABLE {
	public static final String CUSTOMER_ATTRIBUTE_ID = "CUSTOMER_ATTRIBUTE_ID";
	public static final String CODE = "CODE";
	public static final String NAME = "NAME";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String TYPE = "TYPE";
	public static final String DATA_LENGTH = "DATA_LENGTH";
	public static final String MIN_VALUE = "MIN_VALUE";
	public static final String MAX_VALUE = "MAX_VALUE";
	public static final String IS_ENUMERATION = "IS_ENUMERATION";
	public static final String IS_SEARCH = "IS_SEARCH";
	public static final String MANDATORY = "MANDATORY";
	public static final String DISPLAY_ORDER = "DISPLAY_ORDER";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "CUSTOMER_ATTRIBUTE";

	public CUSTOMER_ATTRIBUTE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { CUSTOMER_ATTRIBUTE_ID, CODE, NAME,
				DESCRIPTION, TYPE, DATA_LENGTH, MIN_VALUE, MAX_VALUE,
				IS_ENUMERATION, IS_SEARCH, MANDATORY, DISPLAY_ORDER, STATUS,
				CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * lay thuoc tinh dong kh
	 * 
	 * @author: dungdq3
	 * @since: 4:28:37 PM Jan 27, 2015
	 * @return: Object
	 * @throws:  
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 */
	public ListCustomerAttributeViewDTO getExtraCustomerInfo(String staffId, String shopId,
			String customerId) {
		// TODO Auto-generated method stub
		Cursor cursor = null;
		ListCustomerAttributeViewDTO customerInfo = new ListCustomerAttributeViewDTO();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT ca.customer_attribute_id as CUSTOMER_ATTRIBUTE_ID, ");
		varname1.append("       ca.code as CODE, ");
		varname1.append("       ca.NAME as NAME, ");
		varname1.append("       ca.description as DESCRIPTION, ");
		varname1.append("       ca.type as TYPE, ");
		varname1.append("       ca.data_length as DATA_LENGTH, ");
		varname1.append("       ca.min_value as MIN_VALUE, ");
		varname1.append("       ca.max_value as MAX_VALUE, ");
		varname1.append("       ca.is_enumeration as IS_ENUMERATION, ");
		varname1.append("       ca.is_search as IS_SEARCH, ");
		varname1.append("       ca.mandatory as MANDATORY, ");
		varname1.append("       ca.display_order as DISPLAY_ORDER, ");
		varname1.append("       cae.value as VALUE , ");
		varname1.append("       cae.code as ENUM_CODE, ");
		varname1.append("       cae.customer_attribute_enum_id as CUSTOMER_ATTRIBUTE_ENUM_ID, ");
		varname1.append("       cad.value as DETAIL_VALUE, ");
		varname1.append("       cad.customer_attribute_detail_id as CUSTOMER_ATTRIBUTE_DETAIL_ID, ");
		varname1.append("       cad.customer_attribute_enum_id as DETAIL_ENUM_ID ");
		varname1.append("FROM   (SELECT customer_attribute_id, ");
		varname1.append("               code, ");
		varname1.append("               NAME, ");
		varname1.append("               description, ");
		varname1.append("               type, ");
		varname1.append("               data_length, ");
		varname1.append("               min_value, ");
		varname1.append("               max_value, ");
		varname1.append("               is_enumeration, ");
		varname1.append("               is_search, ");
		varname1.append("               mandatory, ");
		varname1.append("               display_order ");
		varname1.append("        FROM   customer_attribute ");
		varname1.append("        WHERE  1 = 1 ");
		varname1.append("               AND status = 1) ca ");
		varname1.append("       LEFT JOIN (SELECT value, ");
		varname1.append("                         code, ");
		varname1.append("                         customer_attribute_id, ");
		varname1.append("                         customer_attribute_enum_id ");
		varname1.append("                  FROM   customer_attribute_enum ");
		varname1.append("                  WHERE  1 = 1 ");
		varname1.append("                         AND status = 1) cae ");
		varname1.append("              ON cae.customer_attribute_id = ca.customer_attribute_id ");
		varname1.append("       LEFT JOIN (SELECT customer_attribute_id, ");
		varname1.append("                         customer_attribute_detail_id, ");
		varname1.append("                         value, ");
		varname1.append("                         customer_attribute_enum_id ");
		varname1.append("                  FROM   customer_attribute_detail ");
		varname1.append("                  WHERE  1 = 1 ");
		varname1.append("                         AND status = 1 ");
		varname1.append("                         AND customer_id = ?) cad ");
		params.add(customerId);
		varname1.append("              ON ca.customer_attribute_id = cad.customer_attribute_id ");
		varname1.append("              AND (cae.customer_attribute_enum_id is null OR cae.customer_attribute_enum_id = cad.customer_attribute_enum_id) ");
		varname1.append("ORDER  BY -ca.display_order desc;");

		try {
			cursor = rawQueries(varname1.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						CustomerAttributeViewDTO attr = new CustomerAttributeViewDTO();
						CustomerAttributeEnumViewDTO enumAttr = new CustomerAttributeEnumViewDTO();
						attr.initFromCurSor(cursor);
						enumAttr.initFromCursor(cursor);
						customerInfo.addCustomerAttribute(attr, enumAttr);
					} while (cursor.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.e("", ex.toString());
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return customerInfo;
	}
}