/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

/**
 *  Item cua spinner text
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class SpinnerItemDTO{
	public String name;
	public String content;
	public int staffId;
	public int shopId;
	
	public SpinnerItemDTO(){
	}
}
