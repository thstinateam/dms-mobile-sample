package com.ths.dmscore.dto.db.trainingplan;

/**
 * 
 * SupTraningResultDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:42:58 17-11-2014
 */
public class SupTrainingResultDTO {
	private int supTrainingResultId;
	private int trainingRateDetailId;
	private int supTrainingCustomerId;
	private int result;
	private String createUser;
	private String updateUser;
	private String createDate;
	private String updateDate;

	public int getSupTraningResultId() {
		return supTrainingResultId;
	}

	public void setSupTraningResultId(int supTrainingResultId) {
		this.supTrainingResultId = supTrainingResultId;
	}

	public int getTraningRateDetailId() {
		return trainingRateDetailId;
	}

	public void setTraningRateDetailId(int trainingRateDetailId) {
		this.trainingRateDetailId = trainingRateDetailId;
	}

	public int getSupTrainingCustomerId() {
		return supTrainingCustomerId;
	}

	public void setSupTrainingCustomerId(int supTrainingCustomerId) {
		this.supTrainingCustomerId = supTrainingCustomerId;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}