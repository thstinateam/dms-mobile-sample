package com.ths.dmscore.dto.view.filechooser;

public interface FileAttachEvent {
        void onRemoveAttach(int posRemove);
        void onOpenFile(FileInfo info);
}