/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.SaleProductInfoDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * hien thi 1 row trong man hinh thong ke don tong trong ngay cua PreSales
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SaleStatisticsInDayPreSalesRow extends DMSTableRow {
	// stt
	TextView tvSTT;
	// product code
	TextView tvProductCode;
	// product name
	TextView tvProductName;
	// industry product
	TextView tvIndustryProduct;
	// // convfact
	TextView tvConvfact;
	// price
//	TextView tvPrice;
	// number product
	TextView tvNumberProduct;
	// number product approved
	TextView tvNumberProductApproved;
	TextView tvQuantityExchange;
	TextView tvNumberProductApprovedExchange;
	// total amount
	TextView tvTotalAmount;
	// total amount approved
	TextView tvTotalAmountApproved;
	// data to render layout for row
	FindProductSaleOrderDetailViewDTO myData;

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 */
	public SaleStatisticsInDayPreSalesRow(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_sale_statistics_in_day_pre_sales_row, 1.3,
				GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {R.id.tvTotalAmount, R.id.tvTotalAmountApproved});
		listener = listen;
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvIndustryProduct = (TextView) findViewById(R.id.tvIndustryProduct);
		tvConvfact = (TextView) findViewById(R.id.tvConvfact);
//		tvPrice = (TextView) findViewById(R.id.tvPrice);
		tvNumberProduct = (TextView) findViewById(R.id.tvNumberProduct);
		tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
		tvNumberProductApproved = (TextView) findViewById(R.id.tvNumberProductApproved);
		tvTotalAmountApproved = (TextView) findViewById(R.id.tvTotalAmountApproved);
		tvQuantityExchange = (TextView) findViewById(R.id.tvQuantityExchange);
		tvNumberProductApprovedExchange = (TextView) findViewById(R.id.tvNumberProductApprovedExchange);
	}

	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, SaleProductInfoDTO item) {
		display(tvSTT, position);
		display(tvProductCode, item.productInfo.productCode);
		display(tvProductName, item.productInfo.productName);
		display(tvIndustryProduct, item.productInfo.categoryCode);
		display(tvConvfact, item.productInfo.convfact);
		display(tvNumberProduct, item.numberProductFormat);
		display(tvTotalAmount, item.totalAmountSold);
		display(tvNumberProductApproved, item.numberProductFormatApproved);
		display(tvTotalAmountApproved, item.totalAmountSoldApproved);
		display(tvQuantityExchange, item.numberProduct);
		display(tvNumberProductApprovedExchange, item.tongQuyDoi);
	}

	/**
	 * render layout tong
	 *
	 * @author: dungdq3
	 * @since: 5:37:50 PM Apr 3, 2015
	 * @return: void
	 * @throws:
	 * @param item
	 */
	public void renderLayoutSum(SaleProductInfoDTO item){
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT, tvProductCode, tvProductName, tvIndustryProduct, tvConvfact);
		display(tvNumberProduct, Constants.STR_BLANK);
		display(tvNumberProductApproved, Constants.STR_BLANK);
		display(tvQuantityExchange, item.numberProduct);
		display(tvNumberProductApprovedExchange, item.tongQuyDoi);
		display(tvTotalAmount, item.totalAmountSold);
		display(tvTotalAmountApproved, item.totalAmountSoldApproved);
		tvQuantityExchange.setTypeface(null, Typeface.BOLD);
		tvNumberProductApprovedExchange.setTypeface(null, Typeface.BOLD);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
	}

}
