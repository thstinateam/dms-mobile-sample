package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.RPT_CTTL_DETAIL_PAY_TABLE;
import com.ths.dmscore.lib.sqllite.db.RPT_CTTL_PAY_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class RptCttlPayDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -6330993676069878367L;
	public long rptCttlPayId;
	public String createDate;
	public int promotionProgramId;
	public String promotionFromDate;
	public String promotionToDate;
	public int shopId;
	public long customerId;
	public double totalQuantityPayPromotion;
	public long totalAmountPayPromotion;
	public int promotion;
	public int isMigrate;
	public int approved;
	public int staffId;
//	public long amountPromotion;
//	public long actuallyPromotion;
	public long saleOrderId;

	public ArrayList<RptCttlDetailPayDTO> rptCttlDetailList = new ArrayList<RptCttlDetailPayDTO>();
	//Loai co cau qui dinh: so luong hoac so tien khi mua
	public int promotionValueType;

	/**
	 * gen json promotion detail
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateInsertRptCttlPay() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, RPT_CTTL_PAY_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.RPT_CTTL_PAY_ID, rptCttlPayId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.CREATE_DATE, createDate, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.PROMOTION_PROGRAM_ID, promotionProgramId, null));
			if(!StringUtil.isNullOrEmpty(promotionFromDate)) {
				params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.PROMOTION_FROM_DATE, promotionFromDate, null));
			}
			if(!StringUtil.isNullOrEmpty(promotionToDate)) {
				params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.PROMOTION_TO_DATE, promotionToDate, null));
			}
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.SHOP_ID, shopId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.CUSTOMER_ID, customerId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.TOTAL_QUANTITY_PAY_PROMOTION, totalQuantityPayPromotion, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.TOTAL_AMOUNT_PAY_PROMOTION, totalAmountPayPromotion, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.PROMOTION, promotion, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.APPROVED, approved, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.STAFF_ID, staffId, null));
//			if(amountPromotion > 0) {
//				params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.AMOUNT_PROMOTION, amountPromotion, null));
//			}
//			if(actuallyPromotion > 0) {
//				params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.ACTUALLY_PROMOTION, actuallyPromotion, null));
//			}
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.SALE_ORDER_ID, saleOrderId, null));

			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * khoi tao doi tuong tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public void initFromCursor(Cursor c) {
		rptCttlPayId = CursorUtil.getLong(c, RPT_CTTL_PAY_TABLE.RPT_CTTL_PAY_ID);
		totalQuantityPayPromotion = CursorUtil.getDouble(c, RPT_CTTL_PAY_TABLE.TOTAL_QUANTITY_PAY_PROMOTION);
		totalAmountPayPromotion = CursorUtil.getLong(c, RPT_CTTL_PAY_TABLE.TOTAL_AMOUNT_PAY_PROMOTION);
		approved = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.APPROVED);
		createDate = CursorUtil.getString(c, RPT_CTTL_PAY_TABLE.CREATE_DATE);
		promotionProgramId = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.PROMOTION_PROGRAM_ID);
		promotionFromDate = CursorUtil.getString(c, RPT_CTTL_PAY_TABLE.PROMOTION_FROM_DATE);
		promotionToDate = CursorUtil.getString(c, RPT_CTTL_PAY_TABLE.PROMOTION_TO_DATE);
		shopId = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.SHOP_ID);
		customerId = CursorUtil.getLong(c, RPT_CTTL_PAY_TABLE.CUSTOMER_ID);
		promotion = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.PROMOTION);
		isMigrate = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.IS_MIGRATE);
		staffId = CursorUtil.getInt(c, RPT_CTTL_PAY_TABLE.STAFF_ID);
		saleOrderId = CursorUtil.getLong(c, RPT_CTTL_PAY_TABLE.SALE_ORDER_ID);
	}

	/**
	 * gen json xoa rpt_cttl_pay
	 * @author: dungnt19
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateDeleteRptCttlPay() {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, RPT_CTTL_PAY_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(RPT_CTTL_PAY_TABLE.RPT_CTTL_PAY_ID, String.valueOf(rptCttlPayId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * gen json xoa rpt_cttl_detail_pay
	 * @author: dungnt19
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateDeleteRptCttlDetailPay() {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, RPT_CTTL_DETAIL_PAY_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.RPT_CTTL_PAY_ID, String.valueOf(rptCttlPayId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * clone ra doi tuong pay va detail pay
	 * @author: dungnt19
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public RptCttlPayDTO clone() {
		RptCttlPayDTO rptCttlPay = new RptCttlPayDTO();

		rptCttlPay.createDate = createDate;
		rptCttlPay.promotionProgramId = promotionProgramId;
		rptCttlPay.promotionFromDate = promotionFromDate;
		rptCttlPay.promotionToDate = promotionToDate;
		rptCttlPay.shopId = shopId;
		rptCttlPay.customerId = customerId;

		rptCttlPay.promotion = promotion;
		rptCttlPay.approved = approved;
		rptCttlPay.staffId = staffId;
		rptCttlPay.totalQuantityPayPromotion = totalQuantityPayPromotion;
		rptCttlPay.totalAmountPayPromotion = totalAmountPayPromotion;
		rptCttlPay.promotionValueType = promotionValueType;

		for(RptCttlDetailPayDTO payDetail : rptCttlDetailList) {
			rptCttlPay.rptCttlDetailList.add(payDetail.clone());
		}

		return rptCttlPay;
	}

	/**
	 * Reset gia tri da su dung
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void resetValueUsing() {
		promotion = 0;
		totalQuantityPayPromotion = 0;
		totalAmountPayPromotion = 0;

		for(RptCttlDetailPayDTO payDetail : rptCttlDetailList) {
			payDetail.resetValueUsing();
		}
	}
}
