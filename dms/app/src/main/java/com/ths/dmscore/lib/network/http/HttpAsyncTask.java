/**
 * Copyright 2011 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.network.http;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import android.os.AsyncTask;
import android.util.Base64;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.lib.oAuth.model.OAuthRequest;
import com.ths.dmscore.lib.oAuth.model.Response;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.network.http.DataSupplier.Data;
import com.ths.dms.R;

/**
 *  DataSuplier interface
 *  @author: AnhND
 *  @version: 1.0
 *  @since: 1.0
 */
interface DataSupplier {
	public class Data {
		byte[] buffer;
		boolean isFinish;
		int length;
	}
	void getNextPart(Data data);
	void releaseData();
	int overallDataSize();
	void reset();
}

/**
 *  request http
 *  @author: AnhND
 *  @version: 1.0
 *  @since: Jun 10, 2011
 */
public class HttpAsyncTask extends AsyncTask<Void, Void, Void> {
	private HTTPRequest request;
	private HTTPResponse response;
	private boolean isSuccess;
	private static final String LOG_TAG = "HttpAsyncTask";
	private int readTimeout = NetworkTimeout.getREAD_TIME_OUT();
	private int connectTimeout = NetworkTimeout.getCONNECT_TIME_OUT();
	private boolean isRetryRequest = true;
	
	public HttpAsyncTask(HTTPRequest re) {
		this.request = re;
		this.readTimeout = NetworkTimeout.getREAD_TIME_OUT();
	}
	public HttpAsyncTask(HTTPRequest re, boolean isRetryRequest) {
		this.request = re;
		this.readTimeout = NetworkTimeout.getREAD_TIME_OUT();
		this.isRetryRequest = isRetryRequest;
	}
	public HttpAsyncTask(HTTPRequest re, int timeout) {
		this.request = re;
		this.readTimeout = timeout;
	}
	
	public HttpAsyncTask(HTTPRequest re, int connnectTimeOut, int readTimeOut) {
		this.request = re;
		this.connectTimeout = connnectTimeOut;
		this.readTimeout = readTimeOut;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		if (request == null || request.isAlive() == false) {
			if (request == null) {
				MyLog.i(LOG_TAG, "Request null");
			}else {
				MyLog.i(LOG_TAG, "Request NOT alive");
			}
			return null;
		}		
		
		int countRetry = 0;
		final int NUM_RETRY = 1;
		boolean isRetry = false;
		do {
			isRetry = false;
			countRetry++;
			HttpURLConnection connection = null;				
			isSuccess = true;
			//bug sometime response code = -1
			System.setProperty("http.keepAlive", "false");
//			System.setProperty("http.proxyHost", "10.61.11.38");
//			System.setProperty("http.proxyPort", "3128");
			try {
				response = new HTTPResponse(request);
				//re_login cho thuc hien chuc thuc lai
				if(request.getAction() == ActionEventConstant.RE_LOGIN){
					OAuthRequestManager.reInit();
				}
				OAuthRequest oAuthRequest = OAuthRequestManager.getInstance()
						.signRequest(request.getUrl() + request.getMethodName(),
								request.getMethod());
				if(oAuthRequest != null){					
					oAuthRequest.createConnection();
					connection = oAuthRequest.getConnection();			 	
					
					oAuthRequest.setConnectTimeout(connectTimeout, TimeUnit.MILLISECONDS);
					oAuthRequest.setReadTimeout(readTimeout, TimeUnit.MILLISECONDS);
					if (request.getContentType() != null) {
						connection.setRequestProperty("Content-type", request.contentType);
					}
					String basicAuth = "Basic " + new String(Base64.encode("user:pass".getBytes(),Base64.NO_WRAP ));
					connection.setRequestProperty ("Authorization", basicAuth);

					
					if (HTTPRequest.POST.equals(request.getMethod())){					
						Data data = new Data();
						request.getNextPart(data);
						oAuthRequest.addPayload(data.buffer);
					}				
					if (HTTPClient.getSessionID() != null) {
						oAuthRequest.addHeader("Cookie", HTTPClient.getSessionID());
						connection.setRequestProperty("Cookie", HTTPClient.getSessionID());
					}
					
					Response response1 = oAuthRequest.doSend();
					response.setDataText(response1.getBody());
					//HieuNH ghi log de dieu tra bug invalid token
					if(response.dataText != null && response.dataText.contains("Invalid token")){
						//MyLog.logToFile("ACCESS_TOKEN", "Invalid access Token = " + OAuthRequestManager.getInstance().accessToken);
						MyLog.d("ACCESS_TOKEN", "Invalid access Token = " + OAuthRequestManager.getInstance().accessToken);
					}
					
					response.setCode(response1.getCode());
					if(response.getCode() == OAuthRequestManager.TOKEN_INVALID 
							|| response.getCode() == OAuthRequestManager.TIMED_OUT
							|| response.getCode() == OAuthRequestManager.SERVICE_UNAVAILABLE 
							|| response.getCode() == OAuthRequestManager.GATE_WAY_TIME_OUT ){	
						if (response.getCode() == OAuthRequestManager.GATE_WAY_TIME_OUT || 
							response.getCode() == OAuthRequestManager.SERVICE_UNAVAILABLE){
							isSuccess = false;
							response.setError(HTTPClient.ERR_TIME_OUT, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER),"");
						}else{
							response.setCode(OAuthRequestManager.TIMED_OUT);
							isRetry = true;
						}
						OAuthRequestManager.getInstance().accessToken = null;						
					}				
					if (response.getDataText() == null && response.getDataBinary() == null && request.isAlive()) {
						isSuccess = false;
						isRetry = true;
						StringBuffer strBuffer = new StringBuffer();						
						strBuffer.append("ResponseCode: " + connection.getResponseCode());
						strBuffer.append("/ResponseMsg: " + connection.getResponseMessage());
						response.setError(HTTPClient.ERR_UNKNOWN, strBuffer.toString());
					}
				}else{
					response.setCode(OAuthRequestManager.TIMED_OUT);//vi request dang bi timeout
				}
				
			} catch (MalformedURLException e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " Exception : " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Exception : " + e.getMessage());
				isSuccess = false;
				response.setError(HTTPClient.ERR_INVALID_URL, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
			} catch (FileNotFoundException e) {
				// TODO: handle exception
				isSuccess = false;
				response.setError(HTTPClient.ERR_NOT_FOUND, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_RESOURCE_IN_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "FileNotFoundException - " + e.getMessage() + "/" + e.toString());
			} catch (SocketTimeoutException e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " Exception : SocketTimeoutException " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Exception : SocketTimeoutException " + e.getMessage());
				isSuccess = false;
				response.setError(HTTPClient.ERR_TIME_OUT, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_TIME_OUT_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
			} catch (UnknownHostException e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " UnknownHostException : " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " UnknownHostException : " + e.getMessage());
				isSuccess = false;
				if (isRetryRequest){
					isRetry = true;
				}
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
			} catch (ConnectException e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				isSuccess = false;
				if (isRetryRequest){
					isRetry = true;
				}
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECTION), e.getMessage() + "/" + e.toString());
			} catch (IOException e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				isSuccess = false;
				if (isRetryRequest){
					isRetry = true;
				}
				response.setError(HTTPClient.ERR_UNKNOWN, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
			} catch (Exception e) {
				MyLog.e("HttpAsyncTask", DateUtils.now() + " Throwable : " + e.getMessage());
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Throwable : " + e.getMessage());
				isSuccess = false;
				isRetry = false;
				response.setError(HTTPClient.ERR_UNKNOWN, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
			} finally {						
				if (connection != null){
					MyLog.i(LOG_TAG, "disconnect");
					connection.disconnect();
				}
			}
		}while (countRetry <= NUM_RETRY && isRetry);
		
		HTTPListenner listenner = null;
		boolean isAlive = true;
		if (response != null) {
			listenner = response.getObserver();
			isAlive = response.request.isAlive();
		}
		if (listenner != null && isAlive) {
			//if(!GlobalUtil.checkActionSave(request.getAction())){
				if (isSuccess) {
					listenner.onReceiveData(response);
				} else {
					listenner.onReceiveError(response);
				}
			//}
		}
		return null;
	}
		
	/**
	 * send data
	 * @author: AnhND
	 * @param outputStream
	 * @param dataSupplier
	 * @throws IOException
	 * @return: void
	 * @throws:
	 */
	void writeData(OutputStream outputStream, DataSupplier dataSupplier) throws IOException{
		Data data = new Data(); 
		while (true){
			dataSupplier.getNextPart(data);
			if (data.buffer != null && data.length > 0) {
				outputStream.write(data.buffer, 0, data.length);
				outputStream.flush();
				dataSupplier.releaseData();
			}
			if (data.isFinish){
				break;
			}
		}
		
	}	
}
