/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class GROUP_LEVEL_DETAIL_TABLE extends ABSTRACT_TABLE {

	public static final String GROUP_LEVEL_DETAIL_ID = "GROUP_LEVEL_DETAIL_ID";
	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String UOM = "UOM";
	public static final String VALUE_TYPE = "VALUE_TYPE";
	public static final String VALUE = "VALUE";
	public static final String IS_REQUIRED = "IS_REQUIRED";
	public static final String STATUS = "STATUS";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	
	private static final String TABLE_NAME = "GROUP_MAPPING_TABLE";
	public GROUP_LEVEL_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { GROUP_LEVEL_DETAIL_ID, GROUP_LEVEL_ID, 
				PRODUCT_ID, UOM, VALUE_TYPE, VALUE, IS_REQUIRED, STATUS, ORDER_NUMBER,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Lay ds chi tiet muc 1 muc
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<GroupLevelDTO>
	 * @throws:  
	 * @param groupLevelId
	 * @return
	 */
	public ArrayList<GroupLevelDetailDTO> getLevelDetailsOfGroupLevel(long groupLevelId) {
		ArrayList<GroupLevelDetailDTO> result = new ArrayList<GroupLevelDetailDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT * FROM GROUP_LEVEL_DETAIL GLD ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("		AND GLD.STATUS = 1 ");
		sql.append("		AND GLD.GROUP_LEVEL_ID = ? ");
		params.add(String.valueOf(groupLevelId));
		sql.append("	ORDER BY GLD.IS_REQUIRED DESC ");
		
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						GroupLevelDetailDTO detailDTO = new GroupLevelDetailDTO();
						detailDTO.initFromCursor(c);
						result.add(detailDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.d("getLevelDetailsOfGroupLevel: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		return result;
	}
}
