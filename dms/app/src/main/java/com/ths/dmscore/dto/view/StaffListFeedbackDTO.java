package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cho popup ds nhan vien chon de them moi van de
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class StaffListFeedbackDTO {
	public ArrayList<ShopProblemDTO> lstShop = new ArrayList<ShopProblemDTO>();
//	public List<DisplayProgrameItemDTO> lstStaff = new ArrayList<DisplayProgrameItemDTO>();
	// ds nhan vien
	public ArrayList<StaffFeedbackDTO> lstStaff = new ArrayList<StaffFeedbackDTO>();
	// ds loai nhan vien
	public ArrayList<DisplayProgrameItemDTO> lstTypeStaff = new ArrayList<DisplayProgrameItemDTO>();

}
