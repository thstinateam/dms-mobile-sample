package com.ths.dmscore.lib.oAuth.oauth;

import com.ths.dmscore.lib.oAuth.builder.api.DefaultApi20;
import com.ths.dmscore.lib.oAuth.model.OAuthConstants;
import com.ths.dmscore.lib.oAuth.model.OAuthRequest;
import com.ths.dmscore.lib.oAuth.model.Response;
import com.ths.dmscore.lib.oAuth.model.Token;
import com.ths.dmscore.lib.oAuth.model.OAuthConfig;
import com.ths.dmscore.lib.oAuth.model.Verifier;

public class OAuth20ServiceImpl implements OAuthService {
	private static final String VERSION = "2.0";

	private final DefaultApi20 api;
	private final OAuthConfig config;

	/**
	 * Default constructor
	 * 
	 * @param api
	 *            OAuth2.0 api information
	 * @param config
	 *            OAuth 2.0 configuration param object
	 */
	public OAuth20ServiceImpl(DefaultApi20 api, OAuthConfig config) {
		this.api = api;
		this.config = config;
	}

	/**
	 * {@inheritDoc}
	 */
	public Token getAccessToken(Token requestToken, Verifier verifier) {
		OAuthRequest request = new OAuthRequest(api.getAccessTokenVerb(), api.getAccessTokenEndpoint());
		request.addQuerystringParameter(OAuthConstants.CLIENT_ID, config.getApiKey());
		request.addQuerystringParameter(OAuthConstants.CLIENT_SECRET, config.getApiSecret());
		request.addQuerystringParameter(OAuthConstants.GRANT_TYPE, config.getGrantType());
		request.addQuerystringParameter(OAuthConstants.RESPONSE_TYPE, config.getResponseType());
		Response response = request.send();
		return api.getJsonTokenExtractor().extract(response.getBody());
	}

	/**
	 * {@inheritDoc}
	 */
	public Token getRequestToken() {
		throw new UnsupportedOperationException("Unsupported operation, please use 'getAuthorizationUrl' and redirect your users there");
	}

	/**
	 * {@inheritDoc}
	 */
	public String getVersion() {
		return VERSION;
	}

	/**
	 * {@inheritDoc}
	 */
	public void signRequest(Token accessToken, OAuthRequest request) {
		request.addQuerystringParameter(OAuthConstants.ACCESS_TOKEN, accessToken.getToken());
	}

	/**
	 * {@inheritDoc}
	 */
	public String getAuthorizationUrl(Token requestToken) {
		return api.getAuthorizationUrl(config);
	}

}
