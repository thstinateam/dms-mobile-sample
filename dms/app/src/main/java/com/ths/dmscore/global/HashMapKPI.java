/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.global;

/**
 * Dsanh sach chuc nang log KPI
 * @author trungnt56
 * @version 1.0
 */
public enum HashMapKPI {
	GLOBAL_LOGIN("Login online"),
	GLOBAL_GET_FILE_DB("Tải file DB"),
	GLOBAL_SYN_DATA("Đồng bộ dữ liệu"),
	NVBH_DANHSACHKHACHHANG("NVBH : Danh sách khách hàng"),
	NVBH_CHAMTRUNGBAY("NVBH : Chấm trưng bày tại điểm bán"),
	NVBH_TINHKHUYENMAI("NVBH : Đơn đặt hàng - Tính khuyến mãi"),
	NVBH_LUUDONHANG("NVBH : Đơn đặt hàng - Lưu và chuyển đơn hàng"),
	NVBH_SUADONHANG("NVBH : Đơn đặt hàng - Sửa đơn hàng"),
	NVBH_THEMSANPHAM("NVBH : Đơn đặt hàng - Thêm sản phẩm"),
	NVBH_CHITIETKHACHHANG("NVBH : Xem chi tiết khách hàng"),
	NVBH_CHITIETSANPHAM("NVBH : Xem chi tiết sản phẩm"),
	NVBH_LAYDANHSACHHINHANH("NVBH : Danh sách album"),
	NVBH_XEMVIDEOSANPHAM("NVBH : Xem video sản phẩm (Chưa có Video ở client)"),
	NVBH_XEMHINHANHSANPHAM("NVBH : Xem hình ảnh sản phẩm (Chưa có hình ở client)"),
	NVBH_DANHSACHHINHANHTHEOALBUM("NVBH : Danh sách hình ảnh theo album"),
	NVBH_CHUPHINHTAIDIEMBAN("NVBH : Chụp hình tại điểm bán"),
	NVBH_DANHSACHDONHANG("NVBH : Danh sách đơn hàng"),
	NVBH_CHUPHINHKEYSHOP("NVBH : Chụp hình keyshop tại điểm bán"),
	NVBH_CHUPHINHTHIETBI("NVBH : Chụp hình thiết bị tại điểm bán"),
	NVBH_REPORT_KPI_SALE("NVBH : Báo cáo KPI nhân viên"),
	GSNPP_GIAMSATLOTRINHBANHANG("GSNPP : Giám sát lộ trình bán hàng"),
	GSNPP_BAOCAOTIENDONGAY("GSNPP : Báo cáo tiến độ ngày"),
	GSNPP_BAOCAOTIENDOLUYKE("GSNPP : Báo cáo tiến độ luỹ kế"),
	GSNPP_BAOCAOTIENDOMATHANGTRONGTAM("GSNPP : Báo cáo tiến độ Mặt hàng trọng tâm"),
	GSNPP_BAOCAOKHCHUAPSDS("GSNPP : Báo cáo Khách hàng chưa phát sinh doanh số"),
	GSNPP_XEMVITRINHANVIENTRENBANDO("GSNPP : Xem vị trí Nhân viên trên bản đồ"),
	GSNPP_CHAMCONGNHANVIEN("GSNPP : Chấm công nhân viên"),
	GSNPP_THEODOIKETQUADITUYENNHANVIEN("GSNPP : Theo dõi kết quả đi tuyến của nhân viên"),
	GSNPP_DANHSACHKHTRONGNGAYHUANLUYEN("GSNPP : Danh sách khách hàng trong ngày huấn luyện"),
	GSNPP_GHINHANKETQUAHUANLUYEN("GSNPP : Ghi nhận kết quả huấn luyện"),
	GSNPP_DANHSACHHINHANHTHEOALBUM("GSNPP : Danh sách hình ảnh theo album"),
	GSNPP_LAYDANHSACHHINHANH("GSNPP : Lấy danh sách album hình ảnh"),
	GSNPP_CHUPHINHTAIDIEMBAN("GSNPP : Chụp hình tại điểm bán"),
	GSNPP_REPORT_KPI_SALE("GSNPP : Báo cáo KPI nhân viên"),
	TBHV_BAOCAOTONGHOPTIENDONGAY("TBHV : Báo cáo tổng hợp tiến độ ngày"),
	TBHV_BAOCAOCHITIETTIENDONGAY("TBHV : Báo cáo chi tiết tiến độ ngày"),
	TBHV_BAOCAOTONGHOPTIENDOLUYKE("TBHV : Báo cáo tổng hợp tiến độ luỹ kế"),
	TBHV_BAOCAOCHITIETTIENDOLUYKE("TBHV : Báo cáo chi tiết tiến độ luỹ kế"),
	TBHV_BAOCAOTONGHOPKHCHUAPSDS("TBHV : Báo cáo Tổng hợp Khách hàng chưa Phát sinh doanh số"),
	TBHV_BAOCAOCHITIETKHCHUAPSDS("TBHV : Báo cáo chi tiết khách hàng chưa phát sinh doanh số"),
	TBHV_GIAMSATLOTRINHBANHANG("TBHV : Giám sát lộ trình bán hàng"),
	TBHV_XEMVITRIGSNPPTRENBANDO("TBHV - Xem vị trí GSNPP trên bản đồ"),
	TBHV_XEMVITRINVBHTRENBANDO("TBHV : Xem vị trí NVBH trên bản đồ"),
	GLOBAL_SYN_DATA_LOGIN("Đồng bộ dữ liệu login"),
	GLOBAL_LOGIN_FULL("Login online finish");
	String note;
	private HashMapKPI(String pNote){
		note = pNote;
	}

	public String getNote() {
		return note;
	}
}
