package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.TBHVController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.MenuAndSubMenu;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dms.R;

/**
 * Activity chinh: cua nhan vien truong ban hang vung
 *
 * @author : BangHN since : 1.1
 */
public class TBHVActivity extends RoleActivity {
	// confirm show camera
	public static final int CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR = 13;
	private final int ACTION_CANCEL_STANDARD = 14;
	private final int ACTION_END_VISIT_CUSTOMER = 15;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState,PriHashMap.PriMenu.TBHV_MENU, PriHashMap.PriForm.TBHV_MENU);
		// check time
		serverDate = getIntent().getExtras().getString(IntentConstants.INTENT_TIME);
		validateTimeClientServer();

		checkVisitFromActionLog();
		// check va gui toan bo log dang con ton dong
		TransactionProcessManager.getInstance().startChecking(TransactionProcessManager.SYNC_NORMAL);
		readAllowResetLocation();
	}

	/**
	 * Khoi tao menu chuong trinh
	 *
	 * @author : BangHN since : 4:41:56 PM
	 */
	protected void initMenuGroups() {
		lstMenuItems = new ArrayList<MenuItemDTO>();

		// menu va submenu Tong Quan
		MenuAndSubMenu listTongQuan = new MenuAndSubMenu();
		MenuItemDTO item1 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_SUMMARIZE),
				R.drawable.menu_report_icon);
		// submenu tien do ngay
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_PROGRESS_SALE_DAY),
				R.drawable.icon_calendar);
		// submenu tien do thang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_PROGRESS_SALE_MONTH),
				R.drawable.icon_accumulated);
		// submenu mat hang trong tam
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_FOCUS_PRODUCT),
				R.drawable.icon_list_star);
		// submenu CTTB
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_DISPLAY_PROGRAM),
//				R.drawable.icon_reminders);
		// submenu kh chua psds
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_CUSTOMER_NOT_PSDS),
				R.drawable.icon_order);
		// submenu SL theo nganh hang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_QUANTITY_FOLLOW_CATEGORY),
						R.drawable.icon_categories);
		// submenu ds theo nganh hang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_AMOUNT_FOLLOW_CATEGORY),
				R.drawable.icon_categories);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KEY_SHOP),
				R.drawable.icon_report_keyshop);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KPI),
				R.drawable.icon_kpi);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_SUP_ASO),
				R.drawable.icon_kpi);
		item1.setItems(listTongQuan.getListSubMenu());

		// menu va sub Giam sat Lo trinh
		MenuAndSubMenu listGSLoTrinh = new MenuAndSubMenu();
		MenuItemDTO item2 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_MENU_MONITORING),
				R.drawable.menu_customer_icon);
		item2.menuIndex = MENU_INDEX_SELECTED;
		// submenu GS Huan luyen NVBH
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_ROUTE_MANAGE),
				R.drawable.menu_customer_icon);
		// submenu Xem vi tri
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_VIEW_POSITION),
				R.drawable.icon_map);
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_VIEW_POSITION_STAFF),
				R.drawable.icon_map);
		// submenu Cham cong
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_TIMEKEEPING),
				R.drawable.icon_clock);
		// submenu Di tuyen
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_ON_ROUTE),
				R.drawable.icon_task);

		item2.setItems(listGSLoTrinh.getListSubMenu());

		// menu va submenu Huan luyen NVBH
		MenuAndSubMenu listHuanLuyenNVBH = new MenuAndSubMenu();
		MenuItemDTO item3 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_TRAINING_GSNPP_NEW),
				R.drawable.icon_training_new);
		// submenu Huan luyen nvbh
		listHuanLuyenNVBH.addSubMenu(StringUtil.getString(R.string.TEXT_TRAINING_SALEMAN),
				R.drawable.icon_calendar);
		// submenu Lich su huan luyen
		listHuanLuyenNVBH.addSubMenu(StringUtil.getString(R.string.TEXT_TRAINING_HISTORY),
				R.drawable.icon_training);

		item3.setItems(listHuanLuyenNVBH.getListSubMenu());

		// menu va sub menu Danh muc
		MenuAndSubMenu listCus = new MenuAndSubMenu();
		MenuItemDTO item4 =  new MenuItemDTO(StringUtil.getString(R.string.TEXT_SELECT_CUSTOMER_NAME),
				R.drawable.icon_customer_list);
		listCus.addSubMenu(StringUtil.getString(R.string.TEXT_CUSTOMER_LIST_VIEW),
				R.drawable.menu_customer_icon);
		// submenu Hinh Anh
		listCus.addSubMenu(StringUtil.getString(R.string.TEXT_IMAGE_CUSTOMER_LIST_VIEW),
				R.drawable.menu_picture_icon);
		item4.setItems(listCus.getListSubMenu());

		// menu va sub menu Danh muc
		MenuAndSubMenu listDanhMuc = new MenuAndSubMenu();
		MenuItemDTO item5 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_CATEGORY), R.drawable.icon_category);
		// submenu San pham
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_PRODUCT),
				R.drawable.icon_product_list);
		// submenu CTKM
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CTKM),
				R.drawable.menu_promotion_icon);
		// submenu CTTB
//		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CTTB),
//				R.drawable.menu_manage_icon);
		// submenu Hinh Anh
//		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_PICTURE),
//				R.drawable.menu_picture_icon);
		// submenu Tim Kiem Hinh Anh
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_SEARCH_IMAGE),
				R.drawable.icon_image_search);
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_LIST_KEY_SHOP),
				R.drawable.icon_document);
		// submenu Cong van
//		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_OFFICE_DOCUMENT),
//				R.drawable.icon_document);
		item5.setItems(listDanhMuc.getListSubMenu());

		// menu va submenu Theo doi khac phuc
		MenuAndSubMenu listTheoDoiKhacPhuc = new MenuAndSubMenu();
		MenuItemDTO item7 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_PROBLEMS_MANAGE),
				R.drawable.menu_problem_icon);
		// submenu Theo doi khac phuc
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_TAKE_PROBLEM),
				R.drawable.icon_feedback);
		// submenu Theo doi khac phuc
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_NEED_DO_IT),
				R.drawable.icon_feedback);

		item7.setItems(listTheoDoiKhacPhuc.getListSubMenu());

		MenuItemDTO item8 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_DYNAMIC_REPORT),
				R.drawable.icon_product_list);
		item1.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_TONGQUAN.getMenuName();
		item2.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_GIAMSAT.getMenuName();
		// menu huan luyen moi
		item3.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_HUANLUYEN_NEW.getMenuName();
		item4.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_KHACHHANG.getMenuName();
		item5.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_HINHANH.getMenuName();
		item7.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_THEODOIKHACPHUC.getMenuName();
		item8.controlOrdinal = PriHashMap.PriMenu.TBHV_MENU_KPI.getMenuName();
		lstMenuItems.add(item1);
		lstMenuItems.add(item2);
		lstMenuItems.add(item4);
		lstMenuItems.add(item5);
		lstMenuItems.add(item7);
		// add menu huan luyen moi
//		lstMenuItems.add(item3);
//		lstMenuItems.add(item8);

		initHashActionbar();

//		PriUtils.getInstance().checkMenu(lstMenuItems);
		setSelectMenu(item2.menuIndex);
	}

	@Override
	protected void initHashActionbar() {
		hashActionbar = new SparseArray<List<MenuTab>>();
		int index = 0;

		// Menu tien do
		List<MenuTab> listTiendobanhang = new ArrayList<MenuTab>();
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_TBHV_REPORT_PROGRESS_DATE, PriForm.TBHV_BAOCAOTIENDONGAY));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_MONTH, PriForm.TBHV_BAOCAOTIENDOLUYKE));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_SALES_FORCUS, PriForm.TBHV_BAOCAOCTTT));
//		listTiendobanhang.add(new MenuTab(ActionEventConstant.GET_TBHV_DIS_PRO_COM_PROG_REPORT, PriForm.TBHV_BAOCAOCTTB));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_TBHV_REPORT_CUSTOMER_NOT_PSDS, PriForm.TBHV_BAOCAOCHUAPSDS));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_GET_TBHV_TOTAL_CAT_QUANTITY_REPORT, PriForm.TBHV_BAOCAOSANLUONGNGANH));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_GET_TBHV_TOTAL_CAT_AMOUNT_REPORT, PriForm.TBHV_BAOCAODOANHSONGANH));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_KEY_SHOP, PriForm.TBHV_BAOCAOKEYSHOP));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_KPI_SUPVERVISOR, PriForm.TBHV_BAOCAOKPI));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_ASO_SUPVERVISOR, PriForm.TBHV_BAOCAO_ASO));
		hashActionbar.put(index, listTiendobanhang);
		index++;

		// Giam sat lo trinh
		List<MenuTab> listGiamsatlotrinh = new ArrayList<MenuTab>();
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.TBHV_ROUTE_SUPERVISION, PriForm.TBHV_GIAMSAT_LOTRINH_HUANLUYEN));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.TBHV_SUPERVISE_GSNPP_POSITION, PriForm.TBHV_THEODOIVITRIGSNPP));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.SUPERVISE_STAFF_POSITION, PriForm.TBHV_THEODOIVITRINHANVIEN));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.GSNPP_GET_LIST_SALE_FOR_ATTENDANCE, PriForm.TBHV_CHAMCONG));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_VISIT_CUSTOMER_ON_PLAN, PriForm.TBHV_DITUYEN));
//		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.TBHV_ATTENDANCE, PriForm.TBHV_CHAMCONG));
//		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.TBHV_VISIT_CUS_NOTIFICATION, PriForm.TBHV_DITUYEN));
		hashActionbar.put(index, listGiamsatlotrinh);
		index++;


		/*// Huan luyen
		List<MenuTab> listHuanluyen = new ArrayList<MenuTab>();
		listHuanluyen.add(new MenuTab(ActionEventConstant.TBHV_TRAINING_PLAN, PriForm.TBHV_HUANLUYEN_KEHOACH));
		listHuanluyen.add(new MenuTab(ActionEventConstant.TBHV_PLAN_TRAINING_HISTORY_ACC, PriForm.TBHV_HUANLUYEN_LUYKE));
		hashActionbar.put(index, listHuanluyen);
		index++;*/
		// Khach hang
		List<MenuTab> listCus = new ArrayList<MenuTab>();
		listCus.add(new MenuTab(ActionEventConstant.TBHV_GO_TO_CUSTOMER_SALE_LIST, PriForm.GSNPP_DSKHACHHANG));
		listCus.add(new MenuTab(ActionEventConstant.GET_LIST_IMAGE_OF_TBHV, PriForm.TBHV_HINHANH_DANHSACHHINHANH));
		hashActionbar.put(index, listCus);
		index++;

		// Danh muc
		List<MenuTab> listDanhmuc = new ArrayList<MenuTab>();
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_TBHV_PRODUCT_LIST, PriForm.TBHV_DANHMUC_DSSANPHAM));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_TBHV_PROMOTION_PROGRAM, PriForm.TBHV_DANHMUC_CTKM));
//		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_TBHV_DISPLAY_PROGRAM, PriForm.TBHV_BAOCAOCTTB));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_SEARCH_IMAGE_TBHV, PriForm.TBHV_HINHANH_TIMKIEMHINHANH));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_KEY_SHOP_LIST_VIEW, PriForm.GSNPP_DANHSACHCLB));
//		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_DOCUMENT, PriForm.TBHV_DANHMUC_CONGVAN));
		hashActionbar.put(index, listDanhmuc);
		index++;



		// Theo doi khac phuc
		List<MenuTab> listTheodoikhacphuc = new ArrayList<MenuTab>();
		listTheodoikhacphuc.add(new MenuTab(ActionEventConstant.GO_TO_TBHV_FOLLOW_LIST_PROBLEM, PriForm.TBHV_THEODOIKHACPHUC));
		listTheodoikhacphuc.add(new MenuTab(ActionEventConstant.NOTE_LIST_VIEW, PriForm.TBHV_CANTHUCHIEN));
		hashActionbar.put(index, listTheodoikhacphuc);
		index++;
//
//		// Huan luyen moi
//		List<MenuTab> listHuanluyenNew = new ArrayList<MenuTab>();
//		listHuanluyenNew.add(new MenuTab(
//				ActionEventConstant.TBHV_GSNPP_TRAINING_PLAN,
//				PriForm.TBHV_HUANLUYEN_KEHOACH));
//		listHuanluyenNew.add(new MenuTab(
//				ActionEventConstant.TBHV_PLAN_TRAINING_HISTORY_ACC,
//				PriForm.TBHV_HUANLUYEN_LUYKE));
//		hashActionbar.put(index, listHuanluyenNew);
//		index++;

		// KPI
//		List<MenuTab> listKPI = new ArrayList<MenuTab>();
//		listKPI.add(new MenuTab(ActionEventConstant.GO_TO_KPI,
//				PriForm.TBHV_DANHSACHKPI));
//		hashActionbar.put(index, listKPI);
	}

	@Override
	protected void getActionWhenClickMenu(int action) {
		// TODO Auto-generated method stub
		handleSwitchFragment(null, action, TBHVController.getInstance());
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() <= 1) {
			// finish();
			int handleBack = -1;
//			if (GlobalUtil.getTag(TBHVGsnppNvbhPositionView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				TBHVGsnppNvbhPositionView frag = (TBHVGsnppNvbhPositionView)
//						findFragmentByTag(GlobalUtil.getTag(TBHVGsnppNvbhPositionView.class));
//				if (frag != null) {
//					handleBack = frag.onBackPressed();
//				}
//			}
			if (handleBack == -1) {
				GlobalUtil.showDialogConfirm(this,
						StringUtil.getString(R.string.TEXT_CONFIRM_EXIT),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
						CONFIRM_EXIT_APP_OK,
						StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
						CONFIRM_EXIT_APP_CANCEL, null);
			}

		} else if (fm.getBackStackEntryCount() > 1) {
			int handleBack = -1;
			// dinh nghia cac luong xu ly su kien back dac biet
//			if (GlobalUtil.getTag(TBHVGsnppNvbhPositionView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				TBHVGsnppNvbhPositionView frag = (TBHVGsnppNvbhPositionView)
//						findFragmentByTag(GlobalUtil.getTag(TBHVGsnppNvbhPositionView.class));
//				if (frag != null) {
//					handleBack = frag.onBackPressed();
//				}
//			}else
//			if (GlobalUtil.getTag(GSNPPPostFeedbackView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				GSNPPPostFeedbackView view = (GSNPPPostFeedbackView) getFragmentManager()
//						.findFragmentByTag(GlobalUtil.getTag(GSNPPPostFeedbackView.class));
//				if (view != null) {
//					handleBack = view.onBackPressed();
//				}
//			}
			if (handleBack == -1) {
				GlobalInfo.getInstance().popCurrentTag();
				super.onBackPressed();
			}
		} else {
			GlobalInfo.getInstance().popCurrentTag();
			super.onBackPressed();
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case CONFIRM_EXIT_APP_OK:
//			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			TransactionProcessManager.getInstance().cancelTimer();
			//xu ly thoat ung dung
			processExitApp();
//			finish();
			break;
		case CONFIRM_EXIT_APP_CANCEL:
			break;
		case MENU_FINISH_VISIT:
			confirmEndVisitCustomer();
			break;
		case ACTION_END_VISIT_CUSTOMER:
			requestUpdateActionLog("0", "0", null, this);
//			if (GlobalUtil.getTag(TBHVTrainingListStandardView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				GlobalUtil.popBackStack(this);
//			}
			break;
		case MENU_FINISH_VISIT_CLOSE:
			this.showPopupConfirmCustomerClose();
			break;
		case CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR:
			// show camera
			ActionLogDTO al = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			LatLng cusLatLng = new LatLng(al.aCustomer.lat, al.aCustomer.lng);
			LatLng myLatLng = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			double distance = GlobalUtil
					.getDistanceBetween(cusLatLng, myLatLng);
			double distanceAllow = al.aCustomer.shopDTO.distanceOrder;
			if (myLatLng.lat > 0 && myLatLng.lng > 0 && cusLatLng.lat > 0
					&& cusLatLng.lng > 0 && distance <= distanceAllow) {
//				this.isTakePhotoFromMenuCloseCustomer = true;
				this.takenPhoto = GlobalUtil.takePhoto(this,
						RQ_TAKE_PHOTO);
			} else {
				String mess = StringUtil
						.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_1)
						+ " "
						+ al.aCustomer.customerCode
						+ " - "
						+ al.aCustomer.customerName
						+ " "
						+ StringUtil
								.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_2);

				GlobalUtil.showDialogConfirm(this, mess,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
						false);
			}
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 * kiem tra duoi DB lan login truoc co dang ghe tham khach hang nao ko
	 *
	 * @author TamPQ
	 */
	private void checkVisitFromActionLog() {
		Vector<String> v = new Vector<String>();
		v.add(IntentConstants.INTENT_STAFF_ID);
		v.add(String.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId()));
		handleViewEvent(v, ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG, TBHVController.getInstance());
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG: {
			ActionLogDTO action = (ActionLogDTO) modelEvent.getModelData();
			if (action != null) {
				if (StringUtil.isNullOrEmpty(action.endTime)
						&& "0".equals(action.objectType)) {
					// if(DateUtils.compareDate(action.startTime,
					// DateUtils.now()) == -1) {
					//
					// } else
					// if (DateUtils.compareDate(action.endTime,
					// DateUtils.now()) == 0){
					initMenuVisitCustomer(action.aCustomer.customerCode,
							action.aCustomer.customerName);
					if (action.prized)
						removeMenuCloseCustomer();
					// }
				}
				GlobalInfo.getInstance().getProfile()
						.setActionLogVisitCustomer(action);
			}
			break;
		}
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER:
			requestUpdateActionLog("0", "0", null, this);
//			if (GlobalUtil.getTag(TBHVTrainingListStandardView.class).equals(GlobalInfo
//					.getInstance().getCurrentTag())) {
//				GlobalUtil.popBackStack(this);
//			}
			break;
		case ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION:
			ApParamDTO result = (ApParamDTO) modelEvent.getModelData();
			if (result != null
					&& !StringUtil.isNullOrEmpty(result.getApParamCode())) {
				try {
					GlobalInfo.getInstance().setAllowResetLocation(
							Integer.parseInt(result.getApParamCode()));
				} catch (Exception ex) {
					// TODO: handle exception
//					showDialog("asdasd");
				}
			} else {
				GlobalInfo.getInstance().setAllowResetLocation(-1);
			}
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Xac nhan muon ket thuc ghe tham ko
	 *
	 * @author: dungdq3
	 * @since: 1:46:06 PM Dec 8, 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmEndVisitCustomer() {
		// TODO Auto-generated method stub
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if(action != null) {
			if (action.objectType.equals("0")
					&& StringUtil.isNullOrEmpty(action.endTime)) {
				SpannableObject textConfirmed = new SpannableObject();
				textConfirmed.addSpan(StringUtil
						.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				textConfirmed
				.addSpan(" "+ action.aCustomer.customerCode,
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(" - ",
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(action.aCustomer.customerName,
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(" "+StringUtil.getString(R.string.TEXT_IN)+" ",
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				textConfirmed.addSpan(
						DateUtils.getVisitTime(action.startTime, DateUtils
								.getVisitEndTime(action.aCustomer)),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(StringUtil
						.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				GlobalUtil.showDialogConfirm(this,
						textConfirmed.getSpan(),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_END_VISIT_CUSTOMER,
						StringUtil.getString(R.string.TEXT_BUTTON_CANCEL), ACTION_CANCEL_STANDARD, null);
			}
		}
	}

	/**
	 * Hien thi dialog dong cua
	 *
	 * @author: dungdq3
	 * @since: 1:38:46 PM Dec 8, 2014
	 * @return: void
	 * @throws:
	 */
	private void showPopupConfirmCustomerClose() {
		// TODO Auto-generated method stub
		GlobalUtil
		.showDialogConfirm( this,
				getString(R.string.TEXT_NOTIFY_DISPLAY_CAMERA_CLICK_BUTTON_CUSTOMER_CLOSE_THE_DOOR),
				"OK", CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR, "", 0,
				null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			String filePath = "";
			switch (requestCode) {
			case RQ_TAKE_PHOTO: {
					if (resultCode == RESULT_OK && takenPhoto != null) {
						filePath = takenPhoto.getAbsolutePath();
						ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
								this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
						validator.setDataIntent(data);
						if (validator.execute()) {
							requestUpdateActionLog("1", "0", this);
							updateTakenPhoto();
						}
					}
				}
				break;
			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;

			}


		} catch (Exception ex) {
			ServerLogger.sendLog("ActivityState", "GlobalBaseActivity : "
					+ VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}


	 /**
	 * update taken photo
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void updateTakenPhoto() {
		// TODO Auto-generated method stub
		this.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		if (this.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(this.takenPhoto.getName());

			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(this.takenPhoto.getAbsolutePath());
		}

		MediaItemDTO dto = new MediaItemDTO();

		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();

		try {
			dto.objectId = Long.parseLong(action.aCustomer.getCustomerId());
			dto.objectType = 0;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = this.takenPhoto.getAbsolutePath();
			dto.thumbUrl = this.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
			dto.fileSize = this.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
//			dto.type = 1;
			dto.status = 1;

			if (!StringUtil.isNullOrEmpty(action.aCustomer.getShopId())) {
				dto.shopId = Integer.valueOf(action.aCustomer.getShopId());
			}
			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(action.aCustomer.getCustomerId());
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));

			if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
				viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
				viewData.add(action.aCustomer.customerCode);
			}
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
//			viewData.add(IntentConstants.INTENT_TYPE);
//			viewData.add("1");
		} catch (Exception e1) {
			MyLog.e("updateTakenPhoto", "fail", e1);
		}

		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	@Override
	protected void showDetailsChild(int groupPos, int childPos) {
		// TODO Auto-generated method stub
		List<MenuTab> listSubMenu = hashActionbar.get(groupPos);
		MenuTab tab = listSubMenu.get(childPos);
		getActionWhenClickMenu(tab.action);

	}
	/**
	 * Doc cau hinh cho phep reset vi tri
	 *
	 * @author: DungNX
	 */
	private void readAllowResetLocation() {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_CODE, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopCode());
		handleViewEventWithOutAsyntask(b, ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION, UserController.getInstance());
	}
	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			// lay lai thong tin customer info
			readAllowResetLocation();
			break;
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}
}
