/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.ths.dmscore.dto.view.ImageInfoShopDTO;
import com.ths.dmscore.dto.view.TBHVListImageDTOView;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.ShopProblemDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Luu thong tin NPP
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class SHOP_TABLE extends ABSTRACT_TABLE {
	// id npp
	public static final String SHOP_ID = "SHOP_ID";
	// ma NPP
	public static final String SHOP_CODE = "SHOP_CODE";
	// ten NPP
	public static final String SHOP_NAME = "SHOP_NAME";
	// id NPP cha
	public static final String PARENT_SHOP_ID = "PARENT_SHOP_ID";
	// duong
	public static final String STREET = "STREET";
	// phuong
	public static final String WARD = "WARD";
	// quan/huyen
	public static final String DISTRICT = "DISTRICT";
	// tinh
	public static final String PROVINCE = "PROVINCE";
	// ma vung
	public static final String AREA_CODE = "AREA_CODE";
	// nuoc
	public static final String COUNTRY = "COUNTRY";
	// sdt ban
	public static final String PHONE = "PHONE";
	// di dong
	public static final String TELEPHONE = "TELEPHONE";
	// loai NPP, 1: NPP (default), 2: VNM
	public static final String SHOP_TYPE = "SHOP_TYPE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// loai kenh
	public static final String CHANNEL_TYPE_ID = "CHANNEL_TYPE_ID";
	// email giam sat NPP
	public static final String EMAIL = "EMAIL";
	// duong dan den NPP
	public static final String SHOP_PATH = "SHOP_PATH";
	public static final String LAT = "LAT";
	public static final String LNG = "LNG";

	private static final String ROLE_TABLE = "ROLE";

	public SHOP_TABLE(SQLiteDatabase mDB) {
		this.tableName = ROLE_TABLE;
		this.columns = new String[] { SHOP_ID, SHOP_CODE, SHOP_NAME,
				PARENT_SHOP_ID, STREET, WARD, DISTRICT, PROVINCE, AREA_CODE,
				COUNTRY, PHONE, TELEPHONE, SHOP_TYPE, CREATE_USER, UPDATE_USER,
				CREATE_DATE, UPDATE_DATE, CHANNEL_TYPE_ID, EMAIL, SHOP_PATH,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((ShopDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(ShopDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		ShopDTO dtoShop = (ShopDTO) dto;
		ContentValues value = initDataRow(dtoShop);
		String[] params = { "" + dtoShop.shopId };
		return update(value, SHOP_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(SHOP_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		ShopDTO dtoRole = (ShopDTO) dto;
		String[] params = { "" + dtoRole.shopId };
		return delete(SHOP_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: TruongHN
	 * @param id
	 * @return: RoleDTO
	 * @throws:
	 */
	public ShopDTO getRowById(String id) {
		ShopDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(SHOP_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private ShopDTO initDTOFromCursor(Cursor c) {
		ShopDTO dto = new ShopDTO();
		dto.shopId = (CursorUtil.getInt(c, SHOP_ID));
		dto.shopCode = (CursorUtil.getString(c, SHOP_CODE));
		dto.shopName = (CursorUtil.getString(c, SHOP_NAME));
		dto.parentShopId = (CursorUtil.getInt(c, PARENT_SHOP_ID));
		dto.street = (CursorUtil.getString(c, STREET));
		dto.ward = (CursorUtil.getString(c, WARD));
		dto.district = (CursorUtil.getString(c, DISTRICT));
		dto.province = (CursorUtil.getString(c, PROVINCE));
		dto.areaCode = (CursorUtil.getString(c, AREA_CODE));
		dto.country = (CursorUtil.getString(c, COUNTRY));
		dto.phone = (CursorUtil.getString(c, PHONE));
		dto.telephone = (CursorUtil.getString(c, TELEPHONE));
		dto.shopType = (CursorUtil.getInt(c, SHOP_TYPE));
		dto.createUser = (CursorUtil.getString(c, CREATE_USER));
		dto.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dto.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dto.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dto.channelTypeId = (CursorUtil.getInt(c, CHANNEL_TYPE_ID));
		dto.email = (CursorUtil.getString(c, EMAIL));
		dto.shopPath = (CursorUtil.getString(c, SHOP_PATH));

		return dto;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: Vector<ShopDTO>
	 * @throws:
	 */
	public Vector<ShopDTO> getAllRow() {
		Vector<ShopDTO> v = new Vector<ShopDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				ShopDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initDTOFromCursor(c);
						v.addElement(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(ShopDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(SHOP_CODE, dto.shopCode);
		editedValues.put(SHOP_NAME, dto.shopName);
		editedValues.put(PARENT_SHOP_ID, dto.parentShopId);
		editedValues.put(STREET, dto.street);
		editedValues.put(WARD, dto.ward);
		editedValues.put(DISTRICT, dto.district);
		editedValues.put(PROVINCE, dto.province);
		editedValues.put(AREA_CODE, dto.areaCode);
		editedValues.put(COUNTRY, dto.country);
		editedValues.put(PHONE, dto.phone);
		editedValues.put(TELEPHONE, dto.telephone);
		editedValues.put(SHOP_TYPE, dto.shopType);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(CHANNEL_TYPE_ID, dto.channelTypeId);
		editedValues.put(EMAIL, dto.email);
		editedValues.put(SHOP_PATH, dto.shopPath);

		return editedValues;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param string
	 * @return
	 * @return: LatLng
	 * @throws:
	 */

	public LatLng getPositionOfShop(String shopId) {
		ArrayList<String> params = new ArrayList<String>();
		String sql = "SELECT LAT, LNG FROM SHOP WHERE SHOP_ID = ? AND STATUS = 1";
		params.add(shopId);
		Cursor c = null;
		LatLng position = new LatLng();
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					position.lat = CursorUtil.getDouble(c, LAT);
					position.lng = CursorUtil.getDouble(c, LNG);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return position;
	}

	/**
	 * Lay ds shop cha cua 1 shop va de quy toi khi ko con shop cha thi thoi
	 *
	 * @author: Nguyen Thanh Dung
	 * @param string
	 * @return
	 * @return: LatLng
	 * @throws:
	 */

	public ArrayList<String> getShopRecursive(String shopId) throws Exception{
		String sql = "SELECT SHOP_ID, PARENT_SHOP_ID FROM SHOP WHERE STATUS = 1 AND SHOP_ID = ?";

		Cursor c = null;
		String[] params = { shopId };
		ArrayList<String> shopIdArray = new ArrayList<String>();
		if (!StringUtil.isNullOrEmpty(shopId) && !"null".equals(shopId)) {
			shopIdArray.add(shopId);
		}else {
			return shopIdArray;
		}

		try {
			c = rawQuery(sql, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String parentId = CursorUtil.getString(c, PARENT_SHOP_ID);

						ArrayList<String> tempArray = getShopRecursive(parentId);
						shopIdArray.addAll(tempArray);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return shopIdArray;
	}

	/**
	 * Lay ds shop con cua 1 shop va de quy toi khi ko con shop con thi thoi
	 *
	 * @author: Nguyen Thanh Dung
	 * @param string
	 * @return
	 * @return: LatLng
	 * @throws:
	 */

//	public ArrayList<String> getShopRecursiveReverse(String parentShopId) {
//		String sql = "SELECT SHOP_ID, PARENT_SHOP_ID FROM SHOP WHERE STATUS = 1 AND PARENT_SHOP_ID = ?";
//
//		Cursor c = null;
//		String[] params = { parentShopId };
//		ArrayList<String> shopIdArray = new ArrayList<String>();
//		if (!StringUtil.isNullOrEmpty(parentShopId)
//				&& !"null".equals(parentShopId)) {
//			shopIdArray.add(parentShopId);
//		}
//		try {
//			c = rawQuery(sql, params);
//			if (c != null) {
//				if (c.moveToFirst()) {
//					do {
//						String shopId = CursorUtil.getString(c, SHOP_ID);
//
//						ArrayList<String> tempArray = getShopRecursiveReverse(shopId);
//						shopIdArray.addAll(tempArray);
//					} while (c.moveToNext());
//				}
//			}
//		} catch (Exception ex) {
//		} finally {
//			try {
//				if (c != null) {
//					c.close();
//				}
//			} catch (Exception e) {
//			}
//		}
//
//		return shopIdArray;
//	}

	/**
	 * Lay khoang cach dat hang
	 *
	 * @author: TamPQ
	 * @return
	 * @return: intvoid
	 * @throws:
	 */
	public double getShopDistance(String shopId) throws Exception {
		double distance = 0;
		StringBuffer sql = new StringBuffer();
		sql.append(" select DISTANCE_ORDER from shop where shop_id = ? ");
		Cursor cursor = null;
		try {
			cursor = rawQuery(sql.toString(), new String[] { shopId });
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					distance = cursor.getDouble(0);
				}
			}
		}finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
			}
		}
		return distance;
	}

	/**
	 * Lay ds hinh anh cua TBHV
	 *
	 * @author: DungNX
	 * @param shopId
	 * @param numPerPage
	 * @param offset
	 * @return: TBHVListImageDTOView
	 * @throws:
	 */
	public TBHVListImageDTOView getListImageOfTBHV(String shopId,
												   int numPerPage, int offset) throws Exception {
		ArrayList<String> listShopId = getShopRecursiveReverse(shopId);
		String strListShop = TextUtils.join(",", listShopId);
		String now = DateUtils.now();
		String startOf2Month = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -2);
		String staffIdASM = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";
		String listGs = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    staff_id  ,	");
		sqlObject.append("	    staff_code  ,	");
		sqlObject.append("	    staff_name  ,	");
		sqlObject.append("	    npp_shop_id  ,	");
		sqlObject.append("	    npp_shop_code  ,	");
		sqlObject.append("	    npp_shop_name  ,	");
		sqlObject.append("	    Sum(num_image) AS numImage	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (  SELECT	");
		sqlObject.append("	        NPP.*   ,	");
		sqlObject.append("	        (   SELECT	");
		sqlObject.append("	            SUM(NUM_ITEM)	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            (          SELECT	");
		sqlObject.append("	                CTT.CUSTOMER_ID as CUSTOMER_ID,	");
		sqlObject.append("	                CTT.STAFF_ID as STAFF_ID,	");
		sqlObject.append("	                CTT.STAFF_NAME as NVBH_STAFF_NAME,	");
		sqlObject.append("	                COUNT(MI.OBJECT_ID) NUM_ITEM	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                (SELECT	");
		sqlObject.append("	                    CT.CUSTOMER_ID as CUSTOMER_ID,	");
		sqlObject.append("	                    vp.STAFF_ID,	");
		sqlObject.append("	                    staff.staff_name	");
		sqlObject.append("	                FROM	");
		sqlObject.append("	                    visit_plan vp,	");
		sqlObject.append("	                    routing rt,	");
		sqlObject.append("	                    routing_customer rtc,	");
		sqlObject.append("	                    CUSTOMER CT,	");
		sqlObject.append("	                    staff	");
		sqlObject.append("	                WHERE	");
		sqlObject.append("	                    VP.ROUTING_ID = RT.ROUTING_ID	");
		sqlObject.append("	                    AND rt.routing_Id = rtc.routing_id	");
		sqlObject.append("	                    AND rtc.customer_id = ct.customer_id	");
		sqlObject.append("	                    AND rt.status = 1	");
		sqlObject.append("	                    AND ct.status = 1	");
		sqlObject.append("	                    AND substr(VP.FROM_DATE, 1, 10) <= substr(?, 1, 10)	");
		paramsObject.add(now);
		sqlObject.append("	                    AND (	");
		sqlObject.append("	                        DATE(VP.TO_DATE) >= DATE(?)	");
		paramsObject.add(now);
		sqlObject.append("	                        OR DATE(VP.TO_DATE) IS NULL	");
		sqlObject.append("	                    )	");
		sqlObject.append("	                    AND substr(RTC.START_DATE, 1, 10) <= substr(?, 1, 10)	");
		paramsObject.add(now);
		sqlObject.append("	                    AND (	");
		sqlObject.append("	                        DATE(RTC.END_DATE) >= DATE(?)	");
		paramsObject.add(now);
		sqlObject.append("	                        OR DATE(RTC.END_DATE) IS NULL	");
		sqlObject.append("	                    )	");
		sqlObject.append("	                    AND VP.STAFF_ID = STAFF.STAFF_ID	");
		sqlObject.append("	                    AND STAFF.STATUS = 1	");
		sqlObject.append("	                    AND ct.shop_id = NPP.npp_shop_id	");
		sqlObject.append("	                    AND staff.shop_id = NPP.npp_shop_id	");
		sqlObject.append("	                    AND staff.staff_id IN (SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlObject.append("	                    	AND PS.PARENT_STAFF_ID = NPP.staff_id AND PS.STATUS = 1 ");
		sqlObject.append("	                    	AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlObject.append("	                    ) CTT	");
		sqlObject.append("	            LEFT JOIN	");
		sqlObject.append("	                (	");
		sqlObject.append("	                    SELECT	");
		sqlObject.append("	                        *	");
		sqlObject.append("	                    FROM	");
		sqlObject.append("	                        media_item	");
		sqlObject.append("	                    WHERE	");
		sqlObject.append("	                        object_type in (	");
		sqlObject.append("	                            0,1,2, 4	");
		sqlObject.append("	                        )	");
		sqlObject.append("	                        AND STATUS = 1	");
		sqlObject.append("	                        AND TYPE = 1	");
		sqlObject.append("					   AND substr(create_date, 0, 11) >= ?");
		paramsObject.add(startOf2Month);
//		sqlObject.append("	                        AND dayInOrder(create_date) >= dayInOrder(?, 'start of month', '-2 month')	");
//		paramsObject.add(now);
		sqlObject.append("	                        AND media_type = 0	");
		sqlObject.append("	                ) MI	");
		sqlObject.append("	                    ON (CTT.CUSTOMER_ID = MI.object_id AND (CTT.STAFF_ID = MI.STAFF_ID OR MI.STAFF_ID = NPP.STAFF_ID) AND MI.shop_id = NPP.npp_shop_id) ");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1 = 1	");
		sqlObject.append("	            GROUP BY	");
		sqlObject.append("	                CTT.CUSTOMER_ID,	");
		sqlObject.append("	                CTT.STAFF_ID               )          ) NUM_IMAGE	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                (   SELECT	");
		sqlObject.append("	                    DISTINCT st.staff_id AS staff_id    ,	");
		sqlObject.append("	                    st.staff_name AS staff_name    ,	");
		sqlObject.append("	                    st.staff_code staff_code    ,	");
		sqlObject.append("	                    sh.shop_id npp_shop_id    ,	");
		sqlObject.append("	                    sh.shop_code npp_shop_code    ,	");
		sqlObject.append("	                    sh.shop_name npp_shop_name	");
		sqlObject.append("	                FROM	");
		sqlObject.append("	                    (    SELECT	");
		sqlObject.append("	                        LIST_NVBH.nvbh_shop_id SHOP_ID_GS     ,	");
		sqlObject.append("	                        st.*	");
		sqlObject.append("	                    FROM	");
		sqlObject.append("	                        staff st	");
		sqlObject.append("	                    INNER JOIN	");
		sqlObject.append("	                        (	");
		sqlObject.append("	                            SELECT	");
		sqlObject.append("	                                DISTINCT gs.staff_id staff_owner_id     ,	");
		sqlObject.append("	                                s.shop_id AS nvbh_shop_id	");
		sqlObject.append("	                            FROM	");
		sqlObject.append("	                                staff gs      ,	");
		sqlObject.append("	                                staff s      ,	");
		sqlObject.append("	                                channel_type c	");
		sqlObject.append("	                            WHERE	1=1 ");
		sqlObject.append("	                                AND gs.staff_id IN (	");
		sqlObject.append(listGs);
		sqlObject.append("	                                )	");
		sqlObject.append("	                                AND s.staff_id IN 	");
		sqlObject.append("										(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlObject.append("											AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
		sqlObject.append("											AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlObject.append("	                                AND s.staff_type_id = c.channel_type_id	");
		sqlObject.append("	                                AND s.shop_id IN (");
		sqlObject.append(strListShop);
		sqlObject.append(")	");
		sqlObject.append("	                                AND c.object_type IN (	");
		sqlObject.append("	                                    1,2	");
		sqlObject.append("	                                )	");
		sqlObject.append("	                                AND s.STATUS = 1	");
		sqlObject.append("	                                AND c.type = 2	");
		sqlObject.append("	                                AND c.STATUS = 1	");
		sqlObject.append("	                            GROUP BY gs.staff_id, s.shop_id	");
		sqlObject.append("	                        ) LIST_NVBH	");
		sqlObject.append("	                            ON LIST_NVBH.staff_owner_id = st.staff_id	");
		sqlObject.append("	                            AND st.STATUS = 1	");
		sqlObject.append("	                        ) st    ,	");
		sqlObject.append("	                    shop sh	");
		sqlObject.append("	                WHERE	");
		sqlObject.append("	                    st.staff_id IN (	");
		sqlObject.append("	                        SELECT	");
		sqlObject.append("	                            s.staff_id	");
		sqlObject.append("	                        FROM	");
		sqlObject.append("	                            staff s      ,	");
		sqlObject.append("	                            channel_type c	");
		sqlObject.append("	                        WHERE	");
		sqlObject.append("	                            s.staff_type_id = c.channel_type_id	");
		sqlObject.append("	                            AND c.object_type = 5	");
		sqlObject.append("	                            AND s.STATUS = 1	");
		sqlObject.append("	                            AND c.type = 2	");
		sqlObject.append("	                            AND c.STATUS = 1	");
		sqlObject.append("	                    )	");
		sqlObject.append("	                    AND st.STATUS = 1	");
		sqlObject.append("	                    AND sh.STATUS = 1	");
		sqlObject.append("	                    AND st.SHOP_ID_GS = sh.shop_id	");
		sqlObject.append("	                ORDER BY	");
		sqlObject.append("	                    st.staff_code   ) NPP  )	");
		sqlObject.append("	            GROUP BY	");
		sqlObject.append("	                staff_id ,	");
		sqlObject.append("	                npp_shop_id	");
		sqlObject.append("	            ORDER BY	");
		sqlObject.append("	                npp_shop_code  ,	");
		sqlObject.append("	                staff_name	");
		Cursor cursor = null;
		TBHVListImageDTOView data = new TBHVListImageDTOView();

		ArrayList<ImageInfoShopDTO> list = new ArrayList<ImageInfoShopDTO>();
		try {
			cursor = rawQueries(sqlObject.toString(), paramsObject);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						list.add(initImageShopFromCursor(cursor));
					} while (cursor.moveToNext());
				}
				data.listData = list;
			}
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
			}
		}

		return data;
	}

	/**
	 * Khoi tao gia tri ImageShop tu sqlLite
	 *
	 * @author: TruongHN
	 * @param c
	 * @return: ImageShop
	 * @throws:
	 */
	private ImageInfoShopDTO initImageShopFromCursor(Cursor c) {
		ImageInfoShopDTO dto = new ImageInfoShopDTO();
		dto.shopId = CursorUtil.getString(c, "npp_shop_id");
		dto.shopName = CursorUtil.getString(c, "npp_shop_name");
		dto.shopCode = CursorUtil.getString(c, "npp_shop_code");
		dto.supervisorId = CursorUtil.getString(c, "staff_id");
		dto.supervisorName = CursorUtil.getString(c, "staff_name");
		dto.numImages = CursorUtil.getInt(c, "numImage");
		return dto;
	}


	/**
	 * lay thong tin shop
	 * @author: DungNX
	 * @param shopId
	 * @return
	 * @return: ShopDTO
	 * @throws Exception
	 * @throws:
	*/
	public ShopDTO getShopInfo(String shopId) throws Exception {
		ShopDTO shopDTO = null;

		String sql = "SELECT SHOP_ID, ifnull(SHOP_CODE, '') as SHOP_CODE, ifnull(SHOP_NAME, '') as SHOP_NAME, ifnull(ADDRESS, '') as STREET, lat, lng FROM SHOP WHERE SHOP_ID = ? ";

		Cursor c = null;
		String[] params = { shopId };
		try {
			c = rawQuery(sql, params);
			if (c != null) {
				if (c.moveToFirst()) {
					shopDTO = initShopDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
			MyLog.v("Get Shop Info", ex.getMessage());
			throw ex;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return shopDTO;
	}

	/**
	 * khoi tao thong tin shop
	 * @author: DungNX
	 * @param c
	 * @return
	 * @return: ShopDTO
	 * @throws:
	*/
	private ShopDTO initShopDTOFromCursor(Cursor c) {
		ShopDTO dto = new ShopDTO();
		dto.shopId = CursorUtil.getInt(c, SHOP_ID);
		dto.shopCode = CursorUtil.getString(c, SHOP_CODE);
		dto.shopName = CursorUtil.getString(c, SHOP_NAME);
		dto.street = CursorUtil.getString(c, STREET);
		dto.shopLat = CursorUtil.getDouble(c, LAT);
		dto.shopLng = CursorUtil.getDouble(c, LNG);
		return dto;
	}

	/**
	 * lay thong tin shop
	 * @author: DungNX
	 * @param listShopId
	 * @return
	 * @return: ShopDTO
	 * @throws Exception
	 * @throws:
	*/
	public ArrayList<ShopDTO> getListShopInfo(String listShopId) throws Exception {
		ArrayList<ShopDTO> shopDTO = null;

		String sql = "SELECT SHOP_ID, ifnull(SHOP_CODE, '') as SHOP_CODE, ifnull(SHOP_NAME, '') as SHOP_NAME, ifnull(ADDRESS, '') as STREET, lat, lng FROM SHOP WHERE SHOP_ID in (" + listShopId + ") ";

		Cursor c = null;
		try {
			c = rawQuery(sql, null);
			if (c != null) {
				if (c.moveToFirst()) {
					shopDTO = new ArrayList<ShopDTO>();
					do {
						shopDTO.add(initShopDTOFromCursor(c));
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.v("Get Shop Info", ex.getMessage());
			throw ex;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return shopDTO;
	}



	 /**
	 * Lay shop con
	 * @author: Tuanlt11
	 * @param parentShopId
	 * @return
	 * @return: ArrayList<String>
	 * @throws:
	*/
	public ArrayList<String> getGrandShopReverse(String parentShopId) {
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		sqlObject.append("	select shop_id from shop   ");
		sqlObject.append("  where parent_shop_id = ?");
		param.add(parentShopId);
		Cursor c = null;
		ArrayList<String> shopIdArray = new ArrayList<String>();
		try {
			c = rawQueries(sqlObject.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String shopId = CursorUtil.getString(c, SHOP_ID);
						shopIdArray.add(shopId);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return shopIdArray;
	}

	/**
	 * lay danh sach shop con theo shop type
	 *
	 * @author: dungdq3
	 * @since: 2:45:23 PM Mar 23, 2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @param parentShopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getShopRecursiveReverse(String parentShopId) throws Exception {
		String roleID = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT s.shop_id ");
		varname1.append("FROM   shop s ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND s.parent_shop_id = ? ");
		varname1.append("       AND NOT EXISTS (SELECT 1 ");
		varname1.append("                       FROM   org_access_except oae, ");
		varname1.append("                              role_permission_map rpm ");
		varname1.append("                       WHERE  1 = 1 ");
		varname1.append("                              AND oae.status = 1 ");
		varname1.append("                              AND rpm.status = 1 ");
		varname1.append("                              AND rpm.role_id = ? ");
		varname1.append("                              AND oae.shop_id = s.shop_id ");
		varname1.append("                              AND rpm.permission_id = oae.permission_id)");

		Cursor c = null;
		String[] params = { parentShopId, roleID };
		ArrayList<String> shopIdArray = new ArrayList<String>();
		if (!StringUtil.isNullOrEmpty(parentShopId)
				&& !"null".equals(parentShopId)) {
//			if(!StringUtil.isNullOrEmpty(getShopType(parentShopId))) {
				shopIdArray.add(parentShopId);
//			}
		}
		try {
			c = rawQuery(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String shopId = CursorUtil.getString(c, "SHOP_ID");
						// shopIdArray.add(id);

						ArrayList<String> tempArray = getShopRecursiveReverse(shopId);
						shopIdArray.addAll(tempArray);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return shopIdArray;
	}

	/**
	 * lay loai don vi cua shop
	 *
	 * @author: dungdq3
	 * @since: 3:16:14 PM Mar 13, 2015
	 * @return: int
	 * @throws:
	 * @param shopID
	 * @return
	 * @throws Exception
	 */
	public String getShopType(String shopID) throws Exception{
		String shopType = Constants.STR_BLANK;
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT s.[shop_type_id] as SHOP_TYPE_ID ");
		varname1.append("FROM   shop s, ");
		varname1.append("       shop_type st ");
		varname1.append("WHERE  st.status = 1 ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND st.specific_type = ? ");
		params.add(String.valueOf(UserDTO.NPP_SPECIFIC_TYPE));
		varname1.append("       AND s.shop_id = ? ");
		varname1.append("       AND st.[shop_type_id] = s.[shop_type_id];");
		params.add(shopID);
		Cursor c = null;
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						shopType = CursorUtil.getString(c, "SHOP_TYPE_ID");
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
				throw e;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}

		return shopType;
	}

	/**
	 * lay danh sach shop con loai la NPP
	 *
	 * @author: dungdq3
	 * @since: 5:10:53 PM Mar 23, 2015
	 * @return: ShopDTO
	 * @throws:
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ShopDTO> getListShopNPPRecursive(String shopId) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<ShopDTO> listShopDTO = new ArrayList<ShopDTO>();
		ArrayList<String> listShop = getShopRecursiveReverse(shopId);
		String listShopID = TextUtils.join(", ", listShop);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT s.shop_id as SHOP_ID, ");
		varname1.append("		s.shop_code as SHOP_CODE, ");
		varname1.append("		s.shop_name as SHOP_NAME ");
		varname1.append("FROM   shop s, ");
		varname1.append("		shop_type sht ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND    s.status = 1 ");
		varname1.append("       AND    sht.status = 1 ");
		varname1.append("       AND    sht.specific_type = ? ");
		params.add(String.valueOf(UserDTO.NPP_SPECIFIC_TYPE));
		varname1.append("       AND    sht.shop_type_id = s.shop_type_id ");
		varname1.append("       AND    s.shop_id IN ( ");
		varname1.append(listShopID);
		varname1.append(" ) ");
		varname1.append(" ORDER BY SHOP_CODE, ");
		varname1.append("          SHOP_NAME  ");

		Cursor c = null;
		try {
			c = this.rawQueries(varname1.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					ShopDTO item = new ShopDTO();
					item.parseDataFromCusor(c);
					listShopDTO.add(item);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listShopDTO;
	}

	ArrayList<String> shopIdArray = new ArrayList<String>();
	
	/**
	 * Lay danh sach parent_shop: shop dang nhap -> shop cha
	 * Chuc nang danh sach keyshop hinh anh 
	 * @author: yennth16
	 * @since: 10:41:30 08-07-2015
	 * @return: ArrayList<String>
	 * @throws:  
	 * @param parentShopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getShopParentByShopTypeRecursive(String parentShopId) throws Exception {
		String roleID = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT s.PARENT_SHOP_ID ");
		varname1.append("FROM   shop s ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND s.status = 1 ");
		varname1.append("       AND s.shop_id = ? ");
		varname1.append("       AND NOT EXISTS (SELECT 1 ");
		varname1.append("                       FROM   org_access_except oae, ");
		varname1.append("                              role_permission_map rpm ");
		varname1.append("                       WHERE  1 = 1 ");
		varname1.append("                              AND oae.status = 1 ");
		varname1.append("                              AND rpm.status = 1 ");
		varname1.append("                              AND rpm.role_id = ? ");
		varname1.append("                              AND oae.shop_id = s.shop_id ");
		varname1.append("                              AND rpm.permission_id = oae.permission_id)");

		Cursor c = null;
		String[] params = { parentShopId, roleID };
		if (!StringUtil.isNullOrEmpty(parentShopId)
				&& !"null".equals(parentShopId)) {
			shopIdArray.add(parentShopId);
		}
		try {
			c = rawQuery(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String shopId = CursorUtil.getString(c, "PARENT_SHOP_ID");
						if(!StringUtil.isNullOrEmpty(shopId)){
							getShopParentByShopTypeRecursive(shopId);
						}
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return shopIdArray;
	}


	 /**
	 * Lay shop cha mot cap
	 * @author: Tuanlt11
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: ArrayList<String>
	 * @throws:
	*/
	public String getParentShop(String shopId) throws Exception{
		String sql = "SELECT SHOP_ID, PARENT_SHOP_ID FROM SHOP WHERE STATUS = 1 AND SHOP_ID = ?";
		String parentId = "";
		Cursor c = null;
		String[] params = { shopId };

		try {
			c = rawQuery(sql, params);
			if (c != null) {
				if (c.moveToFirst()) {
					parentId = CursorUtil.getString(c, PARENT_SHOP_ID);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return parentId;
	}


	 /**
	 * Lay ds shop con cua mot shop, de quy toi cuoi cung
	 * @author: Tuanlt11
	 * @param shopId
	 * @return
	 * @return: ArrayList<String>
	 * @throws:
	*/
	public ArrayList<ShopProblemDTO> getListShopProblem(String shopId, String shopCode, String shopName, String parentShopId, int level) {
		String sql = "SELECT SHOP_ID,SHOP_NAME,SHOP_CODE, PARENT_SHOP_ID FROM SHOP WHERE STATUS = 1 AND PARENT_SHOP_ID = ? ORDER BY SHOP_CODE, SHOP_NAME";
		Cursor c = null;
		String[] params = { shopId };
		ArrayList<ShopProblemDTO> shopIdArray = new ArrayList<ShopProblemDTO>();
		if (!StringUtil.isNullOrEmpty(shopId)
				&& !"null".equals(shopId)) {
			ShopProblemDTO temp = new ShopProblemDTO();
			try {
				temp.shop.shopId = Integer.parseInt(shopId);
				temp.shop.shopCode = shopCode;
				temp.shop.shopName = shopName;
				temp.parentShopId = Integer.parseInt(parentShopId);
				temp.level = level;
			} catch (Exception e) {
				// TODO: handle exception
			}
			shopIdArray.add(temp);
		}
		try {
			c = rawQuery(sql, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String tempShopId = CursorUtil.getString(c, SHOP_ID);
						String temShopCode = CursorUtil.getString(c, SHOP_CODE);
						String tempShopName = CursorUtil.getString(c, SHOP_NAME);
						ArrayList<ShopProblemDTO> tempArray = getListShopProblem(tempShopId,temShopCode,tempShopName,parentShopId,level+1);
						shopIdArray.addAll(tempArray);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return shopIdArray;
	}


}
