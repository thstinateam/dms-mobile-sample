package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class ProblemsFeedBackDTO {
	public List<ProblemsFeedBackItem> listProblems;
	public int totalRows = 0;
	
	public class ProblemsFeedBackItem {
		// Noi dung
		public String content;
		// Loai van de
		public int type;
		// Ten loai van de
		public String typeName;
		// Ngay nhac nho
		public String remindDate;
		// ngay tao
		public String createDate;
		// done dayInOrder
		public String doneDate;
		// status
		public int status;
	}
	
	public ProblemsFeedBackDTO(){
		listProblems = new ArrayList<ProblemsFeedBackDTO.ProblemsFeedBackItem>();
	}
	
	public ProblemsFeedBackItem initDataFromCursor(Cursor c){
		ProblemsFeedBackItem item = new ProblemsFeedBackItem();
		item.remindDate = CursorUtil.getString(c, "REMIND_DATE");
		item.createDate = CursorUtil.getString(c, "CREATE_DATE");
		item.doneDate = CursorUtil.getString(c, "DONE_DATE");
		item.type = CursorUtil.getInt(c, "TYPE");
		item.typeName = CursorUtil.getString(c, "AP_PARAM_NAME");
		item.content = CursorUtil.getString(c, "CONTENT");
		item.status = CursorUtil.getInt(c, "RESULT", 1);			
		return item;
		
	}
}
