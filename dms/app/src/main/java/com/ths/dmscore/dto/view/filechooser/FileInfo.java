package com.ths.dmscore.dto.view.filechooser;

import java.io.File;

import com.ths.dmscore.util.FileUtil;

public class FileInfo {
    public static final int TYPE_FILE = 1;
    public static final int TYPE_FOLDER = 2;
    public final String extension;

    public File file;
    public int type;
    public String fileName;
    public boolean checked;
    public String fullPath;
	

    public FileInfo(String fullPath, String fileName, String extension, int type) {
    	this.fullPath = fullPath;
        this.extension = extension;
        File fileTemp = new File(fullPath);
        this.file = fileTemp.exists() ? fileTemp : null;
        this.type = type;
        this.fileName = fileName;
        this.checked = false; 
    }
    
    public FileInfo(File file, int type) {
    	try {
			this.fullPath = file.getCanonicalPath();
		} catch (Exception e) {
			this.fullPath = file.getAbsolutePath();
		}
    	this.extension = FileUtil.getFileExtension(file);
    	this.file = file;
    	this.type = type;
    	this.fileName = file.getName();
    	this.checked = false; 
    }
    
    @Override
    public boolean equals(Object fileInfo) {
    	boolean res = false;
    	if (fileInfo != null && fileInfo instanceof FileInfo) {
    		FileInfo file = (FileInfo) fileInfo;
    		res = this.fullPath.equals(file.fullPath);
		}
    	return res;
    }
    
    @Override
    public int hashCode() {
    	return super.hashCode();
    }
}