/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * DTO 1 row trong ds hinh anh cua NPP
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class ImageInfoShopDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	public String supervisorName;
	public String shopName;
	public String shopId;
	public String shopCode;
	public String supervisorId;
	public int numImages;
	public ImageInfoShopDTO(){};
}
