/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.List;
import java.util.Vector;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.FEEDBACK_STAFF_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;

/**
 *
 * Mo ta cho class
 *
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class FollowProblemItemDTO extends AbstractTableDTO {
	public long feedBackId;
	public String staffId;//ma nhan vien
	public String customerId;//ma khach hang
	public String productId;//ma san pham
	public String staffCode;//ma nhan vien
	public String staffName;//ten nhan vien
	public String customerCode;//ma khach hang
	public String customerName;//ten khach hang
	public String typeName;//ten loai van de
	public String content;//noi dung
	public int status;//trang thai
	public String createDate;//ngay tao
	public String remindDate;//ngay nhac nho
	public String doneDate;//ngay tao
	public int ischeck;//kiem tra da duyet
	public int numReturn;//so lan yeu cau lam lai
	public Vector<HistoryItemDTO> vHistory = new Vector<HistoryItemDTO>();
	public String houseNumber;//so nha
	public String street;//duong
	public String address;//dia chi
	public String updateUser; //ma so nguoi update
	public String updateDate;
	public long feedbackStaffId;
	public String customerInfo;
	public int type;//loai van de
	public List<AttachDTO> lstAttach = null;
	/**
	*  Mo ta chuc nang cua ham
	*  @author: ThanhNN8
	*  @param c
	*  @return: void
	*  @throws:
	*/
	public void initDateWithCursor(Cursor c) {
		feedBackId = CursorUtil.getLong(c, "FEEDBACK_ID");
		feedbackStaffId = CursorUtil.getLong(c, "FEEDBACK_STAFF_ID");
		staffId = CursorUtil.getString(c, "STAFF_ID");
		customerId = CursorUtil.getString(c, "CUSTOMER_ID");
		productId = CursorUtil.getString(c, "PRODUCT_ID");
		customerInfo = CursorUtil.getString(c, "CUSTOMER_INFO");

		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		address = CursorUtil.getString(c, "ADDRESS");
		houseNumber = CursorUtil.getString(c, "HOUSENUMBER");
		street = CursorUtil.getString(c, "STREET");
		content = CursorUtil.getString(c, "CONTENT");
		typeName = CursorUtil.getString(c, "AP_PARAM_NAME");
		type = CursorUtil.getInt(c, "TYPE");
		createDate = CursorUtil.getString(c, "CREATE_DATE");
		doneDate = CursorUtil.getString(c, "DONE_DATE");
		remindDate = CursorUtil.getString(c, "REMIND_DATE");
//		status = CursorUtil.getInt(c, "STATUS", 1);
		status = CursorUtil.getInt(c, "RESULT");
		numReturn = CursorUtil.getInt(c, "NUM_RETURN");
		updateUser = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		updateDate = DateUtils.now();
	}
	/**
	*  Mo ta chuc nang cua ham
	*  @author: ThanhNN8
	*  @param dto
	*  @return
	*  @return: JSONObject
	*  @throws:
	*/
	public JSONObject generateUpdateFollowProblemSql(FollowProblemItemDTO dto) {
		// TODO Auto-generated method stub
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME,
					FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.RESULT,
					dto.status, null));
			if (dto.status == FeedBackDTO.FEEDBACK_STATUS_CREATE) {
				detailPara.put(GlobalUtil.getJsonColumn(
						FEEDBACK_STAFF_TABLE.DONE_DATE, "",
						DATA_TYPE.NULL.toString()));
				detailPara.put(GlobalUtil.getJsonColumn(
						FEEDBACK_STAFF_TABLE.NUM_RETURN, dto.numReturn, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.UPDATE_DATE, dto.updateDate,null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.UPDATE_USER, dto.updateUser,null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_ID,
					dto.feedbackStaffId, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}
}
