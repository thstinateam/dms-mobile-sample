/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

/**
 * FeedbackStaffProductDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  10:08:40 21-05-2015
 */
public class FeedbackStaffProductDTO extends AbstractTableDTO {
	// id bang
	public long feedBackStaffProductId;
	// id feedback
	public long feedBackStaffId;
	// if staff
	public long staffId;
	// id san pham
	public long productId;
	// nguoi tao
	public String createUserId;
	// ngay tao
	public String createDate;
	// nguoi cap nhat
	public String updateUser;
	// ngay cap nhat
	public String updateDate;
}
