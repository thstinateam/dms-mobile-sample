/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.image;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.dto.view.PhotoThumbnailListDto;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * Man hinh chua cac album cua mot khach hang
 *
 * @author: SoaN
 * @version: 1.0
 * @since: Aug 1, 2012
 */

public class PhotoThumbnailListView extends BaseFragment implements OnItemClickListener, OnClickListener {
	private static final int ACTION_POSITION = 1;
	private static final int NUM_LOAD_MORE = 20;

	protected static final int[] IMAGE_IDS = { R.id.imgAlbumImage };

	private ImageAdapter thumbs = null;
	PhotoThumbnailListDto photoThumbnailListDto = new PhotoThumbnailListDto();
	private int numTopLoaded = 0;
	private boolean isAbleGetMore = true;
	private GridView gvImageView;
	Button btTakePhoto;
	TextView tvMaKH;
	TextView tvHoTenKH;
	private int totalImage;
	private String fromDate = ""; // tu ngay
	private String toDate = ""; // den ngay
	private String toDateForRequest = ""; // den ngay
	private String fromDateForRequest = ""; // den ngay
	int albumType = 0;
	private String shopId;
	int type;
	private long routingId = 0;
	private long cycleId = 0;
	boolean isTakePhoto = false;

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: ListAlbumUserView
	 * @throws:
	 */

	public static PhotoThumbnailListView getInstance(Bundle data) {
		PhotoThumbnailListView instance = new PhotoThumbnailListView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_photo_thumbnail_user_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		Bundle data = (Bundle) getArguments();
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String customerCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String customerName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		routingId = data.getLong(IntentConstants.INTENT_ROUTING_ID);
		cycleId = data.getLong(IntentConstants.INTENT_CYCLE_ID);
		shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		if (StringUtil.isNullOrEmpty(shopId)) {
			shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		}
		fromDate = data.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		toDate = data.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		AlbumDTO albumDto = (AlbumDTO) data.getSerializable(IntentConstants.INTENT_ALBUM_INFO);

		albumType = albumDto.getAlbumType();
		isTakePhoto = data.getBoolean(IntentConstants.INTENT_IS_TAKE_PHOTO, false);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DSHINHANH_CHITIETHINHANH);
			// Lay doi tuong
			gvImageView = (GridView) PriUtils.getInstance().findViewById(view, R.id.gvImageView, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_ALBUM_CHITIET,
					PriUtils.CONTROL_GRIDVIEW);
			btTakePhoto = (Button) PriUtils.getInstance().findViewByIdGone(view, R.id.btTakePhoto, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_ALBUM_CHUPHINH);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DSHINHANH_CHITIETHINHANH);
			gvImageView = (GridView) PriUtils.getInstance().findViewById(view, R.id.gvImageView, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_CHITIETALBUM_CHITIETHINHANH,
					PriUtils.CONTROL_GRIDVIEW);
			btTakePhoto = (Button) PriUtils.getInstance().findViewByIdGone(view, R.id.btTakePhoto, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_CHITIETALBUM_CHUPHINH);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.TBHV_DSHINHANH_CHITIETHINHANH);
			gvImageView = (GridView) PriUtils.getInstance().findViewById(view, R.id.gvImageView, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_ALBUM_CHITIET_CHITIETHINHANH,
					PriUtils.CONTROL_GRIDVIEW);
			btTakePhoto = (Button) PriUtils.getInstance().findViewByIdGone(view, R.id.btTakePhoto, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_ALBUM_CHITIET_CHUPHINH);
		}
		PriUtils.getInstance().setOnItemClickListener(gvImageView, this);
		PriUtils.getInstance().setOnClickListener(btTakePhoto, this);

		if (albumType == MediaItemDTO.TYPE_LOCATION_CLOSED
				|| (albumType == MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE && !isTakePhoto)) {
			PriUtils.getInstance().setStatus(btTakePhoto, PriUtils.INVISIBLE);
		}

		tvMaKH = (TextView) view.findViewById(R.id.tvMaKH);
		tvMaKH.setText(customerCode);
		tvHoTenKH = (TextView) view.findViewById(R.id.tvHoTenKH);
		tvHoTenKH.setText(customerName);

		if (photoThumbnailListDto.isFirstInit) {
			gvImageView.setAdapter(thumbs);
			thumbs.notifyDataSetChanged();
		} else {
			photoThumbnailListDto.setCustomerId(customerId);
			photoThumbnailListDto.setCustomerName(customerName);
			photoThumbnailListDto.setAlbumInfo(albumDto.clone2());
			photoThumbnailListDto.isFirstInit = true;
			if(thumbs == null){
				thumbs = new ImageAdapter(parent, photoThumbnailListDto.getAlbumInfo().getListPhoto());
			}
			gvImageView.setAdapter(thumbs);
			getListAlbumOfUser();
			totalImage = photoThumbnailListDto.getAlbumInfo().getNumImage();
		}
		type = data.getInt(IntentConstants.INTENT_TYPE, 0);

		updateTitle(totalImage);

		return v;
	}

	/**
	 * Request upload photo
	 *
	 * @author: PhucNT
	 * @param customerName
	 * @return: void
	 * @throws:
	 */
	private void updateTitle(int total) {
		SpannableObject spanObject = new SpannableObject();
		// bo so thu tu
//		if (GlobalInfo.getInstance().getProfile().getUserData().inheritSpecificType == UserDTO.TYPE_STAFF) {
//			if (type == 4) {
//				spanObject.addSpan(getResources().getString(R.string.TITLE_VIEW_ALBUM_DETAIL_USER));
//			} else {
//				spanObject.addSpan(getResources().getString(R.string.TITLE_VIEW_ALBUM_DETAIL_USER_02));
//			}
//		} else {
//			spanObject.addSpan(getString(R.string.TITLE_VIEW_GSNPP_ALBUM_LIST_USER_REVIEW));
//		}

		spanObject.addSpan(photoThumbnailListDto.getAlbumInfo().getAlbumTitle());
		spanObject.addSpan(" (");
		spanObject.addSpan(String.valueOf(total), ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.BOLD);
		spanObject.addSpan(")");
//		setTitleHeaderView(spanObject);
		parent.setTitleName(spanObject);

		// spanObject = new SpannableObject();
		// spanObject.addSpan(String.valueOf(total),
		// ImageUtil.getColor(R.color.RED), android.graphics.Typeface.BOLD);
		// spanObject.addSpan(" " +
		// photoThumbnailListDto.getAlbumInfo().getAlbumTitle(),
		// ImageUtil.getColor(R.color.BLACK), android.graphics.Typeface.BOLD);
		// tvNumImageInfo.setText(spanObject.getSpan());
	}

	/**
	 * get List Album Of User
	 *
	 * @author: PhucNT
	 * @param customerName
	 * @return: void
	 * @throws:
	 */
	private void getListAlbumOfUser() {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, photoThumbnailListDto.getCustomerId());
		bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		bundle.putInt(IntentConstants.INTENT_ALBUM_TYPE, photoThumbnailListDto.getAlbumInfo().getAlbumType());
		bundle.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, NUM_LOAD_MORE);
		bundle.putInt(IntentConstants.INTENT_PAGE, numTopLoaded / NUM_LOAD_MORE);

		if (albumType == 4) {
			String dateTimePattern = StringUtil.getString(R.string.TEXT_DATE_TIME_PATTERN);
			Pattern pattern = Pattern.compile(dateTimePattern);
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				String strTN = fromDate.trim();

				Matcher matcher = pattern.matcher(strTN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strTN, "");
						String strFindTN = StringUtil.dateToString(tn, "yyyy-MM-dd");
						fromDateForRequest = strFindTN;
						bundle.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE, fromDateForRequest);
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				String strDN = toDate.trim();

				Matcher matcher = pattern.matcher(strDN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date dn = StringUtil.stringToDate(strDN, "");
						String strFindDN = StringUtil.dateToString(dn, "yyyy-MM-dd");
						toDateForRequest = strFindDN;
						bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, toDateForRequest);
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			}

			bundle.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE, 
					Constants.STR_BLANK + photoThumbnailListDto.getAlbumInfo().getDisplayProgrameId());
			handleViewEventWithTag(bundle, ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME, SaleController.getInstance(), 0);
		} else {
			handleViewEventWithTag(bundle, ActionEventConstant.GO_TO_ALBUM_DETAIL_USER, SaleController.getInstance(), 0);
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME:
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_USER: {
			PhotoThumbnailListDto tempModel = (PhotoThumbnailListDto) modelEvent.getModelData();
			if (modelEvent.getActionEvent().tag == 0) {
				photoThumbnailListDto.getAlbumInfo().getListPhoto().clear();
				photoThumbnailListDto.getAlbumInfo().getListPhoto().addAll(tempModel.getAlbumInfo().getListPhoto());
				thumbs.notifyDataSetChanged();
				int size = tempModel.getAlbumInfo().getListPhoto().size();
				numTopLoaded = size;
				if (size < NUM_LOAD_MORE) {
					isAbleGetMore = false;
				}
			} else {
				if (tempModel.getAlbumInfo().getListPhoto().size() < NUM_LOAD_MORE) {
					isAbleGetMore = false;
				}
				int size = tempModel.getAlbumInfo().getListPhoto().size();
				numTopLoaded += size;

				photoThumbnailListDto.getAlbumInfo().getListPhoto().addAll(tempModel.getAlbumInfo().getListPhoto());

				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, photoThumbnailListDto.getAlbumInfo());
				// sendBroadcast(ActionEventConstant.GET_RESULT_MORE_PHOTOS,
				// bundle);

				FullImageView orderFragment = (FullImageView)
						findFragmentByTag(GlobalUtil.getTag(FullImageView.class));
				if (orderFragment != null) {
					orderFragment.receiveBroadcast(ActionEventConstant.GET_RESULT_MORE_PHOTOS, bundle);
				}
				thumbs.notifyDataSetChanged();
			}

			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		setMenuItemFocus(eventType);
		switch (eventType) {
		case ACTION_POSITION: {

			// gotoCustomerRouteView();
			break;
		}

		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btTakePhoto) {

			ListAlbumUserView orderFragment = (ListAlbumUserView)
					findFragmentByTag(GlobalUtil.getTag(ListAlbumUserView.class));
			if (orderFragment != null) {
				DisplayProgrameDTO dpTP = new DisplayProgrameDTO();
				dpTP.displayProgrameId = photoThumbnailListDto.getAlbumInfo().getDisplayProgrameId();
				dpTP.displayProgrameCode = photoThumbnailListDto.getAlbumInfo().getAlbumTitle();
				orderFragment.dpTakePhoto = dpTP;
				orderFragment.takePhoto(photoThumbnailListDto.getAlbumInfo().getAlbumType());
			}

		} else {
			super.onClick(v);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, photoThumbnailListDto.getAlbumInfo());
		bundle.putInt(IntentConstants.INTENT_ALBUM_INDEX_IMAGE, arg2);
		bundle.putString(IntentConstants.INTENT_FROM, this.getTAG());
		handleSwitchFragment(bundle, ActionEventConstant.ACTION_LOAD_IMAGE_FULL, UserController.getInstance());
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		super.handleErrorModelViewEvent(modelEvent);
	}

	public class ImageAdapter extends BaseAdapter {

		private ArrayList<PhotoDTO> list = new ArrayList<PhotoDTO>();

		// private Bitmap bitmap = null;
		// private Bitmap bmDefault =null;
		public ImageAdapter(Context c, ArrayList<PhotoDTO> list) {
			this.list = list;
			// bmDefault = BitmapFactory.decodeResource(c.getResources(),
			// R.drawable.album_default);
		}

		@Override
		public int getCount() {
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup pt) {
			View row = convertView;
			ViewHolder holder = null;

			if (convertView == null) {
				LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) parent)
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = layout.inflate(R.layout.layout_album_detail_user, pt, false);
				holder = new ViewHolder();
				holder.imageView = (ImageView) row.findViewById(R.id.imgAlbumImage);
				holder.titleAlbum = (TextView) row.findViewById(R.id.tvAlbumName);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			holder.imageView.setImageResource(R.drawable.album_default);

			if (!StringUtil.isNullOrEmpty(list.get(position).thumbUrl)) {
				if (list.get(position).thumbUrl.contains(ExternalStorage.SDCARD_PATH)){
					ImageUtil.loadImage(list.get(position).thumbUrl, holder.imageView);
				}
				else{
					ImageUtil.loadImage(ServerPath.IMAGE_PATH +  list.get(position).thumbUrl, holder.imageView);
				}

			}

			// holder.titleAlbum.setText(getDateTimeStringFromDateAndTime(list.get(position).createdTime));
			holder.titleAlbum.setText(list.get(position).createdTime);

			if (position == numTopLoaded - 1) {
				getMorePhoto();
			}

			return row;
		}
	}

	/**
	 *
	 * Lay chuoi ngay thang nam yyyy-MM-dd HH:mm:ss tu dd/MM/yyyy HH:mm
	 *
	 * @author: Nguyen Thanh Dung
	 * @param strDate
	 * @param strTime
	 * @return
	 * @return: String
	 * @throws:
	 */
	public static String getDateTimeStringFromDateAndTime(String strDateTime) {
		String[] timeArray = strDateTime.split(" ");
		String strDate = timeArray[0];
		String strTime = timeArray[1];

		String[] days = strDate.split("-");
		StringBuilder sbDateTime = new StringBuilder();

		if (days.length >= 3) {
			sbDateTime.append(days[2]);
			sbDateTime.append("/");
			sbDateTime.append(days[1]);
			sbDateTime.append("/");
			sbDateTime.append(days[0]);

			sbDateTime.append(" - ");
		}

		strTime = strTime.substring(0, strTime.lastIndexOf(":"));
		sbDateTime.append(strTime);

		return sbDateTime.toString().trim();
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */

	public void getMorePhoto() {
		if (isAbleGetMore) {
			// numTopLoaded += NUM_LOAD_MORE;

			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, photoThumbnailListDto.getCustomerId());
			bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
			bundle.putInt(IntentConstants.INTENT_ALBUM_TYPE, photoThumbnailListDto.getAlbumInfo().getAlbumType());
			bundle.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, NUM_LOAD_MORE);
			bundle.putInt(IntentConstants.INTENT_PAGE, numTopLoaded / NUM_LOAD_MORE);

			if (albumType == 4) {
				if (!StringUtil.isNullOrEmpty(fromDateForRequest)) {
					bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, fromDateForRequest);
				}
				if (!StringUtil.isNullOrEmpty(toDateForRequest)) {
					bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, toDateForRequest);
				}

				bundle.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE, photoThumbnailListDto.getAlbumInfo()
						.getAlbumTitle());

				ActionEvent e = new ActionEvent();
				e.action = ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME;
				e.sender = this;
				e.viewData = bundle;
				e.tag = 1;
				SaleController.getInstance().handleViewEvent(e);
			} else {

				ActionEvent e = new ActionEvent();
				e.action = ActionEventConstant.GO_TO_ALBUM_DETAIL_USER;
				e.sender = this;
				e.viewData = bundle;
				e.tag = 1;
				SaleController.getInstance().handleViewEvent(e);
			}
		}
	}

	public static class ViewHolder {
		public ImageView imageView;
		public TextView titleAlbum;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viettel.vinamilk.view.main.BaseFragment#receiveBroadcast(int,
	 * android.os.Bundle)
	 */
	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW: {
			if (this.isVisible()) {
				// cau request du lieu man hinh
				numTopLoaded = 0;
				isAbleGetMore = true;
				getListAlbumOfUser();
			}
			break;
		}
		case ActionEventConstant.GET_MORE_PHOTOS: {
			getMorePhoto();
			break;
		}
		case ActionEventConstant.UPDATE_TAKEN_PHOTO: {
			numTopLoaded = 0;
			isAbleGetMore = true;
			getListAlbumOfUser();
			totalImage++;
			updateTitle(totalImage);
			break;
		}
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viettel.vinamilk.view.main.BaseFragment#onDestroyView()
	 */
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.viettel.vinamilk.view.main.BaseFragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

}
