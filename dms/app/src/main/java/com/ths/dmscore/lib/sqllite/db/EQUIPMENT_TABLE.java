/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.EquipmentHistoryDTO;
import com.ths.dmscore.dto.db.TakePhotoEquipmentDTO;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.dto.view.EquipStatisticRecordDTO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;

/**
 * Danh muc thiet bi
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 16:46:32 16-12-2014
 */
public class EQUIPMENT_TABLE extends ABSTRACT_TABLE {
	// id thiet bi
	public static final String EQUIP_ID = "EQUIP_ID";
	// id nhom thiet bi
	public static final String EQUIP_GROUP_ID = "EQUIP_GROUP_ID";
	// id nha cung cap nhom thiet bi
	public static final String EQUIP_PROVIDER_ID = "EQUIP_PROVIDER_ID";
	// ma thiet bi
	public static final String CODE = "CODE";
	// so serial
	public static final String SERIAL = "SERIAL";
	// trang thai hoat dong cua thiet bi
	public static final String STATUS = "STATUS";
	// Trang thai thiet bi: mat hay con, sua chua
	public static final String USAGE_STATUS = "USAGE_STATUS";
	// tinh trang thiet bi
	public static final String HEALTH_STATUS = "HEALTH_STATUS";
	// Trang thai giao dich
	public static final String TRADE_STATUS = "TRADE_STATUS";
	// Loai trang thai:bao mat, giao nhan..
	public static final String TRADE_TYPE = "TRADE_TYPE";
	// ma kho thiet bi
	public static final String STOCK_CODE = "STOCK_CODE";
	// Loai kho
	public static final String STOCK_TYPE = "STOCK_TYPE";
	// id Doi tuong
	public static final String STOCK_ID = "STOCK_ID";
	// id bien ban bao mat
	public static final String EQUIP_IMPORT_RECORD_ID = "EQUIP_IMPORT_RECORD_ID";
	// Ngay het han bao hanh
	public static final String WARRANTY_EXPIRED_DATE = "WARRANTY_EXPIRED_DATE";
	// nam san xuat
	public static final String MANUFACTURING_YEAR = "MANUFACTURING_YEAR";
	// Nguyen gia
	public static final String PRICE = "PRICE";
	// ngay dau tien dua vao su dung
	public static final String FIRST_DATE_IN_USE = "FIRST_DATE_IN_USE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "EQUIPMENT";

	public EQUIPMENT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;

		this.columns = new String[] { EQUIP_ID, EQUIP_GROUP_ID,
				EQUIP_PROVIDER_ID, CODE, SERIAL, STATUS, USAGE_STATUS,
				HEALTH_STATUS, TRADE_STATUS, TRADE_TYPE, STOCK_CODE,
				STOCK_TYPE, STOCK_ID, WARRANTY_EXPIRED_DATE, EQUIP_IMPORT_RECORD_ID,
				MANUFACTURING_YEAR, PRICE, FIRST_DATE_IN_USE, CREATE_DATE,
				CREATE_USER, UPDATE_USER, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * update trang thai thiet bi khi bao mat tu mobile 
	 * @author: hoanpd1
	 * @since: 11:24:33 30-12-2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 */
	protected long updateLostMobile(AbstractTableDTO dto) {
		EquipmentHistoryDTO item = (EquipmentHistoryDTO) dto;
		ContentValues value = initUpdateDataRow(item);
		String[] params = { "" + item.equipId };
		return update(value, EQUIP_ID + " = ?", params);
	}

	/**
	 * @author: hoanpd1
	 * @since: 15:04:56 19-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param item
	 * @return
	 */
	private ContentValues initUpdateDataRow(EquipmentHistoryDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(EQUIP_IMPORT_RECORD_ID, dto.recordId);
		editedValues.put(TRADE_STATUS, dto.tradeStatus);
		editedValues.put(TRADE_TYPE, dto.tradeType);
		editedValues.put(SYN_STATE, 0);
		return editedValues;
	}
	
	/**
	 * Danh sach thiet bi can kiem ke
	 * @author: hoanpd1
	 * @since: 17:31:23 16-12-2014
	 * @return: ReportLostEquipmentDTO
	 * @throws:  
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public EquipStatisticRecordDTO getListInventoryDevice(Bundle bundle)
			throws Exception {
		EquipStatisticRecordDTO result = new EquipStatisticRecordDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = bundle.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String customerId= bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		long periodI = bundle.getLong(IntentConstants.INTENT_ID_PERIOD);
		int type = bundle.getInt(IntentConstants.INTENT_STATISTIC_RECORD_TYPE);
		int objectType = bundle.getInt(IntentConstants.INTENT_STATISTIC_OBJECT_TYPE);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();

		sql.append("	SELECT * FROM (	");
		sql.append("	SELECT	");
		sql.append("	    eq.equip_id as ID_EQUIP,	");
		sql.append("	    eq.CODE_DEVICE as CODE_EQUIP,	");
		sql.append("	    eq.SERIAL_DEVICE as SERIAL_EQUIP,	");
		sql.append("	    eq.CODE_GROUP as CODE_EQUIP_GROUP,	");
		sql.append("	    eq.NAME_GROUP as NAME_EQUIP_GROUP,	");
		sql.append("	    eq.NAME_CATEGORY as NAME_EQUIP_CATEGORY,	");
		sql.append("	    esg.OBJECT_TYPE as OBJECT_TYPE,	");
		sql.append("	    esr.EQUIP_STATISTIC_RECORD_ID as ID_RECORD_INVEN,	");
		sql.append("	    pr.product_id as ID_UKE,	");
		sql.append("	    pr.product_code as CODE_UKE,	");
		sql.append("	    pr.product_name as NAME_UKE,	");
		sql.append("	    cast(max(esrd.STATISTIC_TIME) as integer) as PRETIMES,	");
		sql.append("	    cast(esr.QUANTITY as integer) as QUANTITY,	");
		sql.append("	    esr.TYPE as TYPE	");
		sql.append("	FROM	");
		sql.append("	    EQUIP_STATISTIC_RECORD  esr,	");
		sql.append("	    EQUIP_STATISTIC_CUSTOMER esc,	");
		sql.append("	    EQUIP_STATISTIC_GROUP esg	");
		
		sql.append("	    left join (	");
		sql.append("	    	SELECT object_id, customer_id	");
		sql.append("	    	FROM ACTION_LOG	");
		sql.append("	    	WHERE STAFF_ID = ? AND  SHOP_ID = ?	");
		params.add(staffId);
		params.add(shopId);
		sql.append("	    		AND DATE(START_TIME) = DATE(?) ");
		params.add(date_now);
		sql.append("	    		AND OBJECT_TYPE = 6 ");
		sql.append("	    	) al ");
		sql.append("	     on esc.EQUIP_STATISTIC_RECORD_ID = al.OBJECT_ID  ");
		sql.append("	     	and esc.customer_id = al.CUSTOMER_ID  ");
		sql.append("	    left join (SELECT	");
		sql.append("	        e.equip_id,	");
		sql.append("	        e.code as CODE_DEVICE,	");
		sql.append("	        e.serial as SERIAL_DEVICE,	");
		sql.append("	        eg.code  as CODE_GROUP,	");
		sql.append("	        eg.name  as NAME_GROUP,	");
		sql.append("	        ec.name  as NAME_CATEGORY,	");
		sql.append("	        e.equip_group_id,	");
		sql.append("	        e.stock_id	");
		sql.append("	    FROM	");
		sql.append("	        equipment e,	");
		sql.append("	        equip_group eg,	");
		sql.append("	        equip_category ec	");
		sql.append("	    WHERE	");
		sql.append("	        1=1	");
		sql.append("	        AND e.status =1	");
		sql.append("	        AND eg.status =1	");
		sql.append("	        AND ec.status =1	");
		sql.append("	        AND e.stock_type =2	");
//		sql.append("	        AND e.trade_status = 0	");
		sql.append("	        AND e.stock_id = ?	");
		params.add(customerId);
		sql.append("	        AND e.usage_status in (	");
		sql.append("	            3, 4	");
		sql.append("	        )	");
		sql.append("	        AND (	");
		sql.append("	            substr(e.first_date_in_use,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	            OR e.first_date_in_use is null	");
		sql.append("	        )	");
//		sql.append("	        and not exists (selecT 1 from EQUIP_STATISTIC_STAFF where STAFF_ID <> ? and e.equip_id = EQUIP_ID)	");
//		sql.append("	        and e.equip_id not in (selecT equip_id from EQUIP_STATISTIC_STAFF where STAFF_ID <> ?)	");
//		params.add(staffId);
		sql.append("	        AND e.equip_group_id = eg.equip_group_id	");
		sql.append("	        AND eg.equip_category_id = ec.equip_category_id) eq	");
		sql.append("	    on ((esg.OBJECT_ID = eq.EQUIP_GROUP_ID and esg.object_type = 1)	");
		sql.append("	    	OR (esg.OBJECT_ID = eq.equip_id and esg.object_type = 3))	");
		sql.append("	left join 	");
		sql.append("	(select pr.product_id, pr.product_code, pr.product_name ");
		sql.append("	    from product pr, product_info pri, EQUIP_SHELF_TOTAL est ");
		sql.append("	    where pr.CAT_ID = pri.PRODUCT_INFO_ID ");
		sql.append("	    	and est.SHELF_ID = pr.product_id 	");
		sql.append("	    	and est.shop_id = ? 	");
		params.add(shopId);
		sql.append("	    	and est.STOCK_ID = ? ");
		params.add(customerId);
		sql.append("	    	AND est.equip_statistic_record_id = ? ");
		params.add(""+periodI);
		sql.append("	    	and pr.status = 1 	");
		sql.append("	    	and pri.PRODUCT_INFO_CODE in 	");
		sql.append("	    		(SELECT EQUIP_PARAM_CODE FROM EQUIP_PARAM WHERE type = 'KIEM_KE')	");
		sql.append("	 ) pr on esg.OBJECT_ID = pr.product_id	");

		sql.append("	    left join EQUIP_STATISTIC_REC_DTL esrd	");
		sql.append("	    	on esr.EQUIP_STATISTIC_RECORD_ID = esrd.EQUIP_STATISTIC_RECORD_ID ");
		sql.append("	    	and esrd.OBJECT_STOCK_ID = ?	");
		params.add(customerId);
		sql.append("	and ((esrd.OBJECT_ID is null or esrd.OBJECT_ID = pr.product_id)	");
		sql.append("		or (esrd.OBJECT_ID is null or esrd.OBJECT_ID = eq.equip_id))	");

		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND esr.equip_statistic_record_id = ?	");
		params.add(""+periodI);
		sql.append("	    AND esc.EQUIP_STATISTIC_RECORD_ID = esr.EQUIP_STATISTIC_RECORD_ID	");
		sql.append("	    AND esg.EQUIP_STATISTIC_RECORD_ID = esr.EQUIP_STATISTIC_RECORD_ID	");
		sql.append("	    AND (	");
		sql.append("	        esr.type = 1	");
		sql.append("	        OR (	");
		sql.append("	            esr.type = 2	");
		sql.append("	            AND esr.QUANTITY > 0	");
		sql.append("	        )	");
		sql.append("	    )	");
		sql.append("	    AND esg.status = 1	");
		sql.append("	    AND esr.RECORD_STATUS =1	");
		sql.append("	    AND esc.status = 1	");
		sql.append("	    AND esc.customer_id = ?	");
		params.add(customerId);
		sql.append("	    AND substr(esr.FROM_DATE,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	    AND (	");
		sql.append("	        substr(esr.TO_DATE,1,10) >= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	        OR esr.TO_DATE IS NULL	");
		sql.append("	    )	");
		sql.append("	        and ((eq.equip_group_id is not null AND esg.OBJECT_TYPE = 1)	");
		sql.append("	        	or (pr.product_id is not null AND esg.OBJECT_TYPE = 2)	");
		sql.append("	        	or (eq.equip_id is not null AND esg.OBJECT_TYPE = 3))	");
		sql.append("	    and al.customer_id is null	");
		sql.append("	    and not exists (selecT 1 	");
		sql.append("						from EQUIP_STATISTIC_STAFF 	");
		sql.append("						where STAFF_ID <> ? 	");
		params.add(staffId);
		sql.append("						and eq.equip_id = EQUIP_ID	");
		sql.append("						AND equip_statistic_record_id = ?)	");
		params.add(""+periodI);

		sql.append("	    GROUP BY eq.equip_id, product_id)	");
		sql.append("	    WHERE TYPE = 1 OR (PRETIMES is null or PRETIMES < QUANTITY)	");
		sql.append("	    ORDER BY CODE_EQUIP, CODE_UKE	");
		
		// Phan trang
		String getTotalCount = "";
		if (page > 0) {
			if (isGetTotalPage) {
				getTotalCount = " select count(*) as TOTAL_ROW from ("
						+ sql + ")";
			}

			sql.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sql.append(" offset "
					+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						EquipInventoryDTO inventory = new EquipInventoryDTO();
						inventory.parseDataEquipInventoryRecord(c);
						result.listEquipInventory.add(inventory);
						result.setEquipStatisticRecordId(periodI);
						result.setEquipStatisticRecordType(type);
						result.setObjectType(objectType);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.i("error", e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		Cursor cTotalRow = null;
		try{
			if (isGetTotalPage) {
				cTotalRow = rawQueries(getTotalCount, params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						result.setTotalItem(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("[Hoan]", e.getMessage());
			throw e;
		} finally {
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
				MyLog.e("[Hoan]", ex.getMessage());
				throw ex;
			}
		}
		return result;

	}
	
	/**
	 * Danh sach thiet bi ma khach hang dang su dung
	 * @author: hoanpd1
	 * @since: 17:05:17 18-12-2014
	 * @return: PeriodInventoryDTO
	 * @throws:  
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public EquipStatisticRecordDTO getReportLostDevice(Bundle bundle)
			throws Exception {
		EquipStatisticRecordDTO result = new EquipStatisticRecordDTO();
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = bundle.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		long customerId =bundle.getLong(IntentConstants.INTENT_CUSTOMER_ID);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT e.equip_id AS ID_EQUIP, ");
		sql.append("       e.code as CODE_EQUIP, ");
		sql.append("       e.serial as SERIAL_EQUIP, ");
		sql.append("       e.STOCK_ID as STOCK_ID, ");
		sql.append("       e.STOCK_CODE as STOCK_CODE, ");
		sql.append("       e.HEALTH_STATUS as HEALTH_STATUS, ");
		sql.append("       e.USAGE_STATUS as USAGE_STATUS, ");
		sql.append("       eg.[EQUIP_GROUP_ID] as ID_EQUIP_GROUP, ");
		sql.append("       eg.code as CODE_EQUIP_GROUP, ");
		sql.append("       eg.name as NAME_EQUIP_GROUP, ");
		sql.append("       ec.[EQUIP_CATEGORY_ID] as ID_EQUIP_CATEGORY, ");
		sql.append("       ec.code as CODE_EQUIP_CATEGORY, ");
		sql.append("       ec.name as NAME_EQUIP_CATEGORY ");
		sql.append("FROM EQUIPMENT e, ");
		sql.append("     EQUIP_GROUP eg, ");
		sql.append("     EQUIP_CATEGORY ec ");
		sql.append(" where 1=1 ");
		sql.append("      AND e.equip_group_id = eg.equip_group_id ");
		sql.append("      AND eg.equip_category_id = ec.equip_category_id ");
		sql.append("      AND (substr(e.first_date_in_use,1,10) <= ? ");
		params.add(date_now);
		sql.append("         or e.first_date_in_use is null ) ");
		sql.append("      AND ec.status =1 ");
		sql.append("      AND eg.status =1 ");
		sql.append("      AND e.USAGE_STATUS in (3,4) ");
		sql.append("      AND e.status =1 ");
		sql.append("      AND e.TRADE_STATUS = 0 ");
		sql.append("      AND e.STOCK_TYPE =2 ");
		sql.append("      AND e.stock_id = ? ");
		params.add(""+customerId);
		sql.append(" ORDER BY CODE_EQUIP, SERIAL_EQUIP, NAME_EQUIP_GROUP ");
		// Phan trang
		String getTotalCount = "";
		if (page > 0) {
			if (isGetTotalPage) {
				getTotalCount = " select count(*) as TOTAL_ROW from ("
						+ sql + ")";
			}
			sql.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sql.append(" offset "
					+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						EquipInventoryDTO equipment = new EquipInventoryDTO();
						equipment.parseDataEquipInventoryRecord(c);
						result.listEquipInventory.add(equipment);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.i("error", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Cursor cTotalRow = null;
		try{
			if (isGetTotalPage) {
				cTotalRow = rawQueries(getTotalCount, params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						result.setTotalItem(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("[Hoan]", e.getMessage());
		} finally {
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
				MyLog.e("[Hoan]", ex.getMessage());
			}
		}
		return result;
	}
	
	/**
	 * @author: DungNX
	 * @return
	 * @throws Exception
	 * @return: List<EquipInventoryDTO>
	 * @throws:
	*/
	public List<EquipInventoryDTO> getEquipmentLostReportError() throws Exception {
		List<EquipInventoryDTO> result = null;
		StringBuffer sqlRequest = new StringBuffer();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		sqlRequest.append("Select distinct e.* From Equipment E, Equip_Lost_Mobile_Rec El ");
		sqlRequest.append(" Where E.Equip_Id = El.Equip_Id ");
		sqlRequest.append(" and el.RECORD_STATUS = 3 ");
		sqlRequest.append(" and dayInOrder(El.CREATE_DATE) = dayInOrder(?) ");

		String[] params = new String[] {date_now};
		Cursor c = null;
		try {
			c = rawQuery(sqlRequest.toString(), params);
			if (c != null) {
				result = new ArrayList<EquipInventoryDTO>();
				if (c.moveToFirst()) {
					do {
						EquipInventoryDTO temp = new EquipInventoryDTO();
						temp.parseDataEquipLostReportError(c);
						result.add(temp);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					throw e2;
				}
			}
		}
		return result;
	}
	
	/**
	 * Lay ds keyshop
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<KeyShopDTO>
	 * @throws:
	*/
	public ArrayList<TakePhotoEquipmentDTO> getListEquipment(Bundle b) throws Exception{
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor c = null;
		ArrayList<TakePhotoEquipmentDTO> lstKeyShop = new ArrayList<TakePhotoEquipmentDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	    SELECT	");
		sqlObject.append("	        e.equip_id EQUIP_ID,	");
		sqlObject.append("	        e.equip_group_id EQUIP_GROUP_ID,	");
		sqlObject.append("	        e.stock_id STOCK_ID,	");
		sqlObject.append("	        e.code CODE	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        equipment e,	");
		sqlObject.append("	        equip_group eg,	");
		sqlObject.append("	        equip_category ec	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        1=1	");
		sqlObject.append("	        AND e.status =1	");
		sqlObject.append("	        AND eg.status =1	");
		sqlObject.append("	        AND ec.status =1	");
		sqlObject.append("	        AND e.stock_type =2	");
		sqlObject.append("	        AND e.trade_status = 0	");
		sqlObject.append("	        AND e.usage_status in (	");
		sqlObject.append("	            3, 4	");
		sqlObject.append("	        )	");
		sqlObject.append("	        AND (	");
		sqlObject.append("	            substr(e.first_date_in_use,1,10) <= substr(?,1,10)	");
		paramsObject.add(dateNow);
		sqlObject.append("	            OR e.first_date_in_use is null	");
		sqlObject.append("	        )	");
		sqlObject.append("	        AND e.equip_group_id = eg.equip_group_id	");
		sqlObject.append("	        AND eg.equip_category_id = ec.equip_category_id	");
		sqlObject.append("	        AND e.stock_id = ?	");
		paramsObject.add(customerId);

		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						TakePhotoEquipmentDTO item = new TakePhotoEquipmentDTO();
						item.initDataFromCursor(c);
						lstKeyShop.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;

	}
	
	/**
	 * Danh sach thiet bi can kiem ke
	 * @author: dungnt19
	 * @return: ReportLostEquipmentDTO
	 * @throws:  
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public EquipStatisticRecordDTO getSupervisorListInventoryDevice(Bundle bundle) throws Exception {
		EquipStatisticRecordDTO result = new EquipStatisticRecordDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = bundle.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		long periodI = bundle.getLong(IntentConstants.INTENT_ID_PERIOD);
		int type = bundle.getInt(IntentConstants.INTENT_STATISTIC_RECORD_TYPE);
		int objectType = bundle.getInt(IntentConstants.INTENT_STATISTIC_OBJECT_TYPE);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();

		sql.append("	SELECT eq.*, cast(max(esrd.STATISTIC_TIME) as integer) as PRETIMES ");
		sql.append("	from  	");
		sql.append("	    (SELECT	");
		sql.append("	        e.equip_id as ID_EQUIP,	");
		sql.append("	        e.code as CODE_EQUIP,	");
		sql.append("	        e.serial as SERIAL_EQUIP,	");
		sql.append("	        e.usage_status as USAGE_STATUS,	");
		sql.append("	        e.trade_status as TRADE_STATUS,	");
		sql.append("	        e.trade_type as TRADE_TYPE,	");
		sql.append("	        eg.code  as CODE_EQUIP_GROUP,	");
		sql.append("	        eg.name  as NAME_EQUIP_GROUP,	");
		sql.append("	        ec.name  as NAME_EQUIP_CATEGORY,	");
		sql.append("	        ec.code  as CODE_EQUIP_CATEGORY,	");
		sql.append("	        e.equip_group_id,	");
		sql.append("	        e.stock_id ");
		sql.append("	    FROM	");
		sql.append("	        equipment e,	");
		sql.append("	        equip_group eg,	");
		sql.append("	        equip_category ec	");
		sql.append("	    WHERE	");
		sql.append("	        1=1	");
		sql.append("	        AND e.status =1	");
		sql.append("	        AND eg.status =1	");
		sql.append("	        AND ec.status =1	");
		sql.append("	        AND e.stock_type = 2	");
		sql.append("	        AND e.stock_id = ?	");
		params.add(customerId);
		sql.append("	        AND e.usage_status in (3)	");
		sql.append("	        AND (	");
		sql.append("	            substr(e.first_date_in_use,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	            OR e.first_date_in_use is null	");
		sql.append("	        )	");
		sql.append("	        AND e.equip_group_id = eg.equip_group_id	");
		sql.append("	        AND eg.equip_category_id = ec.equip_category_id) eq	");
		
		sql.append("	    left join EQUIP_STATISTIC_REC_DTL esrd	");
		sql.append("	    	on esrd.OBJECT_STOCK_ID = ?	");
		params.add(customerId);
		sql.append("	and (esrd.OBJECT_ID is null or esrd.OBJECT_ID = eq.ID_EQUIP)	");
		
//		sql.append("	    left join EQUIP_LOST_MOBILE_REC elmr 	");
//		sql.append("	    	on elmr.STOCK_ID = ?	");
//		params.add(customerId);
//		sql.append("	and (elmr.EQUIP_ID is null or elmr.EQUIP_ID = eq.ID_EQUIP)	");
		sql.append("	WHERE 1 = 1	");
		
		if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_ALL) {
		} else if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_GROUP_EQUIPMENT) {
			sql.append("      AND eq.equip_group_id in ( ");
			sql.append("      select object_id from equip_statistic_group esg ");
			sql.append("      		where 1 = 1 and esg.status = 1 ");
			sql.append("      			and esg.object_type = ? ");
			params.add(String.valueOf(objectType));
			sql.append("      			and esg.equip_statistic_record_id = ? ");
			params.add(String.valueOf(periodI));
			sql.append("      ) ");
		} else if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_EQUIPMENT) {
			sql.append("      AND eq.ID_EQUIP in ( ");
			sql.append("      select object_id from equip_statistic_group esg ");
			sql.append("      		where 1 = 1 and esg.status = 1 ");
			sql.append("      			and esg.object_type = ? ");
			params.add(String.valueOf(objectType));
			sql.append("      			and esg.equip_statistic_record_id = ? ");
			params.add(String.valueOf(periodI));
			sql.append("      ) ");
		}
		
		sql.append("	    GROUP BY eq.ID_EQUIP	");
		sql.append("	    ORDER BY CODE_EQUIP	");
		
		// Phan trang
		String getTotalCount = "";
		if (isGetTotalPage) {
			getTotalCount = " select count(*) as TOTAL_ROW from ("
					+ sql + ")";
		}
//		if (page > 0) {
//			if (isGetTotalPage) {
//				getTotalCount = " select count(*) as TOTAL_ROW from ("
//						+ sql + ")";
//			}
//
//			sql.append(" limit " + Integer.toString(Constants.NUM_ITEM_PER_PAGE));
//			sql.append(" offset " + Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
//		}

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						EquipInventoryDTO inventory = new EquipInventoryDTO();
						inventory.parseDataEquipInventoryRecord(c);
						inventory.setIdRecordInven(periodI);
						inventory.setObjectType(objectType);
						inventory.setShopId(shopId);
						inventory.setStaffId(staffId);
						result.listEquipInventory.add(inventory);
						result.setEquipStatisticRecordId(periodI);
						result.setEquipStatisticRecordType(type);
						result.setObjectType(objectType);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				throw e;
			}
		}
		Cursor cTotalRow = null;
		try{
			if (isGetTotalPage) {
				cTotalRow = rawQueries(getTotalCount, params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						result.setTotalItem(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
				throw ex;
			}
		}
		return result;

	}
}
