package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua class
 *
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class PO_CUSTOMER_TABLE extends ABSTRACT_TABLE {
	public static final String TABLE_NAME = "PO_CUSTOMER";

	// id don dat hang
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	// ma NPP
	public static final String SHOP_ID = "SHOP_ID";
	// Loai don hang: IN: pre->kh; CM: KH->Pre;SO Van->KH; CO KH->Van, TT-> don
	// hang tra thuong
	public static final String ORDER_TYPE = "ORDER_TYPE";
	// ma don hang
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	// 1: don hang ban nhung chua tra, 0: don hang ban da thuc hien tra lai, 2
	// don tra hang
	public static final String TYPE = "TYPE"; //
	// 2: don hang tao tren tablet chua yeu cau xac nhan, 3: don hang tao tren
	// tablet yeu cau xac nhan,
	// 0: don hang chua duyet,1: don hang da duyet, 4 huy do qua ngay khong phe
	// duyet
	public static final String APPROVED = "APPROVED"; //
	// ngay lap don hang
	public static final String ORDER_DATE = "ORDER_DATE";
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// ma nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// id NVGH
	public static final String DELIVERY_ID = "DELIVERY_ID";
	// Xe giao hang
	public static final String CAR_ID = "CAR_ID";
	// Id NVTT: nhan vien thu tien
	public static final String CASHIER_ID = "CASHIER_ID";
	// so tien don hang chua tinh khuyen mai
	public static final String AMOUNT = "AMOUNT";
	// so tien khuyen mai
	public static final String DISCOUNT = "DISCOUNT";
	// so tien don hang sau khi tinh khuyen mai
	public static final String TOTAL = "TOTAL";
	// 1: don hang tren web; 2: don hang tao ra tren table;
	public static final String ORDER_SOURCE = "ORDER_SOURCE";
	// mo ta
	public static final String DESCRIPTION = "DESCRIPTION";
	// id don hang bi tra
	public static final String FROM_SALE_ORDER_ID = "FROM_SALE_ORDER_ID";
	// tong trong luong don hang
	public static final String TOTAL_WEIGHT = "TOTAL_WEIGHT";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// 1: trong tuyen, 0: ngoai tuyen
	public static final String IS_VISIT_PLAN = "IS_VISIT_PLAN";
	// muc do khan
	public static final String PRIORITY = "PRIORITY";
	// ngay dat don hang mong muon
	public static final String DELIVERY_DATE = "DELIVERY_DATE";
	// ngay in
	public static final String TIME_PRINT = "TIME_PRINT";
	// ma user huy dung cho xoa don hang tablet: neu xoa tu dong code = AUTO_DEL
	public static final String DESTROY_CODE = "DESTROY_CODE";
	// tong so luong detail cua don hang
	public static final String TOTAL_DETAIL = "TOTAL_DETAIL";
	// ma NPP
	public static final String SHOP_CODE = "SHOP_CODE";

	//bo sung 151104
	public static final String ROUTING_ID = "ROUTING_ID";
	public static final String ACCOUNT_DATE = "ACCOUNT_DATE";
	public static final String QUANTITY = "QUANTITY";
	public static final String CYCLE_ID = "CYCLE_ID";
	public static final String IS_REWARD_KS = "IS_REWARD_KS";

	public PO_CUSTOMER_TABLE(SQLiteDatabase mDB) {
		tableName = TABLE_NAME;
		this.columns = new String[] { PO_CUSTOMER_ID, SHOP_ID, ORDER_TYPE,
				ORDER_NUMBER, TYPE, APPROVED, ORDER_DATE, CUSTOMER_ID,
				STAFF_ID, DELIVERY_ID, CAR_ID, CASHIER_ID, AMOUNT,
				DISCOUNT, TOTAL, ORDER_SOURCE, DESCRIPTION, FROM_SALE_ORDER_ID,
				TOTAL_WEIGHT, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, IS_VISIT_PLAN, PRIORITY, DELIVERY_DATE,
				TIME_PRINT, DESTROY_CODE, SYN_STATE, TOTAL_DETAIL, ROUTING_ID, ACCOUNT_DATE, QUANTITY,CYCLE_ID,IS_REWARD_KS };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		ContentValues value = initDataRow((SaleOrderDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * thay doi 1 dong cua CSDL
	 *
	 * @author: DungNX
	 * @param inheritId
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public long update(SaleOrderDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.poId };
		return update(value, PO_CUSTOMER_ID + " = ? ", params);
	}

	public long delete(AbstractTableDTO dto) {
		SaleOrderDTO saleDTO = (SaleOrderDTO) dto;
		String[] params = { "" + saleDTO.poId, "" + saleDTO.synState };
		return delete(PO_CUSTOMER_ID + " = ? AND " + SYN_STATE + " = ?", params);
	}

	/**
	 * Init value sale_order_table
	 *
	 * @author : BangHN since : 3:35:36 PM
	 */
	private ContentValues initDataRow(SaleOrderDTO dto) {
		ContentValues editedValues = new ContentValues();

		// not null field
		if (dto.poId > 0) {
			editedValues.put(PO_CUSTOMER_ID, dto.poId);
		}
		if (dto.shopId > 0) {
			editedValues.put(SHOP_ID, dto.shopId);
		}
		if (!StringUtil.isNullOrEmpty(dto.orderNumber)) {
			editedValues.put(ORDER_NUMBER, dto.orderNumber);
		}

		if (!StringUtil.isNullOrEmpty(dto.orderDate)) {
			editedValues.put(ORDER_DATE, dto.orderDate);
		}
		if (dto.customerId > 0) {
			editedValues.put(CUSTOMER_ID, dto.customerId);
		}
		if (dto.saleOrderId > 0) {
			editedValues.put(FROM_SALE_ORDER_ID, dto.saleOrderId);
		}
		if (dto.staffId > 0) {
			editedValues.put(STAFF_ID, dto.staffId);
		}
		if (!StringUtil.isNullOrEmpty(dto.deliveryId) && !StringUtil.isNullOrEmpty(String.valueOf(dto.cashierId))) {
			editedValues.put(DELIVERY_ID, dto.deliveryId);
			editedValues.put(CASHIER_ID, dto.cashierId);
		}

		// if (dto.amount > 0) {
		editedValues.put(AMOUNT, dto.getAmount());
		// }
		// if (dto.total > 0) {
		editedValues.put(TOTAL, dto.getTotal());
		// }
		if (!StringUtil.isNullOrEmpty(dto.createUser)) {
			editedValues.put(CREATE_USER, dto.createUser);
		}

		if (!StringUtil.isNullOrEmpty(dto.createDate))
			editedValues.put(CREATE_DATE, dto.createDate);

		// can null field
		if (dto.orderType != null) {
			editedValues.put(ORDER_TYPE, dto.orderType);
		} else {
			editedValues.put(ORDER_TYPE, "");
		}

		// if (dto.invoiceNumber != null) {
		// editedValues.put(INVOICE_NUMBER, dto.invoiceNumber);
		// } else {
		// editedValues.put(INVOICE_NUMBER, "");
		// }

		if (dto.updateUser != null) {
			editedValues.put(UPDATE_USER, dto.updateUser);
		} else {
			editedValues.put(UPDATE_USER, "");
		}

		if (dto.updateDate != null) {
			editedValues.put(UPDATE_DATE, dto.updateDate);
		} else {
			editedValues.put(UPDATE_DATE, "");
		}

		if (dto.deliveryDate != null) {
			editedValues.put(DELIVERY_DATE, dto.deliveryDate);
		} else {
			editedValues.put(DELIVERY_DATE, "");
		}

		if (dto.priority >= 0) {
			editedValues.put(PRIORITY, dto.priority);
		}

		editedValues.put(DISCOUNT, dto.getDiscount());
		editedValues.put(TYPE, dto.type);
		editedValues.put(IS_VISIT_PLAN, dto.isVisitPlan);
		editedValues.put(ORDER_SOURCE, dto.orderSource);
		editedValues.put(APPROVED, dto.approved);
		editedValues.put(SYN_STATE, dto.synState);
		editedValues.put(TOTAL_WEIGHT, dto.totalWeight);
		editedValues.put(TOTAL_DETAIL, dto.totalDetail);
		editedValues.put(SHOP_CODE, dto.shopCode);
		if (!StringUtil.isNullOrEmpty(dto.accountDate)) {
			editedValues.put(ACCOUNT_DATE, dto.accountDate);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(ROUTING_ID, dto.routingId);
		editedValues.put(CYCLE_ID, dto.cycleId);
		editedValues.put(IS_REWARD_KS, dto.isRewardKS?1:0);
		return editedValues;
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: SaleOrderDTO
	 * @throws:
	 */
	public SaleOrderDTO getSaleById(long id) {
		SaleOrderDTO SaleOrderDTO = null;
		Cursor c = null;
		try {
			//String[] params = { "" + id };
			c = query(PO_CUSTOMER_ID + " = " + id, null, null, null, null);

			if (c != null) {
				if (c.moveToFirst()) {
					SaleOrderDTO = initSaleOrderDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
			MyLog.e("getSaleById", "fail", ex);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("getSaleById", "fail", ex);
			}
		}
		return SaleOrderDTO;
	}

	private SaleOrderDTO initSaleOrderDTOFromCursor(Cursor c) {
		SaleOrderDTO saleOrderDTO = new SaleOrderDTO();

		saleOrderDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		// fix truong hop viewPO ma refresh get PO khong co truong fromPOCustomerId
		saleOrderDTO.fromPOCustomerId = CursorUtil.getLong(c, PO_CUSTOMER_ID);
		saleOrderDTO.createDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, CREATE_DATE));
		saleOrderDTO.createUser = CursorUtil.getString(c, CREATE_USER);
		saleOrderDTO.customerId = CursorUtil.getLong(c, CUSTOMER_ID);
		saleOrderDTO.deliveryId = CursorUtil.getString(c, DELIVERY_ID);
		saleOrderDTO.setDiscount(CursorUtil.getDouble(c, DISCOUNT));
		// saleOrderDTO.invoiceNumber = CursorUtil.getString(c
		// .getColumnIndex(INVOICE_NUMBER);
		saleOrderDTO.isVisitPlan = CursorUtil.getInt(c, IS_VISIT_PLAN);
		saleOrderDTO.orderDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, ORDER_DATE));
		// SaleOrderDTO.userName = CursorUtil.getString(c
		// .getColumnIndex(USER_NAME);
		saleOrderDTO.orderNumber = CursorUtil.getString(c, ORDER_NUMBER);
		saleOrderDTO.orderType = CursorUtil.getString(c, ORDER_TYPE);
		saleOrderDTO.saleOrderId = CursorUtil.getLong(c, PO_CUSTOMER_ID);
		saleOrderDTO.fromSaleOrderId = CursorUtil.getLong(c, FROM_SALE_ORDER_ID);
		saleOrderDTO.shopId = CursorUtil.getInt(c, SHOP_ID);
		saleOrderDTO.staffId = CursorUtil.getInt(c, STAFF_ID);
		saleOrderDTO.approved = CursorUtil.getInt(c, APPROVED);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		// saleOrderDTO.importCode = CursorUtil.getString(c, IMPORT_CODE);
		saleOrderDTO.priority = CursorUtil.getLong(c, PRIORITY);
		saleOrderDTO.deliveryDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, DELIVERY_DATE));
		// ---
		// saleOrderDTO.SYN_OPERATOR =
		// CursorUtil.getString(c, SYN_OPERATOR);
		// saleOrderDTO.synStatus = CursorUtil.getInt(c, SYN_STATUS);
		saleOrderDTO.setTotal(CursorUtil.getDouble(c, TOTAL));
		saleOrderDTO.updateDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, UPDATE_DATE));
		saleOrderDTO.updateUser = CursorUtil.getString(c, UPDATE_USER);
		saleOrderDTO.synState = CursorUtil.getInt(c, SYN_STATE);
		saleOrderDTO.orderSource = CursorUtil.getInt(c, ORDER_SOURCE);
		saleOrderDTO.totalDetail = CursorUtil.getInt(c, TOTAL_DETAIL);
		saleOrderDTO.quantity = CursorUtil.getLong(c, QUANTITY);
		saleOrderDTO.routingId = CursorUtil.getInt(c, ROUTING_ID);
		saleOrderDTO.accountDate = CursorUtil.getString(c, ACCOUNT_DATE);
		saleOrderDTO.isRewardKS = CursorUtil.getInt(c, IS_REWARD_KS) == 1?true:false;
		return saleOrderDTO;
	}

	/**
	 * lay POID tu saleorderid
	 *
	 * @author: DungNX
	 * @param orderJoinTableDTO
	 * @return: void
	 * @throws:
	 */
	public long getPOIDFromSaleOrderID(long saleOrderID) throws Exception {
		long poID = -1;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    po_customer_id as PO_CUSTOMER_ID ");
		sqlObject.append("	FROM	");
		sqlObject.append("	    po_customer	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    FROM_SALE_ORDER_ID = ?	");
		paramsObject.add(String.valueOf(saleOrderID));

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					poID = CursorUtil.getLong(c, "PO_CUSTOMER_ID");
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return poID;
	}
	
	public long updateOrderCancel(long saleOrderId) {
		ContentValues value = initDataRowCancelOrder();
		String[] params = { "" + saleOrderId };
		return update(value, FROM_SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * init data row cancel order
	 * @author: duongdt3
	 * @time: 2:29:08 PM Nov 3, 2015
	 * @param saleOrderId
	 * @return
	*/
	private ContentValues initDataRowCancelOrder() {
		ContentValues editedValues = new ContentValues();
		editedValues.put(UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode());
		editedValues.put(UPDATE_DATE, DateUtils.now());
		editedValues.put(APPROVED, 3);
		return editedValues;
	}
}
