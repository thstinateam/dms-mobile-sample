/**
* Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import android.support.v4.util.LongSparseArray;

import com.ths.dmscore.dto.BigDecimalRound;
import com.ths.dmscore.dto.db.BuyInfoLevel;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.DebitDetailDTO;
import com.ths.dmscore.dto.db.GroupLevelDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.PromotionStaffMapDTO;
import com.ths.dmscore.dto.db.RptCttlDetailPayDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;
import com.ths.dmscore.dto.db.PayReceivedDTO;
import com.ths.dmscore.dto.db.PaymentDetailDTO;
import com.ths.dmscore.dto.db.PromotionCustomerMapDTO;
import com.ths.dmscore.dto.db.PromotionShopMapDTO;
import com.ths.dmscore.dto.db.QuantityRevicevedDTO;
import com.ths.dmscore.dto.db.RoutingCustomerDTO;
import com.ths.dmscore.dto.db.SaleOrderLotDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoLotDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.db.SalePromoMapDTO;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.lib.sqllite.db.CalPromotions;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_CUSTOMER_MAP;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_SHOP_MAP;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_STAFF_MAP;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dms.R;

/**
 *  Thong tin don hang tren view
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class OrderViewDTO implements Serializable {
	private static final long serialVersionUID = 6869413775679487049L;
	public static final int INDEX_PROMOTION_PRODUCT = 1;
	public static final int INDEX_PROMOTION_ORDER = 2;
	public static final int INDEX_PROMOTION_ACCUMULATION = 3;
	public static final int INDEX_PROMOTION_NEW_OPEN = 4;
	public static final int INDEX_PROMOTION_KEY_SHOP = 5;

	public static final int INDEX_PROMOTION_PRODUCT_MONEY = 11;
	public static final int INDEX_PROMOTION_NEW_OPEN_MONEY = 14;

	//Tong so luong cac loai KM ben tren, moi lan them 1 la tang 1, bot 1 la giam 1
	public static final int NUM_PROMOTION_TYPE = 7; // doi 4 thanh 5 vi bo sung them tra thuong keyshop
	// thong tin don hang
	public SaleOrderDTO orderInfo = new SaleOrderDTO();
	// dung cho tra hang vansale
	// don hang goc
	public SaleOrderDTO orderTemp = null;
	// thong tin khach hang
	public CustomerDTO customer = new CustomerDTO();
	// ds mat hang ban
	public ArrayList<OrderDetailViewDTO> listBuyOrders = new ArrayList<OrderDetailViewDTO>();
	// ds mat hang khuyen mai
	public ArrayList<OrderDetailViewDTO> listPromotionOrders = new ArrayList<OrderDetailViewDTO>();
	// ds KM don hang
	public ArrayList<OrderDetailViewDTO> listPromotionOrder = new ArrayList<OrderDetailViewDTO>();
	// ds KM tich luy
	public ArrayList<OrderDetailViewDTO> listPromotionAccumulation = new ArrayList<OrderDetailViewDTO>();
	public ArrayList<OrderDetailViewDTO> listPromotionAccumulationTemp = new ArrayList<OrderDetailViewDTO>();
	// ds KM mo moi
	public ArrayList<OrderDetailViewDTO> listPromotionNewOpen = new ArrayList<OrderDetailViewDTO>();
	//ds mat hang khuyen mai dung de doi giua cac mat hang khuyen mai
	public SortedMap<Long, ArrayList<OrderDetailViewDTO>> listPromotionChange = new TreeMap<Long, ArrayList<OrderDetailViewDTO>>();
	// ds tra thuong keyshop
	public ArrayList<OrderDetailViewDTO> listPromotionKeyShop = new ArrayList<OrderDetailViewDTO>();

	//ds KM san pham tien, %
	public ArrayList<OrderDetailViewDTO> listPromotionOrdersMoney = new ArrayList<OrderDetailViewDTO>();
	//ds KM mo moi tien, %
	public ArrayList<OrderDetailViewDTO> listPromotionNewOpenMoney = new ArrayList<OrderDetailViewDTO>();

	// ds muc do khan
	public ArrayList<ApParamDTO> listPriority = new ArrayList<ApParamDTO>();
	// so SKU mat hang ban
	public long numSKU;
	// ma nhan vien
	public int staffId;
	//So SKU khuyen mai
	public long numSKUPromotion;
	// bien dung de kiem tra thong tin da khoi tao hay chua
	public boolean isFirstInit = true;

	public static final int STATE_NEW = 0;
	public static final int STATE_VIEW_ORDER = 1;
	public static final int STATE_VIEW_PO = 2;
	public static final int STATE_EDIT = 3;

	// bien dung de kiem tra tao moi hay chinh sua don hang
	public int stateOrder = STATE_VIEW_ORDER;
	// bien dung de kiem tra co quay ve man hinh kiem hang ton hay khong?
	public boolean isBackToRemainOrder = false;

	// thoi gian bat dau ghe tham
	public String beginTimeVisit = "";
	// thoi gian chuyen mong muon
	public String deliveryTime = "";
	// ngay chuyen mong muon
	public String deliveryDate = "";

	// kiem tra co enable button luu hay khong
	public boolean isEnableButton = true;
	// kiem tra du lieu co thay doi hay khong
	public boolean isChangeData = false;

	// thong tin staffCustomer
	public StaffCustomerDTO staffCusDto = new StaffCustomerDTO();

	//Thong tin routing
	public RoutingCustomerDTO routingCusDTO = new RoutingCustomerDTO();

	//Ds san pham ko con ton kho hoac ton kho bi thieu
	public SortedMap<Long, OrderDetailViewDTO> listLackRemainProduct = new TreeMap<Long, OrderDetailViewDTO>();
	//Ds san pham ko con ton kho hoac ton kho bi thieu cua KM tich luyc
	public SortedMap<Long, OrderDetailViewDTO> listLackRemainAccumulationProduct = new TreeMap<Long, OrderDetailViewDTO>();
	//Ds ton kho sp tich luy
//	SortedMap<Long, OrderDetailViewDTO> listStockAccumulationProduct = new TreeMap<Long, OrderDetailViewDTO>();

	//Chi tiet cong no
	public DebitDetailDTO debitDetailDto = new DebitDetailDTO();

	//Thong tin cong no
	public DebitDTO debitDto = new DebitDTO();

	// thong tin phieu thu
	public PayReceivedDTO payReceivedDTO = new PayReceivedDTO();

	// chi tiet phieu thu
	public PaymentDetailDTO paymentDetailDto = new PaymentDetailDTO();

	// thong tin phieu chi
	public PayReceivedDTO payReceivedDTOChi;

	// chi tiet phieu chi
	public PaymentDetailDTO paymentDetailDtoChi;

	// tien thanh toan cho vansale
	public double paymentMoney;
	// chiet khau cho vansale
	public double discount;

	//id cua debit da ton tai
	public long debitIdExist;

	//khoi luong cua sp KM
	public double promotionTotalWeight;

	// ds CTKM don hang thay the
	public ArrayList<OrderDetailViewDTO> listPromotionForOrderChange = new ArrayList<OrderDetailViewDTO>();

	// danh cho vansale tinh so xuat KM
	//public PromotionShopMapDTO promotionShopMapDTO;
	//public PromotionCustomerMapDTO promotionCustomerMapDTO;

	// tien KM tinh tren tung sp
//	public long discountAmount = 0;
	//Danh sach so suat cua KM sp
	public ArrayList<SaleOrderPromotionDTO> productQuantityReceivedList = new ArrayList<SaleOrderPromotionDTO>();
	//Danh sach so suat cua KM don hang
	public ArrayList<SaleOrderPromotionDTO> orderQuantityReceivedList = new ArrayList<SaleOrderPromotionDTO>();
	public Collection<QuantityRevicevedDTO> listIncreasePromoQuantityReceived;
	public ArrayList<SaleOrderLotDTO> listSaleOrderLot = null;
	// luu thong tin km lot
	public ArrayList<SaleOrderPromoLotDTO> listSaleOrderPromoLot= new ArrayList<SaleOrderPromoLotDTO>();
	public ArrayList<SaleOrderPromoDetailDTO> listSaleOrderPromoDetailDTO = null;
	public ArrayList<SaleOrderPromotionDTO> listSaleOrderPromotionDTO = null;
	public ArrayList<SalePromoMapDTO> listSalePromomapDTO = null;
	//chi co 1 Km don hang
	public boolean isHaveOneOrderPromotion;

	//Last order de cap nhat
	public String lastOrder;
	//Don hang cua cung cua KH trong ngay
	public int isFinalOrder;
	//Co tinh KM tich luy hay ko?
	public boolean isCalAccuPromotion = false;
	public ArrayList<OrderDetailViewDTO> listProduct = new ArrayList<OrderDetailViewDTO>();
//	public ArrayList<SaleOrderPromoDetailDTO> listPromoDetail = new ArrayList<SaleOrderPromoDetailDTO>();
	public ArrayList<ArrayList<BuyInfoLevel>> buyInfoGroupArray = new ArrayList<ArrayList<BuyInfoLevel>>();
	//So lan dat KM tich luy
	public int multiple;
	public int multipleUsing;
	//Thong tin mua hang cua 1 muc
	public ArrayList<BuyInfoLevel> buyInfoLevelArray;
	//Muc KM tich luy
	public GroupLevelDTO groupLevel;
	//discount tu ds sp KM (tich luy)
	private double promotionDiscount;
	private double promotionMaxDiscount;
	public ArrayList<RptCttlPayDTO> listRptCttPay = new ArrayList<RptCttlPayDTO>();
	//So tien con du dot truoc
	public long balancePromotion;
	//Nhan tinh KM duoc
	public boolean isCalculatePromotion;
	// danh sach id km sai
	public ArrayList<Long> lstWrongProgrameId = new ArrayList<Long>();
	// mac dinh la ko tra thuong keyshop
	public boolean isKeyShop = false;

	public HashMap<Long, QuantityRevicevedDTO> mapQuantityReceiveMissing = new HashMap<Long, QuantityRevicevedDTO>();
	//Thong tin cac CTKM tu dong ma don hang tham gia
	public LongSparseArray<ArrayList<PromotionProgrameDTO>> promotionArrayClone = new LongSparseArray<ArrayList<PromotionProgrameDTO>>();

	public HashMap<Long, QuantityRevicevedDTO> mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();

	/**
	 * @return the promotionDiscount
	 */
	public double getPromotionDiscount() {
		return promotionDiscount;
	}

	/**
	 * @param promotionDiscount the promotionDiscount to set
	 */
	public void setPromotionDiscount(double promotionDiscount) {
		this.promotionDiscount = promotionDiscount;
	}

	/**
	 * @return the promotionMaxDiscount
	 */
	public double getPromotionMaxDiscount() {
		return promotionMaxDiscount;
	}

	/**
	 * @param promotionMaxDiscount the promotionMaxDiscount to set
	 */
	public void setPromotionMaxDiscount(double promotionMaxDiscount) {
		this.promotionMaxDiscount = promotionMaxDiscount;
	}

	/**
	*  Phat sinh ra cau lenh sql chuyen len server
	*  @author: TruongHN
	*  @return: JSONArray
	*  @throws:
	 */
	public JSONArray generateNewOrderSql() {
//		ArrayList<JSONArray> result = new ArrayList<JSONArray>();

//		JSONArray listDeclare = new JSONArray();
		JSONArray listSql = new JSONArray();
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		// khoi tao ben SaleModel
//		customer.setLastOrder(orderInfo.createDate);

		// insert into SalesOrder
		listSql.put(orderInfo.generateCreateSql());
		// insert vao ban database_log tuong ung
//		listSql.put(GlobalUtil.generateSqlSync(SALE_ORDER_TABLE.TABLE_NAME, DataBaseLogDTO.ACTION_INSERT, String.valueOf(orderInfo.saleOrderId), orderInfo.shopId, staffId));

		for (OrderDetailViewDTO detail: listProduct) {
			detail.orderDetailDTO.createDate = orderInfo.createDate;
			detail.orderDetailDTO.createUser = orderInfo.createUser;
			detail.orderDetailDTO.shopId = shopId;
			detail.orderDetailDTO.staffId = staffId;
			// insert sales order detail
			listSql.put(detail.orderDetailDTO.generateCreateSql());
			// insert vao ban database_log tuong ung
//				listSql.put(GlobalUtil.generateSqlSync(SALES_ORDER_DETAIL_TABLE.TABLE_NAME, DataBaseLogDTO.ACTION_INSERT, String.valueOf(detail.orderDetailDTO.salesOrderDetailId), orderInfo.shopId, staffId));


			// cap nhat stock total //Khong cap nhat ton kho nua -> de thuc hien chuc nang hien thi mau` ton kho
			StockTotalDTO stockTotal = StockTotalDTO.createStockTotalInfo(this, detail.orderDetailDTO);

			/*if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
				listSql.put(stockTotal.generateDescreaseSqlPresale());
			} else*/ if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
				listSql.put(stockTotal.generateDescreaseSqlVansale());
			}

			// insert vao ban database_log tuong ung
//				listSql.put(GlobalUtil.generateSqlSync(STOCK_TOTAL_TABLE.TABLE_STOCK_TOTAL, DataBaseLogDTO.ACTION_UPDATE, orderInfo.shopId, staffId));

			//insert them phan chi tiet khuyen mai
			if (detail.listPromoDetail != null && !detail.listPromoDetail.isEmpty()) {
				for (SaleOrderPromoDetailDTO promoDetail : detail.listPromoDetail) {
					if(promoDetail.discountAmount >= 0) {
						listSql.put(promoDetail.generateJsonSaleOrderPromoDetail());
					}
				}
			}

			//insert them phan chi tiet khuyen mai
			if (detail.lstPromotionMap != null && !detail.lstPromotionMap.isEmpty()) {
				for (SalePromoMapDTO promoDetail : detail.lstPromotionMap) {
						listSql.put(promoDetail.generateInsertSql());
				}
			}
		}

		//Ghi log khi don hang bi rot chi tiet
		if(listProduct.isEmpty()) {
			ServerLogger.sendLog("Don hang khong co chi tiet khi gen json createOrder: " + orderInfo.saleOrderId, TabletActionLogDTO.LOG_CLIENT);
		}

		//tien KM tich luy
//		for (SaleOrderPromoDetailDTO promoDetail : listPromoDetail) {
//			listSql.put(promoDetail.generateJsonSaleOrderPromoDetail());
//		}

		//Tra KM tich luy
		for(OrderDetailViewDTO promotion: listPromotionAccumulation) {
			double disAmount = promotion.orderDetailDTO.getDiscountAmount();
			if (promotion.rptCttlPay != null
					&& (disAmount > 0 || (BigDecimal.valueOf(disAmount).compareTo(BigDecimal.ZERO) == 0 && promotion.remainDiscount == 0))) {
//					&& (disAmount > 0 || (disAmount == 0 && promotion.remainDiscount == 0))) {
				listSql.put(promotion.rptCttlPay.generateInsertRptCttlPay());

				for(RptCttlDetailPayDTO payCttlPayDetail : promotion.rptCttlPay.rptCttlDetailList) {
					if(payCttlPayDetail.quantityPromotion > 0 || payCttlPayDetail.amountPromotion > 0) {
						listSql.put(payCttlPayDetail.generateJsonRptCttlDetailPay());
					}
				}
			}
		}
		//gen json so suat
		if (productQuantityReceivedList != null) {
			for (SaleOrderPromotionDTO salePromotion : productQuantityReceivedList) {
				listSql.put(salePromotion.generateJsonInsertSaleOrderPromotion());
			}
		}

		//gen json cho sale_order_lot
		if (listSaleOrderLot != null) {
			for (SaleOrderLotDTO saleOrderLot : listSaleOrderLot) {
				listSql.put(saleOrderLot.generateJsonInsertSaleOrderLot());
			}
		}

		//gen json cho sale_order_lot
		if (listSaleOrderPromoLot != null) {
			for (SaleOrderPromoLotDTO saleOrderPromoLot : listSaleOrderPromoLot) {
				listSql.put(saleOrderPromoLot.generateJsonSaleOrderPromoLot());
			}
		}

		if(orderInfo.getAmount() > 0) {
			if (isFinalOrder == 1) {
				listSql.put(customer.generateUpdateFromOrderSql());

				// cap nhat staff_customer
				listSql.put(staffCusDto.generateInsertOrUpdateFromOrderSql());

				// cap nhat routing customer
				listSql.put(routingCusDTO.generateUpdateFromOrderSql());
			}
		}


		if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			// neu chua co no cua KH thi insert 1 debit cho KH moi

			if (debitIdExist <= 0) {
				listSql.put(debitDto.generateInsertFromOrderSql());
			}
			// Insert chi tiet cong no
			listSql.put(debitDetailDto.generateInsertFromOrderSql());
		}

		// thong nhat chi cap nhat so suat luc in don vansale tren web
//		if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
//			// tang so xuat cho don hang vansale
//			if (listIncreasePromoQuantityReceived != null) {
//				for (QuantityRevicevedDTO quanDto : this.listIncreasePromoQuantityReceived) {
//					quanDto.generateInscreasePromotionSqlVansale(listSql);
//				}
//			}
//		}

//		if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {
//			// tang so xuat cho don hang vansale
//			if (listIncreasePromoQuantityReceived != null) {
//				for (QuantityRevicevedDTO quanDto : this.listIncreasePromoQuantityReceived) {
//					quanDto.generateInscreasePromotionSqlVansale(listSql);
//				}
//			}
//		} else if (orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
//		}
		// tang so xuat cho don hang vansale
		if (listIncreasePromoQuantityReceived != null) {
			for (QuantityRevicevedDTO quanDto : this.listIncreasePromoQuantityReceived) {
				quanDto.generateInsertPromotionMapDelta(listSql);
			}
		}

		return listSql;
	}

	/**
	 *
	*  Kiem tra xem sp co con ton kho hay ko, tinh tong so luong ban cua tung sp
	*  @author: Nguyen Thanh Dung
	*  @return: void
	*  @throws:
	 */
	//ds mat hang de hien thi con ton kho hay ko?
	public SortedMap<Long, OrderDetailViewDTO> listDistinctProduct = new TreeMap<Long, OrderDetailViewDTO>();

	public void checkStockTotal() {
		listDistinctProduct.clear();
		//Ds KH tich luy
		//Duyet qua ds de tinh tong so luong tru vao kho cua 1 san pham
		for (OrderDetailViewDTO detail : listPromotionAccumulation) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
						} else {
							OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
							SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
							orderDetail.orderDetailDTO = orderDTO;
							orderDetail.promotionType = OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT;

							orderDTO.quantity = detail.orderDetailDTO.quantity;
							orderDTO.productId = detail.orderDetailDTO.productId;
							orderDetail.stock = detail.stock;

							listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
						}
					}
				}
			}
		}

		//Duyet qua ds de tinh tong so luong ban cua 1 sp
		for (OrderDetailViewDTO detail: listBuyOrders) {
			Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
			if (listDistinctProduct.containsKey(key2)) {
				OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
				orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
//				orderDetail.stock -= detail.orderDetailDTO.quantity;
			} else {
				OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
				SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
				orderDetail.orderDetailDTO = orderDTO;

				orderDTO.quantity = detail.orderDetailDTO.quantity;
				orderDTO.productId = detail.orderDetailDTO.productId;
				orderDetail.stock = detail.stock;
//				orderDetail.stock -= detail.orderDetailDTO.quantity;
				// orderDTO.price = detail.orderDetailDTO.price;
				// orderDTO.programeCode = detail.orderDetailDTO.programeCode;

				listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
			}
		}

		//Ds KM sp
		for (OrderDetailViewDTO detail : listPromotionOrders) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT
					&& detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
				if(listDistinctProduct.containsKey(key2)) {
					OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
					orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
//					orderDetail.stock -= detail.orderDetailDTO.quantity;
				} else {
					OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
					SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
					orderDetail.orderDetailDTO = orderDTO;

					orderDTO.quantity = detail.orderDetailDTO.quantity;
					orderDTO.productId = detail.orderDetailDTO.productId;
					orderDetail.stock = detail.stock;
//					orderDetail.stock -= detail.orderDetailDTO.quantity;

					listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
				}
			}
		}

		//Ds KM don hang
		//Duyet qua ds de tinh tong so luong tru vao kho cua 1 san pham
		for (OrderDetailViewDTO detail : listPromotionOrder) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;
						} else {
							OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
							SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
							orderDetail.orderDetailDTO = orderDTO;

							orderDTO.quantity = detail.orderDetailDTO.quantity;
							orderDTO.productId = detail.orderDetailDTO.productId;
							orderDetail.stock = detail.stock;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;

							listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
						}
					}
				}
			}
		}

		//Ds KM mo moi
		for (OrderDetailViewDTO detail : listPromotionNewOpen) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;
						} else {
							OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
							SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
							orderDetail.orderDetailDTO = orderDTO;

							orderDTO.quantity = detail.orderDetailDTO.quantity;
							orderDTO.productId = detail.orderDetailDTO.productId;
							orderDetail.stock = detail.stock;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;

							listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
						}
					}
				}
			}
		}

		//Ds KM key shop
		for (OrderDetailViewDTO detail : listPromotionKeyShop) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT
					&& detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
				if(listDistinctProduct.containsKey(key2)) {
					OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
					orderDetail.orderDetailDTO.quantity += detail.orderDetailDTO.quantity;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;
				} else {
					OrderDetailViewDTO orderDetail = new OrderDetailViewDTO();
					SaleOrderDetailDTO orderDTO = new SaleOrderDetailDTO();
					orderDetail.orderDetailDTO = orderDTO;

					orderDTO.quantity = detail.orderDetailDTO.quantity;
					orderDTO.productId = detail.orderDetailDTO.productId;
					orderDetail.stock = detail.stock;
//							orderDetail.stock -= detail.orderDetailDTO.quantity;

					listDistinctProduct.put(Long.valueOf(detail.orderDetailDTO.productId), orderDetail);
				}
			}
		}

		//Cap nhat lai total quantity
		for (OrderDetailViewDTO detail: listBuyOrders) {
			Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
			if (listDistinctProduct.containsKey(key2)) {
				OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
				detail.totalOrderQuantity = orderDetail;
			}
		}

		//KM sp
		for (OrderDetailViewDTO detail : listPromotionOrders) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT
					&& detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
				if(listDistinctProduct.containsKey(key2)) {
					OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
					detail.totalOrderQuantity = orderDetail;
				}
			}
		}

		//Km don hang
		for (OrderDetailViewDTO detail : listPromotionOrder) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							detail.totalOrderQuantity = orderDetail;
						}
					}
				}
			}
		}

		//Km tich luy
		for (OrderDetailViewDTO detail : listPromotionAccumulation) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							detail.totalOrderQuantity = orderDetail;
						}
					}
				}
			}
		}

		//KM mo moi
		for (OrderDetailViewDTO detail : listPromotionNewOpen) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if(listDistinctProduct.containsKey(key2)) {
							OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
							detail.totalOrderQuantity = orderDetail;
						}
					}
				}
			}
		}

		//KM keyshop
		for (OrderDetailViewDTO detail : listPromotionKeyShop) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT
					&& detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
				if(listDistinctProduct.containsKey(key2)) {
					OrderDetailViewDTO orderDetail = listDistinctProduct.get(key2);
					detail.totalOrderQuantity = orderDetail;
				}
			}
		}
//		listStockAccumulationProduct.clear();
//		for(OrderDetailViewDTO stockAccu : listDistinctProduct.values()) {
//			if(stockAccu.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				listStockAccumulationProduct.put(Long.valueOf(stockAccu.orderDetailDTO.productId), stockAccu);
//			}
//		}
	}

	/**
	*  Kiem tra don hang co stock total hop le hay ko -> dung cho vansale
	*  @author: Nguyen Thanh Dung
	*  @return
	*  @return: boolean
	*  @throws:
	*/

	public boolean checkStockTotalSuccess() {
		boolean result = true;
		listLackRemainProduct.clear();
		listLackRemainAccumulationProduct.clear();
		//Ds sp ban
		for (OrderDetailViewDTO detail: listBuyOrders) {
			if(detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock) {
				Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
				if (!listLackRemainProduct.containsKey(key2)) {
					listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
					result = false;
				}
			}
		}

		//DS KM sp
		for (OrderDetailViewDTO detail : listPromotionOrders) {
			if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				if(detail.totalOrderQuantity.orderDetailDTO.quantity > 0 && (detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock)) {
					Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
					if (!listLackRemainProduct.containsKey(key2)) {
						listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
						result = false;
					}
				}
			}
		}

		// KM don hang
		for (OrderDetailViewDTO detail : listPromotionOrder) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						if(detail.totalOrderQuantity.orderDetailDTO.quantity > 0 && (detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock)) {
							Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
							if (!listLackRemainProduct.containsKey(key2)) {
								listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
								result = false;
							}
						}
					}
				}
			}
		}

		//Ds KH tich luy
		//Duyet qua ds de tinh tong so luong tru vao kho cua 1 san pham
		for (OrderDetailViewDTO detail : listPromotionAccumulation) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						if(detail.totalOrderQuantity.orderDetailDTO.quantity > 0 && (detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock)) {
							Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
							if (!listLackRemainProduct.containsKey(key2)) {
								listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
								result = false;
							}

							if (!listLackRemainAccumulationProduct.containsKey(key2)) {
								listLackRemainAccumulationProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
								result = false;
							}
						}
					}
				}
			}
		}

		//Ds KM mo moi
		for (OrderDetailViewDTO detail : listPromotionNewOpen) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						if(detail.totalOrderQuantity.orderDetailDTO.quantity > 0 && (detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock)) {
							Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
							if (!listLackRemainProduct.containsKey(key2)) {
								listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
								result = false;
							}
						}
					}
				}
			}
		}

		//Ds tra thuong keyshop
		for (OrderDetailViewDTO detail : listPromotionKeyShop) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
				if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
					if(detail.totalOrderQuantity.orderDetailDTO.quantity > 0 && (detail.stock <= 0 || detail.totalOrderQuantity.orderDetailDTO.quantity > detail.stock)) {
						Long key2 = Long.valueOf(detail.orderDetailDTO.productId);
						if (!listLackRemainProduct.containsKey(key2)) {
							listLackRemainProduct.put(Long.valueOf(detail.orderDetailDTO.productId), detail);
							result = false;
						}
					}
				}
			}
		}

		return result;
	}

	public void calculateTotalQuantityUsingPopupSelectPromotion(OrderDetailViewDTO item) {
		//Check co bien kiem tra ton kho chua, neu chua thi cap nhat
		OrderDetailViewDTO orderDetail = listDistinctProduct.get(Long.valueOf(item.orderDetailDTO.productId));

		if(orderDetail == null) {
			orderDetail = new OrderDetailViewDTO();
			listDistinctProduct.put(Long.valueOf(item.orderDetailDTO.productId), orderDetail);
			SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
			orderDetail.orderDetailDTO = orderDetailDTO;

			orderDetailDTO.quantity = item.orderDetailDTO.quantity;
			orderDetailDTO.productId = item.orderDetailDTO.productId;
			orderDetail.stock = item.stock;
		}
		item.totalOrderQuantity = orderDetail;
	}

	public void checkStockTotalWithoutAccumulation() {
		listUsingProduct.clear();

		//Duyet qua ds de tinh tong so luong ban cua 1 sp
		for (OrderDetailViewDTO detail: listBuyOrders) {
			if (listUsingProduct.containsKey(detail.orderDetailDTO.productId)) {
				//Tong so luong da su dung ngoai tru KM tich luy
				listUsingProduct.put(detail.orderDetailDTO.productId, listUsingProduct.get(detail.orderDetailDTO.productId) + (int)detail.orderDetailDTO.quantity);
			} else {
				listUsingProduct.put(detail.orderDetailDTO.productId, (int)detail.orderDetailDTO.quantity);
			}
		}

		//Ds KM sp
		for (OrderDetailViewDTO detail : listPromotionOrders) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT
					&& detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
				if (listUsingProduct.containsKey(detail.orderDetailDTO.productId)) {
					//Tong so luong da su dung ngoai tru KM tich luy
					listUsingProduct.put(detail.orderDetailDTO.productId, listUsingProduct.get(detail.orderDetailDTO.productId) + (int)detail.orderDetailDTO.quantity);
				} else {
					listUsingProduct.put(detail.orderDetailDTO.productId, (int)detail.orderDetailDTO.quantity);
				}
			}
		}

		//Ds KM don hang
		//Duyet qua ds de tinh tong so luong tru vao kho cua 1 san pham
		for (OrderDetailViewDTO detail : listPromotionOrder) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						if (listUsingProduct.containsKey(detail.orderDetailDTO.productId)) {
							//Tong so luong da su dung ngoai tru KM tich luy
							listUsingProduct.put(detail.orderDetailDTO.productId, listUsingProduct.get(detail.orderDetailDTO.productId) + (int)detail.orderDetailDTO.quantity);
						} else {
							listUsingProduct.put(detail.orderDetailDTO.productId, (int)detail.orderDetailDTO.quantity);
						}
					}
				}
			}
		}

		//Ds KM mo moi
		for (OrderDetailViewDTO detail : listPromotionNewOpen) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
				List<OrderDetailViewDTO> listPromotionForPromo21 = detail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detail = detailViewDTO1;
					if (detail.type == OrderDetailViewDTO.FREE_PRODUCT && detail.orderDetailDTO.quantity >= 0) {
						if (listUsingProduct.containsKey(detail.orderDetailDTO.productId)) {
							//Tong so luong da su dung ngoai tru KM tich luy
							listUsingProduct.put(detail.orderDetailDTO.productId, listUsingProduct.get(detail.orderDetailDTO.productId) + (int)detail.orderDetailDTO.quantity);
						} else {
							listUsingProduct.put(detail.orderDetailDTO.productId, (int)detail.orderDetailDTO.quantity);
						}
					}
				}
			}
		}
	}

	/**
	*  Tinh so luong co the dat duoc cua sp KM tich luy (don vansale)
	*  @author: Nguyen Thanh Dung
	*  @return
	*  @return: boolean
	*  @throws:
	*/

	SortedMap<Integer, Integer> listUsingProduct = new TreeMap<Integer, Integer>();

	public void checkStockTotalAccumulationProduct3() {
		//Ds KH tich luy
		checkStockTotalWithoutAccumulation();
		//Duyet qua ds de tinh tong so luong tru vao kho cua 1 san pham
		for (OrderDetailViewDTO detail : listPromotionAccumulation) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
				//Co sp co gia tri
				boolean hasPromotionValue = false;
				for(OrderDetailViewDTO pPromotion: detail.listPromotionForPromo21) {
					Long key3 = pPromotion.keyList;

					if(key3 == null) {
						Long key2 = Long.valueOf(pPromotion.orderDetailDTO.productId);
						OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(key2);
						if(stockTotalAccu != null) {
							//So luong da su dung de tru kho
							long quantityUsing = 0;
							//So luong ko su dung de tru vao tong so luong da su dung
							long quantityNoUsed = 0;
							if(!listUsingProduct.containsKey(pPromotion.orderDetailDTO.productId)) {
								listUsingProduct.put(pPromotion.orderDetailDTO.productId, 0);
							}
							if(stockTotalAccu.stock <= 0) {
								quantityUsing = 0;
								quantityNoUsed = (int)pPromotion.orderDetailDTO.quantity;
							} else {
								if (listUsingProduct.get(pPromotion.orderDetailDTO.productId)
										+ pPromotion.orderDetailDTO.maxQuantityFree >= stockTotalAccu.stock) {
									quantityUsing = stockTotalAccu.stock - listUsingProduct.get(pPromotion.orderDetailDTO.productId);
									quantityNoUsed = (int) pPromotion.orderDetailDTO.maxQuantityFree - quantityUsing;
								} else {
									quantityUsing = (int)pPromotion.orderDetailDTO.maxQuantityFree;
									quantityNoUsed = 0;
								}
							}

							stockTotalAccu.orderDetailDTO.quantity -= quantityNoUsed;
							pPromotion.orderDetailDTO.quantity = (int)quantityUsing;

							//Tong so luong da su dung
							if(listUsingProduct.containsKey(pPromotion.orderDetailDTO.productId)) {
								listUsingProduct.put(pPromotion.orderDetailDTO.productId,
										(int) pPromotion.orderDetailDTO.quantity
										+ listUsingProduct.get(pPromotion.orderDetailDTO.productId));
							}

							//Co sp co lay gia tri
							if(quantityUsing > 0) {
								hasPromotionValue = true;
							}
						}
					} else if(key3 != null && listPromotionChange.get(key3) != null) {
						//Khong phai dang nA or nB
						if(!isPromotionNAorNB(listPromotionChange.get(key3))) {
							for (OrderDetailViewDTO detailViewDTO1 : detail.listPromotionForPromo21) {
								//Tim sp ma co sp doi
								if(detailViewDTO1.keyList == key3) {
									if (detailViewDTO1.type == OrderDetailViewDTO.FREE_PRODUCT && detailViewDTO1.orderDetailDTO.quantity >= 0) {
										ArrayList<OrderDetailViewDTO> listProductChange = listPromotionChange.get(key3);
										Long key2 = Long.valueOf(detailViewDTO1.orderDetailDTO.productId);
										OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(key2);
										if(stockTotalAccu != null) {
											//So luong da su dung de tru kho
											long quantityUsing = 0;
											//So luong ko su dung de tru vao tong so luong da su dung
//											long quantityNoUsed = 0;
											if(!listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
												listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId, 0);
											}
											if(stockTotalAccu.stock <= 0) {
												quantityUsing = 0;
//												quantityNoUsed = (int)detailViewDTO1.orderDetailDTO.quantity;
											} else {
												if (listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId)
														+ detailViewDTO1.orderDetailDTO.maxQuantityFree >= stockTotalAccu.stock) {
													if(listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId) < stockTotalAccu.stock) {
														quantityUsing = stockTotalAccu.stock - listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId);
													} else {
														quantityUsing = 0;
													}
												} else {
													quantityUsing = (int)detailViewDTO1.orderDetailDTO.maxQuantityFree;
												}
											}

											stockTotalAccu.orderDetailDTO.quantity -= (detailViewDTO1.orderDetailDTO.maxQuantityFree - quantityUsing);
											detailViewDTO1.orderDetailDTO.quantity = (int)quantityUsing;

											//Tong so luong da su dung
											if(listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
												listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId,
														(int) detailViewDTO1.orderDetailDTO.quantity
														+ listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId));
											}

											//Neu sp o ben ngoai ko duoc tinh thi tim sp ben trong
											if(quantityUsing == 0) {
												boolean isFoundAlterProduct = false;
												for(OrderDetailViewDTO promotion : listProductChange) {
													if(!detail.listPromotionForPromo21.contains(promotion)) {
														promotion.orderDetailDTO.quantity = 0;
													}
													calculateTotalQuantityUsingPopupSelectPromotion(promotion);
													OrderDetailViewDTO stock = listDistinctProduct.get(Long.valueOf(promotion.orderDetailDTO.productId));

													if(stock.stock > 0 && stock.orderDetailDTO.quantity < stock.stock) {
														isFoundAlterProduct = true;
														quantityUsing = (int) (stock.stock - stock.orderDetailDTO.quantity);
														if(quantityUsing > promotion.orderDetailDTO.maxQuantityFree) {
															quantityUsing = promotion.orderDetailDTO.maxQuantityFree;
														}

														stock.orderDetailDTO.quantity = stock.orderDetailDTO.quantity + quantityUsing;
														promotion.orderDetailDTO.quantity = quantityUsing;

														//Tong so luong da su dung
														if(listUsingProduct.containsKey(promotion.orderDetailDTO.productId)) {
															listUsingProduct.put(promotion.orderDetailDTO.productId,
																	(int) promotion.orderDetailDTO.quantity
																	+ listUsingProduct.get(promotion.orderDetailDTO.productId));
														}

														detail.listPromotionForPromo21.set(detail.listPromotionForPromo21.indexOf(detailViewDTO1), promotion);

														break;
													}
												}

												if(!isFoundAlterProduct) {
													stockTotalAccu.orderDetailDTO.quantity += detailViewDTO1.orderDetailDTO.maxQuantityFree;
													detailViewDTO1.orderDetailDTO.quantity = detailViewDTO1.orderDetailDTO.maxQuantityFree;

													//Tong so luong da su dung
													if(listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
														listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId,
																(int) detailViewDTO1.orderDetailDTO.quantity
																+ listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId));
													}

													//Moi chi xu ly cho truong hop KM tich luy co sp doi
													//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
													detail.notUsedAccumulationProduct = true;
												} else {
													//Moi chi xu ly cho truong hop KM tich luy co sp doi
													//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
													detail.notUsedAccumulationProduct = false;

													//Co sp co lay gia tri
													hasPromotionValue = true;
												}
											} else if (quantityUsing < detailViewDTO1.orderDetailDTO.maxQuantityFree) {
												boolean isFoundAlterProduct = false;
												for(OrderDetailViewDTO promotion : listProductChange) {
													if(!detail.listPromotionForPromo21.contains(promotion)) {
														promotion.orderDetailDTO.quantity = 0;
													}
													calculateTotalQuantityUsingPopupSelectPromotion(promotion);
													OrderDetailViewDTO stock = listDistinctProduct.get(Long.valueOf(promotion.orderDetailDTO.productId));

													if(stock.stock > 0 && stock.orderDetailDTO.quantity + promotion.orderDetailDTO.maxQuantityFree < stock.stock) {
														isFoundAlterProduct = true;
//														quantityUsing = (int) (stock.stock - stock.orderDetailDTO.quantity);
//														if(quantityUsing > promotion.orderDetailDTO.maxQuantityFree) {
															quantityUsing = promotion.orderDetailDTO.maxQuantityFree;
//														}

														stock.orderDetailDTO.quantity = stock.orderDetailDTO.quantity + quantityUsing;
														promotion.orderDetailDTO.quantity = quantityUsing;

														//Tong so luong da su dung
														if(listUsingProduct.containsKey(promotion.orderDetailDTO.productId)) {
															listUsingProduct.put(promotion.orderDetailDTO.productId,
																	(int) promotion.orderDetailDTO.quantity
																	+ listUsingProduct.get(promotion.orderDetailDTO.productId));
														}

														detail.listPromotionForPromo21.set(detail.listPromotionForPromo21.indexOf(detailViewDTO1), promotion);

														break;
													}
												}

												//Cap nhat lai thong tin cua sp cu
												if(isFoundAlterProduct) {
													stockTotalAccu.orderDetailDTO.quantity -= detailViewDTO1.orderDetailDTO.quantity;
													detailViewDTO1.orderDetailDTO.quantity = 0;

													//Tong so luong da su dung
													if(listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
														listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId,
																		listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId)
																				- (int) detailViewDTO1.orderDetailDTO.quantity);
													}
												}

												//Moi chi xu ly cho truong hop KM tich luy co sp doi
												//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
												detail.notUsedAccumulationProduct = false;

												//Co sp co lay gia tri
												hasPromotionValue = true;
											} else {
												//Moi chi xu ly cho truong hop KM tich luy co sp doi
												//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
												detail.notUsedAccumulationProduct = false;

												//Co sp co lay gia tri
												hasPromotionValue = true;
											}
										}
									}
								}
							}
						//Dang nA or nB
						} else {
							ArrayList<OrderDetailViewDTO> listProductChange = listPromotionChange.get(key3);
							OrderDetailViewDTO promotionProduct = listProductChange.get(0);
							int quantityRemain = promotionProduct.orderDetailDTO.maxQuantityFree;

							int totalQuantity = 0;
							boolean isNotEnough = false;
							for (OrderDetailViewDTO detailViewDTO1 : detail.listPromotionForPromo21) {
								if(detailViewDTO1.keyList == key3) {
									calculateTotalQuantityUsingPopupSelectPromotion(detailViewDTO1);
									OrderDetailViewDTO stock = listDistinctProduct.get(Long.valueOf(detailViewDTO1.orderDetailDTO.productId));
									//Tong so luong da su dung
									if(!listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
										listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId, 0);
									}
									if(listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId) + (int)detailViewDTO1.orderDetailDTO.quantity > stock.stock) {
										isNotEnough = true;
//									break;
									}

									totalQuantity += (int)detailViewDTO1.orderDetailDTO.quantity;
								}
							}

							if(isNotEnough || totalQuantity < quantityRemain) {
								for(OrderDetailViewDTO promotion : listProductChange) {
									if(quantityRemain >= 0) {
										if(!detail.listPromotionForPromo21.contains(promotion)) {
											promotion.orderDetailDTO.quantity = 0;
										}
										calculateTotalQuantityUsingPopupSelectPromotion(promotion);
										OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(Long.valueOf(promotion.orderDetailDTO.productId));
										listDistinctProduct.get(Long.valueOf(815));
										//So luong da su dung de tru kho
										long quantityUsing = 0;
										if(!listUsingProduct.containsKey(promotion.orderDetailDTO.productId)) {
											listUsingProduct.put(promotion.orderDetailDTO.productId, 0);
										}
										if(stockTotalAccu.stock <= 0) {
											quantityUsing = 0;
										} else {
											//So luong su dung de dat stock
											if (listUsingProduct.get(promotion.orderDetailDTO.productId)
													+ promotion.orderDetailDTO.quantity >= stockTotalAccu.stock) {
												if(listUsingProduct.get(promotion.orderDetailDTO.productId) < stockTotalAccu.stock) {
													quantityUsing = stockTotalAccu.stock - listUsingProduct.get(promotion.orderDetailDTO.productId);
												} else {
													quantityUsing = 0;
												}
											} else {
//												quantityUsing = 0;
												quantityUsing = (int)promotion.orderDetailDTO.quantity;
											}
										}

										if(quantityUsing > quantityRemain) {
											quantityUsing = quantityRemain;
										}

										stockTotalAccu.orderDetailDTO.quantity = stockTotalAccu.orderDetailDTO.quantity - promotion.orderDetailDTO.quantity + quantityUsing;
										totalQuantity = (int) (totalQuantity - promotion.orderDetailDTO.quantity + quantityUsing);
										promotion.orderDetailDTO.quantity = quantityUsing;

										quantityRemain -= quantityUsing;
									}
								}

								//Tang so luong len
								for(OrderDetailViewDTO promotion : listProductChange) {
									if(quantityRemain > 0) {
										OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(Long.valueOf(promotion.orderDetailDTO.productId));
										//So luong da su dung de tru kho
										long quantityUsing = 0;
										if(stockTotalAccu.stock <= 0) {
											quantityUsing = 0;
										} else {
											if (listUsingProduct.get(promotion.orderDetailDTO.productId)
													+ promotion.orderDetailDTO.quantity < stockTotalAccu.stock) {
												quantityUsing = (int)(stockTotalAccu.stock - listUsingProduct.get(promotion.orderDetailDTO.productId) - promotion.orderDetailDTO.quantity);
//											} else {
//												if(totalQuantity < quantityRemain) {
//													quantityUsing = stockTotalAccu.stock - listUsingProduct.get(promotion.orderDetailDTO.productId);
//												}
											}
										}

										if(quantityUsing > quantityRemain) {
											quantityUsing = quantityRemain;
										}

										stockTotalAccu.orderDetailDTO.quantity = stockTotalAccu.orderDetailDTO.quantity + quantityUsing;
										totalQuantity = (int) (totalQuantity + quantityUsing);
										promotion.orderDetailDTO.quantity += quantityUsing;

										quantityRemain -= quantityUsing;
									}
								}

								//Khong con sp nao con kho
								if(quantityRemain == promotionProduct.orderDetailDTO.maxQuantityFree) {
									promotionProduct.orderDetailDTO.quantity = promotionProduct.orderDetailDTO.maxQuantityFree;

									OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(Long.valueOf(promotionProduct.orderDetailDTO.productId));
									stockTotalAccu.orderDetailDTO.quantity += promotionProduct.orderDetailDTO.quantity;

									//Moi chi xu ly cho truong hop KM tich luy co sp doi
									//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
									detail.notUsedAccumulationProduct = true;
								} else {
									//Moi chi xu ly cho truong hop KM tich luy co sp doi
									//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
									detail.notUsedAccumulationProduct = false;

									//Co sp co lay gia tri
									hasPromotionValue = true;
								}

								//Tinh ds sp co so luong > 0
								ArrayList<OrderDetailViewDTO> listPromotionHasQuantity = new ArrayList<OrderDetailViewDTO>();
								for(OrderDetailViewDTO promotion: listProductChange) {
									if(promotion.orderDetailDTO.quantity > 0) {
										listPromotionHasQuantity.add(promotion);
									}
								}

								//Thay the bang ds moi
								if(detail.listPromotionForPromo21.size() > 0) {
									int index = -1;

									//Tim index nho nhat cua sp KM trong nhom ma nho nhat
									for(OrderDetailViewDTO promotion: listProductChange) {
										int realIndex = detail.listPromotionForPromo21.indexOf(promotion);

										if((realIndex >= 0 && realIndex < index) || index == -1) {
											index = realIndex;
										}
									}
									detail.listPromotionForPromo21.removeAll(listProductChange);
									detail.listPromotionForPromo21.addAll(index, listPromotionHasQuantity);
								}
							} else {
								//Moi chi xu ly cho truong hop KM tich luy co sp doi
								//Chua check cho TH: vua co sp ko doi & co sp co doi dang nAmB
								detail.notUsedAccumulationProduct = false;

								//Co sp co lay gia tri
								hasPromotionValue = true;
							}

							for (OrderDetailViewDTO detailViewDTO1 : detail.listPromotionForPromo21) {
								if(detailViewDTO1.keyList == key3) {
									if(listUsingProduct.containsKey(detailViewDTO1.orderDetailDTO.productId)) {
										listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId,
														(int) detailViewDTO1.orderDetailDTO.quantity
																+ listUsingProduct.get(detailViewDTO1.orderDetailDTO.productId));
									} else {
										listUsingProduct.put(detailViewDTO1.orderDetailDTO.productId, (int)detailViewDTO1.orderDetailDTO.quantity);
									}
								}
							}
						}

						//Chua tinh duoc TH ma vua co sp bat buoc & vua co sp option
						break;
					}
				}

				//Neu ko co sp nao lay gia tri
				if(!hasPromotionValue) {
					detail.notUsedAccumulationProduct = true;
					for(OrderDetailViewDTO pPromotion: detail.listPromotionForPromo21) {
						Long key3 = pPromotion.keyList;

						if(key3 == null) {
							Long key2 = Long.valueOf(pPromotion.orderDetailDTO.productId);
							OrderDetailViewDTO stockTotalAccu = listDistinctProduct.get(key2);

							pPromotion.orderDetailDTO.quantity = pPromotion.orderDetailDTO.maxQuantityFree;
							stockTotalAccu.orderDetailDTO.quantity += pPromotion.orderDetailDTO.quantity;

							//Tong so luong da su dung
							if(listUsingProduct.containsKey(pPromotion.orderDetailDTO.productId)) {
								listUsingProduct.put(pPromotion.orderDetailDTO.productId,
										(int) pPromotion.orderDetailDTO.quantity
										+ listUsingProduct.get(pPromotion.orderDetailDTO.productId));
							}
						}
					}
				} else {
					detail.notUsedAccumulationProduct = false;
				}
			}
		}
	}

	/**
	 * kiem tra se show popup doi sp hay so luong
	 *
	 * @author: DungNX
	 * @param listProductChange
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	boolean isPromotionNAorNB(ArrayList<OrderDetailViewDTO> listProductChange) {
		boolean isChangeNum = true;
		for (int i = 1; i < listProductChange.size(); i++) {
			if (listProductChange.get(i).orderDetailDTO.maxQuantityFree != listProductChange
					.get(i - 1).orderDetailDTO.maxQuantityFree) {
				isChangeNum = false;
				break;
			}
		}
		return isChangeNum;
	}

	/**
	 * Phat sinh ra cau lenh sql chuyen len server - clone cho PO_CUSTOMER, PO_CUSTOMER_DETAIL
	 *
	 * @author: DungNX3
	 * @param listSql2
	 * @return: JSONArray
	 * @throws:
	 */
	public JSONArray generateNewPOCustomerSql(JSONArray listSql) {
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId());

		// insert into SalesOrder
		listSql.put(orderInfo.generateCreatePOCustomerSql());

		for (OrderDetailViewDTO detail : listProduct) {
			detail.orderDetailDTO.createDate = orderInfo.createDate;
			detail.orderDetailDTO.createUser = orderInfo.createUser;
			detail.orderDetailDTO.shopId = shopId;
			detail.orderDetailDTO.staffId = staffId;
			// insert sales order detail
			listSql.put(detail.orderDetailDTO.generateCreatePOCustomerDetailSql());

			//insert them phan chi tiet khuyen mai
			if (detail.listPromoDetail != null && !detail.listPromoDetail.isEmpty()) {
				for (SaleOrderPromoDetailDTO promoDetail : detail.listPromoDetail) {
					if(promoDetail.discountAmount > 0) {
						listSql.put(promoDetail.generateJsonPOCustomerPromoDetail());
					}
				}
			}

			//insert them phan chi tiet khuyen mai cho PO
			if (detail.lstPromotionMap != null && !detail.lstPromotionMap.isEmpty()) {
				for (SalePromoMapDTO promoDetail : detail.lstPromotionMap) {
						listSql.put(promoDetail.generateInsertPOCustomerPromoMapSql());
				}
			}
		}

		//Ghi log khi don hang bi rot chi tiet
		if(listProduct.isEmpty()) {
			ServerLogger.sendLog("Don hang khong co chi tiet khi gen json createPo: " + orderInfo.poId, TabletActionLogDTO.LOG_CLIENT);
		}

		//tien KM tich luy
//		for (SaleOrderPromoDetailDTO promoDetail : listPromoDetail) {
//			listSql.put(promoDetail.generateJsonPOCustomerPromoDetail());
//		}

		//gen json so suat
		if (productQuantityReceivedList != null) {
			for (SaleOrderPromotionDTO salePromotion : productQuantityReceivedList) {
				listSql.put(salePromotion.generateJsonInsertPoPromotion());
			}
		}

		//gen json cho po_lot
		if (listSaleOrderLot != null) {
			for (SaleOrderLotDTO saleOrderLot : listSaleOrderLot) {
				listSql.put(saleOrderLot.generateJsonInsertPoLot());
			}
		}

		//gen json cho sale_order_promo_lot
		if (listSaleOrderPromoLot != null) {
			for (SaleOrderPromoLotDTO saleOrderPromoLot : listSaleOrderPromoLot) {
				listSql.put(saleOrderPromoLot.generateJsonPOCustomerPromoLot());
			}
		}

		return listSql;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONArray
	 * @throws:
	*/
	public JSONArray generateReturnOrderSql() {
		JSONArray listSql = new JSONArray();
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		int shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());

		// insert into SalesOrder
		listSql.put(orderInfo.generateCreateSql());

		for (OrderDetailViewDTO detail: listBuyOrders) {
			if (detail.orderDetailDTO.quantity > 0){
				// insert sales order detail
				listSql.put(detail.orderDetailDTO.generateCreateSql());

				// cap nhat stock total
				StockTotalDTO stockTotal = StockTotalDTO.createStockTotalInfoReturnOrder(detail.orderDetailDTO);
				// vansale
				listSql.put(stockTotal.generateIncreaseSql());
			}
		}
		// ds san pham khuyen mai
		for (OrderDetailViewDTO promotionDetail : listPromotionOrders) {
			if (promotionDetail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT
					&& ((promotionDetail.type != OrderDetailViewDTO.FREE_PRODUCT) || (promotionDetail.type == OrderDetailViewDTO.FREE_PRODUCT && promotionDetail.orderDetailDTO.quantity >= 0))) {
				// insert sales order detail
				listSql.put(promotionDetail.orderDetailDTO.generateCreateSql());

				// cap nhat stock total
				StockTotalDTO stockTotal = StockTotalDTO.createStockTotalInfoReturnOrder(promotionDetail.orderDetailDTO);
				// vansale
				listSql.put(stockTotal.generateIncreaseSql());
			} else if (promotionDetail.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
				List<OrderDetailViewDTO> listPromotionForPromo21 =  promotionDetail.listPromotionForPromo21;
				for (OrderDetailViewDTO detailViewDTO1 : listPromotionForPromo21) {
					detailViewDTO1.orderDetailDTO.createDate = orderInfo.createDate;
					detailViewDTO1.orderDetailDTO.createUser = orderInfo.createUser;
					detailViewDTO1.orderDetailDTO.shopId = shopId;
					detailViewDTO1.orderDetailDTO.staffId = staffId;
					// insert sales order detail
					listSql.put(detailViewDTO1.orderDetailDTO.generateCreateSql());

					// cap nhat stock total
					StockTotalDTO stockTotal = StockTotalDTO.createStockTotalInfoReturnOrder(detailViewDTO1.orderDetailDTO);
					// vansale
					listSql.put(stockTotal.generateIncreaseSql());
				}
			}
		}

		//gen json cho sale_order_lot
		if (listSaleOrderLot != null) {
			for (SaleOrderLotDTO saleOrderLot : listSaleOrderLot) {
				listSql.put(saleOrderLot.generateJsonInsertSaleOrderLot());
			}
		}
		//gen json cho sale_order_lot
		if (listSaleOrderPromoLot != null) {
			for (SaleOrderPromoLotDTO saleOrderPromoLot : listSaleOrderPromoLot) {
				listSql.put(saleOrderPromoLot.generateJsonSaleOrderPromoLot());
			}
		}

		//insert them phan chi tiet khuyen mai
		if (listSaleOrderPromoDetailDTO != null && !listSaleOrderPromoDetailDTO.isEmpty()) {
			for (SaleOrderPromoDetailDTO promoDetail : listSaleOrderPromoDetailDTO) {
				listSql.put(promoDetail.generateJsonSaleOrderPromoDetail());
			}
		}

		//insert them phan chi tiet khuyen mai dat hay ko dat
		if (listSalePromomapDTO != null && !listSalePromomapDTO.isEmpty()) {
			for (SalePromoMapDTO promoDetail : listSalePromomapDTO) {
					listSql.put(promoDetail.generateInsertSql());
			}
		}

		//gen json so suat
		if (listSaleOrderPromotionDTO != null) {
			for (SaleOrderPromotionDTO salePromotion : listSaleOrderPromotionDTO) {
				listSql.put(salePromotion.generateJsonInsertSaleOrderPromotion());
			}
		}

		// thong nhat chi cap nhat so suat luc in don vansale tren web
		// giam so xuat cho don hang vansale
//		if (listIncreasePromoQuantityReceived != null) {
//			for (QuantityRevicevedDTO quanDto : this.listIncreasePromoQuantityReceived) {
//				quanDto.generateDescreasePromotionSqlVansale(listSql);
//			}
//		}

		if (listIncreasePromoQuantityReceived != null) {
			for (QuantityRevicevedDTO quanDto : this.listIncreasePromoQuantityReceived) {
				quanDto.generateInsertPromotionMapDelta(listSql);
			}
		}

		for(RptCttlPayDTO rptCttlPay : listRptCttPay) {
			listSql.put(rptCttlPay.generateInsertRptCttlPay());

			for(RptCttlDetailPayDTO rptDetailPay : rptCttlPay.rptCttlDetailList) {
				listSql.put(rptDetailPay.generateJsonRptCttlDetailPay());
			}
		}

		// cap nhat no KH
		if (debitIdExist <= 0) {
			// khong ton tai no KH
			listSql.put(debitDto.generateInsertFromOrderSql());
		}
		listSql.put(debitDetailDto.generateInsertFromOrderSql());

		return listSql;
	}

	/**
	 * Tinh toan xem neu sua so luong ve 0 thi so suat dua ve 0
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void checkQuantityReceived() {
		ArrayList<OrderDetailViewDTO> listPromotion = new ArrayList<OrderDetailViewDTO>();
		listPromotion.addAll(listPromotionOrders);
		listPromotion.addAll(listPromotionOrder);
		listPromotion.addAll(listPromotionNewOpen);
		for	(SaleOrderPromotionDTO quantityReceived : productQuantityReceivedList) {
			//Chi check nhung KM san pham
			//Co KM sp & KHONG co KM tien
			if(quantityReceived.hasProduct == 1 && !quantityReceived.hasMoney) {
				boolean hasQuantity = false;
				for(OrderDetailViewDTO promotion: listPromotion) {
					if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
						if(quantityReceived.promotionProgramCode.equals(promotion.orderDetailDTO.programeCode)
								&& quantityReceived.productGroupId == promotion.orderDetailDTO.productGroupId
								&& quantityReceived.promotionLevel == promotion.orderDetailDTO.promotionOrderNumber
								) {
							if(promotion.orderDetailDTO.quantity > 0) {
								hasQuantity = true;
								break;
							}
						}
					} else if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
							|| promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
						for(OrderDetailViewDTO promotionZV21: promotion.listPromotionForPromo21) {
							if(quantityReceived.promotionProgramCode.equals(promotionZV21.orderDetailDTO.programeCode)
									&& quantityReceived.productGroupId == promotionZV21.orderDetailDTO.productGroupId
									&& quantityReceived.promotionLevel == promotionZV21.orderDetailDTO.promotionOrderNumber
									) {
								if(promotionZV21.orderDetailDTO.quantity > 0) {
									hasQuantity = true;
									break;
								}
							}
						}

						//Neu 1 sp co so luong thi coi nhu ca KM ZV1 co so luong
						if(hasQuantity) {
							break;
						}
					}

				}

				if(!hasQuantity) {
					//Giam so suat huong ve 0
					for(QuantityRevicevedDTO quanDto : mapQuantityReviceved.values()) {
						if(quanDto.getProId() == quantityReceived.promotionProgramId) {
							quanDto.setQuantityReviceved(quanDto
									.getQuantityReviceved() - quantityReceived.quantityReceived);
							quanDto.setNumReceived(quanDto.getNumReceived()
									- quantityReceived.numReceived);
							quanDto.setAmountReceived(quanDto
									.getAmountReceived() - quantityReceived.amountReceived);

							break;
						}
					}

					quantityReceived.quantityReceived = 0;
					quantityReceived.numReceived = 0;
				}
			}
		}
	}

	/**
	 * kiem tra xem don hang co chuong trinh huy doi tra nao khong
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public boolean hasProgramePromotionCancelChangeReturn(){
		boolean hasProgram = false;

		if(listPromotionOrders!=null){
			for(OrderDetailViewDTO item : listPromotionOrders){
				if(item.orderDetailDTO != null){
					if (item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL
						|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE
						|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN) {
						hasProgram = true;
						break;
					}
				}
			}
		}

		return hasProgram;
	}

	/**
	 * kiem tra xem don hang co KM tay
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public String hasProgramePromotionManual(){
		StringBuilder listProgram = new StringBuilder();
		if(listPromotionOrders!=null){
			for(OrderDetailViewDTO item : listPromotionOrders){
				if(item.orderDetailDTO != null){
					if ((item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL
							|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION
							|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL
							|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE
							|| item.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
							)
							&& !StringUtil.isNullOrEmpty(item.orderDetailDTO.programeCode)
							&& listProgram.indexOf(item.orderDetailDTO.programeCode) < 0) {
						listProgram.append(item.orderDetailDTO.programeCode).append(", ");
					}
				}
			}
		}

		if(listProgram.length() > 0) {
			listProgram.delete(listProgram.length() - 2, listProgram.length() - 1);
		}

		return listProgram.toString();
	}

	/**
	 * kiem tra xem don hang co KM tay
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public String hasPromotionProductChangeQuantity(){
		StringBuilder result = new StringBuilder();

		for(OrderDetailViewDTO promotion: listPromotionOrders) {
			if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO
					&& promotion.orderDetailDTO.quantity != promotion.orderDetailDTO.maxQuantityFree
					&& result.indexOf(promotion.productCode) < 0) {
				result.append(promotion.productCode).append(", ");
			}
		}

		for(OrderDetailViewDTO group: listPromotionAccumulation) {
			for(OrderDetailViewDTO promotion: group.listPromotionForPromo21) {
				if (promotion.orderDetailDTO.quantity != promotion.orderDetailDTO.maxQuantityFree
						&& result.indexOf(promotion.productCode) < 0) {
					result.append(promotion.productCode).append(", ");
				}
			}
		}

		for(OrderDetailViewDTO group: listPromotionNewOpen) {
			for(OrderDetailViewDTO promotion: group.listPromotionForPromo21) {
				if (promotion.orderDetailDTO.quantity != promotion.orderDetailDTO.maxQuantityFree
						&& result.indexOf(promotion.productCode) < 0) {
					result.append(promotion.productCode).append(", ");
				}
			}
		}

		for(OrderDetailViewDTO group: listPromotionOrder) {
			for(OrderDetailViewDTO promotion: group.listPromotionForPromo21) {
				if (promotion.orderDetailDTO.quantity != promotion.orderDetailDTO.maxQuantityFree
						&& result.indexOf(promotion.productCode) < 0) {
					result.append(promotion.productCode).append(", ");
				}
			}
		}

		if(result.length() > 0) {
			result.delete(result.length() - 2, result.length() - 1);
		}

		return result.toString();
	}

	/**
	 * Luu ds CTKM tinh duoc vao ds CTKM don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param listPromotion
	 */
	public void setPromotionProduct(ArrayList<OrderDetailViewDTO> listPromotion) {
		// giu lai cac mat hang khuyen mai chon tu man hinh them hang
		for (int i = 0, size = listPromotionOrders.size(); i < size; i++) {
			OrderDetailViewDTO promotion = listPromotionOrders.get(i);
			if (promotion.orderDetailDTO.programeType != PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
				listPromotion.add(0, promotion);

				//Tao ra so suat cho KM tay, huy, doi, tra
				if(promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
						) {
					SaleOrderPromotionDTO quantityReceived = new SaleOrderPromotionDTO();
					quantityReceived.initHTTMQuantityReceived(promotion);
					productQuantityReceivedList.add(quantityReceived);
				}
			}
		}
		listPromotionOrders.clear();
		listPromotionOrders.addAll(listPromotion);
	}

	/**
	 * Tinh toan chiet khau cua don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void calculateDiscount() {
		orderInfo.setDiscount(0);
		promotionDiscount = 0;
		promotionMaxDiscount = 0;
		SortedMap<String, OrderDetailViewDTO> sortAccumulationList = new TreeMap<String, OrderDetailViewDTO>();
		ArrayList<OrderDetailViewDTO> listPromotionAccumulationProduct = new ArrayList<OrderDetailViewDTO>();

		for(OrderDetailViewDTO product : listBuyOrders) {
			orderInfo.adddDiscount(product.orderDetailDTO.getDiscountAmount());
		}

//		for(OrderDetailViewDTO product: listPromotionNewOpen) {
//			promotionDiscount += product.orderDetailDTO.discountAmount;
//		}

		if(isCalculatePromotion) {
			isCalculatePromotion = false;
			listPromotionAccumulation.clear();
			for(OrderDetailViewDTO buyProduct : listBuyOrders) {
				ArrayList<SaleOrderPromoDetailDTO> listPromoDetailRemove = new ArrayList<SaleOrderPromoDetailDTO>();
				for(SaleOrderPromoDetailDTO buyPromotion: buyProduct.listPromoDetail) {
					if(buyPromotion.programTypeCode.equals(CalPromotions.ZV23)) {
						buyProduct.orderDetailDTO.subtractDiscountAmount(buyPromotion.discountAmount);
						orderInfo.subtractDiscount(buyPromotion.discountAmount);
//						orderDTO.orderInfo.total += buyPromotion.discountAmount;

						listPromoDetailRemove.add(buyPromotion);
					}
				}

				//Loai bo cac chi tiet KM cu~
				buyProduct.listPromoDetail.removeAll(listPromoDetailRemove);
			}
			for (OrderDetailViewDTO promotion : listPromotionAccumulationTemp) {
				if(!promotion.notGetThisPromotion) {
					//Tinh so tien tich luy don hang co the nhan duoc
					if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY) {

						//Tien chiet khau van chua vuot qua so tien don hang
						if(orderInfo.getDiscount() + promotionDiscount + promotion.orderDetailDTO.maxAmountFree <= orderInfo.getAmount()) {
							String key = promotion.orderDetailDTO.programeCode;
							if(sortAccumulationList.containsKey(key)) {
								OrderDetailViewDTO groupDetail = sortAccumulationList.get(key);
								groupDetail.orderDetailDTO.addDiscountAmount(promotion.orderDetailDTO.getDiscountAmount());
								groupDetail.orderDetailDTO.maxAmountFree += promotion.orderDetailDTO.maxAmountFree;
								groupDetail.rptCttlPay.promotion += 1;

								for(RptCttlDetailPayDTO second : promotion.rptCttlPay.rptCttlDetailList) {
									boolean isExist = false;
									for(RptCttlDetailPayDTO first: groupDetail.rptCttlPay.rptCttlDetailList) {
										if(second.productId == first.productId) {
											first.amountPromotion += second.amountPromotion;
											groupDetail.rptCttlPay.totalAmountPayPromotion += second.amountPromotion;

											if(promotion.rptCttlPay.promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
												if(first.isConvertProduct) {
													BigDecimal firstQuantityPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", first.quantityPromotion));
													//So luong sp quy doi su dung
//													BigDecimal amountPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#", first.amountPromotion));
													BigDecimalRound amountPromotion = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", first.amountPromotion));
													//Amount sp quy doi su dung
													BigDecimal quantityPromotion = amountPromotion.multiply(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", first.quantityBegin)))
//															.divide(new BigDecimal(first.amountBegin), 2, RoundingMode.DOWN);
															.divide(new BigDecimal(first.amountBegin)).getValue();
													first.quantityPromotion = quantityPromotion.doubleValue();

													BigDecimal groupTotalQuantityPayPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", groupDetail.rptCttlPay.totalQuantityPayPromotion));
													groupDetail.rptCttlPay.totalQuantityPayPromotion = groupTotalQuantityPayPromotion.subtract(firstQuantityPromotion).add(quantityPromotion).doubleValue();
												}
											} else if(promotion.rptCttlPay.promotionValueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
												BigDecimal firstQuantityPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", first.quantityPromotion));
												BigDecimal secondQuantityPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", second.quantityPromotion));

												first.quantityPromotion = firstQuantityPromotion.add(secondQuantityPromotion).doubleValue();

												BigDecimal groupTotalQuantityPayPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", groupDetail.rptCttlPay.totalQuantityPayPromotion));
												groupDetail.rptCttlPay.totalQuantityPayPromotion = groupTotalQuantityPayPromotion.add(secondQuantityPromotion).doubleValue();
											}


											isExist = true;
											break;
										}
									}
									if(!isExist) {
										RptCttlDetailPayDTO secondClone = second.clone();
										groupDetail.rptCttlPay.rptCttlDetailList.add(secondClone);

										BigDecimal secondQuantityPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", second.quantityPromotion));
										BigDecimal groupTotalQuantityPayPromotion = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", groupDetail.rptCttlPay.totalQuantityPayPromotion));
										groupDetail.rptCttlPay.totalQuantityPayPromotion = groupTotalQuantityPayPromotion.add(secondQuantityPromotion).doubleValue();
										groupDetail.rptCttlPay.totalAmountPayPromotion += secondClone.amountPromotion;
									}
								}
							} else {
								OrderDetailViewDTO promotionClone = promotion.cloneAccumulationPromotion();
								sortAccumulationList.put(key, promotionClone);
							}
							promotionDiscount += promotion.orderDetailDTO.getDiscountAmount();
							promotionMaxDiscount += promotion.orderDetailDTO.maxAmountFree;
						} else {
							String key = promotion.orderDetailDTO.programeCode;
							if(sortAccumulationList.containsKey(key)) {
								OrderDetailViewDTO groupDetail = sortAccumulationList.get(key);
								groupDetail.remainDiscount += promotion.orderDetailDTO.maxAmountFree;
							} else {
								OrderDetailViewDTO promotionClone = promotion.cloneAccumulationPromotion();
								promotionClone.resetValueUsing();
								promotionClone.remainDiscount += promotion.orderDetailDTO.maxAmountFree;
								sortAccumulationList.put(key, promotionClone);
							}
						}

						//KM Tich luy sp
					} else {
						listPromotionAccumulationProduct.add(promotion);
					}
				}
			}

			listPromotionAccumulation.addAll(sortAccumulationList.values());
			listPromotionAccumulation.addAll(listPromotionAccumulationProduct);

			//Remove cac ZV23
			for(OrderDetailViewDTO product: listBuyOrders) {
				ArrayList<SaleOrderPromoDetailDTO> listPromoRemove = new ArrayList<SaleOrderPromoDetailDTO>();
				for(SaleOrderPromoDetailDTO promoDetail : product.listPromoDetail) {
					if(CalPromotions.ZV23.equals(promoDetail.programTypeCode)) {
						listPromoRemove.add(promoDetail);
					}
				}

				product.listPromoDetail.removeAll(listPromoRemove);
			}


			//Phan chia tien ra cac sp ban cua KM tich luy
			listPromotionAccumulationProduct.clear();
			for (OrderDetailViewDTO promotion : listPromotionAccumulation) {
				if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY) {
					double proDisAmount = promotion.orderDetailDTO.getDiscountAmount();
					if(proDisAmount > 0) {
						//Tinh lai
						BigDecimalRound discountAmountT = new BigDecimalRound(proDisAmount);
//						BigDecimal discountPercent = discountAmountT.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT)).divide(new BigDecimal(orderInfo.getAmount()), 2, RoundingMode.HALF_UP);
						BigDecimalRound discountPercent = discountAmountT.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT)).divide(new BigDecimal(orderInfo.getAmount()));

						//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
						double totalDiscount = 0;
						double fixDiscountAmount = proDisAmount;
						//KM sp
						int numProduct = 0;
						for (int i = listBuyOrders.size() - 1; i >= 0; i--) {
							OrderDetailViewDTO product = listBuyOrders.get(i);
							numProduct++;
//							double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).doubleValue();
							double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
							//Tinh toan de lam tron, tong so tien <= tong tien KM
							if(discountAmount > fixDiscountAmount) {
								discountAmount = fixDiscountAmount;
							}

							if(totalDiscount + discountAmount > fixDiscountAmount) {
								discountAmount = fixDiscountAmount - totalDiscount;
							} else {
								if(numProduct == listBuyOrders.size()) {
									discountAmount = fixDiscountAmount - totalDiscount;
								}
							}
							totalDiscount += discountAmount;
							product.orderDetailDTO.addDiscountAmount(discountAmount);

							//promo detail sau khi tinh khuyen mai lai
							SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
							promoDetail.updateData(promotion, discountAmount);
							promoDetail.maxAmountFree = promotion.orderDetailDTO.maxAmountFree;
							//km don hang isOwner = 2
							promoDetail.isOwner = 2;
							product.listPromoDetail.add(promoDetail);
						}
					} else {
						listPromotionAccumulationProduct.add(promotion);
					}
				}
			}

			//Remove ds KM tich luy tien ma ko duoc lan dat nao ca
			listPromotionAccumulation.removeAll(listPromotionAccumulationProduct);
		}
	}

	/**
	 * Tinh toan so suat, so tien, so luong duoc huong
	 * dua vao cac bang phan bo promotion_shop_map, promotion_staff_map, promotion_customer_map
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void checkQuantityReceivedFull() throws Exception {
		PROMOTION_SHOP_MAP psmTable = new PROMOTION_SHOP_MAP(SQLUtils.getInstance().getmDB());
		PROMOTION_STAFF_MAP pstmTable = new PROMOTION_STAFF_MAP(SQLUtils.getInstance().getmDB());
		PROMOTION_CUSTOMER_MAP pcmTable = new PROMOTION_CUSTOMER_MAP(SQLUtils.getInstance().getmDB());

		ArrayList<OrderDetailViewDTO> listPromotion = new ArrayList<OrderDetailViewDTO>();
		listPromotion.addAll(listPromotionOrders);
		listPromotion.addAll(listPromotionOrder);
		listPromotion.addAll(listPromotionNewOpen);
		listPromotion.addAll(listPromotionOrdersMoney);
		listPromotion.addAll(listPromotionNewOpenMoney);

		//Gom cac so suat
		HashMap<Long, ArrayList<ArrayList<SaleOrderPromotionDTO>>> mapQuantityRevicevedList = new HashMap<Long, ArrayList<ArrayList<SaleOrderPromotionDTO>>>();
		mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();
		mapQuantityReceiveMissing = new HashMap<Long, QuantityRevicevedDTO>();
		// nhap chung nhieu group cua 1 chuong trinh lai
		for (SaleOrderPromotionDTO quantityReceived : productQuantityReceivedList) {
			//So suat tong
			QuantityRevicevedDTO quanDto = mapQuantityReviceved.get(quantityReceived.promotionProgramId);
			if (quanDto == null) {
				quanDto = new QuantityRevicevedDTO(quantityReceived.promotionProgramId, quantityReceived.promotionProgramCode);
				quanDto.setProName(quantityReceived.promotionProgramName);
				mapQuantityReviceved.put(quantityReceived.promotionProgramId, quanDto);
			}

			//Reset gia tri khi thay doi so luong do doi sp tuy chon
			quantityReceived.quantityReceived = quantityReceived.quantityReceivedMax;
			quantityReceived.numReceived = quantityReceived.numReceivedMax;
			quantityReceived.amountReceived = quantityReceived.amountReceivedMax;

			// cong them so suat
			quanDto.setQuantityReviceved(quanDto.getQuantityReviceved()
					+ quantityReceived.quantityReceived);
			quanDto.setQuantityRevicevedMax(quanDto.getQuantityReviceved());
			// luu thong tin so tien/ so luong nhan duoc cua mot CTKM
			quanDto.setNumReceived(quanDto.getNumReceived() + quantityReceived.numReceived);
			quanDto.setAmountReceived(quanDto.getAmountReceived()
					+ quantityReceived.amountReceived);

			//Danh sach so suat
			ArrayList<ArrayList<SaleOrderPromotionDTO>> salePromotionGroupList = mapQuantityRevicevedList.get(quantityReceived.promotionProgramId);
			if(salePromotionGroupList == null) {
				salePromotionGroupList = new ArrayList<ArrayList<SaleOrderPromotionDTO>>();
				mapQuantityRevicevedList.put(quantityReceived.promotionProgramId, salePromotionGroupList);
			}

			boolean isExistGroup = false;
			int indexGroup, sizeGroup = salePromotionGroupList.size();

			for(indexGroup = 0; indexGroup < sizeGroup; indexGroup++) {
				ArrayList<SaleOrderPromotionDTO> salePromotionDtoList = salePromotionGroupList.get(indexGroup);

				for(SaleOrderPromotionDTO salePromotionDto: salePromotionDtoList) {
					if(salePromotionDto.productGroupId == quantityReceived.productGroupId) {
						isExistGroup = true;
						break;
					}
				}

				if(isExistGroup == true) {
					break;
				}
			}

			//Tao nhom neu chua co
			ArrayList<SaleOrderPromotionDTO> salePromotionDtoList = null;
			if(isExistGroup) {
				salePromotionDtoList = salePromotionGroupList.get(indexGroup);
			} else {
				salePromotionDtoList = new ArrayList<SaleOrderPromotionDTO>();

				//Tim index cua group de add vao ds: ds tang dan
				indexGroup = 0;
				sizeGroup = salePromotionGroupList.size();

				for(indexGroup = 0; indexGroup < sizeGroup; indexGroup++) {

					ArrayList<SaleOrderPromotionDTO> salePromotionLevelList = salePromotionGroupList.get(indexGroup);

					SaleOrderPromotionDTO salePromotionDto = salePromotionLevelList.get(0);

					if(quantityReceived.groupOrderNumber == 0 || quantityReceived.groupOrderNumber > salePromotionDto.groupOrderNumber) {
						break;
					}
				}
				salePromotionGroupList.add(indexGroup, salePromotionDtoList);
			}

			//KIEM TRA LAI CO BI LOI NEU ROI VAO TRUONG HOP DAU TIEN HAY KO?
			int index = 0, size = salePromotionDtoList.size();
			for (index = 0; index < size; index++) {
				SaleOrderPromotionDTO salePromotionDto = salePromotionDtoList.get(index);

				if(quantityReceived.promotionLevel > salePromotionDto.promotionLevel) {
					break;
				}
			}

			salePromotionDtoList.add(index, quantityReceived);
//			salePromotionDtoList.put((long) quantityReceived.promotionLevel, quantityReceived);
		}

		int nullQuantityValue = Integer.MAX_VALUE;
		int nullNumValue = Integer.MAX_VALUE;
		double nullAmountValue = Double.MAX_VALUE;

		for (QuantityRevicevedDTO quanDto : mapQuantityReviceved.values()) {
			PromotionShopMapDTO promotionShopMapDTO = psmTable.getPromotionShopMapByPromotionProgramId(
					quanDto.getProId(), GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());


			//Loai doi tuong dang lay so suat, so luong, so tien min
			int promotionQuantityCheck = SaleOrderPromotionDTO.TYPE_CHECK_NULL;
			int promotionNumCheck = SaleOrderPromotionDTO.TYPE_CHECK_NULL;
			int promotionAmountCheck = SaleOrderPromotionDTO.TYPE_CHECK_NULL;

			int quantityConfigRemain = nullQuantityValue;
			// so luong nhan con lai
			int numConfigRemain = nullNumValue;
			// so tien nhan con lai
			double amountConfigRemain = nullAmountValue;

			//Luu thong promotion detail
			int numPromotionUnit = 3;
			int numPromotionObject = 3;
			int numPromotionValue = 2;
			double[][][] promotionDetailValue = new double[numPromotionUnit][numPromotionObject][numPromotionValue];
			for(int i = 0; i < numPromotionUnit; i++) {
				for (int j = 0; j < numPromotionObject; j++) {
					for(int k = 0; k < numPromotionValue; k++) {
						promotionDetailValue[i][j][k] = -1;
					}
				}
			}
			//End: Luu thong promotion detail

			if (promotionShopMapDTO != null) {
				quanDto.setShopMapID(promotionShopMapDTO.promotionShopMapId);

				PromotionStaffMapDTO promotionStaffMapDTO = pstmTable.getPromotionStaffMapByPromotionShopMapID(
						orderInfo.staffId, orderInfo.shopId, promotionShopMapDTO.promotionShopMapId);
				PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable.getPromotionShopMapByPromotionShopMapID(
								orderInfo.customerId, orderInfo.shopId, promotionShopMapDTO.promotionShopMapId);

				// thi su dung so suat cua NPP
				if(promotionShopMapDTO.quantityMax == nullQuantityValue) {
					quantityConfigRemain = promotionShopMapDTO.quantityMax;
				} else {
					quantityConfigRemain = promotionShopMapDTO.quantityMax - promotionShopMapDTO.quantityReceivedTotal;
					promotionQuantityCheck = SaleOrderPromotionDTO.TYPE_CHECK_SHOP;
				}

				if(promotionShopMapDTO.numMax == nullNumValue) {
					numConfigRemain = promotionShopMapDTO.numMax;
				} else {
					numConfigRemain = promotionShopMapDTO.numMax - promotionShopMapDTO.numReceivedTotal;
					promotionNumCheck = SaleOrderPromotionDTO.TYPE_CHECK_SHOP;
				}

//				if(promotionShopMapDTO.amountMax == nullAmountValue) {
				if(BigDecimal.valueOf(promotionShopMapDTO.amountMax).compareTo(BigDecimal.valueOf(nullAmountValue)) == 0) {
					amountConfigRemain = promotionShopMapDTO.amountMax;
				} else {
					amountConfigRemain = promotionShopMapDTO.amountMax - promotionShopMapDTO.amountReceivedTotal;
					promotionAmountCheck = SaleOrderPromotionDTO.TYPE_CHECK_SHOP;
				}

				//Set value for promotion detail
				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][0] = promotionShopMapDTO.quantityReceivedTotal;
				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][1] = quantityConfigRemain;

				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][0] = promotionShopMapDTO.numReceivedTotal;
				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][1] = numConfigRemain;

				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][0] = promotionShopMapDTO.amountReceivedTotal;
				promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_SHOP -1][1] = amountConfigRemain;

				// co khai bao staff map
				if (promotionStaffMapDTO != null) {
					quanDto.setStaffMapId(promotionStaffMapDTO.promotionStaffMapId);

					// neu so suat con lai cua kh nho hon cua nv
					// thi cap nhat lai so suat ton
					int quantityTemp = promotionStaffMapDTO.quantityMax == nullQuantityValue ? promotionStaffMapDTO.quantityMax : promotionStaffMapDTO.quantityMax - promotionStaffMapDTO.quantityReceivedTotal;
					int numTemp = promotionStaffMapDTO.numMax == nullNumValue ? promotionStaffMapDTO.numMax : promotionStaffMapDTO.numMax - promotionStaffMapDTO.numReceivedTotal;
					double amountTemp = BigDecimal.valueOf(promotionStaffMapDTO.amountMax).compareTo(BigDecimal.valueOf(nullAmountValue)) == 0 ? promotionStaffMapDTO.amountMax : promotionStaffMapDTO.amountMax - promotionStaffMapDTO.amountReceivedTotal;
//					double amountTemp = promotionStaffMapDTO.amountMax == nullAmountValue ? promotionStaffMapDTO.amountMax : promotionStaffMapDTO.amountMax - promotionStaffMapDTO.amountReceivedTotal;

					//Set value for promotion detail
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][0] = promotionStaffMapDTO.quantityReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][1] = quantityTemp;

					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][0] = promotionStaffMapDTO.numReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][1] = numTemp;

					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][0] = promotionStaffMapDTO.amountReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_STAFF -1][1] = amountTemp;

					if (quantityTemp < quantityConfigRemain) {
						quantityConfigRemain = quantityTemp;
						promotionQuantityCheck = SaleOrderPromotionDTO.TYPE_CHECK_STAFF;
					}
					if (numTemp < numConfigRemain) {
						numConfigRemain = numTemp;
						promotionNumCheck = SaleOrderPromotionDTO.TYPE_CHECK_STAFF;
					}
					if (amountTemp < amountConfigRemain) {
						amountConfigRemain = amountTemp;
						promotionAmountCheck = SaleOrderPromotionDTO.TYPE_CHECK_STAFF;
					}
				}

				// co khai bao customer map
				if (promotionCustomerMapDTO != null) {
					quanDto.setCustomerMapId(promotionCustomerMapDTO.promotionCustomerMapId);

					// neu so suat con lai cua kh nho hon cua nv
					// thi cap nhat lai so suat ton
					int quantityTemp = promotionCustomerMapDTO.quantityMax == nullQuantityValue ? promotionCustomerMapDTO.quantityMax : promotionCustomerMapDTO.quantityMax - promotionCustomerMapDTO.quantityReceivedTotal;
					int numTemp = promotionCustomerMapDTO.numMax == nullNumValue ? promotionCustomerMapDTO.numMax : promotionCustomerMapDTO.numMax - promotionCustomerMapDTO.numReceivedTotal;
					double amountTemp = BigDecimal.valueOf(promotionCustomerMapDTO.amountMax).compareTo(BigDecimal.valueOf(nullAmountValue)) == 0 ? promotionCustomerMapDTO.amountMax : promotionCustomerMapDTO.amountMax - promotionCustomerMapDTO.amountReceivedTotal;
//					double amountTemp = promotionCustomerMapDTO.amountMax == nullAmountValue ? promotionCustomerMapDTO.amountMax : promotionCustomerMapDTO.amountMax - promotionCustomerMapDTO.amountReceivedTotal;

					//Set value for promotion detail
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][0] = promotionCustomerMapDTO.quantityReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][1] = quantityTemp;

					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][0] = promotionCustomerMapDTO.numReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_NUM -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][1] = numTemp;

					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][0] = promotionCustomerMapDTO.amountReceivedTotal;
					promotionDetailValue[SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT -1][SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER -1][1] = amountTemp;

					if (quantityTemp < quantityConfigRemain) {
						quantityConfigRemain = quantityTemp;
						promotionQuantityCheck = SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER;
					}
					if (numTemp < numConfigRemain) {
						numConfigRemain = numTemp;
						promotionNumCheck = SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER;
					}
					if (amountTemp < amountConfigRemain) {
						amountConfigRemain = amountTemp;
						promotionAmountCheck = SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER;
					}
				}

				//Luu thong tin min cac so suat, so luong, so tien
				quanDto.setQuantityRevicevedMin(quantityConfigRemain);
				quanDto.setNumRevicevedMin(numConfigRemain);
				quanDto.setAmountRevicevedMin(amountConfigRemain);

				//Co phan bo so suat, so luong hoac so tien(gia tri cu the)
//				if(quantityConfigRemain != nullQuantityValue || numConfigRemain != nullNumValue || amountConfigRemain != nullAmountValue) {
				if(quantityConfigRemain != nullQuantityValue || numConfigRemain != nullNumValue || BigDecimal.valueOf(amountConfigRemain).compareTo(BigDecimal.valueOf(nullAmountValue)) != 0) {
					int promotionType = -1;
					int promotionCheck = -1;

					//Dat nhung Khong duoc huong KM do het so suat, so luong, so tien quy dinh
					if(quantityConfigRemain <= 0 || numConfigRemain <= 0 || amountConfigRemain <= 0) {
						quanDto.setQuantityReviceved(0);
						quanDto.setNumReceived(0);
						quanDto.setAmountReceived(0);

						//Khong duoc huong KM do tien khong du
						if(quantityConfigRemain <= 0) {
							promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY;
							promotionCheck = promotionQuantityCheck;
						}

						//Khong duoc huong KM do tien khong du
						else if(numConfigRemain <= 0) {
							promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_NUM;
							promotionCheck = promotionNumCheck;
						}

						//Khong duoc huong KM do tien khong du
						//if(amountConfigRemain <= 0)
						else {
							promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT;
							promotionCheck = promotionAmountCheck;
						}

						quanDto.setPromotionType(promotionType);
						quanDto.setPromotionCheck(promotionCheck);

						mapQuantityReceiveMissing.put(quanDto.getProId(), quanDto);

						ArrayList<ArrayList<SaleOrderPromotionDTO>> salePromotionGroupList = mapQuantityRevicevedList.get(quanDto.getProId());

						for(ArrayList<SaleOrderPromotionDTO> salePromotionDtoList: salePromotionGroupList) {
							//Giam so suat huong ve 0
							for(SaleOrderPromotionDTO salePromotionDto : salePromotionDtoList) {
								salePromotionDto.quantityReceived = 0;
								salePromotionDto.numReceived = 0;
								salePromotionDto.amountReceived = 0;

								salePromotionDto.exceedUnit = promotionType;
								salePromotionDto.exceedObject = promotionCheck;
							}
						}

						//Giam so luong, so tien cac KM
						for(OrderDetailViewDTO orderDetail : listPromotion) {
							if(orderDetail.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO
							|| orderDetail.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_ORDER
							|| orderDetail.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_NEW_OPEN) {
								//Duyet qua ds so suat cac muc cua 1 CTKM
								for(ArrayList<SaleOrderPromotionDTO> salePromotionDtoList: salePromotionGroupList) {
									for(SaleOrderPromotionDTO salePromotionDto: salePromotionDtoList) {
										if(orderDetail.orderDetailDTO.programeCode != null && orderDetail.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode) &&
												orderDetail.orderDetailDTO.productGroupId == salePromotionDto.productGroupId &&
												orderDetail.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {
											
											orderDetail.orderDetailDTO.quantity = 0;
											orderDetail.orderDetailDTO.discountPercentage = 0;
											orderDetail.orderDetailDTO.setDiscountAmount(0);

											if(!orderDetail.listPromotionForPromo21.isEmpty()) {
												for(OrderDetailViewDTO orderDetail2: orderDetail.listPromotionForPromo21) {
													orderDetail2.orderDetailDTO.quantity = 0;
//													orderDetail2.orderDetailDTO.discountPercentage = 0;
//													orderDetail.orderDetailDTO.setDiscountAmount(0);
												}
											}
											break;
										}
									}
								}
							}
						}

//						if(isLack) {
							SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut = new TreeMap<Long, List<OrderDetailViewDTO>>();
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT), listPromotionOrders);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER), listPromotionOrder);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN), listPromotionNewOpen);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY), listPromotionOrdersMoney);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN_MONEY), listPromotionNewOpenMoney);
							shareAmountPromotionForBuyProduct(sortListOutPut);

//							mapQuantityReceiveMissing.put(quanDto.proId, quanDto);
//						}
					} else {
						boolean isLack = false;

						while (true) {
							//Duoc huong het neu so suat con lai >= so suat su dung
							if (quantityConfigRemain < quanDto.getQuantityReviceved()
									|| numConfigRemain < quanDto.getNumReceived()
									|| amountConfigRemain < quanDto.getAmountReceived()) {
								isLack = true;

								int quantityDescrease = 0;

								if(quantityConfigRemain < quanDto.getQuantityReviceved()) {
									quantityDescrease = quanDto.getQuantityReviceved() - quantityConfigRemain;

									promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY;
									promotionCheck = promotionQuantityCheck;
								} else if(numConfigRemain < quanDto.getNumReceived()) {
									quantityDescrease = 1;

									promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_NUM;
									promotionCheck = promotionNumCheck;
								} else if(amountConfigRemain < quanDto.getAmountReceived()) {
									quantityDescrease = 1;

									promotionType = SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT;
									promotionCheck = promotionAmountCheck;
								}

								quanDto.setQuantityReviceved(quanDto.getQuantityReviceved() - quantityDescrease);
								quanDto.setPromotionType(promotionType);
								quanDto.setPromotionCheck(promotionCheck);

								//Tinh lai so luong, so tien
								//Giam so luong, so tien cua muc thap hon truoc
//								ArrayList<SaleOrderPromotionDTO> salePromotionDtoList = mapQuantityRevicevedList.get(quanDto.proId);
								ArrayList<ArrayList<SaleOrderPromotionDTO>> salePromotionGroupList = mapQuantityRevicevedList.get(quanDto.getProId());

								//Tim suat se giam
								while(quantityDescrease > 0) {
									for(ArrayList<SaleOrderPromotionDTO> salePromotionDtoList: salePromotionGroupList) {
										for(SaleOrderPromotionDTO salePromotionDto : salePromotionDtoList) {
											salePromotionDto.exceedUnit = promotionType;
											salePromotionDto.exceedObject = promotionCheck;

											int quantityDescreaseLevel = 0;
											//So suat cua 1 muc nho hon tong so muc phai giam
											if(salePromotionDto.quantityReceived < quantityDescrease) {
												quantityDescreaseLevel = salePromotionDto.quantityReceived;
											} else {
												quantityDescreaseLevel = quantityDescrease;
											}

											quantityDescrease -= quantityDescreaseLevel;
											salePromotionDto.quantityReceived -= quantityDescreaseLevel;
											salePromotionDto.numReceived = salePromotionDto.quantityReceived * salePromotionDto.numReceivedMax / salePromotionDto.quantityReceivedMax;
											salePromotionDto.amountReceived = salePromotionDto.quantityReceived * salePromotionDto.amountReceivedMax / salePromotionDto.quantityReceivedMax;

											//Cap nhat so tong, tien tong cua so suat
											quanDto.setNumReceived(quanDto
													.getNumReceived()
													- quantityDescreaseLevel * salePromotionDto.numReceivedMax / salePromotionDto.quantityReceivedMax);
											quanDto.setAmountReceived(quanDto
													.getAmountReceived()
													- quantityDescreaseLevel * salePromotionDto.amountReceivedMax / salePromotionDto.quantityReceivedMax);

											//Cap nhat cho cac KM sp, tien, %
											for(OrderDetailViewDTO orderDetail : listPromotion) {
												if(orderDetail.orderDetailDTO.programeCode != null && orderDetail.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode) &&
														orderDetail.orderDetailDTO.productGroupId == salePromotionDto.productGroupId &&
														orderDetail.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {

													if(orderDetail.listPromotionForPromo21.isEmpty()) {
														orderDetail.orderDetailDTO.quantity = salePromotionDto.quantityReceived * orderDetail.orderDetailDTO.maxQuantityFree / salePromotionDto.quantityReceivedMax;
													} else {
														for(OrderDetailViewDTO orderDetail2: orderDetail.listPromotionForPromo21) {
															orderDetail2.orderDetailDTO.quantity = salePromotionDto.quantityReceived * orderDetail2.orderDetailDTO.maxQuantityFree / salePromotionDto.quantityReceivedMax;
														}
													}

													orderDetail.orderDetailDTO.setDiscountAmount(salePromotionDto.quantityReceived * orderDetail.orderDetailDTO.maxAmountFree / salePromotionDto.quantityReceivedMax);
												}
											}

											//Het so suat giam thi dung tim kiem lai
											if(quantityDescrease <= 0) {
												break;
											}
										}
										if(quantityDescrease <= 0) {
											break;
										}
									}
								}
							} else {
								break;
							}
						}

						if(isLack) {
							SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut = new TreeMap<Long, List<OrderDetailViewDTO>>();
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT), listPromotionOrders);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER), listPromotionOrder);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN), listPromotionNewOpen);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY), listPromotionOrdersMoney);
							sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN_MONEY), listPromotionNewOpenMoney);
							shareAmountPromotionForBuyProduct(sortListOutPut);

							mapQuantityReceiveMissing.put(quanDto.getProId(), quanDto);
						}
					}

				}

				//Cap nhat promotion_detail
				ArrayList<ArrayList<SaleOrderPromotionDTO>> salePromotionGroupList = mapQuantityRevicevedList.get(quanDto.getProId());

				for(ArrayList<SaleOrderPromotionDTO> salePromotionDtoList: salePromotionGroupList) {
					//Giam so suat huong ve 0
					for(SaleOrderPromotionDTO salePromotionDto : salePromotionDtoList) {
						salePromotionDto.promotionDetailValue = promotionDetailValue;
						salePromotionDto.promotionDetail = salePromotionDto.generatePromotionDetail();
					}
				}
			} else {

			}
		}

//		for	(SaleOrderPromotionDTO quantityReceived : productQuantityReceivedList) {
//			//Chi check nhung KM san pham
//			//Co KM sp & KHONG co KM tien
//			if(quantityReceived.hasProduct == 1 && !quantityReceived.hasMoney) {
//				boolean hasQuantity = false;
//				for(OrderDetailViewDTO promotion: listPromotion) {
//					if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
//						if(quantityReceived.promotionProgramCode.equals(promotion.orderDetailDTO.programeCode)
//								&& quantityReceived.productGroupId == promotion.orderDetailDTO.productGroupId
//								&& quantityReceived.promotionLevel == promotion.orderDetailDTO.promotionOrderNumber
//								) {
//							if(promotion.orderDetailDTO.quantity > 0) {
//								hasQuantity = true;
//								break;
//							}
//						}
//					} else if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
//							|| promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
//						for(OrderDetailViewDTO promotionZV21: promotion.listPromotionForPromo21) {
//							if(quantityReceived.promotionProgramCode.equals(promotionZV21.orderDetailDTO.programeCode)
//									&& quantityReceived.productGroupId == promotionZV21.orderDetailDTO.productGroupId
//									&& quantityReceived.promotionLevel == promotionZV21.orderDetailDTO.promotionOrderNumber
//									) {
//								if(promotionZV21.orderDetailDTO.quantity > 0) {
//									hasQuantity = true;
//									break;
//								}
//							}
//						}
//
//						//Neu 1 sp co so luong thi coi nhu ca KM ZV1 co so luong
//						if(hasQuantity) {
//							break;
//						}
//					}
//
//				}
//
//				if(!hasQuantity) {
//					quantityReceived.quantityReceived = 0;
//				}
//			}
//		}


//		HashMap<Long, QuantityRevicevedDTO> mapQuantityReviceved = new HashMap<Long, QuantityRevicevedDTO>();
//
//		// qua tung chuong trinh, check so luong so suat
//		for (QuantityRevicevedDTO quanDto : mapQuantityReviceved.values()) {
//
//			PromotionShopMapDTO promotionShopMapDTO = psmTable
//					.getPromotionShopMapByPromotionProgramId(
//							quanDto.proId,
//							GlobalInfo.getInstance().getProfile().getUserData().inheritShopId);
//
//			int quantityShopRemain = -1;
//			// so luong nhan con lai
//			int numConfigRemain = -1;
//			// so tien nhan con lai
//			double amountConfigRemain = -1;
//			if (promotionShopMapDTO != null) {
//				quanDto.shopMapID = promotionShopMapDTO.promotionShopMapId;
//				// truong hop co map voi shop
//
//				// promotionShopMapDTO.quantityReceived +
//				// quanDto.quantityReviceved <
//				// promotionShopMapDTO.quantityMax
//				// lay promotion_customer_map
//				PromotionCustomerMapDTO promotionCustomerMapDTO = pcmTable
//						.getPromotionShopMapByPromotionShopMapID(
//								dto.orderInfo.customerId, dto.orderInfo.shopId,
//								promotionShopMapDTO.promotionShopMapId);
//				PromotionStaffMapDTO promotionStaffMapDTO = pcmTable
//						.getPromotionStaffMapByPromotionShopMapID(
//								dto.orderInfo.staffId, dto.orderInfo.shopId,
//								promotionShopMapDTO.promotionShopMapId);
//
//				// khong cau hinh so suat nhan vien + khach hang
//				if (promotionStaffMapDTO == null
//						&& promotionCustomerMapDTO == null) {
//					// thi su dung so suat cua NPP
//					quantityShopRemain = promotionShopMapDTO.quantityMax
//							- promotionShopMapDTO.quantityReceived;
//					// thi su dung so suat cua NPP
//					numConfigRemain = promotionShopMapDTO.numMax
//							- promotionShopMapDTO.numReceived;
//					// thi su dung so suat cua NPP
//					amountConfigRemain = promotionShopMapDTO.amountMax
//							- promotionShopMapDTO.amountReceived;
//				} else
//				// co khai bao staff map
//				if (promotionStaffMapDTO != null) {
//					quanDto.staffMapId = promotionStaffMapDTO.promotionStaffMapId;
//					// thi su dung so suat cua nv
//					quantityShopRemain = promotionStaffMapDTO.quantityMax
//							- promotionStaffMapDTO.quantityReceived;
//					// thi su dung so suat cua NPP
//					numConfigRemain = promotionStaffMapDTO.numMax
//							- promotionStaffMapDTO.numReceived;
//					// thi su dung so suat cua NPP
//					amountConfigRemain = promotionStaffMapDTO.amountMax
//							- promotionStaffMapDTO.amountReceived;
//
//					// co khai bao customer map
//					if (promotionCustomerMapDTO != null) {
//						quanDto.customerMapId = promotionCustomerMapDTO.promotionCustomerMapId;
//						// neu so suat con lai cua kh nho hon cua nv
//						// thi cap nhat lai so suat ton
//						int quantityTemp = promotionCustomerMapDTO.quantityMax
//								- promotionCustomerMapDTO.quantityReceived;
//						int numTemp = promotionCustomerMapDTO.numMax
//								- promotionCustomerMapDTO.numReceived;
//						double amountTemp = promotionCustomerMapDTO.amountMax
//								- promotionCustomerMapDTO.amountReceived;
//
//						if (quantityTemp < quantityShopRemain) {
//							quantityShopRemain = quantityTemp;
//						}
//						if (numTemp < numConfigRemain) {
//							numConfigRemain = numTemp;
//						}
//						if (amountTemp < amountConfigRemain) {
//							amountConfigRemain = amountTemp;
//						}
//					} else {
//						// khong khai bao customer map
//					}
//				} else {
//					quanDto.customerMapId = promotionCustomerMapDTO.promotionCustomerMapId;
//					// co khai bao so suat kh
//					// thi su dung so suat cua kh
//					quantityShopRemain = promotionCustomerMapDTO.quantityMax
//							- promotionCustomerMapDTO.quantityReceived;
//					numConfigRemain = promotionCustomerMapDTO.numMax
//							- promotionCustomerMapDTO.numReceived;
//					amountConfigRemain = promotionCustomerMapDTO.amountMax
//							- promotionCustomerMapDTO.amountReceived;
//				}
//
//				// neu min so suat con lai khong du thi them vao
//				// danh sach cac chuong trinh thieu so suat
//				if (quantityShopRemain < quanDto.quantityReviceved
//						|| numConfigRemain < quanDto.numReceived
//						|| amountConfigRemain < quanDto.amountReceived) {
//					listPromotionCode.add(quanDto.proCode);
//					isVaildIncreasePromoQuanReceived = false;
//				}
//			} else {
//				// neu khong co so suat npp thi them vao danh sach
//				// cac chuong trinh thieu so suat
//				listPromotionCode.add(quanDto.proCode);
//				isVaildIncreasePromoQuanReceived = false;
//			}
//		}
//
//		// neu cac promotion deu du so suat thi tien hanh tru
//		if (isVaildIncreasePromoQuanReceived) {
//			// tru so suat tung chuong trinh
//			for (QuantityRevicevedDTO quanDto : mapQuantityReviceved.values()) {
//				// tang so xuat shop
//				if (quanDto.shopMapID > 0) {
//					isVaildIncreasePromoQuanReceived = psmTable
//							.increaseQuantityRecevie(quanDto.shopMapID,
//									quanDto.quantityReviceved,
//									quanDto.numReceived, quanDto.amountReceived);
//				}
//
//				if (isVaildIncreasePromoQuanReceived && quanDto.staffMapId > 0) {
//					isVaildIncreasePromoQuanReceived = pcmTable
//							.increaseStaffQuantityRecevie(quanDto.staffMapId,
//									quanDto.quantityReviceved,
//									quanDto.numReceived, quanDto.amountReceived);
//				}
//
//				if (isVaildIncreasePromoQuanReceived
//						&& quanDto.customerMapId > 0) {
//					isVaildIncreasePromoQuanReceived = pcmTable
//							.increaseQuantityRecevie(quanDto.customerMapId,
//									quanDto.quantityReviceved,
//									quanDto.numReceived, quanDto.amountReceived);
//				}
//
//				// neu fail thi break
//				if (!isVaildIncreasePromoQuanReceived) {
//					break;
//				}
//			}
//		}
//
//		// cap nhat list so suat phai tru
//		dto.listIncreasePromoQuantityReceived = mapQuantityReviceved.values();

	}

	//Chia deu tien KM (KM tien, KM %) cho cac sp ban
	//Dua tren so suat thuc te nhan duoc
	public void shareAmountPromotionForBuyProduct(SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut) {
		//Tinh % discount, discount amount cho sp ban tu KM sp
		ArrayList<ArrayList<OrderDetailViewDTO>> listListPromotion = new ArrayList<ArrayList<OrderDetailViewDTO>>();
		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT)));
		//KM mo moi
		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN)));
		// km keyshop
//				listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP)));
//
		//KM sp tien
		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY)));
		//KM mo moi
		listListPromotion.add((ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN_MONEY)));

//				//Reset gia tri
		for (OrderDetailViewDTO product : listBuyOrders) {
			product.orderDetailDTO.discountPercentage = 0;
			product.orderDetailDTO.setDiscountAmount(0);
			//xoa het phan tu trong mang promo detail sau khi tinh khuyen mai lai
			if (product.listPromoDetail == null) {
				product.listPromoDetail = new ArrayList<SaleOrderPromoDetailDTO>();
			} else{
				product.listPromoDetail.clear();
			}
			//xoa het phan tu trong mang promo map sau khi tinh khuyen mai lai
			if (product.lstPromotionMap == null) {
				product.lstPromotionMap = new ArrayList<SalePromoMapDTO>();
			} else{
				product.lstPromotionMap.clear();
			}
		}

		for (ArrayList<OrderDetailViewDTO> listPromotionProduct: listListPromotion) {
//			ArrayList<OrderDetailViewDTO> listPercentPromotion = new ArrayList<OrderDetailViewDTO>();
			for (OrderDetailViewDTO promotion : listPromotionProduct) {
				//Xu ly cho cac KM % cua sp
				if (promotion.type == OrderDetailViewDTO.FREE_PERCENT ||
						//Moi xu ly cho KM mua Amount tang Amount: ZV05, ZV11, ZV17
						promotion.type == OrderDetailViewDTO.FREE_PRICE) {
					//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
					double totalDiscount = 0;
					double fixDiscountAmount = promotion.orderDetailDTO.getDiscountAmount();
					//KM sp
					int numProduct = 0;
					if (promotion.listBuyProduct != null && promotion.listBuyProduct.size() > 0) {
						for (int i = listBuyOrders.size() - 1; i >= 0; i--) {
							OrderDetailViewDTO product = listBuyOrders.get(i);
							for (GroupLevelDetailDTO levelDetail : promotion.listBuyProduct) {
								if (product.orderDetailDTO.productId == levelDetail.productId) {
									numProduct++;
									// Sp tham CTKM dat duoc KM
									product.orderDetailDTO.hasPromotion = true;
									// Lay lai gia tri sau khi tinh KM (phong
									// hop luc dau ko dat KM nhung sua don hang
									// duoc KM)
									product.orderDetailDTO.programeType = promotion.orderDetailDTO.programeType;
									product.orderDetailDTO.programeCode = promotion.orderDetailDTO.programeCode;
									product.orderDetailDTO.programeTypeCode = promotion.orderDetailDTO.programeTypeCode;
									double amount = 0;
									if (promotion.isBundle) {
										amount = levelDetail.usedAmount;
									} else {
										amount = product.orderDetailDTO.getAmount();
									}
									BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
									double discountAmount = discountPercent.multiply(new BigDecimal(amount)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();

									//Giam so tien huong dua vao so suat con lai
									for(SaleOrderPromotionDTO salePromotionDto : productQuantityReceivedList) {
										if(promotion.orderDetailDTO.programeCode != null && promotion.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode) &&
												promotion.orderDetailDTO.productGroupId == salePromotionDto.productGroupId &&
												promotion.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {
											discountAmount = salePromotionDto.quantityReceived * discountAmount / salePromotionDto.quantityReceivedMax;
											break;
										}
									}

									// Tinh toan de lam tron, tong so tien <=
									// tong tien KM
									if (discountAmount > fixDiscountAmount) {
										discountAmount = fixDiscountAmount;
									}

									if (totalDiscount + discountAmount > fixDiscountAmount) {
										discountAmount = fixDiscountAmount - totalDiscount;
									} else {
										if (numProduct == promotion.listBuyProduct.size()) {
											discountAmount = fixDiscountAmount - totalDiscount;
										}
									}

									totalDiscount += discountAmount;

									product.orderDetailDTO.addDiscountAmount(discountAmount);

//									listPercentPromotion.add(promotion);

									// promo detail sau khi tinh khuyen mai lai
									SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
									promoDetail.updateData(promotion,
											discountAmount);
									// neu so luong san pham quy dinh == 1 -> la
									// LINE, isOwner = 1, con lai la isOwner = 0
									promoDetail.isOwner = (promotion.numBuyProductInGroup == 1) ? 1
											: 0;
									// KM tu dong
									product.listPromoDetail.add(promoDetail);

									// luu thong tin CTKM dat
									boolean isExist = false;
									for (SalePromoMapDTO temp : product.lstPromotionMap) {
										if (temp.programCode
												.equals(promotion.orderDetailDTO.programeCode)) {
											isExist = true;
											break;
										}
									}
									if (!isExist) {
										// luu thong tin CTKM dat
										SalePromoMapDTO proMap = new SalePromoMapDTO();
										proMap.updateData(promotion);
										proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
										product.lstPromotionMap.add(proMap);
									}
									break;
								}
							}
						}
					} else if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY || promotion.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
						// neu la sp KM mo moi thuoc dang don hang
						// thi phan bo deu ra cho cac sp nhu KM don hang
						for (int i = listBuyOrders.size() - 1; i >= 0; i--) {
							OrderDetailViewDTO product = listBuyOrders.get(i);
							numProduct++;

							BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
							double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();

							//Giam so tien huong dua vao so suat con lai
							for(SaleOrderPromotionDTO salePromotionDto : productQuantityReceivedList) {
								if(promotion.orderDetailDTO.programeCode != null && promotion.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode) &&
										promotion.orderDetailDTO.productGroupId == salePromotionDto.productGroupId &&
										promotion.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {
									discountAmount = salePromotionDto.quantityReceived * discountAmount / salePromotionDto.quantityReceivedMax;
									break;
								}
							}

							// Tinh toan de lam tron, tong so tien <= tong tien KM
							if (discountAmount > fixDiscountAmount) {
								discountAmount = fixDiscountAmount;
							}

							if (totalDiscount + discountAmount > fixDiscountAmount) {
								discountAmount = fixDiscountAmount - totalDiscount;
							} else {
								if (numProduct == listBuyOrders.size()) {
									discountAmount = fixDiscountAmount - totalDiscount;
								}
							}
							totalDiscount += discountAmount;
							product.orderDetailDTO.addDiscountAmount(discountAmount);

							// promo detail sau khi tinh khuyen mai lai
							SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
							promoDetail.updateData(promotion, discountAmount);
							// km don hang isOwner = 2
							promoDetail.isOwner = 2;
							product.listPromoDetail.add(promoDetail);
						}
					}
					//KM sp
				} else {
					//Cap nhat nhung sp ban dat duoc KM de luu programCode
					for(GroupLevelDetailDTO levelDetail: promotion.listBuyProduct) {
						for (OrderDetailViewDTO product : listBuyOrders) {
							if(product.orderDetailDTO.productId == levelDetail.productId) {
								//Sp tham CTKM dat duoc KM
								product.orderDetailDTO.hasPromotion = true;
								//Lay lai gia tri sau khi tinh KM (phong hop luc dau ko dat KM nhung sua don hang duoc KM)
								product.orderDetailDTO.programeType = promotion.orderDetailDTO.programeType;
								product.orderDetailDTO.programeCode = promotion.orderDetailDTO.programeCode;
								product.orderDetailDTO.programeTypeCode = promotion.orderDetailDTO.programeTypeCode;
								boolean isExist = false;
								for (SalePromoMapDTO promoDetail : product.lstPromotionMap) {
									if (promoDetail.programCode.equals(promotion.orderDetailDTO.programeCode)) {
										isExist = true;
										break;
									}
								}
								if (!isExist) {
									// luu thong tin CTKM dat
									SalePromoMapDTO proMap = new SalePromoMapDTO();
									proMap.updateData(promotion);
									proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
									product.lstPromotionMap.add(proMap);
								}
							}
						}
					}
				}
			}

			//Xoa sp KM phan tram
//			listPromotionProduct.removeAll(listPercentPromotion);
		}

		// luu thong tin nhung CTKM ko dat cho sp
		for (OrderDetailViewDTO product : listBuyOrders) {
//								if(product.listPromoDetail.size() == 0){
				ArrayList<PromotionProgrameDTO> lstPromotionProgram = promotionArrayClone.get(product.orderDetailDTO.productId);
				if(lstPromotionProgram != null && lstPromotionProgram.size() > 0 ){
					for(PromotionProgrameDTO promotionProgram : lstPromotionProgram ){
						boolean isAchive = false;
						for(SalePromoMapDTO promoDetail: product.lstPromotionMap){
						// promo detail sau khi tinh khuyen mai lai
//											SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
//											promoDetail.updateData(promotionProgram);
//											// km don hang isOwner = 2
//											promoDetail.isOwner = CalPromotions.getTypePromtion(promotionProgram.TYPE);
//											product.listPromoDetail.add(promoDetail);
						if(promoDetail.programCode.equals(promotionProgram.getPROMOTION_PROGRAM_CODE())){
								isAchive = true;
								break;
							}
						}
						if(!isAchive){
							// luu thong tin CTKM ko dat
							SalePromoMapDTO proMap = new SalePromoMapDTO();
							proMap.saleOrderDetailId = product.orderDetailDTO.salesOrderDetailId;
							proMap.programCode = promotionProgram.getPROMOTION_PROGRAM_CODE();
							proMap.status = SalePromoMapDTO.TYPE_NOT_ACHIVE;
							product.lstPromotionMap.add(proMap);
						}
					}

				}
		}

		//Cap nhat % discount, discount cua KM don hang
		ArrayList<OrderDetailViewDTO> listPromotionOrder = (ArrayList<OrderDetailViewDTO>) sortListOutPut.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER));
		for (OrderDetailViewDTO promotion : listPromotionOrder) {
			if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER) {
				//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
				double totalDiscount = 0;
				double fixDiscountAmount = promotion.orderDetailDTO.getDiscountAmount();
				//KM sp
				int numProduct = 0;
				for (int i = listBuyOrders.size() - 1; i >= 0; i--) {
					OrderDetailViewDTO product = listBuyOrders.get(i);
					numProduct++;

					BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", promotion.orderDetailDTO.discountPercentage));
					double discountAmount = discountPercent.multiply(new BigDecimal(product.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();

					//Giam so tien huong dua vao so suat con lai
					for(SaleOrderPromotionDTO salePromotionDto : productQuantityReceivedList) {
						if(promotion.orderDetailDTO.programeCode != null && promotion.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode) &&
								promotion.orderDetailDTO.productGroupId == salePromotionDto.productGroupId &&
								promotion.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {
							discountAmount = salePromotionDto.quantityReceived * discountAmount / salePromotionDto.quantityReceivedMax;
							break;
						}
					}

					//Tinh toan de lam tron, tong so tien <= tong tien KM
					if(discountAmount > fixDiscountAmount) {
						discountAmount = fixDiscountAmount;
					}

					if(totalDiscount + discountAmount > fixDiscountAmount) {
						discountAmount = fixDiscountAmount - totalDiscount;
					} else {
						if(numProduct == listBuyOrders.size()) {
							discountAmount = fixDiscountAmount - totalDiscount;
						}
					}
					totalDiscount += discountAmount;
					product.orderDetailDTO.addDiscountAmount(discountAmount);

					//promo detail sau khi tinh khuyen mai lai
					SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
					promoDetail.updateData(promotion, discountAmount);
					//km don hang isOwner = 2
					promoDetail.isOwner = 2;
					product.listPromoDetail.add(promoDetail);

					// luu thong tin CTKM dat
					// SalePromoMapDTO proMap = new SalePromoMapDTO();
					// proMap.updateData(promotion);
					// proMap.status = SalePromoMapDTO.TYPE_ACHIVE;
					// product.lstPromotionMap.add(proMap);
				}
			}
		}
	}

//	public static final int TYPE_PROMOTION_QUANTITY = 1;
//	public static final int TYPE_PROMOTION_NUM = 2;
//	public static final int TYPE_PROMOTION_AMOUNT = 3;
//
//	public static final int TYPE_CHECK_NULL = -1;
//	public static final int TYPE_CHECK_SHOP = 1;
//	public static final int TYPE_CHECK_STAFF = 2;
//	public static final int TYPE_CHECK_CUSTOMER = 3;
	/*
	 * Generate chuoi thong bao het so suat
	 */
	public String generateQuantityReceiveMissingMessage() {
		StringBuilder result = new StringBuilder();

		for(QuantityRevicevedDTO quanDTO : mapQuantityReceiveMissing.values()) {
			String promotionObject = "";
			switch (quanDTO.getPromotionCheck()) {
				case SaleOrderPromotionDTO.TYPE_CHECK_SHOP: {
					promotionObject = StringUtil.getString(R.string.TEXT_PROMOTION_OBJECT_SHOP);
					break;
				}
				case SaleOrderPromotionDTO.TYPE_CHECK_STAFF: {
					promotionObject = StringUtil.getString(R.string.TEXT_PROMOTION_OBJECT_STAFF);
					break;
				}
				case SaleOrderPromotionDTO.TYPE_CHECK_CUSTOMER: {
					promotionObject = StringUtil.getString(R.string.TEXT_PROMOTION_OBJECT_CUSTOMER);
					break;
				}
				default:
					promotionObject = StringUtil.getString(R.string.TEXT_PROMOTION_OBJECT_SHOP);
					break;
			}
			//Huong so suat 1 phan
			if(quanDTO.getQuantityReviceved() > 0) {
				String promotionType = "";
				String value = "";
				switch (quanDTO.getPromotionType()) {
				case SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY: {
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_UNIT_QUANTITY);
					value = String.valueOf(quanDTO.getQuantityRevicevedMin());
					break;
				}
				case SaleOrderPromotionDTO.TYPE_PROMOTION_NUM: {
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_UNIT_NUM);
					value = String.valueOf(quanDTO.getNumRevicevedMin());
					break;
				}
				case SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT: {
					promotionType = GlobalInfo.getInstance().getSysCurrency();
					value = String.valueOf(quanDTO.getAmountRevicevedMin());
					break;
				}
				default:
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_TYPE_QUANTITY);
					break;
				}
				String message = StringUtil.getString(R.string.TEXT_PROMOTION_GET_ONE_PART_QUANTITY_RECEIVE);
				message = String.format(message, quanDTO.getQuantityReviceved(), quanDTO.getQuantityRevicevedMax(), quanDTO.getProCode(), promotionObject, value, promotionType);

				result.append(message);
			//Het so suat
			} else {
				String promotionType = "";
				switch (quanDTO.getPromotionType()) {
				case SaleOrderPromotionDTO.TYPE_PROMOTION_QUANTITY: {
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_TYPE_QUANTITY);
					break;
				}
				case SaleOrderPromotionDTO.TYPE_PROMOTION_NUM: {
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_TYPE_NUM);
					break;
				}
				case SaleOrderPromotionDTO.TYPE_PROMOTION_AMOUNT: {
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_TYPE_AMOUNT);
					break;
				}
				default:
					promotionType = StringUtil.getString(R.string.TEXT_PROMOTION_TYPE_QUANTITY);
					break;
				}

				String message = StringUtil.getString(R.string.TEXT_PROMOTION_OUT_OF_QUANTITY_RECEIVE);
				message = String.format(message, quanDTO.getProCode(), promotionObject, promotionType);

				result.append(message);
			}
		}

		return result.toString();
	}
}
