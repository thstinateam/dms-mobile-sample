/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.STAFF_POSITION_LOG_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Bang ghi vi tri cua NVBH
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class StaffPositionLogDTO extends AbstractTableDTO{
	// id
	public long id;
	// staff id
	public int staffId;
	// lat
	public double lat;
	// lng
	public double lng;
	//createDate
	public String createDate;
	// do chinh xac khi lay vi tri
	public float accuracy = -1;
	public String shopId;
	public double distance = 0;
	// battery
	public double battery;
	// Wifi, GPRS, 3Gv.v.
	public String networkType;
	// Toc do: 0 - yeu, 1 - manh, 2 - rat manh
	public int networkSpeed = 0;
	
	public StaffPositionLogDTO() {
		super(TableType.STAFF_POSITION_LOG);
	}

	/**
	*  Generate cau lenh insert
	*  @author: TruongHN
	*  @param seqOrderName
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateCreateSql(){
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, STAFF_POSITION_LOG_TABLE.STAFF_POSITION_LOG_TABLE);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.STAFF_POSITION_ID,"STAFF_POSITION_LOG_SEQ", DATA_TYPE.SEQUENCE.toString()));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.LAT, this.lat , null));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.LNG,this.lng, null));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.DISTANCE,this.distance, null));
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.PIN,this.battery, null));
			
			if (!StringUtil.isNullOrEmpty(networkType)) {
				params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.NETWORK_TYPE, this.networkType, null));
			}
			params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.SPEED, this.networkSpeed, null));
			
			if (!GlobalUtil.isSettingAutoTimeUpdate()) {
				// //thoi gian server
				params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.CREATE_DATE, this.createDate,
						DATA_TYPE.SYSDATE.toString()));
			} else {
				// //thoi gian client
				params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.CREATE_DATE, this.createDate, null));
			}
			if (this.accuracy >= 0){
				//params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.CREATE_DATE,this.createDate, null));
				params.put(GlobalUtil.getJsonColumn(STAFF_POSITION_LOG_TABLE.ACCURACY,this.accuracy, null));
			}

			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);

		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	public StaffPositionLogDTO initFromCursor(Cursor c) {
		StaffPositionLogDTO temp = new StaffPositionLogDTO();
		temp.id = CursorUtil.getInt(c, "STAFF_POSITION_LOG_ID");
		temp.staffId = CursorUtil.getInt(c, "STAFF_ID");
		temp.lat = CursorUtil.getDouble(c, "LAT");
		temp.lng = CursorUtil.getDouble(c, "LNG");
		temp.createDate = CursorUtil.getString(c, "CREATE_DATE");
		return temp;
	}
}
