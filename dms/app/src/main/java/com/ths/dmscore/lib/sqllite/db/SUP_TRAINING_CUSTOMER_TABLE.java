/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.view.trainingplan.TrainingRateResultDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.DateUtils;

/**
 * 
 * SUP_TRANING_CUSTOMER_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:39:31 17-11-2014
 */
public class SUP_TRAINING_CUSTOMER_TABLE extends ABSTRACT_TABLE {
	public static final String SUP_TRAINING_CUSTOMER_ID = "SUP_TRAINING_CUSTOMER_ID";
	public static final String SUP_TRAINING_PLAN_ID = "SUP_TRAINING_PLAN_ID";
	public static final String TRAINING_RATE_ID = "TRAINING_RATE_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String NOTE = "NOTE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_STAFF_ID = "CREATE_STAFF_ID";

	public static final String TABLE_NAME = "SUP_TRAINING_CUSTOMER";

	public SUP_TRAINING_CUSTOMER_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SUP_TRAINING_CUSTOMER_ID,
				SUP_TRAINING_PLAN_ID, TRAINING_RATE_ID, CUSTOMER_ID, NOTE,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE,CREATE_STAFF_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * luu thong tin danh gia cua tieu chi
	 * 
	 * @author: dungdq3
	 * @since: 5:09:55 PM Nov 17, 2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 * @throws Exception 
	 */
	public long saveTrainingCustomer(TrainingRateResultDTO dto) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		try {
			TABLE_ID tableId = new TABLE_ID(mDB);
			if(dto.getSupTrainingCustomerID() == 0){
				long supTrainCusId = tableId.getMaxIdTime(TABLE_NAME);
				dto.setSupTrainingCustomerID(supTrainCusId);
				returnCode = insert(dto);
			} else if(dto.getSupTrainingCustomerID() > 0) {
				dto.setNew(false);
				returnCode = update(dto);
			}
		} catch (Exception e) {
			throw e;
		}
		return returnCode;
	}
	
	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 10:04:50 AM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private long update(TrainingRateResultDTO dto) {
		// TODO Auto-generated method stub
		ContentValues value = initContentValuesUpdate(dto);
		String[] params = { String.valueOf(dto.getSupTrainingCustomerID()) };
		return update(value, SUP_TRAINING_CUSTOMER_ID + " = ?", params);
	}

	/**
	 * init content for update
	 * 
	 * @author: dungdq3
	 * @since: 10:06:34 AM Nov 18, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private ContentValues initContentValuesUpdate(TrainingRateResultDTO dto) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(NOTE, dto.getNote());
		editedValues.put(UPDATE_DATE, DateUtils.now());
		return editedValues;
	}

	/**
	 * insert db
	 * 
	 * @author: dungdq3
	 * @since: 5:26:56 PM Nov 17, 2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private long insert(TrainingRateResultDTO dto){
		ContentValues cv= generateContentValueInsert(dto);
		return insert(null, cv);
	}

	/**
	 * tao content values de insert
	 * 
	 * @author: dungdq3
	 * @since: 5:26:53 PM Nov 17, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private ContentValues generateContentValueInsert(TrainingRateResultDTO dto) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(SUP_TRAINING_CUSTOMER_ID, dto.getSupTrainingCustomerID());
		editedValues.put(SUP_TRAINING_PLAN_ID, dto.getSupTrainingPlanID());
		editedValues.put(TRAINING_RATE_ID, dto.getTrainingRateID());
		editedValues.put(CREATE_STAFF_ID, dto.getSubStaffID());
		editedValues.put(CUSTOMER_ID, dto.getCustomerID());
		editedValues.put(NOTE, dto.getNote());
		editedValues.put(CREATE_DATE, DateUtils.now());
		return editedValues;
	}
}
