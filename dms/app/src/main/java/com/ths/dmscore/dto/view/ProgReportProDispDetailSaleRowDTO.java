package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class ProgReportProDispDetailSaleRowDTO {
		public String customerCode;
		public String customerName;
		public String CustomerAddress;
		public long amountPlan;
		public String level;
		public long RemainSale;
		public int result;
		
		public ProgReportProDispDetailSaleRowDTO(){
			customerCode= "";
			customerName= "";
			CustomerAddress= "";
			RemainSale= 0;
			result = 0;
		}
		public ProgReportProDispDetailSaleRowDTO(Cursor c){
			customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
			CustomerAddress = CursorUtil.getString(c, "CUSTOMER_ADD");
			RemainSale = CursorUtil.getLong(c, "REMAIN_SALE");
			amountPlan = CursorUtil.getLong(c, "AMOUNT_PLAN");
			level = CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL");
			result = CursorUtil.getInt(c, "RESULT");
		}
}
