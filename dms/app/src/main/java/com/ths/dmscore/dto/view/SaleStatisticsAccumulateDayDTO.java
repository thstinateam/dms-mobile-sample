package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;

/**
 *
 *  dto dung cho man hinh don tong luy ke ngay cua NVBH
 *  @author: Nguyen Huu Hieu
 *  @version: 1.1
 *  @since: 1.0
 */
public class SaleStatisticsAccumulateDayDTO {
	public int totalList = -1;
	public String totalofTotal;

	public ArrayList<SaleStatisticsAccumulateDayItem> arrList;
	public ArrayList<String> listIndustry = new ArrayList<String>();
	public int totalRow = 0;
	// chu ky
	public int numCycle = 0;
	// ngay bat dau chu ky
	public String beginDate = "";
	// ngay ket thuc chu ky
	public String endDate = "";

	public SaleStatisticsAccumulateDayDTO(){
		arrList = new ArrayList<SaleStatisticsAccumulateDayDTO.SaleStatisticsAccumulateDayItem>();
	}

	public void addItem(Cursor c){
		SaleStatisticsAccumulateDayItem item = new SaleStatisticsAccumulateDayItem();
		item.maHang = CursorUtil.getString(c, "maHang");
		item.tenMatHang = CursorUtil.getString(c, "tenMatHang");
		item.nh = CursorUtil.getString(c, "nh");

		item.keHoach = CursorUtil.getDoubleUsingSysConfig(c, "keHoach");
		item.thucHien = CursorUtil.getDoubleUsingSysConfig(c, "thucHien");
		item.duyet = CursorUtil.getDoubleUsingSysConfig(c, "duyet");
		item.conLai = StringUtil.calRemainUsingRound(item.keHoach, item.thucHien);
		item.tienDo = StringUtil.calPercent(item.keHoach, item.thucHien);

		item.keHoachSanLuong = CursorUtil.getLong(c, "keHoachSanLuong");
		item.thucHienSanLuong = CursorUtil.getLong(c, "thucHienSanLuong");
		item.duyetSanLuong = CursorUtil.getLong(c, "duyetSanLuong");
		item.conLaiSanLuong = StringUtil.calRemain(item.keHoachSanLuong, item.thucHienSanLuong);
		item.tienDoSanLuong = StringUtil.calPercent(item.keHoachSanLuong, item.thucHienSanLuong);

		arrList.add(item);
	}

	public static class SaleStatisticsAccumulateDayItem{
		public double tienDoSanLuong;
		public long conLaiSanLuong;
		public long duyetSanLuong;
		public long thucHienSanLuong;
		public long keHoachSanLuong;
		public String stt;
		public String maHang;
		public String tenMatHang;
		public String nh;
		public double keHoach;
		public double thucHien;
		public double duyet;
		public double conLai;
		public double tienDo;

		public SaleStatisticsAccumulateDayItem(){
			stt = "";
			maHang = "";
			tenMatHang = "";
			nh = "";
			keHoach = 0;
			thucHien = 0;
			duyet = 0;
			conLai = 0;
			tienDo = 0;

			keHoachSanLuong = 0;
			thucHienSanLuong = 0;
			duyetSanLuong = 0;
			conLaiSanLuong = 0;
			tienDoSanLuong = 0;
		}

		/**
		 * sum item
		 * @author: duongdt3
		 * @since: 10:51:02 3 Apr 2015
		 * @return: void
		 * @throws:
		 * @param data
		 */
		public void sum(SaleStatisticsAccumulateDayItem data) {
			keHoach += data.keHoach;
			thucHien += data.thucHien;
			duyet += data.duyet;
			conLai += data.conLai;
			tienDo = StringUtil.calPercentUsingRound(keHoach, thucHien);

			keHoachSanLuong += data.keHoachSanLuong;
			thucHienSanLuong += data.thucHienSanLuong;
			duyetSanLuong += data.duyetSanLuong;
			conLaiSanLuong += data.conLaiSanLuong;
			tienDoSanLuong = StringUtil.calPercentUsingRound(keHoachSanLuong, thucHienSanLuong);
		}
	}
}
