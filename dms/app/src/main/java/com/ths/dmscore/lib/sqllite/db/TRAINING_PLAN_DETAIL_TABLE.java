package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.GSNPPTrainingPlanDTO;
import com.ths.dmscore.dto.view.GsnppTrainingResultAccReportDTO;
import com.ths.dmscore.dto.view.ListStaffDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanHistoryAccDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class TRAINING_PLAN_DETAIL_TABLE extends ABSTRACT_TABLE {

	// id bang
	public static final String ID = "ID";
	// ma training plan detail id
	public static final String TRAINING_PLAN_ID = "TRAINING_PLAN_ID";
	// staff id
	public static final String STAFF_ID = "STAFF_ID";
	// training dayInOrder
	public static final String TRAINING_DATE = "TRAINING_DATE";
	// Trang thai cua lich huan luyen: 0: Tao moi; 1: Da huan luyen; 2: Huy bo;
	public static final String STATUS = "STATUS";
	// note info
	public static final String NOTE = "NOTE";
	// score
	public static final String SCORE = "SCORE";
	// create dayInOrder
	public static final String CREATE_DATE = "CREATE_DATE";
	// update dayInOrder
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// amount plan
	public static final String AMOUNT_PLAN = "AMOUNT_PLAN";
	// amount
	public static final String AMOUNT = "AMOUNT";
	// num customer plan
	public static final String NUM_CUSTOMER_PLAN = "NUM_CUSTOMER_PLAN";
	// num customer order
	public static final String NUM_CUSTOMER_ORDER = "NUM_CUSTOMER_ORDER";
	// num customer ir
	public static final String NUM_CUSTOMER_IR = "NUM_CUSTOMER_IR";
	// num customer or
	public static final String NUM_CUSTOMER_OR = "NUM_CUSTOMER_OR";
	// num customer new
	public static final String NUM_CUSTOMER_NEW = "NUM_CUSTOMER_NEW";
	// num customer on
	public static final String NUM_CUSTOMER_ON = "NUM_CUSTOMER_ON";
	// shop id
	public static final String SHOP_ID = "SHOP_ID";

	private static final String TRAINING_PLAN_DETAIL_TABLE = "TRAINING_PLAN_DETAIL_TABLE";

	public TRAINING_PLAN_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TRAINING_PLAN_DETAIL_TABLE;
		// this.columns = new String[] { STAFF_ID, STAFF_NAME, STAFF_CODE,
		// PARENT_STAFF_ID, SHOP_ID, MONTH_AMOUNT_PLAN, MONTH_AMOUNT,
		// MONTH_SCORE, DAY_AMOUNT_PLAN, DAY_AMOUNT, DAY_SCORE,
		// MONTH_SKU_PLAN, MONTH_SKU, FOCUS1_AMOUNT_PLAN, FOCUS1_AMOUNT,
		// FOCUS2_AMOUNT_PLAN, FOCUS2_AMOUNT, CREATE_DATE, UPDATE_DATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 *
	 * get training plan detail id
	 *
	 * @author: HaiTC3
	 * @param shop_id
	 * @param supperStaff_id
	 * @param staff_id
	 * @param customer_id
	 * @return
	 * @return: String
	 * @throws:
	 */
	public String getTrainingPlanDetailId(String shop_id, String supperStaff_id, String staff_id) {
		String tpDetailId = "";
		String request = "SELECT TPD.ID AS TRAINING_PLAN_DETAIL_ID FROM TRAINING_PLAN_DETAIL AS TPD, TRAINING_PLAN AS TP\r\n"
				+ "WHERE TP.SHOP_ID = ?\r\n"
				+ "AND TP.STAFF_ID = ?\r\n"
				+ "AND strftime('%Y/%m', TP.MONTH) = strftime('%Y/%m', 'now','localtime')\r\n"
				+ "AND TP.STATUS = 1\r\n"
				+ "AND TP.ID = TPD.TRAINING_PLAN_ID\r\n"
				+ "AND TPD.STAFF_ID = ?\r\n"
				+ "AND DATE(TPD.TRAINING_DATE) = (SELECT DATE('NOW','LOCALTIME'))\r\n" + "AND TPD.SHOP_ID = TP.SHOP_ID";
		String[] params = { shop_id, supperStaff_id, staff_id };
		Cursor c = null;
		try {
			c = rawQuery(request, params);

			if (c != null) {
				if (c.moveToFirst()) {
					tpDetailId = CursorUtil.getString(c, "TRAINING_PLAN_DETAIL_ID", "0");
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return tpDetailId;
	}

	/**
	 * getAccTrainResultReportDTO
	 *
	 * @author: TamPQ
	 * @return: AccTrainResultReportDTO
	 * @throws:
	 */
	public GsnppTrainingResultAccReportDTO getAccTrainResultReportDTO(int staffId, String shopId) {
		String listStaffId = PriUtils.getInstance().getListStaffOfSupervisorIncludeHistory(String.valueOf(staffId), shopId);
		GsnppTrainingResultAccReportDTO dto = new GsnppTrainingResultAccReportDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT STAFF.STAFF_ID as STAFF_ID, ");
		var1.append("       STAFF.STAFF_CODE as STAFF_CODE , ");
		var1.append("       STAFF.STAFF_NAME as STAFF_NAME, ");
		var1.append("       STAFF.SALE_TYPE_CODE AS SALE_TYPE_CODE, ");
		var1.append("       TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID, ");
		var1.append("       TRAINING_PLAN_DETAIL.TRAINING_DATE, ");
		var1.append("       TRAINING_PLAN_DETAIL.AMOUNT_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL.AMOUNT, ");
		var1.append("       TRAINING_PLAN_DETAIL.NUM_CUSTOMER_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL.NUM_CUSTOMER_ORDER, ");
		var1.append("       TRAINING_PLAN_DETAIL.NUM_CUSTOMER_NEW, ");
		var1.append("       TRAINING_PLAN_DETAIL.NUM_CUSTOMER_ON, ");
		var1.append("       TRAINING_PLAN_DETAIL.NUM_CUSTOMER_OR, ");
		var1.append("       TRAINING_PLAN_DETAIL.SCORE, ");
		var1.append("       TRAINING_PLAN_DETAIL.SHOP_ID ");
		var1.append("FROM   TRAINING_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL, ");
//		var1.append("       STAFF ");
		var1.append("       (SELECT * FROM (SELECT s.staff_id, ");
		var1.append("                         s.staff_code, ");
		var1.append("                         s.staff_name, ");
		var1.append("                         s.mobilephone, ");
		var1.append("                         s.shop_id, ");
		var1.append("                         s.status, ");
		var1.append("                         s.SALE_TYPE_CODE ");
		var1.append("                  FROM   staff s WHERE S.STATUS = 1");
		var1.append("                  UNION ");
		var1.append("                  SELECT sh.staff_id AS staff_id, ");
		var1.append("                         sh.staff_code, ");
		var1.append("                         sh.staff_name, ");
		var1.append("                         sh.mobilephone, ");
		var1.append("                         sh.shop_id, ");
		var1.append("                         sh.status, ");
		var1.append("                         sh.SALE_TYPE_CODE ");
		var1.append("                  FROM   staff_history sh WHERE SH.STATUS = 1) GROUP BY STAFF_ID) STAFF ");
		var1.append("WHERE  TRAINING_PLAN.STAFF_ID = ? ");
		var1.append("       AND TRAINING_PLAN.TRAINING_PLAN_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STATUS IN ( 0,1 ) ");
		var1.append("       AND STAFF.STATUS = 1 ");
		var1.append("       AND STAFF.staff_id IN( ");
		var1.append(listStaffId);
		var1.append("       ) ");
		var1.append("       AND TRAINING_PLAN_DETAIL.SHOP_ID = ? ");
		var1.append("       AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("       AND DATE(TRAINING_PLAN.MONTH, 'start of month') = DATE('" + date_now
				+ "', 'localtime','start of month') ");
		var1.append("       AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE) >= DATE('" + date_now
				+ "', 'localtime','start of month') ");
		var1.append("       AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE) <= DATE(?) ");
		var1.append("ORDER  BY TRAINING_PLAN_DETAIL.TRAINING_DATE DESC ");

		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(), new String[] { String.valueOf(staffId), shopId, date_now });
			if (c != null && c.moveToFirst()) {
				do {
					GsnppTrainingResultAccReportDTO.GsnppTrainingResultAccReportItem i = dto.newAccTrainResultReportItem();
					i.initData(c);

					dto.listResult.add(i);
					dto.amountMonth += i.amountMonth;
					dto.amount += i.amount;
					dto.numCustomerPlan += i.numCustomerPlan;
					dto.numCustomerOrder += i.numCustomerOrder;
					dto.numCustomerNew += i.numCustomerNew;
					dto.numCustomerOn += i.numCustomerIr;
					dto.numCustomerOr += i.numCustomerOr;
					dto.score += i.score;
				} while (c.moveToNext());
			}

		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return dto;
	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: TamPQ
	 * @return: PlanTrainResultReportDTO
	 * @throws Exception
	 * @throws:
	 */
	public GSNPPTrainingPlanDTO getGsnppTrainingPlan(int staffId, String shopId) throws Exception {
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffId = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		GSNPPTrainingPlanDTO dto = new GSNPPTrainingPlanDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT STAFF.STAFF_ID, ");
		var1.append("       STAFF.STAFF_CODE, ");
		var1.append("       STAFF.STAFF_NAME, ");
		var1.append("       STAFF.SHOP_ID, ");
		var1.append("       STAFF.SALE_TYPE_CODE, ");
		var1.append("       SHOP.SHOP_NAME, ");
		var1.append("       SHOP.SHOP_CODE, ");
		var1.append("       TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID, ");
		var1.append("       TRAINING_PLAN_DETAIL.TRAINING_DATE, ");
		var1.append("       TRAINING_PLAN_DETAIL.SCORE ");
		var1.append("FROM   TRAINING_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL, ");
		var1.append("       STAFF, ");
		var1.append("       SHOP ");
		var1.append("WHERE  TRAINING_PLAN.STAFF_ID = ? ");
		param.add("" + staffId);
		if (shopId != null) {
			var1.append("       AND TRAINING_PLAN_DETAIL.SHOP_ID = ? ");
			param.add(shopId);
		}
		var1.append("       AND TRAINING_PLAN.TRAINING_PLAN_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STAFF_ID = STAFF.STAFF_ID ");
//		var1.append("       AND TRAINING_PLAN_DETAIL.STATUS = 1 ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STATUS IN ( 0, 1 ) ");
		var1.append("       AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("       AND STAFF.STATUS = 1 ");
		var1.append("       AND STAFF.staff_id IN( ");
		var1.append(listStaffId);
		var1.append("       ) ");
		var1.append("       AND SHOP.SHOP_ID = STAFF.SHOP_ID ");
		var1.append("       AND DATE(TRAINING_PLAN.MONTH, 'start of month') = DATE('" + date_now
				+ "','start of month') ");
		var1.append("       AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE, 'start of month') = DATE('" + date_now
				+ "','start of month') ");

		Cursor c = null;
		try {
			c = this.rawQueries(var1.toString(), param);
			if (c != null && c.moveToFirst()) {
				do {
					GSNPPTrainingPlanDTO.GSNPPTrainingPlanIem i = dto.newPlanTrainResultReportItem();
					i.initFromCursor(c);
					dto.listResult.put(i.dateString, i);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
		} finally {
			if (c != null) {
				c.close();
			}
		}

		return dto;
	}

	/**
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @return: TBHVGsnppTrainingPlanDTO
	 * @throws Exception
	 * @throws NumberFormatException
	 * @throws:
	 */
	public TBHVTrainingPlanDTO getTbhvTrainingPlan(int staffId, int shopIdVung) throws NumberFormatException, Exception {
		TBHVTrainingPlanDTO dto = new TBHVTrainingPlanDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(String.valueOf(shopIdVung));
		String shopStr = TextUtils.join(",", shopList);

		// lay ds gsnpp thuoc quyen quan ly
		ArrayList<String> param2 = new ArrayList<String>();
		StringBuffer var2 = new StringBuffer();
		var2.append("	SELECT	");
		var2.append("	    *	");
		var2.append("	FROM	");
		var2.append("	    (       SELECT	");
		var2.append("	        GS.STAFF_ID          AS STAFF_ID,	");
		var2.append("	        GS.STAFF_CODE        AS STAFF_CODE,	");
		var2.append("	        GS.STAFF_NAME        AS STAFF_NAME,	");
		var2.append("	        SH.SHOP_ID    AS NVBH_SHOP_ID,	");
		var2.append("	        SH.SHOP_CODE   AS NVBH_SHOP_CODE,	");
		var2.append("	        COUNT(S.STAFF_ID) AS NUM_NVBH	");
		var2.append("	    FROM	");
		var2.append("	        (SELECT	");
		var2.append("	            *	");
		var2.append("	        FROM	");
		var2.append("	            STAFF GS	");
		var2.append("	        WHERE	");
		var2.append("	            1=1	");
		var2.append("	            AND GS.STATUS = 1	");
		var2.append("	            AND GS.SHOP_ID = ?	");
		param2.add(String.valueOf(shopIdVung));
		var2.append("	            AND GS.STAFF_ID in (	");
		var2.append(PriUtils.getInstance().getListSupervisorOfASM(String.valueOf(staffId)));
		var2.append("	            ) ) GS,	");
		var2.append("	        PARENT_STAFF_MAP PS,	");
		var2.append("	        STAFF S,	");
		var2.append("	        SHOP SH,	");
		var2.append("	        CHANNEL_TYPE CT	");
		var2.append("	    WHERE	");
		var2.append("	        PS.PARENT_STAFF_ID =  GS.STAFF_ID	");
		var2.append("	        AND S.STAFF_ID = PS.STAFF_ID	");
		var2.append("	        AND SH.SHOP_ID = S.SHOP_ID	");
		var2.append("	        AND S.SHOP_ID IN (" + shopStr + ")	");
		var2.append("	        AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID	");
		var2.append("	        AND CT.TYPE = 2	");
		var2.append("	        AND CT.OBJECT_TYPE IN (	");
		var2.append("	            1, 2	");
		var2.append("	        )	");
		var2.append("	        AND PS.STATUS = 1	");
		var2.append("	        AND CT.STATUS = 1	");
		var2.append("	        AND S.STATUS = 1	");
		var2.append("	        AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)	");
		var2.append("	    GROUP BY	");
		var2.append("	        GS.STAFF_ID,	");
		var2.append("	        SH.SHOP_ID	");
		var2.append("	    ORDER  BY	");
		var2.append("	        SH.SHOP_CODE,	");
		var2.append("	        GS.STAFF_NAME       ) GSNPP	");
		var2.append("	    LEFT JOIN	");
		var2.append("	        (	");
		var2.append("	            SELECT	");
		var2.append("	                STAFF.STAFF_ID                     AS NVBH_STAFF_ID,	");
		var2.append("	                TRAINING_PLAN_DETAIL.TRAINING_DATE AS NVBH_TRAINING_DATE,	");
		var2.append("	                STAFF.SHOP_ID	");
		var2.append("	            FROM	");
		var2.append("	                TRAINING_PLAN,	");
		var2.append("	                TRAINING_PLAN_DETAIL,	");
		var2.append("	                STAFF,	");
		var2.append("	                CHANNEL_TYPE CH	");
		var2.append("	            WHERE	");
		var2.append("	                1 = 1	");
		var2.append("	                AND TRAINING_PLAN.TRAINING_PLAN_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID	");
		var2.append("	                AND TRAINING_PLAN_DETAIL.STAFF_ID = STAFF.STAFF_ID	");
		var2.append("	                AND TRAINING_PLAN_DETAIL.STATUS IN (	");
		var2.append("	                    0, 1	");
		var2.append("	                )	");
		var2.append("	                AND TRAINING_PLAN.STATUS = 1	");
		var2.append("	                AND STAFF.STATUS = 1	");
		var2.append("	                AND DATE(TRAINING_PLAN.MONTH, 'start of month') = DATE(?,'start of month')	");
		param2.add(dateNow);
		var2.append("	                AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE) = DATE(?)	");
		param2.add(dateNow);
		var2.append("	                AND STAFF.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID	");
		var2.append("	                AND CH.OBJECT_TYPE IN (	");
		var2.append("	                    1,2	");
		var2.append("	                )	");
		var2.append("	                AND CH.TYPE = 2	");
		var2.append("	        ) NVBH_TRAIN	");
		var2.append("	            ON (	");
		var2.append("	                GSNPP.NVBH_SHOP_ID = NVBH_TRAIN.SHOP_ID	");
		var2.append("	                AND NVBH_TRAIN.NVBH_STAFF_ID IN (	");
		var2.append("	                    SELECT	");
		var2.append("	                        PS.STAFF_ID	");
		var2.append("	                FROM	");
		var2.append("	                    PARENT_STAFF_MAP PS	");
		var2.append("	                WHERE	");
		var2.append("	                    1=1	");
		var2.append("	                    AND PS.PARENT_STAFF_ID = GSNPP.STAFF_ID	");
		var2.append("	                    AND PS.STATUS = 1	");
		var2.append("	                    AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)	");
		var2.append("	            )	");
		var2.append("	        )	");
		Cursor cr = null;
		try {
			cr = this.rawQueries(var2.toString(), param2);
			if (cr != null && cr.moveToFirst()) {
				do {
					dto.spinnerStaffList.addItem(cr);
				} while (cr.moveToNext());
			}
		} catch (Exception ex) {
			try {
				throw ex;
			} catch (Exception e) {
			}
		} finally {
			if (cr != null) {
				cr.close();
			}
		}

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT TPD.TRAINING_PLAN_ID, ");
		var1.append("               TPD.TRAINING_PLAN_ID AS TRAINING_PLAN_DETAIL_ID, ");
		var1.append("               TPD.STAFF_ID         AS NVBH_STAFF_ID, ");
		var1.append("               TPD.TRAINING_DATE ");
		var1.append("        FROM   TRAINING_PLAN_DETAIL TPD, ");
		var1.append("               STAFF, ");
		var1.append("               CHANNEL_TYPE CH ");
		var1.append("        WHERE  DATE(TPD.TRAINING_DATE) >= DATE(?,'start of month') ");
		param.add(dateNow);
		var1.append("               AND TPD.AREA_MANAGER_ID = ? ");
		param.add("" + staffId);
		var1.append("               AND TPD.STATUS = 1 ");
		var1.append("               AND TPD.STATUS IN ( 0, 1 ) ");
		var1.append("               AND TPD.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("               AND STAFF.STATUS = 1 ");
		var1.append("               AND STAFF.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID ");
		var1.append("               AND CH.TYPE = 2 ");
		var1.append("               AND CH.OBJECT_TYPE IN ( 1, 2 )) TPDETAIL ");
		var1.append("       JOIN (SELECT TRAINING_PLAN.TRAINING_PLAN_ID, ");
		var1.append("                    TRAINING_PLAN.STAFF_ID AS GSNPP_STAFF_ID, ");
		var1.append("                    STAFF.STAFF_CODE       AS GSNPP_STAFF_CODE, ");
		var1.append("                    STAFF.STAFF_NAME       AS GSNPP_STAFF_NAME ");
		var1.append("             FROM   TRAINING_PLAN, ");
		var1.append("                    STAFF, ");
		var1.append("                    CHANNEL_TYPE CH ");
		var1.append("             WHERE  TRAINING_PLAN.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("                    AND STAFF.STATUS = 1 ");
		var1.append("                    AND STAFF.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID ");
		var1.append("                    AND CH.TYPE = 2 ");
		var1.append("                    AND CH.OBJECT_TYPE = 5 ");
		var1.append("                    AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("                    AND DATE(TRAINING_PLAN.MONTH) >= DATE(?,'start of month'))TPLAN ");
		param.add(dateNow);
		var1.append("         ON TPDETAIL.TRAINING_PLAN_ID = TPLAN.TRAINING_PLAN_ID ");

		Cursor c = null;
		try {
			c = this.rawQueries(var1.toString(), param);
			if (c != null && c.moveToFirst()) {
				do {
					TBHVTrainingPlanDTO.TBHVTrainingPlanItem i = dto.newTBHVGsnppTrainingPlanItem();
					i.initFromCursor(c);
					dto.tbhvTrainingPlan.put(i.dateString, i);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
		} finally {
			if (c != null) {
				c.close();
			}
		}

		for (int i = 0; i < dto.spinnerStaffList.arrList.size(); i++) {
			if (dto.spinnerStaffList.arrList.get(i).nvbhShopId != null) {
				GSNPPTrainingPlanDTO trainingPlan = getGsnppTrainingPlan(
						Integer.valueOf(dto.spinnerStaffList.arrList.get(i).id),dto.spinnerStaffList.arrList.get(i).nvbhShopId);
				if(trainingPlan.listResult.size() > 0) {
					dto.trainingPlanOfGsnppDto = trainingPlan;
					dto.spinnerItemSelected = i;
					break;
				}

			}
		}
		return dto;
	}

	/**
	 * getHistoryPlanTraining
	 *
	 * @author: TamPQ
	 * @return: TBHVHistoryPlanTrainingDTO
	 * @throws:
	 */
	public TBHVTrainingPlanHistoryAccDTO getPlanTrainingHistoryAcc(int staffId, int gsnppStaffId, String shopIdVung, boolean getListStaff) throws Exception {
		TBHVTrainingPlanHistoryAccDTO dto = new TBHVTrainingPlanHistoryAccDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String listGSNPP = SQLUtils.getInstance().getListSupervisorOfASMIncludeHistory(String.valueOf(staffId));
		ArrayList<String> param = new ArrayList<String>();

		if (getListStaff) {
			SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
			ArrayList<String> shopList = shopTable.getShopRecursiveReverse(String.valueOf(shopIdVung));
			String shopStr = TextUtils.join(",", shopList);
			// lay ds gsnpp thuoc quyen quan ly
			ArrayList<String> param2 = new ArrayList<String>();
			StringBuffer var2 = new StringBuffer();
			var2.append("	SELECT	");
			var2.append("	    *	");
			var2.append("	FROM	");
			var2.append("	    (       SELECT	");
			var2.append("	        GS.STAFF_ID          AS STAFF_ID,	");
			var2.append("	        GS.STAFF_CODE        AS STAFF_CODE,	");
			var2.append("	        GS.STAFF_NAME        AS STAFF_NAME,	");
			var2.append("	        SH.SHOP_ID    AS NVBH_SHOP_ID,	");
			var2.append("	        SH.SHOP_CODE   AS NVBH_SHOP_CODE,	");
			var2.append("	        COUNT(S.STAFF_ID) AS NUM_NVBH	");
			var2.append("	    FROM	");
			var2.append("	        (SELECT	");
			var2.append("	            *	");
			var2.append("	        FROM	");
//			var2.append("	            STAFF GS	");
			//lay luon lich su
			var2.append("               (SELECT s.staff_id, ");
			var2.append("                       s.staff_code, ");
			var2.append("                       s.staff_name, ");
			var2.append("                       s.mobilephone, ");
			var2.append("                       s.shop_id, ");
			var2.append("                       s.staff_type_id, ");
			var2.append("                       s.status ");
			var2.append("                FROM   staff s ");
			var2.append("                UNION ");
			var2.append("                SELECT sh.staff_id AS staff_id, ");
			var2.append("                       sh.staff_code, ");
			var2.append("                       sh.staff_name, ");
			var2.append("                       sh.mobilephone, ");
			var2.append("                       sh.shop_id, ");
			var2.append("                       sh.staff_type_id, ");
			var2.append("                       sh.status ");
			var2.append("                FROM   staff_history sh ");
			var2.append("                WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var2.append("                       AND Date(sh.from_date) >= Date('now', 'localtime', ");
			var2.append("                                                 'start of month') ");
			var2.append("                       AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			var2.append("                                                 'start of month') ");
			var2.append("                              OR sh.to_date IS NULL ) ");
			var2.append("                       AND sh.status = 1) GS ");
			var2.append("	        WHERE	");
			var2.append("	            1=1	");
			var2.append("	            AND GS.STATUS = 1	");
			var2.append("	            AND GS.SHOP_ID = ?	");
			param2.add(String.valueOf(shopIdVung));
			var2.append("	            AND GS.STAFF_ID in ( ?	");
			var2.append("	            ) ) GS,	");
			param2.add(listGSNPP);
			var2.append("	        PARENT_STAFF_MAP PS,	");
//			var2.append("	        STAFF S,	");
			//lay lich su
			var2.append("               (SELECT s.staff_id, ");
			var2.append("                       s.staff_code, ");
			var2.append("                       s.staff_name, ");
			var2.append("                       s.mobilephone, ");
			var2.append("                       s.shop_id, ");
			var2.append("                       s.staff_type_id, ");
			var2.append("                       s.status ");
			var2.append("                FROM   staff s ");
			var2.append("                UNION ");
			var2.append("                SELECT sh.staff_id AS staff_id, ");
			var2.append("                       sh.staff_code, ");
			var2.append("                       sh.staff_name, ");
			var2.append("                       sh.mobilephone, ");
			var2.append("                       sh.shop_id, ");
			var2.append("                       sh.staff_type_id, ");
			var2.append("                       sh.status ");
			var2.append("                FROM   staff_history sh ");
			var2.append("                WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var2.append("                       AND Date(sh.from_date) >= Date('now', 'localtime', ");
			var2.append("                                                 'start of month') ");
			var2.append("                       AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			var2.append("                                                 'start of month') ");
			var2.append("                              OR sh.to_date IS NULL ) ");
			var2.append("                       AND sh.status = 1) s, ");
			var2.append("	        SHOP SH,	");
			var2.append("	        CHANNEL_TYPE CT	");
			var2.append("	    WHERE	");
			var2.append("	        PS.PARENT_STAFF_ID =  GS.STAFF_ID	");
			var2.append("	        AND S.STAFF_ID = PS.STAFF_ID	");
			var2.append("	        AND SH.SHOP_ID = S.SHOP_ID	");
			var2.append("	        AND S.SHOP_ID IN (" + shopStr + ")	");
			var2.append("	        AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID	");
			var2.append("	        AND CT.TYPE = 2	");
			var2.append("	        AND CT.OBJECT_TYPE IN (	");
			var2.append("	            1, 2	");
			var2.append("	        )	");
			var2.append("	        AND PS.STATUS = 1	");
			var2.append("	        AND CT.STATUS = 1	");
			var2.append("	        AND S.STATUS = 1	");
			var2.append("	        AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)	");
			var2.append("	    GROUP BY	");
			var2.append("	        GS.STAFF_ID,	");
			var2.append("	        SH.SHOP_ID	");
			var2.append("	    ORDER  BY	");
			var2.append("	        SH.SHOP_CODE,	");
			var2.append("	        GS.STAFF_NAME       ) GSNPP	");
			var2.append("	    LEFT JOIN	");
			var2.append("	        (	");
			var2.append("	            SELECT	");
			var2.append("	                STAFF.STAFF_ID                     AS NVBH_STAFF_ID,	");
			var2.append("	                TRAINING_PLAN_DETAIL.TRAINING_DATE AS NVBH_TRAINING_DATE,	");
			var2.append("	                STAFF.SHOP_ID	");
			var2.append("	            FROM	");
			var2.append("	                TRAINING_PLAN,	");
			var2.append("	                TRAINING_PLAN_DETAIL,	");
//			var2.append("	                STAFF,	");
//			Lay lich su
			var2.append("                         (SELECT s.staff_id, ");
			var2.append("                                 s.staff_code, ");
			var2.append("                                 s.staff_name, ");
			var2.append("                                 s.mobilephone, ");
			var2.append("                                 s.shop_id, ");
			var2.append("                                 s.staff_type_id, ");
			var2.append("                                 s.status ");
			var2.append("                          FROM   staff s ");
			var2.append("                          UNION ");
			var2.append("                          SELECT sh.staff_id AS staff_id, ");
			var2.append("                                 sh.staff_code, ");
			var2.append("                                 sh.staff_name, ");
			var2.append("                                 sh.mobilephone, ");
			var2.append("                                 sh.shop_id, ");
			var2.append("                                 sh.staff_type_id, ");
			var2.append("                                 sh.status ");
			var2.append("                          FROM   staff_history sh ");
			var2.append("                          WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var2.append("                                 AND Date(sh.from_date) >= ");
			var2.append("                                     Date('now', 'localtime', ");
			var2.append("                                     'start of month') ");
			var2.append("                                 AND ( Date(sh.to_date) >= ");
			var2.append("                                       Date('now', 'localtime', ");
			var2.append("                                       'start of month') ");
			var2.append("                                        OR sh.to_date IS NULL ) ");
			var2.append("                                 AND sh.status = 1) staff, ");
			var2.append("	                CHANNEL_TYPE CH	");
			var2.append("	            WHERE	");
			var2.append("	                1 = 1	");
			var2.append("	                AND TRAINING_PLAN.TRAINING_PLAN_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID	");
			var2.append("	                AND TRAINING_PLAN_DETAIL.STAFF_ID = STAFF.STAFF_ID	");
			var2.append("	                AND TRAINING_PLAN_DETAIL.SHOP_ID = STAFF.SHOP_ID	");
			var2.append("	                AND TRAINING_PLAN_DETAIL.STATUS IN (	");
			var2.append("	                    0, 1	");
			var2.append("	                )	");
			var2.append("	                AND TRAINING_PLAN.STATUS = 1	");
			var2.append("	                AND STAFF.STATUS = 1	");
			var2.append("	                AND DATE(TRAINING_PLAN.MONTH, 'start of month') = DATE(?,'start of month')	");
			param2.add(dateNow);
			var2.append("	                AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE) = DATE(?)	");
			param2.add(dateNow);
			var2.append("	                AND STAFF.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID	");
			var2.append("	                AND CH.OBJECT_TYPE IN (	");
			var2.append("	                    1,2	");
			var2.append("	                )	");
			var2.append("	                AND CH.TYPE = 2	");
			var2.append("	        ) NVBH_TRAIN	");
			var2.append("	            ON (	");
			var2.append("	                GSNPP.NVBH_SHOP_ID = NVBH_TRAIN.SHOP_ID	");
			var2.append("	                AND NVBH_TRAIN.NVBH_STAFF_ID IN (	");
			var2.append("	                    SELECT	");
			var2.append("	                        PS.STAFF_ID	");
			var2.append("	                FROM	");
			var2.append("	                    PARENT_STAFF_MAP PS	");
			var2.append("	                WHERE	");
			var2.append("	                    1=1	");
			var2.append("	                    AND PS.PARENT_STAFF_ID = GSNPP.STAFF_ID	");
			var2.append("	                    AND PS.STATUS = 1	");
			var2.append("	                    AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)	");
			var2.append("	            )	");
			var2.append("	        )	");
			Cursor cr = null;
			try {
				cr = this.rawQueries(var2.toString(), param2);
				if (cr != null && cr.moveToFirst()) {
					dto.spinnerStaffList = new ListStaffDTO();
					do {
						dto.spinnerStaffList.addItem(cr);
					} while (cr.moveToNext());
				}
			} catch (Exception ex) {
				try {
					MyLog.d("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
				} catch (Exception e) {
				}
			} finally {
				if (cr != null) {
					cr.close();
				}
			}
			shopIdVung = null;
		}

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT PLAN.*, ");
		var1.append("       SHOP.SHOP_CODE AS NVBH_SHOP_CODE, ");
		var1.append("       SHOP.SHOP_NAME AS NVBH_SHOP_NAME ");
		var1.append("FROM   (select * from (SELECT tpd.*, ");
		var1.append("               STAFF.STAFF_CODE AS NVBH_STAFF_CODE, ");
		var1.append("               STAFF.STAFF_NAME AS NVBH_STAFF_NAME ");
		var1.append("        FROM   TRAINING_PLAN_DETAIL tpd, ");
//		var1.append("               STAFF, ");
//		lay lich su
		var1.append("               (SELECT s.staff_id, ");
		var1.append("                       s.staff_code, ");
		var1.append("                       s.staff_name, ");
		var1.append("                       s.mobilephone, ");
		var1.append("                       s.shop_id, ");
		var1.append("                       s.staff_type_id, ");
		var1.append("                       s.status ");
		var1.append("                FROM   staff s ");
		var1.append("                UNION ");
		var1.append("                SELECT sh.staff_id AS staff_id, ");
		var1.append("                       sh.staff_code, ");
		var1.append("                       sh.staff_name, ");
		var1.append("                       sh.mobilephone, ");
		var1.append("                       sh.shop_id, ");
		var1.append("                       sh.staff_type_id, ");
		var1.append("                       sh.status ");
		var1.append("                FROM   staff_history sh ");
		var1.append("                WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
		var1.append("                       AND Date(sh.from_date) >= Date('now', 'localtime', ");
		var1.append("                                                 'start of month') ");
		var1.append("                       AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
		var1.append("                                                 'start of month') ");
		var1.append("                              OR sh.to_date IS NULL ) ");
		var1.append("                       AND sh.status = 1) STAFF, ");
		var1.append("               CHANNEL_TYPE ch ");
		var1.append("        WHERE  DATE(tpd.TRAINING_DATE) >= DATE(?,'start of month') ");
		param.add(dateNow);
		var1.append("               AND DATE(tpd.TRAINING_DATE) <= DATE(?) ");
		param.add(dateNow);
		var1.append("               AND tpd.AREA_MANAGER_ID = ? ");
		param.add("" + staffId);

		var1.append("               AND tpd.STATUS IN ( 0,1 ) ");
		var1.append("               AND tpd.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("               AND STAFF.STATUS = 1 ");
		var1.append("               AND STAFF.STAFF_TYPE_ID = ch.CHANNEL_TYPE_ID ");
		var1.append("               AND ch.OBJECT_TYPE IN (1,2) ");
		var1.append("               AND ch.TYPE = 2) tpdetail ");
		var1.append("        JOIN (SELECT TRAINING_PLAN.TRAINING_PLAN_ID AS TRAINING_PLAN_ID, ");
		var1.append("                     TRAINING_PLAN.STAFF_ID         AS GSNPP_STAFF_ID, ");
		var1.append("                     STAFF.STAFF_CODE               AS GSNPP_STAFF_CODE, ");
		var1.append("                     STAFF.STAFF_NAME               AS GSNPP_STAFF_NAME ");
		var1.append("              FROM   TRAINING_PLAN, ");
//		var1.append("                     STAFF, ");
//		lay lich su
		var1.append("               (SELECT s.staff_id, ");
		var1.append("                       s.staff_code, ");
		var1.append("                       s.staff_name, ");
		var1.append("                       s.mobilephone, ");
		var1.append("                       s.shop_id, ");
		var1.append("                       s.staff_type_id, ");
		var1.append("                       s.status ");
		var1.append("                FROM   staff s ");
		var1.append("                UNION ");
		var1.append("                SELECT sh.staff_id AS staff_id, ");
		var1.append("                       sh.staff_code, ");
		var1.append("                       sh.staff_name, ");
		var1.append("                       sh.mobilephone, ");
		var1.append("                       sh.shop_id, ");
		var1.append("                       sh.staff_type_id, ");
		var1.append("                       sh.status ");
		var1.append("                FROM   staff_history sh ");
		var1.append("                WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
		var1.append("                       AND Date(sh.from_date) >= Date('now', 'localtime', ");
		var1.append("                                                 'start of month') ");
		var1.append("                       AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
		var1.append("                                                 'start of month') ");
		var1.append("                              OR sh.to_date IS NULL ) ");
		var1.append("                       AND sh.status = 1) STAFF, ");
		var1.append("                     CHANNEL_TYPE ch ");
		var1.append("              WHERE  TRAINING_PLAN.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("                     AND STAFF.STATUS = 1 ");
		var1.append("                     AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("                     AND DATE(TRAINING_PLAN.MONTH) >= DATE(?,'start of month') ");
		param.add(dateNow);
		var1.append("                     AND STAFF.STAFF_TYPE_ID = ch.CHANNEL_TYPE_ID ");
		var1.append("                     AND ch.TYPE = 2 ");
		var1.append("                     AND ch.OBJECT_TYPE = 5)tplan ");
		var1.append("          ON tpdetail.TRAINING_PLAN_ID = tplan.TRAINING_PLAN_ID) PLAN, ");
		var1.append("       SHOP ");
		var1.append("WHERE  PLAN.SHOP_ID = SHOP.SHOP_ID ");
		if (!StringUtil.isNullOrEmpty(shopIdVung)) {
			var1.append("       AND PLAN.SHOP_ID = ? ");
			param.add(shopIdVung);
		}

		if (gsnppStaffId != 0) {
			var1.append("			    AND GSNPP_STAFF_ID = ? ");
			param.add("" + gsnppStaffId);
		}

		var1.append("ORDER  BY TRAINING_DATE ASC ");

		Cursor c = null;
		try {
			c = this.rawQueries(var1.toString(), param);

			if (c != null && c.moveToFirst()) {
				do {
					TBHVTrainingPlanHistoryAccDTO.TBHVHistoryPlanTrainingItem i = dto.newTBHVHistoryPlanTrainingItem();
					i.initFromCursor(c, dto);
				} while (c.moveToNext());
			}

		} catch (Exception ex) {
		} finally {
			if (c != null) {
				c.close();

			}
		}

		return dto;
	}

	/**
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @return: TBHVGsnppTrainingPlanDTO
	 * @throws:
	 */
//	public TBHVTrainingPlanDTO getGsnppTrainingPlan(int staffId, int shopId) {
//		TBHVTrainingPlanDTO dto = new TBHVTrainingPlanDTO();
//		String listGs = PriUtils.getInstance().getListSupervisorOfASM(String.valueOf(staffId));
//		// lay ds gsnpp thuoc quyen quan ly
//		StringBuffer var2 = new StringBuffer();
//		var2.append("SELECT ST.STAFF_ID AS STAFF_ID, ST.STAFF_CODE AS STAFF_CODE, ST.STAFF_NAME AS STAFF_NAME ");
//		var2.append("FROM   STAFF ST ");
//		var2.append("WHERE ST.STAFF_ID IN( ");
//		var2.append(listGs);
//		var2.append("       )");
//		var2.append("ORDER BY ST.STAFF_CODE ASC ");
//
//		Cursor cr = null;
//		try {
//			cr = rawQuery(var2.toString(), null);
//			if (cr != null && cr.moveToFirst()) {
//				do {
//					dto.spinnerStaffList.addItem(cr);
//				} while (cr.moveToNext());
//			}
//		} catch (Exception ex) {
//			MyLog.e("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		} finally {
//			if (cr != null) {
//				cr.close();
//			}
//		}
//
//		ArrayList<String> param2 = new ArrayList<String>();
//		StringBuffer var1 = new StringBuffer();
//		var1.append("SELECT * ");
//		var1.append("FROM   (SELECT TPD.TRAINING_PLAN_ID, ");
//		var1.append("               TPD.TRAINING_PLAN_DETAIL_ID       AS TRAINING_PLAN_DETAIL_ID, ");
//		var1.append("               TPD.STAFF_ID AS NVBH_STAFF_ID, ");
//		var1.append("               TPD.TRAINING_DATE ");
//		var1.append("        FROM   TRAINING_PLAN_DETAIL TPD, ");
//		var1.append("               STAFF ");
//		var1.append("        WHERE  DATE(TPD.TRAINING_DATE) >= DATE('now', 'localtime', 'start of month')");
//		var1.append("               AND TPD.AREA_MANAGER_ID = ? ");
//		param2.add(""+ staffId);
//		var1.append("               AND TPD.STATUS IN ( 0, 1 ) ");
//		var1.append("               AND TPD.STAFF_ID = STAFF.STAFF_ID ");
//		var1.append("               AND STAFF.STATUS = 1 ");
//		var1.append("               					) TPDETAIL ");
//		var1.append("       JOIN (SELECT TRAINING_PLAN.TRAINING_PLAN_ID, ");
//		var1.append("                    TRAINING_PLAN.STAFF_ID AS GSNPP_STAFF_ID, ");
//		var1.append("                    STAFF.STAFF_CODE       AS GSNPP_STAFF_CODE, ");
//		var1.append("                    STAFF.STAFF_NAME             AS GSNPP_STAFF_NAME ");
//		var1.append("             FROM   TRAINING_PLAN, ");
//		var1.append("                    STAFF, ");
//		var1.append("      				 CHANNEL_TYPE CT ");
//		var1.append("             WHERE  TRAINING_PLAN.STAFF_ID = STAFF.STAFF_ID ");
//		var1.append("                    AND STAFF.STATUS = 1 ");
//		var1.append("       			 AND CT.CHANNEL_TYPE_ID = STAFF.STAFF_TYPE_ID ");
//		var1.append("      				 AND CT.TYPE = 2 "); // Loai Nhan vien
//		var1.append("      				 AND CT.OBJECT_TYPE = ?  "); //Loai GSNPP
//		param2.add(String.valueOf(UserDTO.TYPE_GSNPP));
//		var1.append("                    AND TRAINING_PLAN.STATUS = 1 ");
//		var1.append("                    AND DATE(TRAINING_PLAN.MONTH) >= ");
//		var1.append("                        DATE('now', 'localtime', 'start of month'))TPLAN");
//		var1.append("         ON TPDETAIL.TRAINING_PLAN_ID = TPLAN.TRAINING_PLAN_ID ");
//		var1.append("WHERE 1 = 1 ");
//
//		Cursor c = null;
//		try {
//			c = rawQueries(var1.toString(), param2);
//			if (c != null && c.moveToFirst()) {
//				do {
//					TBHVTrainingPlanItem i = dto
//							.newTBHVGsnppTrainingPlanItem();
//					i.initFromCursor(c);
//					dto.tbhvTrainingPlan.put(i.dateString, i);
//				} while (c.moveToNext());
//			}
//		} catch (Exception ex) {
//			MyLog.e("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		} finally {
//			if (c != null) {
//				c.close();
//			}
//		}
//
//		if (dto.spinnerStaffList.arrList.size() > 0) {
//			dto.trainingPlanOfGsnppDto = getPlanTrainResultReportDTO(Integer
//					.valueOf(dto.spinnerStaffList.arrList.get(0).id));
//		}
//		return dto;
//	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: TamPQ
	 * @return: PlanTrainResultReportDTO
	 * @throws:
	 */
	public GSNPPTrainingPlanDTO getPlanTrainResultReportDTO(int staffId) {
		GSNPPTrainingPlanDTO dto = new GSNPPTrainingPlanDTO();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT STAFF.STAFF_ID, ");
		var1.append("       STAFF.STAFF_CODE, ");
		var1.append("       STAFF.NAME, ");
		var1.append("       STAFF.SHOP_ID, ");
		var1.append("       SHOP.SHOP_NAME, ");
		var1.append("       SHOP.SHOP_CODE, ");
		var1.append("       TRAINING_PLAN_DETAIL.ID, ");
		var1.append("       TRAINING_PLAN_DETAIL.TRAINING_DATE, ");
		var1.append("       TRAINING_PLAN_DETAIL.SCORE ");
		var1.append("FROM   TRAINING_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL, ");
		var1.append("       STAFF, ");
		var1.append("       SHOP ");
		var1.append("WHERE  TRAINING_PLAN.STAFF_ID = ? ");
		var1.append("       AND TRAINING_PLAN.ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STATUS IN ( 0, 1 ) ");
		var1.append("       AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("       AND STAFF.STATUS = 1 ");
		var1.append("       AND SHOP.SHOP_ID = STAFF.SHOP_ID ");
		var1.append("       AND DATE(TRAINING_PLAN.MONTH, 'start of month') = ");
		var1.append("           DATE('now', 'localtime', 'start of month') ");
		var1.append("       AND DATE(TRAINING_PLAN_DETAIL.TRAINING_DATE, 'start of month') = ");
		var1.append("           DATE('now', 'localtime', 'start of month') ");

		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					new String[] { String.valueOf(staffId) });
			if (c != null && c.moveToFirst()) {
				do {
					GSNPPTrainingPlanDTO.GSNPPTrainingPlanIem i = dto
							.newPlanTrainResultReportItem();
					i.initFromCursor(c);
					dto.listResult.put(i.dateString, i);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
		} finally {
			if (c != null) {
				c.close();
			}
		}

		return dto;
	}
}
