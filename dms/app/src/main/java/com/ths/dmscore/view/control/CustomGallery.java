/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class CustomGallery extends Gallery {

	private int totalItem;
	private boolean isSpan;

	public CustomGallery(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		setSelection(getSelectedItemPosition());
		return true;
	}

	public void setTotalItem(int total) {
		totalItem = total;
	}

	public void setSpanGallery(boolean span) {
		isSpan = span;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		if (isSpan) {
			if (totalItem <= 5) {
				return false;
			} else {
				return super.onFling(e1, e2, velocityX, velocityY);
			}
		} else {
			if (totalItem <= 6) {
				return false;
			} else {
				return super.onFling(e1, e2, velocityX, velocityY);
			}
		}
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		if (isSpan) {
			if (totalItem <= 5) {
				return false;
			} else {
				return super.onScroll(e1, e2, distanceX, distanceY);
			}
		} else {
			if (totalItem <= 6) {
				return false;
			} else {
				return super.onScroll(e1, e2, distanceX, distanceY);
			}
		}
	}

}
