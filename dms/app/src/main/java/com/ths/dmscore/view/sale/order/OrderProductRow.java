package com.ths.dmscore.view.sale.order;

/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
import java.math.BigDecimal;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.GlobalUtil.QuantityInfo;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

/**
 * Row mat hang khuyen mai
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class OrderProductRow extends DMSTableRow implements OnClickListener,
		TextWatcher, OnFocusChangeListener {
	// so thu tu
	TextView tvSTT;
	// ma mat hang
	TextView tvProductCode;
	// Linear Layout chua product code & dau *
	LinearLayout llMHTT;
	// ten mat hang
	TextView tvProductName;
	// remaind product
	TextView tvRemainProduct;
	// remaind product
	TextView tvRemainProductActual;
	// gia
	EditText etPrice;
	// thuc dat
	EditText etRealOrder;
	// thanh tien
	TextView tvAmount;
	// tien chiet khau
	TextView tvDiscountAmount;
	// khuyen mai
	ImageView ivPromo;
	// linearLayout promo
	LinearLayout llPromo;
	// action
	ImageView ivAction;
	// linearLayout delete
	LinearLayout llActionDelete;
	// listener
	protected OnEventControlListener listener;
	// linear thuc dat
	LinearLayout llRealOrder;
	// dto row
	OrderDetailViewDTO rowDTO;
	// row
	private TableRow row;
	private TextView totalRealOrderTextView;
	private LinearLayout llPrice;
	private LinearLayout llPackagePrice;
	private EditText etPackagePrice;
	boolean isShowPrice;
	boolean isEditPrice;
	public OrderProductRow(Context context, boolean isShowPrice, boolean isEditPrice) {
		super(context, R.layout.order_product_row,
				(isShowPrice ? null : new int[]{ R.id.llPrice, R.id.llPackagePrice, R.id.tvAmount, R.id.tvDiscountAmount }));
		this.isShowPrice = isShowPrice;
		this.isEditPrice = isEditPrice;

		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvProductCode, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHITIETSP);
		PriUtils.getInstance().setOnClickListener(tvProductCode, this);

		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvRemainProduct = (TextView) findViewById(R.id.tvRemainProduct);
		tvRemainProductActual = (TextView) findViewById(R.id.tvRemainProductActual);
		llPrice = (LinearLayout) findViewById(R.id.llPrice);
		etPrice = (EditText) findViewById(R.id.etPrice);
		llPackagePrice = (LinearLayout) findViewById(R.id.llPackagePrice);
		etPackagePrice = (EditText) findViewById(R.id.etPackagePrice);
		if (this.isShowPrice) {
			//dc chinh sua gia, thi them event biet khi chinh sua
			if (isEditPrice) {
				GlobalUtil.setFilterInputPrice(etPrice, Constants.MAX_LENGHT_PRICE);
				GlobalUtil.setFilterInputPrice(etPackagePrice, Constants.MAX_LENGHT_PRICE);
				etPrice.addTextChangedListener(this);
				etPackagePrice.addTextChangedListener(this);
				etPrice.addTextChangedListener(this);
				etPrice.setOnFocusChangeListener(this);
				etPackagePrice.addTextChangedListener(this);
				etPackagePrice.setOnFocusChangeListener(this);
			} else{
				//khong duoc chinh gia thi disable
				etPrice.setEnabled(false);
				etPackagePrice.setEnabled(false);
				etPrice.setFocusable(false);
				etPackagePrice.setFocusable(false);
			}
		}
		etRealOrder = (EditText) findViewById(R.id.etRealOrder);
		GlobalUtil.setFilterInputConvfact(etRealOrder,
				Constants.MAX_LENGHT_QUANTITY);
		// etRealOrder.setFreezesText(true);
		// tvRealOrder.setVisibility(View.GONE);
		etRealOrder.addTextChangedListener(this);
		etRealOrder.setOnFocusChangeListener(this);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvDiscountAmount = (TextView) findViewById(R.id.tvDiscountAmount);
		ivPromo = (ImageView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.ivPromo, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHITIETCTKM);
		PriUtils.getInstance().setOnClickListener(ivPromo, this);
		llPromo = (LinearLayout) findViewById(R.id.llPromo);
		llActionDelete = (LinearLayout) findViewById(R.id.llActionDelete);
		ivAction = (ImageView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.ivAction, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_XOA_SP);
		PriUtils.getInstance().setOnClickListener(ivAction, this);
		// ivAction.setVisibility(View.GONE);
		llRealOrder = (LinearLayout) findViewById(R.id.llRealOrder);
		//chon custom, thi an ban phim ao
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			etRealOrder.setInputType(InputType.TYPE_NULL);
			etRealOrder.setOnClickListener(this);
			//khi duoc chinh gia thi moi can event keyboard
			if (isEditPrice && isShowPrice) {
				etPrice.setInputType(InputType.TYPE_NULL);
				etPrice.setOnClickListener(this);
				etPackagePrice.setInputType(InputType.TYPE_NULL);
				etPackagePrice.setOnClickListener(this);
			}
		}
	}

	public void setListner(OnEventControlListener listener) {
		this.listener = listener;
	}

	/**
	 * Cap nhat data
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	public void updateData(OrderDetailViewDTO dto) {
		updateData(dto, true);
	}

	/**
	 *
	 * Cap nhat da ta co kiem tra stock total
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @param isCheckStockTotal
	 * @return: void
	 * @throws:
	 */
	public void updateData(OrderDetailViewDTO dto, boolean isCheckStockTotal) {
		rowDTO = dto;
		tvSTT.setText(String.valueOf(rowDTO.indexParent + 1));
		String code = rowDTO.productCode;
		if (dto.isFocus > 0) {
			code += "*";
		}
		tvProductCode.setText(code);
		tvProductName.setText(rowDTO.productName);

		//HaiTC: display remain product
		display(tvRemainProduct, dto.remaindStockFormat);
		display(tvRemainProductActual, dto.remaindStockFormatActual);

		if (dto.orderDetailDTO.isPriceNull) {
			display(etPrice, "");
		} else{
			display(etPrice, rowDTO.orderDetailDTO.price);
		}

		if (dto.orderDetailDTO.isPackagePriceNull) {
			display(etPackagePrice, "");
		} else{
			display(etPackagePrice, rowDTO.orderDetailDTO.packagePrice);
		}

		if (StringUtil.isNullOrEmpty(rowDTO.orderDetailDTO.programeCode)) {
			ivPromo.setImageBitmap(null);
			ivPromo.setOnClickListener(null);
		} else{
			ivPromo.setOnClickListener(this);
		}
		display(tvAmount, rowDTO.orderDetailDTO.getAmount());

		double disAmount = rowDTO.orderDetailDTO.getDiscountAmount();
		SpannableString discountAmount = new SpannableString(StringUtil.formatNumber(disAmount));
		if(rowDTO.listPromoDetail.size() > 0) {
			discountAmount.setSpan(new UnderlineSpan(), 0, discountAmount.length(), 0);
			if (tvDiscountAmount != null) {
				tvDiscountAmount.setOnClickListener(this);
			}
			setTextColorResource(R.color.COLOR_USER_NAME, tvDiscountAmount);
		}
		display(tvDiscountAmount, discountAmount);


		if (!StringUtil.isNullOrEmpty(rowDTO.quantity)) {
			//etRealOrder.setTextKeepState(String.valueOf(rowDTO.quantity));
			display(etRealOrder, rowDTO.quantity);
		} else {
			display(etRealOrder, "0");
		}


		// Cap nhat mau khi kiem tra ton kho
		if (isCheckStockTotal) {
			checkStockTotal(dto);
		}
	}

	/**
	 * Kiem tra ton kho
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */

	public void checkStockTotal(OrderDetailViewDTO dto) {
		if (dto.stock <= 0) {
			updateRowWithColor(ImageUtil.getColor(R.color.RED));
		} else if (dto.totalOrderQuantity.orderDetailDTO.quantity > dto.stock) {
			updateRowWithColor(ImageUtil.getColor(R.color.OGRANGE));
		} else {
			setTextColorResource(R.color.BLACK, etRealOrder);
			setTextColorResource(R.color.COLOR_LOCATION_NAME, tvProductCode);
			setTextColorResource(R.color.TITLE_LIGHT_BLACK_COLOR,
					tvSTT, tvProductName, etPrice, etPackagePrice,
					tvRemainProduct, tvRemainProductActual, tvAmount, tvDiscountAmount);
		}
	}

	/**
	 *
	 * Cap nhat full mau cho 1 row
	 *
	 * @author: Nguyen Thanh Dung
	 * @param color
	 * @return: void
	 * @throws:
	 */
	private void updateRowWithColor(int color) {
		setTextColor(color, tvSTT, tvProductCode, tvProductName,
				etRealOrder, etPrice, etPackagePrice, tvRemainProduct,
				tvRemainProductActual, tvAmount, tvDiscountAmount);
	}

	/**
	 * Cap nhat lai STT
	 *
	 * @author: TruongHN
	 * @param numberRow
	 * @return: void
	 * @throws:
	 */
	public void updateOrderNumber() {
		tvSTT.setText(String.valueOf(rowDTO.indexParent + 1));
	}

	/**
	 * Tao row tong - table mat hang ban
	 *
	 * @author: TruongHN
	 * @param price
	 * @param realOrder
	 * @return: void
	 * @throws:
	 */
	public void renderTotalRow(double amount, long numSKU) {
		if (isShowPrice) {
			this.showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT, tvProductCode, tvProductName,
					tvRemainProduct, tvRemainProductActual, llPackagePrice, llPrice);
		} else{
			this.showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT, tvProductCode, tvProductName,
					tvRemainProduct, tvRemainProductActual);
		}
		this.showRowSum(StringUtil.EMPTY_STRING, llPromo, llActionDelete);
		//change TV from ll
		totalRealOrderTextView = changeViewToTextView(etRealOrder);
		if (totalRealOrderTextView != null) {
			totalRealOrderTextView.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
			totalRealOrderTextView.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
					GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5));
		}

		updateTotalValue(amount, numSKU);
	}

	/**
	 * cap nhat row tong
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void updateTotalValue(double amount, long numSKU) {
		// column thanh tien
		SpannableObject objTotal = new SpannableObject();
		objTotal.addSpan(StringUtil.formatNumber(amount),
				ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);
		display(tvAmount, objTotal.getSpan());

		if (totalRealOrderTextView != null) {
			// column sku
			SpannableObject objSku = new SpannableObject();
			objSku.addSpan(StringUtil.formatNumber(numSKU),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			display(totalRealOrderTextView, objSku.getSpan());
		}
	}

	/**
	 * Cap nhat thanh tien
	 *
	 * @author: TruongHN
	 * @param amount
	 * @return: void
	 * @throws:
	 */
	public void updateAmount(double amount) {
		// column thanh tien
		display(tvAmount, amount);

		// Cap nhat mau khi kiem tra ton kho
		checkStockTotal(rowDTO);
	}

	@Override
	public void onClick(View v) {
		if (listener != null) {
			if (v == tvProductCode) {
				listener.onEvent(OrderView.ACTION_VIEW_PRODUCT, this, rowDTO);
			} else if (v == ivPromo) {
				listener.onEvent(OrderView.ACTION_VIEW_PROMOTION, this, rowDTO);
			} else if (v == ivAction) {
				if (rowDTO != null) {
					listener.onEvent(OrderView.ACTION_DELETE, this, rowDTO);
				}
			} else if (tvDiscountAmount != null && v == tvDiscountAmount) {
				listener.onEvent(OrderView.ACTION_VIEW_ORDER_PROMOTION, this, rowDTO);
			} else if (v == row) {
				GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
			} else if (v == etRealOrder || v == etPrice || v == etPackagePrice) {
				listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
			}
		}
	}

	@Override
	public void afterTextChanged(Editable edit) {
		if (etRealOrder != null && edit == etRealOrder.getEditableText()) {
			String realOrder = "";
			if (etRealOrder.getText().toString().length() > 0) {
				realOrder = etRealOrder.getText().toString();
			}
			if (rowDTO != null && rowDTO.orderDetailDTO != null
					&& !realOrder.equals(rowDTO.quantity)) {
				// update row total
				QuantityInfo newQuantity = GlobalUtil.calQuantityFromOrderStr(realOrder, rowDTO.convfact);
				Bundle bundle = new Bundle();
				bundle.putInt(IntentConstants.INTENT_INDEX_PARENT, rowDTO.indexParent);
				bundle.putSerializable(IntentConstants.INTENT_VALUE, newQuantity);

				if (listener != null) {
					listener.onEvent(OrderView.ACTION_CHANGE_REAL_ORDER, this,
							bundle);
				}
			}
		} else if (etPrice != null && edit == etPrice.getEditableText()){
			double price = StringUtil.isNullOrEmpty(etPrice.getText().toString())
					? OrderView.NULL_PRICE : StringUtil.roundPromotion(StringUtil.parseDoubleStr(etPrice.getText().toString()));
			if (rowDTO != null && rowDTO.orderDetailDTO != null
					&& ((!rowDTO.orderDetailDTO.isPriceNull && BigDecimal.valueOf(price).compareTo(BigDecimal.valueOf(rowDTO.orderDetailDTO.price)) != 0)
							|| (rowDTO.orderDetailDTO.isPriceNull && BigDecimal.valueOf(price).compareTo(BigDecimal.valueOf(OrderView.NULL_PRICE)) != 0))){
//					&& ((!rowDTO.orderDetailDTO.isPriceNull && price != rowDTO.orderDetailDTO.price)
//							|| (rowDTO.orderDetailDTO.isPriceNull && price != OrderView.NULL_PRICE))){
				//update price
				Bundle bundle = new Bundle();
				bundle.putInt(IntentConstants.INTENT_INDEX_PARENT, rowDTO.indexParent);
				bundle.putDouble(IntentConstants.INTENT_VALUE, price);

				if (listener != null) {
					listener.onEvent(OrderView.ACTION_CHANGE_PRICE, this, bundle);
				}
			}
		} else if (etPackagePrice != null && edit == etPackagePrice.getEditableText()){
			double price = StringUtil.isNullOrEmpty(etPackagePrice.getText().toString())
						? OrderView.NULL_PRICE : StringUtil.roundPromotion(StringUtil.parseDoubleStr(etPackagePrice.getText().toString()));
			if (rowDTO != null && rowDTO.orderDetailDTO != null
					//truong hop dang khong null thi goi thay doi neu gia thay doi
					&& ((!rowDTO.orderDetailDTO.isPackagePriceNull && BigDecimal.valueOf(price).compareTo(BigDecimal.valueOf(rowDTO.orderDetailDTO.packagePrice)) != 0)
							//truong hop dang null goi thay doi khi gia tri khac null
							|| (rowDTO.orderDetailDTO.isPackagePriceNull && BigDecimal.valueOf(price).compareTo(BigDecimal.valueOf(OrderView.NULL_PRICE)) != 0))){
//					&& ((!rowDTO.orderDetailDTO.isPackagePriceNull && price != rowDTO.orderDetailDTO.packagePrice)
//							//truong hop dang null goi thay doi khi gia tri khac null
//							|| (rowDTO.orderDetailDTO.isPackagePriceNull && price != OrderView.NULL_PRICE))){
				//update package price
				Bundle bundle = new Bundle();
				bundle.putInt(IntentConstants.INTENT_INDEX_PARENT, rowDTO.indexParent);
				bundle.putDouble(IntentConstants.INTENT_VALUE, price);

				if (listener != null) {
					listener.onEvent(OrderView.ACTION_CHANGE_PACKAGE_PRICE, this, bundle);
				}
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		// tinh toan lai so luong gia

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (v == etRealOrder) {
			if (hasFocus) {
				etRealOrder.setSelection(etRealOrder.getText().length());
				if (listener != null) {
					listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
				}
			} else if(etRealOrder.isEnabled()) {
				changeQuantityProductByConfig();
			}
		} else if (v == etPrice || v == etPackagePrice){
			if (!hasFocus) {
				if (v == etPrice) {
					if (rowDTO.orderDetailDTO.isPriceNull) {
						display(etPrice, "");
					} else{
						display(etPrice, rowDTO.orderDetailDTO.price);
					}
				} else {
					if (rowDTO.orderDetailDTO.isPackagePriceNull) {
						display(etPackagePrice, "");
					} else{
						display(etPackagePrice, rowDTO.orderDetailDTO.packagePrice);
					}
				}
			} else if (listener != null) {
				listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
			}
		}
	}

	/**
	 * @author: duongdt3
	 * @since: 16:10:54 13 Apr 2015
	 * @return: void
	 * @throws:
	 * @param b
	 */
	public void setIsEdit(boolean isEdit) {
		if (etPrice != null) {
			etPrice.setEnabled(isEdit);
		}
		if (etPackagePrice != null) {
			etPackagePrice.setEnabled(isEdit);
		}
		if (etRealOrder != null) {
			etRealOrder.setEnabled(isEdit);
		}
	}

	/**
	 * change quantity product by config
	 * @author: duongdt3
	 * @time: 5:24:18 PM Nov 19, 2015
	*/
	public void changeQuantityProductByConfig() {
		String realOrder = etRealOrder.getText().toString();
		String textQuantity = GlobalUtil.getTextQuantity(realOrder);
		if (!StringUtil.isNullOrEmpty(realOrder) && !StringUtil.isNullOrEmpty(textQuantity) && !realOrder.equals(textQuantity)) {
			display(etRealOrder, textQuantity);
		}
	}

}
