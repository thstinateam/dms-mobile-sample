package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dms.R;

/**
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class ListEquipErrorRow extends TableRow {
	Context context;
	View view; 
	TextView tvSTT; 
	TextView tvCodeDevice; 
	TextView tvSeriDevice; 
	EquipInventoryDTO item;
	
	/**
	 * @param context
	 */
	public ListEquipErrorRow(Context context) {
		super(context);
		LayoutInflater vi = (LayoutInflater) context
		.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = vi.inflate(R.layout.layout_list_equip_error_row, this);
		tvSTT = (TextView) view.findViewById(R.id.tvSTT);
		tvCodeDevice = (TextView) view.findViewById(R.id.tvCodeDevice);
		tvSeriDevice = (TextView) view.findViewById(R.id.tvSeriDevice);
	}
	
	/**
	 * @author: DungNX
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(int position, EquipInventoryDTO item){
		this.item = item; 
		String strPos = String.valueOf(position + 1);
		
		tvSTT.setText(strPos);
		tvCodeDevice.setText(item.codeEquip);
		tvSeriDevice.setText(item.seriEquip);
	}
}
