/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.ShopDTO;

public class SupReportAsoViewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	public ArrayList<SupReportAsoDTO> listDto;
	public int totalData;
	public ArrayList<ShopDTO> lstShop;
	public SupReportAsoViewDTO() {
		listDto = new ArrayList<SupReportAsoDTO>();
		lstShop = null;
		totalData = -1;
	}
}
