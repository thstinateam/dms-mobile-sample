/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.GroupMappingDTO;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class GROUP_MAPPING_TABLE extends ABSTRACT_TABLE {

	public static final String GROUP_MAPPING_ID = "GROUP_MAPPING_ID";
	public static final String SALE_GROUP_ID = "SALE_GROUP_ID";
	public static final String SALE_GROUP_LEVEL_ID = "SALE_GROUP_LEVEL_ID";
	public static final String PROMO_GROUP_ID = "PROMO_GROUP_ID";
	public static final String PROMO_GROUP_LEVEL_ID = "PROMO_GROUP_LEVEL_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	
	private static final String TABLE_NAME = "GROUP_MAPPING_TABLE";
	public GROUP_MAPPING_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { GROUP_MAPPING_ID, SALE_GROUP_ID, 
				SALE_GROUP_LEVEL_ID, PROMO_GROUP_ID, PROMO_GROUP_LEVEL_ID, 
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Lay thong tin 1 mapping ung voi 1 group & 1 group level
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: GroupMappingDTO
	 * @throws:  
	 * @param salegroupId
	 * @param saleGroupLevelId
	 * @return
	 * @throws Exception 
	 */
	GroupMappingDTO getGroupMapping(long salegroupId, long saleGroupLevelId) throws Exception {
		GroupMappingDTO result = new GroupMappingDTO();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT * FROM ");
		sql.append("	GROUP_MAPPING GM ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("	AND GM.STATUS = 1 ");
		if(salegroupId > 0) {
			sql.append("	AND GM.SALE_GROUP_ID = ? ");
			params.add(String.valueOf(salegroupId));
		}
		
		if(saleGroupLevelId > 0) {
			sql.append("	AND GM.SALE_GROUP_LEVEL_ID = ? ");
			params.add(String.valueOf(saleGroupLevelId));
		}
		
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					result.initFromCursor(c);
				}
			}
		} catch (Exception e) {
			MyLog.d("getGroupMapping: ", e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		
		return result;
	}

}
