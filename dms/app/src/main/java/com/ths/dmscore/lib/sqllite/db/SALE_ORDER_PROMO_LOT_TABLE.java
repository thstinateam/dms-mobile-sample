package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoLotDTO;


/**
 * Luu thong tin KM
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class SALE_ORDER_PROMO_LOT_TABLE extends ABSTRACT_TABLE {

	// id
	public static final String SALE_ORDER_PROMO_LOT_ID = "SALE_ORDER_PROMO_LOT_ID";
	public static final String SALE_ORDER_DETAIL_ID = "SALE_ORDER_DETAIL_ID";
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// % chiet khau
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// So tien chiet khau
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	//public static final String MAX_QUANTITY_FREE_CK = "MAX_QUANTITY_FREE_CK";
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public static final String IS_OWNER = "IS_OWNER";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String SALE_ORDER_LOT_ID = "SALE_ORDER_LOT_ID";

	public static final String TABLE_NAME = "SALE_ORDER_PROMO_LOT";

	public SALE_ORDER_PROMO_LOT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SALE_ORDER_PROMO_LOT_ID, SALE_ORDER_DETAIL_ID, PROGRAM_TYPE, PROGRAM_CODE, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				 IS_OWNER, SYN_STATE, STAFF_ID, SALE_ORDER_ID, PROGRAME_TYPE_CODE, SHOP_ID, ORDER_DATE,SALE_ORDER_LOT_ID
				/*,MAX_QUANTITY_FREE_CK*/
				};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SaleOrderPromoLotDTO) dto);
		return insert(null, value);
	}

	protected long insert(AbstractTableDTO dto,long saleOderLotId, long saleOrderPromoLotId) {
		SaleOrderPromoDetailDTO promoDto = (SaleOrderPromoDetailDTO) dto;
		return insert(null, initDataRow(promoDto,saleOderLotId,saleOrderPromoLotId));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(SaleOrderPromoDetailDTO dto, long saleOderLotId, long saleOrderPromoLotId) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
		if(dto.isOwner >=0)
			editedValues.put(IS_OWNER, dto.isOwner);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(PROGRAM_TYPE, dto.programType);
		if (!StringUtil.isNullOrEmpty(dto.programTypeCode)) {
			editedValues.put(PROGRAME_TYPE_CODE, dto.programTypeCode);
		}
		if(dto.saleOrderDetailId > 0) {
			editedValues.put(SALE_ORDER_DETAIL_ID, dto.saleOrderDetailId);
		}
		editedValues.put(SALE_ORDER_PROMO_LOT_ID, saleOrderPromoLotId);
		editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(ORDER_DATE, dto.orderDate);
		editedValues.put(SALE_ORDER_LOT_ID, saleOderLotId);

		return editedValues;
	}

	 /**
	 * Xoa  tat ca thong tin lien quan toi promolot
	 * @author: Tuanlt11
	 * @param saleOrderId
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int deleteAllDetailOfOrder(long saleOrderId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(saleOrderId));

		return delete(SALE_ORDER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}

	 /**
	 * Lay thong tin promolot cua don hang
	 * @author: Tuanlt11
	 * @param saleOrderID
	 * @return
	 * @throws Exception
	 * @return: ArrayList<SaleOrderPromoLotDTO>
	 * @throws:
	*/
	public ArrayList<SaleOrderPromoLotDTO> getSaleOrderPromoLotBySaleOrderID(long saleOrderID) throws Exception{
		ArrayList<SaleOrderPromoLotDTO> result = new ArrayList<SaleOrderPromoLotDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    SALE_ORDER_PROMO_LOT	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND SALE_ORDER_ID = ?	");
		paramsObject.add(saleOrderID + "");

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						SaleOrderPromoLotDTO item = new SaleOrderPromoLotDTO();
						item.initDataFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return result;
	}

	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	*/
	private ContentValues initDataRow(SaleOrderPromoLotDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
		editedValues.put(IS_OWNER, dto.isOwner);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(PROGRAM_TYPE, dto.programType);
		editedValues.put(PROGRAME_TYPE_CODE, dto.programTypeCode);
		editedValues.put(SALE_ORDER_DETAIL_ID, dto.saleOrderDetailId);
		editedValues.put(SALE_ORDER_PROMO_LOT_ID, dto.saleOrderPromoLotId);
		editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(ORDER_DATE, dto.orderDate);
		editedValues.put(SALE_ORDER_LOT_ID, dto.saleOrderLotId);

		return editedValues;
	}
}
