package com.ths.dmscore.view.sale.order;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ScrollView;

import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;
import com.ths.dms.R;

public class UpdateRemindFromCallSaleView extends ScrollView implements OnClickListener {
	public SalesPersonActivity parent;
	public OrderView fragParent;
	DMSTableView tbCusFeedBack;// tbCusFeedBack
	Button btClose;
	public View viewLayout;
	CustomerFeedBackDto dto;

	public UpdateRemindFromCallSaleView(Context context, CustomerFeedBackDto dto) {
		super(context);
		parent = (SalesPersonActivity) context;
		fragParent = (OrderView) parent.findFragmentByTag(GlobalUtil.getTag(OrderView.class));
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater.inflate(
				R.layout.layout_update_remind_callsale_view, null);

		tbCusFeedBack = (DMSTableView) viewLayout
				.findViewById(R.id.tbCusFeedBack);
		btClose = (Button) viewLayout.findViewById(R.id.btClose);
		btClose.setOnClickListener(this);
		this.dto = dto;

		renderLayout(dto);

	}

	public void renderLayout(CustomerFeedBackDto dto) {
		tbCusFeedBack.clearAllData();
		if (dto.arrItem.size() > 0) {
			int pos = 1;
			for (int i = 0, s = dto.arrItem.size(); i < s; i++) {
				UpdateRemindFromCallSaleRow row = new UpdateRemindFromCallSaleRow(parent);
				row.cbDone.setOnClickListener(this);
				row.cbDone.setTag(i);
				row.renderLayout(pos, dto.arrItem.get(i));
				pos++;
				tbCusFeedBack.addRow(row);
			}
			tbCusFeedBack.addHeader(new UpdateRemindFromCallSaleRow(parent));
		}
	}

	@Override
	public void onClick(View v) {
		if(v == btClose){
			fragParent.onClick(v);
		}else if(v.getId() == R.id.cbNote){
			CheckBox cb = (CheckBox) v;
			if (cb.isChecked()) {
				Calendar currentDate = Calendar.getInstance(TimeZone
						.getTimeZone("GMT+7"));
				SimpleDateFormat formatter2 = new SimpleDateFormat(
				"dd/MM/yyyy");

				int index = (Integer) cb.getTag();
				FeedBackDTO item = dto.arrItem.get(index);
				item.setDoneDate(DateUtils.now());
				item.setStatus(FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE);

				UpdateRemindFromCallSaleRow row = (UpdateRemindFromCallSaleRow) tbCusFeedBack.getChildAt(index);
				row.tvDoneDate.setText(formatter2.format(currentDate.getTime()));
				fragParent.updateFeedbackRow(item);
			}
		}
	}

}
