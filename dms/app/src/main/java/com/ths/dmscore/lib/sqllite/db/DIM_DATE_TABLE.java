/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

public class DIM_DATE_TABLE extends ABSTRACT_TABLE {
	public static final String DIM_DATE_ID = "DIM_DATE_ID";
	public static final String DATE_TYPE = "DATE_TYPE";
	public static final String DATE_FULL = "DATE_FULL";
	public static final String FROM_DATE = "FROM_DATE";
	public static final String TO_DATE = "TO_DATE";
	public static final String DAY_IN_WEEK_NUM = "DAY_IN_WEEK_NUM";
	public static final String DAY_IN_WEEK_VI = "DAY_IN_WEEK_VI";
	public static final String DAY_IN_MONTH_NUM = "DAY_IN_MONTH_NUM";
	public static final String DAY_IN_YEAR_NUM = "DAY_IN_YEAR_NUM";
	public static final String WEEK_IN_MONTH_NUM = "WEEK_IN_MONTH_NUM";
	public static final String MONTH_IN_YEAR_NUM = "MONTH_IN_YEAR_NUM";
	public static final String QUARTER_IN_YEAR_NUM = "QUARTER_IN_YEAR_NUM";
	public static final String YEAR_NUM = "YEAR_NUM";

	public static final String TABLE_NAME = "DIM_DATE";

	public DIM_DATE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { DIM_DATE_ID, DATE_TYPE, DATE_FULL,
				FROM_DATE, TO_DATE, DAY_IN_WEEK_NUM, DAY_IN_WEEK_VI,
				DAY_IN_MONTH_NUM, DAY_IN_YEAR_NUM, WEEK_IN_MONTH_NUM,
				MONTH_IN_YEAR_NUM, QUARTER_IN_YEAR_NUM, YEAR_NUM, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}