package com.ths.dmscore.lib.network.http;

public class NetworkTimeout {
	public static final int MIN_CONNECT_TIME_OUT = 10000;
	public static final int MIN_READ_TIME_OUT = 10000;
	
	//cau hinh mac dinh
	// thoi gian time-out cho request thong thuong 30s
	public static final int CONNECT_TIME_OUT_DEFAULT = 30000;
	public static final int READ_TIME_OUT_DEFAULT = 120000;
	// thoi gian request dong bo du lieu 4 phut
	public static final int READ_TIME_OUT_SYNDATA_DEFAULT = 240000;
	// thoi gian request login 30s
	public static final int CONNECT_TIME_OUT_LOGIN_DEFAULT = 30000;
	public static final int READ_TIME_OUT_LOGIN_DEFAULT = 30000;
	//thoi gian request download file
	public static final int CONNECT_TIME_OUT_DOWNLOAD_FILE_DEFAULT = 480000;
	public static final int READ_TIME_OUT_DOWNLOAD_FILE_DEFAULT = 1800000;
	
	//cau hinh su dung that
	private static int CONNECT_TIME_OUT = CONNECT_TIME_OUT_DEFAULT;
	private static int READ_TIME_OUT = READ_TIME_OUT_DEFAULT;
	private static int READ_TIME_OUT_SYNDATA = READ_TIME_OUT_SYNDATA_DEFAULT;
	private static int CONNECT_TIME_OUT_LOGIN = CONNECT_TIME_OUT_LOGIN_DEFAULT;
	private static int READ_TIME_OUT_LOGIN = READ_TIME_OUT_LOGIN_DEFAULT;
	private static int CONNECT_TIME_OUT_DOWNLOAD_FILE = CONNECT_TIME_OUT_DOWNLOAD_FILE_DEFAULT;
	private static int READ_TIME_OUT_DOWNLOAD_FILE = READ_TIME_OUT_DOWNLOAD_FILE_DEFAULT;
	
	public static int getCONNECT_TIME_OUT() {
		return CONNECT_TIME_OUT;
	}
	public static void setCONNECT_TIME_OUT(int cONNECT_TIME_OUT) {
		CONNECT_TIME_OUT = cONNECT_TIME_OUT;
	}
	public static int getREAD_TIME_OUT() {
		return READ_TIME_OUT;
	}
	public static void setREAD_TIME_OUT(int rEAD_TIME_OUT) {
		READ_TIME_OUT = rEAD_TIME_OUT;
	}
	public static int getREAD_TIME_OUT_SYNDATA() {
		return READ_TIME_OUT_SYNDATA;
	}
	public static void setREAD_TIME_OUT_SYNDATA(int rEAD_TIME_OUT_SYNDATA) {
		READ_TIME_OUT_SYNDATA = rEAD_TIME_OUT_SYNDATA;
	}
	public static int getCONNECT_TIME_OUT_LOGIN() {
		return CONNECT_TIME_OUT_LOGIN;
	}
	public static void setCONNECT_TIME_OUT_LOGIN(int cONNECT_TIME_OUT_LOGIN) {
		CONNECT_TIME_OUT_LOGIN = cONNECT_TIME_OUT_LOGIN;
	}
	public static int getREAD_TIME_OUT_LOGIN() {
		return READ_TIME_OUT_LOGIN;
	}
	public static void setREAD_TIME_OUT_LOGIN(int rEAD_TIME_OUT_LOGIN) {
		READ_TIME_OUT_LOGIN = rEAD_TIME_OUT_LOGIN;
	}
	public static int getCONNECT_TIME_OUT_DOWNLOAD_FILE() {
		return CONNECT_TIME_OUT_DOWNLOAD_FILE;
	}
	public static void setCONNECT_TIME_OUT_DOWNLOAD_FILE(
			int cONNECT_TIME_OUT_DOWNLOAD_FILE) {
		CONNECT_TIME_OUT_DOWNLOAD_FILE = cONNECT_TIME_OUT_DOWNLOAD_FILE;
	}
	public static int getREAD_TIME_OUT_DOWNLOAD_FILE() {
		return READ_TIME_OUT_DOWNLOAD_FILE;
	}
	public static void setREAD_TIME_OUT_DOWNLOAD_FILE(
			int rEAD_TIME_OUT_DOWNLOAD_FILE) {
		READ_TIME_OUT_DOWNLOAD_FILE = rEAD_TIME_OUT_DOWNLOAD_FILE;
	}
	
	
}
