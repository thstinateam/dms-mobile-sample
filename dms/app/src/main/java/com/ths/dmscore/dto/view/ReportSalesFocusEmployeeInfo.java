/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  thong tin mot dong bao cao mat hang trong tam cua 1 NPP va 1 GSNPP
 *  @author: HaiTC3
 *  @version: 1.1
 *  @since: 1.0
 */
public class ReportSalesFocusEmployeeInfo implements Serializable{
	private static final long serialVersionUID = 4232021236805724107L;
	// children object id
	public long staffId;
	// staff code
	public String staffCode;
	// children object name
	public String staffName;
	// parent object id
	public long staffOwnerId;
	// staff owner code
	public String staffOwnerCode;
	// parent object name
	public String staffOwnerName;
	// list report focus item
	public ArrayList<ReportFocusProductItem> listFocusProductItem = new ArrayList<ReportFocusProductItem>();

	public ReportSalesFocusEmployeeInfo() {
		staffId = 0;
		staffName = "";
		staffOwnerId = 0;
		staffOwnerName = "";
		staffCode = "";
		staffOwnerCode = "";
		listFocusProductItem = new ArrayList<ReportFocusProductItem>();
	}
	
	/**
	 * parse data from cursor after query sql success
	*  Mo ta chuc nang cua ham
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void parseDataFromCursor(Cursor c){
		staffId = CursorUtil.getLong(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		staffOwnerId = CursorUtil.getLong(c, "STAFF_OWNER_ID");
		staffOwnerCode = CursorUtil.getString(c, "STAFF_OWNER_CODE");
		staffOwnerName = CursorUtil.getString(c, "STAFF_OWNER_NAME");
	}
}
