package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.ListReportColumnCatDTO;
import com.ths.dmscore.dto.view.ListReportColumnProductDTO;
import com.ths.dmscore.dto.view.ReportColumnCatDTO;
import com.ths.dmscore.dto.view.ReportColumnProductDTO;
import com.ths.dmscore.dto.view.ReportRowStaffDTO;
import com.ths.dmscore.dto.view.ReportRowStaffDialogDTO;
import com.ths.dmscore.dto.view.ReportRowUnitDTO;
import com.ths.dmscore.dto.view.ReportRowUnitDialogDTO;

public class REPORT_TEMPLATE_OBJECT_TABLE
		extends ABSTRACT_TABLE {
	public static final String REPORT_TEMPLATE_OBJECT_ID = "REPORT_TEMPLATE_OBJECT_ID";
	public static final String REPORT_TEMPLATE_ID = "REPORT_TEMPLATE_ID";
	public static final String TYPE = "TYPE";
	public static final String OBJECT_ID = "OBJECT_ID";
	public static final String VALUE = "VALUE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "REPORT_TEMPLATE_OBJECT";

	public REPORT_TEMPLATE_OBJECT_TABLE(
			SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] {
				REPORT_TEMPLATE_OBJECT_ID,
				REPORT_TEMPLATE_ID,
				TYPE, OBJECT_ID, VALUE,
				CREATE_DATE,
				UPDATE_DATE,
				CREATE_USER,
				UPDATE_USER, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName
				+ ";";
		this.sqlDelete += this.tableName
				+ ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	 /**
	 * Lay du lieu bao cao dong chon nhan vien cho row
	 * @author: Tuanlt11
	 * @param viewData
	 * @return
	 * @throws Exception
	 * @return: CustomerListDTO
	 * @throws:
	*/
	public ReportRowStaffDialogDTO getDataStaffReportChooseRow(Bundle viewData) throws Exception {
		// TODO Auto-generated method stub
		ReportRowStaffDialogDTO dto = new ReportRowStaffDialogDTO();
		boolean isGetTotalPage = (Boolean)viewData
				.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		int page = viewData.getInt(IntentConstants.INTENT_PAGE);
		String textSearch = viewData.getString(IntentConstants.INTENT_SEARCH);
		Integer[] lstObjectid = (Integer[]) viewData
				.getSerializable(IntentConstants.INTENT_LIST_ID_ROW_CHOOSE);
		boolean isLeaveJob = viewData.getBoolean(IntentConstants.INTENT_LEAVE_JOB);
//		String roleId = viewData.getString(IntentConstants.INTENT_ROLE_ID);
//		String shopId = viewData.getString(IntentConstants.INTENT_SHOP_ID);
//		int staffId = viewData.getInt(IntentConstants.INTENT_STAFF_ID);
		String idListString = TextUtils.join(",", lstObjectid);
		StringBuffer sqlObject = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		sqlObject.append("	SELECT	");
		sqlObject.append("	    st.staff_id AS STAFF_ID,	");
		sqlObject.append("	    st.staff_code AS STAFF_CODE,	");
		sqlObject.append("	    st.staff_name AS STAFF_NAME,	");
		sqlObject.append("	    st.status AS STATUS,	");
		sqlObject.append("	    st.NAME_TEXT AS NAME_TEXT,	");
		sqlObject.append("	    sh.SHOP_ID     AS SHOP_ID,	");
		sqlObject.append("	    sh.SHOP_CODE AS SHOP_CODE,	");
		sqlObject.append("		sh.SHOP_NAME AS SHOP_NAME	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    staff st	");
		// chi quy dinh NV co 1 shop duy nhat, de tranh truong hop NV hien thi nhieu shop
		// GS co the co nhieu shop, nhung ko hien thi shop tren view, chi tong hop theo GS
		sqlObject.append("	    LEFT JOIN shop sh	");
		sqlObject.append("	    	 ON st.shop_id = sh.shop_id		");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    st.staff_id IN (	");
		sqlObject.append(idListString);
		sqlObject.append("	        )	");
		if (!isLeaveJob) {
			sqlObject.append("	AND ST.status = 1	");
		}
		if (!StringUtil.isNullOrEmpty(textSearch)) {
			textSearch = StringUtil
					.getEngStringFromUnicodeString(textSearch);
			textSearch = StringUtil.escapeSqlString(textSearch);
			sqlObject.append("	and (upper(st.STAFF_CODE) like upper(?) or upper(st.NAME_TEXT) like upper(?) or upper(sh.SHOP_NAME) like upper(?))  ");
			param.add("%" + textSearch + "%");
			param.add("%" + textSearch + "%");
			param.add("%" + textSearch + "%");
		}
//		sqlObject.append("	order by staff_id,shop_id	");
		sqlObject.append("	order by STAFF_CODE asc ,SHOP_CODE asc	");
		if (page > 0) {
			// get count
			if (isGetTotalPage) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ sqlObject + ")");
			}
			sqlObject.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlObject.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor cCursor = null;
		Cursor cTotalRow = null;

		try {
			cCursor = rawQueries(sqlObject.toString(), param);
			if (cCursor != null) {
				if (cCursor.moveToFirst()) {
					do {
						ReportRowStaffDTO item = new ReportRowStaffDTO();
						item.initFromCursor(cCursor);
						dto.lstStaff.add(item);
					} while (cCursor.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage) {
				cTotalRow = rawQueries(totalPageSql.toString(), param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalStaff = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (cCursor != null) {
					cCursor.close();
				}
			} catch (Exception e) {
			}
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}


	/**
	 * Lay du lieu bao cao dong chon don vi cho row
	 *
	 * @author: Tuanlt11
	 * @param viewData
	 * @return
	 * @throws Exception
	 * @return: CustomerListDTO
	 * @throws:
	 */
	public ReportRowUnitDialogDTO getDataUnitReportChooseRow(Bundle viewData)
			throws Exception {
		ReportRowUnitDialogDTO dto = new ReportRowUnitDialogDTO();
		boolean isGetTotalPage = (Boolean)viewData
				.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		int page = viewData.getInt(IntentConstants.INTENT_PAGE);
		boolean isPending = viewData.getBoolean(IntentConstants.INTENT_LEAVE_JOB);
		String order = viewData.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		boolean orderAscOrDesc = viewData.getBoolean(IntentConstants.INTENT_ORDER_TYPE, true);
		String textSearch = viewData.getString(IntentConstants.INTENT_SEARCH);
		Integer[] lstObjectid = (Integer[]) viewData
				.getSerializable(IntentConstants.INTENT_LIST_ID_ROW_CHOOSE);
		String idListString = TextUtils.join(",", lstObjectid);
		StringBuffer sqlObject = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		sqlObject.append("	SELECT	");
		sqlObject.append("	    sh.SHOP_ID     AS SHOP_ID,	");
		sqlObject.append("	    sh.SHOP_CODE AS SHOP_CODE,	");
		sqlObject.append("	    sh.STATUS AS STATUS,	");
		sqlObject.append("	    sh.SHOP_NAME AS SHOP_NAME	");
		sqlObject.append("	FROM	");
		sqlObject.append("	     SHOP sh	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	      1=1	");
		sqlObject.append("	      AND sh.shop_id IN (	");
		sqlObject.append(idListString);
		sqlObject.append("	        )	");
		if (!isPending) {
			sqlObject.append("	AND sh.status = 1	");
		}
		if (!StringUtil.isNullOrEmpty(textSearch)) {
			textSearch = StringUtil
					.getEngStringFromUnicodeString(textSearch);
			textSearch = StringUtil.escapeSqlString(textSearch);
			sqlObject.append("	and (upper(SH.SHOP_CODE) like upper(?) or upper(SH.NAME_TEXT) like upper(?) )  ");
			param.add("%" + textSearch + "%");
			param.add("%" + textSearch + "%");
		}
		if(StringUtil.isNullOrEmpty(order)) {
			sqlObject.append("	order by sh.shop_code asc, sh.SHOP_NAME asc	");
		} else {
			sqlObject.append(StringUtil.getStringOrder(order, orderAscOrDesc));
		}
		if (page > 0) {
			// get count
			if (isGetTotalPage) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ sqlObject + ")");
			}
			sqlObject.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlObject.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor cCursor = null;
		Cursor cTotalRow = null;

		try {
			cCursor = rawQueries(sqlObject.toString(), param);
			if (cCursor != null) {
				if (cCursor.moveToFirst()) {
					do {
						ReportRowUnitDTO item = new ReportRowUnitDTO();
						item.initFromCursor(cCursor);
						dto.lstUnit.add(item);
					} while (cCursor.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage) {
				cTotalRow = rawQueries(totalPageSql.toString(), param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalUnit = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (cCursor != null) {
					cCursor.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;

	}

	/**
	 * lay danh sach san pham cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:03:03 PM Oct 28, 2014
	 * @return: ChooseParametersDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ListReportColumnProductDTO getProductListForReport(Bundle b) {
		ArrayList<Integer> listID = b.getIntegerArrayList(IntentConstants.INTENT_ARRAY_STRING_LIST);
		boolean isGetTotal = b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, false);
		int page = Integer.parseInt(b.getString(IntentConstants.INTENT_PAGE, "1"));
		String order = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		boolean orderAscOrDesc = b.getBoolean(IntentConstants.INTENT_ORDER_TYPE, true);
		String[] dataSearch = b.getStringArray(IntentConstants.INTENT_DATA);

		String listProductID = TextUtils.join(",", listID);

		ListReportColumnProductDTO listProduct = new ListReportColumnProductDTO();
		Cursor c = null;

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT p.product_code as PRODUCT_CODE, ");
		varname1.append("       p.product_name as PRODUCT_NAME, ");
		varname1.append("       p.product_id as PRODUCT_ID, ");
		varname1.append("       pi.[product_info_name] as PRODUCT_INFO_NAME, ");
		varname1.append("       pi.[product_info_code] as PRODUCT_INFO_CODE, ");
		varname1.append("       p.[cat_id] as PRODUCT_INFO_ID, ");
		varname1.append("       p.[sub_cat_id] as SUB_CAT_ID, ");
		varname1.append("       subpi.[product_info_name] as SUB_CAT_NAME ");
		varname1.append("       ,subpi.[product_info_code] as SUB_CAT_CODE ");
		varname1.append("       ,p.status as STATUS ");
		varname1.append(" FROM   (SELECT product_code, ");
		varname1.append("               product_name, ");
		varname1.append("               product_id, ");
		varname1.append("               cat_id, ");
		varname1.append("               NAME_TEXT, ");
		varname1.append("               status, ");
		varname1.append("               sub_cat_id ");
		varname1.append("        FROM   product ");
		varname1.append("        WHERE  product_id IN (  ");
		varname1.append(listProductID);
		varname1.append(" ) ");
		if(dataSearch.length > 0 && dataSearch[5].equals("0")){
			varname1.append(" and status = 1 ");
		} else {
			varname1.append(" and (status = 1 or status = 0) ");
		}
		varname1.append(" ) p");
		varname1.append("       JOIN (SELECT product_info_id, ");
		varname1.append("                    product_info_code, ");
		varname1.append("                    product_info_name ");
		varname1.append("             FROM   product_info) pi ");
		varname1.append("         ON p.cat_id = pi.product_info_id ");
		varname1.append("       JOIN (SELECT product_info_id, ");
		varname1.append("                    product_info_code, ");
		varname1.append("                    product_info_name ");
		varname1.append("             FROM   product_info) subpi ");
		varname1.append("         ON p.[sub_cat_id] = subpi.product_info_id");
		varname1.append(" WHERE 1=1 ");
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						ReportColumnCatDTO dto = new ReportColumnCatDTO();
						dto.initDataWithCursor(c);
						listProduct.addCategory(dto);
					}while(c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		if(dataSearch != null && dataSearch.length > 0){

			if (!StringUtil.isNullOrEmpty(dataSearch[0])) {
				dataSearch[0] = StringUtil
						.getEngStringFromUnicodeString(dataSearch[0]);
				dataSearch[0] = StringUtil.escapeSqlString(dataSearch[0]);
				dataSearch[0] = DatabaseUtils.sqlEscapeString("%"
						+ dataSearch[0] + "%");
				varname1.append(" and	upper(p.product_code) like upper(");
				varname1.append(dataSearch[0]);
				varname1.append(") escape '^' ");
			}
			if (!StringUtil.isNullOrEmpty(dataSearch[1])) {
				dataSearch[1] = StringUtil
						.getEngStringFromUnicodeString(dataSearch[1]);
				dataSearch[1] = StringUtil.escapeSqlString(dataSearch[1]);
				dataSearch[1] = DatabaseUtils.sqlEscapeString("%"
						+ dataSearch[1] + "%");
				varname1.append(" and	upper(p.NAME_TEXT) LIKE upper(");
				varname1.append(dataSearch[1]);
				varname1.append(") escape '^' ");
			}
			int producCatID = Integer.parseInt(dataSearch[2]);
			if (producCatID > 0) {
				varname1.append(" and	pi.product_info_id = ? ");
				params.add(dataSearch[2]);
			}
			int producSubCatInfoID = Integer.parseInt(dataSearch[3]);
			if (producSubCatInfoID > 0) {
				varname1.append(" and	subpi.product_info_id = ? ");
				params.add(dataSearch[3]);
			}
		}

		if (isGetTotal) {
			try {
				String total = StringUtil.getCountSql(varname1.toString());
				c = rawQueries(total, params);

				if (c != null) {
					if (c.moveToFirst()) {
						listProduct.totalItem = c.getInt(0);
					}
				}
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e) {
				}
			}
		}
		varname1.append(StringUtil.getStringOrder(order, orderAscOrDesc));
		varname1.append(StringUtil.getPagingSql(Constants.NUM_ITEM_PER_PAGE, page));
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						ReportColumnProductDTO dto = new ReportColumnProductDTO();
						dto.initDataWithCursor(c);
						listProduct.listReportProduct.add(dto);
					}while(c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listProduct;
	}

	/**
	 * Lay danh sach Nganh hang cho bao cao
	 *
	 * @author: dungdq3
	 * @since: 2:39:47 PM Oct 30, 2014
	 * @return: ListReportColumnCatDTO
	 * @throws:
	 * @param b
	 * @return
	 */
	public ListReportColumnCatDTO getCategoryForReport(Bundle b) {
		// TODO Auto-generated method stub
		ArrayList<Integer> listID = b.getIntegerArrayList(IntentConstants.INTENT_ARRAY_STRING_LIST);
		boolean isGetTotal = b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, false);
		int page = Integer.parseInt(b.getString(IntentConstants.INTENT_PAGE, "1"));
		String catCode = b.getString(IntentConstants.INTENT_PRODUCT_CAT, Constants.STR_BLANK);
		String catName = b.getString(IntentConstants.INTENT_PRODUCT_CODE, Constants.STR_BLANK);
		boolean isPending = b.getBoolean(IntentConstants.INTENT_LEAVE_JOB, false);
		String order = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		boolean orderAscOrDesc = b.getBoolean(IntentConstants.INTENT_ORDER_TYPE, true);
		String listCatID = TextUtils.join(",", listID);

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT product_info_id as PRODUCT_INFO_ID, ");
		varname1.append("       product_info_name as PRODUCT_INFO_NAME, ");
		varname1.append("       status as STATUS, ");
		varname1.append("       product_info_code as PRODUCT_INFO_CODE ");
		varname1.append("FROM   product_info ");
		varname1.append("WHERE  product_info_id IN ( ");
		varname1.append(listCatID);
		varname1.append(" ) ");
		if(!isPending){
			varname1.append(" and status = 1 ");
		}
		if (!StringUtil.isNullOrEmpty(catCode)) {
			catCode = StringUtil
					.getEngStringFromUnicodeString(catCode);
			catCode = StringUtil.escapeSqlString(catCode);
			catCode = DatabaseUtils.sqlEscapeString("%"
					+ catCode + "%");
			varname1.append(" and	upper(product_info_code) like upper(");
			varname1.append(catCode);
			varname1.append(") escape '^' ");
		}
		if (!StringUtil.isNullOrEmpty(catName)) {
			catName = StringUtil
					.getEngStringFromUnicodeString(catName);
			catName = StringUtil.escapeSqlString(catName);
			catName = DatabaseUtils.sqlEscapeString("%"
					+ catName + "%");
			varname1.append(" and	upper(product_info_name) like upper(");
			varname1.append(catName);
			varname1.append(") escape '^' ");
		}
		
		ListReportColumnCatDTO listProduct = new ListReportColumnCatDTO();
		Cursor c = null;

		if (isGetTotal) {
			try {
				String total = StringUtil.getCountSql(varname1.toString());
				c = rawQueries(total, params);

				if (c != null) {
					if (c.moveToFirst()) {
						listProduct.totalItem = c.getInt(0);
					}
				}
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e) {
				}
			}
		}
		varname1.append(StringUtil.getStringOrder(order, orderAscOrDesc));
		varname1.append(StringUtil.getPagingSql(Constants.NUM_ITEM_PER_PAGE, page));
		try {
			c = rawQueries(varname1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						ReportColumnCatDTO dto = new ReportColumnCatDTO();
						dto.initDataWithCursor(c);
						listProduct.listReportColumnCatDTO.add(dto);
					}while(c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return listProduct;
	}
}