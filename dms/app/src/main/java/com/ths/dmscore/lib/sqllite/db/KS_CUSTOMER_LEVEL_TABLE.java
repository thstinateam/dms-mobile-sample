package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;

/**
 * table muc dang ki keyshop cua kh
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KS_CUSTOMER_LEVEL_TABLE extends ABSTRACT_TABLE {

	public static final String KS_CUSTOMER_LEVEL_ID = "KS_CUSTOMER_LEVEL_ID";
	public static final String KS_CUSTOMER_ID = "KS_CUSTOMER_ID";
	public static final String KS_LEVEL_ID = "KS_LEVEL_ID";
	public static final String MULTIPLIER = "MULTIPLIER";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	public static final String KS_ID = "KS_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String TABLE_NAME = "KS_CUSTOMER_LEVEL";

	public KS_CUSTOMER_LEVEL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { CREATE_DATE, CREATE_USER, KS_CUSTOMER_ID,
				KS_CUSTOMER_LEVEL_ID, KS_LEVEL_ID, MULTIPLIER, STATUS,
				UPDATE_DATE, UPDATE_USER,CREATE_USER_ID,SYN_STATE,KS_ID,SHOP_ID };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}


	/* (non-Javadoc)
	 * @see ABSTRACT_TABLE#insert(AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see ABSTRACT_TABLE#update(AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see ABSTRACT_TABLE#delete(AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public long insert(RegisterKeyShopItemDTO dto) {
		ContentValues value = initDataRowInsert(dto);
		return insert(null, value);
	}

	public long update(RegisterKeyShopItemDTO dto) {
		ContentValues value = initDataRowUpdate(dto);
		String[] params = { "" + dto.ksCustomerLevelItem.ksCustomerLevelId };
		return update(value, KS_CUSTOMER_LEVEL_ID + " = ?", params);
	}

	private ContentValues initDataRowInsert(RegisterKeyShopItemDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(KS_CUSTOMER_LEVEL_ID, dto.ksCustomerLevelItem.ksCustomerLevelId);
		editedValues.put(KS_CUSTOMER_ID, dto.ksCustomerLevelItem.ksCustomerId);
		editedValues.put(KS_LEVEL_ID, dto.ksCustomerLevelItem.ksLevelId);
		editedValues.put(MULTIPLIER, dto.ksCustomerLevelItem.multiplier);
		editedValues.put(CREATE_DATE, dto.ksCustomerLevelItem.createDate);
		editedValues.put(CREATE_USER_ID, dto.ksCustomerLevelItem.createUserId);
		editedValues.put(STATUS, 1);
		editedValues.put(CREATE_USER, dto.ksCustomerLevelItem.createUser);
		editedValues.put(KS_ID, dto.ksCustomerLevelItem.ksId);
		editedValues.put(SHOP_ID, dto.ksCustomerLevelItem.shopId);
		return editedValues;
	}

	private ContentValues initDataRowUpdate(RegisterKeyShopItemDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(MULTIPLIER, dto.ksCustomerLevelItem.multiplier);
		editedValues.put(UPDATE_DATE, dto.ksCustomerLevelItem.updateDate);
		editedValues.put(UPDATE_USER, dto.ksCustomerLevelItem.updateUser);
		return editedValues;
	}


}
