package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * 
 *  Chua du lieu man hinh quan ly thiet bi cua TBHV
 *  @author: Nguyen Thanh Dung
 *  @version: 1.1
 *  @since: 1.0
 */
public class TBHVManagerEquipmentDTO {
	public int totalList;	
	public String totalofTotal;
	
	public ArrayList<TBHVManagerEquipmentItem> arrList;
	
	public TBHVManagerEquipmentDTO(){
		arrList = new ArrayList<TBHVManagerEquipmentDTO.TBHVManagerEquipmentItem>();		
	}
	
	public void addItem(Cursor c){
		TBHVManagerEquipmentItem item = new TBHVManagerEquipmentItem();	
		item.id = CursorUtil.getString(c, "id");
		item.maNVBH = CursorUtil.getString(c, "maNVBH");
		item.nvbh = CursorUtil.getString(c, "nvbh");
		item.soThietBi = CursorUtil.getInt(c, "soThietBi");
		item.khongDat = CursorUtil.getInt(c, "khongDat");		
		arrList.add(item);
	}
	
	public class TBHVManagerEquipmentItem{
		public String id;
		public String maNVBH;
		public String nvbh;	
		public int soThietBi;
		public int khongDat;		
		
		public TBHVManagerEquipmentItem(){
			id = "1";
			maNVBH = "maNVBH";
			nvbh = "maNVBH";
			soThietBi = 1;
			khongDat = 1;					
		}
	}
}
