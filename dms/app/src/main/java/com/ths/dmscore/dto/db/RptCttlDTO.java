package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.RPT_CTTL_TABLE;
import com.ths.dmscore.util.CursorUtil;

public class RptCttlDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public long rptCttlId;
	public String createDate;
	public long promotionProgramId;
	public String promotionFromDate;
	public String promotionToDate;
	public long shopId;
	public long customerId;
	public double totalQuantity;
	public long totalAmount;
	public double totalQuantityPayPromotion;
	public long totalAmountPayPromotion;
	public int totalPromotion;
	public String lastDatePromotion;
	public double balancePromotion; 
	
	/** Khoi tao tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param cOrder
	 */
	public void initFromCursor(Cursor cOrder) {
		rptCttlId = CursorUtil.getLong(cOrder, RPT_CTTL_TABLE.RPT_CTTL_ID);
		totalAmount = CursorUtil.getLong(cOrder, RPT_CTTL_TABLE.TOTAL_AMOUNT);
		totalQuantity = CursorUtil.getDouble(cOrder, RPT_CTTL_TABLE.TOTAL_QUANTITY);
		balancePromotion = CursorUtil.getDouble(cOrder, RPT_CTTL_TABLE.BALANCE_PROMOTION);
	}
}