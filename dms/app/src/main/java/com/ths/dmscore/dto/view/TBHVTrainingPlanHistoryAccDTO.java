package com.ths.dmscore.dto.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * TBHVHistoryPlanTrainingDTO
 * 
 * @author : TamPQ
 * @version: 1.1
 * @since : 1.0
 */
public class TBHVTrainingPlanHistoryAccDTO {
	public ArrayList<TBHVHistoryPlanTrainingItem> listResult = new ArrayList<TBHVHistoryPlanTrainingItem>();
	public double amountMonth; // amount Month
	public double amount;// amount
	public int numCustomerPlan;// tong so customer
	public int numCustomerOrder;// so customer order
	public int numCustomerNew;// so customer moi
	public int numCustomerOn;// so customer trong
	public int numCustomerOr;// so Customer ngoai tuyen
	public double score;// diem
	public int shopId;// shop id
	public ListStaffDTO spinnerStaffList;

	/**
	 * new TBHVHistoryPlanTrainingItem dto
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public TBHVHistoryPlanTrainingItem newTBHVHistoryPlanTrainingItem() {
		return new TBHVHistoryPlanTrainingItem();
	}

	public class TBHVHistoryPlanTrainingItem {
		public int trainDetailId;// training Detail Id
		public int staffId;// npp staff Id
		public int managerId;//  TBHV Id
		public String staffCode;// npp staff Code
		public String staffName;// npp staff Name
		public String date;// dayInOrder
		public double amountMonth;// amount Month
		public double amount;// amount
		public int numCustomerPlan;// num Customer Plan
		public int numCustomerOrder;// num Customer Ordered
		public int numCustomerNew;// num new Customer
		public int numCustomerIr;// num in plan Customer
		public int numCustomerOr;// num out plan Customer
		public int numCustomerOn;// so customer trong
		public double score;// score
		public int shopId;// shop Id
		public String shop_name;// shop_name
		public String shop_code;// shop_code
		public int nvbhStaffId;// nvbh Staff Id
		public String nvbhStaffCode;// nvbh Staff Code
		public String nvbhStaffName;// nvbh Staff Name
		public int nvbh_trainDetailId;// nvbh trainDetail Id
		public String nvbhShopCode;
		public String nvbhShopName;
		public int nvbhShopId;
		public String nvbhTrainingDate;

		/**
		 * init data
		 * 
		 * @author: TamPQ
		 * @return: void
		 * @throws:
		 */
		public void initFromCursor(Cursor c, TBHVTrainingPlanHistoryAccDTO dto) {
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;

			managerId = CursorUtil.getInt(c, "AREA_MANAGER_ID");
			nvbh_trainDetailId = CursorUtil.getInt(c, "TRAINING_PLAN_DETAIL_ID");
			staffId = CursorUtil.getInt(c, "GSNPP_STAFF_ID");
			staffCode = CursorUtil.getString(c, "GSNPP_STAFF_CODE");
			staffName = CursorUtil.getString(c, "GSNPP_STAFF_NAME");
			nvbhStaffId = CursorUtil.getInt(c, "STAFF_ID");
			nvbhStaffCode = CursorUtil.getString(c, "NVBH_STAFF_CODE");
			nvbhStaffName = CursorUtil.getString(c, "NVBH_STAFF_NAME");
			nvbhShopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
			nvbhShopName = CursorUtil.getString(c, "NVBH_SHOP_NAME");
			nvbhShopId = CursorUtil.getInt(c, "SHOP_ID");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			numCustomerPlan = CursorUtil.getInt(c, "NUM_CUSTOMER_PLAN");
			numCustomerOrder = CursorUtil.getInt(c, "NUM_CUSTOMER_ORDER");
			numCustomerNew = CursorUtil.getInt(c, "NUM_CUSTOMER_NEW");
			numCustomerIr = CursorUtil.getInt(c, "NUM_CUSTOMER_IR");
			numCustomerOr = CursorUtil.getInt(c, "NUM_CUSTOMER_OR");
			numCustomerOn = CursorUtil.getInt(c, "NUM_CUSTOMER_ON");
			try {
				score = CursorUtil.getDouble(c, "SCORE");
			} catch (Exception e) {
				score = 0;
			}

			try {
				date = sfd.format(sfs.parse(CursorUtil.getString(c, "TRAINING_DATE")));
			} catch (ParseException e) {

			}
			dto.listResult.add(this);
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.numCustomerPlan += numCustomerPlan;
			dto.numCustomerOrder += numCustomerOrder;
			dto.numCustomerNew += numCustomerNew;
			dto.numCustomerOn += numCustomerOn;
			dto.numCustomerOr += numCustomerOr;
			dto.score += score;

		}

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param trainedDto
		 * @return
		 * @return: booleanvoid
		 * @throws:
		 */
		public boolean isTrainedByTBHV(TBHVTrainingPlanHistoryAccDTO trainedDto) {
			for (int i = 0; i < trainedDto.listResult.size(); i++) {
				if (date.equals(trainedDto.listResult.get(i).date)
						&& managerId == GlobalInfo.getInstance().getProfile().getUserData().getInheritId()) {
					return true;
				}
			}
			return false;
		}

		public boolean isTrainedTodayByTbvh(TBHVTrainingPlanHistoryAccDTO trainedDto) {
			boolean isTrainedTodayByTbvh = false;
			if (isTrainedByTBHV(trainedDto)) {
				isTrainedTodayByTbvh = date.equals(DateUtils.getCurrentDate());
			}
			return isTrainedTodayByTbvh;
		}
	}
}
