package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * thong tin MH chi tiet bao cao doanh so khach hang
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class ReportStaffSaleDetailViewDTO {
	public ArrayList<ReportStaffSaleDetailItemDTO> listReport;

	public ReportStaffSaleDetailViewDTO() {
		listReport = new ArrayList<ReportStaffSaleDetailItemDTO>();
	}
}
