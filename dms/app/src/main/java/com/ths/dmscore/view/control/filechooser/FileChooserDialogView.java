/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ths.dmscore.dto.view.filechooser.FileEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.util.FileUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

public class FileChooserDialogView extends LinearLayout implements FileEvent, OnClickListener {
    private Button btCancel;
    private Button btOk;
    private TextView tvCurrentDir;
    private ListView lvFile;
    private File currentPath;
    private List<FileInfo> fileList;
    private FileAdapter fileAdapter;
    private ImageButton btUp;
    OnEventControlListener listener;
    private List<FileInfo> lstChooserFile;
    int action;
	private FileChooserDialogView(GlobalBaseActivity context, OnEventControlListener listener, int action) {
		super(context);
		this.listener = listener;
		this.action = action;
		LayoutInflater inflater = context.getLayoutInflater();
		inflater.inflate(R.layout.view_file_chooser, this, true);
		lstChooserFile = new ArrayList<FileInfo>();
		initView();
        initData();
	}
	
	private void initData() {
        //load list file
        fileList = new ArrayList<FileInfo>();
        fileAdapter = new FileAdapter(getContext(), fileList, this);
        lvFile.setAdapter(fileAdapter);
    }
	
	public void renderFiles(List<FileInfo> lstFileCurrent){
		lstChooserFile.clear();
		if (lstFileCurrent != null && !lstFileCurrent.isEmpty()) {
			lstChooserFile.addAll(lstFileCurrent);
		}

		File root = null;
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			root = Environment.getExternalStorageDirectory();
		} else{
			root = Environment.getRootDirectory();
		}
		loadFiles(root);
	}

    private void initView() {
        lvFile = (ListView) this.findViewById(R.id.lvFile);
        tvCurrentDir = (TextView) this.findViewById(R.id.tvCurrentDir);
        btOk = (Button) this.findViewById(R.id.btOk);
        btCancel = (Button) this.findViewById(R.id.btCancel);
        btUp = (ImageButton) this.findViewById(R.id.btUp);
        btUp.setOnClickListener(this);
        btOk.setOnClickListener(this);
        btCancel.setOnClickListener(this);
    }
	
	private void loadFiles(File path) {
        fileList.clear();
        tvCurrentDir.setText("");
        this.currentPath = path;
        if (path.exists()) {
            File[] dirs = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (!file.isDirectory() && file.canRead()) {
                    	boolean isPassUploadWhiteList = FileUtil.isValidateUploadAttach(file);
                        return isPassUploadWhiteList;
                    } else {
                        return false;
                    }
                }
            });

            // convert to an array
            if (path.getParentFile() == null) {
                btUp.setEnabled(false);
            } else {
                btUp.setEnabled(true);
            }

            if (dirs != null) {
                Arrays.sort(dirs);
                for (File dir : dirs) {
                    fileList.add(new FileInfo(dir, FileInfo.TYPE_FOLDER));
                }
            }
            if (files != null) {
                Arrays.sort(files);
                for (File file : files) {
                    FileInfo fileInfo = new FileInfo(file, FileInfo.TYPE_FILE);
                    
                    //copy selected
                    FileInfo infoCurrent = null;
                	for (FileInfo fileInfoChoose : lstChooserFile) {
            			if (fileInfoChoose.equals(fileInfo)) {
            				infoCurrent = fileInfoChoose;
            				break;
            			}
            		}
                	
                	if (infoCurrent != null) {
                		//fileInfo.checked = infoCurrent.checked; 
                		fileInfo.checked = true; 
					}
                	
                	//add
					fileList.add(fileInfo);
                }
            }

            // refresh the user interface
            tvCurrentDir.setText(currentPath.getPath());
        }
        fileAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFileChoose(FileInfo infoChooser) {
    	MyLog.d("onCheckedChanged", infoChooser.fileName + " " + infoChooser.checked);
    	FileInfo infoCurrent = null;
    	for (FileInfo fileInfo : lstChooserFile) {
			if (fileInfo.equals(infoChooser)) {
				infoCurrent = fileInfo;
				break;
			}
		}
    	
    	if (infoCurrent == null) {
			if (infoChooser.checked) {
				lstChooserFile.add(infoChooser);
			}
		} else{
			if (!infoChooser.checked) {
				lstChooserFile.remove(infoCurrent);
			}
		}
    }

    @Override
    public void onFileNameSelected(FileInfo info) {
    	MyLog.d("onFolderOpen", info.fileName);
        if (info.type == FileInfo.TYPE_FOLDER) {
            loadFiles(info.file);
        } else if (info.type == FileInfo.TYPE_FILE) {
            info.checked = !info.checked;
            fileAdapter.notifyDataSetChanged();
            onFileChoose(info);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btUp) {
            loadFiles(currentPath.getParentFile());
        } else if(view == btOk){
        	if (dialogChooserFile != null) {
        		dialogChooserFile.dismiss();
			}
        	listener.onEvent(action, this, lstChooserFile);
        } else if(view == btCancel){
        	if (dialogChooserFile != null) {
        		dialogChooserFile.dismiss();
			}
        }
    }
    
    //single ton dialog
    static volatile Dialog dialogChooserFile = null;
    public static void showFileChooserDialog(GlobalBaseActivity activity, List<FileInfo> lstFileInfo, OnEventControlListener listener, int action){
    	if (dialogChooserFile != null && dialogChooserFile.isShowing()) {
    		dialogChooserFile.dismiss();
		}
    	
    	if (activity != null) {
    		Builder build = new AlertDialog.Builder(activity, R.style.CustomDialogTheme);
    		FileChooserDialogView fileChooserView = new FileChooserDialogView(activity, listener, action);
    		build.setView(fileChooserView);
    		dialogChooserFile = build.create();
    		Window window = dialogChooserFile.getWindow();
    		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
    		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    		window.setGravity(Gravity.CENTER);
    		fileChooserView.renderFiles(lstFileInfo);
			activity.showDialog(dialogChooserFile);
		}
    }
}
