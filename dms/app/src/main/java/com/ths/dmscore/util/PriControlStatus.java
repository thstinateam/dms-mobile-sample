package com.ths.dmscore.util;

import java.io.Serializable;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class PriControlStatus implements Serializable {
	private static final long serialVersionUID = -3828410456623028730L;
	public String controlCode;
	public int status;
	// co cho invisible view hay khong
	public boolean allowInvisible;
	// co cho mustGone view hay khong
	public boolean mustGone;
	
	public PriControlStatus(){
		// mac dinh true
		allowInvisible = true;
		mustGone = false;
	}
}
