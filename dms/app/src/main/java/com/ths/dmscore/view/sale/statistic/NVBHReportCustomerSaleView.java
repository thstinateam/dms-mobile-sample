/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.view.CustomerSaleOfNVBHDTO;
import com.ths.dmscore.dto.view.NVBHReportCustomerSaleViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

public class NVBHReportCustomerSaleView extends BaseFragment implements DMSColSortManager.OnSortChange, VinamilkTableListener {
	
	private DMSTableView tbReport;
	// flag check load data the first
	public boolean isDoneLoadFirst = false;
	// data for screen
	public NVBHReportCustomerSaleViewDTO screenData = new NVBHReportCustomerSaleViewDTO();
	String staffId = "";
	String shopId = "";
	private int currentPage = 1;
	private VNMEditTextClearable edCustomer;
	private Button btSearch;
	private String searchInfo = "";

	public static NVBHReportCustomerSaleView getInstance(Bundle data) {
		NVBHReportCustomerSaleView instance = new NVBHReportCustomerSaleView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null && bundle.containsKey(IntentConstants.INTENT_STAFF_ID)) {
			staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		} else {
			staffId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
		}
		if (bundle != null && bundle.containsKey(IntentConstants.INTENT_SHOP_ID)) {
			shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		} else {
			shopId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId());
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_report_customer_sale, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		//PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BAOCAO_KH_DOANH_SO);
		initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_REPORT_CUSTOMER_SALE));
		if (!this.isDoneLoadFirst) {
			this.getReport(true);
		}
		return v;
	}

	private void initView(View v) {
		edCustomer = (VNMEditTextClearable) v.findViewById(R.id.edCustomer);
		btSearch = (Button) v.findViewById(R.id.btSearch);
		tbReport = (DMSTableView) v.findViewById(R.id.tbProductList);
		tbReport.setListener(this);
		btSearch.setOnClickListener(this);
	}

	@Override
	public void onResume() {
		if (this.isDoneLoadFirst) {
			renderLayout();
		}
		super.onResume();
	}

	public void getReport(boolean isGetTotal) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID, staffId);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putInt(IntentConstants.INTENT_PAGE, currentPage);
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotal);
		data.putString(IntentConstants.INTENT_SEARCH, searchInfo);
		
		// add sort info
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbReport.getSortInfo());
		handleViewEvent(data, ActionEventConstant.NVBH_GET_REPORT_CUSTOMER_SALE, SaleController.getInstance());
	}

	/**
	 *
	 * render layout for screen
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void renderLayout() {
		if (isChangeSysConfig()) {
			tbReport.clearAllDataAndHeader();
		} else{
			tbReport.clearAllData();
		}
		
		if (!tbReport.isHeaderExists()) {
			initHeader();
		}
		
		tbReport.setTotalSize(this.screenData.totalData, currentPage);
		tbReport.getPagingControl().setCurrentPage(currentPage);
		
		if (screenData != null) {
			// render table
			if (this.screenData.listDto != null
					&& !this.screenData.listDto.isEmpty()) {
				int pos = 1 + Constants.NUM_ITEM_PER_PAGE * (currentPage - 1);
				// add list row nomal
				for (int i = 0, size = this.screenData.listDto.size(); i < size; i++) {
					CustomerSaleOfNVBHDTO dto = this.screenData.listDto.get(i);
					NVBHReportCustomerSaleRow row = new NVBHReportCustomerSaleRow(parent, this);
					row.setClickable(true);
					row.renderLayout(dto, pos);
					tbReport.addRow(row);
					pos ++;
				}
			} else {
				// if null
				tbReport.showNoContentRow();
			}
		} else {
			tbReport.showNoContentRow();
		}
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				searchInfo = "";
				edCustomer.setText("");
				resetInfoGetData();
				this.getReport(true);
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	private void resetInfoGetData() {
		isDoneLoadFirst = false;
		currentPage = 1;
		tbReport.resetSortInfo();
	}

	/**
	 *
	 * init header for menu
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
		case ActionEventConstant.NVBH_GET_REPORT_CUSTOMER_SALE:
			NVBHReportCustomerSaleViewDTO screenDataTemp = (NVBHReportCustomerSaleViewDTO) modelEvent.getModelData();
			if (this.screenData == null) {
				this.screenData = screenDataTemp;
			} else{
				this.screenData.listDto = screenDataTemp.listDto;
				if(screenDataTemp.totalData >= 0){
					this.screenData.totalData = screenDataTemp.totalData;
				}
			}
			this.renderLayout();
			isDoneLoadFirst = true;
			parent.closeProgressDialog();
			break;

		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	/**
	 * Khoi tao header
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void initHeader() {
		//init header with sort
		DMSListSortInfoBuilder builder = new DMSListSortInfoBuilder()
    		.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
    		.addInfoCaseUnSensitive(3, SortActionConstants.NAME);
		
		if (GlobalInfo.getInstance().isSysShowPrice()) {
			builder = builder.addInfo(4, SortActionConstants.AMOUNT_APPROVED_PRE)
			.addInfo(5, SortActionConstants.AMOUNT_APPROVED)
			.addInfo(6, SortActionConstants.AMOUNT)
			.addInfo(7, SortActionConstants.QUANITY_APPROVED_PRE)
			.addInfo(8, SortActionConstants.QUANITY_APPROVED)
			.addInfo(9, SortActionConstants.QUANITY);
		} else{
			builder = builder.addInfo(4, SortActionConstants.QUANITY_APPROVED_PRE)
					.addInfo(5, SortActionConstants.QUANITY_APPROVED)
					.addInfo(6, SortActionConstants.QUANITY);
		}
		SparseArray<DMSColSortInfo> lstSort = builder.build();
		initHeaderTable(tbReport, new NVBHReportCustomerSaleRow(parent, this), lstSort, this);
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getReport(false);
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbReport) {
			currentPage = tbReport.getPagingControl().getCurrentPage();
			getReport(false);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
		if (action == ActionEventConstant.GO_TO_CUSTOMER_INFO) {
			if (data != null && data instanceof CustomerSaleOfNVBHDTO) {
				CustomerSaleOfNVBHDTO dtoRow = (CustomerSaleOfNVBHDTO)data;
				gotoCustomerInfo(String.valueOf(dtoRow.customerId));
			}
		}
	}
	
	@Override
	public void onClick(View v) {
		if (v == btSearch) {
			searchInfo  = edCustomer.getText().toString();
			resetInfoGetData();
			getReport(true);
		} else{
			super.onClick(v);
		}
	}
	
	public void gotoCustomerInfo(String customerId) {
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleSwitchFragment(bunde,ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}
}
