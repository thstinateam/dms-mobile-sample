/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.TableIdDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.ServerLogger;

/**
 * Luu max id cua cac table
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class TABLE_ID extends ABSTRACT_TABLE {
	// id bang
	public static final String ID = "ID";
	// ten table
	public static final String TABLE_NAME = "TABLE_NAME";
	// max id
	public static final String MAX_ID = "MAX_ID";
	// factor -- bo sung khi tao id moi
	public static final String FACTOR = "FACTOR";
	// last syn
	public static final String LAST_SYN_MAX_ID = "LAST_SYN_MAX_ID";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// staffId
	public static final String STAFF_ID = "STAFF_ID";

	public static final String TABLE_ID_NAME = "TABLE_ID";

	public TABLE_ID(SQLiteDatabase mDB) {
		this.tableName = TABLE_ID_NAME;
		this.columns = new String[] { ID, TABLE_NAME, MAX_ID, FACTOR, LAST_SYN_MAX_ID, SHOP_ID, STAFF_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((TableIdDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(TableIdDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		TableIdDTO tableId = (TableIdDTO) dto;
		ContentValues value = initDataRow(tableId);
		String[] params = { "" + tableId.id };
		return update(value, ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		TableIdDTO cusDTO = (TableIdDTO) dto;
		String[] params = { String.valueOf(cusDTO.id) };
		return delete(ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public TableIdDTO getRowById(String id) {
		TableIdDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initLogDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
			// c = null;
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Lay max_id trong table
	 *
	 * @author: TruongHN
	 * @param tableName
	 * @return: long
	 * @throws:
	 */
	public long getMaxIdTime(String tableName) {
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		long maxId = getMaxIdFromTable(tableName, staffId);
		maxId += 1;
		return maxId;
	}

	private long getMaxIdFromTable(String tableName, long objectId) {
		long res = 0;
		
		long deviceId = GlobalInfo.getInstance().getDeviceId();
		if (deviceId <= 0) {
			deviceId = objectId;
		}
		
		//time stamp by second
		long timeStamp = System.currentTimeMillis() / 10;
		res = deviceId * (long)Math.pow(10, String.valueOf(timeStamp).length()) + timeStamp;
		
		if(deviceId <= 0) {
			ServerLogger.sendLog("getMaxIdFromTable fail", tableName + " deviceId: " + deviceId 
					+ " inheritId: " + objectId, TabletActionLogDTO.LOG_CLIENT);
			throw new IllegalArgumentException("get max id fail");
		}
		return res;
	}

	/**
	 * Lay so fator default
	 *
	 * @author: TruongHN
	 * @return: String
	 * @throws:
	 */
	public long getFactorValue() {
		long res = 1;
		Vector<TableIdDTO> v = getAllRow();
		if (v.size() > 0) {
			res = v.get(0).factor;
		}

		return res;
	}

	/**
	 * Cap nhat maxId
	 *
	 * @author: TruongHN
	 * @param tableName
	 * @param maxId
	 * @return
	 * @return: void
	 * @throws:
	 */
	public int updateMaxId(String tableName, long maxId) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(MAX_ID, maxId);

		String[] params = { tableName };
		return update(editedValues, TABLE_NAME + " like ?", params);
	}

	private TableIdDTO initLogDTOFromCursor(Cursor c) {
		TableIdDTO dto = new TableIdDTO();
		dto.id = (CursorUtil.getInt(c, ID));
		dto.maxId = (CursorUtil.getLong(c, MAX_ID));
		dto.factor = (CursorUtil.getLong(c, FACTOR));
		dto.tableName = (CursorUtil.getString(c, TABLE_NAME));
		dto.lastSynMaxId = (CursorUtil.getLong(c, LAST_SYN_MAX_ID));
		dto.shopId = (CursorUtil.getInt(c, SHOP_ID));
		dto.staffId = (CursorUtil.getInt(c, STAFF_ID));
		dto.synState = (CursorUtil.getInt(c, SYN_STATE));

		return dto;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return: Vector<TableIdDTO>
	 * @throws:
	 */
	public Vector<TableIdDTO> getAllRow() {
		Vector<TableIdDTO> v = new Vector<TableIdDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				TableIdDTO tableDto;
				if (c.moveToFirst()) {
					do {
						tableDto = initLogDTOFromCursor(c);
						v.addElement(tableDto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return v;

	}

	private ContentValues initDataRow(TableIdDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(ID, dto.id);
		editedValues.put(TABLE_NAME, dto.tableName);
		editedValues.put(MAX_ID, dto.maxId);
		editedValues.put(LAST_SYN_MAX_ID, dto.lastSynMaxId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SYN_STATE, dto.synState);
		editedValues.put(FACTOR, dto.factor);

		return editedValues;
	}
}
