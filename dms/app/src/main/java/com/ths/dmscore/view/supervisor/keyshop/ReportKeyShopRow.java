/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.ReportKeyshopItemDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * ReportKeyShopRow.java
 * @author: yennth16
 * @version: 1.0
 * @since:  15:21:07 15-07-2015
 */
public class ReportKeyShopRow extends DMSTableRow implements OnClickListener {

	// nhan vien
	TextView tvStaff;
	// cau lac bo
	TextView tvProgram;
	// so khach hang dang ky
	TextView tvCustomeRegister;
	// so kh dat
	TextView tvCustomerPass;
	// ds dang ky
	TextView tvAmountRegister;
	// ds thuc hien
	TextView tvAmountDone;
	// %
	TextView tvPercent;
	// sl dang ky
	TextView tvQuantityRegister;
	// sl thuc hien
	TextView tvQuantityDone;
	ReportKeyshopItemDTO dto;
	ReportKeyshopItemDTO dtoTotal;

	// khoi tao control
	public ReportKeyShopRow(Context context) {
		super(context, R.layout.layout_report_key_shop_row, GlobalInfo.getInstance()
				.isSysShowPrice() ? null : new int[] { R.id.tvAmountRegister, R.id.tvAmountDone });
		setOnClickListener(this);
		tvStaff = (TextView) findViewById(R.id.tvStaff);
		tvStaff.setOnClickListener(this);
		tvProgram = (TextView) findViewById(R.id.tvProgram);
		tvCustomeRegister = (TextView) findViewById(R.id.tvCustomeRegister);
		tvCustomerPass = (TextView) findViewById(R.id.tvCustomerPass);
		tvPercent = (TextView) findViewById(R.id.tvPercent);
		tvAmountRegister = (TextView) findViewById(R.id.tvAmountRegister);
		tvAmountDone = (TextView) findViewById(R.id.tvAmountDone);
		tvQuantityRegister = (TextView) findViewById(R.id.tvQuantityRegister);
		tvQuantityDone = (TextView) findViewById(R.id.tvQuantityDone);
	}

	/**
	 * renderLayout
	 * @author: yennth16
	 * @since: 09:21:33 22-07-2015
	 * @return: void
	 * @throws:
	 * @param position
	 * @param item
	 */
	public void renderLayout(int position, ReportKeyshopItemDTO item) {
		this.dto = item;
		if(!StringUtil.isNullOrEmpty(item.staff.staffCode)
				&& !StringUtil.isNullOrEmpty(item.staff.name)){
			tvStaff.setText(item.staff.staffCode +" - "+item.staff.name);
		}else{
			tvStaff.setText(item.staff.staffCode);
		}
		tvProgram.setText(item.ksShop.name);
		if(GlobalInfo.getInstance().isSysShowPrice()){
			display(tvAmountRegister, item.amountRegister);
			display(tvAmountDone, item.amountDone);
		}
		display(tvQuantityRegister, item.quantityRegister);
		display(tvQuantityDone, item.quantityDone);
		tvCustomeRegister.setText(Constants.STR_BLANK + item.countCustomerRegister);
		tvCustomerPass.setText(Constants.STR_BLANK + item.countDone);
		displayPercent(tvPercent, item.percent);
	}

	/**
	 *
	 * @author: yennth16
	 * @since: 15:33:06 22-07-2015
	 * @return: void
	 * @throws:
	 * @param position
	 * @param item
	 */
	public void renderLayoutTotal(ReportKeyshopItemDTO item) {
		this.dtoTotal = item;
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvStaff,tvProgram);
		if(GlobalInfo.getInstance().isSysShowPrice()){
			display(tvAmountRegister, item.amountRegister);
			display(tvAmountDone, item.amountDone);
		}
		display(tvQuantityRegister, item.quantityRegister);
		display(tvQuantityDone, item.quantityDone);
		tvCustomeRegister.setText(Constants.STR_BLANK + item.countCustomerRegister);
		tvCustomerPass.setText(Constants.STR_BLANK + item.countDone);
		displayPercent(tvPercent, item.percent);
	}


	@Override
	public void onClick(View arg0) {
		if (arg0 == tvStaff && listener != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH, arg0, dto);
		}
	}
}
