/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin diem cua khach hang ma NVBH da ban trong ngay
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerMathOfStaffDTO {
	// customer id
	public long customer_id;
	// doanh so da dat trong ngay cua khach hang
	public long amount;
	// doanh so chi tieu trong ngay cua khach hang
	public long planAmount;
	// tien do thuc hien
	public double percent;
	// check use in plan or out of plan (0 : ngoai tuyen, 1: trong tuyen)
	public int isVisitPlan;

	public CustomerMathOfStaffDTO() {
		customer_id = 0;
		amount = 0;
		planAmount = 0;
		percent = 0;
		isVisitPlan = 0;
	}

	/**
	 * 
	*  lay thong tin cua khach hang va diem cua khach hang trong ngay
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void initDataFromCursor(Cursor c){
		customer_id = CursorUtil.getLong(c, "CUSTOMER_ID");
		amount = CursorUtil.getLong(c, "AMOUNT");
		planAmount = CursorUtil.getLong(c, "PLAN_AMOUNT");
		percent = CursorUtil.getDouble(c, "PERCENT");
		isVisitPlan = CursorUtil.getInt(c, "IS_VISIT_PLAN");
	}
}
