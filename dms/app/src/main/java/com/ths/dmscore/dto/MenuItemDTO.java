/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.util.ArrayList;

/**
 * Menuitem
 * @author: BangHN
 */
public class MenuItemDTO{
	String menuText;
	int menuIcon;
	public boolean isSelected = false;
	public String controlOrdinal;
	public int menuIndex = 0;
	
	public MenuItemDTO(){
	}

	public MenuItemDTO(String text, int icon){
		menuText = text;
		menuIcon = icon;
	}
	
	public void setTextMenu(String text){
		menuText = text;
	}
	
	public void setIconMenu(int icon){
		menuIcon = icon;
	}
	
	public String getTextMenu(){
		return menuText;
	}
	
	public int getIconMenu(){
		return menuIcon;
	}
	
	public void setSelected(boolean selected){
		isSelected = selected;
	}
	
	public boolean isSelected(){
		return isSelected;
	}
	
	// [item] co phai item child
	public boolean isChild;
	// [item] id item
	public int id = -1;
	// [item group] co phai dang o trang thai expand
	public boolean isExpand;
	// [item] item refresh
	public boolean isRefresh;
	
	private ArrayList<MenuItemDTO> Items;

	public ArrayList<MenuItemDTO> getItems() {
		return Items;
	}

	public void setItems(ArrayList<MenuItemDTO> Items) {
		this.Items = Items;
	}

	public boolean isChild() {
		return isChild;
	}

	public void setChild(boolean isChild) {
		this.isChild = isChild;
	}
	
	public boolean isExpand() {
		return isExpand;
	}

	public void setExpand(boolean isExpand) {
		this.isExpand = isExpand;
	}

	public boolean isRefresh() {
		return isRefresh;
	}

	public void setRefresh(boolean isRefresh) {
		this.isRefresh = isRefresh;
	}
	
	public void createSubMenu() {
		isChild = true;
		Items = null;
	}
}
