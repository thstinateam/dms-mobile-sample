package com.ths.dmscore.view.supervisor.customer;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.VinamilkTableView;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.SupervisorCustomerListDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.MenuItem;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * SupervisorCustomerList
 * 
 * @author : TRUNGHQM since : 26/04/2014 version :
 */
public class SupervisorCustomerList extends BaseFragment implements
		VinamilkTableListener {

	private static final int ACTION_ACC = 1;
	private static final int ACTION_PLAN = 2;
	private static final int ACTION_CUSTOMER_LIST = 3;

	private GlobalBaseActivity parent;
	private VinamilkTableView tbCusList;// tbCusList
	private VNMEditTextClearable edMKH;// edMKH
	private VNMEditTextClearable edTKH;// edTKH
	private Button btSearch; // btSearch
	private Button btReInPut; // btReInPut
	private int currentPage = -1;
	private boolean isFirstLoad = true;
	public SupervisorCustomerListDTO cusDto;
	private String textCusCode = "";
	private String textCusName = "";

	boolean isSearchOrSelectSpiner = false;
	private boolean isUpdateData = false;
	public boolean isBackFromPopStack = false;
	private boolean isBack = false;

	public static SupervisorCustomerList newInstance(Bundle data) {
		SupervisorCustomerList f = new SupervisorCustomerList();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		parent = (GlobalBaseActivity) getActivity();
	}

	@Override
	public void onResume() {
		super.onResume();
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(
				PriForm.GSNPP_HUANLUYEN_DANHSACHKH,
				new MenuTab(StringUtil.getString(R.string.TEXT_MENU_CUS_LIST), R.drawable.menu_customer_icon,
						ACTION_CUSTOMER_LIST, PriForm.GSNPP_HUANLUYEN_DANHSACHKH),
				new MenuTab((StringUtil
						.getString(R.string.TEXT_HEADER_MENU_REPORT_MONTH)),
						R.drawable.icon_accumulated, ACTION_ACC,
						PriForm.GSNPP_HUANLUYEN_LUYKE),
				new MenuTab((StringUtil
						.getString(R.string.TEXT_HEADER_TABLE_PLAN)),
						R.drawable.icon_calendar, ACTION_PLAN,
						PriForm.GSNPP_HUANLUYEN_KEHOACH));
		
		if (isFirstLoad || isBackFromPopStack) {
			isBackFromPopStack = false;
		} else {
			parent.showLoadingDialog();
			isUpdateData = true;
			resetAllValue();
			getCustomerList(1, 1);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_supervisor_customer_list, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_HUANLUYEN_DANHSACHKH);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER_03_03));
		
		edMKH = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(
				v, R.id.edCusCode,
				PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_MATENKH);
		edTKH = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(
				v, R.id.edCusName,
				PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_MATENKH);

		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(
				v, R.id.btSearch,
				PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_TIMKIEM);
		PriUtils.getInstance().setOnClickListener(btSearch, this);
		
		PriUtils.getInstance().findViewByIdGone(v,
				R.id.tvMKH, PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_MATENKH);
		PriUtils.getInstance().findViewByIdGone(v,
				R.id.tvTen, PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_MATENKH);
		
		btReInPut = (Button) PriUtils.getInstance().findViewByIdGone(
				v, R.id.btReText,
				PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_NHAPLAI);
		PriUtils.getInstance().setOnClickListener(btReInPut, this);
		tbCusList = (VinamilkTableView) view.findViewById(R.id.tbCusList);
		tbCusList.setListener(this);
		layoutTableHeader();

		if (!isBackFromPopStack) {
			if (cusDto != null && currentPage != -1) {
				// mGalleryInitializedCount = -1;
				renderLayout();
			} else {

				getCustomerList(1, 1);

			}
		}
		return v;
	}

	/**
	 * renderLayout
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		if (currentPage != -1) {
			if (isBack) {
				isBack = false;
				tbCusList.setTotalSize(cusDto.totalCustomer);
			}
			tbCusList.getPagingControl().setCurrentPage(currentPage);
		} else {
			tbCusList.setTotalSize(cusDto.totalCustomer);
			currentPage = tbCusList.getPagingControl().getCurrentPage();
		}

		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbCusList.getPagingControl().getCurrentPage() - 1);
		tbCusList.clearAllData();
		for (int i = 0, s = cusDto.cusList.size(); i < s; i++) {
			SupervisorCustomerListRow row = new SupervisorCustomerListRow(
					parent, this);
			row.render(pos, cusDto.cusList.get(i));
			pos++;
			tbCusList.addRow(row);
		}

	}

	private void resetAllValue() {
		textCusName = "";
		textCusCode = "";
		edMKH.setText(textCusCode);
		edTKH.setText(textCusName);
		currentPage = -1;
	}

	/**
	 * getCustomerList
	 * 
	 * @author: TRUNGHQM
	 * @return: void
	 * @throws:
	 */
	private void getCustomerList(int page, int isGetTotalPage) {
		if (!this.isVisible() && !isFirstLoad) {
			return;
		}

		parent.showLoadingDialog();
		ActionEvent e = new ActionEvent();

		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putInt(
				IntentConstants.INTENT_STAFF_ID,
				GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritId());
		bundle.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);

		// check data search
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, textCusCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, textCusName);

		e.viewData = bundle;
		e.action = ActionEventConstant.ACTION_LOAD_LIST_CUSTOMER;
		e.sender = this;
		SupervisorController.getInstance().handleViewEvent(e);
	}

	public void layoutTableHeader() {
		// customer list table
		int[] CUSTOMER_LIST_TABLE_WIDTHS = { 60, 90, 350, 400};
		String[] CUSTOMER_LIST_TABLE_TITLES = { 
				StringUtil.getString(R.string.TEXT_STT), 
				StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE),
				StringUtil.getString(R.string.TEXT_TB_CUSTOMER_NAME), 
				StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS)};
		tbCusList.getHeaderView().addColumns(CUSTOMER_LIST_TABLE_WIDTHS,
				CUSTOMER_LIST_TABLE_TITLES, ImageUtil.getColor(R.color.BLACK),
				ImageUtil.getColor(R.color.TABLE_HEADER_BG));
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		if (control instanceof MenuItem) {
			switch (eventType) {
			case ACTION_ACC: {
				ActionEvent e = new ActionEvent();
				e.action = ActionEventConstant.GSNPP_TRAINING_RESULT_ACC_REPORT;
				e.sender = this;
				SupervisorController.getInstance().handleSwitchFragment(e);
				break;
			}
			case ACTION_PLAN: {
				ActionEvent e = new ActionEvent();
				e.action = ActionEventConstant.GSNPP_TRAINING_PLAN;
				e.sender = this;
				SupervisorController.getInstance().handleSwitchFragment(e);
				break;
			}
			default:
				break;
			}
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.ACTION_LOAD_LIST_CUSTOMER:
			if (isSearchOrSelectSpiner) {
				isSearchOrSelectSpiner = false;
				cusDto = null;
				currentPage = -1;
			}
			SupervisorCustomerListDTO tempDto = (SupervisorCustomerListDTO) modelEvent
					.getModelData();
			if (cusDto == null) {
				cusDto = tempDto;
			} else {
				cusDto.cusList = tempDto.cusList;
			}

			if (isUpdateData) {
				isUpdateData = false;
				currentPage = -1;
				cusDto.totalCustomer = tempDto.totalCustomer;
			}

			if (cusDto != null) {
				renderLayout();
			}
			isFirstLoad = false;
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		isFirstLoad = false;
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		if (control == tbCusList) {
			currentPage = tbCusList.getPagingControl().getCurrentPage();
			getCustomerList(currentPage, 0);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.GO_TO_CUSTOMER_INFO: {
			CustomerListItem item = (CustomerListItem) data;
			currentPage = tbCusList.getPagingControl().getCurrentPage();
			isBack = true;
			gotoCustomerInfo(item.aCustomer.getCustomerId());
			break;
		}
		}
	}

	/**
	 * di toi man hinh chi tiet Khach hang
	 * 
	 * @author : TamPQ since : 10:59:39 AM
	 */
	public void gotoCustomerInfo(String customerId) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bunde.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, true);
		e.viewData = bunde;
		e.action = ActionEventConstant.GO_TO_CUSTOMER_INFO;
		SaleController.getInstance().handleSwitchFragment(e);
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSearch:
			isSearchOrSelectSpiner = true;

			textCusCode = edMKH.getText().toString().trim();
			textCusName = edTKH.getText().toString().trim();

			// hide ban phim
			GlobalUtil.forceHideKeyboard(parent);

			getCustomerList(1, 1);
			break;
		case R.id.btReText:
			edMKH.setText("");
			edTKH.setText("");
			textCusCode = edMKH.getText().toString().trim();
			textCusName = edTKH.getText().toString().trim();
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				parent.showLoadingDialog();
				isUpdateData = true;
				resetAllValue();
				getCustomerList(1, 1);
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}
}
