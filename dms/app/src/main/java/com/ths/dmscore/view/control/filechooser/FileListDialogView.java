/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.ths.dmscore.lib.sqllite.download.DownloadFile;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.filechooser.FileAttachEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.util.FileUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dms.R;

public class FileListDialogView extends LinearLayout implements FileAttachEvent {
	GlobalBaseActivity context;
    private ListView lvFile;
    private List<FileInfo> fileList;
    private FileAttachAdapter fileAdapter;
    OnEventControlListener listener;
	private FileListDialogView(GlobalBaseActivity context) {
		super(context);
		this.context = context;
		LayoutInflater inflater = context.getLayoutInflater();
		inflater.inflate(R.layout.view_file_list, this, true);
		initView();
        initData();
	}
	
	private void initData() {
        //load list file
        fileList = new ArrayList<FileInfo>();
        fileAdapter = new FileAttachAdapter(getContext(), fileList, this, false);
        lvFile.setAdapter(fileAdapter);
    }
	
	public void renderFiles(List<FileInfo> lstFile){
		fileList.clear();
		fileList.addAll(lstFile);
		fileAdapter.notifyDataSetChanged();
	}

    private void initView() {
        lvFile = (ListView) this.findViewById(R.id.lvFile);
    }

	@Override
	public void onRemoveAttach(int posRemove) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onOpenFile(FileInfo info) {
		new DownloadFileAttachTask(info, context).execute();
	}

	//single ton dialog
	static volatile Dialog dialogChooserFile = null;
    public static void showFileListDialog(GlobalBaseActivity activity, List<FileInfo> lstFileInfo){
    	if (dialogChooserFile != null && dialogChooserFile.isShowing()) {
    		dialogChooserFile.dismiss();
		}
    	
    	if (activity != null) {
    		Builder build = new AlertDialog.Builder(activity, R.style.CustomDialogTheme);
    		FileListDialogView fileListView = new FileListDialogView(activity);
    		build.setView(fileListView);
    		dialogChooserFile = build.create();
    		Window window = dialogChooserFile.getWindow();
    		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
    		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    		window.setGravity(Gravity.CENTER);
    		fileListView.renderFiles(lstFileInfo);
			activity.showDialog(dialogChooserFile);
		}
    }
    
    public static class DownloadFileAttachTask extends AsyncTask<Void, Void, Exception> {

    	File fileDownload = null;
    	FileInfo fileInfo;
		GlobalBaseActivity activity;
    	public DownloadFileAttachTask(FileInfo file, GlobalBaseActivity activity) {
    		this.fileInfo = file;
    		this.activity = activity;
		}
    	
		@Override
		protected void onPreExecute() {
			String note = StringUtil.getString(R.string.TEXT_DOWNLOAD_ATTACH_FILE, fileInfo.fileName);
			this.activity.showProgressPercentDialog(note, false);
		}

		@SuppressLint("DefaultLocale")
		@Override
		protected Exception doInBackground(Void... params) {
			try {
				if (fileInfo.file != null && fileInfo.file.exists()) {
					fileDownload = fileInfo.file;
				} else{
					String fileNameHash = new Md5FileNameGenerator().generate(fileInfo.fullPath);
					File tempFolder = ExternalStorage.getAttachFileCacheFolder();
					fileDownload = new File(tempFolder + "/" + fileNameHash + "." + fileInfo.extension.toLowerCase());
					if (!fileDownload.exists()) {
						DownloadFile.downloadWithURLConnection(fileInfo.fullPath, fileDownload, tempFolder);
					}
				}
			} catch (Exception e) {
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			this.activity.closeProgressDialog();
			if (result != null) {
				String errorInfo = StringUtil.getString(R.string.TEXT_ERROR_DOWNLOAD_ATTACH_FILE);
				if (result instanceof DownloadFile.DMSDownloadException && !StringUtil.isNullOrEmpty(result.getMessage())) {
					errorInfo += "\n" + result.getMessage();
				}
				this.activity.showDialog(errorInfo);
				MyLog.e("downloadWithURLConnection", "fail", result);
				ServerLogger.sendLog("DownloadFileAttachTask ", MyLog.printStackTrace(result), false, TabletActionLogDTO.LOG_EXCEPTION);
			} else {
				this.activity.updateProgressEndPercentDialog();
				FileUtil.viewFile(this.activity, fileDownload);
			}
		}
	}
}
