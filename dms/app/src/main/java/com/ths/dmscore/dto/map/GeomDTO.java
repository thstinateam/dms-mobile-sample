package com.ths.dmscore.dto.map;

import java.io.Serializable;

import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;


//public class GeomDTO  implements Serializable{
//	public LatLng dataGeom = new LatLng();
//	public boolean isFromDB = false;
//	public void parseFromJson(String data){
//		JSONObject jsonObject;
//		try {
//			jsonObject = new JSONObject(data);			
//			dataGeom.lng = jsonObject.getDouble("lng");
//			dataGeom.lat = jsonObject.getDouble("lat");
//			
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//	}
//	/**
//	 * 
//	 * parse tu Object co san
//	 * @author : DoanDM
//	 * since : 4:54:03 PM
//	 */
//	public void parseFromJson(JSONObject jsonObject){
//		try {
//			dataGeom.lng = jsonObject.getDouble("lng");
//			dataGeom.lat = jsonObject.getDouble("lat");			
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}		
//	}
//}


public class GeomDTO  implements Serializable{
	private static final long serialVersionUID = 4574808566819729198L;
	public double lng;//toa do long
	public double lat;//toa do lat
	public String createDate;
	public int type;
	public double accuracy;
	public void parseFromJson(String data){
		JSONObject jsonObject;
		try {
			jsonObject = new JSONObject(data);			
			lng = jsonObject.getDouble("lng");
			lat = jsonObject.getDouble("lat");			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}		
	}
}