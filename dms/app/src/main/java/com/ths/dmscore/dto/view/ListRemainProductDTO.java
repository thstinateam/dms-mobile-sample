package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;


@SuppressWarnings("serial")
public class ListRemainProductDTO implements Serializable{
	public int total;
	public ArrayList<RemainProductViewDTO> listDTO = new ArrayList<RemainProductViewDTO>();
	
	public ListRemainProductDTO(){
		
	}
}
