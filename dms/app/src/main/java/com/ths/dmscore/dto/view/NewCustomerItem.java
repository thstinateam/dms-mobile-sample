/*
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dms.R;

/**
 * Them Khach Hang item NewCustomerItem.java
 * 
 * @author: duongdt3
 * @version: 1.0
 * @since: 11:14:05 3 Jan 2014
 */
public class NewCustomerItem implements Serializable {

	private static final long serialVersionUID = 1L;

	public long customerId;
	public String tvCustomerName;
	public String tvAddress;
	public String tvPhone;
	public String tvStatus;
	public boolean isEdit;
	public int trangThai;
	public final String[] ARRAY_CUSTOMER_STATE = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_NOT_YET_SEND),
			StringUtil.getString(R.string.TEXT_NOT_YET_APPROVE),
			StringUtil.getString(R.string.TEXT_REFUSE),
			StringUtil.getString(R.string.TEXT_ERROR) };

	public NewCustomerItem() {
		tvCustomerName = "";
		tvAddress = "";
		tvPhone = "";
		tvStatus = "";
		isEdit = false;
		
	}

	public static final int STATE_CUSTOMER_WAIT_APPROVED = 1;// cho duyet
	public static final int STATE_CUSTOMER_NOT_SEND = 2;//chua gui
	public static final int STATE_CUSTOMER_ERROR = 3;// loi
	public static final int STATE_CUSTOMER_REFUSE = 4;// tu choi

	public void initFromCursor(Cursor c) {
		customerId = CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID);
		tvCustomerName = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
		tvAddress = CursorUtil.getString(c, CUSTOMER_TABLE.ADDRESS);
		tvPhone = CursorUtil.getString(c, CUSTOMER_TABLE.PHONE);
		if (StringUtil.isNullOrEmpty(tvPhone)) {
			tvPhone = CursorUtil.getString(c, CUSTOMER_TABLE.MOBIPHONE);
		} else {
			tvPhone = tvPhone
					+ Constants.STR_CROSS
					+ CursorUtil.getString(c, CUSTOMER_TABLE.MOBIPHONE);
		}
		trangThai = CursorUtil.getInt(c, "STATE");

		// 0 -> tat ca, lay nhung trang thai > 1
		if (trangThai > 0
				&& trangThai < new NewCustomerItem().ARRAY_CUSTOMER_STATE.length) {
			int index = 0;
			if (trangThai == 1) {
				index = 2;
			}else if (trangThai == 2) {
				index = 1;
			}else if (trangThai == 3) {
				index = 4;
			}else if (trangThai == 4) {
				index = 3;
			}
			tvStatus = new NewCustomerItem().ARRAY_CUSTOMER_STATE[index];
		}

		// neu nam trong 3 trang thai duoc phep edit
		if (trangThai == STATE_CUSTOMER_NOT_SEND
				|| trangThai == STATE_CUSTOMER_REFUSE
				|| trangThai == STATE_CUSTOMER_ERROR) {
			isEdit = true;
		} else {
			isEdit = false;
		}
	}

}
