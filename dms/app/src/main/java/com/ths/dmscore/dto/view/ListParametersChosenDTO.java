/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.constants.Constants;

/**
 * ListParametersChosenDTO.java
 * @author: dungdq3
 * @version: 1.0 
 * @since:  4:51:15 PM Oct 27, 2014
 */
public class ListParametersChosenDTO
		implements Serializable {

	private static final long serialVersionUID = 1L;
	public int reportTemplateObjectID;
	public int objectID;
	// co the la customercode, shopcode, staffcode ...
	public String objectName;
	public String value;
	// loai tuong ung trong bang REPORT_TEMPLATE_CRITERION
	public int typeID;
	// bien kiem tra ds nay dc chon hay ko de load len dong hoac cot chon tham so tuong ung
	public boolean isChoose;
	// trang thai
	public int status;
	// ten tuong ung: customername hoac shop_name, staff_name
	public String infoName;
	
	public ListParametersChosenDTO() {
		// TODO Auto-generated constructor stub
		objectID = 0;
		objectName = Constants.STR_BLANK;
		value = Constants.STR_BLANK;
		infoName = Constants.STR_BLANK;
		typeID = -1;
		isChoose = true;
		status = 0;
	}
	
	@Override
	public Object clone()  {
		ListParametersChosenDTO p = new ListParametersChosenDTO();
		p.objectID = this.objectID;
		p.isChoose = this.isChoose;
		p.objectName = this.objectName;
		p.value = this.value;
		p.typeID = this.typeID;
		p.status = this.status;
		p.infoName = this.infoName;
		return p;
	}

	/**
	 * Lay thong tin mot row hien tai co id = voi mang da chon
	 * 
	 * @author: Tuanlt11
	 * @param objectId
	 * @param shopId
	 * @param staffId
	 * @param lstObjectRowChose
	 * @param typeRow
	 * @return
	 * @return: ListParametersChosenDTO
	 * @throws:
	 */
	public static ListParametersChosenDTO getDataRow(int objectId, int staffId,
			int shopId, ArrayList<ListParametersChosenDTO> lstObjectRowChose) {
		ListParametersChosenDTO item = new ListParametersChosenDTO();
		for (int i = 0, sizeRow = lstObjectRowChose.size(); i < sizeRow; i++) {
			if (lstObjectRowChose.get(i).objectID == objectId) {
				item = lstObjectRowChose.get(i);
				break;
			}
		}
		return item;
	}

}
