package com.ths.dmscore.view.listener;

import android.view.View;

public interface OnActionMenuBarListener {
	void onClickMenuBarItem(int eventType, View control, Object data);
}
