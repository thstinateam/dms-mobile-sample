package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

public class REPORT_TEMPLATE_STAFF_MAP_TABLE
		extends ABSTRACT_TABLE {

	public static final String REPORT_TEMPLATE_STAFF_MAP_ID = "REPORT_TEMPLATE_STAFF_MAP_ID";
	public static final String REPORT_TEMPLATE_ID = "REPORT_TEMPLATE_ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "REPORT_TEMPLATE_STAFF_MAP";

	public REPORT_TEMPLATE_STAFF_MAP_TABLE(
			SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] {
				REPORT_TEMPLATE_STAFF_MAP_ID,
				REPORT_TEMPLATE_ID,
				STAFF_ID, SHOP_ID,
				STATUS, CREATE_DATE,
				UPDATE_DATE,
				CREATE_USER,
				UPDATE_USER, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName
				+ ";";
		this.sqlDelete += this.tableName
				+ ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}
}