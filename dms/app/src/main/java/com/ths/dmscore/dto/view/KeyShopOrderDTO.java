package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.KSCusProductRewardDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.lib.sqllite.db.KEYSHOP_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Lien quan thong tin keyshop Km cho don hang
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KeyShopOrderDTO implements Serializable {
	private static final long serialVersionUID = -6164913533559070628L;
	// noi dung field
	public long ksId; // id cua CT keyshop
	public String ksCode; // code cua ct keyshop
	public String name; // name cua ct keyshop
	public KSCustomerDTO keyshopItem = new KSCustomerDTO();
	public ArrayList<KSCusProductRewardDTO> lstKSProduct = new ArrayList<KSCusProductRewardDTO>();

	/**
	 * gen sql update thong tin tra keyshop cho don hang
	 *
	 * @author: Tuanlt11
	 * @param remindDate
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONArray generateSqlUpdatePayOrder() {
		JSONArray listSql = new JSONArray();
		listSql.put(keyshopItem.generateSqlUpdateKSCustomer());
		for(KSCusProductRewardDTO item: lstKSProduct){
			listSql.put(item.generateSqlUpdateKSCustomerProduct());
		}
		return listSql;
	}

	/**
	 * Parse du lieu tu cursor
	 *
	 * @author: Tuanlt11
	 * @param cursor
	 * @return: void
	 * @throws:
	 */
	public void parseDataFromCursor(Cursor c) {
		ksCode = CursorUtil.getString(c, KEYSHOP_TABLE.KS_CODE);
		name = CursorUtil.getString(c, KEYSHOP_TABLE.NAME);
		ksId = CursorUtil.getLong(c, KEYSHOP_TABLE.KS_ID);
	}

}
