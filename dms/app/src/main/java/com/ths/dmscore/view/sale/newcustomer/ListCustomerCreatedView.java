/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.view.NewCustomerItem;
import com.ths.dmscore.dto.view.NewCustomerListDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * ListCustomerCreatedView.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 9:15:29 AM Sep 16, 2014
 */
public class ListCustomerCreatedView
		extends BaseFragment implements
		OnEventControlListener,
		VinamilkTableListener,
		OnItemSelectedListener, DMSColSortManager.OnSortChange {

	private static final int ACTION_ORDER_LIST = 1;
	private static final int ACTION_PATH = 2;
	private static final int ACTION_CUS_LIST = 3;
	private static final int ACTION_CREATE_CUSTOMER = 9;
	public static final int ACTION_IMAGE = 10;
	private static final int ACTION_LOCK_DATE = 11;

	// BEGIN USER INFO:
	private String userId;
	private String shopId;
	// END USER INFO

	// BEGIN PAGING: Num row in page, current page
	private static final int NUM_ITEM_PER_PAGE = Constants.NUM_ITEM_PER_PAGE;
	private static final int ACTION_VIEW_CUSTOMER_INFO = -1;
	private static final int ACTION_DELETE_CUSTOMER = -2;
	private static final int ACTION_CANCEL_CUSTOMER = -3;
	private int currentPage = -1;
	// END PAGING

	// BEGIN DECLARE
	private VNMEditTextClearable edTKH;
	private Spinner spinnerState;
	// index spinner
	private int currentIndexSpinnerState = 0;
	private int indexSpinnerStateRequest = 0;
	private Button btSearchCustomer;
	private Button btCreateCustomer;
	private DMSTableView tbNewCustomer;
	// END DECLARE
	private NewCustomerListDTO dto;
	private int totalItem = 0;
	private String lastCustomerNameSearch = "";
	private long customerIdDelete = 0;
	private TextView tvCustomer;
	private TextView tvStatus;

	// BEGIN NEW INSTANCE
	public static ListCustomerCreatedView newInstance(Bundle b) {
		ListCustomerCreatedView instance = new ListCustomerCreatedView();
		instance.setArguments(b);
		return instance;
	}

	// END NEW INSTANCE

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_list_new_customer, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH);
		initUserInfo();
		initView(v);
		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
			.addInfoCaseUnSensitive(2, SortActionConstants.NAME)
			.addInfoCaseUnSensitive(5, SortActionConstants.TYPE)
			.build();
		initHeaderTable(tbNewCustomer, new CustomerCreatedRow(parent, this, 0,0), lstSort, this);
		initHeaderText();
		initHeaderMenu();
		initData();
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();

	}

	/**
	 * initUserInfo from other View send to this
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initUserInfo() {
		userId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
	}

	/**
	 * init View in layout
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initView(View view) {
		edTKH = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(view, R.id.edTKH, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_MAKH);
		tvCustomer = (TextView) PriUtils.getInstance().findViewByIdGone(view, R.id.tvKH, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_MAKH);
		tvStatus = (TextView) PriUtils.getInstance().findViewByIdGone(view, R.id.tvTrangThai, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_SPINNERTRANGTHAI);
		spinnerState = (Spinner) PriUtils.getInstance().findViewById(view, R.id.spinnerState, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_SPINNERTRANGTHAI);
		btSearchCustomer = (Button) PriUtils.getInstance().findViewById(view, R.id.btSearchCustomer, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_TIMKIEM);
		PriUtils.getInstance().setOnClickListener(btSearchCustomer, this);
		btCreateCustomer = (Button) PriUtils.getInstance().findViewById(view, R.id.btCreateCustomer, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_THEMMOI);
		PriUtils.getInstance().setOnClickListener(btCreateCustomer, this);
		tbNewCustomer = (DMSTableView) view.findViewById(R.id.tbNewCustomer);
		tbNewCustomer.setNumItemsPage(NUM_ITEM_PER_PAGE);
		tbNewCustomer.setListener(this);

		// render danh sach trang thai KH
//		SpinnerAdapter adapterState = new SpinnerAdapter(parent, R.layout.simple_spinner_item,  new NewCustomerItem().ARRAY_CUSTOMER_STATE);
        ArrayAdapter adapterState = new ArrayAdapter(parent, R.layout.simple_spinner_item, new NewCustomerItem().ARRAY_CUSTOMER_STATE);
        adapterState.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		this.spinnerState.setAdapter(adapterState);
		spinnerState.setOnItemSelectedListener(this);

		// back lai, hien thi lai nhung gi dang tim kiem
		if (dto != null) {
			// set lai spinner
			indexSpinnerStateRequest = currentIndexSpinnerState;
			this.spinnerState.setSelection(currentIndexSpinnerState);
			// set lai spinner
			edTKH.setText(lastCustomerNameSearch);
		}
	}

	/**
	 * show header text
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initHeaderText() {
		String title = StringUtil.getString(R.string.TITLE_VIEW_LIST_NEW_CUSTOMER);
		parent.setTitleName(title);
//		setTitleHeaderView(title);
	}

	/**
	 * init header menu
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initHeaderMenu() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(
				PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH,
				new MenuTab(R.string.TEXT_PHOTO,
						R.drawable.menu_picture_icon, ACTION_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH),
				new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
						R.drawable.icon_lock, ACTION_LOCK_DATE, PriHashMap.PriForm.NVBH_CHOTKHO),
				new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
						ACTION_CREATE_CUSTOMER, PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH),
				new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
						ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
				new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
						ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
				new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
						ACTION_CUS_LIST, PriHashMap.PriForm.NVBH_BANHANG_DSKH));
	}

	/**
	 * request data on first time
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initData() {
		// reset additional info
		lastCustomerNameSearch = "";
//		currentPage = -1;
//		totalItem = 0;
		currentIndexSpinnerState = -1;
		indexSpinnerStateRequest = currentIndexSpinnerState;

		if (currentIndexSpinnerState < 0) {
			currentIndexSpinnerState = 0;
			indexSpinnerStateRequest = currentIndexSpinnerState;
		}

		// set lai spinner
		this.spinnerState.setSelection(currentIndexSpinnerState);
		// set lai spinner
		edTKH.setText(lastCustomerNameSearch);
		if(dto == null || currentPage < 0){
			requestData(true);
		}else{
			renderLayout();
		}
	}

	/**
	 * request data from DB
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void requestData(boolean isGetTotalPage) {
		// show dialog loading...
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID, userId);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_CUSTOMER_NAME, lastCustomerNameSearch);
		data.putInt(IntentConstants.INTENT_NUM_ITEM_IN_PAGE, NUM_ITEM_PER_PAGE);
		data.putInt(IntentConstants.INTENT_STATE, indexSpinnerStateRequest);
		data.putInt(IntentConstants.INTENT_PAGE, currentPage);
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbNewCustomer.getSortInfo());

		handleViewEvent(data, ActionEventConstant.GET_LIST_NEW_CUSTOMER, SaleController.getInstance());
		// neu lay lan dau thi xem nhu lay trang 1
		if (currentPage < 0) {
			currentPage = 1;
		}

		// hide ban phim
		GlobalUtil.forceHideKeyboardInput(parent, btSearchCustomer);
	}

	/**
	 * render layout, display data
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void renderLayout() {
		if (dto != null) {
			// render thanh so trang
			tbNewCustomer.setTotalSize(totalItem, currentPage);
			tbNewCustomer.getPagingControl().setCurrentPage(currentPage);
			tbNewCustomer.clearAllData();
			if (dto.cusList.size() > 0) {
				int stt = StringUtil.getSTT(currentPage, NUM_ITEM_PER_PAGE);
				for (NewCustomerItem info : dto.cusList) {
					CustomerCreatedRow row = new CustomerCreatedRow(parent,
							this, ACTION_VIEW_CUSTOMER_INFO,
							ACTION_DELETE_CUSTOMER);
					row.render(stt, info);
					// add row
					tbNewCustomer.addRow(row);

					stt++;
				}
			} else {
				tbNewCustomer.showNoContentRow();
			}
		}
		// close Progress Dialog
		this.parent.closeProgressDialog();
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
			case ActionEventConstant.GET_LIST_NEW_CUSTOMER: {
				dto = (NewCustomerListDTO) modelEvent.getModelData();
				// neu co lay tong so item
				if (dto.totalItem >= 0) {
					totalItem = dto.totalItem;
				}
				renderLayout();
				break;
			}
			case ActionEventConstant.ACTION_DELETE_CUSTOMER: {
				parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_DELETE_CUSTOMER_SUCCESS));
				currentPage = -1;
				requestData(true);
				break;
			}
			default:
				super.handleModelViewEvent(modelEvent);
				break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// close Progress Dialog
		this.parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
			default:
				super.handleErrorModelViewEvent(modelEvent);
				break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
			case ACTION_DELETE_CUSTOMER: {
				requestDeleteCustomer(customerIdDelete);
				break;
			}
			case ACTION_PATH:
				gotoCustomerRouteView();
				break;
			case ACTION_ORDER_LIST:
				gotoListOrder();
				break;
			case ACTION_CUS_LIST:
				handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
				break;
			case ACTION_LOCK_DATE:
				handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
				break;
			case ACTION_IMAGE:
				handleSwitchFragment(null, ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
				break;
			default:
				super.onEvent(eventType, control, data);
				break;
		}
	}

	/**
	 * Toi man hinh danh sach don hang
	 *
	 * @author: dungdq3
	 * @since: 3:41:05 PM Sep 17, 2014
	 * @return: void
	 * @throws: :
	 */
	private void gotoListOrder() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER, SaleController.getInstance());
	}

	/**
	 * Toi man hinh lo trinh
	 *
	 * @author: dungdq3
	 * @since: 3:40:06 PM Sep 17, 2014
	 * @return: void
	 * @throws: :
	 */
	private void gotoCustomerRouteView() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_CUSTOMER_ROUTE, SaleController.getInstance());
	}

	/**
	 * table paging
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// get current page
		currentPage = tbNewCustomer.getPagingControl().getCurrentPage();
		// request data with page
		requestData(false);
	}

	/**
	 * event from Row
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
		if (data instanceof NewCustomerItem) {
			NewCustomerItem info = (NewCustomerItem) data;
			switch (action) {
				case ACTION_VIEW_CUSTOMER_INFO:
					currentPage = tbNewCustomer.getPagingControl().getCurrentPage();
					if (info.isEdit) {
						// neu co the edit, hien thi man hinh edit customer
						goToEditCustomer(info);
					} else {
						// hien thi man hinh thong tin chi tiet KH
						goToViewCustomer(info);
					}
					break;
				case ACTION_DELETE_CUSTOMER:
					confirmDeleteCustomer(info);
					break;
				default:
					break;
			}
		}
	}

	/**
	 * Hoi khi xoa khach hang
	 *
	 * @author: duongdt3
	 * @param info
	 * @since: 14:41:34 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmDeleteCustomer(NewCustomerItem info) {
		customerIdDelete = info.customerId;
		String text = StringUtil.getString(R.string.TEXT_CONFIRM_DELETE_CUSTOMER)
				+ " ["
				+ info.tvCustomerName
				+ "] ?";
		GlobalUtil.showDialogConfirm(this, this.parent, text, StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_DELETE_CUSTOMER, StringUtil.getString(R.string.TEXT_BUTTON_DENY),
				ACTION_CANCEL_CUSTOMER, null);
	}

	/**
	 * send request delete customer
	 *
	 * @author: duongdt3
	 * @since: 13:41:00 3 Jan 2014
	 * @return: void
	 * @throws:
	 * @param info
	 */
	private void requestDeleteCustomer(long customerId) {
		parent.showLoadingDialog();

		Bundle data = new Bundle();
		data.putLong(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEvent(data, ActionEventConstant.ACTION_DELETE_CUSTOMER, SaleController.getInstance());
	}

	/**
	 * Chuyen den man hinh thong tin khach hang
	 *
	 * @author: duongdt3
	 * @since: 10:38:54 14 Jan 2014
	 * @return: void
	 * @throws:
	 * @param info
	 * @param isEdit
	 */
	private void goToCustomerInfoView(NewCustomerItem info, boolean isEdit) {
		Bundle data = new Bundle();
		// truyen them id customer de hien thi thong tin
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, String.valueOf(info.customerId));
		data.putString(IntentConstants.INTENT_IS_VIEW_ALL, ListCustomerCreatedView.class.getName());
		data.putBoolean(IntentConstants.INTENT_IS_EDIT, isEdit);
		handleSwitchFragment(data, ActionEventConstant.GO_TO_CREATE_CUSTOMER, SaleController.getInstance());
	}

	/**
	 * go to View edit customer
	 *
	 * @author: duongdt3
	 * @since: 13:40:57 3 Jan 2014
	 * @return: void
	 * @throws:
	 * @param info
	 */
	private void goToEditCustomer(NewCustomerItem info) {
		goToCustomerInfoView(info, true);
	}

	/**
	 * go to View show customer info
	 *
	 * @author: duongdt3
	 * @since: 13:40:50 3 Jan 2014
	 * @return: void
	 * @throws:
	 * @param info
	 */
	private void goToViewCustomer(NewCustomerItem info) {
		goToCustomerInfoView(info, false);
	}

	/**
	 * Broadcast from Activity, (refresh data, ...)
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
			case ActionEventConstant.NOTIFY_REFRESH_VIEW:
				if (this.isVisible()) {
					currentPage = -1;
					dto = null;
					// Re request data
					initData();
				}
				break;
			case ActionEventConstant.BROADCAST_CREATE_OR_UPDATE_CUSTOMER:
				requestData(true);
				break;
			default:
				super.receiveBroadcast(action, extras);
				break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View arg1, int pos, long arg3) {
		if (spinner == spinnerState) {
			if (currentIndexSpinnerState != pos) {
				// update index spinner state
				currentIndexSpinnerState = pos;
				if (pos == 0) {
					indexSpinnerStateRequest = 0;
				} else if (pos == 1) {
					indexSpinnerStateRequest = 2;
				} else if (pos == 2) {
					indexSpinnerStateRequest = 1;
				} else if (pos == 3) {
					indexSpinnerStateRequest = 4;
				} else if (pos == 4) {
					indexSpinnerStateRequest = 3;
				}
				currentPage = -1;
				lastCustomerNameSearch = edTKH.getText().toString();
				requestData(true);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	public void onClick(View v) {
		if (v == btCreateCustomer) {
			// an ban phim
			GlobalUtil.forceHideKeyboardInput(parent, btSearchCustomer);
			// tao moi KH
			goToCreateCustomer();
		} else if (v == btSearchCustomer) {
			currentPage = -1;
			// remove khoang trang edit tim kiem
			edTKH.setText(edTKH.getText().toString().trim());
			lastCustomerNameSearch = edTKH.getText().toString();
			// tim kiem KH
			requestData(true);
		}
		super.onClick(v);
	}

	/**
	 * go to create customer
	 *
	 * @author: duongdt3
	 * @since: 19:52:34 3 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void goToCreateCustomer() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_CREATE_CUSTOMER, SaleController.getInstance());
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		requestData(false);
	}
}
