/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.Constants;

/**
 * thong tin nganh hang cua san pham
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class ProductInfoDTO extends AbstractTableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// id of product info
	private static long productInfoId;
	// code of product info
	private static volatile String productInfoCode;
	// name of product info
	private static volatile String productInfoName;
	// description of product info
	private static volatile String description;
	// status of product info (0: OFF, 1: ON)
	private static int status;
	// type of product info (4:flavour, 5: packing, 1: CAT, 2: SUB CAT, 3:
	// BRAND)
	private static volatile String type;

	public ProductInfoDTO() {
		setProductInfoId(0);
		setProductInfoCode(Constants.STR_BLANK);
		setProductInfoName(Constants.STR_BLANK);
		setDescription(Constants.STR_BLANK);
		setStatus(0);
		setProductInfoType("0");
	}

	public static long getProductInfoId() {
		return productInfoId;
	}

	public static void setProductInfoId(long productInfoId) {
		ProductInfoDTO.productInfoId = productInfoId;
	}

	public static String getProductInfoCode() {
		return productInfoCode;
	}

	public static void setProductInfoCode(String productInfoCode) {
		ProductInfoDTO.productInfoCode = productInfoCode;
	}

	public static String getProductInfoName() {
		return productInfoName;
	}

	public static void setProductInfoName(String productInfoName) {
		ProductInfoDTO.productInfoName = productInfoName;
	}

	public static String getDescription() {
		return description;
	}

	public static void setDescription(String description) {
		ProductInfoDTO.description = description;
	}

	public static int getStatus() {
		return status;
	}

	public static void setStatus(int status) {
		ProductInfoDTO.status = status;
	}

	public static String getProductInfoType() {
		return type;
	}

	public static void setProductInfoType(String type) {
		ProductInfoDTO.type = type;
	}
}
