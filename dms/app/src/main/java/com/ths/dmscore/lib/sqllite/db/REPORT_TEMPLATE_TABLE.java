package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ReportTemplateCriterionDTO;
import com.ths.dmscore.dto.view.ChooseParametersDTO;
import com.ths.dmscore.dto.view.DynamicKPIDTO;
import com.ths.dmscore.dto.view.ListDynamicKPIDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;

public class REPORT_TEMPLATE_TABLE
		extends ABSTRACT_TABLE {

	public static final String REPORT_TEMPLATE_ID = "REPORT_TEMPLATE_ID";
	public static final String NAME = "NAME";
	public static final String KPI_ID = "KPI_ID";
	public static final String IS_SHOW_PLAN = "IS_SHOW_PLAN";
	public static final String IS_SHOW_REALITY = "IS_SHOW_REALITY";
	public static final String IS_SHOW_COLUMN_TOTAL = "IS_SHOW_COLUMN_TOTAL";
	public static final String IS_SHOW_ROW_TOTAL = "IS_SHOW_ROW_TOTAL";
	public static final String CRI_ROW_ID = "CRI_ROW_ID";
	public static final String CRI_SUB_ROW_ID = "CRI_SUB_ROW_ID";
	public static final String CRI_COLUMN_ID = "CRI_COLUMN_ID";
	public static final String PERIOD_TYPE_ID = "PERIOD_TYPE_ID";
	public static final String PERIOD_CYCLE_TYPE = "PERIOD_CYCLE_TYPE";
	public static final String FROM_DATE = "FROM_DATE";
	public static final String TO_DATE = "TO_DATE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String PARENT_PERIOD_TYPE_ID = "PARENT_PERIOD_TYPE_ID";
	public static final String NUM_RECENT_PARENT_PERIOD = "NUM_RECENT_PARENT_PERIOD";

	public static final String TABLE_NAME = "REPORT_TEMPLATE";

	public REPORT_TEMPLATE_TABLE(
			SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] {
				REPORT_TEMPLATE_ID,
				NAME, KPI_ID,
				IS_SHOW_PLAN,
				IS_SHOW_REALITY,
				IS_SHOW_COLUMN_TOTAL,
				IS_SHOW_ROW_TOTAL,
				CRI_ROW_ID,
				CRI_SUB_ROW_ID,
				CRI_COLUMN_ID,
				PERIOD_TYPE_ID,
				PERIOD_CYCLE_TYPE,
				FROM_DATE,
				TO_DATE,
				CREATE_DATE,
				UPDATE_DATE,
				CREATE_USER,
				UPDATE_USER,PARENT_PERIOD_TYPE_ID,NUM_RECENT_PARENT_PERIOD, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName
				+ ";";
		this.sqlDelete += this.tableName
				+ ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	/**
	 * lay danh sach kpi dong
	 *
	 * @author: dungdq3
	 * @since: 2:01:11 PM Oct 24, 2014
	 * @return: StockLockDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ListDynamicKPIDTO getListDynamicKPI(Bundle b) throws Exception {
		boolean isGetTotalPage= b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, false);
		int page = b.getInt(IntentConstants.INTENT_PAGE, 1);
		int staffID = b.getInt(IntentConstants.INTENT_STAFF_ID, 0);
		String shopID = b.getString(IntentConstants.INTENT_SHOP_ID, Constants.STR_BLANK);
		String search = b.getString(IntentConstants.INTENT_SEARCH, Constants.STR_BLANK);
		int channelObjectType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
		DMSSortInfo sortInfo = (DMSSortInfo) b.getSerializable(IntentConstants.INTENT_SORT_DATA);
		ListDynamicKPIDTO list = new ListDynamicKPIDTO();

		ArrayList<String> param = new ArrayList<String>();
		Cursor c = null;
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listParentShopId = shopTB
				.getShopRecursive(shopID);
		String strListParentShop = TextUtils.join(",", listParentShopId);
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    rt.report_template_id AS REPORT_TEMPLATE_ID,	");
		sqlObject.append("	    rt.cri_column_id      AS CRI_COLUMN_ID, 	");
		sqlObject.append("	    rt.name               AS NAME,	");
		sqlObject.append("	    rt.from_date          AS FROM_DATE,	");
		sqlObject.append("	    rt.to_date            AS TO_DATE,	");
		sqlObject.append("	    rt.KPI_ID             AS KPI_ID,	");
		sqlObject.append("	    rt.period_cycle_type  AS PERIOD_CYCLE_TYPE,	");
		sqlObject.append("	    rt.parent_period_type_id  AS PARENT_PERIOD_TYPE_ID,	");
		sqlObject.append("	    rt.num_recent_parent_period  AS NUM_RECENT_PARENT_PERIOD,	");
		sqlObject.append("	    rtc.criterial_name    AS CRITIAL_NAME,	");
		sqlObject.append("	    ap.value              AS PERIOD_TYPE_ID,	");
		sqlObject.append("	    rtc.report_template_criterion_id              AS REPORT_TEMPLATE_CRITERION_ID	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (SELECT	");
		sqlObject.append("	        distinct  rt.report_template_id,	");
		sqlObject.append("	        rt.cri_column_id,	");
		sqlObject.append("	        rt.name,	");
		sqlObject.append("	        rt.from_date,	");
		sqlObject.append("	        rt.to_date,	");
		sqlObject.append("	        rt.name_text,	");
		sqlObject.append("	        rt.KPI_ID,	");
		sqlObject.append("	        rt.period_type_id,	");
		sqlObject.append("	        rt.period_cycle_type,	");
		sqlObject.append("	   		rt.parent_period_type_id,	");
		sqlObject.append("	    	rt.num_recent_parent_period ");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        report_template rt,	");
		sqlObject.append("	        report_template_staff_map rtsm	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        1 = 1	");
		sqlObject.append("	        AND      (	");
		sqlObject.append("	            CASE	");
		sqlObject.append("	                WHEN rtsm.staff_id is null	");
		sqlObject.append("	                AND rtsm.staff_type_id is null THEN 1	");
		sqlObject.append("	                WHEN rtsm.staff_id = ? THEN 1	");
		 param.add(String.valueOf(staffID));
		sqlObject.append("	                WHEN rtsm.staff_id is null	");
		sqlObject.append("	                AND  exists       (	");
		sqlObject.append("	                    SELECT	");
		sqlObject.append("	                        1	");
		sqlObject.append("	                    FROM	");
		sqlObject.append("	                        STAFF_TYPE CT	");
		sqlObject.append("	                    WHERE	");
		sqlObject.append("	                        1 = 1	");
		sqlObject.append("	                        AND CT.specific_type = ?	");
		param.add(""+channelObjectType);
		sqlObject.append("	                        AND rtsm.staff_type_id = CT.staff_type_id	");
		sqlObject.append("	                        AND CT.STATUS = 1	");
		sqlObject.append("	                )   THEN 1	");
		sqlObject.append("	                ELSE 0	");
		sqlObject.append("	            END	");
		sqlObject.append("	        )	");
		sqlObject.append("	        AND rtsm.shop_id IN (	");
		sqlObject.append(strListParentShop  );
		sqlObject.append("	        )	");
		sqlObject.append("	        AND  not exists    (	");
		sqlObject.append("	            SELECT	");
		sqlObject.append("	                1	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                report_template_exception rte	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1 = 1	");
		sqlObject.append("	            	AND rte.report_template_id = rt.report_template_id	");
		sqlObject.append("	                AND      (	");
		sqlObject.append("	                    CASE	");
		sqlObject.append("	                        WHEN rte.staff_id is null	");
		sqlObject.append("	                        AND rte.staff_type_id is null THEN 1	");
		sqlObject.append("	                        WHEN rte.staff_id = ? THEN 1	");
		param.add(String.valueOf(staffID));
		sqlObject.append("	                        WHEN rte.staff_id is null	");
		sqlObject.append("	                        AND exists       (	");
		sqlObject.append("	                            SELECT	");
		sqlObject.append("	                                1	");
		sqlObject.append("	                            FROM	");
		sqlObject.append("	                                STAFF_TYPE CT	");
		sqlObject.append("	                            WHERE	");
		sqlObject.append("	                                1 = 1	");
		sqlObject.append("	                                AND rte.staff_type_id = CT.staff_type_id	");
		sqlObject.append("	                                AND CT.specific_type = ?	");
		param.add(""+channelObjectType);
		sqlObject.append("	                                AND CT.STATUS = 1	");
		sqlObject.append("	                        )   THEN 1	");
		sqlObject.append("	                        ELSE 0	");
		sqlObject.append("	                    END	");
		sqlObject.append("	                )	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND rtsm.status = 1	");
		sqlObject.append("	            AND rtsm.report_template_id = rt.report_template_id	");
		// xu li truong hop chi xem mot so bao cao lien quan don vi hoac nhan vien
		// tuy user dang nhap la nv hay gs hoac ...
		if (channelObjectType == UserDTO.TYPE_STAFF) {
			sqlObject.append("	     AND (CASE	");
			sqlObject.append("	          WHEN rt.cri_row_id IN ( ");
			sqlObject.append(ReportTemplateCriterionDTO.CUSTOMER + "," + ReportTemplateCriterionDTO.STAFF + "," + ReportTemplateCriterionDTO.SUPERVISOR) ;
			sqlObject.append("	   											 ) THEN 1	");
			sqlObject.append("	      ELSE 0   END )	");
		}else if (channelObjectType == UserDTO.TYPE_SUPERVISOR) {
			sqlObject.append("	     AND (CASE	");
			sqlObject.append("	          WHEN rt.cri_row_id IN ( ");
			sqlObject.append(ReportTemplateCriterionDTO.STAFF + "," + ReportTemplateCriterionDTO.SUPERVISOR);
			sqlObject.append("	   											 ) THEN 1	");
			sqlObject.append("	      ELSE 0   END )	");
		}else{
			sqlObject.append("	     AND (CASE	");
			sqlObject.append("	          WHEN rt.cri_row_id IN ( ");
			sqlObject.append(ReportTemplateCriterionDTO.UNIT);
			sqlObject.append("	   											 ) THEN 1	");
			sqlObject.append("	      ELSE 0   END )	");
		}

		sqlObject.append("	    ) rt	");
		sqlObject.append("	LEFT    JOIN	");
		sqlObject.append("	    (	");
		sqlObject.append("	        SELECT	");
		sqlObject.append("	            criterial_name,	");
		sqlObject.append("	            report_template_criterion_id	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_criterion rtc	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            1=1	");
		sqlObject.append("	    ) AS rtc	");
		sqlObject.append("	        ON rt.kpi_id = rtc.report_template_criterion_id	");
		sqlObject.append("	LEFT    JOIN	");
		sqlObject.append("	    (	");
		sqlObject.append("	        SELECT	");
		sqlObject.append("	            ap_param_id,	");
		sqlObject.append("	            value                     ,	");
		sqlObject.append("	            ap_param_name	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            ap_param	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            TYPE = 'RPT_TEMP_PERIOD'	");
		sqlObject.append("	    )ap	");
		sqlObject.append("	        ON ap.ap_param_id = rt.period_type_id	");

		if (!StringUtil.isNullOrEmpty(search)) {
			search = StringUtil
					.getEngStringFromUnicodeString(search);
			search = StringUtil.escapeSqlString(search);
			search = DatabaseUtils.sqlEscapeString("%" + search
					+ "%");
			sqlObject.append(" WHERE ");
			sqlObject.append("	upper(rt.name_text) like upper(");
			sqlObject.append(search);
			sqlObject.append(") escape '^' ");
			sqlObject.append("	or upper(ap.ap_param_name) LIKE upper(");
			sqlObject.append(search);
			sqlObject.append(") escape '^' ");
			sqlObject.append("	or upper(rtc.criterial_name) like upper(");
			sqlObject.append(search);
			sqlObject.append(") escape '^' ");
		}
		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY REPORT_TEMPLATE_ID ";
		
		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.NAME, NAME)
		.addMapper(SortActionConstants.PERIOD, PERIOD_TYPE_ID)
		.addMapper(SortActionConstants.KPI, REPORT_TEMPLATE_CRITERION_TABLE.REPORT_TEMPLATE_CRITERION_ID)
		.defaultOrderString(defaultOrderByStr)
		.build(sortInfo);
		//add order string
		sqlObject.append(orderByStr);
		
		if (isGetTotalPage) {
			try {
				String total = StringUtil.getCountSql(sqlObject.toString());
				c = rawQueries(total, param);

				if (c != null) {
					if (c.moveToFirst()) {
						list.setTotalSize(c.getInt(0));
					}
				}
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e) {
				}
			}
		}

		sqlObject.append(StringUtil.getPagingSql(Constants.NUM_ITEM_PER_PAGE, page));
		try {
			c = rawQueries(sqlObject.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DynamicKPIDTO dto = new DynamicKPIDTO();
						dto.initFromCursor(c);
						list.getListDynamic().add(dto);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return list;
	}

	/**
	 * get parameters
	 *
	 * @author: dungdq3
	 * @since: 9:14:57 AM Oct 27, 2014
	 * @return: ReportTemplateDTO
	 * @throws:
	 * @param b
	 * @return:
	 */
	public ChooseParametersDTO getParameters(Bundle b) throws Exception {
		int staffID = b.getInt(IntentConstants.INTENT_STAFF_ID, 0);
		int reportTemplateID = b.getInt(IntentConstants.INTENT_ID, 0);
		String shopID = b.getString(IntentConstants.INTENT_SHOP_ID, Constants.STR_BLANK);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> param = new ArrayList<String>();
		ChooseParametersDTO dto = null;
		Cursor c = null;
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listParentShopId = shopTB
				.getShopRecursive(shopID);
		String strListParentShop = TextUtils.join(",", listParentShopId);

		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    distinct rt.report_template_id   AS report_template_id,	");
		sqlObject.append("	    rt.name                 AS NAME,	");
		sqlObject.append("	    rt.kpi_id               AS KPI_ID,	");
		sqlObject.append("	    rt.is_show_plan         AS IS_SHOW_PLAN,	");
		sqlObject.append("	    rt.is_show_reality      AS IS_SHOW_REALITY,	");
		sqlObject.append("	    rt.is_show_column_total AS IS_SHOW_COLUMN_TOTAL,	");
		sqlObject.append("	    rt.is_show_row_total    AS IS_SHOW_ROW_TOTAL,	");
		sqlObject.append("	    rt.row_id               AS CRI_ROW_ID,	");
		sqlObject.append("	    rt.sub_row_id           AS CRI_SUB_ROW_ID,	");
		sqlObject.append("	    rt.column_id            AS CRI_COLUMN_ID,	");
		sqlObject.append("	    rt.cri_row_name         AS CRI_ROW_NAME,	");
		sqlObject.append("	    rt.cri_sub_row_name     AS CRI_SUB_ROW_NAME,	");
		sqlObject.append("	    rt.cri_column_name      AS CRI_COLUMN_NAME,	");
		sqlObject.append("	    rt.period_type_id       AS PERIOD_TYPE_ID,	");
		sqlObject.append("	    rt.parent_period_type_id       AS PARENT_PERIOD_TYPE_ID,	");
		sqlObject.append("	    rt.num_recent_parent_period    AS NUM_RECENT_PARENT_PERIOD,	");
		sqlObject.append("	    rt.period_cycle_type    AS PERIOD_CYCLE_TYPE,	");
		sqlObject.append("	    rt.from_date            AS FROM_DATE,	");
		sqlObject.append("	    rt.to_date              AS TO_DATE,	");

		// type = 1 la dong
		sqlObject.append("	    ( CASE	");
		sqlObject.append("	        WHEN TYPE = 1 THEN ( CASE	");
		sqlObject.append("	            WHEN rt.row_id = ? THEN 	");
		param.add(""+ReportTemplateCriterionDTO.CUSTOMER);
//		sqlObject.append(" 					(SELECT	");
//		sqlObject.append("	               		 	substr(customer_code,1,3) as customer_code	");
//		sqlObject.append("	           		 FROM	");
//		sqlObject.append("	              		 	customer	");
//		sqlObject.append("	            	 WHERE	");
//		sqlObject.append("	                		customer_id = object_id )	");

		sqlObject.append(" 	(select Group_concat(ct.short_code ||'___'|| ct.shop_id ||'___'|| ct.status ||'___'|| ct.customer_name) ");
		sqlObject.append("   from visit_plan vp, ");
		sqlObject.append("     	  routing rt, ");
		sqlObject.append("        routing_customer rtc, ");
		sqlObject.append("        customer ct ");
		sqlObject.append("   where 1=1 ");
		sqlObject.append("         and vp.status = 1 ");
		sqlObject.append("         and rt.status = 1 ");
		sqlObject.append("         and vp.staff_id = ? ");
		param.add(""+staffID);
		sqlObject.append("         and vp.shop_id = ? ");
		param.add(""+shopID);
		sqlObject.append("         and vp.routing_id = rt.routing_id ");
		sqlObject.append("         and rtc.routing_id = rt.routing_id ");
		sqlObject.append("         and rtc.customer_id = ct.customer_id ");
		sqlObject.append("         and substr(vp.from_date,1,10) <= ? ");
		param.add(dateNow);
		sqlObject.append("         and (substr(vp.to_date,1,10) >= ? ");
		param.add(dateNow);
		sqlObject.append("            OR substr(vp.to_date,1,10) IS NULL ) ");
		sqlObject.append("         and substr(start_date,1,10) <= ? ");
		param.add(dateNow);
		sqlObject.append("         and ct.customer_id = object_id ");
		sqlObject.append("	 order by ct.CUSTOMER_CODE asc )	");

		// bao cao theo nhan vien
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffID = staff.getListStaffOfSupervisor(String.valueOf(staffID), shopID);
		sqlObject.append("	            WHEN rt.row_id = ? THEN                                    	");
		param.add(""+ReportTemplateCriterionDTO.STAFF);
		// cong staff_code voi shop_id lai de len view cat ra de xong sanh vi co the co 2 dong giong nhau
		sqlObject.append("	(SELECT	");
		sqlObject.append("	    Group_concat(staff_code ||'___'|| shop_id ||'___'|| status ||'___'|| staff_name)	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (  SELECT	");
		sqlObject.append("	        distinct st.staff_id,	");
		sqlObject.append("	        st.staff_code,	");
		sqlObject.append("	        st.staff_name,	");
		sqlObject.append("	        st.shop_id,	");
		sqlObject.append("	        st.status	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        staff st	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        st.staff_id in ( 	");
		sqlObject.append(listStaffID);
		sqlObject.append("	        	) and st.staff_id = object_id )ST WHERE 1 = 1)");

		// bao cao theo GS
		sqlObject.append("	            WHEN rt.row_id = ? THEN                                    	");
		param.add(""+ReportTemplateCriterionDTO.SUPERVISOR);
		// cong staff_code voi shop_id lai de len view cat ra de xong sanh vi co the co 2 dong giong nhau
		sqlObject.append("	(SELECT	");
		sqlObject.append("	    Group_concat(staff_code ||'___'|| shop_id ||'___'|| status ||'___'|| staff_name)	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (  SELECT	");
		sqlObject.append("	        distinct st.staff_id,	");
		sqlObject.append("	        st.staff_code,	");
		sqlObject.append("	        st.staff_name,	");
		sqlObject.append("	        st.shop_id,	");
		sqlObject.append("	        st.status	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        staff st	");
		sqlObject.append("	    JOIN	");
		sqlObject.append("	        parent_staff_map psm	");
		sqlObject.append("	            ON st.staff_id = psm.staff_id	");
		sqlObject.append("	            AND psm.parent_staff_id = ?	");
		param.add(""+staffID);
		sqlObject.append("	            AND NOT EXISTS (	");
		sqlObject.append("	                SELECT	");
		sqlObject.append("	                    *	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                exception_user_access eua	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                eua.staff_id = st.staff_id	");
		sqlObject.append("	                AND eua.parent_staff_id = psm.parent_staff_id	");
		sqlObject.append("	                AND eua.status = 1	)");
		sqlObject.append("	    JOIN	");
		sqlObject.append("	        (	");
		sqlObject.append("	            SELECT	");
		sqlObject.append("	                staff_type_id	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                STAFF_TYPE CT	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1 = 1	");
		sqlObject.append("	    			AND CT.SPECIFIC_TYPE IN (	");
		sqlObject.append(""					 + UserDTO.TYPE_SUPERVISOR);
		sqlObject.append("	    			)	");
		sqlObject.append("	        ) CT	");
		sqlObject.append("	            ON CT.staff_type_id = st.staff_type_id	");
		sqlObject.append("	   WHERE	");
		sqlObject.append("	        st.staff_id = psm.staff_id and st.staff_id  = object_id	and psm.status = 1) )");

		// bao cao don vi
		sqlObject.append("	            WHEN rt.row_id = ? THEN  ");
		param.add(""+ReportTemplateCriterionDTO.UNIT);
		// cong staff_code voi shop_code lai de len view cat ra de xong sanh vi co the co 2 dong giong nhau
		sqlObject.append("					  (	SELECT  Group_concat(sh.shop_code || '___' || sh.shop_id || '___' || sh.status || '___' || sh.shop_name ,',') AS SHOP_CODE ");
		sqlObject.append("						FROM shop sh  ");
		sqlObject.append("						WHERE sh.shop_id IN  ");
		sqlObject.append("						(  ");
		sqlObject.append(PriUtils.getInstance().getListShopChild());
		sqlObject.append("						)  ");
		sqlObject.append("       				AND sh.shop_id = object_id) ");
		// tam thoi command lai doan nay
//		sqlObject.append("						JOIN   ");
//		sqlObject.append("							(SELECT 	distinct oa.shop_id  ");
//		sqlObject.append("							 FROM  	 	role_user ru, ");
//		sqlObject.append("       								role_permission_map rpm, ");
//		sqlObject.append("       								org_access oa ");
//		sqlObject.append("							WHERE  		1 = 1 ");
//		sqlObject.append("       								AND oa.status = 1 ");
//		sqlObject.append("       								AND rpm.status = 1 ");
//		sqlObject.append("       								AND ru.status = 1 ");
//		sqlObject.append("       								AND oa.shop_id = object_id ");
//		sqlObject.append("       								AND ru.role_id = rpm.role_id ");
//		sqlObject.append("       								AND ru.user_id = ? ");
//		param.add(""+staffID);
//		sqlObject.append("	                					AND ru.role_id = ?	");
//		param.add(GlobalInfo.getInstance().getProfile().getUserData().roleId);
//		sqlObject.append("       								AND rpm.permission_id = oa.permission_id ");
//		sqlObject.append("       					) OA");
//		sqlObject.append("       				ON OA.shop_id = sh.shop_id)");
		sqlObject.append("	            END )	");

		// type = 2 la cot
		sqlObject.append("	     ELSE (	");
		sqlObject.append("	            CASE	");
		sqlObject.append("	            WHEN TYPE = 2 THEN ");
		sqlObject.append("	            ( CASE	");
		sqlObject.append("	            WHEN rt.column_id = ? THEN "); // dong la san pham
		param.add(""+ReportTemplateCriterionDTO.PRODUCT);
		sqlObject.append("						(SELECT Group_concat(product_code || '___' || status || '___' || product_name,',') ");
		sqlObject.append("						 FROM   product ");
		sqlObject.append("						 WHERE  product_id = object_id) ");

		sqlObject.append("	            WHEN rt.column_id = ? THEN	"); // nganh hang
		param.add(""+ReportTemplateCriterionDTO.CAT);
		sqlObject.append("						(SELECT Group_concat(pi.product_info_name || '___' || pi.status || '___' || pi.product_info_name,',') ");
		sqlObject.append("						 FROM   product_info pi ");
		sqlObject.append("						 WHERE  pi.[product_info_id] = object_id ");
//		sqlObject.append("       													AND status = 1 ");
		sqlObject.append("       				 AND type = 1)");
		sqlObject.append("	             WHEN rt.column_id = 6 THEN 6	");
		sqlObject.append("	                    END )	");
		sqlObject.append("	                END	");
		sqlObject.append("	            )	");
		sqlObject.append("	        END	");
		sqlObject.append("	    )                 AS ROW_NAME, 	");
		sqlObject.append("	     rto.TYPE         AS TYPE,");
		sqlObject.append("	     rto.object_id as OBJECT_ID,");
		sqlObject.append("	     rto.report_template_object_id as REPORT_TEMPLATE_OBJECT_ID,");
		sqlObject.append("	     rt.row_id as ROWID,");
		sqlObject.append("	     rt.column_id as COLUMNID,");
		sqlObject.append("	     rto.value as VALUE ");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (SELECT	");
		sqlObject.append("	        rt.report_template_id,	");
		sqlObject.append("	        rt.name,	");
		sqlObject.append("	        rt.period_cycle_type,	");
		sqlObject.append("	        rt.parent_period_type_id,	");
		sqlObject.append("	    	rt.num_recent_parent_period    AS NUM_RECENT_PARENT_PERIOD,	");
		sqlObject.append("	        rt.from_date,	");
		sqlObject.append("	        rt.to_date,	");
		sqlObject.append("	        (SELECT	");
		sqlObject.append("	            criterial_name	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_criterion	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            report_template_criterion_id = rt.kpi_id	");
		sqlObject.append("	            AND criterial_type = 4)                 AS KPI_ID,	");
		sqlObject.append("	        rt.is_show_plan,	");
		sqlObject.append("	        rt.is_show_reality,	");
		sqlObject.append("	        rt.is_show_column_total,	");
		sqlObject.append("	        rt.is_show_row_total,	");
		sqlObject.append("	        (SELECT	");
		sqlObject.append("	            criterial_name	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_criterion	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            report_template_criterion_id = rt.cri_row_id	");
		sqlObject.append("	            AND criterial_type = 1)                 AS CRI_ROW_NAME,	");
		sqlObject.append("	        (SELECT	");
		sqlObject.append("	            criterial_name	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_criterion	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            report_template_criterion_id = rt.cri_sub_row_id	");
		sqlObject.append("	            AND criterial_type = 3)                 AS CRI_SUB_ROW_NAME                ,	");
		sqlObject.append("	        (SELECT	");
		sqlObject.append("	            criterial_name	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_criterion	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            report_template_criterion_id = rt.cri_column_id	");
		sqlObject.append("	            AND criterial_type = 2)                 AS CRI_COLUMN_NAME,	");
		sqlObject.append("	        (SELECT	");
		sqlObject.append("	            value	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            ap_param ap	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            TYPE = 'RPT_TEMP_PERIOD'	");
		sqlObject.append("	            AND ap.ap_param_id = rt.period_type_id) AS PERIOD_TYPE_ID                ,	");
		sqlObject.append("	        rt.cri_row_id                AS row_id,	");
		sqlObject.append("	        rt.cri_sub_row_id            AS sub_row_id,	");
		sqlObject.append("	        rt.cri_column_id                                AS column_id	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        report_template rt,	");
		sqlObject.append("	        report_template_staff_map rtsm	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        1 = 1	");
		sqlObject.append("	        AND      (	");
		sqlObject.append("	            CASE	");
		sqlObject.append("	                WHEN rtsm.staff_id is null	");
		sqlObject.append("	                AND rtsm.staff_type_id is null THEN 1	");
		sqlObject.append("	                WHEN rtsm.staff_id = ? THEN 1	");
		 param.add(String.valueOf(staffID));
		sqlObject.append("	                WHEN rtsm.staff_id is null	");
		sqlObject.append("	                AND  exists       (	");
		sqlObject.append("	                    SELECT	");
		sqlObject.append("	                        1	");
		sqlObject.append("	                    FROM	");
		sqlObject.append("	                        STAFF_TYPE CT	");
		sqlObject.append("	                    WHERE	");
		sqlObject.append("	                        1 = 1 AND CT.SPECIFIC_TYPE = ?	");
		param.add(""+ GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType());
		sqlObject.append("	                        AND rtsm.staff_type_id = CT.staff_type_id	");
		sqlObject.append("	                        AND CT.STATUS = 1	");
		sqlObject.append("	                )   THEN 1	");
		sqlObject.append("	                ELSE 0	");
		sqlObject.append("	            END	");
		sqlObject.append("	        )	");
		sqlObject.append("	        AND rtsm.shop_id IN (	");
		sqlObject.append(strListParentShop  );
		sqlObject.append("	        )	");
		sqlObject.append("	        AND  not exists    (	");
		sqlObject.append("	            SELECT	");
		sqlObject.append("	                1	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                report_template_exception rte	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1 = 1	");
		sqlObject.append("	            	AND rte.report_template_id = rt.report_template_id	");
		sqlObject.append("	                AND      (	");
		sqlObject.append("	                    CASE	");
		sqlObject.append("	                        WHEN rte.staff_id is null	");
		sqlObject.append("	                        AND rte.staff_type_id is null THEN 1	");
		sqlObject.append("	                        WHEN rte.staff_id = ? THEN 1	");
		param.add(String.valueOf(staffID));
		sqlObject.append("	                        WHEN rte.staff_id is null	");
		sqlObject.append("	                        AND exists       (	");
		sqlObject.append("	                            SELECT	");
		sqlObject.append("	                                1	");
		sqlObject.append("	                            FROM	");
		sqlObject.append("	                                STAFF_TYPE CT	");
		sqlObject.append("	                            WHERE	");
		sqlObject.append("	                                1 = 1	");
		sqlObject.append("	                                AND rte.staff_type_id = CT.staff_type_id	");
		sqlObject.append("	                                AND CT.SPECIFIC_TYPE = ?	");
		param.add("" + GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType());
		sqlObject.append("	                                AND CT.STATUS = 1	");
		sqlObject.append("	                        )   THEN 1	");
		sqlObject.append("	                        ELSE 0	");
		sqlObject.append("	                    END	");
		sqlObject.append("	                )	");
		sqlObject.append("	            )	");
		sqlObject.append("	        AND rtsm.status = 1	");
		sqlObject.append("	        AND rtsm.report_template_id = rt.report_template_id	");
		sqlObject.append("	        AND rt.report_template_id = ?	");
		param.add(String.valueOf(reportTemplateID));
		sqlObject.append("	    ) RT	");
		sqlObject.append("	LEFT JOIN	");
		sqlObject.append("	    (	");
		sqlObject.append("	        SELECT	");
		sqlObject.append("	            report_template_id,	");
		sqlObject.append("	            report_template_object_id,	");
		sqlObject.append("	            TYPE,	");
		sqlObject.append("	            object_id,	");
		sqlObject.append("	            value	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            report_template_object	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            1 = 1	");
		sqlObject.append("	    ) rto	");
		sqlObject.append("	        ON rto.report_template_id = RT.report_template_id	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
//		sqlObject.append("	order by object_id,row_name	");
		sqlObject.append("	order by row_name asc	");

		try {
			c = rawQueries(sqlObject.toString(), param);

			if (c != null) {
				dto = new ChooseParametersDTO();
				if (c.moveToFirst()) {
					do {
						dto.reportTemplateId = reportTemplateID;
						dto.initFromCursor(c);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}
}