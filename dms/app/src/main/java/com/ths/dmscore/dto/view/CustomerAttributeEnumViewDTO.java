/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_ATTRIBUTE_ENUM_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * CustomerAttributeEnumViewDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:00:55 PM Jan 27, 2015
 */
public class CustomerAttributeEnumViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int enumID;
	public String code;
	public String value;
	public int enumSelected;

	public CustomerAttributeEnumViewDTO() {
		// TODO Auto-generated constructor stub
		code = Constants.STR_BLANK;
		value = Constants.STR_BLANK;
	}
	
	public void initFromCursor(Cursor c){
		code = CursorUtil.getString(c, "ENUM_CODE");
		value = CursorUtil.getString(c, "VALUE");
		enumID = CursorUtil.getInt(c, CUSTOMER_ATTRIBUTE_ENUM_TABLE.CUSTOMER_ATTRIBUTE_ENUM_ID);
		enumSelected = CursorUtil.getInt(c, "DETAIL_ENUM_ID", 0, 0);
	}

}
