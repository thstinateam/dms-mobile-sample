/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.lib.sqllite.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  Mo ta muc dich cua lop (interface)
 *  @author: Nguyen Thanh Dung
 *  @version: 1.1
 *  @since: 1.0
 */

public class ShopSupervisorDTO {
	public int shopId;
	public String shopCode;
	public int staffId;
	public String staffName;
	
	/**
	 * 
	 */
	public ShopSupervisorDTO() {
		shopCode = "";
		staffName = "";
	}
	
	/**
	*  Khoi tao gia tri
	*  @author: Nguyen Thanh Dung
	*  @param c
	*  @return: void
	*  @throws: 
	*/
	
	public void initWithCursor(Cursor c) {
		// id
		staffId = CursorUtil.getInt(c, "GS_STAFF_ID");
		staffName = CursorUtil.getString(c, "GS_STAFF_NAME");
		shopId = CursorUtil.getInt(c, "NVBH_SHOP_ID");
		// code
		shopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
	}
}
