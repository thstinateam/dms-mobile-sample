/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import android.database.Cursor;
import android.support.v4.util.LongSparseArray;

import com.ths.dmscore.lib.sqllite.db.GROUP_LEVEL_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 * ProductGroupDTO.java
 * @author: dungnt19
 * @version: 1.0
 * @since:  1.0
 */
public class GroupLevelDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -5168966521863517114L;
	public static final int PROMOTION_ORDER = 0;
	public static final int PROMOTION_PRODUCT = 1;

	public long groupLevelId;
	public long productGroupId;
	public String groupText;
	public int orderNumber;
	public int minQuantity;
	public int maxQuantity;
	public long minAmount;
	public long maxAmount;
	public int hasProduct;
	public double promotionPercent;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;

	public ArrayList<GroupLevelDetailDTO> levelDetailArray = new ArrayList<GroupLevelDetailDTO>();
	public ArrayList<GroupLevelDTO> subLevelArray = new ArrayList<GroupLevelDTO>();
	public LongSparseArray<GroupLevelDTO> subLevelHash = new LongSparseArray<GroupLevelDTO>();

	/**
	 * Lay thong tin tin cusor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		groupLevelId = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.GROUP_LEVEL_ID);
		productGroupId = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.PRODUCT_GROUP_ID);
		groupText = CursorUtil.getString(c, GROUP_LEVEL_TABLE.GROUP_TEXT);
		orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER);
		minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY);
		maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY);
		minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT);
		maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT);
		promotionPercent = CursorUtil.getDouble(c, GROUP_LEVEL_TABLE.PROMOTION_PERCENT);
		hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT);
	}
}
