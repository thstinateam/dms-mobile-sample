package com.ths.dmscore.dto.db.trainingplan;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SUP_TRAINING_PLAN_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;

/**
 * 
 * SupTraningPlanDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:42:53 17-11-2014
 */
@SuppressWarnings("serial")
public class SupTrainingPlanDTO implements Serializable {
	// id lich huan luyen
	private long supTrainingPlanId;
	// id TBHV
	private int supStaffId;
	// ID NVBH
	private int staffId;
	// ma NVBH
	private String staffCode;
	// ten NVBH
	private String staffName;

	private int staffGSId;
	// ma NVBH
	private String staffGSCode;
	// ten NVBH
	private String staffGSName;
	// ID shop NPP cua NVBH
	private int shopId;
	// ma shop NPP cua NVBH
	private String shopCode;
	// ten shop NPP cua NVBH
	private String shopName;
	// Thang huan luyen
	private String trainingMonth;
	// Ngay huan luyen
	private String trainingDate;
	// chuyen hay chua chuyen
//	private int isSend;
	// ngay chuyen
	private String sendDate;
	// Ke hoach
	private double amountPlan;
	// Thuc hien
	private double amountDone;
	//da duyet
	private double amountApproved;
	// user tao
	private String createUser;
	// user cap nhat
	private String updateUser;
	// ngay tao
	private String createDate;
	// ngay cao nhat
	private String updateDate;
	// Ngay huan luyen
	private String trainingDateForCriteria;
	
	public Date date;// dayInOrder

	public SupTrainingPlanDTO() {
		// TODO Auto-generated constructor stub
		supTrainingPlanId = 0;
		supStaffId = 0;
		staffId = 0;
		staffCode = Constants.STR_BLANK;
		staffName = Constants.STR_BLANK;
		staffGSId = 0;
		staffGSCode = Constants.STR_BLANK;
		staffGSName = Constants.STR_BLANK;
		shopId = 0;
		shopCode = Constants.STR_BLANK;
		shopName = Constants.STR_BLANK;
//		isSend = 0;
		sendDate = Constants.STR_BLANK;
		amountDone = 0.0;
		amountPlan = 0.0;
		trainingDateForCriteria = Constants.STR_BLANK;
		trainingMonth = Constants.STR_BLANK;
		trainingDate = Constants.STR_BLANK;
		createDate = Constants.STR_BLANK;
		updateDate = Constants.STR_BLANK;
		createUser = Constants.STR_BLANK;
		updateUser = Constants.STR_BLANK;
	}

	public long getSupTraningPlanId() {
		return supTrainingPlanId;
	}

	public void setSupTraningPlanId(long supTrainingPlanId) {
		this.supTrainingPlanId = supTrainingPlanId;
	}

	public int getSupStaffId() {
		return supStaffId;
	}

	public void setSupStaffId(int supStaffId) {
		this.supStaffId = supStaffId;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public String getStaffCode() {
		return staffCode;
	}

	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	public String getStaffName() {
		return staffName;
	}

	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}

	public int getStaffGSId() {
		return staffGSId;
	}

	public void setStaffGSId(int staffGSId) {
		this.staffGSId = staffGSId;
	}

	public String getStaffGSCode() {
		return staffGSCode;
	}

	public void setStaffGSCode(String staffGSCode) {
		this.staffGSCode = staffGSCode;
	}

	public String getStaffGSName() {
		return staffGSName;
	}

	public void setStaffGSName(String staffGSName) {
		this.staffGSName = staffGSName;
	}

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getTrainingMonth() {
		return trainingMonth;
	}

	public void setTrainingMonth(String trainingMonth) {
		this.trainingMonth = trainingMonth;
	}

	public String getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(String trainingDate) {
		this.trainingDate = trainingDate;
	}

//	public int getIsSend() {
//		return isSend;
//	}
//
//	public void setIsSend(int isSend) {
//		this.isSend = isSend;
//	}

	public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public double getAmountPlan() {
		return amountPlan;
	}

	public void setAmountPlan(double amountPlan) {
		this.amountPlan = amountPlan;
	}

	public double getAmountDone() {
		return amountDone;
	}
	public double getAmountApproved() {
		return amountApproved;
	}

	public void setAmountDone(double amountDone) {
		this.amountDone = amountDone;
	}
	
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * 
	 * @author: hoanpd1
	 * @since: 19:43:50 20-11-2014
	 * @return: void
	 * @throws:  
	 * @param cur
	 * @throws Exception
	 */
	public void initDataTrainingPlanTBHV(Cursor cur) throws Exception {
		SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
		SimpleDateFormat sfd = DateUtils.defaultDateFormat;
		supTrainingPlanId = CursorUtil.getLong(cur, "TRAINING_PLAN_ID");
		
		// id TBHV
		supStaffId = CursorUtil.getInt(cur, SUP_TRAINING_PLAN_TABLE.SUP_STAFF_ID);
		
		trainingMonth = CursorUtil.getString(cur, "MONTH");
		// ngay huan luyen
		trainingDate = CursorUtil.getString(cur, "DATE");
		trainingDateForCriteria = trainingDate;
		try {
			date = sfs.parse(trainingDate);
			trainingDate = sfd.format(date);
		} catch (ParseException e) {
		}
		
		
		// id NPP NVBH
		shopId = CursorUtil.getInt(cur, SUP_TRAINING_PLAN_TABLE.SHOP_ID);
		
		// ma NPP NVBH
		shopCode = CursorUtil.getString(cur, "SHOP_CODE");
		
		// ten NPP NVBH
		shopName = CursorUtil.getString(cur, "SHOP_NAME");
		
		// id NVBH
		staffGSId = CursorUtil.getInt(cur, "STAFF_ID_GS");
		
		// ma NVBH
		staffGSCode = CursorUtil.getString(cur, "STAFF_CODE_GS");
		
		// ten NVBH
		staffGSName = CursorUtil.getString(cur, "STAFF_NAME_GS");
		
		// id NVBH
		staffId = CursorUtil.getInt(cur, SUP_TRAINING_PLAN_TABLE.STAFF_ID);
		
		// ma NVBH
		staffCode = CursorUtil.getString(cur, "STAFF_CODE");
		
		// ten NVBH
		staffName = CursorUtil.getString(cur, "STAFF_NAME");
		// doanh so
		amountPlan = CursorUtil.getDouble(cur, "AMOUNT_PLAN");
		amountDone = CursorUtil.getDouble(cur, "AMOUNT");
		amountApproved= CursorUtil.getDouble(cur, "AMOUNT_APPROVED");
	}
	

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 8:25:41 AM Nov 20, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateJSONUpdate() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_PLAN_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_PLAN_TABLE.IS_SEND, 1, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_PLAN_TABLE.SEND_DATE, sendDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_PLAN_TABLE.UPDATE_DATE, updateDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_PLAN_TABLE.UPDATE_USER, updateUser, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_PLAN_TABLE.SUP_TRAINING_PLAN_ID, supTrainingPlanId, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	public String getTrainingDateForCriteria() {
		return trainingDateForCriteria;
	}

	public void setTrainingDateForCriteria(String trainingDateForCriteria) {
		this.trainingDateForCriteria = trainingDateForCriteria;
	}
}