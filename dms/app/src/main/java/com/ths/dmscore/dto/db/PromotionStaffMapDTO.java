/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_CUSTOMER_MAP;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_SHOP_MAP;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Thong tin luu tru cua customer promotion map
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 2.1
 */
@SuppressWarnings("serial")
public class PromotionStaffMapDTO extends AbstractTableDTO{
	// id shop promotin
	public long promotionStaffMapId ;
	// id CTKM
	public long promotionShopMapId ;
	// id shop
	public int shopId ;
	//staff id
	public int staffId;
	// so luong toi da
	public int quantityMax ;
	public String quantityMaxString ;
	// so luong thuc nhan
	public int quantityReceived;
	// so luong thuc nhan truoc duyet
	public int quantityReceivedTotal;
	// status
	public int status ;
	//nguoi tao
	public String createUser ;
	// nguoi cap nhat
	public String updateUser ;
	// ngay tao
	public String createDate ;
	// ngay cap nhat
	public String updateDate ;
	public double amountMax;
	public double amountReceived;
	public double amountReceivedTotal; //so tien nhan duoc truoc duyet
	public int numMax; // so luong toi da duoc nhan
	public int numReceived; // so luong da nhan duoc
	public int numReceivedTotal; // so luong nhan duoc truoc duyet
	private String amountMaxString;
	private String numMaxString;

	public PromotionStaffMapDTO(){
		super(TableType.PROMOTION_CUSTOMER_MAP_TABLE);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		promotionStaffMapId = CursorUtil.getLong(c, "PROMOTION_STAFF_MAP_ID", -1);
		promotionShopMapId = CursorUtil.getLong(c, "PROMOTION_SHOP_MAP_ID", -1);
		shopId = CursorUtil.getInt(c, "SHOP_ID", -1);
		staffId = CursorUtil.getInt(c, "STAFF_ID", -1);
		quantityMax = CursorUtil.getInt(c, "QUANTITY_MAX", -1);
		quantityMaxString = CursorUtil.getString(c, "QUANTITY_MAX");
		if(StringUtil.isNullOrEmpty(quantityMaxString)) {
			quantityMax = Integer.MAX_VALUE;
		}
		quantityReceived = CursorUtil.getInt(c, "QUANTITY_RECEIVED", -1);
		quantityReceivedTotal = CursorUtil.getInt(c, PROMOTION_SHOP_MAP.QUANTITY_RECEIVED_TOTAL, 0);
		status = CursorUtil.getInt(c, "STATUS", -1);
		createUser = CursorUtil.getString(c, "CREATE_USER");
		updateUser = CursorUtil.getString(c, "UPDATE_USER");
		createDate = CursorUtil.getString(c, "CREATE_DATE");
		updateDate = CursorUtil.getString(c, "UPDATE_DATE");
		amountMax = CursorUtil.getDouble(c, "AMOUNT_MAX", -1);
		amountMaxString = CursorUtil.getString(c, "AMOUNT_MAX");
		if(StringUtil.isNullOrEmpty(amountMaxString)) {
			amountMax = Double.MAX_VALUE;
		}
		amountReceived = CursorUtil.getDouble(c, "AMOUNT_RECEIVED", -1);
		amountReceivedTotal = CursorUtil.getDouble(c, PROMOTION_SHOP_MAP.AMOUNT_RECEIVED_TOTAL, 0);
		numMax = CursorUtil.getInt(c, "NUM_MAX", -1);
		numMaxString = CursorUtil.getString(c, "NUM_MAX");
		if(StringUtil.isNullOrEmpty(numMaxString)) {
			numMax = Integer.MAX_VALUE;
		}
		numReceived = CursorUtil.getInt(c, "NUM_RECEIVED", -1);
		numReceivedTotal = CursorUtil.getInt(c, PROMOTION_SHOP_MAP.NUM_RECEIVED_TOTAL, 0);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateDescreasePromotionSqlVansale() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*-1",
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
					this.promotionStaffMapId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInscreasePromotionSqlVansale() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*+1",
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
					this.promotionStaffMapId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

}
