package com.ths.dmscore.dto.db;



import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.KS_CUSTOMER_TABLE;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * DTO cho table ks_customer
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KSCustomerDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1827840296275094130L;
	public long ksCustomerId;
	public long ksId;
	public long cycleId;
	public long shopId;
	public long customerId;
	public double amountDisCount;// chiet khau doanh so
	public double overAmountDisCount;// chiet khau vuot doanh so
	public double displayMoney;
	public double supportMoney;
	public double oddDiscount;
	public double addMoney;
	public double totalRewardMoney;
	public double totalRewardMoneyDone;
	public int status;
	public long createUserId;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
//	public int approval; // cac trang thai
	public int rewardType; // loai tra thuong

	// cac loai trang thai cua status
	public static final int STATE_DELETE = -1; // xoa
	public static final int STATE_PENDING = 0; // tam ngung
	public static final int STATE_ACTIVE = 1; // hoat dong
	public static final int STATE_NEW = 2; // chua duyet
	public static final int STATE_REJECT = 3; // tu choi
	public static final int STATE_CANCLE = 4; // huy
	public static final String TEXT_NOT_YET_REGISTER = StringUtil.getString(R.string.TEXT_STATUS_NOT_YET_REGISTER);
//	public static final String TEXT_DELETE = StringUtil.getString(R.string.);
//	public static final String TEXT_PENDING = StringUtil.getString(R.string.);
	public static final String TEXT_ACTIVE = StringUtil.getString(R.string.TEXT_PROBLEM_HAS_APPROVED);
	public static final String TEXT_NEW = StringUtil.getString(R.string.TEXT_NOT_YET_APPROVE);
	public static final String TEXT_REJECT = StringUtil.getString(R.string.TEXT_REFUSE);
//	public static final String TEXT_CANCLE = StringUtil.getString(R.string.);

	// cac trang thai cua reward type
	public static final int TYPE_REWARD_NOT_PAY = 2; // mo khoa chua tra
	public static final int TYPE_REWARD_PART_PAY = 3; // tra mot phan
	public static final int TYPE_REWARD_ALL_PAY = 4; // tra toan bo
	@Override
	public Object clone(){
		// TODO Auto-generated method stub
		KSCustomerDTO object = new KSCustomerDTO();
		object.ksId = ksId;
		object.ksCustomerId = ksCustomerId;
		object.cycleId = cycleId;
		object.shopId = shopId;
		object.customerId = customerId;
		object.amountDisCount = amountDisCount;
		object.overAmountDisCount = overAmountDisCount;
		object.displayMoney = displayMoney;
		object.supportMoney = supportMoney;
		object.oddDiscount = oddDiscount;
		object.addMoney = addMoney;
		object.totalRewardMoney = totalRewardMoney;
		object.totalRewardMoneyDone = totalRewardMoneyDone;
		object.status = status;
		object.createDate = createDate;
		object.createUser = createUser;
		object.updateDate = updateDate;
		object.updateUser = updateUser;
//		object.approval = approval;
		object.rewardType = rewardType;
		object.createUserId = createUserId;
		return object;
	}

	 /**
	 * gen SQL insert hay update
	 * @author: Tuanlt11
	 * @param remindDate
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERTORUPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, KS_CUSTOMER_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.KS_CUSTOMER_ID, ksCustomerId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.KS_ID, ksId, null));
			if(cycleId > 0)
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.CYCLE_ID, cycleId, null));
			if(shopId > 0)
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.SHOP_ID, shopId, null));
			if(customerId > 0)
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.STATUS, status, null));
//			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER.APPROVAL, approval, null));
			if(createUserId > 0)
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.CREATE_USER_ID, createUserId, null));
			if(!StringUtil.isNullOrEmpty(createDate)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.CREATE_DATE, createDate, null));
			}
			if(!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.CREATE_USER, createUser, null));
			}
			if(!StringUtil.isNullOrEmpty(updateDate)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.UPDATE_DATE, updateDate, null));
			}
			if(!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.UPDATE_USER, updateUser, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(
					KS_CUSTOMER_TABLE.KS_CUSTOMER_ID, ksCustomerId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
		}

		return json;
	}

	/**
	 * gen sql keyshop da tra cho don hang
	 *
	 * @author: Tuanlt11
	 * @param remindDate
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateSqlUpdateKSCustomer() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, KS_CUSTOMER_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.KS_CUSTOMER_ID,
					ksCustomerId, null));
			if (!StringUtil.isNullOrEmpty(updateDate)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						KS_CUSTOMER_TABLE.UPDATE_DATE, updateDate, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						KS_CUSTOMER_TABLE.UPDATE_USER, updateUser, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.TOTAL_REWARD_MONEY_DONE,
					totalRewardMoneyDone, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_TABLE.KS_CUSTOMER_ID,
					ksCustomerId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
		}

		return json;
	}

	 /**
	 * Parse du lieu tu cursor
	 * @author: Tuanlt11
	 * @param cursor
	 * @return: void
	 * @throws:
	*/
	public void parseDataFromCursor(Cursor c){
		ksCustomerId = CursorUtil.getLong(c, KS_CUSTOMER_TABLE.KS_CUSTOMER_ID);
		totalRewardMoney = CursorUtil.getDouble(c, KS_CUSTOMER_TABLE.TOTAL_REWARD_MONEY);
		totalRewardMoneyDone = CursorUtil.getDouble(c, KS_CUSTOMER_TABLE.TOTAL_REWARD_MONEY_DONE);
	}


}
