package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.SaleOrderDTO;

public class ListSaleOrderDTO implements Serializable {
	private static final long serialVersionUID = -3743697709350312760L;
	public ArrayList<OrderDetailViewDTO> listData;
	public SaleOrderDTO saleOrderDTO;
	public int numPage = 0;

	public ListSaleOrderDTO() {
		this.listData = new ArrayList<OrderDetailViewDTO>();
		this.saleOrderDTO = new SaleOrderDTO();
		this.numPage = 0;
	}
}
