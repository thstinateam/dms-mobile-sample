/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.ShopDTO;


/**
 * 
 * luu thong tin tim kiem hinh anh
 * 
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
public class ImageSearchViewDTO implements Serializable {
	private static final long serialVersionUID = -6583232634999017977L;
	public boolean isFirstInit;// bien kiem tra request
	public boolean isUpdateProgame= false;// bien cap nhat chuong trinh
	public String fromDateForRequest = "";// tu ngay
	public String toDateForRequest = "";// den ngay
	public String programeId = "";// ma ct
	public String fromDate;// ngay
	public String toDate;// ngay
	public String customerCode = "";// ma kh
	public String customerName = "";// ten kh
	public String objectTypeImage="";// loai hinh anh
	public ArrayList<ImageListItemDTO> listItem= new ArrayList<ImageListItemDTO>();// thong tin image
	public ArrayList<DisplayProgrameDTO> listPrograme= new ArrayList<DisplayProgrameDTO>();// list chuong trinh
	public AlbumDTO albumInfo = new AlbumDTO();// ds hinh anh
	public ArrayList<StaffChoosenDTO> listStaff = new ArrayList<StaffChoosenDTO>();
	// danh sach shop
	public ArrayList<ShopDTO> listShop = new ArrayList<ShopDTO>();
	
	public AlbumDTO getAlbumInfo() {
		return albumInfo;
	}
	public void setAlbumInfo(AlbumDTO albumInfo) {
		this.albumInfo = albumInfo;
	}
	
}
