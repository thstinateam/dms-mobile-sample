/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.global;

import java.io.Serializable;
import java.util.Calendar;

import android.os.SystemClock;

import com.ths.dmscore.controller.AbstractController;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.util.GlobalUtil;


/**
 *  Tao ra cac action event giao tiep
 *  @author: DoanDM
 *  @version: 1.0
 *  @since: 1.0
 */
public class ActionEvent implements Serializable{
	private static final long serialVersionUID = -3232261198025401046L;
	public int action;
	 public Object modelData;
	 public Object viewData;
	 public Object userData;
	 public Object logData;
	 public Object sender;
	 public int tag = 0;


	 private int isController = -1;
	 //AnhND: xu ly relogin do mat session
	 public AbstractController controller;
	 //AnhND: xuy ly cancel request
	 public HTTPRequest request;
	 //AnhND: request co block (show progressDialog)
	 public boolean isBlockRequest;
//	 private HttpUpdateListener updateListener;
	 public boolean isNeedCheckTimeServer = false;
//	 public boolean isCheckTimeSuccess = false;

	//cac bien phuc vu viec ghi kpi ung dung
	public String createDate;
	public Calendar startTimeKPI;
	public long startTimeFromBoot;
	public long startTimeFromBootActive;
	public String kpiNote;

	public ActionEvent(){
		 startTimeKPI = Calendar.getInstance();
		 startTimeFromBoot = SystemClock.elapsedRealtime();
		 kpiNote = "Thread S: " + GlobalUtil.getKPIAsyncTaskNote();
	 }

	 public void reset(){
		 action = 0;
		 modelData = null;
		 viewData = null;
		 userData = null;
		 sender = null;
		 setIsController(-1);
	 }

//	 public void setUpdateListener(HttpUpdateListener lis) {
//		 updateListener = lis;
//	 }

//	public HttpUpdateListener getUpdateListener() {
//		return updateListener;
//	}

	public void setIsController(int isController) {
		this.isController = isController;
	}

	public int getIsController() {
		return isController;
	}

	// truong hop thao tac duoi local
	public static final int TYPE_OFFLINE = 0;
	// truong hop xu li fullDate thang len server
	public static final int TYPE_ONLINE = 1;
	// type default
	public int type = TYPE_OFFLINE;
	// co su dung asyn task ko
	public boolean isUsingAsyntask = true;
	
}
