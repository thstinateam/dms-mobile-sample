package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * Mo ta muc dich cua class
 *
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class ChangeNumPromotionProductRow extends DMSTableRow implements
		OnClickListener, OnFocusChangeListener {
	// STT
	TextView tvSTT;
	// code
	TextView tvProductCode;
	// Name
	TextView tvProductName;
	// number stock
	TextView tvNumStock;
	// number stock Thuc te
	TextView tvNumStockActual;
	// // number tvPrice
	// TextView tvPrice;
	// number tvNumberPromotion
	EditText edNumberPromotion;

	// listener
	// OnEventControlListener
	private OnEventControlListener listener;
	// data to render layout for row
	OrderDetailViewDTO myData;

	public ChangeNumPromotionProductRow(Context context, View aRow) {
		super(context, R.layout.layout_change_num_promotion_product_row);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvNumStock = (TextView) findViewById(R.id.tvNumStock);
		tvNumStockActual = (TextView) findViewById(R.id.tvNumStockActual);
		edNumberPromotion = (EditText) findViewById(R.id.edNumberPromotion);
		edNumberPromotion.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				listener.onEvent(
						OrderView.ACTION_CHANGE_REAL_QUANTITY_PROMOTION_IN_POPUP,
						ChangeNumPromotionProductRow.this, myData);
			}
		});
		myData = new OrderDetailViewDTO();
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			edNumberPromotion.setInputType(InputType.TYPE_NULL);
			edNumberPromotion.setOnClickListener(this);
			edNumberPromotion.setOnFocusChangeListener(this);
		}
	}

	public void setOnEventControlListener(OnEventControlListener listener) {
		this.listener = listener;
		// this.view = view2;
	}

	/**
	 * renderlayout
	 *
	 * @author: DungNX
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, OrderDetailViewDTO item) {
		this.myData = item;
		tvSTT.setText(String.valueOf(position));
		tvProductCode.setText(item.productCode);
		tvProductName.setText(item.productName);
		// long stock1 = item.stock / item.convfact;
		// long stock2 = item.stock % item.convfact;
		// String stock = String.valueOf(stock1) + "/" + String.valueOf(stock2);
		if (StringUtil.isNullOrEmpty(item.remaindStockFormat)) {
			item.remaindStockFormat = GlobalUtil
					.formatNumberProductFlowConvfact(item.stock, item.convfact);
		}
		// </HaiTC>
		if (StringUtil.isNullOrEmpty(item.remaindStockFormatActual)) {
			item.remaindStockFormatActual = GlobalUtil
					.formatNumberProductFlowConvfact(item.stockActual,
							item.convfact);
		}
		tvNumStock.setText(item.remaindStockFormat);
		tvNumStockActual.setText(item.remaindStockFormatActual);
		edNumberPromotion.setText(String
				.valueOf((int) item.orderDetailDTO.quantity));

		// Render check stock total
		checkStockTotal(item);
	}

	/**
	 * Kiem tra ton kho
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */

	public void checkStockTotal(OrderDetailViewDTO item) {
		if (item.stock <= 0) {
			this.updateColorForRow(ImageUtil.getColor(R.color.RED));
		} else if (item.stock < item.totalOrderQuantity.orderDetailDTO.quantity) {
			this.updateColorForRow(ImageUtil.getColor(R.color.OGRANGE));
		} else {
			GlobalUtil.setTextColor(ImageUtil.getColor(R.color.COLOR_LINK),
					tvProductCode);
			GlobalUtil.setTextColor(ImageUtil.getColor(R.color.BLACK), tvSTT,
					tvProductName, tvNumStock, tvNumStockActual,
					edNumberPromotion);
		}
	}

	/**
	 * doi mau row
	 *
	 * @author: DungNX
	 * @param color
	 * @return: void
	 * @throws:
	 */
	public void updateColorForRow(int color) {
		GlobalUtil.setTextColor(color, tvSTT, tvProductCode, tvProductName,
				tvNumStock, tvNumStockActual, edNumberPromotion);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == edNumberPromotion) {
			listener.onEvent(
					ActionEventConstant.SHOW_KEYBOARD_CUSTOM_UPPER_VIEW,
					edNumberPromotion, null);
		}

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v == edNumberPromotion) {
			listener.onEvent(
					ActionEventConstant.SHOW_KEYBOARD_CUSTOM_UPPER_VIEW,
					edNumberPromotion, null);
		}
	}
}
