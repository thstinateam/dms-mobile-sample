/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.trainingplan.SupTrainingIssueDTO;

/**
 * ListSupTrainingIssueDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  8:49:20 AM Nov 19, 2014
 */
public class ListSupTrainingIssueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int totalItem;
	private ArrayList<SupTrainingIssueDTO> listIssue;
	
	public ListSupTrainingIssueDTO() {
		// TODO Auto-generated constructor stub
		totalItem = 0;
		listIssue = new ArrayList<SupTrainingIssueDTO>();
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public ArrayList<SupTrainingIssueDTO> getListIssue() {
		return listIssue;
	}

	public void setListIssue(ArrayList<SupTrainingIssueDTO> listIssue) {
		this.listIssue = listIssue;
	}
	
	/**
	 * lay kich thuoc mang
	 * 
	 * @author: dungdq3
	 * @since: 8:51:26 AM Nov 19, 2014
	 * @return: int
	 * @throws:  
	 * @return
	 */
	public int getSizeOfList(){
		return listIssue.size();
	}
	
	/**
	 * get item in list
	 * 
	 * @author: dungdq3
	 * @since: 8:52:12 AM Nov 19, 2014
	 * @return: SupTrainingIssueDTO
	 * @throws:  
	 * @param pos
	 * @return
	 */
	public SupTrainingIssueDTO getItem(int pos){
		SupTrainingIssueDTO temp = null;
		if(listIssue.size() > 0 && listIssue.size() > pos)
			temp = listIssue.get(pos);
		return temp;
	}
	
	/**
	 * add item
	 * 
	 * @author: dungdq3
	 * @since: 9:20:31 AM Nov 19, 2014
	 * @return: void
	 * @throws:  
	 * @param dto
	 */
	public void addItem(SupTrainingIssueDTO dto){
		listIssue.add(dto);
	}

	/**
	 * add all
	 * 
	 * @author: dungdq3
	 * @since: 2:08:55 PM Nov 19, 2014
	 * @return: void
	 * @throws:  
	 * @param listIssue2
	 */
	public void addAll(ArrayList<SupTrainingIssueDTO> listIssue2) {
		// TODO Auto-generated method stub
		listIssue.clear();
		listIssue.addAll(listIssue2);
	}

}
