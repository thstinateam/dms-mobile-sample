package com.ths.dmscore.view.sale.customer;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dmscore.view.main.SalesPersonActivity.PopupProblemsListener;
import com.ths.dms.R;

/**
 * Danh sach khach hang role NVBH
 *
 * @author: Cuonglt
 * @version: 1.0
 * @since:  11:18:31 20-03-2015
 */
public class CustomerListView extends BaseFragment implements
		OnEventControlListener, OnClickListener, VinamilkTableListener,
		OnItemSelectedListener, DMSColSortManager.OnSortChange {
	private static final int ACTION_CUS_LIST = 1;
	private static final int ACTION_PATH = 2;
	private static final int ACTION_ORDER_LIST = 3;
	private static final int ACTION_CREATE_CUSTOMER = 4 ;
	private static final int ACTION_OK = 5;
	private static final int ACTION_END_VISIT_OK = 6;
	private static final int ACTION_CANCEL = 7;
	public static final int ACTION_POS_OK = 8;
	private static final int ACTION_OK_2 = 9;
	public static final int ACTION_IMAGE = 10;
	private static final int ACTION_LOCK_DATE = 11;

	public CustomerListDTO cusDto;// cusList
	private DMSTableView tbCusList;// tbCusList
//	private VNMEditTextClearable edMKH;// edMKH
	private VNMEditTextClearable edTKH;// edTKH
	private Spinner spinnerLine;// spinnerLine
	private Button btSearch; // btSearch
	private int currentPage = -1;

//	private String textCusCode = "";
	private String textCustomer = "";

	private int currentSpinerSelectedItem = -1;
	private boolean isUpdateData = false;

	private double lat = -1;
	private double lng = -1;
	private boolean isBack = false;
	public boolean isBackFromPopStack = false;
	boolean isSearchOrSelectSpiner = false;
	private boolean isRequestCusList = false;
	private GlobalInfo.VistConfig visitConfig = GlobalInfo.getInstance().getVisitConfig();
	
	public static CustomerListView newInstance(Bundle data) {
		CustomerListView f = new CustomerListView();
		f.setArguments(data);
		return f;
	}

	@SuppressWarnings("unused")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_customer_list_fragment, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		//hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_DSKH);
		lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		LinearLayout llSearch = (LinearLayout) PriUtils.getInstance().findViewByIdGone(v, R.id.llSearch, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM);
		TextView tvTuyen = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTuyen, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM_TUYEN);
		TextView tvMKH = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvMKH, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM_TEN);
		edTKH = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edCustomer, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM_TEN);
		btSearch = (Button) PriUtils.getInstance().findViewById(v, R.id.btSearch, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM_TEN);
		PriUtils.getInstance().setOnClickListener(btSearch, this);
		spinnerLine = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spinnerPath, PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_TIMKIEM_TUYEN);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerLine, this);

		tbCusList = (DMSTableView) view.findViewById(R.id.tbCusList);
		tbCusList.setListener(this);

		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
				.addInfo(8, SortActionConstants.VISIT_STATE)
				.build();

		// khoi tao header cho table
		initHeaderTable(tbCusList, new CustomerListRow(parent, this), lstSort, this);
		// reset lai bien isInsertingActionLogVisit
		parent.isInsertingActionLogVisit = false;
//		SpinnerAdapter adapterLine = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, Constants.getArrayLineChoose());

//		ArrayAdapter adapterLine = ArrayAdapter.createFromResource(parent,
//				R.array.list_days_of_week, R.layout.simple_spinner_item);

		ArrayAdapter adapterLine = new ArrayAdapter(parent, R.layout.simple_spinner_item, Constants.getArrayLineChoose());
		adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);

		if (!isBackFromPopStack) {
			spinnerLine.setAdapter(adapterLine);
			if (cusDto != null && currentPage != -1) {
				spinnerLine.setSelection(currentSpinerSelectedItem);
				renderLayout();
			} else {
				spinnerLine.setSelection(DateUtils.getCurrentDay());
				currentSpinerSelectedItem = spinnerLine
						.getSelectedItemPosition();
				getCustomerList(1, 1);
			}
			parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER));
			// changeTitleAndSearch(currentSpinerSelectedItem);

			// TamPQ: kiem tra trang thai menu ghe tham
			ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			if (actionLog != null && actionLog.isVisited()) {
				parent.endVisitCustomerBar();
			}
			
			if (lat > 0 && lng > 0) {
				parent.reStartLocating();				
			} else {
				parent.reStartLocatingWithWaiting();
			}
		} else {
			isBackFromPopStack = false;
		}

		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// enable menu bar
		/*enableMenuBar(this);
		addMenuItem(
				PriForm.NVBH_BANHANG_DSKH,
				new MenuTab(R.string.TEXT_PHOTO,
						R.drawable.menu_picture_icon, ACTION_IMAGE, PriForm.NVBH_DSHINHANH),
				new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
						R.drawable.icon_lock, ACTION_LOCK_DATE, PriForm.NVBH_CHOTKHO),
				new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
						ACTION_CREATE_CUSTOMER, PriForm.NVBH_THEMMOIKH_DANHSACH),
				new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
						ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
				new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
						ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
				new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
						ACTION_CUS_LIST,PriForm.NVBH_BANHANG_DSKH));*/
	}

	/**
	 * getCustomerList
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void getCustomerList(int page, int isGetTotalPage) {
		isRequestCusList = true;
		// isBack=false;
		parent.showLoadingDialog();

		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putInt(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritId());
		bundle.putInt(
				IntentConstants.INTENT_SHOP_ID,
				Integer.parseInt(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId()));
		if (currentSpinerSelectedItem < 0 || currentSpinerSelectedItem >= Constants.getArrayLineChoose().length) {
			currentSpinerSelectedItem = spinnerLine.getSelectedItemPosition();
		}
		if (currentSpinerSelectedItem < 0 || currentSpinerSelectedItem >= Constants.getArrayLineChoose().length) {
			currentSpinerSelectedItem = 0;
		}
		bundle.putString(IntentConstants.INTENT_VISIT_PLAN, DateUtils.getVisitPlan(Constants.getArrayLineChoose()[currentSpinerSelectedItem]));
		// 0: ko lay ngoai tuyen, 1 lay them ngoai tuyen voi dk da ghe tham
		bundle.putString(IntentConstants.INTENT_GET_WRONG_PLAN, "0");
		bundle.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);

		// check data search
//		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, textCusCode);
//		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, "");
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, textCustomer);

		//add sort info
		bundle.putSerializable(IntentConstants.INTENT_SORT_DATA, tbCusList.getSortInfo());

		handleViewEvent(bundle, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
	}

	public SpannableObject titleString(int dayIndex) {
		if (dayIndex < 0) {
			dayIndex = DateUtils.getCurrentDay();
		}
		String today = DateUtils
				.getVisitPlan(Constants.getArrayLineChoose()[dayIndex]);
		String st = "(" + StringUtil.getString(R.string.TEXT_IN_ROUTE) + " "
				+ today + ")";

		SpannableObject obj = new SpannableObject();
		obj.addSpan(StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER)
				+ Constants.STR_SPACE, ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.NORMAL);
		obj.addSpan(st, ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);
		return obj;
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_CUSTOMER_LIST:
			if (isSearchOrSelectSpiner) {
				isSearchOrSelectSpiner = false;
				cusDto = null;
				currentPage = -1;
			}
			CustomerListDTO tempDto = (CustomerListDTO) modelEvent
					.getModelData();
			if (cusDto == null) {
				cusDto = tempDto;
			} else {
				cusDto.setCusList(tempDto.getCusList());
				cusDto.setDistance(tempDto.getDistance());
			}

			if (isUpdateData) {
				isUpdateData = false;
				currentPage = -1;
				cusDto.setTotalCustomer(tempDto.getTotalCustomer());
			}

			if (cusDto != null) {
				renderLayout();
			}
			parent.closeProgressDialog();
			requestInsertLogKPI(HashMapKPI.NVBH_DANHSACHKHACHHANG, e);
			isRequestCusList = false;
			break;
		case ActionEventConstant.UPDATE_ACTION_LOG:
			GlobalInfo.getInstance().getProfile()
					.setActionLogVisitCustomer((ActionLogDTO) e.viewData);
			processOrder((CustomerListItem) e.userData);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_CUSTOMER_LIST: {
			isRequestCusList = false;
		}
		break;
	}
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onPause() {
		isBack = true;
		super.onPause();
	}

	/**
	 * renderLayout
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		if (currentPage != -1) {
			if (isBack) {
				isBack = false;
				tbCusList.setTotalSize(cusDto.getTotalCustomer(),currentPage);
			}
			 tbCusList.getPagingControl().setCurrentPage(currentPage);
		} else {
			tbCusList.setTotalSize(cusDto.getTotalCustomer(), 1);
			currentPage = tbCusList.getPagingControl().getCurrentPage();
		}
		
		tbCusList.clearAllData();
		
		
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbCusList.getPagingControl().getCurrentPage() - 1);
		if (cusDto.getCusList().size() > 0) {
			if (GlobalInfo.getInfoCustomerChange() != null) {
				CustomerDTO infoCustomerChange = GlobalInfo.getInfoCustomerChange();
				try {
					for (int i = 0, s = cusDto.getCusList().size(); i < s; i++) {
						CustomerListItem cusItem = cusDto.getCusList().get(i);
						if (cusItem.aCustomer.customerId == infoCustomerChange.customerId) {
							cusItem.aCustomer.lat = infoCustomerChange.lat;
							cusItem.aCustomer.lng = infoCustomerChange.lng;
							cusItem.updateCustomerDistance(cusItem.distanceOrder);
							MyLog.d("CustomerListView", "find change customer info, change cus id = " + cusItem.aCustomer.customerId);
							break;
						}
					}
				} catch (Exception e) {
					MyLog.e("CustomerListView", "set change customer info from detail error", e);
				} finally{
					GlobalInfo.setInfoCustomerChange(null);
				}
			}
			
			for (int i = 0, s = cusDto.getCusList().size(); i < s; i++) {
				CustomerListRow row = new CustomerListRow(parent, this);
				row.render(pos, cusDto.getCusList().get(i),
						currentSpinerSelectedItem, visitConfig);
				pos++;
				tbCusList.addRow(row);
			}
		} else {
			tbCusList.showNoContentRow();
		}
	}
	
	/**
	 * di toi man hinh chi tiet Khach hang
	 *
	 * @author : TamPQ since : 10:59:39 AM
	 */
	public void gotoCustomerInfo(String customerId) {
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleSwitchFragment(bunde,ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoVoteDisplayPresentProduct(CustomerListItem listItem) {
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, listItem);
		handleSwitchFragment(b, ActionEventConstant.GOTO_VOTE_DISPLAY_PRESENT_PRODUCT, SaleController.getInstance());
	}
	
	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoTakePhotoEquipment(CustomerListItem listItem) {
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, listItem);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_TAKE_PHOTO_EQUIPMENT, SaleController.getInstance());
	}

	/**
	 * Chuyen den man hinh dat hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoCreateOrder(CustomerListItem dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				dto.aCustomer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				dto.aCustomer.getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				dto.aCustomer.address);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE,
				dto.aCustomer.getCustomerCode());
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				dto.aCustomer.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID,
				dto.aCustomer.deliverID);
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "0");
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				dto.aCustomer.getCustomerTypeId());
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_NEW);
		bundle.putSerializable(IntentConstants.INTENT_SUGGEST_ORDER_LIST, null);
		bundle.putString(IntentConstants.INTENT_IS_OR, String.valueOf(dto.isOr));
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, dto.routingId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {

		switch (eventType) {
		case ACTION_PATH:
			handleSwitchFragment(null,ActionEventConstant.GO_TO_CUSTOMER_ROUTE, UserController.getInstance());
			break;
		case ACTION_ORDER_LIST:
			gotoListOrder();
			break;
		case ACTION_OK:
			CustomerListItem item = (CustomerListItem) data;
			// ket thuc ghe tham
			parent.requestUpdateActionLog("0", null, item, this);
			break;
		case ACTION_POS_OK:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_GENERAL_STATISTICS, UserController.getInstance());
			// notify menu
			Bundle b = new Bundle();
			b.putInt(IntentConstants.INTENT_INDEX, 0);
			sendBroadcast(ActionEventConstant.NOTIFY_MENU, b);
			break;
		case ACTION_OK_2:
			cusDto = null;
			currentPage = -1;
			getCustomerList(1, 1);
			break;
		case ACTION_END_VISIT_OK: {
			final CustomerListItem itemCustomer = (CustomerListItem) data;

			// ket thuc ghe tham
			// parent.endVisitCustomerBar();
			((SalesPersonActivity) parent)
					.showPopupProblemsFeedBack(new PopupProblemsListener() {

						@Override
						public void closePopup() {
							parent.requestUpdateActionLog("0", "0",
									itemCustomer, this);
							processOrder(itemCustomer);
						}

						@Override
						public void gotoCustomerInfo() {
							ActionLogDTO al = GlobalInfo.getInstance()
									.getProfile().getActionLogVisitCustomer();
							parent.requestUpdateActionLog("0", "0",
									itemCustomer, this);
							processOrder(itemCustomer);
							((SalesPersonActivity) parent)
									.gotoFeedBackListOfCusOrder(al.aCustomer);

						}

					});
			break;
		}
		case ACTION_CREATE_CUSTOMER:{
			gotoListCustomerCreated();
			break;
		}
		case ACTION_LOCK_DATE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
			break;
		case ACTION_IMAGE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSearch:
			isSearchOrSelectSpiner = true;

//			textCusCode = edMKH.getText().toString().trim();
			textCustomer = edTKH.getText().toString().trim();

			// hide ban phim
			GlobalUtil.forceHideKeyboard(parent);

			getCustomerList(1, 1);
			break;
		default:
			break;
		}

	}

	/**
	 * ghe tham dat hang
	 *
	 * @author: TamPQ
	 * @param item
	 * @return: voidvoid
	 * @throws:
	 */
	private void processOrder(CustomerListItem item) {
		// Luu thong tin KH ghe tham gan nhat
		if (item.isOr == 1) {
			item.isHaveDisplayProgramNotYetVote = false;
		}
		GlobalInfo.getInstance().getProfile().setLastVisitCustomer(item);
//		item.isHaveSaleOrder = true;
//		item.isTodayCheckedRemain = false;
		if (item.isOr == 0) {// KH trong tuyen
			// neu co chuong trinh trung bay chua cham
//			item.visitStatus != VISIT_STATUS.VISITED_CLOSED &&
//			&& item.visitStatus != VISIT_STATUS.VISITED_FINISHED
			if (item.isHaveDisplayProgramNotYetVote
					&& isValidDistanceToVoteDisplayProgAndCheckRemain(item)) {
				gotoVoteDisplayPresentProduct(item);
			} else if (item.isHaveEquip && isValidDistanceToVoteDisplayProgAndCheckRemain(item)) {
				gotoTakePhotoEquipment(item);
			}else if(item.isExistKeyShop){
				goToVoteKeyShop(item);
			}
			else if (// isValidDistanceToVoteDisplayProgAndCheckRemain(item)
						// &&
			//Neu co don hang lich su thi kiem ton
			//Trong tuyen cung kiem ton
			item.isHaveSaleOrder) {
				gotoRemainProductView(item);
			}else {
				gotoCreateOrder(item);
			}

			if (item.visitStatus != VISIT_STATUS.VISITED_FINISHED) {
				if (item.visitStatus == VISIT_STATUS.VISITED_CLOSED) {
					saveLastVisitToActionLogProfile(item);
					parent.initCustomerNameOnMenuVisit(
							item.aCustomer.customerCode,
							item.aCustomer.customerName);
				} else if (item.visitStatus == VISIT_STATUS.VISITING) {
					saveLastVisitToActionLogProfile(item);
					parent.initMenuVisitCustomer(item.aCustomer.customerCode,
							item.aCustomer.customerName);
					if (item.isTodayCheckedRemain || item.isTodayVoted
							|| item.isTodayOrdered) {
						// da ghe tham, da dat hang, da cham trung bay, da kiem
						// hang
						// ton trong ngay thi an menu dong cua
						parent.removeMenuCloseCustomer();
					}
				} else {// VISIT_STATUS.NONE_VISIT
					parent.initMenuVisitCustomer(item.aCustomer.customerCode,
							item.aCustomer.customerName);
					parent.requestStartInsertVisitActionLog(item);
					item.visitStatus = VISIT_STATUS.VISITING;
				}
			} else {// neu da ghe tham thi chi hien thi title Ghe tham
				parent.initCustomerNameOnMenuVisit(item.aCustomer.customerCode,
						item.aCustomer.customerName);
				saveLastVisitToActionLogProfile(item);
			}

			// check to update EXCEPTION_ORDER_DATE: bien cho phep dat hang khi
			// khoach cach qua xa
			if (!StringUtil.isNullOrEmpty(item.exceptionOrderDate)) {
				requestUpdateExceptionOrderDate(item);
			}

			// set toa do khach hang dang ghe tham de toi uu dinh vi
			GlobalInfo.getInstance().setPositionCustomerVisiting(
					new LatLng(item.aCustomer.lat, item.aCustomer.lng));
		} else {// KH ngoai tuyen
			// van co kiem ke thiet bi
			if (item.isHaveSaleOrder) {
				//Ngoai tuyen cung kiem ton
				gotoRemainProductView(item);
			} else {
				gotoCreateOrder(item);
			}

			if (item.visitStatus == VISIT_STATUS.VISITED_FINISHED) {
				parent.initCustomerNameOnMenuVisit(item.aCustomer.customerCode,
						item.aCustomer.customerName);
			} else {
				parent.initMenuVisitCustomer(item.aCustomer.customerCode,
						item.aCustomer.customerName);

			}
			parent.removeMenuCloseCustomer();
			parent.removeMenuFinishCustomer();

			if (item.visitStatus == VISIT_STATUS.NONE_VISIT) {
				parent.requestStartInsertVisitActionLog(item);
				item.visitStatus = VISIT_STATUS.VISITING;
			} else {
				saveLastVisitToActionLogProfile(item);
			}
		}

		// currentPage = -1;
		// currentSpinerSelectedItem = DateUtils.getCurrentDay();
		// spinnerLine.setSelection(currentSpinerSelectedItem);
		resetAllValue();
		cusDto = null;
	}

	/**
	 * //check to update EXCEPTION_ORDER_DATE: bien cho phep dat hang khi khoach
	 * cach qua xa
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void requestUpdateExceptionOrderDate(CustomerListItem item) {
		StaffCustomerDTO staffCusDto = new StaffCustomerDTO();
		staffCusDto.staffCustomerId = item.staffCustomerId;
		staffCusDto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		staffCusDto.customerId = item.aCustomer.customerId;
		staffCusDto.shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();

		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_STAFF_DTO, staffCusDto);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.action = ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE;
		e.viewData = data;
		e.isNeedCheckTimeServer = false;
		SaleController.getInstance().handleViewEvent(e);

	}

	private void saveLastVisitToActionLogProfile(CustomerListItem item) {
		ActionLogDTO action = new ActionLogDTO();
		// chu y ko co id cua action_log
		action.id = item.visitActLogId;
		action.aCustomer.customerId = item.aCustomer.customerId;
		action.aCustomer.customerName = item.aCustomer.customerName;
		action.aCustomer.customerCode = item.aCustomer.customerCode;
		action.aCustomer.address = item.aCustomer.address;
		action.aCustomer.channelTypeId = item.aCustomer.channelTypeId;
		action.aCustomer.lat = item.aCustomer.lat;
		action.aCustomer.lng = item.aCustomer.lng;
		action.aCustomer.shopDTO.distanceOrder = item.distanceOrder;
		action.startTime = item.visitStartTime;
		action.endTime = item.visitEndTime;
		action.isOr = item.isOr;
		action.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		action.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLastLatitude();
		action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLastLongtitude();
		action.objectType = "0";
		action.routingId = item.routingId;
		GlobalInfo.getInstance().getProfile().setActionLogVisitCustomer(action);
	}

	/**
	 * Di toi kiem hang ton
	 *
	 * @author : BangHN since : 1.0
	 */
	private void gotoRemainProductView(CustomerListItem item) {
		String customerId = item.aCustomer.getCustomerId();
		String customerCode = item.aCustomer.getCustomerCode();
		String customerName = item.aCustomer.getCustomerName();
		String customerAddress = item.aCustomer.getStreet();
		int customerTypeId = item.aCustomer.getCustomerTypeId();
		int isOr = item.isOr;
		String cashierID = item.aCustomer.cashierStaffID;
		String deliverID = item.aCustomer.deliverID;

		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliverID);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, item);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_REMAIN_PRODUCT_VIEW, SaleController.getInstance());
	}


	public void resetAllValue() {
		spinnerLine.setSelection(DateUtils.getCurrentDay());
		currentSpinerSelectedItem = spinnerLine.getSelectedItemPosition();
		// changeTitleAndSearch(currentSpinerSelectedItem);
		textCustomer = "";
//		textCusCode = "";
		// textCusAdd = "";
//		edMKH.setText(textCusCode);
		edTKH.setText(textCustomer);
		// edCusAdd.setText(textCusAdd);
		// searchAdd.setVisibility(View.GONE);
		currentPage = -1;
		cusDto = null;
	}

	public void changeTitleAndSearch(int index) {
		if (index == 7) {
			parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER_2));
		} else {
			parent.setTitleName(titleString(index));
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbCusList) {

			currentPage = tbCusList.getPagingControl().getCurrentPage();
			getCustomerList(currentPage, 0);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int act, View control, Object data) {
		switch (act) {
		case ActionEventConstant.GO_TO_CUSTOMER_INFO: {
			CustomerListItem item = (CustomerListItem) data;
			currentPage = tbCusList.getPagingControl().getCurrentPage();
			isBack = true;
			gotoCustomerInfo(item.aCustomer.getCustomerId());
			break;
		}
		case ActionEventConstant.VISIT_CUSTOMER: {
			CustomerListItem item = (CustomerListItem) data;
			if (isValidDistanceWhenBeginVisitation(item, visitConfig)) {
				ActionLogDTO action = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				CustomerListItem lastVisitCustomer = GlobalInfo.getInstance()
						.getProfile().getLastVisitCustomer();
				if (lastVisitCustomer != null
						&& action != null
						&& lastVisitCustomer.aCustomer.customerId != item.aCustomer.customerId
						&& lastVisitCustomer.isHaveDisplayProgramNotYetVote
						&& lastVisitCustomer.visitStatus != VISIT_STATUS.VISITED_CLOSED
						&& lastVisitCustomer.visitStatus != VISIT_STATUS.VISITED_FINISHED) {
					SpannableObject span = new SpannableObject();
					span.addSpan(StringUtil
							.getString(R.string.TEXT_REQUIRE_VOTE_DISPLAY_OLD_CUSTOMER));
					if (!StringUtil
							.isNullOrEmpty(action.aCustomer.customerCode)) {
						span.addSpan(
								" "
										+ action.aCustomer.customerCode,
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
					}
					span.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
					if (!StringUtil
							.isNullOrEmpty(action.aCustomer.customerName)) {
						span.addSpan(action.aCustomer.customerName,
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
					}
					parent.showDialog(span.getSpan());
//				} else if (lastVisitCustomer != null
//						&& action != null
//						&& lastVisitCustomer.aCustomer.customerId != item.aCustomer.customerId
//						&& lastVisitCustomer.isOr == 0
//						&& lastVisitCustomer.visitStatus == VISIT_STATUS.VISITING) {
//					SpannableObject span = new SpannableObject();
//					span.addSpan(StringUtil
//							.getString(R.string.TEXT_REQUIRE_INVENT_DEVICE_OLD_CUSTOMER));
//					if (!StringUtil
//							.isNullOrEmpty(action.aCustomer.customerCode)) {
//						span.addSpan(
//								" "
//										+ action.aCustomer.customerCode
//												.substring(0, 3),
//								ImageUtil.getColor(R.color.WHITE),
//								android.graphics.Typeface.BOLD);
//					}
//					span.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
//							android.graphics.Typeface.BOLD);
//					if (!StringUtil
//							.isNullOrEmpty(action.aCustomer.customerName)) {
//						span.addSpan(action.aCustomer.customerName,
//								ImageUtil.getColor(R.color.WHITE),
//								android.graphics.Typeface.BOLD);
//					}
//					parent.showDialog(span.getSpan());
				}  else if (action != null
						&& action.aCustomer.customerId != item.aCustomer.customerId
						&& DateUtils.compareDate(action.startTime,
								DateUtils.now()) == 0
						&& StringUtil.isNullOrEmpty(action.endTime)) {
//					ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
//							.getActionLogVisitCustomer();
					if (action.isOr == 1) {
						parent.requestUpdateActionLog("0", "0", item, this);
					} else {
						// kiem tra neu vao lai dung khach hang do thi khong
						// insertlog
						String endTime = DateUtils.getVisitEndTime(action.aCustomer);
						SpannableObject textConfirmed = new SpannableObject();
						textConfirmed.addSpan(StringUtil
								.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.NORMAL);
						if (!StringUtil
								.isNullOrEmpty(action.aCustomer.customerCode)) {
							textConfirmed.addSpan(
									" "
											+ action.aCustomer.customerCode,
									ImageUtil.getColor(R.color.WHITE),
									android.graphics.Typeface.BOLD);
						}
						textConfirmed.addSpan(" - ",
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
						if (!StringUtil
								.isNullOrEmpty(action.aCustomer.customerName)) {
							textConfirmed.addSpan(action.aCustomer.customerName,
									ImageUtil.getColor(R.color.WHITE),
									android.graphics.Typeface.BOLD);
						}
						textConfirmed.addSpan(" " + StringUtil.getString(R.string.TEXT_IN) +" ",
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.NORMAL);
						textConfirmed.addSpan(
								DateUtils.getVisitTime(action.startTime, endTime),
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
						textConfirmed.addSpan(StringUtil
								.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.NORMAL);

						GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this,
								parent, textConfirmed.getSpan(),
								StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
								ACTION_OK,
								StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
								ACTION_CANCEL, item, true, true);
					}
				} else {
					// processOrder(item);
					// Xu ly cho truong hop ghe tham KH co vi tri, sau do KH bi reset vi tri
					if(item.aCustomer.lat <= 0 || item.aCustomer.lng <= 0) {
						if (lastVisitCustomer != null && lastVisitCustomer.aCustomer != null &&
								lastVisitCustomer.aCustomer.customerId == item.aCustomer.customerId &&
								lastVisitCustomer.aCustomer.lat > 0 && lastVisitCustomer.aCustomer.lng > 0) {
							item.aCustomer.lat = lastVisitCustomer.aCustomer.lat;
							item.aCustomer.lng = lastVisitCustomer.aCustomer.lng;
						}
					}
					if (!StringUtil.isNullOrEmpty(item.draftSaleOrderId)) {
						gotoViewOrder(item.draftSaleOrderId, item);
					} else {
						processOrder(item);
					}
				}
			} else {
				String mess = StringUtil
						.getString(R.string.TEXT_TOO_FAR_FROM_CUS_1)
						+ " "
						+ item.aCustomer.customerName
						+ " - "
						+ item.aCustomer.customerCode
						+ StringUtil
								.getString(R.string.TEXT_TOO_FAR_FROM_CUS_2);

				GlobalUtil.showDialogConfirm(this, parent, mess,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
						ACTION_OK_2, item, false);
			}

			break;
		}
		default:
			break;
		}
	}

	/**
	 * K tra lai khoang cach khi bat dau ghe tham
	 *
	 * @author: TamPQ
	 * @param item
	 * @return: voidvoid
	 * @throws:
	 */
	private boolean isValidDistanceWhenBeginVisitation(CustomerListItem item, GlobalInfo.VistConfig visitConfig) {
		boolean isValid = true;
		if (cusDto != null) {
			if (!item.canOrderException()) {
				item.updateCustomerDistance(cusDto.getDistance());
				isValid = (visitConfig != null 
						&& visitConfig.isAllowVisit(item.isTooFarShop, item.isOr, item.isVisit(), item.aCustomer.isHaveLocation()));		
			}
		} else {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * K tra lai khoang cach de cham trung bay, kiem ton
	 *
	 * @author: TamPQ
	 * @param item
	 * @return: voidvoid
	 * @throws:
	 */
	private boolean isValidDistanceToVoteDisplayProgAndCheckRemain(
			CustomerListItem item) {
		item.updateCustomerDistance(cusDto.getDistance());
		return !item.isTooFarShop;
	}

	/**
	 * di toi fragment danh sach don hang
	 *
	 * @author : BangHN since : 10:43:27 AM
	 */
	private void gotoListOrder() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.GO_TO_LIST_ORDER;
		UserController.getInstance().handleSwitchFragment(e);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spinnerLine) {
			if (currentSpinerSelectedItem != spinnerLine
					.getSelectedItemPosition()) {
				currentSpinerSelectedItem = spinnerLine
						.getSelectedItemPosition();

//				edMKH.setText("");
				edTKH.setText("");
				// edCusAdd.setText("");
//				textCusCode = edMKH.getText().toString().trim();
				textCustomer = edTKH.getText().toString().trim();
				// textCusAdd = edCusAdd.getText().toString().trim();

				// changeTitleAndSearch(currentSpinerSelectedItem);
				isSearchOrSelectSpiner = true;
				getCustomerList(1, 1);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				visitConfig = GlobalInfo.getInstance().getVisitConfig();
				parent.showLoadingDialog();
				isUpdateData = true;
				resetAllValue();
				//reset sort info first
				tbCusList.resetSortInfo();
				getCustomerList(1, 1);
				// demo gui file db.zip va log file len server
				// sendzipFile();
			}
			break;
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			ArrayList<DMSTableRow> rows = tbCusList.getListChildRow();
			if (rows != null && !rows.isEmpty()) {
				try {
					for (DMSTableRow row : rows) {
						CustomerListRow rowCus = (CustomerListRow) row;
						rowCus.reRenderVisitStatus(visitConfig);
					}
				} catch (Exception ex){
					MyLog.e("rerender status list cus", ex.getMessage());
				}
			} else{
				if (!isRequestCusList) {
					getCustomerList(1, 1);
				}
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/**
	 * Den man hinh tao don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoViewOrder(String orderId, CustomerListItem cus) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(cus.aCustomer.customerId));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE,
				cus.aCustomer.customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				cus.aCustomer.getStreet());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				cus.aCustomer.getCustomerName());
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				cus.aCustomer.getCustomerTypeId());
		bundle.putString(IntentConstants.INTENT_IS_OR, String.valueOf(cus.isOr));
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_EDIT);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * Toi man hinh them moi KH
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void gotoListCustomerCreated() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		//get customer list
		getCustomerList(currentPage, 0);
	}


	 /**
	 * Toi man hinh cham key shop
	 * @author: Tuanlt11
	 * @param item
	 * @return: void
	 * @throws:
	*/
	private void goToVoteKeyShop(CustomerListItem item) {
		String customerId = item.aCustomer.getCustomerId();
		String customerCode = item.aCustomer.getCustomerCode();
		String customerName = item.aCustomer.getCustomerName();
		String customerAddress = item.aCustomer.getStreet();
		int customerTypeId = item.aCustomer.getCustomerTypeId();
		int isOr = item.isOr;
		String cashierID = item.aCustomer.cashierStaffID;
		String deliverID = item.aCustomer.deliverID;
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliverID);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, item);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_VOTE_KEY_SHOP, SaleController.getInstance());
	}

	public void getchartStaffDay() {
		Vector<String> vt = new Vector<String>();
		vt.add("staff_id");
		vt.add("2359");
		vt.add("from_date");
		vt.add("01/10/2016");
		vt.add("to_date");
		vt.add("30/11/2016");
		handleViewEvent(vt, ActionEventConstant.GET_CHART_STAFF_DAY, SaleController.getInstance());
	}
}
