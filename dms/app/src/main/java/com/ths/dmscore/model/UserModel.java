/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.lib.network.http.NetworkUtil;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.ListOrgTempDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dms.R;

/**
 * Model cua user
 * @author: BangHN
 * @version: 1.1
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class UserModel extends AbstractModelService {
	protected static volatile UserModel instance;

	protected UserModel() {
	}

	public static UserModel getInstance() {
		if (instance == null) {
			instance = new UserModel();
		}
		return instance;
	}

	public void onReceiveData(HTTPMessage mes) {
		MyLog.i("UserModel", "OnReceive: action:" + mes.getAction());
		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		String msgText = mes.getDataText();
		model.setDataText(msgText);
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		// DMD check null or empty
		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			UserController.getInstance().handleErrorModelEvent(model);
			return;
		}
		// chung thuc thoi gian sai
		if (!StringUtil.isNullOrEmpty(msgText) && msgText.contains(Constants.EXPIRED_TIMESTAMP)) {
			model.setModelCode(ErrorConstants.ERROR_EXPIRED_TIMESTAMP);
			UserController.getInstance().handleErrorModelEvent(model);
			return;
		}

		switch (mes.getAction()) {
		case ActionEventConstant.ACTION_UPDATE_POSITION:{
			MyLog.d("onReceiveData", "ACTION_UPDATE_POSITION");
			try {
				JSONObject json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					MyLog.d("sendLoc2ServerSuccess", "ACTION_UPDATE_POSITION");
					sendLoc2ServerSuccess(actionEvent);
				} else{
					MyLog.d("sendLoc2ServerFail", "ACTION_UPDATE_POSITION errCode: " + errCode);
				}
			} catch (Exception e) {
				MyLog.e("onReceive ACTION_UPDATE_POSITION", "error ", e);
			}
			break;
		}
		case ActionEventConstant.ACTION_LOG_OUT:
		case ActionEventConstant.ACTION_CHECK_REQUEST: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
				MyLog.i("Truong, ACTION_CHECK_REQUEST - respoinse", mes.getDataText());
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					JSONObject response = result.getJSONObject("response");
					if (response != null) {
						int state = response.getInt("state");
						model.setModelData(state);
					}
					UserController.getInstance().handleModelEvent(model);
				} else {
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleErrorModelEvent(model);
				}

			} catch (Exception e) {
				ServerLogger.sendLog("Exception",
						e.getMessage() + "||" + mes.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("Exception", e.getMessage() + "||" + mes.getDataText());
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.RE_LOGIN:
		case ActionEventConstant.ACTION_LOGIN: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");

				int errCode = result.getInt("errorCode");
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					model.setModelCode(errCode);
					UserDTO user = new UserDTO();
					user.parseFromJSONLogin(result.getJSONObject("response"), mes.getAction());
					model.setModelData(user);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleModelEvent(model);
				} else {
					model.setModelCode(errCode);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleErrorModelEvent(model);
				}
			} catch (Exception e) {
				ServerLogger.sendLog("Exception",
						e.getMessage() + "||" + mes.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("Exception", e.getMessage() + "||" + mes.getDataText());
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_DELETED_DB: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");

				int errCode = result.getInt("errorCode");
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					model.setModelCode(errCode);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleModelEvent(model);
				} else {
					model.setModelCode(errCode);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleErrorModelEvent(model);
				}
			} catch (Exception e) {
				ServerLogger.sendLog("Exception",
						e.getMessage() + "||" + mes.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("Exception", e.getMessage() + "||" + mes.getDataText());
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
				model.setModelData(actionEvent.userData);
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					JSONObject jsonResponse = result.getJSONObject("response");
					String currentDateServr = jsonResponse.getString("serverDate");
					ArrayList<String> listOrderId = new ArrayList<String>();
					JSONArray jsonArray = jsonResponse.getJSONArray("haveReturnOrder");
					if (jsonArray != null) {
						for (int i = 0, size = jsonArray.length(); i < size; i++) {
							listOrderId.add(jsonArray.getString(i));
						}
					}
					boolean isSync = false;
					boolean isSuccess = false;
					if (actionEvent.viewData != null) {
						Bundle bundle = (Bundle) actionEvent.viewData;
						isSync = bundle.getBoolean(IntentConstants.INTENT_IS_SYNC, false);
					}
					try {
						// kiem tra thoi gian
						if (isSync) {
							// neu la dong bo offline thi kiem tra o
							// TransactionProcessManager
							Bundle bundle = new Bundle();
							bundle.putString(IntentConstants.INTENT_SERVER_DATE, currentDateServr);
							bundle.putStringArrayList(IntentConstants.INTENT_HAVE_RETURN_ORDER, listOrderId);
							model.setModelData(bundle);
							model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
							UserController.getInstance().handleModelEvent(model);
						} else {
							String currentDateClient = DateUtils.now();
							// kiem tra thoi gian hop le
							SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Date dateServer = formatter.parse(currentDateServr);
							// kiem tra thoi gian khi online
							// neu khac ngay, hoac cung ngay ma chenh lech > 1h
							// --> fail
							Date dateClient = formatter.parse(currentDateClient);
							long secs = (dateServer.getTime() - dateClient.getTime()) / 1000;
							int hours = (int) secs / 3600;

							SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
							Date dateServer1 = formatter1.parse(currentDateServr);
							Date dateClient1 = formatter1.parse(currentDateClient);

							if (dateServer1.compareTo(dateClient1) == 0 && Math.abs(hours) <= 24) {
								// client - server <= 1: ok
								if (hours > -1) {
									// thanh cong
									isSuccess = true;
								}
							}
							if (isSuccess) {
								model.setModelData(ModelEvent.MODEL_RESULT_SUCCESS);
								model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
								UserController.getInstance().handleModelEvent(model);
							} else {
								model.setModelData(ModelEvent.MODEL_RESULT_FAIL_TIME_ONLINE);
								model.setModelCode(ErrorConstants.ERROR_COMMON);
								UserController.getInstance().handleErrorModelEvent(model);
							}
						}

					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
						model.setModelData(ModelEvent.MODEL_RESULT_COMMON_FAIL);
						model.setModelCode(ErrorConstants.ERROR_COMMON);
						UserController.getInstance().handleErrorModelEvent(model);
					}

				} else {
					if (errCode != ErrorConstants.ERROR_SESSION_RESET) {
						model.setModelCode(ErrorConstants.ERROR_COMMON);
					}
					model.setModelData(ModelEvent.MODEL_RESULT_COMMON_FAIL);
					UserController.getInstance().handleErrorModelEvent(model);
				}
			} catch (JSONException e) {
				model.setModelData(ModelEvent.MODEL_RESULT_COMMON_FAIL);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			} catch (Exception e) {
				model.setModelData(ModelEvent.MODEL_RESULT_COMMON_FAIL);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.CHANGE_PASS: {
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");

				int errCode = result.getInt("errorCode");
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					model.setModelCode(errCode);
					int response = result.getInt("response");
					model.setModelData(response);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleModelEvent(model);
				} else {
					model.setModelCode(errCode);
					model.setModelMessage(result.getString("errorMessage"));
					UserController.getInstance().handleErrorModelEvent(model);
				}
			} catch (Exception e) {
				ServerLogger.sendLog("Exception",
						e.getMessage() + "||" + mes.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("Exception", e.getMessage() + "||" + mes.getDataText());
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.ACTION_RETRY_REQUEST:
		case ActionEventConstant.UPLOAD_MEDIA_INVENTORY_DEVICE:
		case ActionEventConstant.RETRY_UPLOAD_PHOTO:
		case ActionEventConstant.RETRY_UPLOAD_ATTACH_FILE:
		case ActionEventConstant.REQUEST_UPDATE_POSITION:
			int errCode = ErrorConstants.ERROR_COMMON;
			try {
				JSONObject json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			} finally {
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
					// TH mac dinh la request create/update/delete du lieu
					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
					UserController.getInstance().handleModelEvent(model);
				} else if (errCode == ErrorConstants.ERROR_UNIQUE_CONTRAINTS) {
					// request loi trung khoa -- khong thuc hien goi lai len
					// server nua
					UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_UNIQUE_CONTRAINTS);
				} else if (errCode == ErrorConstants.ERROR_WRONG_FORMAT) {
					UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_WRONG_FORMAT);
				} else if (errCode == ErrorConstants.ERROR_DISPLAY_PROGRAM_ID) {
					UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_OTHER_NOT_RETRY);
				} else if (errCode == ErrorConstants.ERROR_GCM_NOT_REGISTER) {
					UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_OTHER_NOT_RETRY);
				}
				// 1. luong fullDate don hang sau chot kho val doi thanh chot ngay
				// 2. cap nhat thanh toan len server van thuc hien den 12h
				// sau 12h la dong bo dau ngay
//				else if (errCode == ErrorConstants.ERROR_CLOSING_STOCK_VANSALE){
//					UserController.getInstance().handleErrorModelEvent(model);
//					// cap nhat approved don hang va stock
//					updateStateApprovedOfOrder(actionEvent);
//					// nhung don hang huy khong thuc hien gui len server nua
//					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
//				} else if (errCode == ErrorConstants.ERROR_DEBIT_AFTER_LOCK_VANSALE) {
//					UserController.getInstance().handleErrorModelEvent(model);
//					// khong thuc hien gui lai no len server
//					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
//					// cap nhat lai no
//					updateDebit(actionEvent);
//				}

				// loi thiet bi dang giao dich
				else if (errCode == ErrorConstants.ERROR_EQUIPMENT_IN_TRANSACTION) {
					UserController.getInstance().handleErrorModelEvent(model);
					// cap nhat log loi khong fullDate len server
					updateLog(actionEvent, LogDTO.STATE_OTHER_NOT_RETRY);
					// cap nhat status cua equip_lost_mobile
					updateEquipLostMobileStatus(model);
				}
				else {
					UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_FAIL);
				}
			}
			break;
		default:
			int errCodeDefault = ErrorConstants.ERROR_COMMON;
			try {
				JSONObject json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				errCodeDefault = result.getInt("errorCode");
				model.setModelCode(errCodeDefault);
				model.setModelData(actionEvent.userData);
			} catch (Exception e) {
				ServerLogger.sendLog("Exception",
						e.getMessage() + "||" + mes.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("Exception", e.getMessage() + "||" + mes.getDataText());
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			} finally {
				// thanh cong, that bai khong gui tra kq ve view nua
				if (errCodeDefault == ErrorConstants.ERROR_CODE_SUCCESS) {
					// TH mac dinh la request create/update/delete du lieu
					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
					// UserController.getInstance().handleModelEvent(model);
				} else if (errCodeDefault == ErrorConstants.ERROR_UNIQUE_CONTRAINTS) {
					// request loi trung khoa -- khong thuc hien goi lai len
					// server nua
					// UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_UNIQUE_CONTRAINTS);
				} else {
					// ghi log loi len server
					updateLog(actionEvent, LogDTO.STATE_FAIL);
					// UserController.getInstance().handleErrorModelEvent(model);
				}
			}
		}
	}

	public void onReceiveError(HTTPResponse response) {
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		ModelEvent model = new ModelEvent();
		String msgText = response.getDataText();
		model.setDataText(msgText);
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (actionEvent.action == ActionEventConstant.ACTION_LOGIN) {
			GlobalInfo.getInstance().getProfile().getUserData().setLoginState(UserDTO.NOT_LOGIN);
			// chung thuc thoi gian sai
			if (!StringUtil.isNullOrEmpty(msgText) && msgText.contains(Constants.EXPIRED_TIMESTAMP)) {
				model.setModelCode(ErrorConstants.ERROR_EXPIRED_TIMESTAMP);
			} else {
				model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			}
			model.setModelMessage(response.getErrMessage());
			UserController.getInstance().handleErrorModelEvent(model);
		} else if (GlobalUtil.checkActionSave(actionEvent.action) && actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO log = (LogDTO) actionEvent.logData;
			if (LogDTO.STATE_NONE.equals(log.state)) {
				updateLog(actionEvent, LogDTO.STATE_NEW);
			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			UserController.getInstance().handleErrorModelEvent(model);
		} else if (actionEvent.action == ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER) {
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_SERVER_DATE, String.valueOf(ErrorConstants.ERROR_NO_CONNECTION));
			bundle.putBoolean(IntentConstants.INTENT_HAVE_RETURN_ORDER, false);
			model.setModelData(bundle);

			model.setModelData(bundle);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			UserController.getInstance().handleModelEvent(model);

		} else {
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelMessage(response.getErrMessage());
			UserController.getInstance().handleErrorModelEvent(model);
		}
	}

	@Override
	public Object requestHandleModelData(ActionEvent e) throws Exception {
		Object data = null;
		switch (e.action) {
		case ActionEventConstant.ACTION_LOGIN:
			data = requestLoginHTTP(e);
			break;
		case ActionEventConstant.RE_LOGIN:
			data = requestLoginHTTP(e);
			break;
		case ActionEventConstant.ACTION_RETRY_REQUEST:
		case ActionEventConstant.REQUEST_UPDATE_POSITION:
			data = requestRetry(e);
			break;
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER:
			data = getCurrentDateTimeServer(e);
			break;
		case ActionEventConstant.UPDATE_NEW_URL_TO_DB:
			data = requestUdateNewLink(e);
			break;
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER:
			data = requestUploadPhoto(e);
			break;
		case ActionEventConstant.RETRY_UPLOAD_PHOTO:
			data = requestRetryUploadPhoto(e);
			break;
		case ActionEventConstant.RETRY_UPLOAD_ATTACH_FILE:
			data = requestRetryUploadAttachFile(e);
			break;
		case ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE:
			data = requestDeleteOldLogTable(e);
			break;
		case ActionEventConstant.UPLOAD_DB_FILE: {
			data = uploadDBFile(e);
			break;
		}
		case ActionEventConstant.CHECK_USER_DB: {
			data = requestCheckUserDB(e);
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_DELETED_DB: {
			data = requestUpdateDeletedDB(e);
			break;
		}
		case ActionEventConstant.CHANGE_PASS: {
			data = requestChangePass(e);
			break;
		}
		case ActionEventConstant.GET_STAFF_TYPE: {
			data = requestStaffType(e);
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_POSITION: {
			data = requestSendPositionLog(e);
			break;
		}
		case ActionEventConstant.UPLOAD_MEDIA_INVENTORY_DEVICE: {
			data = requestRetryUploadPhotoInventory(e);
			break;
		}
		case ActionEventConstant.GET_APPARAM:
			data = requestGetAppDefineConstant(e);
			break;
		case ActionEventConstant.GET_SHOP_PARAMs:
			data = requestGetShopParams(e);
			break;
		case ActionEventConstant.GET_DATA_FROM_ORG_ACCESS:
			data  = getDataFromOrgAcces(e);
			break;
		case ActionEventConstant.GET_ATTENDANCE_TIME_DEFINE:
			data = getAttendaceTimeDefine(e);
			break;
		case ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION:
			data = requestReadAllowResetLocation(e);
			break;
		default:
			break;
		}
		return data;
	}

	/**
	 * Cap nhat vao bang log
	 *
	 * @author: TruongHN
	 * @param actionEvent
	 * @param newState
	 * @return: void
	 * @throws:
	 */
	public void updateStateApprovedOfOrder(ActionEvent actionEvent) {
		if (GlobalUtil.checkActionSave(actionEvent.action) && actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO logDTO = (LogDTO) actionEvent.logData;
			if (logDTO != null && !StringUtil.isNullOrEmpty(logDTO.logId)) {
				if (logDTO.tableType == LogDTO.TYPE_ORDER
						&& !StringUtil.isNullOrEmpty(logDTO.tableId)) {
					SQLUtils.getInstance().updateStateApprovedOfOrder(logDTO, 4); // don huy sau chot vansale approved = 4
					logDTO.tableType = LogDTO.TYPE_NORMAL;
				}
			}
		}
	}

	/**
	 * cap nhat lai status cua EquipLostMobile
	 * @author: DungNX
	 * @param model
	 * @return: void
	 * @throws:
	*/
	private void updateEquipLostMobileStatus(ModelEvent model){
		if(model.getModelCode() == ErrorConstants.ERROR_EQUIPMENT_IN_TRANSACTION){
			LogDTO logDTO = (LogDTO) model.getActionEvent().logData;
			if(logDTO != null && !StringUtil.isNullOrEmpty(logDTO.logId)){
				try {
					JSONObject query = new JSONObject(logDTO.value);
					JSONArray listSql = new JSONArray(query.getString("listSql"));
					String equipLostMobileID = null;
					for(int i=0; i< listSql.length(); i++){
						JSONObject temp = listSql.getJSONObject(i);
						String tableName = temp.getString("tableName");
						if(tableName.equals("EQUIP_LOST_MOBILE_REC")){
							JSONArray listSql1 = new JSONArray(temp.getString("listParam"));
							for(int j = 0; j < listSql1.length(); j++){
								JSONObject temp1 = listSql1.getJSONObject(j);
								String columnName = temp1.getString("column");
								if(columnName.equals("EQUIP_LOST_MOBILE_REC_ID")){
									equipLostMobileID = temp1.getString("value");
									break;
								}
							}
						}
						if(!StringUtil.isNullOrEmpty(equipLostMobileID)) {
							break;
						}
					}
					if(!StringUtil.isNullOrEmpty(equipLostMobileID)) {
						SQLUtils.getInstance().updateEquipLostMobileStatus(equipLostMobileID);
					}
				} catch (JSONException e) {
					MyLog.e("updateEquipLostMobileStatus", "fail", e);
				}
			}
		}
	}

	/**
	 * Request login http
	 *
	 * @author: DoanDM
	 * @param actionEvent
	 * @return: HTTPRequest
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	public ModelEvent requestLoginHTTP(ActionEvent actionEvent) throws Exception {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		Vector info = (Vector) actionEvent.viewData;
		re = sendHttpRequest("authController/login", info, 
				actionEvent, NetworkTimeout.getCONNECT_TIME_OUT_LOGIN(), NetworkTimeout.getREAD_TIME_OUT_LOGIN(), true);
		return new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Retry lai request
	 *
	 * @author: TruongHN
	 * @param actionEvent
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent requestRetry(ActionEvent actionEvent) throws Exception {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		re = sendHttpRequest("queryController/executeSql", actionEvent.viewData.toString(),
				actionEvent);
		return new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ngay hien tai tren server
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: HTTPRequest
	 * @throws:
	 */
	public ModelEvent getCurrentDateTimeServer(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		HTTPRequest re = null;
		// kiem tra network
		if (GlobalUtil.checkNetworkAccess()) {
			// neu co mang, lay thoi gian hien tai tren server
			e.type = ActionEvent.TYPE_ONLINE;
			Vector<Object> para = new Vector<Object>();
			para.add(IntentConstants.INTENT_STAFF_ID);
			para.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
			para.add(IntentConstants.INTENT_ROLE_TYPE);
			para.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType());
			para.add(IntentConstants.INTENT_SHOP_ID);
			para.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			para.add(IntentConstants.INTENT_ROLE_ID);
			para.add(GlobalInfo.getInstance().getProfile().getUserData().getRoleId());
			re = sendHttpRequest("synDataController/getServerInfo", para, e);
			// xet model data
			model.setModelData(re);
		} else {
			// thuc hien offline
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_SERVER_DATE,
					String.valueOf(ErrorConstants.ERROR_NO_CONNECTION));
			bundle.putBoolean(IntentConstants.INTENT_HAVE_RETURN_ORDER, false);
			model.setModelData(bundle);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}

		return model;
	}

	/**
	 * Request upload photo
	 *
	 * @author: PhucNT
	 * @param e
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent requestUploadPhoto(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		MediaItemDTO mediaDTO = (MediaItemDTO) e.userData;

		long mediaId = SQLUtils.getInstance().insertMediaItem(mediaDTO);
		if (mediaId > 0) {

			// JSONArray sqlPara = mediaDTO.generateInsertMediaItem();

			String logId = GlobalUtil.generateLogId();
			e.logData = logId;
			// send to server
			// Vector<Object> para = new Vector<Object>();
			// para.add(IntentConstants.INTENT_LIST_SQL);
			// para.add(sqlPara);
			// para.add(IntentConstants.INTENT_LOG_ID);
			// para.add(logId);
			// para.add(IntentConstants.INTENT_STAFF_ID_PARA);
			// para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
			// para.add(IntentConstants.INTENT_IMEI_PARA);
			// para.add(GlobalInfo.getInstance().getDeviceIMEI());

			Vector<String> data = (Vector<String>) e.viewData;
			data.add(IntentConstants.INTENT_IMAGE_ID);
			data.add(Long.toString(mediaId));
			data.add(IntentConstants.INTENT_CYCLE_ID);
			data.add(Constants.STR_BLANK + mediaDTO.cycleId);

			// ObjectMapper mapper = new ObjectMapper();
			// String str = mapper.writeValueAsString(mediaDTO);
			//
			// data.add(IntentConstants.INTENT_MEDIA_ITEM_OBJECT);
			// data.add(str);

			// avatar
			String imgPath = data.get(
					data.lastIndexOf(IntentConstants.INTENT_TAKEN_PHOTO) + 1)
					.toString();

			if (StringUtil.isNullOrEmpty(imgPath)) {
				// remove avatar
				// if
				// (bundle.containsKey(IntentConstants.INTENT_REMOVE_AVATAR))
				// {
				// userInfo.add(IntentConstants.INTENT_REMOVE_AVATAR);
				// userInfo.add(bundle
				// .getString(IntentConstants.INTENT_REMOVE_AVATAR));
				// }
				// re = sendHttpRequest("profile.updateUserProfile2",
				// userInfo, e);
			} else {
				httpMultiPartRequest("mediaController/addImage", data, imgPath, e,
						"fileName.jpg", NetworkUtil.getPHOTO(), NetworkUtil.getJPEG());

			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);

		} else {
			ServerLogger.sendLog("Insert mot hinh anh bi that bai "
					+ mediaDTO.id, TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(mediaId);
		return model;
	}

	/**
	 * Retry upload photo
	 *
	 * @author: PhucNT
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestRetryUploadPhoto(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		HTTPRequest re = null;
		try {
			String imgPath = "";
			LogDTO log = (LogDTO) e.logData;
			// tim duong dan image path.
			if (!StringUtil.isNullOrEmpty(log.tableId)) {
				imgPath = log.tableId;
			}
			re = httpMultiPartRequest("mediaController/addImage",
					e.viewData.toString(), imgPath, e, "fileName.jpg",
					NetworkUtil.getPHOTO(), NetworkUtil.getJPEG());
		} catch (Exception ex) {
			MyLog.e("requestRetryUploadPhoto", "fail", ex);
		}
		return new ModelEvent(e, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * retry upload attach file
	 * @author: duongdt3
	 * @since: 09:16:36 26 Aug 2015
	 * @return: ModelEvent
	 * @throws:  
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent requestRetryUploadAttachFile(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		HTTPRequest re = null;
		try {
			String imgPath = "";
			LogDTO log = (LogDTO) e.logData;
			// tim duong dan image path.
			if (!StringUtil.isNullOrEmpty(log.tableId)) {
				imgPath = log.tableId;
			}
			File file = new File(imgPath);
			String fileName = file.getName();
			re = httpMultiPartRequest("mediaController/addAttachFile",
					e.viewData.toString(), imgPath, e, fileName,
					NetworkUtil.getPHOTO(), NetworkUtil.getJPEG());
		} catch (Exception ex) {
			MyLog.e("requestRetryUploadPhoto", "fail", ex);
		}
		return new ModelEvent(e, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * request addEquipAttachFile
	 * @author: DungNX
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	*/
	public ModelEvent requestRetryUploadPhotoInventory(ActionEvent e) {
		HTTPRequest re = null;
		e.type = ActionEvent.TYPE_ONLINE;
		try {
			String imgPath = "";
			LogDTO log = (LogDTO) e.logData;
			// tim duong dan image path.
			if (!StringUtil.isNullOrEmpty(log.tableId)) {
				imgPath = log.tableId;
			}
			re = httpMultiPartRequest("mediaController/addEquipAttachFile",
					e.viewData.toString(), imgPath, e, "fileName.jpg",
					NetworkUtil.getPHOTO(), NetworkUtil.getJPEG());
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return new ModelEvent(e, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	@SuppressWarnings("unchecked")
	public ModelEvent uploadDBFile(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		HTTPRequest re = null;
		Vector<String> vt = (Vector<String>) e.viewData;
		// tim duong dan image path.
		String filePath = vt.elementAt(1);
		re = httpMultiPartRequest("mediaController/uploadLogFile", filePath, e,
				"VinamilkDatabase.zip", NetworkUtil.getPHOTO(), NetworkUtil.getJPEG());
		return new ModelEvent(e, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * update new link url
	 *
	 * @author: PhucNT
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestUdateNewLink(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		// insert to sql Lite & request to server
		MediaItemDTO mediaDTO = (MediaItemDTO) e.viewData;
		int mediaId = SQLUtils.getInstance().updateNewLinkPhoto(mediaDTO);
		if (mediaId > 0) {
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("Update link mot hinh anh bi that bai "
					+ mediaDTO.id, TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(Long.valueOf(mediaId));
		return model;
	}

	/**
	 * Request delete nhung bang ghi log cu truoc 40 ngay
	 *
	 * @author banghn
	 * @param e
	 */
	public ModelEvent requestDeleteOldLogTable(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		long result = SQLUtils.getInstance().deleteOldLogTable();
		model.setModelData(result);
		if (result > 0) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		return model;
	}

	/**
	 * Request kiem tra db co phai cua user dang dang nhap hay kg?
	 *
	 * @author hieunh
	 * @param e
	 */
	public ModelEvent requestCheckUserDB(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		String staffCode = data.getString(IntentConstants.INTENT_STAFF_CODE);
		int roleType = data.getInt(IntentConstants.INTENT_ROLE_TYPE);
		boolean result = SQLUtils.getInstance()
				.checkUserDB(staffCode, roleType);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * request update sau khi deleted thanh cong
	 *
	 * @author banghn
	 * @param actionEvent
	 * @return
	 */
	public ModelEvent requestUpdateDeletedDB(ActionEvent actionEvent)
			throws Exception {
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		HTTPRequest re = null;
		@SuppressWarnings("unchecked")
		Vector<String> info = (Vector<String>) actionEvent.viewData;
		re = sendHttpRequest("synDataController/updateStaffSynDataStatus", info, actionEvent);
		return new ModelEvent(actionEvent, re, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 * doi pass
	 *
	 * @author: TamPQ
	 * @param e
	 * @return
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestChangePass(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		HTTPRequest re = null;
		Vector<?> info = (Vector<?>) e.viewData;
		re = sendHttpRequest("authController/changePassword", info, e,
				NetworkTimeout.getCONNECT_TIME_OUT(), NetworkTimeout.getREAD_TIME_OUT(), true);
		return new ModelEvent(e, re, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestStaffType(ActionEvent e) throws Exception {
		// insert to sql Lite & request to server
		int staffTypeId = (Integer) e.viewData;
		String staffType = SQLUtils.getInstance().requestStaffType(staffTypeId);
		return new ModelEvent(e, staffType, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Request dang ky registerId GCM
	 * @author: dungdq3
	 * @since: 10:12:47 AM Feb 10, 2014
	 * @return: HTTPRequest
	 * @throws:
	 * @param registrationId
	 */
	public HTTPRequest requestRegisterId(String registrationId) {
		// TODO Auto-generated method stub
		String logId = GlobalUtil.generateLogId();
		ActionEvent e = new ActionEvent();
		e.logData = logId;
 		Vector<Object> para = new Vector<Object>();
// 		para.add(IntentConstants.INTENT_SHOP_ID);
// 		para.add( GlobalInfo.getInstance().getProfile().getUserData().shopId);
//		para.add(IntentConstants.INTENT_STAFF_ID);
//		para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
		para.add(IntentConstants.INTENT_REGID);
		para.add(registrationId);
//		HTTPRequest request= sendHttpRequest("gcm.registerStaffKey", para, e);
		return new HTTPRequest();
	}

	/**
	 * Request change password
	 * @author banghn
	 * @param e
	 * @return
	 */
	public ModelEvent requestSendPositionLog(ActionEvent e) throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		@SuppressWarnings("rawtypes")
		Vector info = (Vector) e.viewData;
		sendHttpRequest("queryController/executeSql", info, e, 
				NetworkTimeout.getCONNECT_TIME_OUT_LOGIN(), NetworkTimeout.getREAD_TIME_OUT_LOGIN(), false);
		return new ModelEvent(e, null, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Execute DB Script
	 *
	 * @author: ThangNV31
	 * @param actionEvent
	 */
	public ModelEvent executeDBScript(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		@SuppressWarnings("unchecked")
		List<String> listSciprt = (List<String>) actionEvent.viewData;
		boolean execSuccessfully = SQLUtils.getInstance().executeDBScripts(
				listSciprt);
		if (execSuccessfully) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			model.setModelMessage("Execute DB Script Fail!");
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		return model;
	}

	/**
	 * get app define constant
	 *
	 * @author: dungdq3
	 * @since: 8:15:13 AM Oct 14, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 * @throws Exception
	 */
	private ModelEvent requestGetAppDefineConstant(ActionEvent e) throws Exception {
		ArrayList<ApParamDTO> result = SQLUtils.getInstance()
				.getAppDefineConstant();
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * request Get Shop Params
	 * @author: duongdt3
	 * @since: 15:00:24 4 Apr 2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private ModelEvent requestGetShopParams(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ArrayList<ShopParamDTO> result = SQLUtils.getInstance().getShopParams(data);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin cham cong
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @return: Object
	 * @throws:
	*/
	public ModelEvent getAttendaceTimeDefine(ActionEvent e) throws Exception {
		List<ShopParamDTO> dto = SQLUtils.getInstance().getAttendaceTimeDefine(
				(Bundle) e.viewData);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * cho phep reset location hay ko
	 *
	 * @author: dungdq3
	 * @since: 8:27:11 AM Oct 14, 2014
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return:
	 * @throws Exception
	 */
	private ModelEvent requestReadAllowResetLocation(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		ApParamDTO result = SQLUtils.getInstance()
				.getAllowResetLocation(b);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay data tu org access
	 *
	 * @author: dungdq3
	 * @since: 4:22:21 PM Mar 18, 2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private ModelEvent getDataFromOrgAcces(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle b = (Bundle) e.viewData;
		ListOrgTempDTO result = SQLUtils.getInstance()
				.getDataFromOrgAcces(b);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * send location to sercer success
	 */
	private void sendLoc2ServerSuccess(ActionEvent actionEvent) {
		try {
			if (actionEvent != null) {
				MyLog.d("send Location 2 Server Success", "sendLoc2ServerSuccess");
				Bundle extras = new Bundle();
				extras.putString(IntentConstants.INTENT_DATA, actionEvent.createDate);
				((GlobalBaseActivity) GlobalInfo.getInstance().getActivityContext()).sendBroadcast(
						ActionEventConstant.ACTION_SEND_POSITION_TO_SERVER_SUCCESS, extras);
			}
		} catch (Exception e) {
			MyLog.e("send Location 2 Server Success", "fail", e);
		}
	}
}
