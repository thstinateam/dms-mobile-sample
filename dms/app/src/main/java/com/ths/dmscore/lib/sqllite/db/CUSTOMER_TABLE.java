/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.db.RptStaffSaleDetailDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.CustomerCatAmountPopupDTO;
import com.ths.dmscore.dto.view.CustomerCatAmountPopupViewDTO;
import com.ths.dmscore.dto.view.CustomerInfoDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerNotPsdsInMonthReportDTO;
import com.ths.dmscore.dto.view.CustomerSaleSKUDTO;
import com.ths.dmscore.dto.view.CustomerVisitedViewDTO;
import com.ths.dmscore.dto.view.DynamicCustomerDTO;
import com.ths.dmscore.dto.view.DynamicCustomerListDTO;
import com.ths.dmscore.dto.view.EquipStatisticRecordDTO;
import com.ths.dmscore.dto.view.ImageListDTO;
import com.ths.dmscore.dto.view.ImageListItemDTO;
import com.ths.dmscore.dto.view.NewCustomerListDTO;
import com.ths.dmscore.dto.view.SupervisorCustomerListDTO;
import com.ths.dmscore.dto.view.WrongPlanCustomerDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.AreaItem;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.CustomerType;
import com.ths.dmscore.dto.view.GsnppLessThan2MinsDTO;
import com.ths.dmscore.dto.view.GsnppLessThan2MinsDTO.LessThan2MinsItem;
import com.ths.dmscore.dto.view.ImageSearchViewDTO;
import com.ths.dms.R;

/**
 * Luu thong tin khach hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_TABLE extends ABSTRACT_TABLE {
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// ma khach hang
	public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
	// code rut gon
	public static final String SHORT_CODE = "SHORT_CODE";
	public static final String FIRST_CODE = "FIRST_CODE";
	// ten khach hang
	public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// ?
	public static final String GROUP_TRANSFER_ID = "GROUP_TRANSFER_ID";
	public static final String CASHIER_STAFF_ID = "CASHIER_STAFF_ID";
	// cong no cho phep toi da
	public static final String MAX_DEBIT_AMOUNT = "MAX_DEBIT_AMOUNT";
	// so ngay toi da cho phep no
	public static final String MAX_DEBIT_DATE = "MAX_DEBIT_DATE";
	// cho phep no
	public static final String APPLY_DEBIT_LIMITED = "APPLY_DEBIT_LIMITED";
	// ma vung
	public static final String AREA_ID = "AREA_ID";
	// duong
	public static final String STREET = "STREET";
	// so nha
	public static final String HOUSENUMBER = "HOUSENUMBER";
	// dia chi full
	public static final String ADDRESS = "ADDRESS";
	// dien thoai
	public static final String PHONE = "PHONE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// truong nay chua dung
	// public static final String POSTAL_CODE = "POSTAL_CODE";
	// nguoi lien he
	public static final String CONTACT_NAME = "CONTACT_NAME";
	// truong nay chua dung
	// public static final String TAX_CODE = "TAX_CODE";
	// so di dong
	public static final String MOBIPHONE = "MOBIPHONE";
	// khu vuc 1: thanh thi, 2: nong thon
	public static final String LOCATION = "LOCATION";
	// loai kenh
	public static final String CHANNEL_TYPE_ID = "CHANNEL_TYPE_ID";
	// do trung thanh
	public static final String LOYALTY = "LOYALTY";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// loai chuong trinh tham gia
	public static final String DISPLAY = "DISPLAY";
	// don hang cuoi cung duoc duyet
	public static final String LAST_APPROVE_ORDER = "LAST_APPROVE_ORDER";
	// don hang cuoi cung tao
	public static final String LAST_ORDER = "LAST_ORDER";
	// status
	public static final String STATUS = "STATUS";
	// lat
	public static final String LAT = "LAT";
	// lng
	public static final String LNG = "LNG";

	public static final int AREA_TYPE_PROVINE = 1;
	public static final int AREA_TYPE_DISTRICT = 2;
	public static final int AREA_TYPE_PRECINCT = 3;
	// Lay hang tu dau
	public static final String ORDER_SOURCE = "ORDER_SOURCE";
	public static final String EMAIL = "EMAIL";
	public static final String FAX = "FAX";
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	public static final String NAME_TEXT = "NAME_TEXT";
	public static final String SALE_POSITION_ID = "SALE_POSITION_ID";
	public static final String BIRTHDAY = "BIRTHDAY";

	public static final String CUSTOMER_TABLE = "CUSTOMER";

	public CUSTOMER_TABLE(SQLiteDatabase mDB) {
		this.tableName = CUSTOMER_TABLE;

		this.columns = new String[] { CUSTOMER_ID, SHORT_CODE, CUSTOMER_CODE,
				CUSTOMER_NAME, ADDRESS, SHOP_ID, AREA_ID, STREET, HOUSENUMBER,
				PHONE, CREATE_DATE, UPDATE_DATE, CONTACT_NAME, MOBIPHONE,
				LOCATION, CHANNEL_TYPE_ID, LOYALTY, CREATE_USER, UPDATE_USER,
				DISPLAY, LAST_APPROVE_ORDER, LAST_ORDER, STATUS, LAT, LNG,
				SYN_STATE, MAX_DEBIT_AMOUNT, MAX_DEBIT_DATE, EMAIL, FAX, SALE_POSITION_ID, BIRTHDAY };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((CustomerDTO) dto);
		return insert(null, value);
	}

	/**
	 * Cap nhat customer
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public int update(CustomerDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.getCustomerId() };
		return update(value, CUSTOMER_ID + " = ?", params);
	}

	public long update(AbstractTableDTO dto) {
		CustomerDTO cusDTO = (CustomerDTO) dto;
		ContentValues value = initDataRow(cusDTO);
		String[] params = { "" + cusDTO.getCustomerId() };
		return update(value, CUSTOMER_ID + " = ?", params);
	}

	/**
	 * update vi tri cua khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	public long updateLocation(CustomerDTO dto) {
		ContentValues value = initCustomerLocationData(dto);
		String[] params = { "" + dto.getCustomerId() };
		return update(value, CUSTOMER_ID + " = ?", params);
	}

	/**
	 * Cap nhat khi tao moi order
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long updateLastOrder(AbstractTableDTO dto) {
		CustomerDTO cusDTO = (CustomerDTO) dto;
		ContentValues value = initDataUpdateFromOrder(cusDTO);
		String[] params = { "" + cusDTO.getCustomerId() };
		return update(value, CUSTOMER_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: HieuNH
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(CUSTOMER_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		CustomerDTO cusDTO = (CustomerDTO) dto;
		String[] params = { cusDTO.getCustomerId() };
		return delete(CUSTOMER_ID + " = ?", params);
	}

	/**
	 * Xoa KH
	 *
	 * @author: dungdq3
	 * @since: 1:44:55 PM Sep 22, 2014
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return:
	 */
	public long deleteCustomer(CustomerDTO dto) {
		dto.setStatus(-1);
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, dto.getStatus());
		String[] params = { "" + dto.getCustomerId() };
		return update(editedValues, CUSTOMER_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: BangHN
	 * @param id
	 * @return: CustomerDTO
	 * @throws:
	 */
	public CustomerDTO getCustomerById(String id) throws Exception {
		CustomerDTO CustomerDTO = null;
		Cursor c = null;
			String[] params = { id };
			c = query(CUSTOMER_ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					CustomerDTO = initLogDTOFromCursor(c);
				}
			}


			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		return CustomerDTO;
	}

	/**
	 * Lay thong tin trong CUSTOMER & CUSTOMER_TYPE
	 *
	 * @param inheritId
	 * @return: CustomerDTO
	 * @throws:
	 */
	public CustomerInfoDTO getCustomerBaseInfo(String customerId)
			throws Exception {
		Cursor cursor = null;
		CustomerInfoDTO customerInfo = new CustomerInfoDTO();
		StringBuilder sql = new StringBuilder();
		List<String> params = new ArrayList<String>();
		sql.append(" select c.*, ct.*");
		sql.append(" from customer c, customer_type ct");
		sql.append(" where c.customer_id = ?");
		sql.append(" and c.CUSTOMER_TYPE_ID = ct.CUSTOMER_TYPE_ID");

		params.add(customerId);
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					customerInfo.parseCustomerInfo(cursor, 1);
				}
			}
		} catch (Exception ex) {
			MyLog.e("", ex.toString());
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return customerInfo;
	}

	/**
	 * Lay thong tin trong CUSTOMER & CUSTOMER_TYPE & SKU THANG, NUMBER ORDER
	 * THANG
	 *
	 * @param inheritId
	 * @return: CustomerDTO
	 * @throws:
	 */
	public CustomerInfoDTO getCustomerInfo(String customerId, String shopId, int sysCalUnApproved, int sysCurrencyDivide)
			throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		Cursor cursor = null;
		CustomerInfoDTO customerInfo = new CustomerInfoDTO();
		ArrayList<String> params = new ArrayList<String>();
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
//		boolean isStaffSale = (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_STAFF ||
//									GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES ) ;
		boolean isStaffSale = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF  ;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT C.customer_id, ");
		sql.append("       C.short_code as CUSTOMER_CODE, ");
		sql.append("       C.shop_id, ");
		sql.append("       C.customer_name, ");
		sql.append("       C.housenumber, ");
		sql.append("       C.street, ");
		sql.append("       C.address, ");
		sql.append("       C.phone, ");
		sql.append("       C.MOBIPHONE, ");
		sql.append("       C.CONTACT_NAME, ");
		sql.append("       C.lat, ");
		sql.append("       C.lng, ");
		sql.append("       CT.channel_type_id, ");
		sql.append("       CT.channel_type_code, ");
		sql.append("       CT.channel_type_name, ");
		// sql.append("       (SELECT Count(1) AS COUNT ");
		// sql.append("        FROM   (SELECT Date(a.order_date), ");
		// sql.append("                       Sum(CASE ");
		// sql.append("                             WHEN a.order_type = 'CM' THEN -a.total ");
		// sql.append("                             WHEN a.order_type = 'CO' THEN -a.total ");
		// sql.append("                             ELSE a.total ");
		// sql.append("                           END) AS tong ");
		// sql.append("                FROM   sale_order a ");
		// sql.append("                WHERE  a.customer_id = ? ");
		// sql.append("                       AND ( ( a.approved = 1 ");
		// sql.append("                               AND Date(a.order_date) >= ");
		// sql.append("                                   ? ) ");
		// sql.append("                              OR ( a.approved != 2 ");
		// sql.append("                                   AND Date(a.order_date) = ");
		// sql.append("                                       ? ) ");
		// sql.append("                           ) ");
		// sql.append("                GROUP  BY Date(a.order_date)) ");
		// sql.append("        WHERE  tong > 0)                            AS NUM_ORDER_IN_MONTH, ");
		sql.append("       (select count( t2.ngay)  ");
		sql.append("       		from( ");
		sql.append("       				select dayInOrder(s1.order_date) ngay,  count(s1.sale_order_id) ");
		sql.append("       				from sale_order s1  ");
		sql.append("       				where s1.customer_id = ? ");
		params.add(customerId);
		sql.append("       					and shop_id= ? ");
		params.add(shopId);
		sql.append("       					and s1.type = 1 ");
		sql.append("       					and ((dayInOrder(s1.order_date) < ? ");
		params.add(dateNow);
		sql.append("       					and dayInOrder(s1.order_date) >= ? ");
		params.add(startOfMonth);
		sql.append("       					and s1.approved = 1 ) ");
		sql.append("       					or (dayInOrder(s1.order_date) = ? ");
		params.add(dateNow);
		sql.append("      					 and s1.approved in (0, 1) )) ");
		sql.append("       					group by dayInOrder(s1.order_date) ");
		sql.append("       		having count(s1.sale_order_id)>0) t2 ) AS NUM_ORDER_IN_MONTH,");

		// sku trong thang
		sql.append("       (SELECT Count (DISTINCT sim1.product_id) sku ");
		sql.append("        FROM ( ");
		sql.append("        select sim.product_id ");
		sql.append("        FROM   rpt_sale_primary_month sim ");
		sql.append("        WHERE 1 = 1 and sim.customer_id = ? and sim.shop_id = ? and sim.amount > 0");
		params.add(customerId);
		params.add(shopId);
		if (isStaffSale) {
			sql.append("        and sim.staff_id = ? ");
			params.add(staffId);
		}
		sql.append("            AND substr(sim.affect_date,1,10) = substr( ?, 1,10)");
		params.add(startOfMonth);
		sql.append("       group by sim.product_id) sim1 ");
		sql.append("       ) AS ");
		sql.append("       NUM_SKU, ");
		// doanh so trung binh trong thang
		sql.append(" (select sum(sim1.amount) as amount ");
		sql.append(" 	from ( select SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.amount ");
		}else {
			sql.append(" 	 sim.amount ");
		}
		sql.append(") 	AS AMOUNT ");
		sql.append(" 	from RPT_SALE_PRIMARY_MONTH sim");
		sql.append(" 		where sim.CUSTOMER_ID = ?");
		sql.append(" 		and sim.shop_id = ?");
		params.add(customerId);
		params.add(shopId);
		if (isStaffSale) {
//			sql.append("        and sim.object_id = ? ");
			sql.append("        and sim.staff_id = ? ");
			params.add(staffId);
//			sql.append("        and sim.object_type = ? ");
//			params.add(String.valueOf(UserDTO.TYPE_STAFF));
		}
		sql.append(" 		and amount > 0 ");
		sql.append(" 		and substr(sim.affect_date,1,10) = substr( ?, 1,10) ");
		sql.append("       group by sim.product_id) sim1 ) AS AVG_IN_MONTH, ");
		params.add(startOfMonth);
		// san luong trung binh trong thang
		sql.append(" (select sum(sim1.quantity) as amount ");
		sql.append(" 	from ( select  SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.quantity ");
		}else {
			sql.append(" 	 sim.quantity ");
		}
		sql.append(") 	AS QUANTITY ");
		sql.append(" 	from rpt_sale_primary_month sim");
		sql.append(" 		where sim.CUSTOMER_ID = ?");
		params.add(customerId);
		sql.append(" 		and sim.shop_id = ?");
		params.add(shopId);
		if (isStaffSale) {
//			sql.append("        and sim.object_id = ? ");
			sql.append("        and sim.staff_id = ? ");
			params.add(staffId);
//			sql.append("        and sim.object_type = ? ");
//			params.add(String.valueOf(UserDTO.TYPE_STAFF));
		}
		sql.append(" 		and quantity > 0 ");
		sql.append(" 		and substr(sim.affect_date,1,10) = substr( ?, 1,10) ");
		params.add(startOfMonth);
		sql.append("       group by sim.product_id) sim1 ) AS AVG_QUANTITY_IN_MONTH ");
		sql.append(" FROM   customer c, ");
		sql.append("       channel_type ct ");
		sql.append(" WHERE  c.customer_id = ? ");
		params.add(customerId);
		sql.append(" AND c.shop_id = ? ");
		params.add(shopId);
		sql.append("       AND c.[channel_type_id] = ct.[channel_type_id] ");
		sql.append("       AND ct.[type] = 3 ");

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					customerInfo.parseCustomerInfo(cursor, sysCurrencyDivide);
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return customerInfo;
	}

	public CustomerDTO initLogDTOFromCursor(Cursor c) {
		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setCustomerTypeId(CursorUtil.getInt(c, CHANNEL_TYPE_ID));
		customerDTO.setShortCode(CursorUtil.getString(c, SHORT_CODE));
		customerDTO.address = CursorUtil.getString(c, "ADDRESS");
		customerDTO.setCustomerId(CursorUtil.getLong(c, CUSTOMER_ID));
		customerDTO.setCustomerCode(CursorUtil.getString(c, SHORT_CODE));
		customerDTO.setCustomerName(CursorUtil.getString(c, CUSTOMER_NAME));
		customerDTO.setShopId(CursorUtil.getString(c, SHOP_ID));
		customerDTO.setStreet(CursorUtil.getString(c, STREET));
		customerDTO.setHouseNumber(CursorUtil.getString(c, HOUSENUMBER));
		customerDTO.setPhone(CursorUtil.getString(c, PHONE));
		customerDTO.setContactPerson(CursorUtil.getString(c, CONTACT_NAME));
		customerDTO.setMobilephone(CursorUtil.getString(c, MOBIPHONE));
		customerDTO.setStatus(CursorUtil.getInt(c, STATUS));
		customerDTO.setLat(CursorUtil.getFloat(c, LAT));
		customerDTO.setLng(CursorUtil.getFloat(c, LNG));
		customerDTO.setMaxDebitAmount(CursorUtil.getLong(c, MAX_DEBIT_AMOUNT));
		customerDTO.setMaxDebitDate(CursorUtil.getString(c, MAX_DEBIT_DATE));

		if (StringUtil.isNullOrEmpty(customerDTO.address)) {
			customerDTO.address = Constants.STR_BLANK;
			customerDTO.address = customerDTO.getHouseNumber();
			if (!StringUtil.isNullOrEmpty(customerDTO.street)) {
				customerDTO.address = customerDTO.address + " " + customerDTO.street;
			}

		}
		return customerDTO;
	}

	public ContentValues initCustomerLocationData(CustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
//		editedValues.put(LAT, dto.getLat());
//		editedValues.put(LNG, dto.getLng());
		editedValues.put(LAT, dto.getLat()== Constants.LAT_LNG_NULL ? null : dto.getLat());
		editedValues.put(LNG, dto.getLng()== Constants.LAT_LNG_NULL ? null : dto.getLng());
		return editedValues;
	}

	public ContentValues initDataRow(CustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(CUSTOMER_ID, dto.getCustomerId());
		editedValues.put(CUSTOMER_CODE, dto.getCustomerCode());
		editedValues.put(CUSTOMER_NAME, dto.getCustomerName());
		editedValues.put(SHOP_ID, dto.getShopId());
		editedValues.put(ADDRESS, dto.address);
		editedValues.put(AREA_ID, dto.areaId);
		editedValues.put(SALE_POSITION_ID, dto.typePostitionId);
		String bDay = DateUtils.convertDateOneFromFormatToAnotherFormat(dto.birthDate, DateUtils.DATE_STRING_DD_MM_YYYY, DateUtils.DATE_STRING_YYYY_MM_DD);
		editedValues.put(BIRTHDAY, bDay);
		editedValues.put(HOUSENUMBER, dto.getHouseNumber());
		editedValues.put(STREET, dto.getStreet());
		editedValues.put(PHONE, dto.getPhone());
		editedValues.put(CREATE_DATE, dto.getCreateDate());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());
		editedValues.put(EMAIL, dto.email);
		editedValues.put(CONTACT_NAME, dto.getContactPerson());
		editedValues.put(FAX, dto.fax);
		editedValues.put(MOBIPHONE, dto.getMobilephone());
		editedValues.put(LOCATION, dto.getLocation());
		editedValues.put(CHANNEL_TYPE_ID, dto.getCustomerTypeId());
		editedValues.put(LOYALTY, dto.getLoyalty());
		editedValues.put(CREATE_USER, dto.getCreateUser());
		editedValues.put(UPDATE_USER, dto.getUpdateUser());
		editedValues.put(DISPLAY, dto.getDislay());
//		editedValues.put(LAST_APPROVE_ORDER, dto.getLastApproverder());
		editedValues.put(LAST_ORDER, dto.getLastOrder());
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(LAT, dto.getLat());
		editedValues.put(LNG, dto.getLng());
		editedValues.put(MAX_DEBIT_AMOUNT, dto.getMaxDebitAmount());
		editedValues.put(MAX_DEBIT_DATE, dto.getMaxDebitDate());
		editedValues.put(CREATE_USER_ID, dto.staffId);
		return editedValues;
	}

	/**
	 * Cap nhat customer khi tao moi order
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public ContentValues initDataUpdateFromOrder(CustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(LAST_ORDER, dto.getLastOrder());
//		if (!StringUtil.isNullOrEmpty(dto.getLastApproverder())) {
//			editedValues.put(LAST_APPROVE_ORDER, dto.getLastApproverder());
//		}
		return editedValues;
	}

	/**
	 * Lay ds hinh anh tim kiem cua gsnpp
	 *
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ImageSearchViewDTO
	 * @throws:
	 */
	public ImageSearchViewDTO getImageSearchListGSNPP(Bundle data)
			throws Exception {
		ImageSearchViewDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String listStaff = data.getString(IntentConstants.INTENT_LIST_STAFF);
		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
//		String cusName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		String displayProgramId = data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String day = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String fromDate = data.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String objectTypeImage = data.getString(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
		int numTop = data.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer getCusList = new StringBuffer();
		String startOfTwoPreviousMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -2);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		getCusList.append("SELECT DISTINCT C.SHORT_CODE                                AS CUSTOMER_CODE, ");
		getCusList.append("                M.thumb_url                                 AS THUMB_URL, ");
		getCusList.append("                M.url                                       AS URL, ");
		getCusList.append("                S.staff_code                                AS STAFF_CODE, ");
		getCusList.append("                S.staff_name                                AS STAFF_NAME, ");
		getCusList.append("                M.lat                                       AS LAT_MEDIA, ");
		getCusList.append("                M.lng                                       AS LNG_MEDIA, ");
		getCusList.append("                M.create_user                               AS CREATE_USER, ");
		getCusList.append("                C.customer_id                               AS CUSTOMER_ID, ");
		getCusList.append("                C.customer_name                             AS CUSTOMER_NAME, ");
		getCusList.append("                C.street                                    AS STREET, ");
		getCusList.append("                C.lat                                       AS LAT, ");
		getCusList.append("                C.lng                                       AS LNG, ");
		getCusList.append("                C.housenumber                               AS HOUSENUMBER, ");
		getCusList.append("                ( CASE WHEN RC.monday = 1 THEN 'T2' ELSE ''  END ) ");
		getCusList.append("                || ( CASE WHEN RC.tuesday = 1 THEN ',T3' ELSE ''  END ) ");
		getCusList.append("                || ( CASE WHEN RC.wednesday = 1 THEN ',T4' ELSE '' END ) ");
		getCusList.append("                || ( CASE WHEN RC.thursday = 1 THEN ',T5' ELSE '' END ) ");
		getCusList.append("                || ( CASE WHEN RC.friday = 1 THEN ',T6' ELSE '' END ) ");
		getCusList.append("                || ( CASE WHEN RC.saturday = 1 THEN ',T7' ELSE '' END ) ");
		getCusList.append("                || ( CASE WHEN RC.sunday = 1 THEN ',CN' ELSE '' END ) AS VISIT_PLAN, ");
		getCusList.append("                C.customer_name || C.address  AS CUSTOMER_NAME_ADDRESS_TEXT, ");
		getCusList.append("                S.staff_id                                  AS STAFF_ID, ");
		getCusList.append("                Strftime('%d/%m/%Y - %H:%M', M.create_date) AS CREATE_DATE ");
		getCusList.append("FROM   media_item m ");
		getCusList.append("       JOIN customer C ");
		getCusList.append("         ON C.customer_id = M.object_id ");
		getCusList.append("            AND C.status = 1 ");
		getCusList.append("       JOIN staff S ");
		getCusList.append("              ON S.staff_id = M.staff_id ");
		getCusList.append("                 AND S.status = 1 ");
//		getCusList.append("                 AND M.staff_id IN (SELECT staff_id ");
//		getCusList.append("                                    FROM   staff ");
//		getCusList.append("                                           JOIN channel_type ");
//		getCusList.append("                                             ON staff_type_id = channel_type_id ");
//		getCusList.append("                                    WHERE  object_type IN ( 1, 2, 3, 5 )) ");
		getCusList.append("                 AND M.staff_id IN ("+ listStaff +") ");
		getCusList.append("       LEFT JOIN visit_plan vp ");
		getCusList.append("              ON m.staff_id = vp.staff_id ");
		getCusList.append("                 AND substr(vp.from_date,1,10) <= ? ");
		param.add(date_now);
		getCusList.append("                 AND ( substr(vp.to_date,1,10) >= ? ");
		param.add(date_now);
		getCusList.append("                        OR vp.to_date IS NULL ) ");
		getCusList.append("                 AND Vp.status = 1 ");
		getCusList.append("       LEFT JOIN routing R ");
		getCusList.append("              ON R.routing_id = vp.routing_id ");
		getCusList.append("              AND R.status = 1 ");
		getCusList.append("       LEFT JOIN routing_customer RC ");
		getCusList.append("              ON RC.routing_id = R.routing_id  ");
		getCusList.append("                 AND Date(rc.start_date) <= ? ");
		param.add(date_now);
		getCusList.append("                 AND ( Date(rc.end_date) >= ? ");
		param.add(date_now);
		getCusList.append("                        OR rc.end_date IS NULL ) ");
		getCusList.append("               AND rc.status = 1 ");

		getCusList.append(" WHERE  1 = 1 ");
		// loc cho truong hop co the 1 kh thuoc 2 tuyen se bi ra 2 hinh neu chi ton tai mot hinh
		getCusList.append("       and (case when rc.customer_id is null then 1 else rc.customer_id = m.object_id end) ");
		// -> ket thuc loc hinh
		getCusList.append("       AND object_type IN ( 0, 1, 2, 4 ) ");
		getCusList.append("       AND m.status = 1 ");
//		getCusList.append("       AND m.type = 1 ");
		getCusList
				.append("       AND Date(m.create_date) >= ? ");
		param.add(startOfTwoPreviousMonth);
		getCusList.append("       AND m.media_type = 0 ");
		getCusList.append("       AND M.shop_id = ? ");
		param.add(shopId);

		if (!StringUtil.isNullOrEmpty(cusCode)) {
			cusCode = StringUtil.getEngStringFromUnicodeString(cusCode);
			cusCode = StringUtil.escapeSqlString(cusCode);
			cusCode = DatabaseUtils.sqlEscapeString("%" + cusCode + "%");
			getCusList.append("	and (upper(C.SHORT_CODE) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(C.NAME_TEXT) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(C.CUSTOMER_NAME) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(C.ADDRESS) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append(" ) ");
		}
//		if (!StringUtil.isNullOrEmpty(cusName)) {
//			cusName = StringUtil.getEngStringFromUnicodeString(cusName);
//			cusName = StringUtil.escapeSqlString(cusName);
//			cusName = DatabaseUtils.sqlEscapeString("%" + cusName + "%");
//			getCusList
//					.append("	and upper(ifnull(C.NAME_TEXT,'') || ifnull(C.ADDRESS,'')) like upper(");
//			getCusList.append(cusName);
//			getCusList.append(") escape '^' ");
//		}

		if (!StringUtil.isNullOrEmpty(day)) {
			getCusList.append("	and upper(VISIT_PLAN) like upper(?) ");
			param.add("%" + day + "%");
		}
		if (!StringUtil.isNullOrEmpty(objectTypeImage)) {
			getCusList.append(" AND m.object_type = ? ");
			param.add(objectTypeImage);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			getCusList.append(" and dayInOrder(M.create_date) >= dayInOrder(?) ");
			param.add(fromDate);
		}

		if (!StringUtil.isNullOrEmpty(toDate)) {
			getCusList.append(" and dayInOrder(M.create_date) <= dayInOrder(?) ");
			param.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
			getCusList.append(" and M.object_type = 4 ");// Hinh anh cua 1 CTTB

			getCusList.append("	and M.display_program_id = ? ");
			param.add(displayProgramId);
		}

		if (!StringUtil.isNullOrEmpty(staffId)) {
			getCusList.append(" and M.staff_id = ? ");// nguoi tao
			param.add(staffId);
		}

		getCusList.append(" order by DATETIME(M.CREATE_DATE) DESC ");
		getCusList.append(" limit ? offset ?");
		param.add("" + numTop);
		int offset = page * numTop;
		param.add(String.valueOf(offset));
		try {
			c = rawQueries(getCusList.toString(), param);

			if (c != null) {
				dto = new ImageSearchViewDTO();
				ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						dto.listItem.add(item);
						listPhoto.add(item.mediaItem);
					} while (c.moveToNext());
				}
				dto.albumInfo.setListPhoto(listPhoto);
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 * Ds khach hangf
	 *
	 * @author: TamPQ
	 * @param sortInfo
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public CustomerListDTO getCustomerList(int staffId, int shopId,
										   String customer, String customerCode, String visitPlan,
										   boolean isGetWrongPlan, int page, int isGetTotalPage,
										   boolean isFromRouteView, DMSSortInfo sortInfo) throws Exception {
//		String startOfMonth = DateUtils
//				.getFirstDateOfNumberPreviousMonthWithFormat(
//						DateUtils.DATE_FORMAT_DATE, 0);
		String startOfTwoPreviousMonth = DateUtils
				.getFirstDateOfNumberPreviousMonthWithFormat(
						DateUtils.DATE_FORMAT_DATE, -2);
		CustomerListDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		String daySeq = "";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		if (!StringUtil.isNullOrEmpty(visitPlan)) {
			if (visitPlan.equals(Constants.getDayLine()[0])) {
				daySeq = "2";
			} else if (visitPlan.equals(Constants.getDayLine()[1])) {
				daySeq = "3";
			} else if (visitPlan.equals(Constants.getDayLine()[2])) {
				daySeq = "4";
			} else if (visitPlan.equals(Constants.getDayLine()[3])) {
				daySeq = "5";
			} else if (visitPlan.equals(Constants.getDayLine()[4])) {
				daySeq = "6";
			} else if (visitPlan.equals(Constants.getDayLine()[5])) {
				daySeq = "7";
			} else if (visitPlan.equals(Constants.getDayLine()[6])) {
				daySeq = "8";
			}
		}

		Cursor cDistance = null;
		String getShopDistance = " select DISTANCE_ORDER from SHOP where SHOP_ID = "
				+ shopId;
		try {
			cDistance = rawQuery(getShopDistance, null);
			if (cDistance != null) {
				if (cDistance.moveToFirst()) {
					dto = new CustomerListDTO();
					dto.setDistance(cDistance.getDouble(0));
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (cDistance != null) {
					cDistance.close();
				}
			} catch (Exception ex) {
			}
		}

		StringBuffer var1 = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();

		var1.append("SELECT *, DRAFT.SALE_ORDER_ID_DRAFT      AS DRAFT_SALE_ORDER_ID,KHT.SALE_ORDER_ID   AS SALE_ORDER_ID   ");
		var1.append("FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               CT.SHORT_CODE  AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday((?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		totalParam.add(date_now);
//		var1.append("               AND DATE(RTC.START_DATE) <= DATE(?) ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("               AND (DATE(RTC.END_DATE) >= DATE(?) OR DATE(RTC.END_DATE) IS NULL) ");
//		param.add(date_now);
//		totalParam.add(date_now);
		var1.append("               AND VP.STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         CUSTOMER_ID               AS CUSTOMER_ID_1, ");
		var1.append("                         GROUP_CONCAT(OBJECT_TYPE) AS ACTION ");
		var1.append("                  FROM   ACTION_LOG ");
		var1.append("                  WHERE  STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("                  AND  SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("                         AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                         AND OBJECT_TYPE IN ( 2, 3, 4, 6 ) ");
		var1.append("                  GROUP  BY CUSTOMER_ID) ACTION ");
		var1.append("              ON CUS.CUSTOMER_ID = ACTION.CUSTOMER_ID_1 ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         ACTION_LOG_ID AS VISIT_ACT_LOG_ID, ");
		var1.append("                         CUSTOMER_ID AS CUSTOMER_ID_2, ");
		var1.append("                         OBJECT_TYPE AS VISIT, ");
		var1.append("                         START_TIME, ");
		var1.append("                         END_TIME, ");
		var1.append("               STRFTIME('%H:%M', START_TIME)              AS VISIT_START_TIME_SHORT, ");
		var1.append("                         LAT AS VISITED_LAT, ");
		var1.append("                         LNG AS VISITED_LNG, ");
		var1.append("                         IS_OR ");
		var1.append("                  FROM   ACTION_LOG ");
		var1.append("                  WHERE  STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("                  AND  SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("                         AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                         AND OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("                  GROUP  BY CUSTOMER_ID) VISIT ");
		var1.append("              ON VISIT.CUSTOMER_ID_2 = CUS.CUSTOMER_ID ");

		/* ------ > Tam thoi bo cham CTTB
		var1.append("		LEFT JOIN ");
		var1.append("			(");

		// cttb --> Tam thoi bo cham CTTB
		var1.append("               SELECT DCM.CUSTOMER_ID          AS CUSTOMER_ID_3, ");
		var1.append("               DP.DISPLAY_PROGRAM_ID  AS DISPLAY_PROGRAM_ID, ");
		var1.append("               DP.display_program_code AS DISPLAY_PROGRAM_CODE ");
		var1.append("               FROM display_program AS DP, ");
		var1.append("               display_staff_map AS DSM, ");
		var1.append("               display_program_level AS DPL, ");
		var1.append("               display_customer_map AS DCM ");
		var1.append("               WHERE DP.display_program_id = DSM.display_program_id ");
		var1.append("               AND DP.display_program_id = DPL.display_program_id ");
		var1.append("               AND DSM.display_staff_map_id = DCM.display_staff_map_id ");
		var1.append("               AND DPL.display_program_level_id = DCM.display_program_level_id ");
		var1.append("               AND DATE(DP.from_date) <= Date(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND IFNULL(DATE(DP.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND DP.STATUS = 1 ");
		var1.append("               AND DSM.STAFF_ID = ? AND DATE(DSM.MONTH, 'start of month') = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		param.add(startOfMonth);
		totalParam.add(startOfMonth);
		var1.append("               AND DSM.STATUS = 1 ");
		var1.append("               AND DPL.STATUS = 1 ");
		var1.append("               AND DSM.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("               AND DCM.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("               AND DATE(DCM.from_date) <= Date(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND IFNULL(DATE(DCM.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND DCM.STATUS = 1 ");
		var1.append("                                 AND DP.DISPLAY_PROGRAM_ID NOT IN (SELECT ");
		var1.append("                                     OBJECT_ID AS DISPLAY_PROGRAME_ID ");
		var1.append("                                                                    FROM ");
		var1.append("                                     ACTION_LOG ");
		var1.append("                                                                    WHERE ");
		var1.append("                                     OBJECT_TYPE = 2 ");
		var1.append("                                     AND STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("                  					  AND  SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("                                     AND DATE(START_TIME) <= DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                                     AND CUSTOMER_ID = DCM.CUSTOMER_ID ");
		var1.append("                                     AND (DATE(END_TIME) >= DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                                          OR END_TIME IS NULL ");
		var1.append("                                         ))) ");

		var1.append("			 CTTB ");
		// end cttb

		var1.append("		ON CTTB.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");
		---> Tam thoi bo cham CTTB*/

		var1.append("	LEFT JOIN ");
		var1.append("	(");
		var1.append("	SELECT	");
		var1.append("	    count(ks.ks_id) KEYSHOP_COUNT,	");
		var1.append("	    KSC.CUSTOMER_ID CUSTOMER_ID_3	");
		var1.append("	FROM	");
		var1.append("	    KS ,	");
		var1.append("	    KS_CUSTOMER KSC,	");
		var1.append("	    CYCLE CY	");
		var1.append("	WHERE	");
		var1.append("	    1 = 1	");
		var1.append("	    AND KSC.SHOP_ID = ?	");
		param.add(""+shopId);
		totalParam.add(""+shopId);
		var1.append("	    AND KS.STATUS = 1	");
		var1.append("	    AND KSC.STATUS = ?	");
		var1.append("	    AND KSC.ks_id = ks.ks_id	");
//		var1.append("	    AND KSC.APPROVAL = ?	");
		param.add(""+ KSCustomerDTO.STATE_ACTIVE);
		totalParam.add(""+KSCustomerDTO.STATE_ACTIVE);
		var1.append("	    AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
		var1.append("	    AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
		var1.append("      AND CY.CYCLE_ID = KSC.CYCLE_ID ");
		var1.append("      AND CY.STATUS = 1 ");
		var1.append("      AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("      AND SUBSTR(CY.END_DATE,1,10) >= SUBSTR(?,1,10) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("      GROUP BY KSC.CUSTOMER_ID) KS ");
		var1.append("	   ON KS.CUSTOMER_ID_3 = CUS.CUSTOMER_ID");

		var1.append("       LEFT JOIN (SELECT SO.SALE_ORDER_ID AS SALE_ORDER_ID, SO.CUSTOMER_ID as  CUSTOMER_ID_4");
		var1.append("             FROM   SALE_ORDER SO ");
		var1.append("             WHERE  SO.STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("             AND IFNULL(DATE(SO.ORDER_DATE) >= ");
		var1.append("                  ? ");
		var1.append("                       , 0)  GROUP BY CUSTOMER_ID ) KHT ");
		param.add(startOfTwoPreviousMonth);
		totalParam.add(startOfTwoPreviousMonth);
		var1.append("        ON KHT.CUSTOMER_ID_4 = CUS.CUSTOMER_ID ");

		var1.append("		LEFT JOIN ");
		var1.append("		(SELECT CUSTOMER_ID AS CUSTOMER_ID_5, Strftime('%d/%m/%Y', EXCEPTION_ORDER_DATE) as EXCEPTION_ORDER_DATE, STAFF_CUSTOMER.STAFF_CUSTOMER_ID FROM STAFF_CUSTOMER WHERE STAFF_ID = ? AND SHOP_ID = ?) ST_CT ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("		ON ST_CT.CUSTOMER_ID_5 = CUS.CUSTOMER_ID ");
		var1.append(" LEFT JOIN (SELECT SO.customer_id AS CUSTOMER_ID_6, SO.SALE_ORDER_ID SALE_ORDER_ID_DRAFT ");
		var1.append("    FROM   SALE_ORDER SO  ");
		var1.append("    WHERE  SO.STAFF_ID = ?  ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append(" AND SO.syn_state = 0 AND SO.approved= -1 ");
		var1.append(" AND IFNULL(DATE(SO.ORDER_DATE) = dayInOrder(?), 0)) DRAFT ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("     ON DRAFT.CUSTOMER_ID_6 = CUS.customer_id ");
		
		//Chup hinh thiet bi
		var1.append("	LEFT JOIN	");
		var1.append("	(	");
		var1.append("	    SELECT	");
		var1.append("	        e.equip_id EQUIP_ID,	");
		var1.append("	        e.equip_group_id EQUIP_GROUP_ID,	");
		var1.append("	        e.stock_id STOCK_ID	");
		var1.append("	    FROM	");
		var1.append("	        equipment e,	");
		var1.append("	        equip_group eg,	");
		var1.append("	        equip_category ec	");
		var1.append("	    WHERE	");
		var1.append("	        1=1	");
		var1.append("	        AND e.status =1	");
		var1.append("	        AND eg.status =1	");
		var1.append("	        AND ec.status =1	");
		var1.append("	        AND e.stock_type =2	");
		var1.append("	        AND e.trade_status = 0	");
		var1.append("	        AND e.usage_status in (	");
		var1.append("	            3, 4	");
		var1.append("	        )	");
		var1.append("	        AND (	");
		var1.append("	            substr(e.first_date_in_use,1,10) <= substr(?,1,10)	");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("	            OR e.first_date_in_use is null	");
		var1.append("	        )	");
		var1.append("	        AND e.equip_group_id = eg.equip_group_id	");
		var1.append("	        AND eg.equip_category_id = ec.equip_category_id	");
		var1.append("	    GROUP BY	");
		var1.append("	        e.stock_id	");
		var1.append("	) EQUIP_CUSTOMER	");
		var1.append("	    ON  EQUIP_CUSTOMER.stock_id = cus.CUSTOMER_ID	");
		//Ket thuc chup hinh thiet bi		
		
		var1.append(" WHERE 1 = 1");
		if (!StringUtil.isNullOrEmpty(visitPlan)) {// ko phai chon tat ca
			if (isGetWrongPlan) {
				var1.append("	and (upper(CUS_PLAN) like upper(?) OR IS_OR IN (0,1) )");
				param.add("%" + visitPlan + "%");
				totalParam.add("%" + visitPlan + "%");
			} else {
				var1.append("	and upper(CUS_PLAN) like upper(?) ");
				param.add("%" + visitPlan + "%");
				totalParam.add("%" + visitPlan + "%");
			}
		}
//		if (!StringUtil.isNullOrEmpty(customerCode)) {
//			customerCode = StringUtil.escapeSqlString(customerCode);
//			customerCode = DatabaseUtils.sqlEscapeString("%" + customerCode
//					+ "%");
//			var1.append("	and upper(CUSTOMER_CODE) like upper(");
//			var1.append(customerCode);
//			var1.append(") escape '^' ");
//		}
		if (!StringUtil.isNullOrEmpty(customer)) {
			customer = StringUtil.getEngStringFromUnicodeString(customer);
			customer = StringUtil.escapeSqlString(customer);
			var1.append("	and ( upper(CUSTOMER_CODE) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(NAME_TEXT) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(CUSTOMER_NAME) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(ADDRESS) like upper(?)) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
		}

		var1.append("   group by CUSTOMER_ID ");

		//default order by
		String defaultOrderByStr = "";
		if (isFromRouteView) {
			defaultOrderByStr = "   order by DATETIME(START_TIME) ";
		} else if (!StringUtil.isNullOrEmpty(daySeq)) {
			defaultOrderByStr = "   order by VISIT asc, DAY_SEQ asc, CUSTOMER_CODE asc, CUSTOMER_NAME asc ";
		} else {
			defaultOrderByStr = "   order by VISIT asc, CUSTOMER_CODE asc, CUSTOMER_NAME asc ";
		}

		String orderByStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
			.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
			.addMapper(SortActionConstants.VISIT_STATE, "VISIT")
			.defaultOrderString(defaultOrderByStr)
			.build(sortInfo);

		//add order string
		var1.append(orderByStr);

		if (page > 0) {
			// get count
			if (isGetTotalPage == 1) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ var1 + ")");
			}
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null && dto != null) {
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataFromCursor(c, dto.getDistance(), visitPlan);
						//int num = c.getCount();
						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalCustomer(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}
	/**
	 * Ds khach hangf
	 *
	 * @author: TrungHQM
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public SupervisorCustomerListDTO getGsnppCustomerList(int staffId,
														  String customer, String customerCode, String customerAddress,
														  String visitPlan, int page, int isGetTotalPage) throws Exception {
		SupervisorCustomerListDTO dto = new SupervisorCustomerListDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();
		String listStaffOfSupervisor = PriUtils.getInstance().getListStaffOfSupervisor(String.valueOf(staffId), shopId);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT DISTINCT( CUSTOMER_ID ) CUSTOMER_ID, ");
		var1.append("               CUSTOMER_CODE, ");
		var1.append("               CUSTOMER_NAME, ");
		var1.append("               STREET, ");
		var1.append("               TEMP.staff_id ");
		var1.append("FROM   (SELECT CT.customer_id                 AS CUSTOMER_ID, ");
		var1.append("               Substr(CT.customer_code, 1, 3) AS CUSTOMER_CODE, ");
		var1.append("               CT.customer_name               AS CUSTOMER_NAME, ");
		var1.append("               ( CT.housenumber ");
		var1.append("                 || ' ' ");
		var1.append("                 || CT.street )               AS STREET, ");
		var1.append("               CT.name_text                   AS NAME_TEXT, ");
		var1.append("               VP.staff_id                    AS STAFF_ID ");
		var1.append("        FROM   customer CT, ");
		var1.append("               routing R, ");
		var1.append("               routing_customer RC, ");
		var1.append("               visit_plan VP ");
		var1.append("        WHERE  CT.status = 1 ");
		var1.append("               AND VP.shop_id = ? ");
		param.add(shopId);
		totalParam.add(shopId);
		var1.append("               AND R.status = 1 ");
		var1.append("               AND RC.status = 1 ");
		var1.append("               AND R.routing_id = VP.routing_id ");
		var1.append("               AND VP.status = 1 ");
		var1.append("               AND R.routing_id = RC.routing_id ");
		var1.append("               AND CT.customer_id = RC.customer_id ");
		var1.append("               AND CT.status = 1) TEMP, ");
		var1.append("       (SELECT staff_id ");
		var1.append("        FROM   staff ");
		var1.append("        WHERE  staff_id in (" + listStaffOfSupervisor + ")");
		var1.append("               AND status = 1 and shop_id = ?) STAFFLIST ");
		param.add(shopId);
		totalParam.add(shopId);

		var1.append("WHERE  TEMP.staff_id = STAFFLIST.staff_id ");

//		if (!StringUtil.isNullOrEmpty(customerCode)) {
//			customerCode = StringUtil.escapeSqlString(customerCode);
//			customerCode = DatabaseUtils.sqlEscapeString("%" + customerCode
//					+ "%");
//			var1.append("	and upper(CUSTOMER_CODE) like upper(");
//			var1.append(customerCode);
//			var1.append(") escape '^' ");
//		}
//
//		if (!StringUtil.isNullOrEmpty(customer)) {
//			customer = StringUtil
//					.getEngStringFromUnicodeString(customer);
//			customer = StringUtil.escapeSqlString(customer);
//			customer = DatabaseUtils.sqlEscapeString("%" + customer
//					+ "%");
//			var1.append("	and upper(NAME_TEXT) like upper(");
//			var1.append(customer);
//			var1.append(") escape '^' ");
//		}
		if (!StringUtil.isNullOrEmpty(customer)) {
			customer = StringUtil.getEngStringFromUnicodeString(customer);
			customer = StringUtil.escapeSqlString(customer);
			var1.append("	and ( upper(CUSTOMER_CODE) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(NAME_TEXT) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(CUSTOMER_NAME) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			var1.append("	or upper(ADDRESS) like upper(?)) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
		}

		var1.append("GROUP  BY customer_id  ");

		var1.append("ORDER  BY customer_code ASC, ");
		var1.append("          customer_name ASC ");

		if (page > 0) {
			// get count
			if (isGetTotalPage == 1) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ var1 + ")");
			}
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
					.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataFromCursorSupervisor(c);
						//int num = c.getCount();
						dto.cusList.add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 * Ds khach hangf
	 *
	 * @author: TamPQ
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public CustomerListDTO getTBHVListCustomer(int staffId, int shopId,
			String customerName, String customerCode, String customerAddress,
			String visitPlan, boolean isGetWrongPlan, int page,
			int isGetTotalPage) throws Exception {
		CustomerListDTO dto = null;
		Cursor c = null;
		String daySeq = "";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		if (!StringUtil.isNullOrEmpty(visitPlan)) {
			if (visitPlan.equals(Constants.getDayLine()[0])) {
				daySeq = "2";
			} else if (visitPlan.equals(Constants.getDayLine()[1])) {
				daySeq = "3";
			} else if (visitPlan.equals(Constants.getDayLine()[2])) {
				daySeq = "4";
			} else if (visitPlan.equals(Constants.getDayLine()[3])) {
				daySeq = "5";
			} else if (visitPlan.equals(Constants.getDayLine()[4])) {
				daySeq = "6";
			} else if (visitPlan.equals(Constants.getDayLine()[5])) {
				daySeq = "7";
			} else if (visitPlan.equals(Constants.getDayLine()[6])) {
				daySeq = "8";
			}
		}

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT * ");
		var1.append("FROM   (SELECT VP.VISIT_PLAN_ID               AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID                     AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID                    AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE                   AS FROM_DATE, ");
		var1.append("               VP.TO_DATE                     AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID                  AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE                AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME                AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID                 AS CUSTOMER_ID, ");
		var1.append("               SUBSTR(CT.CUSTOMER_CODE, 1, 3) AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME               AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT                   AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE                   AS MOBIPHONE, ");
		var1.append("               CT.PHONE                       AS PHONE, ");
		var1.append("               CT.LAT                         AS LAT, ");
		var1.append("               CT.LNG                         AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID             AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS                     AS ADDRESS, ");
		var1.append("               ( CT.HOUSENUMBER ");
		var1.append("                 || ' ' ");
		var1.append("                 || CT.STREET )               AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )                     AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )                     AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID        AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       week1, week2, week3, week4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER" );
		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("    ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND ( DATE(VP.TO_DATE) >= DATE(?) ");
		param.add(date_now);
		var1.append("                      OR DATE(VP.TO_DATE) IS NULL ) ");
		var1.append("               AND VP.STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("               AND VP.STATUS = 1 ");
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS = 1 ");
		var1.append("               ) CUS ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         CUSTOMER_ID               AS CUSTOMER_ID_1, ");
		var1.append("                         GROUP_CONCAT(OBJECT_TYPE) AS ACTION ");
		var1.append("                  FROM   ACTION_LOG ");
		var1.append("                  WHERE  STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("                  AND  SHOP_ID = ? ");
		param.add("" + shopId);
		var1.append("                         AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		var1.append("                         AND OBJECT_TYPE IN ( 2, 3, 4 ) ");
		var1.append("                  GROUP  BY CUSTOMER_ID) ACTION ");
		var1.append("              ON CUS.CUSTOMER_ID = ACTION.CUSTOMER_ID_1 ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         CUSTOMER_ID AS CUSTOMER_ID_2, ");
		var1.append("                         OBJECT_TYPE AS VISIT, ");
		var1.append("                         START_TIME, ");
		var1.append("                         END_TIME, ");
		var1.append("                         LAT         AS VISITED_LAT, ");
		var1.append("                         LNG         AS VISITED_LNG, ");
		var1.append("                         IS_OR ");
		var1.append("                  FROM   ACTION_LOG ");
		var1.append("                  WHERE  STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("                  AND    SHOP_ID = ? ");
		param.add("" + shopId);
		var1.append("                         AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		var1.append("                         AND OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("                  GROUP  BY CUSTOMER_ID) VISIT ");
		var1.append("              ON VISIT.CUSTOMER_ID_2 = CUS.CUSTOMER_ID ");
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND ( UPPER(CUS_PLAN) LIKE UPPER(?) OR IS_OR IN (0,1) )");
		param.add("%" + visitPlan + "%");
		try {
			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (dto == null) {
					dto = new CustomerListDTO();
				}
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataFromCursor(c, dto.getDistance(), visitPlan);

						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 *
	 * get list image
	 *
	 * @author: HoanPD1
	 * @param data
	 * @return
	 * @return: ImageListDTO
	 * @throws Exception
	 * @throws:
	 */
	public ImageListDTO getImageList(Bundle data) throws Exception {
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ImageListDTO dto = new ImageListDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
//		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String cusName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		String displayProgramId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String day = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		Boolean isSearch = data.getBoolean(IntentConstants.INTENT_IS_SEARCH);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String startOfTwoPreviousMonth = DateUtils
				.getFirstDateOfNumberPreviousMonthWithFormat(
						DateUtils.DATE_FORMAT_DATE, -2);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer getCusList = new StringBuffer();
		getCusList.append("	SELECT	");
		getCusList.append("	    CTT.CUSTOMER_CODE AS CUSTOMER_CODE,	");
		getCusList.append("	    CTT.CUSTOMER_ID as CUSTOMER_ID,	");
		getCusList.append("	    CTT.CUSTOMER_NAME AS CUSTOMER_NAME,	");
		getCusList.append("	    CTT.ADDRESS AS ADDRESS,	");
		getCusList.append("	    CTT.ROUTING_ID AS ROUTING_ID,	");
		getCusList.append("	    ifnull(CTT.STREET,	");
		getCusList.append("	    '') as STREET,	");
		getCusList.append("	    CTT.LAT AS LAT,	");
		getCusList.append("	    CTT.LNG AS LNG,	");
		getCusList.append("	    ifnull(CTT.HOUSENUMBER,	");
		getCusList.append("	    '') AS HOUSENUMBER,	");
		getCusList.append("	    CTT.VISIT_PLAN AS VISIT_PLAN,	");
		getCusList.append("	    CTT.CUSTOMER_NAME_ADDRESS_TEXT AS CUSTOMER_NAME_ADDRESS_TEXT,	");
		getCusList.append("	    CTT.STAFF_ID as STAFF_ID,	");
		getCusList.append(" (select count (object_id) from  ");
		getCusList.append(" (select * from media_item ");
		// getCusList.append(" where object_type in (0,1,2, 4) ");
		getCusList
				.append(" where object_type in (0,1,2) AND STATUS = 1 ");
		getCusList
				.append(" AND dayInOrder(create_date) >= ?");
		param.add(startOfTwoPreviousMonth);
		getCusList.append(" AND media_type = 0 ");
		getCusList.append(" AND shop_id = ? ");
		param.add(shopId);
		getCusList.append(" UNION ALL select mi.* from media_item mi join ks dp ON mi.display_program_id = dp.ks_id  ");
		getCusList.append(" JOIN ks_shop_map ksm  ON ksm.ks_id = dp.ks_id ");
		getCusList.append(" where mi.object_type = 4 AND mi.STATUS = 1");
		getCusList.append(" and dp.status = 1 ");
		getCusList.append(" and ksm.status = 1 ");
		getCusList.append(" and dayInOrder(mi.create_date) >=  ?");
		param.add(startOfTwoPreviousMonth);
		getCusList.append(" AND mi.media_type = 0 ");
		getCusList.append(" AND mi.shop_id = ? ");
		param.add(shopId);
		getCusList.append(" AND ksm.shop_id IN ");
		getCusList.append("       (").append(idShopList).append(")");
		getCusList.append(") MI ");
		// getCusList.append(" where object_type in (0,1,2, 4) ");
		getCusList.append(" where 1 = 1 ");

		if (!StringUtil.isNullOrEmpty(fromDate)) {
			getCusList.append(" and substr(create_date,1,10) >= ? ");
			param.add(fromDate);
		}

		if (!StringUtil.isNullOrEmpty(toDate)) {
			getCusList.append(" and substr(create_date,1,10) <= ? ");
			param.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
			getCusList.append(" and object_type = 4 ");// Hinh anh cua 1 CTTB

			getCusList.append("	and display_program_id = ? ");
			param.add(displayProgramId);
		}

		getCusList.append(" and object_id = CTT.customer_id ");
		getCusList.append(") NUM_ITEM ");
		getCusList.append("	FROM	");
		getCusList.append("	    (SELECT	");
		getCusList.append("	        CT.SHORT_CODE AS CUSTOMER_CODE,	");
		getCusList.append("	        CT.CUSTOMER_ID as CUSTOMER_ID,	");
		getCusList.append("	        CT.LAT AS LAT,	");
		getCusList.append("	        CT.LNG AS LNG,	");
		getCusList.append("	        CT.HOUSENUMBER AS HOUSENUMBER,	");
		getCusList.append("	        CT.CUSTOMER_NAME AS CUSTOMER_NAME,	");
		getCusList.append("	        CT.ADDRESS AS ADDRESS,	");
		getCusList.append("	        CT.STREET AS STREET,	");
		getCusList.append("	        (CASE	");
		getCusList.append("	            WHEN rtc.MONDAY = 1 THEN 'T2'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.TUESDAY = 1 THEN ',T3'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.WEDNESDAY = 1 THEN ',T4'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.THURSDAY = 1 THEN ',T5'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.FRIDAY = 1 THEN ',T6'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.SATURDAY = 1 THEN ',T7'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END)     || (CASE	");
		getCusList.append("	            WHEN rtc.SUNDAY = 1 THEN ',CN'	");
		getCusList.append("	            ELSE ''	");
		getCusList.append("	        END) AS VISIT_PLAN,	");
		getCusList.append("	        CT.NAME_TEXT AS CUSTOMER_NAME_ADDRESS_TEXT,	");
		getCusList.append("	        vp.ROUTING_ID,	");
		getCusList.append("	        vp.STAFF_ID	");
		getCusList.append("	    FROM	");
		getCusList.append("	        VISIT_PLAN VP,	");
		getCusList.append("	        ROUTING RT,	");
		getCusList.append("	        ROUTING_CUSTOMER RTC,	");
		getCusList.append("	        CUSTOMER CT	");
		getCusList.append("	    WHERE	");
		getCusList.append("	        VP.ROUTING_ID = RT.ROUTING_ID	");
		getCusList.append("	        AND rt.routing_Id = rtc.routing_id	");
		getCusList.append("	        AND rtc.customer_id = ct.customer_id	");
		getCusList.append("	        AND vp.status = 1	");
		getCusList.append("	        AND vp.shop_id = ?	");
		param.add(shopId);
		getCusList.append(" and rt.status = 1 ");
		getCusList.append(" and rtc.status = 1 ");
		getCusList.append(" and ct.status = 1 ");
		getCusList.append(" AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		getCusList.append(" AND ( DATE(VP.TO_DATE) >= DATE(?) ");
		param.add(date_now);
		getCusList.append("      OR DATE(VP.TO_DATE) IS NULL ) ");
		getCusList.append(" AND DATE(RTC.START_DATE) <= DATE(?) ");
		param.add(date_now);
		getCusList.append(" AND ( DATE(RTC.END_DATE) >= DATE(?) ");
		param.add(date_now);
		getCusList.append("      OR DATE(RTC.END_DATE) IS NULL ) ");
		getCusList.append(" and vp.STAFF_ID = ? ");
		param.add("" + staffId);
//		if (!StringUtil.isNullOrEmpty(cusCode)) {
//			cusCode = StringUtil.getEngStringFromUnicodeString(cusCode);
//			cusCode = StringUtil.escapeSqlString(cusCode);
//			cusCode = DatabaseUtils.sqlEscapeString("%" + cusCode + "%");
//			getCusList
//					.append("	and upper(CT.SHORT_CODE) like upper(");
//			getCusList.append(cusCode);
//			getCusList.append(") escape '^' ");
//		}
		if (!StringUtil.isNullOrEmpty(cusName)) {
			cusName = StringUtil.getEngStringFromUnicodeString(cusName);
			cusName = StringUtil.escapeSqlString(cusName);
			cusName = DatabaseUtils.sqlEscapeString("%" + cusName + "%");
			getCusList.append("	and (upper(CT.NAME_TEXT) like upper(");
			getCusList.append(cusName);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(CT.SHORT_CODE) like upper(");
			getCusList.append(cusName);
			getCusList.append(") escape '^'  ");
			getCusList.append("	or upper(CT.CUSTOMER_NAME) like upper(");
			getCusList.append(cusName);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(CT.ADDRESS) like upper(");
			getCusList.append(cusName);
			getCusList.append(") escape '^' ");

			getCusList.append(" ) ");
		}

		if (!StringUtil.isNullOrEmpty(day)) {
			getCusList.append("	and upper(VISIT_PLAN) like upper(?) ");
			param.add("%" + day + "%");
		}
		getCusList.append(" ) CTT ");
//		getCusList.append(" ) CTT LEFT JOIN");
//		getCusList.append(" (select * from media_item ");
//		// getCusList.append(" where object_type in (0,1,2, 4) ");
//		getCusList
//				.append(" where object_type in (0,1,2) AND STATUS = 1 ");
//		getCusList
//				.append(" AND dayInOrder(create_date) >= ?");
//		param.add(startOfTwoPreviousMonth);
//		getCusList.append(" AND media_type = 0 ");
//		getCusList.append(" AND shop_id = ? ");
//		param.add(shopId);
//		getCusList.append(" UNION ALL select mi.* from media_item mi join ks dp ON mi.display_program_id = dp.ks_id ");
//		getCusList.append(" JOIN ks_shop_map ksm  ON ksm.ks_id = dp.ks_id ");
//		getCusList.append(" where mi.object_type = 4 AND mi.STATUS = 1");
//		getCusList.append(" and dp.status = 1 ");
//		getCusList.append(" and ksm.status = 1 ");
//		getCusList.append(" and dayInOrder(mi.create_date) >=  ?");
//		param.add(startOfTwoPreviousMonth);
//		getCusList.append(" AND mi.media_type = 0 ");
//		getCusList.append(" AND mi.shop_id = ? ");
//		param.add(shopId);
//		getCusList.append(" AND ksm.shop_id IN ");
//		getCusList.append("       (").append(idShopList).append(")");


//		if (isSearch) {
//			if (!StringUtil.isNullOrEmpty(fromDate)) {
//				getCusList.append(" and dayInOrder(MI.create_date) >= dayInOrder(?) ");
//				param.add(fromDate);
//			}
//
//			if (!StringUtil.isNullOrEmpty(toDate)) {
//				getCusList.append(" and dayInOrder(MI.create_date) <= dayInOrder(?) ");
//				param.add(toDate);
//			}
//		}
//		getCusList.append(") MI ");
//		getCusList.append(" ON CTT.CUSTOMER_ID = MI.object_id ");
//
//		getCusList.append(" where 1 = 1 ");
//
//		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
//			getCusList.append(" and MI.object_type = 4 ");// Hinh anh cua 1 CTTB
//
//			getCusList.append("	and MI.display_program_id = ? ");
//			param.add(displayProgramId);
//		}
		getCusList.append(" GROUP BY CTT.CUSTOMER_ID");

		/*if (isSearch) {
			getCusList.append(" HAVING NUM_ITEM > 0 ");
		}*/

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   order by CUSTOMER_CODE asc, CUSTOMER_NAME asc ";

		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
		.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
		.addMapper(SortActionConstants.NUM_IMAGE, "NUM_ITEM")
		.defaultOrderString(defaultOrderByStr)
		.build(sortInfo);
		//add order string
		getCusList.append(orderByStr);

		String getTotalCount = "";
		if (page > 0) {
			if (isGetTotalPage == 1) {
				// get count
				getTotalCount = " select count(*) as TOTAL_ROW from ("
						+ getCusList + ")";
			}

			getCusList.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			getCusList
					.append(" offset "
							+ Integer.toString((page - 1)
									* Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(getCusList.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageListItemDTO(c);
						item.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
						dto.listItem.add(item);
					} while (c.moveToNext());
				}
			}
			if (isGetTotalPage == 1) {
				cTotalRow = rawQueries(getTotalCount, param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 *
	 * 07-02 get list image GSNPP
	 *
	 * @author: HoanPD1
	 * @param data
	 * @return
	 * @return: ImageListDTO
	 * @throws Exception
	 * @throws:
	 */
	public ImageListDTO getSupervisorImageList(Bundle data) throws Exception {
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfTwoPreviousMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -2);
		ImageListDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
//		String cusName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String listStaffId = data.getString(IntentConstants.INTENT_LIST_STAFF);
		String day = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String displayProgramId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		Boolean isSearch = data.getBoolean(IntentConstants.INTENT_IS_SEARCH);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> param = new ArrayList<String>();
		StringBuffer getTotalCount = new StringBuffer();
		StringBuffer getCusList = new StringBuffer();
		getCusList.append(" select  CTT.SHORT_CODE AS CUSTOMER_CODE, ");
		getCusList.append(" CTT.CUSTOMER_ID as CUSTOMER_ID, ");
		getCusList.append(" CTT.ROUTING_ID as ROUTING_ID, ");
		getCusList.append(" CTT.CUSTOMER_NAME AS CUSTOMER_NAME,  ");
		getCusList.append(" CTT.ADDRESS              AS ADDRESS, ");
		getCusList.append(" Ifnull(CTT.STREET, '') as STREET, CTT.LAT AS LAT, ");
		getCusList.append(" CTT.LNG AS LNG, Ifnull(CTT.HOUSENUMBER, '') AS HOUSENUMBER, ");
		getCusList.append(" CTT.VISIT_PLAN AS VISIT_PLAN, ");
		getCusList
				.append(" CTT.CUSTOMER_NAME_ADDRESS_TEXT AS CUSTOMER_NAME_ADDRESS_TEXT, ");
		getCusList.append(" CTT.STAFF_ID as STAFF_ID, ");
		getCusList.append(" CTT.STAFF_NAME as NVBH_STAFF_NAME, ");
//		getCusList.append("  COUNT(MI.OBJECT_ID) NUM_ITEM ");

		getCusList.append(" (select count (object_id) from  ");
		getCusList.append(" (select * from media_item ");
		// getCusList.append(" where object_type in (0,1,2, 4) ");
		getCusList
				.append(" where object_type in (0,1,2) AND STATUS = 1 ");
		getCusList
				.append(" AND dayInOrder(create_date) >= ?");
		param.add(startOfTwoPreviousMonth);
		getCusList.append(" AND media_type = 0 ");
		getCusList.append(" AND shop_id = ? ");
		param.add(shopId);
		getCusList.append(" UNION ALL select mi.* from media_item mi join ks dp ON mi.display_program_id = dp.ks_id  ");
		getCusList.append(" JOIN ks_shop_map ksm  ON ksm.ks_id = dp.ks_id ");
		getCusList.append(" where mi.object_type = 4 AND mi.STATUS = 1");
		getCusList.append(" and dp.status = 1 ");
		getCusList.append(" and ksm.status = 1 ");
		getCusList.append(" and dayInOrder(mi.create_date) >=  ?");
		param.add(startOfTwoPreviousMonth);
		getCusList.append(" AND mi.media_type = 0 ");
		getCusList.append(" AND mi.shop_id = ? ");
		param.add(shopId);
		getCusList.append(" AND ksm.shop_id IN ");
		getCusList.append("       (").append(idShopList).append(")");
		getCusList.append(") MI ");
		// getCusList.append(" where object_type in (0,1,2, 4) ");
		getCusList.append(" where 1 = 1 ");

		if (!StringUtil.isNullOrEmpty(fromDate)) {
			getCusList.append(" and substr(create_date,1,10) >= ? ");
			param.add(fromDate);
		}

		if (!StringUtil.isNullOrEmpty(toDate)) {
			getCusList.append(" and substr(create_date,1,10) <= ? ");
			param.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
			getCusList.append(" and object_type = 4 ");// Hinh anh cua 1 CTTB

			getCusList.append("	and display_program_id = ? ");
			param.add(displayProgramId);
		}

		getCusList.append(" and object_id = CTT.customer_id ");
		getCusList.append(") NUM_ITEM ");

		getCusList.append(" from (SELECT  CT.SHORT_CODE AS SHORT_CODE, ");
		getCusList.append(" CT.CUSTOMER_ID as CUSTOMER_ID, CT.LAT AS LAT, ");
		getCusList.append(" CT.LNG AS LNG, CT.HOUSENUMBER AS HOUSENUMBER, ");
		getCusList.append(" CT.CUSTOMER_NAME AS CUSTOMER_NAME,");
		getCusList.append(" CT.STREET AS STREET, ");
		getCusList.append(" (CASE WHEN rtc.MONDAY = 1 THEN 'T2' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.TUESDAY = 1 THEN ',T3' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.WEDNESDAY = 1 THEN ',T4' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.THURSDAY = 1 THEN ',T5' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.FRIDAY = 1 THEN ',T6' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.SATURDAY = 1 THEN ',T7' ELSE '' END) ");
		getCusList
				.append(" || (CASE WHEN rtc.SUNDAY = 1 THEN ',CN' ELSE '' END) AS VISIT_PLAN, ");
		getCusList.append(" CT.NAME_TEXT AS CUSTOMER_NAME_ADDRESS_TEXT, ");
		getCusList.append(" CT.ADDRESS AS ADDRESS, ");
		getCusList.append(" vp.STAFF_ID, ");
		getCusList.append(" vp.ROUTING_ID, ");
		getCusList.append(" staff.staff_name ");
		getCusList
				.append(" FROM visit_plan vp, routing rt, routing_customer rtc, CUSTOMER CT, staff ");
		getCusList.append(" where VP.ROUTING_ID = RT.ROUTING_ID ");
		getCusList.append(" and rt.routing_Id = rtc.routing_id ");
		getCusList.append(" and rtc.customer_id = ct.customer_id ");
		getCusList.append(" and vp.status = 1 ");
		getCusList.append(" and rt.status = 1 ");
		getCusList.append(" and rtc.status = 1 ");
		getCusList.append(" and ct.status = 1 ");
		getCusList.append(" AND substr(VP.FROM_DATE,1,10) <= ? ");
		param.add(date_now);
		getCusList.append(" AND ( substr(VP.TO_DATE,1,10) >= ? ");
		param.add(date_now);
		getCusList.append("      OR substr(VP.TO_DATE,1,10) IS NULL ) ");
		getCusList.append(" AND substr(rtc.start_DATE,1,10) <= ? ");
		param.add(date_now);
		getCusList.append(" AND ( substr(rtc.end_DATE,1,10) >= ? ");
		param.add(date_now);
		getCusList.append("      OR DATE(rtc.end_date) IS NULL ) ");
		getCusList.append(" and VP.STAFF_ID = STAFF.STAFF_ID ");
		getCusList.append(" and STAFF.STATUS = 1 ");
		getCusList.append(" and ct.shop_id = ? ");
		param.add(shopId);
		// thanhnn add check them shop cua staff tren sale online
		getCusList.append(" and staff.shop_id = ? ");
		param.add(shopId);
		getCusList.append(" and vp.staff_id in ( "+ listStaffId + ") ");
		if (!StringUtil.isNullOrEmpty(staffId)) {
			getCusList.append(" and staff.STAFF_ID = ?");
			param.add(staffId);
		}
		if (!StringUtil.isNullOrEmpty(cusCode)) {
			cusCode = StringUtil.escapeSqlString(cusCode);
			cusCode = DatabaseUtils.sqlEscapeString("%" + cusCode + "%");
			getCusList.append("	and (upper(CT.SHORT_CODE) like upper(");
			getCusList.append(cusCode);
			// param.add(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(CT.NAME_TEXT) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(CT.CUSTOMER_NAME) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
			getCusList.append("	or upper(CT.ADDRESS) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");

			getCusList.append(" ) ");

		}
//		if (!StringUtil.isNullOrEmpty(cusName)) {
//			cusName = StringUtil.escapeSqlString(cusName);
//			cusName = DatabaseUtils.sqlEscapeString("%" + cusName + "%");
//			getCusList.append("	and upper(CT.NAME_TEXT) like upper(");
//			getCusList.append(cusName);
//			// param.add(cusName);
//			getCusList.append(") escape '^' ");
//
//		}

		if (!StringUtil.isNullOrEmpty(day)) {
			getCusList.append("	and upper(VISIT_PLAN) like upper(?) ");
			param.add("%" + day + "%");
		}

		// if(staffId <= 0){
		// getCusList.append(" group by ct.customer_id ");
		// }

		getCusList.append(" ) CTT ");
		getCusList.append(" GROUP BY CTT.CUSTOMER_ID, CTT.STAFF_ID ");

		/*if (isSearch) {
			getCusList.append(" HAVING NUM_ITEM > 0 ");
		}*/


		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "  order by SHORT_CODE asc, CUSTOMER_NAME asc ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "SHORT_CODE")
				.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
				.addMapper(SortActionConstants.STAFF, "CTT.STAFF_NAME")
				.addMapper(SortActionConstants.NUM_IMAGE, "NUM_ITEM")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		getCusList.append(orderByStr);

		if (page > 0) {
			// get count
			getTotalCount.append("select count(1) as TOTAL_ROW from ("
					+ getCusList + ")");
			getCusList.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			getCusList
					.append(" offset "
							+ Integer.toString((page - 1)
									* Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(getCusList.toString(), param);

			if (c != null) {
				dto = new ImageListDTO();
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageListItemDTO(c);
						item.cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
						dto.listItem.add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0) {
				cTotalRow = rawQueries(getTotalCount.toString(), param);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	public WrongPlanCustomerDTO getWrongCustomerList(String staffId, String shopId)
			throws Exception {
		WrongPlanCustomerDTO dto = null;
		Cursor cursor = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ArrayList<String> param = new ArrayList<String>();
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT CT.CUSTOMER_ID                 AS CUSTOMER_ID, ");
		var1.append("               ct.short_code, 1, 3 || ' - ' || ct.customer_name)  as CUSTOMER_CODE_NAME, ");
		var1.append("               CT.CUSTOMER_NAME               AS CUSTOMER_NAME, ");
		var1.append("               CT.ADDRESS                     AS ADDRESS, ");
		var1.append("               ( CT.HOUSENUMBER ");
		var1.append("                 || ' ' ");
		var1.append("                 || CT.STREET )               AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )                     AS WEEK_PLAN ");
		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");
		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		param.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append(" ) RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("               AND ( DATE(VP.TO_DATE) >= DATE(?) ");
		param.add(date_now);
		var1.append("                      OR DATE(VP.TO_DATE) IS NULL ) ");
//		var1.append("               AND DATE(RTC.START_DATE) <= DATE(?) ");
//		param.add(date_now);
//		var1.append("               AND ( DATE(RTC.END_DATE) >= DATE(?) ");
//		param.add(date_now);
//		var1.append("                      OR DATE(RTC.END_DATE) IS NULL ) ");
		var1.append("               AND VP.STAFF_ID = ? ");
		param.add(staffId);
		var1.append("               AND VP.STATUS = 1 ");
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               AND RT.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               AND CT.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               ) CUS ");
		var1.append("       JOIN (SELECT CUSTOMER_ID AS CUSTOMER_ID_1, Strftime('%H:%M',START_TIME) AS START_TIME ");
		var1.append("             FROM   ACTION_LOG AL ");
		var1.append("             WHERE  DATE(AL.START_TIME) = DATE(?) ");
		param.add(date_now);
		var1.append("                    AND AL.END_TIME IS NOT NULL ");
		var1.append("                    AND AL.OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("               	 AND AL.STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("               	 AND AL.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("                    AND AL.IS_OR = 1) ALOG ");
		var1.append("         ON CUS.CUSTOMER_ID = ALOG.CUSTOMER_ID_1 ");
		var1.append("       LEFT JOIN (SELECT CUSTOMER_ID AS CUSTOMER_ID_2, ");
		var1.append("                         AMOUNT  AS REVENUE ");
		var1.append("                  FROM   RPT_STAFF_SALE_DETAIL ");
		var1.append("                  WHERE  1 = 1 ");
		var1.append("                         AND STAFF_ID = ? ");
		param.add("" + staffId);
		var1.append("                         AND ( DATE(SALE_DATE) = DATE(?) ) ");
		param.add(date_now);
		var1.append("                         AND SHOP_ID = ? ");
		param.add(shopId);
		var1.append("                  ) SALES ");
		var1.append("              ON CUS.CUSTOMER_ID = SALES.CUSTOMER_ID_2 ");

		try {
			cursor = rawQueries(var1.toString(), param);
			if (cursor != null) {
				dto = new WrongPlanCustomerDTO();
				if (cursor.moveToFirst()) {
					do {
						WrongPlanCustomerDTO.WrongPlanCustomerItem item = dto
								.newWrongPlanCustomerItem();
						item.initWrongPlanCusItem(cursor);
						dto.arrWrong.add(item);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * Request lay danh sach khach hang cua nhan vien trong tuyen Cho nhan vien
	 * giam sat vi tri khach hang
	 *
	 * @author banghn
	 * @param sortInfo
	 * @return
	 */
	public CustomerListDTO requestGetCustomerSaleList(String ownerId,
			String shopId, long staffId, String cusCode, String customer,
			String visitPlan, int page, String listStaffId, DMSSortInfo sortInfo) throws Exception{
		CustomerListDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
//		STAFF_TABLE staff = new STAFF_TABLE(mDB);
//		String listStaffId = staff.getListStaffOfSupervisor(ownerId, String.valueOf(shopId));
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT TMP.*, max(SYN_STATE) ");
		sql.append("FROM   (SELECT VP.visit_plan_id                 AS VISIT_PLAN_ID, ");
		sql.append("               VP.shop_id                       AS SHOP_ID, ");
		sql.append("               VP.staff_id                      AS STAFF_ID, ");
		sql.append("               CT.customer_id                   AS CUSTOMER_ID, ");
		sql.append("               CT.short_code                    AS CUSTOMER_CODE, ");
		sql.append("               CT.customer_name                 AS CUSTOMER_NAME, ");
		sql.append("               CT.name_text                 	AS CUSTOMER_NAME_TEXT, ");
		sql.append("               CT.lat                           AS LAT, ");
		sql.append("               CT.lng                           AS LNG, ");
		sql.append("               CT.address                       AS ADDRESS, ");
		sql.append("               st.[staff_code]                  AS STAFF_CODE, ");
		sql.append("               st.[staff_name]                  AS STAFF_NAME, ");
		sql.append("               sc.[STAFF_CUSTOMER_ID]           AS STAFF_CUSTOMER_ID, ");
		sql.append("               STRFTIME('%d/%m/%Y', sc.exception_order_date)  AS EXCEPTION_ORDER_DATE, ");
		sql.append("               sc.SYN_STATE                     AS SYN_STATE, ");
		sql.append("               ( CT.housenumber ");
		sql.append("                 || ' ' ");
		sql.append("                 || CT.street )                 AS STREET, ");
		sql.append("               ( CASE ");
		sql.append("                   WHEN RTC.monday = 1 THEN 'T2' ");
		sql.append("                   ELSE '' ");
		sql.append("                 end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.tuesday = 1 THEN ',T3' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.wednesday = 1 THEN ',T4' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.thursday = 1 THEN ',T5' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.friday = 1 THEN ',T6' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.saturday = 1 THEN ',T7' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end ");
		sql.append("                 || CASE ");
		sql.append("                      WHEN RTC.sunday = 1 THEN ',CN' ");
		sql.append("                      ELSE '' ");
		sql.append("                    end )                       AS TUYEN, ");

		// check co vi tri hay khong HAVE_POSITION
		sql.append("              ( CASE ");
		sql.append("                     WHEN (CT.LAT <= 0 OR CT.LNG <= 0) THEN 0 ");
		sql.append("                     ELSE 1 ");
		sql.append("                   END )       AS HAVE_POSITION, ");

		// LAY SO LAN UPDATE VI TRI, NGAY CAP NHAT VI TRI
		sql.append("               (SELECT Count(1) ");
		sql.append("                FROM   customer_position_log cpl ");
		sql.append("                WHERE  cpl.[customer_id] = ct.customer_id ");
		sql.append("                       AND cpl.[lat] > 0 ");
		sql.append("                       AND cpl.[lng] > 0)       AS NUM_UPDATE, ");
		sql.append("               Strftime('%d/%m/%Y', (SELECT cpl.create_date ");
		sql.append("                                     FROM   customer_position_log cpl ");
		sql.append("                                     WHERE  cpl.[customer_id] = ct.customer_id ");
		sql.append("                                            AND cpl.[lat] > 0 ");
		sql.append("                                            AND cpl.[lng] > 0 ");
		sql.append("                                     ORDER  BY create_date DESC ");
		sql.append("                                     LIMIT  1)) AS LAST_UPDATE ");
		sql.append("        FROM   visit_plan VP, ");
		sql.append("               routing RT, ");
		sql.append("               (SELECT routing_customer_id, ");
		sql.append("                       routing_id, ");
		sql.append("                       customer_id, ");
		sql.append("                       status, ");
		sql.append("                       week1, week2, week3, week4, ");
		sql.append("                       monday, ");
		sql.append("                       tuesday, ");
		sql.append("                       wednesday, ");
		sql.append("                       thursday, ");
		sql.append("                       friday, ");
		sql.append("                       saturday, ");
		sql.append("                       sunday, ");
		sql.append("                       seq2, ");
		sql.append("                       seq3, ");
		sql.append("                       seq4, ");
		sql.append("                       seq5, ");
		sql.append("                       seq6, ");
		sql.append("                       seq7, ");
		sql.append("                       seq8 ");
		sql.append("                FROM   routing_customer ");

		sql.append("                WHERE (substr(END_DATE,1,10) >= ? OR substr(END_DATE,1,10) IS NULL) and ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("                substr(START_DATE,1,10) <= ? and ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("                ( ");
		sql.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		sql.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("		   (case when 1");
		sql.append("			and (");
		sql.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  then 7 ");
		sql.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  end ) as integer) < ");
		sql.append("		 cast((case when strftime('%w', ?) = '0' ");
		sql.append("										  then 7 ");
		sql.append("										  else strftime('%w', ?)                          ");
		sql.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		sql.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("		   (case when 1");
		sql.append("			and (");
		sql.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  then 7 ");
		sql.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  end ) as integer) < ");
		sql.append("		 cast((case when strftime('%w', ?) = '0' ");
		sql.append("										  then 7 ");
		sql.append("										  else strftime('%w', ?)                          ");
		sql.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		sql.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("		   (case when 1");
		sql.append("			and (");
		sql.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  then 7 ");
		sql.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  end ) as integer) < ");
		sql.append("		 cast((case when strftime('%w', ?) = '0' ");
		sql.append("										  then 7 ");
		sql.append("										  else strftime('%w', ?)                          ");
		sql.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		sql.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("		   (case when 1");
		sql.append("			and (");
		sql.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  then 7 ");
		sql.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("									  end ) as integer) < ");
		sql.append("		 cast((case when strftime('%w', ?) = '0' ");
		sql.append("										  then 7 ");
		sql.append("										  else strftime('%w', ?)                          ");
		sql.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		sql.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		sql.append("                ) RTC,  ");

		sql.append("               customer CT ");
		// LAY THONG TIN NHANVINE
		sql.append("               JOIN staff st ");
		sql.append("                 ON VP.staff_id = st.staff_id ");
		sql.append("                    AND st.status = 1 ");
		sql.append("                    AND st.staff_id IN ( " + listStaffId +" ) ");
		sql.append("                    AND st.shop_id = ct.shop_id ");

		// THONG TIN KHACH HANG
		sql.append("               LEFT JOIN staff_customer sc ");
		sql.append("                      ON (sc.staff_id = VP.staff_id ");
		sql.append("                      	and sc.customer_id = RTC.customer_id AND sc.shop_id = ? )");
		param.add(shopId);
		totalParam.add(shopId);
		sql.append("        WHERE  1 = 1 ");
		sql.append("               AND VP.routing_id = RT.routing_id ");
		sql.append("               AND RTC.routing_id = RT.routing_id ");
		sql.append("               AND RTC.customer_id = CT.customer_id ");
		sql.append("               AND Ifnull(substr(VP.from_date,1,10) <= ?, 0) ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("               AND Ifnull(substr(VP.to_date,1,10) >= ?, 1) ");
		param.add(date_now);
		totalParam.add(date_now);
		sql.append("               AND CT.shop_id = ? ");
		param.add(shopId);
		totalParam.add(shopId);
		sql.append("               AND VP.shop_id = ? ");
		param.add(shopId);
		totalParam.add(shopId);

		sql.append("               AND VP.status in (0,1) ");
		sql.append("               AND RT.status = 1 ");
		sql.append("               AND CT.status = 1 ");
		sql.append("               AND RTC.status in (0,1) ");
		sql.append("               ) AS TMP ");
		sql.append("WHERE  1 = 1 ");

		if (!StringUtil.isNullOrEmpty(visitPlan)) {
			sql.append("       AND Upper(TUYEN) LIKE Upper(?) ");
			param.add("%" + visitPlan + "%");
			totalParam.add("%" + visitPlan + "%");
		}
		if (staffId > 0) {
			sql.append("       AND STAFF_ID = ? ");
			param.add(""+staffId);
			totalParam.add(""+staffId);
		}
		if (!StringUtil.isNullOrEmpty(customer)) {
			customer = StringUtil.getEngStringFromUnicodeString(customer);
			customer = StringUtil.escapeSqlString(customer);
			sql.append("	and ( upper(CUSTOMER_CODE) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			sql.append("	or upper(CUSTOMER_NAME_TEXT) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			sql.append("	or upper(CUSTOMER_NAME) like upper(?) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
			sql.append("	or upper(ADDRESS) like upper(?)) ");
			param.add("%" + customer + "%");
			totalParam.add("%" + customer + "%");
		}
		// group theo staff_id, customer_id lay syn_state lon nhat
		sql.append("group by staff_id, customer_id ");
		// thu tu order
		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   order by STAFF_CODE asc, STAFF_NAME asc, CUSTOMER_CODE asc, CUSTOMER_NAME asc, HAVE_POSITION asc  ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
				.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		sql.append(orderByStr);
		// check phan trang

		if (page > 0) {
			// get count
			totalPageSql.append("select count(1) as TOTAL_ROW from ("
					+ sql + ")");
			sql.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sql
					.append(" offset "
							+ Integer.toString((page - 1)
									* Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(sql.toString(), param);
			if (c != null) {
				if (dto == null) {
					dto = new CustomerListDTO();
				}
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataCustomerSaleItem(c);
						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("Error close Cursor", ex.toString());
			}
		}
		try {
			if (page > 0) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalCustomer(cTotalRow.getInt(0));
					}
				}
			}
		} finally {
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
				MyLog.e("Error close TotalRO", ex.toString());
			}
		}
		return dto;
	}

	/**
	 * HieuNH Dem so luong khach hang chua phat sinh doanh so trong thang
	 *
	 * @param shopId
	 * @param staffOwnerId
	 *            : id NVGS
	 * @param visit_plan
	 *            : tuyen nao (T2, T3,...,)
	 * @param staffId
	 *            : id NVBH
	 * @param page
	 * @return
	 * @throws Exception
	 */

	public int getCountCustomerNotPsdsInMonthReport(int shopId,
			String staffOwnerId, String visit_plan, String staffId) throws Exception {
		int count = 0;
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		sqlQuery.append("SELECT Count(*) COUNT ");
		sqlQuery.append("FROM   (SELECT s.staff_id, ");
		sqlQuery.append("               s.staff_name NVBH, ");
		sqlQuery.append("               a.customer_id ");
		sqlQuery.append("        FROM   (SELECT * ");
		sqlQuery.append("                FROM   routing_customer ");
		sqlQuery.append("                WHERE  Round(Strftime('%W', 'now', 'localtime') - STRFTIME('%W', START_DATE) ");
		sqlQuery.append("                       ) >= 0 ");
		sqlQuery.append("                       AND status = 1) a, ");
		sqlQuery.append("               visit_plan b, ");
		sqlQuery.append("               routing c, ");
		sqlQuery.append("               staff s, ");
		sqlQuery.append("               shop sh, ");
		sqlQuery.append("               customer cu ");
		sqlQuery.append("        WHERE  1 = 1 ");
		sqlQuery.append("               AND b.routing_id = a.routing_id ");
		sqlQuery.append("               AND c.routing_id = a.routing_id ");
		sqlQuery.append("               AND b.staff_id = s.staff_id ");
		sqlQuery.append("               AND a.customer_id = cu.customer_id ");
		sqlQuery.append("               AND s.shop_id = sh.shop_id ");
		sqlQuery.append("               AND sh.status = 1 ");
		sqlQuery.append("               AND a.status = 1 ");
		sqlQuery.append("               AND b.shop_id = ? ");
		params.add("" + shopId);
		sqlQuery.append("               AND s.status = 1 ");
		sqlQuery.append("               AND c.status = 1 ");
		sqlQuery.append("               AND cu.status = 1 ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) >= Date(b.from_date) ");
		sqlQuery.append("                     AND ( b.to_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) <= ");
		sqlQuery.append("                               Date(b.to_date) ) ) ");
		sqlQuery.append("               AND ( (SELECT Date('now', 'localtime')) >= Date(a.start_date) ");
		sqlQuery.append("                     AND ( a.end_date IS NULL ");
		sqlQuery.append("                            OR (SELECT Date('now', 'localtime')) <= ");
		sqlQuery.append("                               Date(a.end_date) ) ) ");
		sqlQuery.append("               AND b.staff_id IN (SELECT staff_id ");
		sqlQuery.append("                                  FROM   staff a, ");
		sqlQuery.append("                                         channel_type b ");
		sqlQuery.append("                                  WHERE  a.staff_type_id = b.channel_type_id ");
		sqlQuery.append("                                         AND a.status = 1 ");
		sqlQuery.append("                                         AND b.status = 1 ");
		sqlQuery.append("                                         AND a.shop_id = ? ");
		params.add("" + shopId);
		if (!StringUtil.isNullOrEmpty(staffOwnerId)) {
			String listStaff = staff.getListStaffOfSupervisor(staffOwnerId, shopId + "");
			sqlQuery.append(" AND staff_id in ( ");
			sqlQuery.append(listStaff);
			sqlQuery.append(" )");
		}
		sqlQuery.append("                                 )) tb1 ");
		sqlQuery.append("       LEFT JOIN staff_customer tb2 ");
		sqlQuery.append("              ON ( tb1.staff_id = tb2.staff_id ");
		sqlQuery.append("                   AND tb1.customer_id = tb2.customer_id AND tb2.SHOP_ID = ?) ");
		params.add("" + shopId);
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND ( Date(tb2.last_approve_order) IS NULL ");
		sqlQuery.append("              OR tb2.last_approve_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_approve_order) < (SELECT ");
		sqlQuery.append("                 Date('now','localtime', 'start of month' ");
		sqlQuery.append("                 )) ) ");
		sqlQuery.append("       AND ( tb2.last_order IS NULL ");
		sqlQuery.append("              OR tb2.last_order = '' ");
		sqlQuery.append("              OR Date(tb2.last_order) < (SELECT Date('now', 'localtime')) ) ");
		if (!StringUtil.isNullOrEmpty(visit_plan)) {// ko phai chon tat ca
			sqlQuery.append("	and TUYEN like ? ");
			params.add("%" + visit_plan + "%");
		}
		if (!StringUtil.isNullOrEmpty(staffId)) {// ko phai chon tat ca
			sqlQuery.append("	and tb1.STAFF_ID = ?");
			params.add(staffId);
		}

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));

			if (c != null && c.moveToFirst()) {
				count = CursorUtil.getInt(c, "COUNT");
			}

		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return count;
	}

	/**
	 * TRUNGHQM lay danh sach khach hang chua phat sinh doanh so trong thang
	 *
	 * @param shopId
	 * @param staffOwnerId
	 *            : id NVGS
	 * @param visit_plan
	 *            : tuyen nao (T2, T3,...,)
	 * @param staffId
	 *            : id NVBH
	 * @param page
	 * @param sortInfo
	 * @return
	 * @throws Exception
	 */
	public CustomerNotPsdsInMonthReportDTO getCustomerNotPsdsInMonthReport(int shopId, String staffOwnerId, String visit_plan, String staffId,
																		   int page, String listStaff, int sysCalUnApproved, DMSSortInfo sortInfo)
			throws Exception {
		String litstStaffId = Constants.STR_BLANK;
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			litstStaffId = staffOwnerId;
		}else {
			litstStaffId = listStaff;
		}
		CustomerNotPsdsInMonthReportDTO dto = new CustomerNotPsdsInMonthReportDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
//		String startOfMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, 0);
//		String startOfOnePreviousMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -1);
//		// ngay cuoi thang hien tai
//		String lastDayOfMonth = DateUtils.getLastDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, 0);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		String startOfOnePreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -1);
		// ngay cuoi thang hien tai
		String lastDayOfMonth = new EXCEPTION_DAY_TABLE(mDB).getEndDayOfOffsetCycle(new Date(), 0);
		String firstDateOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_STRING_YYYY_MM_DD, new Date(), 0);
		StringBuffer countSqlQuery = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT tb1.housenumber                                          SoNha, ");
		sqlQuery.append("       tb1.street                                               Duong, ");
		sqlQuery.append("       tb1.ADDRESS_CUS                                          ADDRESS, ");
		sqlQuery.append("       tb1.customer_id                                          customerId, ");
		sqlQuery.append("       tb1.short_code                                           MaKH, ");
		sqlQuery.append("       tb1.customer_name                                        TenKH, ");
//		sqlQuery.append("       Strftime('%d/%m/%Y',tb2.last_order)                      ORDER_DATE, ");
//		sqlQuery.append("       tb2.last_approve_order                                   APPROVE_ORDER_DATE, ");
//		sqlQuery.append("       tb2.last_order                                           ORDER_DATE, ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlQuery.append("       tb2.last_approve_order                               ORDER_DATE, ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlQuery.append("       tb2.last_order                                       ORDER_DATE, ");
		}else {
			sqlQuery.append("       tb2.last_order                                       ORDER_DATE, ");
		}
		sqlQuery.append("       tb1.staff_id                                             staffId, ");
		sqlQuery.append("       tb1.staff_name                                           NVBH, ");
		sqlQuery.append("       tb1.staff_code, ");
		sqlQuery.append("       (SELECT Count(al.action_log_id) ");
		sqlQuery.append("        FROM action_log al ");
		sqlQuery.append("        WHERE (al.object_type = 0 ");
		sqlQuery.append("                OR al.object_type = 1 ) ");
		sqlQuery.append("               AND al.customer_id = tb1.customer_id ");
		sqlQuery.append("               AND al.staff_id = tb1.staff_id ");
		sqlQuery.append("               AND substr(al.start_time, 1, 10) >= substr(?, 1, 10) ) AS SLGT, ");
		params.add(startOfMonth);
		sqlQuery.append("        (SELECT sum(amount) ");
		sqlQuery.append("         FROM (SELECT  SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlQuery.append(" 	                   rsim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sqlQuery.append(" 	 				   rsim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlQuery.append(" 	                   rsim.amount ");
		}else {
			sqlQuery.append(" 	                   rsim.amount ");
		}
		sqlQuery.append(") 	                    AS AMOUNT ");
		sqlQuery.append("               FROM rpt_sale_primary_month rsim ");
		sqlQuery.append("               WHERE substr(affect_date, 1, 10) = substr(?, 1, 10)");
		params.add(startOfOnePreviousMonth);
		sqlQuery.append("                     AND rsim.customer_id = tb1.customer_id ");
		sqlQuery.append("                     AND rsim.staff_id = tb1.staff_id ");
		sqlQuery.append("                     AND rsim.shop_id = ? ");
		params.add(""+shopId);
		sqlQuery.append("               GROUP BY rsim.product_id  ) ) AS DSThangTruoc, ");
		sqlQuery.append("        (CASE  WHEN tb1.monday = 1 THEN 'T2'  ELSE ''  END ) ");
		sqlQuery.append("        || (CASE  WHEN tb1.tuesday = 1 THEN ',T3' ELSE '' END ) ");
		sqlQuery.append("        || (CASE WHEN tb1.wednesday = 1 THEN ',T4' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.thursday = 1 THEN ',T5' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.friday = 1 THEN ',T6' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.saturday = 1 THEN ',T7' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.sunday = 1 THEN ',CN' ELSE '' END )   AS TUYEN, ");
		sqlQuery.append("        ( CASE WHEN tb1.monday = 1 THEN '2' ELSE '' END ) ");
		sqlQuery.append("        || (CASE WHEN tb1.tuesday = 1 THEN ',3' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.wednesday = 1 THEN ',4' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.thursday = 1 THEN ',5' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.friday = 1 THEN ',6' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.saturday = 1 THEN ',7' ELSE '' END ) ");
		sqlQuery.append("        || ( CASE WHEN tb1.sunday = 1 THEN ',1' ELSE '' END ) AS TUYEN_LIST, ");
		sqlQuery.append("        ( CASE WHEN ( tb2.last_approve_order IS NULL ");
		sqlQuery.append("                          OR tb2.last_approve_order = '' ");
		sqlQuery.append("                          OR substr(tb2.last_approve_order,1,10) < substr(?, 1, 10)) ");
		params.add(startOfOnePreviousMonth);
		sqlQuery.append("                          THEN 1   ELSE 0 END )        LOSS_DISTRIBUTION, ");
		sqlQuery.append("        substr(tb1.start_date,1, 10)           START_DATE, ");
		sqlQuery.append("        substr(tb1.end_date,1,10)              END_DATE, ");
		sqlQuery.append("        tb1.WEEK1                              WEEK1, ");
		sqlQuery.append("        tb1.WEEK2                              WEEK2, ");
		sqlQuery.append("        tb1.WEEK3                              WEEK3, ");
		sqlQuery.append("        tb1.WEEK4                              WEEK4, ");
		sqlQuery.append("        (case WHEN substr(?, 1, 10) >=substr(tb1.start_date,1,10) then ? ");
		params.add(startOfMonth);
		params.add(startOfMonth);
		sqlQuery.append("            ELSE tb1.start_date END) AS START_DATE_MONTH, ");
		sqlQuery.append("        (case WHEN ifnull( substr(?, 1, 10) >= substr(tb1.end_date,1, 10), 0) then tb1.end_date ");
		params.add(lastDayOfMonth);
		sqlQuery.append("            ELSE substr(?, 1, 10) ");
		params.add(lastDayOfMonth);
		sqlQuery.append("            END) AS END_DATE_MONTH, ");
		sqlQuery.append(" ");
		sqlQuery.append("        (case WHEN substr(?, 1, 10) >substr(tb1.start_date,1, 10) ");
		params.add(startOfMonth);
		sqlQuery.append("        then  ((cast((julianday(? ) - julianday(?)) / 7  as integer) ");
		params.add(dateNow);
		params.add(firstDateOfYear);
		sqlQuery.append("        +     (case WHEN 1 AND (  cast((case WHEN strftime('%w', ? ) = '0' then 7 ");
		params.add(dateNow);
		sqlQuery.append("                    ELSE strftime('%w',? ) END ) as integer) ");
		params.add(dateNow);
		sqlQuery.append("                    <    cast((case WHEN strftime('%w', ? ) = '0' then 7 ");
		params.add(firstDateOfYear);
		sqlQuery.append("                    ELSE strftime('%w', ? ) ");
		params.add(firstDateOfYear);
		sqlQuery.append("                END ) as integer) )    then 1 ");
		sqlQuery.append("                ELSE 0   END)) % 4 + 1)    ELSE 1  END) AS START_WEEK, ");
		sqlQuery.append("        (SELECT group_concat(substr(day_off, 1,10)) ");
		sqlQuery.append("        FROM exception_day ");
		sqlQuery.append("        WHERE strftime('%m',day_off)=strftime('%m',?)) as EXCEPTION_DAY ");
		params.add(dateNow);
		sqlQuery.append("FROM (SELECT cu.housenumber housenumber, ");
		sqlQuery.append("            cu.street street, ");
		sqlQuery.append("            cu.address ADDRESS_CUS, ");
		sqlQuery.append("            * ");
		sqlQuery.append("      FROM (SELECT * ");
		sqlQuery.append("            FROM routing_customer ");
		sqlQuery.append("            WHERE 1 = 1 ");
		sqlQuery.append("                  AND  substr(START_DATE, 1, 10)<= ? ");
		params.add(dateNow);
		sqlQuery.append("                  AND (end_date IS NULL ");
		sqlQuery.append("                      OR end_date = '' ");
		sqlQuery.append("                      OR substr(end_date, 1, 10) >= ? ) ");
		params.add(dateNow);
		sqlQuery.append("                  AND status = 1 ");
		sqlQuery.append("                  AND (monday ");
		sqlQuery.append("                       OR tuesday ");
		sqlQuery.append("                       OR wednesday ");
		sqlQuery.append("                       OR thursday ");
		sqlQuery.append("                       OR friday ");
		sqlQuery.append("                       OR saturday ");
		sqlQuery.append("                       OR sunday) ");
		sqlQuery.append("           ) a, ");
		sqlQuery.append("           visit_plan b, ");
		sqlQuery.append("           routing c, ");
		sqlQuery.append("           staff s, ");
		sqlQuery.append("           shop sh, ");
		sqlQuery.append("           customer cu ");
		sqlQuery.append("      WHERE 1 = 1 ");
		sqlQuery.append("            AND b.routing_id = a.routing_id ");
		sqlQuery.append("            AND c.routing_id = a.routing_id ");
		sqlQuery.append("            AND b.staff_id = s.staff_id ");
		sqlQuery.append("            AND a.customer_id = cu.customer_id ");
		sqlQuery.append("            AND s.shop_id = sh.shop_id ");
		sqlQuery.append("            AND sh.status = 1 ");
		sqlQuery.append("            AND a.status = 1 ");
		sqlQuery.append("            AND b.status = 1 ");
		sqlQuery.append("            AND s.status = 1 ");
		sqlQuery.append("            AND c.status = 1 ");
		sqlQuery.append("            AND cu.status = 1 ");
		sqlQuery.append("            AND cu.shop_id = ? ");
		params.add(""+shopId);
		sqlQuery.append("            AND substr(b.from_date, 1, 10) <= ? ");
		params.add(dateNow);
		sqlQuery.append("            AND (b.to_date IS NULL ");
		sqlQuery.append("                   OR ? <= substr(b.to_date, 1, 10)) ");
		params.add(dateNow);
		sqlQuery.append("            AND substr(a.start_date,1,10) <= ? ");
		params.add(dateNow);
		sqlQuery.append("            AND (a.end_date IS NULL ");
		sqlQuery.append("                 OR Date(a.end_date) >= ? ) ");
		params.add(dateNow);
		sqlQuery.append("        AND b.staff_id in ( " + litstStaffId + " ) ) tb1 ");
		sqlQuery.append("  LEFT JOIN staff_customer tb2 ");
		sqlQuery.append("                ON (tb1.staff_id = tb2.staff_id ");
		sqlQuery.append("                    AND tb1.customer_id = tb2.customer_id ");
		sqlQuery.append("                    AND tb2.shop_id = ?  ) ");
		params.add(""+shopId);
		sqlQuery.append("WHERE 1 = 1 ");
		sqlQuery.append("      AND ( tb2.last_approve_order IS NULL ");
		sqlQuery.append("            OR tb2.last_approve_order = '' ");
		sqlQuery.append("            OR substr(tb2.last_approve_order, 1, 10) < substr(?, 1, 10) ) ");
		params.add(startOfMonth);
		sqlQuery.append("      AND ( ");
		sqlQuery.append("                tb2.last_order IS NULL ");
		sqlQuery.append("                OR tb2.last_order = '' ");
		sqlQuery.append("                OR substr(tb2.last_order, 1, 10) < ?  ) ");
		params.add(startOfMonth);
		if (!StringUtil.isNullOrEmpty(visit_plan)) {// ko phai chon tat ca
			sqlQuery.append("	and TUYEN like ? ");
			params.add("%" + visit_plan + "%");
		}
		if (!StringUtil.isNullOrEmpty(staffId)) {// ko phai chon tat ca
			sqlQuery.append("	and staffId = ?");
			params.add(staffId);
		}

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY SLGT DESC, tb1.CUSTOMER_CODE, tb1.CUSTOMER_NAME ";

		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
		.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
		.defaultOrderString(defaultOrderByStr)
		.build(sortInfo);

		//add order string
		sqlQuery.append(orderByStr);

		if (page > 0) {
			// get count
			countSqlQuery.append("select count(1) as TOTAL_ROW from ("
					+ sqlQuery + ")");
			sqlQuery.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlQuery.append(" offset "
					+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}
		Cursor countC = null;
		try {
			EXCEPTION_DAY_TABLE ex = new EXCEPTION_DAY_TABLE(mDB);
			dto.listExcepTionDay = ex.getListExceptionDayInCycle(Constants.STR_BLANK + shopId);

				countC = this.rawQueries(countSqlQuery.toString(), params);
				if (countC != null && countC.moveToFirst()) {
					dto.totalList = CursorUtil.getInt(countC, "TOTAL_ROW");
				}

		} finally {
			try {
				if (countC != null) {
					countC.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return dto;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param lessThan2MinList
	 * @return
	 * @return: LessThan2MinsDTOvoid
	 * @throws:
	 */
	public GsnppLessThan2MinsDTO requestGetLessThan2Mins(String shopId, int staffId,
			String lessThan2MinList) {
		GsnppLessThan2MinsDTO dto = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT CUSTOMER.CUSTOMER_ID, ");
		var1.append("               ( SUBSTR(CUSTOMER.CUSTOMER_CODE, 1, 3) ");
		var1.append("                 || ' - ' ");
		var1.append("                 || CUSTOMER.CUSTOMER_NAME )            AS CUS_CODE_NAME, ");
		var1.append("               ( CUSTOMER.HOUSENUMBER ");
		var1.append("                 || ' ' ");
		var1.append("                 || CUSTOMER.STREET )                   AS STREET, ");
		var1.append("               STRFTIME('%H:%M', ACTION_LOG.START_TIME) AS START_TIME, ");
		var1.append("               STRFTIME('%H:%M', ACTION_LOG.END_TIME)   AS END_TIME ");
		var1.append("        FROM   CUSTOMER, ");
		var1.append("               ACTION_LOG ");
		var1.append("        WHERE  CUSTOMER.CUSTOMER_ID = ACTION_LOG.CUSTOMER_ID ");
		var1.append("               AND DATE(ACTION_LOG.START_TIME) = DATE(?) ");
		var1.append("               AND ACTION_LOG.OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("               AND ACTION_LOG.STAFF_ID = ? ");
		var1.append("               AND ACTION_LOG.SHOP_ID = ? ");
		var1.append("               AND CUSTOMER.CUSTOMER_ID IN ( "
				+ lessThan2MinList + " )) CUS ");
		var1.append("       LEFT JOIN (SELECT CUSTOMER_ID AS CUSTOMER_ID_1, ");
		var1.append("                         AMOUNT  AS REVENUE ");
		var1.append("                  FROM   RPT_STAFF_SALE_DETAIL ");
		var1.append("                  WHERE  1 = 1 ");
		var1.append("                         AND STAFF_ID = ? ");
		var1.append("                         AND ( DATE(SALE_DATE) = DATE(?) ) ");
		var1.append("                  ) SALES ");
		var1.append("              ON CUS.CUSTOMER_ID = SALES.CUSTOMER_ID_1 ");

		Cursor cursor = null;
		try {
			cursor = rawQuery(var1.toString(), new String[] { date_now,
					"" + staffId, shopId, "" + staffId, date_now });
			if (cursor != null) {
				dto = new GsnppLessThan2MinsDTO();
				if (cursor.moveToFirst()) {
					do {
						LessThan2MinsItem item = dto.newLessThan2MinsItem();
						item.initItem(cursor);
						dto.arrList.add(item);
					} while (cursor.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return dto;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param parentStaffId
	 * @param month
	 * @param isGetTotalPage
	 * @param page
	 * @return
	 * @return: CustomerListDTOvoid
	 * @throws:
	 */
	public CustomerListDTO requestGetCusNoPSDS(int rptSaleHisId,
			boolean isGetTotalPage, int page) {
		CustomerListDTO dto = null;
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<String> totalPageParam = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT CUSTOMER.CUSTOMER_ID, ");
		var1.append("       SUBSTR(CUSTOMER.CUSTOMER_CODE, 1, 3) AS CUSTOMER_CODE, ");
		var1.append("       CUSTOMER.CUSTOMER_NAME, ");
		var1.append("       CUSTOMER.HOUSENUMBER ");
		var1.append("       || ' ' ");
		var1.append("       || CUSTOMER.STREET                   AS ADDRESS ");
		var1.append("FROM   RPT_CUSTOMER_INACTIVE, ");
		var1.append("       CUSTOMER ");
		var1.append("WHERE  RPT_SALE_HISTORY_ID = ? ");
		params.add("" + rptSaleHisId);
		totalPageParam.add("" + rptSaleHisId);
		var1.append("       AND RPT_CUSTOMER_INACTIVE.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID ");
		var1.append("ORDER BY CUSTOMER_NAME ");

		// get count
		StringBuffer totalCount = new StringBuffer();
		if (isGetTotalPage == true) {
			totalCount.append("select count(*) as TOTAL_ROW from (" + var1
					+ ")");
		}

		if (page > 0) {
			var1.append("       limit ? offset ?");
			params.add("" + 5);
			params.add("" + (page - 1) * 5);
		}

		Cursor c = null;
		Cursor c_totalRow = null;
		try {
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				dto = new CustomerListDTO();
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataFromNoPSDS(c);
						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == true) {
				c_totalRow = rawQueries(totalCount.toString(), totalPageParam);
				if (c_totalRow != null && dto != null) {
					if (c_totalRow.moveToFirst()) {
						dto.setTotalCustomer(c_totalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (c_totalRow != null) {
					c_totalRow.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * get Select Customer List NVBH
	 *
	 * @author: ToanTT
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public CustomerListDTO getSelectCustomerListNVBH(String shopId, String staffId, double lat, double lng) throws Exception {
		CustomerListDTO dto = null;
		Cursor c = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT TEMP.*, AL.OBJECT_TYPE AS IS_VISIT, AL.END_TIME AS VISIT_END_TIME ");
		var1.append("FROM   (SELECT    CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("	               SUBSTR(CT.CUSTOMER_CODE, 1, 3)  AS CUSTOMER_CODE, ");
		var1.append("	               CT.CUSTOMER_NAME        AS CUSTOMER_NAME,  ");
		var1.append("	               CT.LAT                  AS LAT, ");
		var1.append("	               CT.LNG                  AS LNG, ");
		var1.append("	               ( CT.HOUSENUMBER ");
		var1.append("	                 || ' ' ");
		var1.append("	                 || CT.STREET )        AS STREET, ");
		var1.append("	               ( CASE ");
		var1.append("	                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("	                   ELSE '' ");
		var1.append("	                 END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.WEDNESDAY = 1 THEN ',T4'  ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END ");
		var1.append("	                 || CASE ");
		var1.append("	                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("	                      ELSE '' ");
		var1.append("	                    END )              AS TUYEN ");
		var1.append("       FROM   VISIT_PLAN VP, ");
		var1.append("	               ROUTING RT, ");
		var1.append("	               (SELECT ROUTING_ID, ");
		var1.append("	                       CUSTOMER_ID, ");
		var1.append("	                       STATUS, ");
		var1.append("	                       WEEK1, WEEK2, WEEK3, WEEK4, ");
		var1.append("	                       MONDAY, ");
		var1.append("	                       TUESDAY, ");
		var1.append("	                       WEDNESDAY,  ");
		var1.append("	                       THURSDAY, ");
		var1.append("	                       FRIDAY, ");
		var1.append("	                       SATURDAY, ");
		var1.append("	                       SUNDAY ");
		var1.append("	                FROM   ROUTING_CUSTOMER ");
		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");

		var1.append(" ) RTC, ");
		var1.append("	               CUSTOMER CT ");
		var1.append("	        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("	               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("	               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("	               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("	               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("	               AND VP.STAFF_ID = ? ");
		param.add(staffId);
		var1.append("	               AND VP.STATUS = 1 ");
		var1.append("	               AND RT.STATUS = 1 ");
		var1.append("	               AND CT.STATUS = 1 ");
		var1.append("	               AND RTC.STATUS = 1 ");
		var1.append("	               AND VP.shop_id = ? ");
		param.add(shopId);
		var1.append("	               AND CT.shop_id = ? ");
		param.add(shopId);
		var1.append("	               ) TEMP ");
		var1.append("                LEFT JOIN (SELECT * ");
		var1.append("		                          FROM   ACTION_LOG ");
		var1.append("		                          WHERE  DATE(?) = DATE(ACTION_LOG.START_TIME) ");
		param.add(date_now);
		var1.append("                               AND STAFF_ID = ? ");
		param.add(staffId);
		var1.append("                               AND SHOP_ID = ? ");
		param.add(shopId);
		var1.append("                               AND OBJECT_TYPE IN ( 0, 1 ))AL ");
		var1.append("		                      ON TEMP.CUSTOMER_ID = AL.CUSTOMER_ID ");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (dto == null) {
					dto = new CustomerListDTO();
				}
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						if (item.initDataFromSelectCustomer(c, lat, lng)) {
							dto.getCusList().add(item);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 * get Select Customer List GSNPP
	 * @author: ToanTT
	 * @param shopId
	 * @param staffId
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public CustomerListDTO getSelectCustomerListGSNPP(String shopId,
			String staffId, double lat, double lng) throws Exception {
		CustomerListDTO dto = null;
		Cursor c = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String staffList = PriUtils.getInstance().getListStaffOfSupervisor(staffId, shopId);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT GS_CUS_LIST.*, ");
		var1.append("	      	AL.OBJECT_TYPE AS IS_VISIT, ");
		var1.append("	      	AL.END_TIME    AS VISIT_END_TIME ");
		var1.append("FROM (SELECT * ");
		var1.append("	FROM (SELECT DISTINCT(CUSTOMER_ID), CUSTOMER_CODE, CUSTOMER_NAME, STREET, LAT, LNG ");
		var1.append("	      FROM (SELECT    VP.STAFF_ID AS STAFF_ID, ");
		var1.append("                   CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("		               SUBSTR(CT.CUSTOMER_CODE, 1, 3)  AS CUSTOMER_CODE, ");
		var1.append("		               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("		               CT.LAT                  AS LAT, ");
		var1.append("		               CT.LNG                  AS LNG, ");
		var1.append("		               ( CT.HOUSENUMBER ");
		var1.append("		                 || ' ' ");
		var1.append("		                 || CT.STREET )        AS STREET ");
		var1.append("	       FROM   VISIT_PLAN VP, ");
		var1.append("		               ROUTING RT, ");
		var1.append("		               (SELECT ROUTING_ID, ");
		var1.append("		                       CUSTOMER_ID, ");
		var1.append("		                       STATUS, WEEK1, WEEK2, WEEK3, WEEK4 ");
		var1.append("		                FROM   ROUTING_CUSTOMER ");
		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append(" ) RTC, ");
		var1.append("		               CUSTOMER CT ");
		var1.append("		        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("		               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("		               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("		               AND VP.STATUS = 1 ");
		var1.append("                      AND VP.SHOP_ID = ?");
		param.add(shopId);
		var1.append("		               AND CT.shop_id = ? ");
		param.add(shopId);
		var1.append("		               AND RT.STATUS = 1 ");
		var1.append("		               AND CT.STATUS = 1 ");
		var1.append("		               AND RTC.STATUS = 1 ");
		var1.append("		               ) TEMP, ");
		var1.append("	               (SELECT STAFF_ID ");
		var1.append("	                       FROM STAFF ");
		var1.append("	                         WHERE STAFF_ID IN ( ");
		var1.append(staffList);
		var1.append("	                         )");
		var1.append("	                               AND SHOP_ID = ?");
		param.add(shopId);
		var1.append("	                               AND STATUS = 1) STAFFLIST ");
		var1.append("	           WHERE TEMP.STAFF_ID = STAFFLIST.STAFF_ID ");
		var1.append("	           ORDER BY TEMP.CUSTOMER_ID ASC) GS_CUS ");
		var1.append("	           LEFT JOIN (SELECT DISTINCT (CUSTOMER_ID), TUYEN ");
		var1.append("	     FROM   (SELECT TPD.STAFF_ID ");
		var1.append("	             FROM   TRAINING_PLAN TP, ");
		var1.append("	                    TRAINING_PLAN_DETAIL TPD ");
		var1.append("	             WHERE  TP.STAFF_ID = ? ");
		param.add(staffId);
		var1.append("	             AND    TP.TRAINING_PLAN_ID = TPD.TRAINING_PLAN_ID ");
		var1.append("	             AND    TPD.STATUS IN ( 0, 1 ) ");
		var1.append("	             AND    TP.STATUS = 1 ");
		var1.append("	             AND    DATE(TP.MONTH, 'start of month') = DATE(?, 'start of month') ");
		param.add(date_now);
		var1.append("	             AND    DATE(TPD.TRAINING_DATE) = DATE(?)) GS_TRAIN, ");
		param.add(date_now);
		var1.append("	            (SELECT VP.STAFF_ID AS STAFF_ID, ");
		var1.append("                       CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("		               ( CASE ");
		var1.append("		                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("		                   ELSE '' ");
		var1.append("		                 END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.WEDNESDAY = 1 THEN ',T4'  ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END ");
		var1.append("		                 || CASE ");
		var1.append("		                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("		                      ELSE '' ");
		var1.append("		                    END )              AS TUYEN ");
		var1.append("	       FROM   VISIT_PLAN VP, ");
		var1.append("		               ROUTING RT, ");
		var1.append("		               (SELECT ROUTING_ID, ");
		var1.append("		                       CUSTOMER_ID, ");
		var1.append("		                       STATUS, ");
		var1.append("		                       WEEK1, WEEK2, WEEK3, WEEK4, ");
		var1.append("		                       MONDAY, ");
		var1.append("		                       TUESDAY, ");
		var1.append("		                       WEDNESDAY,  ");
		var1.append("		                       THURSDAY, ");
		var1.append("		                       FRIDAY, ");
		var1.append("		                       SATURDAY, ");
		var1.append("		                       SUNDAY ");
		var1.append("		                FROM   ROUTING_CUSTOMER ");
		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append(" ) RTC, ");
		var1.append("		               CUSTOMER CT ");
		var1.append("		        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("		               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("		               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("		               AND VP.STATUS = 1 ");
		var1.append("                      AND VP.SHOP_ID = ?");
		param.add(shopId);
		var1.append("		               AND CT.shop_id = ? ");
		param.add(shopId);
		var1.append("		               AND RT.STATUS = 1 ");
		var1.append("		               AND CT.STATUS = 1 ");
		var1.append("		               AND RTC.STATUS = 1 ");
		var1.append("		               ) TEMP ");
		var1.append("	     WHERE   TEMP.STAFF_ID = GS_TRAIN.STAFF_ID) GS_CUS_TRAIN ");
		var1.append("	           ON GS_CUS.CUSTOMER_ID = GS_CUS_TRAIN.CUSTOMER_ID ) GS_CUS_LIST LEFT JOIN (SELECT * ");
		var1.append("			                          FROM   ACTION_LOG ");
		var1.append("			                          WHERE  DATE(?) = DATE(ACTION_LOG.START_TIME) ");
		param.add(date_now);
		var1.append("                               AND STAFF_ID = ? ");
		param.add(staffId);
		var1.append("                               AND SHOP_ID = ? ");
		param.add(shopId);
		var1.append("										AND OBJECT_TYPE IN ( 0, 1 ))AL ");
		var1.append("			                      ON GS_CUS_LIST.CUSTOMER_ID = AL.CUSTOMER_ID ");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (dto == null) {
					dto = new CustomerListDTO();
				}
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						if (item.initDataFromSelectCustomer(c, lat, lng)) {
							dto.getCusList().add(item);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 * get Select Customer List TBHV
	 * TBHV
	 *
	 * @author: ToanTT
	 * @param dto
	 * @return: ContentValues
	 * @throws:
	 */
	public CustomerListDTO getSelectCustomerListTBHV(String shopId,
			String staffId, double lat, double lng) throws Exception {
		CustomerListDTO dto = null;
		Cursor c = null;
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);

		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopId);
		String shopStr = TextUtils.join(",", shopList);

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT * ");
		var1.append("	FROM (SELECT DISTINCT(CUSTOMER_ID), CUSTOMER_CODE, CUSTOMER_NAME, STREET, LAT, LNG ");
		var1.append("			FROM (SELECT    VP.STAFF_ID AS STAFF_ID, ");
		var1.append("                       CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("		               SUBSTR(CT.CUSTOMER_CODE, 1, 3)  AS CUSTOMER_CODE, ");
		var1.append("		               CT.CUSTOMER_NAME        AS CUSTOMER_NAME,  ");
		var1.append("		               CT.LAT                  AS LAT, ");
		var1.append("		               CT.LNG                  AS LNG, ");
		var1.append("		               ( CT.HOUSENUMBER ");
		var1.append("		                 || ' ' ");
		var1.append("		                 || CT.STREET )        AS STREET ");
		var1.append("	       FROM   VISIT_PLAN VP, ");
		var1.append("		               ROUTING RT, ");
		var1.append("		               (SELECT ROUTING_ID, ");
		var1.append("		                       CUSTOMER_ID, ");
		var1.append("		                       STATUS, ");
		var1.append("		                       WEEK1, WEEK2, WEEK3, WEEK4 ");
		var1.append("		                FROM   ROUTING_CUSTOMER" );

		var1.append("                WHERE (DATE(END_DATE) >= ? OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(Date(?)) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(start_date)) / 7  as integer) +");
		param.add(date_now);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', start_date) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', start_date)                          ");
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");

		var1.append("   ) RTC, ");
		var1.append("		               CUSTOMER CT ");
		var1.append("		        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("		               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("		               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		var1.append("		               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		var1.append("		               AND VP.STATUS = 1 ");
		var1.append("		               AND RT.STATUS = 1 ");
		var1.append("		               AND CT.STATUS = 1 ");
		var1.append("		               AND RTC.STATUS = 1 ");
		var1.append("		               ) TEMP, ");
		var1.append("	               (SELECT S.STAFF_ID ");
		var1.append("         			  FROM STAFF S, CHANNEL_TYPE CH, SHOP SH ");
		var1.append("        			 WHERE CH.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID ");
		var1.append("          			   AND CH.TYPE = 2 ");
		var1.append("                      AND CH.OBJECT_TYPE IN (1, 2) ");
		var1.append("                      AND S.STATUS = 1 ");
		var1.append("                      AND S.SHOP_ID = SH.SHOP_ID ");
		var1.append("                      AND SH.STATUS = 1 ");
		var1.append("                      AND S.SHOP_ID IN ( " + shopStr
				+ " )) STAFFLIST ");
		var1.append("	           WHERE TEMP.STAFF_ID = STAFFLIST.STAFF_ID ");
		var1.append("	           ORDER BY TEMP.CUSTOMER_ID ASC) ");

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (dto == null) {
					dto = new CustomerListDTO();
				}
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						if (item.initDataFromSelectCustomer(c, lat, lng)) {
							dto.getCusList().add(item);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 *
	 * lay danh sach tim kiem hinh anh
	 *
	 * @author: YenNTH
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ImageListDTO
	 * @throws:
	 */
	public ImageSearchViewDTO getImageSearchList(Bundle data) throws Exception {
		ImageSearchViewDTO dto = new ImageSearchViewDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		// String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String cusName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		String displayProgramId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String day = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String objectTypeImage = data
				.getString(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
		int numTop = data.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT C.short_code                                AS CUSTOMER_CODE, ");
		sql.append("                M.thumb_url                                 AS THUMB_URL, ");
		sql.append("                M.url                                       AS URL, ");
		sql.append("                S.staff_code                                AS STAFF_CODE, ");
		sql.append("                S.staff_name                                AS STAFF_NAME, ");
		sql.append("                M.lat                                       AS LAT_MEDIA, ");
		sql.append("                M.lng                                       AS LNG_MEDIA, ");
		sql.append("                M.create_user                               AS CREATE_USER, ");
		sql.append("                C.customer_id                               AS CUSTOMER_ID, ");
		sql.append("                C.customer_name                             AS CUSTOMER_NAME, ");
		sql.append("                C.street                                    AS STREET, ");
		sql.append("                C.lat                                       AS LAT, ");
		sql.append("                C.lng                                       AS LNG, ");
		sql.append("                C.housenumber                               AS HOUSENUMBER, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN RC.monday = 1 THEN 'T2' ");
		sql.append("                    ELSE '' ");
		sql.append("                  END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.tuesday = 1 THEN ',T3' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.wednesday = 1 THEN ',T4' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.thursday = 1 THEN ',T5' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.friday = 1 THEN ',T6' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.saturday = 1 THEN ',T7' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END ) ");
		sql.append("                || ( CASE ");
		sql.append("                       WHEN RC.sunday = 1 THEN ',CN' ");
		sql.append("                       ELSE '' ");
		sql.append("                     END )                                  AS VISIT_PLAN, ");
		sql.append("                C.customer_name ");
		sql.append("                || C.address                                AS ");
		sql.append("                CUSTOMER_NAME_ADDRESS_TEXT, ");
		sql.append("                S.staff_id                                  AS STAFF_ID, ");
		sql.append("                Strftime('%d/%m/%Y - %H:%M', M.create_date) AS CREATE_DATE ");
		sql.append("FROM   media_item m ");
		sql.append("       JOIN customer C ");
		sql.append("         ON C.customer_id = M.object_id ");
		sql.append("            AND C.status = 1 ");
		sql.append("       LEFT JOIN staff S ");
		sql.append("              ON S.staff_id = M.staff_id ");
		sql.append("                 AND S.status = 1 ");
		sql.append("                 AND M.staff_id IN (SELECT staff_id ");
		sql.append("                                    FROM   staff ");
		sql.append("                                           JOIN channel_type ");
		sql.append("                                             ON staff_type_id = channel_type_id ");
		sql.append("                                    WHERE  object_type IN ( 1, 2, 3 )) ");
		sql.append("       JOIN visit_plan vp ");
		sql.append("              ON m.staff_id = vp.staff_id ");
		sql.append("                 AND Date(vp.from_date) <= Date('now', 'localtime') ");
		sql.append("                 AND ( Date(vp.to_date) >= Date('now', 'localtime') ");
		sql.append("                        OR vp.to_date IS NULL ) ");
		sql.append("                 AND Vp.status = 1 ");
		sql.append("       			AND Vp.shop_id = ? ");
		param.add(shopId);
		sql.append("       LEFT JOIN routing R ");
		sql.append("              ON R.routing_id = vp.routing_id ");
		sql.append("       LEFT JOIN routing_customer RC ");
		sql.append("              ON RC.routing_id = R.routing_id and rc.customer_id = m.object_id");

		sql.append(" WHERE  1 = 1 ");
		sql.append("       AND object_type IN ( 0, 1, 2, 4 ) ");
		sql.append("       AND m.status = 1 ");
//		sql.append("       AND m.type = 1 ");
		sql.append("       AND Date(m.create_date) >= Date('now', 'localtime', 'start of month', ");
		sql.append("                                  '-2 month' ");
		sql.append("                                  ) ");
		sql.append("       AND m.media_type = 0 ");
		sql.append("                 AND Date(rc.start_date) <= Date('now', 'localtime') ");
		sql.append("                 AND ( Date(rc.end_date) >= Date('now', 'localtime') ");
		sql.append("                        OR rc.end_date IS NULL ) ");
		sql.append("                 AND RC.status = 1 ");
		sql.append("       AND M.shop_id = ? ");
		param.add(shopId);
//		if (!StringUtil.isNullOrEmpty(cusCode)) {
//			cusCode = StringUtil.getEngStringFromUnicodeString(cusCode);
//			cusCode = StringUtil.escapeSqlString(cusCode);
//			cusCode = DatabaseUtils.sqlEscapeString("%" + cusCode + "%");
//			sql
//					.append("	and upper(C.short_code) like upper(");
//			sql.append(cusCode);
//			sql.append(") escape '^' ");
//		}
		if (!StringUtil.isNullOrEmpty(cusName)) {
			cusName = StringUtil.getEngStringFromUnicodeString(cusName);
			cusName = StringUtil.escapeSqlString(cusName);
			cusName = DatabaseUtils.sqlEscapeString("%" + cusName + "%");
			sql.append("	and (upper(C.SHORT_CODE) like upper(");
			sql.append(cusName);
			sql.append(") escape '^' ");
			sql.append("	or upper(C.NAME_TEXT) like upper(");
			sql.append(cusName);
			sql.append(") escape '^' ");
			sql.append("	or upper(C.CUSTOMER_NAME) like upper(");
			sql.append(cusName);
			sql.append(") escape '^' ");
			sql.append("	or upper(C.ADDRESS) like upper(");
			sql.append(cusName);
			sql.append(") escape '^' ");
			sql.append(" ) ");
		}

		if (!StringUtil.isNullOrEmpty(day)) {
			sql.append("	and upper(VISIT_PLAN) like upper(?) ");
			param.add("%" + day + "%");
		}
		if (!StringUtil.isNullOrEmpty(objectTypeImage)) {
			sql.append(" AND m.object_type = ? ");
			param.add(objectTypeImage);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sql.append(" and dayInOrder(M.create_date) >= dayInOrder(?) ");
			param.add(fromDate);
		}

		if (!StringUtil.isNullOrEmpty(toDate)) {
			sql.append(" and dayInOrder(M.create_date) <= dayInOrder(?) ");
			param.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
			sql.append(" and M.object_type = 4 ");// Hinh anh cua 1 CTTB

			sql.append("	and M.display_program_id = ? ");
			param.add(displayProgramId);
		}

		sql.append(" order by DATETIME(M.CREATE_DATE) DESC ");
		sql.append(" limit ? offset ?");
		param.add("" + numTop);
		int offset = page * numTop;
		param.add(String.valueOf(offset));
		try {
			c = rawQueries(sql.toString(), param);

			if (c != null) {
				ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						dto.listItem.add(item);
						listPhoto.add(item.mediaItem);
					} while (c.moveToNext());
				}
				dto.albumInfo.setListPhoto(listPhoto);
			}
		}finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 * Lay ds hinh anh tim kiem cua TBHV
	 *
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ImageSearchViewDTO
	 * @throws:
	 */
	public ImageSearchViewDTO getImageSearchListTBHV(Bundle data)
			throws Exception {
		ImageSearchViewDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String cusCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String cusName = data.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		String displayProgramId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String day = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String objectTypeImage = data
				.getString(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
		int numTop = data.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		String staffIdPara = data
				.getString(IntentConstants.INTENT_STAFF_ID_PARA);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer getCusList = new StringBuffer();
		getCusList.append("SELECT DISTINCT CUSTOMER_CODE, ");
		getCusList.append("                THUMB_URL, ");
		getCusList.append("                URL, ");
		getCusList.append("                STAFF_CODE, ");
		getCusList.append("                STAFF_NAME, ");
		getCusList.append("                LAT_MEDIA, ");
		getCusList.append("                LNG_MEDIA, ");
		getCusList.append("                CREATE_USER, ");
		getCusList.append("                CUSTOMER_ID, ");
		getCusList.append("                CUSTOMER_NAME, ");
		getCusList.append("                STREET, ");
		getCusList.append("                LAT, ");
		getCusList.append("                LNG, ");
		getCusList.append("                HOUSENUMBER, ");
		getCusList.append("                CUSTOMER_NAME_ADDRESS_TEXT, ");
		getCusList.append("                STAFF_ID, ");
		getCusList.append("                CREATE_DATE  FROM (");
		getCusList
				.append("SELECT DISTINCT Substr(C.customer_code, 1, 3)               AS CUSTOMER_CODE, ");
		getCusList
				.append("                M.thumb_url                                 AS THUMB_URL, ");
		getCusList
				.append("                M.url                                       AS URL, ");
		getCusList
				.append("                S.staff_code                                AS STAFF_CODE, ");
		getCusList
				.append("                S.staff_name                                AS STAFF_NAME, ");
		getCusList
				.append("                M.lat                                       AS LAT_MEDIA, ");
		getCusList
				.append("                M.lng                                       AS LNG_MEDIA, ");
		getCusList
				.append("                M.create_user                               AS CREATE_USER, ");
		getCusList
				.append("                C.customer_id                               AS CUSTOMER_ID, ");
		getCusList
				.append("                C.customer_name                             AS CUSTOMER_NAME, ");
		getCusList
				.append("                C.street                                    AS STREET, ");
		getCusList
				.append("                C.lat                                       AS LAT, ");
		getCusList
				.append("                C.lng                                       AS LNG, ");
		getCusList
				.append("                C.housenumber                               AS HOUSENUMBER, ");
		getCusList.append("                ( CASE ");
		getCusList.append("                    WHEN RC.monday = 1 THEN 'T2' ");
		getCusList.append("                    ELSE '' ");
		getCusList.append("                  END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.tuesday = 1 THEN ',T3' ");
		getCusList.append("                       ELSE '' ");
		getCusList.append("                     END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.wednesday = 1 THEN ',T4' ");
		getCusList.append("                       ELSE '' ");
		getCusList.append("                     END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.thursday = 1 THEN ',T5' ");
		getCusList.append("                       ELSE '' ");
		getCusList.append("                     END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.friday = 1 THEN ',T6' ");
		getCusList.append("                       ELSE '' ");
		getCusList.append("                     END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.saturday = 1 THEN ',T7' ");
		getCusList.append("                       ELSE '' ");
		getCusList.append("                     END ) ");
		getCusList.append("                || ( CASE ");
		getCusList
				.append("                       WHEN RC.sunday = 1 THEN ',CN' ");
		getCusList.append("                       ELSE '' ");
		getCusList
				.append("                     END )                                  AS VISIT_PLAN, ");
		getCusList.append("                C.customer_name ");
		getCusList
				.append("                || C.address                                AS ");
		getCusList.append("                CUSTOMER_NAME_ADDRESS_TEXT, ");
		getCusList
				.append("                S.staff_id                                  AS STAFF_ID, ");
		getCusList
				.append("                Strftime('%d/%m/%Y - %H:%M', M.create_date) AS CREATE_DATE ");
		getCusList.append("FROM   media_item m ");
		getCusList.append("       JOIN customer C ");
		getCusList.append("         ON C.customer_id = M.object_id ");
		getCusList.append("            AND C.status = 1 ");
		getCusList.append("       JOIN staff S ");
		getCusList.append("              ON S.staff_id = M.staff_id ");
		getCusList.append("                 AND S.status = 1 ");
		getCusList
				.append("                 AND M.staff_id IN (SELECT staff_id ");
		getCusList.append("                                    FROM   staff ");
		getCusList
				.append("                                           JOIN channel_type ");
		getCusList
				.append("                                             ON staff_type_id = ");
		getCusList
				.append("                                                channel_type.channel_type_id ");
		getCusList
				.append("                                    WHERE  object_type IN ( 1, 2, 3, 5, 7 )) ");
		getCusList.append("       LEFT JOIN visit_plan vp ");
		getCusList.append("              ON m.staff_id = vp.staff_id ");
		getCusList
				.append("                 AND Date(vp.from_date) <= Date('now', 'localtime') ");
		getCusList
				.append("                 AND ( Date(vp.to_date) >= Date('now', 'localtime') ");
		getCusList.append("                        OR vp.to_date IS NULL ) ");
		getCusList.append("                 AND Vp.status = 1 ");
		getCusList.append("       LEFT JOIN routing_customer RC ");
		getCusList.append("         ON ( RC.routing_id = R.routing_id ");
		getCusList.append("              AND RC.customer_id = m.object_id ) ");
		getCusList
				.append("                 AND Date(rc.start_date) <= Date('now', 'localtime') ");
		getCusList
				.append("                 AND ( Date(rc.end_date) >= Date('now', 'localtime') ");
		getCusList.append("                        OR rc.end_date IS NULL ) ");
		getCusList.append("               AND rc.status = 1  ");

		getCusList.append("       LEFT JOIN routing R ");
		getCusList.append("         ON R.routing_id = vp.routing_id ");
		getCusList.append("         AND R.status = 1 ");
		getCusList.append("WHERE  1 = 1 ");
		getCusList.append("       AND object_type IN ( 0, 1, 2, 4 ) ");
		getCusList.append("       AND m.status = 1 ");
//		getCusList.append("       AND m.type = 1 ");
		getCusList
				.append("       AND Date(m.create_date) >= Date('now', 'localtime', 'start of month', ");
		getCusList.append("                                  '-2 month' ");
		getCusList.append("                                  ) ");
		getCusList.append("       AND m.media_type = 0 ");
		getCusList.append("       AND M.shop_id IN (SELECT shop_id ");
		getCusList.append("                         FROM   shop ");
		getCusList
				.append("                         WHERE  parent_shop_id = ?) ");
		param.add(shopId);

		if (!StringUtil.isNullOrEmpty(cusCode)) {
			cusCode = StringUtil.getEngStringFromUnicodeString(cusCode);
			cusCode = StringUtil.escapeSqlString(cusCode);
			cusCode = DatabaseUtils.sqlEscapeString("%" + cusCode + "%");
			getCusList
					.append("	and upper(substr(C.CUSTOMER_CODE,1,3)) like upper(");
			getCusList.append(cusCode);
			getCusList.append(") escape '^' ");
		}
		if (!StringUtil.isNullOrEmpty(cusName)) {
			cusName = StringUtil.getEngStringFromUnicodeString(cusName);
			cusName = StringUtil.escapeSqlString(cusName);
			cusName = DatabaseUtils.sqlEscapeString("%" + cusName + "%");
			getCusList
					.append("	and upper(C.NAME_TEXT || C.ADDRESS) like upper(");
			getCusList.append(cusName);
			getCusList.append(") escape '^' ");
		}

		if (!StringUtil.isNullOrEmpty(day)) {
			getCusList.append("	and upper(VISIT_PLAN) like upper(?) ");
			param.add("%" + day + "%");
		}
		if (!StringUtil.isNullOrEmpty(objectTypeImage)) {
			getCusList.append(" AND m.object_type = ? ");
			param.add(objectTypeImage);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			getCusList.append(" and dayInOrder(M.create_date) >= dayInOrder(?) ");
			param.add(fromDate);
		}

		if (!StringUtil.isNullOrEmpty(toDate)) {
			getCusList.append(" and dayInOrder(M.create_date) <= dayInOrder(?) ");
			param.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(displayProgramId)) {
			getCusList.append(" and M.object_type = 4 ");// Hinh anh cua 1 CTTB

			getCusList.append("	and M.display_program_id = ? ");
			param.add(displayProgramId);
		}

		if (!StringUtil.isNullOrEmpty(staffIdPara)) {
			getCusList.append(" and M.staff_id = ? ");// nguoi tao

			param.add(staffIdPara);
		}

		getCusList.append(" order by DATETIME(M.CREATE_DATE) DESC) ");
		getCusList.append(" limit ? offset ?");
		param.add("" + numTop);
		int offset = page * numTop;
		param.add(String.valueOf(offset));
		try {
			c = rawQueries(getCusList.toString(), param);

			if (c != null) {
				dto = new ImageSearchViewDTO();
				ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						dto.listItem.add(item);
						listPhoto.add(item.mediaItem);
					} while (c.moveToNext());
				}
				dto.albumInfo.setListPhoto(listPhoto);
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 * Lay thong tin doanh so SKU cua khach hang mua trong thang
	 *
	 * @author: BANGHN
	 * @param customerId
	 *            : id khach hang
	 * @param inheritShopId
	 *            : ma npp
	 * @return
	 * @throws Exception
	 */
	public ArrayList<CustomerSaleSKUDTO> getCustomerSaleSKUInMonth(Bundle data) throws Exception {
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		int sysCurrencyDivide = data.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
		ArrayList<CustomerSaleSKUDTO> listSaleSKU = new ArrayList<CustomerSaleSKUDTO>();
		ArrayList<String> params = new ArrayList<String>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		String endOfMonth = new EXCEPTION_DAY_TABLE(mDB).getEndDayOfOffsetCycle(new Date(), 0);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT sim.product_id   AS PRODUCT_ID, SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.amount ");
		}else {
			sql.append(" 	 sim.amount ");
		}
		sql.append(") 	AS AMOUNT, SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.quantity ");
		}else {
			sql.append(" 	 sim.quantity ");
		}
		sql.append(") 	AS QUANTITY, ");
		sql.append("       p.[product_code] AS PRODUCT_CODE, ");
		sql.append("       p.[product_name] AS PRODUCT_NAME ");
		sql.append("FROM   rpt_sale_primary_month sim, cycle cy ");
		sql.append("       LEFT JOIN [product] P ");
		sql.append("              ON P.product_id = sim.product_id ");
		sql.append("WHERE  sim.shop_id = ? ");
		params.add(shopId);
		sql.append("       AND sim.customer_id = ? ");
		params.add(customerId);
		sql.append(" 	   AND AMOUNT > 0 ");
//		sql.append("       AND substr(sim.affect_date,1,7) = substr( ?, 1,7) ");
//		params.add(dateNow);
		sql.append("       AND sim.CYCLE_ID = cy.CYCLE_ID ");
		sql.append("       AND substr(cy.BEGIN_DATE,1,10) >= substr( ?, 1,10) ");
		params.add(startOfMonth);
		sql.append("       AND substr(cy.END_DATE,1,10) <= substr( ?, 1,10) ");
		params.add(endOfMonth);
		sql.append("       AND substr(sim.affect_date,1,10) >= substr( cy.BEGIN_DATE, 1,10) ");
		sql.append("       AND substr(sim.affect_date,1,10) <= substr( cy.END_DATE, 1,10)   ");
		sql.append("       group by sim.product_id ");
		sql.append("ORDER  BY product_code, ");
		sql.append("          product_name ");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				CustomerSaleSKUDTO sku;
				if (c.moveToFirst()) {
					do {
						sku = new CustomerSaleSKUDTO();
						sku.parseFromCursor(c, sysCurrencyDivide);
						listSaleSKU.add(sku);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("", ex.toString());
			}

		}
		return listSaleSKU;
	}

	/**
	 * Lay thong tin doanh so theo nganh hang cua KH trong 3 thang gan nhat
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public CustomerCatAmountPopupViewDTO getCustomerCatAmountInNearly3Month(
			Bundle data) throws Exception {
		CustomerCatAmountPopupViewDTO result = new CustomerCatAmountPopupViewDTO();
		ArrayList<CustomerCatAmountPopupDTO> listSaleSKU = new ArrayList<CustomerCatAmountPopupDTO>();
		int sysCurrencyDivide = data.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		String startOfOnePreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -1);
		String startOfTwoPreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -2);
		String startOfThreePreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -3);

		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		ArrayList<String> params = new ArrayList<String>();

		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
//		boolean isStaffSale = (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_STAFF ||
//									GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES ) ;
		boolean isStaffSale = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF ;
		// StringBuffer sql = new StringBuffer();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT PI.product_info_code AS CATEGORY_CODE, PI.product_info_name AS CATEGORY_NAME, ");
		var1.append("               Sum (month_0)        MONTH_0, ");
		var1.append("               Sum (month_1)        MONTH_1, ");
		var1.append("               Sum (month_2)        MONTH_2, ");
		var1.append("               Sum (month_3)        MONTH_3, ");
		var1.append("               Sum (QUANTITY_MONTH_0)        QUANTITY_MONTH_0, ");
		var1.append("               Sum (QUANTITY_MONTH_1)        QUANTITY_MONTH_1, ");
		var1.append("               Sum (QUANTITY_MONTH_2)        QUANTITY_MONTH_2, ");
		var1.append("               Sum (QUANTITY_MONTH_3)        QUANTITY_MONTH_3 ");
		var1.append("        FROM   product_info PI ");
		var1.append("               LEFT JOIN (SELECT sim.*, ");
		var1.append("                                 sim.cat_id  CATEGORY_CODE, ");
		var1.append("                                 SUM( CASE  substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.amount ");
		}else {
			var1.append(" sim.amount  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS MONTH_0, ");
		var1.append("                                 SUM( CASE  substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.quantity ");
		}else {
			var1.append(" sim.quantity  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS QUANTITY_MONTH_0, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfOnePreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.amount ");
		}else {
			var1.append(" sim.amount  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS MONTH_1, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfOnePreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.quantity ");
		}else {
			var1.append(" sim.quantity  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS QUANTITY_MONTH_1, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfTwoPreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.amount ");
		}else {
			var1.append(" sim.amount  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS MONTH_2, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfTwoPreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.quantity ");
		}else {
			var1.append(" sim.quantity  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS QUANTITY_MONTH_2, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfThreePreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.amount ");
		}else {
			var1.append(" sim.amount  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS MONTH_3, ");
		var1.append("                                 SUM( CASE substr(sim.affect_date,1,10) ");
		var1.append("                                     WHEN substr( ?, 1,10) ");
		params.add(startOfThreePreviousMonth);
		var1.append("                                           THEN ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append(" sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" sim.quantity ");
		}else {
			var1.append(" sim.quantity  ");
		}
		var1.append("                                     ELSE 0 ");
		var1.append("                                   END ) AS QUANTITY_MONTH_3 ");
		var1.append("                          FROM   rpt_sale_primary_month sim ");
		var1.append("                                 LEFT JOIN product P ");
		var1.append("                                        ON P.product_id = sim.product_id ");
		var1.append("                          WHERE  substr(sim.affect_date,1,10) >= substr(?,1,10)  ");
		params.add(startOfThreePreviousMonth);
		var1.append("                                 AND sim.shop_id = ? ");
		params.add(shopId);
		var1.append("                                 AND sim.customer_id = ? ");
		params.add(customerId);
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			var1.append("  and sim.amount_approved > 0 ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			var1.append(" and sim.amount_pending > 0 ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			var1.append(" and sim.amount > 0  ");
		}else {
			var1.append(" and sim.amount > 0  ");
		}
		if (isStaffSale) {
			var1.append("                                 AND sim.staff_id  = ? ");
			params.add(staffId);
		}
		var1.append("                          group by sim.product_id,sim.affect_date ) SIM1 ");
		var1.append("                      ON PI.product_info_id = SIM1.category_code where pi.type = 1 and pi.status=1");
		var1.append("        GROUP  BY PI.product_info_code) ");
		// var1.append("ORDER  BY category_code ");

		Cursor c = null;
		CustomerCatAmountPopupDTO catSum = new CustomerCatAmountPopupDTO();
		catSum.categoryCode = StringUtil.getString(R.string.TEXT_TOTAL);
		try {
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				CustomerCatAmountPopupDTO sku;
				if (c.moveToFirst()) {
					do {
						sku = new CustomerCatAmountPopupDTO();
						sku.categoryCode = CursorUtil.getString(c, PRODUCT_CATEGORY_TABLE.CATEGORY_CODE);
						sku.categoryName = CursorUtil.getString(c, PRODUCT_CATEGORY_TABLE.CATEGORY_NAME);
						listSaleSKU.add(sku);

						for (int i = 0; i < 4; i++) {
							double amount = CursorUtil.getDouble(c, "MONTH_"+ i);
							sku.amountArray[i] += amount;
							catSum.amountArray[i] += amount;
							long quantity = c.getLong(c.getColumnIndex("QUANTITY_MONTH_" + i));
							sku.quantitytArray[i] += quantity;
							catSum.quantitytArray[i] += quantity;
						}

					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("", ex.toString());
			}
		}
		result.listCatAmount = listSaleSKU;
		result.catSum = catSum;
		return result;
	}

	/**
	 * Lay danh sach khach hang da ghe tham
	 *
	 * @author: BANGHN
	 * @param staffId
	 * @return
	 */
	public CustomerVisitedViewDTO getCustomerVisited(int staffId, String shopId) throws Exception {
		CustomerVisitedViewDTO dto = new CustomerVisitedViewDTO();
		Cursor cursor = null;
		// String today = DateUtils.getToday();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> param = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT customer_id                   AS CUSTOMER_ID1, ");
		var1.append("               staff_id                      AS STAFF_ID_AL, ");
		var1.append("               is_or, ");
		var1.append("               Strftime('%H:%M', start_time) AS START_TIME_SHORT, ");
		var1.append("               Strftime('%H:%M', end_time)   AS END_TIME_SHORT, ");
		var1.append("               start_time                    AS START_TIME_FULL, ");
		var1.append("               end_time                      AS END_TIME_FULL ");
		var1.append("        FROM   action_log ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND staff_id = ? ");
		param.add(staffId + "");
		var1.append("               AND shop_id = ? ");
		param.add(shopId);
		var1.append("               AND ( ( object_type = 0 ");
		var1.append("                       AND end_time IS NOT NULL ) ");
		var1.append("                      OR object_type = 1 ) ");
		var1.append("               AND Date(start_time) = Date(?) ");
		param.add(date_now);
		var1.append("               AND Ifnull (Date(end_time) >= Date(?), 1)) ");
		param.add(date_now);
		var1.append("       ACTIONLOG ");
		var1.append("       JOIN (SELECT CT.customer_id          AS CUSTOMER_ID, ");
		var1.append("                    CT.customer_code        AS CUSTOMER_CODE, ");
		var1.append("                    VP.staff_id             AS STAFF_ID_VP, ");
		var1.append("                    (CT.short_code ");
		var1.append("                      || ' - ' ");
		var1.append("                      || CT.customer_name ) AS CUS_CODE_NAME, ");
		var1.append("                    CT.customer_name        AS CUSTOMER_NAME, ");
		var1.append("                    ( Ifnull(CT.housenumber,'') ");
		var1.append("                      || ' ' ");
		var1.append("                      || Ifnull(CT.street,'') )        AS STREET, ");
		var1.append("                    ( CASE ");
		var1.append("                        WHEN RC.monday = 1 THEN 'T2' ");
		var1.append("                        ELSE '' ");
		var1.append("                      END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.tuesday = 1 THEN ',T3' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.wednesday = 1 THEN ',T4' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.thursday = 1 THEN ',T5' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.friday = 1 THEN ',T6' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.saturday = 1 THEN ',T7' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.sunday = 1 THEN ',CN' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END )              AS TUYEN ");
		var1.append("             FROM   visit_plan VP, ");
		var1.append("                    customer CT, ");
		var1.append("                    routing R, ");
		var1.append("                    routing_customer RC ");
		var1.append("             WHERE  1 = 1 ");
		var1.append("                    AND VP.status = 1 ");
		var1.append("                    AND CT.status = 1 ");
		var1.append("                    AND R.status = 1 ");
		var1.append("                    AND RC.status = 1 ");
		var1.append("                    AND VP.staff_id = ? ");
		param.add(staffId + "");
		var1.append("                    AND R.routing_id = RC.routing_id ");
		var1.append("                    AND R.routing_id = VP.routing_id ");
		var1.append("                    AND Ifnull(Date(VP.from_date) <= Date(?) , 0) ");
		param.add(date_now);
		var1.append("                    AND Ifnull (Date(VP.to_date) >= Date(?), 1) ");
		param.add(date_now);
		var1.append("                    and    vp.shop_id = ? ");
		param.add(shopId);
		var1.append("                    and    r.shop_id = ? ");
		param.add(shopId);
		var1.append("                    and    ct.shop_id = ? ");
		param.add(shopId);
		var1.append("                    AND Ifnull(Date(RC.start_date) <= Date(?) , 0) ");
		param.add(date_now);
		var1.append("                    AND Ifnull (Date(RC.end_date) >= Date(?), 1) ");
		param.add(date_now);
		var1.append("                    AND RC.customer_id = CT.customer_id) CUS ");
		var1.append("         ON CUS.customer_id = ACTIONLOG.customer_id1 ");
		var1.append("            AND CUS.staff_id_vp = ACTIONLOG.staff_id_al ");
		// var1.append("       LEFT JOIN (SELECT customer_id AS CUSTOMER_ID2, ");
		// var1.append("                         cast(Sum(total)/1000 as int)  AS REVENUE ");
		// var1.append("                  FROM   sale_order ");
		// var1.append("                  WHERE  staff_id = ? ");
		// param.add(staffId + "");
		// var1.append("                         AND Date(order_date) = Date(?) ");
		// // them dieu kien cho don hang
		// var1.append("                         AND TYPE = 1 ");
		// param.add(date_now);
		// var1.append("                  GROUP  BY customer_id) RE ");
		var1.append("       LEFT JOIN (select customer_id customer_id2,  ");
		var1.append("                         day_amount as AMOUNT, ");
		var1.append("                         day_amount_approved as AMOUNT_APPROVED, ");
		var1.append("                         day_amount_pending AMOUNT_PENDING, ");
		var1.append("                         day_quantity as QUANTITY, ");
		var1.append("                         day_quantity_approved as QUANTITY_APPROVED, ");
		var1.append("                         day_quantity_pending as QUANTITY_PENDING ");
		var1.append("                  FROM   RPT_STAFF_SALE_DETAIL ");
		var1.append("                  WHERE  1 = 1 AND object_id = ? ");
		param.add(staffId + "");
		var1.append("                         AND object_type = ? ");
		param.add(CustomerVisitedViewDTO.TYPE_STAFF + "");
		var1.append("     				      AND substr(sale_date, 1, 10) = ? ");
		param.add(date_now);
		var1.append("                         AND shop_id = ? ");
		param.add(shopId);
		var1.append("                  GROUP  BY customer_id) RE ");
		var1.append("              ON CUS.customer_id = RE.customer_id2 ");
		var1.append("ORDER  BY is_or, ");
		var1.append("          start_time_full, ");
		var1.append("          cus_code_name ");

		try {
			cursor = rawQueries(var1.toString(), param);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						CustomerVisitedViewDTO.CustomerVisitedDTO item = dto
								.newWrongPlanCustomerItem();
						item.initCusItemVisited(cursor);
						dto.arrWrong.add(item);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return dto;
	}

	/**
	 * Lay danh sach khach hang da ghe tham
	 *
	 * @author: DungNX
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	public CustomerVisitedViewDTO getCustomerVisitedPaging(String shopId, int staffId,
			int page, int isGetTotalPage) throws Exception {
		CustomerVisitedViewDTO dto = null;
		Cursor cursor = null;
		Cursor cTotalRow = null;

		// String today = DateUtils.getToday();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ArrayList<String> param = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT customer_id                   AS CUSTOMER_ID1, ");
		var1.append("               staff_id                      AS STAFF_ID_AL, ");
		var1.append("               is_or, ");
		var1.append("               Strftime('%H:%M', start_time) AS START_TIME_SHORT, ");
		var1.append("               Strftime('%H:%M', end_time)   AS END_TIME_SHORT, ");
		var1.append("               start_time                    AS START_TIME_FULL, ");
		var1.append("               end_time                      AS END_TIME_FULL ");
		var1.append("        FROM   action_log ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND staff_id = ? ");
		param.add(staffId + "");
		var1.append("               AND shop_id = ? ");
		param.add(shopId);
		var1.append("               AND ( ( object_type = 0 ");
		var1.append("                       AND end_time IS NOT NULL ) ");
		var1.append("                      OR object_type = 1 ) ");
		var1.append("               AND Date(start_time) = Date(?) ");
		param.add(date_now);
		var1.append("               AND Ifnull (Date(end_time) >= Date(?), 1)  ");
		param.add(date_now);
		var1.append("               group by customer_id ) ");
		var1.append("       ACTIONLOG ");
		var1.append("       JOIN (SELECT CT.customer_id          AS CUSTOMER_ID, ");
		var1.append("                    CT.SHORT_CODE        AS CUSTOMER_CODE, ");
		var1.append("                    VP.staff_id             AS STAFF_ID_VP, ");
		var1.append("                    ( CT.short_code ");
		var1.append("                      || ' - ' ");
		var1.append("                      || CT.customer_name ) AS CUS_CODE_NAME, ");
		var1.append("                    CT.customer_name        AS CUSTOMER_NAME, ");
		var1.append("                    ( ifnull(CT.housenumber,'') ");
		var1.append("                      || ' ' ");
		var1.append("                      || ifnull(CT.street,'') )        AS STREET, ");
		var1.append("                    ( CASE ");
		var1.append("                        WHEN RC.monday = 1 THEN 'T2' ");
		var1.append("                        ELSE '' ");
		var1.append("                      END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.tuesday = 1 THEN ',T3' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.wednesday = 1 THEN ',T4' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.thursday = 1 THEN ',T5' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.friday = 1 THEN ',T6' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.saturday = 1 THEN ',T7' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END ) ");
		var1.append("                    || ( CASE ");
		var1.append("                           WHEN RC.sunday = 1 THEN ',CN' ");
		var1.append("                           ELSE '' ");
		var1.append("                         END )              AS TUYEN ");
		var1.append("             FROM   visit_plan VP, ");
		var1.append("                    customer CT, ");
		var1.append("                    routing R, ");
		var1.append("                    routing_customer RC ");
		var1.append("             WHERE  1 = 1 ");
		var1.append("                    AND VP.status = 1 ");
		var1.append("                    AND CT.status = 1 ");
		var1.append("                    AND R.status = 1 ");
		var1.append("                    AND RC.status = 1 ");
		var1.append("                    AND VP.staff_id = ? ");
		param.add(staffId + "");
		var1.append("                    AND VP.shop_id = ? ");
		param.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		var1.append("                    AND R.routing_id = RC.routing_id ");
		var1.append("                    AND R.routing_id = VP.routing_id ");
		var1.append("                    AND Ifnull(Date(VP.from_date) <= Date(?) ");
		var1.append("                        , 0) ");
		param.add(date_now);
		var1.append("                    AND Ifnull (Date(VP.to_date) >= Date(?), ");
		var1.append("                        1) ");
		param.add(date_now);
		var1.append("                    AND Ifnull(Date(RC.start_date) <= Date(?) ");
		var1.append("                        , 0) ");
		param.add(date_now);
		var1.append("                    AND Ifnull (Date(RC.end_date) >= Date(?), ");
		var1.append("                        1) ");
		param.add(date_now);
		var1.append("                    AND RC.customer_id = CT.customer_id ");
		var1.append("               group by CT.customer_id ) CUS ");
		var1.append("         ON CUS.customer_id = ACTIONLOG.customer_id1 ");
		var1.append("            AND CUS.staff_id_vp = ACTIONLOG.staff_id_al ");
		var1.append("       LEFT JOIN (SELECT customer_id AS CUSTOMER_ID2, ");
		if(GlobalInfo.getInstance().getSysCalUnapproved() == 2) {
			var1.append("                         day_amount as AMOUNT ");
			var1.append("                         , day_quantity as QUANTITY ");
		} else if(GlobalInfo.getInstance().getSysCalUnapproved() == 1) {
			var1.append("                         day_amount_approved as AMOUNT ");
			var1.append("                         , day_quantity_approved as QUANTITY ");
		}
		var1.append("                  FROM   RPT_STAFF_SALE_DETAIL ");
		var1.append("                  WHERE  1 = 1 AND object_id = ? ");
		param.add(staffId + "");
		var1.append("                         AND substr(sale_date, 1, 10) = substr(?, 1, 10) ");
		param.add(date_now);
		var1.append("                         AND object_type = ? ");
		param.add(String.valueOf(RptStaffSaleDetailDTO.STAFF));
		var1.append("                  GROUP  BY customer_id) RE ");
		var1.append("              ON CUS.customer_id = RE.customer_id2 ");

		StringBuffer totalPageSql = new StringBuffer();
		if (page > 0) {
			// get count
			if (isGetTotalPage == 1) {
				totalPageSql.append("select count(*) as TOTAL_ROW from (");
				totalPageSql.append(var1);
				totalPageSql.append(") ");
			}
			var1.append("   order by IS_OR, START_TIME_FULL, CUS_CODE_NAME ");
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		} else {
			var1.append("   order by IS_OR, START_TIME_FULL, CUS_CODE_NAME ");
		}
		try {
			cursor = rawQueries(var1.toString(), param);
			if (cursor != null) {
				dto = new CustomerVisitedViewDTO();
				if (cursor.moveToFirst()) {
					do {
						CustomerVisitedViewDTO.CustomerVisitedDTO item = dto
								.newWrongPlanCustomerItem();
						item.initCusItemVisited(cursor);
						dto.arrWrong.add(item);
					} while (cursor.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), param);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
			}
		}
		return dto;
	}

	/**
	 * Lay thong tin KH da ghe tham
	 *
	 * @author: dungnt19
	 * @since: 15:31:06 21-02-2014
	 * @return: CustomerListItem
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public CustomerListItem getCustomerInfoVisit(String staffId, String shopId,
			String customerId) throws Exception {
//		String startOfMonth = DateUtils
//				.getFirstDateOfNumberPreviousMonthWithFormat(
//						DateUtils.DATE_FORMAT_DATE, 0);
		String startOfTwoPreviousMonth = DateUtils
				.getFirstDateOfNumberPreviousMonthWithFormat(
						DateUtils.DATE_FORMAT_DATE, -2);
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		CustomerListItem item = null;
		Cursor c = null;
		Cursor cDistance = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT TEMP.* ");
		var1.append("FROM   (SELECT CT.customer_id                   AS CUSTOMER_ID, ");
		var1.append("               Substr(CT.customer_code, 1, 3)   AS CUSTOMER_CODE, ");
		var1.append("               CT.customer_name                 AS CUSTOMER_NAME, ");
		var1.append("               CT.name_text                     AS NAME_TEXT, ");
		var1.append("               ( CT.housenumber ");
		var1.append("                 || ' ' ");
		var1.append("                 || CT.street )                 AS STREET, ");
		var1.append("               CT.mobiphone                     AS MOBILEPHONE, ");
		var1.append("               CT.phone                         AS PHONE, ");
		var1.append("               AL.CUSTOMER_LAT                           AS LAT, ");
		var1.append("               AL.CUSTOMER_LNG                           AS LNG, ");
		var1.append("               CT.channel_type_id               AS CHANNEL_TYPE_ID, ");
		var1.append("               AL.action_log_id                 AS VISIT_ACT_LOG_ID, ");
		var1.append("               AL.object_type                   AS IS_VISIT, ");
		var1.append("               AL.is_or                         AS IS_OR, ");
		var1.append("               LATLNG.lat                       AS VISITED_LAT, ");
		var1.append("               LATLNG.lng                       AS VISITED_LNG, ");
		var1.append("               Strftime('%H:%M', AL.start_time) AS VISIT_START_TIME_SHORT, ");
		var1.append("               AL.start_time                    AS VISIT_START_TIME, ");
		var1.append("               AL.end_time                      AS VISIT_END_TIME, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN ( AL.object_type = 0 ");
		var1.append("                          AND AL.end_time IS NULL ) THEN NULL ");
		var1.append("                   WHEN ( AL.object_type = 0 ");
		var1.append("                          AND AL.end_time IS NOT NULL ) THEN '0' ");
		var1.append("                   WHEN AL.object_type = 1 THEN '1' ");
		var1.append("                 END )                          AS VISITED, ");
		var1.append("               AL2.object_type                  AS IS_VOTE, ");
		var1.append("               AL3.object_type                  AS IS_CHECK, ");
		var1.append("               AL4.object_type                  AS IS_ORDER, ");
//		var1.append("               CTTB.display_program_code        AS DISPLAY_PROGRAM_CODE, ");
//		var1.append("               ''        AS DISPLAY_PROGRAM_CODE, ");
		var1.append("               KHT.sale_order_id                AS SALE_ORDER_ID ");
		var1.append("        FROM   customer CT ");
		var1.append("               LEFT JOIN (SELECT * ");
		var1.append("                          FROM   action_log ");
		var1.append("                          WHERE  dayInOrder(?) = Date(action_log.start_time) ");
		param.add(date_now);
		var1.append("                                 AND action_log.staff_id = ? ");
		param.add(String.valueOf(staffId));
		var1.append("                                 AND action_log.shop_id = ? ");
		param.add(shopId);
		var1.append("                                 AND action_log.object_type IN ( 0, 1 )) AL ");
		var1.append("                      ON CT.customer_id = AL.customer_id ");
		var1.append("               LEFT JOIN (SELECT * ");
		var1.append("                          FROM   action_log ");
		var1.append("                          WHERE  dayInOrder(?) = Date(start_time) ");
		param.add(date_now);
		var1.append("                                 AND action_log.staff_id = ? ");
		param.add(String.valueOf(staffId));
		var1.append("                                 AND action_log.shop_id = ? ");
		param.add(shopId);
		var1.append("                                 AND object_type = 2) AL2 ");
		var1.append("                      ON CT.customer_id = AL2.customer_id ");
		var1.append("               LEFT JOIN (SELECT * ");
		var1.append("                          FROM   action_log ");
		var1.append("                          WHERE  dayInOrder(?) = Date(start_time) ");
		param.add(date_now);
		var1.append("                                 AND action_log.staff_id = ? ");
		param.add(String.valueOf(staffId));
		var1.append("                                 AND action_log.shop_id = ? ");
		param.add(shopId);
		var1.append("                                 AND object_type = 3) AL3 ");
		var1.append("                      ON CT.customer_id = AL3.customer_id ");
		var1.append("               LEFT JOIN (SELECT * ");
		var1.append("                          FROM   action_log ");
		var1.append("                          WHERE  dayInOrder(?) = Date(start_time) ");
		param.add(date_now);
		var1.append("                                 AND object_type = 4) AL4 ");
		var1.append("                      ON CT.customer_id = AL4.customer_id ");
		var1.append("               LEFT JOIN (SELECT customer_id, ");
		var1.append("                                 lat, ");
		var1.append("                                 lng, ");
		var1.append("                                 Max(start_time) AS START_TIME ");
		var1.append("                          FROM   action_log ");
		var1.append("                          WHERE  ? = Date(start_time) ");
		param.add(date_now);
		var1.append("                                 AND object_type IN ( 0, 1 ) ");
		var1.append("                                 AND end_time IS NOT NULL ");
		var1.append("                                 AND staff_id = ? ");
		param.add(String.valueOf(staffId));
		var1.append("                                 AND shop_id = ? ");
		param.add(shopId);
		var1.append("                          GROUP  BY customer_id) LATLNG ");
		var1.append("                      ON CT.customer_id = LATLNG.customer_id ");
//		var1.append("               LEFT JOIN (SELECT DCM.customer_id         AS CUSTOMER_ID_3, ");
//		var1.append("                                 DP.display_program_id   AS DISPLAY_PROGRAM_ID, ");
//		var1.append("                                 DP.display_program_code AS DISPLAY_PROGRAM_CODE ");
//		var1.append("                          FROM   display_program AS DP, ");
//		var1.append("                                 display_staff_map AS DSM, ");
//		var1.append("                                 display_program_level AS DPL, ");
//		var1.append("                                 display_customer_map AS DCM ");
//		var1.append("                          WHERE  DP.display_program_id = DSM.display_program_id ");
//		var1.append("                                 AND DP.display_program_id = ");
//		var1.append("                                     DPL.display_program_id ");
//		var1.append("                                 AND DSM.display_staff_map_id = ");
//		var1.append("                                     DCM.display_staff_map_id ");
//		var1.append("                                 AND DPL.display_program_level_id = ");
//		var1.append("                                     DCM.display_program_level_id ");
//		var1.append("                                 AND Date(DP.from_date) <= Date(?) ");
//		param.add(date_now);
//		var1.append("                                 AND Ifnull(Date(DP.to_date) >= Date(?), 1) ");
//		param.add(date_now);
//		var1.append("                                 AND DP.status = 1 ");
//		var1.append("                                 AND DSM.staff_id = ? ");
//		param.add(String.valueOf(staffId));
//		var1.append("                                 AND Date(DSM.month, 'start of month') = ? ");
//		param.add(String.valueOf(startOfMonth));
//		var1.append("                                 AND DSM.status = 1 ");
//		var1.append("                                 AND DPL.status = 1 ");
//		var1.append("								AND DCM.SHOP_ID = ? ");
//		param.add(String.valueOf(shopId));
//		var1.append("								AND DSM.SHOP_ID = ? ");
//		param.add(String.valueOf(shopId));
//		var1.append("                                 AND Date(DCM.from_date) <= Date(?) ");
//		param.add(date_now);
//		var1.append("                                 AND Ifnull(Date(DCM.to_date) >= Date(?), 1) ");
//		param.add(date_now);
//		var1.append("                                 AND DCM.status = 1 ");
//		var1.append("                                 AND DP.display_program_id NOT IN (SELECT ");
//		var1.append("                                     object_id AS DISPLAY_PROGRAME_ID ");
//		var1.append("                                                                   FROM ");
//		var1.append("                                     action_log ");
//		var1.append("                                                                   WHERE ");
//		var1.append("                                     object_type = 2 ");
//		var1.append("                                     AND staff_id = ? ");
//		param.add(String.valueOf(staffId));
//		var1.append("                                     AND shop_id = ? ");
//		param.add(shopId);
//		var1.append("                                     AND Date(start_time) ");
//		var1.append("                                         <= Date(?) ");
//		param.add(date_now);
//		var1.append("                                     AND customer_id = ");
//		var1.append("                                         DCM.customer_id ");
//		var1.append("                                     AND ( Date(end_time) ");
//		var1.append("                                           >= Date(?) ");
//		param.add(date_now);
//		var1.append("                                            OR end_time IS ");
//		var1.append("                                               NULL ))) ");
//		var1.append("                                                                       CTTB ");
//		var1.append("                      ON CTTB.customer_id_3 = CT.customer_id ");
		var1.append("               LEFT JOIN (SELECT * ");
		var1.append("                          FROM   sale_order SO ");
		var1.append("                          WHERE  SO.staff_id = ? ");
		param.add(String.valueOf(staffId));
		var1.append("                                 AND Ifnull(Date(SO.order_date) >= ?, 0)) KHT ");
		param.add(startOfTwoPreviousMonth);
		var1.append("                      ON KHT.customer_id = CT.customer_id ");
		var1.append("        WHERE  1 = 1 ");
		var1.append("               AND CT.customer_id = ? ");
		param.add(String.valueOf(customerId));
		var1.append("               AND CT.status = 1) TEMP ");
		var1.append("WHERE  1 = 1 ");
		var1.append("GROUP  BY customer_id ");

		try {
			double shopDistance = 0;
			String getShopDistance = " select DISTANCE_ORDER from SHOP where SHOP_ID = ? ";
			cDistance = rawQuery(getShopDistance, new String[] { "" + shopId });
			if (cDistance != null) {
				if (cDistance.moveToFirst()) {
					shopDistance = cDistance.getDouble(0);
				}
			}
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						item = new CustomerListItem();
						item.initDataFromCursor(c, shopDistance, "");
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (cDistance != null) {
					cDistance.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return item;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param customerDTO
	 * @return: void
	 * @throws Exception
	 * @throws:
	*/
	public void setDebitInfoCustomer(CustomerDTO customerDTO) throws Exception{

		ArrayList<String> params = new ArrayList<String>();
		params.add(""+customerDTO.customerId);
		StringBuffer sql = new StringBuffer();
		sql.append("select APPLY_DEBIT_LIMITED, MAX_DEBIT_AMOUNT, MAX_DEBIT_DATE from CUSTOMER where CUSTOMER_ID = ? ");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						customerDTO.applyDebitLimited = CursorUtil.getInt(c, "APPLY_DEBIT_LIMITED");
						customerDTO.maxDebitAmount = CursorUtil.getLong(c, "MAX_DEBIT_AMOUNT");
						customerDTO.maxDebitDate = CursorUtil.getString(c, "MAX_DEBIT_DATE");
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("", ex.toString());
			}

		}
	}

	/**
	 * danh sach khach hang dang quan ly thiet bi
	 * @author: hoanpd1
	 * @since: 15:27:07 25-12-2014
	 * @return: CustomerListDTO
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerName
	 * @param customerCode
	 * @param visitPlan
	 * @param isGetWrongPlan
	 * @param page
	 * @param isGetTotalPage
	 * @param isFromRouteView
	 * @return
	 * @throws Exception
	 */
	public CustomerListDTO getListCustomerManageEquipment(String ownerId, String staffId, String shopId,
			String customerName, String customerCode, String visitPlan,
			boolean isGetWrongPlan, int page, String listStaff, int isGetTotalPage) throws Exception {
		CustomerListDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		String daySeq = "";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);

		if (!StringUtil.isNullOrEmpty(visitPlan)) {
			if (visitPlan.equals(Constants.getDayLine()[0])) {
				daySeq = "2";
			} else if (visitPlan.equals(Constants.getDayLine()[1])) {
				daySeq = "3";
			} else if (visitPlan.equals(Constants.getDayLine()[2])) {
				daySeq = "4";
			} else if (visitPlan.equals(Constants.getDayLine()[3])) {
				daySeq = "5";
			} else if (visitPlan.equals(Constants.getDayLine()[4])) {
				daySeq = "6";
			} else if (visitPlan.equals(Constants.getDayLine()[5])) {
				daySeq = "7";
			} else if (visitPlan.equals(Constants.getDayLine()[6])) {
				daySeq = "8";
			}
		}

		Cursor cDistance = null;
		String getShopDistance = " select DISTANCE_ORDER from SHOP where SHOP_ID = "
				+ shopId;
		try {
			cDistance = rawQuery(getShopDistance, null);
			if (cDistance != null) {
				if (cDistance.moveToFirst()) {
					dto = new CustomerListDTO();
					dto.setDistance(cDistance.getDouble(0));
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (cDistance != null) {
					cDistance.close();
				}
			} catch (Exception ex) {
			}
		}

		StringBuffer var1 = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();
		var1.append(" SELECT  cus.* , visit.*,   ST_CT.*  ");
		var1.append(" FROM   (SELECT VP.VISIT_PLAN_ID        AS VISIT_PLAN_ID, ");
		var1.append("               VP.SHOP_ID              AS SHOP_ID, ");
		var1.append("               VP.STAFF_ID             AS STAFF_ID, ");
		var1.append("               VP.FROM_DATE            AS FROM_DATE, ");
		var1.append("               VP.TO_DATE              AS TO_DATE, ");
		var1.append("               RT.ROUTING_ID           AS ROUTING_ID, ");
		var1.append("               RT.ROUTING_CODE         AS ROUTING_CODE, ");
		var1.append("               RT.ROUTING_NAME         AS ROUTING_NAME, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               SUBSTR(CT.CUSTOMER_CODE, 1, 3)  AS CUSTOMER_CODE, ");
		var1.append("               CT.SHORT_CODE  			AS SHORT_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               st.staff_code           AS STAFF_CODE, ");
		var1.append("               st.staff_name           AS STAFF_NAME, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.MONDAY = 1 THEN 'T2' ");
		var1.append("                   ELSE '' ");
		var1.append("                 END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.TUESDAY = 1 THEN ',T3' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.WEDNESDAY = 1 THEN ',T4' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.THURSDAY = 1 THEN ',T5' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.FRIDAY = 1 THEN ',T6' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SATURDAY = 1 THEN ',T7' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END ");
		var1.append("                 || CASE ");
		var1.append("                      WHEN RTC.SUNDAY = 1 THEN ',CN' ");
		var1.append("                      ELSE '' ");
		var1.append("                    END )              AS CUS_PLAN, ");
		var1.append("               ( CASE ");
		var1.append("                   WHEN RTC.SEQ2 = 0 THEN '' ");
		var1.append("                   ELSE RTC.SEQ2 ");
		var1.append("                 END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ3 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ3 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ4 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ4 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ5 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ5 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ6 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ6 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ7 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ7 ");
		var1.append("                    END ) ");
		var1.append("               || ( CASE ");
		var1.append("                      WHEN RTC.SEQ8 = 0 THEN '' ");
		var1.append("                      ELSE ',' ");
		var1.append("                           || RTC.SEQ8 ");
		var1.append("                    END )              AS PLAN_SEQ, ");
		var1.append("               RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");

		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("  			, RTC.SEQ" + daySeq + " as DAY_SEQ");
		}

		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");

		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(date_now);
		totalParam.add(date_now);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		var1.append("										  end ) as integer) ) ");
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		param.add(firstDayOfYear);
		totalParam.add(firstDayOfYear);
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		
		// LAY THONG TIN NHANVINE
		var1.append("               JOIN staff st ");
		var1.append("                 ON VP.staff_id = st.staff_id ");
		var1.append("                    AND st.status = 1 ");
		var1.append("                    AND st.staff_id IN ( " + listStaff +" ) ");
		var1.append("                    AND st.shop_id = ct.shop_id ");
		
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(date_now);
		totalParam.add(date_now);
//		var1.append("               AND DATE(RTC.START_DATE) <= DATE(?) ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("               AND (DATE(RTC.END_DATE) >= DATE(?) OR DATE(RTC.END_DATE) IS NULL) ");
//		param.add(date_now);
//		totalParam.add(date_now);
		
		if (staffId != null && Long.parseLong(staffId) > 0) {
			var1.append("               AND VP.staff_id = ? ");
			param.add(String.valueOf(staffId));
			totalParam.add(String.valueOf(staffId));
		}
		
		var1.append("               AND VP.STATUS in (0, 1) ");
		var1.append("               AND VP.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS in (0, 1) ");
		var1.append("               ) CUS ");
		var1.append("				, (select e.equip_id, ");
		var1.append("       				e.equip_group_id, ");
		var1.append("       				e.stock_id ");
		var1.append("    			  from  equipment e, ");
		var1.append("         				equip_group eg, ");
		var1.append("            			equip_category ec  ");
		var1.append("    			  where 1=1 ");
		var1.append("          				and e.status =1 ");
		var1.append("          				and eg.status =1 ");
		var1.append("          				and ec.status =1 ");
		var1.append("          				and e.stock_type =2 ");
		var1.append("          				and e.trade_status = 0 ");
		var1.append("          				and e.usage_status in (3, 4) ");
		var1.append("          				and (substr(e.first_date_in_use,1,10) <= substr( ?,1,10) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("            					or e.first_date_in_use is null ) ");
		var1.append("          				and e.equip_group_id = eg.equip_group_id ");
		var1.append("          				and eg.equip_category_id = ec.equip_category_id ");
		var1.append("          				group by e.stock_id ) eq ");
		var1.append("       LEFT JOIN (SELECT STAFF_ID, ");
		var1.append("                         ACTION_LOG_ID AS VISIT_ACT_LOG_ID, ");
		var1.append("                         CUSTOMER_ID AS CUSTOMER_ID_2, ");
		var1.append("                         OBJECT_TYPE AS VISIT, ");
		var1.append("                         START_TIME, ");
		var1.append("                         END_TIME, ");
		var1.append("               STRFTIME('%H:%M', START_TIME)  AS VISIT_START_TIME_SHORT, ");
		var1.append("                         LAT AS VISITED_LAT, ");
		var1.append("                         LNG AS VISITED_LNG, ");
		var1.append("                         IS_OR ");
		var1.append("                  FROM   ACTION_LOG ");
		var1.append("                  WHERE  STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("                  		  AND  SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("                         AND DATE(START_TIME) = DATE(?) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("                         AND OBJECT_TYPE IN ( 0, 1 ) ");
		var1.append("                  GROUP  BY CUSTOMER_ID) VISIT ");
		var1.append("              ON VISIT.CUSTOMER_ID_2 = CUS.CUSTOMER_ID ");
		var1.append("		LEFT JOIN ");
		var1.append("		         (SELECT CUSTOMER_ID AS CUSTOMER_ID_5,  ");
		var1.append("                    Strftime('%d/%m/%Y', EXCEPTION_ORDER_DATE) as EXCEPTION_ORDER_DATE,  ");
    	var1.append("                    STAFF_CUSTOMER.STAFF_CUSTOMER_ID  ");
    	var1.append("             FROM STAFF_CUSTOMER  ");
    	var1.append("             WHERE STAFF_ID = ?  ");
   		param.add(String.valueOf(staffId));
		totalParam.add(String.valueOf(staffId));
		var1.append("                   AND SHOP_ID = ?) ST_CT ");
		param.add(String.valueOf(shopId));
		totalParam.add(String.valueOf(shopId));
		var1.append("		ON ST_CT.CUSTOMER_ID_5 = CUS.CUSTOMER_ID ");
		var1.append(" WHERE 1 = 1 ");
		var1.append("      AND eq.stock_id = cus.customer_id ");
		if (!StringUtil.isNullOrEmpty(visitPlan)) {// ko phai chon tat ca
			if (isGetWrongPlan) {
				var1.append("	and (upper(CUS_PLAN) like upper(?) OR IS_OR IN (0,1) )");
				param.add("%" + visitPlan + "%");
				totalParam.add("%" + visitPlan + "%");
			} else {
				var1.append("	and upper(CUS_PLAN) like upper(?) ");
				param.add("%" + visitPlan + "%");
				totalParam.add("%" + visitPlan + "%");
			}
		}
		
//		if (!StringUtil.isNullOrEmpty(customerCode)) {
//			customerCode = StringUtil.escapeSqlString(customerCode);
//			customerCode = DatabaseUtils.sqlEscapeString("%" + customerCode
//					+ "%");
//			var1.append("	and upper(CUSTOMER_CODE) like upper(");
//			var1.append(customerCode);
//			var1.append(") escape '^' ");
//		}
		
//		if (!StringUtil.isNullOrEmpty(customerName)) {
//			customerName = StringUtil
//					.getEngStringFromUnicodeString(customerName);
//			customerName = StringUtil.escapeSqlString(customerName);
//			customerName = DatabaseUtils.sqlEscapeString("%" + customerName
//					+ "%");
//			var1.append("	and upper(NAME_TEXT) like upper(");
//			var1.append(customerName);
//			var1.append(") escape '^' ");
//		}
		
		if (!StringUtil.isNullOrEmpty(customerName)) {
			customerName = StringUtil.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			var1.append("	and ( upper(SHORT_CODE) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(NAME_TEXT) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(CUSTOMER_NAME) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(ADDRESS) like upper(?) escape '^') ");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
		}
		
		var1.append("   group by CUSTOMER_ID ");
		if (!StringUtil.isNullOrEmpty(daySeq)) {
			var1.append("   order by DAY_SEQ asc, CUSTOMER_CODE asc, CUSTOMER_NAME asc ");
		} else {
			var1.append("   order by CUSTOMER_CODE asc, CUSTOMER_NAME asc ");
		}

		if (page > 0) {
			// get count
			if (isGetTotalPage == 1) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ var1 + ")");
			}
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null && dto != null) {
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataFromCursor(c, dto.getDistance(), visitPlan);
						//int num = c.getCount();
						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalCustomer(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	/**
	 * @author: dungdq3
	 * @param sortInfo
	 * @since: 10:28:32 AM Sep 16, 2014
	 * @return: NewCustomerListDTO
	 * @param:
	 * @throws Exception
	 */
	public NewCustomerListDTO getListNewCustomer(String staffId, String shopId, String customerName, int numItemInPage, int currentIndexSpinnerState, int currentPage, DMSSortInfo sortInfo) throws Exception {
		// TODO Auto-generated method stub
		NewCustomerListDTO dto = new NewCustomerListDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		boolean isGetTotalItem = currentPage < 0;
		//neu <0 mac dinh lay trang 1
		numItemInPage = numItemInPage < 0 ? 1 : numItemInPage;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("		SELECT * FROM (	");
		var1.append("		  SELECT CT.CUSTOMER_ID AS CUSTOMER_ID	");
		var1.append("			, CT.CUSTOMER_NAME AS CUSTOMER_NAME	");
		var1.append("			, CT.NAME_TEXT AS NAME_TEXT	");
		var1.append("			, CT.MOBIPHONE AS MOBIPHONE	");
		var1.append("			, CT.PHONE AS PHONE	");
		var1.append("			, CT.ADDRESS AS ADDRESS	");
//		var1.append("			, (IFNULL(CT.MOBIPHONE,'') || '/ ' || IFNULL(CT.PHONE,'')) AS PHONE	");
		var1.append("			, (IFNULL(CT.HOUSENUMBER,'') || ' ' || IFNULL(CT.STREET,'')) AS STREET	");
		//cho gui SYN_STATE = 1 => 2
		var1.append("			, (CASE WHEN SYN_STATE = 0 or SYN_STATE is null THEN '2' 	");
		//Loi SYN_STATE = 1 => 3
		var1.append("				ELSE (CASE WHEN SYN_STATE = 1 THEN '3' 	");
		//cho duyet STATUS = 2 => 1
		var1.append("						ELSE (CASE WHEN STATUS = 2 THEN '1' 	");
		//tu choi STATUS = 3 => 4
		var1.append("								ELSE (CASE WHEN STATUS = 3 THEN '4' 	");
		var1.append("									  END) 	");
		var1.append("							  END) 	");
		var1.append("					  END) 	");
		var1.append("			  END) AS STATE  	");
		var1.append("		  FROM CUSTOMER CT	");
		var1.append("		  WHERE 1 = 1	");
		var1.append("			AND ( CT.STATUS IN (2, 3) OR CT.SYN_STATE IN (0, 1) )  	");
		var1.append("			AND CT.STATUS NOT IN (-1)  	");
		var1.append("		   AND CT.CREATE_USER_ID = ?	");
		//ADD PARAM STAFF_ID
		param.add(staffId);
		var1.append("		   AND CT.SHOP_ID = ?	");
		param.add(shopId);
		var1.append("		)	");
		var1.append("		WHERE 1 = 1	");
		if (currentIndexSpinnerState > 0) {
			var1.append("		   AND STATE = ? ");
			//ADD PARAM STATE
			param.add(String.valueOf(currentIndexSpinnerState));
		}

		if (!StringUtil.isNullOrEmpty(customerName)) {
			customerName = StringUtil.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			customerName = DatabaseUtils.sqlEscapeString("%" + customerName + "%");
			customerName = customerName.substring(1, customerName.length() -1);
			var1.append("	AND upper(NAME_TEXT) like upper(?) escape '^' ");
			param.add(customerName);
//			var1.append(customerName);
//			var1.append(") escape '^' ");
		}

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY STATE DESC, upper(CUSTOMER_NAME) ASC ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
				.addMapper(SortActionConstants.TYPE, "STATE")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		var1.append(orderByStr);

		String sql = var1.toString();
		String sqlTotal = StringUtil.getCountSql(sql);

		try {
			if (isGetTotalItem) {
				cTotalRow = rawQueries(sqlTotal, param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						do {
							dto.totalItem = cTotalRow.getInt(0);
						} while (cTotalRow.moveToNext());
					}
				}
			}

			//them phan trang cho sql
			sql += StringUtil.getPagingSql(numItemInPage, currentPage);
			c = rawQueries(sql, param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						dto.AddCustomerItemFromCursor(c);

					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * Lay id khach hang
	 *
	 * @author: dungdq3
	 * @since: 8:38:09 AM Sep 18, 2014
	 * @return: CustomerDTO
	 * @throws:
	 * @param customerId
	 * @return:
	 * @throws Exception
	 */
	public CustomerDTO getCustomerByIdForCreateCustomer(String customerId) throws Exception {
		CustomerDTO customerDTO = null;
		Cursor c = null;
		try {
			String[] params = { customerId };
			c = query(CUSTOMER_ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					customerDTO = new CustomerDTO();
					customerDTO.initCreateCustomerDTOFromCursor(c);
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return customerDTO;
	}


	/**
	 * danh sach vung
	 *
	 * @author: dungdq3
	 * @since: 4:07:30 PM Jan 26, 2015
	 * @return: ArrayList<AreaItem>
	 * @throws:
	 * @param areaType
	 * @param idProvine
	 * @return
	 * @throws Exception
	 */
	public ArrayList<AreaItem> getListArea(int areaType, long idProvine) throws Exception {
		ArrayList<AreaItem> result = new ArrayList<AreaItem>();
		Cursor c = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append(" SELECT a.PARENT_AREA_ID PARENT_AREA_ID, a.AREA_ID AREA_ID, a.AREA_NAME AREA_NAME");
		var1.append(" FROM AREA a ");
		var1.append(" WHERE 1=1 ");
		var1.append(" AND a.TYPE = ? ");
		param.add(String.valueOf(areaType));
		if (idProvine > 0) {
			var1.append(" AND a.PARENT_AREA_ID = ? ");
			param.add(String.valueOf(idProvine));
		}
		var1.append(" ORDER BY UPPER(a.AREA_NAME) ASC ");
		String sql = var1.toString();
		try {
			c = rawQueries(sql, param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						AreaItem area = new AreaItem();
						area.initFromCursor(c);
						result.add(area);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return result;
	}

	/**
	 * get id vung cha
	 *
	 * @author: dungdq3
	 * @since: 4:45:25 PM Jan 26, 2015
	 * @return: long
	 * @throws:
	 * @param idDistrict
	 * @return
	 * @throws Exception
	 */
	public long getParrentAreaId(long idDistrict) throws Exception {
		int result = 0;
		Cursor c = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append(" SELECT a.PARENT_AREA_ID ID ");
		var1.append(" FROM AREA a ");
		var1.append(" WHERE 1=1 ");
		var1.append(" AND AREA_ID = ? ");
		param.add(String.valueOf(idDistrict));

		String sql = var1.toString();
		try {
			c = rawQueries(sql, param);

			if (c != null) {
				if (c.moveToFirst()) {
					 result = (int)CursorUtil.getLong(c, "ID");
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * lay danh sach loai kh
	 *
	 * @author: dungdq3
	 * @since: 4:48:12 PM Jan 26, 2015
	 * @return: ArrayList<CustomerType>
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public ArrayList<CustomerType> getListCustomerType() throws Exception {
		ArrayList<CustomerType> result = new ArrayList<CustomerType>();
		Cursor c = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append(" SELECT CHT.CHANNEL_TYPE_ID CHANNEL_TYPE_ID, CHT.CHANNEL_TYPE_NAME CHANNEL_TYPE_NAME, CHT.OBJECT_TYPE OBJECT_TYPE ");
		var1.append(" FROM CHANNEL_TYPE CHT ");
		var1.append(" WHERE 1=1 AND CHT.STATUS = 1 AND CHT.TYPE = 3 ");
		var1.append(" ORDER BY UPPER(CHT.CHANNEL_TYPE_NAME) ASC ");
		String sql = var1.toString();
		try {
			c = rawQueries(sql, param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						CustomerType type = new CustomerType();
						type.initFromCursor(c);
						result.add(type);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * them moi kh
	 *
	 * @author: dungdq3
	 * @since: 5:49:13 PM Jan 26, 2015
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long insertCustomer(CustomerDTO dto) throws Exception {
		TABLE_ID idTable = new TABLE_ID(mDB);
		dto.customerId = idTable.getMaxIdTime(CUSTOMER_TABLE);
		dto.customerCode = String.valueOf(dto.customerId);
		addInfoForNewCustomer(dto);

		return insert(dto);
	}

	private void addInfoForNewCustomer(CustomerDTO dto) throws Exception {
		dto.setStatus(2);

		String areaAddress = getAddressFromAreaId(dto.areaId);
		String houseNumer = (StringUtil.isNullOrEmpty(dto.houseNumber) ? ""
				: dto.houseNumber
						+ ", ");
		String street = (StringUtil.isNullOrEmpty(dto.street) ? ""
				: dto.street + ", ");
		String address = houseNumer
				+ street;
		dto.setAddress(address + dto.address);

		String nameText = dto.getCustomerName()
				+ " - " + dto.address;
		nameText = StringUtil.getEngStringFromUnicodeString(nameText).toUpperCase();
		dto.nameText = nameText;
	}

	private String getAddressFromAreaId(int areaId) throws Exception {
		String result = "";
		Cursor c = null;

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		var1.append(" SELECT IFNULL(a.PRECINCT_NAME || ', ', '') ||  IFNULL(a.DISTRICT_NAME || ', ', '') ||  IFNULL(a.PROVINCE_NAME, '') AS AREA_ADRRESS");
		var1.append(" FROM AREA a ");
		var1.append(" WHERE 1=1 ");
		var1.append(" AND a.AREA_ID = ? ");
		param.add(String.valueOf(areaId));

		String sql = var1.toString();
		try {
			c = rawQueries(sql, param);
			if (c != null) {
				if (c.moveToFirst()) {
					result = CursorUtil.getString(c, "AREA_ADRRESS");
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return result;
	}

	/**
	 * cap nhat kh
	 *
	 * @author: dungdq3
	 * @since: 11:20:47 AM Jan 27, 2015
	 * @return: long
	 * @throws:
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long updateCustomer(CustomerDTO dto) throws Exception {
		addInfoForNewCustomer(dto);

		ContentValues value = initUpdateDataRow(dto);
		String[] params = { ""
				+ dto.getCustomerId() };
		return update(value, CUSTOMER_ID
				+ " = ?", params);
	}

	private ContentValues initUpdateDataRow(CustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(CUSTOMER_NAME, dto.getCustomerName());
		editedValues.put(AREA_ID, dto.areaId);
		editedValues.put(SALE_POSITION_ID, dto.typePostitionId);
		String bDay = DateUtils.convertDateOneFromFormatToAnotherFormat(dto.birthDate, DateUtils.DATE_STRING_DD_MM_YYYY, DateUtils.DATE_STRING_YYYY_MM_DD);
		editedValues.put(BIRTHDAY, bDay);
		editedValues.put(HOUSENUMBER, dto.getHouseNumber());
		editedValues.put(STREET, dto.getStreet());
		editedValues.put(PHONE, dto.getPhone());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());
		editedValues.put(CONTACT_NAME, dto.getContactPerson());
		editedValues.put(MOBIPHONE, dto.getMobilephone());
		editedValues.put(CHANNEL_TYPE_ID, dto.getCustomerTypeId());
		editedValues.put(UPDATE_USER, dto.getUpdateUser());
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(LAT, dto.getLat());
		editedValues.put(LNG, dto.getLng());
		editedValues.put(ADDRESS, dto.address);
		editedValues.put(EMAIL, dto.email);
		editedValues.put(FAX, dto.fax);
		editedValues.put(NAME_TEXT, dto.nameText);
		editedValues.put(SYN_STATE, 0);
		return editedValues;
	}

	/**
	 * Lay du lieu danh sach khach hang cho row bao cao
	 * @author: hoanpd1
	 * @since: 16:12:03 11-02-2015
	 * @return: ReportRowStaffDialogDTO
	 * @throws:
	 * @param viewData
	 * @return
	 * @throws Exception
	 */
	public DynamicCustomerListDTO getDataCustomerReportChooseRow(Bundle viewData)
			throws Exception {
		// TODO Auto-generated method stub
		DynamicCustomerListDTO dto = new DynamicCustomerListDTO();
		boolean isGetTotalPage = (Boolean) viewData.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		int page = viewData.getInt(IntentConstants.INTENT_PAGE);
		String textSearch = viewData.getString(IntentConstants.INTENT_SEARCH);
		boolean isStatusCustomer = viewData.getBoolean(IntentConstants.INTENT_STATUS_CUSTOMER);
		boolean isStatusCustomerOfLine = viewData.getBoolean(IntentConstants.INTENT_STATUS_CUSTOMER_OF_LINE);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String shopId = viewData.getString(IntentConstants.INTENT_SHOP_ID);
		int staffId = viewData.getInt(IntentConstants.INTENT_STAFF_ID);
		long reportTemplateId = viewData.getLong(IntentConstants.INTENT_REPORT_TEMPLATE_ID);

		String orderColumn = viewData.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		boolean orderType = viewData.getBoolean(IntentConstants.INTENT_ORDER_TYPE, true);

		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer  sqlObject = new StringBuffer();
		sqlObject.append(" select ct.customer_id AS CUSTOMER_ID, ");
		sqlObject.append("       SUBSTR(ct.customer_code,1,3) AS CUSTOMER_CODE , ");
		sqlObject.append("       ct.customer_name AS CUSTOMER_NAME, ");
		sqlObject.append("       (ifnull (ct.address,(ifnull(CT.HOUSENUMBER, '')|| ' ' || ifnull(CT.STREET,'') ))) AS ADDRESS, ");
		sqlObject.append("       ct.status AS STATUS_CUSTOMER, ");
		sqlObject.append("       (CASE WHEN rtc.status = 0 OR (substr(rtc.end_date,1,10) < ? ) ");
		param.add(dateNow);
		sqlObject.append(" 		                   THEN 0 ELSE 1 END) AS STATUS_LINE_CUSTOMER ");
		sqlObject.append(" from visit_plan vp, ");
		sqlObject.append("     routing rt, ");
		sqlObject.append("     routing_customer rtc, ");
		sqlObject.append("     customer ct, ");
		sqlObject.append("     report_template_object rto ");
		sqlObject.append(" where 1=1 ");
		sqlObject.append("      and vp.status = 1 ");
		sqlObject.append("      and rt.status = 1 ");
		sqlObject.append("      and vp.staff_id = ? ");
		param.add(""+staffId);
		sqlObject.append("      and vp.shop_id = ? ");
		param.add(""+shopId);
		sqlObject.append("      and ct.customer_id = rto.object_id ");
		sqlObject.append("      and rto.report_template_id = ? ");
		param.add(""+reportTemplateId);
		sqlObject.append("      and vp.routing_id = rt.routing_id ");
		sqlObject.append("      and rtc.routing_id = rt.routing_id ");
		sqlObject.append("      and rtc.customer_id = ct.customer_id ");
		sqlObject.append("      and substr(vp.from_date,1,10) <= ? ");
		param.add(dateNow);
		sqlObject.append("      and (substr(vp.to_date,1,10) >= ? ");
		param.add(dateNow);
		sqlObject.append("            OR substr(vp.to_date,1,10) IS NULL ) ");
		sqlObject.append("      and substr(start_date,1,10) <= ? ");
		param.add(dateNow);
		if (!isStatusCustomer) {
			sqlObject.append("	and ct.status = 1	");
		}
		if (!isStatusCustomerOfLine) {
			sqlObject.append("	and rtc.status = 1	");
			sqlObject.append("   and (substr(end_date,1,10) >= ? ");
			param.add(dateNow);
			sqlObject.append("                OR substr(end_date,1,10) IS NULL )");
		}
		if (!StringUtil.isNullOrEmpty(textSearch)) {
			textSearch = StringUtil.getEngStringFromUnicodeString(textSearch);
			textSearch = StringUtil.escapeSqlString(textSearch);
			sqlObject.append("	and (upper(ct.customer_code) like upper(?) or upper(ct.customer_name) like upper(?) or upper(ct.address) like upper(?))  ");
			param.add("%" + textSearch + "%");
			param.add("%" + textSearch + "%");
			param.add("%" + textSearch + "%");
		}

		if(StringUtil.isNullOrEmpty(orderColumn)) {
			sqlObject.append("	order by CUSTOMER_CODE asc ,CUSTOMER_NAME, ADDRESS asc	");
		} else {
			sqlObject.append(StringUtil.getStringOrder(orderColumn, orderType));
		}
		if (page > 0) {
			// get count
			if (isGetTotalPage) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ sqlObject + ")");
			}
			sqlObject.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlObject.append(" offset "
					+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor cCursor = null;
		Cursor cTotalRow = null;

		try {
			cCursor = rawQueries(sqlObject.toString(), param);
			if (cCursor != null) {
				if (cCursor.moveToFirst()) {
					do {
						DynamicCustomerDTO item = new DynamicCustomerDTO();
						item.initFromCursor(cCursor);
						dto.listCustomerInPage.add(item);
					} while (cCursor.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage) {
				cTotalRow = rawQueries(totalPageSql.toString(), param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (cCursor != null) {
					cCursor.close();
				}
			} catch (Exception e) {
			}
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}
	
	/**
	 * danh sach khach hang dang quan ly thiet bi
	 * @author dungnt19
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public CustomerListDTO getListCustomerInventoryEquipment(Bundle b) throws Exception {
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String customerName = b.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		int objectType = b.getInt(IntentConstants.INTENT_STATISTIC_OBJECT_TYPE);
		int isGetTotalPage = b.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String equipStatisticRecordId = b.getString(IntentConstants.INTENT_EQUIP_STATISTIC_RECORD_ID);
		
		CustomerListDTO dto = null;
		Cursor c = null;
		Cursor cTotalRow = null;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		Cursor cDistance = null;
		String getShopDistance = " select DISTANCE_ORDER from SHOP where SHOP_ID = " + shopId;
		
		try {
			cDistance = rawQuery(getShopDistance, null);
			if (cDistance != null) {
				if (cDistance.moveToFirst()) {
					dto = new CustomerListDTO();
					dto.setDistance(cDistance.getDouble(0));
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (cDistance != null) {
					cDistance.close();
				}
			} catch (Exception ex) {
			}
		}

		StringBuffer var1 = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalParam = new ArrayList<String>();
		var1.append(" SELECT  cus.*  ");
		var1.append(" ,(select Strftime('%d/%m/%Y', statistic_date) from EQUIP_STATISTIC_REC_DTL ESRD ");
		var1.append(" where ESRD.EQUIP_STATISTIC_RECORD_ID = ? and ESRD.OBJECT_STOCK_TYPE = 2 and ");
		var1.append(" ESRD.OBJECT_STOCK_ID = CUS.CUSTOMER_ID order by create_date desc limit 1) LAST_INVENTORY_DATE ");
		var1.append(" , COUNT (1) NUM_INVENTORY_EQUIPMENT ");
		param.add(equipStatisticRecordId);
		totalParam.add(equipStatisticRecordId);
		var1.append(" FROM    ");
		var1.append(" (SELECT ");
		var1.append("               CT.SHOP_ID              AS SHOP_ID, ");
		var1.append("               CT.CUSTOMER_ID          AS CUSTOMER_ID, ");
		var1.append("               SUBSTR(CT.CUSTOMER_CODE, 1, 3)  AS CUSTOMER_CODE, ");
		var1.append("               CT.SHORT_CODE  			AS SHORT_CODE, ");
		var1.append("               CT.CUSTOMER_NAME        AS CUSTOMER_NAME, ");
		var1.append("               CT.NAME_TEXT        	AS NAME_TEXT, ");
		var1.append("               CT.MOBIPHONE            AS MOBIPHONE, ");
		var1.append("               CT.PHONE                AS PHONE, ");
		var1.append("               CT.DELIVER_ID           AS DELIVER_ID, ");
		var1.append("               CT.CASHIER_STAFF_ID     AS CASHIER_STAFF_ID, ");
		var1.append("               CT.LAT                  AS LAT, ");
		var1.append("               CT.LNG                  AS LNG, ");
		var1.append("               CT.CHANNEL_TYPE_ID      AS CHANNEL_TYPE_ID, ");
		var1.append("               CT.ADDRESS              AS ADDRESS, ");
		var1.append("               ( ifnull(CT.HOUSENUMBER,'') ");
		var1.append("                 || ' ' ");
		var1.append("                 || ifnull(CT.STREET,'') )        AS STREET ");

		var1.append("        FROM    ");
//		var1.append("        FROM   VISIT_PLAN VP, ");
//		var1.append("               ROUTING RT, ");
//		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
//		var1.append("                       ROUTING_ID, ");
//		var1.append("                       CUSTOMER_ID, ");
//		var1.append("                       STATUS, ");
//		var1.append("                       WEEK1, ");
//		var1.append("                       WEEK2, ");
//		var1.append("                       WEEK3, ");
//		var1.append("                       WEEK4, ");
//		var1.append("                       MONDAY, ");
//		var1.append("                       TUESDAY, ");
//		var1.append("                       WEDNESDAY, ");
//		var1.append("                       THURSDAY, ");
//		var1.append("                       FRIDAY, ");
//		var1.append("                       SATURDAY, ");
//		var1.append("                       SUNDAY, ");
//		var1.append("                       SEQ2, ");
//		var1.append("                       SEQ3, ");
//		var1.append("                       SEQ4, ");
//		var1.append("                       SEQ5, ");
//		var1.append("                       SEQ6, ");
//		var1.append("                       SEQ7, ");
//		var1.append("                       SEQ8 ");
//		var1.append("                FROM   ROUTING_CUSTOMER ");
//
//		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("                DATE(START_DATE) <= ? and ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("                ( ");
//		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
//		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
//		param.add(date_now);
//		totalParam.add(date_now);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("		   (case when 1");
//		var1.append("			and (");
//		var1.append("		cast((case when strftime('%w', ?) = '0' ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  then 7 ");
//		var1.append("									  else strftime('%w', ?)                          ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  end ) as integer) < ");
//		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
//		var1.append("										  then 7 ");
//		var1.append("										  else strftime('%w', ?)                          ");
//		var1.append("										  end ) as integer) ) ");
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");
//
//		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
//		param.add(date_now);
//		totalParam.add(date_now);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("		   (case when 1");
//		var1.append("			and (");
//		var1.append("		cast((case when strftime('%w', ?) = '0' ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  then 7 ");
//		var1.append("									  else strftime('%w', ?)                          ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  end ) as integer) < ");
//		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
//		var1.append("										  then 7 ");
//		var1.append("										  else strftime('%w', ?)                          ");
//		var1.append("										  end ) as integer) ) ");
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");
//
//		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
//		param.add(date_now);
//		totalParam.add(date_now);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("		   (case when 1");
//		var1.append("			and (");
//		var1.append("		cast((case when strftime('%w', ?) = '0' ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  then 7 ");
//		var1.append("									  else strftime('%w', ?)                          ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  end ) as integer) < ");
//		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
//		var1.append("										  then 7 ");
//		var1.append("										  else strftime('%w', ?)                          ");
//		var1.append("										  end ) as integer) ) ");
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");
//
//		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
//		param.add(date_now);
//		totalParam.add(date_now);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("		   (case when 1");
//		var1.append("			and (");
//		var1.append("		cast((case when strftime('%w', ?) = '0' ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  then 7 ");
//		var1.append("									  else strftime('%w', ?)                          ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("									  end ) as integer) < ");
//		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
//		var1.append("										  then 7 ");
//		var1.append("										  else strftime('%w', ?)                          ");
//		var1.append("										  end ) as integer) ) ");
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		param.add(firstDayOfYear);
//		totalParam.add(firstDayOfYear);
//		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
//		var1.append("                ) RTC, ");
		var1.append("               CUSTOMER CT ");
		
		var1.append("        WHERE  1 = 1 ");
//		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
//		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
//		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
//		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
//		param.add(date_now);
//		totalParam.add(date_now);
//		
//		var1.append("               AND VP.STATUS in (0, 1) ");
//		var1.append("               AND VP.SHOP_ID = ? ");
//		param.add(String.valueOf(shopId));
//		totalParam.add(String.valueOf(shopId));
//		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("      			AND CT.shop_id = ? ");
		param.add(shopId);
		totalParam.add(shopId);
		var1.append("               ) CUS ");
		var1.append("				, (select e.equip_id, ");
		var1.append("       				e.equip_group_id, ");
		var1.append("       				e.stock_id, ");
		var1.append("       				e.stock_type ");
		var1.append("    			  from  equipment e, ");
		var1.append("         				equip_group eg, ");
		var1.append("            			equip_category ec  ");
		var1.append("    			  where 1=1 ");
		var1.append("          				and e.status =1 ");
		var1.append("          				and eg.status =1 ");
		var1.append("          				and ec.status =1 ");
		var1.append("          				and e.stock_type = 2 ");
		var1.append("          				and e.trade_status in (0, 1) ");
		var1.append("          				and e.usage_status in (3) ");
		var1.append("          				and (substr(e.first_date_in_use,1,10) <= substr( ?,1,10) ");
		param.add(date_now);
		totalParam.add(date_now);
		var1.append("            					or e.first_date_in_use is null ) ");
		var1.append("          				and e.equip_group_id = eg.equip_group_id ");
		var1.append("          				and eg.equip_category_id = ec.equip_category_id ");
		
		if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_ALL) {
		} else if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_GROUP_EQUIPMENT) {
			var1.append("      AND e.equip_group_id in ( ");
			var1.append("      select object_id from equip_statistic_group esg ");
			var1.append("      		where 1 = 1 and esg.status = 1 ");
			var1.append("      			and esg.object_type = ? ");
			param.add(String.valueOf(objectType));
			totalParam.add(String.valueOf(objectType));
			var1.append("      			and esg.equip_statistic_record_id = ? ");
			param.add(String.valueOf(equipStatisticRecordId));
			totalParam.add(String.valueOf(equipStatisticRecordId));
			var1.append("      ) ");
		} else if(objectType == EquipStatisticRecordDTO.OBJECT_TYPE_EQUIPMENT) {
			var1.append("      AND e.equip_id in ( ");
			var1.append("      select object_id from equip_statistic_group esg ");
			var1.append("      		where 1 = 1 and esg.status = 1 ");
			var1.append("      			and esg.object_type = ? ");
			param.add(String.valueOf(objectType));
			totalParam.add(String.valueOf(objectType));
			var1.append("      			and esg.equip_statistic_record_id = ? ");
			param.add(String.valueOf(equipStatisticRecordId));
			totalParam.add(String.valueOf(equipStatisticRecordId));
			var1.append("      ) ");
		}
		
		var1.append("          				) eq ");
		var1.append(" WHERE 1 = 1 ");
		var1.append("      AND eq.stock_id = cus.customer_id ");
		
		if (!StringUtil.isNullOrEmpty(customerName)) {
			customerName = StringUtil.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			var1.append("	and ( upper(SHORT_CODE) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(NAME_TEXT) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(CUSTOMER_NAME) like upper(?) escape '^'");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
			var1.append("	or upper(ADDRESS) like upper(?) escape '^') ");
			param.add("%" + customerName + "%");
			totalParam.add("%" + customerName + "%");
		}
		
		var1.append("   group by CUSTOMER_ID ");
		var1.append("   order by CUSTOMER_CODE asc, CUSTOMER_NAME asc ");

		if (page > 0) {
			if (isGetTotalPage == 1) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("+ var1 + ")");
			}
			var1.append(" limit " + Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null && dto != null) {
				if (c.moveToFirst()) {
					do {
						CustomerListItem item = new CustomerListItem();
						item.initDataCustomerInventoryEquipment(c, dto.getDistance());
						dto.getCusList().add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0 && isGetTotalPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), totalParam);
				if (cTotalRow != null && dto != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalCustomer(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("getListCustomerInventoryEquipment", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}
}
