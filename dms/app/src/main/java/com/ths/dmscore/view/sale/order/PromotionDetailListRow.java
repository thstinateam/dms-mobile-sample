package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.text.Html;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Row ds CTKM them hang
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PromotionDetailListRow extends DMSTableRow implements OnClickListener{
	//so thu tu
	TextView tvSTT;
	//ma chuong trinh
	TextView tvCode;
	//ten chuong trinh
	TextView tvName;
	//ngay bat dau - ket thuc cua chuong trinh
	TextView tvDate;
	// chi tiet
	private TextView tvPromotionDescription;

	PromotionProgrameDTO currentDTO;

	public PromotionDetailListRow(Context context) {
		super(context, R.layout.layout_promotion_detail_row, GlobalUtil
				.getInstance().getDPIScreen()
				- GlobalUtil.dip2Pixel(Constants.LEFT_MARGIN_TABLE_DIP_SMALL
						* 2 + Constants.LEFT_MARGIN_TABLE_DIP * 2));
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvPromotionSTT);
		tvCode = (TextView) findViewById(R.id.tvPromotionCode);
		tvName = (TextView) findViewById(R.id.tvPromotionName);
		tvDate = (TextView) findViewById(R.id.tvPromotionDate);
		tvPromotionDescription = (TextView) findViewById(R.id.tvPromotionDescription);
	}

	 /**
	 * render du lieu cho row
	 * @author: Tuanlt11
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(int position, PromotionProgrameDTO item) {
		this.currentDTO = item;
		tvSTT.setText("" + position);
		tvCode.setText(item.getPROMOTION_PROGRAM_CODE());
		tvName.setText(item.getPROMOTION_PROGRAM_NAME());
		tvDate.setText(item.getFROM_DATE() + " - " + item.getTO_DATE());
		String descrip = "";
		if (item.getDESCRIPTION() != null && item.getDESCRIPTION().length() > 0) {
			descrip = item.getDESCRIPTION();
		}
		tvPromotionDescription.setText(Html.fromHtml(descrip));
	}

}
