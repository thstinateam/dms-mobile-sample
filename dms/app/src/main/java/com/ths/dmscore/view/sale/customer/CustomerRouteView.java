/**
 * Copyright 2012 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.customer;

import java.util.ArrayList;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.ImageButtonOnMap;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.OnEventMapControlListener;
import com.ths.map.BaseFragmentMapView;
import com.ths.map.CustomFragmentMapView;
import com.ths.map.OverlayViewItemObj;
import com.ths.map.OverlayViewOptions;
import com.ths.map.view.CustomerPopupView;
import com.ths.map.view.ShopPopupView;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.control.SpinerRoute;
import com.ths.dms.R;
import com.viettel.maps.base.LatLng;

/**
 * duong di toi uu toi khach hang
 *
 * @author banghn
 *
 * @author : BangHN since : 1.0 version : 1.0
 */
public class CustomerRouteView extends CustomFragmentMapView implements OnItemSelectedListener, OnClickListener, OnEventMapControlListener {
	private final int ACTION_CLICK_MARKER_CUSTOMER = 101;
	private final int ACTION_CLICK_FLAG_CUSTOMER = 102;
	private String[] arrLineChoose = new String[] {
			StringUtil.getString(R.string.TEXT_MONDAY),
			StringUtil.getString(R.string.TEXT_TUESDAY),
			StringUtil.getString(R.string.TEXT_WEDNESDAY),
			StringUtil.getString(R.string.TEXT_THURSDAY),
			StringUtil.getString(R.string.TEXT_FRIDAY),
			StringUtil.getString(R.string.TEXT_SATURDAY),
			StringUtil.getString(R.string.TEXT_SUNDAY) };
	public CustomerListDTO cusDto;// cusList
	private static final int ACTION_CREATE_CUSTOMER = 10;
	private static final int ACTION_ORDER_LIST = 11;
	private static final int ACTION_PATH = 12;
	private static final int ACTION_CUS_LIST = 13;
	private static final int ACTION_END_VISIT_OK = 16;
	private static final int ACTION_OK = 14;
	private static final int ACTION_CANCEL = 15;
	private static final int ACTION_CLICK_MARKER_SHOP = 17;
	public static final int ACTION_IMAGE = 18;
	private static final int ACTION_LOCK_DATE = 19;

	SpinerRoute spiner;// spiner tuyen trong tuan
	ArrayAdapter adapterLine;// spiner tuyen trong tuan
	ImageButtonOnMap direction;// chi dan lo trinh toi uu

	double lat, lng;// toa do ban do
	int totalCustomerInVisitPlan = 0; // tong so kh trong tuyen
	double latNVBH, lngNVBH;// toa do ban do
	boolean isRequestingGetListCustomer = false;
	int selectingItem = -1;
//	private OverlayViewLayer popupLayer;
//	private OverlayViewLayer customerLayer;
	private int realVisiting = 0;
	boolean isFirstView = true;
	private LinearLayout llSpiner;

//	private OverlayViewLayer myShopPosLayer;
//	MarkerMapView maker;
	ShopDTO shopInfo;
//	private OverlayViewLayer shopLayer;
	private GlobalInfo.VistConfig visitConfig = GlobalInfo.getInstance().getVisitConfig();
	
	public static CustomerRouteView getInstance(Bundle data) {
		CustomerRouteView f = new CustomerRouteView();
		f.setArguments(data);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = super.onCreateView(inflater, container, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_LOTRINH);
		lat = latNVBH = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		lng = lngNVBH = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		hideHeaderview();
		if (isFirstView) {
			setCustomerRoute(DateUtils.getCurrentDay());
			getCustomerInVisitPlan();
			getShopInfo();
		}
		//show help layout
		showRoutingInfoLayout();
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(PriHashMap.PriForm.NVBH_LOTRINH,
				new MenuTab(R.string.TEXT_PHOTO,
						R.drawable.menu_picture_icon, ACTION_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH),
				new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
						R.drawable.icon_lock, ACTION_LOCK_DATE, PriHashMap.PriForm.NVBH_CHOTKHO),
				new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
						ACTION_CREATE_CUSTOMER, PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH),
				new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
						ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
				new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
						ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
				new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
						ACTION_CUS_LIST, PriHashMap.PriForm.NVBH_BANHANG_DSKH));
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER_ROUTE));
		if (isRequestingGetListCustomer) {
			setCustomerRoute(selectingItem);
			drawCustomerLocation();
		}
	}

	/**
	 * load danh sach khach hang trong tuyen
	 *
	 * @author : BangHN since : 1.0
	 */
	private void getCustomerInVisitPlan() {
		realVisiting = 0;
		isRequestingGetListCustomer = true;
		Bundle b = new Bundle();
		b.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		b.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		b.putString(IntentConstants.INTENT_VISIT_PLAN,
				Constants.getDayLine()[selectingItem]);
//		b.putString(IntentConstants.INTENT_VISIT_PLAN,
//				getVisitPlan(arrLineChoose[spiner.getSelectedItemPosition()]));
		b.putBoolean(IntentConstants.INTENT_IS_FROM_ROUTE_VIEW, true);
		// 1: lay khach hang ngoai tuyen, 0 : nguoc lai
		if (getVisitPlan(arrLineChoose[spiner.getSelectedItemPosition()])
				.equals(Constants.getDayLine()[DateUtils.getCurrentDay()])) {
			b.putString(IntentConstants.INTENT_GET_WRONG_PLAN, "1");
		} else {
			b.putString(IntentConstants.INTENT_GET_WRONG_PLAN, "0");
		}

		handleViewEvent(b, ActionEventConstant.GET_CUSTOMER_LIST_FOR_ROUTE, SaleController.getInstance());
	}

	/**
	 * set toa do tam ban do
	 *
	 * @author : BangHN since : 1.0
	 */
	private void setMapViewLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
		super.drawMarkerMyPosition(lat, lng);
	}

	private String getVisitPlan(String date) {
		String d = "";
		if (date.equals(StringUtil.getString(R.string.TEXT_MONDAY))) {
			d = StringUtil.getString(R.string.TEXT_MONDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_TUESDAY))) {
			d = StringUtil.getString(R.string.TEXT_TUESDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_WEDNESDAY))) {
			d = StringUtil.getString(R.string.TEXT_WEDNESDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_THURSDAY))) {
			d = StringUtil.getString(R.string.TEXT_THUSDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_FRIDAY))) {
			d = StringUtil.getString(R.string.TEXT_FRIDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_SATURDAY))) {
			d = StringUtil.getString(R.string.TEXT_SATURDAY_);
		} else if (date.equals(StringUtil.getString(R.string.TEXT_SUNDAY))) {
			d = StringUtil.getString(R.string.TEXT_SUNDAY_);
		}
		return d;

	}

	/**
	 * Hien thi thong tin tren duong di
	 *
	 * @author: BangHN
	 * @param index
	 *            : Vi tri cua mot diem tren duong di
	 * @return: void
	 */
	private View showCustomerInfoPopup(int[] data, boolean isClikInCustomer) {
		int index = data[0];
		int isOrWithSelectedDay = data[1];
		if (index < 0) {
			return null;
		}

		OverlayViewOptions opts = new OverlayViewOptions();
		if (isClikInCustomer) {// click tren marker customer
			opts.position(new com.viettel.maps.base.LatLng(cusDto.getCusList()
					.get(index).aCustomer.getLat(),
					cusDto.getCusList().get(index).aCustomer.getLng()));
			opts.offsetHeight(-15);
		} else {// click tren marker ghe tham
			opts.position(new com.viettel.maps.base.LatLng(cusDto.getCusList()
					.get(index).visitedLat,
					cusDto.getCusList().get(index).visitedLng));
			opts.offsetHeight(-23);
		}
		opts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_CENTER);
		CustomerPopupView popupInfoView = new CustomerPopupView(parent, "",
				false);
		popupInfoView.setListener(this);
		popupInfoView.updateInfo(cusDto.getCusList().get(index), index,
				isOrWithSelectedDay, totalCustomerInVisitPlan, visitConfig);

		showPopupWhenClickMarker(popupInfoView, opts);
		return popupInfoView;
//		clearMapLayer(popupLayer);
//		popupLayer.addItemObj(popupInfoView, opts);
	}

	/**
	 * set layout tuyen ghe tham khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void setCustomerRoute(int day) {
		spiner = new SpinerRoute(parent, 0);
		spiner.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		spiner.setMinimumWidth(100);
		spiner.setGravity(Gravity.CENTER_HORIZONTAL);
//		adapterLine = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, arrLineChoose);
		adapterLine = new ArrayAdapter(parent,
				R.layout.simple_spinner_item, arrLineChoose);
		spiner.setOnItemSelectedListener(this);
		spiner.setAdapter(adapterLine);
		spiner.setSelection(day);
		selectingItem = spiner.getSelectedItemPosition();

//		LinearLayout llSpiner;
		if(llSpiner != null) {
			rlMainMapView.removeView(llSpiner);
		}
		llSpiner = new LinearLayout(rlMainMapView.getContext());

		llSpiner.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		llSpiner.setOrientation(LinearLayout.HORIZONTAL);
		llSpiner.addView(spiner);
		llSpiner.setGravity(Gravity.RIGHT | Gravity.TOP);

		rlMainMapView.addView(llSpiner);
//		mapView.invalidate();
	}

	/**
	 * Ve marker thong tin khach hang, nvbh
	 *
	 * @author : BangHN since : 1.0
	 */
	private void drawCustomerLocation() {
		if (cusDto == null) {
			return;
		}
		realVisiting = 0;

		clearCustomerMarkerOnMap();
		int size = cusDto.getCusList().size();
		totalCustomerInVisitPlan = size;
		ArrayList<LatLng> arrayList = new ArrayList<LatLng>();
		for (int i = 0; i < size; i++) {
			CustomerListItem cus = cusDto.getCusList().get(i);
			if (cus.aCustomer.getLat() > 0 && cus.aCustomer.getLng() > 0) {
				if (lat <= 0 || lng <= 0) {
					lat = cus.aCustomer.getLat();
					lng = cus.aCustomer.getLng();
					setMapViewLocation(lat, lng);
				}

				// neu show cac ngay khac hom nay thi marker toan mau xanh
				if (!getVisitPlan(
						arrLineChoose[spiner.getSelectedItemPosition()])
						.equals(Constants.getDayLine()[(DateUtils.getCurrentDay())])) {
					showMarkerInMap(cus.aCustomer.getLat(),
							cus.aCustomer.getLng(), i, 0, cus.seqInDayPlan,
							R.drawable.icon_circle_green, BaseFragmentMapView.MarkerAnchor.CENTER);
				} else {
					if (cus.isOr == 1) {
						totalCustomerInVisitPlan--;
						showMarkerInMap(cus.aCustomer.getLat(),
								cus.aCustomer.getLng(), i, cus.isOr,
								cus.seqInDayPlan, R.drawable.icon_circle_yellow, BaseFragmentMapView.MarkerAnchor.CENTER);
					}// da (ghe tham hoac dong cua) va co don hang
					else if ((cus.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_CLOSED || cus.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_FINISHED)
							&& cus.isTodayOrdered) {
						showMarkerInMap(cus.aCustomer.getLat(),
								cus.aCustomer.getLng(), i, cus.isOr,
								cus.seqInDayPlan, R.drawable.icon_circle_blue, BaseFragmentMapView.MarkerAnchor.CENTER);
					} else if ((cus.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_CLOSED || cus.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_FINISHED)
							&& !cus.isTodayOrdered) {
						// da ghe tham hoac ghe tham dong cua va ko co don hang
						showMarkerInMap(cus.aCustomer.getLat(),
								cus.aCustomer.getLng(), i, cus.isOr,
								cus.seqInDayPlan, R.drawable.icon_circle_red, BaseFragmentMapView.MarkerAnchor.CENTER);
					} else if (cus.visitStatus == CustomerListItem.VISIT_STATUS.VISITING) {
						// dang ghe tham
						showMarkerInMap(cus.aCustomer.getLat(),
								cus.aCustomer.getLng(), i, cus.isOr,
								cus.seqInDayPlan, R.drawable.icon_circle_orange, BaseFragmentMapView.MarkerAnchor.CENTER);
					} else {
						// chua ghe tham
						showMarkerInMap(cus.aCustomer.getLat(),
								cus.aCustomer.getLng(), i, cus.isOr,
								cus.seqInDayPlan, R.drawable.icon_circle_green, BaseFragmentMapView.MarkerAnchor.CENTER);
					}

					// toa do ghe tham cua nvbh voi khach hang
					// if (cus.isOr == 1) {
					// showVisitedOfSaleMan(cus, cusDto.distance, i, 1);
					// } else {
					// showVisitedOfSaleMan(cus, cusDto.distance, i, 0);
					// }

					drawVisitedInfo(cus, cusDto.getDistance(), i, cus.isOr);
				}
				arrayList.add(new LatLng(cus.aCustomer.getLat(), cus.aCustomer
						.getLng()));
			}
		}
		if (latNVBH > 0 && lngNVBH > 0) {
			drawMarkerMyPosition(latNVBH, lngNVBH);
		}
		
		if (shopInfo != null && shopInfo.shopLat > 0 && shopInfo.shopLng > 0) {
			drawShopPosition();
		}
		fitBounds(arrayList);
	}

	/**
	 * Ve thong tin ghe tham khach hang (Thu tu thuc te ghe tham, ma kh, thoi
	 * gian bat dau ghe tham)
	 *
	 * @author: BANGHN
	 * @param dto
	 * @param dis
	 * @param index
	 * @param isOrWithSelectedDay
	 */
	private void drawVisitedInfo(CustomerListItem dto, double dis, int index,
			int isOrWithSelectedDay) {
		if (!StringUtil.isNullOrEmpty(dto.visitStartTime)) {
			OverlayViewOptions opts = new OverlayViewOptions();
			String formate = "";
			if (realVisiting == 0) {
				realVisiting = 1;
			} else {
				realVisiting++;
			}
			formate = (realVisiting) + ".(" + dto.aCustomer.customerCode
					+ "): " + dto.visitStartTimeShort;

			Rect bounds = new Rect();
			Paint textPaint = new TextView(parent).getPaint();
			textPaint.getTextBounds(formate.toString(), 0, formate.toString()
					.length(), bounds);
			final int width = bounds.width();
			TextView text = new TextView(parent) {
				@Override
				protected void onMeasure(int widthMeasureSpec,
						int heightMeasureSpec) {
					super.onMeasure(width, heightMeasureSpec);
				}
			};
			text.setLayoutParams(new ViewGroup.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			text.setText(formate);
			Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);
			text.setTextColor(ImageUtil.getColor(R.color.RED));
			text.setTypeface(tf);
			text.setTextSize(16);
			opts = new OverlayViewOptions();
			opts.position(new com.viettel.maps.base.LatLng(dto.aCustomer
					.getLat(), dto.aCustomer.getLng()));
			opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
			opts.offsetHeight(22);
			clearPopup();
			addTextMarker(text, opts);
//			showPopupWhenClickMarker(text, opts);
//			customerLayer.addItemObj(text, opts);
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void clearCustomerMarkerOnMap() {
		clearPopup();
		clearAllMarkerOnMap();
	}

	/**
	 * Hien thi toa do nvbh so voi khach hang & khoang cach chenh lech
	 *
	 * @author : BangHN since : 1.0
	 */
	// private void showVisitedOfSaleMan(CustomerListItem dto, double dis, int
	// index, int isOrWithSelectedDay) {
	// if (dto.visitedLat <= 0 || dto.visitedLng <= 0 || parent == null) {
	// return;
	// }
	// // MarkerWithTextBelow marker;
	// // com.viettel.vinamilk.util.LatLng cus = new
	// // com.viettel.vinamilk.util.LatLng(dto.aCustomer.getLat(),
	// // dto.aCustomer.getLng());
	// // com.viettel.vinamilk.util.LatLng sale = new
	// // com.viettel.vinamilk.util.LatLng(dto.visitedLat, dto.visitedLng);
	// // double distance = GlobalUtil.getDistanceBetween(cus, sale);
	// // if (distance <= dis) {// be hon 300m
	// // marker = new MarkerWithTextBelow(parent, R.drawable.icon_flag_blue);
	// // } else {
	// // marker = new MarkerWithTextBelow(parent, R.drawable.icon_flag_red);
	// // }
	// // marker.setListener(this, ACTION_CLICK_MARKER_CUSTOMER, new int[] {
	// // index, isOrWithSelectedDay });
	// // marker.updateSaleVitsitedInfo(distance, index, dto);
	// //
	// // OverlayViewOptions opts = new OverlayViewOptions();
	// // opts.position(new com.viettel.maps.base.LatLng(dto.visitedLat,
	// // dto.visitedLng));
	// // opts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
	// // opts.offsetHeight(25);
	// // customerLayer.addItemObj(marker, opts);
	//
	// TextMarkerMapView marker;
	// com.viettel.vinamilk.util.LatLng cus = new
	// com.viettel.vinamilk.util.LatLng(dto.aCustomer.getLat(),
	// dto.aCustomer.getLng());
	// com.viettel.vinamilk.util.LatLng sale = new
	// com.viettel.vinamilk.util.LatLng(dto.visitedLat, dto.visitedLng);
	// double distance = GlobalUtil.getDistanceBetween(cus, sale);
	// if (distance <= dis) {// be hon 300m
	// marker = new TextMarkerMapView(parent, R.drawable.icon_flag_blue, "");
	// } else {
	// marker = new TextMarkerMapView(parent, R.drawable.icon_flag_red, "");
	// }
	// marker.setListener(this, ACTION_CLICK_FLAG_CUSTOMER, new int[] { index,
	// isOrWithSelectedDay });
	// OverlayViewOptions opts = new OverlayViewOptions();
	// opts.position(new LatLng(dto.visitedLat, dto.visitedLng));
	// opts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
	// customerLayer.addItemObj(marker, opts);
	//
	// String formate = "";
	// if (distance >= 1000) {
	// float tempDistance = (float) distance / 1000;
	// DecimalFormat df = new DecimalFormat("#.##");
	// formate = df.format(tempDistance);
	// formate = "(" + dto.aCustomer.customerCode + "): " + formate + " km";
	// } else {
	// DecimalFormat df = new DecimalFormat("#.##");
	// formate = df.format(distance);
	// formate = "(" + dto.aCustomer.customerCode + "): " + formate + " m";
	// }
	//
	// Rect bounds = new Rect();
	// Paint textPaint = new TextView(parent).getPaint();
	// textPaint.getTextBounds(formate.toString(), 0,
	// formate.toString().length(), bounds);
	// final int width = bounds.width();
	// TextView text = new TextView(parent) {
	// @Override
	// protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	// super.onMeasure(width, heightMeasureSpec);
	// }
	// };
	// text.setLayoutParams(new
	// ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
	// LayoutParams.WRAP_CONTENT));
	// text.setText(formate);
	// text.setTextColor(ImageUtil.getColor(R.color.RED));
	// opts = new OverlayViewOptions();
	// opts.position(new LatLng(dto.visitedLat, dto.visitedLng));
	// opts.drawMode(OverlayViewItemObj.DRAW_CENTER);
	// opts.offsetHeight(10);
	// customerLayer.addItemObj(text, opts);
	// }

	/**
	 * Hien thi marker toa do hien tai
	 *
	 * @author : BangHN since : 1.0
	 */
	private void showMarkerInMap(double lat, double lng, int index,
			int isOrWithSelectedDay, String seqInPlan, int drawable, BaseFragmentMapView.MarkerAnchor anchorConfig) {
		String seq = "";
		if (isOrWithSelectedDay == 0) {
			if (StringUtil.isNullOrEmpty(seqInPlan)) {
				seq = "0";
			} else {
				seq = seqInPlan;
			}
		} else {
			seq = "!";
		}

		if (parent != null){
			Object obj = addMarkerToMap(lat, lng, drawable, anchorConfig, seq, "", 0, 0);
			super.setMarkerOnClickListener(obj, this, ACTION_CLICK_MARKER_CUSTOMER, new int[] { index, isOrWithSelectedDay } );
		}
	}

	/**
	 * di toi fragment danh sach don hang
	 *
	 * @author : BangHN since : 10:43:27 AM
	 */
	private void gotoListOrder() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER, UserController.getInstance());
	}

	/**
	 * danh sach khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void gotoCustomerList() {
		handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
	}

	/**
	 * di toi man hinh chi tiet Khach hang
	 *
	 * @author : TamPQ since : 10:59:39 AM
	 */
	private void gotoCustomerInfo(String customerId) {
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleSwitchFragment(bunde, ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	/**
	 * Ghe tham khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void visitCustomer(CustomerListItem item) {
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		CustomerListItem lastVisitCustomer = GlobalInfo.getInstance()
				.getProfile().getLastVisitCustomer();
		if (lastVisitCustomer != null
				&& action != null
				&& lastVisitCustomer.aCustomer.customerId != item.aCustomer.customerId
				&& lastVisitCustomer.isHaveDisplayProgramNotYetVote
				&& lastVisitCustomer.visitStatus != CustomerListItem.VISIT_STATUS.VISITED_CLOSED
				&& lastVisitCustomer.visitStatus != CustomerListItem.VISIT_STATUS.VISITED_FINISHED) {
			SpannableObject span = new SpannableObject();
			span.addSpan(StringUtil
					.getString(R.string.TEXT_REQUIRE_VOTE_DISPLAY_OLD_CUSTOMER));
			if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
				span.addSpan(
						" " + action.aCustomer.customerCode,
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
			}
			span.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.BOLD);
			if (!StringUtil.isNullOrEmpty(action.aCustomer.customerName)) {
				span.addSpan(action.aCustomer.customerName,
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
			}
			parent.showDialog(span.getSpan());
		} else if (action != null
				&& action.aCustomer.customerId != item.aCustomer.customerId
				&& DateUtils.compareDate(action.startTime, DateUtils.now()) == 0
				&& StringUtil.isNullOrEmpty(action.endTime)) {
			// ket thuc ghe tham
			// kiem tra neu vao lai dung khach hang do thi khong insertlog
			if (action.isOr == 1) {
				parent.requestUpdateActionLog("0", "0", item, this);
			} else {
				String endTime = DateUtils.getVisitEndTime(action.aCustomer);
				SpannableObject textConfirmed = new SpannableObject();
				textConfirmed.addSpan(
						StringUtil.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
					textConfirmed.addSpan(
							" " + action.aCustomer.customerCode,
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
				}
				textConfirmed.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				if (!StringUtil.isNullOrEmpty(action.aCustomer.customerName)) {
					textConfirmed.addSpan(action.aCustomer.customerName,
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
				}
				textConfirmed.addSpan(" "+StringUtil.getString(R.string.TEXT_IN)+" ", ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				textConfirmed.addSpan(
						DateUtils.getVisitTime(action.startTime, endTime),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(
						StringUtil.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);

				GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this, parent,
						textConfirmed.getSpan(),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_OK,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
						ACTION_CANCEL, item, false, false);
			}
		} else {
			if (!StringUtil.isNullOrEmpty(item.draftSaleOrderId)) {
				gotoViewOrder(item.draftSaleOrderId, item.aCustomer);
			} else {
				processOrder(item);
			}
		}
	}

	/**
	 * Den man hinh tao don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoViewOrder(String orderId, CustomerDTO cus) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(cus.customerId));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, cus.customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				cus.getStreet());
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID, cus.deliverID);
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				cus.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				cus.getCustomerName());
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				cus.getCustomerTypeId());
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_EDIT);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * Di toi kiem hang ton
	 *
	 * @author : BangHN since : 1.0
	 */
	private void gotoRemainProductView(CustomerListItem item) {
		String customerId = item.aCustomer.getCustomerId();
		String customerCode = item.aCustomer.getCustomerCode();
		String customerName = item.aCustomer.getCustomerName();
		String customerAddress = item.aCustomer.getStreet();
		int customerTypeId = item.aCustomer.getCustomerTypeId();
		int isOr = item.isOr;
		String cashierID = item.aCustomer.cashierStaffID;
		String deliveryID = item.aCustomer.deliverID;

		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliveryID);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, item);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_REMAIN_PRODUCT_VIEW, SaleController.getInstance());
	}

	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoVoteDisplayPresentProduct(CustomerListItem listItem) {
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, listItem);

		handleSwitchFragment(b, ActionEventConstant.GOTO_VOTE_DISPLAY_PRESENT_PRODUCT, SaleController.getInstance());
	}

	/**
	 * Chuyen den man hinh dat hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoCreateOrder(CustomerListItem dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				dto.aCustomer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				dto.aCustomer.getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				dto.aCustomer.getStreet());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE,
				dto.aCustomer.getCustomerCode());
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID,
				dto.aCustomer.deliverID);
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				dto.aCustomer.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "0");
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				dto.aCustomer.getCustomerTypeId());
		bundle.putSerializable(IntentConstants.INTENT_SUGGEST_ORDER_LIST, null);
		bundle.putString(IntentConstants.INTENT_IS_OR, String.valueOf(dto.isOr));
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_NEW);
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, dto.routingId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	// /**
	// * K tra lai khoang cach de cham trung bay, kiem ton
	// *
	// * @author: TamPQ
	// * @param item
	// * @return: voidvoid
	// * @throws:
	// */
	// private boolean
	// isValidDistanceToVoteDisplayProgAndCheckRemain(CustomerListItem item) {
	// item.updateCustomerDistance(cusDto.distance);
	// return !item.isTooFarShop;
	// }

	/**
	 * Thuc hien ghe tham, dat hang,....
	 *
	 * @author : BangHN since : 1.0
	 */
	private void processOrder(CustomerListItem item) {
		if (item.isOr == 1) {
			item.isHaveDisplayProgramNotYetVote = false;
		}
		GlobalInfo.getInstance().getProfile().setLastVisitCustomer(item);
		if (item.isOr == 0) {// KH trong tuyen

			// neu co chuong trinh trung bay chua cham
//			if (item.visitStatus != VISIT_STATUS.VISITED_CLOSED
//					&& item.isHaveDisplayProgramNotYetVote) {
			if (item.isHaveDisplayProgramNotYetVote && isValidDistanceToVoteDisplayProgAndCheckRemain(item)) {
				gotoVoteDisplayPresentProduct(item);
			} else if (item.isHaveEquip && isValidDistanceToVoteDisplayProgAndCheckRemain(item)) {
				gotoTakePhotoEquipment(item);
			}else if(item.isExistKeyShop){
				goToVoteKeyShop(item);
			} else if (item.isHaveSaleOrder) {
				// neu co don dat hang & chua kiem hang ton
				gotoRemainProductView(item);
			} else {
				gotoCreateOrder(item);
			}

			if (item.visitStatus != CustomerListItem.VISIT_STATUS.VISITED_FINISHED) {
				if (item.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_CLOSED) {
					saveLastVisitToActionLogProfile(item);
					parent.initCustomerNameOnMenuVisit(
							item.aCustomer.customerCode,
							item.aCustomer.customerName);
				} else if (item.visitStatus == CustomerListItem.VISIT_STATUS.VISITING) {
					saveLastVisitToActionLogProfile(item);
					parent.initMenuVisitCustomer(item.aCustomer.customerCode,
							item.aCustomer.customerName);
					if (item.isTodayCheckedRemain || item.isTodayVoted
							|| item.isTodayOrdered) {
						// da ghe tham, da dat hang, da cham trung bay, da kiem
						// hang
						// ton trong ngay thi an menu dong cua
						parent.removeMenuCloseCustomer();
					}
				} else {// VISIT_STATUS.NONE_VISIT
					parent.initMenuVisitCustomer(item.aCustomer.customerCode,
							item.aCustomer.customerName);
					parent.requestStartInsertVisitActionLog(item);
					item.visitStatus = CustomerListItem.VISIT_STATUS.VISITING;
				}
			} else {// neu da ghe tham thi chi hien thi title Dang ghe tham
				parent.initCustomerNameOnMenuVisit(item.aCustomer.customerCode,
						item.aCustomer.customerName);
				saveLastVisitToActionLogProfile(item);
			}

			// check to update EXCEPTION_ORDER_DATE: bien cho phep dat hang khi
			// khoach cach qua xa
			if (!StringUtil.isNullOrEmpty(item.exceptionOrderDate)) {
				requestUpdateExceptionOrderDate(item);
			}
		} else {// KH ngoai tuyen
			//Neu co don hang lich su thi kiem ton
			//Ngoai tuyen cung kiem ton
			if (item.isHaveSaleOrder) {
				gotoRemainProductView(item);
			} else {
				gotoCreateOrder(item);
			}

			if (item.visitStatus == CustomerListItem.VISIT_STATUS.VISITED_FINISHED) {
				parent.initCustomerNameOnMenuVisit(item.aCustomer.customerCode,
						item.aCustomer.customerName);
			} else {
				parent.initMenuVisitCustomer(item.aCustomer.customerCode,
						item.aCustomer.customerName);

			}
			parent.removeMenuCloseCustomer();
			parent.removeMenuFinishCustomer();

			if (item.visitStatus == CustomerListItem.VISIT_STATUS.NONE_VISIT) {
				parent.requestStartInsertVisitActionLog(item);
				item.visitStatus = CustomerListItem.VISIT_STATUS.VISITING;
			} else {
				saveLastVisitToActionLogProfile(item);
			}

			// ActionLogDTO action =
			// GlobalInfo.getInstance().getProfile().getActionLogVisitCustomer();
			// if (action == null
			// || (!StringUtil.isNullOrEmpty(action.startTime) &&
			// DateUtils.isCompareWithToDate(action.startTime) != 0)
			// || !(action.aCustomer.customerId == item.aCustomer.customerId)) {
			//
			// } else {
			// saveLastVisitToActionLogProfile(item);
			// }
		}
	}

	/**
	 * //check to update EXCEPTION_ORDER_DATE: bien cho phep dat hang khi khoach
	 * cach qua xa
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void requestUpdateExceptionOrderDate(CustomerListItem item) {
		StaffCustomerDTO staffCusDto = new StaffCustomerDTO();
		staffCusDto.staffCustomerId = item.staffCustomerId;

		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_STAFF_DTO, staffCusDto);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.action = ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE;
		e.viewData = data;
		e.isNeedCheckTimeServer = false;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param item
	 * @return: voidvoid
	 * @throws:
	 */
	private void saveLastVisitToActionLogProfile(CustomerListItem item) {
		ActionLogDTO action = new ActionLogDTO();
		// chu y ko co id cua action_log
		action.id = item.visitActLogId;
		action.aCustomer.customerId = item.aCustomer.customerId;
		action.aCustomer.customerName = item.aCustomer.customerName;
		action.aCustomer.customerCode = item.aCustomer.customerCode;
		action.aCustomer.lat = item.aCustomer.lat;
		action.aCustomer.lng = item.aCustomer.lng;
		action.aCustomer.shopDTO.distanceOrder = item.distanceOrder;
		action.startTime = item.visitStartTime;
		action.endTime = item.visitEndTime;
		action.isOr = item.isOr;
		action.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		action.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLastLatitude();
		action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLastLongtitude();
		// action.objectId = item.visit;
		action.objectType = "0";
		action.routingId = item.routingId;
		GlobalInfo.getInstance().getProfile().setActionLogVisitCustomer(action);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_CUSTOMER_LIST_FOR_ROUTE:
			ArrayList<Object> data = (ArrayList<Object>) modelEvent
					.getModelData();
			cusDto = (CustomerListDTO) data.get(0);
			drawCustomerLocation();
			break;
		case ActionEventConstant.UPDATE_ACTION_LOG:
			GlobalInfo.getInstance().getProfile()
					.setActionLogVisitCustomer((ActionLogDTO) e.viewData);
			processOrder((CustomerListItem) e.userData);
			break;
		case ActionEventConstant.GET_SHOP_INFO:
			shopInfo = (ShopDTO) modelEvent.getModelData();
			// vi tri NPP
//			double shopLat = GlobalInfo.getInstance().getProfile().getUserData().inheritShopLat;
//			double shopLng = GlobalInfo.getInstance().getProfile().getUserData().inheritShopLng;
			drawShopPosition();
			break;
		default:
			break;
		}
		super.handleModelViewEvent(modelEvent);
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_ORDER_LIST:
			gotoListOrder();
			break;
		case ACTION_CUS_LIST:
			gotoCustomerList();
			break;
		case ACTION_CREATE_CUSTOMER:{
			gotoListCustomerCreated();
			break;
		}
		case ACTION_CLICK_MARKER_CUSTOMER:
			showCustomerInfoPopup((int[]) data, true);
			break;
		case ACTION_CLICK_FLAG_CUSTOMER:
			showCustomerInfoPopup((int[]) data, false);
			break;
		case ACTION_END_VISIT_OK:
			CustomerListItem item = (CustomerListItem) data;
			// ket thuc ghe tham
			parent.requestUpdateActionLog("0", "0", item, this);

			processOrder(item);
			break;
		case ACTION_OK:
			CustomerListItem itemCustomer = (CustomerListItem) data;
			// ket thuc ghe tham
			parent.requestUpdateActionLog("0", null, itemCustomer, this);
			break;
		case ACTION_CLICK_MARKER_SHOP:
			showShopInfo();
			break;
		case CustomerPopupView.POP_UP_CLOSE:
			clearPopup();
			break;
		case CustomerPopupView.GOTO_INFO: {
			int index = (Integer) data;
			gotoCustomerInfo(cusDto.getCusList().get(index).aCustomer
					.getCustomerId());
			break;
		}
		case ShopPopupView.POP_UP_CLOSE:
			clearPopup();
			break;
		case CustomerPopupView.GOTO_ORDER: {
			int index = (Integer) data;
			visitCustomer(cusDto.getCusList().get(index));
			break;
		}
		case ACTION_LOCK_DATE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
			break;
		case ACTION_IMAGE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			// tranh truong hop notify nhieu lan
			MyLog.i("Location", DateUtils.now()
					+ " Map: location changed: (lat-lng) "
					+ GlobalInfo.getInstance().getProfile().getMyGPSInfo()
							.getLatitude()
					+ " - "
					+ GlobalInfo.getInstance().getProfile().getMyGPSInfo()
							.getLongtitude());

			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
			if (lat < 0 && lng < 0) {
				return;
			} else {
				drawMarkerMyPosition();
			}
			break;
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				visitConfig = GlobalInfo.getInstance().getVisitConfig();
				spiner.setSelection(DateUtils.getCurrentDay());
				getCustomerInVisitPlan();
//				spiner.setSelection(DateUtils.getCurrentDay());
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg1 == null) {
			if (selectingItem != -1 && selectingItem != arg2) {
				selectingItem = arg2;
				getCustomerInVisitPlan();
			} else {
				selectingItem = arg2;
			}
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}


	/**
	 * lay thong tin shop
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	void getShopInfo() {
		Bundle b = new Bundle();
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		b.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		handleViewEvent(b, ActionEventConstant.GET_SHOP_INFO, SaleController.getInstance());
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void drawShopPosition() {
		if (shopInfo != null && shopInfo.shopLat > 0 && shopInfo.shopLng > 0) {
			Object obj = addMarkerToMap(shopInfo.shopLat, shopInfo.shopLng, R.drawable.icon_shop2, BaseFragmentMapView.MarkerAnchor.CENTER, "", "", 0, 0);
			super.setMarkerOnClickListener(obj, this, ACTION_CLICK_MARKER_SHOP, shopInfo );
		} else {
			parent.showDialog(StringUtil.getString(R.string.TEXT_ALERT_CANT_LOCATE_YOUR_SHOP_POSITION));
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
	}

	/**
	 * hien thi thong tin shop
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private View showShopInfo() {

		OverlayViewOptions opts = new OverlayViewOptions();
		opts.position(new com.viettel.maps.base.LatLng(shopInfo.shopLat, shopInfo.shopLng));
		opts.offsetHeight(-15);
		opts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_CENTER);
		ShopPopupView popupShopView = new ShopPopupView(parent, this);
		if (shopInfo != null) {
			popupShopView.setInfo(shopInfo.shopName, shopInfo.shopCode, shopInfo.street);
		}
		showPopupWhenClickMarker(popupShopView, opts);
		return popupShopView;
//		clearMapLayer(shopLayer);
//		shopLayer.addItemObj(popupShopView, opts);
	}

	/**
	 * Toi man hinh them moi KH
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void gotoListCustomerCreated() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
	}

	@Override
	public View onEventMap(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_CLICK_MARKER_SHOP:
			return showShopInfo();
		case ACTION_CLICK_MARKER_CUSTOMER:
			return showCustomerInfoPopup((int[]) data, true);
		case ACTION_CLICK_FLAG_CUSTOMER:
			return showCustomerInfoPopup((int[]) data, false);
		case ACTION_LOCK_DATE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
			break;
		case ACTION_IMAGE:
			handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
			break;
		default:
			break;
		}
		return null;
	}

	/**
	 * Toi man hinh cham key shop
	 *
	 * @author: Tuanlt11
	 * @param item
	 * @return: void
	 * @throws:
	 */
	private void goToVoteKeyShop(CustomerListItem item) {
		String customerId = item.aCustomer.getCustomerId();
		String customerCode = item.aCustomer.getCustomerCode();
		String customerName = item.aCustomer.getCustomerName();
		String customerAddress = item.aCustomer.getStreet();
		int customerTypeId = item.aCustomer.getCustomerTypeId();
		int isOr = item.isOr;
		String cashierID = item.aCustomer.cashierStaffID;
		String deliverID = item.aCustomer.deliverID;
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliverID);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, item);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_VOTE_KEY_SHOP,
				SaleController.getInstance());
	}
	
	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoTakePhotoEquipment(CustomerListItem listItem) {
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, listItem);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_TAKE_PHOTO_EQUIPMENT, SaleController.getInstance());
	}
	
	/**
	 * K tra lai khoang cach de cham trung bay, kiem ton
	 *
	 * @author: TamPQ
	 * @param item
	 * @return: voidvoid
	 * @throws:
	 */
	private boolean isValidDistanceToVoteDisplayProgAndCheckRemain(
			CustomerListItem item) {
		item.updateCustomerDistance(cusDto.getDistance());
		return !item.isTooFarShop;
	}
}
