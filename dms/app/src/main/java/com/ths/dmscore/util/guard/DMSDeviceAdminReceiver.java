package com.ths.dmscore.util.guard;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class DMSDeviceAdminReceiver extends DeviceAdminReceiver {
	@Override
	public void onEnabled(Context context, Intent intent) {
		super.onEnabled(context, intent);
		//GlobalUtil.showToast("onEnabled");
	}

	@Override
	public void onDisabled(Context context, Intent intent) {
		super.onDisabled(context, intent);
		//GlobalUtil.showToast("onDisabled");
	}
	
	@Override
	public CharSequence onDisableRequested(final Context context, Intent intent) {
		//GlobalUtil.showToast("onDisableRequested");
//		Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
//		context.sendBroadcast(closeDialog);
		return super.onDisableRequested(context, intent);
	}
}
