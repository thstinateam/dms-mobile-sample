/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.DEBIT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Thong tin cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class DebitDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 167048507038013005L;
	// id
	public long id;
	//
	public String objectID;
	//
	public String objectType;
	//
	public double totalAmount;
	public double totalDiscount;
	//
	public double totalPay;
	public double totalDebit;
	public long maxDebitAmount;
	public String maxDebitDate;
	
	// so ngay no cua khach hang tinh tu ngay dat hang xa nhat
	public int maxDebitDays;
	
//	public long staffID;

	public DebitDTO() {
		super(TableType.DEBIT_TABLE);
	}

	/**
	 * Generate cau lenh insertOrupdate
	 * 
	 * @author: TruongHN
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateInsertFromOrderSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, DEBIT_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			// staff_customer_id: id tu tang tren server, va khong cap nhat khi
			// update
			params.put(GlobalUtil.getJsonColumnWithKey(DEBIT_TABLE.DEBIT_ID, id, null));
			params.put(GlobalUtil.getJsonColumnWithKey(DEBIT_TABLE.OBJECT_ID, objectID, null));
			params.put(GlobalUtil.getJsonColumnWithKey(DEBIT_TABLE.OBJECT_TYPE, objectType, null));

			json.put(IntentConstants.INTENT_LIST_PARAM, params);

		} catch (JSONException e) {
		}
		return json;
	}

	public void initFromCursor(Cursor c){
		id =  CursorUtil.getLong(c, DEBIT_TABLE.DEBIT_ID);
		objectID = CursorUtil.getString(c, DEBIT_TABLE.OBJECT_ID);
		objectType = CursorUtil.getString(c, DEBIT_TABLE.OBJECT_TYPE);
		totalAmount = CursorUtil.getLong(c, DEBIT_TABLE.TOTAL_AMOUNT);
		totalDiscount = CursorUtil.getLong(c, DEBIT_TABLE.TOTAL_DISCOUNT);
		totalPay = CursorUtil.getLong(c, DEBIT_TABLE.TOTAL_PAY);
		totalDebit = CursorUtil.getLong(c, DEBIT_TABLE.TOTAL_DEBIT);
		maxDebitAmount = CursorUtil.getLong(c, DEBIT_TABLE.MAX_DEBIT_AMOUNT);
		maxDebitDate = CursorUtil.getString(c, DEBIT_TABLE.MAX_DEBIT_DATE);
	}
	
	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param debitJsonParam
	 * @return
	 * @throws JSONException
	 * @return: DebitDTO
	 * @throws:
	*/
	public static DebitDTO createDebitFromJson(JSONArray debitJsonParam) throws JSONException {
		DebitDTO debit = new DebitDTO();

		for (int i = 0; i < debitJsonParam.length(); i++) {
			JSONObject object = debitJsonParam.getJSONObject(i);
			if (object.getString("column").equals("OBJECT_ID")) {
				debit.objectID = object.getString("value");
			}
			if (object.getString("column").equals("OBJECT_TYPE")) {
				debit.objectType = object.getString("value");
			}
			if (object.getString("column").equals("TOTAL_PAY")) {
				debit.totalPay = getLongFromJsonValue(object.getString("value"));
			}
			if (object.getString("column").equals("TOTAL_AMOUNT")) {
				debit.totalAmount = getLongFromJsonValue(object.getString("value"));
			}
			if (object.getString("column").equals("TOTAL_DEBIT")) {
				debit.totalDebit = getLongFromJsonValue(object.getString("value"));
			}
		}
		return debit;
	}

	private static long getLongFromJsonValue(String value){
		long x = 0;
		long factor = 1;
		if(value.contains("*+")){
			value = value.substring(2);
			if(value.contains("+")){
				value = value.substring(1);
			} else if(value.contains("-")){
				value = value.substring(1);
				factor = -1;
			}
		} else if(value.contains("*-")){
			value = value.substring(2);
			if(value.contains("+")){
				value = value.substring(1);
			} else if(value.contains("-")){
				value = value.substring(1);
				factor = -1;
			}
		}
		
		x = Long.parseLong(value) * factor;
		
		return x;
	}
}
