/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import java.util.ArrayList;

import android.graphics.Point;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO;
import com.ths.dmscore.dto.view.NewCustomerItem;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.map.FragmentMapView;
import com.ths.map.OverlayViewItemObj;
import com.ths.map.OverlayViewLayer;
import com.ths.map.OverlayViewOptions;
import com.ths.map.view.MarkerMapView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;
import com.viettel.maps.base.LatLng;

/**
 * CreateNewCustomerView.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 1:46:46 PM Sep 16, 2014
 */
public class NewCustomerCreateView extends FragmentMapView
		implements OnTouchListener {

	public static final int REQUEST_EDIT_CUSTOMER = -1;
	public static final int REQUEST_CREATE_CUSTOMER = -2;

	private static final int ACTION_INSERT_CUSTOMER = -1;

	private static final int ACTION_UPDATE_CUSTOMER = -2;

	private static final int ACTION_CANCEL_CUSTOMER = -3;

	private static final int REQUEST_VIEW_CUSTOMER = -4;

	private final int ACTION_ORDER_LIST = 1;
	private final int ACTION_PATH = 2;
	private final int ACTION_CUS_LIST = 3;
	private final int ACTION_CREATE_CUSTOMER = 9;

	Bundle savedInstanceState;
	// thong tin khach hang tu man hinh khac gui toi
	CreateCustomerInfoDTO dto = null;
	// CustomerDTO customerInfo = null;
	NewCustomerCreateHeaderView header;
	MarkerMapView maker;
	private int typeRequest = REQUEST_CREATE_CUSTOMER;
	// toa do khach hang
	double cusLat = -1, cusLng = -1;
	// toa do nhan vien
	double staffLat, staffLng;
	// vi tri nhan vien
	MarkerMapView makerPositionStaff;

	OverlayViewOptions markerCustomerOpts = new OverlayViewOptions();
	MarkerMapView markerCustomer;
	OverlayViewLayer markerCustomerLayer;

	// user info
	// user id
	private String userId;
	// shopID
	private String shopId;
	// customer id
	private String customerId;
	// user name
	private String userName;

	public static NewCustomerCreateView newInstance(Bundle data) {
		NewCustomerCreateView f = new NewCustomerCreateView();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
		View v = super.onCreateView(inflater, container, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_THEMMOIKH_THEMMOI);
		hideHeaderview();
		initUserInfo();
		initHeaderText();
		initData();

		return v;
	}

	/**
	 * lay thong tin nhan vien hien dang dang nhap + thong tin man hinh khac goi
	 * qua
	 *
	 * @author: duongdt3
	 * @since: 09:05:05 6 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void initUserInfo() {
		userId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		userName = GlobalInfo.getInstance().getProfile().getUserData().getUserName();
		shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();

		customerId = getArguments().getString(IntentConstants.INTENT_CUSTOMER_ID, "");
		// co truyen qua ID => can cap nhat, mac
		if (!StringUtil.isNullOrEmpty(customerId)) {
			boolean isEdit = getArguments().getBoolean(IntentConstants.INTENT_IS_EDIT, false);
			if (isEdit) {
				typeRequest = REQUEST_EDIT_CUSTOMER;
			} else {
				typeRequest = REQUEST_VIEW_CUSTOMER;
			}
		}
	}

	/**
	 * show header text
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initHeaderText() {
		// TODO Auto-generated method stub
		String title = StringUtil.getString(R.string.TITLE_VIEW_CREATE_CUSTOMER);
		parent.setTitleName(title);
	}

	/**
	 * Go to list khach hang chua duyet
	 *
	 * @author: duongdt3
	 * @since: 20:28:45 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void goToNewCustomerList() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
	}

	/**
	 * init data
	 *
	 * @author: duongdt3
	 * @since: 16:44:08 4 Jan 2014
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("unchecked")
	private void initData() {
		// vi tri can update tren ban do
		markerCustomer = new MarkerMapView(parent, R.drawable.icon_flag);
		markerCustomer.setVisibility(View.GONE);
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
		markerCustomerLayer = new OverlayViewLayer();
		mapView.addLayer(markerCustomerLayer);
		markerCustomerLayer.addItemObj(markerCustomer, markerCustomerOpts);
		showFlagMarkerInMap(cusLat, cusLng);
		drawMarkerMyPosition();

		setCustomerLocationHeader();
		// request data
		requestData();
	}

	/**
	 * get data on db
	 *
	 * @author: duongdt3
	 * @since: 09:07:03 6 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void requestData() {
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID, userId);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEvent(data, ActionEventConstant.GET_CREATE_CUSTOMER_INFO, SaleController.getInstance());
	}

	/**
	 * lay danh sach du lieu dia ban voi id dia ban cha + loai dia ban
	 *
	 * @author: duongdt3
	 * @since: 15:55:46 6 Jan 2014
	 * @return: void
	 * @throws:
	 * @param type
	 * @param parentAreaId
	 */
	protected void requestDataArea(int type, long parentAreaId) {
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putInt(IntentConstants.INTENT_AREA_TYPE, type);
		data.putLong(IntentConstants.INTENT_PARRENT_AREA_ID, parentAreaId);
		handleViewEvent(data, ActionEventConstant.GET_CREATE_CUSTOMER_AREA_INFO, SaleController.getInstance());
	}

	/**
	 * Hien thi header thong tin khach hang tren ban do
	 *
	 * @author: duongdt3
	 * @since: 16:44:22 4 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void setCustomerLocationHeader() {
		if (header == null) {
			// neu ko phai la View mode thi cho chinh sua
			header = new NewCustomerCreateHeaderView(parent, (typeRequest != REQUEST_VIEW_CUSTOMER));
			header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			header.setOnTouchListener(this);
			header.setParrent(this);

			header.btSave.setOnClickListener(this);
			setHeaderView(header);
		}
	}

	/**
	 * Hien thi marker khach hang
	 *
	 * @author: duongdt3
	 * @since: 16:44:29 4 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void showCustomerPositionMarker() {
		markerCustomerOpts.position(new LatLng(cusLat, cusLng));
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_CENTER);
		markerCustomer.setIconMarker(R.drawable.icon_flag);
		mapView.invalidate();
	}

	/**
	 * Hien thi marker toa do hien tai
	 *
	 * @author: duongdt3
	 * @since: 16:44:40 4 Jan 2014
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	private void showFlagMarkerInMap(double lat, double lng) {
		markerCustomerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
		markerCustomerOpts.position(new LatLng(lat, lng));
		// chi hien khi co vi tri
		if (lat > 0 && lng > 0) {
			// hien thi marker cua khach hang
			markerCustomer.setVisibility(View.VISIBLE);

		} else {
			// khong co vi tri thi an
			markerCustomer.setVisibility(View.GONE);
		}
		mapView.invalidate();
	}

	/**
	 * Cap nhat thong tin khach hang
	 *
	 * @author: duongdt3
	 * @since: 17:17:34 4 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void sendRequestInsertCustomerToDB() {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_CUSTOMER, dto.cusInfo);
		handleViewEvent(data, ActionEventConstant.ACTION_CREATE_CUSTOMER, SaleController.getInstance());
	}

	/**
	 * cap nhat thong tin khach hang
	 *
	 * @author: duongdt3
	 * @since: 14:37:10 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void sendRequestUpdateCustomerToDB() {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_CUSTOMER, dto.cusInfo);
		handleViewEvent(data, ActionEventConstant.ACTION_CREATE_CUSTOMER, SaleController.getInstance());
	}

	/**
	 * Hoi khi tao khach hang
	 *
	 * @author: duongdt3
	 * @param info
	 * @since: 14:41:34 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmCreateCustomer() {
		GlobalUtil.showDialogConfirm(this, this.parent, StringUtil.getString(R.string.TEXT_NOTIFY_ACTION_CREATE_CUSTOMER), StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_INSERT_CUSTOMER, StringUtil.getString(R.string.TEXT_BUTTON_DENY),
				ACTION_CANCEL_CUSTOMER, null);
	}

	/**
	 * Hoi khi cap nhat khach hang
	 *
	 * @author: duongdt3
	 * @param info
	 * @since: 14:41:34 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmUpdateCustomer() {
		GlobalUtil.showDialogConfirm(this, this.parent, StringUtil.getString(R.string.TEXT_NOTIFY_ACTION_UPDATE_CUSTOMER), StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_UPDATE_CUSTOMER, StringUtil.getString(R.string.TEXT_BUTTON_DENY),
				ACTION_CANCEL_CUSTOMER, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
			case ActionEventConstant.GET_CREATE_CUSTOMER_INFO: {
				dto = (CreateCustomerInfoDTO) modelEvent.getModelData();
				if (dto != null) {
					if (typeRequest == REQUEST_CREATE_CUSTOMER) {
						dto.cusInfo = new CustomerDTO();
					}

					cusLat = dto.cusInfo.getLat();
					cusLng = dto.cusInfo.getLng();

					showFlagMarkerInMap(cusLat, cusLng);
					if (cusLat > 0
							&& cusLng > 0) {
						mapView.moveTo(new LatLng(cusLat, cusLng));
					} else {
						double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
						double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();

						if (lat > 0
								&& lng > 0) {
							mapView.moveTo(new LatLng(lat, lng));
						} else {
							mapView.moveTo(new LatLng(Constants.LAT_VNM_TOWNER, Constants.LNG_VNM_TOWNER));
						}
					}
					header.setCustomerInfo(dto);
				}
				break;
			}
			case ActionEventConstant.GET_CREATE_CUSTOMER_AREA_INFO: {
				ArrayList<CreateCustomerInfoDTO.AreaItem> arrayArea = (ArrayList<CreateCustomerInfoDTO.AreaItem>) modelEvent.getModelData();
				int type = ((Bundle) modelEvent.getActionEvent().viewData).getInt(IntentConstants.INTENT_AREA_TYPE);
				header.setArrayAreaInfo(type, arrayArea);
				break;
			}
			case ActionEventConstant.ACTION_UPDATE_CUSTOMER: {
				parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_UPDATE_CUSTOMER_SUCCESS));
				goToNewCustomerList();
				break;
			}
			case ActionEventConstant.ACTION_CREATE_CUSTOMER: {
				parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_CREATE_CUSTOMER_SUCCESS));
				goToNewCustomerList();
				break;
			}
			default:
				super.handleModelViewEvent(modelEvent);
				break;
		}

		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 * Goi yeu cau them, cap nhat
	 *
	 * @author: duongdt3
	 * @since: 17:25:29 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void sendRequestCreateOrUpdateWhenVailInput() {
		// cap nhat gia tri cac control trong header view vao dto
		header.getInfoFromView();
		// cap nhat vi tri
		dto.cusInfo.setLat(cusLat);
		dto.cusInfo.setLng(cusLng);
		String dateNow = DateUtils.now();

		if (typeRequest == REQUEST_CREATE_CUSTOMER) {
			// tao thi them nguoi tao, ngay tao, shop_id cua NHBH hien tai,
			// status = 2 => cho xu ly
			dto.cusInfo.setShopId(shopId);
			dto.cusInfo.staffId = Long.valueOf(userId);
			dto.cusInfo.setCreateDate(dateNow);
			dto.cusInfo.setCreateUser(userName);
			dto.cusInfo.setStatus(NewCustomerItem.STATE_CUSTOMER_WAIT_APPROVED);

			// send request to DB
			sendRequestInsertCustomerToDB();
		} else if (typeRequest == REQUEST_EDIT_CUSTOMER) {
			// sua thi them nguoi sua, ngay sua
			dto.cusInfo.setUpdateDate(dateNow);
			dto.cusInfo.setUpdateUser(userName);

			// send request to DB
			sendRequestUpdateCustomerToDB();
		}
	}

	/**
	 * Check & Update customer to DB
	 *
	 * @author: duongdt3
	 * @since: 10:10:08 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void updateCustomerToDB() {
		// check dieu kien
		boolean isVaild = header.checkInputInfo();
		if (isVaild) {
			// tam thoi ko can quan tam toi vi tri
			if (typeRequest == REQUEST_CREATE_CUSTOMER) {
				confirmCreateCustomer();
			} else if (typeRequest == REQUEST_EDIT_CUSTOMER) {
				confirmUpdateCustomer();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v == header.btSave) {
			updateCustomerToDB();
		}
		super.onClick(v);
	}

	/*
	 * @Override public void onClickMarkerMyPositionEvent() { cusLat =
	 * GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
	 * cusLng =
	 * GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude(); //
	 * show la co vi tri moi //showFlagMarkerInMap(cusLat, cusLng);
	 * //mapView.moveTo(new LatLng(cusLat, cusLng));
	 * //super.onClickMarkerMyPositionEvent(); }
	 */

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
			case ACTION_INSERT_CUSTOMER:
			case ACTION_UPDATE_CUSTOMER: {
				sendRequestCreateOrUpdateWhenVailInput();
			}
				break;
			case ACTION_PATH:
				gotoCustomerRouteView();
				break;
			case ACTION_ORDER_LIST:
				gotoListOrder();
				break;
			case ACTION_CUS_LIST:
				handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
				break;
			default:
				super.onEvent(eventType, control, data);
				break;
		}
	}

	/**
	 * Toi man hinh danh sach don hang
	 *
	 * @author: dungdq3
	 * @since: 3:41:05 PM Sep 17, 2014
	 * @return: void
	 * @throws: :
	 */
	private void gotoListOrder() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER, SaleController.getInstance());
	}

	/**
	 * Toi man hinh lo trinh
	 *
	 * @author: dungdq3
	 * @since: 3:40:06 PM Sep 17, 2014
	 * @return: void
	 * @throws: :
	 */
	private void gotoCustomerRouteView() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_CUSTOMER_ROUTE, SaleController.getInstance());
	}

	/**
	 * Di toi man hinh danh gia nhan vien Danh cho module gs npp
	 *
	 * @author banghn
	 * @param item
	 */
	public void gotoReviewsStaffView() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_REVIEWS_STAFF_VIEW, SupervisorController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
			case ActionEventConstant.NOTIFY_REFRESH_VIEW:
				if (this.isVisible()) {
					this.cusLat = -1;
					this.cusLng = -1;
					initData();
					// setCustomerLocationHeader();
					showCustomerPositionMarker();
				}
				break;
			default:
				super.receiveBroadcast(action, extras);
				break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (v == header) {
			// khong xu ly
			return true;
		}
		return false;
	}

	@Override
	public boolean onSingleTap(LatLng latlng, Point point) {
		// neu dang xem thi ko cho chon vi tri
		if (typeRequest != REQUEST_VIEW_CUSTOMER) {
			this.cusLat = latlng.getLatitude();
			this.cusLng = latlng.getLongitude();
			showFlagMarkerInMap(latlng.getLatitude(), latlng.getLongitude());
		}
		return true;
	}

}
