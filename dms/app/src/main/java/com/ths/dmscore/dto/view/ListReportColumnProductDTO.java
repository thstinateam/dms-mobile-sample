package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.util.StringUtil;

public class ListReportColumnProductDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public int totalItem;
	public ArrayList<ReportColumnProductDTO> listReportProduct;
	public ArrayList<ReportColumnCatDTO> listReportCat;
	public ArrayList<ReportColumnCatDTO> listReportSubCat;

	public ListReportColumnProductDTO(){
		totalItem = 0;
		listReportProduct = new ArrayList<ReportColumnProductDTO>();
		listReportCat = new ArrayList<ReportColumnCatDTO>();
		listReportSubCat = new ArrayList<ReportColumnCatDTO>();
	}

	public void addCategory(ReportColumnCatDTO dto) {
		// TODO Auto-generated method stub
		boolean isExist = false;
		for(ReportColumnCatDTO cat: listReportCat){
			if(cat.catID == dto.catID){
				isExist = true;
				break;
			}
		}
		if(!isExist && !StringUtil.isNullOrEmpty(dto.catName)){
			listReportCat.add(dto);
		}
		orderCategory(listReportCat, true);
		isExist = false;
		for(ReportColumnCatDTO cat: listReportSubCat){
			if(cat.subCatID == dto.subCatID ){
				isExist = true;
				break;
			}
		}
		if(!isExist && !StringUtil.isNullOrEmpty(dto.subCatName)){
			listReportSubCat.add(dto);
		}

		orderCategory(listReportSubCat, false);

	}

	 /**
	 * sort category theo thu ten
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void orderCategory(ArrayList<ReportColumnCatDTO> lstSort, boolean isCat) {
		for (int i = 0, size = lstSort.size(); i < size; i++) {
			for (int j = i + 1, siz = lstSort.size(); j < siz; j++) {
				ReportColumnCatDTO item1 = lstSort.get(i);
				ReportColumnCatDTO temp = item1.clone();
				ReportColumnCatDTO item2 = lstSort.get(j);
				if (!isCat) {
					if (lstSort.get(i).subCatName != null
							&& lstSort.get(j).subCatName != null
							&& lstSort.get(i).subCatName.compareTo(lstSort
									.get(j).subCatName) > 0) {
						item1 = item2;
						lstSort.set(i,item1);
						lstSort.set(j, temp);
					}
				} else {
					if (lstSort.get(i).catName != null
							&& lstSort.get(j).catName != null
							&& lstSort.get(i).catName
									.compareTo(lstSort.get(j).catName) > 0) {
						item1 = item2;
						lstSort.set(i,item1);
						lstSort.set(j, temp);
					}
				}
			}
		}
	}
}
