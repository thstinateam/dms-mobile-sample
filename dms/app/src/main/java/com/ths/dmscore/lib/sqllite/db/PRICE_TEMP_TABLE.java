/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PriceTempDTO;
import com.ths.dmscore.util.DateUtils;


/**
 * Table chua gia cua 1 kh lay theo nhieu level
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PRICE_TEMP_TABLE extends ABSTRACT_TABLE {
	// id gia
	public static final String PRICE_ID = "PRICE_ID";
	// id product
	public static final String PRODUCT_ID = "PRODUCT_ID";

	public static final String PRICE_TEMP_ID = "PRICE_TEMP_ID";
	public static final String VAT = "VAT";

	public static final String PRICE_TEMP_TABLE = "PRICE_TEMP";

	public PRICE_TEMP_TABLE(SQLiteDatabase mDB) {
		// TODO Auto-generated constructor stub
		this.tableName = PRICE_TEMP_TABLE;
		this.columns = new String[] { PRICE_ID, PRODUCT_ID,VAT, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	 /**
	 * Insert vao bang PRICE_TEMP
	 * @author: Tuanlt11
	 * @param listOrgTemp
	 * @return
	 * @return: long
	 * @throws:
	*/
	public long insertIntoTable(ArrayList<PriceTempDTO> lstPriceTemp) {
		// TODO Auto-generated method stub
		long success = 0;
		TABLE_ID table = new TABLE_ID(mDB);
		long priceTempId = table.getMaxIdTime(tableName);
		for (PriceTempDTO dto : lstPriceTemp) {
			ContentValues cv = new ContentValues();
			cv.put(PRICE_TEMP_ID, priceTempId++);
			cv.put(PRODUCT_ID, dto.productId);
			cv.put(PRICE_ID, dto.priceId);
			cv.put(VAT, dto.vat);
			success = insert(null, cv);
			if(success <= 0)
				break;
		}
		return success;
	}


	/**
	 * Lay gia sp cua khach hang theo nhieu cap
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceCustomerByLevel(Bundle bundle) throws Exception {
		String customerId = bundle
				.getString(IntentConstants.INTENT_CUSTOMER_ID);
		int customerTypeId = bundle
				.getInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<PriceTempDTO> price= new ArrayList<PriceTempDTO>();
		ArrayList<PriceTempDTO> priceTemp = new ArrayList<PriceTempDTO>();

		int level = 1;// 7 buoc de lay gia
		while (level <= 7) {
			switch (level) {
			case 1:
				price.addAll(getPriceByCustomerId(customerId));
				break;
			case 2:
				priceTemp.addAll(getPriceByCustomerTypeOfShop(customerTypeId, shopId));
				break;
			case 3:
				priceTemp.addAll(getPriceByCustomerTypeOfShopType(customerTypeId,
						shopId));
				break;
			case 4:
				priceTemp.addAll(getPriceByCustomerType(customerTypeId));
				break;
			case 5:
				priceTemp.addAll(getPriceByShop(shopId));
				break;
			case 6:
				priceTemp.addAll(getPriceByShopType(shopId));
				break;
			case 7:
				priceTemp.addAll(getPriceByShopRecursive(shopId));
				break;
			default:
				break;
			}
			// loc lai price id
			for(PriceTempDTO temp : priceTemp){
				boolean isExist = false;
				for(PriceTempDTO priceDto : price){
					if(temp.productId == priceDto.productId){
						isExist = true;
						break;
					}
				}
				if(!isExist)
					price.add(temp);
			}
			level++;
			priceTemp.clear();
		}

		return price;
	}

	/**
	 * Lay gia theo id khach hang cua shop
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByCustomerId(String customerId)
			throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       price_id, ");
		sql.append("       product_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  customer_id = ? ");
		params.add(customerId);
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);
		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo loai khach hang cua shop
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByCustomerTypeOfShop(int customerTypeId,
			String shopId) throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       price_id, ");
		sql.append("       product_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  customer_type_id = ? ");
		params.add(customerTypeId + "");
		sql.append("       AND shop_id = ? ");
		params.add(shopId);
		sql.append("	   AND customer_id is null	");
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo loai khach hang cua loai shop
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByCustomerTypeOfShopType(int customerTypeId,
			String shopId) throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       product_id, ");
		sql.append("       price_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  customer_type_id = ? ");
		params.add(customerTypeId + "");
//		sql.append("       AND shop_id = ? ");
//		params.add(shopId);
		sql.append("	   AND customer_id is null	");
		sql.append("	   AND shop_id is null	");
		sql.append("       AND shop_type_id IN ( ");
		sql.append("	   SELECT   s.shop_type_id ");
		sql.append("	   FROM     shop s, ");
		sql.append("				shop_type sht ");
		sql.append("	   WHERE  1 = 1 ");
		sql.append("      			 AND    s.shop_id = ? ");
		params.add(shopId);
		sql.append("      			 AND    s.status = 1 ");
		sql.append("      			 AND    sht.status = 1 ");
		sql.append("      			 AND    sht.shop_type_id = s.shop_type_id) ");
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo loai khach hang
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByCustomerType(int customerTypeId)
			throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       product_id, ");
		sql.append("       price_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  customer_type_id = ? ");
		params.add(customerTypeId + "");
		sql.append("	   AND customer_id is null	");
		sql.append("	   AND shop_id is null	");
		sql.append("	   AND shop_type_id is null	");
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo shop
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByShop(String shopId) throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       product_id, ");
		sql.append("       price_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  shop_id = ? ");
		params.add(shopId);
		sql.append("	   AND customer_id is null	");
		sql.append("	   AND customer_type_id is null	");
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {

						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);

					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo loai shop
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByShopType(String shopId) throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT vat, ");
		sql.append("       product_id, ");
		sql.append("       price_id ");
		sql.append("FROM   price ");
		sql.append("WHERE  1 = 1");
//		sql.append("       AND shop_id = ? ");
//		params.add(shopId);
		sql.append("	   AND customer_id is null	");
		sql.append("	   AND shop_id is null	");
		sql.append("	   AND customer_type_id is null	");
		sql.append("       AND shop_type_id IN ( ");
		sql.append("	   SELECT   s.shop_type_id ");
		sql.append("	   FROM     shop s, ");
		sql.append("				shop_type sht ");
		sql.append("	   WHERE  1 = 1 ");
		sql.append("      			 AND    s.shop_id = ? ");
		params.add(shopId);
		sql.append("      			 AND    s.status = 1 ");
		sql.append("      			 AND    sht.status = 1 ");
		sql.append("      			 AND    sht.shop_type_id = s.shop_type_id) ");
		sql.append("	   AND STATUS = 1	");
		sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
		params.add(dateNow);
		sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
		params.add(dateNow);

		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						PriceTempDTO temp = new PriceTempDTO();
						temp.initDataFromCursor(cursor);
						priceDTO.add(temp);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return priceDTO;
	}

	/**
	 * Lay gia theo shop de quy len cho toi khi nao la shop cha cuoi cung thi
	 * thoi
	 *
	 * @author: Tuanlt11
	 * @param customerId
	 * @return
	 * @return: PriceTempDTO
	 * @throws:
	 */
	public ArrayList<PriceTempDTO> getPriceByShopRecursive(String shopId) throws Exception {
		String dateNow = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		Cursor cursor = null;
		ArrayList<PriceTempDTO> priceDTO = new ArrayList<PriceTempDTO>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();
		for (String shopTemp : lstShop) {
			params.clear();
			sql.setLength(0);
			sql.append("SELECT vat, ");
			sql.append("       product_id, ");
			sql.append("       price_id ");
			sql.append("FROM   price ");
			sql.append("WHERE  shop_id = ?");
			params.add(shopTemp);
			sql.append("	   AND shop_id <> ?	");
			params.add(shopId);
			sql.append("	   AND STATUS = 1	");
			sql.append("       AND Ifnull(substr(from_date, 1, 10) <= substr(?, 1, 10),0) ");
			params.add(dateNow);
			sql.append("       AND Ifnull(substr(to_date, 1, 10) >= substr(?, 1, 10),1) ");
			params.add(dateNow);
			sql.append("       GROUP BY product_id ");

			try {
				cursor = rawQueries(sql.toString(), params);
				if (cursor != null) {
					if (cursor.moveToFirst()) {
						do {
							PriceTempDTO temp = new PriceTempDTO();
							temp.initDataFromCursor(cursor);
							// loc lai price id
							boolean isExist = false;
							for (PriceTempDTO priceTemp : priceDTO) {
								if (temp.productId == priceTemp.productId) {
									isExist = true;
									break;
								}
							}
							if (!isExist)
								priceDTO.add(temp);

						} while (cursor.moveToNext());
					}
				}
			} finally {
				if (cursor != null) {
					try {
						cursor.close();
					} catch (Exception ex) {
					}
				}
			}
		}


		return priceDTO;
	}

	 /**
	 * Lay gia theo shop
	 * @author: Dungdq
	 * @param bundle
	 * @return
	 * @throws Exception
	 * @return: ArrayList<PriceTempDTO>
	 * @throws:
	*/
	public ArrayList<PriceTempDTO> getPriceShopByLevel(Bundle bundle) throws Exception {
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<PriceTempDTO> price= new ArrayList<PriceTempDTO>();
		ArrayList<PriceTempDTO> priceTemp = new ArrayList<PriceTempDTO>();

		int level = 1;// 7 buoc de lay gia
		while (level <= 3) {
			switch (level) {
			case 1:
				priceTemp.addAll(getPriceByShop(shopId));
				break;
			case 2:
				priceTemp.addAll(getPriceByShopType(shopId));
				break;
			case 3:
				priceTemp.addAll(getPriceByShopRecursive(shopId));
				break;
			default:
				break;
			}
			// loc lai price id
			for(PriceTempDTO temp : priceTemp){
				boolean isExist = false;
				for(PriceTempDTO priceDto : price){
					if(temp.productId == priceDto.productId){
						isExist = true;
						break;
					}
				}
				if(!isExist)
					price.add(temp);
			}
				level++;
		}

		return price;
	}

}
