/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;

import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * Define size/text cho cac tableLayout
 *
 * @author: PhucNT
 * @version: 1.0
 * @since: 1.0
 */
public class TableDefineContanst {
	public final static String COLUMN_NAME_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_STT = 40;
	public final static String COLUMN_NAME_DATE = StringUtil.getString(R.string.TEXT_HEADER_MENU_REPORT_DATE);
	public final static int COLUMN_WITDH_DATE = 120;
	public final static String COLUMN_NAME_CUSTOMER_CODE = StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE);
	public final static int COLUMN_WITDH_CUSTOMER_CODE = 170;
	public final static String COLUMN_NAME_CUSTOMER_NAME = StringUtil.getString(R.string.TEXT_DESCRIPTION);
	public final static int COLUMN_WITDH_CUSTOMER_NAME = 400;
	public final static String COLUMN_NAME_ORDER_NUMBER = StringUtil.getString(R.string.TEXT_ORDER_NUMBER);
	public final static int COLUMN_WITDH_ORDER_NUMBER = 130;
	public final static String COLUMN_NAME_MONEY = StringUtil.getString(R.string.TEXT_BECOME_MONEY);
	public final static int COLUMN_WITDH_MONEY = 130;
	public final static String COLUMN_NAME_ORDER_EMPTY = StringUtil.getString(R.string.TEXT_DESCRIPTION);
	public final static int COLUMN_WITDH_ORDER_EMPTY = 228;
	public final static int COLUMN_WITDH_ORDER_EMPTY_VANSALES = 176;
	public final static String COLUMN_NAME_WAY = "  ";
	public final static int COLUMN_WITDH_WAY_VIEW_PO = 50;
	public final static int COLUMN_WITDH_WAY_RETURN_ORDER = 50;
	public final static int COLUMN_WITDH_WAY = 70;
	public final static String COLUMN_NAME_SELECT = " " +StringUtil.getString(R.string.TEXT_SEND)+" ";
	public final static int COLUMN_WIDTH_SELECT = 90;

	public final static String COLUMN_NAME_MMH = StringUtil.getString(R.string.TEXT_ORDER_CODE_SHORT);
	public final static int COLUMN_WITDH_MMH = 110;
	public final static String COLUMN_NAME_TMH = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WITDH_TMH = 305;
	public final static String COLUMN_NAME_PRICE = StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WITDH_PRICE = 110;
	public final static String COLUMN_NAME_CTKM = StringUtil.getString(R.string.TEXT_CTKM);
	public final static int COLUMN_WITDH_CTKM = 80;
	public final static String COLUMN_NAME_REMAIN_NUMBER = StringUtil.getString(R.string.TEXT_REMAIN);
	public final static int COLUMN_WITDH_REMAIN_NUMBER = 120;
	public final static String COLUMN_NAME_HINT_NUMBER = StringUtil.getString(R.string.TEXT_REMAIN_HINT);
	public final static int COLUMN_WITDH_HINT_NUMBER = 120;
	public final static String COLUMN_NAME_CHECK = "";
	public final static int COLUMN_WITDH_CHECK = 50;
	public final static String COLUMN_NAME_REMAIN = StringUtil.getString(R.string.TEXT_AVAILABLE_STOCK);
	public final static String COLUMN_NAME_REMAIN_ACTUAL = StringUtil.getString(R.string.TEXT_REAL_STOCK);
	public final static int COLUMN_WITDH_REMAIN = 50;
	// bang danh sach don hang

	/**
	 * set with and colum title for table find product add to order list
	 */
	public final static int COLUMN_WITDH_PRODUCT_CODE = 110;
	public final static String COLUMN_NAME_PRODUCT_CODE = StringUtil.getString(R.string.TEXT_ORDER_CODE);
	public final static int COLUMN_WITDH_PRODUCT_NAME = 240;
	public final static String COLUMN_NAME_PRODUCT_NAME = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WITDH_PRODUCT_PRICE = 100;
	public final static String COLUMN_NAME_PRODUCT_PRICE = StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WITDH_PRODUCT_UNIT = 70;
	public final static String COLUMN_NAME_PRODUCT_UNIT = StringUtil.getString(R.string.TEXT_PRODUCT_SPECIFICATION);
	public final static int COLUMN_WITDH_PRODUCT_INVENTORY = 70;
	public final static String COLUMN_NAME_PRODUCT_INVENTORY = StringUtil.getString(R.string.TEXT_AVAILABLE_STOCK);
	public final static int COLUMN_WITDH_PRODUCT_INVENTORY_ACTUAL = 70;
	public final static String COLUMN_NAME_PRODUCT_INVENTORY_ACTUAL = StringUtil.getString(R.string.TEXT_REAL_STOCK);
	// public final static int COLUMN_WITDH_PRODUCT_STATUS = 50;
	// public final static String COLUMN_NAME_PRODUCT_STATUS = "TT";
	public final static int COLUMN_WITDH_PRODUCT_CTKM = 60;
	public final static String COLUMN_NAME_PRODUCT_CTKM = StringUtil.getString(R.string.TEXT_CTKM);
	public final static int COLUMN_WITDH_PRODUCT_NUMBER = 95;
	public final static String COLUMN_NAME_PRODUCT_NUMBER = StringUtil.getString(R.string.TEXT_SL);
	public final static int COLUMN_WITDH_PRODUCT_DETAIL = 74;
	public final static int COLUMN_WITDH_STATUS = 450;
	public final static String COLUMN_NAME_PRODUCT_DETAIL = StringUtil.getString(R.string.TEXT_CT);

	/**
	 * set with and colum title for table promotin
	 */


	/**
	 * set with and colum title for table business support
	 */
	public final static int COLUMN_WITDH_BUSINESS_SUPPORT_STT = 50;
	public final static String COLUMN_NAME_BUSINESS_SUPPORT_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_BUSINESS_SUPPORT_LOAICT = 200;
	public final static String COLUMN_NAME_BUSINESS_SUPPORT_LOAICT = StringUtil.getString(R.string.TEXT_CTTB_PROGRAME_TYPE_COLON);
	public final static int COLUMN_WITDH_BUSINESS_SUPPORT_SOLUONG = 695;
	public final static String COLUMN_NAME_BUSINESS_SUPPORT_SOLUONG = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT);

	/**
	 * set with and colum title for table display promotin
	 */
	/**
	 * set with and colum title for table display promotin
	 */
	public final static int COLUMN_WITDH_DIS_PROMOTION_STT = 50;
	public final static String COLUMN_NAME_DIS_PROMOTION_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_DIS_PROMOTION_MACT = 150;
	public final static String COLUMN_NAME_DIS_PROMOTION_MACT =  StringUtil.getString(R.string.TEXT_PROGRAME_CODE_SHORT_COLON);
	public final static int COLUMN_WITDH_DIS_PROMOTION_TENCT = 290;
	public final static String COLUMN_NAME_DIS_PROMOTION_TENCT = StringUtil.getString(R.string.TEXT_NAME_LABLE);
	public final static int COLUMN_WITDH_DIS_PROMOTION_NGAYSTART = 125;
	public final static String COLUMN_NAME_DIS_PROMOTION_NGAYSTART =  StringUtil.getString(R.string.TEXT_FROM_DATE_COLON);
	public final static int COLUMN_WITDH_DIS_PROMOTION_NGAYEND = 125;
	public final static String COLUMN_NAME_DIS_PROMOTION_NGAYEND = StringUtil.getString(R.string.TEXT_CTKM_DETAIL_PROMOTION_TO_DATE);
	public final static int COLUMN_WITDH_DIS_PROMOTION_TYPE = 100;
	public final static String COLUMN_NAME_DIS_PROMOTION_TYPE = StringUtil.getString(R.string.TEXT_CTTB_PROGRAME_TYPE_COLON);
	public final static int COLUMN_WITDH_DIS_PROMOTION_QUANTITY = 100;
	public final static String COLUMN_NAME_DIS_PROMOTION_QUANTITY = StringUtil.getString(R.string.TEXT_CTTB_QUANTITY);

	/**
	 * set with and colum title for table display promotin
	 */
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_STT = 50;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_MACT = 150;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_MACT = StringUtil.getString(R.string.TEXT_PROGRAME_CODE_SHORT_COLON);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_TENCT = 290;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_TENCT = StringUtil.getString(R.string.TEXT_NAME_LABLE);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_NGAYSTART = 125;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_NGAYSTART =  StringUtil.getString(R.string.TEXT_FROM_DATE_COLON);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_NGAYEND = 125;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_NGAYEND = StringUtil.getString(R.string.TEXT_CTKM_DETAIL_PROMOTION_TO_DATE);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_TYPE = 100;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_TYPE = StringUtil.getString(R.string.TEXT_CTTB_PROGRAME_TYPE_COLON);
	public final static int COLUMN_WITDH_SUPER_DIS_PROMOTION_QUANTITY = 100;
	public final static String COLUMN_NAME_SUPER_DIS_PROMOTION_QUANTITY = StringUtil.getString(R.string.TEXT_NAME_SUPER_DIS_PROMOTION_QUANTITY);

	/**
	 * set with and colum title for table display promotin (TBHV)
	 */
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_STT = 50;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_MACT = 150;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_MACT = StringUtil.getString(R.string.TEXT_PROGRAME_CODE_SHORT_COLON);
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_TENCT = 365;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_TENCT = StringUtil.getString(R.string.TEXT_CTKM_DETAIL_PROMOTION_NAME);
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_NGAYSTART = 125;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_NGAYSTART =  StringUtil.getString(R.string.TEXT_FROM_DATE_COLON);
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_NGAYEND = 125;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_NGAYEND = StringUtil.getString(R.string.TEXT_CTKM_DETAIL_PROMOTION_TO_DATE);
	public final static int COLUMN_WITDH_TBHV_DIS_PROMOTION_TYPE = 125;
	public final static String COLUMN_NAME_TBHV_DIS_PROMOTION_TYPE = StringUtil.getString(R.string.TEXT_CTTB_DEPART);

	/**
	 * set with and colum title for table customer promotin
	 */
	public final static int COLUMN_WITDH_CUS_PROMOTION_STT = 50;
	public final static String COLUMN_NAME_CUS_PROMOTION_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_CUS_PROMOTION_MACT = 150;
	public final static String COLUMN_NAME_CUS_PROMOTION_MACT = StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE);
	public final static int COLUMN_WITDH_CUS_PROMOTION_TENCT = 445;
	public final static String COLUMN_NAME_CUS_PROMOTION_TENCT =StringUtil.getString(R.string.TEXT_TB_CUSTOMER_NAME);
	public final static int COLUMN_WITDH_CUS_PROMOTION_LEVEL = 150;
	public final static String COLUMN_NAME_CUS_PROMOTION_LEVEL = StringUtil.getString(R.string.TEXT_CUSTOMER_LEVEL_LABLE);
	public final static int COLUMN_WITDH_CUS_PROMOTION_SALES_REMAIN = 150;
	public final static String COLUMN_NAME_CUS_PROMOTION_SALES_REMAIN = StringUtil.getString(R.string.TEXT_TB_CUSTOMER_SALES_REMAIN);

	/**
	 * set with and colum title for table customer promotin
	 */
	public final static int COLUMN_WITDH_ITEM_PROMOTION_STT = 50;
	public final static String COLUMN_NAME_ITEM_PROMOTION_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_ITEM_PROMOTION_MACT = 100;
	public final static String COLUMN_NAME_ITEM_PROMOTION_MACT = StringUtil.getString(R.string.TEXT_MA_SP);
	public final static int COLUMN_WITDH_ITEM_PROMOTION_TENCT = 890;
	public final static String COLUMN_NAME_ITEM_PROMOTION_TENCT = StringUtil.getString(R.string.TEXT_NAME_PRODUCT);

	/**
	 * set with and colum title for table list product
	 */
	public final static String COLUMN_NAME_PRODUCTS_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_PRODUCTS_STT = 45;
	public final static int COLUMN_WITDH_PRODUCTS_CODE = 150;
	public final static String COLUMN_NAME_PRODUCTS_CODE = StringUtil.getString(R.string.TEXT_MA_SP);
	public final static int COLUMN_WITDH_PRODUCTS_NAME = 255;
	public final static String COLUMN_NAME_PRODUCTS_NAME = StringUtil.getString(R.string.TEXT_NAME_PRODUCT);
	public final static int COLUMN_WITDH_PRODUCTS_PRICE = 150;
	public final static String COLUMN_NAME_PRODUCTS_PRICE = StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WITDH_PRODUCTS_SPECIFICATION = 150;
	public final static String COLUMN_NAME_PRODUCTS_SPECIFICATION = StringUtil.getString(R.string.TEXT_PRODUCT_SPECIFICATION);
	public final static int COLUMN_WITDH_PRODUCTS_STOCK = 150;
	public final static String COLUMN_NAME_PRODUCTS_STOCK = StringUtil.getString(R.string.TEXT_COLUM_INVENTORY);
	public final static int COLUMN_WITDH_PRODUCTS_TOTAL = 150;
	public final static String COLUMN_NAME_PRODUCTS_TOTAL = StringUtil.getString(R.string.TEXT_NAME_PRODUCTS_TOTAL);

	/**
	 * set width and colum title for table statistics products
	 */
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_CODE = StringUtil.getString(R.string.TEXT_ORDER_CODE_SHORT);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_CODE = 150;
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_NAME =  StringUtil.getString(R.string.TEXT_NAME_STATISTICS_PRODUCT_NAME);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_NAME = 255;
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_PROMOTION = StringUtil.getString(R.string.TEXT_PROMOTION);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_PROMOTION = 150;
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_NUMBER_PRODUCT = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_NUMBER_PRODUCT = 150;
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_SOLD = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT_SOLD);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_SOLD = 150;
	public final static String COLUMN_NAME_STATISTICS_PRODUCT_STOCK = StringUtil.getString(R.string.TEXT_HEADER_TABLE_REMAIN);
	public final static int COLUMN_WIDTH_STATISTICS_PRODUCT_STOCK = 150;

	/**
	 * set width and column title for table vote display present product
	 * promotion
	 */
	public final static String COLUMN_NAME_VOTE_PRODUCT_CODE = StringUtil.getString(R.string.TEXT_ORDER_CODE);
	public final static int COLUMN_WIDTH_VOTE_PRODUCT_CODE = 150;
	public final static String COLUMN_NAME_VOTE_PRODUCT_NAME = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WIDTH_VOTE_PRODUCT_NAME = 485;
	public final static String COLUMN_NAME_VOTE_NUMBER_PRODUCT = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT);
	public final static int COLUMN_WIDTH_VOTE_NUMBER_PRODUCT = 132;
	public final static String COLUMN_NAME_VOTE_NUMBER = StringUtil.getString(R.string.TEXT_VOTE);
	public final static int COLUMN_WIDTH_VOTE_NUMBER = 132;

	/**
	 * set width and colum title for table indebtedness list
	 */
	public final static String COLUMN_NAME_INDEBTEDNESS_CUSTOMMER_CODE = StringUtil.getString(R.string.TEXT_ORDER_CODE_SHORT);
	public final static int COLUMN_WIDTH_INDEBTEDNESS_CUSTOMMER_CODE = 150;
	public final static String COLUMN_NAME_INDEBTEDNESS_CUSTOMMER_NAME =  StringUtil.getString(R.string.TEXT_NAME_STATISTICS_PRODUCT_NAME);
	public final static int COLUMN_WIDTH_INDEBTEDNESS_CUSTOMMER_NAME = 300;
	public final static String COLUMN_NAME_INDEBTEDNESS_BALANCE = StringUtil.getString(R.string.TEXT_BALANCE);
	public final static int COLUMN_WIDTH_INDEBTEDNESS_BALANCE = 200;
	public final static String COLUMN_NAME_INDEBTEDNESS_STATUS = StringUtil.getString(R.string.TEXT_STATUS_COLON);
	public final static int COLUMN_WIDTH_INDEBTEDNESS_STATUS = 150;
	public final static String COLUMN_NAME_INDEBTEDNESS_TIME = StringUtil.getString(R.string.TEXT_TIME);
	public final static int COLUMN_WIDTH_INDEBTEDNESS_TIME = 150;
	public final static String COLUMN_NAME_INDEBTEDNESS_DETAIL = "  ";
	public final static int COLUMN_WIDTH_INDEBTEDNESS_DETAIL = 50;

	/**
	 * set chieu rong va title cho bang doanh so cua khach hang
	 */
	public final static String COLUMN_NAME_CUS_SALE_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WIDTH_CUS_SALE_STT = 40;
	public final static String COLUMN_NAME_CUS_SALE_GROUP = StringUtil.getString(R.string.TEXT_GROUP_DS);
	public final static int COLUMN_WIDTH_CUS_SALE_GROUP = 250;
	public final static String COLUMN_NAME_CUS_SALE_DG = StringUtil.getString(R.string.TEXT_NAME_CUS_SALE_DG);
	public final static int COLUMN_WIDTH_CUS_SALE_DG = 400;
	public final static String COLUMN_NAME_CUS_SALE_MDS = StringUtil.getString(R.string.TEXT_NAME_CUS_SALE_MDS);
	public final static int COLUMN_WIDTH_CUS_SALE_MDS = 250;

	/**
	 * set chieu rong va title cho bang nhung don hang gan fullDate cua khach hang
	 */
	public final static String COLUMN_NAME_CUS_LAST_ORDER_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WIDTH_CUS_LAST_ORDER_STT = 50;
	public final static String COLUMN_NAME_CUS_LAST_ORDER_CODE = StringUtil.getString(R.string.TEXT_ORDER_NUMBER);
	public final static int COLUMN_WIDTH_CUS_LAST_ORDER_CODE = 250;
	public final static String COLUMN_NAME_CUS_LAST_ORDER_DATE = StringUtil.getString(R.string.TEXT_HEADER_MENU_REPORT_DATE);
	public final static int COLUMN_WIDTH_CUS_LAST_ORDER_DATE = 250;
	public final static String COLUMN_NAME_CUS_LAST_ORDER_SKU = StringUtil.getString(R.string.TEXT_SKU_2DOT);
	public final static int COLUMN_WIDTH_CUS_LAST_ORDER_SKU = 195;
	public final static String COLUMN_NAME_CUS_LAST_ORDER_MONEY = StringUtil.getString(R.string.TEXT_BECOME_MONEY);
	public final static int COLUMN_WIDTH_CUS_LAST_ORDER_MONEY = 195;

	/**
	 * set chieu rong va title cho bang nhung don hang gan fullDate cua khach hang
	 */
	public final static String COLUMN_NAME_CUS_TOOL_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WIDTH_CUS_TOOL_STT = 40;
	public final static String COLUMN_NAME_CUS_TOOL_CODE = StringUtil.getString(R.string.TEXT_NAME_CUS_TOOL_CODE);
	public final static int COLUMN_WIDTH_CUS_TOOL_CODE = 210;
	public final static String COLUMN_NAME_CUS_TOOL_NAME = StringUtil.getString(R.string.TEXT_NAME_CUS_TOOL_NAME);
	public final static int COLUMN_WIDTH_CUS_TOOL_NAME = 500;
	public final static String COLUMN_NAME_CUS_TOOL_TARGET = StringUtil.getString(R.string.QUOTA);
	public final static int COLUMN_WIDTH_CUS_TOOL_TARGET = 150;
	public final static String COLUMN_NAME_CUS_TOOL_FACT = StringUtil.getString(R.string.TEXT_NAME_CUS_TOOL_FACT);
	public final static int COLUMN_WIDTH_CUS_TOOL_FACT = 150;

	/**
	 * set with and colum title for table select product has promotion
	 */
	public final static int COLUMN_WITDH_PRODUCT_PROMOTION_CODE = 160;
	public final static String COLUMN_NAME_PRODUCT_PROMOTION_CODE = StringUtil.getString(R.string.TEXT_ORDER_CODE);
	public final static int COLUMN_WITDH_PRODUCT_PROMOTION_NAME = 270;
	public final static String COLUMN_NAME_PRODUCT_PROMOTION_NAME = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WITDH_PRODUCT_PROMOTION_INVENTORY = 154;
	public final static String COLUMN_NAME_PRODUCT_PROMOTION_INVENTORY = StringUtil.getString(R.string.TEXT_COLUM_INVENTORY);
	// public final static int COLUMN_WITDH_PRODUCT_PROMOTION_TOTAL_PRODUCT =
	// 150;
	// public final static String COLUMN_NAME_PRODUCT_PROMOTION_TOTAL_PRODUCT =
	// StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WITDH_PRODUCT_PROMOTION_NUM_PROMOTION = 150;
	public final static String COLUMN_NAME_PRODUCT_PROMOTION_NUM_PROMOTION = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT);

	/**
	 * set width and title name for table in popup list programe select for
	 * product
	 */
	public final static String COLUMN_NAME_PROGRAME_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static String COLUMN_NAME_CUSTOMER = StringUtil.getString(R.string.TEXT_CUSTOMER_NAME_ABB);
	public final static String COLUMN_ADDRESS = StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS);
	public final static String COLUMN_NVBH = StringUtil.getString(R.string.TEXT_HEADER_TABLE_NVBH);
	public final static String COLUMN_MA_NVBH = StringUtil.getString(R.string.TEXT_HEADER_TABLE_NVBH_CODE);
	public final static String COLUMN_SO_THIET_BI = StringUtil.getString(R.string.TEXT_NUM_DEVICE);
	public final static String COLUMN_KHONG_DAT = StringUtil.getString(R.string.TEXT_NO_OK);
	public final static String COLUMN_TUYEN = StringUtil.getString(R.string.TEXT_LINE);
	public final static String COLUMN_SLGT = StringUtil.getString(R.string.TEXT_VISIT_TIMES);
	public final static String COLUMN_DS_THANG_TRUOC = StringUtil.getString(R.string.TEXT_AMOUNT_LAST_MONTH);
	public final static String COLUMN_DATE_DH_LAST = StringUtil.getString(R.string.TEXT_ORDER_LAST);
	public final static int COLUMN_WIDTH_PROGRAME_STT = 45;
	public final static String COLUMN_NAME_PROGRAME_CODE = StringUtil.getString(R.string.TEXT_PROGRAME_CODE_SHORT_COLON);
	public final static int COLUMN_WIDTH_PROGRAME_CODE = 150;
	public final static String COLUMN_NAME_PROGRAME_LEVEL = StringUtil.getString(R.string.TEXT_COLUM_ORDER_LEVEL);
	public final static int COLUMN_WIDTH_PROGRAME_LEVEL = 50;
	public final static String COLUMN_NAME_PROGRAME_NAME = StringUtil.getString(R.string.TEXT_PROGRAME_NAME_SHORT_COLON);
	public final static int COLUMN_WIDTH_PROGRAME_NAME = 300;
	public final static String COLUMN_NAME_PROGRAME_TYPE = StringUtil.getString(R.string.TEXT_CTTB_PROGRAME_TYPE_COLON);
	public final static int COLUMN_WIDTH_PROGRAME_TYPE = 122;

	/**
	 * set with and colum title for table display promotin
	 */
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_CODE = 100;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_CODE = StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE);
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_NAME = 280;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_NAME =StringUtil.getString(R.string.TEXT_TB_CUSTOMER_NAME);
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_ADDRESS = 220;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_ADDRESS = StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS);
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_LEVEL = 53;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_CUSTOMER_LEVEL = StringUtil.getString(R.string.TEXT_HEADER_LIMIT);
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_AMOUNT_REMAIN = 145;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_AMOUNT_REMAIN = StringUtil.getString(R.string.TEXT_DISPLAY_PROGRAME_DETAIL_AMOUNT_REMAIN);
	public final static int COLUMN_WITDH_REPORT_DISPLAY_PROGRAME_DETAIL_AMOUNT_PLAN = 145;
	public final static String COLUMN_NAME_REPORT_DISPLAY_PROGRAME_DETAIL_AMOUNT_PLAN = StringUtil.getString(R.string.TEXT_DISPLAY_PROGRAME_DETAIL_AMOUNT_PLAN);

	/**
	 * set width and colum title for table selected product add to reviews staff
	 */
	public final static String COLUMN_NAME_PRODUCT_CODE_ADD_REVIEWS_STAFF = StringUtil.getString(R.string.TEXT_ORDER_CODE);
	public final static int COLUMN_WIDTH_PRODUCT_CODE_ADD_REVIEWS_STAFF = 130;
	public final static String COLUMN_NAME_PRODUCT_NAME_ADD_REVIEWS_STAFF = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WIDTH_PRODUCT_NAME_ADD_REVIEWS_STAFF = 420;
	public final static String COLUMN_NAME_PRODUCT_PRICE_ADD_REVIEWS_STAFF = StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WIDTH_PRODUCT_PRICE_ADD_REVIEWS_STAFF = 100;
	public final static String COLUMN_NAME_PROGRAME_CODE_ADD_REVIEWS_STAFF = StringUtil.getString(R.string.TEXT_CTKM);
	public final static int COLUMN_WIDTH_PROGRAME_CODE_ADD_REVIEWS_STAFF = 120;
	public final static int COLUMN_WIDTH_CHECK_BOX_ADD_REVIEWS_STAFF = 60;

	/**
	 * set with and colum title for table list product sale satatistics
	 */
	public final static int COLUMN_WITDH_PRODUCTS_CODE_SALE_STATISTICS = 100;
	// chu y dung chung cho nhieu man hinh
	public final static String COLUMN_NAME_PRODUCTS_CODE_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_HEADER_PRODUCT_CODE);
	public final static int COLUMN_WITDH_PRODUCTS_NAME_SALE_STATISTICS = 210;
	public final static String COLUMN_NAME_PRODUCTS_NAME_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_ORDER_NAME);
	public final static int COLUMN_WITDH_INDUSTRY_PRODUCT_SALE_STATISTICS = 45;
	public final static String COLUMN_NAME_INDUSTRY_PRODUCT_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_HEADER_PRODUCT_INDUSTRY);
	public final static int COLUMN_WITDH_CONVFACT_SALE_STATISTICS = 123;
	public final static String COLUMN_NAME_CONVFACT_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_PRODUCT_SPECIFICATION);
	public final static int COLUMN_WITDH_PRODUCTS_PRICE_SALE_STATISTICS = 140;
	public final static String COLUMN_NAME_PRODUCTS_PRICE_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_COLUM_PRICE);
	public final static int COLUMN_WITDH_PRODUCTS_NUMBER_SALE_STATISTICS = 135;
	public final static String COLUMN_NAME_PRODUCTS_NUMBER_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT);
	public final static int COLUMN_WITDH_PRODUCTS_NUMBER_APPROVED_SALE_STATISTICS = 135;
	public final static String COLUMN_NAME_PRODUCTS_NUMBER_APPROVED_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_NUMBER_PRODUCT_APPROVED);
	public final static int COLUMN_WITDH_PRODUCTS_TOTAL_MONNEY_SALE_STATISTICS = 140;
	public final static String COLUMN_NAME_PRODUCTS_TOTAL_MONNEY_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_RETURN_TOTAL_PAY);
	public final static int COLUMN_WITDH_PRODUCTS_TOTAL_MONNEY_APPROVED_SALE_STATISTICS = 140;
	public final static String COLUMN_NAME_PRODUCTS_TOTAL_MONNEY_APPROVED_SALE_STATISTICS = StringUtil.getString(R.string.TEXT_RETURN_TOTAL_PAY_APPROVED);

	// Popup list customer for product video
	public final static int COLUMN_WIDTH_SELECT_CUSTOMER_NAME = 250;
	public final static String COLUMN_NAME_SELECT_CUSTOMER_NAME = StringUtil.getString(R.string.TEXT_FOLLOW_PROBLEM_CUSTOMER);
	public final static int COLUMN_WIDTH_SELECT_CUSTOMER_ADDRESS = 350;
	public final static String COLUMN_NAME_SELECT_CUSTOMER_ADDRESS = StringUtil.getString(R.string.TEXT_CUSTOMER_ADDRESS);
	public final static int COLUMN_WIDTH_SELECT_CUSTOMER_VIDEO = 100;
	public final static String COLUMN_NAME_SELECT_CUSTOMER_VIDEO = StringUtil.getString(R.string.TEXT_WATCH_VIDEO);

	/**
	 * set with and colum title for table document
	 */
	public final static int COLUMN_WITDH_DOCUMENT_STT = 50;
	public final static String COLUMN_NAME_DOCUMENT_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_DOCUMENT_MACT = 150;
	public final static String COLUMN_NAME_DOCUMENT_MACT = StringUtil.getString(R.string.TEXT_DOCUMENT_CODE);
	public final static int COLUMN_WITDH_DOCUMENT_TENCT = 390;
	public final static String COLUMN_NAME_DOCUMENT_TENCT = StringUtil.getString(R.string.TEXT_DOCUMENT_NAME);
	public final static int COLUMN_WITDH_DOCUMENT_NGAYSTART = 125;
	public final static int COLUMN_WITDH_DOCUMENT_TYPE = 100;
	public final static String COLUMN_NAME_DOCUMENT_TYPE = StringUtil.getString(R.string.TEXT_TYPE);
	public final static String COLUMN_NAME_DOCUMENT_NGAYSTART =  StringUtil.getString(R.string.TEXT_FROM_DATE_COLON);
	public final static int COLUMN_WITDH_DOCUMENT_NGAYEND = 125;
	public final static String COLUMN_NAME_DOCUMENT_NGAYEND = StringUtil.getString(R.string.TEXT_CTKM_DETAIL_PROMOTION_TO_DATE);

	/**
	 * set with and colum title for table list problem of gsnpp
	 */
	public final static String COLUMN_NAME_PROBLEM_STT = StringUtil.getString(R.string.TEXT_STT);
	public final static int COLUMN_WITDH_PROBLEM_STT = 45;
	public final static int COLUMN_WITDH_PROBLEM_TYPE = 200;
	public final static String COLUMN_NAME_PROBLEM_TYPE = StringUtil.getString(R.string.TEXT_LABLE_TYPE_PROBLEM);
	public final static int COLUMN_WITDH_PROBLEM_CONTENT = 300;
	public final static String COLUMN_NAME_PROBLEM_CONTENT = StringUtil.getString(R.string.TEXT_LABLE_PROBLEM_CONTENT);
	public final static int COLUMN_WITDH_PROBLEM_CUSTOMER = 195;
	public final static String COLUMN_NAME_PROBLEM_CUSTOMER = StringUtil.getString(R.string.TEXT_FOLLOW_PROBLEM_CUSTOMER);
	public final static int COLUMN_WITDH_PROBLEM_REMIND = 150;
	public final static String COLUMN_NAME_PROBLEM_REMIND = StringUtil.getString(R.string.TEXT_TEXTBOX_REMIND_DATE);
	public final static int COLUMN_WITDH_PROBLEM_STATUS = 50;
	public final static String COLUMN_NAME_PROBLEM_STATUS = "OK";

	public final static String COLUMN_PRODUCT_NAME = StringUtil.getString(R.string.TEXT_PRODUCT_NAME_2);

	/**
	 * set width and colum name for table quan ly thiet bi chi tiet tung NPP
	 */
	private final static int[] EQUIPMENT_DETAIL_TABLE_WIDTHS = { 150, 495, 150, 150 };
	private final static String[] EQUIPMENT_DETAIL_TABLE_TITLES = { COLUMN_MA_NVBH,
			COLUMN_NVBH, COLUMN_SO_THIET_BI, COLUMN_KHONG_DAT };

	/**
	 * set width and colum name for table in select programe list for product
	 */
	private final static int[] SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_WIDTHS = {
			COLUMN_WIDTH_PROGRAME_STT,
			COLUMN_WIDTH_PRODUCT_CODE_ADD_REVIEWS_STAFF,
			COLUMN_WIDTH_PRODUCT_NAME_ADD_REVIEWS_STAFF,
			COLUMN_WIDTH_PRODUCT_PRICE_ADD_REVIEWS_STAFF };
	private final static String[] SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_TITLES = {
			COLUMN_NAME_PROGRAME_STT,
			COLUMN_NAME_PRODUCT_CODE_ADD_REVIEWS_STAFF,
			COLUMN_NAME_PRODUCT_NAME_ADD_REVIEWS_STAFF,

			COLUMN_NAME_PRODUCT_PRICE_ADD_REVIEWS_STAFF };
	// public final static int[] SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_WIDTHS = {
	// COLUMN_WIDTH_PROGRAME_STT,
	// COLUMN_WIDTH_PRODUCT_CODE_ADD_REVIEWS_STAFF,
	// COLUMN_WIDTH_PRODUCT_NAME_ADD_REVIEWS_STAFF,
	// COLUMN_WIDTH_PRODUCT_PRICE_ADD_REVIEWS_STAFF,
	// COLUMN_WIDTH_PROGRAME_CODE_ADD_REVIEWS_STAFF};
	// public final static String[] SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_TITLES = {
	// COLUMN_NAME_PROGRAME_STT,
	// COLUMN_NAME_PRODUCT_CODE_ADD_REVIEWS_STAFF,
	// COLUMN_NAME_PRODUCT_NAME_ADD_REVIEWS_STAFF,
	// COLUMN_NAME_PRODUCT_PRICE_ADD_REVIEWS_STAFF ,
	// COLUMN_NAME_PROGRAME_CODE_ADD_REVIEWS_STAFF};

	// 45 150 150 100 263 100 110 60

	/**
	 * set width and colum name for table customer bao cao CTTB
	 */
	private final static int[] TABLE_DIS_PRO_DETAIL_DAY_WIDTHS = { 60, 250, 320, 50,
			60, 100, 100 };
	private final static String[] TABLE_DIS_PRO_DETAIL_DAY_TITLES = {
			StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE),
			StringUtil.getString(R.string.TEXT_TB_CUSTOMER_NAME),
			StringUtil.getString(R.string.TEXT_ADDRESS),
			StringUtil.getString(R.string.TEXT_HEADER_LIMIT),
			StringUtil.getString(R.string.TEXT_TYPE),
			StringUtil.getString(R.string.TEXT_HEADER_TABLE_PLAN),
			StringUtil.getString(R.string.TEXT_HEADER_TABLE_DONE) };


	/**
	 * set width and colum name for table problem of TBHV
	 */
	private final static int[] TBHV_NEED_TO_DO_WIDTH = { 45, 220, 220, 110,
			130, 165, 50 };
	private final static String[] TBHV_NEED_TO_DO_TITLE = {
			StringUtil.getString(R.string.TEXT_STT),
			StringUtil.getString(R.string.TEXT_PROBLEMS_FOUND),
			StringUtil.getString(R.string.TEXT_NEED_TO_DO),
			StringUtil.getString(R.string.TEXT_PERSON_TO_DO),
			StringUtil.getString(R.string.TEXT_FOLLOW_PROBLEM_STATUS),
			StringUtil.getString(R.string.TEXT_FINISH_DATE),
			" "};
	
	public static int[] getEquipmentDetailTableWidths() {
		return EQUIPMENT_DETAIL_TABLE_WIDTHS;
	}
	public static String[] getEquipmentDetailTableTitles() {
		return EQUIPMENT_DETAIL_TABLE_TITLES;
	}
	public static int[] getSelectProductAddReviewsStaffTableWidths() {
		return SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_WIDTHS;
	}
	public static String[] getSelectProductAddReviewsStaffTableTitles() {
		return SELECT_PRODUCT_ADD_REVIEWS_STAFF_TABLE_TITLES;
	}
	public static int[] getTableDisProDetailDayWidths() {
		return TABLE_DIS_PRO_DETAIL_DAY_WIDTHS;
	}
	public static String[] getTableDisProDetailDayTitles() {
		return TABLE_DIS_PRO_DETAIL_DAY_TITLES;
	}
	public static int[] getTbhvNeedToDoWidth() {
		return TBHV_NEED_TO_DO_WIDTH;
	}
	public static String[] getTbhvNeedToDoTitle() {
		return TBHV_NEED_TO_DO_TITLE;
	}


}
