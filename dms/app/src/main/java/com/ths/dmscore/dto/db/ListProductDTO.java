package com.ths.dmscore.dto.db;

import java.util.ArrayList;
import java.util.List;

public class ListProductDTO {
	// total object after research
	public int total = 0;
	// list object in page
	public List<ProductDTO> producList;
	
	public ListProductDTO(){
		total = 0;
		producList = new ArrayList<ProductDTO>();
	}
}
