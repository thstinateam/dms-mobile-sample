package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.lib.sqllite.db.PRODUCT_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * DTO chon column san pham cho man hinh bao cao dong
 * 
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportColumnProductDTO implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	public int productID;
	public int catID;
	public int subCatID;
	// ma don vi
	public String productCode;
	// ten don vi
	public String productName;
	// nganh hang
	public String catName;
	// nganh hang con
	public String subCatName;
	public boolean isChecked;
	public int status;

	public ReportColumnProductDTO() {
		productCode = Constants.STR_BLANK;
		productName = Constants.STR_BLANK;
		catName = Constants.STR_BLANK;
		subCatName = Constants.STR_BLANK;
		isChecked = true;
	}

	/**
	 * @param c
	 */
	public void initDataWithCursor(Cursor c) {
		// TODO Auto-generated method stub
		productCode = CursorUtil.getString(c,
				PRODUCT_TABLE.PRODUCT_CODE);
		productID = CursorUtil.getInt(c,
				PRODUCT_TABLE.PRODUCT_ID);
		productName = CursorUtil.getString(c,
				PRODUCT_TABLE.PRODUCT_NAME);
		catName = CursorUtil.getString(c, "PRODUCT_INFO_NAME");
		subCatName = CursorUtil.getString(c, "SUB_CAT_NAME");
		catID = CursorUtil.getInt(c, "PRODUCT_INFO_ID");
		subCatID = CursorUtil.getInt(c, "SUB_CAT_ID");
		status = CursorUtil.getInt(c, "status");
	}
	
	public ReportColumnProductDTO cloneProduct() {
		try {
			return clone();
		} catch (CloneNotSupportedException e) {
			MyLog.e("cloneProduct", "fail", e);
		}
		return null;
	}
	
	@Override
	protected ReportColumnProductDTO clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		ReportColumnProductDTO product = new ReportColumnProductDTO();
		product.productCode = productCode;
		product.productID = productID;
		product.productName = productName;
		product.catName = catName;
		product.subCatName = subCatName;
		product.catID = catID;
		product.subCatID = subCatID;
		return product;
	}
}
