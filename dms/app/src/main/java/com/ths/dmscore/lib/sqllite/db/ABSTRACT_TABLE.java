package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteStatement;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.CollumnMapping;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DMSCursor;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;


public abstract class ABSTRACT_TABLE {
	public static final int CREATED_STATUS = 0; // trang thai khi row duoc tao ra
	public static final int TRANSFERED_STATUS = 1; // trang thai khi row duoc chuyen thanh cong len server
	public static final int SYNCHRONIZED_STATUS = 2; // trang thai khi dong bo thanh cong
	
	// colum phuc vu syn
	public static final String SYN_STATE = "SYN_STATE";
	
	protected String[] columns;
	public String sqlGetCountQuerry="SELECT COUNT(*) FROM ";
	public String sqlDelete = "DELETE FROM ";
	public String tableName="";
	public SQLiteDatabase mDB;
	
	abstract protected long insert(AbstractTableDTO dto) ;
	abstract protected long update(AbstractTableDTO dto) ;
	abstract protected long delete(AbstractTableDTO dto) ;
	
	/**
	*  Lay so luong record cua table
	*  @author: TruongHN
	*  @return: long
	*  @throws:
	 */
	public long getCount() {
		SQLiteStatement statement = compileStatement(
				sqlGetCountQuerry);
		long count = statement.simpleQueryForLong();
		return count;
	}
	
	/**
	*   Get MaxId trong table khi truyen vao tableName & ten column
	*  @author: TruongHN
	*  @param tableName
	*  @param columnIdName
	*  @throws Exception
	*  @return: int
	*  @throws:
	 */
	public int getMaxIdInTable(String columnIdName) throws Exception {
		int maxId = -1;
		Cursor cursor = null;
		try {
			StringBuilder sqlState = new StringBuilder();
			sqlState.append("select ifnull(max(" + columnIdName + "), 0) as " + columnIdName + " from " + tableName);
			cursor = rawQuery(sqlState.toString(), null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					maxId = CursorUtil.getInt(cursor, columnIdName);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return maxId;
	}
	
	public int update(ContentValues values, String whereClause, String[] whereArgs){
		MyLog.d("Database", "update: values: " + values.toString()
				+ " whereClause: " + whereClause 
				+ " whereArgs: " + StringUtil.getArrayString(whereArgs));
		int upda = mDB.update(tableName, values, whereClause, whereArgs);
		
		MyLog.i("Truong, update - ", String.valueOf(upda));
		return upda;
	}
	
	public Cursor rawQuery(String sqlQuery,String[] params){
		String sqlInfo = "sql: " + sqlQuery + " params: " + StringUtil.getArrayString(params);
		MyLog.d("Database", sqlInfo );
		MyLog.logToFile("rawQuery", sqlInfo);
		return mDB.rawQuery(sqlQuery, params);
	}
	
	public Cursor rawQueries(String sqlQuery, ArrayList<String> params){
		String[] strParams =  null;
		if(params != null){
			strParams = new String[params.size()];
			for(int i=0,s=params.size();i<s;i++){
				strParams[i] = params.get(i);
			}
		}
		return rawQuery(sqlQuery, strParams);
	}
	
	public void execSQL(String sql){
		MyLog.d("Database", "execSQL sql: " + sql);
		mDB.execSQL(sql);
	}
	
	
	public void execSQL(String sql, Object[] bindArgs){
		MyLog.d("Database", "execSQL sql: " + sql 
				+ " bindArgs: " + StringUtil.getArrayString(bindArgs));
		mDB.execSQL(sql, bindArgs);
	}
	
	public SQLiteStatement compileStatement(String sqlQuery){
		return mDB.compileStatement(sqlQuery);
	}

	public long insert(String nullColumnHack, ContentValues values){
		return insert(tableName, nullColumnHack, values);
	}
	
	public long insert(String nameTable, String nullColumnHack, ContentValues values){
		MyLog.d("Database", "insert: " + nameTable + " values: " + values.toString());
		long insert = -1;
		try {
			insert = mDB.insertOrThrow(nameTable, nullColumnHack, values);
		} catch (Exception e) {
			MyLog.e("insert fail", e);
			ServerLogger.sendLog("fail " + "insert: " + nameTable + " values: " + values.toString() 
					+ "\n" + VNMTraceUnexceptionLog.getReportFromThrowable(e), TabletActionLogDTO.LOG_CLIENT);
		}
		MyLog.i("Truong, insert - ", String.valueOf(insert));
		return insert;
	}
	
	public int delete(String whereClause, String[] whereArgs){
		MyLog.d("Database", "delete: whereClause: " + whereClause 
				+ " whereArgs: " + StringUtil.getArrayString(whereArgs));
		
		int delete = mDB.delete(tableName, whereClause, whereArgs);
		MyLog.i("Truong, delete - ", String.valueOf(delete));
		return delete;
	}
	
	public Cursor query(String selection, String[] selectionArgs, String groupBy, String having, String orderBy){
		return mDB.query(tableName, columns, selection, selectionArgs, groupBy, having, orderBy);
	}
	
	public void clearTable() {
		mDB.execSQL(this.sqlDelete);
	}

	protected CursorRunner cursorRunner = new CursorRunner();
	
	public interface ICursorAction<T>{
		T onMoveCursor(DMSCursor c) throws Exception;
	}
	
	public interface ICursorActionQuery{
		void onMoveCursor(DMSCursor c) throws Exception;
	}
	
	/**
	 * CursorRunner, tu dong mo, dong Cursor khi truy van, ho tro tu phan trang, count
	 * ABSTRACT_TABLE.java
	 * @author: duongdt3
	 * @version: 1.0 
	 * @since:  1.0
	 * @time: 14:22:35 10 Sep 2014
	 */
	public class CursorRunner{
		
		public int queryCount(String sql, String[] params) throws Exception {
			sql = "SELECT COUNT(*) as COUNT FROM (" + sql + ")";
			return queryFirstResult(sql, params, new ICursorAction<Integer>() {
				@Override
				public Integer onMoveCursor(DMSCursor c) throws Exception {
					return c.getInt("COUNT", 0);
				}
			});
		}
		
		public int queryCount(String sql, ArrayList<String> params) throws Exception {
			sql = "SELECT COUNT(*) as COUNT FROM (" + sql + ")";
			return queryFirstResult(sql, params, new ICursorAction<Integer>() {
				@Override
				public Integer onMoveCursor(DMSCursor c) throws Exception {
					return c.getInt("COUNT", 0);
				}
			});
		}
		
		public <T> T queryFirstResult(String sql, String[] params, ICursorAction<T> cursorAction) throws Exception {
			Cursor cu = rawQuery(sql, params);
			return queryFirstResult(cu, cursorAction);
		}
		
		public <T> T queryFirstResult(String sql, ArrayList<String> params, ICursorAction<T> cursorAction) throws Exception {
			Cursor cu = rawQueries(sql, params);
			return queryFirstResult(cu, cursorAction);
		}
		
		public void query(String sql, String[] params, ICursorActionQuery cursorAction) throws Exception {
			query(sql, params, null, cursorAction);
		}
		
		public void query(String sql, String[] params,
						  CollumnMapping colMap, ICursorActionQuery cursorAction) throws Exception {
			Cursor cu = rawQuery(sql, params);
			query(cu, colMap, cursorAction);
		}
		
		public void query(String sql, ArrayList<String> params, ICursorActionQuery cursorAction) throws Exception {
			query(sql, params, null, cursorAction);
		}
		
		public void query(String sql, ArrayList<String> params, 
				CollumnMapping colMap, ICursorActionQuery cursorAction) throws Exception {
			Cursor cu = rawQueries(sql, params);
			query(cu, colMap, cursorAction);
		}
		
		public void query(String sql, ArrayList<String> params, int currentPage, 
				int numItemInPage, ICursorActionQuery cursorAction) throws Exception {
			int offset = (currentPage - 1) * numItemInPage;
			int limit = numItemInPage;
			String sqlLimit =  " limit " + limit + " offset " + offset;
			sql += sqlLimit;
			query(sql, params, null, cursorAction);
		}
		
		public void query(String sql, ArrayList<String> params, int currentPage, 
				int numItemInPage, CollumnMapping colMap, ICursorActionQuery cursorAction) throws Exception {
			Cursor cu = rawQueries(sql, params);
			query(cu, colMap, cursorAction);
		}
		
		public void query(Cursor cu, CollumnMapping colMap, 
				ICursorActionQuery cursorAction) throws Exception {
			if (cu != null) {
				try {
					DMSCursor dmsCu = null;
					while (cu.moveToNext()) {
						if (dmsCu == null) {
							dmsCu = new DMSCursor(cu, colMap);
						}
						cursorAction.onMoveCursor(dmsCu);
					}
				} catch (Exception e) {
					throw e;
				} finally {
					try {
						if (cu != null) {
							cu.close();
						}
					} catch (Exception e) {
						MyLog.e("CursorRunner query close cursor fail", e);
					}
				}
			}
		}
		
		public <T> T queryFirstResult(Cursor cu, ICursorAction<T> cursorAction) throws Exception {
			T firstResult = null; 
			if (cu != null) {
				try {
					DMSCursor dmsCu = null;
					if (cu.moveToFirst()) {
						dmsCu = new DMSCursor(cu, null);
						firstResult = cursorAction.onMoveCursor(dmsCu);
					}
				} catch (Exception e) {
					throw e;
				} finally {
					try {
						if (cu != null) {
							cu.close();
						}
					} catch (Exception e) {
						MyLog.e("CursorRunner queryFirstResult close cursor fail", e);
					}
				}
			}
			return firstResult;
		}
	}
}
