package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.sqllite.db.ROUTING_CUSTOMER_TABLE;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Mo ta muc dich cua class
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class RoutingCustomerDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -191416297467883748L;
	public long routingCustomerId;
	public long shopId;
	public long customerId;
	public String lastOrder;
	public String lastApproveOrder;
	public long routingId;
	public int synState;

	public RoutingCustomerDTO() {
		super(TableType.ROUTING_CUSTOMER_TABLE);
	}

	/**
	 * Generate cau lenh update
	 * @author: TruongHN
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateFromOrderSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, ROUTING_CUSTOMER_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			if (StringUtil.isNullOrEmpty(lastOrder)) {
				params.put(GlobalUtil.getJsonColumn(ROUTING_CUSTOMER_TABLE.LAST_ORDER, "", DATA_TYPE.NULL.toString()));
			} else {
				params.put(GlobalUtil.getJsonColumn(ROUTING_CUSTOMER_TABLE.LAST_ORDER, lastOrder, null));
			}
			
			if (!StringUtil.isNullOrEmpty(lastApproveOrder)) {
				params.put(GlobalUtil.getJsonColumn(ROUTING_CUSTOMER_TABLE.LAST_APPROVE_ORDER, lastApproveOrder, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(ROUTING_CUSTOMER_TABLE.ROUTING_CUSTOMER_ID, routingCustomerId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
		}
		return json;
	}
}
