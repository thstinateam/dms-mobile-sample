/**
 * Copyright 2012 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * Chua du lieu id,name
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ValueItemDTO  implements Serializable {
	private static final long serialVersionUID = 1L;
	public String id;
	public String value;
	public String name;
}
