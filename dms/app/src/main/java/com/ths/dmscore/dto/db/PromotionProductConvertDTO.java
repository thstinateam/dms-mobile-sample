package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PROMOTION_PRODUCT_CONVERT_TABLE;
import com.ths.dmscore.util.CursorUtil;

public class PromotionProductConvertDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public long promotionProductConvertId;
	public String promotionProductConvName;
	public long promotionProgramId;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public int status;
	
	public PromotionProductConvDtlDTO sourceProduct = new PromotionProductConvDtlDTO();
	public ArrayList<PromotionProductConvDtlDTO> listConvertProduct = new ArrayList<PromotionProductConvDtlDTO>();

	/** Khoi tao tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param cOrder
	 */
	public void initFromCursor(Cursor cOrder) {
		promotionProductConvertId = CursorUtil.getLong(cOrder, PROMOTION_PRODUCT_CONVERT_TABLE.PROMOTION_PRODUCT_CONVERT_ID);
		promotionProgramId = CursorUtil.getLong(cOrder, PROMOTION_PRODUCT_CONVERT_TABLE.PROMOTION_PROGRAM_ID);
	}
}