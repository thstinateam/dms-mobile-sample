/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.util.Calendar;
import java.util.Vector;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.lib.network.http.HTTPListenner;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.network.http.HttpAsyncTask;

/**
 *  Send log len server
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ServerLogger implements HTTPListenner {

	private static volatile ServerLogger logger;

	protected ServerLogger() {

	}

	/**
	* get instance of ServerLogger
	*  @author: TruongHN
	*  @return
	*  @return: ServerLogger
	*  @throws:
	 */
	private static ServerLogger getInstance() {
		if (logger == null) {
			logger = new ServerLogger();
		}
		return logger;
	}


	/**
	*  Send log len server
	*  @author: TruongHN
	*  @param content
	*  @param despcription
	*  @return: void
	*  @throws:
	 */
	public static void sendLog(String content, String despcription, int typeLog) {
		TabletActionLogDTO log = new TabletActionLogDTO();
		if (StringUtil.isNullOrEmpty(content)){
			log.content = "";
		}else{
			log.content = content;
		}
		if (StringUtil.isNullOrEmpty(despcription)){
			log.description = "";
		}else{
			log.description = despcription;
		}
		log.type = typeLog;

		sendLog(log, true);

	}

	/**
	*  Send log len server - kiem tra co can phai luu table log hay khong?
	*  @author: TruongHN
	*  @param content
	*  @param despcription
	*  @param isInsertTableLog
	*  @return: void
	*  @throws:
	 */
	public static void sendLog(String content, String despcription, boolean isInsertTableLog, int typeLog) {
		TabletActionLogDTO log = new TabletActionLogDTO();
		if (StringUtil.isNullOrEmpty(content)){
			log.content = "";
		}else{
			log.content = content;
		}
		if (StringUtil.isNullOrEmpty(despcription)){
			log.description = "";
		}else{
			log.description = despcription;
		}
		log.type = typeLog;
		sendLog(log, isInsertTableLog);

	}

	/**
	 *  Send log len server - mac dinh lay content la ten phuong thuc bi loi
	 *  @author: TruongHN
	 *  @param content
	 *  @param despcription
	 *  @return: void
	 *  @throws:
	 */
	public static void sendLog(String despcription, int typeLog) {
		//bo qua log loi tomcat
		if(despcription != null && !despcription.contains(Constants.APACHE_TOMCAT)){
			TabletActionLogDTO log = new TabletActionLogDTO();
			log.content = GlobalUtil.getCurrentMethod();
			log.description = despcription;
			log.type = typeLog;
			sendLog(log, true);
		}
	}

	/**
	*  Send log len server
	*  @author: TruongHN
	*  @param logMsg
	*  @return: void
	*  @throws:
	 */
	 public static void sendLog(TabletActionLogDTO logMsg, boolean isInsertTableLog) {
		// h hien tai
		int currentHour = DateUtils
				.getCurrentTimeByTimeType(Calendar.HOUR_OF_DAY);
		// cho phep request fullDate log loi tu beginTime - > endTime h
		if (currentHour >= GlobalInfo.getInstance().getBeginTimeAllowSynData()
				&& currentHour < GlobalInfo.getInstance()
						.getEndTimeAllowSynData()) {
			int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			if (logMsg != null
					&& staffId > 0
					&& (!StringUtil.isNullOrEmpty(logMsg.content) || !StringUtil
							.isNullOrEmpty(logMsg.description))) {
				JSONArray jarr = new JSONArray();
				jarr.put(logMsg.generateCreateLogSql());
				String logId = GlobalUtil.generateLogId();

				// send to server
				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr);
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
//				para.add(IntentConstants.INTENT_STAFF_ID_PARA);
//				para.add(staffId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());

				String strJson = com.ths.dmscore.lib.network.http.NetworkUtil.getJSONObject(para).toString();

				ActionEvent e = new ActionEvent();
				e.action = ActionEventConstant.ACTION_SEND_LOG_CLIENT;
				e.userData = logMsg;
				if (isInsertTableLog) {
					// ghi lai log
					LogDTO log = new LogDTO();
					log.setType(AbstractTableDTO.TableType.LOG);
					log.logId = logId;
					log.value = strJson;
					log.state = LogDTO.STATE_NONE;
					log.tableType = LogDTO.TYPE_LOG;
					log.userId = String.valueOf(GlobalInfo.getInstance()
							.getProfile().getUserData().getInheritId());
					e.logData = log;
				}

				HTTPRequest httpReq = new HTTPRequest();
				httpReq.setObserver(getInstance());
				httpReq.setUrl(ServerPath.SERVER_HTTP);
				httpReq.setMethod(Constants.HTTPCONNECTION_POST);
				httpReq.setMethodName("queryController/executeSql");
				httpReq.setContentType(HTTPMessage.CONTENT_JSON);
				httpReq.setDataText(strJson);
				httpReq.setUserData(e);

				// sending to server
				new HttpAsyncTask(httpReq).execute();
			}
		}
	}

	@Override
	public void onReceiveData(HTTPMessage mes) {
		// TODO Auto-generated method stub
		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(mes.getDataText());
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		// check null or empty
		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			UserController.getInstance().handleErrorModelEvent(model);
			return;
		}

		boolean isDefaultSuccess = false;
		try {
			JSONObject json = new JSONObject((String) mes.getDataText());
			JSONObject result = json.getJSONObject("result");
			int errCode = result.getInt("errorCode");
			if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
				isDefaultSuccess = true;
			}

		} catch (JSONException e) {
			// loi
			MyLog.i("ServerLogger", "OnReceive: action:" + e.getMessage());
		} catch (Exception e) {
			// loi
			MyLog.i("ServerLogger", "OnReceive: action:" + e.getMessage());
		} finally {
			// thanh cong, that bai khong gui tra kq ve view nua
			if (isDefaultSuccess) {
				// TH mac dinh la request create/update/delete du lieu
				updateLog(actionEvent, LogDTO.STATE_SUCCESS);
				// model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				// UserController.getInstance().handleModelEvent(model);
			} else {
				// ghi log loi len server
				updateLog(actionEvent, LogDTO.STATE_FAIL);
				// UserController.getInstance().handleErrorModelEvent(model);
			}
		}

	}

	@Override
	public void onReceiveError(HTTPResponse response) {
		// TODO Auto-generated method stub
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(response.getDataText());
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (actionEvent.logData != null){
			LogDTO log = (LogDTO) actionEvent.logData;
			if (log != null && LogDTO.STATE_NONE.equals(log.state)) {
				updateLog(actionEvent, LogDTO.STATE_NEW);
			} else {
				updateLog(actionEvent, LogDTO.STATE_FAIL);
			}
			// neu la loi do mang thi ko thuc hien j tiep theo
		}
	}

	/**
	*  Cap nhat vao bang log
	*  @author: TruongHN
	*  @param actionEvent
	*  @param newState
	*  @return: void
	*  @throws:
	 */
	public void updateLog(ActionEvent actionEvent, String newState){
		try {
			if (actionEvent.userData != null){
				TabletActionLogDTO logMsg = (TabletActionLogDTO) actionEvent.userData;
				if (logMsg != null){
					if (logMsg.type != TabletActionLogDTO.LOG_FORCE_CLOSE){
						// khong can luu khi log force close
						// xu ly chung cho cac request
						if (actionEvent.logData != null){
							LogDTO logDTO = (LogDTO) actionEvent.logData;
							if (logDTO != null && !StringUtil.isNullOrEmpty(logDTO.logId) && logDTO.tableType == LogDTO.TYPE_LOG){
								logDTO.setType(AbstractTableDTO.TableType.LOG);
								if (LogDTO.STATE_NONE.equals(logDTO.state)){
									logDTO.state = newState;
									logDTO.createDate = DateUtils.now();
									logDTO.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
									logDTO.needCheckDate = actionEvent.isNeedCheckTimeServer ? LogDTO.NEED_CHECK_TIME: LogDTO.NO_NEED_CHECK_TIME;
									SQLUtils.getInstance().insert(logDTO);
								}else{
									logDTO.state = newState;
									logDTO.updateDate = DateUtils.now();
									logDTO.updateUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
									if (logDTO.tableType == LogDTO.TYPE_ORDER && !StringUtil.isNullOrEmpty(logDTO.tableId)){
										SQLUtils.getInstance().updateLogWithOrder(logDTO, logDTO.tableId);
									}else{
										SQLUtils.getInstance().update(logDTO);
									}
								}
							}
						}
					}else{
						// save md5
						//GlobalUtil.checksumMD5Database();
						//clear cac thong tin trong ctrinh
						GlobalUtil.getInstance().clearAllData();
					}

				}
			}
		} catch (Exception e) {
			MyLog.e("updateLog", "fail", e);
		}
	}
}
