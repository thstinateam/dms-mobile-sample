package com.ths.dmscore.view.supervisor.customer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

public class SupervisorCustomerListRow extends TableRow implements
		OnClickListener {

	Context context;
	View view;
	TextView tvNum;
	public TextView tvCusCode;
	TextView tvCusName;
	TextView tvAddress;
	private TableRow row;
	public SupervisorCustomerList listener;
	CustomerListItem item;

	public SupervisorCustomerListRow(Context context, SupervisorCustomerList lis) {
		super(context);
		this.context = context;
		listener = lis;
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = vi.inflate(R.layout.layout_supervisor_customer_list_row, this);
		row = (TableRow) view.findViewById(R.id.row);
		row.setOnClickListener(this);
		tvNum = (TextView) view.findViewById(R.id.tvNum);
		tvCusCode = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(
				view, R.id.tvCusCode,
				PriHashMap.PriControl.GSNPP_DSKHHUANLUYEN_CHITIET);
		PriUtils.getInstance().setOnClickListener(tvCusCode, this);
		tvCusName = (TextView) view.findViewById(R.id.tvCusName);
		tvAddress = (TextView) view.findViewById(R.id.tvAdd);
	}

	public void render(int pos, CustomerListItem item) {
		this.item = item;
		tvNum.setText("" + pos);
		tvCusCode.setText(item.aCustomer.getCustomerCode());
		tvCusName.setText(item.aCustomer.getCustomerName());
		tvAddress.setText(item.aCustomer.getStreet());
	}

	@Override
	public void onClick(View v) {
		if (v == row && context != null) {
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
		}
		if (v == tvCusCode) {
			listener.handleVinamilkTableRowEvent(
					ActionEventConstant.GO_TO_CUSTOMER_INFO, tvCusCode, item);
		}
	}

}
