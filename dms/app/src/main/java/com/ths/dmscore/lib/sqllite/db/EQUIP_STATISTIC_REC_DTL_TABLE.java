/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.util.StringUtil;

/**
 * EQUIP_STATISTIC_REC_DTL_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 11:35:24 17-12-2014
 */
public class EQUIP_STATISTIC_REC_DTL_TABLE extends ABSTRACT_TABLE {

	// id dot kiem ke
	public static final String EQUIP_STATISTIC_REC_DTL_ID = "EQUIP_STATISTIC_REC_DTL_ID";
	// id thiet bi
	public static final String EQUIP_STATISTIC_RECORD_ID = "EQUIP_STATISTIC_RECORD_ID";
	// id nhom thiet bi
	public static final String SHOP_ID = "SHOP_ID";
	
	public static final String OBJECT_STOCK_ID = "OBJECT_STOCK_ID";
	// id thiet bi, u ke
	public static final String OBJECT_ID = "OBJECT_ID";
	// trang thai hoat dong cua thiet bi
	public static final String STATUS = "STATUS";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// 1 nhom thiet bi, 2 u ke
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// ngay kiem ke
	public static final String STATISTIC_DATE = "STATISTIC_DATE";
	// lan kiem ke
	public static final String STATISTIC_TIME = "STATISTIC_TIME";
	// sl he thong
	public static final String QUANTITY = "QUANTITY";
	// sl thuc te
	public static final String QUANTITY_ACTUALLY = "QUANTITY_ACTUALLY";
	// 1 kho, 2 KH
	public static final String OBJECT_STOCK_TYPE = "OBJECT_STOCK_TYPE";

	public static final String STAFF_ID = "STAFF_ID";
	
	public static final String DATE_IN_WEEK = "DATE_IN_WEEK";

	public static final String TABLE_NAME = "EQUIP_STATISTIC_REC_DTL";

	public EQUIP_STATISTIC_REC_DTL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;

		this.columns = new String[] {EQUIP_STATISTIC_REC_DTL_ID, EQUIP_STATISTIC_RECORD_ID, SHOP_ID,
				STATUS,CREATE_DATE, CREATE_USER, UPDATE_USER, UPDATE_DATE, 
				OBJECT_TYPE, STATISTIC_DATE, STATISTIC_TIME, QUANTITY,
				QUANTITY_ACTUALLY, OBJECT_STOCK_TYPE, STAFF_ID, DATE_IN_WEEK, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		EquipInventoryDTO item = (EquipInventoryDTO) dto;
		ContentValues value = initDataRowInsert(item);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		EquipInventoryDTO item = (EquipInventoryDTO) dto;
		ContentValues value = initDataRow(item);
		String[] params = { "" + item.idInvenDetail };
		return update(value, EQUIP_STATISTIC_REC_DTL_ID + " = ?", params);
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @author: hoanpd1
	 * @since: 11:36:24 17-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param pro
	 * @return
	 */
	private ContentValues initDataRow(EquipInventoryDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(STATUS, dto.status);
		editedValues.put(SYN_STATE, 0);
		return editedValues;
	}

	/**
	 * @author: DungNX
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	*/
	private ContentValues initDataRowInsert(EquipInventoryDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(EQUIP_STATISTIC_REC_DTL_ID, dto.idInvenDetail);
		editedValues.put(EQUIP_STATISTIC_RECORD_ID, dto.getIdRecordInven());
		editedValues.put(SHOP_ID, dto.getShopId());
		editedValues.put(OBJECT_STOCK_ID, dto.objectStockId);
		if(dto.objectId > 0) {
			editedValues.put(OBJECT_ID, dto.objectId);
		}
		editedValues.put(STATUS, dto.status);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(CREATE_USER, dto.createUser);

		editedValues.put(OBJECT_TYPE, dto.getObjectType());
		editedValues.put(STATISTIC_DATE, dto.statisticDate);
		if (dto.statisticTime > 0) {
			editedValues.put(STATISTIC_TIME, dto.statisticTime);
		}
		if (!StringUtil.isNullOrEmpty(dto.dateInWeek)) {
			editedValues.put(DATE_IN_WEEK, dto.dateInWeek);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(QUANTITY_ACTUALLY, dto.quantityActually);
		editedValues.put(OBJECT_STOCK_TYPE, dto.objectStockType);
		editedValues.put(STAFF_ID, dto.getStaffId());
		editedValues.put(SYN_STATE, 0);
		return editedValues;
	}
}
