package com.ths.dmscore.util;

import android.annotation.SuppressLint;
import android.database.Cursor;

/**
 * Xu ly Parse data from Cursor, Map ten cot
 * 
 * @author: ThangNV31
 * @version: 1.0 
 */
@SuppressLint("DefaultLocale")
public class DMSCursor {
	
	public Cursor cursor;
	private CollumnMapping collumnMapping;
		
	public DMSCursor(Cursor c, CollumnMapping mapping) throws NullPointerException{
		if(c == null) {
			throw new NullPointerException();
		}
		this.cursor = c;
		this.collumnMapping = mapping;
		optimizeCursorColumnName();
	}
	
	public void optimizeCursorColumnName() {
		for (int i = 0; i < cursor.getColumnCount(); i++) {
			cursor.getColumnNames()[i] = cursor.getColumnNames()[i].trim().toUpperCase();
		}
	}
	
	/**
	 * lay Index cua cot
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: int
	 * @throws:  
	 * @param columnName
	 * @return
	 */
	public int getColumnIndex(String columnName) {
		if(columnName == null || columnName.trim().equals("")) {
			return -1;
		}
		if(collumnMapping == null){
			return cursor.getColumnIndex(columnName.trim().toUpperCase());
		}
		return cursor.getColumnIndex(collumnMapping.get(columnName));
	}
	
	
	/**
	 * 
	 * Phan lay DATA
	 * 
	 */
	
	public int getInt(String columnName, int defaultValue) {
		int index = getColumnIndex(columnName);
		if(index == -1) {
			return defaultValue;
		}
		return cursor.getInt(index);
	}
	
	public long getLong(String columnName, long defaultValue) {
		int index = getColumnIndex(columnName);
		if(index == -1) {
			return defaultValue;
		}
		return cursor.getLong(index);
	}
	
	public double getDouble(String columnName, double defaultValue) {
		int index = getColumnIndex(columnName);
		if(index == -1) {
			return defaultValue;
		}
		return cursor.getDouble(index);
	}

	public String getString(String columnName, String defaultValue) {
		int index = getColumnIndex(columnName);
		if(index == -1) {
			return defaultValue;
		}
		String s = cursor.getString(index);
		if(s == null){
			s = defaultValue;
		}
		return s;
	}
	
}