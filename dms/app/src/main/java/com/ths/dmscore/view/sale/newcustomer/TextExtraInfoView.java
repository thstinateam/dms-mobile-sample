/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * class gom 1 text view va 1 edittext
 * Thuoc tinh dong kh
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  2:39:27 PM Jan 27, 2015
 */
public class TextExtraInfoView extends AbstractExtraInfoView implements View.OnTouchListener {

	private TextView tv;
	private VNMEditTextClearable ed;
	private CustomerAttributeViewDTO attrDTO;
	private final BaseFragment listener;
	private final int actionSetDate;
	
	public TextExtraInfoView(Context context, BaseFragment listen, int actionSetDateTime) {
		super(context, R.layout.layout_text_extra_info_view);
		// TODO Auto-generated constructor stub
		listener = listen;
		actionSetDate = actionSetDateTime;
		tv = (TextView) arrView.get(0);
		ed = (VNMEditTextClearable) arrView.get(1);
	}

	@Override
	public Object getDataFromView() {
		// TODO Auto-generated method stub
		CustomerAttributeDetailViewDTO detail = new CustomerAttributeDetailViewDTO();
		detail.attrID = attrDTO.customerAttributeID;
		if(attrDTO.type == CustomerAttributeViewDTO.DATE){
			detail.value = DateUtils.convertDateOneFromFormatToAnotherFormat(ed
					.getText().toString(), DateUtils.DATE_FORMAT_DATE_VN,
					DateUtils.DATE_FORMAT_NOW);
		} else {
			detail.value = ed.getText().toString();
		}
		detail.detailID = attrDTO.attributeDetailID;
		return detail;
	}

	@Override
	public void renderLayout(CustomerAttributeViewDTO dto, boolean isEditMode, String fromView) {
		// TODO Auto-generated method stub
		attrDTO = dto;
		if (dto.mandatory == 1) {
			Class<?> clazz;
			Object fromViewObject = null;
			try {
				clazz = Class.forName(fromView);
				fromViewObject = clazz.newInstance();
			} catch (ClassNotFoundException e) {
				MyLog.e("TextExtraInfoView", "ClassNotFoundException", e);
			} catch (java.lang.InstantiationException e) {
				MyLog.e("TextExtraInfoView", "InstantiationException", e);
			} catch (IllegalAccessException e) {
				MyLog.e("TextExtraInfoView", "IllegalAccessException", e);
			}
			if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
				setSymbol("*", dto.name, ImageUtil.getColor(R.color.RED), tv);
			} else {
				tv.setText(dto.name);
			}
		} else if (dto.mandatory == 0) {
			tv.setText(dto.name);
		}
		ed.setImageClearVisibile(isEditMode);
		ed.setFocusable(isEditMode);
		if (!StringUtil.isNullOrEmpty(dto.dataLength)) {
			int length = Integer.parseInt(dto.dataLength);
			InputFilter[] fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(length);
			ed.setFilters(fArray);
		}
		
		if (dto.type == CustomerAttributeViewDTO.TEXT){
			ed.setInputType(InputType.TYPE_CLASS_TEXT);
			ed.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		} else if (dto.type == CustomerAttributeViewDTO.NUMBER){
			ed.setInputType(InputType.TYPE_CLASS_NUMBER);
			ed.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		} else if (dto.type == CustomerAttributeViewDTO.DATE) {
			ed.setIsHandleDefault(false);
			ed.setOnTouchListener(this);
		}
		ed.setEnabled(isEditMode);
		if(!StringUtil.isNullOrEmpty(dto.enumValue)){
			if (dto.type == CustomerAttributeViewDTO.DATE) {
				ed.setText(DateUtils.convertDateOneFromFormatToAnotherFormat(dto.enumValue, DateUtils.DATE_FORMAT_NOW,
						DateUtils.DATE_FORMAT_DATE_VN));
			} else if (dto.type == CustomerAttributeViewDTO.NUMBER || dto.type == CustomerAttributeViewDTO.TEXT) {
				ed.setText(dto.enumValue);
			}
		}
		tag = dto.customerAttributeID;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(v == ed){
			if(!ed.onTouchEvent(event))
				listener.onEvent(actionSetDate, ed, attrDTO.customerAttributeID);
		}
		return true;
	}

	@Override
	public boolean isCheckData() {
		// TODO Auto-generated method stub
		boolean flag = true;
		if (StringUtil.isNullOrEmpty(ed.getText().toString())) {
			flag = false;
		}
		return flag;
	}
	
	public void setDate(String str){
		ed.setText(str);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return attrDTO.name;
	}
	
	@Override
	public boolean requestFocusView() {
		// TODO Auto-generated method stub
		return ed.requestFocus();
	}

}
