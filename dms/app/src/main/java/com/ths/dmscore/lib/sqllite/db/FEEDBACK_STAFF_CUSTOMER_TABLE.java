/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.FeedbackStaffCustomerDTO;

/**
 * FEEDBACK_STAFF_CUSTOMER.java
 * @author: yennth16
 * @version: 1.0
 * @since:  09:52:30 21-05-2015
 */
public class FEEDBACK_STAFF_CUSTOMER_TABLE extends ABSTRACT_TABLE {
		// id bang
		public static final String FEEDBACK_STAFF_CUSTOMER_ID = "FEEDBACK_STAFF_CUSTOMER_ID";
		// FEEDBACK_STAFF_ID
		public static final String FEEDBACK_STAFF_ID = "FEEDBACK_STAFF_ID";
		// STAFF_ID
		public static final String STAFF_ID = "STAFF_ID";
		// RESULT
		public static final String CUSTOMER_ID = "CUSTOMER_ID";
		// parent staff id
		public static final String CREATE_USER_ID = "CREATE_USER_ID";
		// ngay tao
		public static final String CREATE_DATE = "CREATE_DATE";
		// nguoi update
		public static final String UPDATE_USER = "UPDATE_USER";
		// ngay cap nhat
		public static final String UPDATE_DATE = "UPDATE_DATE";

		public static final String FEEDBACK_STAFF_CUSTOMER_TABLE = "FEEDBACK_STAFF_CUSTOMER";

		public FEEDBACK_STAFF_CUSTOMER_TABLE(SQLiteDatabase mDB) {
			this.tableName = FEEDBACK_STAFF_CUSTOMER_TABLE;
			this.columns = new String[] { FEEDBACK_STAFF_CUSTOMER_ID, FEEDBACK_STAFF_ID,
					STAFF_ID, CUSTOMER_ID, CREATE_USER_ID,
					CREATE_DATE, UPDATE_USER, UPDATE_DATE, SYN_STATE };
			this.sqlGetCountQuerry += this.tableName + ";";
			this.sqlDelete += this.tableName + ";";
			this.mDB = mDB;
		}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		ContentValues value = initDataRow((FeedbackStaffCustomerDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(FeedbackStaffCustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(FEEDBACK_STAFF_CUSTOMER_ID, dto.feedBackStaffCustomerId);
		editedValues.put(FEEDBACK_STAFF_ID, dto.feedBackStaffId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(CREATE_USER_ID, dto.createUserId);
		editedValues.put(CREATE_DATE, dto.createDate);
		return editedValues;
	}

	/**
	 * insert feedback staff
	 * @author: Tuanlt11
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	*/
	public long insertFeedbackStaffCustomer(FeedbackStaffCustomerDTO dto) {
		long returnCode = -1;
			try {
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		return returnCode;
	}

}
