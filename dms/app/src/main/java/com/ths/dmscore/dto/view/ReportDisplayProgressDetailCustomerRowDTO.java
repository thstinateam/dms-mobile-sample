package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

public class ReportDisplayProgressDetailCustomerRowDTO {
	public String groupCode;
	public String groupName;
	public double amountPlan;
	public double amount;
	public long quantityPlan;
	public long quantity;
	public long remainQuantity;
	public double remainAmount;
	public double progressAmount;
	public double progressQuantity;
	public boolean isHeader;
	public int type;
	public static final int TYPE_AMOUNT = 1;
	public static final int TYPE_QUANTITY = 2;
	public static final int TYPE_SKU = 3;


	public ReportDisplayProgressDetailCustomerRowDTO() {
		groupCode = "";
		groupName = "";
		amountPlan = 0;
		amount = 0;
		remainAmount = 0;
		progressAmount = 0;
	}

	/**
	 * parse du lieu cho MH bao cao tien do TTTB cua GSNPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void parseReportDisplayProgressDetailCustomerRow(Cursor c) {
		// TODO Auto-generated method stub
		groupCode = CursorUtil.getString(c, "DISPLAY_DP_GROUP_CODE", "");
		groupName = CursorUtil.getString(c, "DISPLAY_DP_GROUP_NAME", "");
		type = CursorUtil.getInt(c, "TYPE", 0);
		if (type == TYPE_AMOUNT) {
			amount = CursorUtil.getDoubleUsingSysConfig(c, "ACTUAL");
			amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "PLAN");
			remainAmount = StringUtil.calRemainUsingRound(amountPlan, amount);
			progressAmount = StringUtil
					.calPercentUsingRound(amountPlan, amount);
		}else{
			// SL va sku giong nhau
			quantity = CursorUtil.getLong(c, "ACTUAL");
			quantityPlan = CursorUtil.getLong(c, "PLAN");
			remainQuantity = StringUtil.calRemain(quantityPlan, quantity);
			progressQuantity = StringUtil
					.calPercentUsingRound(quantityPlan, quantity);
		}
	}
}
