package com.ths.dmscore.view.graphchart;

import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer.Orientation;

import android.content.Context;
import android.util.AttributeSet;

import com.ths.dmscore.dto.view.ChartDTO;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

public class BarChart extends
		AbstractChartView {

	private XYMultipleSeriesDataset xYDataset;
	
	public BarChart(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

	public BarChart(Context context,
			AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

	public BarChart(Context context,
			AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

	 /**
	 * render du lieu va cau hinh chart
	 * @author: Tuanlt11
	 * @param titleChart
	 * @param columnName
	 * @param rowName
	 * @param listColor
	 * @param annotation
	 * @param lstValues
	 * @param lstColumnName
	 * @return: void
	 * @throws:
	*/
	public void drawChart(String titleChart, String columnName, String rowName,
						  int[] listColor, String[] annotation, List<ChartDTO> lstValues, String[] lstColumnName) {
		setDataValues(annotation,lstValues,lstColumnName);
		buildRenderer(listColor);
		renderer.setBarSpacing(0.5f);
		renderer.setOrientation(Orientation.HORIZONTAL);
		renderer.setBackgroundColor(ImageUtil.getColor(R.color.BG_FRAGMENT));
		setRenderSetting(titleChart, columnName, rowName);
		if (chartView == null) {
			chartView = ChartFactory.getBarChartView(con, xYDataset, renderer,
					Type.DEFAULT);
			addView(chartView, new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));
		}
		setPanLimit(true, false);
	}

	 /**
	 * set value cho cac cot
	 * @author: Tuanlt11
	 * @param annotation
	 * @param listData
	 * @return: void
	 * @throws:
	*/
	private void buildDataSetValues(String[] annotation, List<ChartDTO> listData) {
		int length = annotation.length;
		renderer.setXAxisMax(length + 1);
		arrMaxValues = new double[length];
		for (int i = 0; i < length; i++) {
			CategorySeries series = new CategorySeries(annotation[i]);
			double max = 0;
			for(int j = 0, size = listData.size(); j < size; j++){
				double[] yValues = listData.get(j).value;
				double maxTemp = yValues[0];
				series.add(yValues[i]);
				if (maxTemp < yValues[i])
					maxTemp = yValues[i];
				if(max < maxTemp)
					max = maxTemp;
			}
			arrMaxValues[i] = max;
			xYDataset.addSeries(series.toXYSeries());
		}

		findMaxNumber();
	}
	
	/**
	 * set du lieu de render
	 * 
	 * @author: Tuanlt11
	 * @param listData
	 * @param annotation
	 * @param lstColumnName
	 * @param lstIndexChoose
	 * @return: void
	 * @throws:
	 */
	public void setDataValues(String[] annotation, List<ChartDTO> listData,
			String[] lstColumnName) {
		buildDataSetValues(annotation, listData);
		renderer.setXLabels(0);
		int index = 1;
		int position = 0;// vi tri cua mang ten
		int size = lstColumnName.length;
		while (position < size) {
			if (isShowPlan) {
				renderer.addXTextLabel(index, lstColumnName[position] + "-"
						+ StringUtil.getString(R.string.TEXT_KH));
				index++;
			}
			if (isShowReality) {
				renderer.addXTextLabel(index, lstColumnName[position] + "-"
						+ StringUtil.getString(R.string.TEXT_ACTION));
				index++;
			}
			position++;
		}
	}

}
