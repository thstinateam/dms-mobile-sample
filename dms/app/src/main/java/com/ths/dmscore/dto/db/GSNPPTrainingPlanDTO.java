package com.ths.dmscore.dto.db;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class GSNPPTrainingPlanDTO  implements Serializable {
	private static final long serialVersionUID = -1368907430541950364L;

	public class GSNPPTrainingPlanIem implements Serializable {
		private static final long serialVersionUID = -1593651298543736748L;
		public Date date;
		public String dateString;
		public int staffId;
		public String staffName;
		public int trainDetailId;
		public double score;
		public int shopId;
		public String shopCode;
		public String shopName;
		public String saleTypeCode;

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param c
		 * @return: voidvoid
		 * @throws:
		 */
		public void initFromCursor(Cursor c) {
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;
			trainDetailId = CursorUtil.getInt(c, "TRAINING_PLAN_DETAIL_ID");
			staffId = CursorUtil.getInt(c, "STAFF_ID");
			staffName = CursorUtil.getString(c, "STAFF_NAME");
			score = CursorUtil.getDouble(c, "SCORE");
			shopId = CursorUtil.getInt(c, "SHOP_ID");
			shopCode = CursorUtil.getString(c, "SHOP_CODE");
			shopName = CursorUtil.getString(c, "SHOP_NAME");
			saleTypeCode = CursorUtil.getString(c, "SALE_TYPE_CODE");

			try {
				date = sfs.parse(CursorUtil.getString(c, "TRAINING_DATE"));
				dateString = sfd.format(date);
			} catch (ParseException e) {
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
		}

	}

	public Hashtable<String, GSNPPTrainingPlanIem> listResult = new Hashtable<String, GSNPPTrainingPlanIem>();

	public GSNPPTrainingPlanIem newPlanTrainResultReportItem() {
		return new GSNPPTrainingPlanIem();
	}
	
}
