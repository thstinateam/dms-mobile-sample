/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.graphchart;

import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

/**
 * AbstractChartView.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 11:38:50 AM Oct 3, 2014
 */
public class AbstractChartView
		extends LinearLayout implements
		OnClickListener {

	protected final Context con;

	protected GraphicalView chartView;

	protected final XYMultipleSeriesRenderer renderer;
	
	// array max numbers;
	protected double[] arrMaxValues;
	// co show cot kh hay ko
	public boolean isShowPlan= true;
	// co show cot thuc hien hay ko
	public boolean isShowReality = true;
	
	 /**
	 * Co show cac cot quantity plan, plan hay ko
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void setShowColumn(boolean isShowPlan,boolean isShowReality ){
		this.isShowPlan = isShowPlan;
		this.isShowReality = isShowReality;
	}
	public AbstractChartView(
			Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		con = context;
		renderer = new XYMultipleSeriesRenderer();
	}

	public AbstractChartView(
			Context context,
			AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		con = context;
		renderer = new XYMultipleSeriesRenderer();
	}

	public AbstractChartView(
			Context context,
			AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		con = context;
		renderer = new XYMultipleSeriesRenderer();
	}

	/**
	 * build renderer
	 * 
	 * @author: dungdq3
	 * @since: 2:08:16 PM Oct 3, 2014
	 * @return: void
	 * @throws:
	 * @param colors
	 *            :
	 */
	protected final void buildRenderer(int[] colors) {
		renderer.setXLabelsAlign(Align.CENTER);
		renderer.setYLabelsAlign(Align.RIGHT);
		renderer.setPointSize(5f);
		int length = colors.length;
		for (int i = 0; i < length; i++) {
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors[i]);
			r.setPointStyle(PointStyle.CIRCLE);
			r.setFillPoints(true);
			r.setLineWidth(2f);
			r.setChartValuesTextAlign(Align.CENTER);
			r.setDisplayChartValues(true);
			renderer.addSeriesRenderer(r);
		}
//		renderer.setXLabels(0);
//		renderer.addXTextLabel(1, "hello");
	}

	/**
	 * Chu thich tren truc
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 2:35:59 PM Oct 3, 2014
	 * @return: void
	 * @throws: 
	 * @param textSize:
	 */
	public final void setAxisTextSize(int textSize) {
		renderer.setAxisTitleTextSize(textSize);
	}
	
	public final void setChartTitleTextSize(int textSize){
		renderer.setChartTitleTextSize(textSize);
	}
	
	public final void setLabelTextSize(int textSize){
		renderer.setLabelsTextSize(textSize);
	}
	
	public final void setLegendTextSize(int textSize){
		renderer.setLegendTextSize(textSize);
	}

	/**
	 * set title cho chart, ten cot, ten dong, gia tri min max, mau text va mau row
	 * 
	 * @author: dungdq3
	 * @since: 2:25:38 PM Oct 3, 2014
	 * @return: void
	 * @throws: 
	 * @param titleChart
	 * @param columnName
	 * @param rowName
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @param axesColor: mau truc
	 * @param labelsColor: mau chu
	 */
	protected final void setRenderSetting(String titleChart, String columnName, String rowName) {
		renderer.setZoomButtonsVisible(true);
		renderer.setChartTitle(titleChart);
		renderer.setXTitle(rowName);
		renderer.setYTitle(columnName);
		renderer.setXAxisMin(0);
		renderer.setYAxisMin(0);
		renderer.setYLabelsPadding(10);
		renderer.setAxesColor(ImageUtil.getColor(R.color.BLACK));
		renderer.setLabelsColor(ImageUtil.getColor(R.color.BLACK));
		renderer.setAntialiasing(true);
		renderer.setXLabelsColor(ImageUtil.getColor(R.color.BLACK));
		renderer.setYLabelsColor(0, ImageUtil.getColor(R.color.BLACK));
		renderer.setYLabelsAlign(Align.LEFT);
		renderer.setChartTitleTextSize(16);
		renderer.setLabelsTextSize(16);
		renderer.setAxisTitleTextSize(16);
		renderer.setLegendTextSize(13);
		renderer.setMarginsColor(ImageUtil.getColor(R.color.BG_FRAGMENT));
		// tao goc chu 10 de chu ko cham nhau
		renderer.setXLabelsAngle(10);
		renderer.setInScroll(true);
	}
	
	/**
	 * Co muon dich chuyen chart hay khong? true = co, false = ko
	 * 
	 * @author: dungdq3
	 * @since: 2:23:58 PM Oct 3, 2014
	 * @return: void
	 * @throws: 
	 * @param isPan:
	 */
	public final void setPanLimit(boolean isPan){
		setPanLimit(isPan, isPan);
	}
	
	/**
	 * Co muon dich chuyen chart theo truc x hay truc y hay khong? true = co, false = ko
	 * 
	 * @author: dungdq3
	 * @since: 2:23:58 PM Oct 3, 2014
	 * @return: void
	 * @throws: 
	 * @param isPan:
	 */
	public final void setPanLimit(boolean isPanX, boolean isPanY){
		renderer.setPanEnabled(isPanX, isPanY);
	}

	@Override
	public void onClick(View arg0) {
	}
	
	/**
	 * TIm max values, sau do * 120% de ra duoc so lon nhat
	 * 
	 * @author: dungdq3
	 * @since: 9:38:54 AM Oct 6, 2014
	 * @return: void
	 * @throws: :
	 */
	protected void findMaxNumber() {
		int length = arrMaxValues.length;
		if (length > 0) {
			double maxTemp = arrMaxValues[0];
			for (int i = 0; i < length; i++) {
				if (maxTemp < arrMaxValues[i])
					maxTemp = arrMaxValues[i];
			}
			maxTemp = maxTemp * 1.2;
			renderer.setYAxisMax(maxTemp);
		}
	}
	
	 /**
	 * ve lai
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void repaint() {
		if(chartView!= null){
			chartView.invalidate();
			chartView.repaint();
		}
	}

}
