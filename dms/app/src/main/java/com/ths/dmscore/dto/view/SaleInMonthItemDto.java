package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.RPT_SALE_HISTORY;
import com.ths.dmscore.util.CursorUtil;

public class SaleInMonthItemDto implements Serializable {
	private static final long serialVersionUID = 1L;
	public String month;
	public String numCustomer;
	public int noPSDS;
	public double amount;
	public double amountApproved;
	public double amountPending;
	public long quantityApproved;
	public long quantityPending;
	public long quantity;
	public double numSKU;
	public int rptSaleHisId;

	public SaleInMonthItemDto() {
	}

	public void initDataFromCursor(Cursor c) {
		month = CursorUtil.getString(c, RPT_SALE_HISTORY.MONTH);
		numCustomer =  CursorUtil.getString(c, RPT_SALE_HISTORY.NUM_CUSTOMER);
		noPSDS =  CursorUtil.getInt(c, RPT_SALE_HISTORY.NUM_CUST_NOT_ORDER);
		amount = CursorUtil.getDoubleUsingSysConfig(c,RPT_SALE_HISTORY.AMOUNT);
		amountApproved = CursorUtil.getDoubleUsingSysConfig(c,RPT_SALE_HISTORY.AMOUNT_APPROVED);
		amountPending = CursorUtil.getDoubleUsingSysConfig(c,RPT_SALE_HISTORY.AMOUNT_PENDING);
		quantity = CursorUtil.getLong(c,RPT_SALE_HISTORY.QUANTITY);
		quantityApproved = CursorUtil.getLong(c,RPT_SALE_HISTORY.QUANTITY_APPROVED);
		quantityPending = CursorUtil.getLong(c,RPT_SALE_HISTORY.QUANTITY_PENDING);
		numSKU =  CursorUtil.getDouble(c,RPT_SALE_HISTORY.NUM_SKU);
		rptSaleHisId = CursorUtil.getInt(c, "ID");

	}
}
