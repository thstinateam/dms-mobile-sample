/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;

/**
 * Dong hien thi thong tin item cua ung dung
 * @author: yennth16
 * @version: 1.0
 * @since:  18:56:48 25-12-2014
 */
public class BlackListAppRow extends DMSTableRow implements OnClickListener {
	private int actionSelectedApp = -1;
	//private Context context;
	private ImageView imgIcon;
	private TextView tvAppName;
	private TextView tvPackageName;
	private ApplicationInfo itemApp;
	//su kien click row
	private OnEventControlListener listener;
	GlobalBaseActivity parent;

	public BlackListAppRow(Context context, int actionSelectedApp) {
		super(context,R.layout.layout_black_list_app_row);
		parent = (GlobalBaseActivity)context;
		this.actionSelectedApp = actionSelectedApp;
		listener = (OnEventControlListener) context;
		setOnClickListener(this);
		imgIcon = (ImageView)findViewById(R.id.imgAppIcon);
		tvAppName = (TextView) findViewById(R.id.tvAppName);
		tvPackageName = (TextView) findViewById(R.id.tvPackageName);

	}


	 /**
	 * render layout
	 * @author: Tuanlt11
	 * @param pos
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(int pos, ApplicationInfo item) {
		this.itemApp = item;
		imgIcon.setImageDrawable(item.loadIcon(parent.packageManager));
		tvAppName.setText(item.loadLabel(parent.packageManager));
		tvPackageName.setText("( " + itemApp.packageName + " )");
	}

	@Override
	public void onClick(View v) {
		if(v == this && listener != null && (itemApp != null)){
			listener.onEvent(actionSelectedApp, null, itemApp);
		}
	}
}