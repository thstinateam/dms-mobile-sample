package com.ths.dmscore.view.sale.customer;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

public class CustomerFeedBackRow extends DMSTableRow implements OnClickListener {
	public TextView tvNumer;
	public TextView tvFeedBackDate;
	public TextView tvContent;
	public TextView tvRemindDate;
	public TextView tvDoneDate;
	public TextView tvType;
	public CheckBox cbDone;
	public RelativeLayout llDone;
	public LinearLayout llDelete;
	public ImageView ivDetele;
	FeedBackDTO item;

	public CustomerFeedBackRow(Context context, CustomerFeedBackListView lis) {
		super(context, R.layout.layout_customer_feed_back_row);
		setListener(lis);
		setOnClickListener(this);
		tvNumer = (TextView) findViewById(R.id.tvSTT);
		// tvFeedBackDate = (TextView) findViewById(R.id.tvFeedBackDate);
		tvContent = (TextView) findViewById(R.id.tvContent);
		tvRemindDate = (TextView) findViewById(R.id.tvRemindDate);
		tvDoneDate = (TextView) findViewById(R.id.tvOpDate);
		tvType = (TextView) findViewById(R.id.tvType);
//		ivDetele = (ImageView) findViewById(R.id.ivDelete);
//		cbDone = (CheckBox) findViewById(R.id.cbNote);
		ivDetele = (ImageView) PriUtils.getInstance().findViewById(this, R.id.ivDelete, PriHashMap.PriControl.NVBH_GOPY_XOA);
		cbDone = (CheckBox) PriUtils.getInstance().findViewById(this, R.id.cbNote, PriHashMap.PriControl.NVBH_GOPY_THUCHIEN);
		llDone = (RelativeLayout) findViewById(R.id.llNote);
		llDelete = (LinearLayout) findViewById(R.id.llDelete);

//		ivDetele.setOnClickListener(this);
//		cbDone.setOnClickListener(this);
		PriUtils.getInstance().setOnClickListener(ivDetele, this);
		PriUtils.getInstance().setOnClickListener(cbDone, this);
	}

	public void renderLayout(int pos, FeedBackDTO item, CustomerFeedBackDto dto) {
		this.item = item;
		tvNumer.setText("" + pos);
		tvContent.setText(item.getContent());
		tvType.setText(item.getApParamName());
		if (item.getRemindDate() != null) {
			tvRemindDate.setText(item.getRemindDate());
		} else {
			tvRemindDate.setText("");
		}
		if (!StringUtil.isNullOrEmpty(item.getDoneDate())) {
			tvDoneDate.setText(item.getDoneDate());
			cbDone.setChecked(true);
			cbDone.setEnabled(false);
			// tvContent.setPaintFlags(tvContent.getPaintFlags() |
			// Paint.STRIKE_THRU_TEXT_FLAG);
		} else {
			tvDoneDate.setText("");
			cbDone.setChecked(false);
		}

		if (item.getStaffId() != item.getCreateUserId() || !StringUtil.isNullOrEmpty(item.getDoneDate())) {
//			ivDetele.setVisibility(View.GONE);
			PriUtils.getInstance().setStatus(ivDetele, PriUtils.INVISIBLE);
		} else {
//			ivDetele.setVisibility(View.VISIBLE);
			PriUtils.getInstance().setStatus(ivDetele, PriUtils.ENABLE);
		}

		if (!StringUtil.isNullOrEmpty(item.getDoneDate())) {
			setTextColor(ImageUtil.getColor(R.color.BLACK));
			if (item.getDoneDate().equals(DateUtils.getCurrentDate())) {
				setRowColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			} else {
				setRowColor(ImageUtil.getColor(R.color.WHITE));
			}
		} else if (!StringUtil.isNullOrEmpty(item.getRemindDate())
				&& DateUtils.compareWithNow(item.getRemindDate(), "dd/MM/yyyy") == -1) {
			setBackground(R.drawable.style_row_default);
			setTextColor(ImageUtil.getColor(R.color.RED));
		} else {
			setBackground(R.drawable.style_row_default);
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param styleRowDefault
	 * @return: voidvoid
	 * @throws:
	 */
	private void setBackground(int styleRowDefault) {
		tvNumer.setBackgroundResource(styleRowDefault);
		tvContent.setBackgroundResource(styleRowDefault);
		tvContent.setPadding(GlobalUtil.dip2Pixel(10), 0, GlobalUtil.dip2Pixel(10), 0);
		tvType.setBackgroundResource(styleRowDefault);
		tvRemindDate.setBackgroundResource(styleRowDefault);
		tvDoneDate.setBackgroundResource(styleRowDefault);
		llDone.setBackgroundResource(styleRowDefault);
		ivDetele.setBackgroundResource(styleRowDefault);
		llDelete.setBackgroundResource(styleRowDefault);
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param color
	 * @return: voidvoid
	 * @throws:
	 */
	private void setTextColor(int color) {
		tvNumer.setTextColor(color);
		tvContent.setTextColor(color);
		tvType.setTextColor(color);
		tvRemindDate.setTextColor(color);
		tvDoneDate.setTextColor(color);
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param color
	 * @return: voidvoid
	 * @throws:
	 */
	private void setRowColor(int color) {
		tvNumer.setBackgroundColor(color);
		tvContent.setBackgroundColor(color);
		tvType.setBackgroundColor(color);
		tvRemindDate.setBackgroundColor(color);
		tvDoneDate.setBackgroundColor(color);
		llDone.setBackgroundColor(color);
		ivDetele.setBackgroundColor(color);
		llDelete.setBackgroundColor(color);
	}

	@Override
	public void onClick(View v) {
		if (v == this && context != null) {
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
		}
		if (v == ivDetele) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.DELETE_FEEDBACK, ivDetele, item);
		} else if (v == cbDone) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.UPDATE_FEEDBACK, cbDone, item);
		}
	}
}
