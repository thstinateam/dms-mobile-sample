/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import java.io.File;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.AttachDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.view.filechooser.FileAttachEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.util.FileUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.filechooser.FileAttachViewHolder;
import com.ths.dmscore.view.control.filechooser.FileListDialogView.DownloadFileAttachTask;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * hien thi thong tin chi tiet van de theo doi khac phuc cua gsnpp
 *
 * @author: HieuNH
 * @version: 1.0
 * @since: 1.1
 */
public class SalePopupProblemDetailView extends LinearLayout implements OnClickListener, FileAttachEvent {

	// OnEventControlListener
	public OnEventControlListener listener;
	int actionClose, actionDelete, actionDone;
	// context
	GlobalBaseActivity context;
	// root layout
	public View viewLayout;
	// type problem
	TextView tvTypeProblem;
	// create dayInOrder
	TextView tvCreateDate;
	// remind dayInOrder
	TextView tvRemindDate;
	// problem status
	TextView tvProblemStatus;
	// done dayInOrder
	TextView tvDoneDate;
	// customer info
	TextView tvCustomerInfo;
	// problem content
	TextView tvProblemContent;
	// button close popup
	Button btCloseProblemDetail;
	// button done popup
	Button btDoneProblemDetail;
	// data to display popup
	FeedBackDTO currentDTO;
	LinearLayout llAttach;

	/**
	 * @param context
	 * @param attrs
	 */
	public SalePopupProblemDetailView(GlobalBaseActivity context, OnEventControlListener listener, int actionClose, int actionDelete, int actionDone) {
		super(context);
		this.context = context;
		// this.parent = (SalesPersonActivity) context;

		this.listener = listener;
		this.actionClose = actionClose;
		this.actionDelete = actionDelete;
		this.actionDone = actionDone;
		LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_custom_popup_detail_problem_nvbh_view, null);
		tvTypeProblem = (TextView) viewLayout.findViewById(R.id.tvTypeProblem);
		tvCreateDate = (TextView) viewLayout.findViewById(R.id.tvCreateDate);
		tvRemindDate = (TextView) viewLayout.findViewById(R.id.tvRemindDate);
		tvProblemStatus = (TextView) viewLayout.findViewById(R.id.tvStatus);
		tvDoneDate = (TextView) viewLayout.findViewById(R.id.tvDoneDate);
		tvCustomerInfo = (TextView) viewLayout.findViewById(R.id.tvCustomerInfo);
		tvProblemContent = (TextView) viewLayout.findViewById(R.id.tvProblemContent);
		btCloseProblemDetail = (Button) viewLayout.findViewById(R.id.btCloseProblemDetail);
		btCloseProblemDetail.setOnClickListener(this);

		btDoneProblemDetail = (Button) PriUtils.getInstance().findViewById(viewLayout,
				R.id.btDoneProblemDetail,
				PriHashMap.PriControl.NVBH_CANTHUCHIEN_DATHUCHIENCHITIETVANDE
						);
		PriUtils.getInstance().setOnClickListener(btDoneProblemDetail, this);
		llAttach = (LinearLayout) viewLayout.findViewById(R.id.llAttach);
	}


	/**
	 *
	 * render layout for popup with object SupervisorProblemOfGSNPP
	 *
	 * @param object
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public void renderLayoutWithObject(FeedBackDTO object) {
		if (object == null)
			return;
		currentDTO = object;
		tvTypeProblem.setText(object.getApParamName());
		tvCreateDate.setText(object.getFeedbackStaffDTO().createDate);
		tvRemindDate.setText(object.getRemindDate());
		PriUtils.getInstance().setStatus(btDoneProblemDetail, PriUtils.ENABLE);
		btCloseProblemDetail.setVisibility(View.VISIBLE);

		if (object.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_CREATE) {
			tvProblemStatus.setText(StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_CREATE));
		} else if (object.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE) {
			tvProblemStatus.setText(StringUtil.getString(R.string.TEXT_PROBLEM_HAS_DONE));
			btDoneProblemDetail.setVisibility(View.GONE);
		} else if (object.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE) {
			tvProblemStatus.setText(StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_TBHV_DONE));
			btDoneProblemDetail.setVisibility(View.GONE);
		}


		// display done dayInOrder
		tvDoneDate.setText(object.getFeedbackStaffDTO().doneDate);

		if (!StringUtil.isNullOrEmpty(object.getCustomerCode())) {
			tvCustomerInfo.setText(object.getCustomerCode());
		}else{
			tvCustomerInfo.setText(Constants.STR_BLANK);
		}

		// display content
		tvProblemContent.setText(object.getContent());
		renderAttachFiles();
	}

	private void renderAttachFiles() {
		llAttach.removeAllViews();
		if (this.currentDTO != null && this.currentDTO.getLstAttach() != null && !this.currentDTO.getLstAttach().isEmpty()) {
			for (int i = 0, size = this.currentDTO.getLstAttach().size(); i < size; i++) {
				AttachDTO attachInfo = this.currentDTO.getLstAttach().get(i);
				File file = new File(attachInfo.url);
				FileInfo fileInfo = null;
				//in storage
				if (file.exists()) {
					fileInfo = new FileInfo(file, FileInfo.TYPE_FILE);
				} else{
					String path = ServerPath.IMAGE_PATH + attachInfo.url;
					fileInfo = new FileInfo(path, file.getName(), FileUtil.getFileExtension(file), FileInfo.TYPE_FILE); 
				}
				if(!StringUtil.isNullOrEmpty(attachInfo.title)){
					fileInfo.fileName = attachInfo.title;
				}
				FileAttachViewHolder viewHolder = new FileAttachViewHolder(getContext(), llAttach, this, false);
				viewHolder.render(fileInfo, i);
				llAttach.addView(viewHolder.view);
			}
		}
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (listener != null) {
			if (arg0 == btCloseProblemDetail) {
				listener.onEvent(actionClose, arg0, currentDTO);
			} else if (arg0 == btDoneProblemDetail) {
				listener.onEvent(actionDone, arg0, currentDTO);
			}
		}
	}


	@Override
	public void onRemoveAttach(int posRemove) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onOpenFile(FileInfo info) {
		new DownloadFileAttachTask(info, context).execute();
	}

}
