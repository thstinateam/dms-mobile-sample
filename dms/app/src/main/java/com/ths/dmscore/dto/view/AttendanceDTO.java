/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.STAFF_POSITION_LOG_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;

/**
 * Mo ta muc dich cua lop (interface)
 * 
 * @author: Nguyen Thanh Dung
 * @version: 1.1
 * @since: 1.0
 */

public class AttendanceDTO {
	// id nhan vien
	public int staffId;
	// ma nhan vien
	public String staffCode;
	// ten
	public String name;
	public String time1;
	public String time2;
	public LatLng position1;
	public LatLng position2;
	public double distance1 = -1;
	public double distance2 = -1;
	public String shopId;

	public boolean onTime;

	/**
	 * 
	 */
	public AttendanceDTO() {
		position1 = new LatLng();
		position2 = new LatLng();
		time1 = "";
		time2 = "";
	}

	/**
	 * Mo ta chuc nang cua ham
	 * 
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return: void
	 * @throws:
	 */

	public void initWithCursor(Cursor c, LatLng shopPosition, String shopId) {
		// id
		this.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
		this.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		this.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		this.time1 = CursorUtil.getString(c, STAFF_POSITION_LOG_TABLE.CREATE_DATE);
		this.position1.lat = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LAT);
		this.position1.lng = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LNG);
		if (shopId != null) {
			this.shopId = shopId;
		}
		if (shopPosition != null) {
			if (position1.lat > 0 && position1.lng > 0 && shopPosition.lat > 0
					&& shopPosition.lng > 0) {
				this.distance1 = GlobalUtil.getDistanceBetween(position1, shopPosition);
			}
		}

	}
}
