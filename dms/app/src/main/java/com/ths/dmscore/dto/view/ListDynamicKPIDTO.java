/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * ListDynamicKPIDTO.java
 * @author: dungdq3
 * @version: 1.0 
 * @since:  5:07:45 PM Oct 24, 2014
 */
public class ListDynamicKPIDTO
		implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ArrayList<DynamicKPIDTO> listDynamic;
	private int totalSize;

	public ListDynamicKPIDTO() {
		// TODO Auto-generated constructor stub
		totalSize = 0;
		listDynamic = new ArrayList<DynamicKPIDTO>();
	}

	public ArrayList<DynamicKPIDTO> getListDynamic() {
		return listDynamic;
	}

	public void setListDynamic(ArrayList<DynamicKPIDTO> listDynamic) {
		this.listDynamic = listDynamic;
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

}
