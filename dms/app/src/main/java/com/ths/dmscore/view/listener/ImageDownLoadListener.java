package com.ths.dmscore.view.listener;

import android.graphics.Bitmap;

public interface ImageDownLoadListener {
	public void onStart();
	public void onFinished(Bitmap bitmap);
}
