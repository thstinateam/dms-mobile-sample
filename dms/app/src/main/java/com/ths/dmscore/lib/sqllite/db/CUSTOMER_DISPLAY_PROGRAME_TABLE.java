/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.CustomerDisplayProgrameDTO;
import com.ths.dmscore.dto.view.CustomerAttentProgrameDTO;
import com.ths.dmscore.dto.view.CustomerProgrameDTO;
import com.ths.dmscore.dto.view.DisplayPresentProductInfo;
import com.ths.dmscore.dto.view.ListCustomerAttentProgrameDTO;
import com.ths.dmscore.dto.view.VoteDisplayPresentProductViewDTO;
import com.ths.dmscore.dto.view.VoteDisplayProductDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;

/**
 * Khach hang tham gia CTTB
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_DISPLAY_PROGRAME_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String CUSTOMER_DISPLAY_PROGRAME_ID = "CUSTOMER_DISPLAY_PROGRAME_ID";
	// ma khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// ma CTTB
	public static final String DISPLAY_PROGRAME_CODE = "DISPLAY_PROGRAME_CODE";
	// muc CTTB
	public static final String LEVEL_CODE = "LEVEL_CODE";
	// tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// den ngay
	public static final String TO_DATE = "TO_DATE";
	// trang thai
	public static final String STATUS = "STATUS";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_CUSTOMER_DISPLAY_PROGRAME = "CUSTOMER_DISPLAY_PROGRAME";

	public CUSTOMER_DISPLAY_PROGRAME_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_CUSTOMER_DISPLAY_PROGRAME;
		this.columns = new String[] { CUSTOMER_DISPLAY_PROGRAME_ID,
				CUSTOMER_ID, DISPLAY_PROGRAME_CODE, LEVEL_CODE, FROM_DATE,
				TO_DATE, STATUS, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((CustomerDisplayProgrameDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(CustomerDisplayProgrameDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		CustomerDisplayProgrameDTO dtoDisplay = (CustomerDisplayProgrameDTO) dto;
		ContentValues value = initDataRow(dtoDisplay);
		String[] params = { "" + dtoDisplay.customerDisplayProgrameId };
		return update(value, CUSTOMER_DISPLAY_PROGRAME_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(CUSTOMER_DISPLAY_PROGRAME_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		CustomerDisplayProgrameDTO dtoDisplay = (CustomerDisplayProgrameDTO) dto;
		String[] params = { "" + dtoDisplay.customerDisplayProgrameId };
		return delete(CUSTOMER_DISPLAY_PROGRAME_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public CustomerDisplayProgrameDTO getRowById(String id) {
		CustomerDisplayProgrameDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(CUSTOMER_DISPLAY_PROGRAME_ID + " = ?", params, null,
					null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	private CustomerDisplayProgrameDTO initDTOFromCursor(Cursor c) {
		CustomerDisplayProgrameDTO dto = new CustomerDisplayProgrameDTO();
		dto.customerDisplayProgrameId = (CursorUtil.getInt(c, CUSTOMER_DISPLAY_PROGRAME_ID));
		dto.customerId = (CursorUtil.getString(c, CUSTOMER_ID));
		dto.status = (CursorUtil.getInt(c, STATUS));
		dto.fromDate = (CursorUtil.getString(c, FROM_DATE));
		dto.toDate = (CursorUtil.getString(c, TO_DATE));
		dto.createUser = (CursorUtil.getString(c, CREATE_USER));

		dto.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dto.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dto.updateDate = (CursorUtil.getString(c, UPDATE_DATE));

		return dto;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayPrdogrameLvDTO>
	 * @throws:
	 */
	public Vector<CustomerDisplayProgrameDTO> getAllRow() {
		Vector<CustomerDisplayProgrameDTO> v = new Vector<CustomerDisplayProgrameDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		}
		if (c != null) {
			CustomerDisplayProgrameDTO dto;
			if (c.moveToFirst()) {
				do {
					dto = initDTOFromCursor(c);
					v.addElement(dto);
				} while (c.moveToNext());
			}
		}
		try {
			if (c != null) {
				c.close();
			}
		} catch (Exception e2) {
			// TODO: handle exception
		}
		return v;

	}

	private ContentValues initDataRow(CustomerDisplayProgrameDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(CUSTOMER_DISPLAY_PROGRAME_ID,
				dto.customerDisplayProgrameId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(DISPLAY_PROGRAME_CODE, dto.displayProgrameCode);
		editedValues.put(LEVEL_CODE, dto.levelCode);
		editedValues.put(FROM_DATE, dto.fromDate);
		editedValues.put(TO_DATE, dto.toDate);
		editedValues.put(STATUS, dto.status);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(STATUS, dto.status);

		return editedValues;
	}

	/**
	 * tinh doanh so da dat cua khach hang khi tham gia chuong trih
	 *
	 * @author : BangHN since : 1.0
	 */
	public long caculateAmountAchievedOfPrograme(String customerId,
			String programId, boolean isFino, boolean hasFino) {
		long amount = 0;
		String[] params = new String[] { customerId, programId };

		StringBuffer sql = new StringBuffer();
		sql.append("select ( sum ( case when  so.order_type = 'CM' then -sod.amount");
		sql.append(" when so.ORDER_TYPE = 'CO' then -sod.amount else sod.AMOUNT end)) as amount");
		sql.append(" from sales_order_detail sod, sales_order so, product p, display_program_detail dpd");
		sql.append(" where sod.sale_order_id = so.sale_order_id and  p.product_id = dpd.product_id AND p.product_id = sod.product_id");
		sql.append(" and sod.is_free_item = 0");

		if (hasFino && !isFino) {// neu co fino va dang tinh doanh so nhom hang
									// khac
			// sql.append(" and p.sub_cat is  null");
			params = new String[] { programId, customerId, programId };
			sql.append(" and (case when p.sub_cat is null then 0 else p.sub_cat end) not in (select sub_cat from display_programe_sub_cat where status = 1 and display_program_id = ?)");
		}
		if (isFino) {
			// sql.append(" and p.sub_cat = 'C_Fino'");
			params = new String[] { programId, customerId, programId };
			sql.append(" and (case when p.sub_cat is null then 0 else p.sub_cat end) in (select sub_cat from display_programe_sub_cat where status = 1 and display_program_id = ?)");
		}
		sql.append(" and so.customer_id = ?");
		sql.append(" and dpd.display_program_id = ?");
		sql.append(" and ((so.state = 2 and dayInOrder(so.order_date) < dayInOrder('now','localtime')"); // lay
																								// trong
																								// thang
																								// hien
																								// tai
		sql.append(" and dayInOrder(so.order_date) >= DATE('NOW','localtime','start of month'))");

		sql.append(" or (so.state = 1 and so.is_send = 1 and dayInOrder(so.order_date) = dayInOrder('now','localtime')))");

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					amount = CursorUtil.getLong(c, "amount");
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		return amount;
	}

	/**
	 * Lay danh sach chuong trinh cua khach hang tham gia
	 *
	 * @author : BangHN since : 1.0
	 * @param shopId
	 */
	public ArrayList<CustomerProgrameDTO> getCustomerProgrames(String customerId, String shopId) {
		ArrayList<CustomerProgrameDTO> result = new ArrayList<CustomerProgrameDTO>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		StringBuffer  sql = new StringBuffer();
		sql.append("SELECT display_programe_id, ");
		sql.append("       display_programe_code, ");
		sql.append("       display_programe_level, ");
		sql.append("       display_programe_name, ");
		sql.append("       from_date, ");
		sql.append("       to_date, ");
		sql.append("       amount_plan, ");
		sql.append("       amount, ");
		sql.append("       quantity, ");
		sql.append("       quantity_plan, ");
//		sql.append("       AMOUNT_REMAIN AMOUNT_REMAIN, ");
		sql.append("       result, ");
		sql.append("       Group_concat(DISTINCT product_info_code) cat ");
		sql.append("FROM   (SELECT RDP.display_programe_id, ");
		sql.append("               RDP.display_programe_code, ");
		sql.append("               RDP.display_programe_name, ");
		sql.append("               RDP.display_programe_level, ");
		sql.append("               Strftime('%d/%m/%Y', RDP.from_date) AS from_date, ");
		sql.append("               Strftime('%d/%m/%Y', RDP.to_date)   AS to_date, ");
		sql.append("               RDP.amount_plan, ");
		sql.append("               RDP.amount, ");
		sql.append("               RDP.quantity_plan, ");
		sql.append("               RDP.quantity, ");
//		sql.append("               RDP.amount_remain AMOUNT_REMAIN, ");
		sql.append("               ( RDP.amount_plan - RDP.amount )    AS result, ");
		sql.append("               PI.product_info_code ");
		sql.append("        FROM   rpt_display_program RDP ");
		sql.append("               LEFT JOIN display_program_detail DPD ");
		sql.append("                      ON RDP.[display_programe_id] = DPD.display_program_id ");
		sql.append("               LEFT JOIN product PR ");
		sql.append("                      ON DPD.product_id = PR.product_id ");
		sql.append("               LEFT JOIN product_info PI ");
		sql.append("                      ON PR.cat_id = PI.product_info_id ");
		sql.append("        WHERE  RDP.customer_id = ? ");
		sql.append("               AND RDP.SHOP_ID = ? ");
		sql.append("               AND Ifnull(Date(RDP.to_date) >= ?, 0) ");
		sql.append("               AND ( DPD.product_id IS NULL ");
		sql.append("                      OR ( PR.status = 1 ");
		sql.append("                           AND PI.status = 1 ) ) ");
		sql.append("        ORDER  BY product_info_code) ");
		sql.append("GROUP  BY display_programe_code, ");
		sql.append("          display_programe_name ");



		String[] params = new String[] { customerId, shopId, dateNow };
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					CustomerProgrameDTO customerPrograme;
					do {
						customerPrograme = new CustomerProgrameDTO();
						customerPrograme.initCustomerProgrameDTO(c);
						result.add(customerPrograme);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 * Lay danh sach khach hang tham gia chuong trinh
	 *
	 * @author: ThanhNN8
	 * @param extPage
	 * @param displayProgramCode
	 * @param customer_code
	 * @param customer_name
	 * @return
	 * @return: ListCustomerAttentProgrameDTO
	 * @throws:
	 */
	public ListCustomerAttentProgrameDTO getListCustomerAttentPrograme(
			String extPage, String displayProgramCode, long displayProgrameId, String customer_code,
			String customer_name, int staffId, boolean checkPagging) throws Exception{
		StringBuilder sqlCountQuery = new StringBuilder();
		sqlCountQuery.append("select c.customer_id, c.customer_code, c.customer_name, cdp.level_code, dp.display_program_id, dpl.amount, dp.percent_fino");
		sqlCountQuery.append(" from customer as c, customer_display_programe as cdp, display_program_level as dpl, display_program as dp");
		sqlCountQuery.append(" where cdp.customer_id = c.customer_id");
		sqlCountQuery.append(" and cdp.display_programe_code = dpl.display_program_code");
		sqlCountQuery.append(" and cdp.display_programe_code = dp.display_program_code");
		sqlCountQuery.append(" and cdp.level_code = dpl.level_code");
		sqlCountQuery.append(" and c.status = 1");
		sqlCountQuery.append(" and cdp.status = 1");
		sqlCountQuery.append(" and dpl.status = 1");
		sqlCountQuery.append(" and (SELECT dayInOrder('now','localtime')) >= dayInOrder(cdp.from_date)");
		sqlCountQuery.append(" and ((SELECT dayInOrder('now','localtime')) <= dayInOrder(cdp.to_date) or cdp.to_date is null)");
		sqlCountQuery.append(" and cdp.display_programe_code = ?");
		sqlCountQuery.append(" and cdp.staff_id = ?");
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT c.customer_id, c.customer_code, c.customer_name, cdp.level_code, dp.display_program_id, dpl.amount, dp.percent_fino,");
		sqlQuery.append(" (case when dp.percent_fino is not null then");
		sqlQuery.append(" (SELECT (SUM (CASE WHEN so.order_type = 'CM' THEN -sod.amount WHEN so.ORDER_TYPE = 'CO' THEN -sod.amount ELSE sod.AMOUNT END)) AS amount");
		sqlQuery.append(" FROM sales_order_detail sod, sales_order so, product p, display_programe_detail dpd");
		sqlQuery.append(" WHERE sod.sale_order_id = so.sale_order_id AND p.product_id = dpd.product_id AND p.product_id = sod.product_id AND sod.is_free_item = 0");
		sqlQuery.append(" AND p.SUB_CAT is not null AND exists(select 1 from display_programe_sub_cat where status = 1 and display_program_id = ? and sub_cat = p.sub_cat)");
		sqlQuery.append(" AND so.customer_id = c.customer_id AND dpd.display_program_id = ?");
		sqlQuery.append(" AND ((so.state = 2 AND dayInOrder(so.order_date) < dayInOrder('now','localtime') AND dayInOrder(so.order_date) >= DATE('NOW','localtime','start of month'))");
		sqlQuery.append(" OR (so.state = 1 AND so.is_send = 1 AND dayInOrder(so.order_date) = dayInOrder('now','localtime'))))");
		sqlQuery.append(" else 0 end) as AMOUT_FINO,");
		sqlQuery.append(" (SELECT (SUM (CASE WHEN so.order_type = 'CM' THEN -sod.amount WHEN so.ORDER_TYPE = 'CO' THEN -sod.amount ELSE sod.AMOUNT END)) AS amount");
		sqlQuery.append(" FROM sales_order_detail sod, sales_order so, product p, display_program_detail dpd");
		sqlQuery.append(" WHERE sod.sale_order_id = so.sale_order_id AND p.product_id = dpd.product_id AND p.product_id = sod.product_id AND sod.is_free_item = 0");
		sqlQuery.append(" AND not exists(select 1 from display_programe_sub_cat where status = 1 and display_program_id = ? and sub_cat = p.sub_cat)");
		sqlQuery.append(" AND so.customer_id = c.customer_id AND dpd.display_program_id = ?");
		sqlQuery.append(" AND ((so.state = 2 AND dayInOrder(so.order_date) < dayInOrder('now','localtime') AND dayInOrder(so.order_date) >= DATE('NOW','','localtime'))");
		sqlQuery.append(" OR (so.state = 1 AND so.is_send = 1 AND dayInOrder(so.order_date) = dayInOrder('now','localtime')))) as AMOUNT_OTHERS");
		sqlQuery.append(" FROM customer c, customer_display_programe cdp, display_program_level dpl, display_program dp");
		sqlQuery.append(" WHERE cdp.customer_id = c.customer_id AND cdp.display_programe_code = dpl.display_program_code");
		sqlQuery.append(" AND cdp.display_programe_code = dp.display_program_code AND cdp.level_code = dpl.level_code AND c.status = 1 AND cdp.status = 1");
		sqlQuery.append(" AND dayInOrder('now','localtime') >= dayInOrder(cdp.from_date) AND (dayInOrder('now','localtime') <= dayInOrder(cdp.to_date) OR cdp.to_date IS NULL)");
		sqlQuery.append(" AND cdp.display_programe_code = ? AND cdp.staff_id = ?");
		/**
		 * SELECT c.customer_id, c.customer_code, c.customer_name,
		 * cdp.level_code, dp.display_programe_id, dpl.amount, dp.percent_fino,
		 * (case when dp.percent_fino is not null then (
		 *
		 * SELECT (SUM (CASE WHEN so.order_type = 'CM' THEN -sod.amount WHEN
		 * so.ORDER_TYPE = 'CO' THEN -sod.amount ELSE sod.AMOUNT END)) AS amount
		 * FROM sales_order_detail sod, sales_order so, product p,
		 * display_programe_detail dpd WHERE sod.sale_order_id =
		 * so.sale_order_id AND p.product_id = dpd.product_id AND p.product_id =
		 * sod.product_id AND sod.is_free_item = 0 AND p.SUB_CAT is not null AND
		 * exists(select 1 from display_programe_sub_cat where status = 1 and
		 * display_programe_id = ? and sub_cat = p.sub_cat) AND so.customer_id =
		 * c.customer_id AND dpd.display_programe_id = ? AND ((so.state = 2 AND
		 * dayInOrder(so.order_date) < dayInOrder('now','localtime') AND dayInOrder(so.order_date)
		 * >= DATE('NOW','localtime','start of month')) OR (so.state = 1 AND
		 * so.is_send = 1 AND dayInOrder(so.order_date) = dayInOrder('now','localtime')))
		 *
		 * )
		 *
		 * else 0 end) as AMOUT_FINO, (
		 *
		 * SELECT (SUM (CASE WHEN so.order_type = 'CM' THEN -sod.amount WHEN
		 * so.ORDER_TYPE = 'CO' THEN -sod.amount ELSE sod.AMOUNT END)) AS amount
		 * FROM sales_order_detail sod, sales_order so, product p,
		 * display_programe_detail dpd WHERE sod.sale_order_id =
		 * so.sale_order_id AND p.product_id = dpd.product_id AND p.product_id =
		 * sod.product_id AND sod.is_free_item = 0 AND not exists(select 1 from
		 * display_programe_sub_cat where status = 1 and display_programe_id = ?
		 * and sub_cat = p.sub_cat) AND so.customer_id = c.customer_id AND
		 * dpd.display_programe_id = ? AND ((so.state = 2 AND
		 * dayInOrder(so.order_date) < dayInOrder('now','localtime') AND dayInOrder(so.order_date)
		 * >= DATE('NOW','localtime','start of month')) OR (so.state = 1 AND
		 * so.is_send = 1 AND dayInOrder(so.order_date) = dayInOrder('now','localtime')))
		 *
		 * ) as AMOUNT_OTHERS FROM customer AS c, customer_display_programe AS
		 * cdp, display_programe_level AS dpl, display_programe AS dp WHERE
		 * cdp.customer_id = c.customer_id AND cdp.display_programe_code =
		 * dpl.display_programe_code AND cdp.display_programe_code =
		 * dp.display_programe_code AND cdp.level_code = dpl.level_code AND
		 * c.status = 1 AND cdp.status = 1 AND dpl.status = 1 AND
		 * dayInOrder('now','localtime') >= dayInOrder(cdp.from_date) AND
		 * (dayInOrder('now','localtime') <= dayInOrder(cdp.to_date) OR cdp.to_date IS NULL)
		 * AND cdp.display_programe_code = ? AND cdp.staff_id = ? ORDER BY
		 * c.customer_code, c.customer_name
		 */
		List<String> params = new ArrayList<String>();
		params.add(displayProgrameId + "");
		params.add(displayProgrameId + "");
		params.add(displayProgrameId + "");
		params.add(displayProgrameId + "");
		params.add(displayProgramCode);
		params.add("" + staffId);
		//for count sql query
		List<String> countparams = new ArrayList<String>();
		countparams.add(displayProgramCode);
		countparams.add("" + staffId);

		if (customer_code.length() > 0) {
			sqlCountQuery.append(" and LOWER(substr(c.customer_code,1,3)) like ?");
			sqlQuery.append(" and LOWER(substr(c.customer_code,1,3)) like ?");
			params.add("%" + customer_code.toLowerCase() + "%");
			countparams.add("%" + customer_code.toLowerCase() + "%");
		}
		if (customer_name.length() > 0) {
			sqlCountQuery.append(" and LOWER(c.customer_name_address_text) like ?");
			sqlQuery.append(" and LOWER(c.customer_name_address_text) like ?");
			params.add("%" + customer_name.toLowerCase() + "%");
			countparams.add("%" + customer_name.toLowerCase() + "%");
		}
		String getCountProductList = " select count(*) as total_row from ("
				+ sqlCountQuery.toString() + ") ";

		sqlQuery.append(" order by c.customer_code, c.customer_name");

		String queryGetListCustomerAttentPrograme = sqlQuery.toString();
		ListCustomerAttentProgrameDTO result = new ListCustomerAttentProgrameDTO();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			String[] arrCountParam = new String[countparams.size()];
			for (int i = 0, lengcount = countparams.size(); i < lengcount; i++) {
				arrCountParam[i] = countparams.get(i);
			}
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList, arrCountParam);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					result.setTotalSize(total);
					MyLog.v("total item: ", "= " + total);
				}
			}
			// end
			ArrayList<CustomerAttentProgrameDTO> listCustomer = new ArrayList<CustomerAttentProgrameDTO>();
			c = rawQuery(queryGetListCustomerAttentPrograme + extPage, arrParam);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						CustomerAttentProgrameDTO customerAttent = new CustomerAttentProgrameDTO();
						customerAttent.initDataFromCursor(c);
						listCustomer.add(customerAttent);
					} while (c.moveToNext());
				}
			}
			result.setListCustomer(listCustomer);

		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return result;
	}

	/**
	 *
	 * Lay so luong khach hang tham gia chuong trinh
	 *
	 * @author: ThanhNN8
	 * @param programe_code
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getCountCustomerAttentPrograme(String programe_code,
			String staffId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select c.customer_id, c.customer_code, c.customer_name, cdp.level_code, dp.display_program_id, dpl.amount, dp.percent_fino");
		sqlQuery.append(" from customer as c, customer_display_programe as cdp, display_program_level as dpl, display_program as dp");
		sqlQuery.append(" where cdp.customer_id = c.customer_id");
		sqlQuery.append(" and cdp.display_programe_code = dpl.display_program_code");
		sqlQuery.append(" and cdp.display_programe_code = dp.display_program_code");
		sqlQuery.append(" and c.status = 1");
		sqlQuery.append(" and cdp.level_code = dpl.level_code");
		sqlQuery.append(" and cdp.status = 1");
		sqlQuery.append(" and dpl.status = 1");
		sqlQuery.append(" and (SELECT dayInOrder('now','localtime')) >= dayInOrder(cdp.from_date)");
		sqlQuery.append(" and ((SELECT dayInOrder('now','localtime')) <= dayInOrder(cdp.to_date) or cdp.to_date is null)");
		sqlQuery.append(" and cdp.display_programe_code = ?");
		sqlQuery.append(" and cdp.staff_id = ?");
		String queryGetCountCustomerAttentPrograme = sqlQuery.toString();
		String[] params = new String[] { programe_code, staffId };
		String getCountProductList = " select count(*) as total_row from ("
				+ queryGetCountCustomerAttentPrograme + ") ";
		int result = 0;
		Cursor cTmp = null;
		try {
			// get total row first
			cTmp = rawQuery(getCountProductList, params);
			if (cTmp != null) {
				cTmp.moveToFirst();
				result = cTmp.getInt(0);
			}
			// end
		} catch (Exception e) {
			return 0;
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return result;
	}

	/**
	 * get list display program product with customerId
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: List<DisplayPresentProductInfo>
	 * @throws:
	 */
	public ArrayList<DisplayPresentProductInfo> getListVoteDisplayProgram(
			Bundle data) throws Exception {
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);

		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer  var1 = new StringBuffer();
		var1.append("               SELECT distinct ST_DPL.level_code           AS LEVEL_CODE, ");
		var1.append("               ST_DP.DISPLAY_PROGRAM_ID  AS DISPLAY_PROGRAM_ID, ");
		var1.append("               ST_DP.display_program_code AS DISPLAY_PROGRAM_CODE, ");
		var1.append("               ST_DP.display_program_name AS DISPLAY_PROGRAM_NAME ");
		var1.append("               FROM display_program AS ST_DP, ");
		var1.append("               display_staff_map AS ST_DSM, ");
		var1.append("               display_program_level AS ST_DPL, ");
		var1.append("               display_customer_map AS ST_DCM, ");
		var1.append("               DISPLAY_SHOP_MAP AS ST_DSHM ");
		var1.append("               WHERE ST_DP.display_program_id = ST_DSM.display_program_id ");
		var1.append("               AND ST_DP.display_program_id = ST_DPL.display_program_id ");
		var1.append("               AND ST_DSM.display_staff_map_id = ST_DCM.display_staff_map_id ");
		var1.append("               AND ST_DPL.display_program_level_id = ST_DCM.display_program_level_id ");
		var1.append("               AND ST_DP.display_program_id = ST_DSHM.display_program_id ");
		var1.append("               AND DATE(ST_DP.from_date) <= Date('now', 'localtime') ");
		var1.append("               AND IFNULL(DATE(ST_DP.to_date) >=  Date('now', 'localtime'), 1) ");
		var1.append("               AND ST_DP.STATUS = 1 ");
		var1.append("               AND ST_DSM.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("               AND ST_DCM.SHOP_ID = ? ");
		params.add(shopId);
		var1.append("               AND ST_DSM.STAFF_ID = ? AND DATE(ST_DSM.MONTH, 'localtime', 'start of month') = DATE('now', 'localtime', 'start of month') ");
		params.add(String.valueOf(staffId));
		var1.append("               AND ST_DSM.STATUS = 1 ");
		var1.append("               AND ST_DPL.STATUS = 1 ");
		var1.append("               AND ST_DCM.CUSTOMER_ID = ? ");
		params.add(String.valueOf(customerId));
		var1.append("               AND DATE(ST_DCM.from_date) <= Date('now', 'localtime') ");
		var1.append("               AND IFNULL(DATE(ST_DCM.to_date) >=  Date('now', 'localtime'), 1) ");
		var1.append("               AND ST_DCM.STATUS = 1 ");
		var1.append("                                 AND ST_DP.DISPLAY_PROGRAM_ID NOT IN (SELECT ");
		var1.append("                                     OBJECT_ID AS DISPLAY_PROGRAME_ID ");
		var1.append("                                                                    FROM ");
		var1.append("                                     ACTION_LOG ");
		var1.append("                                                                    WHERE ");
		var1.append("                                     OBJECT_TYPE = 2 ");
		var1.append("                                     AND STAFF_ID = ? ");
		params.add(staffId);
		var1.append("                                     AND SHOP_ID = ? ");
		params.add(shopId);
		var1.append("                                     AND DATE(START_TIME) <= DATE(?) ");
		params.add(date_now);
		var1.append("                                     AND CUSTOMER_ID = ST_DCM.CUSTOMER_ID ");
		var1.append("                                     AND (DATE(END_TIME) >= DATE(?) ");
		params.add(date_now);
		var1.append("                                          OR END_TIME IS NULL ");
		var1.append("                                         )) ");
		var1.append("ORDER  BY DISPLAY_PROGRAM_CODE ");



		ArrayList<DisplayPresentProductInfo> listResult = new ArrayList<DisplayPresentProductInfo>();
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayPresentProductInfo orderJoinTableDTO = new DisplayPresentProductInfo();
						orderJoinTableDTO.initDisplayPresentProductInfo(c);
						listResult.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}

		} finally {
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}

		return listResult;
	}

	/**
	 * get list vote display product
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: VoteDisplayPresentProductViewDTO
	 * @throws:
	 */
	public VoteDisplayPresentProductViewDTO getListVoteDisplayProduct(
			Bundle data) throws Exception {
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String displayProgramId = data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String levelCode = data.getString(IntentConstants.INTENT_LEVEL_CODE);
		boolean isGetCount = data.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ST_D_PDG.display_product_group_id, ");
		var1.append("       ST_D_PDG.display_dp_group_code, ");
		var1.append("       ST_D_PDG.display_dp_group_name, ");
		var1.append("       ST_DPGD.product_id, ");
		var1.append("       ST_DPDD.quantity, ");
		var1.append("       P.product_code, ");
		var1.append("       P.convfact, ");
		var1.append("       P.product_name ");
		var1.append("FROM   display_product_group ST_D_PDG, ");
		var1.append("       display_product_group_dtl ST_DPGD, ");
		var1.append("       display_program_level ST_DPL, ");
		var1.append("       product P ");
		var1.append("       LEFT JOIN display_pl_dp_dtl ST_DPDD ");
		var1.append("              ON ST_DPDD.display_product_group_dtl_id = ");
		var1.append("                 ST_DPGD.display_product_group_dtl_id ");
		var1.append("                 AND ST_DPDD.display_program_level_id = ");
		var1.append("                     ST_DPL.display_program_level_id ");
		var1.append("                 AND ST_DPDD.status = 1 ");
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND ST_D_PDG.status = 1 ");
		var1.append("       AND ST_D_PDG.type = 4 ");
		var1.append("       AND ST_D_PDG.display_program_id = ? ");
		params.add(displayProgramId);
		var1.append("       AND ST_DPL.display_program_id = ST_D_PDG.display_program_id ");
		var1.append("       AND ST_DPL.level_code = ? ");
		params.add(levelCode);
		var1.append("       AND ST_D_PDG.display_product_group_id = ST_DPGD.display_product_group_id ");
		var1.append("       AND ST_DPGD.product_id = P.product_id ");
		var1.append("       AND ST_DPGD.status = 1 ");
		var1.append("       AND ST_DPL.status = 1 ");
		var1.append("       AND P.status = 1 ");

		StringBuilder sqlTotal = new StringBuilder();
		sqlTotal.append("SELECT COUNT(*) FROM (");
		sqlTotal.append(var1.toString());
		sqlTotal.append(" )");
		var1.append("	ORDER BY DISPLAY_DP_GROUP_CODE, PRODUCT_CODE ");
		var1.append(ext);

		VoteDisplayPresentProductViewDTO result = new VoteDisplayPresentProductViewDTO();
		Cursor c = null;
		Cursor cTotal = null;
		ArrayList<VoteDisplayProductDTO> listGroup = new ArrayList<VoteDisplayProductDTO>();
		try {
				c = rawQuery(var1.toString(), params.toArray(new String[params.size()]));
				if(c.moveToFirst()) {
					do {
						VoteDisplayProductDTO displayProduct = new VoteDisplayProductDTO();
						displayProduct.initVoteDisplayProduct(c);
						boolean hasGroup = false;
						for(VoteDisplayProductDTO group : listGroup) {
							if(group.groupCode.equals(displayProduct.groupCode)) {
								hasGroup = true;
								break;
							}
						}

						if(!hasGroup) {
							VoteDisplayProductDTO groupProduct = new VoteDisplayProductDTO();
							groupProduct.groupCode = displayProduct.groupCode;
							groupProduct.groupName = displayProduct.groupName;
							groupProduct.isGroup = true;
							listGroup.add(groupProduct);
							result.listProductDisplay.add(groupProduct);
						}

						result.listProductDisplay.add(displayProduct);
					} while(c.moveToNext());
				}

				if(isGetCount) {
					cTotal = rawQuery(sqlTotal.toString(), params.toArray(new String[params.size()]));
					if(cTotal.moveToFirst()) {
						result.totalProductDisplay = cTotal.getInt(0);
					}
				}
		} finally {
			try{
				if (c != null){
					c.close();
				}

				if(cTotal != null) {
					cTotal.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
			}

		}
		return result;
	}

	/**
	 *
	 * get number display programe use voted
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getNumDisplayProgrameUseVoted(Bundle data) throws Exception {
		int kq = 0;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);

		StringBuffer  queryGetListProductForOrder = new StringBuffer();
		queryGetListProductForOrder.append("SELECT Count(*) AS TOTAL_ROW ");
		queryGetListProductForOrder.append("FROM   (SELECT object_id AS DISPLAY_PROGRAME_ID ");
		queryGetListProductForOrder.append("        FROM   action_log ");
		queryGetListProductForOrder.append("        WHERE  object_type = 2 ");
		queryGetListProductForOrder.append("               AND staff_id = ? ");
		queryGetListProductForOrder.append("               AND Date(start_time) <= Date('NOW', 'localtime') ");
		queryGetListProductForOrder.append("               AND ( Date(end_time) >= Date('NOW', 'localtime') ");
		queryGetListProductForOrder.append("                      OR end_time IS NULL ) ");
		queryGetListProductForOrder.append("               AND customer_id = ?) ");

		String[] params = new String[] { staffId, customerId };

		Cursor c = null;
		try {
			c = rawQuery(queryGetListProductForOrder.toString(), params);
			if (c != null && c.moveToFirst()) {
				kq = c.getInt(0);
			}
		}  finally {
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		return kq;
	}


	/**
	 *
	*  lay thong tin d/s chuong trinh trung bay va d/s 10 san pham cho chuong trinh trung bay dau tien
	*  @author: HaiTC3
	*  @param data
	*  @return
	*  @return: VoteDisplayPresentProductViewDTO
	*  @throws:
	 */
	public VoteDisplayPresentProductViewDTO getVoteDisplayPresentProductView(Bundle data) throws Exception{
		VoteDisplayPresentProductViewDTO result = new VoteDisplayPresentProductViewDTO();
		result.listDisplayProgrameInfo = this.getListVoteDisplayProgram(data);
		if(result.listDisplayProgrameInfo != null && result.listDisplayProgrameInfo.size() > 0){
			data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, result.listDisplayProgrameInfo.get(0).displayProgrameID);
			data.putString(IntentConstants.INTENT_LEVEL_CODE, result.listDisplayProgrameInfo.get(0).joinLevel);
			data.putBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM, true);
			data.putString(IntentConstants.INTENT_PAGE, " limit 0, 10 ");
			VoteDisplayPresentProductViewDTO resultTMP = new VoteDisplayPresentProductViewDTO();
			resultTMP = getListVoteDisplayProduct(data);
			result.listProductDisplay = resultTMP.listProductDisplay;
			result.totalProductDisplay = resultTMP.totalProductDisplay;
		}
		result.numDisplayVoted = this.getNumDisplayProgrameUseVoted(data);
		return result;
	}
}
