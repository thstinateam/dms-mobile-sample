/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteException;
import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.commonsware.cwac.cache.HashMapManager;
import com.commonsware.cwac.cache.MemoryUtils;
import com.ths.dmscore.controller.SynDataController;
import com.ths.dmscore.dto.db.RoleUserDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.LogFile;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.control.SpinnerLanguagesAdapter;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.DBVersionDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.syndata.SynDataDTO;
import com.ths.dmscore.dto.view.LanguagesDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.network.http.HTTPClient;
import com.ths.dmscore.lib.sqllite.download.DownloadFile;
import com.ths.dmscore.lib.sqllite.download.DownloadFile.DMSDownloadException;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.SqlCipherUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.util.locating.PositionManager;
import com.ths.dms.R;
import com.viettel.maps.util.AppInfo;

/**
 * Hien thi man hinh login
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class LoginView extends GlobalBaseActivity implements OnClickListener,  OnItemSelectedListener {
	public static final String PREFS_PRIVATE = "com.viettel.vinamilk";
	public static final String VNM_USER_NAME = "com.viettel.vinamilk.username";
	public static final String VNM_USER_AUTH_DATA = "com.viettel.vinamilk.auth";
	public static final String VNM_PASSWORD = "com.viettel.vinamilk.password";
	public static final String VNM_INHERIT_ID = "com.viettel.vinamilk.inheritId";
	public static final String VNM_STAFF_ID = "com.viettel.vinamilk.staffId";
	public static final String VNM_SHOP_ID = "com.viettel.vinamilk.shopId";
	public static final String VNM_SHOP_ID_PROFILE = "com.viettel.vinamilk.shopIdProfile";
	public static final String VNM_LAST_SHOP_LOGIN = "com.viettel.vinamilk.lastShopLogin";
	public static final String VNM_VER_DB = "com.viettel.vinamilk.verdb";
	public static final String VNM_SV_DATE = "com.viettel.vinamilk.getdate";
	private static final String VNM_STATUS_DOWNLOAD_DB = "com.viettel.vinamilk.statusdb";
	public static final String LAST_LOG_ID = "com.viettel.vinamilk.lastLogId";
	public static final String MAX_LAST_LOG_ID = "com.viettel.vinamilk.maxDBLogId";
	public static final String VNM_SHOP_LAT = "com.viettel.vinamilk.shopLat";
	public static final String VNM_SHOP_LNG = "com.viettel.vinamilk.shopLng";
	public static final String VNM_SHOP_CODE = "com.viettel.vinamilk.shopCode";
	public static final String VNM_ROLE_ID = "com.viettel.vinamilk.roleId";
	public static final String VNM_ALLOW_OFFLINE_MODE = "com.viettel.vinamilk.allowConnectionOffline";
	public static final String VNM_DISPLAY_NAME = "com.viettel.vinamilk.displayname";
	// phuc vu viec kiem tra thoi gian
	public static final String DMS_LAST_LOGIN_ONLINE = "com.viettel.vinamilk.lastLoginOnline";
	public static final String DMS_LAST_LOGIN_ONLINE_FROM_BOOT = "com.viettel.vinamilk.lastLoginOnlineSinceBoot";
	public static final String DMS_LAST_RIGHT_TIME = "com.viettel.vinamilk.lastRightTime";

	// public static final String VNM_ROLE_TYPE =
	// "com.viettel.vinamilk.roleType";
	public static final String VNM_CHANEL_TYPE = "com.viettel.vinamilk.chanelType";
	public static final String VNM_CHANEL_OBJECT_TYPE = "com.viettel.vinamilk.chanelObjectType";
	public static final String VNM_STAFF_OWNER_ID = "com.viettel.vinamilk.staffOwnerId";
	public static final String VNM_STAFF_CODE = "com.viettel.vinamilk.staffCode";
	public static final String VNM_SALE_TYPE_CODE = "com.viettel.vinamilk.saleTypeCode";
	public static final String VNM_SERVER_DATE_SYNDATA = "com.viettel.vinamilk.serverdateSynData";
	public static final String VNM_MD5_CHECKSUM_DB = "com.viettel.vinamilk.md5ChecksumDB";
	public static final String VNM_IGNORE_MSG_TAKE_PICTURE_DP = "com.viettel.vinamilk.ignoreTakePicture";
	public static final String VNM_STAFF_TYPE_NAME = "com.viettel.vinamilk.staffTypeName";
	private static final String VNM_NEED_CHANGE_PASS = "need_change_pass";

	// PAY_RECEIVED_NUMBER
	public static final String VNM_PAY_RECEIVED_NUMBER = "com.viettel.vinamilk.payReceivedNumber";
	public static final String DMS_OLD_VERSION = "com.viettel.vinamilk.oldVersion";

	//proccess type when start download native lib
	private static final int PROCCESS_LOGIN_ONLINE = 1;
	private static final int PROCCESS_UPDATE_VERSION_FAIL = 2;
	private static final int PROCCESS_LOGIN_OFFLINE = 3;
	private static final int PROCCESS_NEED_UPDATE_VERSION = 4;

	private final int ACTION_UPDATE_VERSION_SUCCESS = 1;
	private final int ACTION_UPDATE_VERSION_FAIL = 2;
	// download db
	private final int ACTION_DOWNLOAD_DB_OK = 3;
	private final int ACTION_DOWNLOAD_DB_CANCEL = 4;
	private static final int ACTION_CHANGE_PASS = 5;
	private static final int ACTION_LOGIN_WITH_NEW_PASS = 6;
	public static final String VNM_STAFF_TYPE_ID = "staff_type_id";
	// update DB
	private final int ACTION_UPDATE_DB_OK = 101;
	private final int ACTION_UPDATE_DB_CANCEL = 102;
	// tai lieu du lieu moi
	private final int ACTION_RE_DOWNLOAD_DB_OK = 103;
	private final int ACTION_RE_DOWNLOAD_DB_CANCEL = 104;
	// cancle login
	private final int ACTION_CANCEL_LOGIN_OK = 105;
	// action sau khi chon NPP quan ly
	private final int ACTION_SELECTED_SHOP_MANAGE = 106;
	private final int ACTION_SELECTED_ROLE_MANAGE = 107;

	RelativeLayout rlMain;// view root
	EditText edUserName;// control nhap ten
	EditText edPassword;// control nhap pass
	Button btLogin; // button login
	LinearLayout llMenu;// layout header icon
	private UserDTO userDTO;// staff login
	String passMD5;// pass luu tru sau khi login thanh cong
	// dang dong bo du lieu
	private boolean isRequestSynData = false;
	// dialog hien thi danh sach NPP
	private AlertDialog alertListRoleDialog;
	private AlertDialog alertListShopDialog;
	//dung tinh % hoan thanh dong bo
	long beginLogId;
	int percentSynData = 0;
	// ton tai file DB?
	private int stateExistsDB = 0;
	// dang login
	private boolean isRequestLogin = false;
	//check so lan loi download native
	private int numErrorNativeLib = 0;
	//dang download native libs
	public boolean isDownloadNativeLibs = false;
	private Spinner spLanguages;
	private boolean isSelectedLanguages;
	// bien kiem tra truong hop login offline
	boolean isLoginOffline = false;
	private CheckBox cbAllowOffline;
	private boolean isNeedUpdateVersionGlobal = false;
	private Calendar startTimeLoginKPI;
	private Calendar endTimeLoginKPI;
	private AlertDialog alertChangePassDialog;
	private String passNew;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	    Intent intent = getIntent();
		if ((intent != null && (intent.getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0)) {
	        // Activity was brought to front and not created,
	        // Thus finishing this will get us to the last viewed activity
	        finish();
	        return;
	    }

		setContentView(R.layout.layout_login);
		setTitleName(StringUtil.getString(R.string.app_name));
		llMenu = (LinearLayout) findViewById(R.id.llMenu);
		rlMain = (RelativeLayout) findViewById(R.id.rlMain);
		spLanguages = (Spinner) findViewById(R.id.spLanguages);
		ArrayList<LanguagesDTO> listLang = GlobalInfo.getInstance().getListLanguage();
		SpinnerLanguagesAdapter adap = new SpinnerLanguagesAdapter(this, android.R.layout.simple_spinner_item, listLang);
		spLanguages.setAdapter(adap);
		spLanguages.setOnItemSelectedListener(this);
		btLogin = (Button) findViewById(R.id.btLogin);
		btLogin.setOnClickListener(this);
		edUserName = (EditText) findViewById(R.id.edUserName);
		edPassword = (EditText) findViewById(R.id.edPassword);
		cbAllowOffline = (CheckBox) findViewById(R.id.cbOffline);

		GlobalUtil.setEditTextMaxLength(edUserName, 50);
		GlobalUtil.setEditTextMaxLength(edPassword, 50);

		edUserName.setText("");// 0000011717
		edPassword.setText("");// 123456

		// set text menu & icon left gone
		TextView tvTitleMenu = (TextView) findViewById(R.id.tvTitleMenu);
		ImageView iconLeft = (ImageView) llShowHideMenu.findViewById(R.id.ivLeftIcon);
		iconLeft.setVisibility(View.GONE);
		tvTitleMenu.setVisibility(View.GONE);

//		//hardcode lasLogId test dong bo
//		SharedPreferences sharedPreferences = getSharedPreferences(
//				LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
//		Editor prefsPrivateEditor = sharedPreferences.edit();
//		prefsPrivateEditor.putString(LAST_LOG_ID, "11145221");
//		prefsPrivateEditor.commit();

		init();

		rlMain.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				GlobalUtil.forceHideKeyboard(LoginView.this);
				return false;
			}
		});

		// set hash map kpi
		GlobalInfo.getInstance().resetHashMapKPI();

		//showToastMessage("Dung luong bo nho ngoai con trong : " + ExternalStorage.megabytesAvailableOnDisk());
		//dung luong nho hon 500MB canh bao
		if(ExternalStorage.megabytesAvailableOnDisk() < 500){
			showDialog(StringUtil.getString(R.string.ERR_FULL_DISK));
		}

		PositionManager.getInstance().stop();
		// bien gui danh sach app chua go cai dat
		GlobalInfo.getInstance().setFirstTimeSendApp(false);
	}

	private void init() {
		// config duong dan VietteMap su dung 3G
		//AppInfo.setServerAddress("http", "125.212.226.40", 8080);

		// AnhND modify dns time out cache
		System.setProperty("networkaddress.cache.ttl", "0");
		System.setProperty("networkaddress.cache.negative.ttl", "0");

		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		String verDB = sharedPreferences.getString(VNM_VER_DB, "0.0.0");
		String userName = sharedPreferences.getString(VNM_USER_NAME, "");
		passMD5 = sharedPreferences.getString(VNM_PASSWORD, "");

		if (!StringUtil.isNullOrEmpty(userName)) {
			// khong cho edit ten dang nhap khi da co thong tin user dang nhap
			edUserName.setText(userName);
			edUserName.setEnabled(false);
//			edUserName.setBackgroundResource(R.drawable.bg_white_rounded_disable_backup);
//			edUserName.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
//					GlobalUtil.dip2Pixel(5));
			edPassword.requestFocus();
		} else {
			edUserName.setEnabled(true);
			edUserName.requestFocus();
		}

		MyLog.i("Login", "Init get ver DB: " + verDB);
		GlobalInfo.getInstance().getProfile().setVersionDB(verDB);

		// header
		RelativeLayout.LayoutParams llMenuParam = new RelativeLayout.LayoutParams(GlobalUtil.dip2Pixel(50),
				LayoutParams.WRAP_CONTENT);
		llMenu.setLayoutParams(llMenuParam);

		// lay thong tin version
		try {
			GlobalInfo.getInstance().getProfile()
					.setVersionApp(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		} catch (NameNotFoundException e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		String verInfo = "";

		Date date = apkUpdateTime();
		String dateApp = DateUtils.convertDateTimeWithFormat(date, "dd/MM/yyy HH:mm");

//		if(GlobalInfo.isTablet){
//            if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile().getVersionApp())) {
//                verInfo = StringUtil.getString(R.string.TEXT_VERSION_INFO_1)
//                        + GlobalInfo.getInstance().getProfile().getVersionApp() + "." + GlobalUtil.getServerType();
//            }
//            if (!StringUtil.isNullOrEmpty(dateApp)) {
//                verInfo += StringUtil.getString(R.string.TEXT_VERSION_INFO_2) + dateApp;
//            }
//            setStatusVisible(verInfo, View.VISIBLE);
//        }

	}

	/**
	 * Lay ngay cap nhat app
	 *
	 * @author : BangHN since : 1.0
	 */
	private Date apkUpdateTime() {
		PackageManager packageManager = getPackageManager();
		String packageName = getPackageName();
		Date date = null;
		try {
			ApplicationInfo appInfo = packageManager.getApplicationInfo(packageName, 0);
			String appFile = appInfo.sourceDir;
			Long installed = new File(appFile).lastModified();
			date = new Date(installed);
		} catch (NameNotFoundException e) {
			return null; // package not found
		} catch (IllegalArgumentException e) {
		} catch (SecurityException e) {
		}
		// field wasn't found
		return date;
	}

	/**
	 * validate thong tin dang nhap
	 *
	 * @author : BangHN since : 1.0
	 */
	private boolean isValidateInput(String userName, String pass) {
		if (StringUtil.isNullOrEmpty(userName)) {
			showDialog(StringUtil.getString(R.string.PLS_INPUT_US_NAME));
			return false;
		} else if (StringUtil.isNullOrEmpty(pass)) {
			showDialog(StringUtil.getString(R.string.PLS_INPUT_PASSWORD));
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btLogin) {
			HTTPClient.setSessionID(null);
			percentSynData = 0;
			isRequestSynData = false;
			TransactionProcessManager.getInstance().cancelTimer();
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			String userName = edUserName.getText().toString().trim();
			String pass = edPassword.getText().toString().trim();
			//String fullName = StringUtil.generateFullUserName(userName);
			edUserName.setText(userName);
			if (isValidateInput(userName, pass)) {//
				// Khong bat GPS
				if (!PositionManager.getInstance().isEnableGPS()) {
					GlobalUtil.showDialogSettingGPS();
				} else if (GlobalUtil.isMockLocation()) {
					// cho phep gia lap vi tri
					GlobalUtil.getInstance().checkAllowMockLocation();
				} else {
					showProgressDialog(StringUtil.getString(R.string.loading));
					requestLogin(userName, pass);
				}
			}
			//checkAndStartPosition();
			GlobalUtil.forceHideKeyboard(this);
		}
	}


	@Override
	protected void onResume() {
		showWarning(false);
		//chua duoc login
		GlobalInfo.getInstance().setLoginState(-1);
		//restore lai vi tri
		GlobalInfo.getInstance().getStaffPosition();
		resetSavedDataForLogout(false);
		//renderLanguagesSpinner();
		
		//change cskh
		displayCskhInfoTitle();
		super.onResume();
	}

	@SuppressWarnings("unused")
	private void renderLanguagesSpinner() {
		LanguagesDTO dtoLang = GlobalInfo.getInstance().loadLanguages();
		if (dtoLang != null) {
			for (int i = 0; i < spLanguages.getAdapter().getCount(); i++) {
				final LanguagesDTO temp = (LanguagesDTO) spLanguages.getAdapter().getItem(i);
				if (temp.getValue().equals(dtoLang.getValue())) {
					spLanguages.setSelection(i);
					isSelectedLanguages = true;
					break;
				}
			}
		}
	}

	/**
	 * Reset du lieu truoc khi log out
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void resetSavedDataForLogout(boolean isRememberAfterLogin) {
		MemoryUtils.setNullInstance();
		HashMapManager.setNullInstance();
	}

	/**
	 * Luu thong tin dang nhap
	 *
	 * @author : BangHN since : 9:01:36 AM
	 */
	private void saveUserInfo(UserDTO userDTO) {
		GlobalInfo.getInstance().getProfile().setUserData(userDTO);
		// luu lai profile de auto login lan sau
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		Editor prefsPrivateEditor = sharedPreferences.edit();
		String userName = edUserName.getText().toString().trim();
		String pass = edPassword.getText().toString().trim();
		if (!StringUtil.isNullOrEmpty(userName)){
			prefsPrivateEditor.putString(VNM_USER_NAME, userName);
		}
		if (!StringUtil.isNullOrEmpty( userDTO.getDisplayName())){
			prefsPrivateEditor.putString(VNM_DISPLAY_NAME, userDTO.getDisplayName());
		}
		if (!StringUtil.isNullOrEmpty(pass)){
			prefsPrivateEditor.putString(VNM_PASSWORD, userDTO.getPass());
		}
		if (userDTO.getInheritId() > 0){
			prefsPrivateEditor.putInt(VNM_INHERIT_ID, userDTO.getInheritId());
		}
		if (userDTO.getStaffId() > 0){
			prefsPrivateEditor.putInt(VNM_STAFF_ID, userDTO.getStaffId());
		}
		if (userDTO.getStaffTypeId() > 0){
			prefsPrivateEditor.putInt(VNM_STAFF_TYPE_ID, userDTO.getStaffTypeId());
		}
		if (!StringUtil.isNullOrEmpty(userDTO.getInheritShopId())) {
			prefsPrivateEditor.putString(VNM_SHOP_ID, userDTO.getInheritShopId());
		}
		if (!StringUtil.isNullOrEmpty(userDTO.getRoleId())) {
			prefsPrivateEditor.putString(VNM_ROLE_ID, userDTO.getRoleId());
		}
		prefsPrivateEditor.putString(VNM_SHOP_CODE, userDTO.getInheritShopCode());
		if (!StringUtil.isNullOrEmpty(userDTO.getInheritShopIdProfile())) {
			prefsPrivateEditor.putString(VNM_SHOP_ID_PROFILE, userDTO.getInheritShopIdProfile());
		}
		prefsPrivateEditor.putString(VNM_STAFF_CODE, userDTO.getUserCode());
//		prefsPrivateEditor.putInt(VNM_CHANEL_TYPE, userDTO.chanelType);
		prefsPrivateEditor.putInt(VNM_CHANEL_OBJECT_TYPE, userDTO.getInheritSpecificType());
//		prefsPrivateEditor.putLong(VNM_STAFF_OWNER_ID, userDTO.staffOwnerId);
		prefsPrivateEditor.putString(VNM_SALE_TYPE_CODE, userDTO.getInheritSaleTypeCode());
		prefsPrivateEditor.putString(VNM_SHOP_LAT, Double.toString(userDTO.getInheritShopLat()));
		prefsPrivateEditor.putString(VNM_SHOP_LNG, Double.toString(userDTO.getInheritShopLng()));
		prefsPrivateEditor.putString(VNM_STAFF_TYPE_NAME, userDTO.getStaffTypeName());
		// kiem tra sau khi luu chanelObjectType
		// va truoc khi luu VNM_SV_DATE
//		if (userDTO.chanelObjectType == UserDTO.TYPE_VALSALES) {
			// danh cho vansale
		checkResetNumVansale();
//		}

		prefsPrivateEditor.putString(VNM_SV_DATE, DateUtils.now());

		GlobalInfo.getInstance().saveRoleManged(userDTO.getListRole());
		// luu thong tin shop Managed
		GlobalInfo.getInstance().saveShopManaged(userDTO.getShopManaged());
		prefsPrivateEditor.commit();
		// disable user name sau khi dang nhap thanh cong
		edUserName.setEnabled(false);
		//edUserName.setBackgroundResource(R.drawable.bg_white_rounded_disable_backup);
//		edUserName.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
//				GlobalUtil.dip2Pixel(5));
	}

	/**
	 * Luu thong tin version db, sau khi download db thanh cong
	 * @author : BangHN since : 1.0
	 */
	private void saveDBVersionInfo() {
		String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		Editor prefsPrivateEditor = sharedPreferences.edit();
		if (StringUtil.isNullOrEmpty(userDTO.getDbVersion().getVersion())) {
			userDTO.getDbVersion().setVersion("0.0.0");
		}
		//file db tai ve co lastlogid = maxdblog hien thoi
		if(userDTO.getDbVersion().getLastLogId().equals(userDTO.getDbVersion().getMaxDBLogId())){
			prefsPrivateEditor.putString(roleShop + VNM_STATUS_DOWNLOAD_DB, SynDataDTO.UPDATE_TO_DATE);
		}else{
			//nguoc lai file db nay tao ra truoc do 12h, chua up_to_date
			prefsPrivateEditor.putString(roleShop + VNM_STATUS_DOWNLOAD_DB, SynDataDTO.CONTINUE);
		}
		prefsPrivateEditor.putString(roleShop + VNM_VER_DB, userDTO.getDbVersion().getVersion().trim());
		prefsPrivateEditor.putString(roleShop + LAST_LOG_ID, userDTO.getDbVersion().getLastLogId());
		prefsPrivateEditor.putString(roleShop +  MAX_LAST_LOG_ID, userDTO.getDbVersion().getMaxDBLogId());
		GlobalInfo.getInstance().getProfile().setVersionDB(userDTO.getDbVersion().getVersion().trim());
		prefsPrivateEditor.commit();
	}

	/**
	 * Luu thong tin sau khi update DB thanh cong
	 * @author: BANGHN
	 */
	private void saveDBInfoAfterUpdateDB(){
		String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		Editor prefsPrivateEditor = sharedPreferences.edit();
		prefsPrivateEditor.putString(roleShop + VNM_VER_DB, userDTO.getDbVersion().getVersion().trim());
		GlobalInfo.getInstance().getProfile().setVersionDB(userDTO.getDbVersion().getVersion().trim());
		prefsPrivateEditor.commit();
	}


	/**
	 * Luu thong tin trang thai update db la UP_TO_DATE
	 * @author: BANGHN
	 * @param synDTO
	 */
	private void saveDBInfoAfterUpToDate(SynDataDTO synDTO){
		String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		Editor prefsPrivateEditor = sharedPreferences.edit();
		//trang thai hoan thanh cap nhat db
		prefsPrivateEditor.putString(roleShop + VNM_STATUS_DOWNLOAD_DB, SynDataDTO.UPDATE_TO_DATE);
		prefsPrivateEditor.putString(roleShop + LAST_LOG_ID, "" + synDTO.getLastLogId_update());
		prefsPrivateEditor.putString(roleShop + MAX_LAST_LOG_ID, "" + synDTO.getMaxDBLogId());
		prefsPrivateEditor.commit();
	}

	/**
	 * Kiem tra trang thai update du lieu up_to_date?
	 * @author: BANGHN
	 * @return
	 */
	private boolean checkDBUpdateToDate(){
		boolean isUpToDate = true;
		String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		String state = sharedPreferences.getString(roleShop + VNM_STATUS_DOWNLOAD_DB,
				SynDataDTO.UPDATE_TO_DATE);
		if(SynDataDTO.CONTINUE.equals(state)){
			isUpToDate = false;
		}
		return isUpToDate;
	}


	/**
	 * Request login to server
	 * @author: BangHN
	 * @param phone
	 * @param pass
	 *            (neu la autologin thi truyen oauth data)
	 * @return: void
	 * @throws:
	 */
	private void requestLogin(String username, String pass) {
		startTimeKPI = Calendar.getInstance();
		startTimeLoginKPI = Calendar.getInstance();
		try {
			isRequestLogin = true;
			Vector<String> vt = new Vector<String>();
			vt.add(IntentConstants.INTENT_USER_NAME);
			vt.add(username);
			vt.add(IntentConstants.INTENT_LOGIN_PASSWORD);
			String passEcrypted = GlobalInfo.isIS_VERSION_SEND_DATA() ? StringUtil.generateHash(pass, username.trim().toLowerCase()) : pass;
			vt.add(passEcrypted);
			vt.add(IntentConstants.INTENT_LOGIN_IS_REMEMBER);
			vt.add("true");

			vt.add(IntentConstants.INTENT_IMEI);
			vt.add(GlobalInfo.getInstance().getDeviceIMEI());

			if (!StringUtil.isNullOrEmpty(StringUtil.getSimSerialNumber())) {
				vt.add(IntentConstants.INTENT_SIM_SERIAL);
				vt.add(StringUtil.getSimSerialNumber());
			}

			vt.add(IntentConstants.INTENT_LOGIN_PHONE_MODEL);
			vt.add(GlobalInfo.getInstance().PHONE_MODEL);

			vt.add(IntentConstants.INTENT_VERSION_APP);
			vt.add(GlobalInfo.getInstance().getProfile().getVersionApp());

			vt.add(IntentConstants.INTENT_VERSION_DB);
			vt.add(GlobalInfo.getInstance().getProfile().getVersionDB());

			vt.add(IntentConstants.INTENT_LOGIN_PLATFORM);
			vt.add(GlobalInfo.getInstance().PLATFORM_SDK_STRING);
			handleViewEvent(vt, ActionEventConstant.ACTION_LOGIN, UserController.getInstance(), false, true);
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	/**
	 * Login offline khi khong co mang
	 * @author : BangHN since : 1.0
	 */
	private void loginOffline(ModelEvent modelEvent) {
		showProgressDialog(StringUtil.getString(R.string.loading));
		userDTO = new UserDTO();
		// kiem tra da dang nhap truoc do chua
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		String userName = sharedPreferences.getString(VNM_USER_NAME, "");
		String pass = sharedPreferences.getString(VNM_PASSWORD, "");

		userDTO.setUserName(userName);
		userDTO.setDisplayName(sharedPreferences.getString(VNM_DISPLAY_NAME, userName));
		userDTO.setPass(pass);
		userDTO.setUserCode(sharedPreferences.getString(VNM_STAFF_CODE, ""));
		userDTO.setStaffTypeName(sharedPreferences.getString(VNM_STAFF_TYPE_NAME, ""));
		userDTO.setInheritSpecificType(sharedPreferences.getInt(VNM_CHANEL_OBJECT_TYPE, 1));
		userDTO.setInheritShopId(sharedPreferences.getString(VNM_SHOP_ID, ""));
		userDTO.setInheritShopIdProfile(sharedPreferences.getString(VNM_SHOP_ID_PROFILE, ""));
		userDTO.setInheritShopCode(sharedPreferences.getString(VNM_SHOP_CODE, ""));
		userDTO.setInheritSaleTypeCode(sharedPreferences.getString(VNM_SALE_TYPE_CODE, ""));
		userDTO.setInheritId(sharedPreferences.getInt(VNM_INHERIT_ID, 0));
		userDTO.setStaffId(sharedPreferences.getInt(VNM_STAFF_ID, 0));
		userDTO.setStaffTypeId(sharedPreferences.getInt(VNM_STAFF_TYPE_ID, 0));
		userDTO.setInheritShopLat(Double.parseDouble(sharedPreferences.getString(
				VNM_SHOP_LAT, "0")));
		userDTO.setInheritShopLng(Double.parseDouble(sharedPreferences.getString(
				VNM_SHOP_LNG, "0")));
		userDTO.setRoleId(sharedPreferences.getString(VNM_ROLE_ID, ""));
		userDTO.setListRole(GlobalInfo.getInstance().getRoleManaged());
		userDTO.setShopManaged(GlobalInfo.getInstance().getShopManaged());
		try {
			String edPass = StringUtil.generateHash(edPassword.getText().toString().trim(), edUserName.getText()
					.toString().trim().toLowerCase());

			if (userName.equals(edUserName.getText().toString().trim()) && pass.equals(edPass)) {
				GlobalInfo.getInstance().getProfile().setUserData(userDTO);
				proccessAfterLoginOffline();
			} else if (StringUtil.isNullOrEmpty(pass) && edUserName.isEnabled()) {
				closeProgressDialog();
				// neu chua login lan dau tien, ma ket noi mang loi
				showDialog(modelEvent.getModelMessage());
			} else {
				closeProgressDialog();
				showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_PASSWORD_WRONG));
			}
		} catch (Exception e) {
			closeProgressDialog();
			showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_LOGIN_OFFLINE));
		}
	}

	/**
	 * Request sync data
	 * @author : BangHN
	 * since : 1:49:12 PM
	 */
	private void requestSynData() {
		try {
			String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
			isRequestSynData = true;
			Vector<String> vt = new Vector<String>();
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			String lastLogId = sharedPreferences.getString(roleShop + LAST_LOG_ID, "0");
			MyLog.d("SYNDATA", "Login: requestSynData : " + lastLogId);
			vt.add(IntentConstants.INTENT_SYN_STAFFID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getStaffId()));
			vt.add(IntentConstants.INTENT_SYN_INHERITID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
			vt.add(IntentConstants.INTENT_SHOP_ID);
			// doi shopProfile thanh shopId
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			vt.add(IntentConstants.INTENT_LAST_LOG_ID);
			vt.add(lastLogId);// "2489904"
			vt.add(IntentConstants.INTENT_ROLE_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getRoleId());

			ActionEvent e = new ActionEvent();
			// e.action = ActionEventConstant.ACTION_SYN_SYNDATA;
			e.action = ActionEventConstant.ACTION_SYN_SYNDATA;
			e.isBlockRequest = true;
			e.viewData = vt;
			e.userData = lastLogId;
			e.sender = LoginView.this;
			SynDataController.getInstance().handleViewEvent(e);

		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent act = modelEvent.getActionEvent();
		switch (act.action) {
		case ActionEventConstant.CHANGE_PASS:
			setFlagNeedChangePass(false);
			closeProgressDialog();
			closeDialog(alertChangePassDialog);
			GlobalUtil.showDialogConfirm(this, StringUtil.getString(R.string.TEXT_CHANGE_PASS_SUCCEEDED_LOGIN),
					StringUtil.getString(R.string.TEXT_BUTTON_LOGIN_WITH_NEW_PASSWORD), ACTION_LOGIN_WITH_NEW_PASS,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), -1, null);
			break;
		case ActionEventConstant.ACTION_LOGIN: {
			isLoginOffline = false;
			isRequestLogin = false;
			userDTO = (UserDTO) modelEvent.getModelData();
			
			if (UserDTO.isNeedResetPassword()) {
				setFlagNeedChangePass(true);
				edPassword.setText("");
				edPassword.requestFocus();
				closeProgressDialog();
				showDialogChangePass();
			} else {
				//Id thiet bi
				if(userDTO.getDeviceId() > 0) {
					GlobalInfo.getInstance().setDeviceId(userDTO.getDeviceId());
				}
				//check shop de start truoc dinh vi
				checkShopAndStartPosition();
	
				selectRoleAfterLogin();
				//ket thuc login - ghi log neu co
				requestInsertLogKPI(HashMapKPI.GLOBAL_LOGIN, act);
				
				//save need change pass = false
				setFlagNeedChangePass(false);
			}
			break;

		}
		case ActionEventConstant.ACTION_GET_LINK_SQL_FILE: {
			MyLog.d("Login", "Finish request get link DB: " + DateUtils.now());
			userDTO.setDbVersion((DBVersionDTO) modelEvent.getModelData());
			beginLogId = Long.parseLong(userDTO.getDbVersion().getLastLogId());
			new DownloadTask().execute(userDTO.getDbVersion().getUrlDB());
			break;
		}

		case ActionEventConstant.ACTION_SYN_SYNDATA: {
			// chuyen den trang home
			SynDataDTO synDataDTO = (SynDataDTO) modelEvent.getModelData();
			if (SynDataDTO.UPDATE_TO_DATE.equals(synDataDTO.getState())) {
//				GlobalInfo.getInstance().setFirstSynData(DateUtils.formatDate(new Date(), DateUtils.DATE_FORMAT_DATE));
				new DeleteFileTmp().execute("");
				saveDBInfoAfterUpToDate(synDataDTO);
				MyLog.i("SYNDATA", "Login: UPDATE_TO_DATE");
				updateProgressPercentDialog(99);
				// get dong max_id data
				//requestSynDataMax();
				endTimeKPI = Calendar.getInstance();
				requestInsertLogKPI(HashMapKPI.GLOBAL_SYN_DATA_LOGIN);
				generateOrgTempData();
				goToMainView();
			} else if (SynDataDTO.CONTINUE.equals(synDataDTO.getState())) {
//				GlobalInfo.getInstance().setFirstSynData(DateUtils.formatDate(new Date(), DateUtils.DATE_FORMAT_DATE));
				if (GlobalInfo.getInstance().getStateSynData() != GlobalInfo.SYNDATA_CANCELED) {
					MyLog.i("SYNDATA", "Login: CONTINUE");
					requestSynData();

					percentSynData = (int) (100 - ((double) (synDataDTO
							.getMaxDBLogId() - synDataDTO.getLastLogId_update()) / (double) (synDataDTO
							.getMaxDBLogId() - beginLogId)) * 100);
					updateProgressPercentDialog(percentSynData);
				}
			} else if (SynDataDTO.RESET.equals(synDataDTO.getState())) {
				closeProgressDialog();
				GlobalUtil.showDialogConfirm(this,
						StringUtil.getString(R.string.TEXT_CONFIRM_RESET_DB),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
						ACTION_OK_RESET_DB,
						StringUtil.getString(R.string.TEXT_BUTTON_DENY),
						ACTION_CANCEL_RESET_DB, null);
			}
			// isRequestSynData = false;
			break;
		}
		// finish dong bo du lieu
		case ActionEventConstant.ACTION_SYN_SYNDATA_MAX: {
			requestInsertLogKPI(HashMapKPI.GLOBAL_SYN_DATA);
			generateOrgTempData();
			goToMainView();
//			requestDeleteOldLogTable();
			break;
		}
//		case ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE:
//			selectRoleAfterLogin();
//			break;
		case ActionEventConstant.ACTION_UPDATE_DELETED_DB:
			MyLog.d("Login", "Update thanh cong");
			userDTO.setDeleteData(false);
			GlobalInfo.getInstance().getProfile().getUserData().setDeleteData(false);
			GlobalInfo.getInstance().getProfile().save();
			break;
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * set flag need change pass
	 * @author: duongdt3
	 * @time: 2:41:12 PM Nov 3, 2015
	*/
	private void setFlagNeedChangePass(boolean needChangePass) {
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		sharedPreferences.edit().putBoolean(VNM_NEED_CHANGE_PASS, needChangePass).commit();
	}

	/**
	 * Hien thi danh sach NPP can quan ly
	 * @author: BangHN
	 * @param listShop
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	private void showDialogSelectShop(List<ShopDTO> listShop) {
		Builder build = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
		ListShopManagedView view = new ListShopManagedView(this, ACTION_SELECTED_SHOP_MANAGE);
		view.renderLayoutShop(listShop);
		build.setView(view.viewLayout);
		alertListShopDialog = build.create();
		Window window = alertListShopDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		if (!alertListShopDialog.isShowing() && !isFinishing()) {
			alertListShopDialog.show();
		}
		closeProgressDialog();
	}

	/**
	 * Tiep tuc luong save data, update data sau khi login thanh cong
	 *
	 * @author : BangHN since : 1.0
	 */
	private void continueAfterLoginSuccess() {
		// this.hardCodeSaveDBVersionInfo();
		// lay ra thong tin shopId va roleId cu de backup db file
		SharedPreferences sharedPreferences = GlobalInfo.getInstance()
				.getDmsPrivateSharePreference();
		String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID, "");
		String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID, "");
		String[] params = { roleId, shopId };
		// luu thong tin dang nhap
		this.saveUserInfo(userDTO);

		////////////////Check thoi gian\\\\\\\\\\\\\\\\\\\\\
		if (!StringUtil.isNullOrEmpty(userDTO.getServerDate())) {
			GlobalInfo.RightTimeInfo rightInfo = new GlobalInfo.RightTimeInfo();
			//luu lai thoi gian cuoi cung login online server
			rightInfo.setLastTimeOnlineLogin(userDTO.getServerDate());
			//cap nhat thoi gian dung cuoi cung
			rightInfo.setLastRightTime(userDTO.getServerDate());
			//cap nhat thoi gian tu khi boot
			rightInfo.setLastRightTimeSinceBoot(SystemClock.elapsedRealtime());
			//luu thoi gian dung moi nhat
			GlobalInfo.getInstance().setRightTimeInfo(rightInfo);
		}
		////////////////Check thoi gian\\\\\\\\\\\\\\\\\\\\\

		// file DB co ton tai hay available hay khong
		stateExistsDB = GlobalUtil.checkExistsDataBase();

		// lay last lastlogId lan truoc theo role, shop da chon
		beginLogId = Long.parseLong(sharedPreferences.getString(GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser() + LAST_LOG_ID, "0"));

		// Download du lieu khi cap nhat truong isDeleteData
		if (userDTO.isDeleteData()) {
			GlobalUtil.showDialogConfirm(this,
					StringUtil.getString(R.string.TEXT_CONFIRM_RESET_DB),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					ACTION_RE_DOWNLOAD_DB_OK,
					StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
					ACTION_RE_DOWNLOAD_DB_CANCEL, null);
		} else if (GlobalInfo.getInstance().getProfile().getVersionDB()
				.equals("0.0.0")
				|| stateExistsDB == 0) {
			if (!edUserName.isEnabled()) {
				new VNMDBBackup().execute(params);
			} else {
				showProgressDialog(StringUtil.getString(R.string.downloading));
				requestGetLinkFileDB();
			}
		} else if (stateExistsDB == 1) {
			// thuc hien Cipher DB neu chua Cipher, tham so 0 login online
			showProgressDialog(StringUtil.getString(R.string.PROCESS_CIPHER_DB), false);
			new CipherTask().execute(ExternalStorage.getFileDBPath(
					GlobalInfo.getInstance().getAppContext()).getAbsolutePath(), "0");
		} else {
			this.startUpSynDataFlow();
		}
		//dang ky GCM
		//ServiceGCM.getInstance(getApplicationContext()).register();
	}


	/**
	 * Update DB: Them cot trong sales_order
	 * @author: BANGHN
	 */
	private void executeSQLUpdateDB(String sql){
		boolean isSuccess = true;
		try {
			if (!StringUtil.isNullOrEmpty(sql)) {
				if (sql.contains("\r\n")) {
					sql = sql.replaceAll("\r\n", "");
				} else if (sql.contains("\n")) {
					sql = sql.replaceAll("\n", "");
				}
				String[] script = sql.split(";");
				for (int i = 0; i < script.length; i++) {
					try {
						SQLUtils.getInstance().getmDB().execSQL(script[i]);
					} catch (SQLiteException e) {
						if (e.getMessage().contains("already exists")
								|| e.getMessage().contains("duplicate")) {
							//neu ton tai cot, bang chap nhan dung
						}else{
							isSuccess = false;
						}
					}
				}
			}
			if (isSuccess)
				saveDBInfoAfterUpdateDB();
		} catch (Exception e) {
			ServerLogger.sendLog("UPDATE SQLITE", e.getMessage() + sql,
					TabletActionLogDTO.LOG_EXCEPTION);
			MyLog.e("executeSQLUpdateDB", "fail", e);
		}
	}

	/**
	 * get link tai db mac dinh (khong ep tao file moi)
	 * @author: duongdt3
	 * @since: 18:02:56 13 Sep 2015
	 * @return: void
	 * @throws:
	 */
	private void requestGetLinkFileDB() {
		requestGetLinkFileDB(false);
	}

	/**
	 * Request get link DB de tai file DB
	 * @author : BangHN since : 11:53:27 AM
	 * @param isForceCreateFile
	 */
	private void requestGetLinkFileDB(boolean isForceCreateFile) {
		startTimeKPI = Calendar.getInstance();
		try {
			String forceCreateFile = (isForceCreateFile ? "1" : "0");
			Vector<String> vt = new Vector<String>();
			vt.add(IntentConstants.INTENT_SYN_INHERITID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
			vt.add(IntentConstants.INTENT_SYN_STAFFID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getStaffId()));
			vt.add(IntentConstants.INTENT_SHOP_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			vt.add(IntentConstants.INTENT_IMEI);
			vt.add(GlobalInfo.getInstance().getDeviceIMEI());
			vt.add(IntentConstants.INTENT_ROLE_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getRoleId());
			vt.add(IntentConstants.INTENT_FORCE_CREATE_FILE);
			vt.add(forceCreateFile);

			handleViewEvent(vt, ActionEventConstant.ACTION_GET_LINK_SQL_FILE, SynDataController.getInstance(), false, true);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
	}

	/**
	 * gui request thong bao da xoa DB thanh cong
	 *
	 * @author banghn
	 */
	private void requestUpdateDeletedDB() {
		if (userDTO != null && userDTO.isDeleteData()) {
			Vector<String> vt = new Vector<String>();
			vt.add(IntentConstants.INTENT_STAFF_ID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
			vt.add(IntentConstants.INTENT_SHOP_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopIdProfile());
			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.ACTION_UPDATE_DELETED_DB;
			e.viewData = vt;
			e.sender = LoginView.this;
			UserController.getInstance().handleViewEvent(e);
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		MyLog.d("Login", DateUtils.now() + "Action		: " + modelEvent.getActionEvent().action);
		MyLog.d("Login", DateUtils.now() + "Error code	: " + modelEvent.getModelCode());
		MyLog.d("Login", DateUtils.now() + "Message		: " + modelEvent.getModelMessage());
		GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
		closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.CHANGE_PASS:
			this.closeProgressDialog();
			switch (modelEvent.getModelCode()) {
			case ErrorConstants.ERROR_POLICY_PASSWORD:
				this.showDialog(StringUtil.getString(R.string.TEXT_PASS_POLICY_VALIDATE_SERVER));
				break;
			case ErrorConstants.ERROR_SESSION_RESET:
				this.showDialog(StringUtil.getString(R.string.TEXT_PLEASE_LOGIN_BEFORE_DO));
				break;
			default:
				if(modelEvent != null && !StringUtil.isNullOrEmpty(modelEvent.getModelMessage())){
					this.showDialog(modelEvent.getModelMessage());
				}else{
					this.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				}
				break;
			}
			break;
		case ActionEventConstant.ACTION_GET_LINK_SQL_FILE:
			switch (modelEvent.getModelCode()) {
			case ErrorConstants.ERROR_REST_DB_OUT_MAXIMUM:
				showDialog(modelEvent.getModelMessage());
				break;
			default:
				String errorInfo = StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_CANT_CREATE_DB);
				if (modelEvent != null && !StringUtil.isNullOrEmpty(modelEvent.getModelMessage())) {
					errorInfo += "\n" + modelEvent.getModelMessage();
				}
				showDialog(errorInfo);
				break;
			}
			break;
		// truong hop dong bo update du lieu bi loi
		// tiep tuc toi trang home
		case ActionEventConstant.ACTION_SYN_SYNDATA_MAX:
		case ActionEventConstant.ACTION_SYN_SYNDATA:
//			selectRoleAfterLogin();
			if(modelEvent != null && modelEvent.getModelMessage() != null
					&& modelEvent.getModelMessage().contains("ENOSPC")){
				showDialog(StringUtil.getString(R.string.ERR_FULL_DISK));
			} else if(modelEvent != null && !StringUtil.isNullOrEmpty(modelEvent.getModelMessage())) {
				showDialog(modelEvent.getModelMessage());
			} else{
				showDialog(Constants.MESSAGE_ERROR_COMMON);
			}
			break;
		case ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE:
			selectRoleAfterLogin();
			break;
		case ActionEventConstant.ACTION_LOGIN:
			isRequestLogin = false;
			switch (modelEvent.getModelCode()) {
			case ErrorConstants.ERROR_DB_OUT_OF_DATE:
				GlobalUtil.showDialogConfirm(this, StringUtil.getString(R.string.TEXT_CONFIRM_DB_STRUCTURE),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_UPDATE_DB_OK,
						StringUtil.getString(R.string.TEXT_BUTTON_DENY), ACTION_UPDATE_DB_CANCEL, null);
				break;
			case ErrorConstants.ERROR_INVALID_ACCOUNT:
				showDialog(modelEvent.getModelMessage());
				edUserName.requestFocus();
				break;
			case ErrorConstants.ERROR_INVALID_PASSWORD:
				showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_PASSWORD_WRONG));
				edPassword.setText("");
				edPassword.requestFocus();
				break;
			case ErrorConstants.ERROR_ACCOUNT_LOCKED:
				showDialog(StringUtil.getString(R.string.ERROR_ACCOUNT_LOCKED));
				// showDialog(modelEvent.getModelMessage());
				edUserName.requestFocus();
				break;
			case ErrorConstants.ERROR_CONNET_SERVER_DB:
			case ErrorConstants.ERROR_NO_CONNECTION:
				isLoginOffline = true;
				loginOffline(modelEvent);
				break;
			case ErrorConstants.ERROR_EXPIRED_TIMESTAMP:
				GlobalUtil.showDialogSettingTime();
				break;
			case ErrorConstants.ERROR_ACCOUNT_MAXIMUM_LOCK:
				showDialog(StringUtil.getString(R.string.ERROR_ACCOUNT_MAXIMUM_LOCK));
				// showDialog(modelEvent.getModelMessage());
				edPassword.requestFocus();
				break;
			case ErrorConstants.ERROR_INVALID_IMEI:
				showDialog(StringUtil.getString(R.string.ERROR_INVALID_IMEI));
				// showDialog(modelEvent.getModelMessage());
				break;
			case ErrorConstants.ERROR_INVALID_SERIAL:
				showDialog(StringUtil.getString(R.string.ERROR_INVALID_SERIAL));
				// showDialog(modelEvent.getModelMessage());
				break;
			case ErrorConstants.ERROR_INVALID_IMEI_OR_SERIAL:
				showDialog(StringUtil.getString(R.string.ERROR_INVALID_IMEI_OR_SERIAL));
				// showDialog(modelEvent.getModelMessage());
				break;
			case ErrorConstants.ERROR_NOT_SET_ROLE_YET:
				showDialog(StringUtil.getString(R.string.ERROR_NOT_SET_ROLE_YET));
				break;
			default:
				super.handleErrorModelViewEvent(modelEvent);
				break;
			}
			break;

		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Chon NPP sau khi login thanh cong de tiep tuc vao main view
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	private void selectRoleAfterLogin() {
		//co vai tro
		if (userDTO.getListRole() != null && !userDTO.getListRole().isEmpty()){
			//chi co 1 vai tro
			if (userDTO.getListRole().size() == 1) {
				//cho tu dong dang nhap voi role nay
				userDTO.setRoleManaged(userDTO.getListRole().get(0));
				userDTO.setRoleId("" + userDTO.getListRole().get(0).roleId);
				changeCurrentUserFromInheritUser(userDTO.getListRole().get(0));
//				//co quan ly shop
//				if (userDTO.listRole.get(0).listShop != null && !userDTO.listRole.get(0).listShop.isEmpty()){
//					//chi quan ly 1 shop
//					if (userDTO.listRole.get(0).listShop.size() == 1){
//						//cho tu dong dang nhap voi shop nay
//						userDTO.shopManaged = userDTO.listRole.get(0).listShop.get(0);
//						userDTO.shopId = "" + userDTO.listRole.get(0).listShop.get(0).shopId;
//						// neu offline thi di thang vao main view
//						if (isLoginOffline) {
//							executeLoginOffline();
//						}else
//							continueAfterLoginSuccess();
//					} else{ //quan ly nhieu shop
//						showDialogSelectShop(userDTO.roleManaged.listShop);
//					}
//				} else{ //khong quan ly shop
//					userDTO.shopId = userDTO.shopIdProfile;
//					if (isLoginOffline) {
//						executeLoginOffline();
//					}else
//						continueAfterLoginSuccess();
//				}
			} else{ //co nhieu vai tro
				showDialogSelectRole(userDTO.getListRole());
			}
		} else{ //khong co vai tro
//			saveUserInfo(userDTO);
//			goToMainView();
			closeProgressDialog();
			showDialog(StringUtil.getString(R.string.TEXT_NOT_HAVE_ROLE));
		}
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 10:54:51 11 Sep 2014
	 * @return: void
	 * @throws:
	 */
	private void showDialogSelectRole(ArrayList<RoleUserDTO> listRole) {
		Builder build = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
		ListShopManagedView view = new ListShopManagedView(this, ACTION_SELECTED_ROLE_MANAGE);
		view.renderLayoutRole(listRole);
		build.setView(view.viewLayout);
		alertListRoleDialog = build.create();
		Window window = alertListRoleDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		if (!alertListRoleDialog.isShowing() && !isFinishing()) {
			alertListRoleDialog.show();
		}
		closeProgressDialog();
	}

	/**
	 * Di toi man hinh nhan vien ban hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void goToMainView() {
		checkAndStartPosition();
		if (GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_CANCELED) {
			return;
		}
		GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
		isRequestSynData = false;
		GlobalInfo.getInstance().setExitApp(false);

		//neu update du lieu chua toi trang thai up_to_date thi
		//khong tiep tuc thuc hien
		if(!checkDBUpdateToDate()){
			closeProgressDialog();
			showDialog(StringUtil.getString(R.string.ERR_UPDATE_NOT_SUCCESS));
			ServerLogger.sendLog("SYNDATA", "Update du lieu loi chua toi UP_TO_DATE",
					TabletActionLogDTO.LOG_CLIENT);
			return;
		}

		if (userDTO == null) {
			userDTO = GlobalInfo.getInstance().getProfile().getUserData();
		}
		// request update da xoa DB thanh cong neu co
		requestUpdateDeletedDB();
		// constant define app
		getAppDefineConstant();
		
		if(GlobalInfo.getInstance().isVtMapSetServer()) {
			// AnhND modify dns time out cache
			AppInfo.setServerAddress(GlobalInfo.getInstance().getVtMapProtocol(), GlobalInfo.getInstance().getVtMapIP(), GlobalInfo.getInstance().getVtMapPort());
			System.setProperty("networkaddress.cache.ttl", "0");
			System.setProperty("networkaddress.cache.negative.ttl", "0");
		}

		// khoi tao xac dinh co full quyen chuc nang khong?
		PriUtils.getInstance().initIsFullPrivilege();

		endTimeLoginKPI = Calendar.getInstance();
		requestInsertLogKPI(HashMapKPI.GLOBAL_LOGIN_FULL, startTimeLoginKPI, endTimeLoginKPI);
		
		Bundle b = new Bundle();
		if (userDTO != null && !StringUtil.isNullOrEmpty(userDTO.getServerDate())) {
			b.putString(IntentConstants.INTENT_TIME, userDTO.getServerDate());
		} else {
			b.putString(IntentConstants.INTENT_TIME, "");
		}
		
		if (userDTO != null) {
			ActionEvent e = new ActionEvent();
			e.sender = this;
			e.viewData = b;
			if (userDTO.getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {// NVGS
				e.action = ActionEventConstant.GO_TO_SUPERVISOR_VIEW;
			} else if (userDTO.getInheritSpecificType() == UserDTO.TYPE_MANAGER) {// TBHV
				e.action = ActionEventConstant.GO_TO_TBHV_VIEW;
			} else {// NVBH
				e.action = ActionEventConstant.GO_TO_SALE_PERSON_VIEW;
			}
			UserController.getInstance().handleSwitchActivity(e);
			closeProgressDialog();
			//dang nhap thanh cong
			GlobalInfo.getInstance().setLoginState(1);
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_LOGIN_WITH_NEW_PASS:
			//auto login with new pass
			edPassword.setText(passNew);
			onClick(btLogin);
			break;
		case ACTION_CHANGE_PASS:
			@SuppressWarnings("unchecked")
			Vector<String> dataPassword = (Vector<String>) data;
			String passOld = dataPassword.get(0);
			passNew = dataPassword.get(1);
			changePass(passOld, passNew);
			break;
		case ACTION_UPDATE_VERSION_SUCCESS:
			// kiem tra co reset DB hay khong
			if (userDTO.getAppVersion() != null && userDTO.getAppVersion().needResetDB) {
				GlobalUtil.deleteDatabaseVNM();
			}
			// HieuNH: xoa file apk trong thu muc app truoc khi download
			GlobalUtil.getInstance().deleteDir(new File(ExternalStorage.ROOT_SDCARD_DIR + "APP"));
			// thuc hien download update ver moi
			new DownloadTaskInstallApp().execute(((UserDTO) data).getAppVersion().downloadLink);
			break;
		case ACTION_UPDATE_VERSION_FAIL:
			// tiep tuc login vao he thong
			if (userDTO.getAppVersion() != null && userDTO.getAppVersion().forceDownload == false) {
//				continueAfterLoginSuccess();
				proccessAtferUpdateVersionFail();
			} else {
				finish();
			}
			break;
		case ACTION_RE_DOWNLOAD_DB_OK:{
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID,
					"");
			String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID,
					"");
			String[] params = { roleId, shopId };

			// do something here to update db
			VNMDBBackup task = new VNMDBBackup();
			task.setForceCreateFile(true);
			task.execute(params);
			break;
		}
		case ACTION_UPDATE_DB_OK:
			// do something here to update db
			break;
		case ACTION_UPDATE_DB_CANCEL:
			finish();
			break;
		case ACTION_DOWNLOAD_DB_OK:{
			// do something here to update db
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID,
					"");
			String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID,
					"");
			String[] params = { roleId, shopId };
			new VNMDBBackup().execute(params);
			break;
		}
		case ACTION_DOWNLOAD_DB_CANCEL:
			finish();
			break;
		case ACTION_OK_RESET_DB:
			// do something here to update db
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID,
					"");
			String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID,
					"");
			String[] params = { roleId, shopId };
			// do something here to update db
			VNMDBBackup task = new VNMDBBackup();
			task.setForceCreateFile(true);
			task.execute(params);
			
			break;
		case ACTION_CANCEL_LOGIN_OK:
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			break;
		case ACTION_SELECTED_ROLE_MANAGE:
			showProgressDialog(StringUtil.getString(R.string.loading));
			userDTO.setRoleManaged((RoleUserDTO) data);
			userDTO.setRoleId("" + userDTO.getRoleManaged().roleId);
			changeCurrentUserFromInheritUser((RoleUserDTO) data);
			alertListRoleDialog.dismiss();
			break;
		case ACTION_SELECTED_SHOP_MANAGE:
			showProgressDialog(StringUtil.getString(R.string.loading));
			///ShopDTO shop = (ShopDTO) data;
			userDTO.setShopManaged((ShopDTO) data);
			userDTO.setInheritShopId("" + userDTO.getShopManaged().shopId);
			userDTO.setInheritShopCode(userDTO.getShopManaged().shopCode);
			userDTO.setInheritShopLat(userDTO.getShopManaged().shopLat);
			userDTO.setInheritShopLng(userDTO.getShopManaged().shopLng);
			alertListShopDialog.cancel();
			if (isLoginOffline) {
				executeLoginOffline();
			}else{
				continueAfterLoginSuccess();
			}
			alertListShopDialog.dismiss();
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 * doi pass
	 * @author: duongdt3
	 * @param passNew 
	 * @param passOld 
	 * @time: 11:35:04 PM Nov 2, 2015
	*/
	private void changePass(String passOld, String passNew) {
		try {
			String userName = edUserName.getText().toString();
//			String passOldEncryted = StringUtil.generateHash(passOld, userName.toLowerCase());
//			String passNewEncryted = StringUtil.generateHash(passNew, userName.toLowerCase());
			String passOldEncryted = passOld;
			String passNewEncryted = passNew;
			this.showLoadingDialog();
			Vector<String> vt = new Vector<String>();
			vt.add(IntentConstants.INTENT_USER_NAME);
			vt.add(userName);
			vt.add(IntentConstants.NEW_PASS);
			vt.add(passNewEncryted);
			vt.add(IntentConstants.OLD_PASS);
			vt.add(passOldEncryted);
			ActionEvent e = new ActionEvent();
			e.viewData = vt;
			e.action = ActionEventConstant.CHANGE_PASS;
			e.sender = this;
			UserController.getInstance().handleViewEvent(e);
		} catch (Exception e) {
			MyLog.e("changePass", e);
		}
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		switch (action) {
		case ActionEventConstant.ACTION_FINISH_AND_SHOW_LOGIN:
			edPassword.setText("");
			break;
		case ActionEventConstant.REQUEST_TO_SERVER_SUCCESS:
			if (GlobalInfo.getInstance().getLastActivity() != null
					&& GlobalInfo.getInstance().getLastActivity().equals(this)) {
				if (GlobalInfo.getInstance().getProfile().getUserData()
						.getLoginState() == UserDTO.LOGIN_SUCCESS) {
					//startTimeKPI = Calendar.getInstance();
					requestSynData();
				} else {
					closeProgressDialog();
					showDialog(Constants.MESSAGE_ERROR_COMMON);
				}
			}
			break;
		case ActionEventConstant.ACTION_SYN_PERCENT:{
			long lastLogId = bundle.getLong(IntentConstants.INTENT_LAST_LOG_ID);
			long maxLogId = bundle.getLong(IntentConstants.INTENT_MAX_LOG_ID);
			percentSynData = (int) (100 - ((double) (maxLogId - lastLogId) / (double) (maxLogId - beginLogId)) * 100);
			updateProgressPercentDialog(percentSynData);
			break;
		}
		case ActionEventConstant.ACTION_FINISH_SYN_SYNDATA_NORMAL:{
			//check need update version
			if (isNeedUpdateVersionGlobal) {
				isNeedUpdateVersionGlobal = false;
				proccessUpdateVersion();
			}
			break;
		}
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}

	// cai dat ung dung moi
	private void InstallNewApp(String url) {
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(url)),
					"application/vnd.android.package-archive");
			GlobalUtil.startActivityOtherApp(this, intent);
		} catch (Exception e) {
			showDialog(StringUtil.getString(R.string.TEXT_INSTAL_UPGRADE_APP_ERROR));
			ServerLogger.sendLog("InstallNewApp fail " + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		}
	}


	@Override
	public void onCancel(DialogInterface dialog) {
		MyLog.i("Login", "onCancel.... ");
		if (GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_EXECUTING) {
			// showDialog(StringUtil.getString(R.string.MSG_EXECUTING_SYNDATA));
			GlobalUtil.showToastLong(StringUtil.getString(R.string.MSG_EXECUTING_SYNDATA));
			//showProgressDialog(StringUtil.getString(R.string.updating));
			showProgressPercentDialog(StringUtil.getString(R.string.updating), true);
			updateProgressPercentDialog(percentSynData);
		} else if (isRequestSynData) {
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_CANCELED);
		} else if (isRequestLogin){
			GlobalUtil.showToastLong(StringUtil.getString(R.string.MSG_EXECUTING_LOGIN));
			showProgressDialog(StringUtil.getString(R.string.loading));
		}else {
			super.onCancel(dialog);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK: {
			MyLog.i("Login", "onKeyDown.... KEYCODE_BACK");
			TransactionProcessManager.getInstance().cancelTimer();
			// SQLUtils.getInstance().closeDB();
			// cap nhat thoat ung dung
			GlobalInfo.getInstance().setExitApp(true);
			// kiem tra dong database
			if (!SQLUtils.getInstance().isProcessingTrans) {
				// cancel sqlLite
				SQLUtils.getInstance().closeDB();
			}
		}
		}
		return super.onKeyDown(keyCode, event);
	}

	// ////////////////////////////////////////////////////////////////////////
	// Background Task: Download file DB
	// ////////////////////////////////////////////////////////////////////////


	/**
	 * Xoa bo cac file tmp trong qua trinh su dung chuong trinh
	 * @author banghn
	 */
	private class DeleteFileTmp extends AsyncTask<String, Void, Exception> {
		@Override
		protected void onPreExecute() {
		}

		@Override
		protected Exception doInBackground(String... params) {
			try {
				// xoa bo file anh chup truoc do mot ngay ve truoc
				GlobalUtil.deleteTempTakenPhoto();
			} catch (Exception e) {
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			// xu ly sau khi hoan thanh hay loi
		}
	}
	/**
	 * Background task to download and unpack .zip file in background.
	 */
	private class DownloadTask extends AsyncTask<String, Void, Exception> {

		@Override
		protected void onPreExecute() {
			closeProgressDialog();
			showProgressPercentDialog(StringUtil.getString(R.string.downloading), false);
		}

		@Override
		protected Exception doInBackground(String... params) {
			String url = (String) params[0];
			try {
				downloadAllAssets(url);
			} catch (Exception e) {
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			if (result != null) {
				// download loi
				closeProgressDialog();
				String errorInfo = StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_DOWNLOAD_DB);
				if (result instanceof DMSDownloadException && !StringUtil.isNullOrEmpty(result.getMessage())) {
					errorInfo += "\n" + result.getMessage();
				}
				showDialog(errorInfo);
				ServerLogger.sendLog("Download DB", MyLog.printStackTrace(result), TabletActionLogDTO.LOG_EXCEPTION);
			} else {
				//check tranh truong hop goi tai du lieu nhieu lan userDTO.dbVersion = null
				if (userDTO != null && userDTO.getDbVersion() != null) {
					endDownloadDB();
					saveDBVersionInfo();
					updateProgressEndPercentDialog();
					// dong bo du lieu thieu (vi co the lay file db da ton tai truoc do)
					closeProgressDialog();
					//showProgressDialog(StringUtil.getString(R.string.updating));
					showProgressPercentDialog(StringUtil.getString(R.string.updating), false);
					startTimeKPI = Calendar.getInstance();
					requestSynData();
				}
			}
		}
	}

	/**
	 * Ghi log tai file DB
	 * @author: banghn
	 */
	private void endDownloadDB(){
		endTimeKPI = Calendar.getInstance();
		requestInsertLogKPI(HashMapKPI.GLOBAL_GET_FILE_DB);
	}

	// ////////////////////////////////////////////////////////////////////////
	// File Download: Download file cai dat ung dung
	// ////////////////////////////////////////////////////////////////////////

	/**
	 * Download .zip file specified by url, then unzip it to a folder in
	 * external storage.
	 *
	 * @param url
	 */
	private void downloadAllAssets(String url) throws Exception {
		// xoa file db hien tai
		GlobalUtil.deleteDatabaseVNM();
		File zipDir = ExternalStorage.getFileDBPath(getBaseContext());
		// File path to store .zip file before unzipping
		File zipFile = new File(zipDir.getPath() + "/temp.zip");
		File outputDir = ExternalStorage.getFileDBPath(getBaseContext());
		SharedPreferences sharedPreferences = GlobalInfo.getInstance()
				.getDmsPrivateSharePreference();
		String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID, "");
		String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID, "");
		File plaintextDBFile = new File(outputDir.getPath() + "/" + roleId
				+ "_" + shopId + "_" + Constants.DATABASE_NAME_CIPHER);
		File sqlCipherDBFile = new File(outputDir.getPath() + "/" + roleId
				+ "_" + shopId + "_" + Constants.DATABASE_NAME);
		try {
			DownloadFile.downloadWithURLConnection(url, zipFile, zipDir);
			LogFile.logToFile(DateUtils.now()
					+ " || Login: Hoan thanh download file DB");
			DownloadFile.unzipFile(getApplicationContext(), zipFile, outputDir);
			LogFile.logToFile(DateUtils.now()
					+ " || Login: Hoan thanh unzip file DB");
			GlobalInfo.setInProcessCipher(true);
			sqlCipherDBFile.renameTo(new File(outputDir.getPath() + "/"
					+ roleId + "_" + shopId + "_"
					+ Constants.DATABASE_NAME_CIPHER));
			SqlCipherUtil.encryptPlaintextDB(plaintextDBFile.getAbsolutePath(),
					getBaseContext());
			SQLUtils.getInstance().getmDB().execSQL("analyze;");
			GlobalInfo.setInProcessCipher(false);
		} catch (Exception e) {
			MyLog.e("downloadAllAssets", e);
			throw e;
		} finally {
			zipFile.delete();
		}
	}

	// update application automatic
	/**
	 * Background task to download file in background.
	 */
	private class DownloadTaskInstallApp extends AsyncTask<String, String, Exception> {
		@Override
		protected void onPreExecute() {
			showProgressPercentDialog(StringUtil.getString(R.string.APP_DOWNLOADING));
		}

		String pathOutput = null;
		@Override
		protected Exception doInBackground(String... params) {
			String url = (String) params[0];
			try {
				pathOutput = downloadAppNewVersion(url);
			} catch (Exception e) {
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			updateProgressEndPercentDialog();
			closeProgressDialog();
			if (result != null) {
				String errorInfo = StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_APP_VERSION_DOWNLOAD);
				if (result instanceof DMSDownloadException && !StringUtil.isNullOrEmpty(result.getMessage())) {
					errorInfo += "\n" + result.getMessage();
				}
				showDialog(errorInfo);
				ServerLogger.sendLog("Download App Error", MyLog.printStackTrace(result), TabletActionLogDTO.LOG_EXCEPTION);
			} else if(!StringUtil.isNullOrEmpty(pathOutput)){
				InstallNewApp(pathOutput);
			} else{
				showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_APP_VERSION_DOWNLOAD));
			}
		}
	}

	/**
	 * Get file apk sau khi download
	 *
	 * @author ThanhNN
	 * @param path
	 * @throws IOException
	 */
	private String downloadAppNewVersion(String path) throws Exception {
		String apkPath = null;
		if (!URLUtil.isNetworkUrl(path)) {
			return path;
		} else {
			File zipDir = ExternalStorage.getFileApkInstallPath(getBaseContext());
			// File path to store .zip file before unzipping
			File zipFile = new File(zipDir.getPath() + "/" + System.currentTimeMillis() + "_app.apk");
			try {
				DownloadFile.downloadWithURLConnection(path, zipFile, zipDir);
				apkPath = zipFile.getAbsolutePath();
			} catch (Exception e) {
				MyLog.e("downloadAppNewVersion", e);
				throw e;
			}
			return apkPath;
		}
	}

	/**
	 * kiem tra neu lan dau dang nhap trong ngay thi` reset lai PAY_RECEIVED_NUMBER cho vansale
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void checkResetNumVansale(){
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		String lastDate = sharedPreferences.getString(LoginView.VNM_SV_DATE, "");
		Date dateBefore = DateUtils.parseDateFromString(lastDate, DateUtils.DATE_STRING_YYYY_MM_DD);
		Date dateNow = DateUtils.now(DateUtils.DATE_STRING_YYYY_MM_DD);

		int isNewDate = DateUtils.compare(dateBefore, dateNow);
		if(isNewDate != 0){
			// ngay moi, resset lai bien
			Editor prefsPrivateEditor = sharedPreferences.edit();
			prefsPrivateEditor.putInt(VNM_PAY_RECEIVED_NUMBER, 1);
			prefsPrivateEditor.commit();
		}
	}
	/**
	 * Thread thuc hien chuyen doi DB No_Cipher -> DB_Cipher
	 *
	 * @author: ThangNV31
	 */
	private class CipherTask extends AsyncTask<String, Void, Exception> {

		int actionOnPost = 0;

		@Override
		protected void onPreExecute() {
		}
		@Override
		protected Exception doInBackground(String... params) {
			try {
				String dbDirectoryPath = params[0];
				this.actionOnPost = Integer.parseInt(params[1]);
				SharedPreferences sharedPreferences = GlobalInfo.getInstance()
						.getDmsPrivateSharePreference();
				String roleId = sharedPreferences.getString(
						LoginView.VNM_ROLE_ID, "");
				String shopId = sharedPreferences.getString(
						LoginView.VNM_SHOP_ID, "");
				File sqlCipherDBFile = new File(dbDirectoryPath + "/" + roleId
						+ "_" + shopId + "_" + Constants.DATABASE_NAME);
				GlobalInfo.setInProcessCipher(true);
				sqlCipherDBFile.renameTo(new File(dbDirectoryPath + "/"
						+ roleId + "_" + shopId + "_"
						+ Constants.DATABASE_NAME_CIPHER));
				SqlCipherUtil.encryptPlaintextDB(dbDirectoryPath + "/" + roleId
						+ "_" + shopId + "_" + Constants.DATABASE_NAME_CIPHER,
						getBaseContext());
				SQLUtils.getInstance().getmDB().execSQL("analyze;");
				//SQLUtils.getInstance().getmDB().rawExecSQL("analyze");
			} catch (Exception e) {
				return e;
			}
			return null;
		}
		@Override
		protected void onPostExecute(Exception result) {
			closeProgressDialog();
			if (result != null) {
				showDialog(StringUtil.getString(R.string.ERR_CIPHER_DB_FAIL));
				ServerLogger.sendLog("SQLiteCipher", "Chuyen doi DB", true,
						TabletActionLogDTO.LOG_EXCEPTION);
				MyLog.d("CipherDB", "Error Cipher DB Fail :" + result.getMessage());
				return;
			}
			//update db version
			if (userDTO.getDbVersion() != null){
				executeSQLUpdateDB(userDTO.getDbVersion().getScript());
			}

			GlobalInfo.setInProcessCipher(false);
			switch (this.actionOnPost) {
			case 1://login onffline
//				saveUserInfo(userDTO);
				goToMainView();
				isLoginOffline = false;
				break;
			default:
				LoginView.this.startUpSynDataFlow();
			}
		}
	}
	/**
	 * Khoi dong luong Syndata
	 *
	 * @author: ThangNV31
	 * @return: void
	 */
	private void startUpSynDataFlow() {
		// truong hop co network thi moi kich hoat luong syndata do thay doi cach chon role truoc moi fullDate du lieu
		if(GlobalUtil.checkNetworkAccess()){
			showProgressPercentDialog(StringUtil.getString(R.string.updating), false);
			// thuc hien fullDate het request len server truoc khi update ve
			TransactionProcessManager.getInstance().startChecking(TransactionProcessManager.SYNC_FROM_LOGIN);
		}else{
			executeLoginOffline();
		}
	}
	/**
	 * Sao luu du lieu luu tru truoc khi download moi
	 *
	 * @author banghn
	 */
	private class VNMDBBackup extends AsyncTask<String, Void, Exception> {
		boolean isForceCreateFile = false;
		@Override
		protected void onPreExecute() {
			showProgressDialog(StringUtil.getString(R.string.backucking));
		}

		@Override
		protected Exception doInBackground(String... params) {
			try {
				GlobalUtil.backupDatabaseToZIPWithRoleShop(params[0],params[1]);
//				GlobalUtil.backupDatabaseToZIP();
				GlobalUtil.deleteBackupDatabaseVNM();
			} catch (Exception e) {
				return e;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			showProgressDialog(StringUtil.getString(R.string.downloading));
			if (result != null) {
				// zip file loi tiep tuc get file
				requestGetLinkFileDB(isForceCreateFile);
			} else {
				// tiep tuc get file
				requestGetLinkFileDB(isForceCreateFile);
			}
		}

		public void setForceCreateFile(boolean isForceCreateFile) {
			this.isForceCreateFile = isForceCreateFile;
		}
	}

	/**
	 * Kiem tra goi dinh vi
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	private void checkAndStartPosition(){
		// kiem tra start lai dinh vi khi thoat ra man hinh login
		if (!PositionManager.getInstance().getIsStart()) {
			PositionManager.getInstance().start();
		}
	}

	/**
	 * Kiem tra dang nhap neu ton tai 1 shop chon thi start luong dinh vi
	 * Neu co nhieu shop --> Den khi chon role moi start (tranh loi phan quyen staffpositionlog)
	 * @author: DungNX
	 * @return: void
	 */
	private void checkShopAndStartPosition() {
		try {
			boolean isOne = true;
			int shopId = Integer.valueOf(userDTO.getInheritShopId());
			if (userDTO.getListRole() != null) {
				for (RoleUserDTO role : userDTO.getListRole()) {
					// co quan ly shop
					if (role.listShop != null && !role.listShop.isEmpty()) {
						// neu role chi co 1 shop moi kiem tra
						if (role.listShop.size() == 1) {
							if (shopId != 0
									&& shopId != role.listShop.get(0).shopId) {
								isOne = false;
								break;
							}
						}
					}
				}
				// neu user dang nhap ko phan quyen, hoac phan quyen dung 1 shop
				// start truoc luon dinh vi
				if (isOne) {
					checkAndStartPosition();
				}
			}
		}catch(Exception e){
			MyLog.e("checkShopAndStartPosition", "fail", e);
			ServerLogger.sendLog("LoginView", "Check phan quyen dinh vi: "
					+ VNMTraceUnexceptionLog.getReportFromThrowable(e),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

//	private void hardCodeSaveDBVersionInfo() {
//		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
//		Editor prefsPrivateEditor = sharedPreferences.edit();
//		prefsPrivateEditor.putString(VNM_STATUS_DOWNLOAD_DB, SynDataDTO.CONTINUE);
//		if (userDTO.dbVersion == null) {
//			userDTO.dbVersion = new DBVersionDTO();
//		}
//		userDTO.dbVersion.version = "1.0.0";
//		userDTO.dbVersion.lastLogId =  "64616897";
//		userDTO.dbVersion.maxDBLogId = "64617586";
//
//		prefsPrivateEditor.putString(VNM_VER_DB, userDTO.dbVersion.version.trim());
//		prefsPrivateEditor.putString(LAST_LOG_ID, userDTO.dbVersion.lastLogId);
//		prefsPrivateEditor.putString(MAX_LAST_LOG_ID, userDTO.dbVersion.maxDBLogId);
//		prefsPrivateEditor.commit();
//	}

	/**
	 * continue atfer login online success
	 * @author: duongdt3
	 * @since: 15:14:09 5 Dec 2014
	 * @return: void
	 * @throws:
	 */
	private void proccessAtferLoginOnline() {
		boolean isHaveNativeLib = loadNativeLib();
		// neu da co thu vien trong Internal Storage thi cho phep login
		if (isHaveNativeLib) {
			LoginView.this.numErrorNativeLib = 0;
//			userDTO.dbVersion != null && !StringUtil.isNullOrEmpty(userDTO.dbVersion.version)
			if (userDTO.getDbVersion() != null) {
				// reset db
				if (DBVersionDTO.RESET.equals(userDTO.getDbVersion().getAction())) {
					closeProgressDialog();
					GlobalUtil.showDialogConfirm(this, StringUtil
							.getString(R.string.TEXT_CONFIRM_RESET_DB),
							StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
							ACTION_OK_RESET_DB, StringUtil
									.getString(R.string.TEXT_BUTTON_CANCEL),
							ACTION_CANCEL_RESET_DB, null);
				} else {
					// updatedb
					executeSQLUpdateDB(userDTO.getDbVersion().getScript());
					saveDBInfoAfterUpdateDB();
//					continueAfterLoginSuccess();
//					selectRoleAfterLogin();
					selectShopAfterLogin();
				}
			} else {
//				continueAfterLoginSuccess();
//				selectRoleAfterLogin();
				// goToMainView();
				selectShopAfterLogin();
			}

		} else{
			proccessLoadNativeLibFail(PROCCESS_LOGIN_ONLINE);
		}
	}
	/**
	 * continue atfer login offline success
	 * @author: duongdt3
	 * @since: 16:05:00 5 Dec 2014
	 * @return: void
	 * @throws:
	 */
	private void proccessAfterLoginOffline() {
		boolean isHaveNativeLib = loadNativeLib();
		// neu da co thu vien trong Internal Storage thi cho phep login
		if (isHaveNativeLib) {
			LoginView.this.numErrorNativeLib = 0;

			// check va gui toan bo log dang con ton dong
			// TransactionProcessManager.getInstance().startChecking();
//			stateExistsDB = GlobalUtil.checkExistsDataBase();
//			if (stateExistsDB == SqlCipherUtil.NOT_EXIST_DB) {
//				// chua co file db
//				closeProgressDialog();
//				showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_DOWNLOAD_DB));
//			} else if (stateExistsDB == SqlCipherUtil.EXIST_DB_NOT_ENCRYPT) {
//				// thuc hien Cipher DB neu chua Cipher, tham so 1 login offline
//				showProgressDialog(
//						StringUtil.getString(R.string.PROCESS_CIPHER_DB), false);
//				new CipherTask().execute(
//						ExternalStorage.getFileDBPath(
//								GlobalInfo.getInstance().getAppContext())
//								.getAbsolutePath(), "1");
//			} else {
//				this.selectRoleAfterLogin();
//			}
			this.selectRoleAfterLogin();

		} else {
			proccessLoadNativeLibFail(PROCCESS_LOGIN_OFFLINE);
		}
	}

	/**
	 * continue atfer update version without require
	 * @author: duongdt3
	 * @since: 15:13:50 5 Dec 2014
	 * @return: void
	 * @throws:
	 */
	private void proccessAtferUpdateVersionFail() {
		//check native lib
		boolean isHaveNativeLib = loadNativeLib();
		// neu da co thu vien trong Internal Storage thi cho phep login
		if (isHaveNativeLib) {
			LoginView.this.numErrorNativeLib = 0;

			continueAfterLoginSuccess();

		} else{
			proccessLoadNativeLibFail(PROCCESS_UPDATE_VERSION_FAIL);
		}
	}

	 /**
		 * Xu li cho truong hop load lib tu thu muc ung dung
		 * Goi ham tai lib
		 * @author: Duongdt3
		 * @param typeProccess
		 * @return: void
		 * @throws:
		*/
		private void proccessLoadNativeLibFail(final int typeProccess) {
			//count num error native lib
			this.numErrorNativeLib ++;
			String log = DateUtils.now() + " numFail: " + numErrorNativeLib +  " " + Build.CPU_ABI + ", " + Build.CPU_ABI2;
			//delete libs
			ExternalStorage.deleteNativeLibPath(LoginView.this);
			if (this.numErrorNativeLib > 1) {
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setMessage(StringUtil.getString(R.string.ERR_LOAD_NATIVE_LIB));
				alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, StringUtil.getString(R.string.TEXT_FOLLOW_PROBLEM_OK), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if (LoginView.this != null) {
							//xu ly tai, unzip thu vien
							new DownloadNativeLibTask(typeProccess).execute(Build.CPU_ABI);
						}
					}
				});
				this.showDialog(alertDialog);
				ServerLogger.sendLog("Check native lib fail, redownload", log, false, TabletActionLogDTO.LOG_EXCEPTION);
			} else{
				if (!isDownloadNativeLibs) {
					//xu ly tai, unzip thu vien
					new DownloadNativeLibTask(typeProccess).execute(Build.CPU_ABI);
				}
			}

//			log.e("Check native lib fail, redownload", log);
		}

		/**
		 * Download lib dong
		 *
		 */
		private class DownloadNativeLibTask extends AsyncTask<String, Void, Exception> {
			int typeProccess = 0;

			public DownloadNativeLibTask(int typeProccess) {
				this.typeProccess = typeProccess;
			}

			@Override
			protected void onPreExecute() {
				if (LoginView.this != null) {
					isDownloadNativeLibs = true;
					showProgressPercentDialog(StringUtil.getString(R.string.TEXT_DOWNLOADING_NATIVE), false);
				}
			}

			@Override
			protected Exception doInBackground(String... params) {
				Exception result = new Exception("Not params cpu type");
				if (params != null) {
					for (int i = 0, size = params.length; i < size; i++) {
						String cpu = params[i];
						if (!StringUtil.isNullOrEmpty(cpu)) {
							String urlLib = ServerPath.SERVER_PATH_NATIVE_LIB + cpu + ".zip";
							try {
								ExternalStorage.downloadNativeLib(urlLib, LoginView.this);
								result = null;
								break;
							} catch (Exception e) {
								result = e;
								MyLog.e("Download Native Lib error", urlLib + " " + e.getMessage());
							}
						}
					}
				}
				return result;
			}

			@Override
			protected void onPostExecute(Exception result) {
				if (LoginView.this != null) {
					if (result != null) {
						//delete libs
						ExternalStorage.deleteNativeLibPath(LoginView.this);
						LoginView.this.isDownloadNativeLibs = false;
						// download loi
						LoginView.this.closeProgressDialog();
						String errorInfo = StringUtil.getString(R.string.ERR_DOWNLOAD_NATIVE_LIB);
						if (result instanceof DMSDownloadException && !StringUtil.isNullOrEmpty(result.getMessage())) {
							errorInfo += "\n" + result.getMessage();
						}
						LoginView.this.showDialog(errorInfo);
						MyLog.e("Download Native Lib error", DateUtils.now() + " ", result);
						ServerLogger.sendLog("Download Native Lib", MyLog.printStackTrace(result), false, TabletActionLogDTO.LOG_EXCEPTION);
					} else {
						//update 100% finish download native libs
						LoginView.this.updateProgressPercentDialog(100);
						MyLog.d("Download Native Lib finish", DateUtils.now());
						LoginView.this.isDownloadNativeLibs = false;
						LoginView.this.closeProgressDialog();
						switch (typeProccess) {
						case PROCCESS_LOGIN_ONLINE:
							proccessAtferLoginOnline();
							break;
						case PROCCESS_UPDATE_VERSION_FAIL:
							proccessAtferUpdateVersionFail();
							break;
						case PROCCESS_LOGIN_OFFLINE:
							proccessAfterLoginOffline();
							break;
						case PROCCESS_NEED_UPDATE_VERSION:
							proccessNeedUpdateVersion();
							break;
						default:
							break;
						}
					}
				}

			}
		}

		@Override
		public void onItemSelected(AdapterView<?> adap, View arg1, int pos, long arg3) {
			if(!isSelectedLanguages){
				LanguagesDTO lang =(LanguagesDTO) adap.getItemAtPosition(pos);
				GlobalInfo.getInstance().saveLanguages(lang.getValue());
				Intent intent = getIntent();
		        finish();
		        startActivity(intent);
			} else {
				isSelectedLanguages = false;
			}

		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}

	/**
	 * Xu li login offline
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void executeLoginOffline() {
		// luu thong tin user lai do luc chon shop, da cap nhat lai shop dc chon
		//luu lai thong tin shop dc chon
		saveUserInfo(userDTO);
		stateExistsDB = GlobalUtil.checkExistsDataBase();
		if (stateExistsDB == SqlCipherUtil.NOT_EXIST_DB) {
			// chua co file db
			closeProgressDialog();
			showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_DOWNLOAD_DB));
		} else if (stateExistsDB == SqlCipherUtil.EXIST_DB_NOT_ENCRYPT) {
			// thuc hien Cipher DB neu chua Cipher, tham so 1 login offline
			showProgressDialog(
					StringUtil.getString(R.string.PROCESS_CIPHER_DB), false);
			new CipherTask().execute(
					ExternalStorage.getFileDBPath(
							GlobalInfo.getInstance().getAppContext())
							.getAbsolutePath(), "1");
		} else {
			closeProgressDialog();

			//check truong hop can doi pass, sau do offline
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			boolean needChangePass = sharedPreferences.getBoolean(VNM_NEED_CHANGE_PASS, false);
			//check truong hop nang cap phien ban ma cancel, sau do login offline
			
			String oldVersion = sharedPreferences.getString(DMS_OLD_VERSION, "");
			String currentVersion = GlobalUtil.getAppVersion();
			boolean isNeedUpdateVersion = currentVersion != null && currentVersion.equals(oldVersion);

			// neu chua tung dang nhap online trong ngay
			int wrongTime = DateUtils.checkTabletRightTimeLoginOffline();
			
			if(needChangePass){
				showDialog(StringUtil.getString(R.string.ERROR_NEED_CHANGE_PASS));
			} else if(isNeedUpdateVersion){
				//neu chua tung dang nhap online trong ngay
				showDialog(StringUtil.getString(R.string.ERROR_NEED_UPDATE_APP));
			} else if(wrongTime != DateUtils.RIGHT_TIME){
				//neu chua dang nhap online trong ngay
				// cho phep nhap ma login offline xuat don hang
				boolean isAllow = cbAllowOffline.isChecked();
				if (isAllow) {
					showPopupConfirmedCode();
				} else {
					GlobalUtil.showDialogNotAllowOfflineLogin();
				}
			} else{
				goToMainView();
				isLoginOffline = false;
			}

		}

	}

	 /**
	 * Doi thong tin lien quan den current user thanh inherit user sau khi chon role
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void changeCurrentUserFromInheritUser(RoleUserDTO role){
//		userDTO = role.inheritProfile;
		userDTO.setAppVersion(role.appVersion);
		userDTO.setDbVersion(role.dbVersion);
		userDTO.setInheritSpecificType(role.inheritStaffSpecificType);
//		if (userDTO.staffId != role.inheritProfile.staffId && role.inheritProfile.staffId != -1) {
		userDTO.setInheritId(role.inheritProfile.getStaffId());
		userDTO.setInheritShopId(role.inheritProfile.getInheritShopId());
		userDTO.setInheritShopCode(role.inheritProfile.getInheritShopCode());
		userDTO.setInheritShopIdProfile(role.inheritProfile.getInheritShopIdProfile());
		userDTO.setInheritSaleTypeCode(role.inheritProfile.getInheritSaleTypeCode());
//		}

		//luu thong tin truoc khi tien hanh check, tai thu vien
		//dam bao fullDate log loi neu co
		saveUserInfo(userDTO);

		//saveServerDateAfterLogin();
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		if (userDTO.getAppVersion() != null
				&& !StringUtil.isNullOrEmpty(userDTO.getAppVersion().content)) {
			isNeedUpdateVersionGlobal  = true;

			//luu version hien tai de khi login offline + cancel check lai
			if (userDTO.getAppVersion().forceDownload) {
				sharedPreferences.edit().putString(DMS_OLD_VERSION, GlobalUtil.getAppVersion()).commit();
			} else{
				//bo check can cap nhat phien ban moi
				sharedPreferences.edit().putString(DMS_OLD_VERSION, "").commit();
			}

			proccessNeedUpdateVersion();
		} else {
			//bo check can cap nhat phien ban moi
			isNeedUpdateVersionGlobal = false;
			sharedPreferences.edit().putString(DMS_OLD_VERSION, "").commit();
			//tiep tuc login
//			continueAfterLoginSuccess();
			proccessAtferLoginOnline();
		}
	}

	 /**
	 * Chon shop sau khi login xong
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void selectShopAfterLogin(){
		//co quan ly shop
		if(userDTO.getRoleManaged().listShop != null && !userDTO.getRoleManaged().listShop.isEmpty()){
			// chi quan ly 1 shop
			if (userDTO.getRoleManaged().listShop.size() == 1) {
				// cho tu dong dang nhap voi role nay, shop nay
				userDTO.setShopManaged(userDTO.getRoleManaged().listShop.get(0));
				userDTO.setInheritShopId("" + userDTO.getShopManaged().shopId);
				userDTO.setInheritShopCode(userDTO.getShopManaged().shopCode);
				userDTO.setInheritShopLat(userDTO.getShopManaged().shopLat);
				userDTO.setInheritShopLng(userDTO.getShopManaged().shopLng);
				if (isLoginOffline) {
					executeLoginOffline();
				}else{
					continueAfterLoginSuccess();
				}
			} else { // quan ly nhieu shop
				showDialogSelectShop(userDTO.getRoleManaged().listShop);
			}
		} else{ //khong quan ly shop
//			userDTO.inheritShopId = userDTO.inheritShopIdProfile;
//			if (isLoginOffline) {
//				executeLoginOffline();
//			}else
//				continueAfterLoginSuccess();
			closeProgressDialog();
			showDialog(StringUtil.getString(R.string.TEXT_NOT_HAVE_SHOP));
		}

	}

	/**
	 * Show popup confirmed ma code dang nhap offline
	 * @author: banghn
	 */
	private void showPopupConfirmedCode(){
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(StringUtil.getString(R.string.TEXT_CONFIRM_LOGIN_OFFLINE));
		alert.setMessage(StringUtil.getString(R.string.TEXT_REQUIRE_INPUT_CONFIRM_CODE));

		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		input.setHint(StringUtil.getString(R.string.TEXT_INPUT_CONFIRM_CODE));
		input.setTextColor(ImageUtil.getColor(R.color.WHITE));
		alert.setView(input);

		alert.setPositiveButton(StringUtil.getString(R.string.TEXT_CONFIMRED), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				if(checkCodeAllowLoginOffline(value.trim().toLowerCase())){
					//cap nhat thoi gian, xem nhu thoi gian confirmed voi CSKH la thoi giandung
					//cap nhat luu tru trang thai
					DateUtils.updateRightTime(DateUtils.now());
					GlobalInfo.getInstance().setStateConnectionMode(Constants.CONNECTION_OFFLINE);
					//thanh cong vao ben trong
					goToMainView();
					isLoginOffline = false;
				}else{
					LoginView.this.showDialog(StringUtil.getString(R.string.TEXT_INPUT_WRONG_CONFIRM_CODE_PLEASE_REINPUT));
				}
			}
		});
		alert.show();
	}

	/**
	 * Code cho phep dang nhap offline
	 * @author: banghn
	 * @return
	 */
	private boolean checkCodeAllowLoginOffline(String code){
		boolean isRightCode = false;
		try {
			String userName = edUserName.getText().toString().trim().toLowerCase();
			String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
			if(StringUtil.isNullOrEmpty(code) || StringUtil.isNullOrEmpty(userName)){
				isRightCode = false;
			}else if (code.equals(StringUtil.generateHash(dateNow, userName).substring(0, 3))
					|| code.equals(StringUtil.generateHash(dateNow, "vtvnm").substring(0, 3))) {
				isRightCode = true;
			}
		} catch (Exception ex) {
			ServerLogger.sendLog("Login", "check code dang nhap offline :\n"
					+ VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					false, TabletActionLogDTO.LOG_EXCEPTION);
		}
		return isRightCode;
	}

	private void proccessNeedUpdateVersion() {
		MyLog.d("proccessNeedUpdateVersion", "start");
		//check native libs
		boolean isHaveNativeLib = loadNativeLib();
		// neu da co thu vien trong Internal Storage
		if (isHaveNativeLib) {
			LoginView.this.numErrorNativeLib = 0;
			int stateDB = GlobalUtil.checkExistsDataBase();
			//ton tai db da cipher
			if (stateDB == 2) {
				showProgressDialog(StringUtil.getString(R.string.UPDATING_BEFORE_UP_VERSION));
				//fullDate het du lieu hien tai len, dam bao fullDate het len moi cap nhat phien ban
				TransactionProcessManager.getInstance().startChecking(TransactionProcessManager.SYNC_NORMAL);
			} else{
				isNeedUpdateVersionGlobal = false;
				proccessUpdateVersion();
			}
		} else{
			proccessLoadNativeLibFail(PROCCESS_NEED_UPDATE_VERSION);
		}
		MyLog.d("proccessNeedUpdateVersion", "end");
	}

	private void proccessUpdateVersion() {
		MyLog.d("proccessUpdateVersion", "start");
		closeProgressDialog();
		if (userDTO.getAppVersion() != null && !StringUtil.isNullOrEmpty(userDTO.getAppVersion().content)) {
			MyLog.d("proccessUpdateVersion", "proccess");
			String strCancel = "";
			if (userDTO.getAppVersion().forceDownload) {
				strCancel = StringUtil.getString(R.string.TEXT_DENY);
			} else {
				strCancel = StringUtil.getString(R.string.TEXT_UPDATE_LATER);
			}

			GlobalUtil.showDialogConfirm(this, StringUtil.getString(R.string.TEXT_NOTICE_UPDATE_VERSION),
					userDTO.getAppVersion().content, StringUtil.getString(R.string.TEXT_UPDATE), ACTION_UPDATE_VERSION_SUCCESS,
					strCancel, ACTION_UPDATE_VERSION_FAIL, userDTO);
		}
		MyLog.d("proccessUpdateVersion", "end");
	}
	
	protected void displayCskhInfoTitle() {
		TextView tvCskhInfo = (TextView) findViewById(R.id.tvCskhInfo);
		if (tvCskhInfo != null) {
			String res = GlobalInfo.getInstance().getCskhInfo();
			if (StringUtil.isNullOrEmpty(res)) {
				tvCskhInfo.setVisibility(View.GONE);
			} else{
				tvCskhInfo.setVisibility(View.VISIBLE);
				tvCskhInfo.setText(res);
			}

		}
	}
	
	private void showDialogChangePass() {
		closeDialog(alertChangePassDialog);
		
		Builder build = new AlertDialog.Builder(this, R.style.CustomDialogTheme);
		DialogChangePasswordView view = new DialogChangePasswordView(this, ACTION_CHANGE_PASS, edUserName.getText().toString());
		build.setView(view.viewLayout);
		alertChangePassDialog = build.create();
		Window window = alertChangePassDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		showDialog(alertChangePassDialog);
	}
}
