/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import android.database.Cursor;

import com.ths.dmscore.dto.db.ReportTemplateDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.db.REPORT_TEMPLATE_TABLE;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.ReportTemplateCriterionDTO;
import com.ths.dmscore.lib.sqllite.db.REPORT_TEMPLATE_OBJECT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * ReportTemplateDTO.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 10:54:46 AM Oct 27, 2014
 */
public class ChooseParametersDTO extends
		AbstractTableDTO implements
		Serializable {

	private static final long serialVersionUID = 1L;

	public int reportTemplateId;
	public String name;
	public String kpiName;
	public boolean isShowPlan;
	public boolean isShowReality;
	public boolean isShowColumnTotal;
	public boolean isShowRowTotal;
	public String criRowName;
	public String criSubRowName;
	public String criColumnName;
	public int criRowId;
	public int criSubRowId;
	public int criColumnId;
	public String periodType;
	public int periodCycleType = 0; // loai chu ki
	public int parentPeriodTypeId;// loai chu ki cha
	public int numRecentParentPeriod = 0;// trong thang hien tai hoac trong 2 thang hien tai ....
	public String fromDate;
	public String toDate;
	public String fromDateView;
	public String toDateView;
	// chi danh cho Thang Quy
	public String value;
	private int type;

	public int totalItem;
	public boolean isFirstTime ; // bien chi lay nhung thong tin mot lan
	public ArrayList<ListParametersChosenDTO> listColumn;
	public ArrayList<ListParametersChosenDTO> listRow;
	public ArrayList<PopupChoseObjectItem> listColumnTime;
	private boolean isAddSelf = false; // da add chinh no vao ds cac param chua

	public ChooseParametersDTO(
			TableType type) {
		super(type);
		// TODO Auto-generated constructor stub
	}

	public ChooseParametersDTO() {
		// TODO Auto-generated constructor stub
		isAddSelf = false;
		isFirstTime = false;
		listColumn = new ArrayList<ListParametersChosenDTO>();
		listRow = new ArrayList<ListParametersChosenDTO>();
		listColumnTime = new ArrayList<PopupChoseObjectItem>();
	}

	/**
	 * init from cursor
	 *
	 * @author: dungdq3
	 * @since: 11:08:54 AM Oct 27, 2014
	 * @return: void
	 * @throws:
	 * @param c
	 *            :
	 */
	public void initFromCursor(Cursor c) {

		name = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.NAME);
		kpiName = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.KPI_ID);
		criRowName = CursorUtil.getString(c, "CRI_ROW_NAME");
		criSubRowName = CursorUtil.getString(c, "CRI_SUB_ROW_NAME");
		criColumnName = CursorUtil.getString(c, "CRI_COLUMN_NAME");
		criRowId = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.CRI_ROW_ID);
		criSubRowId = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.CRI_SUB_ROW_ID);
		criColumnId= CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.CRI_COLUMN_ID);
		periodType = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.PERIOD_TYPE_ID);
		periodCycleType = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.PERIOD_CYCLE_TYPE);
		parentPeriodTypeId = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.PARENT_PERIOD_TYPE_ID);
		numRecentParentPeriod = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.NUM_RECENT_PARENT_PERIOD);

		int isShowPlan = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.IS_SHOW_PLAN);
		if (isShowPlan == 1) {
			this.isShowPlan = true;
		} else {
			this.isShowPlan = false;
		}
		int isShowReality = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.IS_SHOW_REALITY);
		if (isShowReality == 1) {
			this.isShowReality = true;
		} else {
			this.isShowReality = false;
		}
		int isShowColumnTotal = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.IS_SHOW_COLUMN_TOTAL);
		if (isShowColumnTotal == 1) {
			this.isShowColumnTotal = true;
		} else {
			this.isShowColumnTotal = false;
		}
		int isShowRowTotal = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.IS_SHOW_ROW_TOTAL);
		if (isShowRowTotal == 1) {
			this.isShowRowTotal = true;
		} else {
			this.isShowRowTotal = false;
		}
		type = CursorUtil.getInt(c, REPORT_TEMPLATE_OBJECT_TABLE.TYPE);
		if(!isFirstTime){
			fromDate = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.FROM_DATE);
			toDate = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.TO_DATE);
			handleColumnTime();
			isFirstTime = true;
		}
		if (type == Constants.COLUMN) {
			String objectName = CursorUtil.getString(c, "ROW_NAME");
			if (!StringUtil.isNullOrEmpty(objectName)) {
				String[] arrRow = objectName.split(",");
				if (arrRow.length > 0) {
					String[] arrSplit = arrRow[0].split(Constants.groupString);
					if (arrSplit.length > 2) {
						ListParametersChosenDTO dto = new ListParametersChosenDTO();
						dto.objectName = arrSplit[0];
						dto.infoName = arrSplit[2];
						dto.objectID = CursorUtil.getInt(c,
								"OBJECT_ID");
						dto.value = CursorUtil.getString(c, "VALUE");
						dto.typeID = CursorUtil.getInt(c, "COLUMNID");
						dto.reportTemplateObjectID = CursorUtil
								.getInt(c,
										"REPORT_TEMPLATE_OBJECT_ID");
						dto.status = Integer.parseInt(arrSplit[1]);
						listColumn.add(dto);
					}
				}

			}
		} else if (type == Constants.ROW) {
			String objectName = CursorUtil.getString(c, "ROW_NAME");
			if (!StringUtil.isNullOrEmpty(objectName)) {
				String[] arrRow = objectName.split(",");
				if (arrRow.length > 0) {
					for (int i = 0, size = arrRow.length; i < size; i++) {
						ListParametersChosenDTO dto = new ListParametersChosenDTO();
						String code = "";
						int chanelObjectType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
						dto.objectName = arrRow[i];
						if(!StringUtil.isNullOrEmpty(dto.objectName))
							code = dto.objectName.split(Constants.groupString)[0];
						//doi tuong dang lay co phai la chinh no hay ko
						boolean isSefl = false;
						// truong hop nay lay mac dinh staff dang nhap nen ko quan tam toi object_id trong bang report_template_object nua
						// chi xet doi voi truong hop nhan vien bh dang nhap
						if (GlobalInfo.getInstance().getProfile().getUserData().getUserCode()
								.equals(code) && (chanelObjectType == UserDTO.TYPE_STAFF)  ) {
							dto.objectID = GlobalInfo.getInstance()
									.getProfile().getUserData().getInheritId();
							isSefl = true;
						} else {
							dto.objectID = CursorUtil.getInt(c,
									"OBJECT_ID");
						}
						dto.value = CursorUtil.getString(c, "VALUE");
						dto.typeID = CursorUtil.getInt(c, "ROWID");
						dto.reportTemplateObjectID = CursorUtil
								.getInt(c,
										"REPORT_TEMPLATE_OBJECT_ID");
						String[] arrSplit = arrRow[i].split(Constants.groupString);
						// add ca status, id
						// tat cac cau select phai lon = 4 phan tu
						if (arrSplit.length > 3) {
							dto.status = Integer.parseInt(arrSplit[2]);
							dto.infoName = arrSplit[3];
						}
						if(!isAddSelf || !isSefl){
							listRow.add(dto);
							// da add chinh nvbh vao de xuat du lieu
							isAddSelf = true;
						}
					}

				}

			}
		}
	}

	/**
	 * Xu ly DTO cho popup column thoi gian
	 * @author: hoanpd1
	 * @since: 10:02:31 29-01-2015
	 * @return: void
	 * @throws:
	 */
	public void handleColumnTime() {
		// xem co phai lay bao cao tuan hien tai ko
		boolean isWeekcurrent = false;
		if (criColumnId == ReportTemplateCriterionDTO.WEEK
				|| criColumnId == ReportTemplateCriterionDTO.MONTH
				|| criColumnId == ReportTemplateCriterionDTO.QUARTER
				|| criColumnId == ReportTemplateCriterionDTO.YEAR
				|| criColumnId == ReportTemplateCriterionDTO.DATE) {

			Date dateNow = DateUtils.now(DateUtils.DATE_FORMAT_NOW);
			// loai gia tri tu ngay toi ngay
			if (periodCycleType == ReportTemplateDTO.DATE) {
				fromDateView = fromDate;
				toDateView = toDate;
			} else if (periodCycleType == ReportTemplateDTO.CURRENT_PERIOD) {
				// loai KPI theo tuan
				if (criColumnId == ReportTemplateCriterionDTO.WEEK) {
					isWeekcurrent = true;
					fromDateView = DateUtils.getFirstDateOfWeek(
							DateUtils.DATE_FORMAT_DATE, dateNow,0);
					toDateView = DateUtils
							.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
//					toDateView = "2015-02-01";
				} else if (criColumnId == ReportTemplateCriterionDTO.MONTH) {
					fromDateView = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, dateNow,0);
					toDateView = DateUtils
						.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
				} else if (criColumnId == ReportTemplateCriterionDTO.QUARTER) {
					fromDateView = DateUtils.getFirstDateOfQuarter(
							DateUtils.DATE_FORMAT_DATE, dateNow,0);
					toDateView = DateUtils.getLastDateOfQuarter(
							DateUtils.DATE_FORMAT_DATE, dateNow);
				} else if (criColumnId == ReportTemplateCriterionDTO.YEAR) {
					fromDateView = DateUtils.getFirstDateOfYear(
							DateUtils.DATE_FORMAT_DATE, dateNow,0);
					toDateView = DateUtils.getLastDateOfYear(
							DateUtils.DATE_FORMAT_DATE, dateNow);
				}
			} else if (periodCycleType == ReportTemplateDTO.WITHIN_PARENT_PERIOD) {
//				int currentQuarter = DateUtils.getQuarter(dateNow);
				int currentMonth = DateUtils.getMonth(dateNow);
//				int currentWeek = DateUtils.getWeekOfMonth(dateNow);
				// xu li cho truong hop tuan
				if (criColumnId == ReportTemplateCriterionDTO.WEEK) {
					int numWeek = 0;
					// tuan trong thang hien tai
					if(parentPeriodTypeId == ApParamDTO.PERIOD_MONTH ){
						numWeek = numRecentParentPeriod - 1;
					}
					fromDateView = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, dateNow, -numWeek);
				} else if (criColumnId == ReportTemplateCriterionDTO.MONTH) {// xu li cho truong hop chon thang
					int numMonth = 0;
					if (currentMonth < 3) {
						// truong hop nay la quy 1
						numMonth = currentMonth;
					} else {
						// thang trong quy hien tai
						if (parentPeriodTypeId == ApParamDTO.PERIOD_QUARTER) {
							numMonth = currentMonth - numRecentParentPeriod * 3;
						} else if (parentPeriodTypeId == ApParamDTO.PERIOD_YEAR) {// thang trong nam hien tai
							numMonth = currentMonth - numRecentParentPeriod
									* 12;
						}
					}
					fromDateView = DateUtils
							.getFirstDateOfNumberPreviousMonthWithFormat(
									DateUtils.DATE_FORMAT_DATE, dateNow,
									-numMonth);
				} else if (criColumnId == ReportTemplateCriterionDTO.QUARTER) {// xu li cho quy
					int numYear = 0;
					if(parentPeriodTypeId == ApParamDTO.PERIOD_YEAR ){// quy trong nam hien tai
						numYear = numRecentParentPeriod - 1;
					}
					fromDateView = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_DATE, dateNow,-numYear);
				} else if(criColumnId == ReportTemplateCriterionDTO.YEAR) {// xu li cho nam
					int numYear = 0;
//					if(parentPeriodTypeId == ApParamDTO.PERIOD_YEAR){ // nam hien tai
						numYear = - numRecentParentPeriod + 1;
//					}
					fromDateView = DateUtils.getFirstDateOfYear(
							DateUtils.DATE_FORMAT_DATE, dateNow,numYear);
				}else{// truong hop ngay
					// ngay trong thang hien tai
					if(parentPeriodTypeId == ApParamDTO.PERIOD_MONTH ){
						fromDateView = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(
								DateUtils.DATE_FORMAT_DATE, dateNow,0);
					}else{ // ngay trong tuan hien tai
						fromDateView = DateUtils.getFirstDateOfWeek(
								DateUtils.DATE_FORMAT_DATE, dateNow,0);
					}
				}
				toDateView = DateUtils
						.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
			}
			// gan lai thoi gian fromdate, todate cho truong hop ko co du lieu thoi gian trong bang report_template
			fromDate = fromDateView;
			toDate = toDateView;
			initDateTime(fromDateView, toDateView, isWeekcurrent);

		}else{
			//khi bao cao theo cac loai khac thi ko quan tam toi ngay nua
			fromDate = "";
			toDate = "";
			fromDateView = "";
			toDateView = "";
		}

	}

	/**
	 * init from cursor dung cho popup thoi gian tuan, thang, quy, nam
	 * @author: hoanpd1
	 * @since: 10:10:11 29-01-2015
	 * @return: void
	 * @throws:
	 * @param fromDateView2
	 * @param toDateView2
	 */
	private void initDateTime(String strFromDateCurrent, String strToDateCurrent, boolean isWeekcurrent) {
		int currentWeek = 0;
		if (listColumnTime != null) {
			int timeId = 0;
			while (DateUtils.compareWithNow(strFromDateCurrent, DateUtils.DATE_FORMAT_DATE) != 1
					&& DateUtils.compareDate(strFromDateCurrent, strToDateCurrent) != 1) {
				String strFromDate = "";
				String strToDate = "";
				Date fromDateCurrent = DateUtils.parseDateFromString(
						strFromDateCurrent,
						DateUtils.DATE_FORMAT_DATE);
				PopupChoseObjectItem itemTime = new PopupChoseObjectItem();
				timeId++;
				itemTime.objectTimeId = timeId;
				Date fromDate = DateUtils.parseDateFromString(strFromDateCurrent,DateUtils.DATE_FORMAT_DATE);
				String lastDateInMonth = DateUtils.getLastDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, fromDateCurrent, 0);
				String firstDateInMonthAfter = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, fromDateCurrent, 1);
				int currentQuarter = DateUtils.getQuarter(fromDate);
				int currentMonth = DateUtils.getMonth(fromDate);
				int currentYear = DateUtils.getYear(fromDate);

				if (criColumnId == ReportTemplateCriterionDTO.WEEK) {
					currentMonth++;
//					itemTime.nameTime = Constants.STR_BLANK + (1 + currentWeek);
					itemTime.nameTimeView = StringUtil.getString(R.string.TEXT_WEEK) + Constants.STR_SPACE + (1 + currentWeek);
					currentWeek++;
					strFromDate = DateUtils
							.getFirstDateOfWeek(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 0);
					if(DateUtils.compare(strFromDate,strFromDateCurrent,DateUtils.DATE_FORMAT_DATE)== -1){
						strFromDate  = strFromDateCurrent;
					}
					// so sanh cong them mot tuan neu lon hon ngay hien tai thi gan todate bang ngay hien tai
					if (DateUtils.compareWithNow(DateUtils.getFirstDateOfWeek(
							DateUtils.DATE_FORMAT_DATE, fromDateCurrent, 1),
							DateUtils.DATE_FORMAT_DATE) > 0) {
						strToDate = DateUtils.convertFormatDate(
								DateUtils.now(), DateUtils.DATE_FORMAT_NOW,
								DateUtils.DATE_FORMAT_DATE);
					} else {
						strToDate = DateUtils.getLastDateOfWeek(
								DateUtils.DATE_FORMAT_DATE, fromDateCurrent);
					}
					if(isWeekcurrent){// neu la tuan hien tai thi se lay luon fomdate , todate
						strFromDate = strFromDateCurrent;
						strToDate = strToDateCurrent;
						strFromDateCurrent = DateUtils
								.getFirstDateOfWeek(
										DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
					}else {
						if(DateUtils.compareDate(strToDate, lastDateInMonth)== 1 ){
							strToDate = lastDateInMonth;
							strFromDateCurrent = firstDateInMonthAfter;
						}else {
							strFromDateCurrent = DateUtils
									.getFirstDateOfWeek(
											DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
						}
					}

					itemTime.fromDateView = DateUtils.convertFormatDate(
							strFromDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.toDateView = DateUtils.convertFormatDate(
							strToDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
				} else if (criColumnId == ReportTemplateCriterionDTO.MONTH) {
//					itemTime.nameTime = String.format("%02d",(currentMonth+1)) + "/" + currentYear;
					strFromDate = DateUtils
							.getFirstDateOfNumberPreviousMonthWithFormat(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 0);
					if (DateUtils.compareWithNow(strFromDateCurrent,
							DateUtils.DATE_FORMAT_MONTH_YEAR) == 0) {
						strToDate = DateUtils
								.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
					} else {
						strToDate = DateUtils
								.getLastDateOfNumberPreviousMonthWithFormat(
										DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 0);
					}
					itemTime.fromDateView = DateUtils.convertFormatDate(
							strFromDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.toDateView = DateUtils.convertFormatDate(
							strToDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.nameTimeView = StringUtil.getString(R.string.TEXT_MONTH) + Constants.STR_SPACE + String.format("%02d",(currentMonth+1)) + "/" + currentYear;;
					// cap nhat len thang hien tai
					strFromDateCurrent = DateUtils
							.getFirstDateOfNumberPreviousMonthWithFormat(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
				}else if(criColumnId == ReportTemplateCriterionDTO.QUARTER){
					strFromDate = DateUtils.getFirstDateOfQuarter(
							DateUtils.DATE_FORMAT_DATE, DateUtils
									.parseDateFromString(strFromDateCurrent,
											DateUtils.DATE_FORMAT_DATE), 0);
					if (DateUtils.compareWithNow(strFromDateCurrent,
							DateUtils.DATE_FORMAT_MONTH_YEAR) == 0) {
						strToDate = DateUtils
								.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
					} else {
						strToDate = DateUtils
								.getLastDateOfQuarter(
										DateUtils.DATE_FORMAT_DATE,
										DateUtils.parseDateFromString(
												strFromDateCurrent,
												DateUtils.DATE_FORMAT_DATE));
					}
//					itemTime.nameTime = currentQuarter + "/" + currentYear;
					itemTime.fromDateView = DateUtils.convertFormatDate(
							strFromDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.toDateView = DateUtils.convertFormatDate(
							strToDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.nameTimeView = StringUtil.getString(R.string.TEXT_QUARTER) + Constants.STR_SPACE  + currentQuarter + "/" + currentYear;
					strFromDateCurrent = DateUtils
							.getFirstDateOfQuarter(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
				}else if(criColumnId == ReportTemplateCriterionDTO.YEAR){
					strFromDate = DateUtils.getFirstDateOfYear(
									DateUtils.DATE_FORMAT_DATE,DateUtils.parseDateFromString(strFromDateCurrent,DateUtils.DATE_FORMAT_DATE), 0);
					if (DateUtils.compareWithNow(strFromDateCurrent,DateUtils.DATE_FORMAT_MONTH_YEAR) == 0) {
						strToDate = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
					} else {
						strToDate = DateUtils.getLastDateOfYear(
										DateUtils.DATE_FORMAT_DATE,DateUtils.parseDateFromString(strFromDateCurrent,DateUtils.DATE_FORMAT_DATE));
					}
//					itemTime.nameTime = Constants.STR_BLANK+ currentYear;
					itemTime.fromDateView = DateUtils.convertFormatDate(
							strFromDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.toDateView = DateUtils.convertFormatDate(
							strToDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.nameTimeView = StringUtil.getString(R.string.TEXT_YEAR) + Constants.STR_SPACE + currentYear;
					strFromDateCurrent = DateUtils.getFirstDateOfYear(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
				}else{
					// xu li truong hop ngay
//					itemTime.nameTime = strFromDateCurrent;
					strFromDate = strFromDateCurrent;
					strToDate = strFromDateCurrent;
					itemTime.fromDateView = DateUtils.convertFormatDate(
							strFromDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.toDateView = DateUtils.convertFormatDate(
							strToDate, DateUtils.DATE_FORMAT_DATE,
							DateUtils.DATE_FORMAT_DATE_VN);
					itemTime.nameTimeView = itemTime.fromDateView;
					strFromDateCurrent = DateUtils.getOffsetDay(
									DateUtils.DATE_FORMAT_DATE,fromDateCurrent, 1);
				}

				listColumnTime.add(itemTime);


			}
			totalItem = timeId;
		}

	}

}
