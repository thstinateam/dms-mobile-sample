/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.lib.sqllite.db.ShopSupervisorDTO;
import com.ths.dmscore.util.LatLng;

/**
 *  Thong tin ds cham cong cua GSNPP
 *  @author: Nguyen Thanh Dung
 *  @version: 1.1
 *  @since: 1.0
 */

public class GSNPPTakeAttendaceDTO {
	public List<ShopParamDTO> listInfo;
	public List<AttendanceDTO> listStaff;
	public List<ShopSupervisorDTO> listCombox;
	public LatLng shopPosition;
	
	/**
	 * 
	 */
	public GSNPPTakeAttendaceDTO() {
		listInfo = new ArrayList<ShopParamDTO>();
		listStaff = new ArrayList<AttendanceDTO>();
		listCombox = new ArrayList<ShopSupervisorDTO>();
		shopPosition = new LatLng();
	}
}
