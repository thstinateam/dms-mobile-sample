package com.ths.dmscore.dto.view;

public class PaidShowRoomPromotionDto {
	public String programCode;
	public String programName;
	public String cusCode;
	public String cusName;
	public String fromDate;
	public String toDate;
	public int level;
	public PaidPromotionItem item = new PaidPromotionItem();
	
	public PaidShowRoomPromotionDto(){
		
	}
	
	
	public class PaidPromotionItem{
		public String productCode;
		public String productName;
		public String productPrice;
		public String realNumber;
		public String promotionNumber;
		
		public PaidPromotionItem() {
			productCode = "02AA10";
			productName = "SB D.Alpha Step 1 HT 400g";
			productPrice = "88,700";
			realNumber = "6";
			promotionNumber = "MN05121003";
		}
	}
}
