/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Thong tin mo ta doanh so sku cua khach hang mua
 * @author BANGHN
 * @version 1.0
 */
@SuppressWarnings("serial")
public class CustomerSaleSKUDTO implements Serializable{
	//id san pham
	public long productId;
	//ma san pham
	public String productCode;
	//ten san pham
	public String productName;
	//doanh so cua san pham
	public double amount;
	//san luong cua san pham
	public long quantity;


	/**
	 * Parse du lieu sau khi select tu db
	 * @author: BANGHN
	 * @param c
	 */
	public void parseFromCursor(Cursor c, int sysCurrencyDivide) {
		productId = CursorUtil.getLong(c, "PRODUCT_ID");
		productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		productName = CursorUtil.getString(c, "PRODUCT_NAME");
		amount = CursorUtil.getDouble(c, "AMOUNT");
		quantity = CursorUtil.getLong(c, "QUANTITY");
	}
}
