/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * Thong tin cua mot don hang
 * @author : BangHN
 * since   : 1.0
 * version : 1.0
 */
public class SaleOrderCustomerDTO implements Serializable{
	private static final long serialVersionUID = -525660063352563033L;
	SaleOrderDTO saleOder;//thong tin don hang trong bang: SALE_ORDER
	int SKU = 0;//so luong product trong mot don hang

	public SaleOrderCustomerDTO() {
		saleOder = new SaleOrderDTO();
	}

	public SaleOrderDTO getSaleOder() {
		return saleOder;
	}

	public void setSaleOder(SaleOrderDTO saleOder) {
		this.saleOder = saleOder;
	}

	public int getSKU() {
		return SKU;
	}

	public void setSKU(int sKU) {
		SKU = sKU;
	}

	/**
	 * Thong tin chung
	 * @author : BangHN
	 * since : 2:36:49 PM
	 */
	public static SaleOrderCustomerDTO initOrderFromCursor(Cursor cursor, int sysCurrencyDivide) {
		if(cursor != null){
			SaleOrderCustomerDTO dto = new SaleOrderCustomerDTO();
			dto.saleOder.saleOrderId = CursorUtil.getLong(cursor, "SALE_ORDER_ID");
			dto.saleOder.orderDate = CursorUtil.getString(cursor, "ORDER_DATE");
			dto.saleOder.orderNumber = CursorUtil.getString(cursor, "ORDER_NUMBER");
			dto.saleOder.setTotal(CursorUtil.getDouble(cursor, "TOTAL"));
			dto.SKU = CursorUtil.getInt(cursor, "SKU");
			return dto;
		}
		return null;
	}
	
}
