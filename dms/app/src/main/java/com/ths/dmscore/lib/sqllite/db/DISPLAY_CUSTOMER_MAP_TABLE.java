/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.DisplayPresentProductInfo;
import com.ths.dmscore.dto.view.VoteDisplayPresentProductViewDTO;
import com.ths.dmscore.dto.view.VoteDisplayProductDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua lop (interface)
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class DISPLAY_CUSTOMER_MAP_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String DISPLAY_CUSTOMER_MAP_ID = "DISPLAY_CUSTOMER_MAP_ID";
	// id bang nv phat trien cttb
	public static final String DISPLAY_STAFF_MAP_ID = "DISPLAY_STAFF_MAP_ID";
	// id muc cua cttb
	public static final String DISPLAY_PROGRAM_LEVEL_ID = "DISPLAY_PROGRAM_LEVEL_ID";
	// id kh tham gia cttb
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// trang thai
	public static final String STATUS = "STATUS";
	// tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// den ngay
	public static final String TO_DATE = "TO_DATE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// NGUOI TAO
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String SHOP_ID = "SHOP_ID";

	private static final String TABLE_DISPLAY_CUSTOMER_MAP = "DISPLAY_CUSTOMER_MAP";

	public DISPLAY_CUSTOMER_MAP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_DISPLAY_CUSTOMER_MAP;
		this.columns = new String[] { DISPLAY_CUSTOMER_MAP_ID,
				DISPLAY_STAFF_MAP_ID, DISPLAY_PROGRAM_LEVEL_ID, CUSTOMER_ID,
				STATUS, FROM_DATE, TO_DATE, CREATE_DATE, CREATE_USER,
				UPDATE_USER, UPDATE_DATE, SHOP_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * 
	 * get amount got of customer for display program
	 * 
	 * @param displayProgramId
	 * @param displayProgramLevelId
	 * @param customerId
	 * @return
	 * @return: long
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 15, 2013
	 */
	public long getAmountGotOfCustomerForDisPro(String displayProgramId,
			String displayProgramLevelId, String customerId) {
		long totalAmount = 0;
		StringBuffer sqlGetAmount = new StringBuffer();
		sqlGetAmount.append("SELECT Ifnull(Sum(CASE ");
		sqlGetAmount
				.append("                    WHEN so.order_type = 'CM' THEN -sod.amount ");
		sqlGetAmount
				.append("                    WHEN so.order_type = 'CO' THEN -sod.amount ");
		sqlGetAmount.append("                    ELSE sod.amount ");
		sqlGetAmount.append("                  END), 0) TOTALAMOUNT ");
		sqlGetAmount.append("FROM   sales_order so, ");
		sqlGetAmount.append("       sales_order_detail sod ");
		sqlGetAmount.append("WHERE  1 = 1 ");
		sqlGetAmount.append("       AND so.customer_id = ? ");
		sqlGetAmount
				.append(" AND ((so.state = 2 AND dayInOrder(so.order_date) < dayInOrder('now','localtime') AND dayInOrder(so.order_date) >= DATE('NOW','localtime','start of month'))");
		sqlGetAmount
				.append(" OR (so.state = 1 AND so.is_send = 1 AND dayInOrder(so.order_date) = dayInOrder('now','localtime')))");
		sqlGetAmount.append("       AND so.sale_order_id = sod.sale_order_id ");
		sqlGetAmount
				.append("                               AND ( (SELECT Strftime('%m', 'now', 'localtime')) ");
		sqlGetAmount.append("                                     = (SELECT ");
		sqlGetAmount
				.append("                                         Strftime('%m', sod.order_date)) ) ");
		sqlGetAmount
				.append("       AND sod.product_id IN (SELECT dpd.product_id product_id ");
		sqlGetAmount
				.append("                              FROM   display_program_detail dpd, ");
		sqlGetAmount
				.append("                                     display_program dp, ");
		sqlGetAmount
				.append("                                     display_program_level dpl ");
		sqlGetAmount
				.append("                              WHERE  dp.status = 1 ");
		sqlGetAmount
				.append("                                     AND Date(dp.from_date) <= ");
		sqlGetAmount
				.append("                                         Date('now', 'localtime') ");
		sqlGetAmount
				.append("                                     AND Ifnull(Date(dp.to_date) > ");
		sqlGetAmount
				.append("                                                Date('now', 'localtime') ");
		sqlGetAmount.append("                                         , 1) ");
		sqlGetAmount
				.append("                                     AND dpl.display_program_id = ");
		sqlGetAmount
				.append("                                         dp.display_program_id ");
		sqlGetAmount
				.append("                                     AND dpl.display_program_level_id = ? ");
		sqlGetAmount
				.append("                                     AND dp.display_program_id = ? ");
		sqlGetAmount
				.append("                                     AND dpd.display_program_id = ");
		sqlGetAmount
				.append("                                         dp.display_program_id ");
		sqlGetAmount.append("                             ) ");

		String[] params = new String[] { customerId, displayProgramLevelId,
				displayProgramId };
		Cursor c = null;
		try {
			c = rawQuery(sqlGetAmount.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					totalAmount = CursorUtil.getLong(c, "TOTALAMOUNT");
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return totalAmount;
	}

	/**
	 * 
	 * lay thong tin d/s chuong trinh trung bay va d/s 10 san pham cho chuong
	 * trinh trung bay dau tien
	 * 
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: VoteDisplayPresentProductViewDTO
	 * @throws:
	 */
	public VoteDisplayPresentProductViewDTO getVoteDisplayPresentProductView(
			Bundle data) {
		VoteDisplayPresentProductViewDTO result = new VoteDisplayPresentProductViewDTO();
		result.listDisplayProgrameInfo = this
				.getListDisplayProgramProduct(data);
		if (result.listDisplayProgrameInfo != null
				&& result.listDisplayProgrameInfo.size() > 0) {
			data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID,
					result.listDisplayProgrameInfo.get(0).displayProgrameID);
		}
		VoteDisplayPresentProductViewDTO resultTMP = new VoteDisplayPresentProductViewDTO();
		resultTMP = this.getListVoteDisplayProduct(data);
		result.listProductDisplay = resultTMP.listProductDisplay;
		result.totalProductDisplay = resultTMP.totalProductDisplay;
		result.numDisplayVoted = this.getNumDisplayProgrameUseVoted(data);
		return result;
	}

	/**
	 * get list display program product with customerId
	 * 
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: List<DisplayPresentProductInfo>
	 * @throws:
	 */
	public ArrayList<DisplayPresentProductInfo> getListDisplayProgramProduct(
			Bundle data) {
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		String dateNow = DateUtils.now();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DP.DISPLAY_PROGRAM_CODE AS DISPLAY_PROGRAM_CODE, ");
		sql.append("       DP.DISPLAY_PROGRAM_NAME AS DISPLAY_PROGRAM_NAME, ");
		sql.append("       DPL.LEVEL_CODE          AS LEVEL_CODE, ");
		sql.append("       DP.DISPLAY_PROGRAM_ID   AS DISPLAY_PROGRAM_ID ");
		sql.append("FROM   DISPLAY_CUSTOMER_MAP DCM, ");
		sql.append("       DISPLAY_PROGRAM_LEVEL AS DPL, ");
		sql.append("       DISPLAY_STAFF_MAP DSM, ");
		sql.append("       DISPLAY_PROGRAM AS DP ");
		sql.append("WHERE  DCM.DISPLAY_PROGRAM_LEVEL_ID = DPL.DISPLAY_PROGRAM_LEVEL_ID ");
		sql.append("       AND DP.DISPLAY_PROGRAM_ID = DPL.DISPLAY_PROGRAM_ID ");
		sql.append("       AND DCM.CUSTOMER_ID = ? ");
		sql.append("       AND substr(DCM.FROM_DATE, 1, 10) <= substr(?, 1, 10) ");
		sql.append("       AND Ifnull(Date(DCM.TO_DATE) >= Date(?), 1) ");
		sql.append("       AND DCM.STATUS = 1 ");
		sql.append("       AND DCM.DISPLAY_STAFF_MAP_ID = DSM.DISPLAY_STAFF_MAP_ID ");
		sql.append("       AND DSM.STATUS = 1 ");
		sql.append("       AND DSM.DISPLAY_PROGRAM_ID = DP.DISPLAY_PROGRAM_ID ");
		sql.append("       AND DSM.STAFF_ID = ? ");
		sql.append("       AND DCM.SHOP_ID = ? ");
		sql.append("       AND DSM.SHOP_ID = ? ");
		sql.append("       AND DP.STATUS = 1 ");
		sql.append("       AND substr(DP.FROM_DATE, 1, 10) <= substr(?, 1, 10) ");
		sql.append("       AND Ifnull(( Date(DP.TO_DATE) ) >= Date(?), 1) ");
		sql.append("       AND DPL.STATUS = 1 ");
		sql.append("       AND DP.DISPLAY_PROGRAM_ID NOT IN (SELECT OBJECT_ID AS DISPLAY_PROGRAM_ID ");
		sql.append("                                         FROM   ACTION_LOG ");
		sql.append("                                         WHERE  OBJECT_TYPE = 2 ");
		sql.append("                                                AND substr(START_TIME, 1, 10) <= ");
		sql.append("                                                    substr(?, 1, 10) ");
		sql.append("                                                AND Ifnull(Date(END_TIME) >= ");
		sql.append("                                                           Date(?), 1)  ");
		sql.append("                                                AND SHOP_ID = ? ");
		sql.append("                                                AND CUSTOMER_ID = ? ");
		sql.append("                                                AND STAFF_ID = ?) ");
		sql.append("ORDER  BY DISPLAY_PROGRAM_CODE ASC ");

		String[] params = new String[] { customerId, dateNow, dateNow, staffId, shopId, shopId, dateNow, dateNow, dateNow, dateNow, shopId, customerId,
				staffId };

		ArrayList<DisplayPresentProductInfo> listResult = new ArrayList<DisplayPresentProductInfo>();
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						DisplayPresentProductInfo orderJoinTableDTO = new DisplayPresentProductInfo();
						orderJoinTableDTO.initDisplayPresentProductInfo(c);
						listResult.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return listResult;
	}

	/**
	 * 
	 * get number display programe use voted
	 * 
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getNumDisplayProgrameUseVoted(Bundle data) {
		int kq = 0;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		String dateNow = DateUtils.now();
		
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT Count(*) AS TOTAL_ROW ");
		sql.append("FROM   (SELECT object_id AS DISPLAY_PROGRAME_ID ");
		sql.append("        FROM   action_log ");
		sql.append("        WHERE  object_type = 2 ");
		sql.append("               AND shop_id = ? ");
		sql.append("               AND staff_id = ? ");
		sql.append("               AND substr(start_time, 1, 10) <= substr(?, 1, 10) ");
		sql.append("               AND ( Date(end_time) >= Date(?) ");
		sql.append("                      OR end_time IS NULL ) ");
		sql.append("               AND customer_id = ?) ");

		String[] params = new String[] { shopId, staffId, dateNow, dateNow, customerId };

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);
			if (c != null) {
				c.moveToFirst();
				kq = c.getInt(0);
			}
		} catch (Exception e) {
			kq = -1;
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return kq;
	}

	/**
	 * get list vote display product
	 * 
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: VoteDisplayPresentProductViewDTO
	 * @throws:
	 */
	public VoteDisplayPresentProductViewDTO getListVoteDisplayProduct(
			Bundle data) {
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String displayProgramId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		boolean isGetCount = data
				.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);
		String levelCode = data.getString(IntentConstants.INTENT_LEVEL_CODE);

		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT ST_D_PDG.DISPLAY_ProDuct_GROUP_ID, ST_D_PDG.DISPLAY_DP_GROUP_CODE,  ST_D_PDG.DISPLAY_DP_GROUP_NAME, ST_DPGD.PRODUCT_ID, ");
		sqlQuery.append("	ST_DPDD.QUANTITY, P.PRODUCT_CODE, P.PRODUCT_NAME ");
		sqlQuery.append("	FROM DISPLAY_ProDuct_GROUP ST_D_PDG, DISPLAY_ProDuct_GROUP_DTL ST_DPGD, ");
		sqlQuery.append("	 DISPLAY_PROGram_LEVEL ST_DPL, PRODUCT P ");
		sqlQuery.append("	LEFT JOIN DISPLAY_PL_DP_DTL ST_DPDD ON ST_DPDD.DISPLAY_ProDuct_GROUP_DTL_ID = ST_DPGD.DISPLAY_ProDuct_GROUP_DTL_ID ");
		sqlQuery.append("	AND ST_DPDD.DISPLAY_PROGram_LEVEL_ID = ST_DPL.DISPLAY_PROGram_LEVEL_ID AND ST_DPDD.STATUS = 1 ");
		sqlQuery.append("	WHERE 1 = 1 ");
		sqlQuery.append("	AND ST_D_PDG.STATUS = 1 ");
		sqlQuery.append("	AND ST_D_PDG.TYPE = 4 ");
		sqlQuery.append("	 AND ST_D_PDG.DISPLAY_PROGRAM_ID = ? ");
		sqlQuery.append("	AND ST_DPL.DISPLAY_PROGRAM_ID = ST_D_PDG.DISPLAY_PROGRAM_ID ");
		if (!StringUtil.isNullOrEmpty(levelCode)) {
			sqlQuery.append("	AND ST_DPL.LEVEL_CODE = ? ");
		}
		sqlQuery.append("	AND ST_D_PDG.DISPLAY_ProDuct_GROUP_ID = ST_DPGD.DISPLAY_Product_GROUP_ID ");
		sqlQuery.append("	AND ST_DPGD.PRODUCT_ID = P.PRODUCT_ID ");
		sqlQuery.append("	AND ST_DPGD.STATUS = 1 AND ST_DPL.STATUS = 1 ");
		sqlQuery.append("	AND P.STATUS = 1 ");
		sqlQuery.append("	ORDER BY DISPLAY_DP_GROUP_CODE, PRODUCT_CODE ");
		sqlQuery.append(ext);


		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ sqlQuery.toString() + ") ";
		String[] params;
		if (!StringUtil.isNullOrEmpty(levelCode)) {
			params = new String[] { displayProgramId, levelCode };
		} else {
			params = new String[] { displayProgramId };
		}
		VoteDisplayPresentProductViewDTO result = new VoteDisplayPresentProductViewDTO();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (isGetCount) {
				cTmp = rawQuery(getCountProductList, params);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					result.totalProductDisplay = total;
				}
			}
			// end
			c = rawQuery(sqlQuery.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						VoteDisplayProductDTO displayProduct = new VoteDisplayProductDTO();
						displayProduct.initVoteDisplayProduct(c);
						result.listProductDisplay.add(displayProduct);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (isGetCount && cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

}
