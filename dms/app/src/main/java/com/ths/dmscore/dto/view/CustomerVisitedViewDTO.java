package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

public class CustomerVisitedViewDTO {
	public ArrayList<CustomerVisitedDTO> arrWrong = new ArrayList<CustomerVisitedViewDTO.CustomerVisitedDTO>();
	public int totalCustomer;
	public static final int TYPE_STAFF = 1;
	public static final int TYPE_ROUTING = 2;
	public static final int TYPE_SHOP = 3;


	public CustomerVisitedDTO newWrongPlanCustomerItem() {
		return new CustomerVisitedDTO();
	}

	public class CustomerVisitedDTO {
		public CustomerDTO aCustomer = new CustomerDTO();
		//tuyen
		public String plan;
		//thoi gian bat dau ghe tham theo gio va full
		public String startTimeShort;
		public String startTimeFull;
		//thoi gian ket thuc  theo gio va full
		public String endTimeShort;
		public String endTimeFull;
		//tong thoi gian ghe tham
		public String visitedTime;
		//doanh so dat duoc
		public double amount;
		public double amountApproved;
		public double amountPending;
		// san luong
		public long quantity;
		public long quantityApproved;
		public long quantityPending;

		//ten khach hang
		public String cusCodeName;
		//ngoai tuyen ?
		public int isOr = 0;

		public CustomerVisitedDTO() {
		}

		public void initCusItemVisited(Cursor c) {
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
			} catch (Exception ex) {
			}
			this.aCustomer.setCustomerCode(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE));
			this.aCustomer.setCustomerId(CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID));
			this.aCustomer.setCustomerName(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME));
			this.aCustomer.setStreet(CursorUtil.getString(c, CUSTOMER_TABLE.STREET));
			cusCodeName = CursorUtil.getString(c, "CUS_CODE_NAME");
			plan = CursorUtil.getString(c, "TUYEN");
			plan = CursorUtil.getString(c, "TUYEN");
			isOr = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "is_or")) ? 0: CursorUtil.getInt(c, "is_or");
			if (!StringUtil.isNullOrEmpty(plan) && plan.substring(0, 1).equals(",")) {
				plan = plan.substring(1, plan.length());
			}
			this.amount = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT");
			this.amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_APPROVED");
			this.amountPending = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PENDING");
			this.quantity = CursorUtil.getLong(c, "QUANTITY");
			this.quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
			this.quantityPending = CursorUtil.getLong(c, "QUANTITY_PENDING");
			startTimeFull = CursorUtil.getString(c, "START_TIME_FULL");
			endTimeFull = CursorUtil.getString(c, "END_TIME_FULL");
			startTimeShort = CursorUtil.getString(c, "START_TIME_SHORT");
			endTimeShort = CursorUtil.getString(c, "END_TIME_SHORT");
			visitedTime = DateUtils.getVisitTime(startTimeFull, endTimeFull);

		}
	}
}
