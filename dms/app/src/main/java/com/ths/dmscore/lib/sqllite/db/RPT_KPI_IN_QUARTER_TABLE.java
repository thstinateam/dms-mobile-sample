package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.DimDateDTO;
import com.ths.dmscore.dto.db.ReportTemplateCriterionDTO;
import com.ths.dmscore.dto.view.DynamicReportMonthRowDTO;
import com.ths.dmscore.dto.view.DynamicReportMonthViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.RptKPIInMonthDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * Mo ta muc dich cua class
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_KPI_IN_QUARTER_TABLE extends ABSTRACT_TABLE {

	public static final String RPT_KPI_IN_QUARTER_ID = "RPT_KPI_IN_QUARTER_ID";
	public static final String OBJECT_ID = "OBJECT_ID";
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String KPI_ID = "KPI_ID";
	public static final String QUARTER = "QUARTER";
	public static final String YEAR = "YEAR";
	public static final String COLUMN_CRI_TYPE = "COLUMN_CRI_TYPE";
	public static final String COLUMN_CRI_ID = "COLUMN_CRI_ID";
	public static final String VALUE_PLAN = "VALUE_PLAN";
	public static final String VALUE = "VALUE";
	public static final String PERCENT_COMPLETE = "PERCENT_COMPLETE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_NAME = "RPT_KPI_IN_QUARTER";

	public RPT_KPI_IN_QUARTER_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { RPT_KPI_IN_QUARTER_ID, OBJECT_ID,
				OBJECT_TYPE, SHOP_ID, KPI_ID, QUARTER, YEAR, COLUMN_CRI_TYPE,
				COLUMN_CRI_ID, VALUE_PLAN, VALUE, PERCENT_COMPLETE,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Bao cao theo thang
	 * @author: hoanpd1
	 * @since: 13:28:32 17-01-2015
	 * @return: DynamicReportMonthViewDTO
	 * @throws:
	 * @param objectTypeRowID
	 * @param b
	 * @return
	 */
	public DynamicReportMonthViewDTO getDynamicReportByYear(int objectTypeRowID, Bundle b) {
		int totalRow = getObjectDynamicReportByYear(objectTypeRowID, b);
		int typeRow = b.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE,0);
		String strRow = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING,"");
		String strColumn = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_CLOUMN_STRING,"");
		int typeColumn = b.getInt(IntentConstants.INTENT_TYPE_REPORT_COLUMN_CHOOSE);
		int getAllPage = b.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		boolean isQuantityOrder = b.getBoolean(IntentConstants.INTENT_QUANTITY_ORDER,true);
		int KpiID = b.getInt(IntentConstants.INTENT_KPI_ID);
		int typeKpiID = b.getInt(IntentConstants.INTENT_TYPE_KPI_ID);
		String orderName = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		DynamicReportMonthViewDTO dtoMonth = new DynamicReportMonthViewDTO();
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *,	");
		sqlObject.append("	    Sum(value) AS VALUE_SUM, ");
		sqlObject.append("	    Sum(value_plan) AS VALUE_PLAN_SUM ");
		if (typeColumn == ReportTemplateCriterionDTO.PRODUCT) {
			sqlObject.append("		, (SELECT product_code ");
			sqlObject.append("					FROM   product ");
			sqlObject
					.append("					WHERE  product_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.CAT) {
			sqlObject.append("		, (SELECT product_info_name ");
			sqlObject.append("					FROM   product_info ");
			sqlObject
					.append("					WHERE  product_info_id = column_cri_id) as COLUMN_NAME ");
		}  else if (typeColumn == ReportTemplateCriterionDTO.YEAR) {
			sqlObject.append("		, DD.YEAR_NUM as COLUMN_NAME ");
		}
		if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			sqlObject.append("		, (SELECT staff_code ");
			sqlObject.append("					FROM   staff ");
			sqlObject
					.append("					WHERE  staff_id = rkiq.object_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.UNIT){
			sqlObject.append("		, (SELECT shop_code ");
			sqlObject.append("					FROM   shop ");
			sqlObject
					.append("					WHERE  shop_id = rkiq.shop_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("		, (SELECT   short_code  ");
			sqlObject.append("					FROM   customer ");
			sqlObject.append("					WHERE  customer_id = rkiq.object_id) as ROW_NAME ");
		}else{
			// tam thoi ko co 2 loai staff,unit thi cho row_name = 1

			sqlObject.append("		,1	as ROW_NAME ");
		}

		sqlObject.append("	FROM rpt_kpi_in_quarter rkiq	");
		sqlObject.append("	    JOIN DIM_DATE DD	");
		sqlObject.append("	    	 ON DD.date_key = rkiq.date_key AND dd.date_type = ?	");
		paramsObject.add(""+ DimDateDTO.DATE_TYPE_QUARTER);
		sqlObject.append("	WHERE KPI_ID       = ?	");
		paramsObject.add("" + KpiID);
		sqlObject.append("	      AND DD.YEAR_NUM	in (	");
		sqlObject.append(strColumn);
		sqlObject.append("	      )	");
		sqlObject.append("	      AND object_type_id = ?	");
		paramsObject.add("" + objectTypeRowID);
		sqlObject.append("	      AND column_cri_type_id = ?	");
		// tong hop nam theo quy
		paramsObject.add("" + ReportTemplateCriterionDTO.QUARTER);
		sqlObject.append("	      AND column_cri_id = DD.date_key	");

		if (typeRow == ReportTemplateCriterionDTO.UNIT) {
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");

		}  else if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			// tong hop theo nhan vien va gs
			sqlObject.append("	AND EXISTS (SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    STAFF ST,	");
			sqlObject.append("	    CHANNEL_TYPE CT	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	    ST.STAFF_ID = OBJECT_ID	");
			sqlObject.append("	    AND ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID	");
			sqlObject.append("	    AND CT.TYPE = 2	");
			sqlObject.append("	    AND CT.OBJECT_TYPE IN (	");
			if (typeRow == ReportTemplateCriterionDTO.STAFF)
				sqlObject.append("" + UserDTO.TYPE_STAFF);
			else{
				sqlObject.append("" + UserDTO.TYPE_SUPERVISOR );
			}
			sqlObject.append("	    ))	");
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id = ?	");
			paramsObject.add("" + GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}else{
		}
		sqlObject.append(" group by object_id,DD.year_num" );
		sqlObject.append(StringUtil.getStringOrder(orderName, isQuantityOrder));
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DynamicReportMonthRowDTO dto = new DynamicReportMonthRowDTO();
						int objectId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.OBJECT_ID);
						int staffId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.STAFF_ID);
						int shopId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.SHOP_ID);
						RptKPIInMonthDTO item = new RptKPIInMonthDTO();
						item.initDataFromCursor(c,typeKpiID);
						// bien kiem tra dto da ton tai chua, neu roi thi ko add vao mang nua ma chi them column
						boolean isExisted = false;
						for (int i = 0, size = dtoMonth.lstReportMonth.size(); i < size; i++) {
							DynamicReportMonthRowDTO temp = dtoMonth.lstReportMonth
									.get(i);
							if (typeRow == ReportTemplateCriterionDTO.UNIT
									&& temp.objectId == objectId && temp.shopId == shopId) {
								dto = temp;
								isExisted = true;
								break;
							} else if ((typeRow == ReportTemplateCriterionDTO.STAFF
									|| typeRow == ReportTemplateCriterionDTO.SUPERVISOR || typeRow == ReportTemplateCriterionDTO.CUSTOMER)
									&& temp.objectId == objectId) {
								dto = temp;
								isExisted = true;
								break;
							}
						}
						dto.shopId = shopId;
						dto.objectId = objectId;
						dto.staffId = staffId;
						dto.lstColumn.add(item);
						if (!isExisted) {
							dtoMonth.lstReportMonth.add(dto);
						}
					} while (c.moveToNext());
				}
			}
			if (getAllPage == 1) {
				dtoMonth.totalReportRow = totalRow;
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dtoMonth;
	}



	 /**
	 * Tong hop theo quy
	 * @author: Tuanlt11
	 * @param objectTypeRowID
	 * @param b
	 * @return
	 * @return: DynamicReportMonthViewDTO
	 * @throws:
	*/
	public DynamicReportMonthViewDTO getDynamicReportByQuarter(int objectTypeRowID, Bundle b) {
		// lay tong so luong cac phan tu va 10 phan tu de hien thi
		int totalRow = getObjectDynamicReportByQuarter(objectTypeRowID, b);
		int typeRow = b.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE,0);
		String strRow = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING,"");
		String strColumn = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_CLOUMN_STRING,"");
		int typeColumn = b.getInt(IntentConstants.INTENT_TYPE_REPORT_COLUMN_CHOOSE);
		int getAllPage = b.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		boolean isQuantityOrder = b.getBoolean(IntentConstants.INTENT_QUANTITY_ORDER,true);
		int KpiID = b.getInt(IntentConstants.INTENT_KPI_ID);
		int typeKpiID = b.getInt(IntentConstants.INTENT_TYPE_KPI_ID);
		String orderName = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		DynamicReportMonthViewDTO dtoMonth = new DynamicReportMonthViewDTO();
		Cursor c = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *,	");
		sqlObject.append("	    Sum(value) AS VALUE_SUM, ");
		sqlObject.append("	    Sum(value_plan) AS VALUE_PLAN_SUM ");
		if (typeColumn == ReportTemplateCriterionDTO.PRODUCT) {
			sqlObject.append("		, (SELECT product_code ");
			sqlObject.append("					FROM   product ");
			sqlObject
					.append("					WHERE  product_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.CAT) {
			sqlObject.append("		, (SELECT product_info_name ");
			sqlObject.append("					FROM   product_info ");
			sqlObject
					.append("					WHERE  product_info_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.YEAR) {
			sqlObject.append("		, DD.YEAR_NUM as COLUMN_NAME ");
		}
		if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			sqlObject.append("		, (SELECT staff_code ");
			sqlObject.append("					FROM   staff ");
			sqlObject
					.append("					WHERE  staff_id = rkiq.object_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.UNIT){
			sqlObject.append("		, (SELECT shop_code ");
			sqlObject.append("					FROM   shop ");
			sqlObject
					.append("					WHERE  shop_id = rkiq.shop_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("		, (SELECT   short_code  ");
			sqlObject.append("					FROM   customer ");
			sqlObject.append("					WHERE  customer_id = rkiq.object_id) as ROW_NAME ");
		}else{
			// tam thoi ko co 2 loai staff,unit thi cho row_name = 1

			sqlObject.append("		,1	as ROW_NAME ");
		}

		sqlObject.append("	FROM rpt_kpi_in_quarter rkiq	");
		sqlObject.append("	    JOIN DIM_DATE DD	");
		sqlObject.append("	    	 ON DD.date_key = rkiq.date_key AND dd.date_type = ?	");
		paramsObject.add(""+DimDateDTO.DATE_TYPE_QUARTER);
		sqlObject.append("	WHERE KPI_ID       = ?	");
		paramsObject.add("" + KpiID);
		sqlObject.append("	     AND (CASE	");
		sqlObject.append("	          WHEN DD.YEAR_NUM || '-' || DD.QUARTER_IN_YEAR_NUM IN ( ");
		sqlObject.append(strColumn);
		sqlObject.append("	   											 ) THEN 1	");
		sqlObject.append("	      ELSE 0   END )	");
		sqlObject.append("	      AND object_type_id = ?	");
		paramsObject.add("" + objectTypeRowID);
		sqlObject.append("	    AND column_cri_type_id = ?	");
		paramsObject.add("" + typeColumn);
		sqlObject.append("	    AND column_cri_id = DD.date_key	");

		if (typeRow == ReportTemplateCriterionDTO.UNIT) {
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");

		}else if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			// tong hop theo nhan vien va gs
			sqlObject.append("	AND EXISTS (SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    STAFF ST,	");
			sqlObject.append("	    CHANNEL_TYPE CT	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	    ST.STAFF_ID = OBJECT_ID	");
			sqlObject.append("	    AND ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID	");
			sqlObject.append("	    AND CT.TYPE = 2	");
			sqlObject.append("	    AND CT.OBJECT_TYPE IN (	");
			if (typeRow == ReportTemplateCriterionDTO.STAFF)
				sqlObject.append("" + UserDTO.TYPE_STAFF);
			else{
				sqlObject.append("" + UserDTO.TYPE_SUPERVISOR );
			}
			sqlObject.append("	    ))	");
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id = ?	");
			paramsObject.add("" + GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}else{
		}

		sqlObject.append(" group by object_id,DD.quarter_in_year_num,DD.year_num" );

		sqlObject.append(StringUtil.getStringOrder(orderName, isQuantityOrder));
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DynamicReportMonthRowDTO dto = new DynamicReportMonthRowDTO();
						int objectId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.OBJECT_ID);
						int staffId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.STAFF_ID);
						int shopId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.SHOP_ID);
						RptKPIInMonthDTO item = new RptKPIInMonthDTO();
						item.initDataFromCursor(c,typeKpiID);
						// bien kiem tra dto da ton tai chua, neu roi thi ko add vao mang nua ma chi them column
						boolean isExisted = false;
						for (int i = 0, size = dtoMonth.lstReportMonth.size(); i < size; i++) {
							DynamicReportMonthRowDTO temp = dtoMonth.lstReportMonth
									.get(i);
							if (typeRow == ReportTemplateCriterionDTO.UNIT
									&& temp.shopId == shopId) {
								// bo temp.objectId
								dto = temp;
								isExisted = true;
								break;
							} else if ((typeRow == ReportTemplateCriterionDTO.STAFF
									|| typeRow == ReportTemplateCriterionDTO.SUPERVISOR || typeRow == ReportTemplateCriterionDTO.CUSTOMER)
									&& temp.objectId == objectId) {
								dto = temp;
								isExisted = true;
								break;
							}
						}
						dto.shopId = shopId;
						dto.objectId = objectId;
						dto.staffId = staffId;
						dto.lstColumn.add(item);
						if (!isExisted) {
							dtoMonth.lstReportMonth.add(dto);
						}
					} while (c.moveToNext());
				}
			}
			if (getAllPage == 1) {
				dtoMonth.totalReportRow = totalRow;
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dtoMonth;
	}

	 /**
	 * Lay 10 phan tu bao cao theo nam
	 * @author: Tuanlt11
	 * @param objectTypeRowID
	 * @param b
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int getObjectDynamicReportByYear(int objectTypeRowID, Bundle b) {
		int typeRow = b.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE,0);
		String strRow = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING,"");
		String strColumn = b.getString(IntentConstants.INTENT_DYNAMIC_OBJECT_CLOUMN_STRING,"");
		int typeColumn = b.getInt(IntentConstants.INTENT_TYPE_REPORT_COLUMN_CHOOSE);
		int getAllPage = b.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		boolean isQuantityOrder = b.getBoolean(IntentConstants.INTENT_QUANTITY_ORDER,true);
		int KpiID = b.getInt(IntentConstants.INTENT_KPI_ID);
		int typeKpiID = b.getInt(IntentConstants.INTENT_TYPE_KPI_ID);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		String orderName = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		DynamicReportMonthViewDTO dtoMonth = new DynamicReportMonthViewDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		StringBuffer sqlObject = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *,	");
		sqlObject.append("	    Sum(value) AS VALUE_SUM, ");
		sqlObject.append("	    Sum(value_plan) AS VALUE_PLAN_SUM ");
		if (typeColumn == ReportTemplateCriterionDTO.PRODUCT) {
			sqlObject.append("		, (SELECT product_code ");
			sqlObject.append("					FROM   product ");
			sqlObject
					.append("					WHERE  product_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.CAT) {
			sqlObject.append("		, (SELECT product_info_name ");
			sqlObject.append("					FROM   product_info ");
			sqlObject
					.append("					WHERE  product_info_id = column_cri_id) as COLUMN_NAME ");
		}  else if (typeColumn == ReportTemplateCriterionDTO.YEAR) {
			sqlObject.append("		, DD.YEAR_NUM as COLUMN_NAME ");
		}

		if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			sqlObject.append("		, (SELECT staff_code ");
			sqlObject.append("					FROM   staff ");
			sqlObject
					.append("					WHERE  staff_id = rkiq.object_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.UNIT){
			sqlObject.append("		, (SELECT shop_code ");
			sqlObject.append("					FROM   shop ");
			sqlObject
					.append("					WHERE  shop_id = rkiq.shop_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("		, (SELECT   short_code  ");
			sqlObject.append("					FROM   customer ");
			sqlObject.append("					WHERE  customer_id = rkiq.object_id) as ROW_NAME ");
		}else{
			// tam thoi ko co 2 loai staff,unit thi cho row_name = 1

			sqlObject.append("		,1	as ROW_NAME ");
		}

		sqlObject.append("	FROM rpt_kpi_in_quarter rkiq	");
		sqlObject.append("	    JOIN DIM_DATE DD	");
		sqlObject.append("	    	 ON DD.date_key = rkiq.date_key AND dd.date_type = ?	");
		paramsObject.add(""+DimDateDTO.DATE_TYPE_QUARTER);
		sqlObject.append("	WHERE KPI_ID       = ?	");
		paramsObject.add("" + KpiID);
		sqlObject.append("	      AND DD.YEAR_NUM	in (	");
		sqlObject.append(strColumn);
		sqlObject.append("	      )	");
		sqlObject.append("	      AND object_type_id = ?	");
		paramsObject.add("" + objectTypeRowID);
		sqlObject.append("	      AND column_cri_type_id = ?	");
		// tong hop nam theo quy
		paramsObject.add("" + ReportTemplateCriterionDTO.QUARTER);
		sqlObject.append("	      AND column_cri_id = DD.date_key	");

		if (typeRow == ReportTemplateCriterionDTO.UNIT) {
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");

		}  else if (typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			// tong hop theo nhan vien va gs
			sqlObject.append("	AND EXISTS (SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    STAFF ST,	");
			sqlObject.append("	    CHANNEL_TYPE CT	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	    ST.STAFF_ID = OBJECT_ID	");
			sqlObject.append("	    AND ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID	");
			sqlObject.append("	    AND CT.TYPE = 2	");
			sqlObject.append("	    AND CT.OBJECT_TYPE IN (	");
			if (typeRow == ReportTemplateCriterionDTO.STAFF)
				sqlObject.append("" + UserDTO.TYPE_STAFF);
			else{
				sqlObject.append("" + UserDTO.TYPE_SUPERVISOR );
			}
			sqlObject.append("	    ))	");
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id = ?	");
			paramsObject.add("" + GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}else{
		}
		sqlObject.append(" group by object_id" );
		sqlObject.append(StringUtil.getStringOrder(orderName, isQuantityOrder));
		// get count
		if (getAllPage == 1) {
			totalPageSql.append("select count(*) as TOTAL_ROW from ("
					+ sqlObject + ")");
		}
		sqlObject.append(" limit "
				+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
		sqlObject.append(" offset "
				+ Integer
				.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DynamicReportMonthRowDTO dto = new DynamicReportMonthRowDTO();
						int objectId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.OBJECT_ID);
						int staffId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.STAFF_ID);
						int shopId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.SHOP_ID);
						RptKPIInMonthDTO item = new RptKPIInMonthDTO();
						item.initDataFromCursor(c,typeKpiID);
						// bien kiem tra dto da ton tai chua, neu roi thi ko add vao mang nua ma chi them column
						boolean isExisted = false;
						for (int i = 0, size = dtoMonth.lstReportMonth.size(); i < size; i++) {
							DynamicReportMonthRowDTO temp = dtoMonth.lstReportMonth
									.get(i);
							if (typeRow == ReportTemplateCriterionDTO.UNIT
									&& temp.objectId == objectId && temp.shopId == shopId) {
								dto = temp;
								isExisted = true;
								break;
							} else if ((typeRow == ReportTemplateCriterionDTO.CUSTOMER
									|| typeRow == ReportTemplateCriterionDTO.STAFF || typeRow == ReportTemplateCriterionDTO.SUPERVISOR)
									&& temp.objectId == objectId) {
								dto = temp;
								isExisted = true;
								break;
							}
						}
						dto.shopId = shopId;
						dto.objectId = objectId;
						dto.staffId = staffId;
						dto.lstColumn.add(item);
						if (!isExisted) {
							dtoMonth.lstReportMonth.add(dto);
						}
					} while (c.moveToNext());
				}
			}
			if (getAllPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), paramsObject);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dtoMonth.totalReportRow = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}

		// lay ra 10 object id dc chon
		StringBuilder sb = new StringBuilder();
		for (DynamicReportMonthRowDTO row : dtoMonth.lstReportMonth) {
			// neu la don vi thi add shop vao lai
			if (typeRow == ReportTemplateCriterionDTO.UNIT) {
				sb.append("'" + row.shopId + "'");
			} else
				sb.append("'" + row.objectId + "'");
			sb.append(",");
			sb.append(Constants.STR_SPACE);
		}
		// remove space and dau ,
		if (sb.length() > 1) {
			sb.setLength(sb.length() - 2);
		}
		b.putString(IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING,
				sb.toString());

		return dtoMonth.totalReportRow;
	}

	/**
	 * Lay 10 phan tu theo bao cao quy
	 *
	 * @author: Tuanlt11
	 * @param objectTypeRowID
	 * @param b
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getObjectDynamicReportByQuarter(int objectTypeRowID, Bundle b) {
		int typeRow = b
				.getInt(IntentConstants.INTENT_TYPE_REPORT_ROW_CHOOSE, 0);
		String strRow = b.getString(
				IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING, "");
		String strColumn = b.getString(
				IntentConstants.INTENT_DYNAMIC_OBJECT_CLOUMN_STRING, "");
		int typeColumn = b
				.getInt(IntentConstants.INTENT_TYPE_REPORT_COLUMN_CHOOSE);
		int getAllPage = b.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		boolean isQuantityOrder = b.getBoolean(
				IntentConstants.INTENT_QUANTITY_ORDER, true);
		int KpiID = b.getInt(IntentConstants.INTENT_KPI_ID);
		int typeKpiID = b.getInt(IntentConstants.INTENT_TYPE_KPI_ID);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		String orderName = b.getString(IntentConstants.INTENT_ORDER, Constants.STR_BLANK);
		DynamicReportMonthViewDTO dtoMonth = new DynamicReportMonthViewDTO();
		Cursor c = null;
		Cursor cTotalRow = null;
		StringBuffer sqlObject = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *,	");
		sqlObject.append("	    Sum(value) AS VALUE_SUM, ");
		sqlObject.append("	    Sum(value_plan) AS VALUE_PLAN_SUM ");
		if (typeColumn == ReportTemplateCriterionDTO.PRODUCT) {
			sqlObject.append("		, (SELECT product_code ");
			sqlObject.append("					FROM   product ");
			sqlObject
					.append("					WHERE  product_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.CAT) {
			sqlObject.append("		, (SELECT product_info_name ");
			sqlObject.append("					FROM   product_info ");
			sqlObject
					.append("					WHERE  product_info_id = column_cri_id) as COLUMN_NAME ");
		} else if (typeColumn == ReportTemplateCriterionDTO.YEAR) {
			sqlObject.append("		, DD.YEAR_NUM as COLUMN_NAME ");
		}
		if (typeRow == ReportTemplateCriterionDTO.STAFF
				|| typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			sqlObject.append("		, (SELECT staff_code ");
			sqlObject.append("					FROM   staff ");
			sqlObject
					.append("					WHERE  staff_id = rkiq.object_id) as ROW_NAME ");
		} else if (typeRow == ReportTemplateCriterionDTO.UNIT) {
			sqlObject.append("		, (SELECT shop_code ");
			sqlObject.append("					FROM   shop ");
			sqlObject
					.append("					WHERE  shop_id = rkiq.shop_id) as ROW_NAME ");
		}else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("		, (SELECT   short_code  ");
			sqlObject.append("					FROM   customer ");
			sqlObject.append("					WHERE  customer_id = rkiq.object_id) as ROW_NAME ");
		}else {
			// tam thoi ko co 2 loai staff,unit thi cho row_name = 1
			sqlObject.append("		, 1	as ROW_NAME ");
		}

		sqlObject.append("	FROM rpt_kpi_in_quarter rkiq	");
		sqlObject.append("	    JOIN DIM_DATE DD	");
		sqlObject
				.append("	    	 ON DD.date_key = rkiq.date_key AND dd.date_type = ?	");
		paramsObject.add("" + DimDateDTO.DATE_TYPE_QUARTER);
		sqlObject.append("	WHERE KPI_ID       = ?	");
		paramsObject.add("" + KpiID);
		sqlObject.append("	     AND (CASE	");
		sqlObject
				.append("	          WHEN DD.YEAR_NUM || '-' || DD.QUARTER_IN_YEAR_NUM IN ( ");
		sqlObject.append(strColumn);
		sqlObject.append("	   											 ) THEN 1	");
		sqlObject.append("	      ELSE 0   END )	");
		sqlObject.append("	      AND object_type_id = ?	");
		paramsObject.add("" + objectTypeRowID);
		sqlObject.append("	    AND column_cri_type_id = ?	");
		paramsObject.add("" + typeColumn);
		sqlObject.append("	    AND column_cri_id = DD.date_key	");

		if (typeRow == ReportTemplateCriterionDTO.UNIT) {
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");

		} else if (typeRow == ReportTemplateCriterionDTO.STAFF
				|| typeRow == ReportTemplateCriterionDTO.SUPERVISOR) {
			// tong hop theo nhan vien va gs
			sqlObject.append("	AND EXISTS (SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    STAFF ST,	");
			sqlObject.append("	    CHANNEL_TYPE CT	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	    ST.STAFF_ID = OBJECT_ID	");
			sqlObject.append("	    AND ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID	");
			sqlObject.append("	    AND CT.TYPE = 2	");
			sqlObject.append("	    AND CT.OBJECT_TYPE IN (	");
			if (typeRow == ReportTemplateCriterionDTO.STAFF)
				sqlObject.append("" + UserDTO.TYPE_STAFF);
			else {
				sqlObject.append("" + UserDTO.TYPE_SUPERVISOR);
			}
			sqlObject.append("	    ))	");
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
		} else if(typeRow == ReportTemplateCriterionDTO.CUSTOMER){
			sqlObject.append("	    AND object_id in (	");
			sqlObject.append(strRow);
			sqlObject.append("	    )	");
			sqlObject.append("	    AND shop_id = ?	");
			paramsObject.add("" + GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}else {
		}
		sqlObject
				.append(" group by object_id");

		sqlObject.append(StringUtil.getStringOrder(orderName, isQuantityOrder));

		// get count
		if (getAllPage == 1) {
			totalPageSql.append("select count(*) as TOTAL_ROW from ("
					+ sqlObject + ")");
		}
		sqlObject.append(" limit "
				+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
		sqlObject.append(" offset "
				+ Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DynamicReportMonthRowDTO dto = new DynamicReportMonthRowDTO();
						int objectId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.OBJECT_ID);
						int staffId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.STAFF_ID);
						int shopId = CursorUtil.getInt(c,
								RPT_KPI_IN_MONTH_TABLE.SHOP_ID);
						RptKPIInMonthDTO item = new RptKPIInMonthDTO();
						item.initDataFromCursor(c, typeKpiID);
						// bien kiem tra dto da ton tai chua, neu roi thi ko add
						// vao mang nua ma chi them column
						boolean isExisted = false;
						for (int i = 0, size = dtoMonth.lstReportMonth.size(); i < size; i++) {
							DynamicReportMonthRowDTO temp = dtoMonth.lstReportMonth
									.get(i);
							if (typeRow == ReportTemplateCriterionDTO.UNIT
									&& temp.shopId == shopId) {
								// bo temp.objectId
								dto = temp;
								isExisted = true;
								break;
							} else if ((typeRow == ReportTemplateCriterionDTO.STAFF
									|| typeRow == ReportTemplateCriterionDTO.SUPERVISOR || typeRow == ReportTemplateCriterionDTO.CUSTOMER)
									&& temp.objectId == objectId) {
								dto = temp;
								isExisted = true;
								break;
							}
						}
						dto.shopId = shopId;
						dto.objectId = objectId;
						dto.staffId = staffId;
						dto.lstColumn.add(item);
						if (!isExisted) {
							dtoMonth.lstReportMonth.add(dto);
						}
					} while (c.moveToNext());
				}
			}
			if (getAllPage == 1) {
				cTotalRow = rawQueries(totalPageSql.toString(), paramsObject);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dtoMonth.totalReportRow = cTotalRow.getInt(0);
					}
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
			}
		}
		// lay ra 10 object id dc chon
		StringBuilder sb = new StringBuilder();
		for (DynamicReportMonthRowDTO row : dtoMonth.lstReportMonth) {
			// neu la don vi thi add shop vao lai
			if (typeRow == ReportTemplateCriterionDTO.UNIT) {
				sb.append("'" + row.shopId + "'");
			} else
				sb.append("'" + row.objectId + "'");
			sb.append(",");
			sb.append(Constants.STR_SPACE);
		}
		// remove space and dau ,
		if (sb.length() > 1) {
			sb.setLength(sb.length() - 2);
		}
		b.putString(IntentConstants.INTENT_DYNAMIC_OBJECT_ROW_STRING,
				sb.toString());

		return dtoMonth.totalReportRow;
	}

}
