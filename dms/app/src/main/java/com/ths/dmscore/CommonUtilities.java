/*
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore;

import android.content.Context;
import android.content.Intent;

/**
 * Chua cac thong tin can thiet de dang ky GCM
 * @author: DungDQ3
 * @version: 1.1
 * @since: 1.0
 */
public final class CommonUtilities {
    //thong tin broadcast
    public static final String DISPLAY_MESSAGE_ACTION =
            "com.viettel.vinamilk.DISPLAY_MESSAGE";
    public static final String EXTRA_MESSAGE = "message";

    /**
    * Hien thi tin nhan
    * @author dungdq
    * @param: context
    * @param: message
    */
    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
