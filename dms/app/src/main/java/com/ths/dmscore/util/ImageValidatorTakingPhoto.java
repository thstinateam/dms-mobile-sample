/**
 * Copyright 2011 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.commonsware.cwac.cache.SimpleWebImageCache;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 *  Mo ta muc dich cua lop (interface)
 *  @author: HaiTC
 *  @version: 1.0
 *  @since: 1.0
 */
public class ImageValidatorTakingPhoto extends ImageValidator {
	Intent data;
	/**
	 * @param context
	 * @param filePath
	 * @param maxDimensionAvatar
	 */
	public ImageValidatorTakingPhoto(GlobalBaseActivity context,
			String filePath, int maxDimensionAvatar) {
		super(context, filePath, maxDimensionAvatar);
		// TODO Auto-generated constructor stub
	}
	
	public void setDataIntent(Intent data) {
		this.data = data;
	}
	
	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		if (data != null) {
			Bitmap tempBitmap = null;
			tempBitmap = (Bitmap) data.getExtras().get("data");
			if (tempBitmap != null) {
				FileOutputStream out = null;
				try {
					int srcWidth = tempBitmap.getWidth();
					int srcHeight = tempBitmap.getHeight();
					MyLog.e("PhucNT4", " file truoc khi nen anh " + filePath.length());
					out = new FileOutputStream(	filePath);
					
					if(srcWidth > 0 && srcHeight > 0 && 
							(srcWidth > Constants.MAX_FULL_IMAGE_WIDTH || srcHeight > Constants.MAX_FULL_IMAGE_HEIGHT)){
						Bitmap scaledBitmap = ImageUtil.resizeImageWithOrignal(tempBitmap, Constants.MAX_FULL_IMAGE_HEIGHT);
						scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
						scaledBitmap.recycle();
						scaledBitmap = null;
					}else{
						tempBitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
					}
					MyLog.e("PhucNT4", " file sau khi nen anh " + filePath.length());
				} catch (FileNotFoundException e) {
				} finally {
					try {
						if (out != null) {
							out.close();
						}
						if (tempBitmap != null) {
							tempBitmap.recycle();
						}
						data = null;
					} catch (IOException e) {
					}
				}
			}
		}else {
			FileOutputStream out = null;
			Bitmap tempBitmap =null;
			try {
				File file = new File(filePath);
				MyLog.e("PhucNT4", " file truoc khi nen anh " + file.length());
				BitmapFactory.Options resample = new BitmapFactory.Options();
				resample.inJustDecodeBounds = false;
				resample.inSampleSize = SimpleWebImageCache.computeSampleSizeFromFile(file,
						Constants.MAX_FULL_IMAGE_WIDTH , Constants.MAX_FULL_IMAGE_HEIGHT);
				tempBitmap = BitmapFactory.decodeFile(filePath);
				int srcWidth = tempBitmap.getWidth();
				int srcHeight = tempBitmap.getHeight();
				
				
				out = new FileOutputStream(	filePath);
				
				
				if(srcWidth > 0 && srcHeight > 0 && 
						(srcWidth > Constants.MAX_FULL_IMAGE_WIDTH || srcHeight > Constants.MAX_FULL_IMAGE_HEIGHT)){
					Bitmap scaledBitmap = ImageUtil.resizeImageWithOrignal(tempBitmap, Constants.MAX_FULL_IMAGE_HEIGHT);
					scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
					scaledBitmap.recycle();
					scaledBitmap = null;
				}else{
					tempBitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
				}
				MyLog.e("PhucNT4", " file sau khi nen anh " + file.length());
			} catch (FileNotFoundException e) {
				MyLog.e("validate image", "FileNotFoundException", e);
			} catch (OutOfMemoryError em) {
				System.gc();
				if(bitmapData != null && bitmapData.isRecycled()){
					bitmapData.recycle();
					bitmapData = null;
				}
				//filePath = null;
				context.showDialog(StringUtil.getString(R.string.ERROR_OUT_MEMORY));
			} catch (Exception e){
				MyLog.e("validate image", "Exception", e);
			} finally {
				try {
					if (out != null) {
						out.close();
					}
					if (tempBitmap != null) {
						tempBitmap.recycle();
					}
				} catch (IOException e) {
				}
			}
		
		}
		return super.execute();
	}
}
