/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * hien thi 1 dong thong tin CTKM cho CTKM loai don hang
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class SelectPromotionForPromotionTypeOrder extends DMSTableRow {
	// STT
	TextView tvSTT;
	// code
	TextView tvPromotionCode;
	// description
	TextView tvPromotionDes;

	// promotion content
	TextView tvPromotionContent;

	// data to render layout for row
	OrderDetailViewDTO myData;
	private boolean isPopupDiscount = false; // la popup discount hay popup doi KM

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 */
	public SelectPromotionForPromotionTypeOrder(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_select_promotion_for_promotion_type_order_row);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvPromotionCode = (TextView) findViewById(R.id.tvPromotionCode);
		tvPromotionDes = (TextView) findViewById(R.id.tvPromotionDes);
		tvPromotionContent = (TextView) findViewById(R.id.tvPromotionContent);
		listener = listen;
		myData = new OrderDetailViewDTO();
		setOnClickListener(this);
	}


	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, OrderDetailViewDTO item) {
		isPopupDiscount = false;
		this.myData = item;
		tvSTT.setText(String.valueOf(position));
		tvPromotionCode.setText(item.orderDetailDTO.programeCode);
		tvPromotionDes.setText(item.orderDetailDTO.programeName);
//		if(item.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER){
//			if(item.orderDetailDTO.discountPercentage > 0) {
//				tvPromotionContent.setText(String.valueOf(item.orderDetailDTO.discountPercentage + "%"));
//			} else {
//				tvPromotionContent.setText(StringUtil.parseAmountMoney(String.valueOf(item.orderDetailDTO.maxAmountFree)));
//			}
//		}
//		else if(item.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21){
//			tvPromotionContent.setText(item.promotionDescription);
//
//		}
		tvPromotionContent.setText(item.promotionDescription);
	}

	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, SaleOrderPromoDetailDTO item) {
		isPopupDiscount = true;
//		this.myData = item;
		tvSTT.setText(String.valueOf(position));
		tvPromotionCode.setText(item.programCode);
		tvPromotionDes.setText(item.programName);
//		if(item.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER){
//			if(item.orderDetailDTO.discountPercentage > 0) {
//				tvPromotionContent.setText(String.valueOf(item.orderDetailDTO.discountPercentage + "%"));
//			} else {
//				tvPromotionContent.setText(StringUtil.parseAmountMoney(String.valueOf(item.orderDetailDTO.maxAmountFree)));
//			}
//		}
//		else if(item.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21){
//			tvPromotionContent.setText(item.promotionDescription);
//
//		}
//		tvPromotionContent.setText(StringUtil.parseAmountMoney(item.discountAmount));
		StringUtil.display(tvPromotionContent, item.discountAmount);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == this && listener != null && !isPopupDiscount) {
			listener.handleVinamilkTableRowEvent(
					ActionEventConstant.ACTION_SELECT_PROMOTION_TYPE_PROMOTION_ORDER,
					this, this.myData);
		}
	}

	/**
	 *
	*  cap nhat layout khi dong nay da duoc selected
	*  @author: HaiTC3
	*  @return: void
	*  @throws:
	 */
	public void updateLayoutSelected(){
		tvSTT.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvPromotionCode.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvPromotionDes.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvPromotionContent.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
	}
}