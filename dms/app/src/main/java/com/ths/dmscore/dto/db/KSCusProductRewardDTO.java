package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.KS_CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.KS_CUS_PRODUCT_REWARD_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * DTO cho table ks_customer
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KSCusProductRewardDTO extends AbstractTableDTO {

	// noi dung field
	private static final long serialVersionUID = 1L;
	public long ksCusProductRewardId;
	public long ksCustomerId;
	public int productId; // id sp
	public int productNum;// so luong sp phai tra
	public int productNumDone; // so luong sp da tra
	public int status;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public String productCode; // code sp
	public String productName; // ten sp
	public long ksId; // id keyshop

	 /**
	 * update so luong sp da tra cho kh
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateSqlUpdateKSCustomerProduct() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, KS_CUS_PRODUCT_REWARD_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			if (!StringUtil.isNullOrEmpty(updateDate)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						KS_CUSTOMER_TABLE.UPDATE_DATE, updateDate, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						KS_CUSTOMER_TABLE.UPDATE_USER, updateUser, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_NUM_DONE,
					productNumDone, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(KS_CUS_PRODUCT_REWARD_TABLE.KS_CUSTOMER_ID,
					ksCusProductRewardId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
		}

		return json;
	}

	/**
	 * Khoi tao du lieu
	 *
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		ksCusProductRewardId = CursorUtil.getLong(c, KS_CUS_PRODUCT_REWARD_TABLE.KS_CUS_PRODUCT_REWARD_ID);
		productNumDone = CursorUtil.getInt(c, KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_NUM_DONE);
		productNum = CursorUtil.getInt(c, KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_NUM);
		productId = CursorUtil.getInt(c, KS_CUS_PRODUCT_REWARD_TABLE.PRODUCT_ID);
		productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		productName = CursorUtil.getString(c, "PRODUCT_NAME");
		ksId = CursorUtil.getLong(c, KS_CUS_PRODUCT_REWARD_TABLE.KS_ID);
	}

}
