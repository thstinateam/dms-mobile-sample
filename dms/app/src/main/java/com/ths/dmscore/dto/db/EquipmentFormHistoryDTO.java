/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.EQUIP_FORM_HISTORY_TABLE;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * DTO luu lich su from bao mat 
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  19:14:45 26-12-2014
 */
public class EquipmentFormHistoryDTO  extends AbstractTableDTO{
	private static final long serialVersionUID = 1L;
	
	// Id lich su bien ban
	public long equipFormHistoryId;
	// id di bien ban
	public long recordId;
	// id loai bien ban
	public int recordType;
	// trang thai giao nhan
	public int deliveryStatus;
	//ngay luu lich su
	public String actDate;
	// nhan vien
	public long staffId;
	// trang bien ban
	public int recordStatus;
	public String createUser;
	public String createDate;
	public String updateUser;
	public String updateDate;

	/**
	 * insert lich su bao mat thiet bi
	 * @author: hoanpd1
	 * @since: 12:23:24 30-12-2014
	 * @return: JSONObject
	 * @throws:  
	 * @param item
	 * @return
	 */
	public JSONObject generateInsertEquipFromHistory(EquipmentFormHistoryDTO item) {
		JSONObject jsonInsert = new JSONObject();
		try {
			// Insert
			jsonInsert.put(IntentConstants.INTENT_TYPE, TableAction.INSERT); 
			jsonInsert.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_FORM_HISTORY_TABLE.TABLE_NAME);
			// ds params
			JSONArray jsonDetail = new JSONArray();
			// Khi insert thi insert cac truong sau
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.EQUIP_FORM_HISTORY_ID, item.equipFormHistoryId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.RECORD_ID, item.recordId, null));  
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.RECORD_TYPE, item.recordType, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.RECORD_STATUS, item.recordStatus, null));
			// DELIVERY_STATUS = null
//			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.DELIVERY_STATUS, item.deliveryStatus, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.ACT_DATE, item.actDate, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.STAFF_ID, item.staffId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.CREATE_USER, item.createUser, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_FORM_HISTORY_TABLE.CREATE_DATE, item.createDate, null));

			jsonInsert.put(IntentConstants.INTENT_LIST_PARAM, jsonDetail);

		} catch (JSONException e) {
			MyLog.e("UnexceptionLog",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return jsonInsert;
	}
}
