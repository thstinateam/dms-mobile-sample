package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.STAFF_POSITION_LOG_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Giam sat lo trinh DTO
 * 
 * @author : TamPQ
 * @version: 1.1
 * @since : 1.0
 */
public class TBHVRouteSupervisionDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int totalList;

	public enum STATUS {
		NONE_VISIT, VISITED, WRONG_PLAN, RIGHT_PLAN, NVBH_VISITED_BUT_GSNPP, GSNPP_VISITED_BUT_NVBH
	}

	public ArrayList<THBVRouteSupervisionItem> itemList;

	public TBHVRouteSupervisionDTO() {
		itemList = new ArrayList<THBVRouteSupervisionItem>();
	}

	public THBVRouteSupervisionItem newTHBVRouteSupervisionItem() {
		return new THBVRouteSupervisionItem();
	}

	public class THBVRouteSupervisionItem implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public String nameNpp; // name Npp
		public String codeNpp; // name Npp
		public String nameGsnpp; // name Gsnpp
		public String staffCodeGsnpp; // staffCode Gsnpp
		public String nameNVBH; // name NVBH
		public String nameVisitingKH;
		public String codeVisitingKH;
		public String timeVisitingKH;
		public String gsnppMobile;
		public String gsnppPhone;
		public int idVistingKH;
		public int staffIdNVBH; // staffId NVBH
		public String staffCodeNVBH;// staffCode NVBH
		public int numSaleStore;// num Sale Store
		public int numRightPlan;// num Right Plan
		public int numWrongPlan;// num Wrong Plan
		public int numGsnppVisitedStore;// num Visited Store
		public String addressVisitingKH;
		public double lat;// lat
		public double lng;// lng
		public String staffId;// staffId
		public int shopId;// shopId
		public String shopCodeNVBH;
		public ArrayList<Customer> customerSeqPlan;// customer Seq Plan
		public ArrayList<Customer> gsnppRealSeq;// gsnpp Real Seq
		public ArrayList<Customer> nvbhRealSeq;// nvbh Real Seq
		public String updateTime;
		public String shopCodeGsnpp;
		public int numNvbhVisitedStore;
		// num gsbh visited store
		public int numGsVisitedButNvbh;

		public THBVRouteSupervisionItem() {

		}

		/**
		 * Customer Item
		 * 
		 * @author : TamPQ
		 */
		@SuppressWarnings("serial")
		public class Customer implements Serializable {
			public String visit_start_time;
			public Date visit_start_time_2;
			public String visit_end_time;
			public Date visit_end_time_2;
			public int cusId;
			public STATUS status;
			public double lat;
			public double lng;
			public int seqInPlan = -1;
			public int isOr = -1;
			public String visit_time;
			public String codeName;
			public String address;
			public boolean isVisting;
			public int realSeq;
			public boolean realLast = false;

			public Customer(STATUS st) {
				status = st;
			}

		}

		/**
		 * initDataFromCursor
		 * 
		 * @author: TamPQ
		 * @return: void
		 * @throws:
		 */
		public void initDataFromCursor(Cursor c) {
			nameNpp = CursorUtil.getString(c, "NPP_SHOP_NAME");
			codeNpp = CursorUtil.getString(c, "NPP_SHOP_CODE");
			nameGsnpp = CursorUtil.getString(c, "NPP_STAFF_NAME");
			staffId = CursorUtil.getString(c, "GS_STAFF_ID");
			shopId = CursorUtil.getInt(c, "NPP_SHOP_ID");
			staffCodeGsnpp = CursorUtil.getString(c, "NPP_STAFF_CODE");
			nameNVBH = CursorUtil.getString(c, "NVBH_STAFF_NAME");
			staffCodeNVBH = CursorUtil.getString(c, "NVBH_STAFF_CODE");
			staffIdNVBH = CursorUtil.getInt(c, "NVBH_STAFF_ID");
			shopCodeNVBH = CursorUtil.getString(c, "NVBH_SHOP_CODE");
			nameVisitingKH = CursorUtil.getString(c, "CUSTOMER_NAME");
			codeVisitingKH = CursorUtil.getString(c, "CUSTOMER_CODE");
			addressVisitingKH = CursorUtil.getString(c, "CUSTOMER_ADDRESS");
			idVistingKH = CursorUtil.getInt(c, "CUSTOMER_ID");
			timeVisitingKH = CursorUtil.getString(c, "LAST_VISITED_TIME");
			numSaleStore = CursorUtil.getInt(c, "NUM_CUS_PLAN");

			String strGSNPPSeqPlan;
			String strCustomerSeqPlan;
			String strGSNPPRealSeq;
			String strGSNPPRealLat;
			String strGSNPPRealLng;
			String strGSNPPRealStartTime;
			String strGSNPPRealCustomerCodeName;
			String strGSNPPRealCustomerAddress;
			String strNvbhRealSeq;
			String strNvbhRealLat;
			String strNvbhRealLng;
			String strNvbhRealStartTime;
			String strNvbhIsOr;
			String strNVBHRealCustomerCodeName;
			String strNVBHRealCustomerAddress;
			String strNvbhRealEndTime;
			strGSNPPSeqPlan = CursorUtil.getString(c, "NVBH_DAY_SEQ");
			strCustomerSeqPlan = CursorUtil.getString(c, "NVBH_CUSTOMER_PLAN");
			strGSNPPRealSeq = CursorUtil.getString(c, "NPP_VISITED_ORDER");
			strGSNPPRealLat = CursorUtil.getString(c, "NPP_VISITED_LAT");
			strGSNPPRealLng = CursorUtil.getString(c, "NPP_VISITED_LNG");
			strGSNPPRealStartTime = CursorUtil.getString(c, "NPP_VISITED_START_TIME");
			strGSNPPRealCustomerCodeName = CursorUtil.getString(c, "NPP_VISITED_CUSTOMER_CODE_NAME");
			strGSNPPRealCustomerAddress = CursorUtil.getString(c, "NPP_VISITED_CUSTOMER_ADDRESS");
			strNvbhRealSeq = CursorUtil.getString(c, "NVBH_VISITED_ORDER");
			strNvbhRealLat = CursorUtil.getString(c, "NVBH_VISITED_LAT");
			strNvbhRealLng = CursorUtil.getString(c, "NVBH_VISITED_LNG");
			strNvbhRealStartTime = CursorUtil.getString(c, "NVBH_VISITED_START_TIME");
			strNvbhIsOr = CursorUtil.getString(c, "NVBH_VISITED_IS_OR");
			strNVBHRealCustomerCodeName = CursorUtil.getString(c, "NVBH_VISITED_CUSTOMER_CODE_NAME");
			strNvbhRealEndTime = CursorUtil.getString(c, "NVBH_VISITED_END_TIME");
			strNVBHRealCustomerAddress = CursorUtil.getString(c, "NVBH_VISITED_CUSTOMER_ADDRESS");

			String[] arrGsnppRealSeq = strGSNPPRealSeq.split(",");
			String[] arrGsnppRealLat = strGSNPPRealLat.split(",");
			String[] arrGsnppRealLng = strGSNPPRealLng.split(",");
			String[] arrGsnppRealStartTime = strGSNPPRealStartTime.split(",");
			String[] arrGsnppRealCustomerCodeName = strGSNPPRealCustomerCodeName.split(",");
			String[] arrGsnppRealCustomerAddress = strGSNPPRealCustomerAddress.split("seperator");
			String[] arrNvbhRealSeq = strNvbhRealSeq.split(",");
			String[] arrNvbhRealLat = strNvbhRealLat.split(",");
			String[] arrNvbhRealLng = strNvbhRealLng.split(",");
			String[] arrNvbhRealStartTime = strNvbhRealStartTime.split(",");
			String[] arrNvbhRealEndTime = strNvbhRealEndTime.split(",");
			String[] arrNvbhRealCustomerCodeName = strNVBHRealCustomerCodeName.split(",");
			String[] arrNvbhRealCustomerAddress = strNVBHRealCustomerAddress.split("seperator");

			// init cac danh sach
//			customerSeqPlan = initListCustomer(arrCustomerSeqPlan, arrSeqPlan,
//					null, null, null, null, null, null, 1);
			gsnppRealSeq = initListCustomer(arrGsnppRealSeq, null, arrGsnppRealLat, arrGsnppRealLng,
					arrGsnppRealStartTime, null, arrGsnppRealCustomerCodeName, arrGsnppRealCustomerAddress, 1);

			nvbhRealSeq = initListCustomer(arrNvbhRealSeq, null, arrNvbhRealLat, arrNvbhRealLng, arrNvbhRealStartTime,
					arrNvbhRealEndTime, arrNvbhRealCustomerCodeName, arrNvbhRealCustomerAddress, 0);

			// chuyen cac customer co so sequence = 0 hoac isOr = 1 ra sau danh
			// sach
//			customerSeqPlan = rearrangeList(customerSeqPlan);
//			gsnppRealSeq = rearrangeList(gsnppRealSeq);
//			nvbhRealSeq = rearrangeList(nvbhRealSeq);

			// tong diem da ghe tham/tong so diem phai ghe trong ngay
			//numSaleStore = customerSeqPlan.size();
			numGsnppVisitedStore = gsnppRealSeq.size();
			numNvbhVisitedStore = nvbhRealSeq.size();

			// check dung sai lo trinh
			if (nvbhRealSeq.size() >= 2) {
				for (int i = 0; i < nvbhRealSeq.size(); i++) {
					Customer gsnppCus = getCusInList(nvbhRealSeq.get(i), gsnppRealSeq);
					if (i == 0) {
						if (gsnppCus != null
								&& gsnppCus.visit_start_time_2.before(nvbhRealSeq.get(i + 1).visit_start_time_2)) {
							gsnppCus.status = STATUS.RIGHT_PLAN;
							numRightPlan++;
						} else if (gsnppCus != null) {
							gsnppCus.status = STATUS.WRONG_PLAN;
							//numWrongPlan++;
						} else {
							nvbhRealSeq.get(i).status = STATUS.NVBH_VISITED_BUT_GSNPP;
							//numGsVisitedButNvbh++;
						}
					} else if (i < nvbhRealSeq.size() - 1) {
						if (gsnppCus != null
								&& gsnppCus.visit_start_time_2.after(nvbhRealSeq.get(i - 1).visit_end_time_2)
								&& gsnppCus.visit_start_time_2.before(nvbhRealSeq.get(i + 1).visit_start_time_2)) {
							gsnppCus.status = STATUS.RIGHT_PLAN;
							numRightPlan++;
						} else if (gsnppCus != null) {
							gsnppCus.status = STATUS.WRONG_PLAN;
							//numWrongPlan++;
						} else {
							nvbhRealSeq.get(i).status = STATUS.NVBH_VISITED_BUT_GSNPP;
							//numGsVisitedButNvbh++;
						}
					} else {
						if (gsnppCus != null
								&& gsnppCus.visit_start_time_2.after(nvbhRealSeq.get(i - 1).visit_end_time_2)) {
							gsnppCus.status = STATUS.RIGHT_PLAN;
							numRightPlan++;
						} else if (gsnppCus != null) {
							gsnppCus.status = STATUS.WRONG_PLAN;
							//numWrongPlan++;
						} else {
							nvbhRealSeq.get(i).status = STATUS.NVBH_VISITED_BUT_GSNPP;
							//numGsVisitedButNvbh++;
						}
					}
				}
			} else if (nvbhRealSeq.size() == 1) {
				// neu nvbh moi di 1 KH thi GS luon dung
				Customer gsnppCus = getCusInList(nvbhRealSeq.get(0), gsnppRealSeq);
				if (gsnppCus != null) {
					gsnppCus.status = STATUS.RIGHT_PLAN;
				}
				numRightPlan++;
			}
			
			for (int i = 0; i < gsnppRealSeq.size(); i++) {
				Customer cus = gsnppRealSeq.get(i);
				if (getCusInList(cus, nvbhRealSeq) == null) {
					cus.status = STATUS.GSNPP_VISITED_BUT_NVBH;
					numGsVisitedButNvbh++;
				}
			}
			
			numWrongPlan = numNvbhVisitedStore + numGsVisitedButNvbh - numRightPlan;
		}
		
		/**
		 * getCusInList
		 * 
		 * @author: TamPQ
		 * @return: ArrayList<String>
		 * @throws:
		 */
		public Customer getCusInList(Customer cus, ArrayList<Customer> list) {
			Customer cust = null;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).cusId == cus.cusId) {
					return list.get(i);
				}
			}
			return cust;
		}

		/**
		 * lay cusId lien truoc trong ds
		 * 
		 * @author: TamPQ
		 * @return: ArrayList<String>
		 * @throws:
		 */
		public int previousCusOf(Customer cus, ArrayList<Customer> list) {
			int pre_Id = -1;
			for (int i = 1; i < list.size(); i++) {
				if (list.get(i).cusId == cus.cusId) {
					pre_Id = list.get(i - 1).cusId;
					break;
				}
			}
			return pre_Id;
		}

		// /**
		// * getCusInList
		// *
		// * @author: TamPQ
		// * @return: ArrayList<String>
		// * @throws:
		// */
		// public Customer getCusInList(Customer cus, ArrayList<Customer> list)
		// {
		// Customer cust = null;
		// for (int i = 0; i < list.size(); i++) {
		// if (list.get(i).cusId == cus.cusId) {
		// return list.get(i);
		// }
		// }
		// return cust;
		// }

		/**
		 * isInList
		 * 
		 * @author: TamPQ
		 * @return: ArrayList<String>
		 * @throws:
		 */
		public boolean isInList(Customer cus, ArrayList<Customer> list) {
			boolean isInList = false;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).cusId == cus.cusId) {
					isInList = true;
					break;
				}
			}
			return isInList;
		}

		/**
		 * lay seq trong plan
		 * 
		 * @author: TamPQ
		 * @return: boolean
		 * @throws:
		 */
		private int getSeqInPlan(Customer cus) {
			int seq = -1;
			for (int i = 0; i < customerSeqPlan.size(); i++) {
				if (cus.cusId == customerSeqPlan.get(i).cusId) {
					seq = customerSeqPlan.get(i).seqInPlan;
					break;
				}
			}
			return seq;
		}

		/**
		 * rearrange danh sach KH co isOr = 1 hay seq cua no = 1 thi xep sau ds
		 * 
		 * @author: TamPQ
		 * @return: boolean
		 * @throws:
		 */
		public ArrayList<Customer> rearrangeList(ArrayList<Customer> list) {
			ArrayList<Customer> tempList1 = new ArrayList<Customer>();
			ArrayList<Customer> tempList2 = new ArrayList<Customer>();
			for (int i = 0, s = list.size(); i < s; i++) {
				if (list.get(i).isOr == 0 && getSeqInPlan(list.get(i)) > 0) {
					list.get(i).seqInPlan = getSeqInPlan(list.get(i));
					tempList1.add(list.get(i));
				} else {
					tempList2.add(list.get(i));
				}
			}
			for (int i = 0, s = tempList2.size(); i < s; i++) {
				tempList1.add(tempList2.get(i));
			}
			return tempList1;
		}

		/**
		 * init List Customer In Plan
		 * 
		 * @author: TamPQ
		 * @return: ArrayList<Customer>
		 * @throws:
		 */
		public ArrayList<Customer> initListCustomer(String[] arrCus, String[] arrSeq, String[] lat, String[] lng,
				String[] arrStartTime, String[] arrEndTime, String[] arrCusCodeName, String[] arrCusAdd, int mode) {
			ArrayList<Customer> tempList = new ArrayList<Customer>();
			if (!StringUtil.isNullOrEmpty(arrCus[0])) {
				for (int i = 0; i < arrCus.length; i++) {
					Customer cus = new Customer(STATUS.NONE_VISIT);
					cus.realSeq = i;
					cus.cusId = Integer.parseInt(arrCus[i]);
					if (arrSeq != null && i < arrSeq.length) {
						cus.seqInPlan = Integer.parseInt(arrSeq[i]);
					}
					if (lat != null && i < lat.length) {
						cus.lat = Double.parseDouble(lat[i]);
					}
					if (lng != null && i < lng.length) {
						cus.lng = Double.parseDouble(lng[i]);
					}
					if (arrStartTime != null && i < arrStartTime.length) {
						cus.visit_start_time = arrStartTime[i];
						cus.visit_start_time_2 = DateUtils.parseDateFromString(cus.visit_start_time,
								DateUtils.defaultDateFormat_2);
						cus.visit_start_time = DateUtils.convertDateTimeWithFormat(cus.visit_start_time_2,
								DateUtils.DATE_FORMAT_HOUR_MINUTE);
					}
					if (arrEndTime != null && i < arrEndTime.length) {
						cus.visit_end_time = arrEndTime[i];
						cus.visit_end_time_2 = DateUtils.parseDateFromString(cus.visit_end_time,
								DateUtils.defaultDateFormat_2);
						cus.visit_end_time = DateUtils.convertDateTimeWithFormat(cus.visit_end_time_2,
								DateUtils.DATE_FORMAT_HOUR_MINUTE);
					}
					if (arrCusCodeName != null && i < arrCusCodeName.length) {
						cus.codeName = arrCusCodeName[i];
					}
					if (arrCusAdd != null && i < arrCusAdd.length) {
						cus.address = arrCusAdd[i];
					}
					tempList.add(cus);
				}
				try {	
					if (DateUtils.isIn30Min(tempList.get(tempList.size() - 1).visit_start_time_2)) {
						if(mode == 0){
							if(StringUtil.isNullOrEmpty(tempList.get(tempList.size() - 1).visit_end_time)){
								tempList.get(tempList.size() - 1).isVisting = true;
							}
						}else{
							tempList.get(tempList.size() - 1).isVisting = true;
						}

					}
				} catch (ParseException e) {
				}

			}
			return tempList;
		}

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param idVistingKH2
		 * @return0.
		 * 
		 * @return: Stringvoid
		 * @throws:
		 */
		public int getRealSequence(int idVistingKH, ArrayList<Customer> list) {
			int seq = -1;
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).cusId == idVistingKH) {
					seq = list.get(i).realSeq + 1;
					break;
				}
			}
			return seq;
		}

		/**
		 * get NVBH Visit Time
		 * 
		 * @author: TamPQ
		 * @param cusId
		 * @param nvbhRealSeq2
		 * @return
		 * @return: Stringvoid
		 * @throws:
		 */
		public String getNVBHVisitTime(int cusId, ArrayList<Customer> list) {
			String time = "";
			for (int i = 0; i < list.size(); i++) {
				if (cusId == list.get(i).cusId) {
					time = list.get(i).visit_time;
					break;
				}
			}
			return time;
		}

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param idVistingKH2
		 * @return
		 * @return: booleanvoid
		 * @throws:
		 */
		public boolean isCusWrongPlan(int idVistingKH2) {
			for (int i = 0; i < gsnppRealSeq.size(); i++) {
				if (idVistingKH2 == gsnppRealSeq.get(i).cusId && gsnppRealSeq.get(i).status == STATUS.WRONG_PLAN) {
					return true;
				}
			}
			return false;
		}

		public Customer getCusFromRealSequence(int index, ArrayList<Customer> list) {
			for (int i = 0; i < list.size(); i++) {
				if (index == list.get(i).realSeq) {
					return list.get(i);
				}
			}
			return null;
		}


		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param c
		 * @return: voidvoid
		 * @throws:
		 */
		public void initItem(Cursor c) {
			staffId = CursorUtil.getString(c, STAFF_TABLE.STAFF_ID);
			staffCodeGsnpp = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
			shopCodeGsnpp = CursorUtil.getString(c, "SHOP_CODE");
			nameGsnpp = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
			shopId = CursorUtil.getInt(c, STAFF_TABLE.SHOP_ID);
			gsnppMobile = CursorUtil.getString(c, STAFF_TABLE.MOBILE_PHONE);
			gsnppPhone = CursorUtil.getString(c, STAFF_TABLE.PHONE);
			if (StringUtil.isNullOrEmpty(gsnppMobile)) {
				gsnppMobile = gsnppPhone;
			}
			if (StringUtil.isNullOrEmpty(gsnppMobile)) {
				gsnppMobile = "";
			}
			lat = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LAT);
			lng = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LNG);
			updateTime = CursorUtil.getString(c, "DATE_TIME");
		}

	}
}
