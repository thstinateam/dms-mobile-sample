/*
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.controller.AbstractController;
import com.ths.dmscore.dto.view.OrderDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Lop dung chung cho tat ca cac AlertDialog
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 1.0
 */
public class AbstractAlertDialog extends AlertDialog implements
		OnTouchListener, android.view.View.OnClickListener,
		DialogInterface.OnShowListener {

	protected final GlobalBaseActivity parent;
	protected BaseFragment listener;
	private final View v;
	public TextView tvTitleAbstractAlert;
	private ViewStub viewStub;

	// danh sach cac textview can order
	protected TextView[] listTextViewOrder;

	private AbstractAlertDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		parent = (GlobalBaseActivity) context;
		LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = li.inflate(R.layout.abstract_alertdialog, null);
		setView(v);
		setCanceledOnTouchOutside(false);
		Window window = getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		window.setGravity(Gravity.CENTER);
	}

	public AbstractAlertDialog(Context context, BaseFragment base,
			CharSequence title) {
		this(context);
		setOnShowListener(this);
		tvTitleAbstractAlert = (TextView) v.findViewById(R.id.tvTitleAbstractAlert);
		tvTitleAbstractAlert.setTypeface(null, Typeface.BOLD);
		tvTitleAbstractAlert.setOnTouchListener(this);
		viewStub = (ViewStub) v.findViewById(R.id.vStub);
		v.setOnClickListener(this);
		listener = base;
		setTitle(title);
	}

	/**
	 * Set title
	 *
	 * @author: dungdq3
	 * @param: int action
	 * @param: Bundle b
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 * @date: Jan 9, 2014
	 */
	public void setTitle(CharSequence title){
		tvTitleAbstractAlert.setText(title);
	}

	/**
	 * Chuyen fragment
	 *
	 * @author: dungdq3
	 * @param: int action
	 * @param: Bundle b
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 * @date: Jan 9, 2014
	 */
	protected void switchFragment(int action, Bundle b, AbstractController abController) {
		// TODO Auto-generated method stub
		ActionEvent e = new ActionEvent();
		e.action = action;
		e.sender = listener;
		e.viewData = (b == null ? new Bundle() : b);
		abController.handleSwitchFragment(e);
	}

	/**
	 * Set view cho viewStub
	 *
	 * @author: dungdq3
	 * @param: Tham so cua ham
	 * @return: void
	 * @throws: Ngoai le do ham dua ra (neu co)
	 * @date: Jan 9, 2014
	 */
	protected View setViewLayout(int layoutResource) {
		// TODO Auto-generated method stub
		viewStub.setLayoutResource(layoutResource);
		viewStub.getLayoutParams().width = GlobalUtil.getInstance().getDPIScreen();
		return viewStub.inflate();
	}

	protected View setViewLayout(int layoutResource, int width) {
		// TODO Auto-generated method stub
		viewStub.setLayoutResource(layoutResource);
		viewStub.getLayoutParams().width = width;
		return viewStub.inflate();
	}


	/**
	 * Tao su kien cho control tuong ung,
	 *
	 * @author: dungdq3
	 * @param: int action (hanh dong duoc lay trong ActionEventConstant)
	 * @param: View control (control goi su kien nay)
	 * @param: Object data (du lieu de truyen sang listener)
	 * @return: void
	 * @throws:
	 * @date: Jan 9, 2014
	 */
	protected void setEventFromAlert(int action, View control, Object data) {
		// TODO Auto-generated method stub
		listener.onEvent(action, control, data);
	}

	/**
	 * show dialog by alert
	 *
	 * @author: dungdq3
	 * @param: CharSequence title
	 * @param: String textOK
	 * @param: int actionOK
	 * @param: String textCancel
	 * @param: int actionCancel
	 * @param: Object data
	 * @return: void
	 * @date: Jan 9, 2014
	 */
	protected void showDialogFromAlert(CharSequence title, String textOK, int actionOK, String textCancel, int actionCancel, Object data) {
		// TODO Auto-generated method stub
		GlobalUtil.showDialogConfirm(listener, parent, title, textOK, actionOK, textCancel, actionCancel, data);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		boolean onTouch = false;
		if (v == tvTitleAbstractAlert
				&& event.getAction() == MotionEvent.ACTION_DOWN) {
			Drawable[] arrDrawable = tvTitleAbstractAlert.getCompoundDrawables().clone();
			Drawable drawable = arrDrawable[2]; // get right Drawable;
			int x = (int) event.getX();
			if (x > tvTitleAbstractAlert.getWidth()
					- tvTitleAbstractAlert.getPaddingRight()
					- drawable.getIntrinsicWidth()) { // neu vi tri cham nam
													  // trong drawable thi
				forceHideKeyBoardForDialog();
				dismiss(); // se dong dialog
				onClick(tvTitleAbstractAlert);
				onTouch = true;
			}
		} else if (v == this.v) {
			forceHideKeyBoardForDialog();
		} else if (v == tvTitleAbstractAlert) {
			forceHideKeyBoardForDialog();
		}
		return onTouch;
	}

	/**
	 * handle event to listener
	 *
	 * @author: dungdq3
	 * @since: 1:46:34 PM Mar 6, 2014
	 * @return: void
	 * @param: action
	 * @param: bundle
	 * @param: abController
	 * @throws:
	 */
	protected void setHandleViewEvent( Bundle bundle, int action, AbstractController abController) {
		// TODO Auto-generated method stub
		ActionEvent e = new ActionEvent();
		e.action = action;
		e.sender = listener;
		e.viewData = (bundle == null ? new Bundle() : bundle);
		abController.handleViewEvent(e);
	}

	/**
	 * An keyboard
	 *
	 * @author: dungdq3
	 * @since: 4:56:28 PM Mar 18, 2014
	 * @return: void
	 * @throws:
	 */
	public void forceHideKeyBoardForDialog() {
		// TODO Auto-generated method stub
		GlobalUtil.forceHideKeyboardInput(parent, v);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		forceHideKeyBoardForDialog();
	}

	@Override
	public void onShow(DialogInterface dialog) {
		// TODO Auto-generated method stub
	}

	/**
	 * set su kien order tang hoac giam theo column duoc chon
	 *
	 * @author: dungdq3
	 * @since: 4:04:22 PM Feb 13, 2015
	 * @return: void
	 * @throws:
	 * @param orderAscOrDesc
	 * @param orderColumn
	 */
	protected final void setEventOrder(TextView tvClick, boolean orderAscOrDesc, String orderColumn){
		for(TextView tv : listTextViewOrder) {
			if(tv.getId() != tvClick.getId()) {
				tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			}
		}
		if(orderAscOrDesc) {
			tvClick.setCompoundDrawablesWithIntrinsicBounds(R.drawable.order_asc, 0, 0, 0);
		} else {
			tvClick.setCompoundDrawablesWithIntrinsicBounds(R.drawable.order_desc, 0, 0, 0);
		}
		OrderDTO order = new OrderDTO(orderAscOrDesc, orderColumn);
		listener.onEvent(ActionEventConstant.ACTION_ORDER, tvClick, order);
	}
}
