/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.TBHVController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedBackTBHVDTO;
import com.ths.dmscore.dto.db.ListProductDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.db.TrainingResultDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingIssueDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingPlanDTO;
import com.ths.dmscore.dto.view.CabinetStaffDTO;
import com.ths.dmscore.dto.view.ComboboxDisplayProgrameDTO;
import com.ths.dmscore.dto.view.CustomerNotPsdsInMonthReportDTO;
import com.ths.dmscore.dto.view.GsnppRouteSupervisionDTO;
import com.ths.dmscore.dto.view.IntroduceProductDTO;
import com.ths.dmscore.dto.view.ListStaffDTO;
import com.ths.dmscore.dto.view.OfficeDocumentListDTO;
import com.ths.dmscore.dto.view.ReportNVBHVisitCustomerDTO;
import com.ths.dmscore.dto.view.SupervisorReportStaffSaleViewDTO;
import com.ths.dmscore.dto.view.TBHVAddRequirementViewDTO;
import com.ths.dmscore.dto.view.TBHVAttendanceDTO;
import com.ths.dmscore.dto.view.TBHVCustomerListDTO;
import com.ths.dmscore.dto.view.TBHVCustomerNotPSDSReportDTO;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportDTO;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportNPPDTO;
import com.ths.dmscore.dto.view.TBHVDisplayProgrameModel;
import com.ths.dmscore.dto.view.TBHVFollowProblemDTO;
import com.ths.dmscore.dto.view.TBHVFollowProblemItemDTO;
import com.ths.dmscore.dto.view.TBHVListImageDTOView;
import com.ths.dmscore.dto.view.TBHVManagerEquipmentDTO;
import com.ths.dmscore.dto.view.TBHVProgReportProDispDetailSaleDTO;
import com.ths.dmscore.dto.view.TBHVProgressReportSalesFocusViewDTO;
import com.ths.dmscore.dto.view.TBHVReportDisplayProgressDetailDayViewDTO;
import com.ths.dmscore.dto.view.TBHVRouteSupervisionDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanDayResultReportDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanHistoryAccDTO;
import com.ths.dmscore.dto.view.TBHVVisitCustomerNotificationDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSubTrainingPlanDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSupTrainingIssueDTO;
import com.ths.dmscore.dto.view.trainingplan.ListTrainingRateDTO;
import com.ths.dmscore.dto.view.trainingplan.TBHVDayTrainingSupervisionDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateDetailResultlDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateResultDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.sqllite.db.FEEDBACK_TABLE;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.ImageSearchViewDTO;
import com.ths.dmscore.dto.view.ReportProgressMonthViewDTO;
import com.ths.dmscore.dto.view.TBHVProgressDateReportDTO;
import com.ths.dmscore.dto.view.TBHVPromotionProgrameDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dms.R;

/**
 * Tang xu ly model cua TBHV
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class TBHVModel extends AbstractModelService {
	protected static volatile TBHVModel instance;

	protected TBHVModel() {
	}

	public static TBHVModel getInstance() {
		if (instance == null) {
			instance = new TBHVModel();
		}
		return instance;
	}

	/**
	 * Lay ds hinh anh tim kiem
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getListAlbum(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		ImageSearchViewDTO dto = null;
		try {
			dto = new ImageSearchViewDTO();
			dto = SQLUtils.getInstance().getListAlbumTBHV(data);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		} finally {

		}
	}

	/**
	 * Danh sach hinh anh cua TBHV
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getListImageOfTBHV(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
			int numPerPage = data.getInt(IntentConstants.INTENT_NUMTOP);
			int offset = data.getInt(IntentConstants.INTENT_PAGE);
			TBHVListImageDTOView dto = SQLUtils.getInstance().getListImageOfTBHV(shopId, numPerPage, offset);

			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.MESSAGE_NO_CUSTOMER_INFO));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			SaleController.getInstance().handleErrorModelEvent(model);
		} finally {

		}
	}

	public void onReceiveData(HTTPMessage mes) {
		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(mes.getDataText());
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
			return;
		}
		switch (mes.getAction()) {
		default:
			int errCodeDefault = ErrorConstants.ERROR_COMMON;
			try {
				JSONObject json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				errCodeDefault = result.getInt("errorCode");
				model.setModelCode(errCodeDefault);
				model.setModelData(actionEvent.userData);
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			} finally {
				// thanh cong, that bai khong gui tra kq ve view nua
				if (errCodeDefault == ErrorConstants.ERROR_CODE_SUCCESS) {
					// TH mac dinh la request create/update/delete du lieu
					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
					// UserController.getInstance().handleModelEvent(model);
				} else if (errCodeDefault == ErrorConstants.ERROR_UNIQUE_CONTRAINTS) {
					// request loi trung khoa -- khong thuc hien goi lai len
					// server nua
					updateLog(actionEvent, LogDTO.STATE_UNIQUE_CONTRAINTS);
				} else {
					// ghi log loi len server
					updateLog(actionEvent, LogDTO.STATE_FAIL);
				}
			}
		}
	}

	public void onReceiveError(HTTPResponse response) {
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(response.getDataText());
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (actionEvent.action == ActionEventConstant.ACTION_LOGIN) {
			GlobalInfo.getInstance().getProfile().getUserData().setLoginState(UserDTO.NOT_LOGIN);
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelMessage(response.getErrMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		} else if (GlobalUtil.checkActionSave(actionEvent.action)
				&& actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO log = (LogDTO) actionEvent.logData;
			if (LogDTO.STATE_NONE.equals(log.state)) {
				updateLog(actionEvent, LogDTO.STATE_NEW);
				// log.state = LogDTO.STATE_NEW;
				// SQLUtils.getInstance().insert(log);
			} else {
				// log.state = LogDTO.STATE_FAIL;
				// SQLUtils.getInstance().update(log);
				updateLog(actionEvent, LogDTO.STATE_FAIL);
			}
			// luu thanh cong
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			TBHVController.getInstance().handleModelEvent(model);
		} else if (actionEvent.action == ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER) {
			model.setModelData(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelMessage(response.getErrMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * Lay ds thiet bi theo NVBH cua NPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getListEquipment(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		TBHVManagerEquipmentDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getTBHVListEquipment(shopId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * requestListRouteSupervision
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestListRouteSupervision(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		Bundle bundle = (Bundle) e.viewData;
		int staff_id = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		int day = bundle.getInt(IntentConstants.INTENT_DAY_OF_WEEK);
		TBHVRouteSupervisionDTO dto;
		dto = SQLUtils.getInstance().getTBHVListRouteSupervision(staff_id, shopId, day);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach khuyen mai
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public ModelEvent requestGetListPromotionPrograme(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle bundle = (Bundle) event.viewData;
		TBHVPromotionProgrameDTO result = null;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String ext = bundle.getString(IntentConstants.INTENT_PAGE);
		boolean checkLoadMore = bundle.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		result = SQLUtils.getInstance().getTBHVListPromotionPrograme(shopId, ext, checkLoadMore);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds chuong trinh khuyen mai cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListDisplayPrograme(ActionEvent event) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle ext = (Bundle) event.viewData;
		boolean checkRequestCombobox = ext.getBoolean(
				IntentConstants.INTENT_CHECK_COMBOBOX, false);
		TBHVDisplayProgrameModel result = null;
		result = SQLUtils.getInstance().getListTBHVDisplayPrograme(ext);
		if (checkRequestCombobox) {
			ComboboxDisplayProgrameDTO comboboxModel = new ComboboxDisplayProgrameDTO();
			comboboxModel.listDepartPrograme = SQLUtils.getInstance()
					.getListDepartDisplayPrograme();
			result.setComboboxDTO(comboboxModel);
		} else {
			result.setComboboxDTO(null);
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	/**
	 * HieuNH lay danh sach bao cao ngay cua TBHV
	 *
	 * @param e
	 */
	public ModelEvent getTBHVProgressDateReport(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		TBHVProgressDateReportDTO dto = SQLUtils.getInstance().getTBHVProgressDateReport(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach not PSDS cua TBHV
	 *
	 * @param e
	 */
	public void getTBHVNotPSDSReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		TBHVCustomerNotPSDSReportDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getTBHVNotPSDSReport(staffOwnerId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 * HieuNH lay danh sach bao cao ngay cua NVGS cua NPP thuoc TBHV
	 *
	 * @param e
	 */
	public ModelEvent getTBHVProgressDateDetailReport(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		int sysCurrencyDevide = viewInfo.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE);
		TBHVProgressDateReportDTO dto = SQLUtils.getInstance().getTBHVProgressDateDetailReport(shopId, staffOwnerId,sysCurrencyDevide);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * HieuNH lay danh sach Nha phan phoi
	 *
	 * @param e
	 */
	public ModelEvent getListNPP(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<ShopDTO> dto = SQLUtils.getInstance().getListNPP(shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * lay bao cao luy ke doanh so theo nganh cua tung NPP
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getTBHVTotalCatAmountReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		SupervisorReportStaffSaleViewDTO result = null;
		try {
			result = SQLUtils.getInstance().getTBHVTotalCatAmountReport(data);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			SaleController.getInstance().handleModelEvent(model);
		} else {
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * HieuNH lay danh sach NVGS of report dayInOrder
	 *
	 * @param e
	 */
	public ModelEvent getListNVGSOfTBHVReportDate(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		ListStaffDTO dto = SQLUtils.getInstance().getListNVGSOfTBHVReportDate(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach NVGS
	 *
	 * @param e
	 */
	public void getListNVGSOfTBHVReportPSDS(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		ListStaffDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getListNVGSOfTBHVReportPSDS(shopId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * lay danh sach bao cao tien do chung CTTB ngay (TBHV)
	 *
	 * @author: HoanPD
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getTBHVDisProComProReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		try {
			TBHVDisProComProgReportDTO dto = SQLUtils.getInstance()
					.getTBHVDisProComProReport((Bundle) e.viewData);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);

				model.setIsSendLog(false);
				ServerLogger
						.sendLog(
								"method getTBHVDisProComProReport error: ",
								"sql get report progress cttb for tbhv of module tbhv error",
								TabletActionLogDTO.LOG_CLIENT);

				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());

			model.setIsSendLog(false);
			ServerLogger
					.sendLog(
							"method getTBHVDisProComProReport error: ",
							"sql get report progress cttb for tbhv of module tbhv error",
							TabletActionLogDTO.LOG_CLIENT);

			TBHVController.getInstance().handleErrorModelEvent(model);
		}

		finally {
		}

	}

	/**
	 *
	 * lay danh sach: bao cao tien do chung CTTB ngay (TBHV)
	 *
	 * @author: HoanPD
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getTBHVStaffDisProComProReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		try {
			TBHVDisProComProgReportNPPDTO dto = null;
			dto = SQLUtils.getInstance().getTBHVDisProComProReportNPP(viewInfo);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));

				model.setIsSendLog(false);
				ServerLogger
						.sendLog(
								"method getTBHVStaffDisProComProReport error: ",
								"sql get report progress cttb for npp of module tbhv error",
								TabletActionLogDTO.LOG_CLIENT);

				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());

			model.setIsSendLog(false);
			ServerLogger
					.sendLog(
							"method getTBHVStaffDisProComProReport error: ",
							"sql get report progress cttb for npp of module tbhv error",
							TabletActionLogDTO.LOG_CLIENT);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * Lay ds sp cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getTBHVListProductStorage(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		Bundle bundle = (Bundle) e.viewData;
		ListProductDTO dto = SQLUtils.getInstance().getTBHVListProductStorage(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay thong tin san pham TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public void getIntroduceProduct(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		String productId = data.getString(IntentConstants.INTENT_PRODUCT_ID);
		IntroduceProductDTO result = null;
		try {
			result = SQLUtils.getInstance().getIntroduceProduct(productId);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * get report progress in month
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getReportProgressInMonthTBHV(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub

		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;
		ReportProgressMonthViewDTO result = new ReportProgressMonthViewDTO();
		result = SQLUtils.getInstance().getReportProgressInMonthTBHV(viewInfo);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
//	public void getTbhvTrainingPlan(ActionEvent e) {
//		ModelEvent model = new ModelEvent();
//		model.setActionEvent(e);
//		Bundle data = (Bundle) e.viewData;
//		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
//		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
//		try {
//			TBHVTrainingPlanDTO dto = SQLUtils.getInstance().getTbhvTrainingPlan(staffId, shopId);
//			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
//			model.setModelData(dto);
//			TBHVController.getInstance().handleModelEvent(model);
//		} catch (Exception ex) {
//			model.setModelMessage(ex.getMessage());
//			model.setModelCode(ErrorConstants.ERROR_COMMON);
//			TBHVController.getInstance().handleErrorModelEvent(model);
//		}
//	}

	/**
	 *
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
//	public void getGsnppTrainingPlan(ActionEvent e) {
//		ModelEvent model = new ModelEvent();
//		model.setActionEvent(e);
//		Bundle data = (Bundle) e.viewData;
//		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
//		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
//		try {
//			TBHVTrainingPlanDTO dto = SQLUtils.getInstance()
//					.getGsnppTrainingPlan(staffId, shopId);
//			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
//			model.setModelData(dto);
//			TBHVController.getInstance().handleModelEvent(model);
//		} catch (Exception ex) {
//			model.setModelMessage(ex.getMessage());
//			model.setModelCode(ErrorConstants.ERROR_COMMON);
//			TBHVController.getInstance().handleErrorModelEvent(model);
//		}
//	}

	/**
	 *
	 * Lay ds van de theo GSNPP cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getFollowlistProblem(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		TBHVFollowProblemDTO result = new TBHVFollowProblemDTO();
		try {
			Bundle data = (Bundle) event.viewData;
			result = SQLUtils.getInstance().getTBHVListFollowProblem(data);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * Update trang thai cua van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void updateFollowProblemDone(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		TBHVFollowProblemItemDTO dto = (TBHVFollowProblemItemDTO) e.viewData;
		try {
			returnCode = SQLUtils.getInstance().updateTBHVFollowProblemDone(dto);

			// send to server
			if (returnCode != -1) {
				JSONObject sqlPara = dto.generateUpdateFollowProblemSql(dto);
				JSONArray jarr = new JSONArray();
				jarr.put(sqlPara);
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);

				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());

				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);

			} else {
				ServerLogger.sendLog("tbhv problem id: " + dto.id, TabletActionLogDTO.LOG_CLIENT);
				model.setIsSendLog(false);

				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}

		} catch (Exception ex) {
			ServerLogger.sendLog(ex.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			model.setIsSendLog(false);

			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * HieuNH delete van de cua tbhv
	 *
	 * @param e
	 */
	public void deleteFollowProblemDone(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		TBHVFollowProblemItemDTO dto = (TBHVFollowProblemItemDTO) e.viewData;
		try {
			returnCode = SQLUtils.getInstance().deleteTBHVFollowProblemDone(dto);

			// send to server
			if (returnCode != -1) {
				JSONObject sqlPara = dto.generateDeleteFollowProblemSql(dto);
				JSONArray jarr = new JSONArray();
				jarr.put(sqlPara);
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);

				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());

				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);

			} else {
				ServerLogger.sendLog("tbhv problem id: " + dto.id, TabletActionLogDTO.LOG_CLIENT);
				model.setIsSendLog(false);

				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}

		} catch (Exception ex) {
			ServerLogger.sendLog(ex.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			model.setIsSendLog(false);

			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * getDayTrainingSupervision
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getDayTrainingSupervision(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		int trainDetailId = data.getInt(IntentConstants.INTENT_STAFF_TRAIN_DETAIL_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		try {
			TBHVTrainingPlanDayResultReportDTO dto = SQLUtils.getInstance().getDayTrainingSupervision(trainDetailId, shopId);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * getHistoryPlanTraining
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getPlanTrainingHistoryAcc(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int gsnppStaffId = data.getInt(IntentConstants.INTENT_GSNPP_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		boolean getListStaff = data.getBoolean(IntentConstants.INTENT_GET_LIST_GS);

		try {
			TBHVTrainingPlanHistoryAccDTO dto = SQLUtils.getInstance().getPlanTrainingHistoryAcc(staffId, gsnppStaffId, shopId, getListStaff);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * get report progress in month for module TBHV detail NPP screen
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getReportProgressInMonthTBHVDetailNPP(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;

		ReportProgressMonthViewDTO result = new ReportProgressMonthViewDTO();
		result = SQLUtils.getInstance().getReportProgressInMonthTBHVDetailNPP(data);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach NVBH
	 *
	 * @param e
	 */
	public void getListNVBHNotPSDS(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		try {
			ListStaffDTO dto = SQLUtils.getInstance().getListNVBHNotPSDS(shopId, staffOwnerId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * HieuNH lay danh sach NVBH
	 *
	 * @param e
	 */
	public void getListNVBH(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		try {
			ListStaffDTO dto = SQLUtils.getInstance().getListNVBH(shopId, staffOwnerId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * get report progress sales focus infor
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getReportProgressSalesFocusInfo(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		TBHVProgressReportSalesFocusViewDTO result = new TBHVProgressReportSalesFocusViewDTO();
		try {
			result = SQLUtils.getInstance().getReportProgressTBHVSalesFocusInfo(data);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			model.setModelMessage(ex.getMessage());
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			ServerLogger.sendLog("method getReportProgressSalesFocusInfo error: ", "sql get report progress sales focus info error", TabletActionLogDTO.LOG_CLIENT);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * send request get report progress sales focus detail info view
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getReportProgressSalesFocusDetailInfo(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		TBHVProgressReportSalesFocusViewDTO result = new TBHVProgressReportSalesFocusViewDTO();
		try {
			result = SQLUtils.getInstance().getReportProgressTBHVSalesFocusDetailInfo(data);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			model.setModelMessage(ex.getMessage());
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			ServerLogger.sendLog("method getReportProgressSalesFocusDetailInfo error: ", "sql get report progress sales focus detail info error", TabletActionLogDTO.LOG_CLIENT);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * send request get report progress sales focus detail info view
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getTBHVReviewsGSNPPDetailInfo(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		List<FeedBackTBHVDTO> listTrainingReviews = new ArrayList<FeedBackTBHVDTO>();
		try {
			listTrainingReviews = SQLUtils.getInstance().getTrainingReviewsGSNPPOfTBHVInfo(data);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			model.setModelMessage(ex.getMessage());
			listTrainingReviews = null;
		}
		if (listTrainingReviews != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(listTrainingReviews);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			ServerLogger.sendLog("method getTBHVReviewsGSNPPDetailInfo error: ", "sql get reveiws gsnpp of tbhv error", TabletActionLogDTO.LOG_CLIENT);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * tbhv update training reviews to db local and db server
	 *
	 * @author: HaiTC3
	 * @param actionEvent
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	public HTTPRequest tbhvUpdateTrainingReviewsToDBAndServer(ActionEvent actionEvent) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);

		HTTPRequest re = null;
		try {
			int result = 0;
			Bundle data = (Bundle) actionEvent.viewData;
			@SuppressWarnings("unchecked")
			List<FeedBackTBHVDTO> listTrainingReviews = (List<FeedBackTBHVDTO>) data.getSerializable(IntentConstants.INTENT_TBHV_LIST_TRAINING_SHOP_OBJECT);

			listTrainingReviews = SQLUtils.getInstance().tbhvUpdateIdForTrainingResult(listTrainingReviews);

			result = SQLUtils.getInstance().tbhvInsertAndUpdateReviewsStaffDTO(listTrainingReviews);

			if (result == 1) {
				JSONArray listSql = this.generalSQLInsertAndUpdateTrainingReviewsToServer(listTrainingReviews);
				String logId = GlobalUtil.generateLogId();
				actionEvent.userData = result;
				actionEvent.logData = logId;
				// send to server
				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(listSql);
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(listSql.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);

				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());

				re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				model.setModelData(actionEvent.userData);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				model.setIsSendLog(false);
				ServerLogger.sendLog("method tbhvUpdateTrainingReviewsToDBAndServer error: ", "sql update training reviews to db and server error", TabletActionLogDTO.LOG_CLIENT);
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			ServerLogger.sendLog("method tbhvUpdateTrainingReviewsToDBAndServer error: ", ex.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
		return re;
	}

	/**
	 *
	 * general sql insert and update/delete training reviews of tbhv
	 *
	 * @author: HaiTC3
	 * @param listTrainingReviews
	 * @return
	 * @return: JSONArray
	 * @throws:
	 */
	public JSONArray generalSQLInsertAndUpdateTrainingReviewsToServer(List<FeedBackTBHVDTO> listTrainingReviews) {
		JSONArray kq = new JSONArray();

		// general SQL for reviews info. insert new reviews and update content
		// for old reviews
		for (int i = 0, size = listTrainingReviews.size(); i < size; i++) {
			FeedBackTBHVDTO objectData = listTrainingReviews.get(i);
			// new reviews
			if (objectData.currentState == TrainingResultDTO.STATE_NEW_INSERT) {
				kq.put(objectData.feedBackBasic.generateFeedbackSql());
			}
			// update reviews
			else if (objectData.currentState == TrainingResultDTO.STATE_NEW_UPDATE) {
				kq.put(objectData.feedBackBasic.generateUpdateContentFeedbackSql());
			}
			// delete reviews
			else if (objectData.currentState == TrainingResultDTO.STATE_DELETED) {
				kq.put(objectData.feedBackBasic.generalSqlDeleteFeedBackOutOfDB());
			}
		}
		return kq;
	}

	/**
	 * HieuNH lay danh sach bao cao khach hang chua psds trong thang
	 *
	 * @param e
	 */
	public void getCountCustomerNotPsdsInMonthReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String visit_plan = viewInfo.getString(IntentConstants.INTENT_VISIT_PLAN);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		try {
			int count = 0;
			count = SQLUtils.getInstance().getCountCustomerNotPsdsInMonthReport(shopId, staffOwnerId, visit_plan, staffId);
			if (count >= 0) {
				model.setModelData(Integer.valueOf(count));
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(count);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 * HieuNH lay danh sach bao cao khach hang chua psds trong thang
	 *
	 * @param e
	 */
	public void getCustomerNotPsdsInMonthReport(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String visit_plan = viewInfo.getString(IntentConstants.INTENT_VISIT_PLAN);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		int page = viewInfo.getInt(IntentConstants.INTENT_PAGE);
		int sysCalUnApproved = viewInfo.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		String listStaff = viewInfo.getString(IntentConstants.INTENT_LIST_STAFF);
		DMSSortInfo sortInfo = (DMSSortInfo) viewInfo.getSerializable(IntentConstants.INTENT_SORT_DATA);
		CustomerNotPsdsInMonthReportDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getCustomerNotPsdsInMonthReport(shopId, staffOwnerId, visit_plan, staffId, page, sysCalUnApproved, listStaff
					, sortInfo);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 * HieuNH lay danh sach thiet bi TBHV
	 *
	 * @param e
	 */
	public void getListEquipmentTBHV(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int parentShopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_PARENT_SHOP_ID));
		TBHVManagerEquipmentDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getListEquipmentTBHV(parentShopId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 * Cap nhat duong dan hinh anh duoi tablet cho sp
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void updateClientThumbnailUrl(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		MediaItemDTO dto = (MediaItemDTO) data.get(IntentConstants.INTENT_DATA);
		int result = 0;
		try {
			result = SQLUtils.getInstance().updateClientThumbnailUrl(dto);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		if (result > 0) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * lay danh sach bao cao tien do CTTB chi tiet NVBH ngay ( TBHV)
	 *
	 * @author: HoanPD1
	 * @modified: BangHN
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public void getTBHVReportDisplayProgressDetailDay(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;

		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		// int shopId = bundle.getInt(IntentConstants.INTENT_SHOP_ID);
		String displayProgrameCode = bundle
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String displayProgrameId = bundle
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String displayProgrameLevel = null;
		int checkAll = bundle.getInt(IntentConstants.INTENT_ID);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		TBHVReportDisplayProgressDetailDayViewDTO dto = null;
		try {
			dto = SQLUtils.getInstance()
					.getTBHVReportDisplayProgressDetailDayDTO(staffId,
							displayProgrameId, displayProgrameCode,
							displayProgrameLevel, checkAll, page);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * lay danh sach bao cao tien do CTTB chi tiet NVBH ngay ( TBHV)
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public void getTBHVProgReportProDispDetailSale(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;

		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		// int shopId = bundle.getInt(IntentConstants.INTENT_SHOP_ID);
		String displayProgrameCode = bundle.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String displayProgrameLevel = null;
		int checkAll = bundle.getInt(IntentConstants.INTENT_ID);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		TBHVProgReportProDispDetailSaleDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getTBHVProgReportProDispDetailSaleDTO(staffId, displayProgrameCode, displayProgrameLevel, checkAll, page);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * Lay so luong thiet bi NVBH quan ly
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getCountCabinetStaff(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int staffId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		int isAll = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_IS_ALL));
		try {
			int count = SQLUtils.getInstance().getCountCabinetStaff(shopId, staffId, isAll);
			if (count != -1) {
				model.setModelData(count);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(count);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}

	}

	/**
	 *
	 * Lay ds thiet bi NVBH quan ly
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getCabinetStaff(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int staffId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		int isAll = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_IS_ALL));
		String page = viewInfo.getString(IntentConstants.INTENT_PAGE);
		CabinetStaffDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getCabinetStaff(shopId, staffId, isAll, page);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Lay thong tin so luong ctkm & cttb dang chay
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public void getBusinessSupportProgrameInfo(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		Bundle dto = null;
		try {
			dto = SQLUtils.getInstance().getBusinessSupportProgrameInfo(shopId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * lay danh sach khach hang thuoc GSNPP
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getCustomerListForPostFeedback(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			TBHVCustomerListDTO dto = SQLUtils.getInstance().getCustomerListForPostFeedback(data);

			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.MESSAGE_NO_CUSTOMER_INFO));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		} finally {

		}
	}

	/**
	 * Lay thong tin cho man hinh tao yeu cau: lay GSNPP, loai van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public void getAddRequirementInfo(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle viewInfo = (Bundle) e.viewData;

		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		TBHVAddRequirementViewDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getAddRequirementInfo(shopId);
			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Them van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public HTTPRequest addProblem(ActionEvent actionEvent) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		HTTPRequest re = null;
			// insert to sql Lite & request to server
			FeedBackDTO feedBack = (FeedBackDTO) actionEvent.viewData;
			boolean result = SQLUtils.getInstance().createTBHVFeedback(feedBack);

			if (result) {
				// JSONArray listSql = new JSONArray();
				// JSONObject insertSql = feedBack.generateFeedbackSql();
				// listSql.put(insertSql);
				//
				// String logId = GlobalUtil.generateLogId();
				// actionEvent.logData = logId;
				// // send to server
				// Vector<Object> para = new Vector<Object>();
				// // para.add(IntentConstants.INTENT_LIST_DECLARE);
				// // para.add(sqlPara.get(0));
				// para.add(IntentConstants.INTENT_LIST_SQL);
				// para.add(listSql);
				// para.add(IntentConstants.INTENT_MD5);
				// para.add(StringUtil.md5(listSql.toString()));
				// para.add(IntentConstants.INTENT_LOG_ID);
				// para.add(logId);
				// para.add(IntentConstants.INTENT_STAFF_ID_PARA);
				// para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
				// para.add(IntentConstants.INTENT_IMEI_PARA);
				// para.add(GlobalInfo.getInstance().getDeviceIMEI());
				//
				// re = sendHttpRequestOffline("queryController/executeSql", para,
				// actionEvent, LogDTO.TYPE_NORMAL,
				// String.valueOf(feedBack.feedBackId),
				// FEED_BACK_TABLE.FEED_BACK_TABLE);
				//

				for (int i = 0; i < feedBack.getArrStaffId().size(); i++) {
					JSONArray listSql = new JSONArray();

					feedBack.setStaffId(feedBack.getArrStaffId().get(i));
					JSONObject insertSql = feedBack.generateFeedbackSql();
					listSql.put(insertSql);
					String logId = GlobalUtil.generateLogId();
					actionEvent.logData = logId;
					// send to server
					Vector<Object> para = new Vector<Object>();
					para.add(IntentConstants.INTENT_LIST_SQL);
					para.add(listSql);
					para.add(IntentConstants.INTENT_MD5);
					para.add(StringUtil.md5(listSql.toString()));
					para.add(IntentConstants.INTENT_LOG_ID);
					para.add(logId);

					para.add(IntentConstants.INTENT_IMEI_PARA);
					para.add(GlobalInfo.getInstance().getDeviceIMEI());


					re = sendHttpRequestOffline("queryController/executeSql", para, actionEvent, LogDTO.TYPE_NORMAL,
							String.valueOf(feedBack.getFeedBackId()), FEEDBACK_TABLE.FEEDBACK_TABLE);
				}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			TBHVController.getInstance().handleModelEvent(model);

			try {
				// danh cho GCM nhieu nv
				StringBuffer sb = new StringBuffer();
				for (int i : feedBack.getArrStaffId()) {
					sb.append(i);
					sb.append(";");
				}

				Vector<Object> paraGCM = new Vector<Object>();

//				paraGCM.add(IntentConstants.INTENT_SHOP_ID);
//				paraGCM.add(feedBack.shopId);
//				paraGCM.add(IntentConstants.INTENT_STAFF_ID);
//				paraGCM.add(feedBack.createUserId);

				paraGCM.add(IntentConstants.INTENT_LIST_STAFF);
				paraGCM.add(sb);
				paraGCM.add(StringUtil.getString(R.string.FEEDBACK_TITLE));

				paraGCM.add(IntentConstants.INTENT_CONTENT);
				String loaiVanDe = feedBack.getApParamName();
				String noiDung = feedBack.getContent();
				String guiDi = loaiVanDe + ": " + noiDung;
				paraGCM.add(guiDi);

				String logId = GlobalUtil.generateLogId();
				actionEvent.logData = logId;
//				sendHttpRequest("gcm.broadcastMessage", paraGCM, actionEvent);
				// end send content GCM to server: dungdq
			} catch (Exception ex) {
				ServerLogger.sendLog(ex.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			}
			} else {
				ServerLogger.sendLog("feedback id: " + feedBack.getFeedBackId(), TabletActionLogDTO.LOG_CLIENT);
				model.setIsSendLog(false);

				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		return re;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getGsnppPosition(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;

		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffID = bundle.getString(IntentConstants.INTENT_STAFF_ID, Constants.STR_BLANK);
		TBHVRouteSupervisionDTO dto = SQLUtils.getInstance().getGsnppPosition(shopId, staffID);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getNvbhPosition(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;

		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		GsnppRouteSupervisionDTO dto = SQLUtils.getInstance().getNvbhPosition(staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent getPositionOfGsnppAndNvbh(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		String[] list = bundle.getStringArray(IntentConstants.INTENT_STAFF_LIST);
		ListStaffDTO listStaff = SQLUtils.getInstance().getPositionOfGsnppAndNvbh(list);
		return new ModelEvent(e, listStaff, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds canh bao ghe tham di tuyen
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent getVisitCusNotification(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		TBHVVisitCustomerNotificationDTO dto = SQLUtils.getInstance().getVisitCusNotification(staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay thong tin bao cao cham cong ngay cua NPP
	 *
	 * @param event
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 22, 2013
	 */
	public void getTbhvAttendance(ActionEvent event, int n) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		TBHVAttendanceDTO dto = null;
		try {
			dto = SQLUtils.getInstance().getTBHVAttendance(data);
			if (dto != null) {
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				model.setModelData(dto);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				model.setIsSendLog(false);
				ServerLogger.sendLog("method getReportTakeAttendOfDay error: ", "sql get report take attend of tbhv error", TabletActionLogDTO.LOG_CLIENT);
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setIsSendLog(false);
			ServerLogger.sendLog("method getReportTakeAttendOfDay error: ", ex.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * lay thong tin bao cao cham cong ngay
	 * @author: hoanpd1
	 * @since: 10:42:22 24-02-2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private Object getTbhvAttendance(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getTBHVAttendance(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay du lieu cham cong cac nhan vien cua role quan ly
	 * @author: hoanpd1
	 * @since: 10:54:43 12-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 */
	public Object getListSaleForAttendance(ActionEvent e) {
		Bundle data = (Bundle) e.viewData;
		Object result = SQLUtils.getInstance().getTBHVListAttendance(data);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list report NVBH visit customer
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 21, 2013
	 */
	public ModelEvent getListReportNVBHVisitCustomer(ActionEvent e) throws Exception{
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle b = (Bundle) e.viewData;
		ArrayList<ReportNVBHVisitCustomerDTO> dto = SQLUtils.getInstance().getListReportVisitCustomerInDay(b);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach shop va nhan vien tuong ung
	 * @author: hoanpd1
	 * @since: 19:14:41 12-03-2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private Object getListShopAndStaff(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getListShopAndStaff(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void requestGetListDocument(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle bundle = (Bundle) event.viewData;
		OfficeDocumentListDTO result = null;
		int type = bundle.getInt(IntentConstants.INTENT_DOCUMENT_TYPE);
		String fromDate = bundle.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = bundle.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String ext = bundle.getString(IntentConstants.INTENT_PAGE);
		boolean checkLoadMore = bundle.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		try {
			result = SQLUtils.getInstance().getListDocumentTBHV(type, fromDate, toDate, ext, checkLoadMore, shopId);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * chi tiet cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void requestGetListDocumentDetail(ActionEvent event) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle bundle = (Bundle) event.viewData;
		MediaItemDTO result = null;
		long officeDocumentId = bundle.getLong(IntentConstants.INTENT_OFFICE_DOCUMENT_ID);
		try {
			result = SQLUtils.getInstance().getListDocumentDetail(officeDocumentId);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			result = null;
		}
		if (result != null) {
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(result);
			TBHVController.getInstance().handleModelEvent(model);
		} else {
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * lay thong tin shop
	 * @author: DungNX
	 * @param actionEvent
	 * @return: void
	 * @throws Exception
	 * @throws:
	*/
	public ModelEvent getListShopInfo(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);

		Bundle bundle = (Bundle) actionEvent.viewData;
		String listShopId = bundle.getString(IntentConstants.INTENT_LIST_SHOP_ID);
		ArrayList<ShopDTO> dto = SQLUtils.getInstance().getListShopInfo(listShopId);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Danh sach khach hang cua NVBH trong ngay training cua TBHV
	 * @author: hoanpd1
	 * @since: 16:40:51 18-11-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListCustomerTraingStaff(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			TBHVDayTrainingSupervisionDTO dto = SQLUtils.getInstance()
					.getListCustomerTraingStaff(data);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Lich su huan luyen cua TBHV
	 * @author: hoanpd1
	 * @since: 11:03:14 20-11-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListTrainingHistory(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			ListSubTrainingPlanDTO dto = SQLUtils.getInstance()
					.getListTrainingHistory(data);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * lay danh sach cac tieu chi
	 *
	 * @author: dungdq3
	 * @since: 11:26:39 AM Nov 17, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListCriteria(ActionEvent e) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			ListTrainingRateDTO dto = SQLUtils.getInstance().getListCriteria(data);

			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		} finally {

		}

	}

	/**
	 * lay danh sach cac tieu chuan
	 *
	 * @author: dungdq3
	 * @since: 9:39:50 AM Nov 17, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListStandard(ActionEvent e) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			TrainingRateResultDTO dto = SQLUtils.getInstance()
					.getListStandard(data);

			if (dto != null) {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				TBHVController.getInstance().handleModelEvent(model);
			} else {
				model.setModelData(dto);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				TBHVController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
			TBHVController.getInstance().handleErrorModelEvent(model);
		} finally {

		}
	}

	/**
	 * lay danh sach van de
	 *
	 * @author: dungdq3
	 * @since: 8:54:22 AM Nov 19, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListIssue(ActionEvent e) {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			ListSupTrainingIssueDTO dto = SQLUtils.getInstance()
					.getListIssue(data);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Cap nhat cac van de
	 *
	 * @author: dungdq3
	 * @since: 8:41:44 AM Nov 20, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void updateIssue(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		SupTrainingIssueDTO dto = (SupTrainingIssueDTO) bd
				.getSerializable(IntentConstants.INTENT_TBHV_TRAINING_PLAN_TODAY_ITEM);
		try {
			returnCode = SQLUtils.getInstance().updateIssue(dto);

			// send to server
			if (returnCode != -1) {
				JSONArray jarr = new JSONArray();
				JSONObject jsonUpdateIssue = dto.generateJSONUpdateIssue();
				jarr.put(jsonUpdateIssue);

				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) // tbhv
					sendHttpRequestOffline("queryController/executeSql", para, e);
				else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR)
					sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);

			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * xoa can thuc hien
	 *
	 * @author: dungdq3
	 * @since: 5:07:24 PM Nov 19, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void deleteIssue(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		SupTrainingIssueDTO dto = (SupTrainingIssueDTO) bd
				.getSerializable(IntentConstants.INTENT_TBHV_TRAINING_PLAN_TODAY_ITEM);
		try {
			returnCode = SQLUtils.getInstance().deleteIssue(dto);

			// send to server
			if (returnCode != -1) {
				JSONArray jarr = new JSONArray();
				JSONObject jsonDeleteIssue = dto.generateJSONDeleteIssue();
				jarr.put(jsonDeleteIssue);

				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);

			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * gui kq Huan luyen
	 *
	 * @author: dungdq3
	 * @since: 3:39:20 PM Nov 19, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void sendResultTraining(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle b = (Bundle) e.viewData;
		SupTrainingPlanDTO trainingPlan = (SupTrainingPlanDTO) b.getSerializable(IntentConstants.INTENT_TBHV_TRAINING_PLAN_TODAY_ITEM);
		try {
			returnCode = SQLUtils.getInstance().sendResultTraining(trainingPlan);

			// send to server
			if (returnCode != -1) {
				JSONObject sqlPara = trainingPlan.generateJSONUpdate();
				JSONArray jarr = new JSONArray();
				jarr.put(sqlPara);
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);
			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage("send result training not success. Check your data again");
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage("send result training not success. Check your data again");
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Get lich huan luyen nhan vien cua TBHV
	 * @author: hoanpd1
	 * @since: 14:24:56 17-11-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getStaffTrainingPlan(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			ListSubTrainingPlanDTO dto = SQLUtils.getInstance()
					.getStaffTrainingPlan(data);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * luu feed back
	 *
	 * @author: dungdq3
	 * @since: 8:58:42 AM Nov 14, 2014
	 * @return: void
	 * @throws:
	 */
	public void saveIssue(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		SupTrainingIssueDTO dto = (SupTrainingIssueDTO) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_DTO);
		try {
			returnCode = SQLUtils.getInstance().saveIssue(dto);

			// send to server
			if (returnCode != -1) {
				JSONObject sqlPara = dto.generateInsertIssue();
				JSONArray jarr = new JSONArray();
				jarr.put(sqlPara);
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
//				sendHttpRequestOffline("queryController/executeSql", para, e, LogDTO.STATE_NOT_SEND);
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);

			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Lich su huan luyen cua GSNPP
	 * @author: hoanpd1
	 * @since: 09:58:11 21-11-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void getListTrainingHistoryGSNPP(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		try {
			ListSubTrainingPlanDTO dto = SQLUtils.getInstance()
					.getListTrainingHistoryGSNPP(data);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * luu danh sach tieu chuan
	 *
	 * @author: dungdq3
	 * @since: 4:59:58 PM Nov 17, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void saveListStandard(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		TrainingRateResultDTO dto = (TrainingRateResultDTO) bd
				.getSerializable(IntentConstants.INTENT_LIST_STANDARD);
		try {
			returnCode = SQLUtils.getInstance().saveListStandard(dto);

			// send to server
			if (returnCode != -1) {
				JSONArray jarr = new JSONArray();
				JSONObject sqlTrainingRate = dto.generateTrainingRate();
				jarr.put(sqlTrainingRate);
				for (TrainingRateDetailResultlDTO rateResult : dto.getListStandard()) {
					// nhan vien tao
					if (rateResult.getDefaultRate() == 0) {
						if (!StringUtil.isNullOrEmpty(rateResult.getFullStandardName())) {
							rateResult.generateJSONObject(jarr);
//							if (rateResult.getResult() > 0) {
								JSONObject jsonResult = rateResult.generateJSONObjectResult();
								jarr.put(jsonResult);
//							}
						}
					} else if (rateResult.getDefaultRate() == 1) { // mac dinh cua he thong
						JSONObject jsonResult = rateResult.generateJSONObjectResult();
						jarr.put(jsonResult);
					}
				}

				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
//				sendHttpRequestOffline("queryController/executeSql", para, e, LogDTO.STATE_NOT_SEND);
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);

			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * xoa cac tieu chuan
	 *
	 * @author: dungdq3
	 * @since: 5:05:29 PM Dec 5, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public void deleteListStandard(ActionEvent e) {
		// TODO Auto-generated method stub
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		TrainingRateDetailResultlDTO dto = (TrainingRateDetailResultlDTO) bd
				.getSerializable(IntentConstants.INTENT_LIST_STANDARD);
		try {
			returnCode = SQLUtils.getInstance().deleteListStandard(dto);

			// send to server
			if (returnCode != -1) {
				JSONArray jarr = new JSONArray();
				JSONObject jsonDeleteDetail = dto.generateDelete();
				jarr.put(jsonDeleteDetail);
				JSONObject jsonDeleteResult = dto.generateDeleteResult();
				jarr.put(jsonDeleteResult);

				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
				SaleController.getInstance().handleModelEvent(model);

			} else {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				SaleController.getInstance().handleErrorModelEvent(model);
			}
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			SaleController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 *
	 * get Gsnpp Training Plan
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public void getTbhvTrainingPlan(ActionEvent e) {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		try {
			TBHVTrainingPlanDTO dto = SQLUtils.getInstance().getTbhvTrainingPlan(staffId, shopId);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(dto);
			TBHVController.getInstance().handleModelEvent(model);
		} catch (Exception ex) {
			model.setModelMessage(ex.getMessage());
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			TBHVController.getInstance().handleErrorModelEvent(model);
		}
	}

	/**
	 * Check action log
	 *
	 * @author: dungdq3
	 * @since: 3:35:42 PM Dec 10, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public ModelEvent checkVisitFromActionLog(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		@SuppressWarnings("rawtypes")
		Vector viewInfo = (Vector) e.viewData;

		int staffId = Integer.parseInt(viewInfo.get(
				viewInfo.lastIndexOf(IntentConstants.INTENT_STAFF_ID) + 1)
				.toString());

		ActionLogDTO dto = SQLUtils.getInstance()
				.checkVisitFromActionLogAndPrize(staffId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	@Override
	public Object requestHandleModelData(ActionEvent e) throws Exception {
		Object data = null;
		switch (e.action) {
		case ActionEventConstant.GET_DATA_TBHV_ATTENDANCE:
			data = getTbhvAttendance(e);
			break;
		case ActionEventConstant.TBHV_VISIT_CUS_NOTIFICATION: {
			data = getVisitCusNotification(e);
			break;
		}
		case ActionEventConstant.TBHV_SUPERVISE_GSNPP_POSITION: {
			data = getGsnppPosition(e);
			break;
		}
		case ActionEventConstant.TBHV_NVBH_POSITION: {
			data = getNvbhPosition(e);
			break;
		}
		case ActionEventConstant.GET_SHOP_INFO: {
			data = getListShopInfo(e);
			break;
		}
		case ActionEventConstant.GSNPP_GET_LIST_REPORT_NVBH_VISIT_CUSTOMER:
			data = getListReportNVBHVisitCustomer(e);
			break;
		case ActionEventConstant.TBHV_ROUTE_SUPERVISION:
			data = requestListRouteSupervision(e);
			break;
		case ActionEventConstant.GET_POS_GSNPP_NVBH: {
			data = getPositionOfGsnppAndNvbh(e);
			break;
		}
		case ActionEventConstant.TBHV_GET_LIST_SALE_FOR_ATTENDANCE: {
			data = getListSaleForAttendance(e);
			break;
		}
		case ActionEventConstant.GET_DATA_LIST_SHOP_AND_STAFF:{
			data = getListShopAndStaff(e);
			break;
		}
		case ActionEventConstant.GET_TBHV_LIST_PRODUCT_STORAGE: {
			data = getTBHVListProductStorage(e);
			break;
		}
		case ActionEventConstant.GET_LIST_SHOP_RECURSIVE:
			data = getListShopRecursive(e);
			break;
		case ActionEventConstant.GET_LIST_NVGS_OF_DATE:
			data = getListNVGSOfTBHVReportDate(e);
			break;
		case ActionEventConstant.GET_LIST_NPP: {
			data = getListNPP(e);
			break;
		}
		case ActionEventConstant.GET_TBHV_REPORT_PROGRESS_DATE_DETAIL: {
			data = getTBHVProgressDateDetailReport(e);
			break;
		}
		case ActionEventConstant.GET_TBHV_REPORT_PROGRESS_DATE: {
			data = getTBHVProgressDateReport(e);
			break;
		}
		case ActionEventConstant.GET_REPORT_PROGESS_MONTH_INFO: {
			data = getReportProgressInMonthTBHV(e);
			break;
		}
		case ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG:
			data = checkVisitFromActionLog(e);
			break;
		case ActionEventConstant.GET_REPORT_PROGESS_MONTH_NPP_DETAIL_INFO: {
			data = getReportProgressInMonthTBHVDetailNPP(e);
			break;
		}
		case ActionEventConstant.GET_TBHV_LIST_PROMOTION_PROGRAME:
			data = requestGetListPromotionPrograme(e);
			break;
		case ActionEventConstant.GET_TBHV_LIST_DISPLAY_PROGRAM:
			data = requestGetListDisplayPrograme(e);
			break;
		default:// test
				break;
		}
		return data;
	}

	/**
	 * lay danh sach shop con
	 *
	 * @author: dungdq3
	 * @since: 5:01:11 PM Mar 23, 2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	private ModelEvent getListShopRecursive(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<ShopDTO> dto = SQLUtils.getInstance().requestGetListCombobox(shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

}
