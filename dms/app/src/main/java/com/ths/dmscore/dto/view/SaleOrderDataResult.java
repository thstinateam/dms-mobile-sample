/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 *  Ket qua khi tao moi don hang
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class SaleOrderDataResult {
	public boolean isCreateSqlLiteSuccess = false;
	public long orderId;
	public long poId;
	public ArrayList<Long> listOrderDetailId = new ArrayList<Long>();
	// ds ma chuong trinh KM khong du so xuat
	public ArrayList<String> listPromotionCode;
}
