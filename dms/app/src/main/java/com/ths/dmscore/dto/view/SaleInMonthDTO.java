/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Doanh so cua khach hang trong 1 thang
 * @author : BangHN
 * since   : 1.0
 * version : 1.0
 */
@SuppressWarnings("serial")
public class SaleInMonthDTO implements Serializable{
	public int month;//thang
	public double amount;//doanh so cua thang: month
	public long quantity;//san luon cua thang: month

	public SaleInMonthDTO(){
	}

	/**
	 * init thong tin doanh so trong mot thang sau khi query
	 * @author : BangHN
	 * since : 1.0
	 */
	public void parseSaleInMonth(Cursor c, int sysCurrencyDivide){
		month = CursorUtil.getInt(c, "month");
		amount = CursorUtil.getDouble(c, "amount");
		quantity = CursorUtil.getLong(c, "quantity");
	}
}
