/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.ForcusProductOfNVBHDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

/**
 * display product forcus infor on row
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class NVBHReportForcusProductRow extends DMSTableRow implements
		OnClickListener {
	// stt
	private TextView tvSTT;
	// product code
	private TextView tvProductCode;
	// product name
	private TextView tvProductName;
	// product industry
	private TextView tvProductIndustry;
	// forcus product type
	private TextView tvForcusProductType;
	// plan amount
	private TextView tvAmountPlan;
	// done amount
	private TextView tvAmount;
	// remain amount
	private TextView tvAmountRemain;
	// progress sale
	private TextView tvAmountProgress;
	// plan Quantity
	private TextView tvQuantityPlan;
	// done Quantity
	private TextView tvQuantity;
	// remain Quantity
	private TextView tvQuantityRemain;
	// progress sale
	private TextView tvQuantityProgress;

	public NVBHReportForcusProductRow(Context context, View aRow,
			boolean isTotalRow) {
		super(context, R.layout.layout_nvbh_report_forcus_product_row,
				GlobalInfo.getInstance().isSysShowPrice() ? 1.5 : 1,
				GlobalInfo.getInstance().isSysShowPrice() ? null
						: new int[]{R.id.llAmount});
		setOnClickListener(this);
		this.initViewControlNomalRow();
	}

	/**
	 *
	 * init view control for nomal row (not total row)
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void initViewControlNomalRow() {
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvProductIndustry = (TextView) findViewById(R.id.tvProductIndustry);
		tvForcusProductType = (TextView) findViewById(R.id.tvTypeForcusProduct);
		tvAmountPlan = (TextView) findViewById(R.id.tvAmountPlan);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvAmountRemain = (TextView) findViewById(R.id.tvAmountRemain);
		tvAmountProgress = (TextView) findViewById(R.id.tvAmountProgress);
		tvQuantityPlan = (TextView) findViewById(R.id.tvQuantityPlan);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
		tvQuantityRemain = (TextView) findViewById(R.id.tvQuantityRemain);
		tvQuantityProgress = (TextView) findViewById(R.id.tvQuantityProgress);

	}

	/**
	 *
	 * init view control for total row
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void initViewControlTotalRow(View v) {
		tvForcusProductType = (TextView) v
				.findViewById(R.id.tvTypeForcusProduct);
		tvAmountPlan = (TextView) v.findViewById(R.id.tvPlanAmount);
		tvAmount = (TextView) v.findViewById(R.id.tvAmount);
		tvAmountRemain = (TextView) v.findViewById(R.id.tvAmountRemain);
		tvAmountProgress = (TextView) v.findViewById(R.id.tvAmountProgress);
		tvQuantityPlan = (TextView) findViewById(R.id.tvQuantityPlan);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
		tvQuantityRemain = (TextView) findViewById(R.id.tvQuantityRemain);
		tvQuantityProgress = (TextView) findViewById(R.id.tvQuantityProgress);
	}

	/**
	 *
	 * render layout for row product info
	 *
	 * @param productInfo
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void renderLayoutForRowProductInfo(
			ForcusProductOfNVBHDTO productInfo, int stt, double pecentStandar) {
		tvSTT.setText(String.valueOf(stt));
		tvProductCode.setText(productInfo.productCode);
		tvProductName.setText(productInfo.productName);
//		tvProductIndustry.setText(productInfo.productIndustry);
		tvProductIndustry.setText(productInfo.productIndustryCode);
		tvForcusProductType.setText(productInfo.typeProductFocus);
		Double amountDone = 0.0;
		long quantityDone = 0;
		Double amountRemain = 0.0;
		long quantityRemain = 0;
		if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED){
			amountDone = productInfo.amountApproved;
			quantityDone = productInfo.quantityApproved;
		}else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING){
			amountDone = productInfo.amountPending;
			quantityDone = productInfo.quantityPending;
		}else{
			amountDone = productInfo.amount;
			quantityDone = productInfo.quantity;
		}
		amountRemain = StringUtil.calRemain(productInfo.amountPlan, amountDone);
		quantityRemain = StringUtil.calRemain(productInfo.quantityPlan, quantityDone);
		double amountProgress = StringUtil.calPercentUsingRound(productInfo.amountPlan, amountDone);
		double quantityProgress = StringUtil.calPercentUsingRound(productInfo.quantityPlan, quantityDone);

		display(tvAmountPlan, productInfo.amountPlan);
		display(tvAmount, amountDone);
		display(tvAmountRemain, amountRemain);
		displayPercent(tvAmountProgress, amountProgress);
		display(tvQuantityPlan, productInfo.quantityPlan);
		display(tvQuantity, quantityDone);
		display(tvQuantityRemain, quantityRemain);
		displayPercent(tvQuantityProgress,quantityProgress);
		if (amountProgress < pecentStandar) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvAmountProgress);
		}
		if (quantityProgress < pecentStandar) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvQuantityProgress);
		}
	}

	/**
	 *
	 * render layout for row total
	 *
	 * @param productInfo
	 * @param pecentStandar
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void renderLayoutForRowTotal(ForcusProductOfNVBHDTO productInfo,
			double pecentStandar) {
		showRowSum("", tvSTT,tvProductCode,tvProductName,tvProductIndustry,tvForcusProductType);
		tvForcusProductType.setText(productInfo.typeProductFocus);
		Double amountDone = 0.0;
		long quantityDone = 0;
		Double amountRemain = 0.0;
		long quantityRemain = 0;
		if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED){
			amountDone = productInfo.amountApproved;
			quantityDone = productInfo.quantityApproved;
		}else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING){
			amountDone = productInfo.amountPending;
			quantityDone = productInfo.quantityPending;
		}else{
			amountDone = productInfo.amount;
			quantityDone = productInfo.quantity;
		}
		amountRemain = StringUtil.calRemain(productInfo.amountPlan, amountDone);
		quantityRemain = StringUtil.calRemain(productInfo.quantityPlan, quantityDone);
		double amountProgress = StringUtil.calPercentUsingRound(productInfo.amountPlan, amountDone);
		double quantityProgress = StringUtil.calPercentUsingRound(productInfo.quantityPlan, quantityDone);

		display(tvAmountPlan, productInfo.amountPlan);
		display(tvAmount, amountDone);
		display(tvAmountRemain, amountRemain);
		displayPercent(tvAmountProgress, amountProgress);
		display(tvQuantityPlan, productInfo.quantityPlan);
		display(tvQuantity, quantityDone);
		display(tvQuantityRemain, quantityRemain);
		displayPercent(tvQuantityProgress,quantityProgress);
		if (amountProgress < pecentStandar) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvAmountProgress);
		}
		if (quantityProgress < pecentStandar) {
			setTextColor(ImageUtil.getColor(R.color.RED), tvQuantityProgress);
		}

		setTypeFace(Typeface.BOLD, tvAmountPlan, tvForcusProductType,
				tvAmount, tvAmountRemain, tvAmountProgress,
				tvQuantityPlan,tvQuantity,tvQuantityRemain,tvQuantityProgress);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}
}
