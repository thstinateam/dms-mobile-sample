/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Luu thong tin khach hang
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_ATTRIBUTE_ENUM_TABLE extends ABSTRACT_TABLE {
	public static final String CUSTOMER_ATTRIBUTE_ENUM_ID = "CUSTOMER_ATTRIBUTE_ENUM_ID";
	public static final String CUSTOMER_ATTRIBUTE_ID = "CUSTOMER_ATTRIBUTE_ID";
	public static final String CODE = "CODE";
	public static final String VALUE = "VALUE";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "CUSTOMER_ATTRIBUTE_ENUM";

	public CUSTOMER_ATTRIBUTE_ENUM_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { CUSTOMER_ATTRIBUTE_ENUM_ID,
				CUSTOMER_ATTRIBUTE_ID, CODE, VALUE, STATUS, CREATE_DATE,
				CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}