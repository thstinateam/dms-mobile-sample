/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.dto.view.NvbhReportAsoDTO;
import com.ths.dms.R;

public class NVBHReportASORow extends DMSTableRow implements
		OnClickListener {
	// stt
	private TextView tvSTT;
	
//	private VinamilkTableListener listenerTable;
	private NvbhReportAsoDTO dto;

	private TextView tvCode;
	private TextView tvName;
	private TextView tvTypeName;
	private TextView tvPlan;
	private TextView tvDone;
	private TextView tvRemain;
	private TextView tvPercent;

	public NVBHReportASORow(Context context, VinamilkTableListener listenerTable) {
		super(context, R.layout.layout_nvbh_report_aso_row, 1.0, null);
//		this.listenerTable = listenerTable;
		this.setOnClickListener(this);
		this.initViewControlNomalRow();
	}

	public void initViewControlNomalRow() {
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvCode = (TextView) findViewById(R.id.tvCode);
		tvName = (TextView) findViewById(R.id.tvName);
		tvTypeName = (TextView) findViewById(R.id.tvTypeName);
		tvPlan = (TextView) findViewById(R.id.tvPlan);
		tvDone = (TextView) findViewById(R.id.tvDone);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
		tvPercent = (TextView) findViewById(R.id.tvPercent);
	}

	public void renderLayout(NvbhReportAsoDTO dto2, int stt) {	
		this.dto = dto2;
		display(tvSTT,stt);
		display(tvCode, this.dto.code);
		display(tvName, this.dto.name);

		display(tvTypeName, this.dto.typeName);
		display(tvPlan, this.dto.plan);
		display(tvDone, this.dto.done);
		display(tvRemain, this.dto.remain);
		displayPercent(tvPercent, this.dto.percent);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
	}
}
