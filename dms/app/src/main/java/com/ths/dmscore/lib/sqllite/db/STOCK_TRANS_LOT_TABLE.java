package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.CursorUtil;

public class STOCK_TRANS_LOT_TABLE extends ABSTRACT_TABLE {

	public static final String STOCK_TRANS_LOT_ID = "STOCK_TRANS_LOT_ID";
	public static final String STOCK_TRANS_ID = "STOCK_TRANS_ID";
	public static final String STOCK_TRANS_DETAIL_ID = "STOCK_TRANS_DETAIL_ID";
	public static final String STOCK_TRANS_DATE = "STOCK_TRANS_DATE";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String LOT = "LOT";
	public static final String QUANTITY = "QUANTITY";
	public static final String FROM_OWNER_ID = "FROM_OWNER_ID";
	public static final String TO_OWNER_ID = "TO_OWNER_ID";
	public static final String FROM_OWNER_TYPE = "FROM_OWNER_TYPE";
	public static final String TO_OWNER_TYPE = "TO_OWNER_TYPE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String TABLE_NAME = "STOCK_TRANS_LOT";

	public STOCK_TRANS_LOT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { STOCK_TRANS_LOT_ID, STOCK_TRANS_ID,
				STOCK_TRANS_DETAIL_ID, STOCK_TRANS_DATE, PRODUCT_ID, LOT,
				QUANTITY, FROM_OWNER_ID, TO_OWNER_ID, FROM_OWNER_TYPE,
				TO_OWNER_TYPE, CREATE_USER, CREATE_DATE, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getStockTransDate(Bundle data) throws Exception{
		String stockTransDate = new String();

//		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String createDate = DateUtils.formatNow(DateUtils.DATE_FORMAT_DATE);

		StringBuilder sqlRequest = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sqlRequest.append("SELECT STOCK_TRANS_DATE ");
		sqlRequest.append(" from STOCK_TRANS_LOT ");
		sqlRequest.append(" where ");
		sqlRequest.append("		TO_OWNER_ID = ? ");
		params.add(staffId);
//		sqlRequest.append("		and substr(STOCK_TRANS_DATE, 1, 10) = ? limit 1 ");
//		params.add(createDate);
		sqlRequest.append("		limit 1 ");

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					stockTransDate = CursorUtil
							.getString(c, "STOCK_TRANS_DATE");
				}
			}
		}  finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return stockTransDate;
	}
}