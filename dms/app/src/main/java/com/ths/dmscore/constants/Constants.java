/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;

import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 *  Define hang so trong chuong trinh
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class Constants {
	public static final String EXPIRED_TIMESTAMP = "Expired timestamp";
	public static final String APACHE_TOMCAT = "Apache Tomcat";
//	public static final String VNM_VIETTEL_MAP_KEY_RELEASE = "009303cf4ed1c88318b3fd2cf81041e2";
	public static final String VNM_VIETTEL_MAP_KEY_RELEASE = "cb4a9bfdbcc3af10683a2590148e0c93";
	public static final String VNM_VIETTEL_MAP_KEY_TEST = "TTUDCNTT_KEY_TEST";
	public static final String SENDER_ID = "776014794525";

	//kich thuoc cua hinh anh dinh kem
	//kich thuoc anh toi da upload
	public static final int MAX_FULL_IMAGE_WIDTH = 600;
	public static final int MAX_FULL_IMAGE_HEIGHT = 600;
	//kich thuoc anh toi da upload
	public static final int MAX_THUMB_IMAGE_WIDTH = 250;
	public static final int MAX_THUMB_IMAGE_HEIGHT = 200;

	public static final int MAX_UPLOAD_IMAGE_WIDTH = 740;
	public static final int MAX_UPLOAD_IMAGE_HEIGHT = 740;
	public static final int MAX_LENGHT_QUANTITY = 7;
	public static final int MAX_LENGHT_PRICE = 20;
	public static final int MAX_LENGTH_TRANSACTION_ID = 14;
	public static final int MAX_LENGTH_RANDOM_ID = 18;
	// kich thuoc left menu
	public static final int LEFT_MARGIN_TABLE_DIP = 10;
	public static final int RIGHT_MARGIN_TABLE_DIP = 10;
	public static final int LEFT_MARGIN_TABLE_DIP_SMALL = 5;
	public static final String HTTPCONNECTION_POST = "POST";
	public static final String STR_BLANK = "";
	public static final String STR_SPACE = " ";
	public static final String STR_SUBTRACTION = " - ";
	public static final String STR_CROSS = " / ";
	public static final String LOG_LBS = "LOG_LBS";
	public static final String ACTION_BROADCAST = "vinamilk.action";
	public static final String HASHCODE_BROADCAST = "vinamilk.hashCode";
	public static final String REPLACED_STRING = "xxx";
	public static final int COLUMN = 2;
	public static final int ROW = 1;
	public static final int IN_USE = 1;
	public static final int NO_USE = 0;
	// chuoi dung de ghep vao ds doi tuong bao cao lay ra
	public static final String groupString = "___";
	public static final String STR_BRACKET_LEFT  = " (";
	public static final String STR_BRACKET_RIGHT  = ") ";
	public static final String STR_TOKEN  = ", ";

//	// tao d/s title colum cua level
//		public static final String[] LEVEL_CODE_ARRAY = new String[] { "A", "B", "C", "D", "E", "F",
//						"G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "W",
//						"V", "Y", "Z" };

	public static final int NUM_ITEM_PER_PAGE = 10;
	private static final String DAY_LINE[]={StringUtil.getString(R.string.TEXT_MONDAY_),
		StringUtil.getString(R.string.TEXT_TUESDAY_),
		StringUtil.getString(R.string.TEXT_WEDNESDAY_),
		StringUtil.getString(R.string.TEXT_THUSDAY_),
		StringUtil.getString(R.string.TEXT_FRIDAY_),
		StringUtil.getString(R.string.TEXT_SATURDAY_),
		StringUtil.getString(R.string.TEXT_SUNDAY_)};
	private static final String TODAY[]={"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
	public static final String DATABASE_NAME = "DMSCoreDatabase";
	public static final int MAX_LENGTH_CUSTOMER_NAME= 60;
	private static final String ARRAY_LINE_CHOOSE[] = {
		StringUtil.getString(R.string.TEXT_MONDAY),
		StringUtil.getString(R.string.TEXT_TUESDAY),
		StringUtil.getString(R.string.TEXT_WEDNESDAY),
		StringUtil.getString(R.string.TEXT_THURSDAY),
		StringUtil.getString(R.string.TEXT_FRIDAY),
		StringUtil.getString(R.string.TEXT_SATURDAY),
		StringUtil.getString(R.string.TEXT_SUNDAY),
		StringUtil.getString(R.string.TEXT_ALL) };
	public static final String TEMP_IMG = "temp_image";
	public static final String TEMP_IMG_ATTACH = "attach_image";
	public static final String TEMP_SYNDATA_FILE = "temp_syndata";
	public static final String HAS_ERROR_HAPPEN = StringUtil.getString(R.string.TEXT_HAS_ERROR_HAPPEN);

	// toa do lat, lng de show ban do toan viet nam
	public static final double LAT_VNM_TOWNER = 10.729793;
	public static final double LNG_VNM_TOWNER = 106.724388;
	public static final double DISTANCE_SELECT_CUSTOMER = 300;

	public static final String CIPHER_KEY = "dms@mobile@viettel";

	public static final int CONNECTION_ONLINE  = 1;
	public static final int CONNECTION_OFFLINE  = -1;
	public static final String MESSAGE_ERROR_COMMON = StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON);

	// ten file db ma hoa
	public static final String DATABASE_NAME_CIPHER = "PlaintDMSCoreDatabase";
	public static final long CF_MAP_GOOGLE = 2;
//	public static final double LAT_LNG_NULL = Double.MAX_VALUE;
	public static final double LAT_LNG_NULL = -1;
	public static final int RETURN_DATE_SYS_DATE = 1;
	public static final int RETURN_DATE_ORDER_DATE = 2;
	public static final int RETURN_DATE_APPROVED_DATE = 3;
	public static final int NUM_LOAD_MORE_IMAGE = 10;
	public static final int NUM_LOAD_MORE = 20;
	public static final int TYPE_REGISTER_CTHTTM = 1;// mac dinh duoc dang ky keyshop
	public static final int TYPE_ALLOW_DISTANCE_ORDER = 1;// mac dinh duoc dang ky keyshop
	public static final int DAY_OF_CYCLE = 28;// so ngay cua 1 chu ky
	// dinh nghia cac trang thai cua status cho tat ca cac bang
	public static final int STATUS_DELETE = -1; // xoa
	public static final int STATUS_PENDING = 0;// tam ngung
	public static final int STATUS_APPROVED = 1;// duyet
	public static final int STATUS_UNAPPROVED = 2;// chua duyet
	public static final int STATUS_DENY = 3;// tu choi
	public static final int STATUS_CANCEL = 4;// huy
	public static final int STATUS_UNARCHIVE = 5;// ko dat
	public static final int STATUS_ARCHIVE = 6;// dat
	
	public static final int CHANGE_QUANTITY_2_RETAIL = 0;// 9 -> 0/9
	public static final int CHANGE_QUANTITY_2_PACKAGE = 1;// 9 -> 9/0
	public static final int CHANGE_QUANTITY_NO_CHANGE = 2;// giu nguyen khong doi 100, 1/29, /3, 3/
//	public static final int CHANGE_QUANTITY_AUTO_CONVERT = 3;// 101 confact 50 -> 2/1
	
	public static final int NEED_VALIDATE_CLICK = 1;
	public static final int NO_NEED_VALIDATE_CLICK = 0;
	public static final int TIME_CHECK_DOUBLE_CLICK = 1000;
	
	public static String[] getDayLine() {
		return DAY_LINE;
	}
	public static String[] getToday() {
		return TODAY;
	}
	public static String[] getArrayLineChoose() {
		return ARRAY_LINE_CHOOSE;
	}
}
