package com.ths.dmscore.dto.syndata;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SynDataDTO {
	public final static String UPDATE_TO_DATE = "UPDATE_TO_DATE";
	public final static String CONTINUE = "CONTINUE";
	public final static String RESET = "RESET";
	public final static String EXECUTE_DAILY_FIRST_SYN_SCRIPT = "EXECUTE_DAILY_FIRST_SYN_SCRIPT";
	//dong bo bang cach parse  json
	public final static int TYPE_JSON = 1;
	//dong bo bang cach download file
	public final static int TYPE_FILE = 2;
	
	@JsonProperty("state")
	private String state;
	
	@JsonProperty("lastLogId_update")
	private long lastLogId_update;//lastLogId update
	@JsonProperty("maxDBLogId")
	private long maxDBLogId;
	
	@JsonProperty("dbScript")
	private String dbScript;
	
	//@JsonProperty("serverDate")
	//private String serverDate;//thoi gian server tra ve
	
	@JsonProperty("rowData")
	private List<SynDataTableDTO> rowData = new ArrayList<SynDataTableDTO>();

	public SynDataDTO() {
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public long getLastLogId_update() {
		return lastLogId_update;
	}

	public void setLastLogId_update(long lastLogId_update) {
		this.lastLogId_update = lastLogId_update;
	}
	
	

	public long getMaxDBLogId() {
		return maxDBLogId;
	}

	public void setMaxDBLogId(long maxDBLogId) {
		this.maxDBLogId = maxDBLogId;
	}

	public List<SynDataTableDTO> getRowData() {
		return rowData;
	}

	public void setRowData(List<SynDataTableDTO> rowData) {
		this.rowData = rowData;
	}

	public String getDbScript() {
		return dbScript;
	}

	public void setDbScript(String dbScript) {
		this.dbScript = dbScript;
	}

//	public String getServerDate() {
//		return serverDate;
//	}
//
//	public void setServerDate(String serverDate) {
//		this.serverDate = serverDate;
//	}
}
