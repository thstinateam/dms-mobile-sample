package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.StaffDTO;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_POSITION_LOG_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.dto.db.CustomerPositionLogDTO;

/**
 * DTO thong tin item mot lan cap nhat vi tri khach hang
 * @author banghn
 * @since 1.1
 */
public class CustomerUpdateLocationDTO implements Serializable{
	private static final long serialVersionUID = -9218223442728493022L;

	//thong tin lich su
	public CustomerPositionLogDTO positionLog;
	
	//thong tin nhan vien cap nhat
	public StaffDTO staffSale;
	
	public CustomerUpdateLocationDTO(){
		positionLog = new CustomerPositionLogDTO();
		staffSale = new StaffDTO();
	}

	
	/**
	 * parse du lieu sau khi truy van tu DB
	 * @author banghn
	 * @param c
	 */
	public void initFromCursor(Cursor c) throws Exception {
		if (c == null) {
			throw new Exception("Cursor is empty");
		}
		this.staffSale.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
		this.staffSale.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		this.staffSale.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);

		this.positionLog.customerId = CursorUtil.getLong(c, CUSTOMER_POSITION_LOG_TABLE.CUSTOMER_ID);
		this.positionLog.setLat(CursorUtil.getDouble(c, "LAT"));
		this.positionLog.setLng(CursorUtil.getDouble(c, "LNG"));

		this.positionLog.createDate = CursorUtil.getString(c, CUSTOMER_POSITION_LOG_TABLE.CREATE_DATE);
	}
}
