/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.view.NoteInfoDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * thong tin mot dong thong bao nam trong man hinh thong ke chung
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class FeedBackInfoInGeneralStatisticsRow extends RelativeLayout
		implements OnClickListener {

	// content
	Context context;
	// check box feed back status
	public CheckBox cbFBStatus;
	// display customer name
	public TextView tvCustomerName;
	// customer code
	public TextView tvCustomerCode;
	// feed back type
	public TextView tvFBType;
	// feed back info
	public TextView tvFBInfo;
	// feed back dayInOrder
	public TextView tvFBDate;
	BaseFragment bfParentListener;

	public FeedBackInfoInGeneralStatisticsRow(Context context,
			AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	public FeedBackInfoInGeneralStatisticsRow(Context context) {
		super(context);
		initView(context);
	}

	public FeedBackInfoInGeneralStatisticsRow(Context context,
			AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context);
	}

	/**
	 * 
	 * init view for row
	 * 
	 * @author: HaiTC3
	 * @param context
	 * @return: void
	 * @throws:
	 */
	private void initView(Context context) {
		// TODO Auto-generated method stub
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(
				R.layout.layout_feed_back_general_statistics_row, this);
		cbFBStatus = (CheckBox) row.findViewById(R.id.cbFBStatus);
		cbFBStatus.setOnClickListener(this);
		tvCustomerName = (TextView) row.findViewById(R.id.tvCustomerName);
		tvCustomerCode = (TextView) row.findViewById(R.id.tvCustomerCode);
		tvFBType = (TextView) row.findViewById(R.id.tvFBType);
		tvFBInfo = (TextView) row.findViewById(R.id.tvFBInfo);
		tvFBDate = (TextView) row.findViewById(R.id.tvFBDate);
	}

	/**
	 * 
	 * set parent listener
	 * 
	 * @author: HaiTC3
	 * @param lis
	 * @return: void
	 * @throws:
	 */
	public void setListener(BaseFragment lis) {
		this.bfParentListener = lis;
	}

	/**
	 * 
	 * update layout for row
	 * 
	 * @author: HaiTC3
	 * @param noteInfo
	 * @return: void
	 * @throws:
	 */
	public void updateLayout(NoteInfoDTO noteInfo) {
		tvFBInfo.setText(noteInfo.feedBack.getContent());
		
		tvFBDate.setText(noteInfo.feedBack.getCreateDate());
		tvCustomerName.setText(noteInfo.customerDTO.getCustomerName());
		if (noteInfo.customerDTO.getCustomerCode() != null) {
			tvCustomerCode.setText(noteInfo.customerDTO.getCustomerCode());
		} else {
			tvCustomerCode.setText("");
		}

		tvFBType.setText(noteInfo.feedBack.getTypeTitle(noteInfo.feedBack.getFeedbackType()));
		if (noteInfo.feedBack.getStatus() == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE
				&& !StringUtil.isNullOrEmpty(noteInfo.feedBack.getDoneDate())) {
			cbFBStatus.setChecked(true);
			cbFBStatus.setEnabled(false);
		} else {
			cbFBStatus.setChecked(false);
			cbFBStatus.setEnabled(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == cbFBStatus && bfParentListener != null) {
			this.bfParentListener.onClick(v);
		}
	}
}
