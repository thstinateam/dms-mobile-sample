/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.app.Activity;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.ths.dms.R;


/**
 * KeyboardPopupWindow.java
 * @author: duongdt3
 * @version: 1.0
 * @since:  15:29:58 10 Nov 2014
 */
public class KeyboardPopupWindow extends PopupWindow implements OnClickListener {

	Window currentWindow = null;
	Button buttonDot;
	public KeyboardPopupWindow(Activity ct) {
		super(ct);
		LayoutInflater layoutInflater = (LayoutInflater) ct.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);
		final View popupView = layoutInflater.inflate(R.layout.custom_keyboard, null);
		this.setContentView(popupView);
		this.setWidth(LayoutParams.WRAP_CONTENT);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		
		buttonDot = (Button)popupView.findViewById(R.id.button_dot);
		
		//ImageView btnDrag = (ImageView) popupView.findViewById(R.id.icon_drag);
		popupView.setOnTouchListener(new OnTouchListener() {
			int orgX, orgY;
			int offsetX, offsetY;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					orgX = (int) event.getX();
					orgY = (int) event.getY();
					break;
				case MotionEvent.ACTION_MOVE:
					offsetX = (int) event.getRawX() - orgX;
					offsetY = (int) event.getRawY() - orgY;
					KeyboardPopupWindow.this.update(offsetX, offsetY, -1, -1, true);
					break;
				case MotionEvent.ACTION_UP:
					v.performClick();
				}
				return true;
			}
		});

		//action number key
		initButtons(popupView, 
				R.id.button_0,
				R.id.button_1,
				R.id.button_2,
				R.id.button_3,
				R.id.button_4,
				R.id.button_5,
				R.id.button_6,
				R.id.button_7,
				R.id.button_8,
				R.id.button_9,
				R.id.button_000,
				R.id.button_dot,
				R.id.button_ok,
				R.id.button_del,
				R.id.button_ac,
				R.id.button_next,
				R.id.button_back,
				R.id.button_splash
				);
	}

	void initButtons(View popupView, int ... ids){
		for (int i = 0, size = ids.length; i < size; i++) {
			int id = ids[i];
			Button button = (Button)popupView.findViewById(id);
			button.setOnClickListener(this);
		}
	}
	
	void proccessKey(Button vKey){
		View focusCurrent = currentWindow.getCurrentFocus();
		boolean isEditable = (focusCurrent != null && (focusCurrent instanceof EditText));
		EditText edittext = null;
		Editable editable = null;
		//neu co the edit duoc thi tien hanh xu ly
		if (isEditable) {
			edittext = (EditText) focusCurrent;
			editable = edittext.getText();
			//check View ennable status -> is Edit 
			isEditable = edittext.isEnabled(); 
			//int start = edittext.getSelectionStart();
			//int end = edittext.getSelectionEnd();
		}

		switch (vKey.getId()) {
		case R.id.button_ok:
			hideKeyboard();
			break;
		case R.id.button_del:
			if(editable != null && isEditable){
				int length = editable.length();
				if (length > 0) {
					editable.delete(length - 1, length);
				}
			}
			break;
		case R.id.button_ac:
			if(editable != null && isEditable){
				editable.clear();
			}
			break;
		case R.id.button_next:
			if(edittext != null){
				View focusNew = edittext.focusSearch(View.FOCUS_FORWARD);
				if (focusNew != null && focusNew instanceof EditText && focusNew.isEnabled()) {
					focusNew.requestFocus();
				}
			}
			break;
		case R.id.button_back:
			if(edittext != null){
				View focusNew = edittext.focusSearch(View.FOCUS_BACKWARD);
				if (focusNew != null && focusNew instanceof EditText && focusNew.isEnabled()) {
					focusNew.requestFocus();
				}
			}
			break;

		default:
			//cac ky tu khac thi xu ly theo text
			if (editable != null && isEditable) {
				String keyText = vKey.getText().toString();
				//remove text tren EditText neu chi co so 0 + dang nhap them so
				if("0".equals(editable.toString())){
					if (keyText != null && keyText.length() == 1) {
						char ch = keyText.charAt(0);
						boolean isKeyNumber = (ch >= '0' && ch <= '9');
						if (isKeyNumber) {
							//thay so 0 = so moi nhap
							editable.replace(0, 1, keyText);
						} else {
							int length = editable.length();
							editable.insert(length, keyText);
						}
					}
				} else {
					int length = editable.length();
					editable.insert(length, keyText);
				}
			}
			break;
		}
	}

	/**
	 * @author: duongdt3
	 * @since: 15:54:13 10 Nov 2014
	 * @return: void
	 * @throws:
	 */
	private void hideKeyboard() {
		this.dismiss();
	}

	@Override
	public void onClick(View v) {
		if (v != null && v instanceof Button) {
			if (currentWindow != null) {
				Button vKey = (Button) v;
				proccessKey(vKey);
			}
		}
	}

//	public void show(View parent, int x, int y) {
//		this.showAtLocation(parent, Gravity.LEFT | Gravity.TOP, x, y);
//	}


	public void show(Window win, View parent, int x, int y) {
		this.currentWindow = win;
		this.showAtLocation(parent, Gravity.LEFT | Gravity.TOP, x, y);
	}

	/**
	 * 
	 * @author: duongdt3
	 * @since: 11:31:46 13 Apr 2015
	 * @return: void
	 * @throws:  
	 * @param numberSeparator
	 */
	public void updateDecimalPointChar(String numberSeparator) {
		buttonDot.setText(numberSeparator);
	}

}
