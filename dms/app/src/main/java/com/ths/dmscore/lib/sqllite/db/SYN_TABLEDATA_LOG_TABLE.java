package com.ths.dmscore.lib.sqllite.db;

import java.util.Date;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SynTabledataLogDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;

public class SYN_TABLEDATA_LOG_TABLE extends ABSTRACT_TABLE{
	public final String ID = "ID";
	public final String STAFF_ID = "STAFF_ID";
	public final String LAST_SYNDATE = "LAST_SYNDATE";
	public final String LAST_STATUS = "LAST_STATUS";

	private final String TABLE_NAME = "SYN_TABLEDATA_LOG";

	public SYN_TABLEDATA_LOG_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns =  new String[] { ID, STAFF_ID, LAST_SYNDATE,
				LAST_STATUS, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SynTabledataLogDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(SynTabledataLogDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}
	
	/**
	 * Update 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		SynTabledataLogDTO disDTO = (SynTabledataLogDTO)dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.getID() };
		return update(value, ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: TruongHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(ID + " = ?", params);
	}
	
	public long delete(AbstractTableDTO dto) {
		SynTabledataLogDTO cusDTO = (SynTabledataLogDTO)dto;
		String[] params = { String.valueOf(cusDTO.getID())};
		return delete(ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public SynTabledataLogDTO getRowById(String id) {
		SynTabledataLogDTO dto = null;
		Cursor c = null;
		try {
			String[]params = {id};
			c = query(
					ID + " = ?" , params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initObjectFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	public SynTabledataLogDTO getObject(Integer staffId, Date lastSynDate)
			throws Exception {
		SynTabledataLogDTO object = null;
		Cursor c = null;

		try {
			String[] params = { staffId.toString(),
					StringUtil.dateToString(lastSynDate, "") };

			c = query(this.STAFF_ID + " = ? and " + this.LAST_SYNDATE + " = ? ",
					params, null, null, null);

			if (c != null) {
				if (c.moveToFirst()) {
					object = this.initObjectFromCursor(c);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception ex) {

				}
			}
		}

		return object;
	}

	public SynTabledataLogDTO getLastSynLog(Integer staffId) throws Exception {
		SynTabledataLogDTO object = null;
		Cursor c = null;

		try {
			String[] params = { staffId.toString(), staffId.toString() };
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append(" SELECT * FROM SYN_TABLEDATA_LOG ");
			sqlQuery.append(" WHERE STAFF_ID = ? ");
			sqlQuery.append(" AND LAST_SYNDATE = (SELECT MAX(DATE(LAST_SYNDATE)) FROM SYN_TABLEDATA_LOG WHERE STAFF_ID = ?);");

			c = rawQuery(sqlQuery.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					object = this.initObjectFromCursor(c);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception ex) {

				}
			}
		}

		return object;
	}

	private SynTabledataLogDTO initObjectFromCursor(Cursor c) {
		SynTabledataLogDTO object = new SynTabledataLogDTO();

		try {
			object.setID(CursorUtil.getInt(c, this.ID));
			object.setLAST_STATUS(CursorUtil.getInt(c, this.LAST_STATUS));
			object.setSTAFF_ID(CursorUtil.getInt(c, this.STAFF_ID));
			object.setLAST_SYNDATE(StringUtil.stringToDate(CursorUtil.getString(c, this.LAST_SYNDATE), "yyyy-MM-dd"));

			return object;
		} catch (Exception ex) {
			MyLog.e("initObjectFromCursor(Cursor c)", ex.getMessage());
			return null;
		}
	}

	public Vector<SynTabledataLogDTO> getAllRow() throws Exception {
		Vector<SynTabledataLogDTO> v = new Vector<SynTabledataLogDTO>();
		Cursor c = null;

		try {
			c = query(null, null, null, null, null);

			if (c != null) {
				SynTabledataLogDTO object;

				if (c.moveToFirst()) {
					do {
						object = this.initObjectFromCursor(c);
						v.addElement(object);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception ex) {

				}
			}
		}

		return v;
	}

	private ContentValues initDataRow(SynTabledataLogDTO dto) {
		ContentValues editedValues = new ContentValues();

		Date lastSynDate = dto.getLAST_SYNDATE();
		String strLastSynDate = "";

		if (lastSynDate != null) {
			try {
				strLastSynDate = StringUtil.dateToString(lastSynDate,
						"yyyy-MM-dd");
			} catch (Exception ex) {

			}
		}

		// editedValues.put(this.ID, dto.getID());
		editedValues.put(this.LAST_STATUS, dto.getLAST_STATUS());
		editedValues.put(this.LAST_SYNDATE, strLastSynDate);
		editedValues.put(this.STAFF_ID, dto.getSTAFF_ID());

		return editedValues;
	}
}
