package com.ths.dmscore.view.sale.customer;

/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerProgrameDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;

/**
 * Row chuong trinh trung bay
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerProgrameDisplayRow extends TableRow {
	public static final int ACTION_VIEW_PRODUCT = 1;
	public static final int ACTION_VIEW_PROMOTION = 2;
	public static final int ACTION_DELETE = 3;

	Context mContext;
	View view;
	TextView tvCode;// ma ct
	TextView tvName;// ten ct
	TextView tvDepart;// nganh hang
	TextView tvLevel;// muc thamg gia
	TextView tvQuota;// chi tieu
	TextView tvRemain;// ds con lai

	protected OnEventControlListener listener;
	// linear ds san pham
	LinearLayout llProducts;

	public CustomerProgrameDisplayRow(Context context, View aRow) {
		super(context);
		mContext = context;
		LayoutInflater vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = vi.inflate(R.layout.customer_programe_display_row, this);

		tvCode = (TextView) view.findViewById(R.id.tvCode);
		tvName = (TextView) view.findViewById(R.id.tvName);
		tvLevel = (TextView) view.findViewById(R.id.tvLevel);
		tvDepart = (TextView) view.findViewById(R.id.tvDepart);
		tvQuota = (TextView) view.findViewById(R.id.tvQuota);
		tvRemain = (TextView) view.findViewById(R.id.tvRemain);

	}

	/**
	 * Cap nhat data
	 * 
	 * @author: BangHN
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	public void updateData(CustomerProgrameDTO dto) {
		tvCode.setText(dto.displayProgrameCode);
		tvName.setText(dto.displayProgrameName);
		tvLevel.setText(dto.levelCode);
		tvDepart.setText(dto.cat);
//		tvQuota.setText(StringUtil.parseAmountMoney(dto.amount));
		tvQuota.setText(StringUtil.parseAmountMoney(dto.amountPlan));
		tvRemain.setText(StringUtil.parseAmountMoney(dto.amountRemain));
		// if ((dto.amount - dto.amountPlan) > 0) {
		// tvRemain.setText(StringUtil.parseAmountMoney(dto.amount -
		// dto.amountPlan));
		// } else {
		// tvRemain.setText("0");
		// }
	}
}
