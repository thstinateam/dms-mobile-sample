/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PromotionStaffMapDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.PromotionCustomerMapDTO;

/**
 *  Thong tin khuyen mai cua promotion customer
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 2.1
 */
public class PROMOTION_STAFF_MAP extends ABSTRACT_TABLE {
	// id promotion map
	public static final String PROMOTION_STAFF_MAP_ID = "PROMOTION_CUSTOMER_MAP_ID";
	// id shop max
	public static final String PROMOTION_SHOP_MAP_ID = "PROMOTION_SHOP_MAP_ID";
	// id shop
	public static final String SHOP_ID = "SHOP_ID";
	//customer id
	public static final String STAFF_ID = "STAFF_ID";
	// so luong toi da
	public static final String QUANTITY_MAX = "QUANTITY_MAX";
	// so luong thuc nhan
	public static final String QUANTITY_RECEIVED = "QUANTITY_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet)
	public static final String QUANTITY_RECEIVED_TOTAL = "QUANTITY_RECEIVED_TOTAL";
	// ?
	public static final String STATUS = "STATUS";
	//type
	public static final String TYPE = "TYPE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// so luong toi da
	public static final String AMOUNT_MAX = "AMOUNT_MAX";
	// so luong thuc nhan
	public static final String AMOUNT_RECEIVED = "AMOUNT_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet)
	public static final String AMOUNT_RECEIVED_TOTAL = "AMOUNT_RECEIVED_TOTAL";
	// so luong toi da
	public static final String NUM_MAX = "NUM_MAX";
	// so luong thuc nhan
	public static final String NUM_RECEIVED = "NUM_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet) 
	public static final String NUM_RECEIVED_TOTAL = "NUM_RECEIVED_TOTAL";

	public static final String TABLE_PROMOTION_CUSTOMER_MAP = "PROMOTION_CUSTOMER_MAP";

	public PROMOTION_STAFF_MAP(SQLiteDatabase mDB) {
		this.tableName = TABLE_PROMOTION_CUSTOMER_MAP;
		this.columns = new String[] { PROMOTION_STAFF_MAP_ID,
				PROMOTION_SHOP_MAP_ID, SHOP_ID, STAFF_ID, QUANTITY_MAX, QUANTITY_RECEIVED, STATUS,
				QUANTITY_RECEIVED_TOTAL, NUM_RECEIVED_TOTAL, AMOUNT_RECEIVED_TOTAL,
				TYPE, CREATE_USER, UPDATE_USER, CREATE_DATE, CREATE_USER, UPDATE_USER};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PromotionStaffMapDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PromotionStaffMapDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PromotionStaffMapDTO disDTO = (PromotionStaffMapDTO)dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.promotionStaffMapId };
		return update(value, PROMOTION_STAFF_MAP_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: BangHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(PROMOTION_STAFF_MAP_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PromotionStaffMapDTO proDetail = (PromotionStaffMapDTO)dto;
		String[] params = { "" + proDetail.promotionStaffMapId };
		return delete(PROMOTION_STAFF_MAP_ID + " = ?", params);
	}



	/**
	* intit du lieu update vao bang
	* @author: BangHN
	* @return
	* @return: ContentValues
	*/
	private ContentValues initDataRow(PromotionStaffMapDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PROMOTION_STAFF_MAP_ID, dto.promotionStaffMapId);
		editedValues.put(PROMOTION_SHOP_MAP_ID, dto.promotionStaffMapId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(QUANTITY_MAX, dto.quantityMax);
		editedValues.put(QUANTITY_RECEIVED, dto.quantityReceived);
		editedValues.put(STATUS, dto.status);

		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CREATE_USER, dto.createUser);

		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);

		return editedValues;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param amountReceived
	 * @param numReceived
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public boolean increaseQuantityRecevie(long promotionCustomerMapId, int quantityReceived, int numReceived, double amountReceived) {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_CUSTOMER_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN QUANTITY_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN QUANTITY_RECEIVED + ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", NUM_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN NUM_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN NUM_RECEIVED + ");
		sqlObject.append(numReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(numReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", AMOUNT_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN AMOUNT_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN  AMOUNT_RECEIVED + ");
		sqlObject.append(amountReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(amountReceived);
		sqlObject.append(" END) ");
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_CUSTOMER_MAP_ID = ");
		sqlObject.append(promotionCustomerMapId);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public boolean decreaseQuantityRecevie(long promotionCustomerMapId, int num) {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_CUSTOMER_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED = QUANTITY_RECEIVED - ");
		sqlObject.append(num);
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_CUSTOMER_MAP_ID = ");
		sqlObject.append(promotionCustomerMapId);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param customerId
	 * @param shopId
	 * @param promotionProgramShopMapID
	 * @return
	 * @return: PromotionCustomerMapDTO
	 * @throws Exception
	 * @throws:
	*/
	public PromotionCustomerMapDTO getPromotionShopMapByPromotionShopMapID(long customerId, int shopId, long promotionProgramShopMapID) throws Exception{
		PromotionCustomerMapDTO dto = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    pcm.*	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    PROMOTION_SHOP_MAP psm,	");
		sqlObject.append("	    PROMOTION_CUSTOMER_MAP pcm	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND psm.PROMOTION_SHOP_MAP_ID = pcm.PROMOTION_SHOP_MAP_ID	");
		sqlObject.append("	    AND pcm.CUSTOMER_ID = ?	");
		paramsObject.add("" + customerId);
		sqlObject.append("	    AND pcm.shop_id = ?	");
		sqlObject.append("	    AND pcm.status = 1	");
		paramsObject.add("" + shopId);
		sqlObject.append("	     and psm.PROMOTION_SHOP_MAP_ID = ?	");
		paramsObject.add("" + promotionProgramShopMapID);

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = new PromotionCustomerMapDTO();
					dto.initDataFromCursor(c);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return dto;
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 08:53:27 24 Sep 2014
	 * @return: PromotionStaffMapDTO
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param promotionShopMapId
	 * @return
	 */
	public PromotionStaffMapDTO getPromotionStaffMapByPromotionShopMapID(
			int staffId, int shopId, long promotionProgramShopMapID) {
		PromotionStaffMapDTO dto = null;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    PROMOTION_SHOP_MAP psm,	");
		sqlObject.append("	    PROMOTION_STAFF_MAP pcm	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND psm.PROMOTION_SHOP_MAP_ID = pcm.PROMOTION_SHOP_MAP_ID	");
		sqlObject.append("	    AND pcm.STAFF_ID = ?	");
		paramsObject.add("" + staffId);
		sqlObject.append("	    AND pcm.shop_id = ?	");
		sqlObject.append("	    AND pcm.status = 1	");
		paramsObject.add("" + shopId);
		sqlObject.append("	     and psm.PROMOTION_SHOP_MAP_ID = ?	");
		paramsObject.add("" + promotionProgramShopMapID);

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = new PromotionStaffMapDTO();
					dto.initDataFromCursor(c);
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return dto;
	}

	/**
	 * Tang so suat KM cua staff map
	 * @author: duongdt3
	 * @param amountReceived
	 * @param numReceived
	 * @since: 1.0
	 * @time: 09:46:38 24 Sep 2014
	 * @return: boolean
	 * @throws:
	 * @param staffMapId
	 * @param quantityReviceved
	 * @return
	 */
	public boolean increaseStaffQuantityRecevie(long promotionStaffMapId, int quantityReceived, int numReceived, double amountReceived) {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_STAFF_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN QUANTITY_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN QUANTITY_RECEIVED + ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", NUM_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN NUM_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN NUM_RECEIVED + ");
		sqlObject.append(numReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(numReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", AMOUNT_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN AMOUNT_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN AMOUNT_RECEIVED + ");
		sqlObject.append(amountReceived);
		sqlObject.append(" ELSE AMOUNT_RECEIVED = ");
		sqlObject.append(amountReceived);
		sqlObject.append(" END) ");
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_STAFF_MAP_ID = ");
		sqlObject.append(promotionStaffMapId);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param promotionStaffMapId
	 * @param num
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	*/
	public boolean decreaseStaffQuantityRecevie(long promotionStaffMapId, int num) throws Exception {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_STAFF_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED = QUANTITY_RECEIVED - ");
		sqlObject.append(num);
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_STAFF_MAP_ID = ");
		sqlObject.append(promotionStaffMapId);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.e("decreaseStaffQuantityRecevie", "fail", e);
			throw e;
		} finally {
		}

		return true;
	}

}
