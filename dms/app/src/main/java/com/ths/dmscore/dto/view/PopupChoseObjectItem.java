/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;


/**
 * PopupChoseObjectItem.java
 *
 * @author: hoanpd1
 * @version: 1.0
 * @since: 16:17:51 07-01-2015
 */
public class PopupChoseObjectItem implements Serializable {

	// noi dung field
	private static final long serialVersionUID = 1L;
	//Id item
	public long objectTimeId;
//	// ten thoi gian
//	public String nameTime;
//	// tu ngay
//	public String fromDate;
//	// toi ngay
//	public String toDate;
	// ten hien thi len view
	public String nameTimeView;
	// tu ngay hien thi
	public String fromDateView;
	// den ngay hien thi
	public String toDateView;
	// da check hay chua
	public boolean isCheck;

	public PopupChoseObjectItem() {
		// TODO Auto-generated constructor stub
		objectTimeId = 0;
//		nameTime = Constants.STR_BLANK;
//		fromDate = Constants.STR_BLANK;
//		toDate = Constants.STR_BLANK;
		nameTimeView = Constants.STR_BLANK;
		fromDateView = Constants.STR_BLANK;
		toDateView = Constants.STR_BLANK;
		isCheck =true;
	}

	@Override
	public Object clone()  {
		PopupChoseObjectItem p = new PopupChoseObjectItem();
		p.objectTimeId = this.objectTimeId;
//		p.nameTime = this.nameTime;
//		p.fromDate = this.fromDate;
//		p.toDate = this.toDate;
		p.nameTimeView = this.nameTimeView;
		p.fromDateView = this.fromDateView;
		p.toDateView = this.toDateView;
		p.isCheck = this.isCheck;
		return p;
	}
}
