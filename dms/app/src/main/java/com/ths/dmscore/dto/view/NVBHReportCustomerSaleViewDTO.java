/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

public class NVBHReportCustomerSaleViewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	public ArrayList<CustomerSaleOfNVBHDTO> listDto;
	public int totalData;

	public NVBHReportCustomerSaleViewDTO() {
		listDto = new ArrayList<CustomerSaleOfNVBHDTO>();
		totalData = -1;
	}
}
