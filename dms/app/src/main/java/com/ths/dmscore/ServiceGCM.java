/*
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore;

import android.content.Context;
import android.content.SharedPreferences;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;

/**
* Dich vu GCM
* @author: DungDQ3
* @version: 1.1
* @since: 1.0
*/
public class ServiceGCM {

	// file luu regID
	SharedPreferences sharedPreferences;
	// ID dang kys
	String regID;
	// Service GCM
	private static volatile ServiceGCM instance;
	// context
	Context mContext;

	private static final Object lockObject = new Object();

	public static ServiceGCM getInstance(Context con) {
		if (instance == null) {
			try{
			synchronized (lockObject) {
				if (instance == null) {
					instance = new ServiceGCM(con);
				}
			}
			}catch(Exception ex){
				ex.getMessage();
			}
		}
		return instance;
	}

	public ServiceGCM(Context con) {
		mContext = con;
	}

	/**
	 * Thuc hien dang ky GCM toi server GCM
	 * 
	 * @author: BANGHN
	 */
	public void register() {
		if (mContext != null) {
//			sharedPreferences = mContext
//					.getSharedPreferences(
//							mContext.getString(R.string.app_name),
//							Context.MODE_PRIVATE);
			sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			regID = sharedPreferences.getString(IntentConstants.INTENT_REGID,
					Constants.STR_BLANK);
			if (regID.equals(Constants.STR_BLANK)) {
				GCMRegisterAsync ar = new GCMRegisterAsync(
						mContext.getApplicationContext());
				ar.execute();
			}
			
		}
	}
}