package com.ths.dmscore.dto.view;

import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

import java.io.Serializable;

/**
 * Created by tampq on 11/13/16.
 */

public class ChartStaffDTO implements Serializable{
    public int amount;
    public String areaCode;
    public String fullDate;
    public String month;
    public String regionCode;
    public String shopCode;
    public String shopName;
    public String staffCode;
    public String staffName;
    public String year;
    public int dayInOrder;
    public int monthIndex;

    public ChartStaffDTO(){

    }

    public void parseData(JSONObject json) throws Exception{
        try {
            amount = json.getInt("amount");
            areaCode = json.getString("areaCode");
            fullDate = json.getString("day");
            if(!StringUtil.isNullOrEmpty(fullDate)){
                dayInOrder = Integer.valueOf(fullDate.substring(0,2));
            }
            month = json.getString("month");
            monthIndex = Integer.valueOf(month.substring(0,2));
            regionCode = json.getString("regionCode");
            shopCode = json.getString("shopCode");
            shopName = json.getString("shopName");
            staffCode = json.getString("staffCode");
            staffName = json.getString("staffCode");
            year = json.getString("year");
        }catch (Exception ex){
            MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
        }
    }

}
