/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * DTO man hinh ds hinh anh cua GS NPP
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class TBHVListImageDTOView implements Serializable {
	public int totalItems;
	public ArrayList<ImageInfoShopDTO> listData = new ArrayList<ImageInfoShopDTO>();
	
	
}
