/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

import com.ths.dmscore.dto.db.OfficeDocumentDTO;
import com.ths.dmscore.dto.db.ApParamDTO;

/**
 * dto danh sach cong van
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
public class OfficeDocumentListDTO {
	// save du lieu for request
	public String fromDate = "";// tu ngay
	public String toDate = "";// den ngay
	public String fromDateForRequest = "";// tu ngay
	public String toDateForRequest = "";// den ngay
	public String programeCode = "";// loai cong van
	// list work plan item
	public ArrayList<OfficeDocumentDTO> list = new ArrayList<OfficeDocumentDTO>();// ds cong van
	public List<ApParamDTO> listType = new ArrayList<ApParamDTO>();
	public int total = 0;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
