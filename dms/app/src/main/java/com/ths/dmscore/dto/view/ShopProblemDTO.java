package com.ths.dmscore.dto.view;

import com.ths.dmscore.dto.db.ShopDTO;

/**
 * DTO ds shop van de
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ShopProblemDTO {
	public ShopDTO shop = new ShopDTO();
	public boolean isCheck = true;
	public int parentShopId = 0;
	public int level = 0; // level cua shop
}
