package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;


/**
 * DTO chon row don vi cho man hinh bao cao dong
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportRowUnitDTO  {
	// id don vi
	public int shopId;
	// ma don vi 
	public String shopCode;
	// ten don vi
	public String shopName;
	// ma nguoi quan li
	public String staffId;
	// ten nguoi quan li
	public String staffName;
//	// id nguoi quan li
	public String staffCode;
	// bien kiem tra check hay ko
	public boolean isCheck = false;
	// trang thai
	public int status;
	
	/**
	 * Khoi tao du lieu
	 * 
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initFromCursor(Cursor c) {
		shopId = CursorUtil.getInt(c, "SHOP_ID");
		staffId = CursorUtil.getString(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		shopCode = CursorUtil.getString(c, "SHOP_CODE");
		shopName = CursorUtil.getString(c, "SHOP_NAME");
		status = CursorUtil.getInt(c, "STATUS");
	}
}
