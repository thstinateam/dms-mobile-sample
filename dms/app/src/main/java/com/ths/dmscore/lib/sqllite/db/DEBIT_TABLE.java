/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.CycleInfo;
import com.ths.dmscore.dto.view.CustomerDebitDetailDTO;
import com.ths.dmscore.dto.view.CustomerDebtDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.view.CusDebitDetailDTO;

/**
 * Bang cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class DEBIT_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String DEBIT_ID = "DEBIT_ID";
	//
	public static final String OBJECT_ID = "OBJECT_ID";
	//
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// tong no
	public static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
	// tong no da tra
	public static final String TOTAL_PAY = "TOTAL_PAY";
	// tong no
	public static final String TOTAL_DEBIT = "TOTAL_DEBIT";
	// gia tri lon nhat cho phep no
	public static final String MAX_DEBIT_AMOUNT = "MAX_DEBIT_AMOUNT";
	// thoi han no toi da
	public static final String MAX_DEBIT_DATE = "MAX_DEBIT_DATE";
	// tong chiet khau
	public static final String TOTAL_DISCOUNT = "TOTAL_DISCOUNT";
//	// ID nhan vien
//	public static final String STAFF_ID = "STAFF_ID";

	// ten bang
	public static final String TABLE_NAME = "DEBIT";

	public DEBIT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { DEBIT_ID, OBJECT_ID, OBJECT_TYPE, TOTAL_AMOUNT, TOTAL_PAY, TOTAL_DEBIT,
				MAX_DEBIT_AMOUNT, MAX_DEBIT_DATE, TOTAL_DISCOUNT };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((DebitDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(DebitDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		DebitDTO disDTO = (DebitDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.id };
		return update(value, DEBIT_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * 
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(DEBIT_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		DebitDTO paramDTO = (DebitDTO) dto;
		String[] params = { String.valueOf(paramDTO.id) };
		return delete(DEBIT_ID + " = ?", params);
	}

	private ContentValues initDataRow(DebitDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(DEBIT_ID, dto.id);
		editedValues.put(OBJECT_ID, dto.objectID);
		editedValues.put(OBJECT_TYPE, dto.objectType);
		editedValues.put(TOTAL_AMOUNT, dto.totalAmount);
		editedValues.put(TOTAL_PAY, dto.totalPay);
		editedValues.put(TOTAL_DEBIT, dto.totalDebit);
		editedValues.put(MAX_DEBIT_AMOUNT, dto.maxDebitAmount);
		editedValues.put(MAX_DEBIT_DATE, dto.maxDebitDate);
		editedValues.put(TOTAL_DISCOUNT, dto.totalDiscount);
//		editedValues.put(STAFF_ID, dto.staffID);
		return editedValues;
	}

	/**
	 * Lay danh sach cong no khach hang
	 * 
	 * @author: BangHN
	 * @param customerCode
	 *            : search theo customer code
	 * @param nameAddress
	 *            : search theo name, dia chi
	 * @return
	 * @throws Exception
	 * @return: ArrayList<CustomerDebtDTO>
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ArrayList<CustomerDebtDTO> getCustomerDebt(String customerCode, String nameAddress) throws Exception {
		ArrayList<CustomerDebtDTO> listData = null;
		List<String> params = new ArrayList<String>();
		String staffId = "" + GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		String shopId  = "" + GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		
		String dateStr = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		EXCEPTION_DAY_TABLE exceptionTable = new EXCEPTION_DAY_TABLE(mDB);
		CycleInfo cycleInfo = exceptionTable.getCycleInfoOfDate(dateStr, -1);
		String beginDate = cycleInfo.beginDate;
		StringBuffer  sql = new StringBuffer();
		sql.append("SElect * from (SELECT D.DEBIT_ID AS DEBIT_ID, c.[short_code] AS SHORT_CODE, ");
		sql.append("       c.[customer_id] AS CUSTOMER_ID, ");
		sql.append("       c.[customer_code] AS CUSTOMER_CODE, ");
		sql.append("       c.[customer_name] AS CUSTOMER_NAME, ");
		sql.append("       c.[address] AS ADDRESS, ");
		sql.append("       c.[housenumber] ");
		sql.append("       || ' ' ");
		sql.append("       || c.[street]                                                 AS STREET_HOUSE, ");
		sql.append("       Sum(dd.amount)                                                AS AMOUNT, ");
		sql.append("       Sum(dd.total)                                                 AS TOTAL, ");
		sql.append("       Sum(dd.total_pay)                                             AS ");
		sql.append("       TOTAL_PAY, ");
		sql.append("       Sum(case when dd.remain > 0 and DD.ORDER_TYPE IN ('SO') then dd.remain else 0 END)        AS REMAIN, ");
		sql.append("       Sum(case when dd.remain < 0 and DD.ORDER_TYPE IN ('CO') then dd.remain else 0 END)        AS REPAY, ");
		sql.append("       d.object_id AS OBJECT_ID, ");
		sql.append("       d.debit_id AS DEBIT_ID, ");
		sql.append("       dd.[DEBIT_DETAIL_TEMP_ID] AS DEBIT_DETAIL_TEMP_ID, ");
		sql.append("       Julianday(Date('now', 'localtime')) - Julianday(Date(Min(debit_date_count ))) AS ");
		sql.append("       REMAIN_DAY  ");
		sql.append("FROM    ");
		sql.append("    (select *, (case when remain = 0 then dayInOrder('now', 'localtime') else debit_date end) as debit_date_count from debit_detail_temp ) dd     ");
		sql.append("       JOIN debit d ");
		sql.append("         ON d.debit_id = dd.debit_id ");
		sql.append("       JOIN customer c ");
		sql.append("         ON d.object_id = c.customer_id ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND c.status = 1 ");
		sql.append("       AND c.shop_id = ? ");
		params.add(shopId);
		
		sql.append("       AND c.customer_id IN (SELECT DISTINCT rc.customer_id ");
		sql.append("                             FROM   visit_plan vp, ");
		sql.append("                                    routing_customer rc ");
		sql.append("                             WHERE  vp.routing_id = rc.routing_id ");
		sql.append("                                    AND vp.status = 1 ");
		sql.append("                                    AND rc.status = 1 ");
		sql.append("                                    AND vp.staff_id = ? ");
		params.add(staffId);
		
		sql.append("                                    AND ( Date(vp.to_date) >= ");
		sql.append("                                          Date('NOW', 'localtime') ");
		sql.append("                                           OR vp.to_date IS NULL )) ");
		sql.append("       AND d.object_type = 3 ");
//		sql.append("       AND dd.remain != 0 ");
		sql.append("       AND dd.FROM_OBJECT_ID is not null ");
		
		if (!StringUtil.isNullOrEmpty(customerCode)) {
			sql.append("       AND Upper(c.[short_code]) LIKE Upper(?) ");
			params.add("%" + customerCode.trim() + "%");
		}
		// customer name search
		if (!StringUtil.isNullOrEmpty(nameAddress)) {
			nameAddress = StringUtil.escapeSqlString(nameAddress);
			nameAddress = DatabaseUtils.sqlEscapeString("%" + nameAddress.trim() + "%");
			sql.append("	and ((upper(c.[name_text]) like upper(");
			sql.append(nameAddress);
			sql.append(") escape '^') or (upper(STREET_HOUSE) like upper( ");
			sql.append(nameAddress);
			sql.append(") escape '^') or (upper(c.[address]) like upper( ");
			sql.append(nameAddress);
			sql.append(") escape '^'))");
		}
		
		
		sql.append("       AND substr(dd.debit_date, 1, 10) >= substr(?, 1, 10) ");
		params.add(beginDate);
		sql.append("GROUP  BY c.customer_code ");
		sql.append("ORDER  BY REMAIN_DAY desc, REMAIN desc, customer_name ) DEBIT ");
		sql.append(" LEFT JOIN ");
		sql.append("    ( ");
		sql.append("        SELECT ");
		sql.append("            PAY_RECEIVED_NUMBER, ");
		sql.append("            PD.FOR_OBJECT_ID  AS SALE_ORDER_ID, ");
		sql.append("            PD.amount as PAY_AMOUNT, ");
		sql.append("            SUM(CASE WHEN PR.APPROVED = '2' THEN 1 ELSE 0 END) AS NUMREFUSE, ");
		sql.append("            DD.DEBIT_ID DEBIT_ID1 ");
		sql.append("        FROM ");
		sql.append("            PAYMENT_DETAIL_TEMP PD, ");
		sql.append("            PAY_RECEIVED_TEMP PR, ");
		sql.append("            (SELECT * FROM DEBIT_DETAIL_TEMP GROUP BY FROM_OBJECT_ID) DD ");
		sql.append("        WHERE ");
		sql.append("            PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID and PD.status != '-1'  ");
		sql.append("            AND DD.FROM_OBJECT_ID = PD.FOR_OBJECT_ID  ");
		sql.append("            GROUP BY DEBIT_ID1  ");
		sql.append("    ) P_R ");
		sql.append("        ON DEBIT.DEBIT_ID = P_R.DEBIT_ID1 ");


		Cursor cursor = null;
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				listData = new ArrayList<CustomerDebtDTO>();
				if (cursor.moveToFirst()) {
					do {
						CustomerDebtDTO debtCustomer = new CustomerDebtDTO();
						debtCustomer.initCustomerDebtFromCursor(cursor);
						if (debtCustomer != null)
							listData.add(debtCustomer);
					} while (cursor.moveToNext());
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
		return listData;
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @param cusDebitDetailDto
	 * @return: voidvoid
	 * @throws:
	 */
	public int updateDebt(CusDebitDetailDTO cusDebitDetailDto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(TOTAL_PAY, cusDebitDetailDto.totalPay);
		editedValues.put(TOTAL_DEBIT, cusDebitDetailDto.totalDebit);
		editedValues.put(TOTAL_DISCOUNT, cusDebitDetailDto.totalDiscount);
		String[] params = { "" + cusDebitDetailDto.debitId };
		return update(editedValues, DEBIT_ID + " = ?", params);
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: DungNX
	 * @param cusDebitDetailDto
	 * @return: voidvoid
	 * @throws:
	 */
	public int updateDebt(DebitDTO debitDto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(TOTAL_AMOUNT, debitDto.totalAmount);
		editedValues.put(TOTAL_PAY, debitDto.totalPay);
		editedValues.put(TOTAL_DEBIT, debitDto.totalDebit);
		editedValues.put(TOTAL_DISCOUNT, debitDto.totalDiscount);
		String[] params = { "" + debitDto.id };
		return update(editedValues, DEBIT_ID + " = ?", params);
	}

	/**
	 * 
	 * Kiem tra cong no cua KH co' ton tai hay ko?
	 * 
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @return: boolean
	 * @throws:
	 */
	public long checkDebitExist(String customerId) throws Exception {
		long result = -1;
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT debit_id from debit where object_id = ? and object_type = 3 order by syn_state desc limit 1");

		Cursor cursor = null;
		try {
			String[] arrParam = new String[] { customerId };
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToNext()) {
					result = cursor.getLong(0);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
		return result;
	}

	/**
	 * 
	 * Tang cong no cua 1 KH
	 * 
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean increaseDebit(AbstractTableDTO dto) {
		DebitDTO temp = (DebitDTO) dto;

		DebitDTO disDTO = new DebitDTO();
		disDTO.objectID = temp.objectID;
		disDTO.objectType = temp.objectType;
		disDTO.totalAmount = temp.totalAmount;
		disDTO.totalDebit = temp.totalDebit;
		disDTO.totalPay = temp.totalPay;
		disDTO.totalDiscount = temp.totalDiscount;

		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("UPDATE DEBIT SET ");
		sqlObject.append("TOTAL_AMOUNT = TOTAL_AMOUNT + ");
		sqlObject.append(disDTO.totalAmount);

		sqlObject.append(",TOTAL_PAY = TOTAL_PAY + ");
		sqlObject.append(disDTO.totalPay);
		
		sqlObject.append(",TOTAL_DISCOUNT = TOTAL_DISCOUNT + ");
		sqlObject.append(disDTO.totalDiscount);
		sqlObject.append(",TOTAL_DEBIT = TOTAL_DEBIT + ");
		sqlObject.append(disDTO.totalDebit);
		sqlObject.append(" WHERE ");
		sqlObject.append(" OBJECT_ID = ");
		sqlObject.append(disDTO.objectID);
		sqlObject.append(" AND OBJECT_TYPE = ");
		sqlObject.append(disDTO.objectType);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
		}

		return true;
	}
	
	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws:
	*/
//	public boolean increaseDebitPayment(AbstractTableDTO dto) {
//		DebitDTO temp = (DebitDTO) dto;
//
//		DebitDTO disDTO = new DebitDTO();
//		disDTO.objectID = temp.objectID;
//		disDTO.objectType = temp.objectType;
//		disDTO.totalPay = temp.totalPay;
//		disDTO.totalDiscount = temp.totalDiscount;
//		disDTO.totalDebit = temp.totalDebit;
//
//		StringBuilder sqlObject = new StringBuilder();
//		sqlObject.append("UPDATE DEBIT SET ");
//		if (disDTO.totalPay >= 0) {
//			sqlObject.append("TOTAL_PAY = TOTAL_PAY + ");
//		} else {
//			sqlObject.append("TOTAL_PAY = TOTAL_PAY  ");
//		}
//		sqlObject.append(disDTO.totalPay);
//
//		if (disDTO.totalDiscount >= 0) {
//			sqlObject.append(",TOTAL_DISCOUNT = case when TOTAL_DISCOUNT is null then " + disDTO.totalDiscount + " else TOTAL_DISCOUNT +  " + disDTO.totalDiscount + " END ");
//		} else {
//			sqlObject.append(",TOTAL_DISCOUNT = TOTAL_DISCOUNT  ");
//			sqlObject.append(disDTO.totalDiscount);
//		}
//
//		if (disDTO.totalDebit >=0 ) {
//			sqlObject.append(",TOTAL_DEBIT = TOTAL_DEBIT + ");
//		} else {
//			sqlObject.append(",TOTAL_DEBIT = TOTAL_DEBIT  ");
//		}
//		sqlObject.append(disDTO.totalDebit);
//		sqlObject.append(" WHERE ");
//		sqlObject.append(" OBJECT_ID = ");
//		sqlObject.append(disDTO.objectID);
//		sqlObject.append(" AND OBJECT_TYPE = ");
//		sqlObject.append(disDTO.objectType);
//
//		try {
//			execSQL(sqlObject.toString());
//		} catch (Exception e) {
//			return false;
//		} finally {
//		}
//
//		return true;
//	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @return: DebitDTO
	 * @throws:
	*/
	public DebitDTO getDebitByCustomerID(long customerId) throws Exception {
		DebitDTO result = new DebitDTO();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from debit where object_id = ? and object_type = 3 ");

		Cursor cursor = null;
		try {
			String[] arrParam = new String[] { "" + customerId };
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToNext()) {
					result.initFromCursor(cursor);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
		return result;
	}
	
	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param customerId
	 * @return
	 * @throws Exception
	 * @return: DebitDTO
	 * @throws:
	*/
	public int getMaxDaysDebitCustomer(long customerId, String shopId) throws Exception {
		int days = 0;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    c.[short_code],	");
		sqlObject.append("	    Sum(dd.amount)                                                AS AMOUNT,	");
		sqlObject.append("	    Sum(dd.total)                                                 AS TOTAL,	");
		sqlObject.append("	    Sum(dd.total_pay)                                             AS        TOTAL_PAY,	");
		sqlObject.append("	    Sum(dd.remain)                                                AS REMAIN,	");
		sqlObject.append("	    d.object_id,	");
		sqlObject.append("	    d.debit_id,	");
		sqlObject.append("	    dd.[debit_detail_id],	");
		sqlObject.append("	    Julianday(Date('now',	");
		sqlObject.append("	    'localtime')) - Julianday(Date(Min(so.[ORDER_DATE]))) AS        REMAIN_DAY	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    debit_detail dd,	");
		sqlObject.append("	    debit d,	");
		sqlObject.append("	    customer c,	");
		sqlObject.append("	    sale_order so	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND d.debit_id = dd.debit_id	");
		sqlObject.append("	    AND d.object_id = c.customer_id	");
		sqlObject.append("	    AND so.sale_order_id = dd.FROM_OBJECT_ID	");
		sqlObject.append("	    AND c.customer_id = ?	");
		paramsObject.add("" + customerId);
		sqlObject.append("	    AND c.status = 1	");
		sqlObject.append("	    AND c.shop_id = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	    AND d.object_type = 3	");
		sqlObject.append("	    AND dd.remain > 0	");
		sqlObject.append("	    AND Date(dd.debit_date) >= Date('NOW', 'localtime', 'start of month', '-1 month')	");
		sqlObject.append("	GROUP  BY	");
		sqlObject.append("	    c.customer_code	");
		sqlObject.append("	ORDER  BY	");
		sqlObject.append("	    REMAIN_DAY desc,	");
		sqlObject.append("	    REMAIN desc,	");
		sqlObject.append("	    customer_name	");
		
		Cursor cursor = null;
		try {
			cursor = rawQueries(sqlObject.toString(), paramsObject);

			if (cursor != null) {
				if (cursor.moveToNext()) {
					days = CursorUtil.getInt(cursor, "REMAIN_DAY");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
		return days;
	}

	/**
	 * update debit
	 * @author cuonglt3
	 * @param dto
	 * @return
	 */
	public int updateDebit(CustomerDebitDetailDTO dto) {
		// TODO Auto-generated method stub
		int result = 0;
		result = updateDebt(dto.debitDTO);
		return result;
	}
}
