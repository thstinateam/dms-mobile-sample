/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.view.sale.customer.CustomerInfoView;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dmscore.view.sale.image.ImageSearchNVBHView;
import com.ths.dmscore.view.sale.image.ListAlbumUserView;
import com.ths.dmscore.view.sale.image.PhotoThumbnailListProgrameView;
import com.ths.dmscore.view.sale.newcustomer.NewCustomerCreate;
import com.ths.dmscore.view.sale.order.OrderView;
import com.ths.dmscore.view.sale.order.TakePhotoEquipmentView;
import com.ths.dmscore.view.sale.order.VoteDisplayPresentProductView;
import com.ths.dmscore.view.sale.order.VoteKeyShopView;
import com.ths.dmscore.view.sale.salestatistics.SaleStatisticsInDayPreSalesView;
import com.ths.dmscore.view.sale.salestatistics.SaleStatisticsInDayVanSalesView;
import com.ths.dmscore.view.sale.statistic.ReportStaffSaleDetailView;
import com.ths.dmscore.view.sale.stock.LockDateValView;
import com.ths.dmscore.view.supervisor.keyshop.KeyShopListView;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.model.SaleModel;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackListView;
import com.ths.dmscore.view.sale.customer.CustomerRouteView;
import com.ths.dmscore.view.sale.customer.PostFeedbackView;
import com.ths.dmscore.view.sale.image.ImageListView;
import com.ths.dmscore.view.sale.image.PhotoThumbnailListView;
import com.ths.dmscore.view.sale.newcustomer.ListCustomerCreatedView;
import com.ths.dmscore.view.sale.order.IntroduceProductView;
import com.ths.dmscore.view.sale.order.ListOrderView;
import com.ths.dmscore.view.sale.order.RemainProductView;
import com.ths.dmscore.view.sale.salestatistics.SaleStatisticsAccumulateDayView;
import com.ths.dmscore.view.sale.statistic.GeneralStatisticsView;
import com.ths.dmscore.view.sale.statistic.NVBHReportASOView;
import com.ths.dmscore.view.sale.statistic.NVBHReportCustomerSaleView;
import com.ths.dmscore.view.sale.statistic.NVBHReportForcusProductView;
import com.ths.dmscore.view.sale.statistic.NoteListView;
import com.ths.dmscore.view.sale.statistic.ReportKPISaleView;
import com.ths.dmscore.view.supervisor.keyshop.ReportKeyShopNVBH;

/**
 * Controller xu ly nghiep vu NVBH
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class SaleController extends AbstractController {

	static volatile SaleController instance;

	protected SaleController() {
	}

	public static SaleController getInstance() {
		if (instance == null) {
			instance = new SaleController();
		}
		return instance;
	}


	/**
	 * Xu ly du lieu tra ve tu model
	 *
	 * @author: TruongHN
	 * @param modelEvent
	 * @return: void
	 * @throws:
	 */
	public void handleModelEvent(final ModelEvent modelEvent) {
		if (modelEvent.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
			final ActionEvent e = modelEvent.getActionEvent();
			if (e.sender != null) {
				if (e.sender instanceof GlobalBaseActivity) {
					final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
					if (sender == null || sender.isFinished)
						return;
					sender.runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.isFinished)
								return;
							sender.handleModelViewEvent(modelEvent);
						}
					});
				} else if (e.sender instanceof BaseFragment) {
					final BaseFragment sender = (BaseFragment) e.sender;
					if (sender == null || sender.getActivity() == null || sender.isFinished) {
						return;
					}
					sender.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.getActivity() == null || sender.isFinished) {
								return;
							}
							sender.handleModelViewEvent(modelEvent);
						}
					});
				}
			} else {
				modelEvent.setIsSendLog(false);
				handleErrorModelEvent(modelEvent);
			}
		} else {
			handleErrorModelEvent(modelEvent);
		}
	}

	@Override
	public void handleSwitchActivity(ActionEvent e) {
	}

	@Override
	public boolean handleSwitchFragment(ActionEvent e) {
		Activity base = null;
		if (e.sender instanceof Activity) {
			base = (Activity) e.sender;
		} else if (e.sender instanceof Fragment) {
			base = ((Fragment) e.sender).getActivity();
		}

		boolean resultSwitch = false;
		if (base != null && !base.isFinishing()) {
			//an ban phim truoc khi chuyen man hinh
			GlobalUtil.forceHideKeyboard(base);

			//chuyen man hinh
			boolean isRemoveAllBackStack = false;
			BaseFragment frag = null;
			Bundle data = (e.viewData instanceof Bundle) ? (Bundle)e.viewData : null;
			//level trong truong hop view quan ly
			if (data != null) {
				//put addition info switch action
				data.putInt(IntentConstants.INTENT_SWITCH_ACTION, e.action);
			}

			switch (e.action) {
//			case ActionEventConstant.REVIEW_SHOWROOM:{
//				isRemoveAllBackStack = true;
//				frag = ReviewShowRoomFragment.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.PAID_PROMOTION_LIST:{
//				isRemoveAllBackStack = true;
//				frag = PaidShowRoomPromotionView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.GOTO_VOTE_DISPLAY_PRESENT_PRODUCT: {
				isRemoveAllBackStack = false;
				frag = VoteDisplayPresentProductView.getInstance(data);
				break;
			}
			// GOTO_PRODUCT_INTRODUCTION
			case ActionEventConstant.GOTO_PRODUCT_INTRODUCTION: {
				isRemoveAllBackStack = false;
				frag = IntroduceProductView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_LIST_ALBUM_USER: {
				isRemoveAllBackStack = false;
				frag = ListAlbumUserView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_ALBUM_DETAIL_USER: {
				isRemoveAllBackStack = false;
				frag = PhotoThumbnailListView.getInstance(data);
				break;
			}
//			case ActionEventConstant.ACTION_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH_SALE: {
//				isRemoveAllBackStack = false;
//				frag = CustomerNotPSDSInMonthSaleView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.GET_CUSTOMER_LIST:
				isRemoveAllBackStack = true;
				frag = CustomerListView.newInstance(data);
				break;
			case ActionEventConstant.NOTE_LIST_VIEW:{
				isRemoveAllBackStack = true;
				frag = NoteListView.newInstance(data);
				break;
			}
			case ActionEventConstant.GET_LIST_CUS_FEED_BACK: {
				isRemoveAllBackStack = false;
				frag = CustomerFeedBackListView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_IMAGE_LIST:{
				isRemoveAllBackStack = true;
				frag = ImageListView.getInstance(data);
				break;
			}
			case ActionEventConstant.POST_FEEDBACK: {
				isRemoveAllBackStack = false;
				frag = PostFeedbackView.newInstance(data);
				break;
			}
//			case ActionEventConstant.GOTO_CUSTOMER_DEBT_LIST: {
//				isRemoveAllBackStack = true;
//				frag = CustomerDebtView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GET_CUS_DEBIT_DETAIL: {
//				isRemoveAllBackStack = false;
//				CustomerDebtDTO dto = (CustomerDebtDTO) e.viewData;
//				frag = CustomerDebitDetailView.newInstance(dto);
//				break;
//			}
//			case ActionEventConstant.ACTION_GO_TO_DEBT_REFUSE: {
//				isRemoveAllBackStack = false;
//				CustomerDebtDTO dto = (CustomerDebtDTO) e.viewData;
//				frag = CustomerDebitRefuse.newInstance(dto);
//				break;
//			}
//			case ActionEventConstant.ACTION_GO_TO_VIEW_CUSTOMER_DISPLAY_PROGRESS: {
//				isRemoveAllBackStack = false;
//				frag = ReportDisplayProgressDetailCustomerView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME: {
				isRemoveAllBackStack = false;
				frag = PhotoThumbnailListProgrameView.getInstance(data);
				break;
			}
//			case ActionEventConstant.PAYMENT_VOUCHER: {
//				isRemoveAllBackStack = false;
//				frag = PaymentVoucherView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_IMAGE_LIST_SEARCH: {
				isRemoveAllBackStack = true;
				frag = ImageSearchNVBHView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_STAFF_SALE_DETAIL: {
				isRemoveAllBackStack = false;
				frag = ReportStaffSaleDetailView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW: {
				isRemoveAllBackStack = true;
				frag = LockDateValView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_GENERAL_STATISTICS: {
				isRemoveAllBackStack = true;
				frag = GeneralStatisticsView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW:{
				isRemoveAllBackStack = true;
				frag = NVBHReportForcusProductView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW:{
				isRemoveAllBackStack = true;
				frag = NoteListView.newInstance(data);
				break;
			}
//			case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY: {
//				isRemoveAllBackStack = true;
//				frag = ReportDisplayProgressDetailDayView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_CUSTOMER_ROUTE: {
				isRemoveAllBackStack = true;
				frag = CustomerRouteView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_LIST_ORDER: {
				isRemoveAllBackStack = true;
				frag = ListOrderView.newInstance(data);
				break;
			}
//			case ActionEventConstant.GO_TO_PROMOTION_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = PromotionProgramView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_DISPLAY_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = DisplayProgramView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_DOCUMENT: {
//				isRemoveAllBackStack = true;
//				frag = DocumentView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_PRODUCT_LIST: {
//				isRemoveAllBackStack = true;
//				frag = ProductListView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_PRE: {
				isRemoveAllBackStack = true;
				frag = SaleStatisticsInDayPreSalesView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_MONTH: {
				isRemoveAllBackStack = true;
				frag = SaleStatisticsAccumulateDayView.getInstance(data);
				break;
			}
//			case ActionEventConstant.ACTION_GO_TO_DEBT_PAYMENT: {
//				isRemoveAllBackStack = false;
//				CustomerDebtDTO dto = (CustomerDebtDTO) e.viewData;
//				frag = CustomerDebitPayments.getInstance(dto);
//				break;
//			}
//			case ActionEventConstant.GO_TO_KPI:{
//				isRemoveAllBackStack = true;
//				frag = ListDynamicKPIView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GOTO_CHOOSE_PARAMETERS:{
//				isRemoveAllBackStack = false;
//				frag = ChooseParametersBeforeExportReportView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_DYNAMIC_REPORT:{
//				isRemoveAllBackStack = false;
//				frag = DynamicReportView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_GRAPH_CHART:{
//				isRemoveAllBackStack = false;
//				frag = GraphChartView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED: {
				isRemoveAllBackStack = true;
				frag = ListCustomerCreatedView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_CREATE_CUSTOMER: {
				isRemoveAllBackStack = false;
				frag = NewCustomerCreate.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REMAIN_PRODUCT_VIEW: {
				isRemoveAllBackStack = false;
				frag = RemainProductView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_ORDER_VIEW: {
				isRemoveAllBackStack = false;
				frag = OrderView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_CUSTOMER_INFO: {
				isRemoveAllBackStack = false;
				frag = CustomerInfoView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_VAL: {
				isRemoveAllBackStack = true;
				frag = SaleStatisticsInDayVanSalesView.getInstance(data);
				break;
			}
//			case ActionEventConstant.GO_TO_TBHV_FOLLOW_LIST_PROBLEM: {
////				isRemoveAllBackStack = true;
////				frag = TBHVFollowProblemView.newInstance(data);
//				isRemoveAllBackStack = true;
//				frag = FollowProblemView.newInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_VOTE_KEY_SHOP: {
				isRemoveAllBackStack = false;
				frag = VoteKeyShopView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_TAKE_PHOTO_EQUIPMENT: {
				isRemoveAllBackStack = false;
				frag = TakePhotoEquipmentView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_KEY_SHOP_LIST_VIEW: {
				isRemoveAllBackStack = true;
				frag = KeyShopListView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_KEY_SHOP: {
				isRemoveAllBackStack = true;
				frag = ReportKeyShopNVBH.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_KPI_SALE: {
				isRemoveAllBackStack = true;
				frag = ReportKPISaleView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_CUSTOMER_SALE: {
				isRemoveAllBackStack = true;
				frag = NVBHReportCustomerSaleView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_NVBH_ASO: {
				isRemoveAllBackStack = true;
				frag = NVBHReportASOView.getInstance(data);
				break;
			}
//			case ActionEventConstant.GO_TO_CHART_REPORT:{
//                isRemoveAllBackStack = false;
//                frag = ChartReportView.newInstance(data);
//                break;
//            }
			default:
				break;
			}

			if (frag != null) {
				resultSwitch  = switchFragment(base, frag, isRemoveAllBackStack);
			}
		}

		return resultSwitch;
	}

	@Override
	public void handleViewEvent(final ActionEvent e) {
		if (e.isUsingAsyntask) {
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {
					//cap nhat thoi gian chay cua event
					e.startTimeFromBootActive = SystemClock.elapsedRealtime();
					SaleModel.getInstance().handleControllerEvent(
							SaleController.this, e);
					GlobalBaseActivity base = null;
					if (e.sender instanceof Activity) {
						base = (GlobalBaseActivity) e.sender;
					} else if (e.sender instanceof Fragment) {
						base = (GlobalBaseActivity) ((Fragment) e.sender).getActivity();
					}
					if (e.request != null && base != null) {
						base.addProcessingRequest(e.request, e.isBlockRequest);
					}
					return null;
				}
			};
			task.execute();
		} else {
			SaleModel.getInstance().handleControllerEvent(this, e);
		}
	}

}
