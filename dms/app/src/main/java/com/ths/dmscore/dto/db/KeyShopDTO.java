package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.KEYSHOP_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cho table keyshop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KeyShopDTO extends AbstractTableDTO {
	// noi dung field
	private static final long serialVersionUID = 1L;
	public long ksId;
	public String ksCode;
	public String name;
	public long fromCycleId;
	public long toCycleId;
	// so luong hinh anh toi thieu cho moi CT
	public long minPhotoNum = 0;
	public long takenPhotoNum = 0;

	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c){
		ksId = CursorUtil.getLong(c, KEYSHOP_TABLE.KS_ID);
		ksCode = CursorUtil.getString(c, KEYSHOP_TABLE.KS_CODE);
		name = CursorUtil.getString(c, KEYSHOP_TABLE.NAME);
		minPhotoNum = CursorUtil.getInt(c, KEYSHOP_TABLE.MIN_PHOTO_NUM);
		fromCycleId = CursorUtil.getLong(c, KEYSHOP_TABLE.FROM_CYCLE_ID);
		toCycleId = CursorUtil.getLong(c, KEYSHOP_TABLE.TO_CYCLE_ID);
	}
}
