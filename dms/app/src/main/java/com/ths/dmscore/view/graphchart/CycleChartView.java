package com.ths.dmscore.view.graphchart;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.SeriesSelection;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.ths.dmscore.dto.view.ChartDTO;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

public class CycleChartView extends
		AbstractChartView {
	private final CategorySeries series;

	public CycleChartView(
			Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		series = new CategorySeries("Pie chart");
	}

	public CycleChartView(
			Context context,
			AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		series = new CategorySeries("Pie chart");
	}

	public CycleChartView(
			Context context,
			AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		series = new CategorySeries("Pie chart");
		
	}

	/**
	 * draw pie chart
	 * 
	 * @author: dungdq3
	 * @since: 2:51:08 PM Oct 3, 2014
	 * @return: void
	 * @throws:
	 * @param lstColorChart
	 * @param lstvalues
	 * @param chartTitle
	 */
	public void drawChart(String chartTitle, String[] annotation,int[] lstColorChart, ChartDTO item) {
	
		int length = item.value.length;
		for (int i = 0; i < length; i++) {
			// neu be hon 0 thi cho bang 0 de ve duong tron
			SimpleSeriesRenderer performance = new SimpleSeriesRenderer();
			performance.setColor(lstColorChart[i]);
			renderer.addSeriesRenderer(performance);
			if (item.value[i] > 0){
				series.add(annotation[i], item.value[i]);
			}
			else{
//				performance.setDisplayChartValues(false);
				series.add(annotation[i], 0);
			}
		}
		renderer.setChartTitle(chartTitle);
		renderer.setDisplayValues(true);
		renderer.setZoomButtonsVisible(true);
		renderer.setLabelsColor(ImageUtil.getColor(R.color.BLACK));
		renderer.setZoomEnabled(true);
		renderer.setAntialiasing(true);
		renderer.setChartTitleTextSize(16);
		renderer.setShowLabels(true);
		renderer.setLabelsTextSize(16);
		renderer.setLegendTextSize(16);
		// show cac dong text
		renderer.setShowLegend(true);
		renderer.setInScroll(true);
		chartView = ChartFactory.getPieChartView(con, series, renderer);
		// cho click vao tung manh tren hinh tron
		renderer.setClickEnabled(true);
		renderer.setPanEnabled(true, true);
		chartView.setOnClickListener(this);
		addView(chartView);
	}
	
	@Override
	public void onClick(View arg0) {
		SeriesSelection seriesSelection = chartView.getCurrentSeriesAndPoint();
		if (seriesSelection != null) {
			for (int i = 0; i < series.getItemCount(); i++) {
				if (!renderer.getSeriesRendererAt(i).isHighlighted())
					renderer.getSeriesRendererAt(i).setHighlighted(
							i == seriesSelection.getPointIndex());
				else {
					renderer.getSeriesRendererAt(i).setHighlighted(false);
				}

			}
		}
		chartView.repaint();
	}
}
