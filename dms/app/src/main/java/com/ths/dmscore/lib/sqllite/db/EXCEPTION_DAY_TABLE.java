package com.ths.dmscore.lib.sqllite.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.CycleInfo;
import com.ths.dmscore.dto.view.ValueItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DMSCursor;
import com.ths.dmscore.util.DateUtils;

/**
 *
 * table exception fullDate define from VNM
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class EXCEPTION_DAY_TABLE extends ABSTRACT_TABLE {

	public static final String ID = "ID";
	public static final String DAY_OF = "DAY_OF";
	public static final String TYPE = "TYPE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String EXCEPTION_DAY_TABLE = "EXCEPTION_DAY";

	public EXCEPTION_DAY_TABLE(SQLiteDatabase mDB) {
		this.tableName = EXCEPTION_DAY_TABLE;
		this.columns = new String[] { ID, DAY_OF, TYPE, UPDATE_DATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	/**
	 *
	 * Lay so ngay lam viec hien tai cua thang
	 *
	 * @author: HaiTC3
	 * @param shopId
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getCurrentWorkingDaysOfMonth(String shopId) throws Exception {
		Date dateE = new Date();
		Date dateS = DateUtils.convertToDate(getFirstDayOfOffsetCycle(new Date(), 0),DateUtils.DATE_FORMAT_DATE);
//		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(dateS, dateE);
		int workDays = getNumCurrentDaysOfCycle(dateE);
		Cursor c = null;
		try {
			SimpleDateFormat sfd = DateUtils.defaultSqlDateFormat;
			String sDateE = sfd.format(dateE);
			String sDateS = sfd.format(dateS);
//			String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY"
//					+ " WHERE dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= ?"
//					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= ? "
//					+ " and SHOP_ID = ? ";
			String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY, CYCLE CY"
					+ " WHERE 1 = 1"
					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= ?"
					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= dayInOrder(CY.BEGIN_DATE, 'start of fullDate') "
					+ " and dayInOrder(CY.BEGIN_DATE, 'start of fullDate') <= ? "
					+ " and dayInOrder(CY.END_DATE, 'start of fullDate') >= ? "
					+ " and SHOP_ID = ? ";
			c = this.rawQuery(sqlQuery, new String[] { sDateE, sDateE,sDateS, shopId });
			if (c.moveToFirst()) {
				do {
//					String ds = c.getString(c.getColumnIndex("DAY_OFF"));
//					Date dd = sfd.parse(ds);
//					if (!DateUtils.isSunday(dd)) {
						workDays--;
//					}
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return workDays >= 0 ? workDays : 0;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: TuanLT
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
//	public int getWorkingDaysOfMonthRecursive(String shopId) throws Exception {
//		Date dateE = new Date();
//		Date dateS = DateUtils.getStartTimeOfMonth(dateE);
//		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(dateS, dateE);
//		if (!StringUtil.isNullOrEmpty(shopId)) {
//			Cursor c = null;
//			// de quy lay ngay nghi len shop cha
//			Cursor cShop = null;
//			try {
//				SimpleDateFormat sfd = DateUtils.defaultSqlDateFormat;
//				String sDateE = sfd.format(dateE);
//				String sDateS = sfd.format(dateS);
//				String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY" + " WHERE dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= ?"
//						+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= ? " + " and SHOP_ID = ? ";
//				c = this.rawQuery(sqlQuery, new String[] { sDateE, sDateS, shopId });
//				if (c.moveToFirst()) {
//					do {
//						workDays--;
//					} while (c.moveToNext());
//				} else {
//
//					StringBuffer sqlShop = new StringBuffer();
//					sqlShop.append("	SELECT	");
//					sqlShop.append("	    PARENT_SHOP_ID	");
//					sqlShop.append("	FROM	");
//					sqlShop.append("	    SHOP sh	");
//					sqlShop.append("	WHERE	");
//					sqlShop.append("	    sh.STATUS = 1	");
//					sqlShop.append("	    AND sh.SHOP_ID = ?	");
//					sqlShop.append("	    AND not EXISTS (	");
//					sqlShop.append("	        SELECT	");
//					sqlShop.append("	            1	");
//					sqlShop.append("	        FROM	");
//					sqlShop.append("	            EXCEPTION_DAY ex	");
//					sqlShop.append("	        WHERE	");
//					sqlShop.append("	            ex.SHOP_ID = sh.SHOP_ID	");
//					sqlShop.append("	    )	");
//
//					String[] params = { shopId };
//					cShop = this.rawQuery(sqlShop.toString(), params);
//					if (cShop != null) {
//						if (cShop.moveToFirst()) {
//							do {
//								String parentId = cShop.getString(cShop.getColumnIndex("PARENT_SHOP_ID"));
//								workDays = getWorkingDaysOfMonthRecursive(parentId);
//							} while (cShop.moveToNext());
//						}
//					}
//
//				}
//			} catch (Exception e) {
//				throw e;
//			} finally {
//				try {
//					if (c != null) {
//						c.close();
//					}
//					if (cShop != null) {
//						cShop.close();
//					}
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
//			}
//		}
//		return workDays >= 0 ? workDays : 0;
//	}

	/**
	 *
	 * get number fullDate working in month
	 *
	 * @author: HaiTC3
	 * @param shopId
	 * @return
	 * @return: int
	 * @throws:
	 */
//	public int getWorkingDaysOfMonth() {
//		Date dateE = new Date();
//		Date dateS = DateUtils.getStartTimeOfMonth(dateE);
//		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(dateS, dateE);
//		Cursor c = null;
//		try {
//			SimpleDateFormat sfd = DateUtils.defaultSqlDateFormat;
//			String sDateE = sfd.format(dateE);
//			String sDateS = sfd.format(dateS);
//			String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY"
//					+ " WHERE dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= ?"
//					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= ? ";
//			c = this.rawQuery(sqlQuery, new String[] { sDateE, sDateS });
//			if (c.moveToFirst()) {
//				do {
//					String ds = c.getString(c.getColumnIndex("DAY_OFF"));
//					Date dd = sfd.parse(ds);
//					if (!DateUtils.isSunday(dd)) {
//						workDays--;
//					}
//				} while (c.moveToNext());
//			}
//		} catch (Exception e) {
//		} finally {
//			try {
//				if (c != null) {
//					c.close();
//				}
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}
//		return workDays >= 0 ? workDays : 0;
//	}

	/**
	 * Lay danh sach ngay nghi Exception Day
	 * @author: yennth16
	 * @since: 15:50:48 29-06-2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getListExceptionDayInCycle (String shopId) throws Exception {
		ArrayList<String> listExcepTionDay = new ArrayList<String>();
		String startOfMonth = getFirstDayOfOffsetCycle(new Date(), 0);
		String lastOfMonth = getEndDayOfOffsetCycle(new Date(), 0);
		String sqlQuery = "SELECT distinct dayInOrder(DAY_OFF) DAY_OFF FROM EXCEPTION_DAY"
				+ " WHERE 1 = 1"
				+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF) >= ? "
				+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF) <= ? "
				+ " and SHOP_ID = ? ";
		Cursor c = null;

		try {
			c = this.rawQuery(sqlQuery, new String[] { startOfMonth, lastOfMonth, shopId});
			if (c.moveToFirst()) {
				do {
					String ds = CursorUtil.getString(c, "DAY_OFF");
					listExcepTionDay.add(ds);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listExcepTionDay;
	}

	/**
	 * getListExceptionDay
	 * @author:
	 * @since: 15:51:15 29-06-2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @return
	 */
	public ArrayList<String> getListExceptionDay() {
		ArrayList<String> listExcepTionDay = new ArrayList<String>();
		String sqlQuery = "SELECT distinct dayInOrder(DAY_OFF) DAY_OFF FROM EXCEPTION_DAY";
		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery, null);
			if (c.moveToFirst()) {
				do {
					String ds = CursorUtil.getString(c, "DAY_OFF");
					listExcepTionDay.add(ds);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listExcepTionDay;
	}

	/**
	 *
	*  Check exception fullDate
	*  @author: Nguyen Thanh Dung
	*  @return
	*  @return: int
	*  @throws:
	 */
	public boolean checkExceptionDay(String date) {
		boolean result = false;
		Cursor c = null;
		try {
			String sqlQuery = "SELECT * FROM EXCEPTION_DAY WHERE dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') = dayInOrder(?, 'start of fullDate')";
			c = this.rawQuery(sqlQuery, new String[] { date });
			if (c.moveToFirst()) {
				result = true;
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}

			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return result;
	}

	 /**
	 * Lay so ngay lam viec ke hoach trong thang
	 * Tru di so ngay nghi
	 * @author: Tuanlt11
	 * @param date
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
	public int getPlanWorkingDaysOfMonth(Date date, String shopId) throws Exception {
//		Date dateS = DateUtils.getStartTimeOfMonth(dayInOrder);
//		// lay ngay lam viec cuoi thang
//		Calendar cal=Calendar.getInstance();
//		cal.setTime(dayInOrder);
//		cal.set(Calendar.DAY_OF_MONTH, 1);
//		cal.add(Calendar.MONTH, 1);
//		cal.add(Calendar.DATE, -1);
//		Date dateE =  cal.getTime();

//		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(dateS, dateE);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		int workDays = getNumDaysOfCycle(date);
		Cursor c = null;
		try {
//			SimpleDateFormat sfd = DateUtils.defaultSqlDateFormat;
//			String sDateE = sfd.format(dateE);
//			String sDateS = sfd.format(dateS);
			String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY, CYCLE CY"
					+ " WHERE 1 = 1"
					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= dayInOrder(CY.END_DATE, 'start of fullDate') "
					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= dayInOrder(CY.BEGIN_DATE, 'start of fullDate') "
					+ " and dayInOrder(CY.BEGIN_DATE, 'start of fullDate') <= ? "
					+ " and dayInOrder(CY.END_DATE, 'start of fullDate') >= ? "
					+ " and SHOP_ID = ? ";
			c = this.rawQuery(sqlQuery, new String[] { date_now, date_now, shopId });
			if (c.moveToFirst()) {
				do {
//					String ds = c.getString(c.getColumnIndex("DAY_OFF"));
//					Date dd = sfd.parse(ds);
//					if (!DateUtils.isSunday(dd)) {
						workDays--;
//					}
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return workDays >= 0 ? workDays : 0;
	}

	/**
	 * Lay so ngay nghi theo shop
	 *
	 * @author: dungdq3
	 * @since: 9:56:01 AM Apr 7, 2015
	 * @return: ArrayList<String>
	 * @throws:
	 * @param shopID
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getListExceptionDay(String shopID) throws Exception {
		ArrayList<String> listExcepTionDay = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		String sqlQuery = "SELECT distinct DAY_OFF FROM EXCEPTION_DAY where 1 = 1 and shop_id = ?";
		params.add(shopID);
		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery, params);
			if (c.moveToFirst()) {
				do {
					String ds = CursorUtil.getString(c, "DAY_OFF");
					listExcepTionDay.add(ds);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listExcepTionDay;
	}

	/**
	 * lay so ngay nghi phep tinh tu ngay orderdate
	 *
	 * @author: dungdq3
	 * @since: 10:01:06 AM Apr 7, 2015
	 * @return: String
	 * @throws:
	 * @param shopID
	 * @param orderDate: ngay dat hang
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getMaxDayOrderApproved(String shopID, String orderDate) throws Exception {
		ArrayList<String> listExcepTionDay = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT DAY_OFF ");
		varname1.append("FROM   EXCEPTION_DAY ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND Substr(day_off, 1, 10) >= Substr(?, 1, 10) ");
		params.add(orderDate);
		varname1.append("       AND shop_id = ? ");
		params.add(shopID);
		varname1.append("ORDER  BY day_off;");
		Cursor c = null;
		try {
			c = this.rawQueries(varname1.toString(), params);
			if (c.moveToFirst()) {
				do {
					String ds = CursorUtil.getString(c, "DAY_OFF");
					listExcepTionDay.add(ds);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listExcepTionDay;

	}


	 /**
	 * Lay ngay nghi giua 2 ngay de cong vao ngay sysMaxdayReturn
	 * @author: Tuanlt11
	 * @param createDate
	 * @param serverDate
	 * @param sysMaxdayReturn
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
	public int addCurrentWorkingDaysOfMonth(Date serverDate, int sysMaxdayReturn) throws Exception {
		String previousDay = DateUtils.getOffsetDay(DateUtils.DATE_FORMAT_NOW, serverDate, -sysMaxdayReturn);
		Cursor c = null;
		try {
			SimpleDateFormat sfd = DateUtils.defaultSqlDateFormat;
			String strserverDate = sfd.format(serverDate);
			String sqlQuery = "SELECT distinct * FROM EXCEPTION_DAY"
					+ " WHERE dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') <= ?"
					+ " and dayInOrder(EXCEPTION_DAY.DAY_OFF, 'start of fullDate') >= ? "
					+ " and SHOP_ID = ? ";
			c = this.rawQuery(sqlQuery, new String[] { strserverDate,previousDay, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId() });
			if (c.moveToFirst()) {
				do {
					// truong hop roi trong khoang tu previousDay toi serverDate neu co ngay nghi thi cong vao sysMaxdayReturn 1
					sysMaxdayReturn++;
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return sysMaxdayReturn >= 0 ? sysMaxdayReturn : 0;
	}

	 /**
	 * Lay so ngay lam viec theo chu ki
	 * @author: Tuanlt11
	 * @param date
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
	private int getNumDaysOfCycle(Date date) throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date);
		Date bg = DateUtils.convertToDate(cycleInfo.beginDate, DateUtils.DATE_FORMAT_NOW);
		Date end = DateUtils.convertToDate(cycleInfo.endDate, DateUtils.DATE_FORMAT_NOW);
		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(bg, end);
		return workDays >= 0 ? workDays : 0;
	}

	/**
	 * Lay so ngay lam viec toi hien tai
	 *
	 * @author: Tuanlt11
	 * @param date
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	 */
	private int getNumCurrentDaysOfCycle(Date date) throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date);
		String beginDate = cycleInfo.beginDate;
		Date bg = DateUtils.convertToDate(beginDate, DateUtils.DATE_FORMAT_NOW);
		Date end = date;
		int workDays = DateUtils.getWorkingDaysBetweenTwoDates(bg, end);
		return workDays >= 0 ? workDays : 0;
	}



	 /**
	 * Lay ngay bat dau cua offset chu ki
	 * @author: Tuanlt11
	 * @param date
	 * @param offset
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	*/
	public String getFirstDayOfOffsetCycle(Date date, int offset) throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date, offset);
		String beginDate = cycleInfo.beginDate;
		return beginDate;
	}

	/**
	 * Lay ngay ket thuc cua offset chu ki
	 *
	 * @author: Tuanlt11
	 * @param date
	 * @param offset
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	 */
	public String getEndDayOfOffsetCycle(Date date, int offset) throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date, offset);
		String endDate = cycleInfo.endDate;
		return endDate;
	}

	 /**
	 * Lay thu tu chu ki theo mot ngay truyen vao
	 * @author: Tuanlt11
	 * @param date
	 * @param offset: so luong chu ki truoc hoac sau
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	*/
	public int getNumCycle(Date date, int offset)
			throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date, offset);
		int numDay = cycleInfo.num;
		return numDay;
	}

	/**
	 * Lay cycle id cua chu ky hien tai
	 * @author: yennth16
	 * @since: 08:37:58 25-06-2015
	 * @return: String
	 * @throws:
	 * @param date
	 * @param offset
	 * @return
	 * @throws Exception
	 */
	public long getCycleId(Date date, int offset) throws Exception {
		CycleInfo cycleInfo = getCycleInfoOfDate(date, offset);
		long cycleId = cycleInfo.cycleId;
		return cycleId;
	}

	/**
	 * Lay thu tu chu ki theo mot ngay truyen vao
	 *
	 * @author: Tuanlt11
	 * @param date
	 * @param offset
	 *            : so luong chu ki truoc hoac sau
	 * @return
	 * @throws Exception
	 * @return: String
	 * @throws:
	 */
	public ValueItemDTO getCycleInfo(Date date, int offset) throws Exception {
		ValueItemDTO value  = new ValueItemDTO();
		CycleInfo cycleInfo = getCycleInfoOfDate(date, offset);
		value.id = String.valueOf(cycleInfo.cycleId);
		value.name = String.valueOf(cycleInfo.year);
		value.value = String.valueOf(cycleInfo.num);
		return value;
	}

	 /**
	 * Lay thong tin cacchu ki bat dau nam trong khoang mot chu ky nhat dinh
	 * @author: Tuanlt11
	 * @param fromCycleId
	 * @param toCycleId
	 * @return
	 * @throws Exception
	 * @return: ValueItemDTO
	 * @throws:
	*/
	public ArrayList<ValueItemDTO> getCycleInfo(long fromCycleId, long toCycleId) throws Exception {
		ArrayList<ValueItemDTO> value  = new ArrayList<ValueItemDTO>();
		Cursor c = null;
		try {
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    CYCLE	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	1 = 1	");
			sqlObject.append("	            AND cycle_id >= ?	");
			paramsObject.add(""+fromCycleId);
			sqlObject.append("	            AND cycle_id <= ?	");
			paramsObject.add(""+toCycleId);

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c.moveToFirst()) {
				do {
					ValueItemDTO temp = new ValueItemDTO();
					temp.id = CursorUtil.getString(c, "CYCLE_ID");
					temp.name = CursorUtil.getString(c, "YEAR");
					temp.value = CursorUtil.getString(c, "NUM");
					value.add(temp);
				} while (c.moveToNext());
//				numDay = CursorUtil.getInt(c, "NUM");

			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return value;
	}

	/**
	 * Thong tin chu ky hien tai + 2 chu ky trc
	 * @author: yennth16
	 * @since: 14:51:14 20-07-2015
	 * @return: ArrayList<ValueItemDTO>
	 * @throws:
	 * @param fromCycleId
	 * @param toCycleId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<ValueItemDTO> getCycleInfoReport(long fromCycleId, long toCycleId) throws Exception {
		ArrayList<ValueItemDTO> value  = new ArrayList<ValueItemDTO>();
		Cursor c = null;
		try {
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    *	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    CYCLE	");
			sqlObject.append("	WHERE	");
			sqlObject.append("	1 = 1	");
			sqlObject.append("	            AND cycle_id >= ?	");
			paramsObject.add(""+fromCycleId);
			sqlObject.append("	            AND cycle_id <= ?	");
			paramsObject.add(""+toCycleId);

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c.moveToFirst()) {
				do {
					ValueItemDTO temp = new ValueItemDTO();
					temp.id = CursorUtil.getString(c, "CYCLE_ID");
					temp.name = CursorUtil.getString(c, "CYCLE_NAME");
					temp.value = CursorUtil.getString(c, "NUM");
					value.add(temp);
				} while (c.moveToNext());

			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return value;
	}

	/**
	 *
	 * @author: yennth16
	 * @since: 11:34:24 21-07-2015
	 * @return: long
	 * @throws:
	 * @param date
	 * @param offset
	 * @return
	 * @throws Exception
	 */
	public String getCycleName(String cycleId)
			throws Exception {
		String cycleName = Constants.STR_BLANK;
		Cursor c = null;
		try {
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    CYCLE_NAME	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    CYCLE	");
			sqlObject.append("	WHERE 1 = 1");
			sqlObject.append("	AND CYCLE_ID = ?	");
			paramsObject.add(cycleId);
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c.moveToFirst()) {
				cycleName = CursorUtil.getString(c, "CYCLE_NAME");
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return cycleName;
	}
	
	public CycleInfo getCycleInfoOfDate(Date date) throws Exception {
		String dateStr = DateUtils.formatDate(date, DateUtils.DATE_STRING_YYYY_MM_DD);
		return getCycleInfoOfDate(dateStr);
	}
	
	public CycleInfo getCycleInfoOfDate(Date date, int offset) throws Exception {
		String dateStr = DateUtils.formatDate(date, DateUtils.DATE_STRING_YYYY_MM_DD);
		return getCycleInfoOfDate(dateStr, offset);
	}
	
	public CycleInfo getCycleInfoOfDate(String dateStr) throws Exception {
		return getCycleInfoOfDate(dateStr, 0);
	}
	
	public CycleInfo getCycleInfoOfDate(String dateStr, int offset) throws Exception {
		CycleInfo cycleInfo = null;
		List<CycleInfo> lstCycle = getListCycleInfoOfDate(dateStr, offset, 1);
		if (!lstCycle.isEmpty()) {
			cycleInfo = lstCycle.get(0);
		}
		
		if (cycleInfo == null) {
			cycleInfo = new CycleInfo();
		}
		return cycleInfo;
	}
	
	public List<CycleInfo> getListCycleInfoOfDate(Date beginDate, int offset, int numCycleGet) throws Exception {
		String date = DateUtils.formatDate(beginDate, DateUtils.DATE_STRING_YYYY_MM_DD);
		return getListCycleInfoOfDate(date, offset, numCycleGet);
	}
	
	public List<CycleInfo> getListCycleInfoOfDate(String date, int offset, int numCycleGet) throws Exception {
		final List<CycleInfo> lstCycle = new ArrayList<CycleInfo>();
		String orderStr = "";
		String coditionStr = "";
		if (offset >= 0) {
			orderStr = "asc";
			coditionStr = "AND (substr(BEGIN_DATE, 1, 10) >= ? or (substr(BEGIN_DATE, 1, 10) <= ? AND substr(END_DATE, 1, 10) >= ?))";
		} else{
			orderStr = "desc";
			coditionStr = "AND (substr(BEGIN_DATE, 1, 10) <= ? or (substr(BEGIN_DATE, 1, 10) <= ? AND substr(END_DATE, 1, 10) >= ?))";
		}
		
		offset = Math.abs(offset);
		
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    CYCLE_ID, CYCLE_CODE, CYCLE_NAME, YEAR, NUM, BEGIN_DATE, END_DATE, STATUS	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    CYCLE	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND status = 1	");
		sqlObject.append(coditionStr);
		paramsObject.add(date);
		paramsObject.add(date);
		paramsObject.add(date);
		sqlObject.append("	ORDER BY BEGIN_DATE " + orderStr);
		sqlObject.append("	LIMIT " + numCycleGet + " OFFSET " + offset);
		
		cursorRunner.query(sqlObject.toString(), paramsObject, new ICursorActionQuery() {
			
			@Override
			public void onMoveCursor(DMSCursor c) throws Exception {
				CycleInfo info = new CycleInfo();
				info.initData(c.cursor);
				lstCycle.add(info);
			}
		});
		return lstCycle;
	}

}