/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PRODUCT_INFO_TABLE;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.lib.sqllite.db.PRICE_TABLE;
import com.ths.dmscore.lib.sqllite.db.PRODUCT_TABLE;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_PROGRAME_TABLE;
import com.ths.dmscore.lib.sqllite.db.STOCK_TOTAL_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * data cho man hinh tim kiem san pham them vao don hang
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class FindProductSaleOrderDetailViewDTO implements Serializable {
	/**
	 *
	 */
	public static final int TYPE_OBJECT_SAVE_FILTER = 1;
	public static final int TYPE_OBJECT_SAVE_NOT_FILTER = 2;
	private static final long serialVersionUID = 1L;
	// sale order detail info
	public SaleOrderDetailDTO saleOrderDetail = new SaleOrderDetailDTO();
	// product code
	public String productCode;
	// product name
	public String productName;
	// convfact
	public int convfact;
	// uom2
	public String uom2;
	// stock
	public String stock;
	// QUANTITY : so luong ton kho
	public int quantity;
	public String quantity_format;
	// available_quantity : so luong co the dat hang
	public long available_quantity = 0;
	// available_quantity format : so luong co the dat hang da format
	public String available_quantity_format;
	// mhTT
	public int mhTT;
	// number product when choose
	public String numProduct;
	// su dung cho man hinh chon SKU them vao danh gia NVBH cua GSNPP
	public boolean isSelected = false;

	// promotion programe code (automatic)
	public String promotionProgrameCode;
	// promotionId
	public String promotionProgrameId;
	//promotion program type code
	public String programTypeCode;

	// flag check has promotion programe code
	public boolean hasPromotionProgrameCode = false;

	// flag check display programe code
	public boolean hasDisplayProgrameCode = false;

	// has choose programe
	public boolean hasSelectPrograme;

	// check type object
	public int typeObject;

	// GSNPP REQUEST SALE
	public int gsnppRequest = 0;

	// gross weight of product
	public double grossWeight = 0;

	//Thong tin nganh hang
	public String productInfoCode;

	public FindProductSaleOrderDetailViewDTO() {
		numProduct = "0";
		uom2 = "";
		available_quantity_format = "";
		isSelected = false;
		hasSelectPrograme = false;
		typeObject = TYPE_OBJECT_SAVE_FILTER;
		productInfoCode = "";
	}

	/**
	 *
	 * init object with cursor
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initSaleOrderDetailObjectFromGetProductStatement(Cursor c) {
		saleOrderDetail.quantity = CursorUtil.getInt(c, STOCK_TOTAL_TABLE.QUANTITY);
		convfact = CursorUtil.getInt(c, PRODUCT_TABLE.CONVFACT);
		saleOrderDetail.priceId = CursorUtil.getLong(c, PRICE_TABLE.PRICE_ID);
		saleOrderDetail.productId = CursorUtil.getInt(c, PRODUCT_TABLE.PRODUCT_ID);
		productCode = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_CODE);
		productName = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_NAME);

		// uom2
		uom2 = CursorUtil.getString(c, PRODUCT_TABLE.UOM2);

		saleOrderDetail.price = CursorUtil.getDouble(c, PRICE_TABLE.PRICE);
		saleOrderDetail.packagePrice = CursorUtil.getDouble(c, PRICE_TABLE.PACKAGE_PRICE);
		saleOrderDetail.priceNotVat = CursorUtil.getDouble(c, PRICE_TABLE.PRICE_NOT_VAT);
		saleOrderDetail.packagePriceNotVat = CursorUtil.getDouble(c, PRICE_TABLE.PACKAGE_PRICE_NOT_VAT);
		saleOrderDetail.vat = CursorUtil.getDouble(c, PRICE_TABLE.VAT);

		grossWeight = CursorUtil.getDouble(c, PRODUCT_TABLE.GROSS_WEIGHT);

		// so luong ton kho
		quantity = CursorUtil.getInt(c, STOCK_TOTAL_TABLE.QUANTITY);
		quantity_format = GlobalUtil.formatNumberProductFlowConvfact(quantity, convfact);

		// so luong toi da co the dat hang
		available_quantity = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		available_quantity_format = GlobalUtil.formatNumberProductFlowConvfact(available_quantity, convfact);

		// stock
		if (quantity > 0 && convfact > 0) {

			stock = StringUtil.parseAmountMoney(quantity / convfact) + "/"
					+ StringUtil.parseAmountMoney(quantity % convfact);
		} else {
			stock = "0/0";
		}

		promotionProgrameCode = CursorUtil.getString(c, PROMOTION_PROGRAME_TABLE.PROMOTION_PROGRAM_CODE);
		promotionProgrameId = CursorUtil.getString(c, PROMOTION_PROGRAME_TABLE.PROMOTION_PROGRAM_ID);
		if (!StringUtil.isNullOrEmpty(promotionProgrameCode)) {
			String[] strProgrameCode = promotionProgrameCode.split(",");
			String[] strProgrameId = promotionProgrameId.split(",");
			// gan ds chuong trinh KM lai
			promotionProgrameCode = "";
			promotionProgrameId = "";
			for (int i = 0, size = strProgrameCode.length; i < size; i++) {
				// lay cac bien de kiem tra san pham co thuoc CTKM hay khong
				int COKHAIBAO1 = 0;
				int COKHAIBAO2 = 0;
				int COKHAIBAO3 = 0;
				int COKHAIBAO4 = 0;
				int COKHAIBAO5 = 0;
				int COKHAIBAO6 = 0;
				int COKHAIBAO7 = 0;
				int COKHAIBAO8 = 0;
				int COKHAIBAO9 = 0;
				int COKHAIBAO10 = 0;

				COKHAIBAO1 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO1").split(",")[i]);
				COKHAIBAO2 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO2").split(",")[i]);
				COKHAIBAO3 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO3").split(",")[i]);
				COKHAIBAO4 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO4").split(",")[i]);
				COKHAIBAO5 =Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO5").split(",")[i]);
				COKHAIBAO6 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO6").split(",")[i]);
				COKHAIBAO7 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO7").split(",")[i]);
				COKHAIBAO8 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO8").split(",")[i]);
				COKHAIBAO9 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO9").split(",")[i]);
				COKHAIBAO10 = Integer.valueOf(CursorUtil.getString(c, "COKHAIBAO10").split(",")[i]);
				boolean result = false;
				if (COKHAIBAO1 == 0) {
					result = true;
				} else {
					result = COKHAIBAO2 > 0 ? true : false;
				}
				if (result) {
					if (COKHAIBAO3 == 0) {
						result = true;
					} else {
						result = COKHAIBAO4 > 0 ? true : false;
					}
				} else {
					hasPromotionProgrameCode = false;
				}
				if (result) {
					boolean result1 = false;
					if (COKHAIBAO5 == 0) {
						result1 = true;
					} else {
						result1 = COKHAIBAO6 == COKHAIBAO5 ? true : false;
					}

					boolean result2 = false;
					if (COKHAIBAO7 == 0) {
						result2 = true;
					} else {
						result2 = COKHAIBAO8 == COKHAIBAO7 ? true : false;
					}

					boolean result3 = false;
					if (COKHAIBAO9 == 0) {
						result3 = true;
					} else {
						result3 = COKHAIBAO10 > 0 ? true : false;
					}
					result = result1 && result2 && result3;
				} else {
					hasPromotionProgrameCode = false;
				}
				hasPromotionProgrameCode = result;
				if(hasPromotionProgrameCode){
					if (!StringUtil.isNullOrEmpty(promotionProgrameCode)) {
						promotionProgrameCode = promotionProgrameCode + ","
								+ strProgrameCode[i];
					} else {
						promotionProgrameCode = strProgrameCode[i];
					}
					if (!StringUtil.isNullOrEmpty(promotionProgrameId)) {
						promotionProgrameId = promotionProgrameId + ","
								+ strProgrameId[i];
					} else {
						promotionProgrameId = strProgrameId[i];
					}
				}
			}
		} else {
			hasPromotionProgrameCode = false;
		}

		if(!hasPromotionProgrameCode){
			promotionProgrameCode = "";
		}

		int check = CursorUtil.getInt(c, "DISPLAY_PROGRAME_CODE", 0);
		if (check == 1) {
			hasDisplayProgrameCode = true;
		} else {
			hasDisplayProgrameCode = false;
		}

		mhTT = CursorUtil.getInt(c, "TT");
		gsnppRequest = CursorUtil.getInt(c, "GSNPP_REQUEST");
		productInfoCode = CursorUtil.getString(c, PRODUCT_INFO_TABLE.PRODUCT_INFO_CODE);
		programTypeCode = CursorUtil.getString(c, PROMOTION_PROGRAME_TABLE.TYPE);
		saleOrderDetail.catId = CursorUtil.getInt(c, "CAT_ID");
	}
	
}