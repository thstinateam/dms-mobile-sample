/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ths.dmscore.global.GlobalInfo;

/**
 * BigDecimalRound.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 2:03:09 PM Apr 29, 2015
 */
public class BigDecimalRound implements Serializable {

	private BigDecimal value;

	private static final long serialVersionUID = -8014201198305811763L;

	public BigDecimalRound(String val){
		value = new BigDecimal(val);
	}

	public BigDecimalRound(double val){
		value = new BigDecimal(val);
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public double toDouble() {
		return value.doubleValue();
	}

	public long toLong() {
		return value.longValue();
	}

	public String toString() {
		return value.toString();
	}

	public int toInteger() {
		return value.intValue();
	}

	public BigDecimalRound multiply(BigDecimal multiplyValue){
		// clone ra mot doi tuong khac de ko thay doi gia tri big decimal khi goi ham multiply
		BigDecimalRound p = new BigDecimalRound(value.doubleValue());
		p.value = value.multiply(multiplyValue);
		return p;
	}

	public BigDecimalRound divide(BigDecimal divideValue) {
		int scale = GlobalInfo.getInstance().getSysDigitDecimal();
		if(GlobalInfo.getInstance().getSysNumRoundingPromotion() == 2) {
			// lam tron xuong
			value = value.divide(divideValue, scale, RoundingMode.DOWN);
		} else if (GlobalInfo.getInstance().getSysNumRoundingPromotion() == 1) {
			// lam tron len
			value = value.divide(divideValue, scale, RoundingMode.UP);
		} else if (GlobalInfo.getInstance().getSysNumRoundingPromotion() == 3) {
			value = value.divide(divideValue, scale, RoundingMode.HALF_EVEN);
		}
		return this;
	}

	public BigDecimalRound subtract(BigDecimal substractValue) {
		BigDecimalRound p = new BigDecimalRound(value.doubleValue());
		p.value = value.subtract(substractValue);
		return p;
	}

	public BigDecimalRound add(BigDecimal addedValue) {
		BigDecimalRound p = new BigDecimalRound(value.doubleValue());
		p.value = value.add(addedValue);
		return p;
	}

	public BigDecimalRound setScale(){
		int scale = GlobalInfo.getInstance().getSysDigitDecimal();
		if(GlobalInfo.getInstance().getSysNumRoundingPromotion() == 2) {
			// lam tron xuong
			value = value.setScale(scale, RoundingMode.DOWN);
		} else if (GlobalInfo.getInstance().getSysNumRoundingPromotion() == 1) {
			// lam tron len
			value = value.setScale(scale, RoundingMode.UP);
		} else if (GlobalInfo.getInstance().getSysNumRoundingPromotion() == 3) {
			value = value.setScale(scale, RoundingMode.HALF_EVEN);
		}
		return this;
	}

}
