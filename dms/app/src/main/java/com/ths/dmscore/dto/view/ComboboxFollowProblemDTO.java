/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.List;

/**
 * 
 * Mo ta cho class
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class ComboboxFollowProblemDTO {
	public List<DisplayProgrameItemDTO> listNVBH;
	public List<DisplayProgrameItemDTO> listStatus;
	public List<DisplayProgrameItemDTO> listTypeProblem;	
}
