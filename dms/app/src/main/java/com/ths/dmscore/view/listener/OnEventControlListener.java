/**
 * Copyright 2011 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.listener;

import android.view.View;

/**
 * @author: AnhND
 * @since : May 21, 2011
 * 
 */
public interface OnEventControlListener{
	void onEvent( int eventType, View control, Object data);
}
