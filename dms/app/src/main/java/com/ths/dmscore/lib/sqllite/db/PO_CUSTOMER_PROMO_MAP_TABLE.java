package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.POCustomerPromoMapDTO;

/**
 * Table cua bang luu thong tin KM dat hay ko dat
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PO_CUSTOMER_PROMO_MAP_TABLE extends ABSTRACT_TABLE {
	public static final String PO_CUSTOMER_PROMO_MAP_ID = "PO_CUSTOMER_PROMO_MAP_ID";
	public static final String PO_CUSTOMER_DETAIL_ID = "PO_CUSTOMER_DETAIL_ID";
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	public static final String STATUS = "STATUS";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	public static final String TABLE_NAME = "PO_CUSTOMER_PROMO_MAP";

	public PO_CUSTOMER_PROMO_MAP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] {PO_CUSTOMER_PROMO_MAP_ID,PO_CUSTOMER_DETAIL_ID,PROGRAM_CODE,STATUS,STAFF_ID,PO_CUSTOMER_ID,SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		POCustomerPromoMapDTO promoDto = (POCustomerPromoMapDTO) dto;
		return insert(null, initDataRow(promoDto));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(POCustomerPromoMapDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PO_CUSTOMER_PROMO_MAP_ID, dto.poCustomerPromoMapId);
		editedValues.put(PO_CUSTOMER_DETAIL_ID, dto.poCustomerDetailId);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(STATUS, dto.status);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(PO_CUSTOMER_ID, dto.poCustomerId);
		return editedValues;
	}

	 /**
	 * Xoa tat ca cac km lien quan toi don hang
	 * @author: Tuanlt11
	 * @param saleOrderId
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int deleteAllPromoMapOfPO(long poCustomerId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(poCustomerId));

		return delete(PO_CUSTOMER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}


}
