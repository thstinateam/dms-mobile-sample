/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.trainingplan.ListTrainingRateDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateResultDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateDTO;
import com.ths.dmscore.util.DateUtils;

/**
 * 
 * TRANING_RATE_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:40:28 17-11-2014
 */
public class TRAINING_RATE_TABLE extends ABSTRACT_TABLE {
	public static final String TRAINING_RATE_ID = "TRAINING_RATE_ID";
	public static final String SUB_STAFF_ID = "SUB_STAFF_ID";
	public static final String TRAINING_MONTH = "TRAINING_MONTH";
	public static final String CODE = "CODE";
	public static final String SHORT_NAME = "SHORT_NAME";
	public static final String FULL_NAME = "FULL_NAME";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_NAME = "TRAINING_RATE";

	public TRAINING_RATE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { TRAINING_RATE_ID, SUB_STAFF_ID,
				TRAINING_MONTH, CODE, SHORT_NAME, FULL_NAME, ORDER_NUMBER,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * lay danh sach cac tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 10:57:43 AM Nov 17, 2014
	 * @return: TrainingRateDTO
	 * @throws:  
	 * @param data
	 * @return
	 * @throws Exception 
	 */
	public TrainingRateResultDTO getListStandard(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		TrainingRateResultDTO dto = new TrainingRateResultDTO();
		
		Cursor c = null;
		long customerID = data.getLong(IntentConstants.INTENT_CUSTOMER_ID, 160145);
		String month = data.getString(IntentConstants.INTENT_MONTH, "11");
		int staffID = data.getInt(IntentConstants.INTENT_STAFF_ID, 0);
		long criteriaID = data.getLong(IntentConstants.INTENT_DATA, 0);
		
		dto.setCustomerID(customerID);
		dto.setSubStaffID(staffID);
		dto.setTrainingMonth(month);
		ArrayList<String> param = new ArrayList<String>();

		StringBuffer  varname1 = new StringBuffer();
		varname1.append(" SELECT distinct tr.training_rate_id   AS TRAINING_RATE_ID, ");
		varname1.append("       tr.shortcriterianame AS SHORT_NAME, ");
		varname1.append("       trd.training_rate_detail_id AS TRAINING_RATE_DETAIL_ID, ");
		varname1.append("       tr.fullcriterianame  AS FULL_NAME, ");
		varname1.append("       tr.criteriaOrderNum   AS ORDER_NUMBER, ");
		varname1.append("       trd.shortstandardname AS shortStandardName, ");
		varname1.append("       trd.fullstandardname  AS fullStandardName, ");
		varname1.append("       trd.standardOrderNum  AS standardOrderNum, ");
		varname1.append("       trd.is_default_rate   AS defaultRate, ");
		varname1.append("       str.result           AS result, ");
		varname1.append("       str.sup_training_result_id           AS SUP_TRAINING_RESULT_ID, ");
		varname1.append("       stc.sup_training_customer_id             AS SUP_TRAINING_CUSTOMER_ID, ");
		varname1.append("       stc.note             AS NOTE ");
		varname1.append(" FROM   (SELECT tr.training_rate_id, ");
		varname1.append("				tr.short_name   AS shortCriteriaName, ");
		varname1.append("              tr.full_name    AS fullCriteriaName, ");
		varname1.append("              tr.order_number AS criteriaOrderNum ");
		varname1.append("       FROM   training_rate tr ");
		varname1.append("       WHERE  1 = 1 ");
		varname1.append("              AND tr.training_rate_id = ? ");
		param.add(String.valueOf(criteriaID));
		varname1.append("              AND Substr(tr.training_month, 1, 7) = ");
		varname1.append("              Substr(?, 1, 7) ");
		param.add(month);
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append("       AND sup_staff_id = ?) tr ");
			param.add(String.valueOf(staffID));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append("		AND sup_staff_id IN			(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("								AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(staffID));
			varname1.append("								AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			varname1.append("							 	) tr ");
		}
		varname1.append("      LEFT JOIN (SELECT trd.training_rate_id, ");
		varname1.append("                        trd.training_rate_detail_id as training_rate_detail_id, ");
		varname1.append("                        trd.short_name   AS shortStandardName, ");
		varname1.append("                        trd.full_name    AS fullStandardName, ");
		varname1.append("                        trd.order_number AS standardOrderNum, ");
		varname1.append("                        trd.is_default_rate ");
		varname1.append("                 FROM   training_rate_detail trd ");
		varname1.append("                 WHERE  1 = 1 ");
		
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append("                    AND trd.create_staff_id = ? ");
			param.add(String.valueOf(staffID));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append("		AND trd.create_staff_id IN	(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("								AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(staffID));
			varname1.append("								AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		}
		varname1.append("                        AND (( is_default_rate = 0 ");
		varname1.append("                               AND Substr(trd.create_date, 1, 10) = ");
		varname1.append("                                   Substr(?, 1, 10) ");
		param.add(month);
		varname1.append("                                OR ( is_default_rate = 1 ) ))) trd ");
		varname1.append("             ON tr.training_rate_id = trd.training_rate_id ");
		
		
		varname1.append("       left join (SELECT note, ");
		varname1.append("                         training_rate_id, ");
		varname1.append("                         sup_training_plan_id, ");
		varname1.append("                         sup_training_customer_id ");
		varname1.append("                  FROM   sup_training_customer ");
		varname1.append("                  WHERE  customer_id = ? ");
		param.add(String.valueOf(customerID));
		varname1.append("                         AND Substr(create_date, 1, 10) = ");
		varname1.append("                         Substr(?, 1, 10) ");
		param.add(month);
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append("               	and create_staff_id = ?) stc ");
			param.add(String.valueOf(staffID));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append("		AND create_staff_id IN	(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("								AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(staffID));
			varname1.append("								AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			varname1.append("							 ) stc ");
		}
		varname1.append("              ON stc.training_rate_id = tr.training_rate_id ");
		
		varname1.append("       left join (SELECT result, ");
		varname1.append("                         training_rate_detail_id, ");
		varname1.append("                         sup_training_result_id, ");
		varname1.append("                         sup_training_customer_id ");
		varname1.append("                  FROM   sup_training_result ");
		varname1.append("					where 1=1 ");
		varname1.append("                         AND Substr(create_date, 1, 10) = ");
		varname1.append("                         Substr(?, 1, 10) ");
		param.add(month);
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append("                    and create_staff_id = ?) str ");
			param.add(String.valueOf(staffID));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append("		AND create_staff_id IN			(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("								AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(staffID));
			varname1.append("								AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			varname1.append("							 	) str ");
		}
		varname1.append("              ON str.training_rate_detail_id = trd.training_rate_detail_id ");
		varname1.append("              AND str.sup_training_customer_id = stc.sup_training_customer_id ");
		varname1.append("              where 1 = 1 ");
		varname1.append("              group by trd.training_rate_detail_id ");
		varname1.append(" ORDER  BY trd.is_default_rate desc, trd.standardOrderNum ");
		
		try {

			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						dto.initFromCursor(c);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 * lay danh sach cac tieu chi
	 * 
	 * @author: dungdq3
	 * @since: 11:31:59 AM Nov 17, 2014
	 * @return: TrainingRateDTO
	 * @throws:  
	 * @param data
	 * @return
	 * @throws Exception 
	 */
	public ListTrainingRateDTO getListCriteria(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		ListTrainingRateDTO dto = new ListTrainingRateDTO();
		Cursor c = null;
		String month = data.getString(IntentConstants.INTENT_MONTH, DateUtils.now());
		int staffID = data.getInt(IntentConstants.INTENT_STAFF_ID, 0);

		ArrayList<String> param = new ArrayList<String>();

		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT tr.training_rate_id as TRAINING_RATE_ID, ");
		varname1.append("       tr.short_name AS SHORT_NAME, ");
		varname1.append("       tr.full_name  AS FULL_NAME, ");
		varname1.append("       tr.order_number as ORDER_NUMBER ");
		varname1.append("FROM   training_rate tr ");
		varname1.append("WHERE  1 = 1 ");
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append("       AND sup_staff_id = ? ");
			param.add(String.valueOf(staffID));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append("       AND sup_staff_id IN ");
			varname1.append("			(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("				AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(staffID));
			varname1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			
		}
		varname1.append("       AND substr(tr.training_month, 1, 7) = substr(?, 1, 7) ");
		param.add(month);
		varname1.append(" ORDER  BY tr.order_number ");

		try {
			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						TrainingRateDTO rate = new TrainingRateDTO();
						rate.initFromCursor(c);
						dto.addItem(rate);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}
}
