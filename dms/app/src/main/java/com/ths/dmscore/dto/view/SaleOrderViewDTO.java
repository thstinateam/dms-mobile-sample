package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.ABSTRACT_TABLE;
import com.ths.dmscore.lib.sqllite.db.AP_PARAM_TABLE;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_MAP_TABLE;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.PO_PROMOTION_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_LOT_TABLE;
import com.ths.dmscore.util.GlobalUtil;

public class SaleOrderViewDTO implements Serializable{
	private static final long serialVersionUID = 5799692597940574815L;
	public SaleOrderDTO saleOrder = new SaleOrderDTO();
	public CustomerDTO customer = new CustomerDTO();
	//private String customerCode;
	private String description;
	public boolean isChecked;
	//Ngay/gio dat hang cuoi cung
	public String lastOrder;
	// co phai don hang cuoi cung cua khach hang
	public int isFinalOrder;
//	public String customerStreet = "";
	public String priorityCode;
	public ArrayList<RptCttlPayDTO> listRptCttPay = new ArrayList<RptCttlPayDTO>();

	public String getDescription() {
		if (!StringUtil.isNullOrEmpty(description))
			description=description.trim();
		return description;
	}

	public void setDescription(String descrip) {
		description = descrip;
	}
	
	public static List<SaleOrderViewDTO> initListDataFromCursor(Cursor cursor)
			throws Exception {

		List<SaleOrderViewDTO> listData = new ArrayList<SaleOrderViewDTO>();
		if (cursor == null) {
			throw new Exception("Cursor is empty");
		}

		if (cursor.moveToFirst()) {
			do {
				listData.add(SaleOrderViewDTO.initDataFromCursor(cursor));

			} while (cursor.moveToNext());
		}

		return listData;
	}

	public static SaleOrderViewDTO initDataFromCursor(Cursor c)
			throws Exception {
		SaleOrderViewDTO data = new SaleOrderViewDTO();

		data.customer.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		data.customer.setAddress(CursorUtil.getString(c, "ADDRESS"));
		data.customer.setStreet( CursorUtil.getString(c, "STREET"));
		data.customer.setCustomerName(CursorUtil.getString(c, "CUSTOMER_NAME"));
		data.saleOrder.customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		data.customer.customerId = data.saleOrder.customerId;//for edit order -> get product list
		data.customer.lat = CursorUtil.getDouble(c, "CUSTOMER_LAT");
		data.customer.lng = CursorUtil.getDouble(c, "CUSTOMER_LNG");
		data.customer.setCustomerTypeId(CursorUtil.getInt(c, "CUSTOMER_TYPE_ID"));//for edit order -> get product list
		data.customer.setCashierStaffID(CursorUtil.getString(c, "CASHIER_STAFF_ID"));
		data.customer.setDeliverID(CursorUtil.getString(c, "DELIVER_ID"));
		data.saleOrder.cashierId = CursorUtil.getString(c, "CASHIER_ID");
		data.saleOrder.deliveryId = CursorUtil.getString(c, "DELIVERY_ID");
		data.saleOrder.setDiscount(CursorUtil.getDouble(c, "DISCOUNT"));
		data.saleOrder.deliveryDate = CursorUtil.getString(c, "DELIVERY_DATE");
		data.saleOrder.shopId = CursorUtil.getInt(c, "SHOP_ID");
		data.saleOrder.shopCode = CursorUtil.getString(c, "SHOP_CODE");
		data.saleOrder.staffId = CursorUtil.getInt(c, "STAFF_ID");
		data.saleOrder.orderDate = CursorUtil.getString(c, "ORDER_DATE");
		data.saleOrder.approvedDate = CursorUtil.getString(c, SALE_ORDER_TABLE.APPROVED_DATE);
		data.saleOrder.createDate = CursorUtil.getString(c, "CREATE_DATE");
		data.saleOrder.createUser = CursorUtil.getString(c, "CREATE_USER");
		data.saleOrder.setTotal(CursorUtil.getDouble(c, "TOTAL"));
		data.saleOrder.quantity = (CursorUtil.getLong(c, "QUANTITY"));
		data.saleOrder.setAmount(CursorUtil.getDouble(c, "AMOUNT"));
		data.saleOrder.type = CursorUtil.getInt(c, "TYPE");
		data.saleOrder.totalWeight = CursorUtil.getDouble(c, "TOTAL_WEIGHT");
		data.saleOrder.totalDetail = CursorUtil.getInt(c, "TOTAL_DETAIL");
		data.saleOrder.approved = CursorUtil.getInt(c, "APPROVED");
		data.saleOrder.fromPOCustomerId = CursorUtil.getLong(c, "FROM_PO_CUSTOMER_ID");
		data.saleOrder.isVisitPlan = CursorUtil.getInt(c, "IS_VISIT_PLAN");
		data.saleOrder.orderNumber = CursorUtil.getString(c, "ORDER_NUMBER");
		data.saleOrder.refOrderNumber = CursorUtil.getString(c, "REF_ORDER_NUMBER");
		data.saleOrder.saleOrderId = CursorUtil.getLong(c, "SALE_ORDER_ID");
		data.saleOrder.orderType = CursorUtil.getString(c, "ORDER_TYPE");
		data.saleOrder.synState = CursorUtil.getInt(c, ABSTRACT_TABLE.SYN_STATE);
		data.saleOrder.priority = CursorUtil.getLong(c, SALE_ORDER_TABLE.PRIORITY);
		data.priorityCode = CursorUtil.getString(c, AP_PARAM_TABLE.AP_PARAM_CODE);
		data.description = CursorUtil.getString(c, "DESCRIPTION");
		data.saleOrder.routingId = CursorUtil.getLong(c, "ROUTING_ID");
		data.saleOrder.isRewardKS = CursorUtil.getInt(c, SALE_ORDER_TABLE.IS_REWARD_KS) == 1 ? true : false;
		return data;
	}

	public String getOrderDate() {
		return saleOrder.orderDate ;
	}

	public void setOrderDate(String ord_date) {
		saleOrder.orderDate  = ord_date;
	}

//	public String getCustomerCode() {
//		return customerCode;
//	}
//
//	public void setCustomerCode(String customerCode) {
//		this.customerCode = customerCode;
//	}

	public long getCustomerId() {
		return saleOrder.customerId;
	}

	public void setCusomerId(long customerId) {
		saleOrder.customerId = customerId;
	}

	public double getTotal() {
		return saleOrder.getTotal();
	}

	public void setTotal(double total) {
		this.saleOrder.setTotal(total);
	}

	public int getIsVisitPlan() {
		return saleOrder.isVisitPlan;
	}

	public void setIsVisitPlan(int IsVisitPlan) {
		saleOrder.isVisitPlan = IsVisitPlan;
	}

	public String getOrderNumber() {
		return saleOrder.orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		saleOrder.orderNumber = orderNumber;
	}

	public String getRefOrderNumber() {
		return saleOrder.refOrderNumber;
	}

	public void setRefOrderNumber(String reforderNumber) {
		saleOrder.refOrderNumber = reforderNumber;
	}

	public long getSaleOrderId() {
		return saleOrder.saleOrderId;
	}

	public long getFromPOCustomerId() {
		return saleOrder.fromPOCustomerId;
	}

	public void setSaleOrderId(int saleOrderId) {
		saleOrder.saleOrderId = saleOrderId;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null){
		    return false;
		}

		if (this.getClass() != o.getClass()){
		    return false;
		}
		
		if (saleOrder.saleOrderId == ((SaleOrderViewDTO)o).saleOrder.saleOrderId)
			return true;
		return false;
	}

	public JSONArray generateDeleteSaleOrderSql() {
		JSONArray listSql = new JSONArray();

		try {
			JSONObject saleJson = new JSONObject();
			saleJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
			saleJson.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_TABLE.TABLE_NAME);
			
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED, String.valueOf(3), null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.UPDATE_DATE, DateUtils.now(), null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.UPDATE_USER, String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getUserCode()), null));
			saleJson.put(IntentConstants.INTENT_LIST_PARAM, params);
			
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.SALE_ORDER_ID, saleOrder.saleOrderId, null));
			saleJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, detailPara);
			listSql.put(saleJson);
			
			//po
			JSONObject poJson = new JSONObject();
			poJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
			poJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_TABLE.TABLE_NAME);
			
			// ds params
			JSONArray paramsPo = new JSONArray();
			paramsPo.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.APPROVED, String.valueOf(3), null));
			paramsPo.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.UPDATE_DATE, DateUtils.now(), null));
			paramsPo.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.UPDATE_USER, String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getUserCode()), null));
			poJson.put(IntentConstants.INTENT_LIST_PARAM, paramsPo);
			
			// ds params
			JSONArray detailParaPo = new JSONArray();
			detailParaPo.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.FROM_SALE_ORDER_ID, saleOrder.saleOrderId, null));
			poJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, detailParaPo);
			listSql.put(poJson);
		} catch (Exception e) {
		}
		return listSql;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param listSql
	 * @return
	 * @return: JSONArray
	 * @throws:
	*/
	public JSONArray generateDeletePOCustomerSql(JSONArray listSql) {
		try {
			// xoa nhung po promo detail
			JSONObject promoDetailJson = new JSONObject();
			promoDetailJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			promoDetailJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_PROMO_DETAIL_TABLE.TABLE_NAME);
			// ds params
			JSONArray promoDetailArr = new JSONArray();
			promoDetailArr.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_DETAIL_TABLE.PO_CUSTOMER_ID, saleOrder.poId, null));
//			promoDetailJson.put(IntentConstants.INTENT_LIST_PARAM, "");
			promoDetailJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, promoDetailArr);
			listSql.put(promoDetailJson);

			// xoa nhung promo map lien quan toi KM cua PO
			JSONObject promoMapJson = new JSONObject();
			promoMapJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			promoMapJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_PROMO_MAP_TABLE.TABLE_NAME);
			// ds params
			JSONArray promoMapArr = new JSONArray();
			promoMapArr.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.PO_CUSTOMER_ID, saleOrder.poId,
					null));
			promoMapJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM,
					promoMapArr);
			listSql.put(promoMapJson);


			// xoa nhung sale promo
			JSONObject salePromoJson = new JSONObject();
			salePromoJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			salePromoJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_PROMOTION_TABLE.TABLE_NAME);

			// ds params
			JSONArray salePromoDetailArr = new JSONArray();
			salePromoDetailArr.put(GlobalUtil.getJsonColumn(
					PO_PROMOTION_TABLE.PO_CUSTOMER_ID,
					saleOrder.poId, null));
//			salePromoJson.put(IntentConstants.INTENT_LIST_PARAM, "");
			salePromoJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM,
					promoDetailArr);
			listSql.put(salePromoJson);

			// xoa nhung po lot, neu la Vansale
			if(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(this.saleOrder.orderType)){
				JSONObject saleOrderLot = new JSONObject();
				saleOrderLot.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
				saleOrderLot.put(IntentConstants.INTENT_TABLE_NAME,
						PO_CUSTOMER_LOT_TABLE.TABLE_NAME);

				// ds params
				JSONArray saleOrderLotArr = new JSONArray();
				saleOrderLotArr.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_LOT_TABLE.PO_CUSTOMER_ID,
						saleOrder.poId, null));
				saleOrderLot.put(IntentConstants.INTENT_LIST_WHERE_PARAM, saleOrderLotArr);
				listSql.put(saleOrderLot);
			}

			// xoa nhung sale order detail
			JSONObject sdJson = new JSONObject();
			sdJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			sdJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);
			// ds params
			JSONArray sdArr = new JSONArray();
			sdArr.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_ID, saleOrder.poId, null));
//			sdJson.put(IntentConstants.INTENT_LIST_PARAM, "");
			sdJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, sdArr);
			listSql.put(sdJson);

			// xoa sale order
			JSONObject saleJson = new JSONObject();
			saleJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			saleJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_TABLE.PO_CUSTOMER_ID, saleOrder.poId, null));
//			saleJson.put(IntentConstants.INTENT_LIST_PARAM, "");
			saleJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, detailPara);
			listSql.put(saleJson);
		} catch (Exception e) {
		}
		return listSql;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
