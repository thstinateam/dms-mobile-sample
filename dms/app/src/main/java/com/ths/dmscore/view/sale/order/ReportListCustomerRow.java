package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * Row cho man hinh bao cao ct httm cua kh
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportListCustomerRow extends DMSTableRow {

	TextView tvKeyshop;
	TextView tvLevelRegister;
	TextView tvAmountRegister;
	TextView tvAmountDone;
	TextView tvAmountRemain;
	TextView tvQuantityRegister;
	TextView tvQuantityDone;
	TextView tvQuantityRemain;

	public ReportListCustomerRow(Context context) {
		super(context, R.layout.layout_report_list_customer_row, GlobalUtil
				.getInstance().getDPIScreen()
				- GlobalUtil
						.dip2Pixel(Constants.LEFT_MARGIN_TABLE_DIP_SMALL * 8),
				GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {
						R.id.tvAmountRegister, R.id.tvAmountDone });
		setOnClickListener(this);
		initView();
	}

	/**
	 * Khoi tao view
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void initView() {
		tvKeyshop = (TextView) findViewById(R.id.tvKeyshop);
		tvLevelRegister = (TextView) findViewById(R.id.tvLevelRegister);
		tvAmountRegister = (TextView) findViewById(R.id.tvAmountRegister);
		tvAmountDone = (TextView) findViewById(R.id.tvAmountDone);
		tvAmountRemain = (TextView) findViewById(R.id.tvAmountRemain);
		tvQuantityRegister = (TextView) findViewById(R.id.tvQuantityRegister);
		tvQuantityDone = (TextView) findViewById(R.id.tvQuantityDone);
		tvQuantityRemain = (TextView) findViewById(R.id.tvQuantityRemain);
	}

	 /**
	 * Render layout cho row
	 * @author: Tuanlt11
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(ReportKeyshopStaffItemDTO item){
		tvKeyshop.setText(item.keyshop.ksCode);
		tvLevelRegister.setText(item.levelRegister);
		display(tvAmountRegister, item.amountRegister);
		display(tvAmountDone, item.amountDone);
		display(tvAmountRemain, StringUtil.calRemainUsingRound(item.amountRegister, item.amountDone));
		display(tvQuantityRegister, item.quantityRegister);
		display(tvQuantityDone, item.quantityDone);
		display(tvQuantityRemain, StringUtil.calRemain(item.quantityRegister, item.quantityDone));
	}

}
