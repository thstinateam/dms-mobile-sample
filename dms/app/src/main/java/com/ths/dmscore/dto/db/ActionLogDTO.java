/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.ACTION_LOG_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Thong tin cau hinh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class ActionLogDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -6422159737970787147L;
	public static final String TYPE_VISIT = "0";
	public static final String TYPE_VISIT_CLOSING = "1";
	public static final String TYPE_VOTE_DISPLAY = "2";
	public static final String TYPE_REMAIN_PRODUCT = "3";
	public static final String TYPE_ORDER = "4";
	//cho phep dat hang tu xa
	public static final String TYPE_EXCEPTION_ORDER = "5";
	public static final String TYPE_INVENTORY_DEVICE = "6";// kiem ke thiet bi
	public static final String TYPE_RETURN_ORDER = "7";// tra hang vansale
	// id
	public long id;
	// customer
	public CustomerDTO aCustomer = new CustomerDTO();
	// staff id
	public int staffId;
	// object id
	public String objectId = "";
	// object type
	public String objectType = "";
	// lat
	public double lat;
	// lng
	public double lng;
	// action type
	public String actionType;
	// ngay gio bat dau action
	public String startTime;
	public String endTime;
	// ngay cap nhat
	public String updateDate;
	// nguoi tao
	public String createUser;
	// nguoi cap nhat
	public String updateUser;
	// 0:trong tuyen, 1:ngoai tuyen
	public int isOr = 0; //
	//Thong tin shop
	public long shopId;
	public boolean prized; //
	public long routingId;
	public double distance = 0;

	public ActionLogDTO() {
		super(TableType.ACTION_LOG);
	}

	/**
	 * tao ra mot cau truc sql de gui len server insert action log
	 *
	 * @author : BangHN since : 1.0
	 */
	public JSONObject generateActionLogSql() {
		JSONObject actionJson = new JSONObject();

		// inster into ACTION_LOG
		// (ID, CUSTOMER_ID, STAFF_ID, OBJECT_ID, OBJECT_TYPE, LAT, LNG,
		// ACTION_TYPE, CREATE_DATE)

		try {
			actionJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			actionJson.put(IntentConstants.INTENT_TABLE_NAME, ACTION_LOG_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.ACTION_LOG_ID, id, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.CUSTOMER_ID, aCustomer.getCustomerId(), null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.STAFF_ID, staffId, null));
			if (!StringUtil.isNullOrEmpty(objectId)) {
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_ID, objectId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_ID, "", DATA_TYPE.NULL.toString()));
			}
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_TYPE, objectType, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.LAT, lat, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.LNG, lng, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.CUSTOMER_LAT, aCustomer.lat, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.CUSTOMER_LNG, aCustomer.lng, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.START_TIME, startTime, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.IS_OR, isOr, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.SHOP_ID, shopId, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.ROUTING_ID, routingId, null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.DISTANCE, distance, null));
			if (!StringUtil.isNullOrEmpty(endTime)) {
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.END_TIME, endTime, null));
			}
			actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return actionJson;
	}

	public JSONObject generateUpdateActionLogSql() {
		JSONObject actionJson = new JSONObject();
		try {
			actionJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			actionJson.put(IntentConstants.INTENT_TABLE_NAME, ACTION_LOG_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();

			if (!StringUtil.isNullOrEmpty(objectId)) {
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_ID, objectId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_ID, "", DATA_TYPE.NULL.toString()));
			}
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_TYPE, objectType, null));
			// detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.LAT,
			// lat,
			// null));
			// detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.LNG,
			// lng,
			// null));
			detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.END_TIME, endTime, null));

			actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.ACTION_LOG_ID, this.id, null));
			actionJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (Exception e) {
		}
		return actionJson;
	}

	/**
	 * tao cau lenh delete action_log len server Xoa nhung action_log cua staff,
	 * customer voi object tuong ung trong ngay hien tai
	 *
	 * @author: BangHN
	 * @return
	 * @return: JSONObject
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public JSONObject generateDeleteActionLogSql() {
		JSONObject actionJson = new JSONObject();
		try {
			actionJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			actionJson.put(IntentConstants.INTENT_TABLE_NAME, ACTION_LOG_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.STAFF_ID, String.valueOf(staffId), null));
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.CUSTOMER_ID, String.valueOf(aCustomer.customerId), null));
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_TYPE, objectType, null));
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.START_TIME, startTime, DATA_TYPE.TRUNC.toString()));
			wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.SHOP_ID, shopId, null));

			// wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.ACTION_LOG_ID,
			// this.id, null));
			actionJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (Exception e) {
		}
		return actionJson;
	}

	public void initDataFromCursor(Cursor c) {
		id = CursorUtil.getLong(c, "ACTION_LOG_ID", -1);
		staffId = CursorUtil.getInt(c, "STAFF_ID", -1);
		aCustomer.customerId = CursorUtil.getLong(c, "CUSTOMER_ID", -1);
		objectId = CursorUtil.getString(c, "OBJECT_ID");
		objectType = CursorUtil.getString(c, "OBJECT_TYPE");
		lat = CursorUtil.getDouble(c, "LAT");
		lng = CursorUtil.getDouble(c, "LNG");
		aCustomer.lat = CursorUtil.getDouble(c, "AL_CUSTOMER_LAT");
		aCustomer.shopDTO.distanceOrder = CursorUtil.getDouble(c, "DISTANCE_ORDER");
		aCustomer.lng = CursorUtil.getDouble(c, "AL_CUSTOMER_LNG");
		startTime = CursorUtil.getString(c, "START_TIME");
		endTime = CursorUtil.getString(c, "END_TIME");
		isOr = CursorUtil.getInt(c, "IS_OR");
		aCustomer.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		aCustomer.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		aCustomer.address = CursorUtil.getString(c, "ADDRESS");
		if (!StringUtil.isNullOrEmpty(aCustomer.address)) {
			aCustomer.street = aCustomer.address;
		} else {
			aCustomer.street = CursorUtil.getString(c, "STREET");
			if (StringUtil.isNullOrEmpty(aCustomer.street)) {
				aCustomer.street = "";
			}
		}
		aCustomer.channelTypeId = CursorUtil.getInt(c, "CHANNEL_TYPE_ID");
		shopId = CursorUtil.getLong(c, "SHOP_ID", -1);
		routingId = CursorUtil.getLong(c, "ROUTING_ID");
	}

	public boolean isVisited() {
		boolean isVisited = false;
		if ((objectType.equals("0") && !StringUtil.isNullOrEmpty(endTime)) || objectType.equals("1")) {
			isVisited = true;
		}
		return isVisited;
	}

	/**
	 * Generate code delete action when delete order
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: Vector
	 * @throws:
	 */

	public JSONObject generateDeleteActionWhenDeleteOrder() {
		JSONObject deleteJson = new JSONObject();
		try {
			deleteJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			deleteJson.put(IntentConstants.INTENT_TABLE_NAME, ACTION_LOG_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.STAFF_ID, String.valueOf(staffId), null));
			whereParams.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_ID, objectId, null));
			whereParams.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.OBJECT_TYPE, objectType, null));
			whereParams.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.SHOP_ID, shopId, null));

			deleteJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return deleteJson;
	}

	public ActionLogDTO copyActionLog() {
		ActionLogDTO al = new ActionLogDTO();
		al.actionType = this.actionType;
		al.aCustomer.customerId = this.aCustomer.customerId;
		al.aCustomer.customerName = this.aCustomer.customerName;
		al.aCustomer.customerCode = this.aCustomer.customerCode;
		al.createUser = this.createUser;
		al.endTime = this.endTime;
		al.id = this.id;
		al.isOr = this.isOr;
		al.lat = this.lat;
		al.lng = this.lng;
		al.objectId = this.objectId;
		al.objectType = this.objectType;
		al.staffId = this.staffId;
		al.startTime = this.startTime;
		al.updateDate = this.updateDate;
		al.updateUser = this.updateUser;
		al.shopId = this.shopId;
		al.routingId = this.routingId;
		al.distance = this.distance;

		return al;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param listActLog
	 * @return
	 * @return: JSONArrayvoid
	 * @throws:
	 */
	public JSONArray generateUpdateEntimeListActionLogSql(ArrayList<ActionLogDTO> listActLog) {
		JSONArray jsonArr = new JSONArray();
		for (int i = 0; i < listActLog.size(); i++) {
			ActionLogDTO item = listActLog.get(i);
			JSONObject actionJson = new JSONObject();
			try {
				actionJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				actionJson.put(IntentConstants.INTENT_TABLE_NAME, ACTION_LOG_TABLE.TABLE_NAME);
				// ds params
				JSONArray detailPara = new JSONArray();
				detailPara.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.END_TIME, item.endTime, null));
				actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

				// ds where params --> insert khong co menh de where
				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(ACTION_LOG_TABLE.ACTION_LOG_ID, item.id, null));
				actionJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
			} catch (Exception e) {
			}

			jsonArr.put(actionJson);
		}

		return jsonArr;
	}

	 /**
	 * init data from cursor
	 * @author: dungdq3
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursorAndPrize(Cursor c) {
		initDataFromCursor(c);
		String str = CursorUtil.getString(c, "STR_CUSTOMER_ID");
		if(!StringUtil.isNullOrEmpty(str)){
			prized = true;
		} else {
			prized = false;
		}
	}
}
