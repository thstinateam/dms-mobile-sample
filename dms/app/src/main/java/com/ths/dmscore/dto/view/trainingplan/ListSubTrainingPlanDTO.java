/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

import com.ths.dmscore.dto.db.trainingplan.SupTrainingPlanDTO;

/**
 * ListSubTrainingPlanDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:46:04 PM Nov 14, 2014
 */
public class ListSubTrainingPlanDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// tong so item
	private int totalSize;
	// danh sach huan luyen
	public ArrayList<SupTrainingPlanDTO> listResult;
	public Hashtable<String, SupTrainingPlanDTO> tbhvTrainingPlan;
	
	public ListSubTrainingPlanDTO() {
		// TODO Auto-generated constructor stub
		totalSize = 0;
		listResult = new ArrayList<SupTrainingPlanDTO>();
		tbhvTrainingPlan = new Hashtable<String, SupTrainingPlanDTO>();
	}

	public int getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}

	public ArrayList<SupTrainingPlanDTO> getListSubTrainingPlan() {
		return listResult;
	}

	public void setListSubTrainingPlan(ArrayList<SupTrainingPlanDTO> listSubTrainingPlan) {
		this.listResult = listSubTrainingPlan;
	}
	
	public void addAll(ArrayList<SupTrainingPlanDTO> listSubTrainingPlan){
		this.listResult.addAll(listSubTrainingPlan);
	}

}
