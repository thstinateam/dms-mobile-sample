package com.ths.dmscore.view.sale.order;

/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.GlobalUtil.QuantityInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * Row mat hang ban
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class OrderPromotionRow extends DMSTableRow implements OnClickListener, TextWatcher, Serializable, OnFocusChangeListener {
	private static final long serialVersionUID = 1L;
	Context _context;
	int TYPE_ACTION_DELETE = 0;
	int TYPE_ACTION_CHANGE_PROMOTION_PRODUCT = 1;
	int TYPE_ACTION_CHANGE_PROMOTION_ORDER = 2;
	int TYPE_ACTION_DELETE_ACCUMULATION = 3;
	// so thu tu
	TextView tvSTT;
	// ma mat hang
	TextView tvProductCode;
	// Linear Layout chua product code & dau *
	//LinearLayout llMHTT;
	// Dau * cho MHTT (Mat hang trong tam)
	//TextView tvMHTT;
	// ten mat hang
	TextView tvProductName;
	// so luong ton kho dang format thung/ hop
	TextView tvRemaindStockFormat;
	// so luong ton kho dang format thung/ hop Thuc te
	TextView tvRemaindStockFormatActual;
	// tong
	EditText etTotal;
	// chiet khau
	TextView tvDiscount;
	// khuyen mai
	TextView tvPromo;
	// image view ivActionRow
	ImageView ivActionRow;
	// linerlayout action row
	LinearLayout llActionRow;
	// listener
	protected OnEventControlListener listener;
	// dto row
	private OrderDetailViewDTO rowDTO;
	// textview max quantity
	private TextView tvMaxQuantityFree;
	// linear layout total
	private LinearLayout llTotal;
	int typeActionRow = -1;
	// check when input money > allow value -> settext -> call afterTextChange again
	boolean hasResetAmount = false;
	public EditText etQuantityReceived;
	private TextView tvMaxQuantityReceived;
	private LinearLayout llQuantityReceived;

	public OrderPromotionRow(Context context, boolean isShowPrice) {
		super(context, R.layout.order_promotion_row,
				(isShowPrice ? null : new int[] {R.id.tvDiscount}));
		_context = context;
		tvSTT = (TextView) this.findViewById(R.id.tvSTT);
		tvProductCode = (TextView)PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvProductCode, PriControl.NVBH_BANHANG_DONHANG_CHITIETSP);
		PriUtils.getInstance().setOnClickListener(tvProductCode, this);
		//llMHTT = (LinearLayout) this.findViewById(R.id.llMHTT);
		//tvMHTT = (TextView) this.findViewById(R.id.tvMHTT);
		tvMaxQuantityFree = (TextView) this.findViewById(R.id.tvMaxQuantityFree);
		tvRemaindStockFormat = (TextView) this.findViewById(R.id.tvRemaindStockFormat);
		tvRemaindStockFormatActual = (TextView) this.findViewById(R.id.tvRemaindStockFormatActual);
		tvProductName = (TextView) this.findViewById(R.id.tvProductName);
		etTotal = (EditText) this.findViewById(R.id.etTotal);
		etTotal.addTextChangedListener(this);

		tvDiscount = (TextView) this.findViewById(R.id.tvDiscount);
		tvPromo = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvPromo, PriControl.NVBH_BANHANG_DATHANG_HTTM);
		PriUtils.getInstance().setOnClickListener(tvPromo, this);
		ivActionRow = (ImageView) this.findViewById(R.id.ivActionRow); PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.ivActionRow, PriControl.NVBH_BANHANG_DONHANG_XOA_SP);
//		PriUtils.getInstance().setOnClickListener(ivActionRow, this);
		llActionRow = (LinearLayout) this.findViewById(R.id.llActionRow);
		PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.llActionRow, PriControl.NVBH_BANHANG_DONHANG_XOA_SP);
		PriUtils.getInstance().setOnClickListener(llActionRow, this);
		llTotal = (LinearLayout) this.findViewById(R.id.llTotal);
		etQuantityReceived = (EditText) this.findViewById(R.id.etQuantityReceived);
		etQuantityReceived.addTextChangedListener(this);
		tvMaxQuantityReceived = (TextView) this.findViewById(R.id.tvMaxQuantityReceived);
		llQuantityReceived = (LinearLayout) this.findViewById(R.id.llQuantityReceived);

		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			etTotal.setInputType(InputType.TYPE_NULL);
			etTotal.setOnClickListener(this);
			etTotal.setOnFocusChangeListener(this);
			etQuantityReceived.setInputType(InputType.TYPE_NULL);
			etQuantityReceived.setOnClickListener(this);
			etQuantityReceived.setOnFocusChangeListener(this);
			etQuantityReceived.setInputType(InputType.TYPE_NULL);
		}
	}

	public void setListner(OnEventControlListener listener) {
		this.listener = listener;
	}

	/**
	 * Cap nhat data
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	public void updateData(OrderDetailViewDTO dto) {
		updateData(dto, true);
	}

	/**
	 *
	 * Cap nhat data co kiem tra stock total
	 * @author: Nguyen Thanh Dung
	 * @param STT
	 * @param dto
	 * @param isCheckStockTotal
	 * @return: void
	 * @throws:
	 */
	public void updateData(OrderDetailViewDTO dto, boolean isCheckStockTotal) {
		rowDTO = dto;
		rowDTO.row = this;
		if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
			updateOrderForProduct(dto, isCheckStockTotal);
		} else if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
			updatePromotionForOrder(dto);
		} else if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
			updatePromotionProductZV21(dto, isCheckStockTotal);
		}
	}

	/**
	* Mo ta muc dich ham
	* @author: TruongHN
	* @param dto
	* @param isCheckStockTotal
	* @return: void
	* @throws:
	*/
	private void updatePromotionProductZV21(OrderDetailViewDTO dto,
			boolean isCheckStockTotal) {
		tvProductCode.setText(dto.productCode);
		tvProductName.setText(dto.productName);
		if (StringUtil.isNullOrEmpty(dto.productName)) {
			tvProductName.setText(dto.typeName);
		}
		// etTotal.setText(StringUtil.parseAmountMoney(String.valueOf(dto.orderDetailDTO.quantity)));
//		etTotal.setText(String.valueOf(dto.orderDetailDTO.quantity));
		GlobalUtil.setFilterInputConvfact(etTotal, Constants.MAX_LENGHT_QUANTITY);
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			etTotal.setInputType(InputType.TYPE_NULL);
		}
		etTotal.setText(GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
//		tvMaxQuantityFree.setText(String.valueOf(dto.orderDetailDTO.maxQuantityFree));
		tvMaxQuantityFree.setText(GlobalUtil.formatNumberProductFlowConvfact(dto.orderDetailDTO.maxQuantityFree, dto.convfact));
		tvMaxQuantityFree.setVisibility(View.VISIBLE);
//		//So suat
		etQuantityReceived.setEnabled(false);
		tvMaxQuantityReceived.setEnabled(false);
//		etQuantityReceived.setText(String.valueOf(dto.quantityReceived.quantityReceived));
//		tvMaxQuantityReceived.setText(String.valueOf(dto.quantityReceived.quantityReceivedMax));
		//Sua so luong
		if(dto.isEdited == 1) {
			tvMaxQuantityFree.setText("(" + tvMaxQuantityFree.getText().toString() + ")");
		} else {
			//TH xem lai don hang (isEdit = 0 hoac = 2)
//			if(dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				tvMaxQuantityFree.setText(GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact)
//						+ "(" + tvMaxQuantityFree.getText().toString() + ")");
//			} else {
//				tvMaxQuantityFree.setText(GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
//			}
			tvMaxQuantityFree.setText(GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.maxQuantityFree, dto.convfact));
			etTotal.setEnabled(false);
		}
		display(tvDiscount, "");
		tvPromo.setText(Constants.STR_BLANK);
		tvPromo.setOnClickListener(null);
		// HaiTC: hien thi so luong ton kho dang format thung / hop
		tvRemaindStockFormat.setText(dto.remaindStockFormat);
		tvRemaindStockFormatActual.setText(dto.remaindStockFormatActual);

		//Cho phep sua So suat neu la don vansale & is_edit = 2
		if(dto.quantityReceived != null) {
			if(dto.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE) && dto.isEdited == 2) {
				tvMaxQuantityReceived.setText(tvMaxQuantityReceived.getText().toString());
			} else {
				tvMaxQuantityReceived.setText(String.valueOf(dto.quantityReceived.quantityReceivedMax));
				etQuantityReceived.setText(String.valueOf(dto.quantityReceived.quantityReceived));
				etQuantityReceived.setEnabled(false);
			}
		} else {
			tvMaxQuantityReceived.setText(StringUtil.EMPTY_STRING);
			etQuantityReceived.setText(StringUtil.EMPTY_STRING);
			etQuantityReceived.setEnabled(false);
		}

		//Cho phep doi sp KM hay ko?
		if (dto.changeProduct == 1) {
			typeActionRow = TYPE_ACTION_CHANGE_PROMOTION_PRODUCT;
			ivActionRow.setVisibility(View.VISIBLE);
			ivActionRow.setImageResource(R.drawable.icon_change_promotion);
			// ivAction.setImageBitmap(null);
		} else {
			ivActionRow.setVisibility(View.GONE);
		}
		// Cap nhat mau khi kiem tra ton kho
		if (isCheckStockTotal) {
			checkStockTotal(dto);
		}
	}

	/**
	* Render layout cho sp KM sp
	* @author: TruongHN
	* @param STT
	* @param dto
	* @param isCheckStockTotal
	* @return: void
	* @throws:
	*/
	private void updateOrderForProduct(OrderDetailViewDTO dto, boolean isCheckStockTotal) {
		display(tvSTT, dto.indexParent + 1);
		display(tvProductCode, dto.productCode);
		display(tvProductName, dto.productName);
		if (StringUtil.isNullOrEmpty(dto.productName)) {
			display(tvProductName, dto.typeName);
		}
		// etTotal.setText(StringUtil.parseAmountMoney(String.valueOf(dto.orderDetailDTO.quantity)));
		// HaiTC: hien thi so luong ton kho dang format thung / hop
		display(tvRemaindStockFormat, dto.remaindStockFormat);
		display(tvRemaindStockFormatActual, dto.remaindStockFormatActual);

		if (dto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
			if(dto.type == OrderDetailViewDTO.FREE_PRODUCT) {
				GlobalUtil.setFilterInputConvfact(etTotal,Constants.MAX_LENGHT_QUANTITY);
				if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
					etTotal.setInputType(InputType.TYPE_NULL);
				}
				display(etTotal, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
//				etTotal.setText(String.valueOf(dto.orderDetailDTO.quantity));
				display(tvMaxQuantityFree, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.maxQuantityFree, dto.convfact));
				// HaiTC: hien thi so luong ton kho dang format thung / hop
				display(tvRemaindStockFormat, dto.remaindStockFormat);
				display(tvRemaindStockFormatActual, dto.remaindStockFormatActual);
				//So suat
				if(dto.quantityReceived != null && !dto.quantityReceived.isShowed) {
					display(etQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceived));
					display(tvMaxQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceivedMax));
				} else {
					display(tvMaxQuantityReceived, Constants.STR_BLANK);
					etQuantityReceived.setVisibility(View.GONE);
				}
			} else if(dto.type == OrderDetailViewDTO.FREE_PRICE) {
				StringUtil.display(etTotal, dto.orderDetailDTO.getDiscountAmount());
				StringUtil.display(tvMaxQuantityFree, dto.orderDetailDTO.maxAmountFree);
				// HaiTC: hien thi so luong ton kho dang format thung / hop
				display(tvRemaindStockFormat, Constants.STR_BLANK);
				display(tvRemaindStockFormatActual, Constants.STR_BLANK);
			} else if(dto.type == OrderDetailViewDTO.FREE_PERCENT) {
				StringUtil.display(etTotal, dto.orderDetailDTO.getDiscountAmount());
				StringUtil.display(tvMaxQuantityFree, dto.orderDetailDTO.maxAmountFree);
				// HaiTC: hien thi so luong ton kho dang format thung / hop
				display(tvRemaindStockFormat, Constants.STR_BLANK);
				display(tvRemaindStockFormatActual, Constants.STR_BLANK);

				if((int)dto.orderDetailDTO.discountPercentage == dto.orderDetailDTO.discountPercentage) {
					display(tvDiscount, (int)dto.orderDetailDTO.discountPercentage + "%");
				} else {
					display(tvDiscount, dto.orderDetailDTO.discountPercentage + "%");
				}
			}
			// neu la CTKM tu dong va cau hinh cho phep chinh sua
			tvMaxQuantityFree.setVisibility(View.VISIBLE);
			if(dto.isEdited == 1) {
				display(tvMaxQuantityFree , tvMaxQuantityFree.getText().toString());
			} else {
				//TH xem lai don hang
				display(tvMaxQuantityFree, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.maxQuantityFree, dto.convfact));
				//them de xem khi disable
				display(etTotal, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
				etTotal.setEnabled(false);
			}

			//Cho phep sua So suat neu la don vansale & is_edit = 2
			if(dto.quantityReceived != null) {
				if(dto.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE) && dto.isEdited == 2) {
					display(tvMaxQuantityReceived, tvMaxQuantityReceived.getText().toString());
				} else {
					//TH xem lai don hang
					display(tvMaxQuantityReceived, tvMaxQuantityReceived.getText().toString());
					//them de xem khi disable
					display(etQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceived));
					etQuantityReceived.setEnabled(false);
				}

				dto.quantityReceived.isShowed = true;
			}
		} else {
			//So suat
			//Tra thuong CTTB thi ko co so suat
			if(dto.quantityReceived != null) {
				display(tvMaxQuantityReceived, dto.quantityReceived.quantityReceived);
				//them de xem khi disable
				display(etQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceived));
				etQuantityReceived.setEnabled(false);
			} else {
				display(tvMaxQuantityReceived, Constants.STR_BLANK);
				etQuantityReceived.setVisibility(View.GONE);
				etQuantityReceived.setEnabled(false);
			}
			//So luong
//			display(tvMaxQuantityFree, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
			//them de xem khi disable
//			etTotal.setEnabled(false);
//			display(etTotal, GlobalUtil.formatNumberProductFlowConvfact((int)dto.orderDetailDTO.quantity, dto.convfact));
//			etTotal.setVisibility(View.GONE);
//			tvMaxQuantityFree.setVisibility(View.GONE);
//			llTotal.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
			showRowSum(GlobalUtil.formatNumberProductFlowConvfact(
					(int) dto.orderDetailDTO.quantity, dto.convfact),
					ImageUtil.getColor(R.color.WHITE), llTotal,
					tvMaxQuantityFree);
			tvMaxQuantityFree.setTypeface(null,Typeface.NORMAL);
		}

		tvPromo.setText(dto.orderDetailDTO.programeCode);

		//Tra thuong TB
		if (dto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION) {
			SpannableObject objCk = new SpannableObject();
			objCk.addSpan(dto.orderDetailDTO.programeCode,
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			display(tvPromo, objCk.getSpan());

			tvPromo.setOnClickListener(null);
		}
		// neu la CTKM tu dong thi ko duoc xoa
		if (dto.orderDetailDTO.programeType != PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
			typeActionRow = TYPE_ACTION_DELETE;
			ivActionRow.setVisibility(View.VISIBLE);
			ivActionRow.setImageResource(R.drawable.icon_delete);
			// ivActionDelete.setImageBitmap(null);
		} else {
			if (dto.changeProduct == 1) {
				typeActionRow = TYPE_ACTION_CHANGE_PROMOTION_PRODUCT;
				ivActionRow.setVisibility(View.VISIBLE);
				ivActionRow.setImageResource(R.drawable.icon_change_promotion);
				// ivAction.setImageBitmap(null);
			} else {
				ivActionRow.setVisibility(View.GONE);
			}
		}


		// Cap nhat mau khi kiem tra ton kho
		if (isCheckStockTotal) {
			checkStockTotal(dto);
		}
	}

	/**
	* Mo ta muc dich ham
	* @author: TruongHN
	* @param dto
	* @return: void
	* @throws:
	*/
	private void updatePromotionForOrder(OrderDetailViewDTO dto) {
		tvSTT.setText(String.valueOf(dto.indexParent + 1));
		List<View> lstNameTotalRowView = new ArrayList<View>();
		List<View> lstTotalRowViewValue = new ArrayList<View>();

		lstNameTotalRowView.add(tvProductCode);
		lstNameTotalRowView.add(tvProductName);
		lstNameTotalRowView.add(tvRemaindStockFormat);
		lstNameTotalRowView.add(tvRemaindStockFormatActual);

		// an di so luong ton kho
		//tvRemaindStockFormat.setVisibility(View.GONE);
		//tvRemaindStockFormatActual.setVisibility(View.GONE);
		// an di ma san pham va thong tin MHTT
		//llMHTT.setVisibility(View.GONE);
		//An di so suat neu KM don hang tien hoac la dong KM tieu de cua KM sp
//		etQuantityReceived.setVisibility(View.GONE);
//		tvMaxQuantityReceived.setVisibility(View.GONE);
		if(dto.quantityReceived != null) {
			if(!dto.quantityReceived.isShowed) {
				display(tvMaxQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceivedMax));
				display(etQuantityReceived, String.valueOf(dto.quantityReceived.quantityReceived));
			} else {
				display(tvMaxQuantityReceived, StringUtil.EMPTY_STRING);
				display(etQuantityReceived, StringUtil.EMPTY_STRING);
				etQuantityReceived.setEnabled(false);
			}
			dto.quantityReceived.isShowed = true;

			//Cho phep sua So suat neu la don vansale & is_edit = 2
			if(dto.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE) && dto.isEdited == 2) {
				//da hien thi o tren, khong lam gi them
				if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER
						|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY
						|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY) {
					etQuantityReceived.setEnabled(false);
				}
			} else {
				//khong cho sua so suat
				etQuantityReceived.setEnabled(false);
			}
		} else {
			display(tvMaxQuantityReceived, StringUtil.EMPTY_STRING);
			display(etQuantityReceived, StringUtil.EMPTY_STRING);
			etQuantityReceived.setEnabled(false);
		}


		// hien thi title khuyen mai don hang
		//LayoutParams currentParam = (LayoutParams) tvProductName.getLayoutParams();
		//currentParam.width = GlobalUtil.dip2Pixel(302);
		//tvProductName.setLayoutParams(currentParam);
		//tvProductName.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		String titlePromotionType = "";
		if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21) {
			titlePromotionType = StringUtil.getString(R.string.TEXT_TITLE_PROMOTIOIN_ORDER);
		} else if (dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
			titlePromotionType = StringUtil.getString(R.string.TEXT_TITLE_PROMOTIOIN_ACCUMULATION);
			if(dto.orderDetailDTO.payingOrder > 0) {
				titlePromotionType += " " + StringUtil.getString(R.string.PAYING_ORDER) + " " + dto.orderDetailDTO.payingOrder;
			}
		} else if (dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
			titlePromotionType = StringUtil.getString(R.string.TEXT_TITLE_PROMOTIOIN_NEW_OPEN);
		}else if (dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
			titlePromotionType = StringUtil.getString(R.string.TEXT_TITLE_PROMOTIOIN_KEYSHOP);
		}
		//tvProductName.setText(titlePromotionType);
		showRowSum(titlePromotionType, true, lstNameTotalRowView.toArray(new View[] {}));

		// cap nhat lai layout cho thong tin so tien khuyen mai
		//LayoutParams llTotalLayoutParam = new LayoutParams(
		//		GlobalUtil.dip2Pixel(322),
		//		LinearLayout.LayoutParams.WRAP_CONTENT);
		//llTotalLayoutParam.gravity = Gravity.CENTER;
		//llTotalLayoutParam.setMargins(GlobalUtil.dip2Pixel(1),
		//		GlobalUtil.dip2Pixel(1), GlobalUtil.dip2Pixel(1),
		//		GlobalUtil.dip2Pixel(1));
		//llTotal.setLayoutParams(llTotalLayoutParam);

		//clear list view, add new views
		String maxAmountFreeStr = "";
		if (dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT
				|| dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
			//etTotal.setVisibility(View.GONE);
			//tvMaxQuantityFree.setVisibility(View.GONE);
		}else{
			// hien thi so tien chiet khau
			if(dto.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER ||
					dto.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY
					|| dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
				maxAmountFreeStr = StringUtil.formatNumber(dto.orderDetailDTO.getDiscountAmount());
			} else if(dto.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY) {
//				llTotalLayoutParam = new LayoutParams(
//						GlobalUtil.dip2Pixel(139),
//						LinearLayout.LayoutParams.WRAP_CONTENT);
//				llTotalLayoutParam.gravity = Gravity.RIGHT;
//				llTotalLayoutParam.setMargins(GlobalUtil.dip2Pixel(1),
//						GlobalUtil.dip2Pixel(1), GlobalUtil.dip2Pixel(1),
//						GlobalUtil.dip2Pixel(1));
//				llTotal.setLayoutParams(llTotalLayoutParam);
//
//				LayoutParams llQuantityReceivedParam = new LayoutParams(
//						GlobalUtil.dip2Pixel(259),
//						LinearLayout.LayoutParams.WRAP_CONTENT);
//				llQuantityReceivedParam.gravity = Gravity.LEFT;
//				llQuantityReceivedParam.setMargins(GlobalUtil.dip2Pixel(1),
//						GlobalUtil.dip2Pixel(1), GlobalUtil.dip2Pixel(1),
//						GlobalUtil.dip2Pixel(1));
//				llQuantityReceived.setLayoutParams(llQuantityReceivedParam);

				//So tien tra trong don hang
				maxAmountFreeStr = (StringUtil.formatNumber(dto.orderDetailDTO.getDiscountAmount())
						+ "(" + StringUtil.getString(R.string.TEXT_REMAIN_AMOUNT)
						+ ": " + StringUtil.formatNumber(dto.remainDiscount)+ ")");
				//So tien con du
//				tvMaxQuantityReceived.setText(StringUtil.getString(R.string.TEXT_REMAIN_AMOUNT)
//						+ ": " + StringUtil.parseAmountMoney(String.valueOf(dto.orderDetailDTO.maxAmountFree - dto.orderDetailDTO.discountAmount)));

			}
			// hien thi so tien khuyen mai, mac dinh so tien khuyen mai = so tien chiet khau
//			etTotal.setText(StringUtil.parseAmountMoney(String.valueOf(dto.orderDetailDTO.discountAmount)));
//			GlobalUtil.setFilterInputMoney(etTotal,StringUtil.parseAmountMoney(String.valueOf(dto.orderDetailDTO.discountAmount)).length());

			//KM don hang ZV19, 20 khong cho phep doi tien
//			dto.isEdited = 0;
//			if(dto.isEdited == 1) {
//				tvMaxQuantityFree.setText("(" + tvMaxQuantityFree.getText().toString() + ")");
//				llTotal.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
//			} else {
				//lstTotalRowView.add(llTotal);
				//etTotal.setVisibility(View.GONE);
				//llTotal.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
//			}
		}
		// truong hop lien quan toi km tien keyshop thi cho edit
		if (dto.promotionType != OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
			lstTotalRowViewValue.add(llTotal);
			lstTotalRowViewValue.add(tvMaxQuantityFree);
			showRowSum(maxAmountFreeStr, true, lstTotalRowViewValue.toArray(new View[] {}));
		}else{
			display(etTotal, maxAmountFreeStr);
			display(tvMaxQuantityFree, maxAmountFreeStr);
			showRowSum("", true, lstTotalRowViewValue.toArray(new View[] {}));
		}

		// hien thi phan tram chiet khau
		if(dto.orderDetailDTO.discountPercentage > 0) {
			if((int)dto.orderDetailDTO.discountPercentage == dto.orderDetailDTO.discountPercentage) {
				display(tvDiscount, String.valueOf((int)dto.orderDetailDTO.discountPercentage) + " %");
			} else {
				display(tvDiscount, String.valueOf(dto.orderDetailDTO.discountPercentage) + " %");
			}
		} else {
			display(tvDiscount, Constants.STR_BLANK);
		}

		// hien thi CT HTTM
		tvPromo.setText(dto.orderDetailDTO.programeCode);

		//Hien thi doi CT KM don hang
		if (dto.showChangePromotionOrder == true) {
			ivActionRow.setImageResource(R.drawable.icon_refresh);
			ivActionRow.setVisibility(View.VISIBLE);
			typeActionRow = TYPE_ACTION_CHANGE_PROMOTION_ORDER;
		} else if (dto.allowDeleteAccumulation == true) {
			typeActionRow = TYPE_ACTION_DELETE_ACCUMULATION;
			ivActionRow.setVisibility(View.VISIBLE);
			ivActionRow.setImageResource(R.drawable.icon_delete);
			// ivActionDelete.setImageBitmap(null);
		} else {
			ivActionRow.setVisibility(View.VISIBLE);
			ivActionRow.setImageDrawable(null);
		}
	}

	/**
	 * Kiem tra ton kho
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */

	public void checkStockTotal(OrderDetailViewDTO dto) {
		if (dto.orderDetailDTO.quantity > 0) {// San pham KM hang, SP KM tien
												// thi quantity = 0
			if (dto.stock <= 0) {
				updateRowWithColor(ImageUtil.getColor(R.color.RED));
			} else if (dto.totalOrderQuantity != null && dto.totalOrderQuantity.orderDetailDTO.quantity > dto.stock) {
				updateRowWithColor(ImageUtil.getColor(R.color.OGRANGE));
			} else {
				GlobalUtil.setTextColor(ImageUtil.getColor(R.color.TITLE_LIGHT_BLACK_COLOR),
						tvSTT, tvProductName, etTotal, tvDiscount,
						tvRemaindStockFormat, tvRemaindStockFormatActual, tvMaxQuantityFree);
				GlobalUtil.setTextColor(ImageUtil.getColor(R.color.COLOR_LOCATION_NAME), tvProductCode, tvPromo);
			}
		}
	}

	/**
	 *
	 * Cap nhat full mau cho 1 row
	 *
	 * @author: Nguyen Thanh Dung
	 * @param color
	 * @return: void
	 * @throws:
	 */
	private void updateRowWithColor(int color) {
		GlobalUtil.setTextColor(color,
				tvSTT, tvProductCode, tvProductName, etTotal, tvDiscount,
				tvRemaindStockFormat, tvRemaindStockFormatActual, tvPromo, tvMaxQuantityFree);
	}

	/**
	 * Cap nhat lai STT
	 *
	 * @author: TruongHN
	 * @param numberRow
	 * @return: void
	 * @throws:
	 */
	public void updateNumberRow() {
		tvSTT.setText(String.valueOf(rowDTO.indexParent + 1));
	}

	/**
	 * Cap nhat so luong hien thi
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void updateQuantityLayout() {
		tvMaxQuantityFree.setText(GlobalUtil.formatNumberProductFlowConvfact((int)rowDTO.orderDetailDTO.quantity, rowDTO.convfact));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == tvProductCode && listener != null) {
			listener.onEvent(OrderView.ACTION_VIEW_PRODUCT, this, rowDTO);
		} else if (v == tvPromo && listener != null) {
			listener.onEvent(OrderView.ACTION_VIEW_PROMOTION, this, rowDTO);
		} else if (v == llActionRow && listener != null
				&& typeActionRow == TYPE_ACTION_CHANGE_PROMOTION_PRODUCT) {
			listener.onEvent(OrderView.ACTION_CHANGE_PROMOTION_PRODUCT, this, rowDTO);
		} else if (v == llActionRow && listener != null
				&& typeActionRow == TYPE_ACTION_DELETE) {
			listener.onEvent(OrderView.ACTION_DELETE_PROMOTION, this, rowDTO);
		} else if (v == this && _context != null) {
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity) _context);
		} else if (v == llActionRow && listener != null
				&& typeActionRow == TYPE_ACTION_CHANGE_PROMOTION_ORDER) {
			listener.onEvent(
					OrderView.ACTION_CHANGE_PROMOTION_FOR_PROMOTION_ORDER,
					this, rowDTO);
		} else if (v == llActionRow && listener != null
				&& typeActionRow == TYPE_ACTION_DELETE_ACCUMULATION) {
			listener.onEvent(OrderView.ACTION_DELETE_PROMOTION_ACCUMULATION, this, rowDTO);
		} else if (v == etQuantityReceived && listener != null) {
			listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
					v, null);
		} else if (v == etTotal && listener != null) {
			listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v,
					null);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.text.TextWatcher#afterTextChanged(android.text.Editable)
	 */
	@Override
	public void afterTextChanged(Editable arg0) {
		if(arg0 == etTotal.getEditableText()) {
			//KM cho don hang: ZV19, ZV20 hoac KM cho sp: KM %, KM amount
			if (rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER ||
					(rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT && (rowDTO.type == OrderDetailViewDTO.FREE_PRICE || rowDTO.type == OrderDetailViewDTO.FREE_PERCENT))) {
				// chi khac khi fullDate la khuyen mai loai khuyen mai theo don hang
				double moneyKM = 0;
				if (etTotal.getText().toString().length() > 0) {
					moneyKM = StringUtil.parseDoubleStr(etTotal.getText().toString());
				}

				if (rowDTO != null) {
					if(hasResetAmount == true) {
						hasResetAmount = false;
					} else {
						rowDTO.oldDiscountAmount = rowDTO.orderDetailDTO.getDiscountAmount();
						if (moneyKM > rowDTO.orderDetailDTO.maxAmountFree) {
							hasResetAmount = true;
							etTotal.setText(StringUtil.formatNumber(rowDTO.orderDetailDTO.maxAmountFree));
							rowDTO.orderDetailDTO.setDiscountAmount(rowDTO.orderDetailDTO.maxAmountFree);
						} else {
							rowDTO.orderDetailDTO.setDiscountAmount(moneyKM);
						}

						listener.onEvent(OrderView.ACTION_CHANGE_REAL_MONEY_PROMOTION, this, rowDTO);
					}
				}
			} else {
				QuantityInfo quantityInfo = new QuantityInfo();
				if (etTotal.getText().toString().length() > 0) {
					try {
//					realOrder = Integer.valueOf(etTotal.getText().toString());
						quantityInfo = GlobalUtil.calQuantityFromOrderStr(etTotal.getText().toString(), rowDTO.convfact);
						//realOrder = GlobalUtil.calRealOrder(, rowDTO.convfact);
					} catch (Exception e) {
						// TODO: handle exception
						//realOrder = rowDTO.orderDetailDTO.maxQuantityFree + 1;
					}
				}
				if (rowDTO != null && rowDTO.orderDetailDTO != null
						&& (quantityInfo.quantityPackage != rowDTO.orderDetailDTO.quantityPackage
								|| quantityInfo.quantitySingle != rowDTO.orderDetailDTO.quantityRetail)) {
					Bundle bundle = new Bundle();
					bundle.putInt(IntentConstants.INTENT_INDEX_PARENT, rowDTO.indexParent);
					bundle.putInt(IntentConstants.INTENT_INDEX_CHILD, rowDTO.indexChild);
					bundle.putSerializable(IntentConstants.INTENT_VALUE, rowDTO);
					bundle.putSerializable(IntentConstants.INTENT_TABLE_ROW, this);
					// update row total
					if (rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
						listener.onEvent(OrderView.ACTION_CHANGE_REAL_QUANTITY_PROMOTION, this, bundle);
					} else if (rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
						listener.onEvent(OrderView.ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ORDER, this, bundle);
					} else if (rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
						listener.onEvent(OrderView.ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ACCUMULATION, this, bundle);
					} else if (rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT) {
						listener.onEvent(OrderView.ACTION_CHANGE_REAL_QUANTITY_PROMOTION_NEW_OPEN, this, bundle);
					}
				}
			}
		} else if(arg0 == etQuantityReceived.getEditableText()) {
			int realOrder = 0;
			if (etQuantityReceived.getText().toString().length() > 0) {
				try {
					realOrder = Integer.valueOf(etQuantityReceived.getText().toString());
				} catch (Exception e) {
					// TODO: handle exception
					realOrder = rowDTO.quantityReceived.quantityReceivedMax + 1;
				}
			}
			if (rowDTO != null && rowDTO.quantityReceived != null
					&& realOrder != rowDTO.quantityReceived.quantityReceived) {
				if (realOrder > rowDTO.quantityReceived.quantityReceivedMax) {
					realOrder = rowDTO.quantityReceived.quantityReceivedMax;
					etQuantityReceived.setText(String.valueOf(rowDTO.quantityReceived.quantityReceivedMax));
				} else {

				}

				rowDTO.quantityReceived.quantityReceived = realOrder;
				Bundle bundle = new Bundle();
//				bundle.putInt(IntentConstants.INTENT_INDEX_PARENT, rowDTO.indexParent);
//				bundle.putInt(IntentConstants.INTENT_INDEX_CHILD, rowDTO.indexChild);
				bundle.putSerializable(IntentConstants.INTENT_DATA, rowDTO);
//				bundle.putSerializable(IntentConstants.INTENT_TABLE_ROW, this);
				listener.onEvent(OrderView.ACTION_CHANGE_REAL_QUANTITY_RECEIVED, this, bundle);
			}
		}
	}
//	public String formatAmount(long num)
//	{
//	    DecimalFormat decimalFormat = new DecimalFormat();
//	    DecimalFormatSymbols decimalFormateSymbol = new DecimalFormatSymbols();
//	    decimalFormateSymbol.setGroupingSeparator(',');
//	    decimalFormat.setDecimalFormatSymbols(decimalFormateSymbol);
//	    return decimalFormat.format(num);
//	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.text.TextWatcher#beforeTextChanged(java.lang.CharSequence,
	 * int, int, int)
	 */
	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.text.TextWatcher#onTextChanged(java.lang.CharSequence, int,
	 * int, int)
	 */
	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		// tinh toan lai so luong gia

	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if ( v == etTotal && hasFocus) {
			listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
					v, null);
		}else if (v == etQuantityReceived && hasFocus) {
			listener.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
					v, null);
		}

	}

	public void renderHeader(){
		showRowSum(StringUtil.getString(R.string.TEXT_HEADER_TABLE_RESULT_TOTAL), true, llTotal, tvMaxQuantityFree);
		showRowSum(StringUtil.getString(R.string.TEXT_SLOT_PROMOTION), true, llQuantityReceived, tvMaxQuantityReceived);
	}
	
}
