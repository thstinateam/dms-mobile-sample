/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

/**
 * Thong tin mo ta doanh so sku cua khach hang mua
 * @author BANGHN
 * @version 1.0
 */
public class CustomerCatAmountPopupViewDTO implements Serializable{
	private static final long serialVersionUID = 1305778916927753918L;
	public ArrayList<CustomerCatAmountPopupDTO> listCatAmount = new ArrayList<CustomerCatAmountPopupDTO>();
	public CustomerCatAmountPopupDTO catSum = new CustomerCatAmountPopupDTO();
	
	/**
	 * Parse du lieu sau khi select tu db
	 * @author: BANGHN
	 * @param c
	 */
	public void parseFromCursor(Cursor c){
	}
}
