package com.ths.dmscore.dto.db;

import android.database.Cursor;

/**
 * Dinh nghia cac tham so chung cho bao cao dong
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public abstract class RptKPIDTO extends AbstractTableDTO {
	public static enum ObjectTypeRow {
		CUSTOMER(0), STAFF(1), SUPERVISOR(2), TBHV(3), TTTT(4), GDM(5), TRUONGKENH(
				6), NPP(7), VUNG(8), MIEN(9), KENH(10);

		private int objectTypeRow;

		private ObjectTypeRow(int menuName) {
			this.objectTypeRow = menuName;
		}

		public int getObjectTypeRow() {
			return objectTypeRow;
		}
	}

	public static enum ObjectTypeColumn {
		PRODUCT(0), CAT(1), SUBCAT(2);
		private int objectTypeRow;

		private ObjectTypeColumn(int menuName) {
			this.objectTypeRow = menuName;
		}

		public int getObjectTypeColumn() {
			return objectTypeRow;
		}
	}

	public int objectId;
	public int objectTypeId;
	public long shopId;
	public int kpiId;
	public int columnCriTypeId;
	public int columnCriId;
	public double valuePlan;
	public double value;
	public double percentComplete;
	public String createUser;
	public String updateUser;
	public String createDate;
	public String updateDate;

	public abstract void initDataFromCursor(Cursor c, int typeKpiID);
	public abstract RptKPIDTO clone();

}
