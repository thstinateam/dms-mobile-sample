/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.GROUP_MAPPING_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 * ProductGroupDTO.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class GroupMappingDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -226126550646210274L;
	public long groupMappingId;
	public long saleGroupId;
	public long saleGroupLevelId;
	public long promoGroupId;
	public long promoGroupLevelId;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	
	/**
	 * Khoi tao du lieu tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		saleGroupId = CursorUtil.getLong(c, GROUP_MAPPING_TABLE.SALE_GROUP_ID);
		saleGroupLevelId = CursorUtil.getLong(c, GROUP_MAPPING_TABLE.SALE_GROUP_LEVEL_ID);
		promoGroupId = CursorUtil.getLong(c, GROUP_MAPPING_TABLE.PROMO_GROUP_ID);
		promoGroupLevelId = CursorUtil.getLong(c, GROUP_MAPPING_TABLE.PROMO_GROUP_LEVEL_ID);
	}
}
