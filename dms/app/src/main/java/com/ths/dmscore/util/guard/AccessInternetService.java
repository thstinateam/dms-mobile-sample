/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util.guard;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.AccessInternetDialog;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dms.R;

/**
 * Services chan su dung cac ung dung su dung internet 3g
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class AccessInternetService extends Service {
	// timer dinh thoi l
	private Timer locTimer;
	private TimerTask locTask;
	private Handler mHandler = new Handler();
	private static final String TAG = "AccessInternetService";
	static final String HOME = "com.sec.android.app.launcher";
	private static final long TIME_LOCK_APP_PERIOD = 1500;
	ActivityManager am = null;
	// ds cac ung dung can phai xoa
	// bien de kiem tra cac ung dung xoa co thay doi hay ko, neu ko thi luon
	// hien thi xoa
	public int sizeBlackApps = 0;
	private static boolean isUnlock = false;
	AlertDialog alertDialog = null;
	// ds ung dung chan su dung
	private static ArrayList<String> lstBlackAppPrevent = new ArrayList<String>();
	// view chan network
	AccessInternetDialog preventNetworkView;
	// bien kiem tra xem co can checknetwork hay ko
	private static boolean isCheckNetwork = true;
	private static int countTime = 0;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		MyLog.d(TAG, "onCreate");
		// khoi tao truoc cac ds mac dinh
		am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
		isUnlock = false;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		MyLog.d(TAG, "onStart");
		locTimer = new Timer();
		locTask = new TimerTask() {
			@Override
			public void run() {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						int checkNetwork = GlobalInfo.getInstance().getCheckNetwork();
						if (!GlobalInfo.getInstance().isAppActive()
								&& checkNetwork != UserDTO.UNCHECK_ALL_ACCESS) {
							showAppLock();
						} else{
							if (alertDialog != null && alertDialog.isShowing()) {
								alertDialog.dismiss();
							}
						}
					}
				});
				
				countTime++;
				long timeRestartLock = GlobalInfo.getInstance().getTimeLockAppBlocked() / TIME_LOCK_APP_PERIOD;
				if (countTime >= timeRestartLock) {
					isUnlock = false;
					isCheckNetwork = true;
					countTime = 0;
				}
			}
		};
		locTimer.schedule(locTask, 0, TIME_LOCK_APP_PERIOD);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		MyLog.d(TAG, "onDestroy");
		// xu li ko cho destroy services
//		int checkAccessApp = GlobalInfo.getInstance().getProfile()
//				.getUserData().checkAccessApp;
//		if (checkAccessApp == UserDTO.CHECK_NETWORK
//				|| checkAccessApp == UserDTO.CHECK_ALL_ACCESS
//				|| checkAccessApp == -1) {
//		ServiceUtil.startServiceIfNotRunning(AccessInternetService.class);
//		}
		super.onDestroy();
	}


	/**
	 * hien thi xoa ung dung cai dat
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void showAppLock() {
		List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
		ComponentName componentInfo = taskInfo.get(0).topActivity;
		String currentPackage = componentInfo.getPackageName();
		// bien kiem tra ton tai app bi chan network hay khong
		boolean isExistAppPrevent = false;
		// kiem tra net work su dung
		MyLog.d("Password", "showAppLock ");
		if (isCheckNetwork){
			checkNetworkUsing();
		}
		if (!isUnlock) {
			initBlackListApp();

			// kiem tra activity dang o top
			for (int i = 0, size = lstBlackAppPrevent.size(); i < size; i++) {
				if (!StringUtil.isNullOrEmpty(lstBlackAppPrevent.get(i))
						&& lstBlackAppPrevent.get(i).equals(currentPackage)) {
					// Log.e("WhiteList", currentRunningActivityName);
					isExistAppPrevent = true;
					try {
						if (alertDialog == null) {
							Builder build = new AlertDialog.Builder(this,
									R.style.CustomDialogTheme);
							preventNetworkView = new AccessInternetDialog(this);
							build.setView(preventNetworkView.viewLayout);
							alertDialog = build.create();
							alertDialog.getWindow().setType(
									WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
											| LayoutParams.TYPE_KEYGUARD_DIALOG);
							alertDialog.setCancelable(false);
							// alertDialog.setOnKeyListener(this);
							Window window = alertDialog.getWindow();
							window.setBackgroundDrawable(new ColorDrawable(
									Color.argb(0, 255, 255, 255)));
							window.setLayout(LayoutParams.WRAP_CONTENT,
									LayoutParams.WRAP_CONTENT);
							window.setGravity(Gravity.CENTER);
						}
						//preventNetworkView.showWrongPassword(wrongPass);
						MyLog.d("Password", "show password : " + lstBlackAppPrevent.get(i));
						if (!alertDialog.isShowing()) {
							MyLog.d("Password", "!showing password ");
							alertDialog.show();
							preventNetworkView.showUserName();
							ServerLogger.sendLog("appLock: " + currentPackage, TabletActionLogDTO.LOG_CLIENT);
						}
					} catch (Exception e) {
						MyLog.e("UnexceptionLog", VNMTraceUnexceptionLog
								.getReportFromThrowable(e));
					}
					break;
				}
			}
			//break;
		}
		if (isUnlock || !isExistAppPrevent) {
			if (alertDialog != null && alertDialog.isShowing()) {
				alertDialog.dismiss();
			}
		}
	}

	/**
	 * Kiem tra mang su dung la wifi hay 3g Neu la wifi thi ko chan ung dung su
	 * dung
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void checkNetworkUsing() {
		ConnectivityManager connMgr = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo wifi = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		int checkNetwork = GlobalInfo.getInstance().getCheckNetwork();
		if (checkNetwork == UserDTO.CHECK_NETWORK_OFFLINE){
			isUnlock = false;
		} else if (wifi != null && wifi.isAvailable() && wifi.isConnected()) {
			// chinh bang false de wifi van chan ung dung he thong
			if(checkNetwork == UserDTO.CHECK_NETWORK_WIFI || checkNetwork == UserDTO.CHECK_NETWORK_ALL){
				isUnlock = false;
			} else{
				isUnlock = true;
			}
		} else if (mobile != null && mobile.isAvailable()
				&& mobile.isConnected()) {
			if(checkNetwork == UserDTO.CHECK_NETWORK_3G || checkNetwork == UserDTO.CHECK_NETWORK_ALL){
				isUnlock = false;
			} else{
				isUnlock = true;
			}
		} else{
			//unlock all
			isUnlock = true;
		}
	}

	/**
	 * Khong hien thi chan ung dung nua
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public static void unlockAppPrevent(boolean isUnlockApp) {
		isUnlock = isUnlockApp;
		// neu di tu ung dung dang su dung thi ko checknetwork nua(isUnlock = true)
		isCheckNetwork = !isUnlockApp;
		// reset bien dem de 5 phut sau khoa lai
		countTime = 0;
	}

	/**
	 * Khoi tao danh sach mac dinh khi chua dang nhap, clear data
	 * @author: banghn
	 */
	@SuppressWarnings("unused")
	private void initBlockListDefault(){
		lstBlackAppPrevent.add("com.android.settings");
		lstBlackAppPrevent.add("com.android.browser");
		lstBlackAppPrevent.add("com.android.chrome");
		lstBlackAppPrevent.add("org.mozilla.firefox");
		lstBlackAppPrevent.add("com.android.vending");
		lstBlackAppPrevent.add("com.google.android.youtube");
		lstBlackAppPrevent.add("com.google.android.apps.plus");
	}

	 /**
	 * Khoi tao ds ung dung bi chan
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void initBlackListApp() {
		lstBlackAppPrevent.clear();
		List<ApplicationInfo> packages = getPackageManager()
				.getInstalledApplications(PackageManager.GET_META_DATA);
		ArrayList<String> whiteList = GlobalInfo.getInstance().getWhiteList();
		int sWhiteList = whiteList != null ? whiteList.size() : 0;
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		PackageManager pm = getPackageManager();
		ResolveInfo mInfo = pm.resolveActivity(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		String packageHome = (mInfo != null 
				&& mInfo.activityInfo != null 
				&& mInfo.activityInfo.packageName != null) ? mInfo.activityInfo.packageName : "";
		String myPackage = this.getPackageName();
		for (ApplicationInfo packageInfo : packages) {
			if (packageInfo != null && packageInfo.packageName != null) {
				boolean isAddBlacklist = true;
				
				if (packageInfo.packageName.equals(myPackage) || packageInfo.packageName.equals(packageHome)) {
					isAddBlacklist = false;
				} else if(sWhiteList > 0){
					for (int i = 0; i < sWhiteList; i++) {
						// kiem tra ung dung co trong whitelist ko, neu co thi bo qua
						if (packageInfo.packageName.equals(whiteList.get(i))) {
							isAddBlacklist = false;
							break;
						}
					}
				}
				if (isAddBlacklist) {
					lstBlackAppPrevent.add(packageInfo.packageName);
				}
			}
		}
	}
}