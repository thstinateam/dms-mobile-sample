/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

public class NvbhReportAsoViewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	public ArrayList<NvbhReportAsoDTO> listDto;
	public int totalData;
	public long totalPlan;
	public long totalDone;
	public double totalPercent;

	public NvbhReportAsoViewDTO() {
		listDto = new ArrayList<NvbhReportAsoDTO>();
		totalData = -1;
		totalPlan = 0;
		totalDone = 0;
		totalPercent = 0;
	}
}
