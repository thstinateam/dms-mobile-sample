/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateDetailResultlDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;

/**
 * 
 * TRANING_RATE_DETAIL_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:40:23 17-11-2014
 */
public class TRAINING_RATE_DETAIL_TABLE extends ABSTRACT_TABLE {
	public static final String TRAINING_RATE_DETAIL_ID = "TRAINING_RATE_DETAIL_ID";
	public static final String TRAINING_RATE_ID = "TRAINING_RATE_ID";
	public static final String CODE = "CODE";
	public static final String SHORT_NAME = "SHORT_NAME";
	public static final String FULL_NAME = "FULL_NAME";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String IS_DEFAULT_RATE = "IS_DEFAULT_RATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_STAFF_ID = "CREATE_STAFF_ID";

	public static final String TABLE_NAME = "TRAINING_RATE_DETAIL";

	public TRAINING_RATE_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { TRAINING_RATE_DETAIL_ID, TRAINING_RATE_ID,
				CODE, SHORT_NAME, FULL_NAME, ORDER_NUMBER, IS_DEFAULT_RATE,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * luu tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 9:03:32 AM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResult
	 * @return
	 * @throws Exception 
	 */
	public long saveStandard(TrainingRateDetailResultlDTO rateResult) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		try {
			// chua luu nhung chua xoa
			if (!rateResult.isSaved() && !rateResult.isDeleted()) {
				returnCode = insert(rateResult);
			} else if(rateResult.isSaved()) { // da luu
				// chua xoa thi update
				if(!rateResult.isDeleted())
					returnCode = update(rateResult);
//				else {// da xoa{
//					returnCode = delete(rateResult);
//					if(returnCode > 0){
//						SUP_TRAINING_RESULT_TABLE result = new SUP_TRAINING_RESULT_TABLE(mDB);
//						returnCode = result.delete(rateResult);
//					}
//				}
			} else if (!rateResult.isSaved() && rateResult.isDeleted()){
				returnCode = 0;
			}
		} catch (Exception e) {
			throw e;
		}
		return returnCode;
	}

	/**
	 * xoa tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 2:16:31 PM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	public long delete(TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		String[] params = { String.valueOf(rateResult.getTrainingRateDetailID()) };
		return delete(TRAINING_RATE_DETAIL_ID + " = ?", params);
	}

	/**
	 * cap nhat lai thong tin tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 1:48:10 PM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private long update(TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues value = initContentValuesUpdate(rateResult);
		String[] params = { String.valueOf(rateResult.getTrainingRateDetailID()) };
		return update(value, TRAINING_RATE_DETAIL_ID + " = ?", params);
	}

	/**
	 * update data
	 * 
	 * @author: dungdq3
	 * @since: 1:49:18 PM Nov 18, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private ContentValues initContentValuesUpdate(
			TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(ORDER_NUMBER, rateResult.getOrderNumber());
		editedValues.put(FULL_NAME, rateResult.getFullStandardName());
		editedValues.put(UPDATE_DATE, DateUtils.now());
		return editedValues;
	}

	/**
	 * luu tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 9:04:18 AM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private long insert(TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues cv = generateDataInsert(rateResult);
		return insert(null, cv);
	}

	/**
	 * generate data
	 * 
	 * @author: dungdq3
	 * @since: 9:06:35 AM Nov 18, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private ContentValues generateDataInsert(
			TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(TRAINING_RATE_DETAIL_ID, rateResult.getTrainingRateDetailID());
		editedValues.put(TRAINING_RATE_ID, rateResult.getTrainingRateID());
		editedValues.put(CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		editedValues.put(CODE, rateResult.getCode());
		editedValues.put(SHORT_NAME, rateResult.getShortStandardName()); 
		editedValues.put(FULL_NAME, rateResult.getFullStandardName()); 
		editedValues.put(ORDER_NUMBER, rateResult.getOrderNumber()); 
		editedValues.put(IS_DEFAULT_RATE, rateResult.getDefaultRate()); 
		editedValues.put(CREATE_DATE, rateResult.getCreateDate());
		return editedValues;
	}
}
