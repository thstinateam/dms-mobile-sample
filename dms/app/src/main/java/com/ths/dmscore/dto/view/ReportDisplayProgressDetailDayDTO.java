package com.ths.dmscore.dto.view;

import java.util.ArrayList;


public class ReportDisplayProgressDetailDayDTO {
	public long RemainSaleTotal;

	public ArrayList<ReportDisplayProgressDetailDayRowDTO> listItem;
	public int totalSize;

	public ArrayList<SupervisorReportDisplayProgressStaffItem> listStaff;

	public  ReportDisplayProgressDetailDayDTO(){
		listItem = new ArrayList<ReportDisplayProgressDetailDayRowDTO>();
		listStaff = new ArrayList<SupervisorReportDisplayProgressStaffItem>();
		
	}
	public void addItem(ReportDisplayProgressDetailDayRowDTO c) {
		listItem.add(c);
		RemainSaleTotal +=c.amountPlan;
	}
}
