package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.dto.db.KeyShopDTO;

/**
 * DTO cho man hinh chup hinh key shop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class VoteKeyShopViewDTO {

	// ds hinh anh
	public VoteKeyShopImageViewDTO image = new VoteKeyShopImageViewDTO();
	public ArrayList<KeyShopDTO> lstKeyShop = new ArrayList<KeyShopDTO>();
	public double shopDistance = 0;
}
