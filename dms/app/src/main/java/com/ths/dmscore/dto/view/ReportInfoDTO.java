/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * report infomation
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class ReportInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final int REPORT_DETAIL = 0;
	public static final int REPORT_TOTAL = 1;
	// report content
	public String content;
	public String code;
	// report quota
	public double amountPlan;
	public long quantityPlan;
	// report attain
	public double amount;
	public long quantity;
	// report duyet
	public double amountApproved;
	public long quantityApproved;
	// report pregress
	public double amountProgress;
	public double quantityProgress;
	// report remain
	public double amountRemain;
	public long quantityRemain;
	// report pending
	public double amountPending;
	public long quantityPending;
	// number customer do not have amount in month
	public int numCustomerDoNotAmount;
	// number customer visit in month
	public int numCustomerVisitPlanInMonth;
	// type report
	public int reportType;
	// diem ban hang
	public double startSale;
	public static final String TYPE_AMOUNT = "DS";
	public static final String TYPE_QUANTITY = "SL";

	public ReportInfoDTO() {
		reportType = REPORT_DETAIL;
		content = "";
		code = "";
		amountPlan = 0.0;
		amount =  0.0;
		amountApproved =  0.0;
		amountProgress =  0.0;
		amountRemain =  0.0;
		startSale = 0.0;
	}

	/**
	 *
	 * khoi tao doi tuong voi cursor cho bao cao ngay
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: TRUNGHQM
	 * @date: 25/01/2014
	 */
	public void initWithCursorReportDate(Cursor c) {
		// title name
		this.content = CursorUtil.getString(c, "PRODUCT_INFO_NAME");

		// plan
		this.amountPlan = CursorUtil.getDouble(c, "AMOUNT_PLAN");

		// attain
		this.amount = CursorUtil.getDouble(c, "AMOUNT");
		// attain
		this.amountApproved = CursorUtil.getDouble(c, "AMOUNT_APPROVED");
		this.amountPending = CursorUtil.getDouble(c, "AMOUNT_PENDING");

		// plan
		this.quantityPlan = CursorUtil.getLong(c, "QUANTITY_PLAN");

		// attain
		this.quantity = CursorUtil.getLong(c, "QUANTITY");
		// attain
		this.quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
		this.quantityPending = CursorUtil.getLong(c, "QUANTITY_PENDING");


		// set type object (total or detail)
		String codeDBH = Constants.STR_BLANK;
		codeDBH = CursorUtil.getString(c, "PRODUCT_INFO_CODE");
		if (codeDBH == null) {
			codeDBH = Constants.STR_BLANK;
		}
		code = codeDBH;
		// tinh thong tin diem ban hang
		if ("DBH".contentEquals(codeDBH)) {
			this.startSale = this.amount;
			this.reportType = REPORT_TOTAL;
			// this.quota = this.attain;
			this.amount = 0.0;
			this.amountApproved = 0.0;
			this.amountRemain = 0.0;
			this.amountProgress = 0.0;
		} else {
			// remain
			amountRemain = StringUtil.calRemainUsingRound(amountPlan, amount);
			amountProgress = StringUtil.calPercentUsingRound(amountPlan, amount);
			// remain
			quantityRemain = StringUtil.calRemain(quantityPlan, quantity);
			quantityProgress = StringUtil.calPercentUsingRound(quantityPlan, quantity);

		}
	}

	/**
	 *
	 * khoi tao doi tuong voi cursor cho bao cao ngay
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: TRUNGHQM
	 * @date: 25/01/2014
	 */
	public void initWithCursorReportByDate(Cursor c) {
		// title name
		content = CursorUtil.getString(c, "NAME");



		// set type object (total or detail)
		code = CursorUtil.getString(c, "CODE");
		// tinh thong tin diem ban hang
		if ("DBH".contentEquals(code)) {
			this.startSale = this.amount;
			this.reportType = REPORT_TOTAL;
			// this.quota = this.attain;
			this.amount = 0;
			this.amountApproved = 0;
			this.amountRemain = 0;
			this.amountProgress = 0;
		} else {
			if (TYPE_AMOUNT.equals(code)) {
				// plan
				amountPlan = CursorUtil.getDouble(c, "PLAN",1,GlobalInfo.getInstance().getSysNumRounding());

				// attain
				amount = CursorUtil.getDouble(c, "ACTUALLY",1,GlobalInfo.getInstance().getSysNumRounding());

				// duyet
				amountApproved = CursorUtil.getDouble(c, "ACTUALLY_APPROVED",1,GlobalInfo.getInstance().getSysNumRounding());

				amountRemain = StringUtil.calRemainUsingRound(amountPlan, amount);
				amountProgress = StringUtil.calPercentUsingRound(amountPlan, amount);

			}else{
				// plan
				quantityPlan = CursorUtil.getLong(c, "PLAN");

				// attain
				quantity = CursorUtil.getLong(c, "ACTUALLY");

				// duyet
				quantityApproved = CursorUtil.getLong(c, "ACTUALLY_APPROVED");

				quantityRemain = StringUtil.calRemain(quantityPlan, quantity);
				quantityProgress = StringUtil.calPercentUsingRound(quantityPlan, quantity);
			}
		}
	}
}
