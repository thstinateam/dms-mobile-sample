/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * Luu cac cau hinh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class AP_PARAM_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String AP_PARAM_ID = "AP_PARAM_ID";
	// ma tham so cau hinh
	public static final String AP_PARAM_CODE = "AP_PARAM_CODE";
	// gia tri tham so cau hinh
	public static final String VALUE = "VALUE";
	// trang thai: 0: het hieu luc, 1: dang hieu luc
	public static final String STATUS = "STATUS";
	// ghi chu
	public static final String DESCRIPTION = "DESCRIPTION";
	// ten
	public static final String AP_PARAM_NAME = "AP_PARAM_NAME";
	// type
	public static final String TYPE = "TYPE";

	private static final String TABLE_APP_PARAM = "AP_PARAM";

	public AP_PARAM_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_APP_PARAM;
		this.columns = new String[] { AP_PARAM_ID, AP_PARAM_CODE, VALUE, STATUS, DESCRIPTION, AP_PARAM_NAME, TYPE,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((ApParamDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(ApParamDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		ApParamDTO disDTO = (ApParamDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.getApParamCode() };
		return update(value, TYPE + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param apParamId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(TYPE + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		ApParamDTO paramDTO = (ApParamDTO) dto;
		String[] params = { paramDTO.getApParamCode() };
		return delete(TYPE + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: TruongHN
	 * @param id
	 * @return: ApParamDTO
	 * @throws:
	 */
	public ApParamDTO getRowById(String id) {
		ApParamDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(TYPE + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initApParamDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private ApParamDTO initApParamDTOFromCursor(Cursor c) {
		ApParamDTO dto = new ApParamDTO();
//		dto.apParamId = (c.getLong(c.getColumnIndex(AP_PARAM_ID)));
//		dto.apParamCode = (c.getString(c.getColumnIndex(AP_PARAM_CODE)));
//		dto.status = (c.getString(c.getColumnIndex(STATUS)));
//		dto.value = (c.getString(c.getColumnIndex(VALUE)));
//		dto.description = (c.getString(c.getColumnIndex(DESCRIPTION)));
//		dto.apParamName = (c.getString(c.getColumnIndex(AP_PARAM_NAME)));
//		dto.type = (c.getString(c.getColumnIndex(TYPE)));

		dto.setApParamId(CursorUtil.getLong(c, AP_PARAM_ID));
		dto.setApParamCode(CursorUtil.getString(c, AP_PARAM_CODE));
		dto.setStatus(CursorUtil.getInt(c, STATUS));
		dto.setValue(CursorUtil.getString(c, VALUE));
		dto.setDescription(CursorUtil.getString(c, DESCRIPTION));
		dto.setApParamName(CursorUtil.getString(c, AP_PARAM_NAME));
		dto.setApParamType(CursorUtil.getString(c, TYPE));
		return dto;
	}
	
	private ApParamDTO initApParamPriorityDTOFromCursor(Cursor c) {
		ApParamDTO dto = new ApParamDTO();
		dto.setApParamId(CursorUtil.getLong(c, AP_PARAM_ID));
		dto.setApParamCode(CursorUtil.getString(c, AP_PARAM_CODE));
		dto.setStatus(CursorUtil.getInt(c, STATUS));
		//do web dang su dung code lam value
		dto.setValue(CursorUtil.getString(c, AP_PARAM_CODE));
		dto.setDescription(CursorUtil.getString(c, DESCRIPTION));
		dto.setApParamName(CursorUtil.getString(c, AP_PARAM_NAME));
		dto.setApParamType(CursorUtil.getString(c, TYPE));
		return dto;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: Vector<ApParamDTO>
	 * @throws:
	 */
	public Vector<ApParamDTO> getAllRow() {
		Vector<ApParamDTO> v = new Vector<ApParamDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				ApParamDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initApParamDTOFromCursor(c);
						v.addElement(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	/**
	 * * Lay ds param theo type
	 *
	 * @author: TruongHN
	 * @param typeName
	 * @return: ArrayList<ApParamDTO>
	 * @throws:
	 */
	public ArrayList<ApParamDTO> getPriorityByType(String code) {
		ArrayList<ApParamDTO> v = new ArrayList<ApParamDTO>();
		Cursor c = null;
		try {
			String[] params = { "" + code };
			c = query(TYPE + " = ? AND STATUS = 1", params, null, null, null);
			if (c != null) {
				ApParamDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initApParamPriorityDTOFromCursor(c);
						v.add(dto);
					} while (c.moveToNext());
				}
			}
			// c.close();
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return v;

	}

	private ContentValues initDataRow(ApParamDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(AP_PARAM_ID, String.valueOf(dto.getApParamId()));
		editedValues.put(AP_PARAM_CODE, dto.getApParamCode());
		editedValues.put(VALUE, dto.getValue());
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(DESCRIPTION, dto.getDescription());

		return editedValues;
	}

	public ArrayList<DisplayProgrameItemDTO> getListTypeDisplayPrograme() throws Exception{
		ArrayList<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		String queryGetListTypeDisplayPrograme = "SELECT * FROM AP_PARAM WHERE AP_PARAM_CODE = ? AND STATUS = 1";
		Cursor c = null;
		String params[] = { "DISPLAY_PROGR_TYPE" };
		try {
			// get total row first
			c = rawQuery(queryGetListTypeDisplayPrograme, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO dto = initComboBoxDisProItemFromCursor(c);
						result.add(dto);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {

			}
		}
		return result;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: ComboBoxDisplayProgrameItemDTO
	 * @throws:
	 */
	private DisplayProgrameItemDTO initComboBoxDisProItemFromCursor(Cursor c) {
		// TODO Auto-generated method stub
		DisplayProgrameItemDTO result = new DisplayProgrameItemDTO();
		result.value = (CursorUtil.getString(c, VALUE));
		result.name = (CursorUtil.getString(c, DESCRIPTION));
		return result;
	}

	/**
	 * Lay ds van de cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: List<DisplayProgrameItemDTO>
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getListTBHVProblemType() {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		String sqlQuery = "SELECT AP_PARAM_CODE VALUE, AP_PARAM_NAME as DESCRIPTION FROM AP_PARAM WHERE TYPE = ? AND STATUS = 1";
		Cursor c = null;
		String params[] = { "FEEDBACK_TYPE_TBHV" };
		try {
			// get total row first
			c = rawQuery(sqlQuery, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO dto = initComboBoxDisProItemFromCursor(c);
						result.add(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			result = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {

			}
		}
		return result;
	}

	/**
	 * Lay cac gia tri default trong chuong trinh
	 *
	 * @author: TruongHN
	 * @return: ArrayList<ApParamDTO>
	 * @throws:
	 */
	public ArrayList<ApParamDTO> getConstantsApp() {
		ArrayList<ApParamDTO> v = new ArrayList<ApParamDTO>();
		Cursor c = null;
		try {
			String strStaffTypeId = "_" + GlobalInfo.getInstance().getProfile().getUserData().getStaffTypeId();
			String query = "SELECT * FROM AP_PARAM WHERE AP_PARAM_CODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			String[] params = { "FACTOR", "TIME_TEST_ORDER", "RADIUS_OF_POSITION", "TIME_TRIG_POSITION",
					"TIME_SYNC_TO_SERVER", "TIME_ALLOW_SYN_DATA", "TIMEOUT_WHEN_IDLE", "ALLOW_EDIT_PROMOTION",
					"SERVER_IMAGE_PRO_VNM", "TIME_TRIG_POS_ATTEND", "DISTANCE_ALLOW_FINISH",
					"VISIT_TIME_INTERVAL", "VISIT_TIME_ALLOW", "MAX_LOGKPI_ALLOW", "GSNPP_NUM_DAY_SKU",
					"FLAG_KEYBOARD" ,"SYS_MAP_TYPE","MANAGER_VIEW_LEVEL", "SYS_CAL_UNAPPROVED", "SYS_CURRENCY_DIVIDE", "SYS_SALE_ROUTE",
					"SYS_CURRENCY", "SYS_DIGIT_DECIMAL", "SYS_DECIMAL_POINT", "SYS_DATE_FORMAT", "SYS_NUM_ROUNDING", "SYS_NUM_ROUNDING_PROMOTION", "SYS_CAL_DATE"
					, "FLAG_VALIDATE_MOCK_LOCATION", "BLACK_LIST_MOCK_LOCATION", "WHITE_LIST_MOCK_LOCATION"
					, "INTERVAL_FUSED_POSITION", "FAST_INTERVAL_FUSED_POSITION", "RADIUS_OF_POSITION_FUSED", "FUSED_POSTION_PRIORITY", "MAX_TIME_WRONG"
					, "DISTANCE_TIME_DRAW_ROUTING", "RADIUS_DRAW_ROUTING", "MAX_SIZE_UPLOAD_ATTACH", "WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH","REGISTER_APPROVED_KEYSHOP", "CHECK_APP_INSTALL" + strStaffTypeId, "CHECK_NETWORK" + strStaffTypeId, "WHITE_LIST_APP_INSTALL" + strStaffTypeId, "TIME_UNLOCK_APP_ACCESS" + strStaffTypeId
					,"REGISTER_CTHTTM","ALLOW_DISTANCE_ORDER","DAY_OF_CYCLE", "CSKH_INFO"
					, "READ_TIME_OUT", "READ_TIME_OUT_SYNDATA", "CONNECT_TIME_OUT_LOGIN", "READ_TIME_OUT_LOGIN", "CONNECT_TIME_OUT_DOWNLOAD_FILE", "READ_TIME_OUT_DOWNLOAD_FILE",
					"CHECK_DIS_VISIT_FIRST_IN_ROUTE","CHECK_DIS_VISIT_FIRST_OUT_ROUTE","CHECK_DIS_ORDER_FIRST_IN_ROUTE","CHECK_DIS_ORDER_FIRST_OUT_ROUTE","CHECK_DIS_VISIT_SECOND_IN_ROUTE","CHECK_DIS_VISIT_SECOND_OUT_ROUTE"
					,"CHECK_DIS_ORDER_SECOND_IN_ROUTE","CHECK_DIS_ORDER_SECOND_OUT_ROUTE", "CHECK_CUSTOMER_LOCATION_VISIT", "SYS_ROUTE_TYPE", "SYS_CONVERT_QUANTITY_CONFIG", "VALIDATE_MULTI_CLICK_ORDER",
					"VT_MAP_SET_SERVER","VT_MAP_PROTOCOL","VT_MAP_PORT","VT_MAP_IP"};

			c = rawQuery(query, params);
			if (c != null) {
				ApParamDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initApParamDTOFromCursor(c);
						v.add(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getConstantsApp", e);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.i("getConstantsApp", e.getMessage());
			}
		}
		return v;

	}

	/**
	 * Lay param AllowResetLocation
	 *
	 * @author: DungNX
	 * @return: ArrayList<ApParamDTO>
	 * @throws:
	 */
	public ApParamDTO getAllowResetLocation(Bundle ext) {
		ApParamDTO v = new ApParamDTO();
		Cursor c = null;
		try {
			String shopCode = ext.getString(IntentConstants.INTENT_SHOP_CODE);
			String typeAllowResetLocation = shopCode + "_ALLOW_RESET_LOCATION";
			String query = "SELECT * FROM AP_PARAM WHERE TYPE IN (?) and STATUS = 1";
			String[] params = { typeAllowResetLocation };
			c = rawQuery(query, params);
			if (c != null) {
				if (c.moveToFirst()) {
						v = initApParamDTOFromCursor(c);
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getConstantsApp", e.getMessage());
			}
		}
		return v;

	}

	/**
	 *
	 * get list problem status of gsnpp
	 *
	 * @param data
	 * @return
	 * @return: ArrayList<ProblemStatusDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public ArrayList<ApParamDTO> getListProblemStatus() {
		ArrayList<ApParamDTO> result = new ArrayList<ApParamDTO>();
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT distinct * ");
			sqlQuery.append("FROM   ap_param ");
			sqlQuery.append("WHERE  status = 1 ");
			sqlQuery.append("       AND type = 'FEEDBACK_TYPE_TBHV' ");
			sqlQuery.append("       order by value ");

			String params[] = new String[] {};
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c.moveToFirst()) {
				do {
					ApParamDTO item = new ApParamDTO();
					item.initObjectWithCursor(c);
					result.add(item);
					result.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			result = null;
			// TODO: handle exception
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * lay phan tram mat dinh cua 1 CTTB cho mot ngay lam viec
	 *
	 * @return
	 * @return: float
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 11, 2013
	 */
	public float getPercentDefaultInDay() {
		float percent = 0;
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT ap_param_code ");
			sqlQuery.append("FROM   ap_param ");
			sqlQuery.append("WHERE  status = 1 ");
			sqlQuery.append("       AND type = 'DP_ORDER_PERCENT' ");
			c = this.rawQuery(sqlQuery.toString(), new String[] {});
			if (c != null && c.moveToFirst()) {
				percent = CursorUtil.getFloat(c, AP_PARAM_TABLE.AP_PARAM_CODE);
			}
		} catch (Exception e) {
			percent = 0;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return percent;
	}

	/**
	 *
	 * lay danh sach loai van de
	 *
	 * @return
	 * @return: float
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 11, 2013
	 */
	public Vector<ApParamDTO> getListTypeProblemNVBHGSNPP() {
		Cursor c = null;
		Vector<ApParamDTO> result = new Vector<ApParamDTO>();
		try {
			String sqlQuery = "SELECT AP_PARAM_NAME, AP_PARAM_CODE FROM AP_PARAM WHERE TYPE = 'FEEDBACK_TYPE' AND STATUS = 1 ORDER BY AP_PARAM_NAME ";
			String params[] = new String[] {};
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c.moveToFirst()) {
				do {
					ApParamDTO item = new ApParamDTO();
					item.initObjectWithCursor(c);
					result.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param from
	 * @return
	 * @return: ArrayList<String>void
	 * @throws:
	 */
	public ArrayList<ApParamDTO> requestGetTypeFeedback(int from) {
		ArrayList<ApParamDTO> appList = new ArrayList<ApParamDTO>();
		Cursor c_ap_param = null;
		StringBuffer var2 = new StringBuffer();
		var2.append(" select * from ap_param ap where ap.type like 'FEEDBACK_TYPE' and ap.status = 1 order by ap.ap_param_name");
		try {
			c_ap_param = rawQuery(var2.toString(), null);
			if (c_ap_param != null) {
				if (c_ap_param.moveToFirst()) {
					do {
						ApParamDTO dto = new ApParamDTO();
						dto.initObjectWithCursor(c_ap_param);
						appList.add(dto);
					} while (c_ap_param.moveToNext());
				}
			}
		} finally {
			try {
				if (c_ap_param != null) {
					c_ap_param.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return appList;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param staffTypeId
	 * @return
	 * @return: Stringvoid
	 * @throws:
	 */
	public String requestStaffType(int staffTypeId) {
		String staffType = null;
		StringBuffer sqlRequest = new StringBuffer();
		sqlRequest.append("SELECT * FROM AP_PARAM WHERE AP_PARAM_ID = ?");
		Cursor c = null;
		try {
			c = rawQuery(sqlRequest.toString(), new String[] { "" + staffTypeId });
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ApParamDTO object = new ApParamDTO();
						object.initObjectWithCursor(c);
						staffType = object.getApParamCode();
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}
		return staffType;
	}

	/**
	* Mo ta chuc nang cua ham
	* @param: Tham so cua ham
	* @return: Ket qua tra ve
	* @throws: Ngoai le do ham dua ra (neu co)
	*/
	public ArrayList<ApParamDTO> getApparam() {
		// TODO Auto-generated method stub
		ArrayList<ApParamDTO> appList = new ArrayList<ApParamDTO>();
		Cursor cursor = null;
		StringBuffer var2 = new StringBuffer();
		var2.append(" select * from ap_param ap where ap.value in (6,7,8,9) and ap.AP_PARAM_CODE like '%GSNPP%' and ap.status = 1 ");
		try {
			cursor = rawQuery(var2.toString(), null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						ApParamDTO dto = new ApParamDTO();
						dto.initObjectWithCursor(cursor);
						appList.add(dto);
					} while (cursor.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return appList;
	}
	/**
	*  Lay ds params (co param moi add)
	*  @author: DungNX
	*  @param ext
	*  @return
	*  @return: List<ApParamDTO>
	 * @throws Exception
	*  @throws:
	*/

	public List<ApParamDTO> getListParamForTakeAttendanceSalePerson(Bundle ext) throws Exception {
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);

		List<ApParamDTO> listParam = new ArrayList<ApParamDTO>();

		SHOP_TABLE shop = new SHOP_TABLE(mDB);
		ArrayList<String> listShop = shop.getShopRecursive(shopId);

		for (String str : listShop) {
			StringBuilder sqlRequest = new StringBuilder();
			sqlRequest.append("SELECT * ");
			sqlRequest.append("FROM   shop_param sp ");
			sqlRequest.append("WHERE  sp.status = 1 ");
			sqlRequest
					.append("       AND sp.type IN ('CC_START','CC_END', 'CC_DISTANCE' )");
			sqlRequest.append("       AND sp.shop_id = ? ");
			sqlRequest.append(" limit 3");

			String[] params = new String[] { str };
			Cursor c = null;
			ApParamDTO param1 = null;
			ApParamDTO param2 = null;
			ApParamDTO param3 = null;
			try {
				c = rawQuery(sqlRequest.toString(), params);
				if (c != null) {
					if (c.moveToFirst()) {
						do {
							ShopParamDTO objectShop = new ShopParamDTO();
							objectShop.initObjectWithCursor(c);

							ApParamDTO object = new ApParamDTO();
							object.setApParamCode(objectShop.type);
							object.setValue(objectShop.value);
							object.setDescription(objectShop.name);

							if (object.getApParamCode().contains("CC_START")) {
								param1 = object;
							} else if (object.getApParamCode().contains("CC_END")) {
								param2 = object;
							} else if (object.getApParamCode()
									.contains("CC_DISTANCE")) {
								param3 = object;
							}
						} while (c.moveToNext());
					}
				}
			} finally {
				if (c != null) {
					try {
						c.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			}

			if(param1 != null && param2 != null && param3 != null){
				listParam.add(param1);
				listParam.add(param2);
				listParam.add(param3);
				break;
			}
		}
		return listParam;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @return
	 * @return: ArrayList<String>void
	 * @throws:
	 */
	public ArrayList<ApParamDTO> getListDocumentType() {
		ArrayList<ApParamDTO> docTypeList = new ArrayList<ApParamDTO>();
		Cursor cursor = null;
		StringBuffer var2 = new StringBuffer();
		var2.append(" select * from ap_param ap where ap.type like 'OFFICE_DOCUMENT_TYPE' and ap.status = 1 order by ap.ap_param_code");
		try {
			cursor = rawQuery(var2.toString(), null);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						ApParamDTO dto = new ApParamDTO();
						dto.initObjectWithCursor(cursor);
						docTypeList.add(dto);
					} while (cursor.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return docTypeList;
	}


	 /**
	 * Lay param theo mot type code bat ki
	 * @author: Tuanlt11
	 * @param code
	 * @return
	 * @return: ApParamDTO
	 * @throws:
	*/
	public ApParamDTO getParamByType(String code) {
		ApParamDTO v = new ApParamDTO();
		Cursor c = null;
		try {
			String[] params = { "" + code };
			c = query(AP_PARAM_CODE + " = ? AND STATUS = 1", params, null,
					null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					v = initApParamDTOFromCursor(c);
				}
			}
			// c.close();
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return v;

	}

	public ArrayList<ApParamDTO> getSalePositionTypeParams() {
		ArrayList<ApParamDTO> appList = new ArrayList<ApParamDTO>();
		Cursor c_ap_param = null;
		StringBuffer var2 = new StringBuffer();
		var2.append(" select * from ap_param ap where ap.type like 'CUSTOMER_SALE_POSITION' and ap.status = 1 order by ap.ap_param_name");
		try {
			c_ap_param = rawQuery(var2.toString(), null);
			if (c_ap_param != null) {
				if (c_ap_param.moveToFirst()) {
					do {
						ApParamDTO dto = new ApParamDTO();
						dto.initObjectWithCursor(c_ap_param);
						appList.add(dto);
					} while (c_ap_param.moveToNext());
				}
			}
		} finally {
			try {
				if (c_ap_param != null) {
					c_ap_param.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return appList;
	}
}
