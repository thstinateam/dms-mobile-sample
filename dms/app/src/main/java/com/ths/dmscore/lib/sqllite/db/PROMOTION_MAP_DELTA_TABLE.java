/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.QuantityRevicevedDTO;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class PROMOTION_MAP_DELTA_TABLE extends ABSTRACT_TABLE {

	public static final String PROMOTION_MAP_DELTA_ID = "PROMOTION_MAP_DELTA_ID";
	public static final String PROMOTION_SHOP_MAP_ID = "PROMOTION_SHOP_MAP_ID";
	public static final String PROMOTION_STAFF_MAP_ID = "PROMOTION_STAFF_MAP_ID";
	public static final String PROMOTION_CUSTOMER_MAP_ID = "PROMOTION_CUSTOMER_MAP_ID";
	public static final String QUANTITY_DELTA = "QUANTITY_DELTA";
	public static final String AMOUNT_DELTA = "AMOUNT_DELTA";
	public static final String NUM_DELTA = "NUM_DELTA";
	public static final String SOURCE = "SOURCE";
	public static final String ACTION = "ACTION";
	public static final String FROM_OBJECT_ID = "FROM_OBJECT_ID";
	public static final String ERROR = "ERROR";
	public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";

	public static final String TABLE_NAME = "PROMOTION_MAP_DELTA";
	public PROMOTION_MAP_DELTA_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PROMOTION_MAP_DELTA_ID, PROMOTION_SHOP_MAP_ID, PROMOTION_STAFF_MAP_ID,
				PROMOTION_CUSTOMER_MAP_ID, QUANTITY_DELTA, AMOUNT_DELTA, NUM_DELTA, 
				SOURCE, ACTION, FROM_OBJECT_ID, ERROR, STAFF_ID,
				ERROR_MESSAGE, PROMOTION_PROGRAM_ID, SHOP_ID, CUSTOMER_ID,
				CREATE_USER, CREATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		QuantityRevicevedDTO sDto = (QuantityRevicevedDTO) dto;
		return insert(null, sDto.initInsertPromotionMapDelta());
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}
