/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * thong tin man hinh bao cao thong ke don tong trong ngay
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SaleStatisticsProductInDayInfoViewDTO {
	// list product info
	public ArrayList<SaleProductInfoDTO> listProduct = new ArrayList<SaleProductInfoDTO>();
	// list industry product
	public ArrayList<String> listIndustry = new ArrayList<String>();
	
	
	/**
	 * construct object
	 */
	public SaleStatisticsProductInDayInfoViewDTO(){
		listIndustry = new ArrayList<String>();
		listIndustry.add(StringUtil.getString(R.string.TEXT_ALL));
		listProduct = new ArrayList<SaleProductInfoDTO>();
		
	}
	
	public void initTotalSaleStatisticsProductInDayInfo(){
		
	}
}
