package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;


public class ReportDisplayProgressDetailDayRowDTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	public long rptStDisplayProgrameId;
	public long customerId;
	public String customerCode;
	public String customerName;
	public String customerAddress;
	public double amountPlan;
	public long quantityPlan;
	public double amount;
	public long quantity;
	public String amountQuantityPlan;
	public String amountQuantity;
	public int result = 1;
	public String displayCode;
	public String displayName;
	public String displayLevel;
	public String displayType;
	public int type  = 0;
	public static final int TYPE_AMOUNT = 1;
	public static final int TYPE_QUANTITY = 2;
	public static final int TYPE_AMOUNT_QUANTITY = 3;


	public ReportDisplayProgressDetailDayRowDTO() {
		customerCode = "";
		customerName = "";
		customerAddress = "";
		amountPlan = 0;
		displayCode = "";
		displayName = "";
	}

	/**
	 * parse du lieu cho MH bao cao tien do TTTB cua GSNPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return: void
	 * @throws:
	 */
	void parseSupervisorDataFromCusor(Cursor c) {

	}

	/**
	 * Parse du lieu cho TBHV
	 * @author: BANGHN
	 * @param c
	 * @return
	 */
	public void parseReportDisplayProgressDetailDayRow(Cursor c) {
		rptStDisplayProgrameId = CursorUtil.getLong(c, "RPT_DISPLAY_PROGRAM_ID", 0);
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE", "");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME", "");
		customerAddress = CursorUtil.getString(c, "CUSTOMER_ADDRESS", "");
		displayCode = CursorUtil.getString(c, "DISPLAY_PROGRAM_CODE", "");
		displayName = CursorUtil.getString(c, "DISPLAY_PROGRAM_NAME", "");
		displayLevel = CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL_CODE", "");
		type = CursorUtil.getInt(c, "DISPLAY_PROGRAM_TYPE");
		result = CursorUtil.getInt(c, "RESULT", 0);
		quantity = CursorUtil.getLong(c, "QUANTITY", 0);
		quantityPlan = CursorUtil.getLong(c, "QUANTITY_PLAN", 0);
		amount = CursorUtil.getDouble(c, "AMOUNT");
		amountPlan = CursorUtil.getDouble(c, "AMOUNT_PLAN");
		if (type == TYPE_QUANTITY) {
			displayType = StringUtil.getString(R.string.TEXT_SL);
		} else if(type == TYPE_AMOUNT){
			displayType = StringUtil.getString(R.string.TEXT_DS);
		}else{
			displayType = StringUtil.getString(R.string.TEXT_DS) + "/" + StringUtil.getString(R.string.TEXT_SL);
		}
	}
}
