/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import net.sqlcipher.database.SQLiteDatabase;

import android.support.v4.util.LongSparseArray;

import com.ths.dmscore.dto.BigDecimalRound;
import com.ths.dmscore.dto.db.BuyInfoLevel;
import com.ths.dmscore.dto.db.GroupLevelDTO;
import com.ths.dmscore.dto.db.GroupMappingDTO;
import com.ths.dmscore.dto.db.ProductGroupDTO;
import com.ths.dmscore.dto.db.PromotionProDetailDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dms.R;

/**
 * Class dung de tinh toan khuyen mai
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CalPromotions {
	public static final String ZV01 = "ZV01";//Line-Qtty- Percent
	public static final String ZV02 = "ZV02";//Line-Qtty-Amt
	public static final String ZV03 = "ZV03";//Line-Qtty-FreeItem
	public static final String ZV04 = "ZV04";//Line-Amt-Percent
	public static final String ZV05 = "ZV05";//Line-Amt-Amt
	public static final String ZV06 = "ZV06";//Line-Amt-FreeItem
	public static final String ZV07 = "ZV07";//Group-Qtty-Percent
	public static final String ZV08 = "ZV08";//Group-Qtty-Amount
	public static final String ZV09 = "ZV09";//Group-Qtty-FreeItem
	public static final String ZV10 = "ZV10";//Group-Amt-Percent
	public static final String ZV11 = "ZV11";//Group-Amt-Amount
	public static final String ZV12 = "ZV12";//Group-Amt-FreeItem
	public static final String ZV13 = "ZV13";//Bundle-Qtty-Percent
	public static final String ZV14 = "ZV14";//Bundle-Qtty-Amount
	public static final String ZV15 = "ZV15";//Bundle-Qtty-FreeItem
	public static final String ZV16 = "ZV16";//Bundle-Amt-Percent
	public static final String ZV17 = "ZV17";//Bundle-Amt-Amt
	public static final String ZV18 = "ZV18";//Bundle-Amt-FreeItem
	public static final String ZV19 = "ZV19";//Docmt-Amt-Percent
	public static final String ZV20 = "ZV20";//Docmt-Amt-Amt
	public static final String ZV21 = "ZV21";//Docmt-Amt-FreeItem
	public static final String ZV22 = "ZV22";//Docmt-Amt-FreeItem
	public static final String ZV23 = "ZV23";//Accumulation
	public static final String ZV24 = "ZV24";//New open
	public static final String KS = "KS";//keyshop

	//Dung cho viec lam tron
	// Lam tron 2 chu so sau dau phay
	public static final int ROUND_PERCENT = 100;
	// Lay gia tri thap phan chinh xac
	public static final int ROUND_NO_PERCENT = 100 * ROUND_PERCENT;

	/**
	 * Tinh KM cho don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param orderView
	 * @param promotionProEntity
	 * @param sortListProductSale
	 * @param listProductPromotionsale
	 * @param keyList
	 * @param sortListOutPut
	 * @param mDB
	 * @param hmProgrameProduct
	 * @throws Exception
	 */
	public static SortedMap<Long, List<OrderDetailViewDTO>> calcPromotion(OrderViewDTO orderView,
			PromotionProgrameDTO promotionProEntity,
			SortedMap<Long, OrderDetailViewDTO> sortListProductSale,Long keyList,
			SQLiteDatabase mDB, int isPromotionProduct, LongSparseArray<ArrayList<PromotionProgrameDTO>> promotionArray, HashMap<Integer, ArrayList<Long>> hmProgrameProduct) throws Exception {

		PRODUCT_GROUP_TABLE productGroupTable = new PRODUCT_GROUP_TABLE(mDB);
		GROUP_MAPPING_TABLE groupMapping = new GROUP_MAPPING_TABLE(mDB);
		GROUP_LEVEL_TABLE groupLevelTable = new GROUP_LEVEL_TABLE(mDB);

		SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut = new TreeMap<Long, List<OrderDetailViewDTO>>();
		//Ds sp KM hien thi
		List<OrderDetailViewDTO> listProductPromotionsale = new ArrayList<OrderDetailViewDTO>();
		sortListOutPut.put(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT), listProductPromotionsale);

		//Lay ds product group order theo thu tu uu tien
		ArrayList<ProductGroupDTO> productGroupList = productGroupTable.getProductGroupsOfPromotionProgram(promotionProEntity.getPROMOTION_PROGRAM_ID(), ProductGroupDTO.GROUP_TYPE_PRODUCT, isPromotionProduct);
		//Danh sach cac san pham tham gia 1 CTKM
		boolean hasPromotionGroup = false;
		ArrayList<Long> calculatedKeyList = new ArrayList<Long>();
		for (ProductGroupDTO productGroup : productGroupList) {
			//Neu group co thu tu va duoc huong KM thi dung lai
			if(productGroup.orderNumber > 0 && hasPromotionGroup) {
				continue;
			}

			double totalQuantity = 0;
			double totalAmount = 0;
//			int totalQuantityFix = 0;
			double totalAmountFix = 0;
			double totalAmountOrder = orderView.orderInfo.getAmount();
			boolean hasPromotionLevel = false;
			// Reset price & quantiy
			for (OrderDetailViewDTO sortDetail : sortListProductSale.values()) {
				for (int i = 0, size = orderView.listBuyOrders.size(); i < size; i++) {
					OrderDetailViewDTO detail = orderView.listBuyOrders.get(i);
					if (sortDetail.orderDetailDTO.productId == detail.orderDetailDTO.productId) {
						sortDetail.orderDetailDTO.quantity = detail.orderDetailDTO.quantity;
						sortDetail.orderDetailDTO.quantityPackage = detail.orderDetailDTO.quantityPackage;
						sortDetail.orderDetailDTO.quantityRetail = detail.orderDetailDTO.quantityRetail;
						sortDetail.orderDetailDTO.price = detail.orderDetailDTO.price;
						sortDetail.orderDetailDTO.packagePrice = detail.orderDetailDTO.packagePrice;
						sortDetail.orderDetailDTO.setAmount(detail.orderDetailDTO.getAmount());
						break;
					}
				}
			}

			//Lay ds level & detail cua 1 group
			ArrayList<GroupLevelDTO> groupLevelList = groupLevelTable.getGroupLevelsOfProductGroup3(productGroup.productGroupId, isPromotionProduct);
			ArrayList<BuyInfoLevel> buyInfoLevelArray = new ArrayList<BuyInfoLevel>();

			//Neu khai bao co cau sai thi thoat ra: vd: co muc cha ma ko co muc con
			if(groupLevelList.isEmpty()) {
				continue;
			}
			//Chon 1 muc bat ki de tim ra cac san pham ban cua cung 1 nhom duoc KM
			GroupLevelDTO groupLevel = groupLevelList.get(0);
			//hasProduct = 0: KM tinh tren don hang, 1: KM tinh tren san pham

			if(groupLevel.hasProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
				//Tinh tong tien, tong so luong tham gia group
				for (GroupLevelDTO subLevel : groupLevel.subLevelArray) {
					//Thong tin muc con
					BuyInfoLevel infoSubLevel = new BuyInfoLevel();
					for(GroupLevelDetailDTO levelDetail : subLevel.levelDetailArray) {
						//Tim xem co mua san pham nhu yeu cau khong?
						Long key = Long.valueOf(levelDetail.productId);
						OrderDetailViewDTO p = sortListProductSale.get(key);
						if(p != null) {
							//Thong tin mua hang cua group
							totalQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", totalQuantity)))
									.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.exchangeQuantity()))).doubleValue();
							totalAmount += p.orderDetailDTO.getAmount();
							//De tinh %, tien tren sp ban
//								totalQuantityFix += p.orderDetailDTO.quantity;
							totalAmountFix += p.orderDetailDTO.getAmount();

							//Thong tin mua hang cua muc con
							infoSubLevel.totalQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", infoSubLevel.totalQuantity)))
									.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", p.exchangeQuantity()))).doubleValue();
							infoSubLevel.totalAmount += p.orderDetailDTO.getAmount();
							infoSubLevel.totalAmountFix += p.orderDetailDTO.getAmount();

							//San pham thuoc group -> thuoc 1 CTKM
							calculatedKeyList.add(key);
							//Neu khong tack lo, tack kho thi dung break
//								continue;
						}
					}
					buyInfoLevelArray.add(infoSubLevel);
				}
			} else {
				//Tinh ton tien, tong so luong cua ca don hang
				totalAmount = totalAmountOrder;
				totalAmountFix = totalAmountOrder;

				//Thong tin muc con
				BuyInfoLevel infoSubLevel = new BuyInfoLevel();
				//Thong tin mua hang cua muc con
				infoSubLevel.totalAmount += totalAmount;
				infoSubLevel.totalAmountFix += totalAmountFix;

				buyInfoLevelArray.add(infoSubLevel);
			}

			//Dat dieu kien mua so luong/so tien thoi thieu cua nhom thi moi duoc tinh KM tren nhom
			if((productGroup.minQuantity > 0 && totalQuantity >= productGroup.minQuantity) ||
					(productGroup.minAmount > 0 && totalAmount >= productGroup.minAmount) ||
					(productGroup.minQuantity == 0 && productGroup.minAmount == 0)) {
				boolean hasPercent = false;
				//Duyet danh sach cac muc
				for (GroupLevelDTO level : groupLevelList) {
					//KM dang bundle
					boolean isBundle = true;
					long totalAmountFixBundle = 0;
					ArrayList<GroupLevelDetailDTO> listBuyProduct = new ArrayList<GroupLevelDetailDTO>();
					//Duoc huong KM hay ko?
					boolean checkPro = true;

					//Muon dat KM thi phai dat qui cac muc con mua
					if(!level.subLevelArray.isEmpty()) {
						for(GroupLevelDTO subLevel : level.subLevelArray) {
							//Dat dieu kien mua so luong/so tien toi thieu tren cac muc con thi moi dat
							BuyInfoLevel subLevelBuyInfo = buyInfoLevelArray.get(level.subLevelArray.indexOf(subLevel));
							if((subLevel.minQuantity > 0 && subLevelBuyInfo.totalQuantity >= subLevel.minQuantity) ||
									(subLevel.minAmount > 0 && subLevelBuyInfo.totalAmount >= subLevel.minAmount) ||
									(subLevel.minQuantity == 0 && subLevel.minAmount == 0)) {
								// Neu muc cha co khai bao sp -> KM sp
								if(level.hasProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
									//Ds san pham cua tung muc
									for (GroupLevelDetailDTO levelDetail : subLevel.levelDetailArray) {
										//Tim xem co mua san pham nhu yeu cau khong?
										Long key = Long.valueOf(levelDetail.productId);
										OrderDetailViewDTO p = sortListProductSale.get(key);

										//Yeu cau phai mua nhung khong mua thi khong duoc KM
										if(levelDetail.isRequired == 1 && (p == null || p.orderDetailDTO.quantity == 0)) {
											checkPro = false;
											break;
										}

										//Yeu cau mua voi so luong, so tien ma khong mua du thi khong duoc KM
										if(levelDetail.isRequired == 1 && levelDetail.value > 0) {
											double value = 0;
											if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
												value = p.exchangeQuantity();
											} else if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
												value = p.orderDetailDTO.getAmount();
											}

											if(value < levelDetail.value) {
												checkPro = false;
												break;
											}
										} else {
											//Co 1 sp ko bat buoc thi la ko phai la KM Bundle
											isBundle = false;
										}
									}

									// Khong dat KM tren muc muc hoac 1 muc con
									if(!checkPro) {
										break;
									}
								//KM don hang
								} else {
									isBundle = false;
								}
							} else {
								checkPro = false;
								break;
							}
						}
					} else {
						checkPro = false;
					}

					int n = -1;
					int numReceived = 0;
					double amountReceived = 0;
					//Tinh KM neu thoa man dk KM
					if(checkPro) {
						for(GroupLevelDTO subLevel : level.subLevelArray) {
							BuyInfoLevel subLevelBuyInfo = buyInfoLevelArray.get(level.subLevelArray.indexOf(subLevel));

							//Tim ra boi so -> lay boi so nho nhat cua tung san pham lam boi so chung cho muc
							boolean hasRequireValue = false;
							for (GroupLevelDetailDTO levelDetail : subLevel.levelDetailArray) {
								//Tim xem co mua san pham nhu yeu cau khong?
								Long key = Long.valueOf(levelDetail.productId);
								OrderDetailViewDTO p = sortListProductSale.get(key);

								//San pham yeu cau ban thi phai thoa man so luong/ so tien qui dinh
								if(p != null) {
									double buyValue = 0;
									long requireValue = 0;
									if(levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
										buyValue = p.exchangeQuantity();
									} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
										buyValue = p.orderDetailDTO.getAmount();
									}

									requireValue = levelDetail.value;

									//Lay boi so nho nhat cua tung san pham
									if(levelDetail.isRequired == 1 && requireValue > 0) {
										if (n > buyValue / requireValue || n < 0) {
											n = (int) (buyValue / requireValue);
										}
										//Co yeu cau mua voi so luong bao nhieu
										hasRequireValue = true;
									}
								}
							}

							//Co tinh boi so tren muc sau khi tinh boi so tren tung san pham
							//Kiem tra xem KM theo so luong hay so tien
							double buyValue = 0;
							long requireValue = 0;
							if(subLevel.minQuantity > 0) {
								buyValue = subLevelBuyInfo.totalQuantity;
								requireValue = subLevel.minQuantity;
							} else if (subLevel.minAmount > 0){
								buyValue = subLevelBuyInfo.totalAmount;
								requireValue = subLevel.minAmount;
							}

							if(requireValue > 0) {
								if (n > buyValue / requireValue || n < 0) {
//										if (n > buyValue / requireValue || n < 0 || !hasRequireValue) {
									n = (int) (buyValue / requireValue);
								}
							}

							// CTKM tich luy
							if(orderView.isCalAccuPromotion) {
								orderView.multiple = n;
								n = 1;
								orderView.multipleUsing += 1;
							}

							// Khi ko luy ke
							if (productGroup.multiple == 0) {
								n = 1;
							}


							//Ds san pham cua tung muc
							for (GroupLevelDetailDTO levelDetail : subLevel.levelDetailArray) {
								//Tim xem co mua san pham nhu yeu cau khong?
								Long key = Long.valueOf(levelDetail.productId);
								OrderDetailViewDTO product = sortListProductSale.get(key);

								if (product != null) {
									if(levelDetail.isRequired == 1 && levelDetail.value > 0) {
										long amount = 0;
										if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
											BigDecimal usedQuantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", n * levelDetail.value));

											product.orderDetailDTO.quantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", product.exchangeQuantity()))).subtract(usedQuantity).doubleValue();
											//Cap nhat amount mua
											amount = usedQuantity.multiply(new BigDecimal(product.orderDetailDTO.price)).longValue();
											levelDetail.usedAmount = amount;

											//Dung cho KM tich luy group
											subLevelBuyInfo.usedQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.usedQuantity))).add(usedQuantity).doubleValue();
										} else if (levelDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_AMOUNT) {
											amount = n * levelDetail.value;
											product.orderDetailDTO.subtractAmount(amount);
											levelDetail.usedAmount = amount;

											//Dung cho KM tich luy group
											subLevelBuyInfo.usedAmount += n * levelDetail.value;
										}

										//Tong amount tren muc cua Bundle
										totalAmountFixBundle += amount;

									}
									listBuyProduct.add(levelDetail);
//									continue;
								}
							}

							//Tru tren muc
							if(subLevel.minQuantity > 0) {
								//Dung cho KM tich luy group
//								BigDecimal usedQuantity = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", n * subLevel.minQuantity));
								BigDecimalRound usedQuantity = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", n * subLevel.minQuantity));
								subLevelBuyInfo.usedQuantity = usedQuantity.toDouble();
								subLevelBuyInfo.usedAmount = usedQuantity.multiply(new BigDecimal(subLevelBuyInfo.totalAmount))
//										.divide(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.totalQuantity)), 0, RoundingMode.UP).longValue();
										.divide(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.totalQuantity))).toLong();
//								subLevelBuyInfo.multiple = n;
//								totalQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", totalQuantity))).subtract(usedQuantity).doubleValue();
								totalQuantity = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", totalQuantity)).subtract(usedQuantity.getValue()).toDouble();
//								subLevelBuyInfo.totalQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.totalQuantity))).subtract(usedQuantity).doubleValue();
								subLevelBuyInfo.totalQuantity = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.totalQuantity)).subtract(usedQuantity.getValue()).toDouble();
							} else if (subLevel.minAmount > 0){
								//Dung cho KM tich luy group
//								BigDecimal usedAmount = new BigDecimal(StringUtil.decimalFormatSymbols("#", n * subLevel.minAmount));
								BigDecimalRound usedAmount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", n * subLevel.minAmount));
								subLevelBuyInfo.usedAmount = usedAmount.toLong();
								subLevelBuyInfo.usedQuantity = usedAmount.multiply(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", subLevelBuyInfo.totalQuantity)))
//										.divide(new BigDecimal(subLevelBuyInfo.totalAmount), 2, RoundingMode.UP).doubleValue();
										.divide(new BigDecimal(subLevelBuyInfo.totalAmount)).toDouble();
//								subLevelBuyInfo.multiple = n;
//								totalAmount = new BigDecimal(StringUtil.decimalFormatSymbols("#", totalAmount)).subtract(usedAmount).longValue();
								totalAmount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", totalAmount)).subtract(usedAmount.getValue()).toLong();
//								subLevelBuyInfo.totalAmount = (new BigDecimal(StringUtil.decimalFormatSymbols("#", subLevelBuyInfo.totalAmount))).subtract(usedAmount).longValue();
								subLevelBuyInfo.totalAmount = new BigDecimalRound(StringUtil.decimalFormatSymbols("#", subLevelBuyInfo.totalAmount)).subtract(usedAmount.getValue()).toLong();
							}
						}

						//Lay ra thong tin KM ung voi group va level
						GroupMappingDTO mapping = groupMapping.getGroupMapping(productGroup.productGroupId, level.groupLevelId);
						//Co ton tai 1 mapping
						if(mapping != null && mapping.promoGroupId > 0 && mapping.promoGroupLevelId > 0) {
							hasPromotionGroup = true;
							hasPromotionLevel = true;
							//Co duoc KM tien trong 1 muc cha -> ko duoc sua so suat khi thay doi so luong
							boolean hasMoney = false;
//							ProductGroupDTO promoGroup = productGroupTable.getProductGroupInfo(mapping.promoGroupId);
							GroupLevelDTO promoGroupLevel = groupLevelTable.getGroupLevelInfo2(mapping.promoGroupId, mapping.promoGroupLevelId);

							for(GroupLevelDTO promoSubLevel : promoGroupLevel.subLevelArray) {
								keyList++;
								//KM san pham
								if(promoSubLevel.hasProduct == 1) {
										ArrayList<OrderDetailViewDTO> requiredList = new ArrayList<OrderDetailViewDTO>();
										ArrayList<OrderDetailViewDTO> optionList = new ArrayList<OrderDetailViewDTO>();

									for(GroupLevelDetailDTO promoDetail : promoSubLevel.levelDetailArray) {
										OrderDetailViewDTO detailView = new OrderDetailViewDTO();
										SaleOrderDetailDTO selectedOrderDetail = new SaleOrderDetailDTO();
										detailView.orderDetailDTO = selectedOrderDetail;
										 // mat hang khuyen mai
										//KM san pham cho don hang
										if(level.hasProduct == GroupLevelDTO.PROMOTION_ORDER) {
											detailView.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT;
										}
										selectedOrderDetail.programeTypeCode = promotionProEntity.getTYPE();
										selectedOrderDetail.isFreeItem = 1;
										selectedOrderDetail.programeId = promotionProEntity.getPROMOTION_PROGRAM_ID();
										selectedOrderDetail.programeCode = promotionProEntity.getPROMOTION_PROGRAM_CODE();
										selectedOrderDetail.programeName = promotionProEntity.getPROMOTION_PROGRAM_NAME();
										selectedOrderDetail.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
										selectedOrderDetail.promotionOrderNumber = promoGroupLevel.orderNumber;

										if(promoDetail.valueType == GroupLevelDetailDTO.VALUE_TYPE_QUANTITY) {
											selectedOrderDetail.price = 0;
											selectedOrderDetail.setDiscountAmount(0);
											selectedOrderDetail.quantity = (int)promoDetail.value * n;
											selectedOrderDetail.maxQuantityFree = (int)selectedOrderDetail.quantity;
											selectedOrderDetail.maxQuantityFreeCK = selectedOrderDetail.maxQuantityFree;
											//Dung cho viec kiem tra sua so luong = 0 -> so suat = 0
											selectedOrderDetail.productGroupId = mapping.promoGroupId;
											selectedOrderDetail.groupLevelId = mapping.promoGroupLevelId;

											detailView.isEdited = promotionProEntity.getIsEdited();
											detailView.oldIsEdited = promotionProEntity.getIsEdited();
											detailView.isHasQuantityReceived = promotionProEntity.isHasQuantityReceived();
											detailView.productPromoId = promoDetail.productId;
											detailView.listBuyProduct = listBuyProduct;
											detailView.subLevelId = promoSubLevel.groupLevelId;
											detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
											detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);
										}

										//Sp KM co bat buoc phai tra, sp tuy cho thi cho phep doi hang
										if(promoDetail.isRequired == 1) {
											requiredList.add(detailView);
											// luu thong tin so luong nhan cua mot CTKM
											numReceived += detailView.orderDetailDTO.quantity;
										} else {
											optionList.add(detailView);
										}
									}
									if(optionList.size() > 0) {
										// luu thong tin so luong nhan cua mot CTKM
										numReceived += optionList.get(0).orderDetailDTO.quantity;
										
										//Neu chi co 1 sp tuy chon thi coi nhu duoc huong bat buoc luon
										if(optionList.size() == 1) {
											requiredList.addAll(optionList);
											optionList.clear();
										} else {
											//Tao key list
											for(OrderDetailViewDTO optionProduct : optionList) {
												optionProduct.changeProduct = 1;
												optionProduct.keyList = keyList;
											}

											//Lay 1 sp trong ds tuy chon dua vao ds KM hien thi
											requiredList.add(optionList.get(0));
										}
									}

									//KM san pham
									if(level.hasProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
										listProductPromotionsale.addAll(requiredList);
										sortListOutPut.put(keyList, optionList);
									//KM don hang
									} else {
										OrderDetailViewDTO detailView = null;
										if(requiredList.size() > 0) {
											SaleOrderDetailDTO productSaleDTO;
											detailView = new OrderDetailViewDTO();
											detailView.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ZV21;
											productSaleDTO = new SaleOrderDetailDTO();
											detailView.orderDetailDTO = productSaleDTO;

											productSaleDTO.programeId = promotionProEntity.getPROMOTION_PROGRAM_ID();
											productSaleDTO.programeCode = promotionProEntity.getPROMOTION_PROGRAM_CODE();
											productSaleDTO.programeName = promotionProEntity.getPROMOTION_PROGRAM_NAME();
											productSaleDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
											productSaleDTO.programeTypeCode = ZV21;
											productSaleDTO.isFreeItem = 1; // mat hang khuyen mai
											productSaleDTO.price = 0;
											productSaleDTO.setDiscountAmount(0);// amountPromo = 0;
											productSaleDTO.maxQuantityFree = (int)productSaleDTO.quantity;
											productSaleDTO.maxQuantityFreeCK = productSaleDTO.maxQuantityFree;
											//Dung cho viec kiem tra sua so luong = 0 -> so suat = 0
											productSaleDTO.productGroupId = mapping.promoGroupId;
											productSaleDTO.groupLevelId = mapping.promoGroupLevelId;

											detailView.isEdited = promotionProEntity.getIsEdited();
											detailView.oldIsEdited = promotionProEntity.getIsEdited();
											detailView.isHasQuantityReceived = promotionProEntity.isHasQuantityReceived();
											detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
											detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);
											detailView.listPromotionForPromo21 = requiredList;

//													if (promotionDescription.length() > 0){
//														detailView.promotionDescription = String.valueOf(promotionDescription.toString());
//													}

											listProductPromotionsale.add(detailView);
											sortListOutPut.put(keyList, optionList);
										}
									}

								//KM tien, %
								} else {
									//Co duoc KM tien trong 1 muc
									hasMoney = true;

									OrderDetailViewDTO detailView = new OrderDetailViewDTO();
									SaleOrderDetailDTO selectedOrderDetail = new SaleOrderDetailDTO();
									detailView.orderDetailDTO = selectedOrderDetail;
//										selectedOrderDetail.productId = productSalePro.orderDetailDTO.productId;
									if(level.hasProduct == GroupLevelDTO.PROMOTION_ORDER) {
										detailView.promotionType = OrderDetailViewDTO.PROMOTION_FOR_ORDER;
									}
									selectedOrderDetail.isFreeItem = 1;
									selectedOrderDetail.programeId = promotionProEntity.getPROMOTION_PROGRAM_ID();
									selectedOrderDetail.programeCode = promotionProEntity.getPROMOTION_PROGRAM_CODE();
									selectedOrderDetail.programeName = promotionProEntity.getPROMOTION_PROGRAM_NAME();
									selectedOrderDetail.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
									selectedOrderDetail.promotionOrderNumber = promoGroupLevel.orderNumber;
									selectedOrderDetail.programeTypeCode = promotionProEntity.getTYPE();
									selectedOrderDetail.productId = 0;
									selectedOrderDetail.price = 0;
									//Dung cho viec kiem tra sua so luong = 0 -> so suat = 0
									selectedOrderDetail.productGroupId = mapping.promoGroupId;
									selectedOrderDetail.groupLevelId = mapping.promoGroupLevelId;

									detailView.isEdited = promotionProEntity.getIsEdited();
									detailView.oldIsEdited = promotionProEntity.getIsEdited();
									detailView.isHasQuantityReceived = promotionProEntity.isHasQuantityReceived();
									detailView.productCode = "";
									detailView.productName = "";
									detailView.productPromoId = 0;
									detailView.numBuyProductInGroup = level.subLevelArray.get(0).levelDetailArray.size();
									detailView.listBuyProduct = listBuyProduct;
									detailView.isBundle = isBundle;
									//KM tien
									if(promoSubLevel.maxAmount > 0) {
										selectedOrderDetail.setDiscountAmount(n * promoSubLevel.maxAmount);
										selectedOrderDetail.maxAmountFree = selectedOrderDetail.getDiscountAmount();
										double totalAmountBuy = 0;
										if(isBundle) {
											totalAmountBuy = totalAmountFixBundle;
										} else {
											totalAmountBuy = totalAmountFix;
										}
										
//										if (totalAmountBuy != 0) {										
										if (BigDecimal.valueOf(totalAmountBuy).compareTo(BigDecimal.ZERO) != 0) {
//											BigDecimal discountAmount = new BigDecimal(selectedOrderDetail.getDiscountAmount());
//											selectedOrderDetail.discountPercentage = discountAmount.multiply(new BigDecimal(ROUND_PERCENT)).divide(new BigDecimal(totalAmountBuy), 2, RoundingMode.HALF_UP).doubleValue();
											BigDecimalRound discountAmount = new BigDecimalRound(selectedOrderDetail.getDiscountAmount());
											selectedOrderDetail.discountPercentage = discountAmount.multiply(new BigDecimal(ROUND_PERCENT)).divide(new BigDecimal(totalAmountBuy)).toDouble();
										} else{
											selectedOrderDetail.discountPercentage = 0;
										}
//										selectedOrderDetail.discountPercentage = StringUtil.roundDoubleDown("#.##", Math.round((double)selectedOrderDetail.discountAmount * ROUND_NO_PERCENT / totalAmountBuy) / (double)ROUND_PERCENT);
										selectedOrderDetail.quantity = 0;

										detailView.type = OrderDetailViewDTO.FREE_PRICE;
										detailView.typeName = StringUtil.getString(R.string.TEXT_MONEY_PROMOTION_NAME);
									} else if(promoSubLevel.promotionPercent > 0) {
										//Neu da huong KM % roi thi ko duoc huong nua
										if(hasPercent && !isBundle) {
											continue;
										} else {
											hasPercent = true;
										}
										selectedOrderDetail.discountPercentage = promoSubLevel.promotionPercent;
										double totalDiscount = 0;
//										selectedOrderDetail.discountPercentage = StringUtil.roundDoubleDown("#.##", selectedOrderDetail.discountPercentage);
//										BigDecimal discountPercent = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", selectedOrderDetail.discountPercentage));
										BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", selectedOrderDetail.discountPercentage));
										if(isBundle) {
											for(GroupLevelDetailDTO levelDetail: listBuyProduct) {
												totalDiscount += discountPercent.multiply(new BigDecimal(levelDetail.usedAmount)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
//												totalDiscount += Math.round((levelDetail.usedAmount * Math.round(selectedOrderDetail.discountPercentage * ROUND_PERCENT) / (double)ROUND_NO_PERCENT));
											}
										} else {
											double amountUsed = totalAmountFix;
											if(orderView.isCalAccuPromotion) {
												amountUsed = 0;
												for(GroupLevelDTO subLevel : level.subLevelArray) {
													BuyInfoLevel subLevelBuyInfo = buyInfoLevelArray.get(level.subLevelArray.indexOf(subLevel));
													amountUsed += subLevelBuyInfo.usedAmount;
												}
											}
//											totalDiscount = discountPercent.multiply(new BigDecimal(amountUsed)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).longValue();
											totalDiscount = discountPercent.multiply(new BigDecimal(amountUsed)).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
//											totalDiscount = (long)(amountUsed * Math.round(selectedOrderDetail.discountPercentage * ROUND_PERCENT) / (double)ROUND_NO_PERCENT);
											//KM % cua group hoac don hang thi chi duoc 1 suat
											//Bundle thi duoc nhieu suat
											n = 1;
										}
										selectedOrderDetail.setDiscountAmount(totalDiscount);
										selectedOrderDetail.maxAmountFree = selectedOrderDetail.getDiscountAmount();

										detailView.type = OrderDetailViewDTO.FREE_PERCENT;
										detailView.typeName = "KM %";
									}

//										detailView.listIdProductOfGroup = listIdProductOfGroup;
									// luu thong tin tien nhan cua mot CTKM
									amountReceived = selectedOrderDetail.getDiscountAmount();
									listProductPromotionsale.add(detailView);
								}
							}

							//Tinh so suat
							ArrayList<SaleOrderPromotionDTO> quantityReceivedList;
							//KM sp
							if(level.hasProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
								quantityReceivedList = orderView.productQuantityReceivedList;
							//KM don hang
							} else {
								quantityReceivedList = orderView.orderQuantityReceivedList;
							}
							boolean isExist = false;
							for(SaleOrderPromotionDTO orderPromotion: quantityReceivedList) {
								if(orderPromotion.promotionProgramId == promotionProEntity.getPROMOTION_PROGRAM_ID() &&
										orderPromotion.productGroupId == mapping.promoGroupId &&
										orderPromotion.groupLevelId == mapping.promoGroupLevelId
										) {
									isExist = true;
									orderPromotion.quantityReceived += n;
									orderPromotion.quantityReceivedMax = orderPromotion.quantityReceived;
									break;
								}
							}
							if(!isExist) {
								SaleOrderPromotionDTO orderPromotion = new SaleOrderPromotionDTO();
								orderPromotion.promotionProgramId = promotionProEntity.getPROMOTION_PROGRAM_ID();
								orderPromotion.promotionProgramCode = promotionProEntity.getPROMOTION_PROGRAM_CODE();
								orderPromotion.promotionProgramName = promotionProEntity.getPROMOTION_PROGRAM_NAME();
								orderPromotion.productGroupId = mapping.promoGroupId;
								orderPromotion.groupLevelId = mapping.promoGroupLevelId;
								orderPromotion.promotionLevel = promoGroupLevel.orderNumber;
								orderPromotion.groupOrderNumber = productGroup.orderNumber;
								orderPromotion.hasProduct = promoGroupLevel.hasProduct;
								orderPromotion.hasMoney = hasMoney;
								orderPromotion.quantityReceived = n;
								orderPromotion.quantityReceivedMax = orderPromotion.quantityReceived;
								// luu thong tin so tien/ so luong nhan duoc cua mot CTKM
								orderPromotion.numReceived = numReceived;
								orderPromotion.amountReceived = amountReceived;
								orderPromotion.numReceivedMax = orderPromotion.numReceived;
								orderPromotion.amountReceivedMax = orderPromotion.amountReceived;

								quantityReceivedList.add(orderPromotion);
							}
						}
					}

					//Khong toi uu
					if(productGroup.recursive == 0 && hasPromotionLevel) {
						break;
					}
				}
			}

//			//Neu group co thu tu va duoc huong KM thi dung lai
//			if(productGroup.orderNumber > 0 && hasPromotion) {
//				break;
//			}
			//Luu thong tin cho KM tich luy
			orderView.groupLevel = groupLevel;
			orderView.buyInfoLevelArray = buyInfoLevelArray;
		}

		//Lay ds product id thuoc cac group thuoc cung CT
		if(promotionArray != null) {
			for(int i = 0; i<promotionArray.size(); i++) {
				ArrayList<PromotionProgrameDTO> lstPromotion = promotionArray.valueAt(i);
				for (PromotionProgrameDTO promotion : lstPromotion) {
					if (promotion.getPROMOTION_PROGRAM_ID() == promotionProEntity.getPROMOTION_PROGRAM_ID()) {
						calculatedKeyList.add(promotionArray.keyAt(i));
					}
				}
			}
		}
		// dua ds sp cung thuoc mot CTKM vao list de sau khoi tinh lai khi duyet qua tung sp
		if(hmProgrameProduct != null)
			hmProgrameProduct.put(promotionProEntity.getPROMOTION_PROGRAM_ID(), calculatedKeyList);
		//Loai bo cac sp da tinh KM trong cac nhom
//		for(Long key : calculatedKeyList) {
//			sortListProductSale.remove(key);
//		}

		return sortListOutPut;
	}

	/**
	 * Tao doi tuong KM %
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: OrderDetailViewDTO
	 * @throws:
	 * @param promotionProgram
	 * @param promotionDetail
	 * @param totalDiscountAmount
	 * @return
	 */
	private static OrderDetailViewDTO createPercentPromotionInfo(
			PromotionProgrameDTO promotionProgram,
			PromotionProDetailDTO promotionDetail, int buyProductId, long totalDiscountAmount) {
		OrderDetailViewDTO detailView = new OrderDetailViewDTO();
		SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
		detailView.orderDetailDTO = orderDetailDTO;

		updatePromotionProductCommonInfo(promotionProgram, orderDetailDTO, buyProductId);

		orderDetailDTO.setDiscountAmount(totalDiscountAmount);
		orderDetailDTO.maxAmountFree = orderDetailDTO.getDiscountAmount();
		orderDetailDTO.discountPercentage = promotionDetail.discPer;

		detailView.type = OrderDetailViewDTO.FREE_PERCENT;
		detailView.typeName = "KM %";

		return detailView;
	}

	/**
	 * Tao doi tuong KM tien
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: OrderDetailViewDTO
	 * @throws:
	 * @param promotionProgram
	 * @param productSalePro
	 * @param promotionDetail
	 * @param multiplier
	 * @return
	 */
	private static OrderDetailViewDTO createAmountPromotionInfo(
			PromotionProgrameDTO promotionProgram,
			PromotionProDetailDTO promotionDetail, int buyProductId,
			long totalAmount, long totalDiscountAmount) {
		OrderDetailViewDTO detailView = new OrderDetailViewDTO();
		SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
		detailView.orderDetailDTO = orderDetailDTO;

		updatePromotionProductCommonInfo(promotionProgram, orderDetailDTO, buyProductId);

		// so tien khuyen mai
		orderDetailDTO.setDiscountAmount(totalDiscountAmount);
		orderDetailDTO.maxAmountFree = orderDetailDTO.getDiscountAmount();
		orderDetailDTO.discountPercentage = Math.round((double)orderDetailDTO.getDiscountAmount() * ROUND_NO_PERCENT / totalAmount) / (double)ROUND_PERCENT;

		detailView.type = OrderDetailViewDTO.FREE_PRICE;
		detailView.typeName = StringUtil.getString(R.string.TEXT_MONEY_PROMOTION_NAME);

		return detailView;
	}

	/**
	 * Tao doi tuong KM san pham
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: OrderDetailViewDTO
	 * @throws:
	 * @param promotionProgram
	 * @param promotionDetail
	 * @param multiple
	 * @param j
	 * @return
	 */
	private static OrderDetailViewDTO createProductPromotionInfo(
			PromotionProgrameDTO promotionProgram,
			PromotionProDetailDTO promotionDetail,
			int multiple) {
		OrderDetailViewDTO detailView = new OrderDetailViewDTO();
		SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
		detailView.orderDetailDTO = orderDetailDTO;

		updatePromotionProductCommonInfo(promotionProgram, orderDetailDTO, promotionDetail.productId);

		orderDetailDTO.quantity = promotionDetail.freeQTY * multiple;
		orderDetailDTO.maxQuantityFree = (int)orderDetailDTO.quantity;
		orderDetailDTO.maxQuantityFreeCK = orderDetailDTO.maxQuantityFree;

		detailView.productPromoId = promotionDetail.freeProductId;
		detailView.type = OrderDetailViewDTO.FREE_PRODUCT;
		detailView.typeName = StringUtil.getString(R.string.TEXT_PRODUCT_PROMOTION_NAME);

		return detailView;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param promotionProgram
	 * @param orderDetailDTO
	 * @param buyProductId
	 */
	private static void updatePromotionProductCommonInfo(
			PromotionProgrameDTO promotionProgram,
			SaleOrderDetailDTO orderDetailDTO, int buyProductId) {
		orderDetailDTO.programeCode = promotionProgram.getPROMOTION_PROGRAM_CODE();
		orderDetailDTO.programeName = promotionProgram.getPROMOTION_PROGRAM_NAME();
		orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
		orderDetailDTO.programeTypeCode = promotionProgram.getTYPE();
		orderDetailDTO.productId = buyProductId;
		orderDetailDTO.price = 0;
		orderDetailDTO.packagePrice = 0;
		orderDetailDTO.isFreeItem = 1;
	}

	/**
	 * Tao ds KM tuy chon
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param listProductPromotionsale
	 * @param keyList
	 * @param sortListOutPut
	 * @param requiredList
	 * @param optionList
	 */
	private static void updateOptionPromotionList(
			List<OrderDetailViewDTO> listProductPromotionsale, Long keyList,
			SortedMap<Long, List<OrderDetailViewDTO>> sortListOutPut,
			SortedMap<Long, OrderDetailViewDTO> requiredList,
			SortedMap<Long, OrderDetailViewDTO> optionList) {
		if(optionList.size() > 0) {
			//Neu chi co 1 sp tuy chon thi coi nhu duoc huong bat buoc luon
			if(optionList.size() == 1) {
				requiredList.putAll(optionList);
				optionList.clear();
			} else {
				//Tao key list
				for(OrderDetailViewDTO optionProduct : optionList.values()) {
					optionProduct.changeProduct = 1;
					optionProduct.keyList = keyList;
				}

				//Lay 1 sp trong ds tuy chon dua vao ds KM hien thi
				requiredList.put(optionList.firstKey(), optionList.get(optionList.firstKey()));
			}
		}

		listProductPromotionsale.addAll(requiredList.values());
		List<OrderDetailViewDTO> m = new ArrayList<OrderDetailViewDTO>(optionList.values());
		sortListOutPut.put(keyList, m);
	}

	 /**
	 * Tra ve loai CTKM (0: group, bundle, 1: line, -1 : zv22)
	 * @author: Tuanlt11
	 * @param zv
	 * @return
	 * @return: int
	 * @throws:
	*/
	public static int getTypePromtion(String zv){
		if(ZV22.equals(zv))
			return -1;
		else if(ZV01.equals(zv) || ZV02.equals(zv) || ZV03.equals(zv) || ZV04.equals(zv) || ZV05.equals(zv) || ZV06.equals(zv) ){
			return 1;
		}
		return 0;
	}
}
