package com.ths.dmscore.view.sale.salestatistics;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.SaleStatisticsAccumulateDayDTO.SaleStatisticsAccumulateDayItem;
import com.ths.dms.R;

/**
 *
 * Sale Statistics Accumulate Day Row
 * @author: duongdt3
 * @version: 1.0
 * @since:  11:12:11 3 Apr 2015
 */
public class SaleStatisticsAccumulateDayRow extends DMSTableRow implements
		OnClickListener {
	SaleStatisticsAccumulateDayItem item;

	public TextView tvStt;
	public TextView tvMaHang;
	public TextView tvTenMatHang;
	public TextView tvNH;
	public TextView tvKeHoach;
	public TextView tvThucHien;
	public TextView tvDuyet;
	public TextView tvConLai;
	public TextView tvTienDo;

	public TextView tvKeHoachSanLuong;
	public TextView tvThucHienSanLuong;
	public TextView tvDuyetSanLuong;
	public TextView tvConLaiSanLuong;
	public TextView tvTienDoSanLuong;
	private TextView tvAmountTitle;

	public SaleStatisticsAccumulateDayRow(Context context, boolean isShowAmount) {
		super(context, R.layout.layout_sale_statistics_accumulate_day_row,
				(isShowAmount ? 1.5 : 1.0),
				(isShowAmount ? null : new int[] {R.id.llAmount}));

		tvStt = (TextView) findViewById(R.id.tvStt);
		tvMaHang = (TextView) findViewById(R.id.tvMaHang);
		tvTenMatHang = (TextView) findViewById(R.id.tvTenMatHang);
		tvNH = (TextView) findViewById(R.id.tvNH);

		tvKeHoach = (TextView) findViewById(R.id.tvKeHoach);
		tvThucHien = (TextView) findViewById(R.id.tvThucHien);
		tvDuyet = (TextView) findViewById(R.id.tvDuyet);
		tvConLai = (TextView) findViewById(R.id.tvConLai);
		tvTienDo = (TextView) findViewById(R.id.tvTienDo);

		tvKeHoachSanLuong = (TextView) findViewById(R.id.tvKeHoachSanLuong);
		tvThucHienSanLuong = (TextView) findViewById(R.id.tvThucHienSanLuong);
		tvDuyetSanLuong = (TextView) findViewById(R.id.tvDuyetSanLuong);
		tvConLaiSanLuong = (TextView) findViewById(R.id.tvConLaiSanLuong);
		tvTienDoSanLuong = (TextView) findViewById(R.id.tvTienDoSanLuong);

		tvAmountTitle = (TextView) findViewById(R.id.tvAmountTitle);
		display(tvAmountTitle, StringUtil.getReportUnitTitle(StringUtil.getString(R.string.TEXT_SALES_INFO)));
	}

	public void render(int pos, SaleStatisticsAccumulateDayItem dto) {
		item = dto;
		display(tvStt, pos);
		display(tvMaHang, dto.maHang);
		display(tvTenMatHang, dto.tenMatHang);
		display(tvNH, dto.nh);

		display(tvKeHoach, dto.keHoach);
		display(tvThucHien, dto.thucHien);
		display(tvDuyet, dto.duyet);
		display(tvConLai, dto.conLai);
		displayPercent(tvTienDo, dto.tienDo);

		display(tvKeHoachSanLuong, dto.keHoachSanLuong);
		display(tvThucHienSanLuong, dto.thucHienSanLuong);
		display(tvDuyetSanLuong, dto.duyetSanLuong);
		display(tvConLaiSanLuong, dto.conLaiSanLuong);
		displayPercent(tvTienDoSanLuong, dto.tienDoSanLuong);
	}

	/**
	 * @author: duongdt3
	 * @since: 10:55:04 3 Apr 2015
	 * @return: void
	 * @throws:
	 * @param itemSum
	 */
	public void renderSum(SaleStatisticsAccumulateDayItem itemSum) {
		render(0, itemSum);
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvStt, tvMaHang, tvTenMatHang, tvNH);
		setTypeFace(Typeface.BOLD, tvKeHoach,tvThucHien,tvDuyet,tvConLai,tvTienDo,tvKeHoachSanLuong,tvThucHienSanLuong,tvDuyetSanLuong,tvConLaiSanLuong,tvTienDoSanLuong);
	}
}
