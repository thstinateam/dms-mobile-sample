/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Map.Entry;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.view.main.LoginView;

public class SqlCipherUtil {

	public static final int NOT_EXIST_DB = 0;
	public static final int EXIST_DB_NOT_ENCRYPT = 1;
	public static final int EXIST_DB_ENCRYPT = 2;
	/**
	 * Ma hoa File DB
	 *
	 * @author: ThangNV31
	 * @return: void
	 * @throws:
	 * @param fileName
	 */
	public static void encryptPlaintextDB(String fileName, Context context) throws Exception {
		File databaseFile = new File(fileName);
		if (databaseFile.exists()) {
			SQLiteDatabase mDB;
			mDB = SQLiteDatabase.openDatabase(fileName, "", null, SQLiteDatabase.OPEN_READWRITE);
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID, "");
			String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID, "");
			if (mDB == null) {
				throw new Exception("Cannot open database: " + fileName);
			}
			File encryptedDB = new File(ExternalStorage.getFileDBPath(context).getPath() + "/" + roleId+ "_" + shopId + "_" + Constants.DATABASE_NAME);
			if (encryptedDB.exists()) {
				encryptedDB.delete();
			}
			String tempDB = ExternalStorage.getFileDBPath(context).getPath() + "/" + roleId+ "_" + shopId + "_" +System.currentTimeMillis();
			String realDB = ExternalStorage.getFileDBPath(context).getPath() + "/" + roleId+ "_" + shopId + "_" + Constants.DATABASE_NAME;
            File tempEncryptedDB = new File(tempDB);
            File realEncryptedDB = new File(realDB);
            if (realEncryptedDB.exists()) {
                  realEncryptedDB.delete();
            }

			mDB.rawExecSQL(String.format("ATTACH DATABASE '%s' AS encrypted KEY '%s'", tempDB,
					Constants.CIPHER_KEY));
			mDB.rawExecSQL("PRAGMA encrypted.cipher = 'rc4'");
			mDB.rawExecSQL("SELECT sqlcipher_export('encrypted')");
			mDB.rawExecSQL("DETACH DATABASE encrypted");
			mDB.close();
			databaseFile.delete();
            //doi file tam ma hoa thanh file that
            boolean isRenameSusscess = tempEncryptedDB.renameTo(realEncryptedDB);
            if (!isRenameSusscess) {
                throw new Exception("Encrypted DB fail");
          }

		} else {
			throw new FileNotFoundException(fileName + " does not exists");
		}
	}

	/**
	 * bat buoc dung SecurePreference
	 *
	 * @author: ThangNV31
	 * @return: void
	 * @throws:
	 * @param context
	 */
	public static void ensureSecurePreference(Context context) {
		SharedPreferences sharedpreferences = context.getSharedPreferences(LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
		if (sharedpreferences.contains(LoginView.VNM_INHERIT_ID)) {
			Map<String, ?> map = sharedpreferences.getAll();
			sharedpreferences.edit().clear().commit();
			SharedPreferences securePreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			Editor editor = securePreferences.edit();
			for (Entry<String, ?> entry : map.entrySet()) {
				editor.putString(entry.getKey(), "" + entry.getValue());
			}
			editor.commit();
			return;
		}
		GlobalInfo.getInstance().getDmsPrivateSharePreference();
	}

	/**
	 * Giai ma File DB
	 * @author: ThangNV31
	 * @return: void
	 * @throws:
	 * @param fileName
	 */
	public static void decryptCiphertextDB(String fileName, String deCryptFile)
			throws Exception {
		File databaseFile = new File(fileName);
		if (databaseFile.exists()) {
			SQLiteDatabase mDB;
			SQLiteDatabaseHook hook = new SQLiteDatabaseHook() {
				@Override
				public void preKey(SQLiteDatabase db) {
					db.rawExecSQL(String.format("PRAGMA key = '%s'",
							Constants.CIPHER_KEY));
					db.rawExecSQL("PRAGMA cipher = 'rc4'");
				}

				@Override
				public void postKey(SQLiteDatabase db) {
					db.rawExecSQL(String.format("PRAGMA key = '%s'",
							Constants.CIPHER_KEY));
					db.rawExecSQL("PRAGMA cipher = 'rc4'");
				}
			};

			mDB = SQLiteDatabase.openOrCreateDatabase(databaseFile,
					Constants.CIPHER_KEY, null, hook);

			if (mDB == null) {
				throw new Exception("Cannot open database: " + fileName);
			}
			File decryptDB = new File(deCryptFile);
			if (decryptDB.exists()) {
				decryptDB.delete();
			}
			mDB.rawExecSQL(String.format("PRAGMA key = '%s'",
					Constants.CIPHER_KEY));
			mDB.rawExecSQL(String.format("PRAGMA cipher = '%s'", "rc4"));

			mDB.rawExecSQL(String.format("ATTACH DATABASE '%s' AS plaintext KEY ''", deCryptFile));
			mDB.rawExecSQL("SELECT sqlcipher_export('plaintext')");
			mDB.rawExecSQL("DETACH DATABASE plaintext");
			mDB.close();
		} else {
			throw new FileNotFoundException(fileName + " does not exists");
		}
	}
}
