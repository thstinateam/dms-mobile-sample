package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Mo ta muc dich cua class
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class GsnppLessThan2MinsDTO {
	public boolean isLessThan2Min;
	public ArrayList<LessThan2MinsItem> arrList;

	public GsnppLessThan2MinsDTO() {
		arrList = new ArrayList<GsnppLessThan2MinsDTO.LessThan2MinsItem>();
	}

	public LessThan2MinsItem newLessThan2MinsItem() {
		return new LessThan2MinsItem();
	}

	public class LessThan2MinsItem {
		public String customerName;
		public String customerId;
		public String customerAddress;
		public String startTime;
		public String endTime;
		public double sales;

		public LessThan2MinsItem() {

		}

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param cursor
		 * @return: voidvoid
		 * @throws:
		 */
		public void initItem(Cursor c) {
			if (c == null) {
				return;
			}
			customerName = CursorUtil.getString(c, "CUS_CODE_NAME");
			customerId = CursorUtil.getString(c, "CUSTOMER_ID");
			customerAddress = CursorUtil.getString(c, "STREET");
			startTime = CursorUtil.getString(c, "START_TIME");
			endTime = CursorUtil.getString(c, "END_TIME");
			sales = CursorUtil.getDouble(c, "REVENUE");
		}
	}

}
