package com.ths.dmscore.view.supervisor.customer;

import java.util.Calendar;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO.StaffItemDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Man hinh danh sach khach hang role GSNPP
 * CustomerSaleList.java
 * @author: hoanpd1
 * @version: 1.0
 * @since:  10:25:48 05-03-2015
 */
public class CustomerSaleList extends BaseFragment implements OnItemSelectedListener, VinamilkTableListener,
		OnClickListener, DMSColSortManager.OnSortChange {
	private static final int ACTION_ALLOW_ORDER_OK = 1;
	private static final int ACTION_ALLOW_ORDER_CANCEL = 2;

	//action menu
	private static final int MENU_IMAGE = 17;
	private static final int MENU_SEARCH_IMAGE = 18;
	private static final int MENU_CUSTOMER = 15;
	// key chuyen man hinh
	public static final int KEY = 2;
	// danh sach nhan vien ban hang
//	private ListStaffDTO dtoListStaff;
	// danh sach khach hang
	public CustomerListDTO cusDto = new CustomerListDTO();
	// private CustomerDTO customer;
	// list ds nhan vien o spiner
	private String[] arrMaNPP;
	// arr sprinner tuyen
	private String[] arrLineChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_MONDAY),
			StringUtil.getString(R.string.TEXT_TUESDAY),
			StringUtil.getString(R.string.TEXT_WEDNESDAY),
			StringUtil.getString(R.string.TEXT_THURSDAY),
			StringUtil.getString(R.string.TEXT_FRIDAY),
			StringUtil.getString(R.string.TEXT_SATURDAY),
			StringUtil.getString(R.string.TEXT_SUNDAY) };
	// vao man hinh lan dau tien hay kg?
	private boolean isFirst = true;
	// back ve tu ban do
	private boolean isBack = false;
	// tuyen dang chon
	int currentTuyen = -1;
	// KH dang chon
	int currentNVBH = -1;
	// trang hien tai
	private int currentPage = -1;

	// string search
//	private String textCusCode = "";
	private String textCustomer = "";
	private long staffId;
	private String textShopId = "";

	// spiner list nhan vien ban hang
	Spinner spNVBH;
	// spiner tuyen trong tuan
	Spinner spTuyen;
	// ma nhan vien
//	VNMEditTextClearable edCusCode;
	// ten va dia chi nhan vien
	VNMEditTextClearable edCustomer;
	// nhap lai
	Button btReText;
	// tim kiem
	Button btSearch;
	// tbCusList
	private DMSTableView tbCusList;
	// action log ghi lai khi tao exception order dayInOrder
	ActionLogDTO action;
	// ngay hien tai
	Calendar calendar = Calendar.getInstance();
	//String ngay hien tai
	String sToday = DateUtils.defaultDateFormat.format(calendar.getTime());
	//don vi
	private Spinner spShop;
	private int indexSPShop = -1;
	int allowDistanceOrder = 0;

	ListComboboxShopStaffDTO combobox;

	public static CustomerSaleList getInstance(Bundle data) {
		CustomerSaleList f = new CustomerSaleList();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		allowDistanceOrder = GlobalInfo.getInstance().getAllowDistanceOrder();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_customer_sale_list_fragment, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_GSNPP_CUSTOMER_SALE_LIST));
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_CUSTOMER_LIST);
		initView(v);
		initHeaderTable();

		if (isFirst && combobox == null) {
			textShopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			getListCombobox();
		} else {
			initSpNPP();
			initSpNVBH();
			renderLayout();
		}
		return v;
	}

	/**
	 *
	 * Khoi tao menu cho man hinh
	 *
	 * @author: HoanPD1
	 * @return: void
	 * @throws:
	 * @date: Jan 8, 2013
	 */
	private void initMenuActionBar() {
		addMenuItem(
				PriForm.GSNPP_DSKHACHHANG,
				new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
						MENU_IMAGE, PriForm.GSNPP_DSHINHANH),
				new MenuTab(R.string.TEXT_CUSTOMER, R.drawable.menu_customer_icon,
						MENU_CUSTOMER, PriForm.GSNPP_DSKHACHHANG, View.INVISIBLE));
	}

	/**
	 * Init view control layout
	 *
	 * @author banghn
	 * @param v
	 */
	private void initView(View v) {
		PriUtils.getInstance().genPriHashMapForForm(PriForm.GSNPP_DSKHACHHANG);
		spNVBH = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spNVBH, PriControl.GSNPP_DANHSACHDIEMBAN_NHANVIEN);
		PriUtils.getInstance().findViewByIdGone(v,
				R.id.tvNVBH, PriControl.GSNPP_DANHSACHDIEMBAN_NHANVIEN);
		PriUtils.getInstance().setOnItemSelectedListener(spNVBH, this);
		spTuyen = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spTuyen, PriControl.GSNPP_DANHSACHDIEMBAN_TUYEN);
		PriUtils.getInstance().findViewByIdGone(v,
				R.id.tvTuyen, PriControl.GSNPP_DANHSACHDIEMBAN_TUYEN);
		PriUtils.getInstance().setOnItemSelectedListener(spTuyen, this);

		PriUtils.getInstance().findViewByIdGone(v, R.id.tvMaKH,
				PriControl.GSNPP_DANHSACHDIEMBAN_MAKH);
//		edCusCode = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edCusCode,
//				PriControl.GSNPP_DANHSACHDIEMBAN_MAKH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvCusNameAddress,
				PriControl.GSNPP_DANHSACHDIEMBAN_TENKH);
		edCustomer = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edCustomer,
				PriControl.GSNPP_DANHSACHDIEMBAN_TENKH);
		btReText = (Button) v.findViewById(R.id.btReText);
		btReText.setOnClickListener(this);
		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btSearch, PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM);
		PriUtils.getInstance().setOnClickListener(btSearch, this);
		tbCusList = (DMSTableView) v.findViewById(R.id.tbCusList);
		tbCusList.setListener(this);

		SpinnerAdapter adapterLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrLineChoose);
		spTuyen.setAdapter(adapterLine);
		if (isFirst && combobox == null) {
			currentTuyen = DateUtils.getCurrentDay() + 1;// them tat ca o dau
		}
		spTuyen.setSelection(currentTuyen);
		PriUtils.getInstance().findViewByIdGone(v, R.id.lnSearch, PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM);
		spShop = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spShop, PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM_DONVI);
		PriUtils.getInstance().setOnItemSelectedListener(spShop, this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvShop, PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM_DONVI);
	}

	/**
	 * request lay danh sach nhan vien ban hang trong shop
	 * @author: hoanpd1
	 * @since: 10:21:38 05-03-2015
	 * @return: void
	 * @throws:
	 */
	private void getListCombobox() {
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_DATA, combobox);
		b.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		b.putString(IntentConstants.INTENT_STAFF_OWNER_ID, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		b.putBoolean(IntentConstants.INTENT_ORDER, true);
		b.putBoolean(IntentConstants.INTENT_IS_ALL, true);
		b.putBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST, isFirst);
		b.putBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID, false);
		handleViewEvent(b, ActionEventConstant.GET_LIST_COMBOBOX_SHOP_STAFF, SupervisorController.getInstance());
	}

	/**
	 * Lay danh sach diem ban
	 * @author: hoanpd1
	 * @since: 10:21:49 05-03-2015
	 * @return: void
	 * @throws:
	 * @param page
	 */
	private void getCustomerSaleList(int page) {
		parent.showLoadingDialog();
//		tbCusList.clearAllData();
		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		bundle.putString(IntentConstants.INTENT_VISIT_PLAN, DateUtils.getVisitPlan(arrLineChoose[currentTuyen]));
		// check data search
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, "");
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				StringUtil.getEngStringFromUnicodeString(textCustomer));
		bundle.putLong(IntentConstants.INTENT_STAFF_ID, staffId);
		bundle.putString(IntentConstants.INTENT_USER_ID, "" + GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		bundle.putString(IntentConstants.INTENT_LIST_STAFF, "" + combobox.listStaffId);
		bundle.putSerializable(IntentConstants.INTENT_SORT_DATA, tbCusList.getSortInfo());

		handleViewEvent(bundle, ActionEventConstant.ACTION_GET_CUSTOMER_SALE_LIST, SaleController.getInstance());
	}

	/**
	 * khoi tao spinner Don vi
	 * @author: hoanpd1
	 * @since: 19:38:12 23-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNPP() {
		if(combobox.listShop != null && combobox.listShop.size() > 0){
			String [] arrMaNPP = new String[combobox.listShop.size()];
			int i = 0;
			for (ShopDTO shop : combobox.listShop) {
				if (!StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopCode + " - " + shop.shopName;
				} else if (StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopName;
				} else if (!StringUtil.isNullOrEmpty(shop.shopCode) && StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopCode;
				}else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spShop.setAdapter(adapterNPP);
			if(indexSPShop < 0){
				spShop.setSelection(0);
				indexSPShop = 0;
			}else{
				spShop.setSelection(indexSPShop);
			}
			textShopId = combobox.listShop.get(0).shopId+"";
		} else {
			// truong hop ko co ds nv ban hang
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spShop.setAdapter(adapterNPP);
			cusDto.setTotalCustomer(0);
			cusDto.getCusList().clear();
			renderLayout();
		}
	}

	/**
	 * khoi tao sprinner danh sach NVB
	 * @author: hoanpd1
	 * @since: 11:41:36 26-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNVBH() {
		if (combobox.listStaff != null && combobox.listStaff.size() > 0) {
			int size = combobox.listStaff.size();
			// cong size them 1 do add them tat ca vap mang
			int i = 0;
			StaffItemDTO item1 = new ListComboboxShopStaffDTO().newStaffItem();
			if(!combobox.listStaff.get(0).staffName.equalsIgnoreCase(StringUtil.getString(R.string.TEXT_ALL))){
				arrMaNPP = new String[size + 1];
				item1.addAll(0, StringUtil.getString(R.string.TEXT_ALL), "");
				combobox.listStaff.add(0, item1);
			}
			for (StaffItemDTO item : combobox.listStaff) {
				if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode + " - " + item.staffName;
				} else if (StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffName;
				} else if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode;
				} else {
					arrMaNPP[i] = "";
				}
				i++;

			}
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrMaNPP);
			spNVBH.setAdapter(adapterNPP);
			if(currentNVBH < 0){
				spNVBH.setSelection(0);
			}else
				spNVBH.setSelection(currentNVBH);
		}else {
			// truong hop ko co ds nv ban hang
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
								R.layout.simple_spinner_item, new String[0]);
			spNVBH.setAdapter(adapterNPP);
			cusDto.setTotalCustomer(0);
			cusDto.getCusList().clear();
			renderLayout();
		}
	}

	/**
	 * renderLayout
	 * @author: hoanpd1
	 * @since: 10:22:28 05-03-2015
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		if (currentPage != -1) {
			if (isBack) {
				isBack = false;
				tbCusList.setTotalSize(cusDto.getTotalCustomer(), currentPage);
			}
		} else {
			tbCusList.setTotalSize(cusDto.getTotalCustomer(), 1);
			currentPage = tbCusList.getPagingControl().getCurrentPage();
		}
		if(allowDistanceOrder != GlobalInfo.getInstance().getAllowDistanceOrder()){
			allowDistanceOrder = GlobalInfo.getInstance().getAllowDistanceOrder();
			tbCusList.clearAllDataAndHeader();
			initHeaderTable();
		}else{
			tbCusList.clearAllData();
		}

		int pos = 1 + Constants.NUM_ITEM_PER_PAGE * (tbCusList.getPagingControl().getCurrentPage() - 1);
		if (cusDto.getCusList() != null && cusDto.getCusList().size() > 0) {
			int size = cusDto.getCusList().size();
			for (int i = 0; i < size; i++) {
				CustomerSaleListRow row = new CustomerSaleListRow(parent, this);
				PriUtils.getInstance().setOnClickListener(row.ivMap, this);
				PriUtils.getInstance().setOnClickListener(row.tvNumUpdate, this);
				PriUtils.getInstance().setOnClickListener(row.tvCustomer, this);
				row.ivMap.setTag(i);
				if(row.ivAllowOrder!= null){
					PriUtils.getInstance().setOnClickListener(row.ivAllowOrder, this);
					row.ivAllowOrder.setTag(i);
				}
				row.tvNumUpdate.setTag(i);
				row.tvCustomer.setTag(i);
				row.render(pos, cusDto.getCusList().get(i));
				pos++;
				tbCusList.addRow(row);
			}
		} else {
			tbCusList.showNoContentRow();
		}

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_COMBOBOX_SHOP_STAFF:
			combobox = (ListComboboxShopStaffDTO) modelEvent.getModelData();
			if (combobox != null) {
				updateData();
			}else {
				// truong hop ko co ds nv ban hang
				cusDto.setTotalCustomer(0);
				cusDto.getCusList().clear();
				renderLayout();
			}
			break;
		case ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE:
			getCustomerSaleList(currentPage);
			updateActionLogCreateExceptionDate(e);
			break;
		case ActionEventConstant.ACTION_GET_CUSTOMER_SALE_LIST:
			CustomerListDTO tempDto = (CustomerListDTO) modelEvent.getModelData();
			cusDto.setCusList(tempDto.getCusList());
			if(isFirst){
				isFirst = false;
				currentPage = -1;
				cusDto.setTotalCustomer(tempDto.getTotalCustomer());
			}
//			if (cusDto == null) {
//				// truong hop ko co ds nv ban hang
//				cusDto.totalCustomer = 0;
//				cusDto.cusList.clear();
//			}
			renderLayout();

			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE:
			parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Cap nhat du lieu sp man hinh
	 *
	 * @author: ThanhNN8
	 * @param modelData
	 * @return: void
	 * @throws:
	 */
	private void updateData() {
		if(isFirst){
			initSpNPP();
		}
		initSpNVBH();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSearch:
			if (combobox != null && combobox.listShop != null
					&& combobox.listStaff != null
					&& combobox.listShop.size() > 0
					&& combobox.listStaff.size() > 0) {
				isFirst = true;
				currentPage = -1;
				// textCusCode = edCusCode.getText().toString().trim();
				textCustomer = edCustomer.getText().toString().trim();
				currentTuyen = spTuyen.getSelectedItemPosition();
				staffId = combobox.listStaff.get(spNVBH.getSelectedItemPosition()).staffId;
				textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
				// hide ban phim
				GlobalUtil.forceHideKeyboard(parent);
				getCustomerSaleList(1);
			}
			break;
		case R.id.btReText:
//			edCusCode.setText("");
			edCustomer.setText("");
			spNVBH.setSelection(0);
			currentTuyen = DateUtils.getCurrentDay() + 1;// them tat ca o dau
			spTuyen.setSelection(currentTuyen);
			break;
		case R.id.ivMap:
			int index = (Integer) v.getTag();
			if (cusDto != null && cusDto.getCusList().get(index).aCustomer != null) {
				isBack = true;
				gotoCustomerSaleLocationMap(cusDto.getCusList().get(index).aCustomer, true);
			}
			break;
		case R.id.ivAllowOrder:
			if (GlobalUtil.checkNetworkAccess()) {
				index = (Integer) v.getTag();
				if (cusDto != null && cusDto.getCusList().get(index).aCustomer != null) {
					showConfirmedDialogAllowOrder(cusDto.getCusList().get(index));
				}
			} else {
				parent.showDialog(StringUtil.getString(R.string.TEXT_NETWORK_DISABLE));
			}
			break;
		case R.id.tvNumUpdate:
			index = (Integer) v.getTag();
			if (cusDto != null && cusDto.getCusList().get(index).aCustomer != null
					&& cusDto.getCusList().get(index).numUpdatePosition > 0) {
				isBack = true;
				gotoCustomerSaleLocationMap(cusDto.getCusList().get(index).aCustomer, false);
			}
			break;
		case R.id.tvCustomer:
			index = (Integer) v.getTag();
			if (cusDto != null && cusDto.getCusList().get(index).aCustomer != null) {
				isBack = true;
				currentPage = tbCusList.getPagingControl().getCurrentPage();
				gotoCustomerInfo(cusDto.getCusList().get(index).aCustomer.getCustomerId());
			}
			break;
		default:
			super.onClick(v);
			break;
		}
	}

	public void gotoCustomerInfo(String customerId) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleSwitchFragment(bunde, ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				parent.showLoadingDialog();
				currentPage = -1;
				isFirst = true;
				indexSPShop = -1;
				currentTuyen = DateUtils.getCurrentDay() + 1;// them tat ca o dau
				currentNVBH = -1;
				textShopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
				tbCusList.resetSortInfo();
				getListCombobox();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/**
	 * Show dialog confimed cho phep dat hang tu xa doi voi nhan vien
	 *
	 * @author BangHN
	 * @param dto
	 */
	private void showConfirmedDialogAllowOrder(CustomerListItem dto) {
		SpannableObject textConfirmed = new SpannableObject();
		String titleConfirm;
		// CHO PHEP DAT HANG TU XA
		if (dto.exceptionOrderDate == null || !sToday.equals(dto.exceptionOrderDate)) {
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_DHTX_1),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			textConfirmed.addSpan(dto.staffSale.staffCode + "-" + dto.staffSale.name,
					ImageUtil.getColor(R.color.COLOR_USER_NAME), android.graphics.Typeface.BOLD);
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_DHTX_2),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			textConfirmed.addSpan(dto.aCustomer.customerCode + "-" + dto.aCustomer.customerName,
					ImageUtil.getColor(R.color.COLOR_USER_NAME), android.graphics.Typeface.BOLD);
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_DHTX_3),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			titleConfirm = StringUtil.getString(R.string.TEXT_CONFIRM_DHTX_0);
		} else {// HUY CHO PHEP DAT HANG TU XA
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_CANCEL_DHTX_1),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			textConfirmed.addSpan(dto.staffSale.staffCode + "-" + dto.staffSale.name,
					ImageUtil.getColor(R.color.COLOR_USER_NAME), android.graphics.Typeface.BOLD);
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_CANCEL_DHTX_2),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			textConfirmed.addSpan(dto.aCustomer.customerCode + "-" + dto.aCustomer.customerName,
					ImageUtil.getColor(R.color.COLOR_USER_NAME), android.graphics.Typeface.BOLD);
			textConfirmed.addSpan(StringUtil.getString(R.string.TEXT_CONFIRM_CANCEL_DHTX_3),
					ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			titleConfirm = StringUtil.getString(R.string.TEXT_CONFIRM_CANCEL_DHTX_0);
		}

		GlobalUtil.showDialogConfirm(this, titleConfirm, textConfirmed.getSpan(),
				StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_ALLOW_ORDER_OK,
				StringUtil.getString(R.string.TEXT_BUTTON_DENY), ACTION_ALLOW_ORDER_CANCEL, dto);
	}

	/**
	 * request update ExceptionOrderDate khi thuc hien cho phep dat hang ngoai
	 * le
	 *
	 * @author banghn
	 */
	private void updateExceptionOrderDate(CustomerListItem dto) {
		if (dto != null && dto.staffSale != null && dto.aCustomer != null) {
			Bundle bundle = new Bundle();
			parent.showLoadingDialog();
			// du lieu insert bang position log
			StaffCustomerDTO staffCustomerDTO = new StaffCustomerDTO();
			staffCustomerDTO.staffId = dto.staffSale.staffId;
			staffCustomerDTO.shopId = dto.staffSale.shopId + "";
			staffCustomerDTO.customerId = dto.aCustomer.customerId;
			staffCustomerDTO.staffCustomerId = dto.staffCustomerId;
			if (dto.exceptionOrderDate == null || !sToday.equals(dto.exceptionOrderDate)) {
				staffCustomerDTO.exceptionOrderDate = DateUtils.now();
			} else {
				staffCustomerDTO.exceptionOrderDate = null;
			}
			bundle.putSerializable(IntentConstants.INTENT_STAFF_DTO, staffCustomerDTO);
			handleViewEvent(bundle, ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE, SupervisorController.getInstance());
			//cap nhat thong tin cua action_log
			action = new ActionLogDTO();
			action.objectId = "" + dto.staffCustomerId;
			action.aCustomer = dto.aCustomer;
			action.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			action.objectType = ActionLogDTO.TYPE_EXCEPTION_ORDER;
			action.startTime = DateUtils.now();
			action.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		}
	}

	/**
	 * tao mot record luu action log sau khi tao exception order dayInOrder thanh cong
	 *
	 * @author banghn
	 * @param e
	 */
	private void updateActionLogCreateExceptionDate(ActionEvent e) {
		if (action != null && e != null) {
			Bundle bundle = (Bundle) e.viewData;
			StaffCustomerDTO staffCusDto = (StaffCustomerDTO) bundle.getSerializable(IntentConstants.INTENT_STAFF_DTO);
			if (staffCusDto.exceptionOrderDate != null) {
				if (StringUtil.isNullOrEmpty(action.objectId) || action.objectId.equals("0")) {
					action.objectId = "" + staffCusDto.staffCustomerId;
				}
				action.endTime = DateUtils.now();
				action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
				action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
				parent.requestInsertActionLog(action);
			} else {
				parent.requestDeleteActionLog(action);
			}
		}
	}

	/**
	 * di toi man hinh ban do lich su cap nhat vi tri cua diem ban
	 *
	 * @author banghn
	 */
	private void gotoCustomerSaleLocationMap(CustomerDTO cusDTO, boolean isUpdate) {
		if (cusDTO == null) {
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, cusDTO);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		bundle.putBoolean(IntentConstants.INTENT_UPDATE_POSITION_CUSTOMER, isUpdate);
		handleSwitchFragment(bundle, ActionEventConstant.GOTO_CUSTOMER_SALE_LOCATION_RESET, SupervisorController.getInstance());
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_ALLOW_ORDER_OK:
			updateExceptionOrderDate((CustomerListItem) data);
			break;
		case MENU_IMAGE:
			gotoImageList();
			break;
		case MENU_SEARCH_IMAGE:
			gotoSearchImage();
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 *
	 * Sang man hinh tim kiem hinh anh
	 *
	 * @author: HoanPD1
	 * @return: void
	 * @throws:
	 * @since : 20-09-2013
	 */
	private void gotoSearchImage() {
		Bundle bundle = new Bundle();
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_SEARCH_IMAGE, SupervisorController.getInstance());
	}

	private void gotoImageList() {
		Bundle bundle = new Bundle();
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_GSNPP_IMAGE_LIST, SupervisorController.getInstance());
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (arg0 == spShop) {
			if (combobox != null && indexSPShop != spShop.getSelectedItemPosition()) {
				indexSPShop = spShop.getSelectedItemPosition();
				textCustomer = "";
				if(!(isFirst && combobox !=null)){
					currentTuyen = DateUtils.getCurrentDay() + 1;// them tat ca o dau
				}
				currentPage = -1;
				currentNVBH = -1;
				textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
				getListCombobox();
			}
		} else if (arg0 == spNVBH) {
			if (combobox != null && currentNVBH != spNVBH.getSelectedItemPosition() && !combobox.listStaff.isEmpty()) {
				// edCusCode.setText("");
				edCustomer.setText("");
				// textCusCode = edCusCode.getText().toString().trim();
				textCustomer = edCustomer.getText().toString().trim();
//				if(!(isFirst && combobox !=null)){
//					currentTuyen = DateUtils.getCurrentDay() + 1;// them tat ca o dau
//				}
				spTuyen.setSelection(currentTuyen);
				currentNVBH = spNVBH.getSelectedItemPosition();
				currentPage = -1;
				staffId = combobox.listStaff.get(spNVBH.getSelectedItemPosition()).staffId;
//				textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
				isFirst = true;
				getCustomerSaleList(1);

			}
		} else if (arg0 == spTuyen) {
			if (combobox != null && currentTuyen != spTuyen.getSelectedItemPosition()) {
				// textCusCode = edCusCode.getText().toString().trim();
				textCustomer = edCustomer.getText().toString().trim();
				// currentNVBH = spNVBH.getSelectedItemPosition();
				currentTuyen = spTuyen.getSelectedItemPosition();
				currentPage = -1;
				staffId = combobox.listStaff.get(spNVBH.getSelectedItemPosition()).staffId;
//				textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
				isFirst = true;
				getCustomerSaleList(1);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbCusList) {
			isFirst = false;
			currentPage = tbCusList.getPagingControl().getCurrentPage();
			getCustomerSaleList(currentPage);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
		 switch (action) {
		case ActionEventConstant.GO_TO_CUSTOMER_INFO:
			isBack = true;
			CustomerListItem item = (CustomerListItem) data;
			gotoCustomerInfo(item);
			break;

		default:
			break;
		}
	}

	/**
	 * di toi man hinh thong tin chi tiet khach hang
	 *
	 * @author: dungdq3
	 * @since: 2:23:16 PM Mar 12, 2015
	 * @return: void
	 * @throws:
	 * @param item
	 */
	private void gotoCustomerInfo(CustomerListItem item) {
		String customerId = String.valueOf(item.aCustomer.customerId);
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bunde.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, true);
		handleSwitchFragment(bunde, ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		initMenuActionBar();
	}

	 /**
	 * Khoi tao header cho table
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void initHeaderTable(){
		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
				.build();
		// khoi tao header cho table
		initHeaderTable(tbCusList, new CustomerSaleListRow(parent, this), lstSort, this);
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getCustomerSaleList(currentPage);
	}
}
