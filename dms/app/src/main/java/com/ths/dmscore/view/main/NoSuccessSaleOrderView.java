/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dms.R;

/**
 *  Popup canh bao don hang loi
 *  @author: HieuNH
 *  @version: 1.0
 *  @since: 1.0
 */
public class NoSuccessSaleOrderView extends ScrollView implements
		OnClickListener {

	private GlobalBaseActivity parent;
	public View viewLayout;
	private DMSTableView tbList;
	private TextView tvTitle;
	private Button btClose;
	private NoSuccessSaleOrderDto dto;

	public NoSuccessSaleOrderView(Context context, NoSuccessSaleOrderDto dto) {
		super(context);
		parent = (GlobalBaseActivity) context;
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_no_success_order_list, null);
		tbList = (DMSTableView) viewLayout.findViewById(R.id.tbCusFeedBack);
		btClose = (Button) viewLayout.findViewById(R.id.btClose);
		tvTitle = (TextView) viewLayout.findViewById(R.id.tvTitle);
		btClose.setOnClickListener(this);
		this.dto = dto;
		renderLayout(dto);
	}

	private void renderLayout(NoSuccessSaleOrderDto dto2) {
		tvTitle.setText(StringUtil.getString(R.string.TEXT_WARNING));
		tbList.clearAllDataAndHeader();
		boolean isSysShowPrice = GlobalInfo.getInstance().isSysShowPrice();
		tbList.addHeader(new NoSuccessSaleOrderRow(parent, isSysShowPrice));

		int pos = 1;
		if (dto.itemList.size() > 0) {
			for (int i = 0, s = dto.itemList.size(); i < s; i++) {
				NoSuccessSaleOrderRow row = new NoSuccessSaleOrderRow(parent, isSysShowPrice);
				row.renderLayout(pos, dto.itemList.get(i));
				pos++;
				tbList.addRow(row);
			}
		}

	}

	@Override
	public void onClick(View v) {
		if (v == btClose) {
			parent.onClick(v);
		}
	}

}
