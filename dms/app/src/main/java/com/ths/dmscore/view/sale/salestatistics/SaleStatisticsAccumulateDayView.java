/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.view.SaleStatisticsAccumulateDayDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * man hinh thong ke don tong luy ke ngay
 *
 * @author: HieuNH
 * @version: 1.0
 * @since: 1.1
 */
public class SaleStatisticsAccumulateDayView extends BaseFragment implements
		OnItemSelectedListener, VinamilkTableListener, DMSColSortManager.OnSortChange {
	// show report sale statistics in fullDate
	private static final int ACTION_MENU_SALE_STATISTICS_IN_DAY_PRE = 2;
	private static final int ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL = 3;
	// show report sale statistics in month
	private static final int ACTION_MENU_SALE_STATISTICS_IN_MONTH = 1;
	// list product industry
	Spinner spIndustryProduct;
	// edit text product code
	VNMEditTextClearable etInputProductCode;
	// edit text product name
	VNMEditTextClearable etInputProductName;
	// image button search
	Button ibtSearch;
	// table list product
	DMSTableView tbProductList;
	// sold data
	SaleStatisticsAccumulateDayDTO saleData = new SaleStatisticsAccumulateDayDTO();
	private int currentSelected;
	private boolean isCount = false;// co goi request count product hay kg
	private boolean isShowPrice;

	/**
	 *
	 * get instance for object
	 *
	 * @param data
	 * @return
	 * @return: SaleStatisticsAccumulateDayView
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 19, 2012
	 */
	public static SaleStatisticsAccumulateDayView getInstance(Bundle data) {
		SaleStatisticsAccumulateDayView instance = new SaleStatisticsAccumulateDayView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_sale_statistics_accumulate_day_view, container,
				false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		//setting show price
		isShowPrice = GlobalInfo.getInstance().isSysShowPrice();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DONTONGLUYKE);
		this.initView(v);
		parent.setTitleName(StringUtil
				.getString(R.string.TITLE_SALE_STATISTICS_ACCUMULATE_DAY_VIEW));
		this.requestGetAccumulateDay();
		return v;
	}

	/**
	 *
	 * init controls for view
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 19, 2012
	 */
	public void initView(View v) {
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvIndustryProduct, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_SPNGANH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvInputProductCode, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_ETMAHANG);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvInputProductName, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_ETTENHANG);
		spIndustryProduct = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spIndustryProduct, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_SPNGANH);
		PriUtils.getInstance().setOnItemSelectedListener(spIndustryProduct, this);
		etInputProductCode = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.etInputProductCode, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_ETMAHANG);
		etInputProductName = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.etInputProductName, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_ETTENHANG);
		ibtSearch = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.ibtSearch, PriHashMap.PriControl.NVBH_DONTONG_DONTONG_LUYKE_BTTIMKIEM);
		ibtSearch.setOnClickListener(this);
		PriUtils.getInstance().setOnClickListener(ibtSearch, this);

		tbProductList = (DMSTableView) v.findViewById(R.id.tbProductList);
		tbProductList.setListener(this);
	}

	/**
	 *
	 * get list product
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 20, 2012
	 */
	public void requestGetAccumulateDay() {
		if (!parent.isShowProgressDialog()) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}
		isCount = false;
		// send request get list vote product
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		String page = " limit " + (0) + "," + Constants.NUM_ITEM_PER_PAGE;
		data.putString(IntentConstants.INTENT_PAGE, page);
		handleViewEvent(data, ActionEventConstant.GET_LIST_PRODUCT_SALE_STATISTICS_ACCUMULATE_DAY, SaleController.getInstance());
	}

	/**
	 *
	 * get list product
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 20, 2012
	 */
	public void requestListProductSold() {
		if (!parent.isShowProgressDialog()) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}
		// send request get list vote product
		Bundle data = new Bundle();
		String productCode = StringUtil
				.getEngStringFromUnicodeString(etInputProductCode.getText()
						.toString());
		String productName = StringUtil
				.getEngStringFromUnicodeString(etInputProductName.getText()
						.toString());
		String industryProduct = "";

		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());

		if (this.saleData != null && this.saleData.listIndustry != null
				&& this.saleData.listIndustry.size() > this.currentSelected) {
			industryProduct = this.saleData.listIndustry
					.get(this.currentSelected);
		}

		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		if (!StringUtil.isNullOrEmpty(productCode)) {
			data.putString(IntentConstants.INTENT_PRODUCT_CODE,
					productCode.trim());
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			data.putString(IntentConstants.INTENT_PRODUCT_NAME,
					productName.trim());
		}
		if (!StringUtil.isNullOrEmpty(industryProduct)
				&& !StringUtil.getString(R.string.TEXT_ALL).equals(industryProduct)) {
			data.putString(IntentConstants.INTENT_INDUSTRY,
					industryProduct.trim());
		}
		int page = tbProductList.getPagingControl().getCurrentPage();
		data.putInt(IntentConstants.INTENT_PAGE, page);
		//add sort info
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbProductList.getSortInfo());
		handleViewEvent(data, ActionEventConstant.GET_SALE_STATISTICS_ACCUMULATE_DAY_LIST_PRODUCT, SaleController.getInstance());
	}

	/**
	 *
	 * init list industry product
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 20, 2012
	 */
	public void renderListIndustryProduct() {
		if (saleData != null && saleData.arrList != null) {
			if (this.saleData.listIndustry.size() > 1) {
				this.saleData.listIndustry.add(0, StringUtil.getString(R.string.TEXT_ALL));
			}
			SpinnerAdapter adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item,
					this.saleData.listIndustry
							.toArray(new String[this.saleData.listIndustry
									.size()]));
			// this.isFirstLoadProduct = true;
			this.spIndustryProduct.setAdapter(adapterLine);
		}
	}

	/**
	 *
	 * render layout for screen
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 19, 2012
	 */
	public void renderLayout() {
		boolean isShowPriceNew = GlobalInfo.getInstance().isSysShowPrice();
		//chi xoa header neu khac setting
		if (isShowPriceNew == isShowPrice) {
			String headerStr = StringUtil
					.getString(R.string.TITLE_SALE_STATISTICS_ACCUMULATE_DAY_VIEW)
					+ Constants.STR_SPACE
					+ saleData.numCycle
					+ Constants.STR_BRACKET_LEFT
					+ DateUtils.convertFormatDate(saleData.beginDate,DateUtils.DATE_FORMAT_NOW,DateUtils.DATE_FORMAT_DATE_VN)
					+ Constants.STR_SUBTRACTION
					+ DateUtils.convertFormatDate(saleData.endDate,DateUtils.DATE_FORMAT_NOW,DateUtils.DATE_FORMAT_DATE_VN)
					+ Constants.STR_BRACKET_RIGHT;
			parent.setTitleName(headerStr);
			tbProductList.clearAllData();
		} else{
			tbProductList.resetSortInfo();
			tbProductList.clearAllDataAndHeader();
			//update setting show price
			isShowPrice = isShowPriceNew;
		}

		if(!tbProductList.isHeaderExists()){
			SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
				.addInfo(4, SortActionConstants.CAT)
				.addInfo(5, SortActionConstants.AMOUNT_PLAN)
				.addInfo(6, SortActionConstants.AMOUNT_DONE)
				.addInfo(7, SortActionConstants.AMOUNT_APPROVED)
				.addInfo(8, SortActionConstants.AMOUNT_REMAIN)
				.addInfo(10, SortActionConstants.QUANITY_PLAN)
				.addInfo(11, SortActionConstants.QUANITY_DONE)
				.addInfo(12, SortActionConstants.QUANITY_APPROVED)
				.addInfo(13, SortActionConstants.QUANITY_REMAIN)
				.build();
			//add header
			initHeaderTable(tbProductList, new SaleStatisticsAccumulateDayRow(parent, isShowPrice), lstSort, this);
		}

		if (isCount) {
			isCount = false;
//			tbProductList.getPagingControl().setCurrentPage(1);
//			tbProductList.setTotalSize(this.saleData.totalList, 1);
		}
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbProductList.getPagingControl().getCurrentPage() - 1);
		SaleStatisticsAccumulateDayDTO.SaleStatisticsAccumulateDayItem itemSum = new SaleStatisticsAccumulateDayDTO.SaleStatisticsAccumulateDayItem();
		for (int i = 0, size = this.saleData.arrList.size(); i < size; i++) {
			SaleStatisticsAccumulateDayDTO.SaleStatisticsAccumulateDayItem data = this.saleData.arrList
					.get(i);
			SaleStatisticsAccumulateDayRow row = new SaleStatisticsAccumulateDayRow(
					parent, isShowPrice);
			row.render(pos, data);
			tbProductList.addRow(row);
			pos++;
			//add data to itemSum
			itemSum.sum(data);
		}

		SaleStatisticsAccumulateDayRow row = new SaleStatisticsAccumulateDayRow(parent, isShowPrice);
		row.renderSum(itemSum);
		tbProductList.addRowSum(row);

		parent.closeProgressDialog();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == ibtSearch) {
			this.requestListProductSold();
			GlobalUtil.forceHideKeyboard(parent);
		}
		super.onClick(v);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_PRODUCT_SALE_STATISTICS_ACCUMULATE_DAY:
			this.saleData.listIndustry = (ArrayList<String>) modelEvent
					.getModelData();
			this.saleData.totalList = -1;
			renderListIndustryProduct();
			//requestCountListProductSold();
			requestListProductSold();
			break;
		case ActionEventConstant.GET_SALE_STATISTICS_ACCUMULATE_DAY_LIST_PRODUCT:
			SaleStatisticsAccumulateDayDTO dto = ((SaleStatisticsAccumulateDayDTO) modelEvent.getModelData());
			this.saleData.arrList = dto.arrList;
			this.saleData.numCycle = dto.numCycle;
			this.saleData.beginDate = dto.beginDate;
			this.saleData.endDate = dto.endDate;
			if (this.saleData.totalList < 0) {
				this.saleData.totalList = dto.totalRow;
				isCount = true;
			}
			renderLayout();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		if (currentSelected != arg2) {
			currentSelected = arg2;
			this.requestListProductSold();
		}
	}

	/**
	 *
	 * clear all data in input text
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 20, 2012
	 */
	public void clearAllData() {
		this.etInputProductCode.setText(Constants.STR_BLANK);
		this.etInputProductName.setText(Constants.STR_BLANK);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_MENU_SALE_STATISTICS_IN_DAY_PRE:
			this.gotoSaleStatisticsProductListInDay();
			break;
		case ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL:
			gotoSaleStatisticsProductListInDayVal();
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 *
	 * display sale statistics product list in fullDate
	 *
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Oct 22, 2012
	 */
	private void gotoSaleStatisticsProductListInDay() {
		handleSwitchFragment(new Bundle(),
				ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_PRE, SaleController.getInstance());
	}

	private void gotoSaleStatisticsProductListInDayVal() {
		handleSwitchFragment(new Bundle(),
				ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_VAL, UserController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				etInputProductCode.setText("");
				etInputProductName.setText("");
				currentSelected = 0;
				spIndustryProduct.setSelection(currentSelected);
				tbProductList.resetSortInfo();
				this.requestGetAccumulateDay();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		// load more data for table product list
		if (control == tbProductList) {
			requestListProductSold();
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub

	}

	public void onResume() {
		super.onResume();
		initMenuBar();
	};

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void initMenuBar() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(PriHashMap.PriForm.NVBH_DONTONGLUYKE,
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_REPORT_MONTH),
						R.drawable.icon_accumulated, ACTION_MENU_SALE_STATISTICS_IN_MONTH, PriHashMap.PriForm.NVBH_DONTONGLUYKE),
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_SALE_STATISTICS_IN_DAY_VAL),
						R.drawable.icon_calendar, ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL, PriHashMap.PriForm.NVBH_DONTONGNGAYVAN),
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_SALE_STATISTICS_IN_DAY_PRE),
						R.drawable.icon_calendar, ACTION_MENU_SALE_STATISTICS_IN_DAY_PRE, PriHashMap.PriForm.NVBH_DONTONGNGAYPRE));
	}

	/* (non-Javadoc)
	 * @see DMSColSortManager.OnSortChange#onSortChange(DMSTableView, DMSSortInfo)
	 */
	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		if (tb == tbProductList) {
			requestListProductSold();
		}
	}
}
