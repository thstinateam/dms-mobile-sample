package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class SupervisorCustomerListDTO implements Serializable {
	public int totalCustomer;
	public ArrayList<CustomerListItem> cusList;
	public int curPage = -1;

	public SupervisorCustomerListDTO() {
		cusList = new ArrayList<CustomerListItem>();
	}
}
