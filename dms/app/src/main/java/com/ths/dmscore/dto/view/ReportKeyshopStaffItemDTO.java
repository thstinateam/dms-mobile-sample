/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.KSLevelDTO;

/**
 * ReportKeyshopStaffItemDTO.java
 * @author: yennth16
 * @version: 1.0
 * @since:  16:36:21 20-07-2015
 */
public class ReportKeyshopStaffItemDTO {
	public long id;
	public KeyShopItemDTO keyshop = new KeyShopItemDTO();
	public long customerId;
	public String customer;
	public String customerCode;
	public String customerName;
	public String address;
	public String levelRegister;
	public int result;
	public int award = TYPE_RESULT_NOT_ASSIGN; // mac dinh chua co co cau tra thuong
	public double amountRegister;
	public double amountDone;
	public long quantityRegister;
	public long quantityDone;
	public ArrayList<KSLevelDTO> ksLevelItem = new ArrayList<KSLevelDTO>();
	public String productInfo = Constants.STR_BLANK;
	public String cycleName = Constants.STR_BLANK;
	public long ksCustomerId;
	public ArrayList<KSLevelDTO> ksProductItem = new ArrayList<KSLevelDTO>();
	public double totalReward;
	public double totalRewardDone;
	public static final int TYPE_RESULT_NOT_ATTAIN = 5; // CHUA DAT
	public static final int TYPE_RESULT_ATTAIN = 6; // DAT
	public static final int TYPE_REWARD_TRANSFER= 0; // chuyen khoan
	public static final int TYPE_RESULT_LOCK = 1; // khoa
	public static final int TYPE_RESULT_NOT_PAID = 2; // chua tra
	public static final int TYPE_RESULT_PAID_PART = 3; // tra mot phan
	public static final int TYPE_RESULT_PAID = 4; // tra toan bo
	public static final int TYPE_RESULT_NOT_ASSIGN = 5; // chua co cau tra thuong


	/**
	 * initData
	 * @author: yennth16
	 * @since: 20:17:17 20-07-2015
	 * @return: void
	 * @throws:
	 * @param c
	 */
	public void initData(Cursor c){
		id = CursorUtil.getLong(c, "RPT_KS_MOBILE_ID");
		customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		ksCustomerId = CursorUtil.getLong(c, "KS_CUSTOMER_ID");
		quantityRegister = CursorUtil.getLong(c, "QUANTITY_TARGET");
		amountRegister = CursorUtil.getDouble(c, "AMOUNT_TARGET",1, GlobalInfo.getInstance().getSysNumRounding());
		quantityDone = CursorUtil.getLong(c, "QUANTITY");
		amountDone = CursorUtil.getDouble(c, "AMOUNT",1,GlobalInfo.getInstance().getSysNumRounding());
		result = CursorUtil.getInt(c, "RESULT");
		award = CursorUtil.getInt(c, "REWARD_TYPE",TYPE_RESULT_NOT_ASSIGN,TYPE_RESULT_NOT_ASSIGN);
		levelRegister = CursorUtil.getString(c, "REGISTERED_LEVEL");
		customer = CursorUtil.getString(c, "CUSTOMER_CODE") + "-" + CursorUtil.getString(c, "CUSTOMER_NAME");
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		address = CursorUtil.getString(c, "ADDRESS");
		if (StringUtil.isNullOrEmpty(address)) {
			address = CursorUtil.getString(c, "STREET");
		}
		keyshop.ksId = CursorUtil.getLong(c, "KS_ID");
		keyshop.ksCode = CursorUtil.getString(c, "KS_CODE");
		keyshop.name = CursorUtil.getString(c, "NAME");
		keyshop.description = CursorUtil.getString(c, "DESCRIPTION");
		keyshop.fromCycleId = CursorUtil.getString(c, "FROM_CYCLE_ID");
		keyshop.toCycleId = CursorUtil.getString(c, "TO_CYCLE_ID");
		totalReward = CursorUtil.getDouble(c, "TOTAL_REWARD_MONEY",1,GlobalInfo.getInstance().getSysNumRounding());
		totalRewardDone = CursorUtil.getDouble(c, "TOTAL_REWARD_MONEY_DONE",1,GlobalInfo.getInstance().getSysNumRounding());
	}

}
