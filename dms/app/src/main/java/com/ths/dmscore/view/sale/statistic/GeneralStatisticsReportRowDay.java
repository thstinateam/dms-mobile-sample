/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.dto.view.ReportInfoDTO;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Thong tin cua 1 bao cao theo ngay
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 4:43:41 PM | Jun 11, 2012
 */
public class GeneralStatisticsReportRowDay extends DMSTableRow {
	TextView tvContent;
	TextView tvQuota;
	TextView tvAttain;
	TextView tvDuyet;
	TextView tvRemain;
//	TextView tvTotalMathNVBH;

	public GeneralStatisticsReportRowDay(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_general_statistics_row_day);
//		tvContent = (TextView) view.findViewById(R.id.tvContent);
		listener = listen;
		tvContent = (TextView) PriUtils.getInstance().findViewById(this, R.id.tvContent, PriHashMap.PriControl.NVBH_THONGKE_CHITIETTIENDONGAY);
		tvQuota = (TextView) findViewById(R.id.tvQuota);
		tvAttain = (TextView) findViewById(R.id.tvAttain);
		tvDuyet = (TextView) findViewById(R.id.tvDuyet);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
//		tvTotalMathNVBH = (TextView) findViewById(R.id.tvTotalMathNVBH);
	}

	/**
	 *
	 * render layout for row with a GeneralStatisticsViewDTO
	 *
	 * @author: HaiTC3
	 * @since: 4:51:47 PM | Jun 11, 2012
	 * @param position
	 * @param GeneralStatisticsViewDTO
	 *            item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, ReportInfoDTO item) {
		tvContent.setText(item.content);
		if (item.reportType == ReportInfoDTO.REPORT_DETAIL) {
			// truong hop doanh so
			if ((ReportInfoDTO.TYPE_AMOUNT.equals(item.code))) {
				display(tvQuota, item.amountPlan);
				display(tvAttain, item.amount);
				display(tvDuyet, item.amountApproved);
				display(tvRemain, item.amountRemain);
			} else {
				// truong hop SL phai ep ve kieu long de khoi hien thi dau thap
				// phan o sau 50.00
				display(tvQuota, (long) item.quantityPlan);
				display(tvAttain, (long)item.quantity);
				display(tvDuyet, (long)item.quantityApproved);
				display(tvRemain, (long)item.quantityRemain);
			}
//			tvTotalMathNVBH.setVisibility(View.GONE);
		} else {
			tvQuota.setVisibility(View.GONE);
			tvAttain.setVisibility(View.GONE);
			tvDuyet.setVisibility(View.GONE);
			tvRemain.setVisibility(View.GONE);

			// tvContent.setTypeface(null, Typeface.BOLD);
			tvContent.setVisibility(View.GONE);
//			PriUtils.setStatus(tvContent, PriUtils.INVISIBLE);
//			tvTotalMathNVBH.setVisibility(View.VISIBLE);
//			tvTotalMathNVBH.setText(StringUtil.getString(R.string.TEXT_MATH_SALE_2) + Constants.STR_SPACE
//					+ String.valueOf(item.startSale));
		}
	}

	int action;
	public void setAction(int _action) {
		this.action = _action;
//		tvContent.setOnClickListener(this);
		PriUtils.getInstance().setOnClickListener(tvContent, this);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.tvContent:
			listener.handleVinamilkTableRowEvent(action, arg0, null);
			break;

		default:
			super.onClick(arg0);
			break;
		}

	}

}
