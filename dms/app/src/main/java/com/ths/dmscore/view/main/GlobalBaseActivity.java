package com.ths.dmscore.view.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.commonsware.cwac.cache.HashMapManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapFragment;
import com.google.code.microlog4android.appender.LogCatAppender;
import com.google.code.microlog4android.config.PropertyConfigurator;
import com.ths.dms.R;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SynDataController;
import com.ths.dmscore.controller.TBHVController;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.dto.db.StaffPositionLogDTO;
import com.ths.dmscore.dto.syndata.SynDataDTO;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPClient;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.LogFile;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.ServiceUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.util.guard.DMSDeviceAdminReceiver;
import com.ths.dmscore.view.control.RightHeaderMenuBar;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.VinamilkTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.sale.image.ListAlbumUserView;
import com.ths.dmscore.CommonUtilities;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.AbstractController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.NotifyOrderDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto;
import com.ths.dmscore.dto.view.SaleOrderViewDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.util.AsyncTaskUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.GlobalUtil.AppInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.NetworkUtil;
import com.ths.dmscore.util.NetworkUtil.DMSNetworkInfo;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.StatusNotificationHandler;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.util.guard.AccessInternetService;
import com.ths.dmscore.util.locating.Locating;
import com.ths.dmscore.util.locating.LocationService;
import com.ths.dmscore.util.locating.PositionManager;
import com.ths.dmscore.view.control.KeyboardPopupWindow;
import com.ths.dmscore.view.listener.OnDialogControlListener;
import com.ths.dmscore.view.sale.order.ListEquipErrorRow;
import com.ths.dmscore.view.sale.order.RemainProductView;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

public class GlobalBaseActivity extends Activity implements
		OnCompletionListener, OnDialogControlListener, OnEventControlListener,
		OnClickListener, OnCancelListener {
	public static final String TIME_SAVE_IMAGE_LIST = "timeSaveImageList";
	public static final String LAST_LOG_ID = "com.viettel.vinamilk.lastLogId";
	public static final String DATE_CLEAR_OLD_DATA = "com.viettel.vinamilk.dateClearData";
	// chuoi action cua cac broadcast message
	public static final String VNM_ACTION = "com.viettel.dmscore.BROADCAST";
	// sync du lieu khi ung dung dang inactive
	public static final int REQUEST_SYNC_FROM_INACTIVE_APP = 0;
	// sync du lieu khi nguoi dung thao tac
	public static final int REQUEST_SYNC_FROM_USER = 1;

	// thoi gian rung khi co notification, chat
	private static final int VIBRATION_DURATION = 500;
	// take photo
	public static final int RQ_PICK_PHOTO_ATTACH = 9998;
	public static final int RQ_TAKE_PHOTO_ATTACH = 9999;
	public static final int RQ_TAKE_PHOTO = 999;
	public static final int RQ_SALE_TAKE_PHOTO = 996;
	public static final int RQ_TAKE_PHOTO_VOTE_DP = 998;
	public static final int RQ_SUPERVISOR_TAKE_PHOTO = 997;
	public static final int RQ_TAKE_PHOTO_INVENTORY_DV= 1000;
	private static final int ACTIVATION_REQUEST_ADMIN = -1991;

	// before re-login
	public static final int MENU_FINISH_VISIT = 1001;
	public static final int MENU_FINISH_VISIT_CLOSE = 1002;
	// action dong y ra lai man hinh login
	final int ACTION_SHOW_LOGIN = 1003;
	// action dong y dang nhap lai de tai du lieu
	final int ACTION_OK_RESET_DB = 1004;
	final int ACTION_CANCEL_RESET_DB = 1005;
	public String draftOrderId = "";
	// check mock location
	public final static int ACTION_ALLOW_MOCK_LOCATION_OK = 1007;
	public final static int ACTION_ALLOW_MOCK_LOCATION_CANCEL = 1008;
	public final int ACTION_SELECTED_APP_DELETE = 1009;

	private boolean isFakeGPS = true;
	//chua danh sach cac vi tri cu
	private List<Location> listLastKnowLocation = new ArrayList<Location>();

	// kiem tra activity da finish hay chua
	public boolean isFinished = false;
	// check re-login one time
	private boolean blReLogin = false;
	// save actionevent
	public ActionEvent actionBeforeReLogin = null;
	// mang cac request dang xu ly do activity request
	private ArrayList<HTTPRequest> unblockReqs = new ArrayList<HTTPRequest>();
	private ArrayList<HTTPRequest> blockReqs = new ArrayList<HTTPRequest>();
	protected final int ICON_RIGHT_PARENT = 0;
	protected final int ICON_NEXT_1 = 1;
	protected final int ICON_BUTTON = -1;
	public static final int RESEND_TYPE = -1;
	private LinearLayout rootView;
	protected LinearLayout mainContent;
	protected LinearLayout llShowHideMenu;// layout chua icon an hien menu
	protected LinearLayout llMenubar;// layout menu icon ben phai
	protected LinearLayout llMenuUpdate;// menu update du lieu tu server
	protected LinearLayout llMenuGPS;
	protected LinearLayout llMenuEquip;// menu thong bao thiet bi dang giao dich
	protected LinearLayout llStatus;// layout chua trang thai
	protected TextView tvStatus;// text trang thai
	// dialog hien thi khi request
	protected ProgressDialog progressDlg;
	// header kunkun
	private View rlHeader;
	protected LinearLayout llMenuWarning;// logo Warning
	// ds orderId fail
	ArrayList<Long> listOrderId = new ArrayList<Long>();
	// menu icon goc phai trong activity
	public RightHeaderMenuBar menuBar;
	// thuc hien hieu ung rung khi co notification
	private Vibrator vibrator;
	// am thanh khi co notification, chat
	private MediaPlayer soundPlayer;
	protected int synType;
	// cancel request udate du lieu giua chung hay khong
	private boolean isCancelUpdateData = false;
	// dang insert action log?
	public boolean isInsertingActionLogVisit = false;
	// control dayInOrder picker
	public DatePickerDialog datePickerDialog;
	// fullDate for dialog dayInOrder picker
	public int day = 0;
	// month for dialog dayInOrder picker
	public int month = 0;
	// year for dialog dayInOrder picker
	public int year = 0;
	//dialog check google play
	private Dialog dialogCheckGooglePlay;

	//public String fragmentTag = "";
	public static final int DATE_DIALOG_ID = 0;
	public static final int TIME_DIALOG_ID = 1;
	public static final String BROACAST_PERMISSION = "com.viettel.idp.permission.BROADCAST";

	protected Bundle trainingReviewStaff;

	// bien do log KPI
	public Calendar startTimeKPI = null;
	public Calendar endTimeKPI;

	// dung tinh % hoan thanh dong bo
	long beginLogId;

	//relogin khi mat session
	int numRelogin = 0;

	// popup ds cac thiet bi bao mat
	private AlertDialog alertProductDetail;
	private VinamilkTableView tbListEquipError;
	Button btPass;
	// ds cac ung dung can phai xoa
	ArrayList<ApplicationInfo> blackApps = new ArrayList<ApplicationInfo>();
	PackageManager packageManager = null;
	// alert cac ung dung can xoa
	private AlertDialog alertBackListAppDialog;
	// view hien thi ds cac ung dung can xoa
	BlackListAppView blackListView;
	
	static volatile GlobalBaseActivity instance;

	public static GlobalBaseActivity getInstance() {
		if (instance == null) {
			instance = new GlobalBaseActivity();
		}
		return instance;
	}

	public void setTrainingReviewStaffBundle(Bundle bundle) {
		this.trainingReviewStaff = bundle;
	}

	public Bundle getTrainingReviewStaffBundle() {
		return this.trainingReviewStaff;
	}

	// broadcast receiver, nhan broadcast
	BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int action = intent.getExtras().getInt(Constants.ACTION_BROADCAST);
			int hasCode = intent.getExtras().getInt(
					Constants.HASHCODE_BROADCAST);
			if (hasCode != GlobalBaseActivity.this.hashCode()) {
				receiveBroadcast(action, intent.getExtras());
			}
			String newMessage = intent.getExtras().getString(
					CommonUtilities.EXTRA_MESSAGE);
			if (newMessage != null) {
				showDialog(newMessage);
			}
		}
	};
	public File takenPhoto;
	private AlertDialog alertRemindDialog;
	private TextView tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			Thread.setDefaultUncaughtExceptionHandler(new VNMTraceUnexceptionLog(this));
		} catch (Exception e) {
			MyLog.e("setDefaultUncaughtExceptionHandler", e);
		}
		GlobalInfo.getInstance().setActivityContext(this);

		isLoadedNativeLib = false;
		loadNativeLib();
		// checkUnCaughtExeptionFile(this);
		registerReceiver(receiver, new IntentFilter(
				CommonUtilities.DISPLAY_MESSAGE_ACTION));
		PropertyConfigurator.getConfigurator(this).configure();
		if (MyLog.logger.getNumberOfAppenders() == 0) {
			MyLog.logger.addAppender(new LogCatAppender());
		}
		if (savedInstanceState != null) {
			if (savedInstanceState
					.containsKey(IntentConstants.INTENT_GLOBAL_BUNDLE)) {
				getIntent()
						.putExtras(
								savedInstanceState
										.getBundle(IntentConstants.INTENT_GLOBAL_BUNDLE));
			}
			if (savedInstanceState != null) {
				if (savedInstanceState
						.containsKey(IntentConstants.INTENT_GLOBAL_BUNDLE)) {
					getIntent()
							.putExtras(
									savedInstanceState
											.getBundle(IntentConstants.INTENT_GLOBAL_BUNDLE));
				}
				if (StringUtil.isNullOrEmpty(HTTPClient.getSessionID())
						&& savedInstanceState
								.containsKey(IntentConstants.INTENT_SESSION_ID)) {
					HTTPClient.setSessionID(savedInstanceState
							.getString(IntentConstants.INTENT_SESSION_ID));
				}
			}
			if (savedInstanceState.containsKey(IntentConstants.INTENT_PRI_UTIL)) {
				PriUtils pri = (PriUtils) savedInstanceState.getSerializable(IntentConstants.INTENT_PRI_UTIL);
				if (pri != null) {
					GlobalInfo.getInstance().setPriUtilsInstance(pri);
				}
			}
		}
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		try {
			IntentFilter filter = new IntentFilter(VNM_ACTION);
			registerReceiver(receiver, filter);
		} catch (Exception e) {
		}
		packageManager = getPackageManager();
	}

	/**
	 * ham onCreate voi tham so co dang ky lang nghe broadcast khong
	 * @author: Unknown
	 * @param savedInstanceState
	 * @param broadcast
	 * @return: void
	 * @throws:
	 */
	protected void onCreate(Bundle savedInstanceState, boolean broadcast) {
		super.onCreate(savedInstanceState);
		try {
			Thread.setDefaultUncaughtExceptionHandler(new VNMTraceUnexceptionLog(this));
		} catch (Exception e) {
			MyLog.e("setDefaultUncaughtExceptionHandler", e);
		}
		GlobalInfo.getInstance().setActivityContext(this);

		isLoadedNativeLib = false;
		loadNativeLib();
		registerReceiver(receiver, new IntentFilter(
				CommonUtilities.DISPLAY_MESSAGE_ACTION));
		if (savedInstanceState != null) {
			if (savedInstanceState
					.containsKey(IntentConstants.INTENT_GLOBAL_BUNDLE)) {
				getIntent()
						.putExtras(
								savedInstanceState
										.getBundle(IntentConstants.INTENT_GLOBAL_BUNDLE));
			}
			if (savedInstanceState != null) {
				if (savedInstanceState
						.containsKey(IntentConstants.INTENT_GLOBAL_BUNDLE)) {
					getIntent()
							.putExtras(
									savedInstanceState
											.getBundle(IntentConstants.INTENT_GLOBAL_BUNDLE));
				}
				if (StringUtil.isNullOrEmpty(HTTPClient.getSessionID())
						&& savedInstanceState
								.containsKey(IntentConstants.INTENT_SESSION_ID)) {
					HTTPClient.setSessionID(savedInstanceState
							.getString(IntentConstants.INTENT_SESSION_ID));
					// KunKunUtils.reloginXMPP(this);
				}
			}
		}

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		// this.broadcast = broadcast;
		if (broadcast) {
			try {
				IntentFilter filter = new IntentFilter(VNM_ACTION);
				registerReceiver(receiver, filter);

			} catch (Exception e) {
			}
		}
		packageManager = getPackageManager();
	}

	@Override
	protected void onStart() {
		super.onStart();
		NetworkUtil.me().startListenerSignal();
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(R.layout.layout_global_main);
		rootView = (LinearLayout) findViewById(R.id.ll_global_main);
		mainContent = (LinearLayout) findViewById(R.id.ll_content);
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(layoutResID, mainContent);
		initialize();
		// setTitleName("Vinamilk");
	}

	@Override
	protected void onStop() {
		super.onStop();
		NetworkUtil.me().stopListenerSignal();
	}

	@Override
	protected void onDestroy() {
		if (this == GlobalInfo.getInstance().getLastActivity()) {
			GlobalInfo.getInstance().setLastActivity(null);
		}
		try {
			unregisterReceiver(receiver);
			if(progressDlg != null && progressDlg.isShowing()) {
				progressDlg.dismiss();
			}
			if (dialogCheckGooglePlay != null && dialogCheckGooglePlay.isShowing()) {
				dialogCheckGooglePlay.dismiss();
			}
		} catch (Exception e) {
		}
		GlobalUtil.nullViewDrawablesRecursive(rootView);
		HashMapManager.getInstance().clearHashMapById(this.toString());
		System.gc();
		System.runFinalization();
		super.onDestroy();
	}

	@Override
	public void finish() {
		isFinished = true;
		super.finish();
	}

	@Override
	public void finishActivity(int requestCode) {
		isFinished = true;
		super.finishActivity(requestCode);
	}


	/**
	 * Init view base activity
	 * @author : BangHN since : 1.0
	 */
	private void initialize() {
		LayoutInflater inflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rlHeader = inflater.inflate(R.layout.global_header, null);
		rootView.addView(rlHeader, 0);

		// Cap nhat hien thi logo loading
		llShowHideMenu = (LinearLayout) rlHeader
				.findViewById(R.id.llShowHideMenu);
		llMenubar = (LinearLayout) rlHeader.findViewById(R.id.llMenubar);
		llMenuUpdate = (LinearLayout) rlHeader.findViewById(R.id.llMenuUpdate);
		llMenuEquip = (LinearLayout) rlHeader.findViewById(R.id.llMenuEquip);
		llStatus = (LinearLayout) rlHeader.findViewById(R.id.llStatus);
		tvStatus = (TextView) rlHeader.findViewById(R.id.tvStatus);
		tvTitle = (TextView) rlHeader.findViewById(R.id.tvTitle);
		tvTitle.setOnClickListener(this);
		llMenuWarning = (LinearLayout) rlHeader
				.findViewById(R.id.llMenuWarning);
		// tvLocation = (EditText) rl_header.findViewById(R.id.tvLocation);
		llMenuGPS = (LinearLayout) rlHeader.findViewById(R.id.llMenuGPS);
		llMenuGPS.setVisibility(View.GONE);
		llMenuGPS.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyLog.d("restartLocating", "Re Locating button click");
				reStartLocatingWithWaiting();
			}
		});

		llMenuUpdate.setVisibility(View.GONE);
		llMenuUpdate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				int numAsyncTaskActive = AsyncTaskUtil.getNumAsyncTaskActive();
				if (numAsyncTaskActive >= 3
						|| GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_EXECUTING) {
					// ghi log
					ServerLogger.sendLog(
							"SynData", "Luong cap nhat tam chua xu ly (active, stateSyn): "
									+ numAsyncTaskActive + ", "
									+ GlobalInfo.getInstance().getStateSynData(),
							false, TabletActionLogDTO.LOG_CLIENT);
					showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
				} else if (progressDlg == null
						|| (progressDlg != null && !progressDlg.isShowing())) {
					isCancelUpdateData = false;
					showProgressDialog(StringUtil.getString(R.string.updating));
					if (GlobalUtil.checkNetworkAccess()) {
						TransactionProcessManager.getInstance().resetChecking(
								TransactionProcessManager.SYNC_FROM_UPDATE);
						synType = REQUEST_SYNC_FROM_USER;
					} else {
						startTimeKPI = Calendar.getInstance();
						requestSynData(REQUEST_SYNC_FROM_USER);
					}
					
					//show thong bao qua gio fullDate du lieu
					if (!GlobalInfo.getInstance().isInSynTime()) {
						int begin = GlobalInfo.getInstance().getBeginTimeAllowSynData();
						int end = GlobalInfo.getInstance().getEndTimeAllowSynData();
						
						//check thoi gian syn khong hop le, bao loi
						GlobalBaseActivity.this.showToastMessage(StringUtil.getString(R.string.ERROR_WRONG_UPDATE_TIME, begin, end));
					}
				}
			}
		});

		llMenuEquip.setVisibility(View.GONE);
		llMenuEquip.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// hien popup ds thong tin thiet bi bao mat nhung dang giao dich
				getEquipList();
			}
		});

		llMenuWarning.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// danh sach order_id : listOrderId
				int numAsyncTaskActive = AsyncTaskUtil.getNumAsyncTaskActive();
				if (numAsyncTaskActive >= 3
						|| GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_EXECUTING) {
					showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
				} else if (progressDlg == null
						|| (progressDlg != null && !progressDlg.isShowing())) {
					if (GlobalInfo.getInstance().getNotifyOrderReturnInfo().isSyncDataFromServer) {
						isCancelUpdateData = false;
						// neu can update du lieu tu server thi update truoc khi
						// hien thi popup
						showProgressDialog(StringUtil
								.getString(R.string.updating));
						if (GlobalUtil.checkNetworkAccess()) {
							TransactionProcessManager
									.getInstance()
									.resetChecking(
											TransactionProcessManager.SYNC_FROM_UPDATE);
							synType = REQUEST_SYNC_FROM_INACTIVE_APP;
						} else {
							startTimeKPI = Calendar.getInstance();
							requestSynData(REQUEST_SYNC_FROM_INACTIVE_APP);
						}
					} else {
						showProgressDialog(StringUtil
								.getString(R.string.loading));
						requestGetAllOrderFail();
					}
				}
			}
		});
	}

	public void addView(View v) {
		mainContent.addView(v);
		mainContent.forceLayout();
	}

	/**
	 * xoa view
	 * @author: DoanDM
	 * @param view
	 * @return: void
	 * @throws:
	 */
	public void removeView(View view) {
		mainContent.removeView(view);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			String filePath = "";
			switch (requestCode) {
			case ACTIVATION_REQUEST_ADMIN:
				if (resultCode == Activity.RESULT_OK) {
					MyLog.i("ACTIVATION_REQUEST_ADMIN", "Administration enabled!");
				} else {
					MyLog.i("ACTIVATION_REQUEST_ADMIN", "Administration enable FAILED!");
				}
				break;
			case GlobalBaseActivity.RQ_SALE_TAKE_PHOTO: {
				if (resultCode == RESULT_OK && takenPhoto != null) {
					filePath = takenPhoto.getAbsolutePath();
					ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
							this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
					validator.setDataIntent(data);
					if (validator.execute()) {
						// GlobalInfo.getInstance().setBitmapData(validator.getBitmap());
						ListAlbumUserView orderFragment = (ListAlbumUserView)
								this.findFragmentByTag(GlobalUtil.getTag(ListAlbumUserView.class));
						if (orderFragment != null) {
							orderFragment.updateTakenPhoto();
						}
					}
				}
				break;
			}
			case RQ_TAKE_PHOTO: {
				if (resultCode == RESULT_OK && takenPhoto != null) {
					filePath = takenPhoto.getAbsolutePath();
					ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
							this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
					validator.setDataIntent(data);
					if (validator.execute()) {
						ListAlbumUserView orderFragment = (ListAlbumUserView)
								this.findFragmentByTag(GlobalUtil.getTag(ListAlbumUserView.class));
						if (orderFragment != null) {
							orderFragment.updateTakenPhoto();
						}
					}
				}
				break;
			}
			case RQ_SUPERVISOR_TAKE_PHOTO: {

				if (resultCode == RESULT_OK) {
					filePath = takenPhoto.getAbsolutePath();
					ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
							this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
					validator.setDataIntent(data);
//					if (validator.execute()) {
//						// GlobalInfo.getInstance().setBitmapData(validator.getBitmap());
//						SupervisorListAlbumUserView orderFragment = (SupervisorListAlbumUserView)
//								this.findFragmentByTag(GlobalUtil.getTag(SupervisorListAlbumUserView.class));
//						if (orderFragment != null) {
//							orderFragment.updateTakenPhoto();
//						}
//					}
				}
			}

			}
		} catch (Exception ex) {
			ServerLogger.sendLog("ActivityState", "GlobalBaseActivity : "
					+ VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 * Return dialog object
	 * @author banghn
	 * @return
	 */
	public ProgressDialog getProgressDialog() {
		return progressDlg;
	}

	/**
	 * show progress dialog
	 * @author: AnhND
	 * @param content
	 * @param cancleable
	 * @return: void
	 * @throws:
	 */
	public void showProgressDialog(String content, boolean cancleable) {
		if (progressDlg != null && progressDlg.isShowing()) {
			closeProgressDialog();
		}
		if (!isFinishing()) {
			progressDlg = ProgressDialog.show(this, "", content, true, true);
			progressDlg.setOnCancelListener(this);
			progressDlg.setCancelable(cancleable);
			progressDlg.setCanceledOnTouchOutside(false);
		}
	}

	public void closeProgressDialog() {
		if (progressDlg != null) {
			try {
				this.closeDialog(progressDlg);
				progressDlg = null;
			} catch (Exception e) {
				MyLog.i("Exception", e.toString());
			}
		}
	}

	public boolean isShowProgressDialog() {
		if (progressDlg != null && progressDlg.isShowing()) {
			return true;
		}
		return false;
	}

	public void showProgressPercentDialog(String content) {
		if (!isFinishing()) {
			if (progressDlg != null && progressDlg.isShowing()) {
				closeProgressDialog();
			}
			progressDlg = new ProgressDialog(this);
			progressDlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDlg.setMessage(content);
			progressDlg.setCancelable(false);
			progressDlg.show();
		}
	}

	public void showProgressPercentDialog(String content, boolean isCancelable) {
		if (!isFinishing()) {
			if (progressDlg != null && progressDlg.isShowing()) {
				closeProgressDialog();
			}
			progressDlg = new ProgressDialog(this);
			progressDlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDlg.setMessage(content);
			progressDlg.setCancelable(isCancelable);
			progressDlg.setCanceledOnTouchOutside(false);
			progressDlg.setOnCancelListener(this);
			progressDlg.setProgress(0);
			progressDlg.show();
		}
	}

	public void updateProgressPercentDialog(int percent) {
		if (progressDlg != null) {
			progressDlg.setProgress(percent);
		}
	}

	public void updateProgressEndPercentDialog() {
		updateProgressPercentDialog(98);
		updateProgressPercentDialog(99);
		updateProgressPercentDialog(100);
	}

	public void closeProgressPercentDialog() {
		this.closeDialog(progressDlg);
		progressDlg = null;
	}

	public View getLogoHeader() {
		return rlHeader;
	}

	/**
	 * Enable menu bar icon goc phai o action bar cua activity
	 *
	 * @author : BangHN since : 1.0
	 */
	public void enableMenuActionBar(OnEventControlListener listener) {
		menuBar = new RightHeaderMenuBar(this);
		menuBar.setOnEventControlListener(listener);
		llMenubar.addView(menuBar);
	}

	public void disableMenuActionBar() {
		llMenubar.removeAllViews();
	}

	/**
	 * Add icon menu
	 *
	 * @author : TamPQ since : 4:30:39 PM
	 */
	public void addMenuActionBarItem(int drawableResId, int action) {
		menuBar.addMenuItem(drawableResId, action, View.VISIBLE);
	}

	public void addMenuActionBarItem(String text, int drawableResId, int action) {
		menuBar.addMenuItem(text, drawableResId, action, View.VISIBLE);
	}

	/**
	 * Add menu icon kem theo co hien thi dau ngan cach hay khong (GONE ||
	 * VISIBLE)
	 * @author : BangHN since : 9:20:11 AM
	 */
	public void addMenuActionBarItem(int drawableResId, int action,
			int separateVisible) {
		menuBar.addMenuItem(drawableResId, action, separateVisible);
	}

	public void addMenuActionBarItem(String text, int drawableResId,
			int action, int separateVisible) {
		menuBar.addMenuItem(text, drawableResId, action, separateVisible);
	}

	/**
	 * Init menu bar khi ghe tham khach hang
	 * @author : BangHN since : 1.0
	 */
	public void initMenuVisitCustomer(String cusCode, String cusName) {
		llMenubar.removeAllViews();
		enableMenuActionBar(this);
		addMenuActionBarItem(StringUtil.getString(R.string.TEXT_FINISH_VISIT),
				R.drawable.icon_exit, MENU_FINISH_VISIT);
		addMenuActionBarItem(StringUtil.getString(R.string.TEXT_CLOSE_DOOR),
				R.drawable.icon_shop, MENU_FINISH_VISIT_CLOSE);
		String span = new String();
		span += StringUtil.getString(R.string.TEXT_VISITTING);
		if (!StringUtil
				.isNullOrEmpty(cusName)) {
			span += cusName;
		}
		span += " - ";
		if (!StringUtil.isNullOrEmpty(cusCode)) {
			span += cusCode + " ";
		}
		setStatusVisible(span, View.VISIBLE);
	}

	/**
	 * Init menu bar khi ghe tham khach hang, chi init Ten khach hang
	 * @author : TamPQ since : 1.0
	 */
	public void initCustomerNameOnMenuVisit(String cusCode, String cusName) {
		llMenubar.removeAllViews();
		enableMenuActionBar(this);
		String span = new String();
		if (!StringUtil
				.isNullOrEmpty(cusName)) {
			span += cusName;
		}
		span += " - ";
		if (!StringUtil
				.isNullOrEmpty(cusCode)) {
			span += cusCode + " ";
		}
		setStatusVisible(span, View.VISIBLE);
	}

	/**
	 * remove menu khach hang dong cua
	 * @author : BangHN since : 1.0
	 */
	public void removeMenuCloseCustomer() {
		if (menuBar != null && menuBar.view != null) {
			try {
				menuBar.view.removeViewAt(2);
			} catch (Exception e) {
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
		}
	}

	/**
	 * remove menu khach hang dong cua
	 * @author : TamPQ since : 1.0
	 */
	public void removeMenuFinishCustomer() {
		if (menuBar != null && menuBar.view != null) {
			try {
				menuBar.view.removeViewAt(1);
			} catch (Exception e) {
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
		}
	}

	/**
	 * An status & menu khi ket thuc ghe tham ket hang
	 * @author : BangHN since : 1.0
	 */
	public void endVisitCustomerBar() {
		if (menuBar != null) {
			menuBar.removeAllViews();
		}
		setStatusVisible("", View.GONE);
	}

	/**
	 * Set trang thai
	 * @author : BangHN since : 1.0
	 */
	public void setStatusVisible(String status, int visible) {
		if (status != null && tvStatus!=null) {
			tvStatus.setText(status);
			tvStatus.setOnClickListener(null);
			//draftOrderId=StringUtil.EMPTY_STRING;
		}
		if(GlobalInfo.isTablet){
			if (llStatus != null) {
				llStatus.setVisibility(visible);
			}
		}else{
			llStatus.setVisibility(View.GONE);
		}
	}

	/**
	 * set drawable cho icon header
	 * @author: DoanDM
	 * @param id
	 * @return: void
	 * @throws:
	 */
	protected void setDrawableIconHeader(int pos, int id, String text) {
		switch (pos) {
		case ICON_RIGHT_PARENT:
			RelativeLayout rl1 = (RelativeLayout) rlHeader
					.findViewById(R.id.icon1);
			ImageView iv1 = (ImageView) rl1.findViewById(R.id.ivIcon1);
			iv1.setImageDrawable(getResources().getDrawable(id));
			rl1.setVisibility(View.VISIBLE);
			break;
		case ICON_NEXT_1:
			RelativeLayout rl2 = (RelativeLayout) rlHeader
					.findViewById(R.id.icon2);
			ImageView iv2 = (ImageView) rl2.findViewById(R.id.ivIcon2);
			iv2.setImageDrawable(getResources().getDrawable(id));
			rl2.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
	}

	/**
	 * Set icon header
	 * @author banghn
	 * @param res
	 */
	protected void showWarning(boolean isShowWarning) {
		if (llMenuWarning != null && isShowWarning) {
			llMenuWarning.setVisibility(View.VISIBLE);
		} else if (llMenuWarning != null) {
			llMenuWarning.setVisibility(View.GONE);
		}
	}

	/**
	 * hien thong bao thiet bi bao mat dang giao dich
	 * @author: DungNX
	 * @param isShowWarning
	 * @return: void
	 * @throws:
	*/
	public void showMenuEquip(boolean isShowWarning) {
		if (llMenuEquip != null && isShowWarning) {
			llMenuEquip.setVisibility(View.VISIBLE);
		} else if (llMenuEquip != null) {
			llMenuEquip.setVisibility(View.GONE);
		}
	}

	/**
	 * Lay icon header view
	 *
	 * @author: DoanDM
	 * @param pos
	 * @return
	 * @return: View
	 * @throws:
	 */
	protected View getIconHeader(int pos) {
		View v = null;
		switch (pos) {
		case ICON_RIGHT_PARENT:
			RelativeLayout rl1 = (RelativeLayout) rlHeader
					.findViewById(R.id.icon1);
			v = rl1.findViewById(R.id.ivIcon1);
			break;
		case ICON_NEXT_1:
			RelativeLayout rl2 = (RelativeLayout) rlHeader
					.findViewById(R.id.icon2);
			v = rl2.findViewById(R.id.ivIcon2);
			break;
		default:
			break;
		}
		return v;
	}

	/**
	 * Set title bar
	 * @author: DoanDM
	 * @param val
	 * @return: void
	 * @throws:
	 */
	public void setTitleName(String val) {
		if (val == null) {
			val = "";
		}
		TextView title = (TextView) rlHeader.findViewById(R.id.tvTitle);
		title.setText(val);
		title.setVisibility(View.VISIBLE);
	}

	/**
	 * Set title bar
	 * @author: DoanDM
	 * @param val
	 * @return: void
	 * @throws:
	 */
	public void setTitleName(SpannableObject val) {
		TextView title = (TextView) rlHeader.findViewById(R.id.tvTitle);
		title.setText(val.getSpan());
		title.setVisibility(View.VISIBLE);
	}

	public void sendBroadcast(int action, Bundle bundle) {
		Intent intent = new Intent(VNM_ACTION);
		bundle.putInt(Constants.ACTION_BROADCAST, action);
		bundle.putInt(Constants.HASHCODE_BROADCAST, intent.getClass()
				.hashCode());
		intent.putExtras(bundle);

		sendBroadcast(intent, GlobalBaseActivity.BROACAST_PERMISSION);
	}

	/**
	 * Nhan cac broadcast
	 * @author : BangHN since : 1.0
	 */
	public void receiveBroadcast(int action, Bundle bundle) {
		switch (action) {
		case ActionEventConstant.ACTION_FINISH_APP:
			//stop service dinh vi location client
			stopFusedLocationService();
			// clear profile
			GlobalUtil.getInstance().clearAllData();
			finish();
			break;
		case ActionEventConstant.ACTION_STOP_GOOGLE_PLAY_SERVICE:
			//stop service dinh vi location client
			stopFusedLocationService();
			break;
		case ActionEventConstant.ACTION_FINISH_AND_SHOW_LOGIN:
			finish();
			break;
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			//co vi tri thi tat timer dinh vi
			if (timerGPSRequest != null) {
				timerGPSRequest.cancel();
			}
			//dong gps dialog
			closeGPSProgressDialog();
			break;
		case ActionEventConstant.ACTION_UPDATE_POSITION_SERVICE:
			if (this == GlobalInfo.getInstance().getActivityContext()){
				MyLog.d("Locating", "Global Received location update ................. ");
				Location location = bundle.getParcelable(IntentConstants.INTENT_DATA);
				updatePosition(location.getLongitude(), location.getLatitude(), location);
			} else{
				MyLog.d("ACTION_UPDATE_POSITION_SERVICE not except", String.valueOf(this));
			}

			break;
		case ActionEventConstant.NOTIFY_ORDER_STATE:
			if (GlobalInfo.getInstance().getNotifyOrderReturnInfo().hasOrderFail) {
				showWarning(true);
			} else {
				showWarning(false);
			}
			break;
		case ActionEventConstant.NOTIFY_DELETE_SALE_ORDER:
			if (GlobalInfo.getInstance().getLastActivity() != null
					&& GlobalInfo.getInstance().getLastActivity().equals(this)) {
				long saleOrderIdDelete = bundle
						.getLong(IntentConstants.INTENT_SALE_ORDER_ID);
				if (saleOrderIdDelete >= 0 && listOrderId != null) {
					listOrderId.remove(saleOrderIdDelete);
				}
				// xoa don hang da luu
				String orderId = String.valueOf(saleOrderIdDelete);
				if (!GlobalInfo.getInstance().getNotifyOrderReturnInfo().listOrderProcessedId
						.contains(orderId)) {
					GlobalInfo.getInstance().getNotifyOrderReturnInfo().listOrderProcessedId
							.add(orderId);
				}
				// cap nhat icon notify
				if (listOrderId != null && listOrderId.size() > 0) {
					showWarning(true);
				} else {
					showWarning(false);
				}
			}
			break;
		case ActionEventConstant.REQUEST_TO_SERVER_SUCCESS:
			closeProgressDialog();
			if (GlobalInfo.getInstance().getLastActivity() != null
					&& GlobalInfo.getInstance().getLastActivity().equals(this)
					&& GlobalInfo.getInstance().getProfile().getUserData()
					.getLoginState() == UserDTO.LOGIN_SUCCESS) {
				showProgressPercentDialog(StringUtil.getString(R.string.updating), true);
				//luu lai moc de tinh % hoan thanh dong bo
				SharedPreferences sharedPreferences = GlobalInfo.getInstance()
						.getDmsPrivateSharePreference();
				beginLogId = Long.parseLong(sharedPreferences.getString(LAST_LOG_ID, "0"));

				//startTimeKPI = Calendar.getInstance();
				requestSynData(this.synType);
			}
			break;
		case ActionEventConstant.NOTIFY_ORDER_DRAFT: {
			draftOrderId = bundle.getString(IntentConstants.INTENT_ORDER_ID);
			showTitleDraftOrder();
			break;
		}
		case ActionEventConstant.UPGRADE_NOTIFY: {
			closeProgressDialog();
			GlobalUtil.showDialogConfirm(this,
					StringUtil.getString(R.string.ERR_UPGRADE_RELOGIN),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					ACTION_SHOW_LOGIN, null, -1, null);
			break;
		}
		default:
			break;
		}
	}

	/**
	 * show dialog
	 * @param mes
	 * @return
	 */
	public AlertDialog showDialog(final CharSequence mes) {
		if (isFinishing()) {
			return null;
		}
		AlertDialog alertDialog = null;
		try {
			alertDialog = new AlertDialog.Builder(this).create();
			// alertDialog.setTitle("Thong bao");
			alertDialog.setMessage(mes);
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							// TamPQ: neu truong hop ko tim thay doi tuong thi
							// quay tro ve man hinh truoc
							return;

						}
					});
			alertDialog.show();
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return alertDialog;
	}

	/**
	 * show dialog ForAddRequirementView
	 *
	 * @param mes
	 * @return
	 */
	public AlertDialog showDialogForAddRequirementView(final CharSequence mes,
			final Object sender) {
		if (isFinishing()) {
			return null;
		}
		AlertDialog alertDialog = null;
		try {
			alertDialog = new AlertDialog.Builder(this).create();
			// alertDialog.setTitle("Thong bao");
			alertDialog.setMessage(mes);
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							// TamPQ: neu truong hop ko tim thay doi tuong thi
							// quay tro ve man hinh truoc
							ActionEvent e = new ActionEvent();
							e.action = ActionEventConstant.GO_TO_TBHV_FOLLOW_LIST_PROBLEM;
							e.sender = sender;
							TBHVController.getInstance()
									.handleSwitchFragment(e);
							return;

						}
					});
			alertDialog.show();
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return alertDialog;
	}

	/**
	 * set cac constant define in app params "FACTOR", "TIME_TEST_ORDER",
	 * "RADIUS_OF_POSITION", "TIME_TRIG_POSITION", "ALLOW_EDIT_PROMOTION"
	 *
	 * @author banghn
	 */
	protected void setAppDefineConstant(ArrayList<ApParamDTO> result) {
		if (result != null && result.size() > 0) {
			long beginInterval = GlobalInfo.getInstance().getIntervalFusedPosition();
			long beginFastInterval = GlobalInfo.getInstance().getFastIntervalFusedPosition();
			long beginPriority = GlobalInfo.getInstance().getFusedPositionPriority();
			String strStaffTypeId = "_" + GlobalInfo.getInstance().getProfile().getUserData().getStaffTypeId();
			String blackListAppMockLocation = "";
			String whiteListAppMockLocation = "";
			String whiteListAppInstall = "";
			int size = result.size();
			for (int i = 0; i < size; i++) {
				ApParamDTO apParamDTO = result.get(i);
				try {
					MyLog.d("setAppDefineConstant", String.valueOf(apParamDTO));
					if ("TIME_TEST_ORDER".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeTestOrder(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("RADIUS_OF_POSITION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setRadiusOfPosition(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("TIME_TRIG_POSITION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeTrigPosition(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("TIMEOUT_WHEN_IDLE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeAllowIdleStatus(
								Long.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("TIME_ALLOW_SYN_DATA".equals(apParamDTO.getApParamCode())) {
						String[] time = apParamDTO.getValue().split("-");
						GlobalInfo.getInstance().setBeginTimeAllowSynData(
								Integer.valueOf(time[0]), apParamDTO.getStatus());
						GlobalInfo.getInstance().setEndTimeAllowSynData(
								Integer.valueOf(time[1]), apParamDTO.getStatus());
					} else if ("ALLOW_EDIT_PROMOTION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setAllowEditPromotion(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SERVER_IMAGE_PRO_VNM".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setServerImageProductVNM(
								apParamDTO.getValue(), apParamDTO.getStatus());
					} else if ("TIME_SYNC_TO_SERVER".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeSyncToServer(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("TIME_TRIG_POS_ATTEND".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeTrigPositionAttendance(
								Integer.valueOf(apParamDTO.getValue()) * 60 * 1000, apParamDTO.getStatus());
					} else if ("DISTANCE_ALLOW_FINISH".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setDistanceAllowFinish(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("CHECK_DATA_NOTSYN".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDataNotSyn(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("VISIT_TIME_INTERVAL".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setVisitTimeInterval(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("VISIT_TIME_ALLOW".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setVisitTimeAllow(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("MAX_LOGKPI_ALLOW".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setAllowRequestLogKPINumber(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("GSNPP_NUM_DAY_SKU".equals(apParamDTO.getApParamCode())){
						GlobalInfo.getInstance().setNumDaySKUOfGSNPP(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					}else if("FLAG_KEYBOARD".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setFlagKeyboard(Integer.valueOf(apParamDTO.getValue()),
								apParamDTO.getStatus());
					} else if ("SYS_MAP_TYPE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setMapTypeIsUse(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("MANAGER_VIEW_LEVEL".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setManagerViewLevel(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_CAL_UNAPPROVED".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysCalUnapproved(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					}  else if ("SYS_CURRENCY_DIVIDE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysCurrencyDivide(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_SALE_ROUTE".equals(apParamDTO.getApParamCode())) {
					GlobalInfo.getInstance().setSysSaleRoute(
							Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_CURRENCY".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysCurrency(apParamDTO.getValue(), apParamDTO.getStatus());
					} else if ("SYS_DIGIT_DECIMAL".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysDigitDecimal(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_DECIMAL_POINT".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysDecimalPoint(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_DATE_FORMAT".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysDateFormat(apParamDTO.getValue(), apParamDTO.getStatus());
					} else if ("SYS_NUM_ROUNDING".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysNumRounding(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_NUM_ROUNDING_PROMOTION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysNumRoundingPromotion(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("SYS_CAL_DATE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setSysCalDate(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("FLAG_VALIDATE_MOCK_LOCATION".equals(apParamDTO.getApParamType())) {
						GlobalInfo.getInstance().setFlagValidateMockLocation(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("BLACK_LIST_MOCK_LOCATION".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() != 0) {
							blackListAppMockLocation += apParamDTO.getValue() + ";";
						}
					} else if("WHITE_LIST_MOCK_LOCATION".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() != 0) {
							whiteListAppMockLocation += apParamDTO.getValue() + ";";
						}
					} else if("INTERVAL_FUSED_POSITION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setIntervalFusedPosition(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("FAST_INTERVAL_FUSED_POSITION".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setFastIntervalFusedPosition(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("RADIUS_OF_POSITION_FUSED".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setRadiusFusedPosition(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("FUSED_POSTION_PRIORITY".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setFusedPositionPriority(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("MAX_TIME_WRONG".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setMaxTimeWrong(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("DISTANCE_TIME_DRAW_ROUTING".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setDistanceTimeDrawRouting(
								Long.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("RADIUS_DRAW_ROUTING".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setRadiusDrawRouting(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("MAX_SIZE_UPLOAD_ATTACH".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setMaxSizeUploadAttachFile(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if ("WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setWhiteListUploadAttach(apParamDTO.getValue(), apParamDTO.getStatus());
					} else if("REGISTER_APPROVED_KEYSHOP".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setRegisterApprovedKeyshop(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("REGISTER_CTHTTM".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setRegisterCTHTTM(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("ALLOW_DISTANCE_ORDER".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setAllowDistanceOrder(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("DAY_OF_CYCLE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setDayOfCycle(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CSKH_INFO".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCskhInfo(apParamDTO.getValue(), apParamDTO.getStatus());
					} else if("CONNECT_TIME_OUT".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_CONNECT_TIME_OUT && timeOut != NetworkTimeout.getCONNECT_TIME_OUT()) {
								NetworkTimeout.setCONNECT_TIME_OUT(timeOut);
							}
						} else{
							NetworkTimeout.setCONNECT_TIME_OUT(NetworkTimeout.CONNECT_TIME_OUT_DEFAULT);
						}
					} else if("READ_TIME_OUT".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_READ_TIME_OUT && timeOut != NetworkTimeout.getREAD_TIME_OUT()) {
								NetworkTimeout.setREAD_TIME_OUT(timeOut);
							}
						} else{
							NetworkTimeout.setREAD_TIME_OUT(NetworkTimeout.READ_TIME_OUT_DEFAULT);
						}
					} else if("READ_TIME_OUT_SYNDATA".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_READ_TIME_OUT && timeOut != NetworkTimeout.getREAD_TIME_OUT_SYNDATA()) {
								NetworkTimeout.setREAD_TIME_OUT_SYNDATA(timeOut);
							}
						} else{
							NetworkTimeout.setREAD_TIME_OUT_SYNDATA(NetworkTimeout.READ_TIME_OUT_SYNDATA_DEFAULT);
						}
					} else if("CONNECT_TIME_OUT_LOGIN".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_CONNECT_TIME_OUT && timeOut != NetworkTimeout.getCONNECT_TIME_OUT_LOGIN()) {
								NetworkTimeout.setCONNECT_TIME_OUT_LOGIN(timeOut);
							}
						} else{
							NetworkTimeout.setCONNECT_TIME_OUT_LOGIN(NetworkTimeout.CONNECT_TIME_OUT_LOGIN_DEFAULT);
						}
					} else if("READ_TIME_OUT_LOGIN".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_READ_TIME_OUT && timeOut != NetworkTimeout.getREAD_TIME_OUT_LOGIN()) {
								NetworkTimeout.setREAD_TIME_OUT_LOGIN(timeOut);
							}
						} else{
							NetworkTimeout.setREAD_TIME_OUT_LOGIN(NetworkTimeout.READ_TIME_OUT_LOGIN_DEFAULT);
						}
						
					} else if("CONNECT_TIME_OUT_DOWNLOAD_FILE".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_CONNECT_TIME_OUT && timeOut != NetworkTimeout.getCONNECT_TIME_OUT_DOWNLOAD_FILE()) {
								NetworkTimeout.setCONNECT_TIME_OUT_DOWNLOAD_FILE(timeOut);
							}
						} else{
							NetworkTimeout.setCONNECT_TIME_OUT_DOWNLOAD_FILE(NetworkTimeout.CONNECT_TIME_OUT_DOWNLOAD_FILE_DEFAULT);
						}
					} else if("READ_TIME_OUT_DOWNLOAD_FILE".equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() == 1) {
							int timeOut = Integer.valueOf(apParamDTO.getValue());
							if (timeOut >= NetworkTimeout.MIN_READ_TIME_OUT && timeOut != NetworkTimeout.getREAD_TIME_OUT_DOWNLOAD_FILE()) {
								NetworkTimeout.setREAD_TIME_OUT_DOWNLOAD_FILE(timeOut);
							}
						} else{
							NetworkTimeout.setREAD_TIME_OUT_DOWNLOAD_FILE(NetworkTimeout.READ_TIME_OUT_DOWNLOAD_FILE_DEFAULT);
						}
					} else if("CHECK_DIS_VISIT_FIRST_IN_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisVisitFirstInRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_VISIT_FIRST_OUT_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisVisitFirstOutRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_ORDER_FIRST_IN_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisOrderFirstInRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_ORDER_FIRST_OUT_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisOrderFirstOutRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_VISIT_SECOND_IN_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisVisitSecondInRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_VISIT_SECOND_OUT_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisVisitSecondOutRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_ORDER_SECOND_IN_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisOrderSecondInRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_DIS_ORDER_SECOND_OUT_ROUTE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckDisOrderSecondOutRoute(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("CHECK_CUSTOMER_LOCATION_VISIT".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckCustomerLocationVisit(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("SYS_ROUTE_TYPE".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setKpiRouteType(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("SYS_CONVERT_QUANTITY_CONFIG".equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setConfigConvertQuantity(Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if(("WHITE_LIST_APP_INSTALL" + strStaffTypeId).equals(apParamDTO.getApParamCode())) {
						if (apParamDTO.getStatus() != 0) {
							whiteListAppInstall += apParamDTO.getValue() + ",";
						}
					} else if(("CHECK_APP_INSTALL" + strStaffTypeId).equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckAppInstall(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if(("CHECK_NETWORK" + strStaffTypeId).equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setCheckNetwork(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if(("TIME_UNLOCK_APP_ACCESS" + strStaffTypeId).equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setTimeLockAppBlocked(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if(("VALIDATE_MULTI_CLICK_ORDER").equals(apParamDTO.getApParamCode())) {
						GlobalInfo.getInstance().setValidateMultiClickOrder(
								Integer.valueOf(apParamDTO.getValue()), apParamDTO.getStatus());
					} else if("VT_MAP_SET_SERVER".equals(result.get(i).getApParamType())) {
						GlobalInfo.getInstance().setVtMapSetServer(Integer.valueOf(result.get(i).getValue()) == 1 ? true : false);
					} else if("VT_MAP_PROTOCOL".equals(result.get(i).getApParamType())) {
						GlobalInfo.getInstance().setVtMapProtocol(result.get(i).getValue());
					} else if("VT_MAP_IP".equals(result.get(i).getApParamType())) {
						GlobalInfo.getInstance().setVtMapIP(result.get(i).getValue());
					} else if("VT_MAP_PORT".equals(result.get(i).getApParamType())) {
						GlobalInfo.getInstance().setVtMapPort(Integer.valueOf(result.get(i).getValue()));
					}
				} catch (Exception e) {
					MyLog.e("setAppDefineConstant", "fail " + String.valueOf(apParamDTO), e);
				}
			}
			try {
				GlobalInfo.getInstance().setWhiteListApp(whiteListAppInstall);
				GlobalInfo.getInstance().setAppBlackListMockLocation(blackListAppMockLocation);
				GlobalInfo.getInstance().setAppWhiteListMockLocation(whiteListAppMockLocation);
			} catch (Exception e) {
				MyLog.e("set app ap param", e);
			}

			long endInterval = GlobalInfo.getInstance().getIntervalFusedPosition();
			long endFastInterval = GlobalInfo.getInstance().getFastIntervalFusedPosition();
			long endPriority = GlobalInfo.getInstance().getFusedPositionPriority();
			//kiem tra tham so dinh vi fused thay doi
			if (beginInterval != endInterval
					|| beginFastInterval != endFastInterval
					|| beginPriority != endPriority) {
				MyLog.d("restart startFusedLocationService setAppConstants", "change config, restart");
				//start lai luong dong bo fused voi tham so moi (neu co)
				startFusedLocationService();
			} else{
				MyLog.d("restart startFusedLocationService setAppConstants", "not change, not restart");
			}

			//update keyboard params
			updateKeyboardParams();
		}
		// doc thoi gian cham cong
		getAttendaceTime();

//		sendBroadcast(ActionEventConstant.NOTIFY_REFRESH_VIEW,
//				new Bundle());

		// send broadcast toi supervisoractivity lay cau hinh cho phep reset vi
		// tri
		// sendBroadcast(ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION,
		// new Bundle());

		// send broadcast toi salespersonactivity lay cau hinh stock_lock
		// vansales
		// sendBroadcast(ActionEventConstant.ACTION_READ_STOCK_LOCK,
		// new Bundle());

		// send broadcast toi salespersonactivity lay cau hinh shop_lock
		// vansales
		// sendBroadcast(ActionEventConstant.ACTION_READ_SHOP_LOCK,
		// new Bundle());
	}

	/**
	 * Doc cau hinh thoi gian cham cong luu vao global-info
	 * @author: BANGHN
	 */
	private void readAttendaceTime(List<ShopParamDTO> paramList) {
//		if (paramList != null && paramList.size() >= 3
//				&& paramList.get(0).code != null
//				&& paramList.get(1).code != null
//				&& paramList.get(2).code != null) {
//			// starttime
//			int hour = 7, minute = 30;
//			if (paramList.get(0).code != null) {
//				hour = Integer.valueOf(paramList.get(0).code.split(":")[0]);
//				minute = Integer.valueOf(paramList.get(0).code.split(":")[1]);
//			}
//			if (minute > 5)
//				minute -= 5;
//			String ccStartTime = hour + ":" + minute;
//			GlobalInfo.getInstance().setCcStartTime(ccStartTime);
//
//			// endtime
//			hour = 8;
//			minute = 15;
//			hour = Integer.valueOf(paramList.get(1).code.split(":")[0]);
//			minute = Integer.valueOf(paramList.get(1).code.split(":")[1]);
//			String ccEtartTime = hour + ":" + minute;
//			GlobalInfo.getInstance().setCcEndTime(ccEtartTime);
//
//			int distance = 0;
//			try {
//				distance = Integer.parseInt(paramList.get(2).code);
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//			if (distance>0) {
//				// cc_distance
//				GlobalInfo.getInstance().setCcDistance(distance);
//			}
//		}
	}

	public void setStatusVisible(SpannableString status, int visible) {
		if (status != null && tvStatus != null) {
			tvStatus.setText(status);
		}
		if (llStatus != null) {
			llStatus.setVisibility(visible);
		}
	}

	/**
	 * Hien thi title draft order
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void showTitleDraftOrder() {
		final ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if (!StringUtil.isNullOrEmpty(draftOrderId)) {
			SpannableString content = new SpannableString(StringUtil.getString(R.string.TEXT_VISITTING_1) + " : "
					+ action.aCustomer.customerName + " - "
					+ action.aCustomer.customerCode + " ");
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			content.setSpan(
					new ForegroundColorSpan(ImageUtil
							.getColor(R.color.COLOR_LINK)), 0,
					content.length(), 0);
			setStatusVisible(content, View.VISIBLE);
			tvStatus.setOnClickListener(this);
		} else {
			if (action != null && action.objectType.equals("0")
					&& StringUtil.isNullOrEmpty(action.endTime)) {
				// initMenuVisitCustomer(action.aCustomer.customerCode,
				// action.aCustomer.customerName);
				setStatusVisible(StringUtil.getString(R.string.TEXT_VISITTING_1) + " : "
						+ action.aCustomer.customerName + " - "
						+ action.aCustomer.customerCode + " ",
						View.VISIBLE);
			} else {
				setStatusVisible("", View.GONE);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.DELETE_DRAFT_ORDER: {
			draftOrderId = "";
			showTitleDraftOrder();
			break;
		}
		case ActionEventConstant.RE_LOGIN:
			UserDTO userDTO = (UserDTO) modelEvent.getModelData();
			GlobalInfo.getInstance().getProfile().setUserData(userDTO);

			if (!StringUtil.isNullOrEmpty(userDTO.getServerDate())) {
				//cap nhat thoi gian dung sau khi relogin
				DateUtils.updateRightTime(userDTO.getServerDate());
			}

			// kiem tra thoi gian hop le? sau khi relogin
			if (userDTO != null
					&& (userDTO.getAppVersion() != null || userDTO.getDbVersion() != null)) {
				GlobalUtil.showDialogConfirm(this,
						StringUtil.getString(R.string.ERR_UPGRADE_RELOGIN),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
						ACTION_SHOW_LOGIN, null, -1, null);
			}else if (userDTO != null
					&& !DateUtils.checkTimeToShowLogin(userDTO.getServerDate())) {
				GlobalUtil.showDialogConfirm(this,
						StringUtil.getString(R.string.TEXT_CONFIRM_RELOGIN),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
						ACTION_SHOW_LOGIN, null, -1, null);
			} else if (actionBeforeReLogin != null
					&& actionBeforeReLogin.action != ActionEventConstant.RE_LOGIN) {
				actionBeforeReLogin.controller
						.handleViewEvent(actionBeforeReLogin);
			}
			blReLogin = false;
			if (actionBeforeReLogin != null
					&& actionBeforeReLogin.action == ActionEventConstant.RE_LOGIN) {
				ServerLogger.sendLog("LOGIN", "ReLogin vo tan",
						TabletActionLogDTO.LOG_CLIENT);
			}
			break;
		case ActionEventConstant.ACTION_SYN_SYNDATA: {
			SynDataDTO synDataDTO = (SynDataDTO) modelEvent.getModelData();
			if (SynDataDTO.UPDATE_TO_DATE.equals(synDataDTO.getState())) {
//				GlobalInfo.getInstance().setFirstSynData(DateUtils.formatDate(new Date(), DateUtils.DATE_FORMAT_DATE));
				if (!TransactionProcessManager.getInstance().isStarting()) {
					TransactionProcessManager.getInstance().startChecking(
							TransactionProcessManager.SYNC_NORMAL);
				}
				if (modelEvent.getActionEvent().tag == REQUEST_SYNC_FROM_USER) {
					closeProgressDialog();
				} else if (modelEvent.getActionEvent().tag == REQUEST_SYNC_FROM_INACTIVE_APP) {
					// hien thi ds don hang loi
					requestGetAllOrderFail();
				}
				// update cac tham so dinh nghia tren server
				getAppDefineConstant();
				//change display cskh
				//displayCskhInfoTitle();
				GlobalInfo.getInstance().getNotifyOrderReturnInfo().isSyncDataFromServer = false;
				// refresh view
				generateOrgTempData();
				//cap nhat 99 trong truong hop toi up_to_date
				updateProgressEndPercentDialog();
				closeProgressPercentDialog();
				endTimeKPI = Calendar.getInstance();
				requestInsertLogKPI(HashMapKPI.GLOBAL_SYN_DATA);
				sendBroadcast(ActionEventConstant.NOTIFY_REFRESH_VIEW, new Bundle());
			} else if (SynDataDTO.CONTINUE.equals(synDataDTO.getState())) {
//				GlobalInfo.getInstance().setFirstSynData(DateUtils.formatDate(new Date(), DateUtils.DATE_FORMAT_DATE));
				// fixed truong hop nhan phim back nhung van con dong bo tiep
				if (!isCancelUpdateData) {
					requestSynData(modelEvent.getActionEvent().tag);

					int percent = (int) (100 - ((double) (synDataDTO
							.getMaxDBLogId() - synDataDTO.getLastLogId_update()) / (double) (synDataDTO
							.getMaxDBLogId() - beginLogId)) * 100);
					updateProgressPercentDialog(percent);
				}
			} else if (SynDataDTO.RESET.equals(synDataDTO.getState())) {
				closeProgressDialog();
				GlobalUtil.showDialogConfirm(this, StringUtil
						.getString(R.string.TEXT_CONFIRM_RELOGIN_TO_RESET_DB),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
						ACTION_OK_RESET_DB, StringUtil
								.getString(R.string.TEXT_BUTTON_DENY),
						ACTION_CANCEL_RESET_DB, null);
			}
			break;
		}case ActionEventConstant.ACTION_SYN_PROMOTION: {
			long programeId = (Long) modelEvent.getActionEvent().userData;
			GlobalInfo.getInstance().getLstWrongProgrameId().remove(programeId);
			//request chuong trinh tiep theo neu co
			if(GlobalInfo.getInstance().getLstWrongProgrameId().size() > 0){
				requestSynProgrameData(GlobalInfo.getInstance().getLstWrongProgrameId().get(0));
			}else{
				closeProgressDialog();
				if (!TransactionProcessManager.getInstance().isStarting()) {
					TransactionProcessManager.getInstance().startChecking(
							TransactionProcessManager.SYNC_NORMAL);
				}
				((GlobalBaseActivity) GlobalInfo.getInstance()
						.getActivityContext()).showToastMessage(StringUtil.getString(R.string.PROMOTION_SYN_SUCCESS));
			}
			break;
		}
		case ActionEventConstant.INSERT_ACTION_LOG:
			isInsertingActionLogVisit = false;
			break;
		case ActionEventConstant.START_INSERT_ACTION_LOG:
			// update last customer visit
			isInsertingActionLogVisit = false;
			//luu lai (cap nhat) id action log dang ghe tham
			GlobalInfo.getInstance().getProfile().save();
			break;
		case ActionEventConstant.NO_SUCCESS_ORDER_LIST:
		case ActionEventConstant.ACTION_GET_ALL_ORDER_FAIL:
			closeProgressDialog();
			NoSuccessSaleOrderDto dto = (NoSuccessSaleOrderDto) modelEvent
					.getModelData();
			// luu ds don hang
			if (dto != null && dto.itemList != null) {
				if (listOrderId != null) {
					listOrderId.clear();
					// GlobalInfo.getInstance().notifyOrderReturnInfo.listOrderUpdateId.clear();
					for (int i = 0, size = dto.itemList.size(); i < size; i++) {
						SaleOrderDTO saleDTO = dto.itemList.get(i).saleOrder;
						listOrderId.add(saleDTO.saleOrderId);
					}
				}
			}
			if (dto != null && dto.itemList.size() > 0) {
				showNoSuccessOrderDialog(dto);
			} else {
				if (listOrderId != null && listOrderId.size() <= 0) {
					showWarning(false);
				}
			}

			break;
		case ActionEventConstant.GET_ORDER_DRAFT: {
			String orderId = (String) modelEvent.getModelData();
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
			sendBroadcast(ActionEventConstant.NOTIFY_ORDER_DRAFT, bundle);
			break;
		}
		case ActionEventConstant.GET_ORDER_IN_LOG: {
			NotifyOrderDTO notifyDTO = (NotifyOrderDTO) modelEvent
					.getModelData();
			sendBroadcaseNotifyOrder(notifyDTO);
			break;
		}
		case ActionEventConstant.UPDATE_ACTION_LOG: {
			sendBroadcast(ActionEventConstant.NOTIFY_REFRESH_VIEW, new Bundle());
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			int errCode = modelEvent.getModelCode();
			if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
				// thanh cong thi ko can lam j ca
			} else {
				// ghi vao log de thuc hien lai
				if (e.userData != null){
					StaffPositionLogDTO staffPos = (StaffPositionLogDTO)e.userData;
					if (staffPos != null){
						GlobalInfo.getInstance().addPosition(staffPos);
					}
				}
			}
			break;
		case ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE:
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			Editor prefsPrivateEditor = sharedPreferences.edit();
			prefsPrivateEditor.putString(DATE_CLEAR_OLD_DATA, DateUtils
					.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE));
			prefsPrivateEditor.commit();

			closeProgressDialog();
			LogFile.logToFile(DateUtils.now() + " || Hoan thanh thuc hien xoa du lieu cu");
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			break;
		case ActionEventConstant.GET_EQUIPMENT_LOST_REPORT_ERROR: {
			// hien popup ds cac thiet bi
			ArrayList<EquipInventoryDTO> listEquipInventory = (ArrayList<EquipInventoryDTO>) modelEvent.getModelData();
			showPopUpEquipError(listEquipInventory);
			break;
		}
		case ActionEventConstant.GET_APPARAM:{
			ArrayList<ApParamDTO> result = (ArrayList<ApParamDTO>) modelEvent.getModelData();
			setAppDefineConstant(result);
			break;
		}
		case ActionEventConstant.GET_SHOP_PARAMs:{
			ArrayList<ShopParamDTO> result = (ArrayList<ShopParamDTO>) modelEvent.getModelData();
			setShopDefineConstant(result);
			break;
		}
		case ActionEventConstant.GET_ATTENDANCE_TIME_DEFINE:
			List<ShopParamDTO> paramList = (List<ShopParamDTO>) modelEvent.getModelData();
			readAttendaceTime(paramList);
			break;
		case ActionEventConstant.GET_DATA_FROM_ORG_ACCESS:
			break;
		default:
			break;
		}
	}

	/**
	 * @author: duongdt3
	 * @since: 18:36:40 4 Apr 2015
	 * @return: void
	 * @throws:
	 * @param result
	 */
	private void setShopDefineConstant(ArrayList<ShopParamDTO> result) {
		if (result != null && !result.isEmpty()) {
			int size = result.size();
			for (int i = 0; i < size; i++) {
				ShopParamDTO item = result.get(i);
				MyLog.d("setShopDefineConstant", item.code + " " + item.value + " " + item.status);
				try {
					if ("SYS_MODIFY_PRICE".equals(item.code)) {
						GlobalInfo.getInstance().setSysModifyPrice(Integer.valueOf(item.value), item.status);
					} else if ("SYS_SHOW_PRICE".equals(item.code)) {
						GlobalInfo.getInstance().setSysShowPrice(Integer.valueOf(item.value), item.status);
					} else if ("SYS_MAXDAY_RETURN".equals(item.code)) {
						GlobalInfo.getInstance().setSysMaxdayReturn(Integer.valueOf(item.value), item.status);
					} else if ("SYS_MAXDAY_APPROVE".equals(item.code)) {
						GlobalInfo.getInstance().setSysMaxdayApprove(Integer.valueOf(item.value), item.status);
					} else if ("SYS_RETURN_DATE".equals(item.code)) {
						GlobalInfo.getInstance().setSysReturnDate(Integer.valueOf(item.value), item.status);
					} else if ("SYS_NEED_SAVE_REMAIN".equals(item.code)) {
						GlobalInfo.getInstance().setSysNeedSaveRemain(Integer.valueOf(item.value), item.status);
					} else if ("SYS_NEED_TAKE_PHOTO_EQUIPMENT".equals(item.code)) {
						GlobalInfo.getInstance().setNeedTakePhotoEquipment(Integer.valueOf(item.value), item.status);
					}
				} catch (Exception e) {
					MyLog.e("setShopDefineConstant", e);
				}
			}
		}
	}

	/**
	 * show dialog DS don hang chua chuyen thanh cong
	 * @author : TamPQ since : 1.0
	 */
	private void showNoSuccessOrderDialog(NoSuccessSaleOrderDto dto) {
		this.closeDialog(alertRemindDialog);
		Builder build = new AlertDialog.Builder(this, R.style.CustomDialogTheme);

		NoSuccessSaleOrderView view = new NoSuccessSaleOrderView(this, dto);
		build.setView(view.viewLayout);
		alertRemindDialog = build.create();
		Window window = alertRemindDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		if (!alertRemindDialog.isShowing()) {
			alertRemindDialog.show();
		}
	}

	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		closeProgressDialog();
		MyLog.d("BaseAcitivty", DateUtils.now() + "Action		: "
				+ modelEvent.getActionEvent().action);
		MyLog.d("BaseAcitivty", DateUtils.now() + "Error code	: "
				+ modelEvent.getModelCode());
		MyLog.d("BaseAcitivty", DateUtils.now() + "Message		: "
				+ modelEvent.getModelMessage());
		if (modelEvent.getActionEvent().action == ActionEventConstant.START_INSERT_ACTION_LOG
				|| modelEvent.getActionEvent().action == ActionEventConstant.INSERT_ACTION_LOG) {
			isInsertingActionLogVisit = false;
		}
		// neu luong dong bo loi thi kick lai luong dong bo dinh ky 3 phut
		if (modelEvent.getActionEvent().action == ActionEventConstant.ACTION_SYN_SYNDATA) {
			if (!TransactionProcessManager.getInstance().isStarting()) {
				TransactionProcessManager.getInstance().startChecking(
						TransactionProcessManager.SYNC_NORMAL);
			}
			closeProgressPercentDialog();
		}

		// neu luong dong bo KM loi thi kick lai luong dong bo dinh ky 3 phut
		if (modelEvent.getActionEvent().action == ActionEventConstant.ACTION_SYN_PROMOTION
				&& modelEvent.getModelCode() != ErrorConstants.ERROR_SESSION_RESET) {
			((GlobalBaseActivity) GlobalInfo.getInstance()
					.getActivityContext()).showToastMessage(StringUtil.getString(R.string.PROMOTION_SYN_FAIL));
			if (!TransactionProcessManager.getInstance().isStarting()) {
				TransactionProcessManager.getInstance().startChecking(
						TransactionProcessManager.SYNC_NORMAL);
			}
		}

		//thoat app sau khi clear data old (o hanh dong thoat ung dung)
		if (modelEvent.getActionEvent().action == ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE) {
			closeProgressDialog();
			LogFile.logToFile(DateUtils.now() + " || Hoan thanh thuc hien xoa du lieu cu");
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
		}

		// gap loi (network, offline,...) luu vao log de gui len lai
		if (modelEvent.getActionEvent().action == ActionEventConstant.ACTION_UPDATE_POSITION) {
			// ghi vao log de thuc hien lai
			if (modelEvent.getActionEvent().userData != null) {
				StaffPositionLogDTO staffPos = (StaffPositionLogDTO) modelEvent
						.getActionEvent().userData;
				if (staffPos != null) {
					GlobalInfo.getInstance().addPosition(staffPos);
				}
			}
		}

		switch (modelEvent.getModelCode()) {
			case ErrorConstants.ERROR_COMMON:
				if (modelEvent.getActionEvent().action != ActionEventConstant.ACTION_UPDATE_POSITION) {

					if (modelEvent.getActionEvent().action != ActionEventConstant.ACTION_SYN_SYNDATA) {
						showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
					}
					// send log server
					sendLogModel(modelEvent);
				}
				break;
			case ErrorConstants.ERROR_NO_CONNECTION:
				showDialog(modelEvent.getModelMessage());
				break;
			case ErrorConstants.ERROR_EXPIRED_TIMESTAMP:
				/*
				 * BangHN: Loi chung thuc thoi gian do tomcat bao Tam thoi bo vi
				 * da check bang system auto-time
				 */
				// GlobalUtil.showDialogSettingTime();
				break;
			case ErrorConstants.ERROR_SESSION_RESET: // error when session time
													 // out.
				// re-login and re-request
				// kiem tra da dang nhap thanh cong chua
				if (GlobalInfo.getInstance().getProfile().getUserData().getLoginState() == UserDTO.LOGIN_SUCCESS) {
					ServerLogger.sendLog("RELOGIN", "ERROR_SESSION_RESET - State Success", TabletActionLogDTO.LOG_CLIENT);
					// da login & mat session
					if (!blReLogin) {
						if (modelEvent.getActionEvent().action != ActionEventConstant.RE_LOGIN) {
							actionBeforeReLogin = modelEvent.getActionEvent();
						} else {
							ServerLogger.sendLog("RELOGIN", "Loop Non-Stop", TabletActionLogDTO.LOG_CLIENT);
						}
						blReLogin = true;
						// get Oauth thanh cong
						if (GlobalUtil.checkNetworkAccess()) {
							blReLogin = false;
							if (actionBeforeReLogin != null) {
								actionBeforeReLogin.controller.handleViewEvent(actionBeforeReLogin);
							}
						} else {// get Oauth fail
							blReLogin = false;
							modelEvent.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
							modelEvent.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECTION));
							handleErrorModelViewEvent(modelEvent);
						}
					} else {
						blReLogin = false;
						modelEvent.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
						handleErrorModelViewEvent(modelEvent);
					}
				} else {
					// chua login thanh cong
					if (modelEvent != null
							&& modelEvent.getActionEvent() != null) {
						if (modelEvent.getActionEvent().action != ActionEventConstant.RE_LOGIN) {
							numRelogin = 0;
							actionBeforeReLogin = modelEvent.getActionEvent();
						} else {
							numRelogin++;
							ServerLogger.sendLog("RELOGIN", "Loop Non-Stop", TabletActionLogDTO.LOG_CLIENT);
						}
					} else {
						numRelogin++;
					}

					Vector<String> vt = new Vector<String>();
					vt.add(IntentConstants.INTENT_USER_NAME);
					vt.add(GlobalInfo.getInstance().getProfile().getUserData().getUserName());

					vt.add(IntentConstants.INTENT_LOGIN_PASSWORD);
					vt.add(GlobalInfo.getInstance().getProfile().getUserData().getPass());
					vt.add(IntentConstants.INTENT_LOGIN_IS_REMEMBER);
					vt.add("true");

					vt.add(IntentConstants.INTENT_LOGIN_PHONE_MODEL);
					vt.add(GlobalInfo.getInstance().PHONE_MODEL);

					vt.add(IntentConstants.INTENT_IMEI);
					vt.add(GlobalInfo.getInstance().getDeviceIMEI());

					if (!StringUtil.isNullOrEmpty(StringUtil.getSimSerialNumber())) {
						vt.add(IntentConstants.INTENT_SIM_SERIAL);
						vt.add(StringUtil.getSimSerialNumber());
					}

					vt.add(IntentConstants.INTENT_VERSION_APP);
					vt.add(GlobalInfo.getInstance().getProfile().getVersionApp());

					vt.add(IntentConstants.INTENT_VERSION_DB);
					vt.add(GlobalInfo.getInstance().getProfile().getVersionDB());

					vt.add(IntentConstants.INTENT_LOGIN_PLATFORM);
					vt.add(GlobalInfo.getInstance().PLATFORM_SDK_STRING);

					ActionEvent e = new ActionEvent();
					e.action = ActionEventConstant.RE_LOGIN;
					e.viewData = vt;
					e.sender = this;
					UserController.getInstance().handleViewEvent(e);
					break;
				}
		}
	}

	/**
	 * Thuc hien lay tat ca don hang chua thuc hien thanh cong: bi loi, chua
	 * goi, tra ve
	 * @author: TruongHN
	 * @param action
	 * @return: void
	 * @throws:
	 */
	public void requestGetAllOrderFail() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.action = ActionEventConstant.ACTION_GET_ALL_ORDER_FAIL;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * request bat dau ghe tham khach hang
	 * @author : BangHN since : 1.0
	 */
	public void requestInsertActionLog(ActionLogDTO action) {
		if (!isInsertingActionLogVisit) {
			isInsertingActionLogVisit = true;
			Bundle bundle = new Bundle();
			action.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			action.staffId = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId();
			action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLatitude();
			action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLongtitude();
			action.endTime = DateUtils.now();
			action.distance = GlobalUtil.getDistance(new LatLng(action.lat,
					action.lng), new LatLng(action.aCustomer.lat,
					action.aCustomer.lng));
			MyLog.i("MyLog", "insert action: startTime - endTime : "
					+ action.startTime + " - " + action.endTime);
			bundle.putSerializable(IntentConstants.INTENT_DATA, action);
			handleViewEvent(bundle, ActionEventConstant.INSERT_ACTION_LOG, SaleController.getInstance());
		}
	}

	/**
	 * request bat dau ghe tham khach hang
	 * @author : BangHN since : 1.0
	 */
	public void requestDeleteActionLog(ActionLogDTO action) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_DATA, action);
		handleViewEvent(bundle, ActionEventConstant.DELETE_ACTION_LOG, SaleController.getInstance());
	}

	/**
	 * request bat dau ghe tham khach hang
	 * @author : TamPQ since : 1.0
	 */
	public void requestStartInsertVisitActionLog(CustomerListItem item) {
		if (!isInsertingActionLogVisit) {
			isInsertingActionLogVisit = true;
			ActionLogDTO visitCustomer = new ActionLogDTO();
			visitCustomer.aCustomer.customerId = item.aCustomer.customerId;
			visitCustomer.aCustomer.customerName = item.aCustomer.customerName;
			visitCustomer.aCustomer.customerCode = item.aCustomer.customerCode;
			visitCustomer.aCustomer.address = item.aCustomer.address;
			visitCustomer.aCustomer.channelTypeId = item.aCustomer.channelTypeId;
			visitCustomer.aCustomer.lat=item.aCustomer.lat;
			visitCustomer.aCustomer.lng=item.aCustomer.lng;
			visitCustomer.aCustomer.shopDTO.distanceOrder=item.distanceOrder;
			visitCustomer.startTime = DateUtils.now();
			visitCustomer.isOr = item.isOr;
			visitCustomer.routingId = item.routingId;
			visitCustomer.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			visitCustomer.staffId = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId();
			visitCustomer.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLastLatitude();
			visitCustomer.lng = GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLastLongtitude();
			// visitCustomer.objectId = "0";
			visitCustomer.objectType = "0";
			// visitCustomer.endTime = DateUtils.now();
			visitCustomer.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			visitCustomer.distance = GlobalUtil.getDistance(new LatLng(visitCustomer.lat,
					visitCustomer.lng), new LatLng(visitCustomer.aCustomer.lat,
							visitCustomer.aCustomer.lng));
			GlobalInfo.getInstance().getProfile()
					.setActionLogVisitCustomer(visitCustomer);

			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_DATA, visitCustomer);
			handleViewEvent(b, ActionEventConstant.START_INSERT_ACTION_LOG, SaleController.getInstance());
		}
	}

	/**
	 * request ket thuc ghe tham
	 * @author : TamPQ since : 1.0
	 */
	public void requestUpdateActionLog(String objType, String objId,
			CustomerListItem customer, Object sender) {
		endVisitCustomerBar();
		ActionLogDTO al = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();

		// kiem tra khong cach truoc khi ket thuc ghe tham
		// kc cho phep: GlobalInfo.getInstance().getDistanceAllowFinish()
		// kc < khcp endTime = now
		// kc > khcp endTime = tg cua log cuoi cung trong kccp

		if (al != null && StringUtil.isNullOrEmpty(al.endTime)) {
			al.endTime = DateUtils.getVisitEndTime(al.aCustomer);
			al.objectType = objType;
			al.objectId = objId;

			GlobalInfo.getInstance().getProfile().setActionLogVisitCustomer(al);

			ActionEvent e = new ActionEvent();
			e.viewData = al;
			e.sender = sender;
			e.userData = customer;
			e.action = ActionEventConstant.UPDATE_ACTION_LOG;
			e.isNeedCheckTimeServer = false;
			SaleController.getInstance().handleViewEvent(e);

			//reset lai danh sach vi tri nhan vien
			GlobalInfo.getInstance().clearStaffPosition();

			// Xoa don hang draft
			ActionEvent ev = new ActionEvent();
			Bundle bundle = new Bundle();

			if (!StringUtil.isNullOrEmpty(draftOrderId)) {
				SaleOrderViewDTO dto = new SaleOrderViewDTO();
				dto.saleOrder.saleOrderId = Long.parseLong(draftOrderId);
				bundle.putString(IntentConstants.INTENT_ORDER_ID, draftOrderId);
				ev.viewData = bundle;
				ev.sender = this;
				ev.action = ActionEventConstant.DELETE_DRAFT_ORDER;
				ev.isNeedCheckTimeServer = false;
				SaleController.getInstance().handleViewEvent(ev);
			}
		}
	}

	 /**
	 * Ket thuc action log
	 * @author: Tuanlt11
	 * @param objType
	 * @param objId
	 * @param sender
	 * @return: void
	 * @throws:
	*/
	public void requestUpdateActionLog(String objType, String objId, Object sender) {
		endVisitCustomerBar();

		// kiem tra khong cach truoc khi ket thuc ghe tham
		// kc cho phep: GlobalInfo.getInstance().getDistanceAllowFinish()
		// kc < khcp endTime = now
		// kc > khcp endTime = tg cua log cuoi cung trong kccp
		ActionLogDTO al = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if (al != null && StringUtil.isNullOrEmpty(al.endTime)) {
			al.endTime = DateUtils.getVisitEndTime(al.aCustomer);
			al.objectType = objType;
			al.objectId = objId;

			ActionEvent e = new ActionEvent();
			e.viewData = al;
			e.sender = sender;
			e.action = ActionEventConstant.UPDATE_ACTION_LOG;
			e.isNeedCheckTimeServer = false;
			SaleController.getInstance().handleViewEvent(e);

			//reset lai danh sach vi tri nhan vien
			GlobalInfo.getInstance().clearStaffPosition();

		}
	}

	/**
	 * ghi du lieu ghe tham vao action log
	 *
	 * @author: TruongHN
	 * @param startTime
	 * @param objectType
	 * @param objectId
	 * @param customerId
	 * @param routingId
	 * @return: void
	 * @throws:
	 */
	public void requestInserActionLog(String startTime, String objectType,
			String objectId, long customerId, String isVisitPlan, double cusLat, double cusLng, long routingId) {
		if (!isInsertingActionLogVisit) {
			isInsertingActionLogVisit = true;
			ActionEvent e = new ActionEvent();
			ActionLogDTO log = new ActionLogDTO();
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			log.aCustomer.customerId = customerId;
			log.aCustomer.lat = cusLat;
			log.aCustomer.lng = cusLng;
			log.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			log.endTime = DateUtils.now();
			log.startTime = startTime;
			log.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLatitude();
			log.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLongtitude();
			log.objectId = objectId;
			log.objectType = objectType;
			log.isOr = Integer.parseInt(isVisitPlan);
			log.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			log.routingId = routingId;
			log.distance = GlobalUtil.getDistance(new LatLng(log.lat,
					log.lng), new LatLng(log.aCustomer.lat,
							log.aCustomer.lng));

			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_DATA, log);
			e.viewData = b;
			e.sender = this;
			e.action = ActionEventConstant.INSERT_ACTION_LOG;
			e.isNeedCheckTimeServer = false;
			SaleController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * Lay cac don hang trong log
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void getOrderInLogForNotify() {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();

		e.viewData = bundle;
		e.sender = this;
		e.action = ActionEventConstant.GET_ORDER_IN_LOG;

		SaleController.getInstance().handleViewEvent(e);
	}

	@Override
	protected void onPause() {
		super.onPause();
		// bat dau tinh thoi gian idle
		GlobalInfo.getInstance().setTimeStartIdleStatus(
				System.currentTimeMillis());
		// van hoat dong gps khi home, pause chuong trinh
		GlobalInfo.getInstance().setAppActive(false);
		// PositionManager.getInstance().stop();
		System.gc();
	}

	@Override
	public void onLowMemory() {
		System.gc();
		super.onLowMemory();
	}

	@Override
	protected void onResume() {
		GlobalInfo.getInstance().setActivityContext(this);
		// neu hien thi cac dialog khac thi ko hien thi xoa app
		boolean isExistedDialog = false;
		//check mock location
		checkMockLocation();

		// check datetime auto update
		if (!GlobalUtil.isRightSettingTime()) {
			/*GlobalUtil.showDialogSettingTimeAutomatic();
			isExistedDialog = true;*/
		} else {
			// ngoai tru login view ko check time.
			if (!(this instanceof LoginView)) {
				int wrongTime = DateUtils.checkTabletRightTimeWorking();
				if (wrongTime != DateUtils.RIGHT_TIME) {
					// neu chua tung online trong ngay
					GlobalUtil.showDialogCheckWrongTime(wrongTime);
					isExistedDialog = true;
				}
			}
		}

		// kiem tra thoi gian cham cong & luong dinh vi(Neu khong trong thoi
		// gian cham cong
		// va dinh ki = 2 phut thi set lai la 5 phut
		if (!DateUtils.isInAttendaceTime()
				&& PositionManager.getCurrentTimePeriod() == GlobalInfo
						.getInstance().getTimeTrigPositionAttendance()) {
			PositionManager.getInstance().stop();
			PositionManager.getInstance().start();
		}

		// start service dinh vi location client
		if (isGooglePlayServiceAvailable()) {
			startFusedLocationService();
		} else if(this instanceof LoginView){
			showGooglePlayServicesError();
		}

		try {
			PackageInfo packageInfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			GlobalInfo.getInstance().isDebugMode = ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
		} catch (Exception ex) {
			GlobalInfo.getInstance().isDebugMode = false;
		}
		GlobalInfo.getInstance().setLastActivity(this);
		GlobalInfo.getInstance().setAppActive(true);
		if (!PositionManager.getInstance().getIsStart()
				&& !(this instanceof LoginView)) {
			if (GlobalInfo.getInstance().getLoc() != null
					&& GlobalInfo.getInstance().getLoc().getTime() < System
							.currentTimeMillis() - Locating.MAX_TIME_RESET) {
				GlobalInfo.getInstance().setLoc(null);
				GlobalInfo.getInstance().getProfile().getMyGPSInfo()
						.setLongtitude(-1);
				GlobalInfo.getInstance().getProfile().getMyGPSInfo()
						.setLattitude(-1);
				GlobalInfo.getInstance().getProfile().save();
			}
			PositionManager.getInstance().start();
		}

		// check allow mock location neu truong hop tu setting quay ve ma khong
		// tick bo chon
		if (GlobalUtil.isMockLocation()) {
			GlobalUtil.getInstance().checkAllowMockLocation();
			isExistedDialog = true;
		}
		// kiem tra co always Finish activities enable?
		GlobalUtil.getInstance().checkStatusAlwaysFinishActivities();
		// hien thi dialog ds ung dung can xoa
		// cac dialog khac chua hien thi thi moi hien thi dialog xoa ung dung
		int checkAccessApp = GlobalInfo.getInstance().getCheckAppInstall();
		if (!isExistedDialog && (checkAccessApp == UserDTO.CHECK_INSTALL)) {
			if (!(this instanceof LoginView)) {
				initBlackListApp();
				showDialogAppUninstall(blackApps);
			}
		}
		// kiem tra neu services chua start thi start service kiem tra chan network
		if (GlobalInfo.getInstance().getCheckNetwork() != UserDTO.UNCHECK_ALL_ACCESS) {
			AccessInternetService.unlockAppPrevent(false);
			ServiceUtil.startServiceIfNotRunning(AccessInternetService.class);
		}else{
			ServiceUtil.stopService(AccessInternetService.class);
		}
		
		//requestAdmin();
		//hien thi thong bao dinh vi, thong bao so data can goi
		super.onResume();
	}

	private void checkMockLocation() {
		//xu ly check mock location khi resume
		List<AppInfo> lstAppMockEnable = GlobalInfo.getInstance()
				.isVaildateMockLocation() ? GlobalUtil.getListMockApps(this, true) : null;
		// khong con ung dung nao co quyen mock thi cho phep nhan request vi tri
		if ((lstAppMockEnable == null || lstAppMockEnable.isEmpty())
				&& !GlobalUtil.isMockLocation()) {
			// neu truoc fullDate co gia lap vi tri
			if (GlobalBaseActivity.getInstance().isFakeGPS()) {
				reloadListKnownLocation();
				GlobalBaseActivity.getInstance().setFakeGPS(false);
			}
		} else {
			GlobalBaseActivity.getInstance().setFakeGPS(true);

			// chi yeu cau go cai dat + sendlog khi o man hinh khac LoginView
			if (!(this instanceof LoginView)
					&& (lstAppMockEnable != null && !lstAppMockEnable.isEmpty())) {
				String contentError = StringUtil
						.getString(R.string.TEXT_DETECT_GPS_FAKE_APPS) + " ";
				String content = StringUtil
						.getString(R.string.TEXT_DETECT_GPS_FAKE_APPS_FORCE);
				for (int i = 0, size = lstAppMockEnable.size(); i < size; i++) {
					AppInfo app = lstAppMockEnable.get(i);
					if (i > 0) {
						contentError += ",";
						content += ",";
					} else {
						content += "\n";
					}
					content += app.appName;
					contentError += app.appName + "(" + app.packageName + ")";
					// go cai dat app co mock location
					GlobalUtil.uninstallApp(this, app.packageName);
				}
				showToastLongMessage(content);
				ServerLogger.sendLog(contentError, TabletActionLogDTO.LOG_CLIENT);
			}
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		boolean isNotifyInactive = intent.getExtras().getBoolean(
				IntentConstants.INTENT_NOTIFY_INACTIVE);
		if (isNotifyInactive) {
			// request dong bo du lieu
			showProgressDialog(StringUtil.getString(R.string.updating));
			TransactionProcessManager.getInstance().startChecking(
					TransactionProcessManager.SYNC_FROM_UPDATE);
			this.synType = REQUEST_SYNC_FROM_INACTIVE_APP;
			// requestSynData(REQUEST_SYNC_FROM_INACTIVE_APP);
		}
	}

	@Override
	public void onDialogListener(int eventType, int eventCode, Object data) {
		// TODO Auto-generated method stub
		if (eventType == RESEND_TYPE) {
			ActionEvent eOld = (ActionEvent) data;
			UserController.getInstance().handleViewEvent(eOld);
		}
	}

	public void showProgressDialog(String content) {
		showProgressDialog(content, true);
	}

	public void showLoadingDialog() {
		showProgressDialog(StringUtil.getString(R.string.loading), true);
	}

	public void showToastMessage(String message) {
		GlobalUtil.showToast(message);
	}

	public void showToastLongMessage(String message) {
		GlobalUtil.showToastLong(message);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_ALLOW_MOCK_LOCATION_OK:
			GlobalUtil.startActivityOtherApp(this,
					new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
			break;
		case ACTION_ALLOW_MOCK_LOCATION_CANCEL:
			GlobalUtil.showToastLong(StringUtil.getString(R.string.TEXT_NO_TICE));
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			finish();
			break;

		case ACTION_SHOW_LOGIN:
		case ACTION_OK_RESET_DB:
			closeProgressDialog();
			// send broadcast ra login
			sendBroadcast(ActionEventConstant.ACTION_FINISH_AND_SHOW_LOGIN,
					new Bundle());
			break;
		case ActionEventConstant.ACTION_REFRESH_APP_UNINSTALL:
			sendBroadcast(ActionEventConstant.ACTION_FINISH_AND_SHOW_LOGIN,
					new Bundle());
			break;
		case ACTION_SELECTED_APP_DELETE:
			ApplicationInfo app = (ApplicationInfo)data;
			Uri packageURI = Uri.parse("package:" + app.packageName);
			Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
			GlobalUtil.startActivityOtherApp(this, uninstallIntent);
			break;
		default:
			break;
		}
	}

	/*
	 * @author: BanghN Request sync datao
	 */
	protected void requestSynData(int notifyInactive) {
		try {
			String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
			isCancelUpdateData = false;
			Vector<String> vt = new Vector<String>();
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String lastLogId = sharedPreferences.getString(roleShop + LAST_LOG_ID, "0");

			vt.add(IntentConstants.INTENT_SYN_STAFFID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getStaffId()));
			vt.add(IntentConstants.INTENT_SYN_INHERITID);
			vt.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
			vt.add(IntentConstants.INTENT_SHOP_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			vt.add(IntentConstants.INTENT_LAST_LOG_ID);
			vt.add(lastLogId);// "21246863");
			vt.add(IntentConstants.INTENT_ROLE_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getRoleId());

			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.ACTION_SYN_SYNDATA;
			e.isBlockRequest = true;
			e.tag = notifyInactive;
			e.viewData = vt;
			e.userData = lastLogId;
			e.sender = this;

			SynDataController.getInstance().handleViewEvent(e);
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
	}

	/**
	 * Lay thoi gian hien tai tren server
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void getCurrentDateTimeServer(ActionEvent actionEventBeforeGetTime) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putBoolean(IntentConstants.INTENT_IS_SYNC, false);
		e.viewData = bundle;
		e.sender = this;
		e.userData = actionEventBeforeGetTime;
		e.action = ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER;
		UserController.getInstance().handleViewEvent(e);
	}

	/**
	 * Notify cho ds don hang & icon notify cac don hang bi loi
	 * @author: Nguyen Thanh Dung
	 * @param list
	 * @return: void
	 * @throws:
	 */
	public void sendBroadcaseNotifyOrder(NotifyOrderDTO notifyDTO) {
		if (notifyDTO != null) {
			// hien thi icon don hang chua goi
			ArrayList<String> listOrder = new ArrayList<String>();
			for (int i = 0, size = notifyDTO.listOrderInLog.size(); i < size; i++) {
				if (LogDTO.STATE_NEW
						.equals(notifyDTO.listOrderInLog.get(i).state)
						|| LogDTO.STATE_FAIL.equals(notifyDTO.listOrderInLog
								.get(i).state)
						|| LogDTO.STATE_INVALID_TIME
								.equals(notifyDTO.listOrderInLog.get(i).state)
						|| LogDTO.STATE_UNIQUE_CONTRAINTS
								.equals(notifyDTO.listOrderInLog.get(i).state)) {
					listOrder.add(notifyDTO.listOrderInLog.get(i).tableId);
				}
			}

			boolean hasOrderFail = listOrder.size() > 0 ? true : false;
			if (!hasOrderFail) {
				hasOrderFail = notifyDTO.numOrderReturnNPP > 0 ? true : false;
			}
			GlobalInfo.getInstance().getNotifyOrderReturnInfo().hasOrderFail = hasOrderFail;
			sendBroadcast(ActionEventConstant.NOTIFY_ORDER_STATE, new Bundle());
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btClose) {
			if (alertRemindDialog != null && alertRemindDialog.isShowing()) {
				alertRemindDialog.dismiss();
			}
		} else if (v == tvTitle) {
//			OrderView view = (OrderView) this.findFragmentByTag(GlobalUtil.getTag(OrderView.class));
//			if(view == null) {
//				gotoChangePassView();
//			}
		} else if (v == btPass) {
			if(alertProductDetail != null){
				alertProductDetail.dismiss();
				showMenuEquip(false);
			}
		} else if (v == tvStatus) {
			final ActionLogDTO actionLog = GlobalInfo.getInstance()
					.getProfile().getActionLogVisitCustomer();
			boolean isRemainCurrentView = false;
			if (GlobalUtil.getTag(RemainProductView.class).equals(GlobalInfo.getInstance()
					.getCurrentTag())) {
				isRemainCurrentView = true;
			}
			gotoViewOrder(draftOrderId, actionLog.aCustomer,
					isRemainCurrentView);
		}
	}

	/**
	 * Den man hinh tao don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoViewOrder(String orderId, CustomerDTO cus, boolean hasData) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(cus.customerId));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, cus.customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				cus.address);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				cus.getCustomerName());
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				cus.getCustomerTypeId());
		bundle.putBoolean(IntentConstants.INTENT_HAS_REMAIN_PRODUCT, hasData);
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_EDIT);
		e.viewData = bundle;
		e.action = ActionEventConstant.GO_TO_ORDER_VIEW;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * go to ChangePassView
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void gotoChangePassView() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.CHANGE_PASS;
		UserController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * add cac request dang xu ly vao mang request
	 * @author: BangHN
	 * @param req
	 *            , isBlock
	 * @return: void
	 * @throws:
	 */
	public void addProcessingRequest(HTTPRequest req, boolean isBlock) {
		if (isBlock) {
			blockReqs.add(req);
		} else {
			unblockReqs.add(req);
		}
	}

	/**
	 * remove all processing request
	 *
	 * @author: banghn
	 * @return: void
	 * @throws:
	 */
	public void removeAllProcessingRequest() {
		cancelRequest(blockReqs);
		cancelRequest(unblockReqs);
	}

	/**
	 * remove processing request
	 *
	 * @author: banghn
	 * @param req
	 * @return: void
	 * @throws:
	 */
	public void removeProcessingRequest(HTTPRequest req, boolean isBlock) {
		if (isBlock) {
			cancelRequest(blockReqs, req);
		} else {
			cancelRequest(unblockReqs, req);
		}
	}

	/**
	 * Cancle request mot array request
	 *
	 * @author banghn
	 * @param arrReq
	 */
	private void cancelRequest(ArrayList<HTTPRequest> arrReq) {
		HTTPRequest req = null;
		for (int i = 0, n = arrReq.size(); i < n; i++) {
			req = arrReq.get(i);
			req.setAlive(false);
		}
		arrReq.clear();
	}

	/**
	 * cancle mot request
	 *
	 * @author banghn
	 * @param arrReq
	 * @param req
	 */
	private void cancelRequest(ArrayList<HTTPRequest> arrReq, HTTPRequest req) {
		HTTPRequest curReq = null;
		for (int i = 0, n = arrReq.size(); i < n; i++) {
			curReq = arrReq.get(i);
			if (curReq == req) {
				arrReq.remove(i);
				req.setAlive(false);
				break;
			}
		}
		arrReq.clear();
	}

	/**
	 * Kiem tra co ton tai request dang xu ly hay khong
	 *
	 * @author: TruongHN
	 * @param reqAction
	 * @return: boolean
	 * @throws:
	 */
	public boolean checkExistRequestProcessing(int reqAction) {
		boolean res = false;
		HTTPRequest curReq = null;
		for (int i = 0, n = blockReqs.size(); i < n; i++) {
			curReq = blockReqs.get(i);
			if (curReq.isAlive() && curReq.getAction() == reqAction) {
				res = true;
				break;
			}
		}
		return res;
	}

	public void onCancel(DialogInterface dialog) {
		// dang hien thi progressDialog => bam nut back
		if (dialog == progressDlg) {
			cancelRequest(this.blockReqs);
			isCancelUpdateData = true;
		}
	}

	/**
	 * Hien thi thong bao khi chuong trinh Home
	 *
	 * @author: TruongHN
	 * @param action
	 * @param bundle
	 * @return: void
	 * @throws:
	 */
	public void handleNotifyInActiveView(int action, Bundle bundle) {
		// neu co setting thong bao ra man hinh chinh thi moi hien thi
		if (GlobalInfo.getInstance().getProfile() != null) {
			// rung
			vibrate();
			playSound();
			StatusNotificationHandler handler = GlobalInfo.getInstance()
					.getStatusNotifier();
			if (handler != null) {
				handler.handleAction(action, bundle, this);
			}
		}
	}

	/**
	 * Bat hieu ung rung
	 *
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	public void vibrate() {
		try {
			stopVibrating();
			vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATION_DURATION);
		} catch (Exception ex) {
		}
	}

	/**
	 * Ngung hieu ung rung
	 *
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	protected void stopVibrating() {
		if (vibrator != null) {
			vibrator.cancel();
		}
	}

	/**
	 * bat am thanh
	 *
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	public void playSound() {
		try {
			Uri alert = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			if (alert != null) {
				stopPlayingSound();
				soundPlayer = new MediaPlayer();
				soundPlayer.setOnCompletionListener(this);

				soundPlayer.setDataSource(this, alert);
				final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
				if (audioManager
						.getStreamVolume(AudioManager.STREAM_NOTIFICATION) != 0) {
					soundPlayer
							.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
					soundPlayer.setLooping(false);
					soundPlayer.prepare();
					soundPlayer.start();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (soundPlayer != null) {
				soundPlayer.release();
				soundPlayer = null;
			}
		}
	}

	/**
	 * tat am thanh
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	protected void stopPlayingSound() {
		if (soundPlayer != null && soundPlayer.isPlaying()) {
			soundPlayer.stop();
			soundPlayer.release();
			soundPlayer = null;
		}
	}

	@Override
	public void onCompletion(MediaPlayer paramMediaPlayer) {
		// TODO Auto-generated method stub
		if (soundPlayer != null) {
			soundPlayer.release();
		}
		soundPlayer = null;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// get the current dayInOrder
			if (day == 0 || month == 0 || year == 0) {
				final Calendar c = Calendar.getInstance();
				day = c.get(Calendar.DAY_OF_MONTH);
				month = c.get(Calendar.MONTH);
				year = c.get(Calendar.YEAR);
			}
			datePickerDialog = new DatePickerDialog(this, mDateSetListener,
					year, month, day);
			return datePickerDialog;
		case TIME_DIALOG_ID:
			// get the current dayInOrder
			final Calendar time = Calendar.getInstance();
			int hour = time.get(Calendar.HOUR_OF_DAY);
			int minute = time.get(Calendar.MINUTE);
			return new TimePickerDialog(this, mTimeSetListener, hour, minute,
					true);
		}
		return null;
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {
		int mHour;
		int mMinute;
		switch (id) {
		case DATE_DIALOG_ID:
			// get the current dayInOrder
			if (day == 0 || month == 0 || year == 0) {
				final Calendar c = Calendar.getInstance();
				day = c.get(Calendar.DAY_OF_MONTH);
				month = c.get(Calendar.MONTH);
				year = c.get(Calendar.YEAR);
			}
			datePickerDialog = new DatePickerDialog(this, mDateSetListener,
					year, month, day);
			return datePickerDialog;
		case TIME_DIALOG_ID:
			// get the current dayInOrder
			if (bundle != null) {
				mHour = bundle.getInt(IntentConstants.INTENT_HOUR);
				mMinute = bundle.getInt(IntentConstants.INTENT_MINUTE);
			} else {
				final Calendar time = Calendar.getInstance();
				mHour = time.get(Calendar.HOUR_OF_DAY);
				mMinute = time.get(Calendar.MINUTE);
			}
			return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute,
					true);
		}
		return null;
	}

	/**
	 * DatePickerDialog
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (GlobalBaseActivity.this.currentDatePopupListener != null) {
				GlobalBaseActivity.this.currentDatePopupListener.onDateSet(view, year, monthOfYear, dayOfMonth);
			}
			/*if (fragmentTag.equals(PromotionProgramView.TAG)) {
				PromotionProgramView pro = (PromotionProgramView) getFragmentManager()
						.findFragmentByTag(PromotionProgramView.TAG);
				pro.updateBirtday(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(PostFeedbackView.TAG)) {
				PostFeedbackView v = (PostFeedbackView) getFragmentManager()
						.findFragmentByTag(PostFeedbackView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(OrderView.TAG)) {
				OrderView v = (OrderView) getFragmentManager()
						.findFragmentByTag(OrderView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ListOrderView.TAG)) {
				ListOrderView v = (ListOrderView) getFragmentManager()
						.findFragmentByTag(ListOrderView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ImageListView.TAG)) {
				ImageListView v = (ImageListView) getFragmentManager()
						.findFragmentByTag(ImageListView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ListAlbumUserView.TAG)) {
				ListAlbumUserView v = (ListAlbumUserView) getFragmentManager()
						.findFragmentByTag(ListAlbumUserView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(FollowProblemView.TAG)) {
				FollowProblemView pro = (FollowProblemView) getFragmentManager()
						.findFragmentByTag(FollowProblemView.TAG);
				pro.update(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(TrackAndFixProblemsOfGSNPPView.TAG)) {
				TrackAndFixProblemsOfGSNPPView pro = (TrackAndFixProblemsOfGSNPPView) getFragmentManager()
						.findFragmentByTag(TrackAndFixProblemsOfGSNPPView.TAG);
				pro.updateFromDateAndEndDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(GSNPPPostFeedbackView.TAG)) {
				GSNPPPostFeedbackView pro = (GSNPPPostFeedbackView) getFragmentManager()
						.findFragmentByTag(GSNPPPostFeedbackView.TAG);
				pro.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ReviewsStaffView.TAG)) {
				ReviewsStaffView pro = (ReviewsStaffView) getFragmentManager()
						.findFragmentByTag(ReviewsStaffView.TAG);
				pro.updateFromDateAndEndDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(SupervisorListAlbumUserView.TAG)) {
				SupervisorListAlbumUserView pro = (SupervisorListAlbumUserView) getFragmentManager()
						.findFragmentByTag(SupervisorListAlbumUserView.TAG);
				pro.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(SupervisorImageListView.TAG)) {
				SupervisorImageListView v = (SupervisorImageListView) getFragmentManager()
						.findFragmentByTag(SupervisorImageListView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(TBHVFollowProblemView.TAG)) {
				TBHVFollowProblemView pro = (TBHVFollowProblemView) getFragmentManager()
						.findFragmentByTag(TBHVFollowProblemView.TAG);
				pro.update(dayOfMonth, monthOfYear, year);
			}
			if (fragmentTag.equals(TBHVAddRequirementView.TAG)) {
				TBHVAddRequirementView pro = (TBHVAddRequirementView) getFragmentManager()
						.findFragmentByTag(TBHVAddRequirementView.TAG);
				pro.update(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(TBHVReviewsGSNPPView.TAG)) {
				TBHVReviewsGSNPPView pro = (TBHVReviewsGSNPPView) getFragmentManager()
						.findFragmentByTag(TBHVReviewsGSNPPView.TAG);
				pro.updateFromDateAndEndDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(SupervisorListAlbumUserView.TAG)) {
				SupervisorListAlbumUserView pro = (SupervisorListAlbumUserView) getFragmentManager()
						.findFragmentByTag(SupervisorListAlbumUserView.TAG);
				pro.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ImageSearchGSNPPView.TAG)) {
				ImageSearchGSNPPView v = (ImageSearchGSNPPView) getFragmentManager()
						.findFragmentByTag(ImageSearchGSNPPView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ImageSearchNVBHView.TAG)) {
				ImageSearchNVBHView v = (ImageSearchNVBHView) getFragmentManager()
						.findFragmentByTag(ImageSearchNVBHView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(DocumentView.TAG)) {
				DocumentView v = (DocumentView) getFragmentManager()
						.findFragmentByTag(DocumentView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			}else if (fragmentTag.equals(GSNPPDocumentView.TAG)) {
				GSNPPDocumentView v = (GSNPPDocumentView) getFragmentManager()
						.findFragmentByTag(GSNPPDocumentView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			}  else if (fragmentTag.equals(TBHVDocumentView.TAG)) {
				TBHVDocumentView v = (TBHVDocumentView) getFragmentManager()
						.findFragmentByTag(TBHVDocumentView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ImageSearchTBHVView.TAG)) {
				ImageSearchTBHVView v = (ImageSearchTBHVView) getFragmentManager()
						.findFragmentByTag(ImageSearchTBHVView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(TBHVTrainingNeedToDoView.TAG)) {
				TBHVTrainingNeedToDoView v = (TBHVTrainingNeedToDoView) getFragmentManager()
						.findFragmentByTag(TBHVTrainingNeedToDoView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ReportLostInforView.TAG)) {
				ReportLostInforView v = (ReportLostInforView) getFragmentManager()
						.findFragmentByTag(ReportLostInforView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(ChooseParametersBeforeExportReportView.TAG)) {
				ChooseParametersBeforeExportReportView v = (ChooseParametersBeforeExportReportView) getFragmentManager()
						.findFragmentByTag(ChooseParametersBeforeExportReportView.TAG);
				v.updateDate(dayOfMonth, monthOfYear, year);
			} else if (fragmentTag.equals(NewCustomerCreate.TAG)) {
				NewCustomerCreate v = (NewCustomerCreate) getFragmentManager()
						.findFragmentByTag(NewCustomerCreate.TAG);
				v.setDateTime(dayOfMonth, monthOfYear, year);
			}*/
		}
	};

	/**
	 * DatePickerDialog
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (GlobalBaseActivity.this.currentTimePopupListener != null) {
				GlobalBaseActivity.this.currentTimePopupListener.onTimeSet(view, hourOfDay, minute);
			}
//			if (fragmentTag.equals(OrderView.TAG)) {
//				OrderView v = (OrderView) getFragmentManager()
//						.findFragmentByTag(OrderView.TAG);
//				v.updateTime(hourOfDay, minute);
//
//			}
		}
	};
	private boolean isLoadedNativeLib = false;

	/**
	 * reset gia tri ngay tren dayInOrder picker
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	public void resetDatePickerDialog() {
		int mYear;
		int mMonth;
		int mDay;
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);
		if (datePickerDialog != null) {
			datePickerDialog.updateDate(mYear, mMonth, mDay);
		}
	}

	/**
	 * hien thi popup dayInOrder picker voi ngay tuong ung dayInOrder truyen vao theo
	 * format: dd/mm/yyyy || dd-mm-yyyy || dd:mm:yyyy bien isSetDefaultDate chi
	 * co tac dung khi dayInOrder = null or dayInOrder = ""
	 * @author: HaiTC3
	 * @param dialogId
	 * @param date
	 * @param isSetDefaultDate
	 * @return: void
	 * @throws:
	 * @since: Feb 20, 2013
	 */
	public void showDatePickerDialog(String date, boolean isSetDefaultDate, DatePickerDialog.OnDateSetListener listener) {
		//set listener
		setDatePopupListener(listener);

		int numDate = 0;
		int numMonth = 0;
		int numYear = 0;
		if (!StringUtil.isNullOrEmpty(date)) {
			String[] strDate = new String[] {};
			if (date.indexOf("/") >= 0) {
				strDate = date.split("/");
			} else if (date.indexOf("-") >= 0) {
				strDate = date.split("-");
			} else if (date.indexOf(":") >= 0) {
				strDate = date.split(":");
			}
			if (strDate.length == 3) {
				numDate = Integer.parseInt(strDate[0].trim());
				numMonth = Integer.parseInt(strDate[1].trim()) - 1;
				numYear = Integer.parseInt(strDate[2].trim());
			} else {
				final Calendar c = Calendar.getInstance();
				numYear = c.get(Calendar.YEAR);
				numMonth = c.get(Calendar.MONTH);
				numDate = c.get(Calendar.DAY_OF_MONTH);
			}
			if (datePickerDialog != null) {
				datePickerDialog.updateDate(numYear, numMonth, numDate);
			} else {
				day = numDate;
				month = numMonth;
				year = numYear;
			}
		} else {
			if (isSetDefaultDate) {
				final Calendar c = Calendar.getInstance();
				day = c.get(Calendar.DAY_OF_MONTH);
				month = c.get(Calendar.MONTH);
				year = c.get(Calendar.YEAR);
				if (datePickerDialog != null) {
					datePickerDialog.updateDate(year, month, day);
				}
			}
		}
		showDialog(GlobalBaseActivity.DATE_DIALOG_ID);
	}

	public void showTimePickerDialog(TimePickerDialog.OnTimeSetListener listener) {
		//set listener
		this.setTimePopupListener(listener);
		//show dialog
		showDialog(TIME_DIALOG_ID);
	}

	/**
	 * Lay cac don hang draft
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void getOrderDraft(String customerId) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		e.viewData = bundle;
		e.sender = this;
		e.action = ActionEventConstant.GET_ORDER_DRAFT;

		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * KPI chuc nang co request thong qua actionevent
	 * @author: banghn
	 * @param kpi
	 * @param e
	 */
	public void requestInsertLogKPI(HashMapKPI kpi, ActionEvent e){
		int valueKey = GlobalInfo.getInstance().getHashMapKPI().get(kpi.ordinal());
		Calendar endTimeKPI = Calendar.getInstance();
		if (e.startTimeKPI != null
				&& endTimeKPI != null
				&& GlobalInfo.getInstance().getProfile().getUserData().getEnableClientLog() == 1
				&& (valueKey < GlobalInfo.getInstance().getAllowRequestLogKPINumber())) {

			String kpiNote = "";
			//them thoi gian cho khi start AsynTask
			long waitTime = e.startTimeFromBootActive - e.startTimeFromBoot;
			if (waitTime != 0) {
				kpiNote += "WAIT_TIME: " + waitTime;
			}
			GlobalUtil.requestInsertLogKPI(kpi, e.startTimeKPI, endTimeKPI, kpiNote);
		}
	}

	
	/**
	 * KPI request truc tiep (downfile,...)
	 * @author: banghn
	 * @param kpi
	 */
	public void requestInsertLogKPI(HashMapKPI kpi, Calendar start, Calendar end) {
		if (startTimeKPI != null
				&& endTimeKPI != null
				&& GlobalInfo.getInstance().getProfile().getUserData().getEnableClientLog() == 1) {
			GlobalUtil.requestInsertLogKPI(kpi, start, end, "");
		}
	}
	
	/**
	 * KPI request truc tiep (downfile,...)
	 * @author: banghn
	 * @param kpi
	 */
	public void requestInsertLogKPI(HashMapKPI kpi) {
		requestInsertLogKPI(kpi, startTimeKPI, endTimeKPI);
	}

	/**
	 * Update vi tri vao profile
	 * @author: banghn
	 * @param lng
	 * @param lat
	 * @param loc
	 */
	public void updatePosition(double lng, double lat, Location loc) {
		//kiem tra toa do nam trong lanh tho VN
		if (lat < 8.45 || lng < 102.0
				|| lat > 23.5 || lng > 110) {
			MyLog.d(Constants.LOG_LBS, "updatePosition, lng = "
					+ GlobalInfo.getInstance().getProfile().getMyGPSInfo()
							.getLongtitude() + " lat = ");
			return;
		}

		// kiem tra ghi log vi tri len server
		if (lat > 0	&& lng > 0
			&& GlobalInfo.getInstance().getProfile() != null
			&& GlobalInfo.getInstance().getProfile().getUserData() != null
			&& GlobalInfo.getInstance().getProfile().getUserData().getInheritId() > 0) {

			double lastLat = GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude();
			double lastLng = GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLongtitude();
			//vi tri moi
//			if (lat != lastLat || lng != lastLng) {
			if (BigDecimal.valueOf(lat).compareTo(BigDecimal.valueOf(lastLat)) != 0
					|| BigDecimal.valueOf(lng).compareTo(BigDecimal.valueOf(lastLng)) != 0) {
				long timeCurrent = System.currentTimeMillis();
				//check han che log vi tri gui len nhieu (toi da 5s gui 1 log)
				boolean isTimeGetLocation = Math.abs(timeCurrent
						- GlobalInfo.getInstance().getTimeSendLogPosition()) > 5000;
				String debugLog = GlobalInfo.getInstance().isDebugMode
						? String.format(" %s %sm", loc.getProvider(), loc.getAccuracy()) : "";
				// kiem tra muc do dang tin cua vi tri
				boolean isTrustLocation = true;
				// neu can check trust location, tranh fake mock location
				if (isTimeGetLocation && GlobalInfo.getInstance().isVaildateMockLocation()) {
					if (GlobalBaseActivity.getInstance().isFakeGPS() || !isTrustLocation(loc)) {
						isTrustLocation = false;
						MyLog.d(Constants.LOG_LBS, "updatePosition, lat = " + lat + " lng = " + lng + " isFakeGPS: " + GlobalBaseActivity.getInstance().isFakeGPS());
						// 10s show toast vi tri khong dang tin 1 lan, tranh
						// truong hop GPS network fullDate lien tuc
						if (Math.abs(timeCurrent - GlobalInfo.getInstance().getTimeToastUnstrustPosition()) > 10000) {
							GlobalInfo.getInstance().setTimeToastUnstrustPosition(timeCurrent);
							if (GlobalBaseActivity.getInstance().isFakeGPS()) {
								showToastMessage(StringUtil.getString(R.string.TEXT_DETECT_LOCATION_GPS_FAKE) + debugLog);
							} else {
								// khi luong network chay se fullDate lien tuc vi tri
								// cu ve, khong nen hien lien tuc toast
								showToastMessage(StringUtil.getString(R.string.TEXT_DETECT_LOCATION_GPS_OLD) + debugLog);
							}
						}
					}
				}
				//han che goi nhieu + vi tri dang tin
				if (isTimeGetLocation && isTrustLocation) {
					//show locating success
					GlobalUtil.showToast(StringUtil.getString(R.string.TEXT_LOCATING_SUCCESS) + debugLog);

					GlobalInfo.getInstance().setTimeSendLogPosition(System.currentTimeMillis());

					//phai trong thoi gian dong bo thi goi
					if (GlobalUtil.isInTimeSyn()) {
						MyLog.d("Locating", "send log ................. ");
						// khoi tao staffPosition
						StaffPositionLogDTO staffPos = new StaffPositionLogDTO();
						staffPos.createDate = DateUtils.now();
						staffPos.lat = lat;
						staffPos.lng = lng;
						staffPos.accuracy = loc.getAccuracy();
						staffPos.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
						staffPos.shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
						staffPos.battery = GlobalUtil.getBatteryPercent();

						// Mang
						DMSNetworkInfo network = null;
						try {
							network = NetworkUtil.me().getCurrentTypeConnect();
							if (network != null) {
								staffPos.networkType = network.type;
								staffPos.networkSpeed = network.value;
							}
						} catch (Exception e) {
						}

						ShopDTO shop = GlobalInfo.getInstance().getProfile().getUserData().getShopManaged();
						if(shop != null){
							staffPos.distance = GlobalUtil.getDistance(
									new LatLng(shop.shopLat, shop.shopLng), new LatLng(staffPos.lat, staffPos.lng));
						}else{
							staffPos.distance = 0;
						}

						if (GlobalInfo.getInstance().getProfile().getUserData().getLoginState() == UserDTO.LOGIN_SUCCESS
								&& AsyncTaskUtil.getNumAsyncTaskActive() <= 3
								&& GlobalUtil.checkNetworkAccess()) {
							sendPositionToServer(staffPos);
						} else {
							// luu vi tri de dong bo offline
							GlobalInfo.getInstance().addPosition(staffPos);
						}
					}

					//luu vi tri moi
					GlobalInfo.getInstance().setLoc(loc);
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().setLongtitude(lng);
					GlobalInfo.getInstance().getProfile().getMyGPSInfo().setLattitude(lat);
					GlobalInfo.getInstance().getProfile().save();

					Bundle bd = new Bundle();
					if (GlobalInfo.getInstance().getActivityContext() instanceof GlobalBaseActivity) {
						((GlobalBaseActivity) GlobalInfo.getInstance().getActivityContext())
						.sendBroadcast(ActionEventConstant.ACTION_UPDATE_POSITION, bd);
					}
				}

			}
		}
	}


	/**
	 * Tao request position de goi len server
	 * @author: BangHN
	 * @param lng
	 * @param lat
	 * @return: void
	 * @throws:
	 */
	public void sendPositionToServer(StaffPositionLogDTO staffPos) {
		if (GlobalInfo.isIS_VERSION_SEND_DATA()) {
			// id
			String id = GlobalUtil.generateLogId();
			// khoi tao sql
			JSONArray json = new JSONArray();
			json.put(staffPos.generateCreateSql());
			Vector<Object> para = new Vector<Object>();
			para.add(IntentConstants.INTENT_LIST_SQL);
			para.add(json);
			para.add(IntentConstants.INTENT_MD5);
			para.add(StringUtil.md5(json.toString()));
			para.add(IntentConstants.INTENT_LOG_ID);
			para.add(id);
			para.add(IntentConstants.INTENT_IMEI_PARA);
			para.add(GlobalInfo.getInstance().getDeviceIMEI());

			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.ACTION_UPDATE_POSITION;
			e.sender = this;
			e.userData = staffPos;
			e.viewData = para;
			//add time to action event
			e.createDate = staffPos.createDate;
			UserController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * Kiem tra GooglePlayService co ton tai hay khong
	 * @author: banghn
	 * @return
	 */
	private boolean isGooglePlayServiceAvailable(){
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(GlobalInfo.getInstance());
		if(status == ConnectionResult.SUCCESS) {
		    return true;
		}
		return false;
	}

	public class MenuTab{
		public int action;
		public PriHashMap.PriForm form;

		public MenuTab(int action, PriHashMap.PriForm form){
			this.action = action;
			this.form = form;
		}
	}

	/**
	 * Xu ly thoat ung dung
	 * @author: banghn
	 */
	public void processExitApp(){
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		String dateClearData = sharedPreferences.getString(DATE_CLEAR_OLD_DATA, "");
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		if(dateClearData.equals(dateNow)){
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
		}else{
			requestDeleteOldLogTable();
		}
	}

	/**
	 * Request xoa bo nhung record cu trong bang log_table, action log
	 * @author banghn
	 */
	public void requestDeleteOldLogTable() {
		showProgressDialog(StringUtil.getString(R.string.exitting));
		LogFile.logToFile(DateUtils.now()
				+ " || Bat dau thuc hien xoa du lieu cu");
		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.ACTION_DELETE_OLD_LOG_TABLE;
		e.viewData = new Bundle();
		e.sender = GlobalBaseActivity.this;
		UserController.getInstance().handleViewEvent(e);
	}

	/**
	 * Set trang thai cap nhat du lieu
	 *
	 * @author ThangNV31
	 * @param listScript
	 */
	public void setIsCanceledUpdateData(boolean isCanceledUpdateData) {
		this.isCancelUpdateData = isCanceledUpdateData;
	}

	/**
	 * Gui log model len server
	 * @author: banghn
	 */
	public void sendLogModel(ModelEvent modelEvent){
		//Remove thong tin nhay cam
		Object viewData = modelEvent.getActionEvent().viewData;
		String param = modelEvent.getParams();
		
		if (modelEvent.getActionEvent().action == ActionEventConstant.ACTION_LOGIN
				|| modelEvent.getActionEvent().action == ActionEventConstant.RE_LOGIN) {
			Vector<String> vt = (Vector<String>) viewData;
			vt.removeElementAt(0);
			vt.removeElementAt(0);
			vt.removeElementAt(0);
			vt.removeElementAt(0);
			viewData = vt;		
			
			try {
				JSONObject json = new JSONObject(param);
				json.remove(IntentConstants.INTENT_USER_NAME);
				json.remove(IntentConstants.INTENT_LOGIN_PASSWORD);
				
				param = json.toString();
			} catch (JSONException e1) {
			}
		}
		
		ServerLogger.sendLog("MESSAGE_ERROR_COMMON",
				String.format("Action: %s || Sender: %s || Controller: %s || LogData: %s || ModelData: %s ||"
						+ " Request: %s || Tag: %s || UserData: %s || ViewData: %s || "
						+ "Code: %s || DataText: %s || ModelCode: %s || ModelData: %s || ModelMessage: %s || Param: %s",
				modelEvent.getActionEvent().action,
				modelEvent.getActionEvent().sender,
				modelEvent.getActionEvent().controller,
				modelEvent.getActionEvent().logData,
				modelEvent.getActionEvent().modelData,
				modelEvent.getActionEvent().request,
				modelEvent.getActionEvent().tag,
				modelEvent.getActionEvent().userData,
				viewData,
				modelEvent.getCode(),
				modelEvent.getDataText(),
				modelEvent.getModelCode(),
				modelEvent.getModelData(),
				modelEvent.getModelMessage(),
				param
				), TabletActionLogDTO.LOG_CLIENT);
	}


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if (GlobalInfo.getInstance().getPriUtilsInstance() != null) {
			GlobalInfo.getInstance().savePriUtilsInstance();
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(!GlobalInfo.getInstance().restorePriUtilsInstance()){
			showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_PRI));
		}
		super.onRestoreInstanceState(savedInstanceState);
	}

	 /**
	 * insert actionlog ko co endtimer
	 * @author: Tuanlt11
	 * @param action
	 * @return: void
	 * @throws:
	*/
	public void requestInsertActionLogWithoutEndTime(ActionLogDTO action){
		if (!isInsertingActionLogVisit) {
			isInsertingActionLogVisit = true;
			ActionEvent e = new ActionEvent();
			action.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			action.staffId = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId();
			action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLatitude();
			action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLastLongtitude();
			MyLog.i("MyLog", "insert action: startTime - endTime : "
					+ action.startTime + " - " + action.endTime);
			e.sender = this;
			e.action = ActionEventConstant.INSERT_ACTION_LOG;

			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_DATA, action);
			e.viewData = action;
			e.isNeedCheckTimeServer = false;
			SaleController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * Load native lib
	 * @author: duongdt3
	 * @since: 09:32:59 4 Dec 2014
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	public boolean loadNativeLib(){
		boolean isHaveNativeLib = isLoadedNativeLib ;
		if (!isHaveNativeLib) {
			 isHaveNativeLib = ExternalStorage.checkNativeLib(this);
			// neu da co thu vien trong Internal Storage thi check load ICU sqlcipher
			if (isHaveNativeLib) {
				isHaveNativeLib = ExternalStorage.checkIcuLibVaild(this);
			}
		}
		//set isLoadedNativeLib from load libs
		isLoadedNativeLib = isHaveNativeLib;
		return isHaveNativeLib;
	}

	//custom keyboard
	KeyboardPopupWindow keyboardCustom = null;

	/**
	 * show keyboard custom
	 *
	 * @author: duongdt3
	 * @since: 13:41:52 11 Nov 2014
	 * @return: void
	 * @throws:
	 * @param x
	 * @param y
	 */
	public void showKeyboardCustom(int x, int y) {
		showKeyboardCustom(this.getWindow(), rootView, x, y);
	}

	/**
	 * update keyboard params data
	 *
	 * @author: duongdt3
	 * @since: 11:33:59 13 Apr 2015
	 * @return: void
	 * @throws:
	 */
	public void updateKeyboardParams() {
		int sysDecimalPoint = GlobalInfo.getInstance().getSysDecimalPoint();
		// 2 la dung dau cham ngan cach phan thap phan
		String numberSeparator = (sysDecimalPoint == 2) ? "." : ",";
		if (keyboardCustom != null) {
			keyboardCustom.updateDecimalPointChar(numberSeparator);
		}
	}

	/**
	 * hide keyboard custom
	 *
	 * @author: duongdt3
	 * @since: 13:43:07 11 Nov 2014
	 * @return: void
	 * @throws:
	 */
	public void hideKeyboardCustom() {
		if (keyboardCustom != null && keyboardCustom.isShowing()) {
			keyboardCustom.dismiss();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// this.hideKeyboardCustom();
		super.onBackPressed();
	}

	/**
	 * lay ds cac thiet bi bao mat loi
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	public void getEquipList() {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		e.viewData = bundle;
		e.sender = this;
		e.action = ActionEventConstant.GET_EQUIPMENT_LOST_REPORT_ERROR;
		SupervisorController.getInstance().handleViewEvent(e);
	}

	/**
	 * @author: DungNX
	 * @param listEquipInventory
	 * @return: void
	 * @throws:
	*/
	private void showPopUpEquipError(ArrayList<EquipInventoryDTO> listEquipInventory) {
		if (alertProductDetail == null) {
			Builder build = new AlertDialog.Builder(this,
					R.style.CustomDialogTheme);
			build.setCancelable(true);
			LayoutInflater inflater = this.getLayoutInflater();
			View view = inflater.inflate(R.layout.equip_error, null);
			build.setView(view);
			btPass = (Button) view.findViewById(R.id.btPass);
			btPass.setOnClickListener(this);

			int[] LIST_TABLE_WIDTHS = { 45, 300, 200 };
			String[] TABLE_TITLES = {
					StringUtil.getString(R.string.TEXT_STT),
					StringUtil.getString(R.string.TEXT_CODE_DEVICE),
					StringUtil.getString(R.string.TEXT_SERI_DEVICE) };
			tbListEquipError = (VinamilkTableView) view.findViewById(R.id.tbListEquipError);
			tbListEquipError.getHeaderView().addColumns(
					LIST_TABLE_WIDTHS,
					TABLE_TITLES,
					ImageUtil.getColor(R.color.BLACK),
					ImageUtil.getColor(R.color.TABLE_HEADER_BG));

			alertProductDetail = build.create();
			Window window = alertProductDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		// cap nhat du lieu vao tb
		tbListEquipError.clearAllData();
		List<TableRow> listRows = new ArrayList<TableRow>();
		if (listEquipInventory != null && listEquipInventory.size() > 0){
			for(int i = 0; i < listEquipInventory.size(); i++){
				ListEquipErrorRow row = new ListEquipErrorRow(this);
				row.renderLayout(i, listEquipInventory.get(i));
				listRows.add(row);
			}
		}
		tbListEquipError.addContent(listRows);

		if (!alertProductDetail.isShowing()) {
			alertProductDetail.show();
		}
	}

	/**
	 * Doi ngon ngu ung dung
	 *
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 11:38:35 19 Jun 2014
	 * @return: void
	 * @throws:
	 */
	protected void changeLanguage(String languageName) {
		GlobalInfo.getInstance().saveLanguages(languageName);
		restartActivity();
	}

	private void restartActivity() {
		Intent intent = getIntent();
		finish();
		startActivity(intent);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	/**
	 * Lay thong tin app tu ap_param
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void getAppDefineConstant() {
		//get ap_params
		handleViewEventWithOutAsyntask(new Bundle(), ActionEventConstant.GET_APPARAM, UserController.getInstance());

		//get shop_params
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		String[] arrParams = new String[] { "SYS_MODIFY_PRICE", "SYS_SHOW_PRICE", "SYS_MAXDAY_RETURN", "SYS_MAXDAY_APPROVE", "SYS_RETURN_DATE", "SYS_NEED_SAVE_REMAIN", "SYS_NEED_TAKE_PHOTO_EQUIPMENT" };
		data.putStringArray(IntentConstants.INTENT_PARAMS, arrParams);
		handleViewEventWithOutAsyntask(data, ActionEventConstant.GET_SHOP_PARAMs, UserController.getInstance());
	}

	/**
	 * chuyen sang cac fragment khac, su dung trong activity
	 *
	 * @author: dungdq3
	 * @since: 3:15:28 PM Oct 9, 2014
	 * @return: void
	 * @throws:
	 * @param b
	 * @param action
	 * @param abs:
	 */
	protected void handleSwitchFragment(Bundle b, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = (b == null ? new Bundle() : b);
		e.viewData = bundle;
		e.action = action;
		abs.handleSwitchFragment(e);
	}

	 /**
	 * handle cac su kien tu view
	 * @author: Tuanlt11
	 * @param b
	 * @param action
	 * @param abs
	 * @return: void
	 * @throws:
	*/
	protected void handleViewEvent(Bundle b, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = (b == null ? new Bundle() : b);
		e.viewData = bundle;
		e.action = action;
		abs.handleViewEvent(e);
	}

	 /**
	 * hanlde cac su kien ko xai asyntask
	 * @author: Tuanlt11
	 * @param b
	 * @param action
	 * @param abs
	 * @return: void
	 * @throws:
	*/
	protected void handleViewEventWithOutAsyntask(Bundle b, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = (b == null ? new Bundle() : b);
		e.viewData = bundle;
		e.action = action;
		e.isUsingAsyntask = false;
		abs.handleViewEvent(e);
	}

	 /**
	 * handle cac su kien tu view
	 * @author: Tuanlt11
	 * @param vector
	 * @param action
	 * @param abs
	 * @return: void
	 * @throws:
	*/
	@SuppressWarnings("rawtypes")
	protected void handleViewEvent(Vector vector, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = vector;
		e.action = action;
		abs.handleViewEvent(e);
	}

	 /**
	 * handle su kien tu view
	 * @author: Tuanlt11
	 * @param Bundle
	 * @param action
	 * @param abs
	 * @param isNeedCheckTimeServer: can check thoi gian server ko
	 * @param isBlockRequest: co can block request ko
	 * @return: void
	 * @throws:
	*/
	protected void handleViewEvent(Bundle b, int action,
			AbstractController abs, boolean isNeedCheckTimeServer,
			boolean isBlockRequest) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = b;
		e.action = action;
		e.isNeedCheckTimeServer = isNeedCheckTimeServer;
		e.isBlockRequest = isBlockRequest;
		abs.handleViewEvent(e);
	}

	/**
	 * handle su kien tu view
	 *
	 * @author: Tuanlt11
	 * @param Vector
	 * @param action
	 * @param abs
	 * @param isNeedCheckTimeServer
	 *            : can check thoi gian server ko
	 * @param isBlockRequest
	 *            : co can block request ko
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected void handleViewEvent(Vector vector, int action,
			AbstractController abs, boolean isNeedCheckTimeServer,
			boolean isBlockRequest) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = vector;
		e.action = action;
		e.isNeedCheckTimeServer = isNeedCheckTimeServer;
		e.isBlockRequest = isBlockRequest;
		abs.handleViewEvent(e);
	}


	/**
	 * Lay thong tin cham cong
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void getAttendaceTime() {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		handleViewEventWithOutAsyntask(b,
				ActionEventConstant.GET_ATTENDANCE_TIME_DEFINE,
				UserController.getInstance());
	}

	/**
	 * huy google map
	 *
	 * @author: dungdq3
	 * @since: 10:05:00 AM Mar 16, 2015
	 * @return: void
	 * @throws:
	 */
	public void destroyMapGoogle() {
		MapFragment mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
		if (mapFragment != null)
	        getFragmentManager().beginTransaction().remove(mapFragment).commit();
	}

	/**
	 * click menu search
	 *
	 * @author: dungdq3
	 * @since: 10:20:21 AM Mar 16, 2015
	 * @return: void
	 * @throws:
	 */
	protected void onClickSearchMenu(){

	}

	/**
	 * insert data vao bang tam org-acess
	 *
	 * @author: dungdq3
	 * @since: 4:20:43 PM Mar 18, 2015
	 * @return: void
	 * @throws:
	 */
	protected void generateOrgTempData(){
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_STAFF_ID, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		b.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEventWithOutAsyntask(b, ActionEventConstant.GET_DATA_FROM_ORG_ACCESS,
				UserController.getInstance());
	}

	private DatePickerDialog.OnDateSetListener currentDatePopupListener = null;
	public void setDatePopupListener(DatePickerDialog.OnDateSetListener listener){
		currentDatePopupListener = listener;
	}

	private TimePickerDialog.OnTimeSetListener currentTimePopupListener = null;

	public void setTimePopupListener(TimePickerDialog.OnTimeSetListener listener){
		currentTimePopupListener = listener;
	}

	/**
	 * tim fragment tu tag
	 * @author: duongdt3
	 * @since: 08:29:35 24 Mar 2015
	 * @return: Fragment
	 * @throws:
	 * @param tag
	 * @return
	 */
	public Fragment findFragmentByTag (String tag){
		Fragment frag = null;
		frag = getFragmentManager().findFragmentByTag(tag);
		return frag;
	}

	public void showKeyboardCustom(Window win, View parentView){
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			showKeyboardCustom(win, parentView, 400, 200);
		}
	}

	/**
	 * show keyboard custom with custom window
	 * @author: duongdt3
	 * @since: 13:41:52 11 Nov 2014
	 * @return: void
	 * @throws:
	 * @param win
	 * @param parentView
	 * @param x
	 * @param y
	 */
	Window currentWindow = null;
	CountDownTimer timerGPSRequest;
	public void showKeyboardCustom(Window win, View parentView, int x, int y){
		if (keyboardCustom == null) {
			//khoi tao keyboard
			keyboardCustom = new KeyboardPopupWindow(this);
			updateKeyboardParams();
		}
		if (currentWindow != win && keyboardCustom.isShowing()) {
			keyboardCustom.dismiss();
		}
		currentWindow = win;
		if (keyboardCustom != null && !keyboardCustom.isShowing()) {
			if (parentView != null && !this.isFinishing()) {
				GlobalUtil.forceHideKeyboard(this);
				keyboardCustom.show(win, parentView, x, y);
			}
		}
	}

	/**
	 * show keyboard custom with location default
	 * @author: duongdt3
	 * @since: 15:33:14 11 Nov 2014
	 * @return: void
	 * @throws:
	 */
	public void showKeyboardCustom(View view){
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			boolean isShowKeyboard = (view == null) || (view != null && view.isEnabled());
			if (isShowKeyboard) {
				showKeyboardCustom(this.getWindow(), rootView, 300, 200);
			}
		}
	}

	/**
	 * Lay chuong trinh khuyen mai bi sai
	 * @author: banghn
	 * @param promotionProgrameId
	 */
	public void requestSynProgrameData(long promotionProgrameId) {
		//tam dung huy luong dong bo ngam
		TransactionProcessManager.getInstance().cancelTimer();
		try {
			isCancelUpdateData = false;
			Vector<String> vt = new Vector<String>();
			vt.add(IntentConstants.PROMOTION_PROGRAME_ID);
			vt.add(Long.toString(promotionProgrameId));
			vt.add(IntentConstants.INTENT_SYN_INHERITID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
			vt.add(IntentConstants.INTENT_SHOP_ID);
			vt.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());

			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.ACTION_SYN_PROMOTION;
			e.isBlockRequest = true;
			e.viewData = vt;
			e.userData = promotionProgrameId;
			e.sender = this;
			SynDataController.getInstance().handleViewEvent(e);
		} catch (Exception ex) {
			MyLog.e("requestSynProgrameData", "fail", ex);
		}
	}

	/**
	 * check location vaild
	 * @author: duongdt3
	 * @since: 17:06:59 26 Jan 2015
	 * @return: boolean
	 * @throws:
	 * @param location
	 * @return
	 */
	private boolean isTrustLocation(Location location) {
		boolean isTrust = false;
		//check dieu kien vi tri khi can vaildate mock
		if(GlobalInfo.getInstance().isVaildateMockLocation()){
			if (location != null) {
				isTrust = true;
				if (GlobalBaseActivity.getInstance().getListLastKnowLocation() != null) {
					//check in list lastKnowLocation
					for (Location lastLoc : GlobalBaseActivity.getInstance().getListLastKnowLocation()) {
						if (lastLoc.getLatitude() == location.getLatitude()
								&& lastLoc.getLongitude() == location.getLongitude()) {
							isTrust = false;
							break;
						}
					}
				}
				MyLog.d(Constants.LOG_LBS, "isVaildLocation isVaild: " + isTrust);
			}
		} else{
			isTrust = true;
		}

		return isTrust;
	}

	public static void reloadListKnownLocation(){
		LocationManager locManager = (LocationManager) GlobalInfo
				.getInstance().getAppContext()
				.getSystemService(Context.LOCATION_SERVICE);
		List<Location> lstLoc = new ArrayList<Location>();
		if (locManager != null) {
			List<String> lstString = locManager.getAllProviders();
			
			for (String provider : lstString) {
				Location loc = locManager.getLastKnownLocation(provider);
				if (loc != null) {
					MyLog.d("getListKnownLocation", "provider: " + loc.getProvider()
					+ " " + loc.getLatitude() + " " + loc.getLongitude());
					lstLoc.add(loc);
				}
			}
		}

		if (GlobalBaseActivity.getInstance().getListLastKnowLocation() == null) {
			GlobalBaseActivity.getInstance().setListLastKnowLocation(new ArrayList<Location>());
		}
		GlobalBaseActivity.getInstance().getListLastKnowLocation().clear();
		GlobalBaseActivity.getInstance().getListLastKnowLocation().addAll(lstLoc);
	}

	/**
	 * start service dinh vi fused,
	 * neu lan thu 2 goi se truyen data toi service dang chay, khong khoi chay service cai moi
	 * @author: duongdt3
	 * @since: 13:40:26 5 Nov 2014
	 * @return: void
	 * @throws:
	 * @param isToastError
	 * @param isShowPopupError
	 */
	public void startFusedLocationService() {
		if (isGooglePlayServiceAvailable()) {
			Intent service = new Intent(GlobalInfo.getInstance(), LocationService.class);
			Bundle data = new Bundle();
			data.putInt(IntentConstants.RADIUS_OF_POSITION_FUSED, GlobalInfo.getInstance().getRadiusFusedPosition());
			data.putInt(IntentConstants.FAST_INTERVAL_FUSED_POSITION, GlobalInfo.getInstance().getFastIntervalFusedPosition());
			data.putInt(IntentConstants.INTERVAL_FUSED_POSITION, GlobalInfo.getInstance().getIntervalFusedPosition());
			data.putInt(IntentConstants.FUSED_POSTION_PRIORITY, GlobalInfo.getInstance().getFusedPositionPriority());
			data.putBoolean(IntentConstants.FUSED_POSTION_RESTART, false);
			service.putExtras(data);
			startService(service);
		}
	}

	/**
	 * restart service dinh vi fused,
	 * neu lan thu 2 goi se truyen data toi service dang chay, khong khoi chay service cai moi
	 * @author: duongdt3
	 * @since: 13:40:26 5 Nov 2014
	 * @return: void
	 * @throws:
	 * @param isToastError
	 * @param isShowPopupError
	 */
	public void reStartFusedLocationService() {
		if (isGooglePlayServiceAvailable()) {
			Intent service = new Intent(GlobalInfo.getInstance(), LocationService.class);
			Bundle data = new Bundle();
			data.putInt(IntentConstants.RADIUS_OF_POSITION_FUSED, GlobalInfo.getInstance().getRadiusFusedPosition());
			data.putInt(IntentConstants.FAST_INTERVAL_FUSED_POSITION, GlobalInfo.getInstance().getFastIntervalFusedPosition());
			data.putInt(IntentConstants.INTERVAL_FUSED_POSITION, GlobalInfo.getInstance().getIntervalFusedPosition());
			data.putInt(IntentConstants.FUSED_POSTION_PRIORITY, GlobalInfo.getInstance().getFusedPositionPriority());
			data.putBoolean(IntentConstants.FUSED_POSTION_RESTART, true);
			service.putExtras(data);
			startService(service);
		}
	}

	/**
	 * stop service dinh vi fused
	 * @author: duongdt3
	 * @since: 14:34:41 6 Nov 2014
	 * @return: void
	 * @throws:
	 */
	public void stopFusedLocationService() {
		Intent service = new Intent(GlobalInfo.getInstance()
				.getAppContext(), LocationService.class);
		stopService(service);
	}


	/**
	 * Kiem tra GooglePlayService co ton tai hay khong
	 * @author: banghn
	 * @return
	 */
	private void showGooglePlayServicesError(){
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(GlobalInfo.getInstance());
		if(status != ConnectionResult.SUCCESS) {
			String errorStr = StringUtil.getString(R.string.TEXT_ACTIVE_GOOGLE_SERVICE_UNSUCCESS)
					+ ": " + GooglePlayServicesUtil.getErrorString(status);
			showToastMessage(errorStr);

			//goi log bao loi kich hoat dinh vi
			try {
				this.closeDialog(dialogCheckGooglePlay);
				dialogCheckGooglePlay = GooglePlayServicesUtil.getErrorDialog(status, this, -1);
				if (dialogCheckGooglePlay != null) {
					this.showDialog(dialogCheckGooglePlay);
				}
				ServerLogger.sendLog(errorStr, TabletActionLogDTO.LOG_CLIENT);
			} catch (Exception e) {
			}
		}
	}

	/**
	 * show dialog with check activity
	 * @author: duongdt3
	 * @since: 11:37:36 7 Jan 2015
	 * @return: void
	 * @throws:
	 * @param dialog
	 */
	public void showDialog(Dialog dialog) {
		if (dialog != null && !isFinishing()) {
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			dialog.show();
		}
	}

	/**
	 * re Start Locating
	 * @author: duongdt3
	 * @since: 11:37:36 7 Jan 2015
	 * @return: void
	 * @throws:
	 */
	public void reStartLocating(){
		reStartFusedLocationService();
		PositionManager.getInstance().reStart();
	}

	/**
	 * re Start Locating
	 * @author: duongdt3
	 * @since: 11:37:36 7 Jan 2015
	 * @return: void
	 * @throws:
	 */
	public void reStartLocatingWithWaiting(){
		if (timerGPSRequest != null){
			timerGPSRequest.cancel();
		}

		//khoi tao gps request timer de thong bao loi
		timerGPSRequest =  new CountDownTimer(20000, 20000) {

			public void onTick(long millisUntilFinished) {}

			public void onFinish() {
				//chua co vi tri, dong progress dialog
				closeGPSProgressDialog();
				//hien thi thong bao dinh vi chua duoc
				showToastLongMessage(StringUtil.getString(R.string.TEXT_REQUEST_LOCATING_ERROR));
			}
		}.start();

		showGPSProgressDialog();

		reStartLocating();
	}

	ProgressDialog progressDlgGPSLocating = null;

	public void closeGPSProgressDialog() {
		this.closeDialog(progressDlgGPSLocating);
	}

	public void showGPSProgressDialog() {
		closeGPSProgressDialog();

		if (progressDlgGPSLocating == null) {
			progressDlgGPSLocating = new ProgressDialog(this);
			progressDlgGPSLocating.setMessage(StringUtil.getString(R.string.TEXT_REQUEST_LOCATING));
			progressDlgGPSLocating.setCancelable(true);
			progressDlgGPSLocating.setCanceledOnTouchOutside(false);
		}
		showDialog(progressDlgGPSLocating);
	}

	/**
	 * start activity for result with try catch
	 * @param intent
	 * @param requestCode
	 * @param actionName
	 */
	public void startActivityForResult(Intent intent, int requestCode, String actionName) {
		try {
			GlobalUtil.startActivityForResultFromActivity(this, intent, requestCode);
		} catch (ActivityNotFoundException e) {
			String note = StringUtil.getString(R.string.TEXT_NOT_ACTIVITY_FOUND, actionName);
			showDialog(note);
			ServerLogger.sendLog(actionName + " not found activity "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		} catch (Exception e) {
			String note = StringUtil.getString(R.string.TEXT_START_ACTIVITY_FAIL, actionName);
			showDialog(note);
			ServerLogger.sendLog(actionName + " fail "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 * Khoi tao mang blacklist
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void initBlackListApp() {
		blackApps.clear();
		List<ApplicationInfo> packages = packageManager
				.getInstalledApplications(PackageManager.GET_META_DATA);
		StringBuffer strApp = new StringBuffer();
		//StringBuffer strAppSystem = new StringBuffer();
		ArrayList<String> whitelist = GlobalInfo.getInstance().getWhiteList();
		int sWhiteList = whitelist != null ? whitelist.size() : 0;
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		PackageManager pm = getPackageManager();
		ResolveInfo mInfo = pm.resolveActivity(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		String packageHome = (mInfo != null 
				&& mInfo.activityInfo != null 
				&& mInfo.activityInfo.packageName != null) ? mInfo.activityInfo.packageName : "";
		String myPackage = this.getPackageName();
		for (ApplicationInfo packageInfo : packages) {
			if (packageInfo != null && packageInfo.packageName != null) {
				if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 1) {
					boolean isAddBlacklist = true;
					
					if (packageInfo.packageName.equals(myPackage) || packageInfo.packageName.equals(packageHome)) {
						isAddBlacklist = false;
					} else if(sWhiteList > 0){
						for (int i = 0; i < sWhiteList; i++) {
							// kiem tra ung dung co trong whitelist ko, neu co thi bo qua
							if (packageInfo.packageName.equals(whitelist.get(i))) {
								isAddBlacklist = false;
								break;
							}
						}
					}
					if (isAddBlacklist) {
						blackApps.add(packageInfo);
						strApp.append(packageInfo.loadLabel(packageManager)
								+ ":" + packageInfo.packageName + "\n");
					}
				}
			}
		}
		
		String strLogApp = "";
		// bo di dau phay cuoi cung
		if (strApp.length() > 1) {
			strApp.setLength(strApp.length() - 1);
			strLogApp += "App Install: " + strApp.toString() + "\n";
			if (!GlobalInfo.getInstance().isFirstTimeSendApp()) {
				ServerLogger.sendLog("AppPrevent", strLogApp, true,
						TabletActionLogDTO.LOG_CLIENT);
				GlobalInfo.getInstance().setFirstTimeSendApp(true);
			}
		}
	}

	/**
	 * Hien thi dialog app can xoa
	 * @author: Tuanlt11
	 * @param listApp
	 * @return: void
	 * @throws:
	 */
	public void showDialogAppUninstall(List<ApplicationInfo> listApp) {
		if (listApp != null && !listApp.isEmpty()) {
			if (alertBackListAppDialog == null) {
				Builder build = new AlertDialog.Builder(this,
						R.style.CustomDialogTheme);
				blackListView = new BlackListAppView(this,
						ACTION_SELECTED_APP_DELETE);
				build.setView(blackListView.viewLayout);
				alertBackListAppDialog = build.create();
				alertBackListAppDialog.setCancelable(false);
				Window window = alertBackListAppDialog.getWindow();
				window.setBackgroundDrawable(new ColorDrawable(Color.argb(0,
						255, 255, 255)));
				window.setLayout(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT);
				window.setGravity(Gravity.CENTER);
			}
			blackListView.renderLayout(listApp);
			if (!alertBackListAppDialog.isShowing())
				alertBackListAppDialog.show();
		} else {
			if (alertBackListAppDialog != null
					&& alertBackListAppDialog.isShowing()) {
				alertBackListAppDialog.dismiss();
			}
		}
	}
	
	public void closeDialog(Dialog dialog){
		if (!this.isFinishing() && dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

	/**
	 * yeu cau nguoi dung cap quyen admin device
	 * @author: duongdt3
	 * @time: 8:50:10 AM Oct 19, 2015
	 */
	private void requestAdmin(){
		ComponentName deviceAdmin = new ComponentName(this, DMSDeviceAdminReceiver.class);
		DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
		List<ComponentName> lstActiveAdmins = devicePolicyManager.getActiveAdmins();
		boolean isEnable = false;
		
		if (lstActiveAdmins == null || lstActiveAdmins.isEmpty()) {
			isEnable = false;
		} else{
			for (ComponentName componentName : lstActiveAdmins) {
				if (componentName != null) {
					MyLog.i("requestAdmin", "componentName.getClassName() " + componentName.getClassName());
					if(deviceAdmin.getClassName().equals(componentName.getClassName())){
						isEnable = true;
						break;
					}
				}
			}
		}
		
		if (!isEnable) {
			// Activate device administration
			Intent intent = new Intent(
					DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
			intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, deviceAdmin);
			intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, StringUtil.getString(R.string.TEXT_ADMIN_REQUEST_CONTENT));
			GlobalUtil.startActivityForResultFromActivity(this, intent, ACTIVATION_REQUEST_ADMIN);
		}
	}

	public List<Location> getListLastKnowLocation() {
		return listLastKnowLocation;
	}

	public void setListLastKnowLocation(List<Location> listLastKnowLocation) {
		this.listLastKnowLocation = listLastKnowLocation;
	}

	public boolean isFakeGPS() {
		return isFakeGPS;
	}

	public void setFakeGPS(boolean isFakeGPS) {
		this.isFakeGPS = isFakeGPS;
	}
	
//	@Override
//	public void onWindowFocusChanged(boolean hasFocus) {
//		super.onWindowFocusChanged(hasFocus);
//		if (!hasFocus) {
//			Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
//			this.sendBroadcast(closeDialog);
//		}
//	}
}
