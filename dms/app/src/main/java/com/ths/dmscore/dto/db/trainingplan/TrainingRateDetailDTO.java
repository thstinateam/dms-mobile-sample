package com.ths.dmscore.dto.db.trainingplan;

/**
 * 
 * TraningRateDetailDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:43:13 17-11-2014
 */
public class TrainingRateDetailDTO {
	private int trainingRateDetailId;
	private int trainingRateId;
	private String code;
	private String shortName;
	private String fullName;
	private int orderNumber;
	private int isDefaultRate;
	private String createUser;
	private String updateUser;
	private String createDate;
	private String updateDate;

	public int getTraningRateDetailId() {
		return trainingRateDetailId;
	}

	public void setTraningRateDetailId(int trainingRateDetailId) {
		this.trainingRateDetailId = trainingRateDetailId;
	}

	public int getTraningRateId() {
		return trainingRateId;
	}

	public void setTraningRateId(int trainingRateId) {
		this.trainingRateId = trainingRateId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public int getIsDefaultRate() {
		return isDefaultRate;
	}

	public void setIsDefaultRate(int isDefaultRate) {
		this.isDefaultRate = isDefaultRate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}