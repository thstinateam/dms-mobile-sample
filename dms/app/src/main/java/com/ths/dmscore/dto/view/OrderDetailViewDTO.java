/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.db.SalePromoMapDTO;
import com.ths.dmscore.view.sale.order.OrderPromotionRow;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;

/**
 *  Thong tin chi tiet don hang tren view
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class OrderDetailViewDTO implements Serializable {
	private static final long serialVersionUID = 1893687628115519475L;
	public static final int FREE_PRODUCT = 1;
    public static final int FREE_PRICE = 2;
    public static final int FREE_PERCENT = 3;
    // KM cho mat hang ban
    public static final int PROMOTION_FOR_PRODUCT = 0;
    // KM cho don hang (type promotion ZV019, ZV020)
    public static final int PROMOTION_FOR_ORDER = 1;
    // KM cho don hang (type promotion ZV021)
    public static final int PROMOTION_FOR_ZV21 = 2;
    // san pham khuyen mai cho don hang (type promotion ZV021)
    public static final int PROMOTION_FOR_ORDER_TYPE_PRODUCT = 3;
    // KM cho don hang (type promotion accumulation money)
    public static final int PROMOTION_ACCUMULCATION_MONEY = 4;
    // KM cho don hang (type promotion accumulation header product)
    public static final int PROMOTION_ACCUMULCATION_HEADER_PRODUCT = 5;
    // san pham khuyen mai cho don hang (type promotion product in)
    public static final int PROMOTION_ACCUMULCATION_PRODUCT = 6;
    // KM cho don hang (type promotion accumulation money)
    public static final int PROMOTION_NEW_OPEN_MONEY = 7;
    // KM cho don hang (type promotion accumulation header product)
    public static final int PROMOTION_NEW_OPEN_HEADER_PRODUCT = 8;
    // san pham khuyen mai cho don hang (type promotion product in)
    public static final int PROMOTION_NEW_OPEN_PRODUCT = 9;
    // tra thuong keyshop dang product
    public static final int PROMOTION_KEYSHOP_PRODUCT = 10;
    // tra thuong keyshop dang tien
    public static final int PROMOTION_KEYSHOP_MONEY = 11;

	// stt o ngoai
	public int indexParent;
	// stt ben trong: zv21
	public int indexChild;
	// thong tin chi tiet don hang
	public SaleOrderDetailDTO orderDetailDTO;
	// thanh tien = don gia * so luong
	public double totalAmount;
	// ma mat hang
	public String productCode = "";
	// ten mat hang
	public String productName = "";
	// don vi quy doi tinh toan
	public int convfact = 1;
	// so luong dat hien thi tren view: vi du 2/4
	public String quantity;
	// mat hang trong tam hay ko?
	public int isFocus;

	public int type;//1: tra thuong sp, 2: tra thuong tien, 3: tra thuong % tien
	public String typeName;
	public long productPromoId;
	public int changeProduct = 0;//1: la duoc lua chon
	public Long keyList;
	public long quantityEdit;
	public long stock; //ton kho
	public long stockActual; //ton kho thuc te
	// ton kho theo dang format thung/hop
	public String remaindStockFormat;
	public String remaindStockFormatActual;
	public OrderDetailViewDTO totalOrderQuantity;//Tong so luong cua sp trong trong don hang, gom: ban, KM tu dong, KM = tay
	public double grossWeight;//Trong luong cua san pham

	// bien dung de kiem tra co phai chon tu man hinh them hang khong --> dung cho ds san pham khuyen mai
//	public boolean isSelectFromAddProduct = false;
	//Loai KM: KM sp, KM don hang -> render layout
	public int promotionType = 0;
	public String promotionDescription = "";

	// ds mat hang KM cua CTKM 21
	public ArrayList<OrderDetailViewDTO> listPromotionForPromo21 = new ArrayList<OrderDetailViewDTO>();
	public double oldDiscountAmount = 0;
		//Ds Id san pham thuc te mua
//	public ArrayList<Long> listIdProductOfGroup;
	// So luong san pham ban qui dinh trong group de dung cho truong isOwner
	public int numBuyProductInGroup;
	public ArrayList<SaleOrderPromoDetailDTO> listPromoDetail = new ArrayList<SaleOrderPromoDetailDTO>();
	public int isEdited;
	public int oldIsEdited;
	public ArrayList<GroupLevelDetailDTO> listBuyProduct = new ArrayList<GroupLevelDetailDTO>();
	public boolean isBundle;
	public long stockId;
	public boolean showChangePromotionOrder;
//	//Id cua group KM
//	public long productGroupId;
//	//Id cua level KM
//	public long groupLevelId;
	//Id muc con
	public long subLevelId;
	//So suat
	public SaleOrderPromotionDTO quantityReceived;
	//Loai don hang
	public String orderType;
	public OrderPromotionRow row;
	//Thong tin ban rpt cttl pay
	public RptCttlPayDTO rptCttlPay;
	//Co cho phep xoa KM tich luy sp hay ko?
	public boolean allowDeleteAccumulation;
	//So tien Tich luy con lai duoc huong
	public long remainDiscount;
	//CTKM co khai bao so suat hay ko
	public boolean isHasQuantityReceived;
	//Khong lay lan dat KM nay
	public boolean notGetThisPromotion;
	public boolean isModifyPrice = false;
	public boolean isModifyPackagePrice = false;
	//Lan dat KM tich luy nay ko co sp nao co so luong ca, ko lay
	public boolean notUsedAccumulationProduct;
	// luu thong tin cac CTKM ma san pham dat hay ko dat
	public ArrayList<SalePromoMapDTO> lstPromotionMap = new ArrayList<SalePromoMapDTO>();
	// luu cac thong tin lien quan toi keyshop
	public long keyshopProductNum;
	public long keyshopProductNumDone;
	public double keyshopRewardMoney;
	public double keyshopRewardMoneyDone;


	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param quantityReceivedList
	 */
	public void getQuantityReceived(ArrayList<SaleOrderPromotionDTO> quantityReceivedList) {
		if(orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO
				|| orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_ORDER
				|| orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_ACCUMULATION
				|| orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_NEW_OPEN
				|| orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP) {
			for(SaleOrderPromotionDTO received: quantityReceivedList) {
				if(orderDetailDTO.programeCode != null && orderDetailDTO.programeCode.equals(received.promotionProgramCode) &&
						orderDetailDTO.productGroupId == received.productGroupId &&
						orderDetailDTO.groupLevelId == received.groupLevelId) {
					quantityReceived = received;
					break;
				}
			}
		} else if(orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
				orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
				orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
				orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
				) {
			for(SaleOrderPromotionDTO received: quantityReceivedList) {
				if(orderDetailDTO.programeCode != null && orderDetailDTO.programeCode.equals(received.promotionProgramCode)) {
					quantityReceived = received;
					break;
				}
			}
		}
	}

	/**
	 * Clone thong tin KM tich luy
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: OrderDetailViewDTO
	 * @throws:
	 * @return
	 */
	public OrderDetailViewDTO cloneAccumulationPromotion() {
		//Thong tin KM
		OrderDetailViewDTO detailView = new OrderDetailViewDTO();
		SaleOrderDetailDTO selectedOrderDetail = new SaleOrderDetailDTO();
		detailView.orderDetailDTO = selectedOrderDetail;
		detailView.promotionType = promotionType;
		selectedOrderDetail.isFreeItem = orderDetailDTO.isFreeItem;
		selectedOrderDetail.programeId = orderDetailDTO.programeId;
		selectedOrderDetail.programeCode = orderDetailDTO.programeCode;
		selectedOrderDetail.programeName = orderDetailDTO.programeName;
		selectedOrderDetail.programeType = orderDetailDTO.programeType;
		selectedOrderDetail.promotionOrderNumber = orderDetailDTO.promotionOrderNumber;
		selectedOrderDetail.programeTypeCode = orderDetailDTO.programeTypeCode;
		//Dung cho viec kiem tra sua so luong = 0 -> so suat = 0
		selectedOrderDetail.productGroupId = orderDetailDTO.productGroupId;
		selectedOrderDetail.groupLevelId = orderDetailDTO.groupLevelId;

		detailView.isEdited = isEdited;
		detailView.oldIsEdited = oldIsEdited;
		detailView.isHasQuantityReceived = isHasQuantityReceived;
		detailView.numBuyProductInGroup = numBuyProductInGroup;
		detailView.listBuyProduct = listBuyProduct;
		detailView.isBundle = isBundle;

		//Thong tin quan trong cua KM
		selectedOrderDetail.discountPercentage = orderDetailDTO.discountPercentage;
		selectedOrderDetail.setDiscountAmount(orderDetailDTO.getDiscountAmount());
		selectedOrderDetail.maxAmountFree = orderDetailDTO.maxAmountFree;
		detailView.type = type;
		detailView.typeName = typeName;

		//Thong tin pay
		detailView.rptCttlPay = rptCttlPay.clone();

		return detailView;
	}

	/**
	 * Reset cac gia tri tich luy da su dung
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void resetValueUsing() {
		orderDetailDTO.discountPercentage = 0;
		orderDetailDTO.setDiscountAmount(0);
		orderDetailDTO.maxAmountFree = 0;

		rptCttlPay.resetValueUsing();
	}

	/**
	 * quy doi tu thung/le -> le
	 *
	 * @author: dungdq3
	 * @since: 2:29:02 PM Apr 8, 2015
	 * @return: long
	 * @throws:
	 * @return
	 */
	public long exchangeQuantity() {
//		return (long) ((orderDetailDTO.quantityPackage * convfact) + orderDetailDTO.quantityRetail);
		return (long) ((orderDetailDTO.quantityPackage * convfact) + orderDetailDTO.quantityRetail);
	}

	public String quantityStr() {
		if(orderDetailDTO.quantityPackage == 0){
			return orderDetailDTO.quantityRetail + "";
		}
		return orderDetailDTO.quantityPackage + "/" + orderDetailDTO.quantityRetail;
	}
	
	public String quantityProductStr() {
		return orderDetailDTO.quantityPackage + "/" + orderDetailDTO.quantityRetail;
	}
	
}
