/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.constants.Constants;

/**
 * bao cao tien do luy ke theo nganh hang
 * 
 * @author: DungNT19
 * @version: 1.1
 * @since: 1.0
 */
public class SupervisorReportStaffSaleViewDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	// so ngay ke hoach trong thang
	public int numPlanDate;
	// so ngay da ban trong thang
	public int numPlanDateDone;
	// tien do ban hang
	public double progressSales;
	// chu ky
	public int numCycle = 0;
	// ngay bat dau chu ky
	public String beginDate = "";
	// ngay ket thuc chu ky
	public String endDate = "";
	// list report info
	public ArrayList<SupervisorReportStaffSaleItemDTO> listFocusInfoRow = new ArrayList<SupervisorReportStaffSaleItemDTO>();
	// row info report total
	public SupervisorReportStaffSaleItemDTO objectReportTotal = new SupervisorReportStaffSaleItemDTO();
	public ArrayList<String> arrMMTTText;

	public SupervisorReportStaffSaleViewDTO() {
		numPlanDate = 0;
		numPlanDateDone = 0;
		progressSales = 0;
		numCycle = 0;
		beginDate = Constants.STR_BLANK;
		endDate = Constants.STR_BLANK;
		listFocusInfoRow = new ArrayList<SupervisorReportStaffSaleItemDTO>();
		objectReportTotal = new SupervisorReportStaffSaleItemDTO();
		arrMMTTText = new ArrayList<String>();
	}
	
}
