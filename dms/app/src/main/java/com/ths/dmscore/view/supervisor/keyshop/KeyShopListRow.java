/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Row danh sach keyshop role GSBH
 * 
 * @author: yennth16
 * @version: 1.0
 * @since: 10:38:26 10-07-2015
 */
public class KeyShopListRow extends DMSTableRow implements OnClickListener {
	private TextView tvSTT;
	private TextView tvKeyshopCode;
	private TextView tvKeyshopName;
	private TextView tvFromCycle;
	private TextView tvToCycle;
	private TextView tvContent;

	KeyShopItemDTO data;

	public KeyShopListRow(Context context, VinamilkTableListener listener) {
		super(context, R.layout.layout_keyshop_row);
		setOnClickListener(this);
		setListener(listener);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvKeyshopCode = (TextView) findViewById(R.id.tvKeyshopCode);
		tvKeyshopCode.setOnClickListener(this);
		tvKeyshopName = (TextView) findViewById(R.id.tvKeyshopName);
		tvFromCycle = (TextView) findViewById(R.id.tvFromCycle);
		tvToCycle = (TextView) findViewById(R.id.tvToCycle);
		tvContent = (TextView) findViewById(R.id.tvContent);
	}

	/**
	 * render du lieu
	 * @author: yennth16
	 * @since: 08:18:38 14-07-2015
	 * @return: void
	 * @throws:  
	 * @param item
	 * @param position
	 */
	public void render(KeyShopItemDTO item, int position) {
		this.data = item;
		tvSTT.setText("" + position);
		tvKeyshopCode.setText(item.ksCode);
		tvKeyshopName.setText(item.name);
		tvFromCycle.setText(item.fromCycleId);
		tvToCycle.setText(item.toCycleId);
		tvContent.setText(item.description);
	}

	@Override
	public void onClick(View v) {
		if (v == tvKeyshopCode) {
			listener.handleVinamilkTableRowEvent(
					ActionEventConstant.GO_TO_KEY_SHOP_DETAIL_VIEW, null,
					this.data);
		}
	}
}
