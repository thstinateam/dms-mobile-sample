/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ProductGroupDTO;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class PRODUCT_GROUP_TABLE extends ABSTRACT_TABLE {

	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String PRODUCT_GROUP_CODE = "PRODUCT_GROUP_CODE";
	public static final String PRODUCT_GROUP_NAME = "PRODUCT_GROUP_NAME";
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	public static final String GROUP_TYPE = "GROUP_TYPE";
	public static final String MIN_QUANTITY = "MIN_QUANTITY";
	public static final String MAX_QUANTITY = "MAX_QUANTITY";
	public static final String MIN_AMOUNT = "MIN_AMOUNT";
	public static final String MAX_AMOUNT = "MAX_AMOUNT";
	public static final String MULTIPLE = "MULTIPLE";
	public static final String RECURSIVE = "RECURSIVE";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	private static final String TABLE_NAME = "PRODUCT_GROUP_TABLE";
	public PRODUCT_GROUP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PRODUCT_GROUP_ID, PRODUCT_GROUP_CODE, PRODUCT_GROUP_NAME,
				PROMOTION_PROGRAM_ID, GROUP_TYPE, MIN_QUANTITY, MAX_QUANTITY, 
				MIN_AMOUNT,MAX_AMOUNT, MULTIPLE, RECURSIVE, ORDER_NUMBER, 
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	/**
	 * Lay ds nhom CTKM
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<ProductGroupDTO>
	 * @throws:  
	 * @param promotionId
	 * @return
	 * @throws Exception 
	 */
	public ArrayList<ProductGroupDTO> getProductGroupsOfPromotionProgram(long promotionId, int groupType, int isPromotionProduct) throws Exception {
		ArrayList<ProductGroupDTO> result = new ArrayList<ProductGroupDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT ");
		sql.append("	PG.* ");
		sql.append("	FROM ");
		sql.append("	PRODUCT_GROUP PG, ");
		sql.append("	GROUP_LEVEL GL ");
		sql.append("	WHERE ");
		sql.append("	PG.PROMOTION_PROGRAM_ID = ? ");
		params.add(String.valueOf(promotionId));
		sql.append("	AND PG.PRODUCT_GROUP_ID = GL.PRODUCT_GROUP_ID ");
		sql.append("	AND PG.STATUS = 1 AND GL.STATUS = 1     ");
		sql.append("	AND PG.GROUP_TYPE = ? ");
		params.add(String.valueOf(groupType));
		sql.append("	AND GL.HAS_PRODUCT = ? ");
		params.add(String.valueOf(isPromotionProduct));
		sql.append("	GROUP BY PG.PRODUCT_GROUP_ID ");
		sql.append("	ORDER BY PG.ORDER_NUMBER  ");
		
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ProductGroupDTO dto = new ProductGroupDTO();
						dto.initFromCursor(c);
						result.add(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.d("getProductGroupsOfPromotionProgram: ", e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		
		return result;
	}
	/**
	 * Lay thong tin cua 1 group
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<GroupLevelDTO>
	 * @throws:  
	 * @param groupLevelId
	 * @return
	 */
	public ProductGroupDTO getProductGroupInfo(long groupId) {
		ProductGroupDTO result = new ProductGroupDTO();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT * FROM PRODUCT_GROUP PG ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("		AND PG.STATUS = 1 ");
		sql.append("		AND PG.PRODUCT_GROUP_ID = ? ");
		params.add(String.valueOf(groupId));
		
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					result.initFromCursor(c);
				}
			}
		} catch (Exception e) {
			MyLog.d("getProductGroupInfo: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		return result;
	}
}
