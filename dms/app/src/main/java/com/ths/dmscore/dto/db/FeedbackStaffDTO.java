/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.FEEDBACK_STAFF_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.util.GlobalUtil;

/**
 * FeedbackStaffDTO.java
 * @author: yennth16
 * @version: 1.0
 * @since:  09:59:45 21-05-2015
 */
public class FeedbackStaffDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 7170964120955772567L;
	// id bang
	public long feedBackStaffId;
	// id feedback
	public long feedBackId;
	// if staff
	public long staffId;
	// ket qua
	public int result;
	// ngay thuc hien
	public String doneDate;
	// so lan yeu cau lam lai
	public int numReturn;
	// id huan luyen
	public long trainingPlanDetailId;
	// nguoi tao
	public String createUserId;
	// ngay tao
	public String createDate;
	// nguoi cap nhat
	public String updateUser;
	// ngay cap nhat
	public String updateDate;


	 /**
	 * insert feeback staff
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateFeedbackStaffSql() {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_ID, feedBackStaffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.FEEDBACK_ID, feedBackId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.CREATE_USER_ID, createUserId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.CREATE_DATE, createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.RESULT, result, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.NUM_RETURN, numReturn, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return feedbackJson;
	}


}
