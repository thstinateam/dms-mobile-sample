/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;

/**
 * TrainingRateDetailDTO.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 4:03:24 PM Nov 14, 2014
 */
public class TrainingRateDetaiResultlDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	// ma tieu chi
	private long trainingRateID;
	// ma tieu chuan
	private long trainingRateDetailID;
	private long subTrainingCustomerID;
	// ten viet tat de hien thi len tab
	private String shortName;
	// ten fullDate du
	private String fullName;
	// so thu tu de hien thi
	private int orderNumber;
	// ket qua cham: 0: chua cham, 1: ko ap dung, 2: dat, 3: ko dat
	private int result;
	// nguoi tao
	private String createUser;
	// ngay tao
	private String createDate;
	// nguoi cap nhat
	private String updateUser;
	// ngay cap nhat
	private String updateDate;

	public TrainingRateDetaiResultlDTO() {
		// TODO Auto-generated constructor stub
		trainingRateID = 0;
		trainingRateDetailID = 0;
		orderNumber = 0;
		subTrainingCustomerID = 0;
		result = 0;
		shortName = Constants.STR_BLANK;
		fullName = Constants.STR_BLANK;
		createUser = Constants.STR_BLANK;
		createDate = Constants.STR_BLANK;
		updateDate = Constants.STR_BLANK;
		updateUser = Constants.STR_BLANK;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public long getTrainingRateID() {
		return trainingRateID;
	}

	public void setTrainingRateID(long trainingRateID) {
		this.trainingRateID = trainingRateID;
	}

	public long getTrainingRateDetailID() {
		return trainingRateDetailID;
	}

	public void setTrainingRateDetailID(long trainingRateDetailID) {
		this.trainingRateDetailID = trainingRateDetailID;
	}

	public long getSubTrainingCustomerID() {
		return subTrainingCustomerID;
	}

	public void setSubTrainingCustomerID(long subTrainingCustomerID) {
		this.subTrainingCustomerID = subTrainingCustomerID;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}
