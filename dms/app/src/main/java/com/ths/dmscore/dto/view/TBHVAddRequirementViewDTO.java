package com.ths.dmscore.dto.view;

import java.util.List;

public class TBHVAddRequirementViewDTO {
	public List<DisplayProgrameItemDTO> listNVBH;
	public List<DisplayProgrameItemDTO> listTypeProblem;
	
	public TBHVAddRequirementViewDTO() {
		
	}
}
