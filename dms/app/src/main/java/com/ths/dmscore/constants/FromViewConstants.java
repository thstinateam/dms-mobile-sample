/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;


/**
 *  Define hang so cho biet di tu man hinh nao
 *  @author: dungnt19
 *  @version: 1.0
 *  @since: 1.0
 */
public class FromViewConstants {
	public static final int FROM_INVENTORY_EQUIPMENT_LIST = 1;
	public static final int FROM_REPORT_LOST_EQUIPMENT_LIST = 2;
}
