package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

@SuppressWarnings("serial")
public class RemainProductViewDTO implements Serializable{

	public int stt;
	private int PRODUCT_ID;
	private String PRODUCT_NAME;
	public long VAT;
	public float GROSS_WEIGHT;
	private int QUANTITY;
	public String REMAIN_NUMBER="";
	private String HINT_NUMBER="0";
	private boolean isCheck = false;
	private String PRODUCT_CODE;
	private int QUANTITY_REMAIN;//so luong ton kho cua mat hang
	private int HAS_REMAIN;
	private long CUSTOMER_ID;
	private int STAFF_ID;
	private long SALE_ORDER_ID;
	public int CONVFACT;
	public int sale;
	public long stock;
	public long stockActual;
	public int ordinal;
	public long getSALE_ORDER_ID() {
		return SALE_ORDER_ID;
	}

	public void setSALE_ORDER_ID(long sALE_ORDER_ID) {
		SALE_ORDER_ID = sALE_ORDER_ID;
	}

	public int getQUANTITY() {
		return QUANTITY;
	}

	public void setQUANTITY(int qUANTITY) {
		QUANTITY = qUANTITY;
	}

	public String getPRODUCT_CODE() {
		return PRODUCT_CODE;
	}

	public void setPRODUCT_CODE(String pRODUCT_CODE) {
		PRODUCT_CODE = pRODUCT_CODE;
	}

	public int getHAS_REMAIN() {
		return HAS_REMAIN;
	}

	public void setHAS_REMAIN(int hAS_REMAIN) {
		HAS_REMAIN = hAS_REMAIN;
	}

	public long getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(long cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public int getSTAFF_ID() {
		return STAFF_ID;
	}

	public void setSTAFF_ID(int sTAFF_ID) {
		STAFF_ID = sTAFF_ID;
	}

	public static List<RemainProductViewDTO> initListDataFromCursor(Cursor cursor)
			throws Exception {

		List<RemainProductViewDTO> listData = new ArrayList<RemainProductViewDTO>();
		if (cursor == null) {
			throw new Exception("Cursor is empty");
		}

		if (cursor.moveToFirst()) {
			do {
				RemainProductViewDTO rp = new RemainProductViewDTO();
				rp.initDataFromCursor(cursor);
				listData.add(rp);

			} while (cursor.moveToNext());
		}

		return listData;
	}

	public void initDataFromCursor(Cursor c)
			throws Exception {
		PRODUCT_ID= CursorUtil.getInt(c, "PRODUCT_ID");
		PRODUCT_CODE = CursorUtil.getString(c,"PRODUCT_CODE");
		PRODUCT_NAME = CursorUtil.getString(c,"PRODUCT_NAME");
		GROSS_WEIGHT = CursorUtil.getFloat(c,"GROSS_WEIGHT");
		CONVFACT = CursorUtil.getInt(c,"CONVFACT");
	}
	public int getPRODUCT_ID() {
		return PRODUCT_ID;
	}

	public void setPRODUCT_ID(int product) {
		PRODUCT_ID = product;
	}

	public String getPRODUCT_NAME() {
		return PRODUCT_NAME;
	}

	public void setPRODUCT_NAME(String cUSTOMER_NAME) {
		PRODUCT_NAME = cUSTOMER_NAME;
	}

	public int getQuantity() {
		return QUANTITY;
	}

	public void setQuantity(int quanlity) {
		QUANTITY = quanlity;
	}

	public String getHINT_NUMBER() {
		return HINT_NUMBER;
	}

	public void setHINT_NUMBER(String hINT_NUMBER) {
		HINT_NUMBER = hINT_NUMBER;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
		    return false;
		}

		if (this.getClass() != o.getClass()) {
		    return false;
		}
		
		if (PRODUCT_ID == ((RemainProductViewDTO)o).PRODUCT_ID) {
			return true;
		}
		return false;
	}

	public int getQUANTITY_REMAIN() {
		return QUANTITY_REMAIN;
	}

	public void setQUANTITY_REMAIN(int qUANTITY_REMAIN) {
		QUANTITY_REMAIN = qUANTITY_REMAIN;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
