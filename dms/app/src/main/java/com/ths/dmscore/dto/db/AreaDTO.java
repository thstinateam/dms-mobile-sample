/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

/**
 *  Thong tin dia ban
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class AreaDTO extends AbstractTableDTO {
	// ma dia ban
	public String areaCode ;
	// ten dia ban
	public String areaName ; 
	// loai dia ban
	public String type ;
	// ma dia ban cha
	public String parentCode ; 
	// ma tinh
	public String province ; 
	// ten tinh
	public String provinceName ; 
	// ma huyen
	public String district ; 
	// ten huyen
	public String districtName ; 
	// ma xa
	public String precinct ;
	// ten xa
	public String precinctName ;
	// trang thai 0: het hieu luc, 1: hieu luc
	public int status;
	// ma vung
	public String centerCode;
	// thoi gian update
	public String updateTime;
	// thoi gian tao
	public String createTime;
	// nguoi update
	public String updateUser;
	// nguio tao
	public String createUser;
	
	public AreaDTO(){
		super(TableType.AREA_TABLE);
	}
}
