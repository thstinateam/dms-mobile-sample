package com.ths.dmscore.dto.view;

import java.util.ArrayList;

public class TotalStatisticsDto {
	public String productName;
	public String productCode;
	public String[] productType;
	public String[] productSubType;
	public int quantity;
	public int available_quantity;
	public ArrayList<TotalStatisticsItem> arrList = new ArrayList<TotalStatisticsItem>();
	
	public TotalStatisticsDto() {
	}
}
