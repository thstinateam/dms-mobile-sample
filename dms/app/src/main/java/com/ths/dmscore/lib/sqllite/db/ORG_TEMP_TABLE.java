/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.OrgTempDTO;
import com.ths.dmscore.dto.view.ListOrgTempDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * bang tam dung de luu nhan vien nao quan ly shop nao
 *
 * @author: dungdq3
 * @version: 1.0
 * @since:  4:27:10 PM Mar 18, 2015
 */
public class ORG_TEMP_TABLE extends ABSTRACT_TABLE {

	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// id shop
	public static final String SHOP_ID = "SHOP_ID";

	public static final String ORG_TEMP_ID = "ORG_TEMP_ID";

	public static final String ORG_TEMP_TABLE = "ORG_TEMP";

	public ORG_TEMP_TABLE(SQLiteDatabase mDB) {
		// TODO Auto-generated constructor stub
		this.tableName = ORG_TEMP_TABLE;
		this.columns = new String[] { STAFF_ID, SHOP_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * lay du lieu tu org access
	 *
	 * @author: dungdq3
	 * @since: 4:33:50 PM Mar 18, 2015
	 * @return: void
	 * @param staffOwnerId
	 * @param shopId
	 * @throws Exception
	 */
	public ListOrgTempDTO getDataFromOrgAcces(String staffOwnerId, String shopId) throws Exception{
		ArrayList<String> params = new ArrayList<String>();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		Cursor c = null;
		ListOrgTempDTO listOrgTemp = new ListOrgTempDTO();
		ArrayList<String> listShopChild = staff.getListShopChild(shopId);
		if (listShopChild != null && listShopChild.size() > 0 ) {
			ArrayList<String> listStaff = staff.getListStaffInParentStaffMap(
					staffOwnerId, shopId);
			if (listStaff == null || listStaff.size() == 0) {
				listStaff = staff.getListStaffInShop(shopId, staffOwnerId);
			}
			for(String staffTemp : listStaff) {
				OrgTempDTO orgTemp = new OrgTempDTO();
				orgTemp.shopID = shopId;
				orgTemp.staffID = staffTemp;
				listOrgTemp.addItem(orgTemp);
			}
			for (String str : listShopChild) {
				params.clear();
				ArrayList<String> listStaffId = new ArrayList<String>();
				try {
					StringBuffer varname1 = new StringBuffer();
					varname1.append(" SELECT DISTINCT ru.user_id as STAFF_ID ");
					varname1.append(" FROM   role_user ru, ");
					varname1.append("       role_permission_map rpm, ");
					varname1.append("       org_access oa ");
					varname1.append(" WHERE  1 = 1 ");
					varname1.append("       AND oa.status = 1 ");
					varname1.append("       AND rpm.status = 1 ");
					varname1.append("       AND ru.status = 1 ");
					varname1.append("       AND ru.role_id = rpm.role_id ");
					varname1.append("       AND rpm.permission_id = oa.permission_id ");
					varname1.append("       AND oa.shop_id = ? ");
					params.add(str);
					varname1.append("       AND NOT EXISTS (SELECT 1 ");
					varname1.append("                       FROM   organization_except oe ");
					varname1.append("                       WHERE  oe.org_id = (SELECT organization_id ");
					varname1.append("                                           FROM   staff ");
					varname1.append("                                           WHERE  staff_id = ?) ");
					params.add(staffOwnerId);
					varname1.append("                              AND org_except_id = (SELECT organization_id ");
					varname1.append("                                                   FROM   staff ");
					varname1.append("                                                   WHERE  staff_id = ru.user_id) ");
					varname1.append("                      			AND oe.status = 1 ");
					varname1.append("                      )");

					c = rawQueries(varname1.toString(), params);
					if (c != null) {
						if (c.moveToFirst()) {
							do {
								String temp = CursorUtil.getString(c, "STAFF_ID");
								listStaffId.add(temp);
							} while (c.moveToNext());
						}
					}
				} finally {
					try {
						if (c != null) {
							c.close();
						}
					} catch (Exception e) {
					}
				}
				if (listStaffId.size() > 0) {
					for(String staffTemp : listStaffId) {
						OrgTempDTO orgTemp = new OrgTempDTO();
						orgTemp.shopID = str;
						orgTemp.staffID = staffTemp;
						listOrgTemp.addItem(orgTemp);
					}
				} else if (listStaffId.size() <= 0) {
					ListOrgTempDTO listTemp = getDataFromOrgAcces(staffOwnerId, str);
					if(listTemp != null && listTemp.listOrgTemp.size() > 0)
						listOrgTemp.listOrgTemp.addAll(listTemp.listOrgTemp);
				}
			}
		} else {
			String listStaff = staff.getListStaffOfSupervisor(staffOwnerId, shopId);
			if (!StringUtil.isNullOrEmpty(listStaff)) {
				String[] listStaffID = listStaff.split(",");
				for (String staffTemp : listStaffID) {
					OrgTempDTO orgTemp = new OrgTempDTO();
					orgTemp.shopID = shopId;
					orgTemp.staffID = staffTemp;
					listOrgTemp.addItem(orgTemp);
				}
			}
		}

		return listOrgTemp;
	}

	/**
	 * insert data into DB
	 *
	 * @author: dungdq3
	 * @since: 4:56:00 PM Mar 18, 2015
	 * @return: int
	 * @throws:
	 * @param listOrgTemp
	 * @return
	 */
	public long insertIntoTable(ListOrgTempDTO listOrgTemp) {
		// TODO Auto-generated method stub
		long success = 0;
		TABLE_ID table = new TABLE_ID(mDB);
		long id = table.getMaxIdTime(tableName);
		for (OrgTempDTO dto : listOrgTemp.listOrgTemp) {
			ContentValues cv = new ContentValues();
			cv.put(ORG_TEMP_ID, id);
			cv.put(STAFF_ID, dto.staffID);
			cv.put(SHOP_ID, dto.shopID);
			success = insert(null, cv);
			if(success <= 0){
				break;
			} else{
				id ++;
			}
		}
		return success;
	}

}
