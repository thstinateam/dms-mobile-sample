/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.MediaItemDTO;

/**
 * display present product info
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class VoteDisplayPresentProductViewDTO implements Serializable {
	private static final long serialVersionUID = -2235749524820312020L;
	// list display programe info
	public ArrayList<DisplayPresentProductInfo> listDisplayProgrameInfo = new ArrayList<DisplayPresentProductInfo>();
	// list product display vote
	public ArrayList<VoteDisplayProductDTO> listProductDisplay = new ArrayList<VoteDisplayProductDTO>();
	// ds hinh anh cua product
	public ArrayList<MediaItemDTO> listImageDisplay = new ArrayList<MediaItemDTO>();

	public ArrayList<VoteDisplayProductDTO> listProductHasChangeVote = new ArrayList<VoteDisplayProductDTO>();
	// total product
	public int totalProductDisplay = 0;
	// number display used voted
	public int numDisplayVoted = -1;
	// k.c cho phep dat hang, cham trung bay
	public double shopDistance;
	// so lan ghe tham trong thang
	public int monthSeq;

	public VoteDisplayPresentProductViewDTO() {
		totalProductDisplay = 0;
		numDisplayVoted = -1;
	}
}
