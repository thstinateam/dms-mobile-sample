/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.db.TRAINING_RATE_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * TrainingRateDTO.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 11:28:42 AM Nov 17, 2014
 */
public class TrainingRateDTO implements Serializable {
	private static final long serialVersionUID = 8936312640798996827L;
	// ma tieu chi
	private long trainingRateID;
	// ten viet tat de hien thi len tab
	private String shortCriteriaName;
	// ten fullDate du
	private String fullCriteriaName;
	// so thu tu de hien thi
	private int orderNumber;
	// nguoi tao
	private String createUser;
	// ngay tao
	private String createDate;
	// nguoi cap nhat
	private String updateUser;
	// ngay cap nhat
	private String updateDate;

	public TrainingRateDTO() {
		// TODO Auto-generated constructor stub
		trainingRateID = 0;
		orderNumber = 0;
		shortCriteriaName = Constants.STR_BLANK;
		fullCriteriaName = Constants.STR_BLANK;
		createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		createDate = DateUtils.now();
		updateUser = Constants.STR_BLANK;
		updateDate = Constants.STR_BLANK;
	}

	public long getTrainingRateID() {
		return trainingRateID;
	}

	public void setTrainingRateID(long trainingRateID) {
		this.trainingRateID = trainingRateID;
	}

	public String getShortCriteriaName() {
		return shortCriteriaName;
	}

	public void setShortCriteriaName(String shortCriteriaName) {
		this.shortCriteriaName = shortCriteriaName;
	}

	public String getFullCriteriaName() {
		return fullCriteriaName;
	}

	public void setFullCriteriaName(String fullCriteriaName) {
		this.fullCriteriaName = fullCriteriaName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * khoi tao du lieu tu cursor
	 * 
	 * @author: dungdq3
	 * @since: 11:32:52 AM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		trainingRateID = CursorUtil.getLong(c, TRAINING_RATE_TABLE.TRAINING_RATE_ID);
		shortCriteriaName = CursorUtil.getString(c, TRAINING_RATE_TABLE.SHORT_NAME);
		fullCriteriaName = CursorUtil.getString(c, TRAINING_RATE_TABLE.FULL_NAME);
		orderNumber = CursorUtil.getInt(c, TRAINING_RATE_TABLE.ORDER_NUMBER);
		
	}

}
