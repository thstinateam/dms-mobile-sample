/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;

/**
 * SubTrainingPlanDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:41:14 PM Nov 14, 2014
 */
public class SubTrainingPlanDTO implements
		Serializable {

	private static final long serialVersionUID = 1L;

	// ma tbhv
	private long subStaffID;
	//ma nv huan luyen
	private long staffID;
	// shop id
	private long shopID;
	// thang huan luyen
	private int trainingMonth;
	// ngay huan luyen
	private String trainingDate;
	// ngay tao
	private String createDate;
	// ngay cap nhat
	private String updateDate;
	// nguoi tao
	private String createUser;
	// nguoi cap nhat
	private String updateUser;
	
	public SubTrainingPlanDTO() {
		// TODO Auto-generated constructor stub
		subStaffID = 0;
		staffID = 0;
		shopID = 0;
		trainingMonth = 0;
		trainingDate = Constants.STR_BLANK;
		createDate = Constants.STR_BLANK;
		updateDate = Constants.STR_BLANK;
		createUser = Constants.STR_BLANK;
		updateUser = Constants.STR_BLANK;
	}

	public long getSubStaffID() {
		return subStaffID;
	}

	public void setSubStaffID(long subStaffID) {
		this.subStaffID = subStaffID;
	}

	public long getStaffID() {
		return staffID;
	}

	public void setStaffID(long staffID) {
		this.staffID = staffID;
	}

	public long getShopID() {
		return shopID;
	}

	public void setShopID(long shopID) {
		this.shopID = shopID;
	}

	public int getTrainingMonth() {
		return trainingMonth;
	}

	public void setTrainingMonth(int trainingMonth) {
		this.trainingMonth = trainingMonth;
	}

	public String getTrainingDate() {
		return trainingDate;
	}

	public void setTrainingDate(String trainingDate) {
		this.trainingDate = trainingDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

}
