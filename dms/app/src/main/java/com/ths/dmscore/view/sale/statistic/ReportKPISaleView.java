package com.ths.dmscore.view.sale.statistic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.view.ReportKPIViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.dto.view.ReportKPIItemDTO;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;


/**
 * Bao cao KPI cho role NVBH
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPISaleView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener {

	// ngay ban hang theo ke hoach
	public TextView tvPlanDate;
	// ngay ban hang da qua
	public TextView tvSoldDate;
	// tien do ban hang
	public TextView tvProgress;
	// table d/s bao cao
	private DMSTableView tbReportKPI;
	// data view
	public ReportKPIViewDTO dto;
	// check done load data the first
	public boolean isFirstLoad = false;

	public static ReportKPISaleView getInstance(Bundle data) {
		ReportKPISaleView f = new ReportKPISaleView();
		f.setArguments(data);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_report_kpi_sale_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BAOCAOKPI);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TEXT_REPORT_KPI_SALE_VIEW));

		// init view for screen
		initView(v);
		if (!this.isFirstLoad) {
			getReportKPI();
			isFirstLoad = true;
		}else{
			renderLayout();
		}

		return v;
	}


	 /**
	 * Khoi tao control cho view
	 * @author: Tuanlt11
	 * @param view
	 * @return: void
	 * @throws:
	*/
	public void initView(View view) {
		tvPlanDate = (TextView) PriUtils.getInstance().findViewByIdGone(view, R.id.tvPlanDate, PriHashMap.PriControl.NVBH_BAOCAOKPI_NGAYBANHANGKEHOACH);
		tvProgress = (TextView) PriUtils.getInstance().findViewByIdGone(view, R.id.tvProgress, PriHashMap.PriControl.NVBH_BAOCAOKPI_TIENDO);
		tvSoldDate = (TextView) PriUtils.getInstance().findViewByIdGone(view, R.id.tvSoldDate, PriHashMap.PriControl.NVBH_BAOCAOKPI_NGAYBANHANGDAQUA);
		PriUtils.getInstance().findViewByIdGone(view, R.id.tvPlanDateTitle, PriHashMap.PriControl.NVBH_BAOCAOKPI_NGAYBANHANGKEHOACH);
		PriUtils.getInstance().findViewByIdGone(view, R.id.tvProgressTitle, PriHashMap.PriControl.NVBH_BAOCAOKPI_TIENDO);
		PriUtils.getInstance().findViewByIdGone(view, R.id.tvSoldDateTitle, PriHashMap.PriControl.NVBH_BAOCAOKPI_NGAYBANHANGDAQUA);

		tbReportKPI = (DMSTableView) view.findViewById(R.id.tbReportKPI);
		tbReportKPI.setListener(this);
		initHeaderTable(tbReportKPI, new ReportKPISaleRow(parent));
	}


	 /**
	 * Lay bao cao KPI
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getReportKPI() {
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		b.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId());
		handleViewEvent(b, ActionEventConstant.GET_REPORT_KPI_SALE, SaleController.getInstance());
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_REPORT_KPI_SALE:
			dto = (ReportKPIViewDTO) modelEvent.getModelData();
			renderLayout();
			parent.closeProgressDialog();
			requestInsertLogKPI(HashMapKPI.NVBH_REPORT_KPI_SALE, e);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 *
	 * render layout
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 10, 2013
	 */
	private void renderLayout() {
		tbReportKPI.clearAllData();
		tvPlanDate.setText(String.valueOf(dto.monthSalePlan));
		tvSoldDate.setText(String.valueOf(dto.soldSalePlan));
		StringUtil.displayPercent(tvProgress, dto.perSalePlan);
		for (ReportKPIItemDTO item : dto.lstReportKPI) {
			// ko show san luong
			if (item.dataType == ReportKPIItemDTO.TYPE_FLOAT
					&& !GlobalInfo.getInstance().isSysShowPrice()) {
			}else{
				ReportKPISaleRow row = new ReportKPISaleRow(parent);
				row.render(item);
				tbReportKPI.addRow(row);
			}
		}

	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				getReportKPI();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}
}
