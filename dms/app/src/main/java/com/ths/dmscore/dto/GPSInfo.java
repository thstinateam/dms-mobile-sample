package com.ths.dmscore.dto;

import java.io.Serializable;

import com.ths.dmscore.global.GlobalInfo;

public class GPSInfo implements Serializable{
	private static final long serialVersionUID = 4728275641384376062L;
	// lat, long
	private double longitude = -1;
	private double latitude = -1;
	// toa do lat, lng khong bi reset 10 phut
	private double lastLongitude = -1;
	private double lastLatitude = -1;
	// info cell
	public String cellId;
	public String MNC;
	public String LAC;
	public String cellType;

	public GPSInfo() {
	}

	// 10.784892,106.68302
	public double getLongtitude() {
		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR() && longitude <= 0) {
			return 106.608;
		} else{
			return longitude;
		}
	}

	public double getLatitude() {
		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR() && latitude <= 0) {
			return 10.8095;
		} else{
			return latitude;
		}
	}

	public void setLongtitude(double longtitude) {
		this.longitude = longtitude;
		if (longtitude > 0) {
			lastLongitude = longtitude;
		}
	}

	public void setLattitude(double lattitude) {
		this.latitude = lattitude;
		if (lattitude > 0) {
			lastLatitude = lattitude;
		}
	}

	// vi tri khong bi reset ve -1,-1 de insert actionlog
	public double getLastLongtitude() {
		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR() && lastLongitude <= 0) {
			return 106.68302;
		} else{
			return lastLongitude;
		}
	}

	// vi tri khong bi reset ve -1,-1 de insert actionlog
	public double getLastLatitude() {
		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR() && lastLatitude <= 0) {
			return 10.784892;
		} else{
			return lastLatitude;
		}
	}
}
