package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cho gia theo nhieu cap
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PriceTempDTO {

	public int priceId; //id gia san pham
	public int productId;// id san pham
	public double vat; // vat sp

	 /**
	 * Khoi tao du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c){
		priceId = CursorUtil.getInt(c, "PRICE_ID");
		productId = CursorUtil.getInt(c, "PRODUCT_ID");
		vat = CursorUtil.getDouble(c, "VAT");
	}


}
