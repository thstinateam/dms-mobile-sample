/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.Constants;

/**
 * tong hop thong tin ban hang cua NVBH
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class RptSaleSummaryDTO extends AbstractTableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// id of table
	private static long rptSaleSummaryId;
	// Tuy vao Object_type de link den bang tuong ung (1: SHOP_ID, 2: STAFF_ID)
	private static volatile String objectId;
	// 1: NPP, 2: STAFF
	private static volatile String objectType;
	// Du lieu cho ngay hoac thang, neu la thang thi chinh la ngay dau thang
	private static volatile String saleDate;
	// 0: OFF, 1: ON (default: 1)
	private static volatile String status;
	// code dung de truy van tuy nghiep vu
	private static volatile String code;
	// ten dung de hien thi tren man hinh
	private static volatile String name;
	// mo ta them, khong hien thi tren man hinh, dung de tham khao
	private static volatile String description;
	// gia tri theo ke hoach
	private static volatile String plan;
	// gia tri thuc te theo tung chi tieu
	private static volatile String actually;
	// 1: theo ngay, 2: Luy ke theo thang
	private static volatile String type;
	// 1: Gia tri luu tru cho tinh toan, 2: dung de hien thi tren tablet
	private static volatile String viewType;
	// Thu tu sap xep cac item tren man hinh
	private static volatile String ordinal;

	public RptSaleSummaryDTO() {
		setRptSaleSummaryId(0);
		setObjectId(Constants.STR_BLANK);
		setObjectType(Constants.STR_BLANK);
		setSaleDate(Constants.STR_BLANK);
		setStatus(Constants.STR_BLANK);
		setCode(Constants.STR_BLANK);
		setName(Constants.STR_BLANK);
		setDescription(Constants.STR_BLANK);
		setPlan(Constants.STR_BLANK);
		setActually(Constants.STR_BLANK);
		setRptType(Constants.STR_BLANK);
		setViewType(Constants.STR_BLANK);
		setOrdinal(Constants.STR_BLANK);
	}

	public static long getRptSaleSummaryId() {
		return rptSaleSummaryId;
	}

	public static void setRptSaleSummaryId(long rptSaleSummaryId) {
		RptSaleSummaryDTO.rptSaleSummaryId = rptSaleSummaryId;
	}
	public static String getObjectId() {
		return objectId;
	}

	public static void setObjectId(String objectId) {
		RptSaleSummaryDTO.objectId = objectId;
	}

	public static String getObjectType() {
		return objectType;
	}

	public static void setObjectType(String objectType) {
		RptSaleSummaryDTO.objectType = objectType;
	}

	public static String getSaleDate() {
		return saleDate;
	}

	public static void setSaleDate(String saleDate) {
		RptSaleSummaryDTO.saleDate = saleDate;
	}

	public static String getStatus() {
		return status;
	}

	public static void setStatus(String status) {
		RptSaleSummaryDTO.status = status;
	}

	public static String getCode() {
		return code;
	}

	public static void setCode(String code) {
		RptSaleSummaryDTO.code = code;
	}

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		RptSaleSummaryDTO.name = name;
	}

	public static String getDescription() {
		return description;
	}

	public static void setDescription(String description) {
		RptSaleSummaryDTO.description = description;
	}

	public static String getPlan() {
		return plan;
	}

	public static void setPlan(String plan) {
		RptSaleSummaryDTO.plan = plan;
	}

	public static String getActually() {
		return actually;
	}

	public static void setActually(String actually) {
		RptSaleSummaryDTO.actually = actually;
	}

	public static String getRptType() {
		return type;
	}

	public static void setRptType(String type) {
		RptSaleSummaryDTO.type = type;
	}

	public static String getViewType() {
		return viewType;
	}

	public static void setViewType(String viewType) {
		RptSaleSummaryDTO.viewType = viewType;
	}

	public static String getOrdinal() {
		return ordinal;
	}

	public static void setOrdinal(String ordinal) {
		RptSaleSummaryDTO.ordinal = ordinal;
	}
}
