/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.EquipStatisticRecordDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.ListEquipInventoryRecordsDTO;
import com.ths.dmscore.util.DateUtils;

/**
 * Bien ban kiem ke
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:10:24 16-12-2014
 */
public class EQUIP_STATISTIC_RECORD_TABLE extends ABSTRACT_TABLE {

	// id dot kiem ke
	public static final String EQUIP_STATISTIC_RECORD_ID = "EQUIP_STATISTIC_RECORD_ID";
	// ma dot kiem ke
	public static final String CODE = "CODE";
	// ten dot kiem ke
	public static final String NAME = "NAME";
	// thoi gian bat dau dot kiem ke
	public static final String FROM_DATE = "FROM_DATE";
	// thoi gian ket thuc dot kiem ke
	public static final String TO_DATE = "TO_DATE";
	// trang thai bien ban
	public static final String RECORD_STATUS = "RECORD_STATUS";
	// id ky lam vie
	public static final String EQUIP_PRIOD_ID = "EQUIP_PRIOD_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String QUANTITY = "QUANTITY";

	public static final String EQUIP_STATISTIC_RECORD_TABLE = "EQUIP_STATISTIC_RECORD";

	public EQUIP_STATISTIC_RECORD_TABLE(SQLiteDatabase mDB) {
		this.tableName = EQUIP_STATISTIC_RECORD_TABLE;

		this.columns = new String[] { EQUIP_STATISTIC_RECORD_ID, CODE, NAME,
				FROM_DATE, TO_DATE, RECORD_STATUS, EQUIP_PRIOD_ID, CREATE_DATE,
				CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Danh sach cac dot kiem ke
	 * 
	 * @author: hoanpd1
	 * @since: 10:25:46 16-12-2014
	 * @return: InventoryDeviceOfCustomerViewDTO
	 * @throws:
	 * @param bundle
	 * @return
	 */
	public ListEquipInventoryRecordsDTO getListPeriodInventory(Bundle bundle) {
		ListEquipInventoryRecordsDTO result = new ListEquipInventoryRecordsDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();

		sql.append("	SELECT * FROM (	");
		sql.append("	SELECT	");
		sql.append("	    esr.EQUIP_STATISTIC_RECORD_ID             AS ID_RECORD_INVEN,	");
		sql.append("	    esr.code                                  AS CODE_RECORD_INVEN,	");
		sql.append("	    esr.NAME                                  AS NAME_RECORD_INVEN,	");
		sql.append("	    Strftime('%d/%m/%Y',	");
		sql.append("	    Date(esr.from_date)) AS FROM_DATE,	");
		sql.append("	    Strftime('%d/%m/%Y',	");
		sql.append("	    Date(esr.to_date)) AS TO_DATE,	");
		sql.append("	    esg.OBJECT_TYPE AS OBJECT_TYPE, ");
		sql.append("	    esr.TYPE AS TYPE, ");
		sql.append("	    ifnull(cast(max(esrd.STATISTIC_TIME) as integer), 0) as PRETIMES,	");
		sql.append("	    cast(esr.QUANTITY as integer) as QUANTITY	");
		sql.append("	FROM	");
		sql.append("	    EQUIP_STATISTIC_RECORD  esr,	");
		sql.append("	    EQUIP_STATISTIC_CUSTOMER esc,	");
		sql.append("	    EQUIP_STATISTIC_GROUP esg	");
		
		sql.append("	    left join (	");
		sql.append("	    	SELECT object_id, customer_id	");
		sql.append("	    	FROM ACTION_LOG	");
		sql.append("	    	WHERE STAFF_ID = ? AND  SHOP_ID = ?	");
		params.add(staffId);
		params.add(shopId);
		sql.append("	    		AND DATE(START_TIME) = DATE(?) ");
		params.add(date_now);
		sql.append("	    		AND OBJECT_TYPE = 6 ");
		sql.append("	    	) al ");
		sql.append("	     on esc.EQUIP_STATISTIC_RECORD_ID = al.OBJECT_ID  ");
		sql.append("	     	and esc.customer_id = al.CUSTOMER_ID  ");
		sql.append("	    left join (SELECT	");
		sql.append("	        e.equip_id,	");
		sql.append("	        e.equip_group_id,	");
		sql.append("	        e.stock_id	");
		sql.append("	    FROM	");
		sql.append("	        equipment e,	");
		sql.append("	        equip_group eg,	");
		sql.append("	        equip_category ec	");
		sql.append("	    WHERE	");
		sql.append("	        1=1	");
		sql.append("	        AND e.status =1	");
		sql.append("	        AND eg.status =1	");
		sql.append("	        AND ec.status =1	");
		sql.append("	        AND e.stock_type =2	");
//		sql.append("	        AND e.trade_status = 0	");
		sql.append("	        AND e.stock_id = ?	");
		params.add(customerId);
		sql.append("	        AND e.usage_status in (	");
		sql.append("	            3, 4	");
		sql.append("	        )	");
		sql.append("	        AND (	");
		sql.append("	            substr(e.first_date_in_use,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	            OR e.first_date_in_use is null	");
		sql.append("	        )	");
//		sql.append("	        and not exists (selecT 1 from EQUIP_STATISTIC_STAFF where STAFF_ID <> ? and e.equip_id = EQUIP_ID)	");
//		sql.append("	        and e.equip_id not in (selecT equip_id from EQUIP_STATISTIC_STAFF where STAFF_ID <> ?)	");
//		params.add(staffId);
		sql.append("	        AND e.equip_group_id = eg.equip_group_id	");
		sql.append("	        AND eg.equip_category_id = ec.equip_category_id) eq	");
		sql.append("	    on ((esg.OBJECT_ID = eq.EQUIP_GROUP_ID and esg.object_type = 1)	");
		sql.append("	    	OR (esg.OBJECT_ID = eq.equip_id and esg.object_type = 3))	");
		sql.append("	left join ");
		sql.append("	(select pr.product_id, pr.product_code, pr.product_name, est.equip_statistic_record_id ");
		sql.append("	    from product pr, product_info pri, EQUIP_SHELF_TOTAL est ");
		sql.append("	    where pr.CAT_ID = pri.PRODUCT_INFO_ID ");
		sql.append("	    	and est.SHELF_ID = pr.product_id 	");
		sql.append("	    	and est.shop_id = ? 	");
		params.add(shopId);
		sql.append("	    	and est.STOCK_ID = ? ");
		params.add(customerId);
		sql.append("	    	and pr.status = 1 	");
		sql.append("	    	and pri.PRODUCT_INFO_CODE in 	");
		sql.append("	    		(SELECT EQUIP_PARAM_CODE FROM EQUIP_PARAM WHERE type = 'KIEM_KE')	");
		sql.append("	 ) pr on esg.OBJECT_ID = pr.product_id	");
		sql.append("	 and pr.equip_statistic_record_id = esr.equip_statistic_record_id	");

		sql.append("	    left join EQUIP_STATISTIC_REC_DTL esrd	");
		sql.append("	    	on esr.EQUIP_STATISTIC_RECORD_ID = esrd.EQUIP_STATISTIC_RECORD_ID ");
		sql.append("	    	and esrd.OBJECT_STOCK_ID = ?	");
		params.add(customerId);
		sql.append("	and ((esrd.OBJECT_ID is null or esrd.OBJECT_ID = pr.product_id)	");
		sql.append("		or (esrd.OBJECT_ID is null or esrd.OBJECT_ID = eq.equip_id))	");

		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND esc.EQUIP_STATISTIC_RECORD_ID = esr.EQUIP_STATISTIC_RECORD_ID	");
		sql.append("	    AND esg.EQUIP_STATISTIC_RECORD_ID = esr.EQUIP_STATISTIC_RECORD_ID	");
		sql.append("	    AND (	");
		sql.append("	        esr.type = 1	");
		sql.append("	        OR (	");
		sql.append("	            esr.type = 2	");
		sql.append("	            AND esr.QUANTITY > 0	");
		sql.append("	        )	");
		sql.append("	    )	");
		sql.append("	    AND esg.STATUS =1	");
		sql.append("	    AND esr.RECORD_STATUS =1	");
		sql.append("	    AND esc.status = 1	");
		sql.append("      AND esc.customer_id = ? ");
		params.add(customerId);
		sql.append("	    AND substr(esr.FROM_DATE,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	    AND (	");
		sql.append("	        substr(esr.TO_DATE,1,10) >= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	        OR esr.TO_DATE IS NULL	");
		sql.append("	    )	");
		sql.append("	        and ((eq.equip_group_id is not null AND esg.OBJECT_TYPE = 1)	");
		sql.append("	        	or (pr.product_id is not null AND esg.OBJECT_TYPE = 2)	");
		sql.append("	        	or (eq.equip_id is not null AND esg.OBJECT_TYPE = 3))	");
		sql.append("	    and al.customer_id is null	");
		sql.append("	    and not exists (selecT 1 	");
		sql.append("						from EQUIP_STATISTIC_STAFF 	");
		sql.append("						where STAFF_ID <> ? 	");
		params.add(staffId);
		sql.append("						and eq.equip_id = EQUIP_ID	");
		sql.append("						AND equip_statistic_record_id = esr.EQUIP_STATISTIC_RECORD_ID)	");
		
		sql.append("	GROUP BY	");
		sql.append("	    esr.EQUIP_STATISTIC_RECORD_ID, eq.equip_id, pr.product_id)	");
		sql.append("	    WHERE TYPE = 1 OR (PRETIMES is null or PRETIMES < QUANTITY)	");
		sql.append("	GROUP BY ID_RECORD_INVEN	");
		sql.append("	ORDER BY CODE_RECORD_INVEN	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						EquipStatisticRecordDTO inventory = new EquipStatisticRecordDTO();
						inventory.initPeriodInventoryInfo(c);
						result.listInventoryRecord.add(inventory);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.i("error", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}
	
	public CustomerListDTO getListInventoryEquipment(Bundle bundle) throws Exception {
		CustomerListDTO result = new CustomerListDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTable.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sql = new StringBuffer();

		sql.append("	SELECT * FROM (	");
		sql.append("	SELECT	");
		sql.append("	    esr.EQUIP_STATISTIC_RECORD_ID             AS EQUIP_STATISTIC_RECORD_ID,	");
		sql.append("	    esr.CODE                                  AS CODE,	");
		sql.append("	    esr.NAME                                  AS NAME,	");
		sql.append("	    esr.FROM_DATE                             AS FROM_DATE,	");
		sql.append("	    esr.TO_DATE                               AS TO_DATE,	");
		sql.append("	    esr.TYPE AS OBJECT_TYPE, ");
		sql.append("	    esr.TYPE AS TYPE ");
		sql.append("	FROM	");
		sql.append("	    EQUIP_STATISTIC_RECORD  esr,	");
		sql.append("	    EQUIP_STATISTIC_UNIT  esu	");

		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND esu.EQUIP_STATISTIC_RECORD_ID = esr.EQUIP_STATISTIC_RECORD_ID	");
		sql.append("	    AND esr.RECORD_STATUS = 1	");
		sql.append("	    AND substr(esr.FROM_DATE,1,10) <= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	    AND (	");
		sql.append("	        substr(esr.TO_DATE,1,10) >= substr(?,1,10)	");
		params.add(date_now);
		sql.append("	        OR esr.TO_DATE IS NULL	");
		sql.append("	    )	");
		sql.append("	    AND esu.unit_id in (	");
		sql.append(idShopList);
		sql.append("	    )	");
		sql.append("	    group by esr.EQUIP_STATISTIC_RECORD_ID)	");

		sql.append("	ORDER BY FROM_DATE DESC, TO_DATE ASC, CODE ASC	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						EquipStatisticRecordDTO inventory = new EquipStatisticRecordDTO();
						inventory.initPeriodInventoryInfo(c);
						result.getListInventoryRecord().add(inventory);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.i("error", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}
	
}
