/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.Vector;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.FEED_BACK_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO.TableAction;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * 
 * Mo ta cho class
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVFollowProblemItemDTO {
	public long id = 0;// id
	public String staffId = "";// ma nhan vien
	public String staffCode = "";// ma nhan vien
	public String staffName = "";// ten nhan vien
	public String type = "";// loai van de
	public String content = "";// noi dung
	public int status = 0;// trang thai
	public String createDate = "";// ngay tao
	public String remindDate = DateUtils.getCurrentDateTimeWithFormat("dd/MM/yyyy");// ngay
																					// nhac
																					// nho
	public String doneDate = "";// ngay thuc hien
	public int numReturn=0;
	public String customer_code;
	public String customer_name;
	public String housenumber;
	public String street;
	public int ischeck = 0;// kiem tra da duyet
	public Vector<HistoryItemDTO> vHistory = new Vector<HistoryItemDTO>();
	public String updateDate;
	public String updateUser;
//	public int isDeleted = 0;// trang thai

	public static final int STATUS_DELETED = 0;
	public static final int STATUS_NEW = 1;
	public static final int STATUS_DONE = 2;
	public static final int STATUS_APPROVE = 3;

	/**
	 * Mo ta chuc nang cua ham
	 * 
	 * @author: ThanhNN8
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDateWithCursor(Cursor c) {
		id = CursorUtil.getLong(c, "ID");
		customer_code = CursorUtil.getString(c, "CUSTOMER_CODE");
		customer_name = CursorUtil.getString(c, "CUSTOMER_NAME");
		housenumber = CursorUtil.getString(c, "HOUSENUMBER");
		street = CursorUtil.getString(c, "STREET");
		staffId = CursorUtil.getString(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "NAME");
		if (StringUtil.isNullOrEmpty(staffName)) {
			staffName = CursorUtil.getString(c, "STAFF_NAME");
		}
		
		content = CursorUtil.getString(c, "CONTENT");
		type = CursorUtil.getString(c, "DESCRIPTION");
		createDate = CursorUtil.getString(c, "CREATE_DATE");
		remindDate = CursorUtil.getString(c, "REMIND_DATE", DateUtils.getCurrentDateTimeWithFormat("dd/MM/yyyy"));
		doneDate = CursorUtil.getString(c, "DONE_DATE");
		numReturn = CursorUtil.getInt(c, "NUM_RETURN");
		status = CursorUtil.getInt(c, "STATUS", STATUS_NEW);
	}

	/**
	 * Mo ta chuc nang cua ham
	 * 
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateFollowProblemSql(TBHVFollowProblemItemDTO dto) {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, dto.status, null));
			if (!StringUtil.isNullOrEmpty(dto.updateDate)) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_DATE, dto.updateDate, null));
			}
//			if (!StringUtil.isNullOrEmpty(dto.doneDate)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.DONE_DATE, dto.doneDate, null));
//			} else {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.DONE_DATE, "", DATA_TYPE.NULL.toString()));
//			}
			if (!StringUtil.isNullOrEmpty(dto.updateUser)) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_USER, dto.updateUser, null));
			}
//			if (dto.numReturn>0) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.NUM_RETURN, dto.numReturn, null));
//			}
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.IS_DELETED, dto.isDeleted, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, dto.id, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	/**
	 * HieuNH gui yeu cau delete request len server
	 * 
	 * @param dto
	 * @return
	 */
	public JSONObject generateDeleteFollowProblemSql(TBHVFollowProblemItemDTO dto) {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, 0, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, dto.id, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}
}
