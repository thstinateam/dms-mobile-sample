/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  Thong tin dia ban
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class CustomerDisplayProgrameDTO extends AbstractTableDTO {
	// id bang
	public int customerDisplayProgrameId ;
	// ma khach hang
	public String customerId ; 
	// ma CTTB
	public String displayProgrameCode ;
	// muc CTTB
	public String levelCode ; 
	// tu ngay
	public String fromDate ; 
	// den ngay
	public String toDate ; 
	// trang thai
	public int status;
	// nguoi tao
	public String createUser ; 
	// nguoi cap nhat
	public String updateUser ; 
	// ngay tao
	public String createDate ;
	// ngay cap nhat
	public String updateDate ;

	
	public CustomerDisplayProgrameDTO(){
		super(TableType.CUSTOMER_DISPLAY_PROGRAME);
	}
	
	public void initDataFromCursor(Cursor c) throws Exception {
		customerId = CursorUtil.getString(c, "CUSTOMER_ID");
	}
}
