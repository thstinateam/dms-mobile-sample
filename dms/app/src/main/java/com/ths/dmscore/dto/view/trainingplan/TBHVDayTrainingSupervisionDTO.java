package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;

/**
 * TBHVDayTrainingSupervisionDTO
 * 
 * @author : TamPQ
 * @version: 1.1
 * @since : 1.0
 */
public class TBHVDayTrainingSupervisionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public double amountMonth;
	public double amount;
	public double approved;
	public int isNew;
	public int isOn;
	public int isOr;
	public double score;
	public double distance;

	public ArrayList<TBHVDayTrainingSupervisionItem> listResult = new ArrayList<TBHVDayTrainingSupervisionDTO.TBHVDayTrainingSupervisionItem>();

	public class TBHVDayTrainingSupervisionItem implements Serializable {
		private static final long serialVersionUID = 1L;
		public int stt;
		public String custCode;
		public int distanceOrder;
		public long custId;
		public String custName;
		public String custAddr;
		public double amountMonth;
		public double amount;
		public double amountApproved;
		public double score;
		public int isNew;
		public int isOn;
		public int isOr;
		public double lat;
		public double lng;
		public boolean isVisit;
		public boolean isEnded;
		public int objectTypeActionLog;
		public boolean isPrize;
		public double kc;
		public boolean isTooFarShop = false;

		public TBHVDayTrainingSupervisionItem() {

		}

		public void initFromCursor(Cursor c, TBHVDayTrainingSupervisionDTO dto, int stt2) {
			stt = stt2;
			custCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			custId = CursorUtil.getInt(c, "CUSTOMER_ID");
			custName = CursorUtil.getString(c, "CUSTOMER_NAME");
			custAddr = CursorUtil.getString(c, "HOUSENUMBER");
			custAddr += " " + CursorUtil.getString(c, "STREET");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			isNew = CursorUtil.getInt(c, "IS_NEW");
			isOn = CursorUtil.getInt(c, "IS_ON");
			isOr = CursorUtil.getInt(c, "IS_OR");
			score = CursorUtil.getDouble(c, "SCORE");
			lat = CursorUtil.getDouble(c, "LAT");
			lng = CursorUtil.getDouble(c, "LNG");
			dto.listResult.add(this);
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.isNew += isNew;
			dto.isOr += isOr;
			dto.isOn += isOn;
			// dto.score += score;

		}

		/**
		 * Parse data khach hang traning cua TBHV
		 * 
		 * @author: hoanpd1
		 * @since: 08:26:42 19-11-2014
		 * @return: void
		 * @throws:
		 * @param c
		 * @param dto
		 * @param stt2
		 * @throws Exception
		 */
		public void initDataCustomerTraing(Cursor c, TBHVDayTrainingSupervisionDTO dto)
				throws Exception {
			custId = CursorUtil.getLong(c, "CUSTOMER_ID");
			custCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			custName = CursorUtil.getString(c, "CUSTOMER_NAME");
			custAddr = CursorUtil.getString(c, "STREET");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			amountApproved = CursorUtil.getDouble(c, "AMOUNT_APPROVED");
			
			lat = CursorUtil.getDouble(c, "LAT");
			
			lng = CursorUtil.getDouble(c, "LNG");
			
			if (!StringUtil.isNullOrEmpty(CursorUtil.getString(c, "AL_CUSTOMER_ID"))) {
				isVisit = true;
			} else {
				isVisit = false;
			}
			distanceOrder = CursorUtil.getInt(c, "distance_order");
			updateCustomerDistance(distanceOrder);
			objectTypeActionLog = CursorUtil.getInt(c, "OBJECT_TYPE");
			String str = CursorUtil.getString(c, "END_TIME");
			if(StringUtil.isNullOrEmpty(str)){
				isEnded = false;
			} else {
				isEnded = true;
			}
			
			isPrize = !StringUtil.isNullOrEmpty(CursorUtil.getString(c, "STR_CUSTOMER_ID"));
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.approved += amountApproved;
		}
		public void updateCustomerDistance(double shopDistance) {
			LatLng myLatLng = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			LatLng cusLatLng = new LatLng(lat, lng);
			if (myLatLng.lat > 0 && myLatLng.lng > 0 && cusLatLng.lat > 0
					&& cusLatLng.lng > 0) {
				kc = GlobalUtil.getDistanceBetween(myLatLng, cusLatLng);
				if (kc > shopDistance) {
					isTooFarShop = true;
				}
			} else {
				kc = -1;
				isTooFarShop = true;
			}
			//neu hoat dong che do cho phep ko ket noi thi ko check k.c ghe tham
//			if (GlobalInfo.getInstance().getStateConnectionMode() == Constants.CONNECTION_OFFLINE) {
//				isTooFarShop = false;
//			}
			// //HARD-CORE KHONG CHECK KHOANG CACH GHE THAM
			// isTooFarShop = false;
		}

	}

	public TBHVDayTrainingSupervisionDTO() {
		listResult = new ArrayList<TBHVDayTrainingSupervisionItem>();
	}

	public TBHVDayTrainingSupervisionItem newStaffTrainResultReportItem() {
		return new TBHVDayTrainingSupervisionItem();
	}

}
