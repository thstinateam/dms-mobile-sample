package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.dto.db.RptStaffSaleDetailDTO;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * thong tin mot dong chi tiet bao cao doanh so khach hang
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class ReportStaffSaleDetailItemDTO {
	public RptStaffSaleDetailDTO saleDetailDTO;
	public String customerCode;
	public String customerName;
	public String customerAddress;
	public boolean isVisited;

	public ReportStaffSaleDetailItemDTO() {
		saleDetailDTO = new RptStaffSaleDetailDTO();
	}

	/**
	 * khoi tao thong tin doi tuong chi tiet doanh so ngay
	 * 
	 * @author: DungNT19
	 * @param 
	 * @return
	 * @return: void
	 * @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		customerCode = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE);
		customerName = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
		String houseNumber = CursorUtil.getString(c, CUSTOMER_TABLE.HOUSENUMBER);
		String street = CursorUtil.getString(c, CUSTOMER_TABLE.STREET);
		StringBuilder address = new StringBuilder();
		if (!StringUtil.isNullOrEmpty(houseNumber)) {
			address.append(houseNumber);
		}

		if (!StringUtil.isNullOrEmpty(street)) {
			if (!StringUtil.isNullOrEmpty(houseNumber)) {
				address.append(", ");
			}
			address.append(street);
		}

		customerAddress = address.toString();
		if(StringUtil.isNullOrEmpty(CursorUtil.getString(c, "VISITED"))) {
			isVisited = false;
		} else {
			isVisited = true;
		}
		saleDetailDTO.initDataFromCursor(c);
	}
}
