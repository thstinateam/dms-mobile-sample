package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * DTO row cua bao cao
 * @author hieunq1
 *
 */
public class SupervisorReportDisplayProgressStaffItem implements Serializable{
	private static final long serialVersionUID = 7229291450320227494L;
	public int staffId;
	public String staffCode;
	public String staffName;
	public ArrayList<ReportDisplayProgressResult> arrLevelCode;
	public ReportDisplayProgressResult itemResultTotal;
	
	public SupervisorReportDisplayProgressStaffItem() {
		arrLevelCode = new ArrayList<ReportDisplayProgressResult>();
		itemResultTotal = new ReportDisplayProgressResult();
		staffCode = "";
		staffName = "";
	}
	
}
