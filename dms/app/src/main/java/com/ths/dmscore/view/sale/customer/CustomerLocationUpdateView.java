/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.customer;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.GPSInfo;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.map.view.MarkerMapView;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.CustomerPositionLogDTO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;
import com.ths.map.BaseFragmentMapView.MarkerAnchor;
import com.ths.map.CustomFragmentMapView;
import com.viettel.maps.base.LatLng;

/**
 * man hinh cap nhat vi tri khach hang
 *
 * @author : BangHN since : 1.0 version : 1.0
 */
public class CustomerLocationUpdateView extends CustomFragmentMapView implements OnTouchListener {
	public static final int MENU_LIST_FEEDBACKS = 11;
	public static final int MENU_MAP = 12;
	public static final int MENU_IMAGE = 13;
	public static final int MENU_INFO_DETAIL = 14;
	public static final int MENU_REVIEW = 15;
	Bundle savedInstanceState;
	// thong tin khach hang tu man hinh khac gui toi
	CustomerDTO customerInfo;
	CustomerLocationUpdateHeaderView header;
	//MarkerMapView maker;
	boolean isViewPosition = false;// chi xem ko cap nhat
	// toa do khach hang
	double cusLat, cusLng;
	// toa do nhan vien
	double staffLat, staffLng;
	// vi tri nhan vien
	MarkerMapView makerPositionStaff;

	boolean isFirstInitView = false;
	//OverlayViewOptions markerOpts = new OverlayViewOptions();
	//MarkerMapView marker;
	//OverlayViewLayer markerLayer;
	private boolean isFromGS;

	public static CustomerLocationUpdateView newInstance(Bundle data) {
		CustomerLocationUpdateView f = new CustomerLocationUpdateView();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		customerInfo = (CustomerDTO) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER);
		isFromGS = getArguments().getBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, false);
		// doc lai tu DB, truong hop sau cap nhat vi tri chuyen sang tab khac ko
		// notify duoc cap nhat vi tri
		getCustomerByID(customerInfo.getCustomerId());
	}

	/**
	 * Lay TT KH = ID
	 *
	 * @author: dungdq3
	 * @since: 8:24:15 AM Oct 7, 2014
	 * @return: void
	 * @throws:
	 * @param customerId:
	 */
	private void getCustomerByID(String customerId) {
		parent.showLoadingDialog();
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEventWithOutAsyntask(bundle, ActionEventConstant.GET_CUSTOMER_BY_ID, SaleController.getInstance());
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
		View v = super.onCreateView(inflater, container, savedInstanceState);
		hideHeaderview();
		// Title NVBH
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_LOCATION_UPDATE));
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH_VITRI);
		} else {
			parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_GSNPP_LOCATION_UPDATE));
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_VITRI);
			isFromGS = true;
		}
		header = new CustomerLocationUpdateHeaderView(parent);
		header.setOnTouchListener(this);

		if(isFromGS){
			parent.setTitleName(StringUtil.getString(R.string.TEXT_LOCATION_CUSTOMER));
		}
		initData();
		setCustomerLocationHeader();

		// vi tri can update tren ban do
		// MarkerOptions markerOpts = new MarkerOptions();
		// Bitmap icon = BitmapFactory.decodeResource(parent.getResources(),
		// R.drawable.icon_flag, null);
//		marker = new MarkerMapView(getActivity(), R.drawable.icon_flag);
//		markerOpts.drawMode(OverlayViewItemObj.DRAW_BOTTOM_LEFT);
//		markerLayer = new OverlayViewLayer();
//		mapView.addLayer(markerLayer);
//		markerLayer.addItemObj(marker, markerOpts);
		showCustomerPositionMarker();
		return v;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void initMenuActionBar() {
		// enable menu bar
		enableMenuBar(this);
		MenuTab[] tabs = new MenuTab[3];
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH_HINHANHKH);
//			tabs[2] = new MenuTab(R.string.TEXT_FEEDBACK, R.drawable.icon_reminders,
//					MENU_LIST_FEEDBACKS, PriHashMap.PriForm.NVBH_GOPY);
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH);
			addMenuItem(PriForm.NVBH_BANHANG_THONGTINKH_VITRI, tabs);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR
				|| GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.GSNPP_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.GSNPP_HINHANH);
//			if (isFromGS == false) {
//				tabs[2] = new MenuTab(R.string.TEXT_LABLE_REVIEWS,
//						R.drawable.icon_note, MENU_REVIEW, PriForm.GSNPP_DANHGIA);
//			}else{
//				tabs[2] = new MenuTab(StringUtil.getString(R.string.TEXT_FEEDBACK),
//						R.drawable.icon_reminders, MENU_LIST_FEEDBACKS, PriForm.GSNPP_GOPY);
//			}
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.GSNPP_THONGTINKH);
			addMenuItem(PriForm.GSNPP_VITRI, tabs);
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initMenuActionBar();
		showStaffPositionWithError();
	}

	private void showStaffPositionWithError() {
		GPSInfo gps = GlobalInfo.getInstance().getProfile().getMyGPSInfo();
		if (gps != null && gps.getLatitude() > 0 && gps.getLongtitude() > 0) {
			drawMarkerMyPosition();
		} else {
			parent.showDialog(StringUtil.getString(R.string.TEXT_ALERT_CANT_LOCATE_YOUR_POSITION));
		}
	}

	/**
	 * Khoi tao du lieu ban do
	 *
	 * @author banghn
	 */
	private void initData() {
		if (customerInfo != null) {
			cusLat = customerInfo.getLat();
			cusLng = customerInfo.getLng();
		}

		if (cusLat > 0 && cusLng > 0) {
			moveTo(new LatLng(cusLat, cusLng));
			isViewPosition = true;
		} else {
			isViewPosition = false;
			parent.reStartLocatingWithWaiting();

			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();

			if (lat > 0 && lng > 0) {
				moveTo(new LatLng(lat, lng));
			} else {
				moveTo(new LatLng(Constants.LAT_VNM_TOWNER, Constants.LNG_VNM_TOWNER));
			}

			// dung lai vi tri truoc do, cho vi tri moi ve cap nhat lai
			GlobalInfo.getInstance().getProfile().getMyGPSInfo().setLongtitude(lng);
			GlobalInfo.getInstance().getProfile().getMyGPSInfo().setLattitude(lat);
			GlobalInfo.getInstance().getProfile().save();
		}
	}

	/**
	 * Hien thi header thong tin khach hang tren ban do
	 *
	 * @author : BangHN since : 1.0
	 */
	private void setCustomerLocationHeader() {
		if (!isFirstInitView) {
			LinearLayout llHeader;
			llHeader = new LinearLayout(parent);

			llHeader.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			llHeader.setOrientation(LinearLayout.HORIZONTAL);

			header = new CustomerLocationUpdateHeaderView(parent);
			header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			header.setOnTouchListener(this);
			header.setCustomerInfo(customerInfo);

			llHeader.addView(header);
			llHeader.setGravity(Gravity.CENTER | Gravity.TOP);
			header.btUpdate.setOnClickListener(this);
			if(header.btUseMyPosition != null){
				header.btUseMyPosition.setOnClickListener(this);
			}
			PriUtils.getInstance().setOnClickListener(header.btUpdate, this);
			rlMainMapView.addView(llHeader);
			isFirstInitView = true;
		}
		//mapView.invalidate();
	}

	/**
	 * Hien thi popup avatar khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void showCustomerPositionMarker() {
		if (cusLat > 0 && cusLng > 0 && isViewPosition) {
			Object obj = addMarkerToMap(cusLat, cusLng, R.drawable.icon_flag, MarkerAnchor.BOTTOM_LEFT, "", "", 0, 0);
		}
	}

	/**
	 * Hien thi marker toa do hien tai
	 *
	 * @author : BangHN since : 1.0
	 */
	private void showFlagCustomerPositionInMap(double lat, double lng) {
		clearAllMarkerOnMap();
		Object obj = addMarkerToMap(lat, lng, R.drawable.icon_flag, MarkerAnchor.BOTTOM_LEFT, "", "", 0, 0);
	}

	/**
	 * gotoFeedBackList
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoFeedBackList() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customerInfo);
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		e.viewData = bundle;
		e.action = ActionEventConstant.GET_LIST_CUS_FEED_BACK;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * di toi man hinh chi tiet Khach hang
	 *
	 * @author : BangHN since : 10:59:39 AM
	 */
	public void gotoCustomerInfo(String customerId) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bunde.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		e.viewData = bunde;
		e.action = ActionEventConstant.GO_TO_CUSTOMER_INFO;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * update thong tin user (update lat, lng)
	 *
	 * @author : BangHN since : 1.0
	 */
	private void updateCustomerLocationToDB() {
		// kiem tra truong hop bo qua check dinh vi
		if (cusLat <= 0 || cusLng <= 0) {
			parent.showDialog(StringUtil.getString(R.string.MESSAGE_CANT_UPDATE_CUS_LOCATION));
		} else {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
			customerInfo.setLng(cusLng);
			customerInfo.setLat(cusLat);

			// du lieu insert bang position log
			CustomerPositionLogDTO logPosition = new CustomerPositionLogDTO();
			logPosition.createDate = DateUtils.now();
			logPosition.customerId = customerInfo.customerId;
			logPosition.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			logPosition.lat = customerInfo.getLat();
			logPosition.lng = customerInfo.getLng();

			Bundle data = new Bundle();
			data.putSerializable(IntentConstants.INTENT_CUSTOMER_ID, customerInfo);
			data.putSerializable(IntentConstants.INTENT_CUSTOMER_POSITION_LOG, logPosition);
			
			handleViewEvent(data, ActionEventConstant.UPDATE_CUSTOMER_LOATION, SaleController.getInstance());
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.UPDATE_CUSTOMER_LOATION:
			parent.showDialog(StringUtil.getString(R.string.TEXT_UPDATE_POSITION));
			isViewPosition = true;
			customerInfo.setLat(cusLat);
			customerInfo.setLat(cusLng);
			header.setCustomerInfo(customerInfo);
			showCustomerPositionMarker();
			GlobalInfo.setChangeInfoCurrentCustomer(true);
			//mapView.invalidate();
			break;
		case ActionEventConstant.GET_CUSTOMER_BY_ID:
			customerInfo = (CustomerDTO) modelEvent.getModelData();
			break;
		default:
			break;
		}
		super.handleModelViewEvent(modelEvent);
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onClick(View v) {
		if(header.btUpdate == v){
			if (cusLat > 0 && cusLng > 0) {
				updateCustomerLocationToDB();
			} else {
				parent.showDialog(StringUtil.getString(R.string.MESSAGE_CANT_UPDATE_CUS_LOCATION));
			}
		} else if(header.btUseMyPosition == v){
			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			if (lat > 0 && lng > 0) {
				cusLat = lat;
				cusLng = lng;
				moveTo(new LatLng(lat, lng));
				showFlagCustomerPositionInMap(lat, lng);
				drawMarkerMyPosition();
			} else{
				parent.showDialog(StringUtil.getString(R.string.TEXT_ALERT_CANT_LOCATE_YOUR_POSITION));
			}
		} else{
			super.onClick(v);
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case MENU_LIST_FEEDBACKS:
			GlobalUtil.popBackStack(this.getActivity());
			gotoFeedBackList();
			break;
		case MENU_INFO_DETAIL:
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerInfo(customerInfo.getCustomerId());
			break;
		case MENU_IMAGE:
			GlobalUtil.popBackStack(this.getActivity());
			gotoListAlbumUser(customerInfo);
			break;
		case MENU_REVIEW:
			GlobalUtil.popBackStack(this.getActivity());
			gotoReviewsStaffView();
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 * Di toi man hinh danh gia nhan vien Danh cho module gs npp
	 *
	 * @author banghn
	 * @param item
	 */
	public void gotoReviewsStaffView() {
		Bundle data = new Bundle();
		ActionEvent event = new ActionEvent();
		event.action = ActionEventConstant.GO_TO_REVIEWS_STAFF_VIEW;
		event.sender = this;
		event.viewData = data;
		SupervisorController.getInstance().handleSwitchFragment(event);
	}

	/**
	 * Toi man hinh ds album cua kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customer
	 * @return: void
	 * @throws:
	 */

	private void gotoListAlbumUser(CustomerDTO customer) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = bundle;
		e.action = ActionEventConstant.GO_TO_LIST_ALBUM_USER;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			parent.closeProgressDialog();
			MyLog.i("Location", "Map: location changed: (lat-lng) "
					+ GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude() + " - "
					+ GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude());
			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			if (lat > 0 && lng > 0) {
				// tranh truong hop notify nhieu lan
				if (this.cusLat <= 0 && this.cusLng <= 0) {
					cusLat = lat;
					cusLng = lng;
					moveTo(new LatLng(lat, lng));
					showFlagCustomerPositionInMap(lat, lng);
				}
				drawMarkerMyPosition();
			}
			break;
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				isFirstInitView = false;
				getCustomerByID(customerInfo.getCustomerId());
				initData();
				setCustomerLocationHeader();
				showCustomerPositionMarker();
				showStaffPositionWithError();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (v == header) {
			// khong xu ly
			return true;
		}
		return false;
	}

	@Override
	public boolean onSingleTap(LatLng latlng, Point point) {
		if (!isViewPosition && !isFromGS) {
			this.cusLat = latlng.getLatitude();
			this.cusLng = latlng.getLongitude();
			showFlagCustomerPositionInMap(latlng.getLatitude(), latlng.getLongitude());
			drawMarkerMyPosition();
		}
		return true;
	}

}
