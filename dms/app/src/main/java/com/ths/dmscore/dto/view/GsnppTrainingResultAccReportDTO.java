package com.ths.dmscore.dto.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

public class GsnppTrainingResultAccReportDTO {

	public ArrayList<GsnppTrainingResultAccReportItem> listResult = new ArrayList<GsnppTrainingResultAccReportItem>();
	public double amountMonth;
	public double amount;
	public int numCustomerPlan;
	public int numCustomerOrder;
	public int numCustomerNew;
	public int numCustomerOn;
	public int numCustomerOr;
	public double score;
	public int shopId;

	public GsnppTrainingResultAccReportItem newAccTrainResultReportItem() {
		return new GsnppTrainingResultAccReportItem();
	}

	public class GsnppTrainingResultAccReportItem {
		public long trainDetailId;
		public int staffId;
		public String staffCode;
		public String staffName;
		public String date;
		public double amountMonth;
		public double amount;
		public int numCustomerPlan;
		public int numCustomerOrder;
		public int numCustomerNew;
		public int numCustomerIr;
		public int numCustomerOr;
		public double score;
		public int shopId;
		public String saleTypeCode;

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param c
		 * @return: voidvoid
		 * @throws:
		 */
		public void initData(Cursor c) {
			if (c == null) {
				return;
			}
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;
			trainDetailId = CursorUtil.getInt(c, "TRAINING_PLAN_DETAIL_ID");
			staffId = CursorUtil.getInt(c, "STAFF_ID");
			staffCode = CursorUtil.getString(c, "STAFF_CODE");
			staffName = CursorUtil.getString(c, "STAFF_NAME");
			shopId = CursorUtil.getInt(c, "SHOP_ID");
			saleTypeCode = CursorUtil.getString(c, "SALE_TYPE_CODE");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			numCustomerPlan = CursorUtil.getInt(c, "NUM_CUSTOMER_PLAN");
			numCustomerOrder = CursorUtil.getInt(c, "NUM_CUSTOMER_ORDER");
			numCustomerNew = CursorUtil.getInt(c, "NUM_CUSTOMER_NEW");
			numCustomerIr = CursorUtil.getInt(c, "NUM_CUSTOMER_ON");
			numCustomerOr = CursorUtil.getInt(c, "NUM_CUSTOMER_OR");
			score = CursorUtil.getDouble(c, "SCORE");
			try {
				date = sfd.format(sfs.parse(CursorUtil.getString(c, "TRAINING_DATE")));
			} catch (ParseException e) {
			}
		}
	}
}
