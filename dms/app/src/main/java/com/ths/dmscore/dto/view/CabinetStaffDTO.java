package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dms.R;

/**
 * 
 * Du lieu MH Quan ly thiet bi cua tung NVBH
 * 
 * @author: Nguyen Thanh Dung
 * @version: 1.1
 * @since: 1.0
 */
public class CabinetStaffDTO {
	public int totalList;
	public String totalofTotal;

	public ArrayList<CabinetStaffItem> arrList;

	public CabinetStaffDTO() {
		arrList = new ArrayList<CabinetStaffDTO.CabinetStaffItem>();
	}

	public void addItem(Cursor c) {
		CabinetStaffItem item = new CabinetStaffItem();
		item.maKh = CursorUtil.getString(c, "MA_KH").substring(0, 3);
		item.tenKh = CursorUtil.getString(c, "TEN_KH");
		item.diaChi = "";
		
		if (!StringUtil.isNullOrEmpty(CursorUtil.getString(c, "SO_NHA"))	) {
			item.diaChi = CursorUtil.getString(c, "SO_NHA");
		}
		
		if(!StringUtil.isNullOrEmpty(CursorUtil.getString(c, "DUONG"))){
			item.diaChi += " " + CursorUtil.getString(c, "DUONG");
		}
		
		item.keHoach = CursorUtil.getInt(c, "KE_HOACH");// (double)
																// Math.round(c.getDouble(c.getColumnIndex("KE_HOACH"))
																// / 1000);
		item.thucHien = CursorUtil.getInt(c, "THUC_HIEN");// (double)Math.round(c.getDouble(c.getColumnIndex("THUC_HIEN"))
																// / 1000);
																// item.ketqua =
																// c.getInt(c.getColumnIndex("DAT"));//
																// (double)Math.round(c.getDouble(c.getColumnIndex("THUC_HIEN"))
		// / 1000);
		item.conLai = item.keHoach - item.thucHien;
		if (item.conLai <= 0) {
			item.danhGia = StringUtil.getString(R.string.TEXT_CUSTOMER_PASS);
			item.conLai = 0;
		} else {
			item.danhGia = StringUtil.getString(R.string.TEXT_CUSTOMER_NOT_PASS);
		}
		arrList.add(item);
	}

	public class CabinetStaffItem {
		public String stt;// So thu tu
		public String maKh;// Ma khach hang
		public String tenKh;// Ten khach hang
		public String diaChi;// Dia chi
		public int keHoach;// Doanh so ke hoach
		public int thucHien;// Doanh so thuc hien
		public int conLai;// Doanh so con lai
		public int ketqua;// truong result
		public String danhGia;// Chuoi danh gia
		public String tenThietbi;

		public CabinetStaffItem() {
			stt = "0";
			maKh = "maKh";
			tenKh = "tenKh";
			diaChi = "diaChi";
			keHoach = 0;
			thucHien = 0;
			conLai = 0;
			danhGia = "dat";
		}
	}
}
