/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.support.v4.util.LongSparseArray;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.GroupLevelDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.GroupLevelDetailDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0
 * @since:  1.0
 */
public class GROUP_LEVEL_TABLE extends ABSTRACT_TABLE {

	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String GROUP_TEXT = "GROUP_TEXT";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String HAS_PRODUCT = "HAS_PRODUCT";
	public static final String MIN_QUANTITY = "MIN_QUANTITY";
	public static final String MAX_QUANTITY = "MAX_QUANTITY";
	public static final String MIN_AMOUNT = "MIN_AMOUNT";
	public static final String MAX_AMOUNT = "MAX_AMOUNT";
	public static final String PROMOTION_PERCENT = "PROMOTION_PERCENT";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	private static final String TABLE_NAME = "GROUP_LEVEL_TABLE";
	public GROUP_LEVEL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { GROUP_LEVEL_ID, PRODUCT_GROUP_ID, GROUP_TEXT,
				ORDER_NUMBER, HAS_PRODUCT, PROMOTION_PERCENT, STATUS,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}
	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Lay ds muc cua 1 nhom
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<GroupLevelDTO>
	 * @throws:
	 * @param groupId
	 * @return
	 */
	public ArrayList<GroupLevelDTO> getGroupLevelsOfProductGroup(long groupId, int isPromotionProduct) {
		ArrayList<GroupLevelDTO> result = new ArrayList<GroupLevelDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT GL.GROUP_LEVEL_ID, GL.ORDER_NUMBER, GL.MIN_QUANTITY, GL.MAX_QUANTITY ");
		sql.append("	   , GL.MIN_AMOUNT, GL.MAX_AMOUNT, GL.HAS_PRODUCT ");
		sql.append("	   , GLD.GROUP_LEVEL_DETAIL_ID, GLD.PRODUCT_ID, GLD.VALUE_TYPE, GLD.VALUE, GLD.IS_REQUIRED ");
		sql.append("FROM GROUP_LEVEL GL ");
		sql.append("	LEFT JOIN GROUP_LEVEL_DETAIL GLD ON GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID");
		sql.append("			AND GLD.STATUS = 1 ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("			AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(String.valueOf(groupId));
		sql.append("			AND GL.HAS_PRODUCT = ? ");
		params.add(String.valueOf(isPromotionProduct));
		sql.append("			AND GL.STATUS = 1 ");
		sql.append("	ORDER BY GL.ORDER_NUMBER, GLD.IS_REQUIRED DESC ");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						GroupLevelDetailDTO detailDTO = new GroupLevelDetailDTO();
						detailDTO.initFromCursor(c);
						if(isPromotionProduct == GroupLevelDTO.PROMOTION_ORDER || detailDTO.groupLevelDetailId > 0) {
							int orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER);

							boolean isExist = false;
							for(GroupLevelDTO dto: result) {
								if(dto.groupLevelId == detailDTO.groupLevelId && dto.orderNumber == orderNumber) {
									dto.levelDetailArray.add(detailDTO);
									isExist = true;
									break;
								}
							}

							if(!isExist) {
								GroupLevelDTO dto = new GroupLevelDTO();
								dto.groupLevelId = detailDTO.groupLevelId;
								dto.orderNumber = orderNumber;
								dto.minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY);
								dto.maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY);
								dto.minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT);
								dto.maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT);
								dto.hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT);
								dto.levelDetailArray.add(detailDTO);
								result.add(dto);
							}
						}

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.d("getGroupLevelsOfProductGroup: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		return result;
	}

	public ArrayList<GroupLevelDTO> getGroupLevelsOfProductGroup2(long groupId, int isPromotionProduct) {
		ArrayList<GroupLevelDTO> result = new ArrayList<GroupLevelDTO>();
		StringBuilder sqlLevel = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sqlLevel.append("SELECT GL.GROUP_LEVEL_ID, GL.ORDER_NUMBER, GL.MIN_QUANTITY, GL.MAX_QUANTITY ");
		sqlLevel.append("	   , GL.MIN_AMOUNT, GL.MAX_AMOUNT, GL.HAS_PRODUCT ");
		sqlLevel.append("FROM GROUP_LEVEL GL ");
		sqlLevel.append("	WHERE 1 = 1 ");
		sqlLevel.append("			AND GL.PARENT_GROUP_LEVEL_ID IS NULL ");
		sqlLevel.append("			AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(String.valueOf(groupId));
		sqlLevel.append("			AND GL.STATUS = 1 ");
		sqlLevel.append("	ORDER BY GL.ORDER_NUMBER");

		Cursor cLevel = null;
		try {
			cLevel = rawQueries(sqlLevel.toString(), params);
			if (cLevel != null) {
				if (cLevel.moveToFirst()) {
					do {
						GroupLevelDTO dto = new GroupLevelDTO();
						dto.initFromCursor(cLevel);
						result.add(dto);
					} while (cLevel.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.d("getGroupLevelsOfProductGroup: ", e.toString());
		} finally {
			try {
				if (cLevel != null) {
					cLevel.close();
				}
			} catch (Exception e2) {
			}
		}

		//Lay ds cac muc con
		StringBuilder sqlSubLevel = new StringBuilder();
		ArrayList<String> paramsSubLevel = new ArrayList<String>();
		sqlSubLevel.append("SELECT GL.GROUP_LEVEL_ID, GL.ORDER_NUMBER, GL.MIN_QUANTITY, GL.MAX_QUANTITY ");
		sqlSubLevel.append("	   , GL.MIN_AMOUNT, GL.MAX_AMOUNT, GL.HAS_PRODUCT ");
		sqlSubLevel.append("FROM GROUP_LEVEL GL ");
		sqlSubLevel.append("	WHERE 1 = 1 ");
		sqlSubLevel.append("			AND GL.PARENT_GROUP_LEVEL_ID  = ? ");
		sqlSubLevel.append("				AND GL.HAS_PRODUCT = ? ");
		sqlSubLevel.append("			AND GL.STATUS = 1 ");
		sqlSubLevel.append("	ORDER BY GL.ORDER_NUMBER");

		for(GroupLevelDTO levelDTO : result) {
			Cursor cSubLevel = null;
			paramsSubLevel.clear();
			paramsSubLevel.add(String.valueOf(levelDTO.groupLevelId));
			paramsSubLevel.add(String.valueOf(isPromotionProduct));
			try {
				cSubLevel = rawQueries(sqlSubLevel.toString(), paramsSubLevel);
				if (cSubLevel != null) {
					if (cSubLevel.moveToFirst()) {
						do {
							GroupLevelDTO dto = new GroupLevelDTO();
							dto.initFromCursor(cSubLevel);
							levelDTO.subLevelArray.add(dto);
						} while (cSubLevel.moveToNext());
					}
				}
			} catch (Exception e) {
				MyLog.d("get sub level: ", e.toString());
			} finally {
				try {
					if (cSubLevel != null) {
						cSubLevel.close();
					}
				} catch (Exception e2) {
				}
			}

			//Lay ds sp cua cac muc con
			if(isPromotionProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
				StringBuilder sqlLevelDetail = new StringBuilder();
				ArrayList<String> paramsLevelDetail = new ArrayList<String>();
				sqlLevelDetail.append("SELECT GLD.GROUP_LEVEL_ID, GLD.GROUP_LEVEL_DETAIL_ID, GLD.PRODUCT_ID, GLD.VALUE_TYPE, GLD.VALUE, GLD.IS_REQUIRED ");
				sqlLevelDetail.append("	FROM GROUP_LEVEL_DETAIL GLD ");
				sqlLevelDetail.append("	WHERE 1 = 1 ");
				sqlLevelDetail.append("		AND GLD.STATUS = 1 ");
				sqlLevelDetail.append("		AND GLD.GROUP_LEVEL_ID = ? ");
				sqlLevelDetail.append("	ORDER BY GLD.IS_REQUIRED DESC, GLD.PRODUCT_ID ");

				ArrayList<GroupLevelDTO> subLevelArray = new ArrayList<GroupLevelDTO>();
				if(levelDTO.subLevelArray.size() == 0) {
					subLevelArray.add(levelDTO);
				} else {
					subLevelArray = levelDTO.subLevelArray;
				}

				for(GroupLevelDTO subLevelDTO : subLevelArray) {
					Cursor cLevelDetail = null;
					paramsLevelDetail.clear();
					paramsLevelDetail.add(String.valueOf(subLevelDTO.groupLevelId));
					try {
						cLevelDetail = rawQueries(sqlLevelDetail.toString(), paramsLevelDetail);
						if (cLevelDetail != null) {
							if (cLevelDetail.moveToFirst()) {
								do {
									GroupLevelDetailDTO levelDetail = new GroupLevelDetailDTO();
									levelDetail.initFromCursor(cLevelDetail);
									subLevelDTO.levelDetailArray.add(levelDetail);
								} while (cLevelDetail.moveToNext());
							}
						}
					} catch (Exception e) {
						MyLog.d("get group level detail: ", e.toString());
					} finally {
						try {
							if (cLevelDetail != null) {
								cLevelDetail.close();
							}
						} catch (Exception e2) {
						}
					}
				}
			}
		}

		return result;
	}

	public ArrayList<GroupLevelDTO> getGroupLevelsOfProductGroup3(long groupId, int isPromotionProduct) throws Exception {
		ArrayList<GroupLevelDTO> result = new ArrayList<GroupLevelDTO>();
		LongSparseArray<GroupLevelDTO> resultHash = new LongSparseArray<GroupLevelDTO>();

		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("	SELECT	");
		sql.append("	    GLP.GROUP_LEVEL_ID GROUP_LEVEL_ID_P,	");
		sql.append("	    GLP.ORDER_NUMBER ORDER_NUMBER_P,	");
		sql.append("	    GLP.MIN_QUANTITY MIN_QUANTITY_P,	");
		sql.append("	    GLP.MAX_QUANTITY MAX_QUANTITY_P   ,	");
		sql.append("	    GLP.MIN_AMOUNT MIN_AMOUNT_P,	");
		sql.append("	    GLP.MAX_AMOUNT MAX_AMOUNT_P,	");
		sql.append("	    GLP.HAS_PRODUCT HAS_PRODUCT_P  ,	");
		sql.append("	    GLC.GROUP_LEVEL_ID GROUP_LEVEL_ID_C,	");
		sql.append("	    GLC.ORDER_NUMBER ORDER_NUMBER_C,	");
		sql.append("	    GLC.MIN_QUANTITY MIN_QUANTITY_C,	");
		sql.append("	    GLC.MAX_QUANTITY MAX_QUANTITY_C   ,	");
		sql.append("	    GLC.MIN_AMOUNT MIN_AMOUNT_C,	");
		sql.append("	    GLC.MAX_AMOUNT MAX_AMOUNT_C,	");
		sql.append("	    GLC.HAS_PRODUCT HAS_PRODUCT_C  ,	");
		sql.append("	    GLD.GROUP_LEVEL_ID,	");
		sql.append("	    GLD.GROUP_LEVEL_DETAIL_ID,	");
		sql.append("	    GLD.PRODUCT_ID,	");
		sql.append("	    GLD.VALUE_TYPE,	");
		sql.append("	    GLD.VALUE,	");
		sql.append("	    GLD.IS_REQUIRED	");
		sql.append("	FROM	");
		sql.append("	    GROUP_LEVEL GLP,	");
		sql.append("	    GROUP_LEVEL GLC	");
		sql.append("	LEFT JOIN	");
		sql.append("	    GROUP_LEVEL_DETAIL GLD	");
		sql.append("	        ON GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
		sql.append("	        AND GLD.STATUS = 1	");
		sql.append("	WHERE	");
		sql.append("	    1 = 1	");
		sql.append("	    AND GLP.PARENT_GROUP_LEVEL_ID IS NULL	");
		sql.append("	    AND GLP.PRODUCT_GROUP_ID = ?	");
		params.add(String.valueOf(groupId));
		sql.append("	    AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
		sql.append("	    AND GLC.HAS_PRODUCT = ?	");
		params.add(String.valueOf(isPromotionProduct));
		sql.append("	    AND GLP.STATUS = 1	");
		sql.append("	    AND GLC.STATUS = 1	");
//		sql.append("	    AND GLD.STATUS = 1	");
		sql.append("	ORDER BY	");
		sql.append("	    GLP.ORDER_NUMBER,	");
		sql.append("	    GLC.ORDER_NUMBER,	");
		sql.append("	    GLD.IS_REQUIRED DESC,	");
		sql.append("	    GLD.PRODUCT_ID	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					String supfixP = "_P";
					String supfixC = "_C";
					do {
						//Id cua group level cha & con
						long groupLevelIdP = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.GROUP_LEVEL_ID + supfixP);
						long groupLevelIdC = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.GROUP_LEVEL_ID + supfixC);
						GroupLevelDTO groupLevelP = new GroupLevelDTO();

						//Group level cha
						if( resultHash.get(groupLevelIdP) == null) {
							groupLevelP.groupLevelId = groupLevelIdP;
							groupLevelP.orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER + supfixP);
							groupLevelP.minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY + supfixP);
							groupLevelP.maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY + supfixP);
							groupLevelP.minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT + supfixP);
							groupLevelP.maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT + supfixP);
							groupLevelP.hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT + supfixP);

							result.add(groupLevelP);
							resultHash.put(groupLevelIdP, groupLevelP);
						} else {
							groupLevelP = resultHash.get(groupLevelIdP);
						}

						//Group level con
						GroupLevelDTO groupLevelC = new GroupLevelDTO();
						if( groupLevelP.subLevelHash.get(groupLevelIdC) == null ) {
							groupLevelC.groupLevelId = groupLevelIdC;
							groupLevelC.orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER + supfixC);
							groupLevelC.minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY + supfixC);
							groupLevelC.maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY + supfixC);
							groupLevelC.minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT + supfixC);
							groupLevelC.maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT + supfixC);
							groupLevelC.hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT + supfixC);

							groupLevelP.subLevelArray.add(groupLevelC);
							groupLevelP.subLevelHash.put(groupLevelIdC, groupLevelC);
						} else {
							groupLevelC = groupLevelP.subLevelHash.get(groupLevelIdC);
						}

						if(isPromotionProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
							GroupLevelDetailDTO groupLevelDetail = new GroupLevelDetailDTO();
							groupLevelDetail.initFromCursor(c);
							groupLevelC.levelDetailArray.add(groupLevelDetail);
						}

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}

		return result;
	}

	/**
	 * Lay thong tin cua 1 muc
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<GroupLevelDTO>
	 * @throws:
	 * @param groupLevelId
	 * @return
	 */
	public GroupLevelDTO getGroupLevelInfo(long groupId, long groupLevelId) {
		GroupLevelDTO result = new GroupLevelDTO();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT * FROM GROUP_LEVEL GL ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("		AND GL.STATUS = 1 ");
		sql.append("		AND GL.GROUP_LEVEL_ID = ? ");
		params.add(String.valueOf(groupLevelId));
		sql.append("		AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(String.valueOf(groupId));

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					result.initFromCursor(c);
				}
			}
		} catch (Exception e) {
			MyLog.d("getGroupLevelInfo: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}
		return result;
	}
	/**
	 * Lay thong tin cua 1 muc
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<GroupLevelDTO>
	 * @throws:
	 * @param groupLevelId
	 * @return
	 * @throws Exception 
	 */
	public GroupLevelDTO getGroupLevelInfo2(long groupId, long groupLevelId) throws Exception {
		GroupLevelDTO result = new GroupLevelDTO();
		ArrayList<GroupLevelDTO> groupLevelList = new ArrayList<GroupLevelDTO>();
		LongSparseArray<GroupLevelDTO> resultHash = new LongSparseArray<GroupLevelDTO>();

		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("	SELECT	");
		sql.append("	    GLP.GROUP_LEVEL_ID GROUP_LEVEL_ID_P,	");
		sql.append("	    GLP.ORDER_NUMBER ORDER_NUMBER_P,	");
		sql.append("	    GLP.MIN_QUANTITY MIN_QUANTITY_P,	");
		sql.append("	    GLP.MAX_QUANTITY MAX_QUANTITY_P   ,	");
		sql.append("	    GLP.MIN_AMOUNT MIN_AMOUNT_P,	");
		sql.append("	    GLP.MAX_AMOUNT MAX_AMOUNT_P,	");
		sql.append("	    GLP.HAS_PRODUCT HAS_PRODUCT_P  ,	");
		sql.append("	    GLC.GROUP_LEVEL_ID GROUP_LEVEL_ID_C,	");
		sql.append("	    GLC.ORDER_NUMBER ORDER_NUMBER_C,	");
		sql.append("	    GLC.MIN_QUANTITY MIN_QUANTITY_C,	");
		sql.append("	    GLC.MAX_QUANTITY MAX_QUANTITY_C   ,	");
		sql.append("	    GLC.MIN_AMOUNT MIN_AMOUNT_C,	");
		sql.append("	    GLC.MAX_AMOUNT MAX_AMOUNT_C,	");
		sql.append("	    GLC.PROMOTION_PERCENT PROMOTION_PERCENT_C,	");
		sql.append("	    GLC.HAS_PRODUCT HAS_PRODUCT_C  ,	");
		sql.append("	    GLD.GROUP_LEVEL_ID,	");
		sql.append("	    GLD.GROUP_LEVEL_DETAIL_ID,	");
		sql.append("	    GLD.PRODUCT_ID,	");
		sql.append("	    GLD.VALUE_TYPE,	");
		sql.append("	    GLD.VALUE,	");
		sql.append("	    GLD.IS_REQUIRED ,	");
		sql.append("	    (SELECT	");
		sql.append("	        PRODUCT_CODE	");
		sql.append("	    FROM	");
		sql.append("	        PRODUCT	");
		sql.append("	    WHERE	");
		sql.append("	        STATUS = 1	");
		sql.append("	        AND PRODUCT_ID = GLD.PRODUCT_ID) PRODUCT_CODE	");
		sql.append("	FROM	");
		sql.append("	    GROUP_LEVEL GLP,	");
		sql.append("	    GROUP_LEVEL GLC	");
		sql.append("	LEFT JOIN	");
		sql.append("	    GROUP_LEVEL_DETAIL GLD	");
		sql.append("	        ON GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
		sql.append("	        AND GLD.STATUS = 1	");
		sql.append("	WHERE	");
		sql.append("	    1 = 1	");
		sql.append("	    AND GLP.PARENT_GROUP_LEVEL_ID IS NULL	");
		sql.append("	    AND GLP.PRODUCT_GROUP_ID = ?	");
		params.add(String.valueOf(groupId));
		sql.append("	    AND GLP.GROUP_LEVEL_ID =  ?	");
		params.add(String.valueOf(groupLevelId));
		sql.append("	    AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
		sql.append("	    AND GLP.STATUS = 1	");
		sql.append("	    AND GLC.STATUS = 1	");
		sql.append("	ORDER BY	");
		sql.append("	    GLP.ORDER_NUMBER,	");
		sql.append("	    GLC.ORDER_NUMBER,	");
		sql.append("	    GLD.IS_REQUIRED DESC,	");
		sql.append("	    PRODUCT_CODE;	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					String supfixP = "_P";
					String supfixC = "_C";
					do {
						//Id cua group level cha & con
						long groupLevelIdP = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.GROUP_LEVEL_ID + supfixP);
						long groupLevelIdC = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.GROUP_LEVEL_ID + supfixC);
						GroupLevelDTO groupLevelP = new GroupLevelDTO();

						//Group level cha
						if(resultHash.get(groupLevelIdP) == null) {
							groupLevelP.groupLevelId = groupLevelIdP;
							groupLevelP.orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER + supfixP);
							groupLevelP.minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY + supfixP);
							groupLevelP.maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY + supfixP);
							groupLevelP.minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT + supfixP);
							groupLevelP.maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT + supfixP);
							groupLevelP.hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT + supfixP);

							groupLevelList.add(groupLevelP);
							resultHash.put(groupLevelIdP, groupLevelP);
						} else {
							groupLevelP = resultHash.get(groupLevelIdP);
						}

						//Group level con
						GroupLevelDTO groupLevelC = new GroupLevelDTO();
						if(groupLevelP.subLevelHash.get(groupLevelIdC) == null) {
							groupLevelC.groupLevelId = groupLevelIdC;
							groupLevelC.orderNumber = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.ORDER_NUMBER + supfixC);
							groupLevelC.minQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MIN_QUANTITY + supfixC);
							groupLevelC.maxQuantity = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.MAX_QUANTITY + supfixC);
							groupLevelC.minAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MIN_AMOUNT + supfixC);
							groupLevelC.maxAmount = CursorUtil.getLong(c, GROUP_LEVEL_TABLE.MAX_AMOUNT + supfixC);
							groupLevelC.promotionPercent = CursorUtil.getDouble(c, GROUP_LEVEL_TABLE.PROMOTION_PERCENT + supfixC);
							groupLevelC.hasProduct = CursorUtil.getInt(c, GROUP_LEVEL_TABLE.HAS_PRODUCT + supfixC);

							groupLevelP.subLevelArray.add(groupLevelC);
							groupLevelP.subLevelHash.put(groupLevelIdC, groupLevelC);
						} else {
							groupLevelC = groupLevelP.subLevelHash.get(groupLevelIdC);
						}

						if(groupLevelC.hasProduct == GroupLevelDTO.PROMOTION_PRODUCT) {
							GroupLevelDetailDTO groupLevelDetail = new GroupLevelDetailDTO();
							groupLevelDetail.initFromCursor(c);
							groupLevelC.levelDetailArray.add(groupLevelDetail);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.d("getGroupLevelsOfProductGroup3: ", e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
			}
		}

//		GroupLevelDTO result = new GroupLevelDTO();
//		StringBuilder sqlLevel = new StringBuilder();
//		ArrayList<String> params = new ArrayList<String>();
//		sqlLevel.append("SELECT * FROM GROUP_LEVEL GL ");
//		sqlLevel.append("	WHERE 1 = 1 ");
//		sqlLevel.append("		AND GL.STATUS = 1 ");
//		sqlLevel.append("		AND GL.GROUP_LEVEL_ID = ? ");
//		params.add(String.valueOf(groupLevelId));
//		sqlLevel.append("		AND GL.PRODUCT_GROUP_ID = ? ");
//		params.add(String.valueOf(groupId));
//
//		Cursor cLevel = null;
//		try {
//			cLevel = rawQueries(sqlLevel.toString(), params);
//			if (cLevel != null) {
//				if (cLevel.moveToFirst()) {
////					do {
//						result.initFromCursor(cLevel);
////					} while (cLevel.moveToNext());
//				}
//			}
//		} catch (Exception e) {
//			MyLog.d("getGroupLevelsOfProductGroup: ", e.toString());
//		} finally {
//			try {
//				if (cLevel != null) {
//					cLevel.close();
//				}
//			} catch (Exception e2) {
//			}
//		}
//
//		//Lay ds cac muc con
////		if(result.hasProduct == 1) {
//			StringBuilder sqlSubLevel = new StringBuilder();
//			ArrayList<String> paramsSubLevel = new ArrayList<String>();
//			sqlSubLevel.append("SELECT GL.GROUP_LEVEL_ID, GL.ORDER_NUMBER, GL.MIN_QUANTITY, GL.MAX_QUANTITY ");
//			sqlSubLevel.append("	   , GL.MIN_AMOUNT, GL.MAX_AMOUNT, GL.PROMOTION_PERCENT, GL.HAS_PRODUCT ");
//			sqlSubLevel.append("FROM GROUP_LEVEL GL ");
//			sqlSubLevel.append("	WHERE 1 = 1 ");
//			sqlSubLevel.append("			AND GL.PARENT_GROUP_LEVEL_ID  = ? ");
//			paramsSubLevel.add(String.valueOf(result.groupLevelId));
//			sqlSubLevel.append("			AND GL.STATUS = 1 ");
//			sqlSubLevel.append("	ORDER BY GL.ORDER_NUMBER");
//
//			Cursor cSubLevel = null;
//			try {
//				cSubLevel = rawQueries(sqlSubLevel.toString(), paramsSubLevel);
//				if (cSubLevel != null) {
//					if (cSubLevel.moveToFirst()) {
//						do {
//							GroupLevelDTO dto = new GroupLevelDTO();
//							dto.initFromCursor(cSubLevel);
//							result.subLevelArray.add(dto);
//						} while (cSubLevel.moveToNext());
//					}
//				}
//			} catch (Exception e) {
//				MyLog.d("get sub level: ", e.toString());
//			} finally {
//				try {
//					if (cSubLevel != null) {
//						cSubLevel.close();
//					}
//				} catch (Exception e2) {
//				}
//			}
//
//
//			//Lay ds sp cua 1 muc hoac cua cac muc con
//			StringBuilder sqlLevelDetail = new StringBuilder();
//			ArrayList<String> paramsLevelDetail = new ArrayList<String>();
//			sqlLevelDetail.append("SELECT GLD.GROUP_LEVEL_ID, GLD.GROUP_LEVEL_DETAIL_ID, GLD.PRODUCT_ID, GLD.VALUE_TYPE, GLD.VALUE, GLD.IS_REQUIRED ");
//			sqlLevelDetail.append(", (SELECT PRODUCT_CODE FROM PRODUCT WHERE STATUS = 1 AND PRODUCT_ID = GLD.PRODUCT_ID) PRODUCT_CODE ");
//			sqlLevelDetail.append("	FROM GROUP_LEVEL_DETAIL GLD ");
//			sqlLevelDetail.append("	WHERE 1 = 1 ");
//			sqlLevelDetail.append("		AND GLD.STATUS = 1 ");
//			sqlLevelDetail.append("		AND GLD.GROUP_LEVEL_ID = ? ");
//			sqlLevelDetail.append("	ORDER BY GLD.IS_REQUIRED DESC, PRODUCT_CODE ");
//
////			ArrayList<GroupLevelDTO> subLevelArray = new ArrayList<GroupLevelDTO>();
////			if(result.subLevelArray.size() == 0) {
////				subLevelArray.add(result);
////			} else {
////				subLevelArray = result.subLevelArray;
////			}
//
//			for(GroupLevelDTO subLevelDTO : result.subLevelArray) {
//				if(subLevelDTO.hasProduct == 1) {
//					Cursor cLevelDetail = null;
//					paramsLevelDetail.clear();
//					paramsLevelDetail.add(String.valueOf(subLevelDTO.groupLevelId));
//					try {
//						cLevelDetail = rawQueries(sqlLevelDetail.toString(), paramsLevelDetail);
//						if (cLevelDetail != null) {
//							if (cLevelDetail.moveToFirst()) {
//								do {
//									GroupLevelDetailDTO levelDetail = new GroupLevelDetailDTO();
//									levelDetail.initFromCursor(cLevelDetail);
//									subLevelDTO.levelDetailArray.add(levelDetail);
//								} while (cLevelDetail.moveToNext());
//							}
//						}
//					} catch (Exception e) {
//						MyLog.d("get group level detail: ", e.toString());
//					} finally {
//						try {
//							if (cLevelDetail != null) {
//								cLevelDetail.close();
//							}
//						} catch (Exception e2) {
//						}
//					}
//				}
//			}
////		}
		if(groupLevelList.size() > 0) {
			result = groupLevelList.get(0);
		}

		return result;
	}
}
