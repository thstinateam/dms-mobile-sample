/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.view.SaleProductInfoDTO;
import com.ths.dmscore.dto.view.SaleStatisticsProductInDayInfoViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * man hinh thong ke don tong trong ngay Pre Sales
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SaleStatisticsInDayPreSalesView extends BaseFragment implements OnItemSelectedListener, VinamilkTableListener, DMSColSortManager.OnSortChange {
	// show report sale statistics in fullDate
	private static final int ACTION_MENU_SALE_STATISTICS_IN_DAY_PRE = 3;
	private static final int ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL = 2;
	// show report sale statistics in month
	private static final int ACTION_MENU_SALE_STATISTICS_IN_MONTH = 1;
	// list product industry
	Spinner spIndustryProduct;
	// edit text product code
	VNMEditTextClearable etInputProductCode;
	// edit text product name
	VNMEditTextClearable etInputProductName;
	// table list product
	DMSTableView tbProductList;
	// flag the first load data
	boolean isDoneLoadFirst = false;
	// image button search
	Button btSearch;
	// sold data
	SaleStatisticsProductInDayInfoViewDTO saleData = new SaleStatisticsProductInDayInfoViewDTO();
	// index selected
	private int currentSelected = -1;
	private SparseArray<DMSColSortInfo> lstSortInfo;

	/**
	 *
	 * get instance for object
	 *
	 * @param data
	 * @return
	 * @return: SaleStatisticsInDayPreSalesView
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 19, 2012
	 */
	public static SaleStatisticsInDayPreSalesView getInstance(Bundle data) {
		SaleStatisticsInDayPreSalesView instance = new SaleStatisticsInDayPreSalesView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		lstSortInfo = new DMSListSortInfoBuilder()
		.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
		.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
		.addInfo(4, SortActionConstants.CAT)
		.addInfo(5, SortActionConstants.CONVFACT).build();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_sale_statistics_in_day_pre_sales_view, container,false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		this.initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_SALE_STATISTICS_IN_DAY_VIEW_PRE));
		
		if (!this.isDoneLoadFirst) {
			this.requestGetSaleStatisticsInfo();
		} else {
			renderListIndustryProduct();
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initMenuBar();
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void initMenuBar() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(PriForm.NVBH_DONTONGNGAYPRE,
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_REPORT_MONTH),
						R.drawable.icon_accumulated, ACTION_MENU_SALE_STATISTICS_IN_MONTH, PriForm.NVBH_DONTONGLUYKE),
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_SALE_STATISTICS_IN_DAY_VAL),
						R.drawable.icon_calendar, ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL, PriForm.NVBH_DONTONGNGAYVAN),
				new MenuTab(
						StringUtil.getString(R.string.TEXT_HEADER_MENU_SALE_STATISTICS_IN_DAY_PRE),
						R.drawable.icon_calendar, ACTION_MENU_SALE_STATISTICS_IN_DAY_PRE, PriForm.NVBH_DONTONGNGAYPRE));
	}

	/**
	 *
	 * init controls for view
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 19, 2012
	 */
	public void initView(View v) {
		PriUtils.getInstance().genPriHashMapForForm(PriForm.NVBH_DONTONGNGAYPRE);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvIndustryProduct, PriControl.NVBH_DONTONG_DONTONG_SPNGANH_PRE);
		spIndustryProduct = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spIndustryProduct, PriControl.NVBH_DONTONG_DONTONG_SPNGANH_PRE);
		PriUtils.getInstance().setOnItemSelectedListener(spIndustryProduct, this);
		etInputProductCode = (VNMEditTextClearable) PriUtils.getInstance()
				.findViewByIdGone(v, R.id.etInputProductCode,
						PriControl.NVBH_DONTONG_DONTONG_ETMAHANG_PRE);
		PriUtils.getInstance()
				.findViewByIdGone(v, R.id.tvInputProductCode,
						PriControl.NVBH_DONTONG_DONTONG_ETMAHANG_PRE);
		etInputProductName = (VNMEditTextClearable) PriUtils.getInstance()
				.findViewByIdGone(v, R.id.etInputProductName,
						PriControl.NVBH_DONTONG_DONTONG_ETTENHANG_PRE);
		PriUtils.getInstance()
				.findViewByIdGone(v, R.id.tvInputProductName,
						PriControl.NVBH_DONTONG_DONTONG_ETTENHANG_PRE);
		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btSearch, PriControl.NVBH_DONTONG_DONTONG_BTTIMKIEM_PRE);
		PriUtils.getInstance().setOnClickListener(btSearch, this);
		tbProductList = (DMSTableView) v.findViewById(R.id.tbProductList);
	}

	/**
	 *
	 * get all sku sale in fullDate
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 19, 2012
	 */
	private void requestGetSaleStatisticsInfo() {
		parent.showLoadingDialog();

		// send request get list vote product
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		data.putString(IntentConstants.INTENT_SHOP_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId()));
		data.putInt(IntentConstants.INTENT_STAFF_TYPE, 1);
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbProductList.getSortInfo());
		handleViewEvent(data, ActionEventConstant.GET_LIST_INDUSTRY_PRODUCT_AND_LIST_PRODUCT, SaleController.getInstance());
		isDoneLoadFirst = true;
	}

	/**
	 *
	 * get list product
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 20, 2012
	 */
	private void requestListProductSold() {
		parent.showLoadingDialog();

		// send request get list vote product
		Bundle data = new Bundle();
		String productCode = etInputProductCode.getText().toString();
		String productName = etInputProductName.getText().toString();
		String industryProduct = Constants.STR_BLANK;
		if (this.saleData != null && this.saleData.listIndustry != null
				&& this.saleData.listIndustry.size() >= this.currentSelected) {
			if (currentSelected > 0) {
				industryProduct = this.saleData.listIndustry.get(this.currentSelected);

			}
		}

		data.putString(IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		data.putString(IntentConstants.INTENT_SHOP_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId()));
		if (!StringUtil.isNullOrEmpty(productCode)) {
			data.putString(IntentConstants.INTENT_PRODUCT_CODE, productCode.trim());
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			data.putString(IntentConstants.INTENT_PRODUCT_NAME,
					StringUtil.getEngStringFromUnicodeString(productName.trim()));
		}
		if (!StringUtil.isNullOrEmpty(industryProduct)) {
			data.putString(IntentConstants.INTENT_INDUSTRY, industryProduct.trim());
		}
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbProductList.getSortInfo());
		handleViewEvent(data, ActionEventConstant.GET_LIST_PRODUCT_PRE_SALE_SOLD, SaleController.getInstance());
	}

	/**
	 *
	 * init list industry product
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 20, 2012
	 */
	private void renderListIndustryProduct() {
		if (saleData != null && saleData.listIndustry != null) {
			String[] arrPromotion = new String[this.saleData.listIndustry.size()];
			for (int i = 0, size = this.saleData.listIndustry.size(); i < size; i++) {
				arrPromotion[i] = this.saleData.listIndustry.get(i);
			}
			SpinnerAdapter adapterLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrPromotion);
			// this.isFirstLoadProduct = true;
			if(currentSelected > - 1) {
				spIndustryProduct.setSelection(currentSelected);
			} else if(currentSelected < 0) {
				spIndustryProduct.setSelection(0);
			}
			this.spIndustryProduct.setAdapter(adapterLine);
		}
	}

	/**
	 *
	 * render layout for screen
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 19, 2012
	 */
	private void renderLayout() {
		tbProductList.clearAllDataAndHeader();
		SaleStatisticsInDayPreSalesRow header = new SaleStatisticsInDayPreSalesRow(parent, null);
		initHeaderTable(tbProductList, header, lstSortInfo, this);
		SaleProductInfoDTO total = new SaleProductInfoDTO();
		int pos = 1;
		if (this.saleData.listProduct.size() > 0) {
			for (SaleProductInfoDTO data : saleData.listProduct) {
				SaleStatisticsInDayPreSalesRow row = new SaleStatisticsInDayPreSalesRow(parent, this);
				row.renderLayout(pos, data);
				// doanh thu
				total.totalAmountSold += data.totalAmountSold;
				total.totalAmountSoldApproved += data.totalAmountSoldApproved;
				// san luong quy doi
				total.numberProduct += data.numberProduct;
				total.tongQuyDoi += data.tongQuyDoi;
				tbProductList.addRow(row);
				pos++;
			}
		} else {
			tbProductList.showNoContentRow(StringUtil.getString(R.string.TEXT_NOTIFY_PRODUCT_NULL));
		}

		if (!isDoneLoadFirst) {
			tbProductList.addHeader(new SaleStatisticsInDayPreSalesRow(parent, null));
		}
		
		SaleStatisticsInDayPreSalesRow rowTotal = new SaleStatisticsInDayPreSalesRow(parent, null);
		rowTotal.renderLayoutSum(total);
		tbProductList.addRow(rowTotal);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_INDUSTRY_PRODUCT_AND_LIST_PRODUCT:
			this.saleData = (SaleStatisticsProductInDayInfoViewDTO) modelEvent.getModelData();
			if (saleData != null && saleData.listIndustry != null) {
				// add item "tat ca" len top
				if (saleData.listIndustry.size() > 0) {
					this.saleData.listIndustry.add(0, StringUtil.getString(R.string.TEXT_ALL));
				} else {
					this.saleData.listIndustry.add(StringUtil.getString(R.string.TEXT_ALL));
				}
				this.renderListIndustryProduct();
//				this.renderLayout();
				this.isDoneLoadFirst = true;
			}
			this.parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_PRE_SALE_SOLD:
			this.saleData.listProduct = (ArrayList<SaleProductInfoDTO>) modelEvent.getModelData();
			if (this.saleData.listProduct != null) {
				this.renderLayout();
			}
			this.parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_PRODUCT_PRE_SALE_SOLD:
			this.parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_LIST_INDUSTRY_PRODUCT_AND_LIST_PRODUCT:
			this.parent.closeProgressDialog();
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_MENU_SALE_STATISTICS_IN_MONTH:
			this.gotoSaleStatisticsProductListInMonth();
			break;
		case ACTION_MENU_SALE_STATISTICS_IN_DAY_VAL:
			gotoSaleStatisticsProductListInDayVal();
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if (currentSelected != arg2 && this.isDoneLoadFirst) {
			currentSelected = arg2;
			etInputProductCode.setText(Constants.STR_BLANK);
			etInputProductName.setText(Constants.STR_BLANK);
			this.requestListProductSold();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btSearch) {
			this.requestListProductSold();
		} else {
			super.onClick(v);
		}
	}

	/**
	 *
	 * display sale statistics in month
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 22, 2012
	 */
	private void gotoSaleStatisticsProductListInMonth() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_MONTH, SaleController.getInstance());
	}

	private void gotoSaleStatisticsProductListInDayVal() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_VAL, UserController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				if (etInputProductCode != null) {
					etInputProductCode.setText(Constants.STR_BLANK);
				}
				if (etInputProductName != null) {
					etInputProductName.setText(Constants.STR_BLANK);
				}
				if (saleData != null && saleData.listIndustry != null && saleData.listIndustry.size() > 0) {
					this.spIndustryProduct.setSelection(0);
				}
				this.currentSelected = -1;
				requestGetSaleStatisticsInfo();
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		requestListProductSold();
	}
}
