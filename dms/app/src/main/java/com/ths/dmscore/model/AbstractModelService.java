/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.io.Serializable;
import java.util.Vector;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.network.http.HTTPMultiPartRequest;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.lib.network.http.NetworkUtil;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.AbstractController;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.network.http.HTTPListenner;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.network.http.HttpAsyncTask;
import com.ths.dmscore.lib.network.http.MultiPartInputStream;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Lop Abstract model (interface)
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public abstract class AbstractModelService implements HTTPListenner,
		Serializable {

	abstract public Object requestHandleModelData(ActionEvent e) throws Exception;
	public void onReceiveError(HTTPResponse response) {

	}

	public void onReceiveData(HTTPMessage mes) {
		// TODO Auto-generated method stub

	}

	/**
	 * Cap nhat vao bang log
	 *
	 * @author: TruongHN
	 * @param actionEvent
	 * @param newState
	 * @return: void
	 * @throws:
	 */
	public void updateLog(ActionEvent actionEvent, String newState) {
		if (GlobalUtil.checkActionSave(actionEvent.action)
				&& actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO logDTO = (LogDTO) actionEvent.logData;
			if (logDTO != null && !StringUtil.isNullOrEmpty(logDTO.logId)) {
				logDTO.setType(AbstractTableDTO.TableType.LOG);
				if (LogDTO.STATE_NONE.equals(logDTO.state)) {
					logDTO.state = newState;
					logDTO.createDate = DateUtils.now();
					logDTO.createUser = GlobalInfo.getInstance().getProfile()
							.getUserData().getUserCode();
					logDTO.needCheckDate = actionEvent.isNeedCheckTimeServer ? LogDTO.NEED_CHECK_TIME
							: LogDTO.NO_NEED_CHECK_TIME;
					SQLUtils.getInstance().insert(logDTO);
				} else {
					logDTO.state = newState;
					logDTO.updateDate = DateUtils.now();
					logDTO.updateUser = GlobalInfo.getInstance().getProfile()
							.getUserData().getUserCode();
					if (logDTO.tableType == LogDTO.TYPE_ORDER
							&& !StringUtil.isNullOrEmpty(logDTO.tableId)) {
						SQLUtils.getInstance().updateLogWithOrder(logDTO,
								logDTO.tableId);
					} else {
						SQLUtils.getInstance().update(logDTO);
					}

				}
			}
		}
	}

	/**
	 * Request text
	 *
	 * @author: HieuNH
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequest(String method, Vector data,
										  ActionEvent actionEvent) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NONE;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			actionEvent.logData = log;
		}

		re.setUrl(ServerPath.SERVER_HTTP);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		re.setMethodName(method);
		new HttpAsyncTask(re).execute();
		return re;
	}
	
	protected HTTPRequest sendHttpRequestSyndata(String method, Vector data, 
			ActionEvent actionEvent) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));
		
		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NONE;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			actionEvent.logData = log;
		}
		
		re.setUrl(ServerPath.SERVER_HTTP);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		re.setMethodName(method);
		new HttpAsyncTask(re, NetworkTimeout.getREAD_TIME_OUT_SYNDATA()).execute();
		return re;
	}


	/**
	 * Send request voi connect, read time out
	 *
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @param connectTimeOut
	 * @param readTimeOut
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequest(String method, Vector data,
			ActionEvent actionEvent, int connectTimeOut, int readTimeOut) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NONE;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			actionEvent.logData = log;
		}

		re.setUrl(ServerPath.SERVER_HTTP);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setMethodName(method);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		new HttpAsyncTask(re, connectTimeOut, readTimeOut).execute();
		return re;
	}

//	/**
//	 * Send request voi connect, read time out, HTTPS
//	 *
//	 * @param method
//	 * @param data
//	 * @param actionEvent
//	 * @param connectTimeOut
//	 * @param readTimeOut
//	 * @return
//	 */
//	@SuppressWarnings("rawtypes")
//	protected HTTPRequest sendHttpsRequest(String method, Vector data,
//			ActionEvent actionEvent, int connectTimeOut, int readTimeOut) {
//		HTTPRequest re = new HTTPRequest();
//		StringBuffer strBuffer = new StringBuffer();
//		strBuffer.append(NetworkUtil.getJSONObject(data));
//
//		if (GlobalUtil.checkActionSave(actionEvent.action)) {
//			LogDTO log = new LogDTO();
//			log.setType(AbstractTableDTO.TableType.LOG);
//			log.logId = (String) actionEvent.logData;
//			log.value = strBuffer.toString();
//			log.state = LogDTO.STATE_NONE;
//			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
//					.getUserData().id);
//			actionEvent.logData = log;
//		}
//
//		re.setUrl(ServerPath.SERVER_PATH_HTTPS);
//		re.setAction(actionEvent.action);
//		re.setContentType(HTTPMessage.CONTENT_JSON);
//		re.setMethod(Constants.HTTPCONNECTION_POST);
//		re.setMethodName(method);
//		re.setDataText(strBuffer.toString());
//		re.setObserver(this);
//		re.setUserData(actionEvent);
//		new HttpAsyncTask(re, connectTimeOut, readTimeOut).execute();
//		return re;
//	}

	/**
	 * Request text
	 *
	 * @author: HieuNH
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequestOffline(String method, Vector data,
			ActionEvent actionEvent) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NEW;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			log.createDate = DateUtils.now();
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			log.needCheckDate = actionEvent.isNeedCheckTimeServer ? LogDTO.NEED_CHECK_TIME
					: LogDTO.NO_NEED_CHECK_TIME;
			actionEvent.logData = log;
			SQLUtils.getInstance().insert(log);
			// kick lai luong offline
			// TransactionProcessManager.getInstance().resetChecking();

		} else {
			if(method.equals("gcm.registerStaffKey")|| method.equals("gcm.broadcastMessage")){
				re.setUrl(ServerPath.SERVER_HTTPS);
			} else{
				re.setUrl(ServerPath.SERVER_HTTP);
			}
			re.setAction(actionEvent.action);
			re.setContentType(HTTPMessage.CONTENT_JSON);
			re.setMethod(Constants.HTTPCONNECTION_POST);
			re.setDataText(strBuffer.toString());
			re.setObserver(this);
			re.setUserData(actionEvent);
			re.setMethodName(method);
			new HttpAsyncTask(re).execute();
		}

		return re;
	}

	/**
	 * Request len server, voi fullDate du thong tin table
	 *
	 * @author: TruongHN
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @param tableType
	 * @param tableId
	 * @param tableName
	 * @return: HTTPRequest
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequestOffline(String method, Vector data,
			ActionEvent actionEvent, int tableType, String tableId,
			String tableName) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NEW;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			log.createDate = DateUtils.now() ;
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			log.needCheckDate = actionEvent.isNeedCheckTimeServer ? LogDTO.NEED_CHECK_TIME
					: LogDTO.NO_NEED_CHECK_TIME;
			log.tableType = tableType;
			log.tableName = tableName;
			log.tableId = tableId;
			actionEvent.logData = log;
			// luu log
			SQLUtils.getInstance().insert(log);
			// kick lai luong offline
			// TransactionProcessManager.getInstance().resetChecking();

		} else {

			re.setUrl(ServerPath.SERVER_HTTP);
			re.setAction(actionEvent.action);
			re.setContentType(HTTPMessage.CONTENT_JSON);
			re.setMethod(Constants.HTTPCONNECTION_POST);
			re.setDataText(strBuffer.toString());
			re.setObserver(this);
			re.setUserData(actionEvent);
			re.setMethodName(method);
			new HttpAsyncTask(re).execute();
		}

		return re;
	}

	/**
	 * Request text
	 *
	 * @author: TruognHN
	 * @param method
	 * @param dataText
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	protected HTTPRequest sendHttpRequest(String method, String dataText,
			ActionEvent actionEvent) {
		HTTPRequest re = new HTTPRequest();
		re.setUrl(ServerPath.SERVER_HTTP);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setDataText(dataText);
		re.setObserver(this);
		re.setUserData(actionEvent);
		re.setMethodName(method);
		new HttpAsyncTask(re, false).execute();
		return re;
	}

	/**
	 * Request text
	 *
	 * @author: HieuNH
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequest(String method, Vector data,
			ActionEvent actionEvent, int timeout) {
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));
		HTTPRequest re = new HTTPRequest();
		re.setUrl(ServerPath.SERVER_HTTP);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setMethodName(method);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		new HttpAsyncTask(re, timeout).execute();
		return re;
	}

	/**
	 * Request multipart
	 *
	 * @author: HieuNH
	 * @param method
	 * @param data
	 * @param imgPath
	 * @param actionEvent
	 * @param fileName
	 * @param fileField
	 * @param fileType
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected HTTPRequest httpMultiPartRequest(String method, Vector data,
			String imgPath, ActionEvent actionEvent, String fileName,
			String fileField, String fileType) {
		HTTPRequest re = new HTTPMultiPartRequest();
		data.add(IntentConstants.INTENT_LOG_ID);
		data.add(actionEvent.logData);
		data.add(IntentConstants.INTENT_IMEI_PARA);
		data.add(GlobalInfo.getInstance().getDeviceIMEI());
		String dataText = NetworkUtil.getJSONObject(data).toString();
		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			// Log.e("AbstractModelService --> Save Log voi ten :",
			// GlobalUtil.getFormatLogFile(timer, method));
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = dataText;
			log.state = LogDTO.STATE_NEW;
			log.tableType = LogDTO.TYPE_IMAGE;
			log.tableId = imgPath;// chuoi img_path
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			log.createDate = DateUtils.now();
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			actionEvent.logData = log;

			// luu log
			SQLUtils.getInstance().insert(log);
			// kick lai luong offline
			// TransactionProcessManager.getInstance().resetChecking();
		}

		return re;
	}

	/**
	 * Request multipart test
	 *
	 * @author: PhucNT
	 * @param method
	 * @param data
	 * @param imgPath
	 * @param actionEvent
	 * @param fileName
	 * @param fileField
	 * @param fileType
	 * @return: void
	 * @throws:
	 */
	protected HTTPRequest httpMultiPartRequest(String method, String data,
			String imgPath, ActionEvent actionEvent, String fileName,
			String fileField, String fileType) throws Exception{
		//Bitmap bmp = GlobalInfo.getInstance().getBitmapData();
		HTTPRequest re = new HTTPMultiPartRequest();
		re.setContentType(HTTPMessage.MULTIPART_JSON);
		re.setDataTypeSend(HTTPRequest.CONTENT_TYPE_BINARY);
		MultiPartInputStream multiPartStream;
		try {
			multiPartStream = new MultiPartInputStream(data, imgPath,
					fileName, fileField, fileType, "");
			((HTTPMultiPartRequest) re).setMultipartStream(multiPartStream);
			((HTTPMultiPartRequest) re).setDataText(data);
			re.setUrl(ServerPath.SERVER_HTTP);
			re.setAction(actionEvent.action);
			re.setMethod("POST");
			re.setMethodName(method);
			re.setObserver(this);
			re.setUserData(actionEvent);

			new HttpAsyncTask(re, false).execute();
		} catch (Exception e) {
			throw e;
		}
		return re;
	}


	/**
	 * Request multipart  1 file zip
	 *
	 * @author: TamPQ
	 * @param method
	 * @param data
	 * @param imgPath
	 * @param actionEvent
	 * @param fileName
	 * @param fileField
	 * @param fileType
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("unchecked")
	protected HTTPRequest httpMultiPartRequest(String method, String filePath,
			ActionEvent actionEvent, String fileName, String fileField,
			String fileType) {
		HTTPRequest re = new HTTPMultiPartRequest();
		re.setContentType(HTTPMessage.MULTIPART_JSON);
		re.setDataTypeSend(HTTPRequest.CONTENT_TYPE_BINARY);
		String dataText = NetworkUtil.getJSONObject((Vector<String>) actionEvent.viewData).toString();
		MultiPartInputStream multiPartStream;
		try {
			multiPartStream = new MultiPartInputStream(dataText, filePath,
					fileName, fileField, fileType, "");
			((HTTPMultiPartRequest) re).setMultipartStream(multiPartStream);
			((HTTPMultiPartRequest) re).setDataText(dataText);
			re.setUrl(ServerPath.SERVER_HTTP);
			re.setAction(actionEvent.action);
			re.setMethod("POST");
			re.setMethodName(method);
			re.setObserver(this);
			re.setUserData(actionEvent);

			new HttpAsyncTask(re).execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return re;
	}

	/**
	 * Request multipart voi anh duoc decode tu link image
	 *
	 * @author: BangHN
	 * @param method
	 * @param data
	 * @param imgPath
	 * @param actionEvent
	 * @param fileName
	 * @param fileField
	 * @param fileType
	 * @param isUsedDecodeBitmapFromImgPath
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest httpMultiPartRequestWithDeoceImageFromImagePath(
			String method, Vector data, String imgPath,
			ActionEvent actionEvent, String fileName, String fileField,
			String fileType) {
		HTTPRequest re = new HTTPMultiPartRequest();
		re.setContentType(HTTPMessage.MULTIPART_JSON);
		re.setDataTypeSend(HTTPRequest.CONTENT_TYPE_BINARY);
		MultiPartInputStream multiPartStream;
		try {
			String dataText = NetworkUtil.getJSONObject(data).toString();
			multiPartStream = new MultiPartInputStream(dataText, imgPath,
					fileName, fileField, fileType);
			((HTTPMultiPartRequest) re).setMultipartStream(multiPartStream);
			((HTTPMultiPartRequest) re).setDataText(dataText);
			re.setUrl(ServerPath.SERVER_HTTP);
			re.setAction(actionEvent.action);
			re.setMethod("POST");
			re.setMethodName(method);
			re.setObserver(this);
			re.setUserData(actionEvent);

			new HttpAsyncTask(re).execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return re;
	}

	/**
	 * request up hinh thiet bi
	 * @author: DungNX
	 * @param data
	 * @param imgPath
	 * @param actionEvent
	 * @return
	 * @return: HTTPRequest
	 * @throws:
	*/
	@SuppressWarnings("rawtypes")
	protected HTTPRequest httpMultiPartRequestInventory(Vector data,
			String imgPath, ActionEvent actionEvent) {
		HTTPRequest re = new HTTPMultiPartRequest();
		String dataText = NetworkUtil.getJSONObject(data).toString();
		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = dataText;
			log.state = LogDTO.STATE_NEW;
			log.tableType = LogDTO.TYPE_IMAGE_INVENTORY_DEVICE;
			log.tableId = imgPath;// chuoi img_path
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			log.createDate = DateUtils.now();
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			actionEvent.logData = log;
			// luu log
			SQLUtils.getInstance().insert(log);
		}

		return re;
	}

	/**
	 * Handle su kien dua xuong tu controller
	 * @author: Tuanlt11
	 * @param abstractController
	 * @param e
	 * @return: void
	 * @throws:
	*/
	public void handleControllerEvent(AbstractController abstractController, final ActionEvent e) {
		try {
			// tra ve model data
			ModelEvent model = (ModelEvent) requestHandleModelData(e);
			Object dto = model.getModelData();
			if (e.type == ActionEvent.TYPE_OFFLINE) {
				if (model.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
					abstractController.handleModelEvent(model);
				}else{
					abstractController.handleErrorModelEvent(model);
				}
			} else {
				// truong hop online , xu li add request de nhan back ve ko goi
				// request nua
				if (dto != null && dto instanceof HTTPRequest) {
					HTTPRequest request = (HTTPRequest) dto;
					e.request = request;
				}

			}

		} catch (Exception ex) {
			String exceptionMessage = VNMTraceUnexceptionLog.getReportFromThrowable(ex);
			// neu that bai, se log ra Console + log len server
			ModelEvent model = new ModelEvent();
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setIsSendLog(false);
			model.setActionEvent(e);
			abstractController.handleErrorModelEvent(model);
			// chi tiet loi se gom ten Controller + action
			String logDecription = String.format(
					"Controller: %s ActionCode: %s", abstractController
							.getClass().getName(), e.action);
			// log to Server
			ServerLogger.sendLog(exceptionMessage, logDecription,
					TabletActionLogDTO.LOG_EXCEPTION);
			// log to Console
			MyLog.e(logDecription, exceptionMessage);
		}
	}

	/**
	 * gen json goi server
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @param jsonArr
	 * @return: void
	 * @throws:
	 */
	public void genJsonSendToServer(ActionEvent e, JSONArray jsonArr) {
		String logId = GlobalUtil.generateLogId();
		e.logData = logId;
		Vector<Object> para = new Vector<Object>();
		para.add(IntentConstants.INTENT_LIST_SQL);
		para.add(jsonArr.toString());
		para.add(IntentConstants.INTENT_MD5);
		para.add(StringUtil.md5(jsonArr.toString()));
		para.add(IntentConstants.INTENT_LOG_ID);
		para.add(logId);
		para.add(IntentConstants.INTENT_IMEI_PARA);
		para.add(GlobalInfo.getInstance().getDeviceIMEI());
		if (e.type == ActionEvent.TYPE_OFFLINE) {
			sendHttpRequestOffline("queryController/executeSql", para, e);
		} else {
			sendHttpRequest("queryController/executeSql", para, e);
		}
	}

	/**
	 * Send request voi connect, read time out
	 *
	 * @param method
	 * @param data
	 * @param actionEvent
	 * @param connectTimeOut
	 * @param readTimeOut
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	protected HTTPRequest sendHttpRequest(String method, Vector data,
										   ActionEvent actionEvent, int connectTimeOut, int readTimeOut, boolean isUsingHTTPs) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NONE;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			actionEvent.logData = log;
		}
		if(isUsingHTTPs) {
			re.setUrl(ServerPath.SERVER_HTTPS);
		} else {
			re.setUrl(ServerPath.SERVER_HTTP);
		}
//		re.setUrl(ServerPath.SERVER_PATH);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_JSON);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		re.setMethodName(method);
		new HttpAsyncTask(re, connectTimeOut, readTimeOut).execute();
		return re;
	}

	protected HTTPRequest sendHttpRequestChart(String method, Vector data,
										  ActionEvent actionEvent, int connectTimeOut, int readTimeOut, boolean isUsingHTTPs) {
		HTTPRequest re = new HTTPRequest();
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(NetworkUtil.getJSONObject(data));

		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = strBuffer.toString();
			log.state = LogDTO.STATE_NONE;
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			actionEvent.logData = log;
		}
		if(isUsingHTTPs) {
			re.setUrl(ServerPath.SERVER_HTTPS_CHART);
		} else {
			re.setUrl(ServerPath.SERVER_HTTP_CHART);
		}
//		re.setUrl(ServerPath.SERVER_PATH);
		re.setAction(actionEvent.action);
		re.setContentType(HTTPMessage.CONTENT_X_WWW_FORM_URLENCODED);
		re.setMethod(Constants.HTTPCONNECTION_POST);
		re.setDataText(strBuffer.toString());
		re.setObserver(this);
		re.setUserData(actionEvent);
		re.setMethodName(method);
		new HttpAsyncTask(re, connectTimeOut, readTimeOut).execute();
		return re;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected HTTPRequest httpMultiPartRequestAttachFile(Vector data,
			String imgPath, ActionEvent actionEvent) {
		HTTPRequest re = new HTTPMultiPartRequest();
		data.add(IntentConstants.INTENT_LOG_ID);
		data.add(actionEvent.logData);
		data.add(IntentConstants.INTENT_IMEI_PARA);
		data.add(GlobalInfo.getInstance().getDeviceIMEI());
		String dataText = NetworkUtil.getJSONObject(data).toString();
		if (GlobalUtil.checkActionSave(actionEvent.action)) {
			LogDTO log = new LogDTO();
			log.setType(AbstractTableDTO.TableType.LOG);
			log.logId = (String) actionEvent.logData;
			log.value = dataText;
			log.state = LogDTO.STATE_NEW;
			log.tableType = LogDTO.TYPE_ATTACH_FILE;
			log.tableId = imgPath;// chuoi img_path
			log.userId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			log.createDate = DateUtils.now();
			log.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			actionEvent.logData = log;

			// luu log
			SQLUtils.getInstance().insert(log);
		}

		return re;
	}
}
