/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PayReceivedDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Bang cong no
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class PAY_RECEIVED_TEMP_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String PAY_RECEIVED_ID = "PAY_RECEIVED_ID";
	//
	public static final String PAY_RECEIVED_NUMBER = "PAY_RECEIVED_NUMBER";
	//
	public static final String AMOUNT = "AMOUNT";
	//
	public static final String PAYMENT_TYPE = "PAYMENT_TYPE";
	//
	public static final String SHOP_ID = "SHOP_ID";
	//
//	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	//
	public static final String RECEIPT_TYPE = "RECEIPT_TYPE";
	//
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String TYPE = "TYPE";

	public static final String PAYMENT_STATUS = "PAYMENT_STATUS";
	public static final String PAYER_NAME = "PAYER_NAME";
	public static final String PAYER_ADDRESS = "PAYER_ADDRESS";
	public static final String PAYMENT_REASON = "PAYMENT_REASON";
	// ten bang
	public static final String STAFF_ID = "STAFF_ID";
	public static final String TABLE_NAME = "PAY_RECEIVED_TEMP";

	public PAY_RECEIVED_TEMP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PAY_RECEIVED_ID, PAY_RECEIVED_NUMBER, AMOUNT, PAYMENT_TYPE, SHOP_ID,
				RECEIPT_TYPE, TYPE, PAYMENT_STATUS, PAYER_NAME, PAYER_ADDRESS, PAYMENT_REASON };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PayReceivedDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PayReceivedDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PayReceivedDTO disDTO = (PayReceivedDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.payReceivedID };
		return update(value, PAY_RECEIVED_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(PAY_RECEIVED_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PayReceivedDTO paramDTO = (PayReceivedDTO) dto;
		String[] params = { String.valueOf(paramDTO.payReceivedID) };
		return delete(PAY_RECEIVED_ID + " = ?", params);
	}

	private ContentValues initDataRow(PayReceivedDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PAY_RECEIVED_ID, dto.payReceivedID);
		editedValues.put(PAY_RECEIVED_NUMBER, dto.payReceivedNumber);
		editedValues.put(AMOUNT, dto.amount);
		editedValues.put(PAYMENT_TYPE, dto.paymentType);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
//		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(RECEIPT_TYPE, dto.receiptType);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(TYPE, dto.type);

		editedValues.put(PAYMENT_STATUS, dto.paymentStatus);
		if (!StringUtil.isNullOrEmpty(dto.payerName)) {
			editedValues.put(PAYER_NAME, dto.payerName);
		}
		if (!StringUtil.isNullOrEmpty(dto.payerAddress)) {
			editedValues.put(PAYER_ADDRESS, dto.payerAddress);
		}
		if (!StringUtil.isNullOrEmpty(dto.paymentReason)) {
			editedValues.put(PAYMENT_REASON, dto.paymentReason);
		}
		return editedValues;
	}

	/**dem so payreceived da tao trong ngay
	 * @author cuonglt3
	 * @return
	 */
	public int getNumPayReceivedCreateInDay() {
		// TODO Auto-generated method stub
		int numReceived = 0;
//		String sql = "select count(*) as numPayReceived from PAY_RECEIVED WHERE staff_id = ? AND dayInOrder(CREATE_DATE) = ?";
		String sqlTemp = "select count(*) as numPayReceived from PAY_RECEIVED_TEMP WHERE staff_id = ? AND dayInOrder(CREATE_DATE) = ?";
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> param = new ArrayList<String>();
		param.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		param.add(date_now);
//		Cursor c = null;
//		c = rawQueries(sql, param);
		Cursor cTemp = null;
		cTemp = rawQueries(sqlTemp, param);
		try {
			if ( cTemp != null) {
				cTemp.moveToFirst();
				numReceived += CursorUtil.getInt(cTemp, "numPayReceived");
			}
		} catch (Exception e) {
			MyLog.d("", e.toString());
		} finally {
			if (cTemp != null) {
				try {
					cTemp.close();
				} catch (Exception ex) {
				}
			}
		}
		return numReceived;
	}

}
