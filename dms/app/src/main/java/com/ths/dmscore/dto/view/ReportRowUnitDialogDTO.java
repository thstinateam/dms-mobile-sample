package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cho man hinh chon don vi
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportRowUnitDialogDTO {
	public ArrayList<ReportRowUnitDTO> lstUnit = new ArrayList<ReportRowUnitDTO>();
	public int totalUnit = 0;
}
