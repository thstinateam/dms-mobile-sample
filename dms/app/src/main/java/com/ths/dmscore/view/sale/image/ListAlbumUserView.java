/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.image;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.view.ListAlbumUserDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnImageTakingPopupListener;
import com.ths.dmscore.view.main.PopupChooseDisplayProgram;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.VNMStableGridView;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * Man hinh chua cac album cua mot khach hang
 *
 * @author: SoaN
 * @version: 1.0
 * @since: Aug 1, 2012
 */

public class ListAlbumUserView extends BaseFragment implements
		OnItemClickListener, OnClickListener, OnItemSelectedListener,
		OnTouchListener, OnImageTakingPopupListener, OnDateSetListener {

	public static final int MENU_LIST_FEEDBACKS = 11;
	public static final int MENU_MAP = 12;
	public static final int MENU_IMAGE = 13;
	public static final int MENU_INFO_DETAIL = 14;
	public static final int MENU_REVIEW = 15;

	public static final int ACTION_CLOSE = 16;

	protected static final int[] IMAGE_IDS = { R.id.imgAlbumImage };

	private ImageAdapter thumbs = null;
	private ImageAdapter programeThumbs = null;
	ListAlbumUserDTO model = new ListAlbumUserDTO();

	private VNMStableGridView gvUserImageView;
	private VNMStableGridView gvProgrameImageView;
	Button btTakePhoto;
	Button btSearch;
	// ma khach hang
	TextView tvKH;
	// ho ten
	TextView tvHoTenKH;
	VNMEditTextClearable etFromDate;
	VNMEditTextClearable etToDate;
	Spinner spinnerPhotoPrograme;
	LinearLayout rlSearch;
	TextView tvNoDataResult;
	private int ofAlbum;

	// thong tin CTTB chup hinh
	public DisplayProgrameDTO dpTakePhoto = null;

	private int currentCalender;
	private static final int DATE_FROM_CONTROL = 1;
	private static final int DATE_TO_CONTROL = 2;

	// save du lieu for request
	private String fromDate = "";
	private String toDate = "";
	private String fromDateForRequest = "";
	private String toDateForRequest = "";
	private String programeCode = "";
	// du lieu cho combobox chuong trinh
	private List<DisplayProgrameItemDTO> listPrograme = null;
	// dialog chup anh
	AlertDialog popupTakeImage;
	private CustomerDTO customer;
	private long routingId = 0;
	private long cycleId = 0;
	private String shopId;
	Bundle trainingReviewStaffBundle;
	private int staffId;
	int type;
	private boolean isFromGS;
	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: ListAlbumUserView
	 * @throws:
	 */
	public static ListAlbumUserView getInstance(Bundle data) {
		ListAlbumUserView instance = new ListAlbumUserView();
		instance.setArguments(data);

		return instance;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		trainingReviewStaffBundle = parent.getTrainingReviewStaffBundle();
		Bundle args = getArguments();
		if (args != null) {
			isFromGS = args.getBoolean(IntentConstants.INTENT_FROM_SUPERVISOR,
					false);
		}
		MyLog.i("TamPQ", "fragment onAttach");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		MyLog.i("TamPQ", "fragment onActivityCreated");
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// enable menu bar
		enableMenuBar(this);
		initMenuActionBar();
		MyLog.i("TamPQ", "fragment onResume");
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		MyLog.i("TamPQ", "fragment onSaveInstanceState");
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		MyLog.i("TamPQ", "fragment onStart");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		MyLog.i("TamPQ", "fragment onCreateView");
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_album_user_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);

		Bundle data = (Bundle) getArguments();
		customer = (CustomerDTO) data
				.getSerializable(IntentConstants.INTENT_CUSTOMER);
		routingId = data.getLong(IntentConstants.INTENT_ROUTING_ID);
		cycleId = data.getLong(IntentConstants.INTENT_CYCLE_ID);
//		shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		if (isFromGS) {
			shopId = customer.getShopId();
		} else {
			shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		}
		if (StringUtil.isNullOrEmpty(shopId)) {
			shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		}
		// TamPQ: giai phong TrainingReviewStaff de phan biet luong Hinh anh WW
		// voi DS Hinh anh
		if (trainingReviewStaffBundle != null) {
			if (trainingReviewStaffBundle.containsKey(IntentConstants.INTENT_STAFF_ID)
					&& !StringUtil.isNullOrEmpty(trainingReviewStaffBundle.getString(IntentConstants.INTENT_STAFF_ID))) {
				staffId = Integer.valueOf(trainingReviewStaffBundle.getString(IntentConstants.INTENT_STAFF_ID));
			}
		} else {
			staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		}

		SpannableObject spanObject = new SpannableObject();
		type = data.getInt(IntentConstants.INTENT_TYPE, 0);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DSHINHANH_HINHANHKH);
			if (type == 4) {
				spanObject
						.addSpan(StringUtil.getString(R.string.TITLE_VIEW_ALBUM_LIST_USER_FROM_ALBUM));
			} else {
				spanObject
						.addSpan(StringUtil.getString(R.string.TITLE_VIEW_ALBUM_LIST_USER_FROM_USER));
			}
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DSHINHANH_HINHANHKH);
			spanObject
					.addSpan(StringUtil.getString(R.string.TITLE_VIEW_GSNPP_ALBUM_LIST_USER));
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.TBHV_DSHINHANH_HINHANHKH);
			spanObject
					.addSpan(StringUtil.getString(R.string.TITLE_VIEW_GSNPP_ALBUM_LIST_USER));
		}

		// Lay doi tuong
		initView(view);

		if(isFromGS){
			spanObject = new SpannableObject();
			spanObject.addSpan(StringUtil.getString(R.string.TEXT_IMAGE_LIST_OF));
		}
		spanObject.addSpan(" " + customer.getCustomerName(),
				ImageUtil.getColor(R.color.WHITE),
				android.graphics.Typeface.BOLD);
		hideHeaderview();
		parent.setTitleName(spanObject);

		tvKH.setText(customer.getCustomerCode());
		tvHoTenKH.setText(customer.getCustomerName());

		if (model.isFirstInit) {
			gvUserImageView.setAdapter(thumbs);
			thumbs.notifyDataSetChanged();
			gvProgrameImageView.setAdapter(programeThumbs);
			programeThumbs.notifyDataSetChanged();
			updateDataCombobox();
		} else {
			gvProgrameImageView.setVisibility(View.GONE);
			model.setCustomer(customer);
			model.getListAlbum().clear();
			model.getListProgrameAlbum().clear();
			model.isFirstInit = true;
			initData();
			getListAlbumOfUser(true);
		}
		return v;
	}

	/**
	 *
	 * khoi tao menu cho man hinh
	 *
	 * @author: HaiTC3 support comment DungNT
	 * @return: void
	 * @throws:
	 */
	private void initMenuActionBar() {
		MenuTab[] tabs = new MenuTab[3];
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH_HINHANHKH);
//			tabs[2] = new MenuTab(R.string.TEXT_FEEDBACK, R.drawable.icon_reminders,
//					MENU_LIST_FEEDBACKS, PriHashMap.PriForm.NVBH_GOPY);
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH);
			addMenuItem(PriForm.NVBH_DSHINHANH_HINHANHKH, tabs);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR
				|| GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.GSNPP_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.GSNPP_HINHANH);
//			if (isFromGS == false) {
//				tabs[2] = new MenuTab(R.string.TEXT_LABLE_REVIEWS,
//						R.drawable.icon_note, MENU_REVIEW, PriForm.GSNPP_DANHGIA);
//			}else{
//				tabs[2] = new MenuTab(StringUtil.getString(R.string.TEXT_FEEDBACK),
//						R.drawable.icon_reminders, MENU_LIST_FEEDBACKS, PriForm.GSNPP_GOPY);
//			}
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.GSNPP_THONGTINKH);
			addMenuItem(PriForm.GSNPP_HINHANH, tabs);
		}
	}

	/**
	 * Di toi man hinh danh gia nhan vien Danh cho module gs npp
	 *
	 * @author banghn
	 * @param item
	 */
	public void gotoReviewsStaffView() {
		handleSwitchFragment(new Bundle(), ActionEventConstant.GO_TO_REVIEWS_STAFF_VIEW, SupervisorController.getInstance());
	}

	/**
	 * gotoFeedBackList
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoFeedBackList() {
		if (model.getCustomer() == null) {
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
				model.getCustomer());
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		handleSwitchFragment(bundle, ActionEventConstant.GET_LIST_CUS_FEED_BACK, SaleController.getInstance());

	}

	/**
	 * di toi man hinh cap nhat vi tri khach hang
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoCustomerLocation() {
		if (model.getCustomer() == null) {
			return;
		}

		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
				model.getCustomer());
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		handleSwitchFragment(bundle, ActionEventConstant.GOTO_CUSTOMER_LOCATION, UserController.getInstance());
	}

	/**
	 *
	 * Qua man hinh chi tiet co ban khach hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param
	 * @return: void
	 * @throws:
	 */
	public void gotoCustomerInfo() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(model.getCustomer().customerId));
		bunde.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromGS);
		handleSwitchFragment(bunde, ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param view
	 * @return: void
	 * @throws:
	 */

	private void initView(ViewGroup view) {
		gvUserImageView = (VNMStableGridView) PriUtils.getInstance().findViewByIdNotAllowInvisible(view, R.id.gvImageUserAlbumView, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_CHITIETHINHANH);
		PriUtils.getInstance().setOnItemClickListener(gvUserImageView, this);
		gvUserImageView.setExpanded(true);
		gvProgrameImageView = (VNMStableGridView) view
				.findViewById(R.id.gvImageProgrameAlbumView);
		gvProgrameImageView.setOnItemClickListener(this);
		gvProgrameImageView.setExpanded(true);


		btSearch = (Button) view.findViewById(R.id.btSearch);
		btSearch.setOnClickListener(this);

		tvKH = (TextView) view.findViewById(R.id.tvKH);
		tvHoTenKH = (TextView) view.findViewById(R.id.tvHoTenKH);

		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			btTakePhoto = (Button) PriUtils.getInstance().findViewById(view, R.id.btTakePhoto, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_CHUPHINH);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoFromDate, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_TUNGAY);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoToDate, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_DENNGAY);
			etFromDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etFromDate,
					PriHashMap.PriControl.NVBH_BANHANG_HINHANH_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			etToDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etToDate,
					PriHashMap.PriControl.NVBH_BANHANG_HINHANH_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			PriUtils.getInstance().findViewById(view, R.id.tvPhotoPrograme, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_TIMKIEM_CHUONGTRINH);
			spinnerPhotoPrograme = (Spinner) PriUtils.getInstance().findViewById(view, R.id.spinnerPhotoPrograme,
					PriHashMap.PriControl.NVBH_BANHANG_HINHANH_TIMKIEM_CHUONGTRINH);
			rlSearch = (LinearLayout) PriUtils.getInstance().findViewById(view, R.id.rlSearch, PriHashMap.PriControl.NVBH_BANHANG_HINHANH_TIMKIEM);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			btTakePhoto = (Button) PriUtils.getInstance().findViewById(view, R.id.btTakePhoto, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_CHUPHINH);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoFromDate, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_TUNGAY);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoToDate, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_DENNGAY);
			etFromDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etFromDate,
					PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			etToDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etToDate,
					PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			PriUtils.getInstance().findViewById(view, R.id.tvPhotoPrograme, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_TIMKIEMCHUONGTRINH);
			spinnerPhotoPrograme = (Spinner) PriUtils.getInstance().findViewById(view, R.id.spinnerPhotoPrograme,
					PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_TIMKIEMCHUONGTRINH);
			rlSearch = (LinearLayout) PriUtils.getInstance().findViewById(view, R.id.rlSearch, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_DSHINHANH_ALBUM_TIMKIEM);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			btTakePhoto = (Button) PriUtils.getInstance().findViewById(view, R.id.btTakePhoto, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_ALBUM_CHUPHINH);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoFromDate, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_TUNGAY);
			PriUtils.getInstance().findViewById(view, R.id.tvTakePhotoToDate, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_DENNGAY);
			etFromDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etFromDate,
					PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			etToDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.etToDate,
					PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			PriUtils.getInstance().findViewById(view, R.id.tvPhotoPrograme, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_TIMKIEM_CHUONGTRINH);
			spinnerPhotoPrograme = (Spinner) PriUtils.getInstance().findViewById(view, R.id.spinnerPhotoPrograme,
					PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_TIMKIEM_CHUONGTRINH);
			rlSearch = (LinearLayout) PriUtils.getInstance().findViewById(view, R.id.rlSearch, PriHashMap.PriControl.TBHV_HINHANH_DANHSACHHINHANH_TIMKIEM);
		}

		PriUtils.getInstance().setOnClickListener(btTakePhoto, this);
		PriUtils.getInstance().setOnTouchListener(etFromDate, this);
		PriUtils.getInstance().setOnTouchListener(etToDate, this);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerPhotoPrograme, this);

		etFromDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getFirstDateOfOffsetMonth(-2),
				DateUtils.defaultDateFormat.toPattern()));
		etToDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		etToDate.setIsHandleDefault(false);
		etFromDate.setIsHandleDefault(false);


		tvNoDataResult = (TextView) view.findViewById(R.id.tvNoDataResult);
		tvNoDataResult.setVisibility(View.GONE);
	}

	/**
	 *
	 * khoi tao data cho man hinh
	 *
	 * @author: HaiTC3 suport comment DungNT
	 * @return: void
	 * @throws:
	 */
	private void initData() {
		String[] typeAlbum = { "0", "1", "2" };
		String[] nameAlbum = {  StringUtil.getString(R.string.TEXT_PHOTO_CLOSE_DOOR),
								StringUtil.getString(R.string.TEXT_PHOTO_DISPLAY),
								StringUtil.getString(R.string.TEXT_PHOTO_SALE_POSITION) };
		for (int i = 0, size = typeAlbum.length; i < size; i++) {
			AlbumDTO albumInfo = new AlbumDTO();
			String type = typeAlbum[i];
			String name = nameAlbum[i];

			albumInfo.setAlbumType(Integer.parseInt(type));
			albumInfo.setAlbumTitle(name);
			model.getListAlbum().add(albumInfo);
		}

		//Su dung class ImageAdapter de hien thi hinh anh.
		if (thumbs == null) {
			thumbs = new ImageAdapter(parent, model.getListAlbum());
		}
		gvUserImageView.setAdapter(thumbs);

		if (programeThumbs == null) {
			programeThumbs = new ImageAdapter(parent,
					model.getListProgrameAlbum());
		}
		gvProgrameImageView.setAdapter(programeThumbs);

	}

	@Override
	public void onImageTakingPopupEvent(int eventType, Object data) {
		switch (eventType) {
		case OnImageTakingPopupListener.ACTION_TAKING_IMAGE_LOCATION:
			takePhoto(MediaItemDTO.TYPE_LOCATION_IMAGE);
			break;
		case OnImageTakingPopupListener.ACTION_TAKING_IMAGE_CTTB:
			takePhoto(MediaItemDTO.TYPE_DISPLAY_PROGAME_IMAGE);
			break;
		case OnImageTakingPopupListener.ACTION_TAKING_IMAGE_ALBUM_CTTB:
			dpTakePhoto = (DisplayProgrameDTO) data;
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
//				if (model.visitType == ListAlbumUserDTO.TYPE_IS_VISITED
//						|| model.visitType == ListAlbumUserDTO.TYPE_IS_OR) {
//					dpTakePhoto = (DisplayProgrameDTO) data;
//					takePhoto(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
//				} else {
//					parent.showDialog(getString(R.string.TEXT_NOTIFY_PLEASE_VISIT_BEFORE_TAKE_PHOTO));
//				}
				takePhoto(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
			} else {
				takePhoto(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
			}
			break;
		default:
			break;
		}

		if (popupTakeImage != null && popupTakeImage.isShowing()) {
			popupTakeImage.dismiss();
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case MENU_MAP: {
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerLocation();
			break;
		}
		case MENU_LIST_FEEDBACKS: {
			GlobalUtil.popBackStack(this.getActivity());
			gotoFeedBackList();
			break;
		}
		case MENU_INFO_DETAIL: {
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerInfo();
			break;
		}
		case MENU_REVIEW:
			GlobalUtil.popBackStack(this.getActivity());
			gotoReviewsStaffView();
			break;
		case ACTION_CLOSE:
			if (popupTakeImage != null && popupTakeImage.isShowing()) {
				popupTakeImage.dismiss();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		MyLog.i("TamPQ", "start camera");
		if (v == btTakePhoto) {
			showTakingImageDialog();
		} else if (v == btSearch) {
			GlobalUtil.forceHideKeyboard(parent);
			// luu lai gia tri de thuc hien tim kiem
			String dateTimePattern = StringUtil
					.getString(R.string.TEXT_DATE_TIME_PATTERN);
			Pattern pattern = Pattern.compile(dateTimePattern);
			if (!StringUtil.isNullOrEmpty(this.etFromDate.getText().toString())) {
				String strTN = this.etFromDate.getText().toString().trim();
				fromDate = etFromDate.getText().toString();
				Matcher matcher = pattern.matcher(strTN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strTN, "");
						String strFindTN = StringUtil.dateToString(tn,
								"yyyy-MM-dd");

						fromDateForRequest = strFindTN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil
								.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				fromDate = "";
				fromDateForRequest = "";
			}
			if (!StringUtil.isNullOrEmpty(this.etToDate.getText().toString())) {
				String strDN = this.etToDate.getText().toString().trim();
				toDate = etToDate.getText().toString();
				Matcher matcher = pattern.matcher(strDN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date dn = StringUtil.stringToDate(strDN, "");
						String strFindDN = StringUtil.dateToString(dn,
								"yyyy-MM-dd");

						toDateForRequest = strFindDN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil
								.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				toDate = "";
				toDateForRequest = "";
			}
			// luu lai combobox chuong trinh
			if (model.getListDisplayPrograme() == null) {
				programeCode = "";
			} else {
				int selected = spinnerPhotoPrograme.getSelectedItemPosition();
				if (selected < 0) {
					selected = 0;
				}
				if (listPrograme != null && listPrograme.size() > 0) {
					DisplayProgrameItemDTO itemDTO = listPrograme.get(selected);
					programeCode = itemDTO.value;
				}
			}

			if (!StringUtil.isNullOrEmpty(fromDateForRequest)
					&& !StringUtil.isNullOrEmpty(toDateForRequest)
					&& DateUtils.compareDate(fromDateForRequest,
							toDateForRequest) == 1) {
				GlobalUtil
						.showDialogConfirm(this, parent, StringUtil
								.getString(R.string.TEXT_DATE_TIME_INVALID_2),
								StringUtil
										.getString(R.string.TEXT_BUTTON_CLOSE),
								0, null, false);
			} else {
				getListAlbumOfPrograme(programeCode, fromDateForRequest,
						toDateForRequest);
			}
		} else {
			super.onClick(v);
		}
	}

	/**
	 *
	 * Request danh sach hinh anh cua 1 CTTB
	 *
	 * @author: TRUNGHQM
	 * @return: void
	 * @throws:
	 */
	private void getListAlbumOfPrograme(String programeCode2,
			String fromDateForRequest2, String toDateForRequest2) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, model
				.getCustomer().getCustomerId());
		bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		bundle.putString(IntentConstants.INTENT_STAFF_ID,
				String.valueOf(staffId));
		bundle.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE,
				programeCode2);
		bundle.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
				fromDateForRequest2);
		bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE,
				toDateForRequest2);
		handleViewEvent(bundle, ActionEventConstant.GO_TO_LIST_ALBUM_PROGRAME, SaleController.getInstance());
	}

	/**
	 * Show popup chup anh trung bay
	 *
	 * @author: BANGHN
	 */
	private void showTakingImageDialog() {
		if (popupTakeImage == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			PopupChooseDisplayProgram popup = new PopupChooseDisplayProgram(
					parent, this);

			ArrayList<DisplayProgrameDTO> data = model.getListPhotoDPrograme();
			popup.setTitlePopup(StringUtil.getString(R.string.TEXT_SELECT_ALBUM_TAKE_PHOTO));
			popup.setDataDisplayPrograme(data);
			build.setView(popup.view);
			popupTakeImage = build.create();
			popupTakeImage.setCanceledOnTouchOutside(false);
			Window window = popupTakeImage.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		popupTakeImage.show();
	}

	/**
	 * Chup hinh
	 *
	 * @author: TRUNGHQM
	 */
	public void takePhoto(int album) {
		if (model.visitType == ListAlbumUserDTO.TYPE_IS_VISITED
				|| model.visitType == ListAlbumUserDTO.TYPE_IS_OR
				// GS thi ko check ghe tham
				|| GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() != UserDTO.TYPE_STAFF
				 // hinh anh CTTB tong the thi ko can ghe tham
				|| album != MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE) {
			LatLng cusLatLng = new LatLng(customer.lat, customer.lng);
			LatLng myLatLng = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			if (myLatLng.lat > 0
					&& myLatLng.lng > 0
					&& cusLatLng.lat > 0
					&& cusLatLng.lng > 0
					&& GlobalUtil.getDistanceBetween(myLatLng, cusLatLng) <= model.shopDistance) {
				this.ofAlbum = album;
				parent.takenPhoto = GlobalUtil.takePhoto(parent,
						GlobalBaseActivity.RQ_SALE_TAKE_PHOTO);
			} else {
				String mess = StringUtil
						.getString(R.string.TEXT_TAKE_PHOTO_TOO_FAR_FROM_SHOP_1)
						+ " "
						+ customer.customerName
						+ " - "
						+ customer.customerCode
						+ " "
						+ StringUtil
								.getString(R.string.TEXT_TAKE_PHOTO_TOO_FAR_FROM_SHOP_2);

				GlobalUtil.showDialogConfirm(this, parent, mess,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
						ACTION_CLOSE, null, false);
			}
		} else {
			parent.showDialog(StringUtil.getString(R.string.TEXT_NOTIFY_PLEASE_VISIT_BEFORE_TAKE_PHOTO));
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (arg0 == gvUserImageView) {
			if (arg2 >= model.getListAlbum().size()) {
				return;
			}
			slideShowImage(arg2);
		} else if (arg0 == gvProgrameImageView) {
			if (arg2 >= model.getListProgrameAlbum().size()) {
				return;
			}
			goProgrameShowImage(arg2);
		}
	}

	/**
	 * checkIsTakePhoto
	 * @author: yennth16
	 * @since: 11:20:49 15-07-2015
	 * @return: boolean
	 * @throws:  
	 * @param id
	 * @return
	 */
	public boolean checkIsTakePhoto(long id){
		if(listPrograme != null && listPrograme.size() > 0 && id > 0){
			for (int i = 0; i < listPrograme.size(); i++) {
				if(String.valueOf(id).equals(listPrograme.get(i).value)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Vao man hinh chi tiet album cua chuong trinh
	 *
	 * @author ThanhNN
	 * @param arg2
	 */
	private void goProgrameShowImage(int index) {
		// TODO Auto-generated method stub
		if (model.getListProgrameAlbum().get(index).getNumImage() > 0) {
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, model
					.getCustomer().getCustomerId());
			bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
			bundle.putString(IntentConstants.INTENT_STAFF_ID,
					String.valueOf(staffId));
			bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, model
					.getCustomer().getCustomerName());
			bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, model
					.getCustomer().getCustomerCode());
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
					fromDate);
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, toDate);
			bundle.putInt(IntentConstants.INTENT_VISIT_TYPE, model.visitType);
			bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, model
					.getListProgrameAlbum().get(index));
			bundle.putInt(IntentConstants.INTENT_TYPE, type);
			bundle.putLong(IntentConstants.INTENT_ROUTING_ID, routingId);
			bundle.putLong(IntentConstants.INTENT_CYCLE_ID, cycleId);
			if(checkIsTakePhoto(model.getListProgrameAlbum().get(index).getDisplayProgrameId())){
				bundle.putBoolean(IntentConstants.INTENT_IS_TAKE_PHOTO, true);
			}else{
				bundle.putBoolean(IntentConstants.INTENT_IS_TAKE_PHOTO, false);
			}
			handleSwitchFragment(bundle,  ActionEventConstant.GO_TO_ALBUM_DETAIL_USER, SaleController.getInstance());
		}
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param arg2
	 * @return: void
	 * @throws:
	 */

	private void slideShowImage(int index) {
		if (model.getListAlbum().get(index).getNumImage() > 0) {
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, model
					.getCustomer().getCustomerId());
			bundle.putString(IntentConstants.INTENT_STAFF_ID,
					String.valueOf(staffId));
			bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
			bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, model
					.getCustomer().getCustomerName());
			bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, model
					.getCustomer().getCustomerCode());
			bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, model
					.getListAlbum().get(index));
			bundle.putInt(IntentConstants.INTENT_TYPE, type);

			handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ALBUM_DETAIL_USER, SaleController.getInstance());
		}
	}

	/**
	 *
	 * request get list album for user
	 *
	 * @author: HaiTC3 review code support comment for DungNT
	 * @return: void
	 * @throws:
	 */
	private void getListAlbumOfUser(boolean requestCombobox) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, model
				.getCustomer().getCustomerId());
		bundle.putString(IntentConstants.INTENT_STAFF_ID,
				String.valueOf(staffId));
		bundle.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		bundle.putBoolean(IntentConstants.INTENT_CHECK_COMBOBOX,
				requestCombobox);
		handleViewEvent(bundle, ActionEventConstant.GO_TO_LIST_ALBUM_USER, SaleController.getInstance());
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GO_TO_LIST_ALBUM_USER: {
			ListAlbumUserDTO tempModel = (ListAlbumUserDTO) modelEvent
					.getModelData();
			model.visitType = tempModel.visitType;
			model.shopDistance = tempModel.shopDistance;
			model.monthSeq = tempModel.monthSeq;
			for (int i = 0, size = model.getListAlbum().size(); i < size; i++) {
				AlbumDTO albumDTOOld = model.getListAlbum().get(i);
				AlbumDTO albumDTONew = tempModel.getListAlbum().get(i);

				albumDTOOld.setNumImage(albumDTONew.getNumImage());
				albumDTOOld.setThumbUrl(albumDTONew.getThumbUrl());
			}

			// add data to model
			if (tempModel.getListDisplayPrograme() != null
					&& tempModel.getListDisplayPrograme().size() > 0) {
				if (model.getListDisplayPrograme() != null) {
					model.getListDisplayPrograme().clear();
				}
				model.setListDisplayPrograme(tempModel.getListDisplayPrograme());
				if (tempModel.getListPhotoDPrograme() != null
						&& tempModel.getListPhotoDPrograme().size() > 0) {
					if (model.getListPhotoDPrograme() != null) {
						model.getListPhotoDPrograme().clear();
					}
					if (popupTakeImage != null) {
						popupTakeImage = null;
					}
					model.setListPhotoDPrograme(tempModel
							.getListPhotoDPrograme());
				}
				if (listPrograme != null) {
					listPrograme.clear();
					listPrograme = null;
				}
				updateDataCombobox();
			}
			if (tempModel.getListProgrameAlbum() != null
					&& tempModel.getListProgrameAlbum().size() > 0) {
				model.getListProgrameAlbum().clear();
				model.getListProgrameAlbum().addAll(tempModel.getListProgrameAlbum());
				gvProgrameImageView.setVisibility(View.VISIBLE);
				rlSearch.setVisibility(View.VISIBLE);
			}

//			thumbs = new ThumbnailAdapter(parent, new ImageAdapter(parent,
//					model.getListAlbum()),
//					new SimpleWebImageCache<ThumbnailBus, ThumbnailMessage>(
//							null, null, 101, bus), IMAGE_IDS);
			thumbs.notifyDataSetChanged();

			// render giao dien phan hinh anh cua chuong trinh
//			programeBus = new ThumbnailBus();
//			programeThumbs = new ThumbnailAdapter(parent, new ImageAdapter(
//					parent, model.getListProgrameAlbum()),
//					new SimpleWebImageCache<ThumbnailBus, ThumbnailMessage>(
//							null, null, 101, programeBus), IMAGE_IDS);
			programeThumbs.notifyDataSetChanged();
			requestInsertLogKPI(HashMapKPI.NVBH_LAYDANHSACHHINHANH, modelEvent.getActionEvent());
			break;
		}
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER: {
			requestInsertLogKPI(HashMapKPI.NVBH_CHUPHINHTAIDIEMBAN, modelEvent.getActionEvent());
			getListAlbumOfUser(false);
			// gui broadcast cho man hinh xem chi tiet album neu nhan chup hinh
			// tu chi tiet album
			PhotoThumbnailListView orderFragment = (PhotoThumbnailListView)
					findFragmentByTag(GlobalUtil.getTag(PhotoThumbnailListView.class));
			if (orderFragment != null) {
				orderFragment.receiveBroadcast(
						ActionEventConstant.UPDATE_TAKEN_PHOTO, null);
			}
			dpTakePhoto = null;
			break;
		}
		case ActionEventConstant.UPDATE_NEW_URL_TO_DB: {
			// xoa file anh hien tai
			File file = new File(parent.takenPhoto.getAbsolutePath());
			if (file.exists())
				file.delete();

			getListAlbumOfUser(false);
			// gui broadcast cho man hinh xem chi tiet album neu nhan chup hinh
			// tu chi tiet album
			PhotoThumbnailListView orderFragment = (PhotoThumbnailListView)
					findFragmentByTag(GlobalUtil.getTag(PhotoThumbnailListView.class));
			if (orderFragment != null) {
				orderFragment.receiveBroadcast(
						ActionEventConstant.UPDATE_TAKEN_PHOTO, null);
			}
			break;
		}
		case ActionEventConstant.INSERT_MEDIA_ITEM: {

			break;
		}
		case ActionEventConstant.GO_TO_LIST_ALBUM_PROGRAME: {
			// update lai model
			@SuppressWarnings("unchecked")
			ArrayList<AlbumDTO> tempArrayListPrograme = (ArrayList<AlbumDTO>) modelEvent
					.getModelData();
			if (tempArrayListPrograme != null
					&& tempArrayListPrograme.size() > 0) {
				model.getListProgrameAlbum().clear();
				model.getListProgrameAlbum().addAll(tempArrayListPrograme);
				tvNoDataResult.setVisibility(View.GONE);
				gvProgrameImageView.setVisibility(View.VISIBLE);
				gvProgrameImageView.requestFocus();
			} else {
				gvProgrameImageView.setVisibility(View.GONE);
				tvNoDataResult.setVisibility(View.VISIBLE);
				tvNoDataResult.requestFocus();
			}

			// render lai giao dien phan hinh anh chuong trinh
			// clear programeThumbs
//			programeThumbs = new ThumbnailAdapter(parent, new ImageAdapter(
//					parent, model.getListProgrameAlbum()),
//					new SimpleWebImageCache<ThumbnailBus, ThumbnailMessage>(
//							null, null, 101, bus), IMAGE_IDS);
//			gvProgrameImageView.setAdapter(programeThumbs);
			programeThumbs.notifyDataSetChanged();
			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {

		case ActionEventConstant.INSERT_MEDIA_ITEM:
			switch (modelEvent.getModelCode()) {

			case ErrorConstants.ERROR_CODE_SUCCESS:
				// da ghi log , fullDate la truong hop offline
				getListAlbumOfUser(true);
				break;
			default:
				super.handleErrorModelViewEvent(modelEvent);
				break;
			}
			break;

		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	public class ImageAdapter extends BaseAdapter {

		private ArrayList<AlbumDTO> list = new ArrayList<AlbumDTO>();

		public ImageAdapter(Context c, ArrayList<AlbumDTO> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup pt) {
			View row = convertView;
			ViewHolder holder = null;
			if (convertView == null) {
				LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) parent)
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = layout.inflate(R.layout.layout_album_user, pt, false);
				holder = new ViewHolder();
				holder.imageView = (ImageView) row
						.findViewById(R.id.imgAlbumImage);
				holder.imageViewBg = (ImageView) row
						.findViewById(R.id.imgAlbumBackground);
				holder.titleAlbum = (TextView) row
						.findViewById(R.id.tvAlbumName);
				holder.numImage = (TextView) row.findViewById(R.id.tvNumImage);
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			holder.imageView.setImageResource(R.drawable.album_default);
			holder.imageViewBg.setImageResource(R.drawable.album_background);
			if (!StringUtil.isNullOrEmpty(list.get(position).getThumbUrl())) {
				if (list.get(position).getThumbUrl()
						.contains(ExternalStorage.SDCARD_PATH))
					holder.imageView.setTag(list.get(position).getThumbUrl());
				else
					holder.imageView.setTag(ServerPath.IMAGE_PATH
							+ list.get(position).getThumbUrl());
			}
			if (!StringUtil.isNullOrEmpty(list.get(position).getThumbUrl())) {
				if (list.get(position).getThumbUrl()
						.contains(ExternalStorage.SDCARD_PATH)){
					ImageUtil.loadImage(list.get(position).getThumbUrl(),
							holder.imageView);
				}
				else{
					ImageUtil.loadImage(ServerPath.IMAGE_PATH + list.get(position).getThumbUrl(),
							holder.imageView);
				}

			}

			holder.titleAlbum.setText(list.get(position).getAlbumTitle());
			holder.numImage.setText(list.get(position).getNumImage()
					+ " " + StringUtil.getString(R.string.TEXT_PHOTO_1));

			return row;
		}

	}

	public static class ViewHolder {
		public ImageView imageView;
		public ImageView imageViewBg;
		public TextView titleAlbum;
		public TextView numImage;
	}

	public void updateTakenPhoto() {
		// TODO Auto-generated method stub
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		viewData.add(IntentConstants.INTENT_STAFF_ID);
		viewData.add(Integer.toString(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId()));
		
		viewData.add(IntentConstants.INTENT_ROUTING_ID);
		viewData.add(Constants.STR_BLANK + routingId);
//		viewData.add(IntentConstants.INTENT_CYCLE_ID);
//		viewData.add(Constants.STR_BLANK + cycleId);
		
		viewData.add(IntentConstants.INTENT_SHOP_ID);
		if (!StringUtil.isNullOrEmpty(shopId)) {
			viewData.add(shopId);
		}

		if (parent.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(parent.takenPhoto.getName());
			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(parent.takenPhoto.getAbsolutePath());
		}

		// tao doi tuong mediaItem de insert row du lieu
		MediaItemDTO dto = new MediaItemDTO();

		try {
			dto.objectId = Long.parseLong(model.getCustomer().getCustomerId());
			dto.objectType = ofAlbum;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = parent.takenPhoto.getAbsolutePath();
			dto.thumbUrl = parent.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
			dto.routingId =  routingId;
//			dto.type = 1;
			dto.status = 1;
			if (!StringUtil.isNullOrEmpty(shopId)) {
				dto.shopId = Integer.valueOf(shopId);
			}

			dto.fileSize = parent.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(model.getCustomer().getCustomerId());
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));

			if (dpTakePhoto != null
					&& ofAlbum == MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE) {
				dto.displayProgrameId = dpTakePhoto.displayProgrameId;
				viewData.add(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
				viewData.add("" + dpTakePhoto.displayProgrameId);
				if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == 1) {
					dto.monthSeq = model.monthSeq;
					viewData.add(IntentConstants.INTENT_MONTH_SEQ);
					viewData.add(String.valueOf(dto.monthSeq));
				}
				if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
					// da chup hinh trung bay thi set lai
					// isHaveDisplayProgramNotYetVote = false
					CustomerListItem lastVisitCustomer = GlobalInfo.getInstance().getProfile().getLastVisitCustomer();
					if (lastVisitCustomer != null && lastVisitCustomer.aCustomer != null && lastVisitCustomer.aCustomer.customerId == dto.objectId) {
						parent.removeMenuCloseCustomer();
						if (lastVisitCustomer.displayProgramIdCaptured == null) {
							lastVisitCustomer.displayProgramIdCaptured = new ArrayList<String>();
						}
						// neu id CTTB nay moi add vao mang da hoan thanh cham
						// CTTB
						String idProgram = dto.displayProgrameId + "";
						if (!lastVisitCustomer.displayProgramIdCaptured.contains(idProgram)) {
							lastVisitCustomer.displayProgramIdCaptured.add(idProgram);
						}
						// update bien kiem tra cham xong chuong trinh hay chua
						lastVisitCustomer.checkCaptureDisplayProgram();
					}
				}
			}
			viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
			viewData.add(model.getCustomer().customerCode);
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
//			viewData.add(IntentConstants.INTENT_TYPE);
//			viewData.add("1");
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}

		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// cau request du lieu man hinh
				getListAlbumOfUser(true);
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
//		if (thumbs != null) {
//			thumbs.recycleAllBitmaps();
//		}
//		if (programeThumbs != null) {
//			programeThumbs.recycleAllBitmaps();
//		}
	}

	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		if (arg0 == etFromDate) {
			if (!etFromDate.onTouchEvent(arg1)) {
				currentCalender = DATE_FROM_CONTROL;
				parent.showDatePickerDialog(etFromDate.getText().toString(), true, this);
			}
		}
		if (arg0 == etToDate) {
			if (!etToDate.onTouchEvent(arg1)) {
				currentCalender = DATE_TO_CONTROL;
				parent.showDatePickerDialog(etToDate.getText().toString(), true, this);
			}
		}
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Ham tra ve khi picker dayInOrder
	 *
	 * @author: ThanhNN8
	 * @param dayOfMonth
	 * @param monthOfYear
	 * @param year
	 * @return: void
	 * @throws:
	 */
	public void updateDate(int dayOfMonth, int monthOfYear, int year) {
		// TODO Auto-generated method stub
		String sDay = String.valueOf(dayOfMonth);
		String sMonth = String.valueOf(monthOfYear + 1);
		if (dayOfMonth < 10) {
			sDay = "0" + sDay;
		}
		if (monthOfYear + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		if (currentCalender == DATE_FROM_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				etFromDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				etFromDate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/")
						.append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent,
						StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
						null, false);
			}
		}
		if (currentCalender == DATE_TO_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				etToDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				etToDate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/")
						.append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent,
						StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
						null, false);
			}
		}
	}

	private void updateDataCombobox() {
		// TODO Auto-generated method stub
		if (listPrograme == null) {
			listPrograme = new ArrayList<DisplayProgrameItemDTO>();
			DisplayProgrameItemDTO itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_ALL);
			itemDTO.fullName = StringUtil.getString(R.string.TEXT_ALL);
			itemDTO.value = "";
			if (model.getListDisplayPrograme() != null
					&& model.getListDisplayPrograme().size() > 0) {
				listPrograme.add(itemDTO);
			}

			if (model.getListDisplayPrograme() != null) {
				for (int j = 0, jlength = model.getListDisplayPrograme().size(); j < jlength; j++) {
					itemDTO = model.getListDisplayPrograme().get(j);
					listPrograme.add(itemDTO);
				}
			}

		}
		int lengthPrograme = listPrograme.size();
		String programeCode[] = new String[lengthPrograme];
		// khoi tao gia tri cho ma chuong trinh
		for (int i = 0; i < lengthPrograme; i++) {
			DisplayProgrameItemDTO dto = listPrograme.get(i);
			if (i == 0)
				programeCode[i] = dto.name;
			else
				programeCode[i] = dto.value + " - " + dto.name ;
		}

//		SpinnerAdapter adapterNVBH = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, programeCode);
		ArrayAdapter adapterNVBH = new ArrayAdapter(parent,
				R.layout.simple_spinner_item, programeCode) ;
		this.spinnerPhotoPrograme.setAdapter(adapterNVBH);
		spinnerPhotoPrograme.setSelection(0);

	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}
}
