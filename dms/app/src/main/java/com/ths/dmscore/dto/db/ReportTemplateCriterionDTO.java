package com.ths.dmscore.dto.db;

import java.io.Serializable;

public class ReportTemplateCriterionDTO extends AbstractTableDTO implements Serializable {

	public static final long serialVersionUID = 1L;

	public int reportTemplateCriterionId;
	public int criterialType;
	public int criterialId;
	public int parentCriterionId;
	public String criterialName;
	public String createDate;
	public String updateDate;
	public String createUser;
	public String updateUser;

	// ds cac tieu chi report theo row va column
	public static final int QUANTITY = 1;
	public static final int MONTH = 2;
	public static final int QUARTER = 3;
	public static final int PRODUCT = 4;
	public static final int CAT = 5;
	public static final int SUBCAT = 6;
	public static final int CUSTOMER = 7;
	public static final int STAFF = 8;
	public static final int SUPERVISOR = 9;
	public static final int MANAGER = 10;
	public static final int UNIT = 11;
	public static final int TBHV = 12;
	public static final int TTTT = 13;
	public static final int GDM = 14;
	public static final int TRUONGKENH = 15;
	public static final int NPP = 16;
	public static final int VUNG = 17;
	public static final int MIEN = 18;
	public static final int KENH = 19;
	public static final int WEEK = 20;
	public static final int AMOUNT = 21;
	public static final int DATE = 22;
	public static final int YEAR = 23;
	// danh cho cac loai report theo thoi gian: ngay, tuan, thang, quy, nam
	public static final int TIME = 24;

}
