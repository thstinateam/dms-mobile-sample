/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO.StaffItemDTO;
import com.ths.dmscore.dto.view.ReportKeyshopDTO;
import com.ths.dmscore.dto.view.ReportKeyshopItemDTO;
import com.ths.dmscore.dto.view.ValueItemDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager.OnSortChange;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Bao cao key shop role GSBH
 *
 * @author: yennth16
 * @version: 1.0
 * @since: 14:18:36 15-07-2015
 */
public class ReportKeyShopView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener, OnClickListener,
		 OnItemSelectedListener, OnSortChange {

	private static final int MENU_CUSTOMER = 15;
	private static final int MENU_SEARCH_IMAGE = 18;

	private Button btSearch;// tim kiem
	private DMSTableView tbReportKeyshop;
	public ReportKeyshopDTO ilDTO = new ReportKeyshopDTO();
	boolean isReload = false;
	private Spinner spNVBH;
	private Spinner spPrograme;
	private Spinner spLine;
	SpinnerAdapter adapterLine;
	SpinnerAdapter adapterNPP;
	SpinnerAdapter adapterNVBH;
	SpinnerAdapter adapterDisPlay;
	private boolean isSearch;
	private String programeId;
	private String gsnppStaffId;
	private String gsnppShopId;
	private boolean isFromTBHV;
	// vo man hinh lan dau tien hay kg?
	private boolean isFirst = true;

	// don vi
	private Spinner spShop;
	ListComboboxShopStaffDTO combobox = new ListComboboxShopStaffDTO();
	private int indexSpNVBH = -1;
	private int indexSPShop = -1;
	private int indexSPLine;
	private int indexSPProgram;
	private String staffId;
	private String textShopId = "";
	private String[] arrMaNPP;;
	private String[] arrDisplayPro;
	private String cycleId = Constants.STR_BLANK;

	public static ReportKeyShopView getInstance(Bundle b) {
		ReportKeyShopView f = new ReportKeyShopView();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		gsnppStaffId = getArguments()
				.getString(IntentConstants.INTENT_STAFF_ID);
		if (StringUtil.isNullOrEmpty(gsnppStaffId)) {
			gsnppStaffId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			gsnppShopId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId());
		} else {
			isFromTBHV = true;
			// gsnppStaffName =
			// getArguments().getString(IntentConstants.INTENT_STAFF_NAME);
			gsnppShopId = getArguments().getString(
					IntentConstants.INTENT_SHOP_ID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_report_key_shop_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		if( GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_BAOCAOKEYSHOP);
		}else{
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.TBHV_BAOCAOKEYSHOP);
		}
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_REPORT_KEYSHOP));
		initView(v);
		initHeaderTable(v);
		if (isFirst) {
			textShopId = gsnppShopId;
			getListCombobox();
		} else {
			spShop.setAdapter(adapterNPP);
			spShop.setSelection(indexSPShop);
			spPrograme.setAdapter(adapterDisPlay);
			spPrograme.setSelection(indexSPProgram);
			spNVBH.setAdapter(adapterNVBH);
			spNVBH.setSelection(indexSpNVBH);
			spLine.setAdapter(adapterLine);
			spLine.setSelection(indexSPLine);
			// reset mang total
			ilDTO.total = null;
			ilDTO.total = new ReportKeyshopItemDTO();
			renderLayout();
		}
		return v;

	}

	/**
	 * reqquest list SHOP va list NVBH cua shop
	 * @author: yennth16
	 * @since: 15:49:20 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void getListCombobox() {
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_DATA, combobox);
		b.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		b.putString(
				IntentConstants.INTENT_STAFF_OWNER_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		b.putBoolean(IntentConstants.INTENT_ORDER, true);
		b.putBoolean(IntentConstants.INTENT_IS_ALL, true);
		b.putBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST, isFirst);
		b.putBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID, false);
		handleViewEvent(b,
				ActionEventConstant.ACTION_GET_COMBOBOX_CTHTTM,
				SupervisorController.getInstance());
	}

	/**
	 *  khoi tao spriner NVBH va don vi
	 * @author: yennth16
	 * @since: 15:49:06 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpinner() {
		indexSPLine = 0;
		indexSpNVBH = 0;
		indexSPProgram = 0;
		indexSPShop = 0;
		if (isFirst) {
			initSpNPP();
			isFirst = false;
		}
		initSpNVBH();
		initSpDisplayProgram();
		initCycleInfo();
		initValueSearch();
	}

	/**
	 * initCycleInfo
	 * @author: yennth16
	 * @since: 09:34:29 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initCycleInfo() {
		if (combobox.listCycle != null && combobox.listCycle.size() > 0) {
			String[] arrMaNPP = new String[combobox.listCycle.size()];
			int i = 0;
			for (ValueItemDTO shop : combobox.listCycle) {
				if (!StringUtil.isNullOrEmpty(shop.name)) {
					arrMaNPP[i] = shop.name;
					if (shop.id.equalsIgnoreCase(combobox.curentCycle)) {
						indexSPLine = i;
					}
				} else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spLine.setAdapter(adapterLine);
			if (indexSPLine < 0) {
				spLine.setSelection(0);
			} else
				spLine.setSelection(indexSPLine);
			if (spLine.getSelectedItemPosition() != -1) {
				textShopId = String.valueOf(combobox.listShop.get(spShop
						.getSelectedItemPosition()).shopId);
			}
		} else {
			adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spLine.setAdapter(adapterLine);
			// truong hop ko co ds nv ban hang
			renderLayout();
		}

	}

	/**
	 * initSpNPP
	 * @author: yennth16
	 * @since: 15:48:55 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNPP() {
		if (combobox.listShop != null && combobox.listShop.size() > 0) {
			String[] arrMaNPP = new String[combobox.listShop.size()];
			int i = 0;
			for (ShopDTO shop : combobox.listShop) {
				if (!StringUtil.isNullOrEmpty(shop.shopCode)
						&& !StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopCode + " - " + shop.shopName;
				} else if (StringUtil.isNullOrEmpty(shop.shopCode)
						&& !StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopName;
				} else if (!StringUtil.isNullOrEmpty(shop.shopCode)
						&& StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopCode;
				} else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spShop.setAdapter(adapterNPP);
			if (indexSPShop < 0) {
				spShop.setSelection(0);
			} else
				spShop.setSelection(indexSPShop);
			if (spShop.getSelectedItemPosition() != -1) {
				textShopId = String.valueOf(combobox.listShop.get(spShop
						.getSelectedItemPosition()).shopId);
			}
		} else {
			adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spShop.setAdapter(adapterNPP);
			// truong hop ko co ds nv ban hang
			ilDTO.listItem.clear();
			renderLayout();
		}

	}

	/**
	 * initSpNVBH
	 * @author: yennth16
	 * @since: 15:48:43 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNVBH() {
		if (combobox.listStaff != null && combobox.listStaff.size() > 0) {
			int size = combobox.listStaff.size();
			// cong size them 1 do add them tat ca vap mang
			arrMaNPP = new String[size + 1];
			int i = 0;
			StaffItemDTO item1 = new ListComboboxShopStaffDTO().newStaffItem();
			item1.addAll(-1, StringUtil.getString(R.string.TEXT_ALL), "");
			combobox.listStaff.add(0, item1);
			for (StaffItemDTO item : combobox.listStaff) {
				if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode + " - " + item.staffName;
				} else if (StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffName;
				} else if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode;
				} else {
					arrMaNPP[i] = "";
				}
				i++;

			}
			adapterNVBH = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spNVBH.setAdapter(adapterNVBH);
			spNVBH.setSelection(indexSpNVBH);
			indexSpNVBH = spNVBH.getSelectedItemPosition();
			if (spNVBH.getSelectedItemPosition() > 0) {
				staffId = String.valueOf(combobox.listStaff.get(spNVBH
						.getSelectedItemPosition()).staffId);
			}
		} else {
			// truong hop ko co ds nv ban hang
			adapterNVBH = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spNVBH.setAdapter(adapterNVBH);
//			ilDTO.totalCustomer = 0;
			ilDTO.listItem.clear();
			renderLayout();
		}
	}

	/**
	 * initSpDisplayProgram
	 * @author: yennth16
	 * @since: 15:48:33 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpDisplayProgram() {
		if (combobox.listDisplayProgramThreeCycle != null
				&& combobox.listDisplayProgramThreeCycle.size() > 0) {
			int size = combobox.listDisplayProgramThreeCycle.size();
			// cong size them 1 do add them tat ca vap mang
			arrDisplayPro = new String[size + 1];
			int i = 0;
			DisplayProgrameDTO item1 = new DisplayProgrameDTO();
			item1.addAll(-1, StringUtil.getString(R.string.TEXT_ALL), "");
			combobox.listDisplayProgramThreeCycle.add(0, item1);
			for (DisplayProgrameDTO item : combobox.listDisplayProgramThreeCycle) {
				if (!StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& !StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameCode + " - "
							+ item.displayProgrameName;
				} else if (StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& !StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameName;
				} else if (!StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameCode;
				} else {
					arrDisplayPro[i] = "";
				}
				i++;

			}
			adapterDisPlay = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrDisplayPro);
			spPrograme.setAdapter(adapterDisPlay);
			spPrograme.setSelection(indexSPProgram);
			indexSPProgram = spPrograme.getSelectedItemPosition();
			if (spPrograme.getSelectedItemPosition() > 0) {
				programeId = String
						.valueOf(combobox.listDisplayProgramThreeCycle.get(spPrograme
								.getSelectedItemPosition()).displayProgrameId);
			}
		} else {
			// truong hop ko co ds nv ban hang
			adapterDisPlay = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spPrograme.setAdapter(adapterDisPlay);
//			ilDTO.totalCustomer = 0;
			ilDTO.listItem.clear();
			renderLayout();
		}
	}

	/**
	 * initView
	 * @author: yennth16
	 * @since: 15:48:23 21-07-2015
	 * @return: void
	 * @throws:
	 * @param v
	 */
	private void initView(View v) {
		PriUtils.getInstance()
					.genPriHashMapForForm(PriForm.GSNPP_DSHINHANH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.lnSearch,
					PriControl.GSNPP_DSHINHANH_TIMKIEM);
		spNVBH = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
					R.id.spNVBH, PriControl.GSNPP_DSHINHANH_NHANVIEN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvStaff,
					PriControl.GSNPP_DSHINHANH_NHANVIEN);

		spPrograme = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
					R.id.spPrograme, PriControl.GSNPP_DSHINHANH_CHUONGTRINH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvPrograme,
					PriControl.GSNPP_DSHINHANH_CHUONGTRINH);

		spLine = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
					R.id.spLine, PriControl.GSNPP_DSHINHANH_TUYEN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvLine,
					PriControl.GSNPP_DSHINHANH_TUYEN);
		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v,
					R.id.btSearch, PriControl.GSNPP_DSHINHANH_TIMKIEM);
		btSearch.setVisibility(View.GONE);
		PriUtils.getInstance().setOnItemSelectedListener(spNVBH, this);
		PriUtils.getInstance().setOnItemSelectedListener(spPrograme, this);
		PriUtils.getInstance().setOnItemSelectedListener(spLine, this);
		PriUtils.getInstance().setOnClickListener(btSearch, this);

		tbReportKeyshop = (DMSTableView) v.findViewById(R.id.tbReportKeyshop);
		tbReportKeyshop.setListener(this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.lnSearch,
				PriControl.GSNPP_DSHINHANH_DSDONVI);
		spShop = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spShop, PriControl.GSNPP_DSHINHANH_DSDONVI);
		PriUtils.getInstance().setOnItemSelectedListener(spShop, this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvShop,
				PriControl.GSNPP_DSHINHANH_DSDONVI);

	}


	/**
	 * init gia tri tim kiem
	 *
	 * @author: hoanpd1
	 * @since: 09:53:34 14-04-2015
	 * @return: void
	 * @throws:
	 */
	private void initValueSearch() {
		tbReportKeyshop.getPagingControl().totalPage = -1;
		indexSPProgram = spPrograme.getSelectedItemPosition();
		indexSPLine = spLine.getSelectedItemPosition();
		indexSpNVBH = spNVBH.getSelectedItemPosition();
		indexSPShop = spShop.getSelectedItemPosition();
		if (combobox.listShop != null && combobox.listShop.size() > 0) {
			textShopId = String.valueOf(combobox.listShop.get(spShop
					.getSelectedItemPosition()).shopId);
		}
		programeId = Constants.STR_BLANK;
		staffId = Constants.STR_BLANK;
		if (spPrograme.getSelectedItemPosition() > 0) {
			programeId = String.valueOf(combobox.listDisplayProgramThreeCycle
					.get(indexSPProgram).displayProgrameId);
		}
		if (spNVBH.getSelectedItemPosition() > 0) {
			staffId = String.valueOf(combobox.listStaff.get(spNVBH
					.getSelectedItemPosition()).staffId);
		}

		if (spLine.getSelectedItemPosition() >= 0) {
			cycleId = String.valueOf(combobox.listCycle
					.get(indexSPLine).id);
		}
		getReportList(1);
	}

	/**
	 * initHeaderTable
	 * @author: yennth16
	 * @since: 15:21:49 15-07-2015
	 * @return: void
	 * @throws:
	 * @param v
	 */
	private void initHeaderTable(View v) {
		// init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(1, SortActionConstants.STAFF)
				.addInfoCaseUnSensitive(2, SortActionConstants.NAME)
				.addInfo(6, SortActionConstants.AMOUNT_PLAN)
				.addInfo(7, SortActionConstants.AMOUNT_DONE)
				.addInfo(8, SortActionConstants.QUANITY_PLAN)
				.addInfo(9, SortActionConstants.QUANITY_DONE)
				.build();
		// khoi tao header cho table
		initHeaderTable(tbReportKeyshop, new ReportKeyShopRow(parent), lstSort,
				this);
	}

	/**
	 * getReportList
	 * @author: yennth16
	 * @since: 15:01:56 22-07-2015
	 * @return: void
	 * @throws:
	 * @param page
	 */
	private void getReportList(int page) {// (int page) {
		if (!parent.isShowProgressDialog())
			parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, programeId);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		data.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		if(!StringUtil.isNullOrEmpty(staffId)){
			data.putString(IntentConstants.INTENT_STAFF_ID, staffId);
		}else{
			data.putString(IntentConstants.INTENT_STAFF_ID, combobox.listStaffId);
		}

		data.putSerializable(IntentConstants.INTENT_SORT_DATA,
				tbReportKeyshop.getSortInfo());

		handleViewEvent(data, ActionEventConstant.GO_TO_REPORT_LIST_STAFF,
				SupervisorController.getInstance());
	}


	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.ACTION_GET_COMBOBOX_CTHTTM:
			combobox = (ListComboboxShopStaffDTO) modelEvent.getModelData();
			if (combobox != null) {
				initSpinner();
			} else {
				// truong hop ko co ds nv ban hang
				ilDTO.listItem.clear();
				renderLayout();
			}
			break;
		case ActionEventConstant.GO_TO_REPORT_LIST_STAFF:
			ReportKeyshopDTO tempDto = (ReportKeyshopDTO) modelEvent.getModelData();
			ilDTO = tempDto;
			renderLayout();
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 * renderLayout
	 * @author: yennth16
	 * @since: 15:44:30 22-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbReportKeyshop.getPagingControl().getCurrentPage() - 1);
		tbReportKeyshop.clearAllData();
		ReportKeyShopRow row;
		if (ilDTO.listItem.size() > 0) {
			for (int i = 0, s = ilDTO.listItem.size(); i < s; i++) {
				ReportKeyshopItemDTO item = ilDTO.listItem.get(i);
			    row = new ReportKeyShopRow(parent);
				row.renderLayout(pos, ilDTO.listItem.get(i));
				pos++;
				row.setListener(this);
				tbReportKeyshop.addRow(row);
				ilDTO.total.amountRegister += item.amountRegister;
				ilDTO.total.amountDone += item.amountDone;
				ilDTO.total.quantityRegister += item.quantityRegister;
				ilDTO.total.quantityDone += item.quantityDone;
				ilDTO.total.countCustomerRegister += item.countCustomerRegister;
				ilDTO.total.countDone += item.countDone;
			}
			ilDTO.total.percent =  StringUtil.calPercentUsingRound(ilDTO.total.countCustomerRegister, ilDTO.total.countDone);
			row = new ReportKeyShopRow(parent);
			row.renderLayoutTotal(ilDTO.total);
			tbReportKeyshop.addRow(row);
		}else {
			tbReportKeyshop.showNoContentRow();
		}



	}

	/**
	 *
	 * xu ly khi click button tim kiem
	 *
	 * @author: HoanPD1
	 * @param : v
	 * @return: void
	 * @throws:
	 */
	@Override
	public void onClick(View v) {
		if (v == btSearch && ilDTO != null) {
			// hide ban phim
			initValueSearch();
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void resetAllValue() {
		isSearch = false;
		indexSPLine = 0;
		staffId = "";
		indexSpNVBH = -1;
		indexSPProgram = 0;
		programeId = "";
		isFirst = true;
		textShopId = gsnppShopId;
		resetLayout();
		indexSPShop = -1;
	}

	/**
	 *
	 * Reset gia tri tren layout ve gia tri mac dinh
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	private void resetLayout() {
		spLine.setSelection(0);
		spPrograme.setSelection(0);
		spNVBH.setSelection(0);
		spShop.setSelection(0);
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case MENU_CUSTOMER:
			handleSwitchFragment(null,
					ActionEventConstant.GO_TO_CUSTOMER_SALE_LIST,
					SupervisorController.getInstance());
			break;
		case MENU_SEARCH_IMAGE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_SEARCH_IMAGE,
					SupervisorController.getInstance());
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {

		switch (action) {
		case ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH: {
			ReportKeyshopItemDTO dto = (ReportKeyshopItemDTO) data;
			gotoReportStaff(dto);
			break;
		}
		default:
			break;
		}
	}

	/**
	 * Toi man hinh ds album cua kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customer
	 * @return: void
	 * @throws:
	 */
	private void gotoReportStaff(ReportKeyshopItemDTO dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_SHOP_ID, Constants.STR_BLANK + 	dto.staff.shopId);
		bundle.putString(IntentConstants.INTENT_STAFF_ID, Constants.STR_BLANK + dto.staff.staffId);
		bundle.putString(IntentConstants.INTENT_STAFF_NAME, dto.staff.name);
		bundle.putLong(IntentConstants.INTENT_KS_ID, dto.ksShop.ksId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH_VIEW,
				SupervisorController.getInstance());
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spShop) {
			if (indexSPShop != spShop.getSelectedItemPosition()) {
				indexSPLine = spLine.getSelectedItemPosition();
				indexSpNVBH = -1;
				if (combobox.listShop.size() > spShop.getSelectedItemPosition())
					textShopId = String.valueOf(combobox.listShop.get(spShop
							.getSelectedItemPosition()).shopId);
				if (!isFirst) {
					combobox.listStaff = null;
					getListCombobox();
				}
				isFirst = false;
			}
		} else if (arg0 == spNVBH) {
			if (indexSpNVBH != spNVBH.getSelectedItemPosition()) {
				onClick(btSearch);
			}
		} else if (arg0 == spPrograme) {
			if (indexSPProgram != spPrograme.getSelectedItemPosition()) {
				onClick(btSearch);
			}
		} else if (arg0 == spLine
				&& arg0.getSelectedItemPosition() != indexSPLine) {
			indexSPLine = spLine.getSelectedItemPosition();
			if (spNVBH.getSelectedItemPosition() > 0) {
				staffId = String.valueOf(combobox.listStaff.get(spNVBH
						.getSelectedItemPosition()).staffId);
			}
			onClick(btSearch);
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// hide ban phim
				GlobalUtil.forceHideKeyboard(parent);
				tbReportKeyshop.resetSortInfo();
				resetAllValue();
				getListCombobox();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}



	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getReportList(tbReportKeyshop.getPagingControl().getCurrentPage());
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub

	}
}
