package com.ths.dmscore.util.TextDrawable;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

/**
 * @author amulya
 * @datetime 14 Oct 2014, 5:20 PM
 */
public class ColorGenerator {

    private static volatile ColorGenerator DEFAULT;

    private static volatile ColorGenerator MATERIAL;

    static {
        setDEFAULT(create(Arrays.asList(
                0xfff16364,
                0xfff58559,
                0xfff9a43e,
                0xffe4c62e,
                0xff67bf74,
                0xff59a2be,
                0xff2093cd,
                0xffad62a7,
                0xff805781
        )));
        setMATERIAL(create(Arrays.asList(
                0xffe57373,
                0xfff06292,
                0xffba68c8,
                0xff9575cd,
                0xff7986cb,
                0xff64b5f6,
                0xff4fc3f7,
                0xff4dd0e1,
                0xff4db6ac,
                0xff81c784,
                0xffaed581,
                0xffff8a65,
                0xffd4e157,
                0xffffd54f,
                0xffffb74d,
                0xffa1887f,
                0xff90a4ae
        )));
    }

    private final List<Integer> mColors;
    private final SecureRandom mRandom;

    public static ColorGenerator create(List<Integer> colorList) {
        return new ColorGenerator(colorList);
    }

    private ColorGenerator(List<Integer> colorList) {
        mColors = colorList;
        mRandom = new SecureRandom();
    }

    public int getRandomColor() {
        return mColors.get(mRandom.nextInt(mColors.size()));
    }

    public int getColor(Object key) {
    	int hashCode = key.hashCode();
    	hashCode = hashCode < 0 ? -hashCode : hashCode;
        return mColors.get(hashCode % mColors.size());
    }

	public static ColorGenerator getDEFAULT() {
		return DEFAULT;
	}

	public static void setDEFAULT(ColorGenerator dEFAULT) {
		DEFAULT = dEFAULT;
	}

	public static ColorGenerator getMATERIAL() {
		return MATERIAL;
	}

	public static void setMATERIAL(ColorGenerator mATERIAL) {
		MATERIAL = mATERIAL;
	}
}
