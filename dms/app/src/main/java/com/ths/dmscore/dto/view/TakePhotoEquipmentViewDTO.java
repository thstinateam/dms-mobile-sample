package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.dto.db.TakePhotoEquipmentDTO;

/**
 * DTO cho man hinh chup hinh thiet bi
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class TakePhotoEquipmentViewDTO {

	// ds hinh anh
	public TakePhotoEquipmentImageViewDTO image = new TakePhotoEquipmentImageViewDTO();
	public ArrayList<TakePhotoEquipmentDTO> lstKeyShop = new ArrayList<TakePhotoEquipmentDTO>();
	public double shopDistance = 0;
}
