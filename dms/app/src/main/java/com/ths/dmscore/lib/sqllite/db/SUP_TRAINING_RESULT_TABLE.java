/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.dto.view.trainingplan.TrainingRateDetailResultlDTO;
import com.ths.dmscore.util.DateUtils;

/**
 * 
 * SUP_TRANING_RESULT_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:40:06 17-11-2014
 */
public class SUP_TRAINING_RESULT_TABLE extends ABSTRACT_TABLE {
	public static final String SUP_TRAINING_RESULT_ID = "SUP_TRAINING_RESULT_ID";
	public static final String TRAINING_RATE_DETAIL_ID = "TRAINING_RATE_DETAIL_ID";
	public static final String SUP_TRAINING_CUSTOMER_ID = "SUP_TRAINING_CUSTOMER_ID";
	public static final String RESULT = "RESULT";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_STAFF_ID = "CREATE_STAFF_ID";

	public static final String TABLE_NAME = "SUP_TRAINING_RESULT";

	public SUP_TRAINING_RESULT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SUP_TRAINING_RESULT_ID,
				TRAINING_RATE_DETAIL_ID, SUP_TRAINING_CUSTOMER_ID, RESULT,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 5:35:53 PM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 * @param rateResult
	 * @throws Exception 
	 */
	public long saveTrainingResult(TrainingRateDetailResultlDTO rateResult) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		try {
			if(rateResult.getSubTrainingResultID() == 0){	
				returnCode = insert(rateResult);
			} else if(rateResult.getSubTrainingResultID() > 0){
				rateResult.setInsert(false);
				returnCode = update(rateResult);
			}
		} catch (Exception e) {
			throw e;
		}
		return returnCode;
	}
	
	/**
	 * update kq
	 * 
	 * @author: dungdq3
	 * @since: 10:00:11 AM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private long update(TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues value = initContentValuesUpdate(rateResult);
		String[] params = { String.valueOf(rateResult.getSubTrainingResultID()) };
		return update(value, SUP_TRAINING_RESULT_ID + " = ?", params);
	}

	/**
	 * update danh gia
	 * 
	 * @author: dungdq3
	 * @since: 10:02:32 AM Nov 18, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param rateResult
	 * @return
	 */
	private ContentValues initContentValuesUpdate(
			TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(RESULT, rateResult.getResult());
		editedValues.put(UPDATE_DATE, DateUtils.now());
		return editedValues;
	}

	/**
	 * Luu thong tin danh gia tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 8:25:24 AM Nov 18, 2014
	 * @return: long
	 * @throws:  
	 * @param rateResultlDTO
	 * @return
	 */
	private long insert(TrainingRateDetailResultlDTO rateResultlDTO){
		ContentValues cv = initContentValues(rateResultlDTO);
		return insert(null, cv);
	}

	/**
	 * tao du lieu de luu xuong db
	 * 
	 * @author: dungdq3
	 * @since: 5:44:33 PM Nov 17, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param rateResultlDTO
	 * @return
	 */
	private ContentValues initContentValues(
			TrainingRateDetailResultlDTO rateResultlDTO) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(SUP_TRAINING_CUSTOMER_ID, rateResultlDTO.getSubTrainingCustomerID());
		editedValues.put(SUP_TRAINING_RESULT_ID, rateResultlDTO.getSubTrainingResultID());
		editedValues.put(TRAINING_RATE_DETAIL_ID, rateResultlDTO.getTrainingRateDetailID());
		editedValues.put(CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		editedValues.put(RESULT, rateResultlDTO.getResult());
		editedValues.put(CREATE_DATE, rateResultlDTO.getCreateDate());
		return editedValues;
	}

	/**
	 * delete kq
	 * 
	 * @author: dungdq3
	 * @since: 5:00:18 PM Nov 18, 2014
	 * @return: void
	 * @throws:  
	 * @param rateResult
	 */
	public long delete(TrainingRateDetailResultlDTO rateResult) {
		// TODO Auto-generated method stub
		String[] params = { String.valueOf(rateResult.getTrainingRateDetailID()) };
		return delete(TRAINING_RATE_DETAIL_ID + " = ?", params);
	}
}
