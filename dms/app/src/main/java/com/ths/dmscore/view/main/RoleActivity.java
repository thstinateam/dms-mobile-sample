/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.dto.NotifyDataDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.view.control.MenuListAdapter;
import com.ths.dmscore.view.sale.order.OrderView;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * RoleActivity.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 9:13:17 AM Mar 16, 2015
 */
public class RoleActivity extends GlobalBaseActivity implements
		OnClickListener, OnGroupClickListener, OnChildClickListener {

	// action dong y thoat khoi app
	public static final int CONFIRM_EXIT_APP_OK = 1;
	// action ko dong y thoat khoi app
	public static final int CONFIRM_EXIT_APP_CANCEL = 2;
	// action chon team(nhom giam sat) quan ly
	public final int ACTION_SELECTED_SHOP_MANAGE = 3;
	// action dong y thoat khoi man hinh dat hang khi dang dat hang
	public static final int CONFIRM_EXIT_ORDER_VIEW_OK = 14;
	// action ko dong y thoat khoi man hinh dat hang khi dang dat hang
	public static final int CONFIRM_EXIT_ORDER_VIEW_CANCEL = 15;
	protected final int MENU_INDEX_SELECTED = 200;

	// layout dung de ve left menu
	private DrawerLayout mDrawerLayout;
	// layout chua thong tin user
	private RelativeLayout rlInfo;

	// danh sach cac item trong left menu
	protected ArrayList<MenuItemDTO> lstMenuItems;
	// adapter cho left menu
	protected MenuListAdapter lstMenuAdapter;

	// vi tri group cu truoc khi click chon group
	protected int preGroupPosition = -1;
	// vi tri group cu truoc khi click chon child
	protected int groupdPosSelected = -1;
	// vi tri child cu truoc khi click chon child
	protected int childPosSelected = -1;

	protected boolean isLeadFragment = true;
	protected LinearLayout lnMenu;

	protected ExpandableListView lvMenu;

	// Hash luu cac tab trong 1 menu
	// Menu1 : tab11, tab12, tab13
	// Menu2 : tab21, tab22, tab23
	// ..
	protected SparseArray<List<MenuTab>> hashActionbar;
	protected ImageView iconLeft;
	protected TextView tvTitleMenu;
	protected LinearLayout llMenu;// layout header icon
	// thoi gian tu server tra ve
	protected String serverDate;

	//notify info
	protected TextView tvLastLocation;
	protected TextView tvSynStatus;
	protected LinearLayout llSynStatus;
	protected LinearLayout llLastLocation;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	protected void onCreate(Bundle savedInstanceState, PriHashMap.PriMenu menu, PriHashMap.PriForm form) {
		super.onCreate(savedInstanceState);
		PriUtils.getInstance().genPriHashMapForMenu(menu, form);
		setContentView(R.layout.layout_main);
		iconLeft = (ImageView) llShowHideMenu.findViewById(R.id.ivLeftIcon);
		llShowHideMenu.setOnClickListener(this);
		llMenuUpdate.setVisibility(View.VISIBLE);
		llMenuGPS.setVisibility(View.VISIBLE);
		tvTitleMenu = (TextView) findViewById(R.id.tvTitleMenu);
		llMenu = (LinearLayout) findViewById(R.id.llMenu);

		initView();
	}

	/**
	 * khoi tao view cho left menu
	 *
	 * @author: toantt3
	 * @since: 13:02:17 09-08-2014
	 * @return: void
	 * @throws:
	 */
	public void initView() {
		lvMenu = (ExpandableListView) findViewById(R.id.lvMenu);
		lnMenu = (LinearLayout) findViewById(R.id.lnMenu);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.mDrawerLayout);
//		mDrawerLayout.setDrawerListener(this);
		rlInfo = (RelativeLayout) findViewById(R.id.rlInfo);
		rlInfo.setOnClickListener(this);
		iconLeft.setVisibility(View.GONE);
		tvTitleMenu.setVisibility(View.GONE);
		iconLeft.setVisibility(View.GONE);
		tvTitleMenu.setVisibility(View.GONE);

		RelativeLayout.LayoutParams llMenuParam = new RelativeLayout.LayoutParams(
				GlobalUtil.dip2Pixel(50), LayoutParams.WRAP_CONTENT);
		llMenu.setLayoutParams(llMenuParam);
		initMenu();
		setTitleName(GlobalInfo.getInstance().getProfile().getUserData().getUserCode());
		initStaffInfo();

		llSynStatus = (LinearLayout) findViewById(R.id.llSynStatus);
		llLastLocation = (LinearLayout) findViewById(R.id.llLastLocation);
		tvLastLocation = (TextView) findViewById(R.id.tvLastLocation);
		tvSynStatus = (TextView) findViewById(R.id.tvSynStatus);
	}

	/**
	 * lay thong tin staff dang nhap hien tai
	 *
	 * @author: toantt3
	 * @since: 13:09:18 09-08-2014
	 * @return: void
	 * @throws:
	 */
	private void initStaffInfo() {
		TextView tvStaffName = (TextView) rlInfo.findViewById(R.id.tvStaffName);
		TextView tvStaffCode = (TextView) rlInfo.findViewById(R.id.tvStaffCode);
		tvStaffName
				.setText(GlobalInfo.getInstance().getProfile().getUserData().getDisplayName());
		tvStaffCode
				.setText(GlobalInfo.getInstance().getProfile().getUserData().getUserCode());
	}

	@Override
	public void onClick(View v) {
		if (v == llShowHideMenu)
			drawListMenu();
		else if (v == rlInfo) {
			if (!isCheckConfirmOrderView(-1, -1)) {
				gotoChangePassView();
				drawListMenu();
			}
		} else{
			super.onClick(v);
		}

	}

	/**
	 * an/hien left menu
	 *
	 * @author: toantt3
	 * @since: 13:20:51 09-08-2014
	 * @return: void
	 * @throws:
	 */
	public void drawListMenu() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				if (mDrawerLayout.isDrawerOpen(lnMenu)) {
					mDrawerLayout.closeDrawers();
				} else {
					mDrawerLayout.openDrawer(lnMenu);
				}
			}
		}, 100l);
	}

	/**
	 * an left menu
	 *
	 * @author: toantt3
	 * @since: 13:21:59 09-08-2014
	 * @return: void
	 * @throws:
	 */
	public void closeLeftMenu() {
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			public void run() {
				if (mDrawerLayout.isDrawerOpen(lnMenu)) {
					mDrawerLayout.closeDrawers();
				}
			}
		}, 100l);
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() == 1) {
			GlobalUtil.showDialogConfirm(this,
					StringUtil.getString(R.string.TEXT_CONFIRM_EXIT),
					StringUtil.getString(R.string.TEXT_AGREE),
					CONFIRM_EXIT_APP_OK,
					StringUtil.getString(R.string.TEXT_DENY),
					CONFIRM_EXIT_APP_CANCEL, null);
		} else {
			destroyMapGoogle();
			GlobalInfo.getInstance().popCurrentTag();
			super.onBackPressed();
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case CONFIRM_EXIT_APP_OK: {
			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			TransactionProcessManager.getInstance().cancelTimer();
			finish();
		}
			break;
		case CONFIRM_EXIT_ORDER_VIEW_OK: {
			Bundle bun = (Bundle) data;
			int group = bun.getInt("group", -1);
			int child = bun.getInt("child", -1);
			if (group < 0) {
				gotoChangePassView();
				drawListMenu();
			} else {
				if (child >= 0) {
					processClickOnChildMenu(group, child);
					drawListMenu();
				} else {
					processClickOnGroupMenu(group);
				}
			}
		}
			break;
		case CONFIRM_EXIT_ORDER_VIEW_CANCEL:
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 * di toi man hinh thay doi password
	 *
	 * @author: toantt3
	 * @since: 13:23:21 09-08-2014
	 * @return: void
	 * @throws:
	 */
	private void gotoChangePassView() {
		if (preGroupPosition >= 0) {
			lvMenu.collapseGroup(preGroupPosition);
			lstMenuItems.get(preGroupPosition).setRefresh(true);
		}
		preGroupPosition = -1;
		processClickOnChildMenu(-1, -1);
		Bundle bundle = new Bundle();
		bundle.putBoolean("isLeadFragment", true);
		bundle.putInt("actionLeadFragment", ActionEventConstant.CHANGE_PASS);
		bundle.putString("leadFragmentTag", GlobalUtil.getTag(ChangePasswordView.class));
		handleSwitchFragment(bundle, ActionEventConstant.CHANGE_PASS,
				UserController.getInstance());
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		switch (action) {
		case ActionEventConstant.ACTION_NOTIFY_DATA:{
			NotifyDataDTO data = (NotifyDataDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
			MyLog.d("receiver notify data", "send notify: " + data);
			setNotifyData(data);
			break;
		}
		case ActionEventConstant.ACTION_SEND_POSITION_TO_SERVER_SUCCESS:{
			String createDate = bundle.getString(IntentConstants.INTENT_DATA);
			String time = DateUtils.convertFormatDate(createDate, DateUtils.DATE_FORMAT_NOW, "HH:mm");
			setLastLocationInfo(time);
			MyLog.d("receiveBroadcast", "ACTION_SEND_POSITION_TO_SERVER_SUCCESS");
			break;
		}
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}

	protected void initMenu() {
		initMenuGroups();
		lstMenuAdapter = new MenuListAdapter(this, lstMenuItems);
		lvMenu.setAdapter(lstMenuAdapter);
		lvMenu.setOnGroupClickListener(this);
		lvMenu.setOnChildClickListener(this);		
		lvMenu.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
				if (!isMenuSwitchTeam(groupPosition)) {
					if (groupPosition != preGroupPosition) {
						lvMenu.collapseGroup(preGroupPosition);
					}
					preGroupPosition = groupPosition;
				}
			}
		});

		lvMenu.setOnGroupCollapseListener(new OnGroupCollapseListener() {
			@Override
			public void onGroupCollapse(int groupPosition) {

			}
		});

//		processClickOnMenuById(200);
	}

	@Override
	public boolean onGroupClick(ExpandableListView parent, View v,
			int groupPosition, long id) {
		if (lstMenuItems.get(groupPosition).getItems() != null
				|| !isCheckConfirmOrderView(groupPosition, -1)) {
			lstMenuItems.get(groupPosition).setRefresh(true);
			if (!isLeadFragment || preGroupPosition != groupPosition) {
				processClickOnGroupMenu(groupPosition);
			}
		}
		return false;
	}

	/**
	 * click tren group item cua left menu
	 *
	 * @author: toantt3
	 * @since: 13:27:34 27-06-2014
	 * @return: void
	 * @throws:
	 * @param groupPosition
	 */
	private void processClickOnGroupMenu(int groupPosition) {
		if (!isMenuSwitchTeam(groupPosition)) {
			lstMenuItems.get(groupPosition).setSelected(true);
			if (preGroupPosition >= 0) {
				lstMenuItems.get(preGroupPosition).setSelected(false);
			}
			// khi click vao menu cha thi hien cac menu con
			if (lstMenuItems.get(groupPosition).getItems() != null) {
				lstMenuAdapter.notifyDataSetChanged();
			} else {
				// reset lai trang thai select cho nhung item khac, thuoc group
				// khac
				setSelectStateMenuChildItem(-1, -1);
				// khi menu khong co danh sach con thi vao truc tiep man hinh
				showDetails(groupPosition);
				lstMenuAdapter.notifyDataSetChanged();
				drawListMenu();
			}
		} else {
			showDetails(groupPosition);
			lstMenuAdapter.notifyDataSetChanged();
			drawListMenu();
		}
	}

	/**
	 * kiem tra co phai dang click menu switch team hay ko
	 *
	 * @author: toantt3
	 * @since: 19:48:56 08-08-2014
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	private boolean isMenuSwitchTeam(int groupPosition) {
//		if (GlobalInfo.getInstance().getProfile().getUserData().inheritSpecificType != UserDTO.TYPE_SUPERVISOR
////				|| GlobalInfo.getInstance().getProfile().getUserData().listShop
////						.size() == 1
//				|| groupPosition != lstMenuItems.size() - 1) {
//			return false;
//		}
//		return true;
		return false;
	}

	/**
	 * Set trang thai selected cho 1 menu con cua 1 group, hoac reset neu
	 * groupdPos < 0, childPos < 0
	 *
	 * @author: duongdt3
	 * @since: 14:43:56 15 Jul 2014
	 * @return: void
	 * @throws:
	 * @param groupdPos
	 * @param childPos
	 */
	void setSelectStateMenuChildItem(int groupdPos, int childPos) {
		if (groupdPosSelected >= 0) {
			if (lstMenuItems.size() > groupdPosSelected
					&& lstMenuItems.get(groupdPosSelected).getItems() != null
					&& lstMenuItems.get(groupdPosSelected).getItems().size() > childPosSelected) {
				// reset lai group dang duoc select
				ArrayList<MenuItemDTO> items = lstMenuItems.get(
						groupdPosSelected).getItems();
				for (MenuItemDTO menuItemDTO : items) {
					menuItemDTO.setSelected(false);
				}
			}
		}

		// set lai item moi trang thai select
		if (groupdPos >= 0) {
			if (lstMenuItems.size() > groupdPos
					&& lstMenuItems.get(groupdPos).getItems() != null
					&& lstMenuItems.get(groupdPos).getItems().size() > childPos) {
				// select lai group moi
				lstMenuItems.get(groupdPos).getItems().get(childPos)
						.setSelected(true);
			}
		}

		// cap nhat group duoc select moi
		groupdPosSelected = groupdPos;
		childPosSelected = childPos;
	}

	/**
	 * khi click tren item child cua left menu
	 *
	 * @author: toantt3
	 * @since: 16:40:15 09-08-2014
	 * @return: void
	 * @throws:
	 * @param groupdPos
	 * @param childPos
	 */
	private void processClickOnChildMenu(int groupdPos, int childPos) {
		// set lai trang thai selected cho menu con
		setSelectStateMenuChildItem(groupdPos, childPos);
		lstMenuAdapter.notifyDataSetChanged();
		if (groupdPos >= 0 && childPos >= 0) {
			showDetailsChild(groupdPos, childPos);
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		if (!isCheckConfirmOrderView(groupPosition, childPosition)) {
			if (!isLeadFragment || groupdPosSelected != groupPosition
					|| childPosSelected != childPosition) {
				processClickOnChildMenu(groupPosition, childPosition);
				drawListMenu();
			}
		}
		return false;
	}

	/**
	 * xu ly cho truong hop load focus
	 *
	 * @author: toantt3
	 * @since: 16:41:51 09-08-2014
	 * @return: void
	 * @throws:
	 * @param index
	 */
	public void processClickOnDefaultMenu(int index) {
		lstMenuItems.get(index).setSelected(true);
		lstMenuItems.get(index).setRefresh(true);
		lvMenu.expandGroup(index);
		if (lstMenuItems.get(index).getItems() != null)
			processClickOnChildMenu(index, 0);
		else
			showDetails(index);
	}

	/**
	 * hien thi mac dinh vao item theo id
	 *
	 * @author: toantt3
	 * @since: 16:41:03 09-08-2014
	 * @return: void
	 * @throws:
	 * @param id
	 */
	public void processClickOnMenuById(int id) {
		int index = 0;
		for (int i = 0; i < lstMenuItems.size(); i++) {
			if (lstMenuItems.get(i).id == id) {
				index = i;
			}
		}
		processClickOnDefaultMenu(index);
	}

	/**
	 * khoi tao cac item trong left menu
	 *
	 * @author: toantt3
	 * @since: 16:47:42 09-08-2014
	 * @return: ArrayList<MenuItemDTO>
	 * @throws:
	 * @return
	 */
	protected void initMenuGroups() {
	}

	/**
	 * di den man hinh chi tiet da chon child trong left menu
	 *
	 * @author: toantt3
	 * @since: 16:51:34 09-08-2014
	 * @return: void
	 * @throws:
	 * @param groupPos
	 * @param childPos
	 */
	protected void showDetailsChild(int groupPos, int childPos) {
		List<MenuTab> listSubMenu = hashActionbar.get(groupPos);
		MenuTab tab = listSubMenu.get(childPos);
		processSwitchMenu(tab.action);
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 * kiem tra co phai dang dat hang truoc khi back ve hay chon left menu
	 *
	 * @author: toantt3
	 * @since: 16:49:34 09-08-2014
	 * @return: boolean
	 * @throws:
	 * @param index
	 * @return
	 */
	private boolean isCheckConfirmOrderView(int groupPosition, int childPosition) {
		boolean isConfirm = false;
		if ((groupPosition <= 0 || ((childPosition >= 0 && lstMenuItems.get(
				groupPosition).getItems() != null) || (childPosition < 0 && lstMenuItems
				.get(groupPosition).getItems() == null)))
				&& GlobalUtil.getTag(OrderView.class).equals(GlobalInfo.getInstance()
						.getCurrentTag())) {
			// kiem tra co thay doi du lieu ko
			OrderView orderFragment = (OrderView) findFragmentByTag(GlobalUtil.getTag(OrderView.class));
			if (orderFragment != null) {
//				String changeData = orderFragment.checkChangeData();
				String changeData = Constants.STR_BLANK;

				if (!StringUtil.isNullOrEmpty(changeData)) {
					// hien thi thong bao
					Bundle bun = new Bundle();
					bun.putInt("group", groupPosition);
					bun.putInt("child", childPosition);
					GlobalUtil
							.showDialogConfirm(this, changeData,
									StringUtil.getString(R.string.TEXT_AGREE),
									CONFIRM_EXIT_ORDER_VIEW_OK, StringUtil.getString(
													R.string.TEXT_DENY),
									CONFIRM_EXIT_ORDER_VIEW_CANCEL, bun);
					isConfirm = true;
				}
			}
		}
		return isConfirm;
	}

	@Override
	protected void onClickSearchMenu() {
		super.onClickSearchMenu();
		closeLeftMenu();
	}

	/**
	 * di toi man hinh chi tiet
	 *
	 * @author: dungdq3
	 * @since: 10:13:47 AM Mar 16, 2015
	 * @return: void
	 * @throws:
	 * @param index
	 */
	protected void showDetails(int index) {
		List<MenuTab> tabs = hashActionbar.get(index);
		if(tabs != null){
			for (MenuTab menuTab : tabs) {
				if(menuTab.form != null){
					int status = PriUtils.getInstance().checkMenu(menuTab.form.getFormName());
					if(status == 2){ // enable
						// Go to
						processSwitchMenu(menuTab.action);
						break;
					}
				}
			}
		}
	}

	protected void getActionWhenClickMenu(int action){

	}

	/**
	 * set menu duoc chon
	 *
	 * @author: dungdq3
	 * @since: 1:58:26 PM Mar 16, 2015
	 * @return: void
	 * @throws:
	 * @param index
	 */
	protected void setSelectMenu(int index) {
		boolean isFounded = false;
		for (int i = 0, count = lstMenuItems.size(); i < count; i++) {
			MenuItemDTO menu = lstMenuItems.get(i);
			if(menu.menuIndex == index){
				lstMenuItems.get(i).setSelected(true);
				lvMenu.setSelection(i);
				isFounded = true;
				showDetails(i);
				break;
			}
		}
		// Menu da bi remove
		if (!isFounded && lstMenuItems.size() > 0) {
			lstMenuItems.get(0).setSelected(true);
			lvMenu.setSelection(0);
			showDetails(0);
		}
	}

	/**
	 * kiem tra h client & server
	 *
	 * @author: dungdq3
	 * @since: 3:57:49 PM Mar 16, 2015
	 * @return: void
	 * @throws:
	 */
	protected void validateTimeClientServer() {
		if (!StringUtil.isNullOrEmpty(serverDate)) {
			int validate = DateUtils.checkTimeClientAndServer(serverDate);
			if (validate == DateUtils.TIME_INVALID) {
				GlobalUtil.showDialogSettingTime();
			}
		}
	}

	public static interface PreProcess{
		boolean process(int action);
	}

	List<PreProcess> switchMenuPreProcess = new ArrayList<RoleActivity.PreProcess>();
	public void addPreProcessSwitchMenu(PreProcess process){
		switchMenuPreProcess.add(process);
	}

	public void removePreProcessSwitchMenu(PreProcess process){
		switchMenuPreProcess.remove(process);
	}

	private void processSwitchMenu(int action){
		boolean isPreProcessPass = preProcessSwitchMenu(action);
		if (isPreProcessPass) {
			getActionWhenClickMenu(action);
			lastSaveActionSwitchMenu = -1;
		} else{
			lastSaveActionSwitchMenu = action;
		}
	}

	int lastSaveActionSwitchMenu = -1;
	private boolean preProcessSwitchMenu(int action){
		boolean result = true;
		if (switchMenuPreProcess != null && !switchMenuPreProcess.isEmpty()) {
			for (PreProcess process : switchMenuPreProcess) {
				result = (process != null ? process.process(action) : true);
				if (!result) {
					break;
				}
			}
		}
		return result ;
	}

	public void switchMenuLastSaveAction(){
		if (lastSaveActionSwitchMenu >= 0) {
			getActionWhenClickMenu(lastSaveActionSwitchMenu);
			lastSaveActionSwitchMenu = -1;
		}
	}

	protected void initHashActionbar() {

	}

	/**
	 * render notify data
	 * @param data
	 */
	public void setNotifyData(NotifyDataDTO data) {
		setLastLocationInfo(data.lastTimeHaveLoc);
		setNumDataNeedSync(data.numDataNeedSync);
	}

	static final String DEFAULT_LAST_LOC_TIME = "??:??";
	/**
	 * set last location info
	 * @param val
	 */
	public void setLastLocationInfo(CharSequence val) {
		MyLog.d("setLastLocationInfo", "val" + val);
		if (tvLastLocation != null) {
			String currentTime = tvLastLocation.getText().toString();
			if (!StringUtil.isNullOrEmpty(val)) {
				if (StringUtil.isNullOrEmpty(currentTime)) {
					tvLastLocation.setText(val);
				} else{
					//chi thay doi khi co thoi gian lon hon hoac khi chua co gia tri
					if (DEFAULT_LAST_LOC_TIME.equals(currentTime) || currentTime.compareTo(val.toString()) < 0) {
						tvLastLocation.setText(val);
					}
				}
			} else{
				tvLastLocation.setText(DEFAULT_LAST_LOC_TIME);
			}
		}
	}

	/**
	 * set num data need sync
	 * @param val
	 */
	public void setNumDataNeedSync(int numDataNeedSync) {
		if (tvSynStatus != null) {
			String info = String.valueOf(numDataNeedSync);
			if (numDataNeedSync > 99) {
				info = "99+";
			}
			tvSynStatus.setText(info);
		}
	}
}
