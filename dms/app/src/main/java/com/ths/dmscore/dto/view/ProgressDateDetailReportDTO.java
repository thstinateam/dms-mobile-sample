package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class ProgressDateDetailReportDTO {
	public int totalList;
	public String totalofTotal;

	public ArrayList<ProgressDateDetailReportItem> arrList;
	public String beginDate;
	public String endDate;
	public int numCycle;

	public ProgressDateDetailReportDTO(){
		arrList = new ArrayList<ProgressDateDetailReportDTO.ProgressDateDetailReportItem>();
	}

	public void addItem(Cursor c, int sysCurrencyDivide){
		ProgressDateDetailReportItem item = new ProgressDateDetailReportItem();
		item.customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		item.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		item.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		item.customerAddress = CursorUtil.getString(c, "CUSTOMER_ADDRESS");

		item.amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PLAN");
		item.amountDone = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT");
		item.amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_APPROVED");

		item.quantityPlan = CursorUtil.getLong(c, "QUANTITY_PLAN");
		item.quantityDone = CursorUtil.getLong(c, "QUANTITY");
		item.quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
		item.inRoute = CursorUtil.getInt(c, "IS_NT");
		arrList.add(item);
	}

	public class ProgressDateDetailReportItem{


		public String stt;
		public long customerId;
		public String customerCode;
		public String customerName;
		public String customerAddress;

		public Double amountPlan;
		public Double amountDone;
		public Double amountApproved;
		public Double amountPending;

		public long quantityPlan;
		public long quantityDone;
		public long quantityApproved;
		public long quantityPending;

		public int customerNew;
		public int inRoute;
		public int wrongRoute;


		public ProgressDateDetailReportItem(){
			stt = "";
			customerId =0 ;
			customerCode = "";
			customerName = "";
			customerAddress = "";

			amountPlan = 0.0;
			amountDone = 0.0;
			amountApproved  = 0.0;;
			amountPending = 0.0;

			quantityPlan = 0;
			quantityDone = 0;
			quantityApproved = 0;
			quantityPending = 0;

			customerNew = 0;
			inRoute = 0;
			wrongRoute = 0;
		}
	}
}
