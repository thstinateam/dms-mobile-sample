package com.ths.dmscore.view.sale.statistic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.SpannableString;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.view.AttachDTO;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.FileUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.SuperscriptSpanAdjuster;
import com.ths.dmscore.view.control.filechooser.FileListDialogView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

public class NoteListRow extends DMSTableRow implements OnClickListener {
	public TextView tvNumer;
	// public TextView tvFeedBackDate;
	public TextView tvContent;
	public TextView tvRemindDate;
	public TextView tvDoneDate;
	public CheckBox cbDone;
	public LinearLayout rlNote;
	// public ImageView ivDetele;
	public TextView tvMKH;
	public TextView tvType;
	int action;
	FeedBackDTO dto;
//	private TableRow row;
	private ImageView ivAttach;
	private List<FileInfo> lstFileInfo = new ArrayList<FileInfo>();
	private LinearLayout llAttach;

	public NoteListRow(Context context, VinamilkTableListener listener,
			int action, FeedBackDTO dto) {
		super(context, R.layout.layout_note_list_row);
		this.setListener(listener);
		this.action = action;
		this.dto = dto;
//		row = (TableRow) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.row, PriControl.NVBH_CANTHUCHIEN_CHITIETVANDE);
//		PriUtils.getInstance().setOnClickListener(row, this);

		tvNumer = (TextView) findViewById(R.id.tvSTT);
		tvNumer.setOnClickListener(this);
		// tvFeedBackDate = (TextView) findViewById(R.id.tvFeedBackDate);
		tvContent = (TextView) findViewById(R.id.tvContent);
		tvContent.setOnClickListener(this);
		tvRemindDate = (TextView) findViewById(R.id.tvRemindDate);
		tvRemindDate.setOnClickListener(this);
		tvDoneDate = (TextView) findViewById(R.id.tvOpDate);
		tvDoneDate.setOnClickListener(this);
		// ivDetele = (ImageView) findViewById(R.id.ivDelete);
//		cbDone = (CheckBox) findViewById(R.id.cbNote);
		cbDone = (CheckBox) PriUtils.getInstance().findViewById(this, R.id.cbNote, PriControl.NVBH_CANTHUCHIEN_DATHUCHIENCHITIETVANDE);
		cbDone.setBackgroundColor(Color.TRANSPARENT);
		rlNote = (LinearLayout) findViewById(R.id.rlNote);
		tvMKH = (TextView) findViewById(R.id.tvMKH);
		tvMKH.setOnClickListener(this);
		tvType = (TextView) findViewById(R.id.tvType);
		tvType.setOnClickListener(this);
		ivAttach = (ImageView) findViewById(R.id.ivAttach);
		llAttach = (LinearLayout) findViewById(R.id.llAttach);
	}

	public void render(int pos, FeedBackDTO item) {
		tvNumer.setText(Constants.STR_BLANK + pos);
		tvType.setText(item.getApParamName());
		tvContent.setText(item.getContent());
		if (item.getCustomerCode() != null) {
//			if (item.createUserId == Long.valueOf(GlobalInfo.getInstance()
//					.getProfile().getUserData().staffOwnerId)) {
			if (item.getCreateUserId() != Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId())) {
				tvMKH.setTextColor(ImageUtil.getColor(R.color.RED));
				SpannableString contentAmount = new SpannableString(
						tvMKH.getText() + "*");
				contentAmount.setSpan(new SuperscriptSpanAdjuster(1.5 / 5.0),
						contentAmount.length() - 1, contentAmount.length(),
						SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
				tvMKH.append(contentAmount);
			}
			tvMKH.append(item.getCustomerCode());
		}else{
			tvMKH.append(Constants.STR_BLANK);
		}
		if (item.getRemindDate() != null) {
			tvRemindDate.setText(item.getRemindDate());
		} else {
			tvRemindDate.setText(Constants.STR_BLANK);
		}

		if(item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE ||
				item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE){
			tvDoneDate.setText(item.getFeedbackStaffDTO().doneDate);
			cbDone.setChecked(true);
//			cbDone.setEnabled(false);
			PriUtils.getInstance().setStatus(cbDone, PriUtils.DISABLE);
			tvContent.setPaintFlags(tvContent.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		}else{
			tvDoneDate.setText("");
			cbDone.setChecked(false);
		}

		// type =2,3
		if ((item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE ||
				item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE)
				&& DateUtils.compareWithNow(item.getFeedbackStaffDTO().doneDate, "dd/MM/yyyy") != 0) {
			setColorBackgroundForRow(ImageUtil
					.getColor(R.color.COLOR_GRAY_DONE));
		} else if ((item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE ||
				item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE)
				&& DateUtils.compareWithNow(item.getFeedbackStaffDTO().doneDate, "dd/MM/yyyy") == 0) {
			setColorBackgroundForRow(ImageUtil
					.getColor(R.color.COLOR_BLUE_DONE_TODAY));
		} else if (item.getFeedbackStaffDTO().result == FeedBackDTO.FEEDBACK_STATUS_CREATE
				&& DateUtils.compareWithNow(item.getRemindDate(), "dd/MM/yyyy") == -1) {
			setColorForTextRow(ImageUtil.getColor(R.color.RED));
		}
		
		if (item.getLstAttach() != null && !item.getLstAttach().isEmpty()) {
			ivAttach.setVisibility(View.VISIBLE);
			ivAttach.setOnClickListener(this);
			for (AttachDTO attachInfo : item.getLstAttach()) {
				File file = new File(attachInfo.url);
				FileInfo fileInfo = null;
				//in storage
				if (file.exists()) {
					fileInfo = new FileInfo(file, FileInfo.TYPE_FILE);
				} else{
					String path = ServerPath.IMAGE_PATH + attachInfo.url;
					fileInfo = new FileInfo(path, file.getName(), FileUtil.getFileExtension(file), FileInfo.TYPE_FILE); 
				}
				
				if(!StringUtil.isNullOrEmpty(attachInfo.title)){
					fileInfo.fileName = attachInfo.title;
				}
				
				lstFileInfo.add(fileInfo);
			}
		} else{
			ivAttach.setVisibility(View.GONE);
		}
	}

	/**
	 *
	 * set color for row
	 *
	 * @param color
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Nov 6, 2012
	 */
	public void setColorForTextRow(int color) {
		tvNumer.setTextColor(color);
		tvContent.setTextColor(color);
		tvRemindDate.setTextColor(color);
		tvDoneDate.setTextColor(color);
		tvRemindDate.setTextColor(color);
		tvMKH.setTextColor(color);
		tvType.setTextColor(color);
	}

	/**
	 *
	 * set background color for row
	 *
	 * @param color
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Nov 6, 2012
	 */
	public void setColorBackgroundForRow(int color) {
		tvNumer.setBackgroundColor(color);
		tvContent.setBackgroundColor(color);
		tvRemindDate.setBackgroundColor(color);
		tvDoneDate.setBackgroundColor(color);
		tvRemindDate.setBackgroundColor(color);
//		cbDone.setBackgroundColor(color);
		tvMKH.setBackgroundColor(color);
		tvType.setBackgroundColor(color);
//		cbDone.setBackgroundColor(color);
		rlNote.setBackgroundColor(color);
		llAttach.setBackgroundColor(color);
	}

	@Override
	public void onClick(View v) {
		if(v == tvNumer || v == tvContent || v== tvMKH || v== tvType || v== tvRemindDate || v== tvDoneDate){
			listener.handleVinamilkTableRowEvent(action, v, dto);
		} else if (v == ivAttach) {
			FileListDialogView.showFileListDialog((GlobalBaseActivity)GlobalInfo.getInstance().getActivityContext(), lstFileInfo);
		}
	}

}
