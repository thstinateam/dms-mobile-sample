package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

import com.ths.dmscore.dto.NotifyOrderDTO;


public class ListOrderMngDTO {
	public int total=0;
	public int totalIsSend=0;
	public List<SaleOrderViewDTO> listDTO;
	// don hang loi can thong bao
	public NotifyOrderDTO notifyDTO;

	public ListOrderMngDTO() {
		listDTO = new ArrayList<SaleOrderViewDTO>();
		notifyDTO = new NotifyOrderDTO();
	}

	public List<SaleOrderViewDTO> getListDTO() {
		return listDTO;
	}

}
