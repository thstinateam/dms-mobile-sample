/*
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.google.android.gcm.GCMRegistrar;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.global.ServerPath;

/**
 * Chua cac thong tin cua server de dang ky GCM
 * @author: DungDQ3
 * @version: 1.1
 * @since: 1.0
 */
public final class ServerUtilities {

    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final SecureRandom random = new SecureRandom();

    /**
    * Dang ky thong tin len server
    * @author dungdq
    * @param: context, regId
    * @return: boolean
    */
    public static boolean register(final Context context, final String regId) {
        //String serverUrl = ServerPath.SERVER_PATH_OAUTH + "register";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        long backOff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        for (int i = 1; i <= MAX_ATTEMPTS; i++) {
            try {
                post(ServerPath.SERVER_HTTP, params);
                GCMRegistrar.setRegisteredOnServer(context, true);
                return true;
            } catch (IOException e) {
                if (i == MAX_ATTEMPTS) {
                    break;
                }
                try {
                    Thread.sleep(backOff);
                } catch (InterruptedException e1) {
                    // Activity finished before we complete - exit.
                    Thread.currentThread().interrupt();
                    return false;
                }
                // increase backoff exponentially
                backOff *= 2;
            }
        }
        return false;
    }

    /**
    * huy dang ky len server
    * @author dungdq
    * @param: context, regId
    */
    public static void unregister(final Context context, final String regId) {
        String serverUrl = ServerPath.SERVER_HTTP;
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
        } catch (IOException e) {
        	e.getStackTrace();
        }
    }

    /**
    * Gui thong tin len server
    * @author dungdq
    * @param: endpoint, param
    * @throws: IOException
    */

    private static void post(String endpoint, Map<String, String> params)
            throws IOException {
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
       do {

            if (iterator.hasNext()) {
            	 Entry<String, String> param = iterator.next();
                 bodyBuilder.append(param.getKey()).append('=')
                         .append(param.getValue());
                bodyBuilder.append('&');
            }
        } while (iterator.hasNext());

        String body = bodyBuilder.toString();
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;


        try {
        	conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setChunkedStreamingMode(0);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            }

        }
        catch (Exception e) {
        	MyLog.e("post", "fail", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
      }
}
