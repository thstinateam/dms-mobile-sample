/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;

/**
 * Mo ta chuc nang class
 * 
 * @author BANGHN
 * @version 1.0
 */
public class LogFile {

	public static void logToFile(String logContent) {
		//Release mode thi ko ghi log
		if(!GlobalInfo.getInstance().isDebugMode) {
			return;
		}
		
		File logFile = new File(ExternalStorage.SDCARD_PATH + "/myLogFile.txt");
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
		}
		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(logContent);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}
}
