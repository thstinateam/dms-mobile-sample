package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;

import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.main.AbstractAlertDialog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;


/**
 * Popup thong bao cao ct httm ma khach hang tham gia
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportListCustomerView extends AbstractAlertDialog{
	private DMSTableView tbReportListCustomer;// table data
	public ReportListCustomerView(Context context, BaseFragment listener, String title) {
		super(context, listener, title);
		View viewLayout = setViewLayout(
				R.layout.layout_report_list_customer_view);
		tbReportListCustomer = (DMSTableView) viewLayout.findViewById(R.id.tbReportListCustomer);
		tbReportListCustomer.addHeader(new ReportListCustomerRow(parent));
	}

	 /**
	 * render layout
	 * @author: Tuanlt11
	 * @param lstReport
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(ArrayList<ReportKeyshopStaffItemDTO> lstReport){
		for(int i = 0, size = lstReport.size(); i < size; i++){
			ReportKeyshopStaffItemDTO item = lstReport.get(i);
			ReportListCustomerRow row = new ReportListCustomerRow(parent);
			row.renderLayout(item);
			tbReportListCustomer.addRow(row);
		}
	}







}
