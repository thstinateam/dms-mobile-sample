package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class SALE_ORDER_PROMO_DETAIL_TABLE extends ABSTRACT_TABLE {

	// id
	public static final String SALE_ORDER_PROMO_DETAIL_ID = "SALE_ORDER_PROMO_DETAIL_ID";
	public static final String SALE_ORDER_DETAIL_ID = "SALE_ORDER_DETAIL_ID";
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// % chiet khau
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// So tien chiet khau
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	public static final String MAX_AMOUNT_FREE = "MAX_AMOUNT_FREE";
	//public static final String MAX_QUANTITY_FREE_CK = "MAX_QUANTITY_FREE_CK";
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public static final String IS_OWNER = "IS_OWNER";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String STATUS = "STATUS";

	public static final String TABLE_NAME = "SALE_ORDER_PROMO_DETAIL";

	public SALE_ORDER_PROMO_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SALE_ORDER_PROMO_DETAIL_ID, SALE_ORDER_DETAIL_ID, PROGRAM_TYPE, PROGRAM_CODE, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				MAX_AMOUNT_FREE, IS_OWNER, SYN_STATE, STAFF_ID, SALE_ORDER_ID, PROGRAME_TYPE_CODE, SHOP_ID, ORDER_DATE, PRODUCT_GROUP_ID, GROUP_LEVEL_ID
				/*,MAX_QUANTITY_FREE_CK*/
				};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		SaleOrderPromoDetailDTO promoDto = (SaleOrderPromoDetailDTO) dto;
		return insert(null, initDataRow(promoDto));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(SaleOrderPromoDetailDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
		if(dto.isOwner >=0)
			editedValues.put(IS_OWNER, dto.isOwner);
		editedValues.put(MAX_AMOUNT_FREE, dto.maxAmountFree);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(PROGRAM_TYPE, dto.programType);
		if (!StringUtil.isNullOrEmpty(dto.programTypeCode)) {
			editedValues.put(PROGRAME_TYPE_CODE, dto.programTypeCode);
		}
		if(dto.saleOrderDetailId > 0) {
			editedValues.put(SALE_ORDER_DETAIL_ID, dto.saleOrderDetailId);
		}
		editedValues.put(SALE_ORDER_PROMO_DETAIL_ID, dto.saleOrderPromoDetailId);
		editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(ORDER_DATE, dto.orderDate);
		editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
		editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);

		return editedValues;
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 08:18:37 26 Aug 2014
	 * @return: int
	 * @throws:
	 * @param saleOrderId
	 * @return
	 */
	public int deleteAllPromoDetailOfOrder(long saleOrderId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(saleOrderId));

		return delete(SALE_ORDER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}

	/**
	 * @author: DungNX
	 * @param parentStaffId
	 * @param inheritShopId
	 * @param saleOrderID
	 * @return
	 * @throws Exception
	 * @return: List<SaleOrderPromoDetailDTO>
	 * @throws:
	*/
	public ArrayList<SaleOrderPromoDetailDTO> getSaleOrderPromoDetailBySaleOrderID(long saleOrderID) throws Exception{
		ArrayList<SaleOrderPromoDetailDTO> result = new ArrayList<SaleOrderPromoDetailDTO>();;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    SALE_ORDER_PROMO_DETAIL	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND SALE_ORDER_ID = ?	");
		paramsObject.add(saleOrderID + "");
//		sqlObject.append("	    AND STAFF_ID = ?	");
//		paramsObject.add();
//		sqlObject.append("	    AND SHOP_ID = ?	");
//		paramsObject.add();

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						SaleOrderPromoDetailDTO item = new SaleOrderPromoDetailDTO();
						item.initDataFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return result;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<SaleOrderPromoDetailDTO>
	 * @throws:
	 * @param orderId
	 * @return
	 */
	public ArrayList<SaleOrderPromoDetailDTO> getSaleOrderPromoDetailAccumulation(long orderId) {
		ArrayList<SaleOrderPromoDetailDTO> result = new ArrayList<SaleOrderPromoDetailDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT ");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    SALE_ORDER_PROMO_DETAIL	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND SALE_ORDER_ID = ? ");
		sqlObject.append("	    AND SALE_ORDER_DETAIL_ID IS NULL ");
		paramsObject.add(String.valueOf(orderId));

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {

					do{
						SaleOrderPromoDetailDTO item = new SaleOrderPromoDetailDTO();
						item.initDataFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("", e.toString());
			}
		}

		return result;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<SaleOrderPromoDetailDTO>
	 * @throws:
	 * @param poCustomerId
	 * @return
	 */
	public ArrayList<SaleOrderPromoDetailDTO> getPoCustomerPromoDetailAccumulation(long poCustomerId) {
		ArrayList<SaleOrderPromoDetailDTO> result = new ArrayList<SaleOrderPromoDetailDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT ");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    PO_CUSTOMER_PROMO_DETAIL	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND PO_CUSTOMER_ID = ? ");
		sqlObject.append("	    AND PO_CUSTOMER_DETAIL_ID IS NULL ");
		paramsObject.add(String.valueOf(poCustomerId));

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {

					do{
						SaleOrderPromoDetailDTO item = new SaleOrderPromoDetailDTO();
						item.initDataFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("", e.toString());
			}
		}

		return result;
	}
}
