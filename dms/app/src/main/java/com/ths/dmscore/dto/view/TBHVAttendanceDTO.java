/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;

/**
 * TBHV thong tin bao cao cham cong ngay cua GSNPP
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class TBHVAttendanceDTO implements Serializable {
	private static final long serialVersionUID = -6240820845577100247L;
	public ArrayList<TVBHAttendanceItem> arrGsnppList = new ArrayList<TBHVAttendanceDTO.TVBHAttendanceItem>();
	public ArrayList<NvbhLog> arrNvbhList = new ArrayList<NvbhLog>();
	public Hashtable<String, ShopParamDTO> listParam = new Hashtable<String, ShopParamDTO>();

	public TVBHAttendanceItem newTVBHAttendanceItem() {
		return new TVBHAttendanceItem();
	}

	public NvbhLog newNvbhLog() {
		return new NvbhLog();
	}

	public class TVBHAttendanceItem implements Serializable {
		private static final long serialVersionUID = 1L;
		public String nvbhShopCode;
		public String nvbhShopName;
		public double nvbhShopLat;
		public double nvbhShopLng;
		public int gsnppStaffId;
		public String gsnppStaffCode;
		public String gsnppStaffName;
		public int numNvbh;;
		public int lateArrivedNPP;
		public int notArrivedNPP;
		public int nvbhShopId;
		public int gsnppObjectType;// loai gs

		/**
		 * init data with cursor
		 * 
		 * @param c
		 * @return: void
		 * @throws:
		 * @author: HaiTC3
		 * @date: Jan 22, 2013
		 */
		public void initObjectWithCursor(Cursor c) throws Exception{
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
				gsnppStaffId = CursorUtil.getInt(c, "GS_STAFF_ID");
				gsnppStaffCode = CursorUtil.getString(c, "GS_STAFF_CODE");
				gsnppStaffName = CursorUtil.getString(c, "GS_STAFF_NAME");
				nvbhShopId = CursorUtil.getInt(c, "NVBH_SHOP_ID");
				nvbhShopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
				nvbhShopName = CursorUtil.getString(c, "NVBH_SHOP_NAME");
				nvbhShopLat = CursorUtil.getDouble(c, "NVBH_SHOP_LAT");
				nvbhShopLng = CursorUtil.getDouble(c, "NVBH_SHOP_LNG");
				numNvbh = CursorUtil.getInt(c, "NUM_NVBH");
				//+1 them chinh gsnpp do
				numNvbh = CursorUtil.getInt(c, "NUM_NVBH") + 1;
				gsnppObjectType = CursorUtil.getInt(c, "GS_OBJECT_TYPE");
				notArrivedNPP = numNvbh;
			} catch (Exception e) {
				throw e;
			}
		}
	}

	public class NvbhLog implements Serializable {
		private static final long serialVersionUID = 1L;
		public int nvbhStaffId;
		public int gsnppStaffId;
		// public int shopDistance;
		public ArrayList<LatLng> latLngList = new ArrayList<LatLng>();
		public ArrayList<LatLng> latLngList815 = new ArrayList<LatLng>();
		public LatLng nppLatLng;
		public boolean isValidArrival;
		public boolean isLateArrival;
		public int nvbhShopId;

		public void initCursor(Cursor c) {
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
				nvbhStaffId = CursorUtil.getInt(c, "STAFF_ID");
				gsnppStaffId = CursorUtil.getInt(c, "NPP_STAFF_ID");
				nvbhShopId = CursorUtil.getInt(c, "SHOP_ID");
				double nppLat = 0;
				nppLat = CursorUtil.getDouble(c, "SHOP_LAT");
				double nppLng = 0;
				nppLng = CursorUtil.getDouble(c, "SHOP_LNG");
				nppLatLng = new LatLng(nppLat, nppLng);

				String latListStr = null;
				String lngListStr = null;
				String latList815Str = null;
				String lngList815Str = null;
				latListStr = CursorUtil.getString(c, "LAT_LIST");
				lngListStr = CursorUtil.getString(c, "LNG_LIST");
				latList815Str = CursorUtil.getString(c, "LAT_LIST_815");
				lngList815Str = CursorUtil.getString(c, "LNG_LIST_815");
				String[] latList = latListStr.split(",");
				String[] lngList = lngListStr.split(",");
				String[] latList815 = latList815Str.split(",");
				String[] lngList815 = lngList815Str.split(",");
				for (int i = 0; i < latList.length; i++) {
					LatLng latLng = new LatLng();
					if (!StringUtil.isNullOrEmpty(latList[i])) {
						latLng.lat = Double.parseDouble(latList[i]);
						latLng.lng = Double.parseDouble(lngList[i]);
					}
					latLngList.add(latLng);
				}
				for (int i = 0; i < latLngList.size(); i++) {
					double distance = GlobalUtil.getDistanceBetween(nppLatLng, latLngList.get(i));
					if (distance <= Double.parseDouble(listParam.get("CC_DISTANCE").value)) {
						isValidArrival = true;
						break;
					}
				}

				for (int i = 0; i < latList815.length; i++) {
					LatLng latLng = new LatLng();
					if (!StringUtil.isNullOrEmpty(latList815[i])) {
						latLng.lat = Double.parseDouble(latList815[i]);
						latLng.lng = Double.parseDouble(lngList815[i]);
					}
					latLngList815.add(latLng);
				}
				// neu chua toi trong khoang quy dinh thi check tiep toi muon
				// gio
				if (!isValidArrival) {
					for (int i = 0; i < latLngList815.size(); i++) {
						double distance = GlobalUtil.getDistanceBetween(nppLatLng, latLngList815.get(i));
						if (distance <= Double.parseDouble(listParam.get("CC_DISTANCE").value)) {
							isLateArrival = true;
							break;
						}
					}
				}

			} catch (Exception e) {
			}
		}
		
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public void processCount() {
		for (int i = 0; i < arrGsnppList.size(); i++) {
			for (int j = 0; j < arrNvbhList.size(); j++) {
				if (arrGsnppList.get(i).gsnppStaffId == arrNvbhList.get(j).gsnppStaffId) {
					if (arrNvbhList.get(j).isValidArrival
							&& arrNvbhList.get(j).nvbhShopId == arrGsnppList
									.get(i).nvbhShopId) {
						arrGsnppList.get(i).notArrivedNPP--;
					}
					if (arrNvbhList.get(j).isLateArrival
							&& arrNvbhList.get(j).nvbhShopId == arrGsnppList
									.get(i).nvbhShopId) {
						arrGsnppList.get(i).lateArrivedNPP++;
					}
				}
			}
		}
	}

}
