package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class TBHVCustomerNotPSDSReportDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int totalList;	
	public String totalofTotal;
	
	public ArrayList<TBHVCustomerNotPSDSReportItem> arrList;
	
	public TBHVCustomerNotPSDSReportDTO(){
		arrList = new ArrayList<TBHVCustomerNotPSDSReportDTO.TBHVCustomerNotPSDSReportItem>();		
	}
	
	public void addItem(Cursor c){
		TBHVCustomerNotPSDSReportItem item = new TBHVCustomerNotPSDSReportItem();		
		item.tenNPP = CursorUtil.getString(c, "TEN_NPP");
		item.maNPP = CursorUtil.getString(c, "MA_NPP");
		item.idNPP = CursorUtil.getString(c, "ID_NPP");
		item.idGSNPP = CursorUtil.getString(c, "STAFF_ID");
		item.maGSNPP = CursorUtil.getString(c, "STAFF_CODE");
		item.tenGSNPP = CursorUtil.getString(c, "TEN_GSNPP");
		item.tongSoKH = CursorUtil.getString(c, "TONG_SO_KH");		
		item.khongPSDS = CursorUtil.getString(c, "KHONG_PSDS");
		item.psdsLuyKe = "" + (Integer.parseInt(item.tongSoKH) - Integer.parseInt(item.khongPSDS));
		arrList.add(item);
	}
	
	public class TBHVCustomerNotPSDSReportItem implements Serializable{		
		private static final long serialVersionUID = 1L;
		public String stt;
		public String tenNPP;
		public String maNPP;
		public String idNPP;
		public String tenGSNPP;
		public String tongSoKH;
		public String psdsLuyKe;
		public String khongPSDS;	
		public String maGSNPP = "";
		public String idGSNPP = "";
		
		public TBHVCustomerNotPSDSReportItem(){			
			stt = "";
			tenNPP = "";
			maNPP = "";
			idNPP = "";
			tenGSNPP = "";
			maGSNPP = "";
			idGSNPP = "";
			tongSoKH = "";
			psdsLuyKe = "";
			khongPSDS = "";						
		}
	}
}
