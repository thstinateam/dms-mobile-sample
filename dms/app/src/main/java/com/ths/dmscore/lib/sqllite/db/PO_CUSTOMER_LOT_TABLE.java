package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderLotDTO;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class PO_CUSTOMER_LOT_TABLE extends ABSTRACT_TABLE {

	// id
	public static final String PO_CUSTOMER_LOT_ID = "PO_CUSTOMER_LOT_ID";
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	public static final String PO_CUSTOMER_DETAIL_ID = "PO_CUSTOMER_DETAIL_ID";
	public static final String WAREHOUSE_ID = "WAREHOUSE_ID";
	// Id Stock total
	public static final String STOCK_TOTAL_ID = "STOCK_TOTAL_ID";
	// So lo (neu co)
	public static final String LOT = "LOT";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String PRICE = "PRICE";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String STAFF_ID = "STAFF_ID";
	// So luong
	public static final String QUANTITY = "QUANTITY";
	// Ngay het han cua lo hang
	public static final String EXPIRATION_DATE = "EXPIRATION_DATE";
	// Ngay dat hang
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	// luu discount percent cho Kho (lo) nay
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// discount cua kho(lo) nay
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	public static final String PRICE_ID = "PRICE_ID";
	public static final String QUANTITY_RETAIL = "QUANTITY_RETAIL";
	public static final String QUANTITY_PACKAGE = "QUANTITY_PACKAGE";
	public static final String PACKAGE_PRICE = "PACKAGE_PRICE";

	public static final String TABLE_NAME = "PO_CUSTOMER_LOT";

	public PO_CUSTOMER_LOT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PO_CUSTOMER_LOT_ID, PO_CUSTOMER_ID, PO_CUSTOMER_DETAIL_ID, WAREHOUSE_ID, STOCK_TOTAL_ID, LOT, PRODUCT_ID, PRICE, SHOP_ID,
				STAFF_ID, QUANTITY, EXPIRATION_DATE, ORDER_DATE, CREATE_DATE, UPDATE_DATE, CREATE_USER, UPDATE_USER, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				SYN_STATE,PRICE_ID,QUANTITY_RETAIL,QUANTITY_PACKAGE,PACKAGE_PRICE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SaleOrderLotDTO) dto);
		return insert(null, value);
	}

	private ContentValues initDataRow(SaleOrderLotDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(PO_CUSTOMER_LOT_ID, dto.poLotId);
		editedValues.put(PO_CUSTOMER_ID, dto.poId);
		editedValues.put(PO_CUSTOMER_DETAIL_ID, dto.poDetailId);
		if (dto.wareHouseId > 0) {
			editedValues.put(WAREHOUSE_ID, dto.wareHouseId);
		}
		if(dto.stockTotalId > 0)
			editedValues.put(STOCK_TOTAL_ID, dto.stockTotalId);
		if (!StringUtil.isNullOrEmpty(dto.lot)) {
			editedValues.put(LOT, dto.lot);
		}
		editedValues.put(PRODUCT_ID, dto.productId);
		editedValues.put(PRICE, dto.price);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(QUANTITY, dto.quantity);

		if (!StringUtil.isNullOrEmpty(dto.expirationDate)) {
			editedValues.put(EXPIRATION_DATE, dto.expirationDate);
		}

		if (!StringUtil.isNullOrEmpty(dto.orderDate)) {
			editedValues.put(ORDER_DATE, dto.orderDate);
		}
		if (!StringUtil.isNullOrEmpty(dto.updateDate)) {
			editedValues.put(UPDATE_DATE, dto.updateDate);
		}
		if (!StringUtil.isNullOrEmpty(dto.createDate)) {
			editedValues.put(CREATE_DATE, dto.createDate);
		}
		if (!StringUtil.isNullOrEmpty(dto.createUser)) {
			editedValues.put(CREATE_USER, dto.createUser);
		}
		if (!StringUtil.isNullOrEmpty(dto.udpateUser)) {
			editedValues.put(UPDATE_USER, dto.udpateUser);
		}
//		if (dto.discountAmount > 0) {
//			editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
//		}

		if (dto.discountAmount > 0) {
			editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		}
		if (dto.priceId > 0) {
			editedValues.put(PRICE_ID, dto.priceId);
		}
		if (dto.quantityRetail > 0) {
			editedValues.put(QUANTITY_RETAIL, dto.quantityRetail);
		}
		if (dto.quantityPackage > 0) {
			editedValues.put(QUANTITY_PACKAGE, dto.quantityPackage);
		}
		if (dto.packagePrice > 0) {
			editedValues.put(PACKAGE_PRICE, dto.packagePrice);
		}
		return editedValues;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 11:15:06 26 Sep 2014
	 * @return: int
	 * @throws:
	 * @param poId
	 * @return
	 */
	public int deleteAllDetailOfPo(long poId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(poId));

		return delete(PO_CUSTOMER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}

}
