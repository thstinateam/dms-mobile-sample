/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.util.LatLng;

/**
 * general statistics info view
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class GeneralStatisticsInfoViewDTO {
	// cham cong
	public List<AttendanceDTO> attendanceDTO;
	// dung de hien thi len man hinh
	public AttendanceDTO attendance;
	// thong tin dung de loc cham cong
	public List<ApParamDTO> listInfo;
	public LatLng shopPosition;
	// tong thoi gian ghe tham
	public int time;

	// so ngay ban hang theo ke hoach
	public int numberDayPlan;
	// so ngay ban hang da qua
	public int numberDaySold;
	// tien do chuan
	public double progressSold;
	// list report follow dayInOrder
	public List<ReportInfoDTO> listReportDate = new ArrayList<ReportInfoDTO>();

	// list report follow month

	public List<ReportInfoDTO> listReportMonth = new ArrayList<ReportInfoDTO>();
	// bao cao nganh hang
	public List<ReportInfoDTO> listReportCat = new ArrayList<ReportInfoDTO>();
	public GeneralStatisticsInfoViewDTO() {
		attendanceDTO=new ArrayList<AttendanceDTO>();
		attendance = new AttendanceDTO();
		listInfo = new ArrayList<ApParamDTO>();
		shopPosition=new LatLng();

		numberDayPlan = 0;
		numberDaySold = 0;
		progressSold = 0;
		listReportDate = new ArrayList<ReportInfoDTO>();
		listReportMonth = new ArrayList<ReportInfoDTO>();
	}
}
