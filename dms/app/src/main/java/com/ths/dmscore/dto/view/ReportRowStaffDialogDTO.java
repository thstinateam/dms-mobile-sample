package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cho man hinh chon nhan vien
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportRowStaffDialogDTO {
	public ArrayList<ReportRowStaffDTO> lstStaff = new ArrayList<ReportRowStaffDTO>();
	public int totalStaff = 0;
}
