package com.ths.dmscore.dto.db;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.RPT_KPI_IN_DATE_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * dto cho bao cao ngay
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class RptKPIInDateDTO extends RptKPIDTO implements Serializable {
	public long rptKpiInDateId;
	public String rptInDate;
	public int staffId;
	// ten cua cot
	public String columnName;

	public RptKPIInDateDTO() {
		super();
	}

	@Override
	public RptKPIInDateDTO clone(){
		RptKPIInDateDTO dto = new RptKPIInDateDTO();
		dto.rptKpiInDateId = this.rptKpiInDateId;
		dto.objectId = this.objectId;
		dto.objectTypeId = this.objectTypeId;
		dto.shopId = this.shopId;
		dto.kpiId = this.kpiId;
		dto.rptInDate = this.rptInDate;
		dto.columnCriId = this.columnCriId;
		dto.columnCriTypeId = this.columnCriTypeId;
		dto.valuePlan = this.valuePlan;
		dto.value = this.value;
		dto.percentComplete = this.percentComplete;
		dto.staffId  = this.staffId;
		dto.columnName = this.columnName;
		return dto;
	}

	 /**
	 * Khoi tao du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @param typeKpiID : loai KPI IDs
	 * @return: void
	 * @throws:
	*/
	@Override
	public void initDataFromCursor(Cursor c, int typeKpiID){
		rptKpiInDateId = CursorUtil.getLong(c, RPT_KPI_IN_DATE_TABLE.RPT_KPI_IN_DATE_ID);
		objectId = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.OBJECT_ID);
		objectTypeId = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.OBJECT_TYPE_ID);
		shopId = CursorUtil.getLong(c, RPT_KPI_IN_DATE_TABLE.SHOP_ID);
		kpiId = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.KPI_ID);
		rptInDate = CursorUtil.getString(c, RPT_KPI_IN_DATE_TABLE.RPT_IN_DATE);
		columnCriId = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.COLUMN_CRI_ID);
		columnCriTypeId = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.COLUMN_CRI_TYPE_ID);
		value = CursorUtil.getDouble(c, "VALUE_SUM");
		valuePlan = CursorUtil.getDouble(c,
				"VALUE_PLAN_SUM");
//		valuePlan = + value;
		// neu la san luong thi chia 1000 ra so tien
		if (typeKpiID == ReportTemplateCriterionDTO.AMOUNT) {
			valuePlan = valuePlan / 1000;
			value = value / 1000;
		}
		percentComplete = CursorUtil.getDouble(c, RPT_KPI_IN_DATE_TABLE.PERCENT_COMPLETE);
		// tinh lai percent cho chac
		percentComplete = valuePlan > 0 ? value / valuePlan * 100.0
						: (value> 0 ? 100.0:0);
		staffId  = CursorUtil.getInt(c, RPT_KPI_IN_DATE_TABLE.STAFF_ID);
		columnName = CursorUtil.getString(c, "COLUMN_NAME");
	}
}
