package com.ths.dmscore.view.control;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Tao mot bang table layout co bo tron
 * 
 * @author: PhucNT
 * @version: 1.0
 * @since: 1.0
 */
public class VinamilkTableView extends LinearLayout implements 
		OnEventControlListener, View.OnTouchListener {

	public static final int ACTION_CHANGE_PAGE = 1;
	public static final int LIMIT_ROW_PER_PAGE = 10;


	Context context;
	// listener control load more for table
	private VinamilkTableListener mListener;

	// number item in one page
	private int numItemInPage = Constants.NUM_ITEM_PER_PAGE;
	// list table row
	private List<TableRow> listRowView = new ArrayList<TableRow>();
	// table content === table row list for this table
	public TableLayout tbContent;
	// header for table
	private VinamilkHeaderTable tbHeader;
	// control paging for table
	private PagingControl tbPaging;
	// total size
	private int totalSize;
	// duongdt
	private View rowNoContentSimpleHeader;
	private View rowNoContentComplexHeader;
	private TextView tvNoContentSimpleHeader;
	private TextView tvNoContentComplexHeader;

	/**
	 * init for vinamilk table view with context, attributeset
	 * @param context
	 * @param attrs
	 */
	public VinamilkTableView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}

	public VinamilkTableView(Context context) {
		super(context);
		initView(context);
	}
	public VinamilkTableView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context);
	}

	/**
	 * init controls in table view
	 * 
	 * @author : PhucNT param :
	 */
	private void initView(Context context) {
		// TODO Auto-generated method stub
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View table = inflater.inflate(R.layout.layout_vinamilk_table, this);
		tbHeader = (VinamilkHeaderTable) table.findViewById(R.id.header);
		tbHeader.setMyListener(this);
		tbContent = (TableLayout) table.findViewById(R.id.tbContent);
		tbPaging = (PagingControl) table.findViewById(R.id.paging);
		tbPaging.setItemListener(this);
		tbPaging.setAction(ACTION_CHANGE_PAGE);
		// duongdt no content row
		rowNoContentSimpleHeader = table.findViewById(R.id.rowNoContentSimpleHeader);
		rowNoContentComplexHeader = table.findViewById(R.id.rowNoContentComplexHeader);
		tvNoContentSimpleHeader = (TextView) table.findViewById(R.id.tvNoContentSimpleHeader);
		tvNoContentComplexHeader = (TextView) table.findViewById(R.id.tvNoContentComplexHeader);
	}

	public void setNumItemsPage(int num) {
		this.numItemInPage = num;
	}

	public void setListener(VinamilkTableListener l) {
		mListener = l;
	}
	
	public void disablePagingControl(){
		tbPaging.setVisibility(View.GONE);
	}

	public VinamilkHeaderTable getHeaderView() {
		return tbHeader;
	}
	public PagingControl getPagingControl() {
		return tbPaging;
	}
	/**
	 * set total so luong row trong bang
	 * 
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	public void setTotalSize(int count) {
		this.totalSize = count;
		int page = 0;
		if (count % numItemInPage == 0) {
			page = count / numItemInPage;
		} else {
			page = count / numItemInPage + 1;
		}
		
		if(totalSize <= numItemInPage){
			tbPaging.setVisibility(View.GONE);
		}else{
			tbPaging.setVisibility(View.VISIBLE);
//			tbPaging.setTotalPage(page);
		}
		tbPaging.setTotalPage(page);
		tbPaging.setCurrentPage(1);

	}

	/**
	 * tao nhung row cho table layout
	 * 
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	public void addContent(List<TableRow> listRow) {
	
		// remove all object in list
		this.getListRowView().clear();
		
		int size = listRow.size();
		for (int i = 0; i < size; i++) {
			this.getListRowView().add(listRow.get(i));
		}
		// mac dinh hien thi trang dau tien
		updateTable();
	}

	/**
	 * tao nhung row cho table layout
	 * 
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	public void addHighLightContent(List<TableRow> listRow,int isHighlight) {
	
		// remove all object in list
		this.getListRowView().clear();
		
		int size = listRow.size();
		for (int i = 0; i < size; i++) {
			this.getListRowView().add(listRow.get(i));
		}
		// mac dinh hien thi trang dau tien
		updateTable();
	}
	/**
	 * tao nhung row cho table layout
	 * 
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	private void updateTable() {
		// mac dinh hien thi trang dau tien
		tbContent.removeAllViews();
//		int beginIndex = numItemInPage * (tbPaging.getCurrentPage()-1);
//		int endIndex = Math.min(beginIndex + numItemInPage, listRowView.size());
		for (int i = 0, size = getListRowView().size(); i < size; i++) {
			tbContent.addView(getListRowView().get(i));
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
//		updateTable();
		if (mListener != null && control == tbPaging) {
			mListener.handleVinamilkTableloadMore(this, data);			
		}
		else if(mListener != null && control == tbHeader){
			mListener.handleVinamilkTableRowEvent(eventType, this, data);
		}
	}

	/**
	 * @return the listRowView
	 */
	public List<TableRow> getListRowView() {
		return listRowView;
	}

	/**
	 * @param listRowView the listRowView to set
	 */
	public void setListRowView(List<TableRow> listRowView) {
		this.listRowView = listRowView;
	}

	/**
	 * Clear all data and view before add one row
	 * 
	 * @author: TRUNGHQM
	 * @return: void
	 * @throws:
	 */
	public void clearAllData(){
		this.getListRowView().clear();
		tbContent.removeAllViews();
	}
	
	/**
	 * add one row into table
	 * 
	 * @author: TRUNGHQM
	 * param : TableRow 
	 * @return: void
	 * @throws:
	 */
	public void addRow(TableRow row){
		this.getListRowView().add(row);
		tbContent.addView(row);
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		//an khi nhan vao header
			GlobalUtil.forceHideKeyboardInput(getContext(),this);
		return false;
	}
	
	/**
	 * Show row no content
	 * 
	 * @author: duongdt 19:39:52 12 Oct 2013
	 * @return: void
	 * @throws:
	 */
	public void showNoContentRow() {
		// have header simple
		if (tbHeader.getHeaderCount() > 0) {
			rowNoContentSimpleHeader.setVisibility(VISIBLE);
			rowNoContentComplexHeader.setVisibility(GONE);
		} else {
			// if complex thi GONE tbContent
			rowNoContentSimpleHeader.setVisibility(GONE);
			rowNoContentComplexHeader.setVisibility(VISIBLE);
		}
		tbContent.setVisibility(GONE);
	}
	
	/**
	 * Hien thi dong voi noi dung mong muon.
	 * @author: dungdq3
	 * @return: void
	 */
	public void showNoContentRowWithString(String strContent) {
		// have header simple
		if (tbHeader.getHeaderCount() > 0) {
			rowNoContentSimpleHeader.setVisibility(VISIBLE);
			tvNoContentSimpleHeader.setText(strContent);
			rowNoContentComplexHeader.setVisibility(GONE);
		} else {
			// if complex thi GONE tbContent
			rowNoContentSimpleHeader.setVisibility(GONE);
			rowNoContentComplexHeader.setVisibility(VISIBLE);
			tvNoContentComplexHeader.setText(strContent);
		}
		tbContent.setVisibility(GONE);
	}

	/**
	 * Set text display when no row
	 * 
	 * @param textNoContent
	 */
	public void setNoContentText(String textNoContent) {
		tvNoContentComplexHeader.setText(textNoContent);
		tvNoContentSimpleHeader.setText(textNoContent);
	}

	/**
	 * Hide row no content
	 * 
	 * @author: duongdt 19:39:52 12 Oct 2013
	 * @return: void
	 * @throws:
	 */
	public void hideNoContentRow() {
		rowNoContentSimpleHeader.setVisibility(GONE);
		rowNoContentComplexHeader.setVisibility(GONE);
		tbContent.setVisibility(VISIBLE);
	}
}
