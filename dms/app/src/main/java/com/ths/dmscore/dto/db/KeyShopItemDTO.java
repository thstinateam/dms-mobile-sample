/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.CursorUtil;

/**
 * KeyShopItemDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  16:11:03 10-07-2015
 */
public class KeyShopItemDTO {
	public long ksId;
	public String ksCode = Constants.STR_BLANK;
	public String name = Constants.STR_BLANK;
	public String description = Constants.STR_BLANK;
	public String fromCycleId = Constants.STR_BLANK;
	public String toCycleId = Constants.STR_BLANK;
	public ArrayList<KSLevelDTO> ksLevelItem = new ArrayList<KSLevelDTO>();
	public String productInfo = Constants.STR_BLANK;

	
	/**
	 * initData
	 * @author: yennth16
	 * @since: 11:03:01 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initData(Cursor c){
		ksId = CursorUtil.getLong(c, "KS_ID");
		ksCode = CursorUtil.getString(c, "KS_CODE");
		name = CursorUtil.getString(c, "NAME");
		description = CursorUtil.getString(c, "DESCRIPTION");
		fromCycleId = CursorUtil.getString(c, "FROM_CYCLE_ID");
		toCycleId = CursorUtil.getString(c, "TO_CYCLE_ID");
	}

}

