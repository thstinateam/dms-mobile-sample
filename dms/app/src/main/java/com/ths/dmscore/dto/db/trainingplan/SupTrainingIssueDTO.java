/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db.trainingplan;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO.TableAction;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.SUP_TRAINING_ISSUE_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * SupTrainingIssueDTO.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 8:42:35 AM Nov 19, 2014
 */
public class SupTrainingIssueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// khoa chinh
	private long supTrainingIssueID;
	// ma huan luyen
	private long supTrainingPlanID;
	// van de
	private String issue;
	// noi dung thuc hien
	private String jobContent;
	// ma nv huan luyen
	private long staffID;
	// ma giam sat cua nv duoc huan luyen
	private long excuteStaffID;
	// chuc danh thuc hien
	private String excuteRole;
	// ngay yeu cau
	private String dueDate;
	private String dueDateView;
	// ngay hoan thanh
	private String completeDate;
	private String completeDateView;
	// ma nv tao van de
	private long createStaffID;
	// Trang thai: 1: tao moi; 2: da thuc hien; 3: da duyet, 4: thuc hien lai
	private int status;
	// nguoi tao
	private String createUser;
	// ngay tao
	private String createDate;
	// nguoi cap nhat
	private String updateUser;
	// ngay cap nhat
	private String updateDate;

	public SupTrainingIssueDTO() {
		// TODO Auto-generated constructor stub
		supTrainingIssueID = 0;
		supTrainingPlanID = 0;
		issue = Constants.STR_BLANK;
		jobContent = Constants.STR_BLANK;
		excuteStaffID = 0;
		excuteRole = Constants.STR_BLANK;
		dueDate = Constants.STR_BLANK;
		completeDate = Constants.STR_BLANK;
		status = 1;
		staffID = 0;
		createStaffID = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		createUser = Constants.STR_BLANK;
		createDate = Constants.STR_BLANK;
		updateUser = Constants.STR_BLANK;
		updateDate = Constants.STR_BLANK;
	}

	public long getSupTrainingIssueID() {
		return supTrainingIssueID;
	}

	public void setSupTrainingIssueID(long supTrainingIssueID) {
		this.supTrainingIssueID = supTrainingIssueID;
	}

	public long getSupTrainingPlanID() {
		return supTrainingPlanID;
	}

	public void setSupTrainingPlanID(long supTrainingPlanID) {
		this.supTrainingPlanID = supTrainingPlanID;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getJobContent() {
		return jobContent;
	}

	public void setJobContent(String jobContent) {
		this.jobContent = jobContent;
	}

	public long getExcuteStaffID() {
		return excuteStaffID;
	}

	public void setExcuteStaffID(long excuteStaffID) {
		this.excuteStaffID = excuteStaffID;
	}

	public String getExcuteRole() {
		return excuteRole;
	}

	public void setExcuteRole(String excuteRole) {
		this.excuteRole = excuteRole;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(String completeDate) {
		this.completeDate = completeDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * init from cursor
	 * 
	 * @author: dungdq3
	 * @since: 9:20:48 AM Nov 19, 2014
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		supTrainingIssueID = CursorUtil.getLong(c, SUP_TRAINING_ISSUE_TABLE.SUP_TRAINING_ISSUE_ID);
		issue = CursorUtil.getString(c, SUP_TRAINING_ISSUE_TABLE.ISSUE);
		jobContent = CursorUtil.getString(c, SUP_TRAINING_ISSUE_TABLE.JOB_CONTENT);
		excuteStaffID = CursorUtil.getLong(c, SUP_TRAINING_ISSUE_TABLE.EXECUTE_STAFF_ID);
		excuteRole = CursorUtil.getString(c, SUP_TRAINING_ISSUE_TABLE.EXECUTE_ROLE);
		dueDate = CursorUtil.getString(c, SUP_TRAINING_ISSUE_TABLE.DUE_DATE);
		dueDateView = CursorUtil.getString(c, "DUE_DATE_VIEW");
		completeDate = CursorUtil.getString(c, SUP_TRAINING_ISSUE_TABLE.COMPLETE_DATE);
		status = CursorUtil.getInt(c, SUP_TRAINING_ISSUE_TABLE.STATUS);
	}

	public long getStaffID() {
		return staffID;
	}

	public void setStaffID(long staffID) {
		this.staffID = staffID;
	}

	public long getCreateStaffID() {
		return createStaffID;
	}

	public void setCreateStaffID(long createStaffID) {
		this.createStaffID = createStaffID;
	}

	/**
	 * luu issue
	 * 
	 * @author: dungdq3
	 * @since: 3:31:42 PM Nov 19, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateInsertIssue() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject issueJSON = new JSONObject();
		try {
			issueJSON.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			issueJSON.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_ISSUE_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.SUP_TRAINING_ISSUE_ID, supTrainingIssueID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.SUP_TRAINING_PLAN_ID, supTrainingPlanID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.ISSUE, issue, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.JOB_CONTENT, jobContent, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.EXECUTE_STAFF_ID, excuteStaffID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.EXECUTE_ROLE, excuteRole, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.DUE_DATE, dueDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.CREATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.CREATE_STAFF_ID, createStaffID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.CREATE_USER, createUser, null));
			issueJSON.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
			throw e;
		}

		return issueJSON;
	}

	/**
	 * generate JSON delete
	 * 
	 * @author: dungdq3
	 * @since: 5:28:20 PM Nov 19, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateJSONDeleteIssue() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_ISSUE_TABLE.TABLE_NAME);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.SUP_TRAINING_ISSUE_ID, supTrainingIssueID, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 8:46:40 AM Nov 20, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateJSONUpdateIssue() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject issueJSON = new JSONObject();
		try {
			issueJSON.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			issueJSON.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_ISSUE_TABLE.TABLE_NAME);
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.JOB_CONTENT, jobContent, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.ISSUE, issue, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.EXECUTE_ROLE, excuteRole, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.DUE_DATE, dueDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.UPDATE_DATE, updateDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.UPDATE_USER, updateUser, null));
			if(!StringUtil.isNullOrEmpty(completeDate)){
				detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.COMPLETE_DATE, completeDate, null));
			}
			issueJSON.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_ISSUE_TABLE.SUP_TRAINING_ISSUE_ID, supTrainingIssueID, null));
			issueJSON.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return issueJSON;
	}

	public String getDueDateView() {
		return dueDateView;
	}

	public void setDueDateView(String dueDateView) {
		this.dueDateView = dueDateView;
	}

	public String getCompleteDateView() {
		return completeDateView;
	}

	public void setCompleteDateView(String completeDateView) {
		this.completeDateView = completeDateView;
	}

}
