package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;


/**
 * DTO cho thong tin nhan vien man hinh them van de
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class StaffFeedbackDTO implements Serializable  {
	// noi dung field
	private static final long serialVersionUID = 1L;
	// id nv
	public int staffId;
	// ma nv
	public String staffCode;
	// ten nv
	public String staffName;
	// shopId
	public String shopId;
	// ten shop
	public String shopName;
	// ma code
	public String shopCode;
	// bien de biet row co duoc chon hay ko
	public boolean isCheck = false;
	// loai nhan vien
	public String staffType = "";
	public int specificType;

	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initFromCursor(Cursor c) {
		staffId = CursorUtil.getInt(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		shopId = CursorUtil.getString(c, "SHOP_ID");
		shopName = CursorUtil.getString(c, "SHOP_NAME");
		shopCode = CursorUtil.getString(c, "SHOP_CODE");
		staffType = CursorUtil.getString(c, "STAFF_TYPE");
		specificType = CursorUtil.getInt(c, "SPECIFIC_TYPE");
	}

	@Override
	public Object clone()  {
		StaffFeedbackDTO p = new StaffFeedbackDTO();
		p.staffId = this.staffId;
		p.staffCode = this.staffCode;
		p.staffName = this.staffName;
		p.shopId = this.shopId;
		p.shopName = this.shopName;
		p.shopCode = this.shopCode;
		p.isCheck = this.isCheck;
		p.specificType = this.specificType;
		return p;
	}
}
