package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.TODO_TASK;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Luu thong tin ghi chu
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class ToDoTaskDTO extends AbstractTableDTO {
	// id
	public int id;
	// id khach hang
	public int staffId;
	// noi dung
	public String content;
	// ngay tao
	public String createDate;
	// ngay nhac nho
	public String remindDate;
	// ngay thuc hien
	public String doneDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getRemindDate() {
		return remindDate;
	}

	public void setRemindDate(String remindDate) {
		this.remindDate = remindDate;
	}

	public String getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(String doneDate) {
		this.doneDate = doneDate;
	}

	public void initDataFromCursor(Cursor c) {
		content = CursorUtil.getString(c, "CONTENT");
		remindDate = CursorUtil.getString(c, "REMIND_DATE");
		doneDate = CursorUtil.getString(c, "DONE_DATE");
	}

	public JSONObject generateTakeNoteSql() {
		JSONObject feedbackJson = new JSONObject();

		// inster into TODO_TASK
		// (STAFF_ID, CONTENT, CREATE_DATE)
		// values (staffId, content, createDate)

		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME,
					TODO_TASK.TODO_TASK);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(TODO_TASK.ID,
					"TODO_TASK_SEQ", DATA_TYPE.SEQUENCE.toString()));
			detailPara.put(GlobalUtil.getJsonColumn(TODO_TASK.STAFF_ID,
					staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(TODO_TASK.CONTENT, content,
					null));
			detailPara
					.put(GlobalUtil.getJsonColumn(TODO_TASK.CREATE_DATE,
							DATA_VALUE.sysdate.toString(),
							DATA_TYPE.SYSDATE.toString()));

			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return feedbackJson;
	}
}
