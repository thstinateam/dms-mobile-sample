/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util.locating;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;

/**
 * Service lay dinh vi tu GMS (Google Play Service)
 * com.viettel.vinamilk.util.LocationService
 * @author banghn
 * @version 1.0
 */
public class LocationService extends Service implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, LocationListener {

	private static final String TAG = LocationService.class.getSimpleName();
	private static final long ONE_SECOND = 1000;
	public final static long TIME_SEND_LOG_LOCATING = 600000;
	// dinh thoi goi dinh vi mat dinh
	public static final long TIME_LOC_PERIOD = 180000;

	private GoogleApiClient mLocationClient;

	long radius = 1000;
	long interval = 2 * 60 * 1000;
	long fastInterval = 10 * 1000;
	int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
	boolean isManualDisconnect = false;
	long timeSendLogFusedLocating = 0;
	@Override
	public void onCreate() {
		MyLog.d(TAG, "Creating..");
		mLocationClient = new GoogleApiClient.Builder(this)
				.addApi(LocationServices.API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.build();
		//mLocationClient.setMockMode(false);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		MyLog.d(TAG, "Starting.. intent: " + intent + " flags: " + flags);
		boolean isNotConnect = !mLocationClient.isConnected() && !mLocationClient.isConnecting();
		boolean isReconnect = false;

		if (intent != null) {
			//truong hop co data
			long tempRadius = intent.getIntExtra(IntentConstants.RADIUS_OF_POSITION_FUSED, 1000);
			long tempInterval = intent.getIntExtra(IntentConstants.INTERVAL_FUSED_POSITION, 120) * ONE_SECOND;
			long tempFastInterval = intent.getIntExtra(IntentConstants.FAST_INTERVAL_FUSED_POSITION, 10) * ONE_SECOND;
			long tempPriorityId = intent.getIntExtra(IntentConstants.FUSED_POSTION_PRIORITY, 1);
			int tempPriority = tempPriorityId == 1 ? LocationRequest.PRIORITY_HIGH_ACCURACY : LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
			boolean isRestart = intent.getBooleanExtra(IntentConstants.FUSED_POSTION_RESTART, false);
			//check need reconnect when config request change
			if (isRestart || tempInterval != this.interval || tempFastInterval != this.fastInterval || tempPriority != this.priority) {
				isReconnect = true;
			}
			//save config
			setConfig(tempRadius, tempInterval, tempFastInterval, tempPriority);
		} else{
			//khong co data, co the do retry service, nen lay lai cau hinh luu file
			long tempRadius = GlobalInfo.getInstance().getRadiusFusedPosition();
			long tempInterval = GlobalInfo.getInstance().getIntervalFusedPosition() * ONE_SECOND;
			long tempFastInterval = GlobalInfo.getInstance().getFastIntervalFusedPosition() * ONE_SECOND;
			long tempPriorityId = GlobalInfo.getInstance().getFusedPositionPriority();
			int tempPriority = tempPriorityId == 1 ? LocationRequest.PRIORITY_HIGH_ACCURACY : LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
			//save config
			setConfig(tempRadius, tempInterval, tempFastInterval, tempPriority);
		}

		//chua ket noi thi tien hanh ket noi
		if (isNotConnect) {
			MyLog.d(TAG, "Connecting location client..");
			try {
				mLocationClient.connect();
			} catch (Exception e) {
				MyLog.e("isNotConnect", "connect fail ", e);
			}
		} else if (isReconnect) {
			//neu can reconnect thi disconnect roi tien hanh ket noi lai
			MyLog.d(TAG, "ReConnect location client..");
			try {
				if (mLocationClient.isConnected()) {
					LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this);
				}
			} catch (Exception e) {
				MyLog.e("isReconnect", "isReconnect fail ", e);
			}
			try {
				isManualDisconnect = true;
				mLocationClient.disconnect();
				mLocationClient.connect();
			} catch (Exception e) {
				MyLog.e("isReconnect", "isReconnect fail ", e);
			}
		}

		MyLog.d("LocationService onStartCommand", "radius: " + radius + " interval: " + interval + " fastInterval: " + fastInterval + " priority: " + priority);
		return START_STICKY;
	}

	/**
	 * @author: duongdt3
	 * @since: 11:58:57 6 Nov 2014
	 * @return: void
	 * @throws:
	 * @param tempRadius
	 * @param tempInterval
	 * @param tempFastInterval
	 * @param tempPriority
	 */
	private void setConfig(long tempRadius, long tempInterval, long tempFastInterval, int tempPriority) {
		//luu lai
		this.radius = tempRadius;
		this.interval = tempInterval;
		this.fastInterval = tempFastInterval;
		this.priority = tempPriority;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		MyLog.d(TAG, "Connection failed..");
		stopSelf();
	}

	@Override
	public void onConnected(Bundle bundle) {
		MyLog.d(TAG, "Connected ...");
		LocationRequest request = LocationRequest.create()
				.setInterval(interval)
				.setFastestInterval(fastInterval)
				.setPriority(priority);
		LocationServices.FusedLocationApi.requestLocationUpdates(
				mLocationClient, request, this);
		MyLog.d("LocationService onConnected", "radius: " + radius + " interval: " + interval + " fastInterval: " + fastInterval + " priority: " + priority);
	}

//	@Override
//	public void onDisconnected() {
//		MyLog.d(TAG, "Disconnected..");
//		if (isManualDisconnect) {
//			MyLog.d(TAG, "Manual Disconnected..");
//			isManualDisconnect = false;
//		} else{
//			MyLog.d(TAG, "System Disconnected..");
//			stopSelf();
//		}
//	}

	@Override
	public void onConnectionSuspended(int i) {
		MyLog.d(TAG, "Disconnected..");
		if (isManualDisconnect) {
			MyLog.d(TAG, "Manual Disconnected..");
			isManualDisconnect = false;
		} else{
			MyLog.d(TAG, "System Disconnected..");
			stopSelf();
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void onLocationChanged(Location location) {
		MyLog.d("LocationService onLocationChanged", "radius: " + radius + " interval: " + interval + " fastInterval: " + fastInterval + " priority: " + priority);
		if (location != null) {
			String log = "Provider :  "
					+ location.getProvider()
					+ "  -  [lat,lng]: (" + location.getLatitude() + ","
					+ location.getLongitude() + ")" + "  -  Accuracy :  "
					+ location.getAccuracy() + " Radius : " + radius;
			//sai so qua cho phep, ghi log
			if (location.getAccuracy() > radius) {
				if (Math.abs(System.currentTimeMillis() - this.timeSendLogFusedLocating) >= TIME_SEND_LOG_LOCATING && GlobalUtil.checkNetworkAccess()) {
					this.timeSendLogFusedLocating = System.currentTimeMillis();
					MyLog.d("LocationService onLocationChanged FAIL", log);
					ServerLogger.sendLog("Locating", log, false, TabletActionLogDTO.LOG_CLIENT);
				}
			} else{
				Bundle bd = new Bundle();
				bd.putParcelable(IntentConstants.INTENT_DATA, location);
				sendBroadcast(ActionEventConstant.ACTION_UPDATE_POSITION_SERVICE, bd);
				MyLog.d("LocationService onLocationChanged PASS", log);
			}
		}
	}

	@Override
	public void onDestroy() {
		MyLog.d(TAG, "Destroying..");
		try {
			LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this);
			isManualDisconnect = true;
			mLocationClient.disconnect();
		} catch (Exception e) {
			MyLog.e(TAG, MyLog.printStackTrace(e));
		}
	}


	/**
	 * Send broadcast
	 * @author: banghn
	 * @param action
	 * @param bundle
	 */
	public void sendBroadcast(int action, Bundle bundle) {
		Intent intent = new Intent(GlobalBaseActivity.VNM_ACTION);
		bundle.putInt(Constants.ACTION_BROADCAST, action);
		bundle.putInt(Constants.HASHCODE_BROADCAST, intent.getClass()
				.hashCode());
		intent.putExtras(bundle);
		sendBroadcast(intent, GlobalBaseActivity.BROACAST_PERMISSION);
	}
}
