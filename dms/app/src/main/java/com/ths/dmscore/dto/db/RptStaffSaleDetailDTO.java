package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.RPT_STAFF_SALE_DETAIL_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;


@SuppressWarnings("serial")
public class RptStaffSaleDetailDTO extends AbstractTableDTO{
	public long id;
	public long staffId;
	public long customerId;
	public long shopId;
	public String saleDate;
	public double amountPlan;
	public double amount;
	public double amountApproved;
	public long quantityPlan;
	public long quantity;
	public long quantityApproved;
	//public double score;
	public int isNew;
	public int isOn;
	public int isOr;
	public String createDate;
	public String updateDate;
	public long parentStaffId;
	public double WWAmountPlan;
	public int isKA;
	
	public static final int STAFF = 1;
	public static final int ROUTED = 2;
	public static final int SHOP = 3;
	
	public RptStaffSaleDetailDTO() {
		super(TableType.RPT_STAFF_SALE_DETAIL);
	}
	
	/**
	 * 
	 * khoi tao thong tin doi tuong chi tiet doanh so ngay
	 * 
	 * @author: DungNT19
	 * @param 
	 * @return
	 * @return: void
	 * @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		isOr = CursorUtil.getInt(c, RPT_STAFF_SALE_DETAIL_TABLE.IS_OR);
		//amount
		amountPlan = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_AMOUNT_PLAN);
		amount = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_AMOUNT);
		amountApproved = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_AMOUNT_APPROVED);
		//quantity
		quantityPlan = CursorUtil.getLong(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_QUANTITY_PLAN);
		quantity = CursorUtil.getLong(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_QUANTITY);
		quantityApproved = CursorUtil.getLong(c, RPT_STAFF_SALE_DETAIL_TABLE.DAY_QUANTITY_APPROVED);
		//score = CursorUtil.getDouble(c, RPT_STAFF_SALE_DETAIL_TABLE.SCORE, 1, GlobalInfo.getInstance().getSysNumRounding());
		customerId = CursorUtil.getLong(c, RPT_STAFF_SALE_DETAIL_TABLE.CUSTOMER_ID);
	}

	/**
	 * sum items
	 * @author: duongdt3
	 * @since: 15:38:03 20 Apr 2015
	 * @return: void
	 * @throws:  
	 * @param saleDetailDTO
	 */
	public void sumItem(RptStaffSaleDetailDTO saleDetailDTO) {
		//amount
		this.amount += saleDetailDTO.amount;
		this.amountApproved += saleDetailDTO.amountApproved;
		this.amountPlan += saleDetailDTO.amountPlan;
		//quantity
		this.quantity += saleDetailDTO.quantity;
		this.quantityApproved += saleDetailDTO.quantityApproved;
		this.quantityPlan += saleDetailDTO.quantityPlan;
		
		
	}

	/**
	 * sum process
	 * @author: duongdt3
	 * @since: 15:40:06 20 Apr 2015
	 * @return: void
	 * @throws:  
	 */
	public void sumProcess() {
		//amount
		this.amount = StringUtil.round(this.amount);
		this.amountApproved = StringUtil.round(this.amountApproved);
		this.amountPlan = StringUtil.round(this.amountPlan);
	}
}
