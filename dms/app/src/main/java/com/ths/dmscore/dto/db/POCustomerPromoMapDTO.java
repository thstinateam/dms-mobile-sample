package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_MAP_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Luu thong tin san pham mua dat KM hay ko dat
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class POCustomerPromoMapDTO extends AbstractTableDTO {
	// noi dung field
	private static final long serialVersionUID = 1L;
	public long poCustomerPromoMapId; // id bang
	public long poCustomerDetailId; // id cua saleoder detail
	public String programCode; // ma CTKM
	public int status; // trang thai dat hay ko dat CTKM
	public static final int TYPE_ACHIVE = 5;// dat
	public static final int TYPE_NOT_ACHIVE = 6;// ko dat KM
	public long staffId; // id nhan vien
	public long poCustomerId; // id don hang


	 /**
	 * Cap nhat du lieu
	 * @author: Tuanlt11
	 * @param saleOrderDetailId
	 * @param promotion
	 * @return: void
	 * @throws:
	*/
	public void updateData(OrderDetailViewDTO promotion){
		programCode = promotion.orderDetailDTO.programeCode;
	}

	/**
	 * Xoa cac KM dat hoac ko dat lien quan toi PO customer
	 * @author: Tuanlt11
	 * @param poCustomerId
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateDeletePromoMapByPOSql(long poCustomerId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_PROMO_MAP_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_PROMO_MAP_TABLE.PO_CUSTOMER_ID, String.valueOf(poCustomerId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}
}
