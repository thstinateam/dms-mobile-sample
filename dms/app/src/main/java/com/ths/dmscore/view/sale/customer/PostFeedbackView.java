package com.ths.dmscore.view.sale.customer;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.TBHVController;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.NoDefaultSpinner;
import com.ths.dmscore.view.control.filechooser.FileAttachAdapter;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dmscore.dto.view.CustomerListFeedbackDTO;
import com.ths.dmscore.dto.view.filechooser.FileAttachEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Tao gop y
 *
 * @author : TamPQ since : 9:48:52 AM version :
 */
public class PostFeedbackView extends BaseFragment implements OnClickListener,
		VinamilkTableListener, OnTouchListener, OnDateSetListener, FileAttachEvent {
	// parent
	private SalesPersonActivity parent;
	// btFeedBack
	Button btFeedBack;
	// etDate
	VNMEditTextClearable etDate;
	// etDate
	VNMEditTextClearable etCusCode;
	// spinnerType
	NoDefaultSpinner spinnerType;
	// etContent
	EditText etContent;
	// mDay
	private int mDay;
	// mMonth
	private int mMonth;
	// mYear
	private int mYear;
	// remindDateTime
	private String remindDateTime = "";
	// firstSelect
	boolean firstSelect = true;
	// adapterLine
	SpinnerAdapter adapterLine;

	public static final int FROM_NOTE_LIST_VIEW = 0;
	public static final int FROM_FEEDBACK_LIST_VIEW = 1;
	public static final int FROM_GSNPP_POST_FEEDBACK_VIEW = 2;

	private static final int ACTION_GET_CUSTOMER_LIST = 3;
	private static final int ACTION_CHOOSE_CUSTOMER = 4;
	// Search customer
	// customer code for search
	private String customerCodeSearch = "";
	// customer name for search
	private String customerNameSearch = "";
	// get total page or not
	boolean isGetTotalPage;
	// Thong tin KH tu MH Huan luyen ngay TBHV
	CustomerDTO customer;
	// tu man hinh NoteListView hay FeedbackListView qua
	private int from;
	int page;
	// dialog product detail view
	AlertDialog customerListPopup;
	// MH chi tiet van de
	//TBHVCustomerListView followProblemDetail;
	// Ds Kh hien thi
	CustomerListFeedbackDTO customerListDto = new CustomerListFeedbackDTO();
	private ArrayList<ApParamDTO> typeList;
	private ListView lvFile;
	private Button btAddFile;

	List<FileInfo> fileListAttach;
    private FileAttachAdapter fileAttachAdapter;
    
	public static PostFeedbackView newInstance(Bundle b) {
		PostFeedbackView f = new PostFeedbackView();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		parent = (SalesPersonActivity) getActivity();
		customer = (CustomerDTO) getArguments().getSerializable(
				IntentConstants.INTENT_CUSTOMER);
		from = getArguments().getInt(IntentConstants.INTENT_FROM);

		super.onAttach(activity);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_post_feed_back, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		if (from == PostFeedbackView.FROM_NOTE_LIST_VIEW) {
			parent.setTitleName(StringUtil
					.getString(R.string.TITLE_VIEW_POST_FEEDBACK_2));
		} else {
			parent.setTitleName(StringUtil
					.getString(R.string.TITLE_VIEW_POST_FEEDBACK));
		}
		// init
		btFeedBack = (Button) v.findViewById(R.id.btFeedBack);
		btFeedBack.setOnClickListener(this);
		remindDateTime = DateUtils.now();
		etContent = (EditText) v.findViewById(R.id.etContent);
		if (from == FROM_NOTE_LIST_VIEW) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_GIAOVANDE);
			PriUtils.getInstance().findViewById(v, R.id.tvCusCode, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_KHACHHANG);
			PriUtils.getInstance().findViewById(v, R.id.tvTypeCus, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_LOAIVANDE);
			PriUtils.getInstance().findViewById(v, R.id.tvTH, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_NGAYNHACNHO);
			etDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.etDate, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_NGAYNHACNHO, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			spinnerType = (NoDefaultSpinner) PriUtils.getInstance().findViewById(v, R.id.spinnerType, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_LOAIVANDE);
			etCusCode = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.etCusCode, PriHashMap.PriControl.NVBH_THONGKECHUNG_CANTHUCHIEN_THEMGHICHU_KHACHHANG);
			PriUtils.getInstance().setOnTouchListener(etCusCode, this);
			etCusCode.setIsHandleDefault(false);
		} else {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_GIAOVANDE_THEMMOIVANDE);
			PriUtils.getInstance().findViewById(v, R.id.tvCusCode, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_KHACHHANG);
			PriUtils.getInstance().findViewById(v, R.id.tvTypeCus, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_LOAIVANDE);
			PriUtils.getInstance().findViewById(v, R.id.tvTH, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_NGAYNHACNHO);
			etDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.etDate, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_NGAYNHACNHO, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
			spinnerType = (NoDefaultSpinner) PriUtils.getInstance().findViewById(v, R.id.spinnerType, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_LOAIVANDE);
			etCusCode = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.etCusCode, PriHashMap.PriControl.NVBH_BANHANG_GOPY_THEMGOPY_KHACHHANG);
			etCusCode.setImageClearVisibile(false);
			PriUtils.getInstance().setStatus(etCusCode, PriUtils.DISABLE);
			GlobalUtil.setEnableEditTextClear(etCusCode, false);
			etCusCode.setPadding(GlobalUtil.dip2Pixel(5), 0,
					GlobalUtil.dip2Pixel(5), 0);
		}
		etDate.setOnTouchListener(this);
		etDate.setIsHandleDefault(false);
		etDate.setText(DateUtils.getCurrentDate());
		customer = (CustomerDTO) getArguments().getSerializable(
				IntentConstants.INTENT_CUSTOMER);
		if (customer != null) {
			etCusCode.setText(customer.getCustomerCode()
					+ " - " + customer.getCustomerName());
		}

		getTypeFeedback();

		lvFile = (ListView) v.findViewById(R.id.lvFile);
		btAddFile = (Button) v.findViewById(R.id.btAddFile);
		//load list attach file
        fileListAttach = new ArrayList<FileInfo>();
        fileAttachAdapter = new FileAttachAdapter(parent, fileListAttach, this, true);
        lvFile.setAdapter(fileAttachAdapter);
        loadFilesAttach();
        
		return v;
	}

	/**
	 * getTypeFeedback
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void getTypeFeedback() {
		Bundle b = new Bundle();
		b.putInt(IntentConstants.INTENT_FROM, from);
		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.GET_TYPE_FEEDBACK;
		e.viewData = b;
		e.sender = this;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 *
	 * Hien thi pop up danh sach khach hang
	 *
	 * @author: Nguyen Huu Hieu
	 * @return: void
	 * @throws:
	 */
	private void showCustomerList() {
		if (customerListPopup == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
//			followProblemDetail = new TBHVCustomerListView(parent, this,
//					ACTION_GET_CUSTOMER_LIST, ACTION_CHOOSE_CUSTOMER, 0);
//			build.setView(followProblemDetail.viewLayout);
			customerListPopup = build.create();
			customerListPopup.setCanceledOnTouchOutside(false);
			Window window = customerListPopup.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		customerListPopup.show();
	}

	/**
	 *
	 * get danh sach khach hang
	 *
	 * @author: HieuNH
	 * @param : page
	 * @param : isGetTotolPage
	 * @return: void
	 * @throws:
	 */
	private void getCustomerList(int page, boolean getTotalPage) {
		parent.showLoadingDialog();
		ActionEvent e = new ActionEvent();
		Bundle data = new Bundle();

		data.putInt(IntentConstants.INTENT_PAGE, page);
		data.putString(IntentConstants.INTENT_STAFF_ID, ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		data.putString(IntentConstants.INTENT_SHOP_ID, ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, getTotalPage);
		data.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCodeSearch);
		data.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerNameSearch);
		customerListDto.codeCus = customerCodeSearch;
		customerListDto.nameCus = customerNameSearch;

		e.viewData = data;
		e.action = ActionEventConstant.GET_CUSTOMER_LIST_FOR_POST_FEEDBACK;
		e.sender = this;
		TBHVController.getInstance().handleViewEvent(e);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_CUSTOMER_LIST_FOR_POST_FEEDBACK: {
			CustomerListFeedbackDTO temp = (CustomerListFeedbackDTO) modelEvent
					.getModelData();
//			if (page == 1 && isGetTotalPage) {
//				customerListDto.totalCustomer = temp.totalCustomer;
//				followProblemDetail.setTotalSize(customerListDto.totalCustomer);
//			}
//			customerListDto.listCustomer.clear();
//			customerListDto.listCustomer.addAll(temp.listCustomer);
//			if (followProblemDetail != null) {
//				if (StringUtil.isNullOrEmpty(etCusCode.getText().toString())) {
//					customer = null;
//				}
//				followProblemDetail.renderLayout(customerListDto, customer);
//			}
			break;
		}
		case ActionEventConstant.GET_TYPE_FEEDBACK: {
			typeList = (ArrayList<ApParamDTO>) modelEvent.getModelData();
			String[] arrLineChoose = new String[typeList.size()];
			for (int i = 0; i < typeList.size(); i++) {
				arrLineChoose[i] = typeList.get(i).getApParamName();
			}
			adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrLineChoose);
			spinnerType.setAdapter(adapterLine);
			spinnerType.setSelection(0);
			break;
		}
		case ActionEventConstant.POST_FEEDBACK:
			if (from == FROM_NOTE_LIST_VIEW) {
				ActionEvent ev = new ActionEvent();
				ev.sender = this;
				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
						customer);
				ev.viewData = bundle;
				ev.action = ActionEventConstant.NOTE_LIST_VIEW;
				SaleController.getInstance().handleSwitchFragment(ev);
			} else {
				ActionEvent ev = new ActionEvent();
				ev.sender = this;
				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
						customer);
				ev.viewData = bundle;
				ev.action = ActionEventConstant.GET_LIST_CUS_FEED_BACK;
				SaleController.getInstance().handleSwitchFragment(ev);
			}
			break;
		default:
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 * updateDisplay
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public void updateDate(int day, int month, int year) {
		this.mDay = day;
		this.mMonth = month;
		this.mYear = year;

		String sDay = String.valueOf(mDay);
		String sMonth = String.valueOf(mMonth + 1);
		if (mDay < 10) {
			sDay = "0" + sDay;
		}
		if (mMonth + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		etDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
		etDate.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(sDay).append("/").append(sMonth).append("/")
				.append(mYear).append(" "));

		remindDateTime = mYear + "-" + sMonth + "-" + sDay + " " + "00:00:01";
	}

	/**
	 * postFeedBack
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void postFeedBack() {
		if (StringUtil.isNullOrEmpty(etContent.getText().toString().trim())) {
			parent.showDialog(StringUtil.getString(R.string.TEXT_INPUT_CONTENT));
		} else {
			if (spinnerType.getSelectedItemPosition() == -1) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_CHOOSE_FEEDBACK_TYPE));
			} else if (DateUtils.compareDate(remindDateTime, DateUtils.now()) == -1) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_BEFORE_NOW));
			} else if (StringUtil.isNullOrEmpty(etDate.getText().toString()
					.trim())) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_CHOOSE_DAY));
			} else {
				parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
				btFeedBack.setEnabled(false);
				FeedBackDTO itemdto = new FeedBackDTO();
				itemdto.setRemindDate(remindDateTime);
				itemdto.setContent(etContent.getText().toString().trim());
				itemdto.setCreateDate(DateUtils.now());
				itemdto.setStaffId(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId());
				if (customer != null && !StringUtil.isNullOrEmpty(etCusCode.getText().toString())) {
					itemdto.setCustomerId(customer.getCustomerId());
				}
				itemdto.setStatus(FeedBackDTO.FEEDBACK_STATUS_CREATE);
				itemdto.setFeedbackType(Integer.parseInt(typeList.get(spinnerType
						.getSelectedItemPosition()).getApParamCode()));
				itemdto.setIsDeleted(0);
				itemdto.setCreateUserId(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId());
				itemdto.setShopId(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId());

				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentConstants.INTENT_FEEDBACK_DTO,
						itemdto);

				ActionEvent e = new ActionEvent();
				e.sender = this;
				e.action = ActionEventConstant.POST_FEEDBACK;
				e.viewData = bundle;
				e.isNeedCheckTimeServer = false;
				SaleController.getInstance().handleViewEvent(e);
			}

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btFeedBack:
			GlobalUtil.forceHideKeyboard(parent);
			postFeedBack();
			break;
		default:
			break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_GET_CUSTOMER_LIST: {
			Bundle bundle = (Bundle) data;
			customerCodeSearch = bundle
					.getString(IntentConstants.INTENT_CUSTOMER_CODE);
			customerNameSearch = bundle
					.getString(IntentConstants.INTENT_CUSTOMER_NAME);
			page = bundle.getInt(IntentConstants.INTENT_PAGE);
			isGetTotalPage = bundle
					.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
			getCustomerList(page, isGetTotalPage);
			break;
		}
		case ACTION_CHOOSE_CUSTOMER: {
			customer = (CustomerDTO) data;
			etCusCode.setText(customer.customerCode + " - "
					+ customer.customerName);
			customerListPopup.dismiss();
			break;
		}
		default:
			break;
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		if (v == etDate) {
			if (!etDate.onTouchEvent(arg1)) {
				parent.showDatePickerDialog(etDate.getText().toString(), true, this);
			}
		}
		if (v == etCusCode) {
			if (!etCusCode.onTouchEvent(arg1)) {
				showCustomerList();
				page = 1;
				customerCodeSearch = "";
				customerNameSearch = "";
				isGetTotalPage = true;
				getCustomerList(page, isGetTotalPage);
			}
		}
		return false;
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				etDate.setText(DateUtils.getCurrentDate());
				if (etCusCode.isEnabled()) {
					etCusCode.setText(Constants.STR_BLANK);
				}
				etContent.setText(Constants.STR_BLANK);
				if (spinnerType != null && typeList.size() > 0
						&& adapterLine != null) {
					spinnerType.setAdapter(adapterLine);
					spinnerType.setSelection(0);
				}
				getTypeFeedback();
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}

	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}

	/* (non-Javadoc)
	 * @see VinamilkTableListener#handleVinamilkTableloadMore(android.view.View, java.lang.Object)
	 */
	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see VinamilkTableListener#handleVinamilkTableRowEvent(int, android.view.View, java.lang.Object)
	 */
	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub

	}
	
	private void loadFilesAttach() {
        fileListAttach.clear();
        File path = Environment.getRootDirectory();
        if (path.exists()) {
            File[] dirs = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return (file.isDirectory() && file.canRead());
                }
            });
            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    if (!file.isDirectory() && file.canRead()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            // convert to an array
            if (dirs != null) {
                Arrays.sort(dirs);
                for (File dir : dirs) {
                    fileListAttach.add(new FileInfo(dir, FileInfo.TYPE_FOLDER));
                }
            }
            if (files != null) {
                Arrays.sort(files);
                for (File file : files) {
                    fileListAttach.add(new FileInfo(file, FileInfo.TYPE_FILE));
                }
            }
        }
        fileAttachAdapter.notifyDataSetChanged();
    }

	@Override
	public void onRemoveAttach(int posRemove) {
		fileListAttach.remove(posRemove);
        fileAttachAdapter.notifyDataSetChanged();
	}

	@Override
	public void onOpenFile(FileInfo info) {
		
	}
}
