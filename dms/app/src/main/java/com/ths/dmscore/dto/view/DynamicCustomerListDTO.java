/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO man hinh danh sach khach hang cho bao cao dong
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  15:32:39 11-02-2015
 */
public class DynamicCustomerListDTO {
	//ma khach hang
	public String codeCus;
	//ten hoac dia chi khach hang
	public String nameCus;

	public List<DynamicCustomerDTO> listCustomerInPage; 
	public List<DynamicCustomerDTO> listCustomerAll; 
	public int totalCustomer;
	public boolean isCheckAll;

	public DynamicCustomerListDTO() {
		listCustomerInPage = new ArrayList<DynamicCustomerDTO>();
		listCustomerAll = new ArrayList<DynamicCustomerDTO>();
	}
}
