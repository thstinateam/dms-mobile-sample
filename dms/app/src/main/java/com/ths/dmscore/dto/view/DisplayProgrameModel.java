/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.DisplayProgrameDTO;

/**
 * Model du lieu cho trang chuong trinh khuyen mai
 * 
 * @author: SoaN
 * @version: 1.0
 * @since: Jun 19, 2012
 */

public class DisplayProgrameModel implements Serializable {
	private static final long serialVersionUID = -6104519373896455481L;
	ArrayList<DisplayProgrameDTO> modelData;
	ComboboxDisplayProgrameDTO comboboxDTO;

	/**
	 * @return the comboboxDTO
	 */
	public ComboboxDisplayProgrameDTO getComboboxDTO() {
		return comboboxDTO;
	}

	/**
	 * @param comboboxDTO
	 *            the comboboxDTO to set
	 */
	public void setComboboxDTO(ComboboxDisplayProgrameDTO comboboxDTO) {
		this.comboboxDTO = comboboxDTO;
	}

	int total;

	/**
	 * @return the modelData
	 */
	public ArrayList<DisplayProgrameDTO> getModelData() {
		return modelData;
	}

	/**
	 * @param modelData
	 *            the modelData to set
	 */
	public void setModelData(ArrayList<DisplayProgrameDTO> modelData) {
		this.modelData = modelData;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

}
