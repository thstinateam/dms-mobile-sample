package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.CustomerCanLoseItemDto;
import com.ths.dmscore.dto.view.SaleInMonthItemDto;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.StaffInfoDTO;

public class RPT_SALE_HISTORY extends ABSTRACT_TABLE {
	public static final String ID = "ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String MONTH = "MONTH";
	public static final String NUM_CUSTOMER = "NUM_CUSTOMER";
	public static final String NUM_CUST_NOT_ORDER = "NUM_CUST_NOT_ORDER";
	public static final String AMOUNT = "AMOUNT";
	public static final String AMOUNT_APPROVED = "AMOUNT_APPROVED";
	public static final String AMOUNT_PENDING = "AMOUNT_PENDING";
	public static final String QUANTITY = "QUANTITY";
	public static final String QUANTITY_PENDING = "QUANTITY_PENDING";
	public static final String QUANTITY_APPROVED = "QUANTITY_APPROVED";
	public static final String NUM_SKU = "NUM_SKU";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String SHOP_ID = "SHOP_ID";

	public static final String RPT_SALE_HISTORY = "RPT_SALE_HISTORY";

	public RPT_SALE_HISTORY(SQLiteDatabase mDB) {
		this.tableName = RPT_SALE_HISTORY;
		this.columns = new String[] { ID, STAFF_ID, MONTH, NUM_CUSTOMER, NUM_CUST_NOT_ORDER, AMOUNT,AMOUNT_PENDING,AMOUNT_APPROVED,
				NUM_SKU, CREATE_DATE, SHOP_ID, QUANTITY,QUANTITY_APPROVED,QUANTITY_PENDING, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	public StaffInfoDTO getStaffInformation(String staffId, int sysSaleRoute, int page, int isGetTotalPage, int isLoadSaleInMonth, String shopId) throws Exception {
		StaffInfoDTO dto = new StaffInfoDTO();
		String date_now = DateUtils.now();
//		String shopId = GlobalInfo.getInstance().getProfile().getUserData().inheritShopId;
		String startOfOnePreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -1);
		String startOfThreePreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -3);

		StringBuffer var = new StringBuffer();
		var.append("SELECT RSH.rpt_sale_history_id   	AS ID, ");
		var.append("       cy.NUM 						AS MONTH, ");
		var.append("       RSH.month 				 	AS MONTH_2, ");
		var.append("       RSH.num_customer          	AS NUM_CUSTOMER, ");
		var.append("       RSH.num_cust_not_order		AS NUM_CUST_NOT_ORDER, ");
		var.append("       RSH.AMOUNT  					AS AMOUNT, ");
		var.append("       RSH.AMOUNT_APPROVED  		AS AMOUNT_APPROVED, ");
		var.append("       RSH.AMOUNT_PENDING  			AS AMOUNT_PENDING, ");
		var.append("       RSH.QUANTITY_APPROVED  		AS QUANTITY_APPROVED, ");
		var.append("       RSH.QUANTITY_PENDING  		AS QUANTITY_PENDING, ");
		var.append("       RSH.QUANTITY  				AS QUANTITY, ");
//		var.append("       RSH.num_sku               AS NUM_SKU, ");
		var.append("       RSH.create_date           	AS CREATE_DATE ");
		var.append("FROM   rpt_sale_history RSH, cycle cy ");
		var.append("WHERE  RSH.OBJECT_ID = ? ");
//		var.append("		and RSH.OBJECT_ID = staff.staff_id ");
//		var.append("		and staff.status = 1 ");
		var.append("		and RSH.shop_id = ? ");
//		var.append("       AND Date(month) <= Date(?,'start of month', '-1 MONTH') ");
//		var.append("       AND Date(month) >= Date(?,'start of month', '-3 MONTH') ");
		var.append("       AND Date(month) <= Date(?) ");
		var.append("       AND Date(month) >= Date(?) ");
		var.append("	   and RSH.OBJECT_TYPE = ? ");
		var.append("	   AND RSH.CYCLE_ID = cy.CYCLE_ID ");
		var.append("ORDER  BY MONTH_2 ASC ");

		String params[] = { staffId, shopId, startOfOnePreviousMonth, startOfThreePreviousMonth, "" + sysSaleRoute };

		Cursor curList = null;
		try {
			curList = rawQuery(var.toString(), params);

			if (curList.moveToFirst()) {
				do {
					SaleInMonthItemDto item = new SaleInMonthItemDto();
					item.initDataFromCursor(curList);
					dto.arrSaleProgress.add(item);
				} while (curList.moveToNext());
			}
		} finally {
			try {
				if (curList != null) {
					curList.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		ArrayList<String> param1 = new ArrayList<String>();
		ArrayList<String> param2 = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT CUS.*, ");
		var1.append("       Strftime('%d/%m/%Y', STC.LAST_ORDER) LAST_ORDER, ");
		var1.append("       Strftime('%d/%m/%Y', STC.LAST_APPROVE_ORDER) AS LAST_APPROVE_ORDER ");
		var1.append("FROM   (SELECT VP.STAFF_ID, ");
		var1.append("               CT.CUSTOMER_ID, ");
		var1.append("               CT.short_code AS CUSTOMER_CODE, ");
		var1.append("               CT.CUSTOMER_NAME, ");
		var1.append("               CT.ADDRESS 					AS ADDRESS, ");
		var1.append("               ( CT.HOUSENUMBER ");
		var1.append("                 || ' ' ");
		var1.append("                 || CT.STREET )               AS STREET ");
		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               ROUTING_CUSTOMER RTC, ");
		var1.append("               CUSTOMER CT ");
		var1.append("        WHERE  substr(VP.FROM_DATE, 1, 10) <= substr(?, 1, 10) ");
		param1.add(date_now);
		param2.add(date_now);
		var1.append("               AND ( VP.TO_DATE IS NULL ");
		var1.append("                      OR DATE(VP.TO_DATE) >= DATE(?) ) ");
		param1.add(date_now);
		param2.add(date_now);
		var1.append("        		AND  substr(RTC.START_DATE, 1, 10) <= substr(?, 1, 10) ");
		param1.add(date_now);
		param2.add(date_now);
		var1.append("               AND ( RTC.END_DATE IS NULL ");
		var1.append("                      OR DATE(RTC.END_DATE) >= DATE(?) ) ");
		param1.add(date_now);
		param2.add(date_now);
		var1.append("               AND VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RT.ROUTING_ID = RTC.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		
		if (sysSaleRoute == ApParamDTO.SYS_SALE_ROUTE) {
			var1.append("               AND VP.ROUTING_ID = ? ");
			param1.add(staffId);
			param2.add(staffId);
		} else if (sysSaleRoute == ApParamDTO.SYS_SALE_STAFF) {
			var1.append("               AND VP.STAFF_ID = ? ");
			param1.add(staffId);
			param2.add(staffId);
		} else if (sysSaleRoute == ApParamDTO.SYS_SALE_SHOP) {
		}
		var1.append("               AND VP.SHOP_ID = ? ");
		param1.add(shopId);
		param2.add(shopId);
		var1.append("               AND VP.STATUS = 1 ");
		var1.append("               AND RTC.STATUS = 1 ");
		var1.append("               AND CT.STATUS = 1 ");
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("        GROUP  BY CT.CUSTOMER_ID) CUS ");
		var1.append("       LEFT JOIN STAFF_CUSTOMER STC ");
		var1.append("              ON ( CUS.STAFF_ID = STC.STAFF_ID ");
		var1.append("                   AND CUS.CUSTOMER_ID = STC.CUSTOMER_ID AND STC.SHOP_ID = ?) ");
		param1.add(shopId);
		param2.add(shopId);
		var1.append("WHERE  ( STC.LAST_APPROVE_ORDER IS NULL ");
		var1.append("          OR DATE(STC.LAST_APPROVE_ORDER) < DATE(?) ) ");
		param1.add(startOfOnePreviousMonth);
		param2.add(startOfOnePreviousMonth);
		var1.append("       AND ( STC.LAST_ORDER IS NULL ");
		var1.append("              OR DATE(STC.LAST_ORDER) < DATE(?) ) ");
		param1.add(date_now);
		param2.add(date_now);

		// get count
		StringBuilder getTotalCusCanLose = new StringBuilder();
		getTotalCusCanLose.append("select count(*) as TOTAL_ROW from (" + var1.toString() + ")");

		var1.append(" limit ? offset ?");
		param1.add("" + Constants.NUM_ITEM_PER_PAGE);
		param1.add("" + (page - 1) * Constants.NUM_ITEM_PER_PAGE);

		Cursor curCusCanLose = null;
		Cursor curTotalCusCanLose = null;

		try {
			curCusCanLose = rawQueries(var1.toString(), param1);

			if (curCusCanLose.moveToFirst()) {
				do {
					CustomerCanLoseItemDto item = new CustomerCanLoseItemDto();
					item.initDataFromCursor(curCusCanLose);
					dto.arrCusCanLose.add(item);
				} while (curCusCanLose.moveToNext());
			}

			curTotalCusCanLose = rawQueries(getTotalCusCanLose.toString(), param2);
			if (curTotalCusCanLose != null) {
				if (curTotalCusCanLose.moveToFirst()) {
					dto.totalCusCanLose = curTotalCusCanLose.getInt(0);
				}
			}
		}finally {
			try {
				if (curCusCanLose != null) {
					curCusCanLose.close();
				}
				if (curTotalCusCanLose != null) {
					curTotalCusCanLose.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		return dto;
	}

}
