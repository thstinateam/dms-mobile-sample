/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.view.ProgrameForProductDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

/**
 * hien thi thong tin mot chuong trinh di kem voi san pham
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class SelectProgrameForProduct extends DMSTableRow implements
		OnClickListener {
	// STT
	TextView tvSTT;
	// code
	TextView tvProgrameCode;

	// Name
	TextView tvProgrameName;

	// programe type
	TextView tvProgrameType;

	// table parent
	private View tableParent;
	// data to render layout for row
	ProgrameForProductDTO myData;

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 */
	public SelectProgrameForProduct(Context context, View aRow) {
		super(context, R.layout.layout_select_programe_for_product_row,0.7);
		tableParent = aRow;
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProgrameCode = (TextView) findViewById(R.id.tvProgrameCode);
		tvProgrameName = (TextView) findViewById(R.id.tvProgrameName);
		tvProgrameType = (TextView) findViewById(R.id.tvProgrameType);

		myData = new ProgrameForProductDTO();
	}

	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, ProgrameForProductDTO item) {
		this.myData = item;
		tvSTT.setText(String.valueOf(position));
		tvProgrameCode.setText(item.programeCode);
		tvProgrameName.setText(item.programeName);
		tvProgrameType.setText(item.programeTypeName);
		if (StringUtil.isNullOrEmpty(item.programeTypeName)) {
			if (item.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION) {
				tvProgrameType.setText(StringUtil
						.getString(R.string.TEXT_HEADER_MENU_CTTB));
			} else {
				tvProgrameType.setText(StringUtil
						.getString(R.string.TEXT_NAME_PROMOTION_ZM));
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == this && listener != null) {
			listener.handleVinamilkTableRowEvent(
					ActionEventConstant.ACTION_SELECT_PROGRAME, tableParent,
					this.myData);
		}
	}

	/**
	 *
	 * set lai background cho dong khi dong duoc chon
	 *
	 * @author: DungNX3
	 * @return: void
	 * @throws:
	 */
	public void renderLayoutSelectedRow(){
		int color= ImageUtil.getColor(R.color.WHITE);
		tvSTT.setBackgroundColor(color);
		tvProgrameCode.setBackgroundColor(color);
		tvProgrameName.setBackgroundColor(color);
		tvProgrameType.setBackgroundColor(color);
	}

	/**
	 * reset lai mau cho dong
	 *
	 * @author: DungNX3
	 * @return: void
	 * @throws:
	 */
	public void resetLayout(){
		tvSTT.setBackgroundColor(Color.WHITE);
		tvProgrameCode.setBackgroundColor(Color.WHITE);
		tvProgrameName.setBackgroundColor(Color.WHITE);
		tvProgrameType.setBackgroundColor(Color.WHITE);
	}
}
