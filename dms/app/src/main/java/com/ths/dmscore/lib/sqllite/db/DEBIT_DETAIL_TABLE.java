/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.DebitDetailDTO;
import com.ths.dmscore.dto.view.CustomerDebitDetailDTO;
import com.ths.dmscore.dto.view.CustomerDebitDetailItem;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.view.CusDebitDetailDTO.CusDebitDetailItem;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * Bang chi tiet cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class DEBIT_DETAIL_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String DEBIT_ID = "DEBIT_ID";
	public static final String DEBIT_DETAIL_ID = "DEBIT_DETAIL_ID";
	//
	public static final String FROM_OBJECT_ID = "FROM_OBJECT_ID";
	//
	public static final String AMOUNT = "AMOUNT";
	//
	public static final String DISCOUNT = "DISCOUNT";
	//
	public static final String TOTAL = "TOTAL";
	//
	public static final String TOTAL_PAY = "TOTAL_PAY";
	//
	public static final String REMAIN = "REMAIN";
	//
	public static final String FROM_OBJECT_NUMBER = "FROM_OBJECT_NUMBER";
	//
	public static final String INVOICE_NUMBER = "INVOICE_NUMBER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	//
	public static final String TABLE_NAME = "DEBIT_DETAIL";
	
	public static final String DEBIT_DATE = "DEBIT_DATE";

	public static final String SHOP_ID = "SHOP_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	
	public static final String TYPE = "TYPE";
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";

	// ID nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	
	public DEBIT_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { DEBIT_ID, DEBIT_DETAIL_ID,
				FROM_OBJECT_ID, AMOUNT, DISCOUNT, TOTAL, TOTAL_PAY, REMAIN,
				FROM_OBJECT_NUMBER, INVOICE_NUMBER, DEBIT_DATE, TYPE,
				CREATE_DATE, DISCOUNT_AMOUNT, UPDATE_DATE, CREATE_USER,
				UPDATE_USER, SHOP_ID, CUSTOMER_ID , STAFF_ID, SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((DebitDetailDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(DebitDetailDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		DebitDetailDTO disDTO = (DebitDetailDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.debitID };
		return update(value, DEBIT_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * 
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(DEBIT_ID + " = ?", params);
	}

	public int deleteByDebitDetailID(String debitDetailID) {
		String[] params = { debitDetailID };
		return delete(DEBIT_DETAIL_ID + " = ?", params);
	}
	
	public long delete(AbstractTableDTO dto) {
		DebitDTO paramDTO = (DebitDTO) dto;
		String[] params = { String.valueOf(paramDTO.id) };
		return delete(DEBIT_ID + " = ?", params);
	}

	private ContentValues initDataRow(DebitDetailDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(DEBIT_ID, dto.debitID);
		editedValues.put(DEBIT_DETAIL_ID, dto.debitDetailID);
		editedValues.put(FROM_OBJECT_ID, dto.fromObjectID);
		editedValues.put(FROM_OBJECT_NUMBER, dto.fromObjectNumber);
		editedValues.put(AMOUNT, dto.amount);
		editedValues.put(DISCOUNT, dto.discount);
		editedValues.put(TOTAL, dto.total);
		editedValues.put(TOTAL_PAY, dto.totalPay);
		editedValues.put(REMAIN, dto.remain);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(DEBIT_DATE, dto.debitDate);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(TYPE, dto.type);
		editedValues.put(STAFF_ID, dto.staffID);
		return editedValues;
	}

	/**
	 * ghi no chi tiet
	 * 
	 * @author: TamPQ
	 * @param cusId
	 * @return
	 * @return: CusDebitDetailDTOvoid
	 * @throws:
	 */
	public CustomerDebitDetailDTO requestDebitDetail(long debitId) {
		CustomerDebitDetailDTO debitDetail = null;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT DD.DEBIT_DETAIL_ID, ");
		var1.append("               SO.ORDER_NUMBER, ");
		var1.append("               SO.SALE_ORDER_ID, ");
		var1.append("               STRFTIME('%d/%m/%Y', SO.ORDER_DATE) AS ORDER_DATE, ");
		var1.append("               SO.ORDER_DATE                       AS ORDER_DATE_2, ");
		var1.append("               DD.TOTAL, ");
		var1.append("               DD.TOTAL_PAY, ");
		var1.append("               DD.REMAIN, ");
		var1.append("               DD.DISCOUNT_AMOUNT, ");
		var1.append("               DD.DISCOUNT, ");
		var1.append("               DD.FROM_OBJECT_ID ");
		var1.append("        FROM   DEBIT_DETAIL DD, ");
		var1.append("               DEBIT DB, ");
		var1.append("               SALE_ORDER SO ");
		var1.append("        WHERE  DD.DEBIT_ID = DB.DEBIT_ID ");
		// remain > 0
		var1.append("               AND DD.REMAIN > 0 ");
		var1.append("               AND SO.SALE_ORDER_ID = DD.FROM_OBJECT_ID ");
		var1.append("               AND SO.ORDER_TYPE IN ('SO', 'IN') ");
		var1.append("               AND DATE(DD.CREATE_DATE) >= DATE('" + date_now
				+ "','localtime', 'start of month', '-1 MONTH') ");
		var1.append("               AND DD.DEBIT_ID = ?) D_D   JOIN ");
		var1.append("       (SELECT PAY_RECEIVED_NUMBER, PD.PAYMENT_DETAIL_ID AS PAYMENT_DETAIL_ID, ");
		var1.append("       			      PR.AMOUNT AS PAY_AMOUNT, ");
		var1.append("       			      PD.DISCOUNT AS PAYMENT_DISCOUNT, ");
		var1.append("                         PD.DEBIT_DETAIL_ID  AS JOIN_COLUMN, ");
		var1.append("                         STRFTIME('%d/%m/%Y', MAX(PD.PAY_DATE)) AS PAY_DATE ");
		var1.append("                  FROM   PAYMENT_DETAIL PD, ");
		var1.append("                         PAY_RECEIVED PR ");
		var1.append("                  WHERE  PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID ");
//		var1.append("                  GROUP  BY PD.DEBIT_DETAIL_ID, PR.PAY_RECEIVED_ID) P_R ");
		var1.append("                  GROUP  BY PAYMENT_DETAIL_ID) P_R ");
		var1.append("              ON D_D.DEBIT_DETAIL_ID = P_R.JOIN_COLUMN ");
		var1.append("UNION ");
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT DD.DEBIT_DETAIL_ID, ");
		var1.append("               SO.ORDER_NUMBER, ");
		var1.append("               SO.SALE_ORDER_ID, ");
		var1.append("               STRFTIME('%d/%m/%Y', SO.ORDER_DATE) AS ORDER_DATE, ");
		var1.append("               SO.ORDER_DATE                       AS ORDER_DATE_2, ");
		var1.append("               DD.TOTAL, ");
		var1.append("               DD.TOTAL_PAY, ");
		var1.append("               DD.REMAIN, ");
		var1.append("               DD.DISCOUNT_AMOUNT, ");
		var1.append("               DD.DISCOUNT, ");
		var1.append("               DD.FROM_OBJECT_ID ");
		var1.append("        FROM   DEBIT_DETAIL DD, ");
		var1.append("               DEBIT DB, ");
		var1.append("               SALE_ORDER SO ");
		var1.append("        WHERE  DD.DEBIT_ID = DB.DEBIT_ID ");
		// remain > 0
		var1.append("               AND DD.REMAIN > 0 ");
		var1.append("               AND SO.SALE_ORDER_ID = DD.FROM_OBJECT_ID ");
		var1.append("               AND SO.ORDER_TYPE IN ('SO', 'IN') ");
		var1.append("               AND DATE(DD.CREATE_DATE) >= DATE('" + date_now
				+ "','localtime', 'start of month', '-1 MONTH') ");
		var1.append("               AND DD.DEBIT_ID = ?) D_D  LEFT JOIN ");
		var1.append("       (SELECT PAY_RECEIVED_NUMBER, PD.PAYMENT_DETAIL_ID AS PAYMENT_DETAIL_ID, ");
		var1.append("       			      PR.AMOUNT AS PAY_AMOUNT, ");
		var1.append("       			      PD.DISCOUNT AS PAYMENT_DISCOUNT, ");
		var1.append("                         PD.FOR_OBJECT_ID  AS JOIN_COLUMN, ");
		var1.append("                         STRFTIME('%d/%m/%Y', MAX(PD.PAY_DATE)) AS PAY_DATE ");
		var1.append("                  FROM   PAYMENT_DETAIL_TEMP PD, ");
		var1.append("                         PAY_RECEIVED_TEMP PR ");
		var1.append("                  WHERE  PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID ");
//		var1.append("                  GROUP  BY PD.DEBIT_DETAIL_ID, PR.PAY_RECEIVED_ID) P_R ");
		var1.append("                  GROUP  BY PAYMENT_DETAIL_ID) P_R ");
		var1.append("              ON D_D.SALE_ORDER_ID = P_R.JOIN_COLUMN ");
		var1.append("ORDER  BY ORDER_DATE_2 , ");
		var1.append("         PAY_DATE , ");
		var1.append("          REMAIN DESC ");

		String[] params = { "" + debitId, "" + debitId };
		Cursor c = null;
		ArrayList<Long> debitDetailIDArr = new ArrayList<Long>();
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				debitDetail = new CustomerDebitDetailDTO();
				if (c.moveToFirst()) {
					do {
						CustomerDebitDetailItem item = new CustomerDebitDetailItem();
						item.initFromCursor(c);
						debitDetail.arrList.add(item);
						if (!debitDetailIDArr.contains(item.debitDetailDTO.debitDetailID)) {
								//neu remain cua debit_detail > 0 va da co pay_received thi clone 1 debit_detail de gach no
								debitDetailIDArr.add(item.debitDetailDTO.debitDetailID);
								if (item.debitDetailDTO.remain > 0 && !StringUtil.isNullOrEmpty(item.payReceiveNumber)) {
									CustomerDebitDetailItem cloneItem = new CustomerDebitDetailItem();
									cloneItem.debitDetailDTO = item.debitDetailDTO;
									debitDetail.arrList.add(cloneItem);
								}
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		StringBuffer var2 = new StringBuffer();
		var2.append("SELECT * FROM DEBIT WHERE DEBIT_ID = ? ");
		String[] params2 = { "" + debitId };
		Cursor c2 = null;
		try {
			c2 = rawQuery(var2.toString(), params2);
			if (c2 != null && c2.moveToFirst()) {
				if (debitDetail != null) {
					debitDetail.debitDTO.id = CursorUtil.getLong(c2, "DEBIT_ID");
					debitDetail.debitDTO.totalPay = CursorUtil.getLong(c2, "TOTAL_PAY");
					debitDetail.debitDTO.totalDebit = CursorUtil.getLong(c2, "TOTAL_DEBIT");
					debitDetail.debitDTO.totalDiscount = CursorUtil.getLong(c2, "TOTAL_DISCOUNT");
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c2 != null) {
					c2.close();
				}
			} catch (Exception e) {
			}			
		}
		
		if (debitDetail != null) {
			//fullDate nhung phieu da thanh toan roi xuong duoi
			int size = debitDetail.arrList.size();
			for (int i = 0; i < size; i++) {
				if (!StringUtil.isNullOrEmpty(debitDetail.arrList.get(i).payReceiveNumber)) {
					CustomerDebitDetailItem item = debitDetail.arrList.get(i);
					debitDetail.arrList.remove(i);
					i--;
					size--;
					debitDetail.arrList.add(item);
				}
			}
		}
		return debitDetail;
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @param cusDebitDetailItem
	 * @return: voidvoid
	 * @throws:
	 */
	public int updateDebt(CusDebitDetailItem cusDebitDetailItem) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(TOTAL_PAY, cusDebitDetailItem.paidAmount);
		editedValues.put(REMAIN, cusDebitDetailItem.remainingAmount);
		editedValues.put(DISCOUNT, cusDebitDetailItem.discount);
		editedValues.put(UPDATE_DATE, DateUtils.now());
		editedValues.put(UPDATE_USER, "" + GlobalInfo.getInstance().getProfile().getUserData().getUserName());
		String[] params = { "" + cusDebitDetailItem.debitDetailId };
		return update(editedValues, DEBIT_DETAIL_ID + " = ?", params);

	}

	/**
	 * update chi tiet cong no
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: int
	 * @throws:  
	 * @param debitDetailItem
	 * @return
	 */
	public int updateDebt(DebitDetailDTO debitDetailItem) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(TOTAL_PAY, debitDetailItem.totalPay);
		editedValues.put(REMAIN, debitDetailItem.remain);
		editedValues.put(DISCOUNT_AMOUNT, debitDetailItem.discountAmount);
		editedValues.put(UPDATE_DATE, DateUtils.now());
		editedValues.put(UPDATE_USER, "" + GlobalInfo.getInstance().getProfile().getUserData().getUserName());
		String[] params = { "" + debitDetailItem.debitDetailID };
		return update(editedValues, DEBIT_DETAIL_ID + " = ?", params);

	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param saleOrderId
	 * @return
	 * @throws Exception
	 * @return: DebitDTO
	 * @throws:
	*/
	public DebitDetailDTO getDebitDetailBySaleOrderID(long saleOrderId) throws Exception {
		DebitDetailDTO result = null;

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * from DEBIT_DETAIL where 1=1 and DEBIT_DETAIL.FROM_OBJECT_ID = ? limit 1");

		Cursor cursor = null;
		try {
			String[] arrParam = new String[] { "" + saleOrderId };
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToNext()) {
					result = initDebitDetail(cursor);
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			}
		}
		return result;
	}
	
	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param cursor
	 * @return
	 * @return: DebitDetailDTO
	 * @throws:
	*/
	DebitDetailDTO initDebitDetail(Cursor c){
		DebitDetailDTO result = new DebitDetailDTO();
		result.debitID = CursorUtil.getLong(c, DEBIT_ID);
		result.debitDetailID = CursorUtil.getLong(c, DEBIT_DETAIL_ID);
		result.fromObjectID = CursorUtil.getLong(c, FROM_OBJECT_ID);
		result.amount = CursorUtil.getLong(c, AMOUNT);
		result.discount = CursorUtil.getLong(c, DISCOUNT);
		result.total = CursorUtil.getLong(c, TOTAL);
		result.totalPay = CursorUtil.getLong(c, TOTAL_PAY);
		result.discountAmount = CursorUtil.getLong(c, DISCOUNT_AMOUNT);
		result.remain = CursorUtil.getLong(c, REMAIN);
		result.fromObjectNumber = CursorUtil.getString(c, FROM_OBJECT_NUMBER);
		result.invoiceNumber = CursorUtil.getLong(c, INVOICE_NUMBER);
		result.createDate = CursorUtil.getString(c, CREATE_DATE);
		result.createUser = CursorUtil.getString(c, CREATE_USER);
		result.debitDate = CursorUtil.getString(c, DEBIT_DATE);
		result.type = CursorUtil.getInt(c, TYPE);

		return result;
	}

	/**lay danh sach don hang tra lai
	 * @author cuonglt3
	 * @param debitId
	 * @return
	 */
	public CustomerDebitDetailDTO requestDebitDetailReturn(long debitId) {
		CustomerDebitDetailDTO debitDetail = null;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT DD.DEBIT_DETAIL_ID, ");
		var1.append("               SO.ORDER_NUMBER, ");
		var1.append("               SO.SALE_ORDER_ID, ");
		var1.append("               DD.FROM_OBJECT_ID, ");
		var1.append("               (case when so.type = 2 then STRFTIME('%d/%m/%Y', ");
		var1.append("                             SOR.ORDER_DATE) else STRFTIME('%d/%m/%Y', ");
		var1.append("  					SO.ORDER_DATE) end ) AS ORDER_DATE,   ");
		var1.append("               (case when so.type = 2  then STRFTIME('%d/%m/%Y', ");
		var1.append("               SO.ORDER_DATE) else null end) RETURN_DATE, ");
		var1.append("               DD.TOTAL, ");
		var1.append("               DD.TOTAL_PAY, ");
		var1.append("               DD.DEBIT_DATE AS SORT_DEBIT, ");
		var1.append("               DD.REMAIN, ");
		var1.append("               DD.DISCOUNT_AMOUNT, ");
		var1.append("               DD.DISCOUNT, ");
		var1.append("               (CASE WHEN DD.REMAIN < 0 THEN 1 ELSE 0 END) AS NOTPAID ");
		var1.append("        FROM   DEBIT_DETAIL DD, ");
		var1.append("               DEBIT DB, ");
		var1.append("               SALE_ORDER SO ");
		var1.append("               LEFT JOIN sale_order SOR ON (SOR.SALE_ORDER_ID = SO.FROM_SALE_ORDER_ID ) ");
		var1.append("        WHERE  DD.DEBIT_ID = DB.DEBIT_ID ");
		// remain > 0
		var1.append("               AND DD.TOTAL < 0 ");
//		var1.append("               AND DD.CREATE_USER =  ? ");
		var1.append("               AND SO.SALE_ORDER_ID = DD.FROM_OBJECT_ID ");
		var1.append("               AND SO.order_type in ('CO') ");
		var1.append("               AND DATE(DD.CREATE_DATE) >= DATE('" + date_now
				+ "','localtime', 'start of month', '-1 MONTH') ");
		var1.append("               AND DD.DEBIT_ID = ?) D_D ");
		var1.append("        JOIN (SELECT PAY_RECEIVED_NUMBER, ");
		var1.append("                         DEBIT_DETAIL_ID  AS JOIN_COLUMN, ");
		var1.append("                         STRFTIME('%d/%m/%Y',PR.create_date) AS PAY_DATE, PR.amount as PAY_AMOUNT ");
		var1.append("                  FROM   PAYMENT_DETAIL PD, ");
		var1.append("                         PAY_RECEIVED PR ");
		var1.append("                  WHERE  PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID ");
		var1.append("                 		  AND PD.status != -1 ");
		var1.append("                  GROUP  BY JOIN_COLUMN) P_R ");
		var1.append("              ON D_D.DEBIT_DETAIL_ID = P_R.JOIN_COLUMN ");
		var1.append("UNION ");
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT DD.DEBIT_DETAIL_ID, ");
		var1.append("               SO.ORDER_NUMBER, ");
		var1.append("               SO.SALE_ORDER_ID, ");
		var1.append("               DD.FROM_OBJECT_ID, ");
		var1.append("               (case when so.type = 2 then STRFTIME('%d/%m/%Y', ");
		var1.append("                             SOR.ORDER_DATE) else STRFTIME('%d/%m/%Y', ");
		var1.append("  					SO.ORDER_DATE) end ) AS ORDER_DATE,   ");
		var1.append("               (case when so.type = 2  then STRFTIME('%d/%m/%Y', ");
		var1.append("               SO.ORDER_DATE) else null end) RETURN_DATE, ");
		var1.append("               DD.TOTAL, ");
		var1.append("               DD.TOTAL_PAY, ");
		var1.append("               DD.DEBIT_DATE AS SORT_DEBIT, ");
		var1.append("               DD.REMAIN, ");
		var1.append("               DD.DISCOUNT_AMOUNT, ");
		var1.append("               DD.DISCOUNT, ");
		var1.append("               (CASE WHEN DD.REMAIN < 0 THEN 1 ELSE 0 END) AS NOTPAID ");
		var1.append("        FROM   DEBIT_DETAIL DD, ");
		var1.append("               DEBIT DB, ");
		var1.append("               SALE_ORDER SO ");
		var1.append("               LEFT JOIN sale_order SOR ON (SOR.SALE_ORDER_ID = SO.FROM_SALE_ORDER_ID ) ");
		var1.append("        WHERE  DD.DEBIT_ID = DB.DEBIT_ID ");
		// remain > 0
		var1.append("               AND DD.TOTAL < 0 ");
//		var1.append("               AND DD.CREATE_USER =  ? ");
		var1.append("               AND SO.SALE_ORDER_ID = DD.FROM_OBJECT_ID ");
		var1.append("               AND SO.order_type in ('CO') ");
		var1.append("               AND DATE(DD.CREATE_DATE) >= DATE('" + date_now
				+ "','localtime', 'start of month', '-1 MONTH') ");
		var1.append("               AND DD.DEBIT_ID = ?) D_D ");
		var1.append("       LEFT JOIN (SELECT PAY_RECEIVED_NUMBER, ");
		var1.append("                         PD.FOR_OBJECT_ID  AS JOIN_COLUMN, ");
		var1.append("                         STRFTIME('%d/%m/%Y',PR.create_date) AS PAY_DATE, PR.amount as PAY_AMOUNT ");
		var1.append("                  FROM   PAYMENT_DETAIL_TEMP PD, ");
		var1.append("                         PAY_RECEIVED_TEMP PR ");
		var1.append("                  WHERE  PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID ");
		var1.append("                 		  AND PD.status != -1 ");
		var1.append("                  GROUP  BY JOIN_COLUMN) P_R ");
		var1.append("              ON D_D.SALE_ORDER_ID = P_R.JOIN_COLUMN ");
		var1.append("ORDER  BY SORT_DEBIT, ");
		var1.append("          REMAIN DESC ");
		
		String[] params = { "" + debitId,  "" + debitId};
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				debitDetail = new CustomerDebitDetailDTO();
				if (c.moveToFirst()) {
					do {
						CustomerDebitDetailItem item = new CustomerDebitDetailItem();
						item.initFromCursor(c);
						debitDetail.arrList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
			MyLog.e("", ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		
		StringBuffer var2 = new StringBuffer();
		var2.append("SELECT * FROM DEBIT WHERE DEBIT_ID = ? ");
		String[] params2 = { "" + debitId };
		Cursor c2 = null;
		try {
			c2 = rawQuery(var2.toString(), params2);
			if (c2 != null && c2.moveToFirst()) {
				if (debitDetail != null) {
					debitDetail.debitDTO.id = CursorUtil.getLong(c2, "DEBIT_ID");
					debitDetail.debitDTO.totalPay = CursorUtil.getLong(c2, "TOTAL_PAY");
					debitDetail.debitDTO.totalDebit = CursorUtil.getLong(c2, "TOTAL_DEBIT");
					debitDetail.debitDTO.totalDiscount = CursorUtil.getLong(c2, "TOTAL_DISCOUNT");
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c2 != null) {
					c2.close();
				}
			} catch (Exception e) {
			}
		}
		return debitDetail;
	}
	
	/**lay danh sach thanh toan bi tu choi
	 * @param debitID
	 * @return
	 */
	public CustomerDebitDetailDTO getPaymentDetailRefused(long debitID){
		CustomerDebitDetailDTO debitDetail = null;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   (SELECT DD.DEBIT_DETAIL_ID, ");
		var1.append("               SO.ORDER_NUMBER, ");
		var1.append("               SO.SALE_ORDER_ID, ");
		var1.append("               STRFTIME('%d/%m/%Y', SO.ORDER_DATE) AS ORDER_DATE, ");
		var1.append("               STRFTIME('%d/%m/%Y', SOR.ORDER_DATE)  AS RETURN_DATE, ");
		var1.append("               STRFTIME('%d/%m/%Y', DD.DEBIT_DATE)  AS DEBIT_DATE, ");
		var1.append("               DD.TOTAL, ");
		var1.append("               DD.TOTAL_PAY, ");
		var1.append("               DD.DEBIT_DATE AS SORT_DEBIT, ");
		var1.append("               DD.REMAIN, ");
		var1.append("               DD.DISCOUNT_AMOUNT, ");
		var1.append("               DD.DISCOUNT, ");
		var1.append("               (CASE WHEN DD.REMAIN < 0 THEN 1 ELSE 0 END) AS NOTPAID ");
		var1.append("        FROM   DEBIT_DETAIL DD, ");
		var1.append("               DEBIT DB, ");
		var1.append("               SALE_ORDER SO ");
		var1.append("               LEFT JOIN sale_order SOR ON (SOR.FROM_SALE_ORDER_ID = SO.SALE_ORDER_ID) ");
		var1.append("        WHERE  DD.DEBIT_ID = DB.DEBIT_ID ");
		var1.append("               AND SO.SALE_ORDER_ID = DD.FROM_OBJECT_ID ");
		var1.append("               AND DATE(DD.CREATE_DATE) >= DATE( ?,'localtime', 'start of month', '-1 MONTH') ");
		var1.append("               AND DD.DEBIT_ID = ?) D_D ");
		var1.append("        JOIN (SELECT PAY_RECEIVED_NUMBER, PR.RECEIPT_TYPE AS RECEIPT_TYPE,");
		var1.append("                         PD.FOR_OBJECT_ID  AS SALE_ORDER_ID, ");
		var1.append("                         STRFTIME('%d/%m/%Y', MAX(PD.PAY_DATE)) AS PAY_DATE, sum(PD.discount) as PAYMENT_DISCOUNT, ");
		var1.append("                         PD.payment_detail_id  as PAYMENT_DETAIL_ID,   ");
		var1.append("                         PR.amount  as AMOUNT   ");
		var1.append("                  FROM   PAYMENT_DETAIL_TEMP PD, ");
		var1.append("                         PAY_RECEIVED_TEMP PR ");
		var1.append("                  WHERE  PD.PAY_RECEIVED_ID = PR.PAY_RECEIVED_ID ");
		var1.append("                  AND    PR.APPROVED = '2' ");
		var1.append("                  AND    PD.status != -1 ");
		var1.append("                  GROUP BY PD.PAYMENT_DETAIL_ID, PR.PAY_RECEIVED_ID) P_R ");
		var1.append("              ON D_D.SALE_ORDER_ID = P_R.SALE_ORDER_ID ");
		var1.append("              where 1=1 ");
		var1.append("ORDER  BY dayInOrder(SORT_DEBIT), ");
		var1.append("          REMAIN DESC ");
		
		String[] params = {date_now, "" + debitID };
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				debitDetail = new CustomerDebitDetailDTO();
				if (c.moveToFirst()) {
					do {
						CustomerDebitDetailItem item = new CustomerDebitDetailItem();
						item.initFromCursor(c);
						debitDetail.arrList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("", e.toString());
			}
		}
		
		StringBuffer var2 = new StringBuffer();
		var2.append("SELECT * FROM DEBIT WHERE DEBIT_ID = ? ");
		String[] params2 = { "" + debitID };
		Cursor c2 = null;
		try {
			c2 = rawQuery(var2.toString(), params2);
			if (c2 != null && c2.moveToFirst()) {
				if (debitDetail != null) {
					debitDetail.debitDTO.id = CursorUtil.getLong(c2, "DEBIT_ID");
					debitDetail.debitDTO.totalPay = CursorUtil.getLong(c2, "TOTAL_PAY");
					debitDetail.debitDTO.totalDebit = CursorUtil.getLong(c2, "TOTAL_DEBIT");
					debitDetail.debitDTO.totalDiscount = CursorUtil.getLong(c2, "TOTAL_DISCOUNT");
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c2 != null) {
					c2.close();
				}
			} catch (Exception e) {
			}
		}
		return debitDetail;
	
	}

	/**update debit_detail
	 * @author cuonglt3
	 * @param dto
	 * @return
	 */
	public int updateDebitDetail(CustomerDebitDetailDTO dto) {
		// TODO Auto-generated method stub
		int result = -1;
		for (int i = 0; i < dto.arrList.size(); i++) {
			if (dto.arrList.get(i).debitDetailDTO.isChanged) {	
				dto.arrList.get(i).debitDetailDTO.remain = 0;
				result = updateDebt(dto.arrList.get(i).debitDetailDTO);
				if (result == -1) {
					return result;
				}
			}		
		}
		return result;
	}
	
}
