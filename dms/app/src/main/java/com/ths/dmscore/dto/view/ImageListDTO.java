package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.dto.db.DisplayProgrameDTO;

public class ImageListDTO {	
	public int totalCustomer;
	public ArrayList<ImageListItemDTO> listItem;
	public ArrayList<DisplayProgrameDTO> listPrograme;
	public ListStaffDTO staffList;
	
	public ImageListDTO(){
		listItem = new ArrayList<ImageListItemDTO>();
		totalCustomer=0;
		listPrograme = new ArrayList<DisplayProgrameDTO>();
	}
	
	public void addItem(ImageListItemDTO c) {
		listItem.add(c);
	}
	public ImageListItemDTO newImageListRowDTO() {
		return new ImageListItemDTO();
	}
}
