/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.table;

import android.util.SparseArray;

import com.ths.dmscore.util.MyLog;

/**
 * DMSSortQueryBuilder.java
 * @author: duongdt3
 * @version: 1.0 
 * @since:  14:29:17 26 Mar 2015
 */
public class DMSSortQueryBuilder {
	SparseArray<String> lstSortMapper = new SparseArray<String>();
	String defaultOrderByStr = "";
	public DMSSortQueryBuilder addMapper(int action, String colOrder){
		lstSortMapper.put(action, colOrder);
		return this;
	}
	
	public DMSSortQueryBuilder defaultOrderString(String defaultOrderByStr){
		this.defaultOrderByStr = defaultOrderByStr;
		return this;
	}
	
	public String build(DMSSortInfo sortInfo){
		String result = defaultOrderByStr;
		if (sortInfo != null) {
			String col = lstSortMapper.get(sortInfo.getSortAction());
			if (col != null) {
				String type = (sortInfo.getSortType() == DMSSortInfo.ASC_TYPE) ? "ASC" : "DESC";
				//khong phan biet hoa thuong
				if (sortInfo.getDataType() == DMSSortInfo.DATA_TYPE_CASE_UN_SENSITIVE) {
					col = "UPPER(" + col + ")";
				}
				
				result = String.format(" ORDER BY %s %s ", col, type);
			}
		}
		
		if (result == null) {
			result = "";
		}
		MyLog.d("build sort info", result);
		return result;
	}
	
	public String build(DMSSortInfo sortInfo, String defaultOrderByStr){
		this.defaultOrderByStr = defaultOrderByStr;
		return build(sortInfo);
	}
}
