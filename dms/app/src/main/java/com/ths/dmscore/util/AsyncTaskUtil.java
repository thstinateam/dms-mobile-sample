/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.util.concurrent.ThreadPoolExecutor;

import android.os.AsyncTask;

/**
 * Thuc hien mot so ultil kiem tra asynctask
 * @author banghn
 * @version 1.0
 */
public class AsyncTaskUtil {
	
	public static String getThreadInfo(){
		StringBuffer log = new StringBuffer();
		try {
			//log.append(" Total thread: " + getNumAsyncTaskTotal());
			log.append("Thread active: " + getNumAsyncTaskActive());
			log.append("\n Thread waiting: " + getNumAsyncTaskWait());
			//log.append("\n Thread completed: " + getNumAsyncTaskComplete());
			//log.append("\n SQL in transaction: " + SQLUtils.getInstance().isInTransaction());
		} catch (Exception e) {
			//neu loi thi loi os, khong can xu ly gi them neu ko lay duoc
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return log.toString();
	}
	
	// Lay so luong AsyncTask dang thuc thi (tham khao)
	public static int getNumAsyncTaskActive() {
		int result = 0;
		try {
			ThreadPoolExecutor executor = getThreadPoolExecutor();
			if (executor != null) {
				result = executor.getActiveCount();
			}
		} catch (Exception ex) {
			// neu loi thi loi os, khong can xu ly gi them neu ko lay duoc
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return result;
	}

	// Lay so luong AsyncTask dang cho trong queue (tham khao)
	public static int getNumAsyncTaskWait() {
		int result = 0;
		try {
			ThreadPoolExecutor executor = getThreadPoolExecutor();
			if (executor != null && executor.getQueue() != null) {
				result = executor.getQueue().size();
			}
		} catch (Exception ex) {
			// neu loi thi loi os, khong can xu ly gi them neu ko lay duoc
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return result;
	}

	// Lay so luong AsyncTask da hoan thanh(tham khao)
	public static long getNumAsyncTaskComplete() {
		long result = 0;
		try {
			ThreadPoolExecutor executor = getThreadPoolExecutor();
			if (executor != null) {
				result = executor.getCompletedTaskCount();
			}
		} catch (Exception ex) {
			// neu loi thi loi os, khong can xu ly gi them neu ko lay duoc
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return result;
	}

	// Lay tong so luong AsyncTask da duoc queue(tham khao)
	public static long getNumAsyncTaskTotal(){
		long result = 0;
		try {
			ThreadPoolExecutor executor = getThreadPoolExecutor();
			if (executor != null) {
				result = executor.getTaskCount();
			}
		} catch (Exception ex) {
			// neu loi thi loi os, khong can xu ly gi them neu ko lay duoc
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return result;
	}

	public static ThreadPoolExecutor getThreadPoolExecutor() throws Exception{
		return AsyncTask.THREAD_POOL_EXECUTOR instanceof ThreadPoolExecutor ? ((ThreadPoolExecutor) AsyncTask.THREAD_POOL_EXECUTOR)
				: null;
	}

}
