/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * LanguagesDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 08:47:01 21-01-2015
 */
public class LanguagesDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	// name that will show in spinner
	private String name;
	// values is languages in android, "en", 'vi'
	private String value;
	// national flag.
	private int icon;

	public LanguagesDTO(String nameLanguages, String valuesLanguages,
			int iconLanguages) {
		// TODO Auto-generated constructor stub
		name = nameLanguages;
		value = valuesLanguages;
		icon = iconLanguages;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name;
	}

}
