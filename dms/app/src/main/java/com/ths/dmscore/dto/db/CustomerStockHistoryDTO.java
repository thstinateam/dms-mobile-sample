/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.dto.view.RemainProductViewDTO;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_STOCK_HISTORY_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Thong tin loai customer stock history
 * 
 * @author: PhucNT
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerStockHistoryDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 8108228136123132039L;

	public static final int TYPE_CTTB = 1;

	// id cua bang
	public long customerStockHistoryId;
	// id cua khach hang
	public long customerId;
	// id cua nhan vien ban hang
	public int staffId;
	// id cua san pham
	public int productId;
	// object type (1: CTTB)
	public int objectType;
	// object id (tuong ung voi object type (1: DISPLAY_PROGRAM_ID))
	public long objectId;
	// result: so luong ton kho cua san pham luc cham
	public int result;
	// ngay tao
	public String createDate;

	// // id sale order
	// public int saleOrderId ;
	// so luong
	// public int quanlity ;
	public String checkDate;
	public int ordinal;

	public CustomerStockHistoryDTO() {
		super(TableType.CUSTOMER_STOCK_HISTORY_TABLE);
	}

	public void convertFromRemainProductDTO(RemainProductViewDTO s) {
		// TODO Auto-generated method stub
		customerId = s.getCUSTOMER_ID();
		staffId = s.getSTAFF_ID();
		productId = s.getPRODUCT_ID();
		if(!StringUtil.isNullOrEmpty(s.REMAIN_NUMBER)){
			result = Integer.parseInt(s.REMAIN_NUMBER);
		}
		ordinal = s.ordinal;
	}

	public JSONObject generateUpdateRemainProductSql() {
		// TODO Auto-generated method stub
		JSONObject orderJson = new JSONObject();
		try {
			// UPDATE don hang thanh trang thai da chuyen

			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_STOCK_HISTORY_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CUSTOMER_STOCK_HISTORY_ID,
					customerStockHistoryId, null));
			// state -- don hang do minh tao nen state = 1
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CUSTOMER_ID, customerId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.STAFF_ID, staffId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.PRODUCT_ID, productId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.OBJECT_TYPE, objectType, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.OBJECT_ID, objectId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.RESULT, result, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CREATE_DATE, createDate, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.ORDINAL, ordinal, null));

			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * general sql to insert to server when vote display program
	 * 
	 * @return
	 * @return: Vector
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 16, 2013
	 */
	public JSONObject generateCreateSqlInsertVoteDisplayProgram() {
		JSONObject orderJson = new JSONObject();
		try {

			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_STOCK_HISTORY_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CUSTOMER_STOCK_HISTORY_ID,
					customerStockHistoryId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CUSTOMER_ID, customerId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.STAFF_ID, staffId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.PRODUCT_ID, productId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.OBJECT_TYPE, objectType, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.OBJECT_ID, objectId, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.RESULT, result, null));
			params.put(GlobalUtil.getJsonColumn(CUSTOMER_STOCK_HISTORY_TABLE.CREATE_DATE, createDate, null));

			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}
}
