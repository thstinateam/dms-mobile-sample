/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Hien thi popup chi tiet cau lac bo role GSBH
 * 
 * @author: yennth16
 * @version: 1.0
 * @since: 13:39:53 13-07-2015
 */
public class KeyShopDetailView extends LinearLayout implements OnClickListener {

	public static final int CLOSE_POPUP_DETAIL_PROMOTION = 1000;
	// ma clb
	TextView tvKeyshopCode;
	// ten clb
	TextView tvKeyshopName;
	// tu chu ky
	TextView tvFromCycle;
	// den chu ky
	TextView tvToCycle;
	// san pham
	TextView tvProductInfo;
	// mo ta
	TextView tvContentKeyshop;
	TextView tvText;
	// muc chuong trinh
	private DMSTableView tbLevelKeyshop;
	// button close
	public Button btCloseKeyshopDetail;

	// BaseFragment
	public BaseFragment listener;
	public View viewLayout;
	KeyShopItemDTO dtoData;
	ReportKeyshopStaffItemDTO dtoReport;
	Context context;

	/**
	 * @param context
	 * @param attrs
	 */
	public KeyShopDetailView(Context context, BaseFragment listener) {
		super(context);
		this.context = context;

		this.listener = listener;
		LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_key_shop_detail_view,
				null);
		btCloseKeyshopDetail = (Button) viewLayout.findViewById(R.id.btCloseKeyshopDetail);
		btCloseKeyshopDetail.setOnClickListener(this);

		tvKeyshopCode = (TextView) viewLayout.findViewById(R.id.tvKeyshopCode);
		tvKeyshopName = (TextView) viewLayout.findViewById(R.id.tvKeyshopName);
		tvFromCycle = (TextView) viewLayout.findViewById(R.id.tvFromCycle);
		tvToCycle = (TextView) viewLayout.findViewById(R.id.tvToCycle);
		tvProductInfo = (TextView) viewLayout.findViewById(R.id.tvProductInfo);
		tvContentKeyshop = (TextView) viewLayout.findViewById(R.id.tvContentKeyshop);
		tvText = (TextView) viewLayout.findViewById(R.id.tvText);
		tbLevelKeyshop = (DMSTableView) viewLayout.findViewById(R.id.tbLevelKeyshop);
		initHeaderTable();
	}

	/**
	 * update layout
	 * @author: yennth16
	 * @since: 19:50:57 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param dto
	 */
	public void updateLayout(KeyShopItemDTO dto) {
		this.dtoData = dto;
		tvKeyshopCode.setText(dto.ksCode);
		tvKeyshopName.setText(dto.name);
		tvFromCycle.setText(dto.fromCycleId);
		tvToCycle.setText(dto.toCycleId);
		tvProductInfo.setText(dto.productInfo);
		tvContentKeyshop.setText(dto.description);
		renderDataTableLevel();
	}
	/**
	 * renderDataTableLevel
	 * @author: yennth16
	 * @since: 19:50:40 13-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderDataTableLevel() {
		tbLevelKeyshop.clearAllData();
		if (dtoData != null && dtoData.ksLevelItem != null
				&& dtoData.ksLevelItem.size() > 0) {
			tbLevelKeyshop.setVisibility(View.VISIBLE);
			KSLevelDTO item;
			for (int i = 0, s = dtoData.ksLevelItem.size(); i < s; i++) {
				item = dtoData.ksLevelItem.get(i);
				KeyShopLevelRow row = new KeyShopLevelRow(context);
				row.render(item, i + 1);
				tbLevelKeyshop.addRow(row);
			}
		} else {
			tbLevelKeyshop.setVisibility(View.GONE);
		}
	}
	
	/**
	 * update layout
	 * @author: yennth16
	 * @since: 19:50:57 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param dto
	 */
	public void updateLayoutReport(ReportKeyshopStaffItemDTO dto) {
		this.dtoReport = dto;
		tvKeyshopCode.setText(dto.keyshop.ksCode);
		tvKeyshopName.setText(dto.keyshop.name);
		tvFromCycle.setText(dto.keyshop.fromCycleId);
		tvToCycle.setText(dto.keyshop.toCycleId);
		tvProductInfo.setText(dto.productInfo);
		tvContentKeyshop.setText(dto.keyshop.description);
		renderDataTableLevelReport();
	}

	
	/**
	 * renderDataTableLevel
	 * @author: yennth16
	 * @since: 19:50:40 13-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderDataTableLevelReport() {
		tbLevelKeyshop.clearAllData();
		if (dtoReport != null && dtoReport.ksLevelItem != null
				&& dtoReport.ksLevelItem.size() > 0) {
			tbLevelKeyshop.setVisibility(View.VISIBLE);
			tvText.setVisibility(View.VISIBLE);
			KSLevelDTO item;
			for (int i = 0, s = dtoReport.ksLevelItem.size(); i < s; i++) {
				item = dtoReport.ksLevelItem.get(i);
				KeyShopLevelRow row = new KeyShopLevelRow(context);
				row.render(item, i + 1);
				tbLevelKeyshop.addRow(row);
			}
		} else {
			tbLevelKeyshop.setVisibility(View.GONE);
			tvText.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View view) {
		if (view == btCloseKeyshopDetail) {
			listener.onClick(view);
		}
	}

	/**
	 * initHeaderTable
	 * @author: yennth16
	 * @since: 15:37:52 13-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initHeaderTable() {
		KeyShopLevelRow header = new KeyShopLevelRow(context);
		tbLevelKeyshop.addHeader(header);
	}
}