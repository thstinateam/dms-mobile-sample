/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALES_ORDER_DETAIL_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Thong tin chi tiet don hang
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class SaleOrderDetailDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -3990068159928498253L;
	// id chi tiet don hang
	public long salesOrderDetailId;
	// ma san pham
	public int productId;
	// tong so luong
	public double quantity;
	// so luong thung
	public long quantityPackage;
	// so luong thung
	public long quantityRetail;
	// gia lich su
	public long priceId;
	// % khuyen mai
	public double discountPercentage;
	// so tien khuyen mai
	private double discountAmount;
	// 0: hang ban, 1: hang KM
	public int isFreeItem;
	// so tien
	private double amount;
	// id don dat hang
	public long salesOrderId;
	// gia ban le
	public double price;
	public boolean isPriceNull = false;
	// gia ban
	public double packagePrice;
	public boolean isPackagePriceNull = false;
	// nguoi tao
	public String createUser;
	// nguoi cap nhat
	public String updateUser;
	// ngay tao
	public String createDate;
	// ngay cap nhat
	public String updateDate;
	// ngay dat
	public String orderDate;
	// so luong KM max cua mat hang
	public int maxQuantityFree;
	// so luong KM max cua mat hang dung cho thay doi so luong sp
	public int maxQuantityFreeTemp;
	// loai chuong trinh: 0 - CTKM tinh tu dong, 1 - CTKM tinh manual, 2 - CTTB, 3: doi, 4: huy, 5: tra, 6: don hang, 7: tich luy, 8: mo moi
	public int programeType = -1; // mac dinh la -1 de kiem tra khi luu
	// ma chuong trinh khuyen mai tay - ma chuong trinh trung bay tay - CTKM tu dong
	public String programeCode;
	// ten chuong trinh khuyen mai
	public String programeName;
//	public String programeLevel;
	// id cua nv
	public int staffId;
	// id nha phan phoi
	public int shopId;

	// thue VAT
	public double vat;
	// gia le chua VAT
	public double priceNotVat;
	// gia thung chua VAT
	public double packagePriceNotVat;
	// trong luong
	public double totalWeight;
	//max tien KM
	public double maxAmountFree;
	//luu loai KM (vi du ZV01, 02...)
	public String programeTypeCode;

	public int maxQuantityFreeCK;

	public long productGroupId;
	public long groupLevelId;
	public int payingOrder;

	// trang thai da syn hay chua?
	public int synState;
	//has promotion
	public boolean hasPromotion;
	//Id CT HTTM dung de tinh toan so suat
	public long programeId;
	// ds CTKM cua sp thuoc ve de view o man hinh ds san pham dat
	public String programeIdBuyView;

	//so luong da su dung
	public double quantityUsed;
	public long amountUsed;
	public double quantityBegin;
	public long amountBegin;
	public long priceBegin;

	public SaleOrderDetailDTO(){
		super(TableType.SALE_ORDER_DETAIL);
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return StringUtil.round(amount, GlobalInfo.getInstance().getSysNumRoundingPromotion());
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void addAmount(double amountAdd) {
		this.amount += amountAdd;
	}

	public void subtractAmount(double amountSubtract) {
		this.amount -= amountSubtract;
	}

	/**
	 * @return the discountAmount
	 */
	public double getDiscountAmount() {
		return StringUtil.round(discountAmount, GlobalInfo.getInstance().getSysNumRoundingPromotion());
	}

	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public void addDiscountAmount(double discountAdd) {
		this.discountAmount += discountAdd;
	}

	public void subtractDiscountAmount(double discountSubtract) {
		this.discountAmount -= discountSubtract;
	}

	public String insertSql() {
		String sql = "insert into SALES_ORDER (SALE_ORDER_ID,SHOP_ID, ORDER_NUMBER, ORDER_DATE, CUSTOMER_ID, STAFF_ID,"
				+ "DELIVERY_ID, AMOUNT, TOTAL, CREATE_USER, CREATE_DATE) values (SALES_ORDER_SEQ.nextval,?,?,sysdate,?,?,?,?,?,?,sysdate)";
		return sql;
	}

	public ArrayList<String> generateInsertOrderSql() {
		ArrayList<String> result = new ArrayList<String>();
		String sql = "insert into SALES_ORDER (SALE_ORDER_ID,SHOP_ID, ORDER_NUMBER, ORDER_DATE, CUSTOMER_ID, STAFF_ID,"
				+ "DELIVERY_ID, AMOUNT, TOTAL, CREATE_USER, CREATE_DATE) values (SALES_ORDER_SEQ.nextval,?,?,sysdate,?,?,?,?,?,?,sysdate)";

		StringBuffer paraBuff = new StringBuffer();
//		paraBuff.append(this.shopId);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.orderNumber);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.customerId);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.staffId);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.DELIVERY_CODE);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.amount);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.TOTAL);
//		paraBuff.append(com.viettel.vinamilk.constants.Constants.STR_SPLIT_SQL);
//		paraBuff.append(this.createUser);

		result.add(sql);
		result.add(paraBuff.toString());
		return result;
	}



	/**
	*  Generate cau lenh insert
	*  @author: TruongHN
	*  @param orderId
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateCreateSql(){
		JSONObject orderDetailJson = new JSONObject();
		try{
//			INSERT INTO
//			SALES_ORDER_DETAIL(SALES_ORDER_DETAIL_ID,PRODUCT_ID,QUANTITY,PRICE_ID,DISCOUNT_PERCENTAGE,DISCOUNT_AMOUNT,IS_FREE_ITEM,
//			PROMOTION_CODE,AMOUNT,REASON_CODE,SALE_ORDER_ID,PRICE,CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,
//			ORDER_DATE,MAX_QUANTITY_FREE) VALUES()

			orderDetailJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderDetailJson.put(IntentConstants.INTENT_TABLE_NAME, SALES_ORDER_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALE_ORDER_DETAIL_ID,salesOrderDetailId, null));
//			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALES_ORDER_DETAIL_ID,"SALES_ORDER_DETAIL_SEQ", DATA_TYPE.SEQUENCE.toString()));

			if(productId > 0) {
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_ID, productId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_ID,"", DATA_TYPE.NULL.toString()));
			}
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY,quantity, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY_RETAIL,quantityRetail, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY_PACKAGE,quantityPackage, null));
//			if(maxQuantityFree > 0) {
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
//			}
			// thong tin khuyen mai
			if (discountPercentage > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.DISCOUNT_PERCENT,discountPercentage, null));
			}
			if (discountAmount > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.DISCOUNT_AMOUNT,discountAmount, null));
			}

			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.IS_FREE_ITEM,isFreeItem, null));
//			if (!StringUtil.isNullOrEmpty(programeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeCode)){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.JOIN_PROGRAM_CODE,programeCode, null));
//			}
//
//			// them vao programeType neu la mat hang khuyen mai
//			if (hasPromotion || isFreeItem == 1) {
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
//			}
			// tuan them
			if(isFreeItem == 1){

				if (!StringUtil.isNullOrEmpty(programeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
				}
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
				if (promotionOrderNumber > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
				}
				if (!StringUtil.isNullOrEmpty(programeTypeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
				}
//				if(maxQuantityFreeCK > 0){
//					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//				}
				if(maxQuantityFree > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
				}

				if(productGroupId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
				}

				if(groupLevelId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
				}

				if(payingOrder > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PAYING_ORDER, payingOrder, null));
				}
			}


			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.AMOUNT,amount, null));
			// reasonCode -- chua dung nen ko can insert
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALE_ORDER_ID,salesOrderId, null));

			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE,price, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PACKAGE_PRICE, packagePrice, null));
			if (priceId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE_ID,priceId, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CREATE_USER,createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.UPDATE_USER,updateUser, null));
			}
			if (shopId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SHOP_ID,shopId, null));
			}
			if (staffId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.STAFF_ID,staffId, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CREATE_DATE,createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.ORDER_DATE,orderDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE_NOT_VAT,priceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PACKAGE_PRICE_NOT_VAT, packagePriceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CAT_ID, catId, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.TOTAL_WEIGHT,totalWeight, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.VAT,vat, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CONVFACT, convfact, null));
//			if (promotionOrderNumber > 0) {
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeTypeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
//			}
////			if(maxQuantityFreeCK > 0){
////				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
////			}
//
//			if(productGroupId > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
//			}
//
//			if(groupLevelId > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
//			}
//
//			if(payingOrder > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PAYING_ORDER, payingOrder, null));
//			}
			orderDetailJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM, value);

		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderDetailJson;
	}

	public JSONObject generateCreateSqlForUpdate(){
		JSONObject orderDetailJson = new JSONObject();
		try{
//			INSERT INTO
//			SALES_ORDER_DETAIL(SALES_ORDER_DETAIL_ID,PRODUCT_ID,QUANTITY,PRICE_ID,DISCOUNT_PERCENTAGE,DISCOUNT_AMOUNT,IS_FREE_ITEM,
//			PROMOTION_CODE,AMOUNT,REASON_CODE,SALE_ORDER_ID,PRICE,CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,
//			ORDER_DATE,MAX_QUANTITY_FREE) VALUES()

			orderDetailJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderDetailJson.put(IntentConstants.INTENT_TABLE_NAME, SALES_ORDER_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALE_ORDER_DETAIL_ID,salesOrderDetailId, null));
//			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALES_ORDER_DETAIL_ID,"SALES_ORDER_DETAIL_SEQ", DATA_TYPE.SEQUENCE.toString()));

			if(productId > 0) {
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_ID, productId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_ID,"", DATA_TYPE.NULL.toString()));
			}

			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY,quantity, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY_RETAIL,quantityRetail, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.QUANTITY_PACKAGE,quantityPackage, null));
			// thong tin khuyen mai
			if (discountPercentage > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.DISCOUNT_PERCENT,discountPercentage, null));
			}
			if (discountAmount > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.DISCOUNT_AMOUNT,discountAmount, null));
			}

			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.IS_FREE_ITEM,isFreeItem, null));
			// tuan them, neu sp km moi luu cac thong tin km
			if(isFreeItem == 1){
				if (!StringUtil.isNullOrEmpty(programeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
				}
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
				if (promotionOrderNumber > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
				}
				if (!StringUtil.isNullOrEmpty(programeTypeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
				}
//							if(maxQuantityFreeCK > 0){
//								detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//							}
				if(maxQuantityFree > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
				}

				if(productGroupId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
				}

				if(groupLevelId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
				}

				if(payingOrder > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PAYING_ORDER, payingOrder, null));
				}
			}
//			if (!StringUtil.isNullOrEmpty(programeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeCode)){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.JOIN_PROGRAM_CODE,programeCode, null));
//			}
//			// them vao programeType neu la mat hang khuyen mai
//			if (hasPromotion || isFreeItem == 1) {
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
//			}
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.AMOUNT,amount, null));
			// reasonCode -- chua dung nen ko can insert
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALE_ORDER_ID,salesOrderId, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE,price, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE, packagePrice, null));
			if (priceId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE_ID,priceId, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CREATE_USER,createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.UPDATE_USER,updateUser, null));
			}
			if (shopId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SHOP_ID,shopId, null));
			}
			if (staffId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.STAFF_ID,staffId, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CREATE_DATE,createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.ORDER_DATE,orderDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.UPDATE_DATE,updateDate, null));

			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRICE_NOT_VAT,priceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE_NOT_VAT, packagePriceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.TOTAL_WEIGHT,totalWeight, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.VAT,vat, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CONVFACT, convfact, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.CAT_ID, catId, null));

//			if(maxQuantityFreeCK > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//			}

			// max quantity free
			orderDetailJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM, value);

		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderDetailJson;
	}

	/**
	 *
	*  Genereate delete sale order detail
	*  @author: Nguyen Thanh Dung
	*  @return
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateDeleteOrderSql(long saleOrderId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, SALES_ORDER_DETAIL_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.SALE_ORDER_ID, String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	boolean isCloned = false;
	public long poDetailId;
	public long poId;
	public int promotionOrderNumber;
	public int catId;
	public int convfact = 1;

	/**
	*  Generate cau lenh insert - clone cho PO_CUSTOMER, PO_CUSTOMER_DETAIL
	*  @author: DungNX3
	*  @param orderId
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateCreatePOCustomerDetailSql(){
		JSONObject orderDetailJson = new JSONObject();
		try{
//			INSERT INTO
//			SALES_ORDER_DETAIL(SALES_ORDER_DETAIL_ID,PRODUCT_ID,QUANTITY,PRICE_ID,DISCOUNT_PERCENTAGE,DISCOUNT_AMOUNT,IS_FREE_ITEM,
//			PROMOTION_CODE,AMOUNT,REASON_CODE,SALE_ORDER_ID,PRICE,CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,
//			ORDER_DATE,MAX_QUANTITY_FREE) VALUES()

			orderDetailJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderDetailJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_DETAIL_ID, poDetailId,
					null));
			// detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.SALES_ORDER_DETAIL_ID,"SALES_ORDER_DETAIL_SEQ",
			// DATA_TYPE.SEQUENCE.toString()));

			if (productId > 0) {
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRODUCT_ID, productId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRODUCT_ID,"", DATA_TYPE.NULL.toString()));
			}
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY, quantity, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY_RETAIL, quantityRetail, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY_PACKAGE, quantityPackage, null));
//			if(maxQuantityFree > 0) {
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
//			}
			// thong tin khuyen mai
			if (discountPercentage > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.DISCOUNT_PERCENT,discountPercentage, null));
			}
			if (discountAmount > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.DISCOUNT_AMOUNT,discountAmount, null));
			}

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.IS_FREE_ITEM,isFreeItem, null));
//			if (!StringUtil.isNullOrEmpty(programeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeCode)){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.JOIN_PROGRAM_CODE, programeCode, null));
//			}
//			// them vao programeType neu la mat hang khuyen mai
//			if (hasPromotion || isFreeItem == 1){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
//			}

			if(isFreeItem == 1){
				if (!StringUtil.isNullOrEmpty(programeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
				}
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
				if (promotionOrderNumber > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
				}
				if (!StringUtil.isNullOrEmpty(programeTypeCode)){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
				}
//				if(maxQuantityFreeCK > 0){
//					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//				}
				if(maxQuantityFree > 0) {
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
				}

				if(productGroupId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
				}

				if(groupLevelId > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
				}

				if(payingOrder > 0){
					detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PAYING_ORDER, payingOrder, null));
				}
			}

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.AMOUNT,amount, null));
			// reasonCode -- chua dung nen ko can insert
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_ID,poId, null));

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE,price, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE, packagePrice, null));
			if (priceId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE_ID,priceId, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CREATE_USER,createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.UPDATE_USER,updateUser, null));
			}
			if (shopId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.SHOP_ID,shopId, null));
			}
			if (staffId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.STAFF_ID,staffId, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CREATE_DATE,createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.ORDER_DATE,orderDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE_NOT_VAT,priceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE_NOT_VAT, packagePriceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CAT_ID, catId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.TOTAL_WEIGHT,totalWeight, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.VAT,vat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CONVFACT, convfact, null));
//			if (promotionOrderNumber > 0) {
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeTypeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
//			}

//			if(maxQuantityFreeCK > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//			}

//			if(productGroupId > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
//			}
//
//			if(groupLevelId > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
//			}
//
//			if(payingOrder > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PAYING_ORDER, payingOrder, null));
//			}

			orderDetailJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM, value);

		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderDetailJson;
	}

	/**
	 *
	*  Genereate delete sale order detail - clone PO_CUSTOMER
	*  @author: DungNX3
	*  @return
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateDeletePODetailSql(long poCustomerId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_ID, String.valueOf(poCustomerId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	public JSONObject generateCreatePOSqlForUpdate(){
		JSONObject orderDetailJson = new JSONObject();
		try{
			orderDetailJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderDetailJson.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_DETAIL_ID, poDetailId, null));

			if(productId > 0) {
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRODUCT_ID, productId, null));
			} else {
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRODUCT_ID,"", DATA_TYPE.NULL.toString()));
			}

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY,quantity, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY_RETAIL, quantityRetail, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.QUANTITY_PACKAGE, quantityPackage, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.MAX_QUANTITY_FREE,maxQuantityFree, null));
			// thong tin khuyen mai
			if (discountPercentage > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.DISCOUNT_PERCENT,discountPercentage, null));
			}
			if (discountAmount > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.DISCOUNT_AMOUNT,discountAmount, null));
			}

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.IS_FREE_ITEM,isFreeItem, null));
//			if (!StringUtil.isNullOrEmpty(programeCode) && (hasPromotion || isFreeItem == 1)){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAM_CODE,programeCode, null));
//			}
//			if (!StringUtil.isNullOrEmpty(programeCode)){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.JOIN_PROGRAM_CODE,programeCode, null));
//			}
//			// them vao programeType neu la mat hang khuyen mai
//			if (hasPromotion || isFreeItem == 1){
//				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAM_TYPE,programeType, null));
//			}
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.AMOUNT,amount, null));
			// reasonCode -- chua dung nen ko can insert
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_DETAIL_TABLE.PO_CUSTOMER_ID, poId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE,price, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE, packagePrice, null));
			if (priceId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE_ID,priceId, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CREATE_USER,createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.UPDATE_USER,updateUser, null));
			}
			if (shopId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.SHOP_ID,shopId, null));
			}
			if (staffId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.STAFF_ID,staffId, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CREATE_DATE,createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.ORDER_DATE,orderDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.UPDATE_DATE,updateDate, null));

			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PRICE_NOT_VAT,priceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PACKAGE_PRICE_NOT_VAT, packagePriceNotVat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.TOTAL_WEIGHT,totalWeight, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.VAT,vat, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CONVFACT, convfact, null));
			detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.CAT_ID, catId, null));
			if (promotionOrderNumber > 0) {
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PROMOTION_ORDER_NUMBER, promotionOrderNumber, null));
			}
			//Loai KM
			if (!StringUtil.isNullOrEmpty(programeTypeCode) && (hasPromotion || isFreeItem == 1)){
				detailPara.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_DETAIL_TABLE.PROGRAME_TYPE_CODE, programeTypeCode, null));
			}

//			if(maxQuantityFreeCK > 0){
//				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.MAX_QUANTITY_FREE_CK, maxQuantityFreeCK, null));
//			}

			if(productGroupId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID, productGroupId, null));
			}

			if(groupLevelId > 0){
				detailPara.put(GlobalUtil.getJsonColumn(SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID, groupLevelId, null));
			}

			// max quantity free
			orderDetailJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM, value);

		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderDetailJson;
	}

	/**
	 * tinh tien san pham
	 *
	 * @author: dungdq3
	 * @since: 1:34:43 PM Apr 8, 2015
	 * @return: double
	 * @throws:
	 * @return
	 */
	public double calculateAmountProduct(){
		return (quantityRetail * price) + (quantityPackage * packagePrice);
	}

}
