/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * KeyShopRewarDetailView.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  10:29:48 21-07-2015
 */
public class KeyShopRewarDetailView extends LinearLayout implements OnClickListener {

	public static final int CLOSE_POPUP_DETAIL_PROMOTION = 1000;
	// ma clb
	TextView tvKeyshopCode;
	// chu ky
	TextView tvCycle;
	// kh
	TextView tvCustomer;
	// trang thai
	TextView tvRewad;
	// muc dang ky
	TextView tvLevelRegister;
	// ket qua
	TextView tvResult;
	// tien thuong
	TextView tvAward;
	// da tra
	TextView tvPaid;
	TextView tvText;
	// muc chuong trinh
	private DMSTableView tbLevelKeyshop;
	// button close
	public Button btCloseKeyshopDetail;

	// BaseFragment
	public BaseFragment listener;
	public View viewLayout;
	ReportKeyshopStaffItemDTO dtoData;
	Context context;

	/**
	 * @param context
	 * @param attrs
	 */
	public KeyShopRewarDetailView(Context context, BaseFragment listener) {
		super(context);
		this.context = context;

		this.listener = listener;
		LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_key_shop_reward_detail_view,
				null);
		btCloseKeyshopDetail = (Button) viewLayout.findViewById(R.id.btCloseKeyshopDetail);
		btCloseKeyshopDetail.setOnClickListener(this);

		tvKeyshopCode = (TextView) viewLayout.findViewById(R.id.tvKeyshopCode);
		tvCycle = (TextView) viewLayout.findViewById(R.id.tvCycle);
		tvCustomer = (TextView) viewLayout.findViewById(R.id.tvCustomer);
		tvRewad = (TextView) viewLayout.findViewById(R.id.tvRewad);
		tvLevelRegister = (TextView) viewLayout.findViewById(R.id.tvLevelRegister);
		tvResult = (TextView) viewLayout.findViewById(R.id.tvResult);
		tvAward = (TextView) viewLayout.findViewById(R.id.tvAward);
		tvPaid = (TextView) viewLayout.findViewById(R.id.tvPaid);
		tvText = (TextView) viewLayout.findViewById(R.id.tvText);
		tbLevelKeyshop = (DMSTableView) viewLayout.findViewById(R.id.tbLevelKeyshop);
		initHeaderTable();
	}

	/**
	 * update layout
	 * @author: yennth16
	 * @since: 19:50:57 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param dto
	 */
	public void updateLayout(ReportKeyshopStaffItemDTO dto) {
		this.dtoData = dto;
		tvKeyshopCode.setText(dto.keyshop.ksCode +" - " + dto.keyshop.name);
		tvCycle.setText(dto.cycleName);
		tvCustomer.setText(dto.customer);
		tvLevelRegister.setText(dto.levelRegister);
		if(dto.result == 5){
			tvResult.setText(StringUtil.getString(R.string.TEXT_NOT_ATTAIN));
		}else if(dto.result == 6){
			tvResult.setText(StringUtil.getString(R.string.ATTAIN));
		}else{
			tvResult.setText(Constants.STR_BLANK);
		}
		if(dto.id >  0){
			if(dto.award==0){
				tvRewad.setText(StringUtil.getString(R.string.TEXT_TRANSFER));
			}else if(dto.award==1){
				tvRewad.setText(StringUtil.getString(R.string.TEXT_LOCK));
			}else if(dto.award==2){
				tvRewad.setText(StringUtil.getString(R.string.TEXT_NOT_PAID));
			}else if(dto.award==3){
				tvRewad.setText(StringUtil.getString(R.string.TEXT_PAID_PART));
			}else if(dto.award==4){
				tvRewad.setText(StringUtil.getString(R.string.TEXT_PAID));
			}else{
				tvRewad.setText(StringUtil.getString(R.string.TEXT_KEY_SHOP_NOT_AWARD));
			}
		}else{
			tvRewad.setText(Constants.STR_BLANK);
		}
		if(dto.totalReward >0){
			StringUtil.display(tvAward, dto.totalReward);
		}else{
			tvAward.setText(Constants.STR_BLANK);
		}
		if(dto.totalRewardDone >0){
			StringUtil.display(tvPaid, dto.totalRewardDone);
		}else{
			tvPaid.setText(Constants.STR_BLANK);
		}
		renderDataTableLevel();
	}
	/**
	 * renderDataTableLevel
	 * @author: yennth16
	 * @since: 19:50:40 13-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderDataTableLevel() {
		tbLevelKeyshop.clearAllData();
		if (dtoData != null && dtoData.ksProductItem != null
				&& dtoData.ksProductItem.size() > 0) {
			tbLevelKeyshop.setVisibility(View.VISIBLE);
			tvText.setVisibility(View.VISIBLE);
			KSLevelDTO item;
			for (int i = 0, s = dtoData.ksProductItem.size(); i < s; i++) {
				item = dtoData.ksProductItem.get(i);
				KeyShopLevelRewardRow row = new KeyShopLevelRewardRow(context);
				row.render(item, i + 1);
				tbLevelKeyshop.addRow(row);
			}
		} else {
			tbLevelKeyshop.setVisibility(View.GONE);
			tvText.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View view) {
		if (view == btCloseKeyshopDetail) {
			listener.onClick(view);
		}
	}

	/**
	 * initHeaderTable
	 * @author: yennth16
	 * @since: 15:37:52 13-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initHeaderTable() {
		KeyShopLevelRewardRow header = new KeyShopLevelRewardRow(context);
		tbLevelKeyshop.addHeader(header);
	}

}
