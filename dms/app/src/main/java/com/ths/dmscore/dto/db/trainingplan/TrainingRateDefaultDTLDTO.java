package com.ths.dmscore.dto.db.trainingplan;

/**
 * 
 * TraningRateDefaultDTLDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:43:03 17-11-2014
 */
public class TrainingRateDefaultDTLDTO {
	private int trainingRateDefaultDtlId;
	private int trainingRateId;
	private String shortName;
	private String fullName;
	private int orderNumber;
	private String createUser;
	private String updateUser;
	private String createDate;
	private String updateDate;

	public int getTraningRateDefaultDtlId() {
		return trainingRateDefaultDtlId;
	}

	public void setTraningRateDefaultDtlId(int trainingRateDefaultDtlId) {
		this.trainingRateDefaultDtlId = trainingRateDefaultDtlId;
	}

	public int getTraningRateId() {
		return trainingRateId;
	}

	public void setTraningRateId(int trainingRateId) {
		this.trainingRateId = trainingRateId;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}