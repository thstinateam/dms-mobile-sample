/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.EquipmentFormHistoryDTO;

/**
 * EQUIP_FORM_HISTORY_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 16:21:38 26-12-2014
 */
public class EQUIP_FORM_HISTORY_TABLE extends ABSTRACT_TABLE {

	public static final String EQUIP_FORM_HISTORY_ID = "EQUIP_FORM_HISTORY_ID";
	public static final String RECORD_ID = "RECORD_ID";
	public static final String RECORD_TYPE = "RECORD_TYPE";
	public static final String DELIVERY_STATUS = "DELIVERY_STATUS";
	public static final String ACT_DATE = "ACT_DATE";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String RECORD_STATUS = "RECORD_STATUS";
	public static final String TABLE_NAME = "EQUIP_FORM_HISTORY";

	public EQUIP_FORM_HISTORY_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { EQUIP_FORM_HISTORY_ID, RECORD_ID,
				RECORD_TYPE, DELIVERY_STATUS, ACT_DATE, STAFF_ID, CREATE_DATE,
				CREATE_USER, UPDATE_USER, UPDATE_DATE, RECORD_STATUS, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((EquipmentFormHistoryDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Inser thiet bi bao mat mobile
	 * @author: hoanpd1
	 * @since: 09:50:05 19-12-2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long insertEquipHistoryFrom(EquipmentFormHistoryDTO dto) throws Exception {
		TABLE_ID idTable = new TABLE_ID(mDB);
		dto.equipFormHistoryId = idTable.getMaxIdTime(TABLE_NAME);
		return insert(dto);
	}

	/**
	 * @author: hoanpd1
	 * @since: 16:28:19 26-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private ContentValues initDataRow(EquipmentFormHistoryDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(EQUIP_FORM_HISTORY_ID, dto.equipFormHistoryId);
		editedValues.put(RECORD_ID, dto.recordId);
		editedValues.put(RECORD_TYPE, dto.recordType);
		// DELIVERY_STATUS = null
//		editedValues.put(DELIVERY_STATUS, dto.deliveryStatus);
		editedValues.put(RECORD_STATUS, dto.recordStatus);
		editedValues.put(ACT_DATE, dto.actDate);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		return editedValues;
	}
	

}
