/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;

/**
 * Luu thong tin ton kho
 * 
 * @author: duongdt
 * @version: 1.0
 * @since: 1.0
 */
public class WAREHOUSE_TABLE extends ABSTRACT_TABLE {
	
	public static final String TABLE_WAREHOUSE = "WAREHOUSE";
	public static final String WAREHOUSE_ID = "WAREHOUSE_ID";
	public static final String WAREHOUSE_CODE = "WAREHOUSE_CODE";
	public static final String WAREHOUSE_NAME = "WAREHOUSE_NAME";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String STATUS = "STATUS";
	public static final String WAREHOUSE_TYPE = "WAREHOUSE_TYPE";
	public static final String SEQ = "SEQ";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String WAREHOUSE_NAME_TEXT = "WAREHOUSE_NAME_TEXT";
	public static final String SYN_STATE = "SYN_STATE";

	public WAREHOUSE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_WAREHOUSE;
		this.columns = new String[] { WAREHOUSE_ID, WAREHOUSE_CODE, WAREHOUSE_NAME, SHOP_ID, DESCRIPTION, STATUS, WAREHOUSE_TYPE, SEQ, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, WAREHOUSE_NAME_TEXT, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * 
	 * @author: TruongHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(WAREHOUSE_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		StockTotalDTO cusDTO = (StockTotalDTO) dto;
		String[] params = { String.valueOf(cusDTO.stockTotalId) };
		return delete(WAREHOUSE_ID + " = ?", params);
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}
}
