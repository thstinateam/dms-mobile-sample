/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.view.CategoryCodeDTO;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ProgrameForProductDTO;
import com.ths.dmscore.dto.view.RemainProductViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;
import com.ths.dmscore.dto.view.AutoCompleteFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ListFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ListRemainProductDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MeasuringTime;
import com.ths.dms.R;

/**
 * Thong tin chi tiet don hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressLint("SimpleDateFormat")
public class SALES_ORDER_DETAIL_TABLE extends ABSTRACT_TABLE {
	// id chi tiet don hang
	public static final String SALE_ORDER_DETAIL_ID = "SALE_ORDER_DETAIL_ID";
	// ma san pham
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// so luong
	public static final String QUANTITY = "QUANTITY";
	// gia lich su
	public static final String PRICE_ID = "PRICE_ID";
	// % khuyen mai
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// so tien khuyen mai
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	// 0: hang ban, 1: hang KM
	public static final String IS_FREE_ITEM = "IS_FREE_ITEM";
	// ma chuong trinh khuyen mai
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// ma chuong trinh KM tham gia
	public static final String JOIN_PROGRAM_CODE = "JOIN_PROGRAM_CODE";
	// loai khuyen mai hay chuong trinh trung bay: 0 - CTKM tinh tu dong, 1 -
	// CTKM tinh manual, 2 - CTTB, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	// so tien
	public static final String AMOUNT = "AMOUNT";
	// id don dat hang
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	// gia ban
	public static final String PRICE = "PRICE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// ngay dat
	public static final String ORDER_DATE = "ORDER_DATE";
	// so luong KM max cua mat hang
	public static final String MAX_QUANTITY_FREE = "MAX_QUANTITY_FREE";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// Thue VAT
	public static final String VAT = "VAT";
	// Trong luong
	public static final String TOTAL_WEIGHT = "TOTAL_WEIGHT";
	// ID hoa don VAT
	public static final String INVOICE_ID = "INVOICE_ID";
	// gia chua co VAT
	public static final String PRICE_NOT_VAT = "PRICE_NOT_VAT";
	//luu loai KM (vi du ZV01, 02...)
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String PAYING_ORDER = "PAYING_ORDER";

	//public static final String MAX_QUANTITY_FREE_CK = "MAX_QUANTITY_FREE_CK";
	public static final String PROMOTION_ORDER_NUMBER = "PROMOTION_ORDER_NUMBER";

	public static final String QUANTITY_RETAIL = "QUANTITY_RETAIL";
	public static final String QUANTITY_PACKAGE = "QUANTITY_PACKAGE";
	public static final String PACKAGE_PRICE = "PACKAGE_PRICE";
	public static final String PACKAGE_PRICE_NOT_VAT = "PACKAGE_PRICE_NOT_VAT";
	public static final String CAT_ID = "CAT_ID";
	public static final String CONVFACT = "CONVFACT";

	public static final String TABLE_NAME = "SALE_ORDER_DETAIL";

	/**
	 *
	 * tao va mo mot CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: SQLiteUtil
	 * @throws:
	 */
	public SALES_ORDER_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.columns = new String[] { SALE_ORDER_DETAIL_ID, PRODUCT_ID,
				QUANTITY, PRICE_ID, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				IS_FREE_ITEM, PROGRAM_CODE, JOIN_PROGRAM_CODE, PROGRAM_TYPE, AMOUNT,
				SALE_ORDER_ID, PRICE, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, ORDER_DATE, MAX_QUANTITY_FREE, SHOP_ID, STAFF_ID,
				VAT, TOTAL_WEIGHT, INVOICE_ID, PRICE_NOT_VAT, PROGRAME_TYPE_CODE, PROMOTION_ORDER_NUMBER,
				PRODUCT_GROUP_ID, GROUP_LEVEL_ID, PAYING_ORDER, QUANTITY_RETAIL, QUANTITY_PACKAGE, PACKAGE_PRICE,
				PACKAGE_PRICE_NOT_VAT, CAT_ID, CONVFACT, SYN_STATE };
		this.tableName = TABLE_NAME;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SaleOrderDetailDTO) dto);
		return insert(null, value);
	}

	/**
	 * Thay doi 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	public int update(SaleOrderDetailDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.salesOrderDetailId, "" + dto.synState };
		return update(value, SALE_ORDER_DETAIL_ID + " = ? AND " + SYN_STATE
				+ " = ? ", params);
	}

	public long update(AbstractTableDTO dto) {
		SaleOrderDetailDTO saleOrder = (SaleOrderDetailDTO) dto;
		ContentValues value = initDataRow(saleOrder);
		String[] params = { "" + saleOrder.salesOrderDetailId,
				"" + saleOrder.synState };
		return update(value, SALE_ORDER_DETAIL_ID + " = ? AND " + SYN_STATE
				+ " = ? ", params);
	}

	public long delete(AbstractTableDTO dto) {
		SaleOrderDetailDTO saleDTO = (SaleOrderDetailDTO) dto;
		String[] params = { "" + saleDTO.salesOrderDetailId,
				"" + saleDTO.synState };
		return delete(SALE_ORDER_DETAIL_ID + " = ? AND " + SYN_STATE + " = ? ",
				params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public SaleOrderDetailDTO getRowById(String id) {
		SaleOrderDetailDTO DisplayPrdogrameLvDTO = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(SALE_ORDER_DETAIL_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				DisplayPrdogrameLvDTO = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return DisplayPrdogrameLvDTO;
	}

	/**
	 *
	 * get Detail of SaleOrder by saleOrderById.
	 *
	 * @author: thuattq
	 * @param saleOrderID
	 * @return
	 * @throws Exception
	 * @return: List<SaleOrderDetailDTO>
	 * @throws:
	 */
	public List<SaleOrderDetailDTO> getAllDetailOfSaleOrder(long saleOrderID)
			throws Exception {
		List<SaleOrderDetailDTO> listDetail = new ArrayList<SaleOrderDetailDTO>();
		Cursor c = null;

		try {
			// String[] params = { Long.toString(saleOrderID) };
//			ArrayList<String> params = new ArrayList<String>();
//			params.add(String.valueOf(saleOrderID));
//			c = query(SALE_ORDER_ID + " = ?",
//					params.toArray(new String[params.size()]), null, null, null);
			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    sod.*, 	");
			sqlObject.append("	    Group_concat(SPM.PROGRAM_CODE) PROGRAM_CODE_GROUP,	"); // CODE CTKM ma sp dat duoc
//			sqlObject.append("	    Group_concat(SPM.PROGRAM_ID) PROGRAM_ID	"); // ID CTKM ma sp dat duoc
			sqlObject.append("	    Group_concat(KS.PROGRAM_CODE) KS_PROGRAM_CODE_GROUP	"); // CODE CTKM ma sp dat duoc
//			sqlObject.append("	    Group_concat(KS.PROGRAM_ID) KS_PROGRAM_ID	"); // ID CTKM ma sp dat duoc
			sqlObject.append("	FROM	");
			sqlObject.append("	    sale_order_detail sod	");
			sqlObject.append("	LEFT JOIN (SELECT	");
			sqlObject.append("	    		PP.PROMOTION_PROGRAM_ID PROGRAM_ID,	");
			sqlObject.append("	    		PP.PROMOTION_PROGRAM_CODE PROGRAM_CODE	");
			sqlObject.append("			   FROM	");
			sqlObject.append("	   			  PROMOTION_PROGRAM PP ");
			sqlObject.append("			   WHERE	");
			sqlObject.append("	    		1 = 1  	");
			sqlObject.append("	    		) SPM ON SPM.PROGRAM_CODE = SOD.PROGRAM_CODE  AND SOD.PROGRAM_TYPE <> ?	");
			paramsObject.add(""+ PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
			sqlObject.append("	LEFT JOIN (SELECT	");
			sqlObject.append("	    		KS.KS_ID PROGRAM_ID,	");
			sqlObject.append("	    		KS.KS_CODE PROGRAM_CODE	");
			sqlObject.append("			   FROM	");
			sqlObject.append("	   			  KS  ");
			sqlObject.append("			   WHERE	");
			sqlObject.append("	    		1 = 1  	");
			sqlObject.append("	    		) KS ON KS.PROGRAM_CODE = SOD.PROGRAM_CODE AND SOD.PROGRAM_TYPE = ?	");
			paramsObject.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
			sqlObject.append("	WHERE	");
			sqlObject.append("	    1 = 1	");
			sqlObject.append("	    AND sod.sale_order_id = ?	");
			paramsObject.add(""+saleOrderID);
			sqlObject.append("	GROUP BY  sod.sale_order_detail_id ");
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleOrderDetailDTO item = initLogDTOFromCursor(c);
						// truong hop keyshop thi cap nhat lai programe_code
						if(item.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP){
							item.programeCode = CursorUtil.getString(c, "KS_PROGRAM_CODE_GROUP");
							if(!StringUtil.isNullOrEmpty(item.programeCode)){
								item.hasPromotion = true;
							}
						}
						listDetail.add(item);
					} while (c.moveToNext());
				}
			}
		}  finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception ex) {

				}
			}
		}

		return listDetail;
	}

	private SaleOrderDetailDTO initLogDTOFromCursor(Cursor c) {
		SaleOrderDetailDTO dpDetailDTO = new SaleOrderDetailDTO();

		dpDetailDTO.salesOrderDetailId = (CursorUtil.getLong(c, SALE_ORDER_DETAIL_ID));
		dpDetailDTO.productId = (CursorUtil.getInt(c, PRODUCT_ID));
		dpDetailDTO.quantity = (CursorUtil.getInt(c, QUANTITY));
		dpDetailDTO.quantityRetail = (CursorUtil.getInt(c, QUANTITY_RETAIL));
		dpDetailDTO.quantityPackage = (CursorUtil.getInt(c, QUANTITY_PACKAGE));
		dpDetailDTO.priceId = (CursorUtil.getLong(c, PRICE_ID));
		dpDetailDTO.discountPercentage = (CursorUtil.getDouble(c, DISCOUNT_PERCENT));
		dpDetailDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));

		dpDetailDTO.isFreeItem = (CursorUtil.getInt(c, IS_FREE_ITEM));
		dpDetailDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		dpDetailDTO.salesOrderId = (CursorUtil.getLong(c, SALE_ORDER_ID));
		dpDetailDTO.price = (CursorUtil.getDouble(c, PRICE));
		dpDetailDTO.packagePrice = (CursorUtil.getDouble(c, PACKAGE_PRICE));

		dpDetailDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		dpDetailDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dpDetailDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dpDetailDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dpDetailDTO.orderDate = (CursorUtil.getString(c, ORDER_DATE));
		dpDetailDTO.synState = (CursorUtil.getInt(c, SYN_STATE));
		dpDetailDTO.maxQuantityFree = (CursorUtil.getInt(c, MAX_QUANTITY_FREE));
		//dpDetailDTO.maxQuantityFreeCK = (CursorUtil.getInt(c, MAX_QUANTITY_FREE_CK));
//		dpDetailDTO.maxAmountFree = (CursorUtil.getInt(c, MAX_AMOUNT_FREE));
		dpDetailDTO.programeTypeCode = CursorUtil.getString(c, PROGRAME_TYPE_CODE);
		dpDetailDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		dpDetailDTO.programeCode = CursorUtil.getString(c, "PROGRAM_CODE_GROUP");
		dpDetailDTO.staffId = CursorUtil.getInt(c, STAFF_ID);
		dpDetailDTO.shopId = CursorUtil.getInt(c, SHOP_ID);
		dpDetailDTO.vat = CursorUtil.getDouble(c, VAT);
		dpDetailDTO.priceNotVat = CursorUtil.getLong(c, PRICE_NOT_VAT);
		dpDetailDTO.packagePriceNotVat = CursorUtil.getLong(c, PACKAGE_PRICE_NOT_VAT);
		dpDetailDTO.catId = CursorUtil.getInt(c, CAT_ID);
		dpDetailDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		dpDetailDTO.convfact = CursorUtil.getInt(c, CONVFACT);

		dpDetailDTO.promotionOrderNumber = CursorUtil.getInt(c, PROMOTION_ORDER_NUMBER);
		dpDetailDTO.productGroupId = CursorUtil.getLong(c, PRODUCT_GROUP_ID);
		dpDetailDTO.groupLevelId = CursorUtil.getLong(c, GROUP_LEVEL_ID);

		if(!StringUtil.isNullOrEmpty(dpDetailDTO.programeCode)){
			dpDetailDTO.hasPromotion = true;
		}
		return dpDetailDTO;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayPrdogrameLvDTO>
	 * @throws:
	 */
	public Vector<SaleOrderDetailDTO> getAllRow() {
		Vector<SaleOrderDetailDTO> v = new Vector<SaleOrderDetailDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				SaleOrderDetailDTO DisplayPrdogrameLvDTO;
				if (c.moveToFirst()) {
					do {
						DisplayPrdogrameLvDTO = initLogDTOFromCursor(c);
						v.addElement(DisplayPrdogrameLvDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(SaleOrderDetailDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(SALE_ORDER_DETAIL_ID, dto.salesOrderDetailId);
		if (dto.productId > 0) {
			editedValues.put(PRODUCT_ID, dto.productId);
		} else {
			String temp = null;
			editedValues.put(PRODUCT_ID, temp);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(QUANTITY_RETAIL, dto.quantityRetail);
		editedValues.put(QUANTITY_PACKAGE, dto.quantityPackage);
		editedValues.put(PRICE_ID, dto.priceId);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercentage);
		editedValues.put(DISCOUNT_AMOUNT, dto.getDiscountAmount());

		editedValues.put(IS_FREE_ITEM, dto.isFreeItem);
		//Sp ban duoc huong KM hoac sp KM
//		if (!StringUtil.isNullOrEmpty(dto.programeCode) && (dto.hasPromotion || dto.isFreeItem == 1)){
//			editedValues.put(PROGRAM_CODE, dto.programeCode);
//		}
//		editedValues.put(JOIN_PROGRAM_CODE, dto.programeCode);
//		//Sp ban duoc huong KM hoac sp KM
//		if (dto.hasPromotion || dto.isFreeItem == 1) {
//			editedValues.put(PROGRAM_TYPE, dto.programeType);
//		}
		if(dto.isFreeItem == 1){
			if (!StringUtil.isNullOrEmpty(dto.programeCode)){
				editedValues.put(PROGRAM_CODE, dto.programeCode);
			}
			editedValues.put(PROGRAM_TYPE, dto.programeType);
			if (dto.promotionOrderNumber > 0) {
				editedValues.put(PROMOTION_ORDER_NUMBER, dto.promotionOrderNumber);
			}
			if (!StringUtil.isNullOrEmpty(dto.programeTypeCode)){
				editedValues.put(PROGRAME_TYPE_CODE, dto.programeTypeCode);
			}
			if(dto.maxQuantityFree > 0) {
				editedValues.put(MAX_QUANTITY_FREE, dto.maxQuantityFree);
			}

			if(dto.productGroupId > 0) {
				editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
			}

			if(dto.groupLevelId > 0) {
				editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);
			}

			if(dto.payingOrder > 0) {
				editedValues.put(PAYING_ORDER, dto.payingOrder);
			}
		}
		editedValues.put(AMOUNT, dto.getAmount());
		editedValues.put(SALE_ORDER_ID, dto.salesOrderId);
		editedValues.put(PRICE, dto.price);
		editedValues.put(PACKAGE_PRICE, dto.packagePrice);

		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(ORDER_DATE, dto.orderDate);

		editedValues.put(SYN_STATE, dto.synState);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(VAT, dto.vat);
		editedValues.put(PRICE_NOT_VAT, dto.priceNotVat);
		editedValues.put(PACKAGE_PRICE_NOT_VAT, dto.packagePriceNotVat);
		editedValues.put(CAT_ID, dto.catId);
		editedValues.put(TOTAL_WEIGHT, dto.totalWeight);
		editedValues.put(CONVFACT, dto.convfact);
		//Sp ban duoc huong KM hoac sp KM
//		if (!StringUtil.isNullOrEmpty(dto.programeTypeCode) && (dto.hasPromotion || dto.isFreeItem == 1)){
//			editedValues.put(PROGRAME_TYPE_CODE, dto.programeTypeCode);
//		}
//		if(dto.maxQuantityFreeCK > 0){
//			editedValues.put(MAX_QUANTITY_FREE_CK, dto.maxQuantityFreeCK);
//		}

//		if(dto.productGroupId > 0) {
//			editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
//		}
//
//		if(dto.groupLevelId > 0) {
//			editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);
//		}
//
//		if(dto.payingOrder > 0) {
//			editedValues.put(PAYING_ORDER, dto.payingOrder);
//		}

		return editedValues;
	}

	/**
	 *
	 * Xoa cac chi tiet dat hang cu~
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderID
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int deleteAllDetailOfOrder(long orderID) {
		// String[] params = { "" + orderID };
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(orderID));

		return delete(SALE_ORDER_ID + " = ? ",
				params.toArray(new String[params.size()]));
	}

	/**
	 *
	 * get init cac textview de thuc hien autoComplete
	 *
	 * @param data
	 * @return
	 * @return: AutoCompleteFindProductSaleOrderDetailViewDTO
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 16, 2013
	 */
	public AutoCompleteFindProductSaleOrderDetailViewDTO getInitListProductAddToOrderView(
			Bundle data) throws Exception{
//		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String ext = data.getString(IntentConstants.INTENT_PAGE);
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		// gen bang gia
		SQLUtils.getInstance().genPriceTemp(data);
		// ket thuc gen bang gia

		// String productCode =
		// data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		// String productName =
		// data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		String orderType = data.getString(IntentConstants.INTENT_ORDER_TYPE);

		// String ctkmCode = data.getString(IntentConstants.INTENT_CTKM_CODE);

		// productCode = StringUtil.toOracleSearchText(productCode,
		// false).trim();
		// productName = StringUtil.toOracleSearchText(productName,
		// false).trim();
		// ctkmCode = StringUtil.toOracleSearchText(ctkmCode, false).trim();
		//
		// productCode = "%" + productCode + "%";
		// productName = "%" + productName + "%";
		// ctkmCode = "%" + ctkmCode + "%";

		ArrayList<String> listParams = new ArrayList<String>();
		/**
		 * SQL lay danh sach product code
		 */
		StringBuffer sql = new StringBuffer();
		// version 2
		sql.append("SELECT DISTINCT p.product_code      PRODUCT_CODE, ");
		sql.append("                p.product_name      PRODUCT_NAME, ");
		sql.append("                p.product_name_text PRODUCT_NAME_TEXT ");
		sql.append("FROM   (SELECT p.product_id   product_id, ");
		sql.append("               p.product_code product_code, ");
		sql.append("               p.product_name product_name, ");
		sql.append("               p.name_text    product_name_text, ");
		sql.append("               p.cat_id       cat_id, ");
		sql.append("               p.uom2         uom2, ");
		sql.append("               p.sub_cat_id        SUB_CAT_ID ");
		sql.append("        FROM   product p ");
		sql.append("        WHERE  p.status = 1) p ");

		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			sql.append("       LEFT ");
		}
		sql.append("        JOIN (");

		//doi cau lay ton kho (nhiu kho)
		sql.append("       SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sql.append("       FROM stock_total st ");
		sql.append("       LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("       WHERE 1=1 ");
		sql.append("    		AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		listParams.add(shopId);
		sql.append("        	AND st.object_id = ? AND st.status = 1 ");
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			listParams.add(shopId);
		} else {
			listParams.add(staffId);
		}
		sql.append("       		AND st.object_type = ? ");
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			listParams.add(String.valueOf(StockTotalDTO.TYPE_SHOP));
		} else {
			listParams.add(String.valueOf(StockTotalDTO.TYPE_VANSALE));
		}
		sql.append("       group by st.product_id ");
		//end lay ton kho (nhiu kho)

		sql.append("               ) st ");
		sql.append("               ON st.product_id = p.product_id ");
		sql.append("       LEFT JOIN (");

		//begin lay gia theo cach moi (nhiu gia)
		sql.append("	SELECT * FROM PRICE_TEMP	");
		sql.append("	) pri_id ON p.product_id = pri_id.product_id	");
		sql.append("    left JOIN PRICE pr_ ON pri_id.price_id = pr_.price_id, ");
		//end lay gia theo cach moi (nhiu gia)

		sql.append("       product_info pi, ap_param ap ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND pi.status = 1 ");
		sql.append("       AND pi.type = 1 ");
		sql.append("       AND pi.product_info_id = p.cat_id ");
		sql.append("       AND p.uom2 = ap.ap_param_code ");
		sql.append("       AND ap.status = 1 ");
		sql.append("       AND ap.type = 'UOM' ");
		sql.append("       AND pi.product_info_id = p.cat_id ");

		//chi load nhung sp co nganh hang ma nhan vien duoc khai bao
		sql.append("		AND (	");
		sql.append("	    CASE	");
		sql.append("	        WHEN (	");
		sql.append("	            SELECT	");
		sql.append("	                COUNT (1)	");
		sql.append("	            FROM	");
		sql.append("	                staff_sale_cat ssc	");
		sql.append("	            WHERE	");
		sql.append("	                ssc.staff_id = ?	");
		listParams.add(staffId);
		sql.append("	        ) = 0 THEN 1	");
		sql.append("	        ELSE P.SUB_CAT_ID IN (	");
		sql.append("	            SELECT	");
		sql.append("	                CAT_ID	");
		sql.append("	            FROM	");
		sql.append("	                staff_sale_cat ssc	");
		sql.append("	            WHERE	");
		sql.append("	                ssc.staff_id = ?	");
		listParams.add(staffId);
		sql.append("	        )	");
		sql.append("	    END	");
		sql.append("	)	");

		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)){
			sql.append("              AND  ST.quantity > 0 ");
			sql.append("              AND  ST.available_quantity > 0 ");
		}

		//Sp co khai bao gia thi moi load len
//		sql.append("       and pr_.price_id > 0 and PR_.price >= 0 ");

//		sql.append("       AND (PI.product_info_code = 'Z' OR (PI.product_info_code != 'Z' AND PR_.price > 0)) ");
		sql.append("ORDER  BY product_code ASC");

		AutoCompleteFindProductSaleOrderDetailViewDTO listResult = new AutoCompleteFindProductSaleOrderDetailViewDTO();
		Cursor c = null;
		try {
			c = rawQuery(sql.toString() + ext, listParams.toArray(new String[listParams.size()]));
			if (c != null) {

				if (c.moveToFirst()) {
					do {
						listResult.init(c);
					} while (c.moveToNext());
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listResult;
	}

	/**
	 *
	 * get list product to add order
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public ListFindProductSaleOrderDetailViewDTO getListProductAddToOrderView(Bundle data) throws Exception{
		MeasuringTime.getStartTimeParser();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String productCode = data.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data.getString(IntentConstants.INTENT_PRODUCT_NAME);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		String strListShop = TextUtils.join(",", listShopId);

		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		String customerTypeId = data.getString(IntentConstants.INTENT_CUSTOMER_TYPE_ID);
		String saleTypeCode = data.getString(IntentConstants.INTENT_SALE_TYPE_CODE);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String listProductNotIn = data.getString(IntentConstants.INTENT_LIST_PRODUCT_NOT_IN);
//		int staffType = data.getInt(IntentConstants.INTENT_STAFF_TYPE);
		String orderType = data.getString(IntentConstants.INTENT_ORDER_TYPE);
		boolean isGetCount = data.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);

		boolean hasProductCode = !StringUtil.isNullOrEmpty(productCode);
		boolean hasProductName = !StringUtil.isNullOrEmpty(productName);
		boolean hasListProductNotIn = !StringUtil.isNullOrEmpty(listProductNotIn);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String productInfoCat = data.getString(IntentConstants.INTENT_PRODUCT_CAT);
		String productInfoSubCat = data.getString(IntentConstants.INTENT_PRODUCT_SUB_CAT);
		// productCode = StringUtil.toOracleSearchText(productCode,
		// false).trim();
		// productName = StringUtil.toOracleSearchText(productName,
		// false).trim();

		// productCode = "%" + productCode + "%";
		// productName = "%" + productName + "%";

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils.sqlEscapeString("%" + productCode + "%");
			productCode = productCode.substring(1, productCode.length() - 1);
		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils.sqlEscapeString("%" + productName + "%");
			productName = productName.substring(1, productName.length() - 1);
		}

		/**
		 * SQL lay danh sach san pham
		 */
		// khoi tao tham so sql lay danh sach
		String[] paramsList = new String[] {};
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT p.PRODUCT_ID PRODUCT_ID, ");
		sql.append("                pr_.PRICE_ID PRICE_ID, ");
		sql.append("                pr_.PRICE PRICE, ");
		sql.append("                pr_.PACKAGE_PRICE PACKAGE_PRICE, ");
		sql.append("                pr_.VAT VAT, ");
		sql.append("                pr_.PRICE_NOT_VAT PRICE_NOT_VAT, ");
		sql.append("                pr_.PACKAGE_PRICE_NOT_VAT PACKAGE_PRICE_NOT_VAT, ");
		sql.append("                p.GROSS_WEIGHT GROSS_WEIGHT, ");
		sql.append("                p.CONVFACT CONVFACT, ");
		sql.append("                ap.ap_param_name UOM2, ");
		sql.append("                p.cat_id CAT_ID, ");
		sql.append("                ST.QUANTITY QUANTITY, ");
		sql.append("                ST.AVAILABLE_QUANTITY AVAILABLE_QUANTITY, ");

		//BangHN: kiem tra co stock total hay ko de order
		sql.append("                (CASE WHEN ST.quantity IS NULL THEN 0 ");
		sql.append("                	ELSE 1 ");
		sql.append("               	 END ) AS HAVE_STOCK_TOTAL, ");

		sql.append("                p.PRODUCT_NAME PRODUCT_NAME, ");
		sql.append("                p.PRODUCT_CODE PRODUCT_CODE, ");
		sql.append("                pi.PRODUCT_INFO_CODE PRODUCT_INFO_CODE, ");
		sql.append("                1              AS DISPLAY_PROGRAME_CODE, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN fp.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )        AS TT ");
//		sql.append("                ( CASE ");
//		sql.append("                    WHEN TP.tpd_id IS NULL THEN 0 ");
//		sql.append("                    ELSE 1 ");
//		sql.append("                  END )        AS GSNPP_REQUEST ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO1)       AS COKHAIBAO1 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO2)       AS COKHAIBAO2 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO3)      AS COKHAIBAO3 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO4)       AS COKHAIBAO4 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO5)       AS COKHAIBAO5 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO6)       AS COKHAIBAO6 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO7)       AS COKHAIBAO7 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO8)       AS COKHAIBAO8 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO9)       AS COKHAIBAO9 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.COKHAIBAO10)       AS COKHAIBAO10 ");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.PROMOTION_PROGRAM_CODE) PROMOTION_PROGRAM_CODE");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.PROMOTION_PROGRAM_ID) PROMOTION_PROGRAM_ID");
		sql.append("                ,GROUP_CONCAT(NEW_PROMOTION.TYPE) TYPE");

		// check bang product & product info
		sql.append("        FROM   (select * from product where status = 1) p ");

		// check bang stock total
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			sql.append("       LEFT ");
		}
		sql.append("       JOIN ( ");

		//begin doi cau lay ton kho (nhiu kho)
		sql.append("	SELECT	");
		sql.append("	    SUM(st.quantity) as QUANTITY,	");
		sql.append("	    SUM(st.available_quantity) AVAILABLE_QUANTITY,	");
		sql.append("	    st.product_id product_id	");
		sql.append("	FROM	");
		sql.append("	    stock_total st	");
		sql.append("	LEFT JOIN	");
		sql.append("	    warehouse wh	");
		sql.append("	        ON (	");
		sql.append("	            st.object_type = 1	");
		sql.append("	            AND wh.warehouse_id = st.warehouse_id	");
		sql.append("	            AND wh.status = 1	");
		sql.append("	        )	");
		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND (	");
		sql.append("	        (	");
		sql.append("	            st.shop_id = ?	");
		params.add(shopId);
		sql.append("	            AND st.object_type = 2	");
		sql.append("	        )	");
		sql.append("	        OR (	");
		sql.append("	            st.object_type = 1	");
		sql.append("	            AND ifnull(wh.warehouse_id, '') <> ''	");
		sql.append("	        )	");
		sql.append("	    )	");
		sql.append("	    AND st.object_id = ?	");
		sql.append("	    AND st.status = 1	");

		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			params.add(shopId);
		} else {
			params.add(staffId);
		}
		sql.append("       		AND st.object_type = ? ");
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			params.add(String.valueOf(StockTotalDTO.TYPE_SHOP));
		} else {
			params.add(String.valueOf(StockTotalDTO.TYPE_VANSALE));
		}
		sql.append("       group by st.product_id ");
		//end doi cau lay ton kho (nhiu kho)

		sql.append("              ) st ");
		sql.append("               ON st.product_id = p.product_id ");

		//de left join de co the lay duoc mat hang Z khong co gia, nhung phai co diu kien lc khac Z
		sql.append(" LEFT JOIN ( ");

		//begin lay gia theo cach moi (nhiu gia)
		sql.append("	SELECT * FROM PRICE_TEMP	");
		sql.append("	) pri_id ");
		// mac dinh khi load thi khong load nganh Z
		// nhung khi tim kiem thi phai co Z
//		sql.append("	ON  p.product_id = pri_id.pri_product_id AND pri_id.price_id is not null ");
		sql.append("	ON  p.product_id = pri_id.product_id ");
		sql.append("   LEFT JOIN PRICE AS pr_ ON pri_id.price_id = pr_.price_id ");
		//end lay gia theo cach moi (nhiu gia)

		// mat hang trong tam
		sql.append("   LEFT JOIN ( SELECT DISTINCT ");
		sql.append("       fcmd.product_id, ");
		sql.append("       fp.focus_program_id focus_program_code, ");
		sql.append("       AP.VALUE VALUE_INDEX ");
		sql.append("FROM   focus_program fp, ");
		sql.append("       focus_shop_map fsm, ");
		sql.append("       focus_channel_map fcm, ");
		sql.append("       focus_channel_map_product fcmd, ");
		sql.append("       AP_PARAM AP ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND fp.status = 1 ");
		sql.append("       AND fsm.status = 1 ");
		sql.append("       AND fcm.status = 1 ");
		sql.append("       AND fcm.sale_type_code = ? ");
		params.add(saleTypeCode);
		sql.append("       AND fp.focus_program_id = fsm.focus_program_id ");
		sql.append("       AND fp.focus_program_id = fcm.focus_program_id ");
		sql.append("       AND fcm.focus_channel_map_id = fcmd.focus_channel_map_id ");
		sql.append("       AND substr(fp.from_date, 1, 10) <= ?");
		sql.append("       AND ifnull(substr(fp.to_date, 1, 10) >= ?, 1)");
		params.add(dateNow);
		params.add(dateNow);
		sql.append("       AND fsm.shop_id IN ( ");
		sql.append(strListShop + ") ");
		sql.append("       AND AP.TYPE = 'FOCUS_PRODUCT_TYPE' ");
		sql.append("       AND AP.AP_PARAM_CODE = fcmd.TYPE ");
		sql.append("        ) AS FP");
		sql.append("              ON p.product_id = FP.product_id ");

		// Mat hang GSNPP yeu cau ban
//		sql.append("	LEFT JOIN	");
//		sql.append("	(	");
//		sql.append("	    SELECT	");
//		sql.append("	        fbd.product_id product_id,	");
//		sql.append("	        fb.training_plan_detail_id TPD_ID	");
//		sql.append("	    FROM	");
//		sql.append("	        feedback fb,	");
//		sql.append("	        feedback_detail fbd,	");
//		sql.append("	        ap_param ap	");
//		sql.append("	    WHERE	");
//		sql.append("	        ap.type = 'GSNPP_NUM_DAY_SKU'	");
//		sql.append("	        AND ap.status = 1	");
//		sql.append("	        AND fb.staff_id = ?	");
//		params.add(staffId);
//		sql.append("	        AND fb.customer_id = ?	");
//		params.add(customerId);
//		sql.append("	        AND fb.shop_id = ?	");
//		params.add(shopId);
//		sql.append("	        AND fbd.feedback_id = fb.feedback_id	");
//		sql.append("	        AND substr(fb.create_date, 1, 10) >= dayInOrder( ?, '-' || ap.ap_param_code || ' DAY')	");
//		params.add(dateNow);
//		sql.append("	) AS tp	");
//		sql.append("	    ON tp.product_id = p.product_id	");


		// Start check KM
		sql.append("               LEFT JOIN ( ");
		sql.append("                SELECT ");
		sql.append("                PP.COKHAIBAO1       AS COKHAIBAO1 ");
		sql.append("                ,PP.COKHAIBAO2       AS COKHAIBAO2 ");
		sql.append("                ,PP.COKHAIBAO3       AS COKHAIBAO3 ");
		sql.append("                ,PP.COKHAIBAO4       AS COKHAIBAO4 ");
		sql.append("                ,PP.COKHAIBAO5       AS COKHAIBAO5 ");
		sql.append("                ,PP.COKHAIBAO6       AS COKHAIBAO6 ");
		sql.append("                ,PP.COKHAIBAO7       AS COKHAIBAO7 ");
		sql.append("                ,PP.COKHAIBAO8       AS COKHAIBAO8 ");
		sql.append("                ,PP.COKHAIBAO9       AS COKHAIBAO9 ");
		sql.append("                ,PP.COKHAIBAO10       AS COKHAIBAO10 ");
		sql.append("                ,PP.PROMOTION_PROGRAM_CODE PROMOTION_PROGRAM_CODE ");
		sql.append("                ,PP.PROMOTION_PROGRAM_ID PROMOTION_PROGRAM_ID ");
		sql.append("                ,PP.TYPE TYPE ");
		sql.append("                ,GLD.PRODUCT_ID ");
		sql.append("                FROM ");

		sql.append("               		(SELECT pp.PROMOTION_PROGRAM_CODE       , ");
		sql.append("                                          pp.PROMOTION_PROGRAM_ID, ");
		sql.append("                                          pp.TYPE, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 2 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 2 ");
		sql.append("                                                   AND pcad.object_id = ? ");
		params.add(customerTypeId);
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad, ");
		sql.append("                                                   sale_level_cat slc, ");
		sql.append("                                                   customer_cat_level ccl ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND slc.status = 1 ");
		sql.append("                                                   AND ccl.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_detail CAD ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND ( CASE ");
		sql.append("                                                           WHEN CA.type = 2 THEN CAD.value IS NOT NULL AND Ifnull(Cast(CAD.value AS INTEGER) >= pca.from_value, 1) ");
		sql.append("                                                                                       AND Ifnull(Cast(CAD.value AS INTEGER) <= pca.to_value, 1) ");
		sql.append("                                                           WHEN CA.type = 1 THEN Trim(CAD.value) = Trim(pca.from_value) ");
		sql.append("                                                           WHEN CA.type = 3 THEN CAD.value IS NOT NULL ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) >= substr(pca.from_value, 1, 10), 1) ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) <= substr(pca.to_value, 1, 10), 1) ");
		sql.append("                                                         END ) ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = CAD.customer_attribute_enum_id ");
		sql.append("                                                   AND CAD.customer_attribute_detail_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");

		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                                                   ,(SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//			sql.append("                                                   (SELECT Count(1)                      ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//			params.add(staffId);
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                        FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//			params.add(customerId);
//		}

		sql.append("                   FROM       (select promotion_program_id, promotion_program_code, type from promotion_program ");
		sql.append("                                  where 1 = 1 ");
		sql.append("                                  AND substr(from_date, 1, 10) <= ? ");
		sql.append("                                  AND Ifnull (substr(to_date, 1, 10) >= ?, 1) ");
		sql.append("                                  and status = 1 ");
		sql.append("                                  )pp,  ");
		params.add(dateNow);
		params.add(dateNow);
		sql.append("       promotion_shop_map psm ");
		sql.append("		WHERE	");
		sql.append("			1 = 1	");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append("       AND psm.shop_id IN ( ");
		sql.append(strListShop + " )");
		sql.append("       AND psm.status = 1 ");
		sql.append("       AND substr(psm.from_date, 1, 10) <= ? ");
		sql.append("       AND Ifnull (substr(psm.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
		params.add(dateNow);
		//Lay ds CT co KM loai KH cua KH
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO1 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO2 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO3 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO4 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO5 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO6 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO7 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO8 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO9 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO10 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO3 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO4 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO5 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO6 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO7 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO8 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO9 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO10 > 0 ");
		sql.append("              END) ");

		//Check customer map & staff map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("              AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//			sql.append("              AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//		}

		sql.append("              GROUP BY pp.PROMOTION_PROGRAM_CODE, ");
		sql.append("              pp.PROMOTION_PROGRAM_ID, ");
		sql.append("              pp.TYPE) pp ");

		sql.append("			,PRODUCT_GROUP PG,	");
		sql.append("			GROUP_LEVEL GLP,	");
		sql.append("			GROUP_LEVEL GLC,	");
		sql.append("			GROUP_LEVEL_DETAIL GLD	");
		sql.append("		WHERE	");
		sql.append("			1 = 1	");
		sql.append("			AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID	");
		sql.append("			AND PG.PRODUCT_GROUP_ID = GLP.PRODUCT_GROUP_ID	");
		sql.append("			AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
		sql.append("			AND GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
		sql.append("			AND PG.GROUP_TYPE = 1	");
		sql.append("			AND GLP.HAS_PRODUCT = 1	");
		sql.append("			AND GLC.HAS_PRODUCT = 1	");
		sql.append("			AND PG.STATUS = 1	");
		sql.append("			AND GLP.STATUS = 1	");
		sql.append("			AND GLC.STATUS = 1	");
		sql.append("			AND GLD.STATUS = 1	");

		sql.append("            GROUP BY pp.PROMOTION_PROGRAM_CODE, ");
		sql.append("            pp.PROMOTION_PROGRAM_ID, ");
		sql.append("            pp.TYPE, ");
		sql.append("            GLD.PRODUCT_ID ");
		sql.append("             ) NEW_PROMOTION on NEW_PROMOTION.product_id = p.product_id ");

		// dk where
		sql.append(" , PRODUCT_INFO pi, ap_param ap ");
		sql.append("WHERE  1 = 1 ");
		sql.append(" AND p.STATUS = 1 ");
		sql.append(" AND ap.status = 1 ");
		sql.append(" AND ap.ap_param_code = p.uom2 ");
		sql.append(" AND ap.type = 'UOM' ");
		sql.append(" AND pi.STATUS = 1 ");
		sql.append(" AND pi.TYPE = 1 ");
		sql.append(" AND pi.PRODUCT_INFO_ID = p.CAT_ID ");
//		sql.append(" AND pi.PRODUCT_INFO_ID = p.sub_cat_id ");
		if(!StringUtil.isNullOrEmpty(productInfoCat)){
			sql.append(" AND pi.PRODUCT_INFO_CODE = ? ");
			params.add(productInfoCat);
		}
		if(!StringUtil.isNullOrEmpty(productInfoSubCat)){
			sql.append(" and exists (SELECT 1 ");
			sql.append("                  FROM   product_info pit ");
			sql.append("                  WHERE  1 = 1 ");
			sql.append("                         AND pit.type = 2 ");
			sql.append(" AND pit.PRODUCT_INFO_CODE = ? and pit.product_info_id = p.sub_cat_id) ");
			params.add(productInfoSubCat);
		}


		//chi load nhung sp co nganh hang ma nhan vien duoc khai bao
		sql.append(" AND (	");
		sql.append("	    CASE	");
		sql.append("	        WHEN (	");
		sql.append("	            SELECT	");
		sql.append("	                COUNT (1)	");
		sql.append("	            FROM	");
		sql.append("	                staff_sale_cat ssc	");
		sql.append("	            WHERE	");
		sql.append("	                ssc.staff_id = ?	");
		params.add(staffId);
		sql.append("	        ) = 0 THEN 1	");
		// sua cat_id thanh sub_cat_id
		sql.append("	        ELSE P.SUB_CAT_ID IN (	");
		sql.append("	            SELECT	");
		sql.append("	                CAT_ID	");
		sql.append("	            FROM	");
		sql.append("	                staff_sale_cat ssc	");
		sql.append("	            WHERE	");
		sql.append("	                ssc.staff_id = ?	");
		params.add(staffId);
		sql.append("	        )	");
		sql.append("	    END	");
		sql.append("	)	");

		if (hasListProductNotIn) {
			sql.append(" AND p.product_code NOT IN ( " + listProductNotIn + ")");
		}
		if (hasProductCode) {
			sql.append("AND upper(p.product_code) LIKE upper(?) escape '^'");
			params.add(productCode);
		}
		if (hasProductName) {
			sql.append("AND upper(p.name_text) LIKE upper(?) escape '^' ");
			params.add(productName);
		}
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)){
			sql.append("            AND    ST.quantity > 0 ");
			sql.append("            AND    ST.available_quantity > 0 ");
		}

		sql.append("GROUP BY p.PRODUCT_ID	");
//		sql.append("ORDER BY HAVE_STOCK_TOTAL desc, tt DESC, gsnpp_request DESC, product_code ASC, product_name ASC");
//		String defaultOrderByStr = "ORDER BY HAVE_STOCK_TOTAL desc, tt DESC, gsnpp_request DESC, product_code ASC, product_name ASC";
		String defaultOrderByStr = "ORDER BY case when fp.VALUE_INDEX is null then 1 else 0 end,P.ORDER_INDEX,HAVE_STOCK_TOTAL desc, tt DESC, product_code ASC, product_name ASC";
		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.PRODUCT_CODE, "PRODUCT_CODE")
				.addMapper(SortActionConstants.PRODUCT_NAME, "PRODUCT_NAME")
				.addMapper(SortActionConstants.PRODUCT_STOCK_TOTAL, "HAVE_STOCK_TOTAL")
				.addMapper(SortActionConstants.PROMOTION, "PROMOTION_PROGRAM_CODE")
				.defaultOrderString(defaultOrderByStr).build(sortInfo);

		// add order string
		sql.append(orderByStr);
		//lay tong so luong dong
		String[] paramsTotal = new String[] {};
		ArrayList<String> totalParams = new ArrayList<String>();

		StringBuffer sql2 = new StringBuffer();
		sql2.append("SELECT Distinct p.product_id AS PRODUCT_ID ");

		// check bang product & product info
		sql2.append(" FROM (select * from product where status = 1) p ");

		//hien tai stock-total dang lay theo left join nen ko can
		//check dk stock-total
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			sql2.append("       LEFT ");
		}
		sql2.append("   JOIN ( ");
		//doi cau lay ton kho (nhieu kho)
		sql2.append("	SELECT	");
		sql2.append("	    SUM(st.quantity) as QUANTITY,	");
		sql2.append("	    SUM(st.available_quantity) AVAILABLE_QUANTITY,	");
		sql2.append("	    st.product_id product_id	");
		sql2.append("	FROM	");
		sql2.append("	    stock_total st	");
		sql2.append("	LEFT JOIN	");
		sql2.append("	    warehouse wh	");
		sql2.append("	        ON (	");
		sql2.append("	            st.object_type = 1	");
		sql2.append("	            AND wh.warehouse_id = st.warehouse_id	");
		sql2.append("	            AND wh.status = 1	");
		sql2.append("	        )	");
		sql2.append("	WHERE	");
		sql2.append("	    1=1	");
		sql2.append("	    AND (	");
		sql2.append("	        (	");
		sql2.append("	            st.shop_id = ?	");
		totalParams.add(shopId);
		sql2.append("	            AND st.object_type = 2	");
		sql2.append("	        )	");
		sql2.append("	        OR (	");
		sql2.append("	            st.object_type = 1	");
		sql2.append("	            AND ifnull(wh.warehouse_id, '') <> ''	");
		sql2.append("	        )	");
		sql2.append("	    )	");
		sql2.append("	    AND st.object_id = ?	");
		sql2.append("	    AND st.status = 1	");

		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			totalParams.add(shopId);
		} else {
			totalParams.add(staffId);
		}
		sql2.append("       		AND st.object_type = ? ");
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
			totalParams.add(String.valueOf(StockTotalDTO.TYPE_SHOP));
		} else {
			totalParams.add(String.valueOf(StockTotalDTO.TYPE_VANSALE));
		}
		sql2.append("       group by st.product_id ");
		sql2.append("                          	) st ");
		sql2.append("               ON st.product_id = p.product_id ");

		//de left join de co the lay duoc mat hang Z khong co gia, nhung phai co dieu kien khac Z
		sql2.append(" LEFT JOIN ( ");

		//begin lay gia theo cach moi (nhiu gia)
		sql2.append("	SELECT * FROM PRICE_TEMP	");
		sql2.append("	) pri_id ");
		// ket thuc lay theo gia moi
		// mac dinh khi load thi khong load nganh Z
		// nhung khi tim kiem thi phai co Z
//		sql2.append("	ON  p.product_id = pri_id.pri_product_id AND pri_id.price_id is not null ");
		sql2.append("	ON  p.product_id = pri_id.product_id ");
		sql2.append("   LEFT JOIN PRICE AS pr_ ON pri_id.price_id = pr_.price_id ");
		//end lay gia theo cach moi (nhiu gia)

		sql2.append(" , PRODUCT_INFO pi, ap_param ap ");
		sql2.append("WHERE  1 = 1 ");
		sql2.append(" AND p.STATUS = 1 ");
		sql2.append(" AND ap.status = 1 ");
		sql2.append(" AND ap.ap_param_code = p.uom2 ");
		sql2.append(" AND ap.type = 'UOM' ");
		sql2.append(" AND pi.STATUS = 1 ");
		sql2.append(" AND pi.TYPE = 1 ");
		sql2.append(" AND pi.PRODUCT_INFO_ID = p.CAT_ID ");
//		sql2.append(" AND pi.PRODUCT_INFO_ID = p.sub_cat_id ");
		if(!StringUtil.isNullOrEmpty(productInfoCat)){
			sql2.append(" AND pi.PRODUCT_INFO_CODE = ? ");
			totalParams.add(productInfoCat);
		}
		if(!StringUtil.isNullOrEmpty(productInfoSubCat)){
			sql2.append(" and exists (SELECT 1 ");
			sql2.append("                  FROM   product_info pit ");
			sql2.append("                  WHERE  1 = 1 ");
			sql2.append("                         AND pit.type = 2 ");
			sql2.append(" AND pit.PRODUCT_INFO_CODE = ? and pit.product_info_id = p.sub_cat_id) ");
			totalParams.add(productInfoSubCat);
		}

		//neu khong tim kiem thi them diu kien loai tru mat hang Z
//		if (!hasProductCode && !hasProductName) {
//			sql2.append(" AND pi.PRODUCT_INFO_CODE != 'Z' ");
//		}

		//Sp co khai bao gia thi moi load len
//		sql2.append("       and pr_.price_id > 0 and PR_.price >= 0 ");

		//chi load nhung sp co nganh hang ma nhan vien duoc khai bao
		sql2.append("	AND (	");
		sql2.append("	    CASE	");
		sql2.append("	        WHEN (	");
		sql2.append("	            SELECT	");
		sql2.append("	                COUNT (1)	");
		sql2.append("	            FROM	");
		sql2.append("	                staff_sale_cat ssc	");
		sql2.append("	            WHERE	");
		sql2.append("	                ssc.staff_id = ?	");
		totalParams.add(staffId);
		sql2.append("	        ) = 0 THEN 1	");
		//sua cat_id thanh sub_cat_id
		sql2.append("	        ELSE P.SUB_CAT_ID IN (	");
		sql2.append("	            SELECT	");
		sql2.append("	                CAT_ID	");
		sql2.append("	            FROM	");
		sql2.append("	                staff_sale_cat ssc	");
		sql2.append("	            WHERE	");
		sql2.append("	                ssc.staff_id = ?	");
		totalParams.add(staffId);
		sql2.append("	        )	");
		sql2.append("	    END	");
		sql2.append("	)	");


		if (hasListProductNotIn) {
			sql2.append(" AND p.product_code NOT IN ( " + listProductNotIn + ")");
		}
		if (hasProductCode) {
			sql2.append("AND upper(product_code) LIKE upper(?) escape '^' ");
			totalParams.add(productCode);
		}
		if (hasProductName) {
			sql2.append("AND upper(name_text) LIKE upper(?) escape '^' ");
			totalParams.add(productName);
		}
		if(orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)){
			sql2.append("              AND  ST.quantity > 0 ");
			sql2.append("              AND  ST.available_quantity > 0 ");
		}


		String getCountProductList = " select count(*) as total_row from ("+ sql2.toString() + ") ";

		// bo xung tham so lay d/s san pham
		paramsList = params.toArray(new String[params.size()]);

		// bo xung tham so cho sql lay tong so luong
		paramsTotal = totalParams.toArray(new String[totalParams.size()]);

		ListFindProductSaleOrderDetailViewDTO listResult = new ListFindProductSaleOrderDetailViewDTO();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			int total = 0;
			if (isGetCount) {
				cTmp = rawQuery(getCountProductList, paramsTotal);
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					listResult.totalObject = total;
					try {
						cTmp.close();
					} catch (Exception e) {
					}
				}
			}
			// end
			c = rawQuery(sql.toString() + ext,
					paramsList);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FindProductSaleOrderDetailViewDTO orderJoinTableDTO = new FindProductSaleOrderDetailViewDTO();
						orderJoinTableDTO.initSaleOrderDetailObjectFromGetProductStatement(c);
						listResult.listObject.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
		}finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		MeasuringTime.getEndTimeParser();
		return listResult;
	}

	/**
	 *
	 * get list nganh hang, nganh hang con
	 *
	 * @author: HieuNH
	 * @param data
	 * @return
	 * @return: ListProductDTO
	 * @throws:
	 */
	public CategoryCodeDTO getListCategoryCodeProduct() {

		StringBuilder getCategoryCodeList = new StringBuilder();
		getCategoryCodeList
				.append("select distinct P.category_code from PRODUCT as P where P.status = 1");

		StringBuilder queryGetSubCategoryList = new StringBuilder();
		queryGetSubCategoryList
				.append("select distinct P.subcategory_id from PRODUCT as P where P.status = 1");

		CategoryCodeDTO listResult = new CategoryCodeDTO();
		Cursor c = null;
		try {
			// get total row first
			String[] arrParam = new String[] {};
			c = rawQuery(getCategoryCodeList + "", arrParam);
			listResult.categoryCode.addElement(StringUtil.getString(R.string.TEXT_CHOOSE_PRODUCT_INDUSTRY));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						listResult.categoryCode.addElement(CursorUtil.getString(c, "CATEGORY_CODE"));
					} while (c.moveToNext());
				}
				c.close();
			}
			// end
			c = rawQuery(queryGetSubCategoryList + "", arrParam);
			listResult.subCategoryCode.addElement(StringUtil.getString(R.string.TEXT_CHOOSE_PRODUCT_INDUSTRY_CHILDREN));
			if (c != null) {
				if (c.moveToFirst()) {
					if (c.moveToFirst()) {
						do {
							listResult.subCategoryCode.addElement(CursorUtil.getString(c, "SUBCATEGORY_ID"));
						} while (c.moveToNext());
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listResult;
	}

	/**
	 *
	 * tinh so lan ghe tham con lai cua NVBH tren khach hang tinh tu ngay hien
	 * tai den cuoi thang (tinh ca ngay hien tai)
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 * @return: int
	 * @throws:
	 * @since: Feb 22, 2013
	 */
	public int getNumberVisitRemain(String staffId, String shopId,
			String customerId) {
		int numberVisit = 0;
		String dateNow = DateUtils.now();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT rtc.start_week, ");
		sql.append("       rtc.week_interval, ");
		sql.append("       ( CASE ");
		sql.append("           WHEN rtc.monday = 1 THEN ',T2' ");
		sql.append("           ELSE '' ");
		sql.append("         END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.tuesday = 1 THEN ',T3' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.wednesday = 1 THEN ',T4' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.thursday = 1 THEN ',T5' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.friday = 1 THEN ',T6' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.saturday = 1 THEN ',T7' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) ");
		sql.append("       || ( CASE ");
		sql.append("              WHEN rtc.sunday = 1 THEN ',CN' ");
		sql.append("              ELSE '' ");
		sql.append("            END ) TUYEN ");
		sql.append("FROM   routing_customer rtc, ");
		sql.append("       customer ct, ");
		sql.append("       visit_plan vp, ");
		sql.append("       routing RT ");
		sql.append("WHERE  VP.routing_id = RT.routing_id ");
		sql.append("       AND RTC.routing_id = RT.routing_id ");
		sql.append("       AND RTC.customer_id = CT.customer_id ");
		sql.append("       AND substr(VP.from_date, 1, 10) <= substr(?, 1, 10) ");
		sql.append("       AND Ifnull(Date(VP.to_date) >= Date(?), 1) ");
		sql.append("       AND VP.staff_id = ? ");
		sql.append("       AND VP.status = 1 ");
		sql.append("       AND RT.status = 1 ");
		sql.append("       AND CT.status = 1 ");
		sql.append("       AND RTC.status = 1 ");
		sql.append("       AND ct.customer_id = ? ");
		sql.append("       AND ct.shop_id = ? ");

		String[] params = new String[] { dateNow, dateNow, staffId, customerId, shopId };
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					String tuyen = Constants.STR_BLANK;
					int startWeek = 0;
					int weekInterval = 0;
					tuyen = CursorUtil.getString(c, "TUYEN");
					if (!StringUtil.isNullOrEmpty(tuyen)) {
						tuyen = tuyen.substring(1, tuyen.length());
					}
					startWeek = CursorUtil.getInt(c, ROUTING_CUSTOMER_TABLE.START_WEEK);
					weekInterval = CursorUtil.getInt(c, ROUTING_CUSTOMER_TABLE.WEEK_INTERVAL);

					numberVisit = getNumberTimesVisitPlanInMonth(tuyen, startWeek, weekInterval);
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return numberVisit;
	}

	/**
	 *
	 * tinh so lan ghe tham theo ke hoach trong thang cua mot khach hang, tinh
	 * tu ngay hien tai den cuoi thang (tinh ca ngay hien tai)
	 *
	 * @author: HaiTC3
	 * @param visitPlan
	 * @param startWeek
	 * @param weekInterval
	 * @return
	 * @return: int
	 * @throws:
	 * @since: Feb 22, 2013
	 */
	public int getNumberTimesVisitPlanInMonth(String visitPlan, int startWeek,
			int weekInterval) {
		int count = 0;
		// tinh cac thu can di trong tuyen
		int T2 = 0;
		int T3 = 0;
		int T4 = 0;
		int T5 = 0;
		int T6 = 0;
		int T7 = 0;
		int CN = 0;
		// lay d/s so ngay nghi trong nam tu bang exception fullDate
		ArrayList<String> listExcepTionDay = new EXCEPTION_DAY_TABLE(mDB)
				.getListExceptionDay();

		String[] listT = visitPlan.split(",");
		for (int i = 0, size = listT.length; i < size; i++) {
			if ("T2".equals(listT[i])) {
				T2 = 1;
			} else if ("T3".equals(listT[i])) {
				T3 = 1;
			} else if ("T4".equals(listT[i])) {
				T4 = 1;
			} else if ("T5".equals(listT[i])) {
				T5 = 1;
			} else if ("T6".equals(listT[i])) {
				T6 = 1;
			} else if ("T7".equals(listT[i])) {
				T7 = 1;
			} else if ("CN".equals(listT[i])) {
				CN = 1;
			}
		}
		// tinh so ngay trong thang hien tai
		Calendar calendar = Calendar.getInstance();
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int numDay = calendar.getActualMaximum(Calendar.DATE);
		for (int i = day; i <= numDay; i++) {
			calendar.set(year, month, i);

			boolean isExcepTionDay = false;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			sdf.format(calendar.getTime());
			for (int j = 0, size = listExcepTionDay.size(); j < size; j++) {
				if (listExcepTionDay.get(j).equals(
						sdf.format(calendar.getTime()))) {
					isExcepTionDay = true;
				}
			}
			if (!isExcepTionDay) {
				int currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
				int weekOfDayInYear = calendar.get(Calendar.WEEK_OF_YEAR);
				if (currentDayOfWeek == 1 && CN == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 2 && T2 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 3 && T3 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 4 && T4 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 5 && T5 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 6 && T6 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 7 && T7 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				}
			}
		}
		return count;
	}

	 /**
	 * Lay cac sp kiem ton
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ListRemainProductDTO
	 * @throws:
	*/
	public ListRemainProductDTO getRemainProduct(Bundle data) throws Exception{
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String page = data.getString(IntentConstants.INTENT_PAGE);
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String startOfTwoPreviousCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -2);

		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    PRODUCT_ID,	");
		sqlObject.append("	    CONVFACT,	");
		sqlObject.append("	    PRODUCT_NAME,	");
		sqlObject.append("	    PRODUCT_CODE,	");
		sqlObject.append("	    GROSS_WEIGHT	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (   SELECT	");
		sqlObject.append("	        DISTINCT P.PRODUCT_ID,	");
		sqlObject.append("	        P.CONVFACT,	");
		sqlObject.append("	        P.PRODUCT_NAME,	");
		sqlObject.append("	        P.PRODUCT_CODE,	");
		sqlObject.append("	        P.GROSS_WEIGHT	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        (          SELECT	");
		sqlObject.append("	            product_id,	");
		sqlObject.append("	            convfact,	");
		sqlObject.append("	            product_code,	");
		sqlObject.append("	            product_name,	");
		sqlObject.append("	            gross_weight,	");
		sqlObject.append("	            cat_id	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            product	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            status = 1      ) P,	");
		sqlObject.append("	        (          SELECT	");
		sqlObject.append("	            distinct SOD.product_id product_id	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            sale_order_detail SOD	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            SOD.is_free_item = 0	");
		sqlObject.append("	            AND SOD.sale_order_id IN (	");
		sqlObject.append("	                SELECT	");
		sqlObject.append("	                    SO.sale_order_id	");
		sqlObject.append("	                FROM	");
		sqlObject.append("	                    sale_order SO	");
		sqlObject.append("	                WHERE	");
		sqlObject.append("	                    SO.approved = 1	");
		sqlObject.append("	                    AND SO.staff_id = ?	");
		paramsObject.add(staffId);
		sqlObject.append("	                    AND SO.shop_id = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	                    AND SO.type = 1	");
		sqlObject.append("	                    AND SO.customer_id = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	                    AND Ifnull(substr(SO.order_date, 1, 10) >= substr(?,1,10), 0)	");
//		sqlObject.append("	                    AND Ifnull(substr(SO.order_date, 1, 10) >= Date(?, 'start of month', '-2 month'), 0)	");
//		paramsObject.add(dateNow);
		paramsObject.add(startOfTwoPreviousCycle);
		sqlObject.append("	            )	");
		sqlObject.append("	        ) SD_CHECK  ON P.product_id = SD_CHECK.product_id)	");
		sqlObject.append("	GROUP  BY	");
		sqlObject.append("	    product_id,	");
		sqlObject.append("	    convfact,	");
		sqlObject.append("	    product_name,	");
		sqlObject.append("	    gross_weight,	");
		sqlObject.append("	    product_code	");

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY product_code ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "product_code")
				.addMapper(SortActionConstants.NAME, "product_name")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		sqlObject.append(orderByStr);

		ListRemainProductDTO listResult = null;
		Cursor c = null;
		try {
			c = rawQuery(sqlObject.append(page).toString(), paramsObject.toArray(new String[paramsObject.size()]));

			if (c != null) {
				listResult = new ListRemainProductDTO();
				if (c.moveToFirst()) {

					RemainProductViewDTO productInfo;
					do {
						productInfo = new RemainProductViewDTO();
						productInfo.initDataFromCursor(c);

						listResult.listDTO.add(productInfo);

					} while (c.moveToNext());
					listResult.total = listResult.listDTO.size();
				}

			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listResult;
	}

	/**
	 * Lay ordinal cua kiem ton
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @throws Exception
	 * @return: ListRemainProductDTO
	 * @throws:
	*/
	public Bundle getLastOrdinalRemainProduct(Bundle data) throws Exception{
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
//		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);

		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT ORDINAL	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    customer_stock_history	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND staff_id = ?	");
		paramsObject.add(staffId);
		sqlObject.append("	    AND customer_id = ?	");
		paramsObject.add(customerId);
		sqlObject.append("	    AND object_type = 0	");
		sqlObject.append("	ORDER BY	");
		sqlObject.append("	    create_date desc limit 1	");

		Bundle listResult = new Bundle();
		int lastOrderRemain = 0;

		Cursor c = null;
		try {
			c = rawQuery(sqlObject.toString(), paramsObject.toArray(new String[paramsObject.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					lastOrderRemain = CursorUtil.getInt(c, "ORDINAL");
				}
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		listResult.putInt(IntentConstants.INTENT_LAST_ORDINAL_REMAIN, lastOrderRemain);

		return listResult;
	}

//	/**
//	 *
//	 * get Remain Product
//	 *
//	 * @author: HieuNH
//	 * @param data
//	 * @return
//	 * @return: ListProductDTO
//	 * @throws:
//	 */
//	public ListRemainProductDTO tinhluongGoiY(Bundle data,
//			ArrayList<RemainProductViewDTO> list, int numVisitRemain) {
//		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
//		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
//		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String staff_type = ""
//				+ GlobalInfo.getInstance().getProfile().getUserData().inheritSpecificType;
//		ArrayList<String> listParams = new ArrayList<String>();
//		if (StringUtil.isNullOrEmpty(staff_type))
//			staff_type = "";
//		StringBuffer sql = new StringBuffer();
//		sql.append("SELECT DSBQ_2_THANG_TRUOC_SP.product_id AS PRODUCT_ID, ");
//		sql.append("       ifnull(TL_THANH_CONG.ap_param_code,1)      AS TY_LE_THANH_CONG, ");
//		sql.append("       ( ( ifnull(DSBQ_2_THANG_TRUOC_SP.amount,0) * TY_TRONG.value ) - ");
//		sql.append("         ifnull(DS_MUA_TRONG_THANG_SP.sl_mua,0) ) AS REMAIN_DS ");
//		sql.append("FROM   (SELECT ap_param_code ");
//		sql.append("        FROM   ap_param ");
//		sql.append("        WHERE  type = 'SUCCESS_VISIT_PER' ");
//		sql.append("               AND status = 1) TL_THANH_CONG, ");
//		sql.append("       (SELECT Ifnull(actually, 1) AS value ");
//		sql.append("        FROM   rpt_sale_summary ");
//		sql.append("        WHERE  code = 'TI_TRONG' ");
//		sql.append("               AND shop_id = ? ");
//		listParams.add(shopId);
//		sql.append("               AND object_id = ? ");
//		listParams.add(staffId);
//		sql.append("               AND object_type = 2 ");
//		sql.append("               AND sale_date >= Date('now', 'localtime', ");
//		sql.append("                                        'start of month') ");
//		sql.append("               AND status = 1 LIMIT 1) TY_TRONG, ");
//		sql.append("       (SELECT sim1.product_id, ");
//		sql.append("               sum(sim1.amount)/2 amount ");
//		sql.append("        FROM   ( ");
//		sql.append("        select * FROM   rpt_sale_in_month sim ");
//		sql.append("        WHERE  Date(sim.month) >= ");
//		sql.append("               Date('now', 'localtime', 'start of month', ");
//		sql.append("               '-2 month' )");
//		sql.append("               AND Date(sim.month) < ");
//		sql.append("                   Date('now', 'localtime', 'start of month') ");
//		sql.append("               AND sim.customer_id = ? ");
//		listParams.add(staffId);
//		sql.append("               AND sim.status = 1 ");
//		sql.append("               AND sim.shop_id = ? ");
//		listParams.add(shopId);
//		sql.append("               AND sim.staff_id = ? ");
//		listParams.add(staffId);
//		sql.append("        GROUP  BY product_id) sim1  group by product_id ) DSBQ_2_THANG_TRUOC_SP ");
//		sql.append("       LEFT JOIN (SELECT sod.product_id, ");
//		sql.append("                         Sum(sod.amount) SL_mua ");
//		sql.append("                  FROM   sale_order so, ");
//		sql.append("                         sale_order_detail sod ");
//		sql.append("                  WHERE  so.sale_order_id = sod.sale_order_id ");
//		sql.append("                         AND so.shop_id = ? ");
//		listParams.add(shopId);
//		sql.append("                         AND so.customer_id = ? ");
//		listParams.add(customerId);
//		sql.append("                         AND type = 1 ");
//		sql.append("                         AND so.staff_id = ? ");
//		listParams.add(staffId);
//		sql.append("                         AND sod.is_free_item = 0 ");
//		sql.append("                         AND ( ( so.approved = 1 ");
//		sql.append("                                 AND Date(so.order_date) >= ");
//		sql.append("                                     Date('now', 'localtime', ");
//		sql.append("                                     'start of month') ");
//		sql.append("                                 AND Date(so.order_date) < ");
//		sql.append("                                     Date('now', 'localtime') ) ");
//		sql.append("                                OR ( so.approved IN ( 0, 1 ) ");
//		sql.append("                                     AND Date(so.order_date) >= ");
//		sql.append("                                         Date('now', 'localtime' ");
//		sql.append("                                         ) ) ) ");
//		sql.append("                  GROUP  BY product_id) AS DS_MUA_TRONG_THANG_SP ");
//		sql.append("              ON DS_MUA_TRONG_THANG_SP.product_id = ");
//		sql.append("                 DSBQ_2_THANG_TRUOC_SP.product_id ");
//		sql.append("GROUP  BY DSBQ_2_THANG_TRUOC_SP.product_id");
//
//		ListRemainProductDTO listResult = null;
//		Cursor c = null;
//		try {
//			c = rawQueries(sql.toString(), listParams);
//
//			if (c != null) {
//				listResult = new ListRemainProductDTO();
//				if (c.moveToFirst()) {
//
//					do {
//						int product_id = c.getInt(c
//								.getColumnIndex("PRODUCT_ID"));
//						double percentSuccess = c.getDouble(c
//								.getColumnIndex("TY_LE_THANH_CONG"));
//						long remainAmount = c.getLong(c
//								.getColumnIndex("REMAIN_DS"));
//
//						double doanhThuGoiY = remainAmount
//								/ (numVisitRemain * (percentSuccess / 100));
//
//						for (int i = 0; i < list.size(); i++) {
//							if (product_id == list.get(i).getPRODUCT_ID()) {
//								int slgy = (int) Math
//										.round((doanhThuGoiY / list.get(i)
//												.getPRICE()));
//								if (slgy < 0)
//									slgy = 0;
//								list.get(i)
//										.setHINT_NUMBER(String.valueOf(slgy));
//							}
//						}
//
//					} while (c.moveToNext());
//				}
//			}
//
//		} catch (Exception e) {
//			MyLog.w("",	 e.toString());
//			return null;
//		} finally {
//			c.close();
//		}
//		return listResult;
//	}

	// /**
	// *
	// * get Remain Product
	// *
	// * @author: HieuNH
	// * @param data
	// * @return
	// * @return: ListProductDTO
	// * @throws:
	// */
	// public void calculateHintNumber(RemainProductViewDTO productInfo, String
	// staffId , String shopId, String customerId ) {
	//
	// //Cursor c = null;
	// try {
	// float tiLeThanhCong = (float)(getTiLeThanhCong() / 100.0);
	// int keHoachThangNPP = getKeHoachThangNPP(shopId, staffId);
	// int binhQuanDoanhSo = getBinhQuanDoanhSo(staffId);
	// // + gia tri 4: ti le tang truong
	// float tiLeTangTruong = 0;
	// if (binhQuanDoanhSo != 0) {
	// tiLeTangTruong = (float) (1.0000 * keHoachThangNPP / binhQuanDoanhSo);
	// }
	//
	// if (productInfo.GS.equals("0")){
	//
	// productInfo.setHINT_NUMBER(Integer
	// .toString(getDTGYOneProductOneCustomerDate(
	// staffId, shopId, String
	// .valueOf(productInfo
	// .getPRODUCT_ID()),
	// customerId, productInfo.getPRICE(),
	// tiLeThanhCong, keHoachThangNPP,
	// binhQuanDoanhSo, tiLeTangTruong)));
	//
	// }
	//
	// } catch (Exception e) {
	//
	// } finally {
	// //c.close();
	// }
	//
	// }

	/**
	 *
	 * get Remain Product
	 *
	 * @author: HieuNH
	 * @param data
	 * @return
	 * @return: ListProductDTO
	 * @throws:
	 */
	public void calculateHintNumber(RemainProductViewDTO productInfo,
			String staffId, String shopId, String customerId) {

		// // Cursor c = null;
		// try {
		// float tiLeThanhCong = (float) (getTiLeThanhCong() / 100.0);
		// int keHoachThangNPP = getKeHoachThangNPP(shopId, staffId);
		// int binhQuanDoanhSo = getBinhQuanDoanhSo(staffId);
		// // + gia tri 4: ti le tang truong
		// float tiLeTangTruong = 0;
		// if (binhQuanDoanhSo != 0) {
		// tiLeTangTruong = (float) (1.0000 * keHoachThangNPP /
		// binhQuanDoanhSo);
		// }
		//
		// if (productInfo.GS.equals("0")) {
		//
		// productInfo.setHINT_NUMBER(Integer
		// .toString(getDTGYOneProductOneCustomerDate(staffId,
		// shopId,
		// String.valueOf(productInfo.getPRODUCT_ID()),
		// customerId, productInfo.getPRICE(),
		// tiLeThanhCong, keHoachThangNPP,
		// binhQuanDoanhSo, tiLeTangTruong)));
		//
		// }
		//
		// } catch (Exception e) {
		//		MyLog.w("",	 e.toString());
		//
		// } finally {
		// // c.close();
		// }

	}

	/**
	 *
	 * Khoi tao doi tuong chi tiet Don hang da.ng KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	private OrderDetailViewDTO initPromotionProductFromCursor(Cursor c) {
		SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
		OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();

		saleOrderDTO.productId = CursorUtil.getInt(c, PRODUCT_ID);
		saleOrderDTO.price = CursorUtil.getLong(c, PRICE);
		saleOrderDTO.packagePrice = CursorUtil.getLong(c, PACKAGE_PRICE_NOT_VAT);
		saleOrderDTO.priceId = CursorUtil.getLong(c, PRICE_ID);
		saleOrderDTO.quantity = CursorUtil.getInt(c, "ACTUAL");
		saleOrderDTO.quantityPackage = CursorUtil.getLong(c, QUANTITY_PACKAGE);
		saleOrderDTO.quantityRetail = CursorUtil.getLong(c, QUANTITY_RETAIL);
		saleOrderDTO.maxQuantityFree = CursorUtil.getInt(c, MAX_QUANTITY_FREE);
		saleOrderDTO.setAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.discountPercentage = CursorUtil.getDouble(c, DISCOUNT_PERCENT);
//		saleOrderDTO.programeCode = CursorUtil.getString(c, JOIN_PROGRAM_CODE);
		saleOrderDTO.programeCode = CursorUtil.getString(c, PROGRAM_CODE);
		saleOrderDTO.programeId = CursorUtil.getLong(c, "PROGRAM_ID");
		saleOrderDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		saleOrderDTO.isFreeItem = CursorUtil.getInt(c, "IS_FREE_ITEM");
		saleOrderDTO.priceNotVat = CursorUtil.getLong(c, PRICE_NOT_VAT);
		saleOrderDTO.packagePriceNotVat = CursorUtil.getLong(c, PACKAGE_PRICE_NOT_VAT);
		saleOrderDTO.catId = CursorUtil.getInt(c, CAT_ID);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		saleOrderDTO.vat = CursorUtil.getDouble(c, "VAT");
		detailViewDTO.grossWeight = CursorUtil.getDouble(c, "GROSS_WEIGHT");
		detailViewDTO.productCode = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_CODE);
		detailViewDTO.productName = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_NAME);
		saleOrderDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		detailViewDTO.convfact = CursorUtil.getInt(c, CONVFACT);

		if (detailViewDTO.productName == null
				|| detailViewDTO.productName.length() == 0) {
			detailViewDTO.productName = StringUtil
					.getString(R.string.TEXT_KM_CK_GET);
		}
		detailViewDTO.stock = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		detailViewDTO.stockActual = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.QUANTITY);
		detailViewDTO.quantity = String.valueOf(saleOrderDTO.quantityPackage * detailViewDTO.convfact + saleOrderDTO.quantity);
		saleOrderDTO.programeTypeCode = CursorUtil.getString(c, SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE);
		saleOrderDTO.productGroupId = CursorUtil.getLong(c, SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID);
		saleOrderDTO.groupLevelId = CursorUtil.getLong(c, SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID);
		saleOrderDTO.payingOrder = CursorUtil.getInt(c, SALES_ORDER_DETAIL_TABLE.PAYING_ORDER);

		//Get type of promotion for render
//		if(saleOrderDTO.maxQuantityFree > 0) {
			detailViewDTO.type = OrderDetailViewDTO.FREE_PRODUCT;
//		} else if (saleOrderDTO.discountPercentage > 0) {
//			detailViewDTO.type = OrderDetailViewDTO.FREE_PERCENT;
//		} else if (saleOrderDTO.maxAmountFree > 0) {
//			detailViewDTO.type = OrderDetailViewDTO.FREE_PRICE;
//		}

		detailViewDTO.orderDetailDTO = saleOrderDTO;
		return detailViewDTO;
	}

	/**
	 *
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	private OrderDetailViewDTO initSaleOrderDetailObjectFromGetProductStatement(Cursor c) {
		SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
		OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();

		saleOrderDTO.salesOrderDetailId = CursorUtil.getLong(c, SALE_ORDER_DETAIL_ID);
		saleOrderDTO.productId = CursorUtil.getInt(c, PRODUCT_ID);
		saleOrderDTO.price = CursorUtil.getDouble(c, PRICE);
		saleOrderDTO.packagePrice = CursorUtil.getDouble(c, PACKAGE_PRICE);
		saleOrderDTO.priceId = CursorUtil.getLong(c, PRICE_ID);
		saleOrderDTO.quantity = CursorUtil.getInt(c, "ACTUAL");
		saleOrderDTO.quantityPackage = CursorUtil.getLong(c, QUANTITY_PACKAGE);
		saleOrderDTO.quantityRetail = CursorUtil.getLong(c, QUANTITY_RETAIL);
		saleOrderDTO.maxQuantityFree = CursorUtil.getInt(c, MAX_QUANTITY_FREE);
		saleOrderDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		saleOrderDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.discountPercentage = CursorUtil.getDouble(c, DISCOUNT_PERCENT);
		saleOrderDTO.isFreeItem = CursorUtil.getInt(c, IS_FREE_ITEM);

		saleOrderDTO.programeCode = CursorUtil.getString(c, PROGRAM_CODE);
		saleOrderDTO.programeIdBuyView = CursorUtil.getString(c, "PROGRAM_ID");

//		saleOrderDTO.programeCode = CursorUtil.getString(c, JOIN_PROGRAM_CODE);
		saleOrderDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		saleOrderDTO.synState = ABSTRACT_TABLE.CREATED_STATUS;
		saleOrderDTO.priceNotVat = CursorUtil.getDouble(c, PRICE_NOT_VAT);
		saleOrderDTO.packagePriceNotVat = CursorUtil.getDouble(c, PACKAGE_PRICE_NOT_VAT);
		saleOrderDTO.catId = CursorUtil.getInt(c, CAT_ID);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		saleOrderDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		detailViewDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		saleOrderDTO.vat = CursorUtil.getDouble(c, "VAT");
		detailViewDTO.grossWeight = CursorUtil.getDouble(c, "GROSS_WEIGHT");

		detailViewDTO.productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		detailViewDTO.productName = CursorUtil.getString(c, "PRODUCT_NAME");
		detailViewDTO.stock = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		detailViewDTO.stockActual = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.QUANTITY);
		saleOrderDTO.programeTypeCode = CursorUtil.getString(c, PROGRAME_TYPE_CODE);

		detailViewDTO.orderDetailDTO = saleOrderDTO;
		detailViewDTO.quantity = detailViewDTO.quantityStr();
		return detailViewDTO;
	}

	/**
	 *
	 * Khoi tao doi tuong chi tiet don hang cho viet gui server
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	private OrderDetailViewDTO initSaleOrderDetailObject(Cursor c) {
		SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
		OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();

		saleOrderDTO.salesOrderDetailId = CursorUtil.getLong(c, SALE_ORDER_DETAIL_ID);
		saleOrderDTO.productId = CursorUtil.getInt(c, PRODUCT_ID);
		saleOrderDTO.quantity = CursorUtil.getInt(c, QUANTITY);
		saleOrderDTO.priceId = CursorUtil.getLong(c, PRICE_ID);
		saleOrderDTO.discountPercentage = CursorUtil.getDouble(c, DISCOUNT_PERCENT);
		saleOrderDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.isFreeItem = CursorUtil.getInt(c, IS_FREE_ITEM);
		saleOrderDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		saleOrderDTO.salesOrderId = CursorUtil.getLong(c, SALE_ORDER_ID);
		saleOrderDTO.price = CursorUtil.getDouble(c, PRICE);
		saleOrderDTO.packagePrice = CursorUtil.getDouble(c, PACKAGE_PRICE);
		saleOrderDTO.createUser = CursorUtil.getString(c, CREATE_USER);
		saleOrderDTO.updateUser = CursorUtil.getString(c, UPDATE_USER);
		saleOrderDTO.createDate = CursorUtil.getString(c, CREATE_DATE);
		saleOrderDTO.updateDate = CursorUtil.getString(c, UPDATE_DATE);
		saleOrderDTO.orderDate = CursorUtil.getString(c, ORDER_DATE);
		saleOrderDTO.maxQuantityFree = CursorUtil.getInt(c, MAX_QUANTITY_FREE);
		saleOrderDTO.programeCode = CursorUtil.getString(c, PROGRAM_CODE);
		saleOrderDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		saleOrderDTO.shopId = CursorUtil.getInt(c, SHOP_ID);
		saleOrderDTO.staffId = CursorUtil.getInt(c, STAFF_ID);
		saleOrderDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		saleOrderDTO.synState = CursorUtil.getInt(c, SYN_STATE);
		detailViewDTO.orderDetailDTO = saleOrderDTO;
		return detailViewDTO;

	}

	/**
	 *
	 * Lay ds sp ban de sua don hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 */
	public List<OrderDetailViewDTO> getListProductForEdit(String orderId,
			String orderType, long customerId, String shopId, String staffId) throws Exception {
		Cursor c = null;
		String objectId = "0";
		int objectType = 1;
		if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
				|| orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = staffId;
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}
		// String[] params = new String[] { orderId };
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    pr.PRODUCT_ID ,	");
		sqlObject.append("	    pr.PRODUCT_CODE,	");
		sqlObject.append("	    pr.PRODUCT_NAME,	");
		sqlObject.append("	    sod.PRICE,	");
		sqlObject.append("	    sod.PACKAGE_PRICE,	");
		sqlObject.append("	    sod.quantity as ACTUAL,	");
		sqlObject.append("	    sod.QUANTITY_RETAIL,	");
		sqlObject.append("	    sod.QUANTITY_PACKAGE,	");
		sqlObject.append("	    sod.MAX_QUANTITY_FREE,	");
		sqlObject.append("	    sod.AMOUNT,	");
		sqlObject.append("	    sod.IS_FREE_ITEM,	");
//		sqlObject.append("	    sod.PROGRAM_CODE,	");
//		sqlObject.append("	    sod.JOIN_PROGRAM_CODE,	");
		sqlObject.append("	    Group_concat(SPM.PROGRAM_CODE) PROGRAM_CODE,	"); // CODE CTKM ma sp dat duoc
		sqlObject.append("	    Group_concat(SPM.PROGRAM_ID) PROGRAM_ID,	"); // ID CTKM ma sp dat duoc
		sqlObject.append("	    sod.PROGRAM_TYPE,	");
		sqlObject.append("	    sod.PRICE_ID,	");
		sqlObject.append("	    sod.CONVFACT,	");
		sqlObject.append("	    st.QUANTITY,	");
		sqlObject.append("	    st.AVAILABLE_QUANTITY,	");
		sqlObject.append("	    sod.PRICE_NOT_VAT ,	");
		sqlObject.append("	    sod.PACKAGE_PRICE_NOT_VAT,	");
		sqlObject.append("	    sod.DISCOUNT_PERCENT,	");
		sqlObject.append("	    sod.DISCOUNT_AMOUNT,	");
		sqlObject.append("	    sod.SALE_ORDER_DETAIL_ID,	");
		sqlObject.append("	    sod.TOTAL_WEIGHT,	");
		sqlObject.append("	    sod.VAT,	");
		sqlObject.append("	    pr.GROSS_WEIGHT,	");
		sqlObject.append("	    sod.PROGRAME_TYPE_CODE	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    sale_order_detail sod	");
		sqlObject.append("	LEFT JOIN (SELECT	");
		sqlObject.append("	    		PP.PROMOTION_PROGRAM_ID PROGRAM_ID,	");
		sqlObject.append("	    		SPM.PROGRAM_CODE,	");
		sqlObject.append("	    		SPM.SALE_ORDER_DETAIL_ID	");
		sqlObject.append("			   FROM	");
		sqlObject.append("	   			  PROMOTION_PROGRAM PP,	");
		sqlObject.append("	  			  SALE_PROMO_MAP SPM	");
		sqlObject.append("			   WHERE	");
		sqlObject.append("	    		  SPM.PROGRAM_CODE = PP.PROMOTION_PROGRAM_CODE	");
		sqlObject.append("	    		) SPM ON SPM.SALE_ORDER_DETAIL_ID = SOD.SALE_ORDER_DETAIL_ID,	");
		sqlObject.append("	    product pr	");
		sqlObject.append("	LEFT OUTER JOIN	");
		sqlObject.append("	    (	");
		sqlObject.append("	        SELECT	");
		sqlObject.append("	            SUM(st.quantity) as QUANTITY,	");
		sqlObject.append("	            SUM(st.available_quantity) AVAILABLE_QUANTITY,	");
		sqlObject.append("	            st.PRODUCT_ID	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            stock_total st	");
		sqlObject.append("	        LEFT JOIN	");
		sqlObject.append("	            warehouse wh	");
		sqlObject.append("	                ON (	");
		sqlObject.append("	                    st.object_type = 1	");
		sqlObject.append("	                    AND wh.warehouse_id = st.warehouse_id	");
		sqlObject.append("	                    AND wh.status = 1	");
		sqlObject.append("	                )	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            1=1	");
		sqlObject.append("	            AND (	");
		sqlObject.append("	                (	");
		sqlObject.append("	                    st.shop_id = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	                    AND st.object_type = 2	");
		sqlObject.append("	                )	");
		sqlObject.append("	                OR (	");
		sqlObject.append("	                    st.object_type = 1	");
		sqlObject.append("	                    AND ifnull(wh.warehouse_id, '') <> ''	");
		sqlObject.append("	                )	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND st.object_id = ?	");
		paramsObject.add(objectId);
		sqlObject.append("	            AND st.status = 1	");
		sqlObject.append("	            AND st.object_type = ?	");
		paramsObject.add(""+objectType);
		sqlObject.append("	        GROUP BY	");
		sqlObject.append("	            st.product_id	");
		sqlObject.append("	    ) st	");
		sqlObject.append("	        ON st.product_id = pr.product_id	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND sod.product_id = pr.product_id	");
		sqlObject.append("	    AND sod.sale_order_id = ?	");
		paramsObject.add(orderId);
		sqlObject.append("	    AND sod.is_free_item = 0	");
		sqlObject.append("	GROUP BY  sod.sale_order_detail_id ");

		ArrayList<OrderDetailViewDTO> v = new ArrayList<OrderDetailViewDTO>();

		try {
			c = rawQueries(sqlObject.toString(),paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {

					do {
						OrderDetailViewDTO orderDetail = initSaleOrderDetailObjectFromGetProductStatement(c);
						getPromoDetailOfOrderDetail(orderDetail);
						// lay chi tiet tra thuong cua keyshop
						getPromoDetailKeyShopOfOrderDetail(orderDetail,customerId);
						//tam thoi bo check focus
						//checkFocusProduct(orderDetail);

						v.add(orderDetail);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return v;
	}

	/**
	 *
	 * Dung cho chuc nang chuyen don hang tren ds don hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 * @return
	 * @throws Exception
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 */
	public List<OrderDetailViewDTO> getListProductForSend(String orderId) {
		Cursor c = null;
		ArrayList<String> params = new ArrayList<String>();
		params.add(orderId);

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT sod.* ");
		sql.append(" FROM   sales_order_detail sod ");
		sql.append(" WHERE       1 = 1 ");
		sql.append(" AND sod.sale_order_id = ? ");
		// sql.append(" AND sod.is_free_item = 0 ");

		ArrayList<OrderDetailViewDTO> v = new ArrayList<OrderDetailViewDTO>();

		try {
			c = rawQuery(sql.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					OrderDetailViewDTO orderJoinTableDTO;

					do {
						orderJoinTableDTO = initSaleOrderDetailObject(c);
						v.add(orderJoinTableDTO);

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return v;
	}


	/**
	 * Kiem tra sp co thuoc chuong trinh trong tam hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderDetailDTO
	 * @return: void
	 * @throws:
	 */

	private void checkFocusProduct(OrderDetailViewDTO orderDetailDTO)throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		params.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		params.add(String.valueOf(orderDetailDTO.orderDetailDTO.productId));

		sql.append(" SELECT distinct fpd.product_id AS product_id ");
		sql.append(" FROM focus_programe fp_, focus_programe_detail fpd, focus_shop_map map , STAFF AS STF ");
		sql.append(" WHERE fp_.status = 1 ");
		sql.append(" AND IFNULL(DATE(fp_.from_date) <= DATE('now','localtime'),0) ");
		sql.append(" AND (IFNULL(DATE(fp_.TO_DATE) >= DATE('now','localtime'),0) ");
		sql.append(" OR DATE(fp_.TO_DATE) IS NULL OR fp_.TO_DATE = '') ");
		sql.append(" AND fp_.focus_program_id = MAP.focus_program_id ");
		sql.append(" AND (map.status = 1 OR map.status = 'true') ");
		sql.append(" AND MAP.FOCUS_SHOP_MAP_ID = fpd.FOCUS_SHOP_MAP_ID ");
		sql.append(" AND MAP.STAFF_TYPE = STF.STAFF_TYPE ");
		sql.append(" AND STF.STAFF_ID = ?");
		// sql.append(GlobalInfo.getInstance().getProfile().getUserData().id);
		sql.append(" AND map.shop_id = ?");
		// sql.append(GlobalInfo.getInstance().getProfile().getUserData().shopId);
		sql.append(" AND fpd.product_id = ?");
		// sql.append(orderJoinTableDTO.orderDetailDTO.productId);

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					int product_id = CursorUtil.getInt(c, "product_id");
					if (product_id > 0) {
						orderDetailDTO.isFocus = 1;
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}
	}
	/**
	 * Kiem tra sp co thuoc chuong trinh trong tam hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderDetailDTO
	 * @return: void
	 * @throws:
	 */

	private void getPromoDetailOfOrderDetail(OrderDetailViewDTO orderDetailDTO) throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append(" SELECT *,PP.PROMOTION_PROGRAM_ID PROGRAM_ID ");
		sql.append(" FROM sale_order_promo_detail sopd,  PROMOTION_PROGRAM PP  ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND sopd.sale_order_detail_id = ? ");
		params.add(String.valueOf(orderDetailDTO.orderDetailDTO.salesOrderDetailId));
		sql.append(" AND sopd.PROGRAM_CODE = PP.PROMOTION_PROGRAM_CODE ");
		sql.append(" AND sopd.PROGRAM_TYPE <> ? ");
		params.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleOrderPromoDetailDTO promoDetailDTO = new SaleOrderPromoDetailDTO();
						promoDetailDTO.parseDataFromCursor(c);
						orderDetailDTO.listPromoDetail.add(promoDetailDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
			}
		}
	}
	/**
	 * Lay ds san pham khuyen mai cua 1 don hang co san
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderId
	 *            : id cua don hang
	 * @param customerId
	 * @param staffId
	 * @param shopId2
	 * @param string
	 * @return
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 */

	public List<OrderDetailViewDTO> getPromotionProductsForEdit(String orderId, String orderType, long customerId, String shopId, String staffId) throws Exception {
		Cursor c = null;
		String objectId = "0";
		int objectType = 1;
		if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
				|| orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = staffId;
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}
		KEYSHOP_TABLE ksTable = new KEYSHOP_TABLE(mDB);

		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    pr.PRODUCT_CODE,	");
		sqlObject.append("	    pr.PRODUCT_NAME,	");
		sqlObject.append("	    sod.PRICE,	");
		sqlObject.append("	    sod.PACKAGE_PRICE,	");
		sqlObject.append("	    sod.PRICE_ID,	");
		sqlObject.append("	    sod.PRICE_NOT_VAT,	");
		sqlObject.append("	    sod.PACKAGE_PRICE_NOT_VAT,	");
		sqlObject.append("	    sod.TOTAL_WEIGHT,	");
		sqlObject.append("	    sod.MAX_QUANTITY_FREE,	");
		sqlObject.append("	    sod.quantity AS ACTUAL ,	");
		sqlObject.append("	    sod.DISCOUNT_AMOUNT,	");
		sqlObject.append("	    sod.IS_FREE_ITEM,	");
		sqlObject.append("	    sod.PROGRAM_CODE,	");
		sqlObject.append("	    pp.PROMOTION_PROGRAM_ID PROGRAM_ID,	");
		sqlObject.append("	    ks.ks_id KS_PROGRAM_ID,	");
//		sqlObject.append("	    sod.JOIN_PROGRAM_CODE,	");
		sqlObject.append("	    sod.PROGRAM_TYPE,	");
		sqlObject.append("	    pr.PRODUCT_ID,	");
		sqlObject.append("	    st.QUANTITY,	");
		sqlObject.append("	    st.AVAILABLE_QUANTITY,	");
		sqlObject.append("	    sod.QUANTITY_RETAIL,	");
		sqlObject.append("	    sod.QUANTITY_PACKAGE,	");
		sqlObject.append("	    sod.VAT,	");
		sqlObject.append("	    pr.GROSS_WEIGHT,	");
		sqlObject.append("	    sod.PRODUCT_GROUP_ID,	");
		sqlObject.append("	    sod.GROUP_LEVEL_ID,	");
		sqlObject.append("	    sod.PAYING_ORDER,	");
		sqlObject.append("	    sod.DISCOUNT_PERCENT,	");
		sqlObject.append("	    sod.PROGRAME_TYPE_CODE,	");
		sqlObject.append("	    sod.CONVFACT CONVFACT	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    sale_order_detail sod	");
		sqlObject.append("	LEFT JOIN PROMOTION_PROGRAM PP	");
		sqlObject.append("	    	ON PP.PROMOTION_PROGRAM_CODE = SOD.PROGRAM_CODE	AND SOD.PROGRAM_TYPE <> ?");
		paramsObject.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
		sqlObject.append("	LEFT JOIN KS KS	");
		sqlObject.append("	    	ON KS.KS_CODE = SOD.PROGRAM_CODE AND SOD.PROGRAM_TYPE = ?	");
		paramsObject.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
		sqlObject.append("	LEFT OUTER JOIN	");
		sqlObject.append("	    product pr	");
		sqlObject.append("	        ON sod.product_id = pr.product_id	");
		sqlObject.append("	LEFT OUTER JOIN	");
		sqlObject.append("	    (	");
		sqlObject.append("	        SELECT	");
		sqlObject.append("	            SUM(st.quantity) as QUANTITY,	");
		sqlObject.append("	            SUM(st.available_quantity) AVAILABLE_QUANTITY,	");
		sqlObject.append("	            st.PRODUCT_ID	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            stock_total st	");
		sqlObject.append("	        LEFT JOIN	");
		sqlObject.append("	            warehouse wh	");
		sqlObject.append("	                ON (	");
		sqlObject.append("	                    st.object_type = 1	");
		sqlObject.append("	                    AND wh.warehouse_id = st.warehouse_id	");
		sqlObject.append("	                    AND wh.status = 1	");
		sqlObject.append("	                )	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            1=1	");
		sqlObject.append("	            AND (	");
		sqlObject.append("	                (	");
		sqlObject.append("	                    st.shop_id = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	                    AND st.object_type = 2	");
		sqlObject.append("	                )	");
		sqlObject.append("	                OR (	");
		sqlObject.append("	                    st.object_type = 1	");
		sqlObject.append("	                    AND ifnull(wh.warehouse_id, '') <> ''	");
		sqlObject.append("	                )	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND st.object_id = ?	");
		paramsObject.add(objectId);
		sqlObject.append("	            AND st.status = 1	");
		sqlObject.append("	            AND st.object_type = ?	");
		paramsObject.add(""+objectType);
		sqlObject.append("	        GROUP BY	");
		sqlObject.append("	            st.product_id	");
		sqlObject.append("	    ) st	");
		sqlObject.append("	        ON st.product_id = pr.product_id	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND sod.sale_order_id = ?	");
		paramsObject.add(orderId);
		sqlObject.append("	    AND sod.is_free_item = 1	");
		sqlObject.append("	ORDER BY ks.KS_CODE,pr.order_index	");

		ArrayList<OrderDetailViewDTO> v = new ArrayList<OrderDetailViewDTO>();

		try {
			c = rawQueries(sqlObject.toString(),
					paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						OrderDetailViewDTO orderJoinTableDTO = initPromotionProductFromCursor(c);
						// neu la CT key shop thi cap nhat lai programeId
						if(orderJoinTableDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP ){
							orderJoinTableDTO.orderDetailDTO.programeId = CursorUtil.getLong(c, "KS_PROGRAM_ID");
						}
						ksTable.getNumProductRewardByCustomer(
								orderJoinTableDTO,
								orderJoinTableDTO.orderDetailDTO.programeId,
								orderJoinTableDTO.orderDetailDTO.productId,customerId);
						v.add(orderJoinTableDTO);

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return v;
	}

	/**
	 *
	 * get list programe for product
	 *
	 * @author: HaiTC3
	 * @param orderType
	 * @param dto
	 * @return
	 * @return: List<ProgrameForProductDTO>
	 * @throws:
	 */
	public List<ProgrameForProductDTO> getListProgrameForProduct(
			String shop_id, String staff_id, String customer_id,
			String customerTypeId, String orderType) throws Exception{
		List<ProgrameForProductDTO> result = new ArrayList<ProgrameForProductDTO>();

		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopIdRecursive = shopTB.getShopRecursive(shop_id);
		String strListShop = TextUtils.join(",", listShopIdRecursive);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		ArrayList<String> params = new ArrayList<String>();

		// CTTB
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * ");
		sql.append("FROM   ( ");
//		sql.append("SELECT DISTINCT DPV.display_program_vnm_id AS PROMOTION_PROGRAM_ID, DPV.display_program_code AS PROGRAM_CODE, ");
//		sql.append("                DPV.display_program_name AS PROGRAM_NAME, ");
//		sql.append("                        'CTTB'                  AP_PARAM_NAME, ");
//		sql.append("                        'TB'                    TYPE, ");
//		sql.append("                        2                       AS PROGRAM_TYPE, ");
//		sql.append("                        0                       COKHAIBAO1, ");
//		sql.append("                        0                       COKHAIBAO2, ");
//		sql.append("                        0                       COKHAIBAO3, ");
//		sql.append("                        0                       COKHAIBAO4, ");
//		sql.append("                        0                       COKHAIBAO5, ");
//		sql.append("                        0                       COKHAIBAO6, ");
//		sql.append("                        0                       COKHAIBAO7, ");
//		sql.append("                        0                       COKHAIBAO8, ");
//		sql.append("                        0                       COKHAIBAO9, ");
//		sql.append("                        0                       COKHAIBAO10 ");
//
////		sql.append("                ST_DP1.level_code          AS LEVEL_CODE, ");
////		sql.append("                ST_DP1.customer_id         AS CUSTOMER_ID ");
//		sql.append("FROM   DISPLAY_PROGRAM_VNM DPV, DISPLAY_PRO_SHOP_MAP_VNM DSMV  ");
//		sql.append(" WHERE 1 = 1 ");
//		sql.append(" AND DPV.DISPLAY_PROGRAM_VNM_ID = DSMV.DISPLAY_PROGRAM_ID ");
//		sql.append(" AND DPV.STATUS = 1 ");
//		sql.append(" AND DSMV.STATUS = 1 ");
//		sql.append(" AND DSMV.SHOP_ID = ? ");
//		params.add(shop_id);
//		sql.append(" AND Substr(DPV.from_date,0,11) <= ? ");
//		params.add(dateNow);
//		sql.append(" AND (DPV.to_date is null or DPV.to_date = '' or IFNULL(Substr(DPV.to_date,0,11) >=  DATE(?, 'START OF MONTH', '-2 MONTH'), 1)) ");
//		params.add(dateNow);
//
//		//CTKM
//		sql.append("        UNION ALL ");
		sql.append("        SELECT DISTINCT * ");
		sql.append("        FROM   (SELECT DISTINCT pp.promotion_program_id PROMOTION_PROGRAM_ID, ");
		sql.append("                                pp.promotion_program_code PROGRAM_CODE, ");
		sql.append("                                pp.promotion_program_name      PROGRAM_NAME, ");
		sql.append("                                ap.ap_param_name               AP_PARAM_NAME, ");
		sql.append("                                pp.type                        TYPE, ");
		sql.append("                                1                              AS PROGRAM_TYPE, ");
		sql.append("                                (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 2 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 2 ");
		sql.append("                                                   AND pcad.object_id = ? ");
		params.add(customerTypeId);
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad, ");
		sql.append("                                                   sale_level_cat slc, ");
		sql.append("                                                   customer_cat_level ccl ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND slc.status = 1 ");
		sql.append("                                                   AND ccl.customer_id = ? ");
		params.add(customer_id);
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_detail CAD ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customer_id);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND ( CASE ");
		sql.append("                                                           WHEN CA.type = 2 THEN CAD.value IS NOT NULL AND Ifnull(Cast(CAD.value AS INTEGER) >= pca.from_value, 1) ");
		sql.append("                                                                                       AND Ifnull(Cast(CAD.value AS INTEGER) <= pca.to_value, 1) ");
		sql.append("                                                           WHEN CA.type = 1 THEN Trim(CAD.value) = Trim(pca.from_value) ");
		sql.append("                                                           WHEN CA.type = 3 THEN CAD.value IS NOT NULL ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) >= substr(pca.from_value, 1, 10), 1) ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) <= substr(pca.to_value, 1, 10), 1) ");
		sql.append("                                                         END ) ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customer_id);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = CAD.customer_attribute_enum_id ");
		sql.append("                                                   AND CAD.customer_attribute_detail_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customer_id);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");

//		sql.append("                                Ifnull(TTTT1.num_condition, 0) COKHAIBAO1, ");
//		sql.append("                                Ifnull(TTTT2.num_condition, 0) COKHAIBAO2, ");
//		sql.append("                                Ifnull(TTTT3.num_condition, 0) COKHAIBAO3, ");
//		sql.append("                                Ifnull(TTTT4.num_condition, 0) COKHAIBAO4, ");
//		sql.append("                                Ifnull(TTTT5.num_condition, 0) COKHAIBAO5, ");
//		sql.append("                                Ifnull(TTTT6.num_condition, 0) COKHAIBAO6, ");
//		sql.append("                                Ifnull(TTTT7.num_condition, 0) COKHAIBAO7, ");
//		sql.append("                                Ifnull(TTTT8.num_condition, 0) COKHAIBAO8, ");
//		sql.append("                                Ifnull(TTTT9.num_condition, 0) COKHAIBAO9, ");
//		sql.append("                                Ifnull(TTTT10.num_condition, 0) COKHAIBAO10 ");
//		sql.append("                ''          AS LEVEL_CODE, ");
//		sql.append("                ''          AS CUSTOMER_ID ");
		sql.append("                FROM   promotion_program pp ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca ");
//		sql.append("                                  WHERE  pca.object_type = 2 ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT1 ");
//		sql.append("                              ON TTTT1.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         promotion_cust_attr_detail pcad ");
//		sql.append("                                  WHERE  1 = 1 ");
//		sql.append("                                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                                             pcad.promotion_cust_attr_id ");
//		sql.append("                                         AND pca.object_type = 2 ");
//		sql.append("                                         AND pcad.object_id = ? ");
//		params.add(customerTypeId);
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND pcad.status = 1 ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT2 ");
//		sql.append("                              ON TTTT2.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca ");
//		sql.append("                                  WHERE  pca.object_type = 3 ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                  GROUP  BY promotion_program_id) TTTT3 ");
//		sql.append("                              ON TTTT3.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         promotion_cust_attr_detail pcad, ");
//		sql.append("                                         sale_level_cat slc, ");
//		sql.append("                                         customer_cat_level ccl ");
//		sql.append("                                  WHERE  1 = 1 ");
//		sql.append("                                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                                             pcad.promotion_cust_attr_id ");
//		sql.append("                                         AND pcad.object_id = ");
//		sql.append("                                             slc.sale_level_cat_id ");
//		sql.append("                                         AND slc.sale_level_cat_id = ");
//		sql.append("                                             ccl.sale_level_cat_id ");
//		sql.append("                                         AND pca.object_type = 3 ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND pcad.status = 1 ");
//		sql.append("                                         AND slc.status = 1 ");
//		sql.append("                                         AND ccl.customer_id = ? ");
//		params.add(customer_id);
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT4 ");
//		sql.append("                              ON TTTT4.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT5 ");
//		sql.append("                              ON TTTT5.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT, ");
//		sql.append("                                         attribute_value atv ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND atv.status = 1 ");
//		sql.append("                                         AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND atv.table_id = ? ");
//		params.add(customer_id);
//		sql.append("                                         AND ( CASE ");
//		sql.append("                                                 WHEN AT.value_type = 2 THEN ");
//		sql.append("                                                 Ifnull( ");
//		sql.append("                                                 cast(atv.value as integer) >= pca.from_value, ");
//		sql.append("                                                                             1) ");
//		sql.append("                                                                             AND ");
//		sql.append("                                                 Ifnull(cast(atv.value as integer) <= ");
//		sql.append("                                                        pca.to_value, 1) ");
//		sql.append("                                                 WHEN AT.value_type = 1 THEN ");
//		sql.append("                                                 trim(atv.value) = trim(pca.from_value) ");
//		sql.append("                                                 WHEN AT.value_type = 3 THEN atv.value is not null and ");
//		sql.append("                                                 Ifnull( ");
//		sql.append("                                                 Date(atv.value) >= ");
//		sql.append("                                                 Date(pca.from_value), ");
//		sql.append("                                                          1) ");
//		sql.append("                                                          AND ");
//		sql.append("                                                 Ifnull(Date(atv.value) <= ");
//		sql.append("                                                        Date(pca.to_value), ");
//		sql.append("                                                 1) ");
//		sql.append("                                               END ) ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT6 ");
//		sql.append("                              ON TTTT6.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND AT.value_type = 4 ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT7 ");
//		sql.append("                              ON TTTT7.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT, ");
//		sql.append("                                         attribute_value atv, ");
//		sql.append("                                         attribute_detail atd, ");
//		sql.append("                                         attribute_value_detail atvd, ");
//		sql.append("                                         promotion_cust_attr_detail pcad ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                         AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                         AND atv.attribute_value_id = ");
//		sql.append("                                             atvd.attribute_value_id ");
//		sql.append("                                         AND atd.attribute_detail_id = ");
//		sql.append("                                             atvd.attribute_detail_id ");
//		sql.append("                                         AND atd.attribute_detail_id = ");
//		sql.append("                                             pcad.object_id ");
//		sql.append("                                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                                             pcad.promotion_cust_attr_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND atv.status = 1 ");
//		sql.append("                                         AND atd.status = 1 ");
//		sql.append("                                         AND atvd.status = 1 ");
//		sql.append("                                         AND pcad.status = 1 ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND AT.value_type = 4 ");
//		sql.append("                                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND atv.table_id = ? ");
//		params.add(customer_id);
//		sql.append("                                  GROUP  BY pca.promotion_program_id)TTTT8 ");
//		sql.append("                              ON TTTT8.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND AT.value_type = 5 ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                  GROUP  BY pca.promotion_program_id) TTTT9 ");
//		sql.append("                              ON TTTT9.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id ");
//		sql.append("                       LEFT JOIN (SELECT pca.promotion_program_id ");
//		sql.append("                                         promotion_program_id, ");
//		sql.append("                                         Count (pca.promotion_cust_attr_id) ");
//		sql.append("                                         NUM_CONDITION ");
//		sql.append("                                  FROM   promotion_cust_attr pca, ");
//		sql.append("                                         attribute AT, ");
//		sql.append("                                         attribute_value atv, ");
//		sql.append("                                         attribute_detail atd, ");
//		sql.append("                                         attribute_value_detail atvd, ");
//		sql.append("                                         promotion_cust_attr_detail pcad ");
//		sql.append("                                  WHERE  pca.object_type = 1 ");
//		sql.append("                                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                         AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                         AND atv.attribute_value_id = ");
//		sql.append("                                             atvd.attribute_value_id ");
//		sql.append("                                         AND atd.attribute_detail_id = ");
//		sql.append("                                             atvd.attribute_detail_id ");
//		sql.append("                                         AND atd.attribute_detail_id = ");
//		sql.append("                                             pcad.object_id ");
//		sql.append("                                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                                             pcad.promotion_cust_attr_id ");
//		sql.append("                                         AND pca.status = 1 ");
//		sql.append("                                         AND AT.status = 1 ");
//		sql.append("                                         AND atv.status = 1 ");
//		sql.append("                                         AND atd.status = 1 ");
//		sql.append("                                         AND atvd.status = 1 ");
//		sql.append("                                         AND pcad.status = 1 ");
//		sql.append("                                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND AT.value_type = 5 ");
//		sql.append("                                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                         AND atv.table_id = ? ");
//		params.add(customer_id);
//		sql.append("                                  GROUP  BY pca.promotion_program_id)TTTT10 ");
//		sql.append("                              ON TTTT10.promotion_program_id = ");
//		sql.append("                                 pp.promotion_program_id, ");

		sql.append("                       ,ap_param ap, ");
		sql.append("                       promotion_shop_map psm ");
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                       LEFT JOIN promotion_customer_map pcm ");
//			sql.append("                              ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("       					LEFT JOIN promotion_staff_map pstm ");
//			sql.append("              					ON pstm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		}

		sql.append("                WHERE  1 = 1 ");
		sql.append("                       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append("                       AND psm.shop_id IN ( " + strListShop);
		sql.append("                       ) ");
		sql.append("                       AND pp.status = 1 ");
		sql.append("                       AND ap.status = 1 ");
		sql.append("                       AND psm.status = 1 ");
		sql.append("       AND substr(psm.from_date, 1, 10) <= ? ");
		sql.append("       AND Ifnull (substr(psm.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
		params.add(dateNow);
		sql.append("                       AND substr(pp.from_date, 1, 10) <= ? ");
		sql.append("                       AND Ifnull (substr(pp.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
		params.add(dateNow);

		//Lay ds CT co KM loai KH cua KH
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO1 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO2 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO3 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO4 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO5 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO6 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO7 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO8 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO9 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO10 > 0 ");
		sql.append("              END) ");

//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                       AND ( CASE ");
//			sql.append("                               WHEN (SELECT Count(1) ");
//			sql.append("                                     FROM   promotion_customer_map pcmm ");
//			sql.append("                                     WHERE  pcmm.promotion_shop_map_id = ");
//			sql.append("                                            psm.promotion_shop_map_id and pcmm.status = 1) = 0 ");
//			sql.append("                                 THEN 1 ");
//			sql.append("                               ELSE pcm.customer_id = ? AND pcm.status = 1 END ) ");
//			params.add(customer_id);
//			sql.append("       				AND ( CASE ");
//			sql.append("               				WHEN (SELECT Count(1) ");
//			sql.append("                     				FROM   promotion_staff_map pstmm ");
//			sql.append("                    		 		WHERE  pstmm.promotion_shop_map_id = psm.promotion_shop_map_id and pstmm.status = 1) = 0 ");
//			sql.append("              				 THEN 1 ");
//			sql.append("               				ELSE pstm.staff_id = ? AND pstm.status = 1 END ) ");
//			params.add(staff_id);
//		}

		sql.append("                       AND ( pp.type LIKE 'ZM%' ");
		//neu la don dat hang presale thi moi co load ZH, ZD, ZT
		if (SALE_ORDER_TABLE.ORDER_TYPE_PRESALE.equals(orderType)) {
			sql.append("                              OR PP.type IN ('ZH', 'ZD', 'ZT')  ");
		}
		sql.append("                       ) AND ap.ap_param_code = pp.type ");
		sql.append("                       AND ap.type = 'PROMOTION_MANUAL')) ");
		sql.append("ORDER  BY program_type, ");
		sql.append("          program_code, ");
		sql.append("          program_name ");


		Cursor c = null;
//		String[] params = new String[] { customer_id, staff_id, customerTypeId,
//				customer_id, customer_id, customer_id, customer_id, customer_id };

		// ST
//		String[] params = new String[] { staff_id, shop_id, shop_id, customer_id, customerTypeId,
//				customer_id, customer_id, customer_id, customer_id, customer_id, staff_id};
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ProgrameForProductDTO programeDTO = new ProgrameForProductDTO();
						programeDTO.initPrograme(c);
						// chi co CTTB va CTKM (phai hop le voi cach tinh KM moi) thi moi them vao d/s
						if(programeDTO.isCTKMInvalid){
							result.add(programeDTO);
						}
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * lay d/s san pham de bo xung vao noi dung danh gia NVBH
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ListFindProductSaleOrderDetailViewDTO
	 * @throws:
	 */
	public ListFindProductSaleOrderDetailViewDTO getListProductToAddReviewsStaff(
			Bundle data) throws Exception{

		String dateNow = DateUtils.now();
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		String productCode = data
				.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data
				.getString(IntentConstants.INTENT_PRODUCT_NAME);
//		String progameCode = data.getString(IntentConstants.INTENT_CTKM_CODE);
		// shop id cua NVBH
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		String strListShop = TextUtils.join(",", listShopId);
		// id cua NVBH
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		// id cua khach hang
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
//		// type id cua khach hang
//		String customerTypeId = data.getString(IntentConstants.INTENT_CUSTOMER_TYPE_ID);
		// sale type code cua NVBH
//		String saleTypeCode = data
//				.getString(IntentConstants.INTENT_SALE_TYPE_CODE);
		// id cua GSNPP
		// String supperStaffId = data
		// .getString(IntentConstants.INTENT_STAFF_OWNER_ID);

		boolean isGetTotalItem = data
				.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);

		boolean hasProductCode = !StringUtil.isNullOrEmpty(productCode);
//		boolean hasProgrameCode = !StringUtil.isNullOrEmpty(progameCode);
		boolean hasProductName = !StringUtil.isNullOrEmpty(productName);

		if (!StringUtil.isNullOrEmpty(productCode)) {
			productCode = StringUtil.escapeSqlString(productCode);
			productCode = DatabaseUtils
					.sqlEscapeString("%" + productCode + "%");
			productCode = productCode.substring(1, productCode.length() - 1);
		}
//		if (!StringUtil.isNullOrEmpty(progameCode)) {
//			progameCode = StringUtil.escapeSqlString(progameCode);
//			progameCode = DatabaseUtils
//					.sqlEscapeString("%" + progameCode + "%");
//			progameCode = progameCode.substring(1, progameCode.length() - 1);
//		}
		if (!StringUtil.isNullOrEmpty(productName)) {
			productName = StringUtil.getEngStringFromUnicodeString(productName);
			productName = StringUtil.escapeSqlString(productName);
			productName = DatabaseUtils
					.sqlEscapeString("%" + productName + "%");
			productName = productName.substring(1, productName.length() - 1);
		}

		/**
		 * SQL lay danh sach san pham
		 */
		StringBuffer sql = new StringBuffer();
		ArrayList<String> listParams = new ArrayList<String>();
		sql.append("SELECT DISTINCT p.product_id   AS PRODUCT_ID, ");
		sql.append("                pr_.price_id   AS PRICE_ID, ");
		sql.append("                pr_.price      AS PRICE, ");
		sql.append("                p.product_name AS PRODUCT_NAME, ");
		sql.append("                p.product_code AS PRODUCT_CODE, ");
		sql.append("                st.available_quantity AS AVAILABLE_QUANTITY, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN fp.product_id IS NULL THEN 0 ");
		sql.append("                    ELSE 1 ");
		sql.append("                  END )        AS TT ");

//		sql.append("                ,NEW_PROMOTION.COKHAIBAO1       AS COKHAIBAO1 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO2       AS COKHAIBAO2 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO3       AS COKHAIBAO3 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO4       AS COKHAIBAO4 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO5       AS COKHAIBAO5 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO6       AS COKHAIBAO6 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO7       AS COKHAIBAO7 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO8       AS COKHAIBAO8 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO9       AS COKHAIBAO9 ");
//		sql.append("                ,NEW_PROMOTION.COKHAIBAO10       AS COKHAIBAO10 ");
//		sql.append("                ,NEW_PROMOTION.code       AS CODE ");

		// check bang product & product info
		sql.append(" FROM   (SELECT p.PRODUCT_CODE       product_code, ");
		sql.append("               p.PRODUCT_ID       product_id, ");
		sql.append("               p.PRODUCT_NAME       product_name, ");
		sql.append("               p.NAME_TEXT  product_name_text, ");
		sql.append("               p.convfact  convfact, ");
		sql.append("               p.GROSS_WEIGHT  GROSS_WEIGHT, ");
		sql.append("               p.uom2  uom2, ");
		sql.append("               p.cat_id cat_id ");
		sql.append("        FROM   PRODUCT p, ");
		sql.append("               PRODUCT_INFO pi ");
		sql.append("        WHERE  p.STATUS = 1 ");
		sql.append("               AND pi.STATUS = 1 ");
		sql.append("               AND pi.TYPE = 1 ");
		sql.append("               AND pi.PRODUCT_INFO_ID = p.CAT_ID ");
		sql.append("               AND pi.PRODUCT_INFO_CODE != 'Z') AS P ");

		// check bang stock total
		sql.append("       INNER JOIN ( ");
		//doi cau lay ton kho (nhiu kho)
		sql.append("       SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sql.append("       FROM stock_total st ");
		sql.append("       LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("       WHERE 1=1 ");
		sql.append("    		AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		listParams.add(shopId);
		sql.append("        	AND st.object_id = ? AND st.status = 1 ");
		listParams.add(shopId);
		sql.append("       		AND st.object_type = ? ");
		listParams.add(String.valueOf(StockTotalDTO.TYPE_SHOP));
		sql.append("       group by st.product_id ");
		sql.append("                          					) st ");
		sql.append("               ON st.product_id = p.product_id ");

		// check bang price
		sql.append("       INNER JOIN (");

		//begin lay gia theo cach moi (nhiu gia)
		sql.append("	SELECT prRight.* FROM(	");
		sql.append("	SELECT	");
		sql.append("	    IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(IFNULL(pr_kh_npp,	");
		sql.append("	    pr_kh_vung),	");
		sql.append("	    pr_kh_mien),	");
		sql.append("	    pr_kh_kenh),	");
		sql.append("	    pr_kh_vnm),	");
		sql.append("	    pr_kh_vnm_nhomdv),	");
		sql.append("	    pr_npp),	");
		sql.append("	    pr_vung),	");
		sql.append("	    pr_mien),	");
		sql.append("	    pr_kenh),	");
		sql.append("	    pr_vnm),	");
		sql.append("	    pr_vnm_nhomdv) price_id	");
		sql.append("	FROM	");
		sql.append("	    (SELECT	");
		sql.append("	        prc.price_id,	");
		sql.append("	        prc.PRODUCT_ID PRODUCT_ID,	");
		sql.append("	        prc.CUSTOMER_TYPE_ID,	");
		sql.append("	        prc.shop_id,	");
		sql.append("	        IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	        ''),	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id = ?	");
		listParams.add(shopId);
		sql.append("	            AND prc.shop_id IN                        (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                             (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 3))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = ''	");
		sql.append("	                THEN price_id	");
		sql.append("	            END) pr_kh_npp,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 2))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_vung,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 1))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_mien,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = '0'))	");
		sql.append("	                AND IFNULL(prc.SHOP_CHANNEL, '') = '' THEN price_id	");
		sql.append("	            END) pr_kh_kenh,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND IFNULL(prc.shop_id,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND PRC.SHOP_CHANNEL = SHOP.SHOP_CHANNEL                THEN price_id	");
		sql.append("	        END) pr_kh_vnm_nhomdv,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND prc.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("				AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("				)	");
		sql.append("				AND prc.shop_id IN                  (	");
		sql.append("	    			SELECT	");
		sql.append("	        			shop_id	");
		sql.append("	    			FROM	");
		sql.append("	         			shop	");
		sql.append("	     			WHERE	");
		sql.append("	         			status = 1	");
		sql.append("	         			AND shop_type_id IN                       (	");
		sql.append("	             			SELECT	");
		sql.append("	                 			channel_type_id	");
		sql.append("	             			FROM	");
		sql.append("	                 			channel_type	");
		sql.append("	             			WHERE	");
		sql.append("	                 			TYPE = 1	");
		sql.append("	                 			AND object_type = 14	");
		sql.append("	     			)) THEN price_id	");
		sql.append("	        END) pr_kh_vnm,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (?)	");
		listParams.add(shopId);
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 3)) THEN price_id	");
		sql.append("	            END) pr_npp,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 2)) THEN price_id	");
		sql.append("	            END) pr_vung,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = 1)) THEN price_id	");
		sql.append("	            END) pr_mien,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("	            )	");
		sql.append("	            AND prc.shop_id IN                  (SELECT	");
		sql.append("	                shop_id	");
		sql.append("	            FROM	");
		sql.append("	                shop	");
		sql.append("	            WHERE	");
		sql.append("	                status = 1	");
		sql.append("	                AND shop_type_id IN                       (SELECT	");
		sql.append("	                    channel_type_id	");
		sql.append("	                FROM	");
		sql.append("	                    channel_type	");
		sql.append("	                WHERE	");
		sql.append("	                    TYPE = 1	");
		sql.append("	                    AND object_type = '0')) THEN price_id	");
		sql.append("	            END) pr_kenh,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') <> ''	");
		sql.append("	            AND PRC.SHOP_CHANNEL = SHOP.SHOP_CHANNEL	");
		sql.append("	            AND IFNULL(prc.shop_id,	");
		sql.append("	            '') = '' THEN price_id	");
		sql.append("	        END) pr_vnm_nhomdv,	");
		sql.append("	        MAX(CASE	");
		sql.append("	            WHEN IFNULL(prc.CUSTOMER_TYPE_ID,	");
		sql.append("	            '') = ''	");
		sql.append("	            AND IFNULL(prc.SHOP_CHANNEL,	");
		sql.append("	            '') = ''	");
		sql.append("				AND prc.shop_id IN (	");
		sql.append(strListShop);
		sql.append("				)	");
		sql.append("				AND prc.shop_id IN                  (	");
		sql.append("	    			SELECT	");
		sql.append("	        			shop_id	");
		sql.append("	    			FROM	");
		sql.append("	        			shop	");
		sql.append("	    			WHERE	");
		sql.append("	        			status = 1	");
		sql.append("	        			AND shop_type_id IN                       (	");
		sql.append("	            			SELECT	");
		sql.append("	                			channel_type_id	");
		sql.append("	            			FROM	");
		sql.append("	                			channel_type	");
		sql.append("	            			WHERE	");
		sql.append("	                			TYPE = 1	");
		sql.append("	                			AND object_type = 14	");
		sql.append("	        			)	");
		sql.append("	    			) THEN price_id	");
		sql.append("	        END) pr_vnm	");
		sql.append("	    FROM	");
		sql.append("	        (SELECT	");
		sql.append("	            *	");
		sql.append("	        FROM	");
		sql.append("	            PRICE	");
		sql.append("	        WHERE	");
		sql.append("	            1 = 1	");
		sql.append("	            AND PRICE.STATUS = 1	");
		sql.append("	            AND Ifnull(DATE (PRICE.from_date) <= DATE (?), 0)	");
		listParams.add(dateNow);
		sql.append("	            AND Ifnull(DATE(PRICE.to_date) >= DATE (?), 1)) prc	");
		listParams.add(dateNow);
		sql.append("	    JOIN	");
		sql.append("	        (	");
		sql.append("	            SELECT	");
		sql.append("	                CHANNEL_TYPE_ID AS CUSTOMER_TYPE_ID	");
		sql.append("	            FROM	");
		sql.append("	                CUSTOMER	");
		sql.append("	            WHERE	");
		sql.append("	                CUSTOMER_ID = ?	");
		listParams.add(customerId);
		sql.append("	        ) c	");
		sql.append("	            ON 1=1	");
		sql.append("	    JOIN	");
		sql.append("	        (	");
		sql.append("	            SELECT	");
		sql.append("	                SHOP_CHANNEL	");
		sql.append("	            FROM	");
		sql.append("	                SHOP	");
		sql.append("	            WHERE	");
		sql.append("	                SHOP_ID = ?	");
		listParams.add(shopId);
		sql.append("	        ) shop	");
		sql.append("	            ON 1=1	");
		sql.append("	    GROUP BY	");
		sql.append("	        prc.PRODUCT_ID	");
		sql.append("	)) pri_id, PRICE prRight	");
		sql.append("	WHERE 1=1	");
		sql.append("	and pri_id.price_id = prRight.price_id	");
		//end lay gia theo cach moi (nhieu gia)

		sql.append("                                 ) AS pr_ ");
		sql.append("               ON p.product_id = pr_.product_id ");

		// mat hang trong tam
		sql.append("   LEFT JOIN ( SELECT DISTINCT ");
		sql.append("       fcmd.product_id, ");
		sql.append("       fp.focus_program_id focus_program_code ");
		sql.append("FROM   focus_program fp, ");
		sql.append("       focus_shop_map fsm, ");
		sql.append("       focus_channel_map fcm, ");
		sql.append("       focus_channel_map_product fcmd, staff s ");
		sql.append("WHERE  fp.focus_program_id = fsm.focus_program_id and s.sale_type_code = fcm.sale_type_code and s.staff_id = ? ");
		listParams.add(staffId);
		sql.append("       AND fp.focus_program_id = fcm.focus_program_id ");
		sql.append("       AND fcm.focus_channel_map_id = fcmd.focus_channel_map_id ");
//		sql.append("       AND fcm.sale_type_code = ? ");
//		listParams.add(saleTypeCode);
		sql.append("       AND dayInOrder(fp.from_date) <= dayInOrder('now','localtime')");
		sql.append("       AND ifnull(dayInOrder(fp.to_date) >= dayInOrder('now','localtime'), 1)");
		sql.append("       AND fp.status = 1 ");
		sql.append("       AND fsm.status = 1 ");
		sql.append("       AND fcm.status = 1 ");
		sql.append("       AND fsm.shop_id IN ( ");
		sql.append(strListShop + ") ");
		sql.append("        ) AS FP");
		sql.append("              ON p.product_id = FP.product_id ");

		// Chuong trinh khuyen mai
//		listParams.add(customerTypeId);
//		listParams.add(customerId);
//		listParams.add(customerId);
//		listParams.add(customerId);
//		listParams.add(customerId);
//		listParams.add(customerId);
//		sql.append(" LEFT JOIN ( SELECT distinct pp.promotion_program_code code, pp.promotion_program_id, ");
//		sql.append("                ppd.product_id product_id, ");
//		sql.append("                Ifnull(TTTT1.num_condition, 0) COKHAIBAO1, ");
//		sql.append("                Ifnull(TTTT2.num_condition, 0) COKHAIBAO2, ");
//		sql.append("                Ifnull(TTTT3.num_condition, 0) COKHAIBAO3, ");
//		sql.append("                Ifnull(TTTT4.num_condition, 0) COKHAIBAO4, ");
//		sql.append("                Ifnull(TTTT5.num_condition, 0) COKHAIBAO5, ");
//		sql.append("                Ifnull(TTTT6.num_condition, 0) COKHAIBAO6, ");
//		sql.append("                Ifnull(TTTT7.num_condition, 0) COKHAIBAO7, ");
//		sql.append("                Ifnull(TTTT8.num_condition, 0) COKHAIBAO8, ");
//		sql.append("                Ifnull(TTTT9.num_condition, 0) COKHAIBAO9, ");
//		sql.append("                Ifnull(TTTT10.num_condition, 0) COKHAIBAO10 ");
//		sql.append("FROM   promotion_program pp ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id, ");
//		sql.append("                         Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca ");
//		sql.append("                  WHERE  pca.object_type = 2 ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT1 ");
//		sql.append("              ON TTTT1.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id ");
//		sql.append("                         , ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         promotion_cust_attr_detail pcad ");
//		sql.append("                  WHERE  1 = 1 ");
//		sql.append("                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                             pcad.promotion_cust_attr_id ");
//		sql.append("                         AND pca.object_type = 2 ");
//		sql.append("                         AND pcad.object_id = ? ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND pcad.status = 1 ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT2 ");
//		sql.append("              ON TTTT2.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id       promotion_program_id, ");
//		sql.append("                         Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca ");
//		sql.append("                  WHERE  pca.object_type = 3 ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                  GROUP  BY promotion_program_id) TTTT3 ");
//		sql.append("              ON TTTT3.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id ");
//		sql.append("                         , ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         promotion_cust_attr_detail pcad, ");
//		sql.append("                         sale_level_cat slc, ");
//		sql.append("                         customer_cat_level ccl ");
//		sql.append("                  WHERE  1 = 1 ");
//		sql.append("                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                             pcad.promotion_cust_attr_id ");
//		sql.append("                         AND pcad.object_id = slc.sale_level_cat_id ");
//		sql.append("                         AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
//		sql.append("                         AND pca.object_type = 3 ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND pcad.status = 1 ");
//		sql.append("                         AND slc.status = 1 ");
//		sql.append("                         AND ccl.customer_id = ? ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT4 ");
//		sql.append("              ON TTTT4.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id       promotion_program_id, ");
//		sql.append("                         Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT5 ");
//		sql.append("              ON TTTT5.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id ");
//		sql.append("                         , ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT, ");
//		sql.append("                         attribute_value atv ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND atv.status = 1 ");
//		sql.append("                         AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                         AND atv.table_id = ? ");
//		sql.append("                         AND ( CASE ");
//		sql.append("                                 WHEN AT.value_type = 2 THEN Ifnull( ");
//		sql.append("                                 cast(atv.value as integer) >= pca.from_value, ");
//		sql.append("                                                             1) ");
//		sql.append("                                                             AND ");
//		sql.append("                                 Ifnull(cast(atv.value as integer) <= pca.to_value, 1) ");
//		sql.append("                                 WHEN AT.value_type = 1 THEN ");
//		sql.append("                                 trim(lower(atv.value)) = trim(lower(pca.from_value)) ");
//		sql.append("                                 WHEN AT.value_type = 3 THEN atv.value is not null and Ifnull( ");
//		sql.append("                                 Date(atv.value) >= Date(pca.from_value), ");
//		sql.append("                                                             1) ");
//		sql.append("                                                             AND ");
//		sql.append("                                 Ifnull(Date(atv.value) <= Date(pca.to_value), ");
//		sql.append("                                 1) ");
//		sql.append("                               END ) ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT6 ");
//		sql.append("              ON TTTT6.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id, ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND AT.value_type = 4 ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT7 ");
//		sql.append("              ON TTTT7.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id, ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT, ");
//		sql.append("                         attribute_value atv, ");
//		sql.append("                         attribute_detail atd, ");
//		sql.append("                         attribute_value_detail atvd, ");
//		sql.append("                         promotion_cust_attr_detail pcad ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                         AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                         AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                         AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                         AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                             pcad.promotion_cust_attr_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND atv.status = 1 ");
//		sql.append("                         AND atd.status = 1 ");
//		sql.append("                         AND atvd.status = 1 ");
//		sql.append("                         AND pcad.status = 1 ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                         AND AT.value_type = 4 ");
//		sql.append("                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                         AND atv.table_id = ? ");
//		sql.append("                  GROUP  BY pca.promotion_program_id)TTTT8 ");
//		sql.append("              ON TTTT8.promotion_program_id = pp.promotion_program_id ");
//
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id, ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND AT.value_type = 5 ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT9 ");
//		sql.append("              ON TTTT9.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       LEFT JOIN (SELECT pca.promotion_program_id           promotion_program_id, ");
//		sql.append("       Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                  FROM   promotion_cust_attr pca, ");
//		sql.append("                         attribute AT, ");
//		sql.append("                         attribute_value atv, ");
//		sql.append("                         attribute_detail atd, ");
//		sql.append("                         attribute_value_detail atvd, ");
//		sql.append("                         promotion_cust_attr_detail pcad ");
//		sql.append("                  WHERE  pca.object_type = 1 ");
//		sql.append("                         AND pca.object_id = AT.attribute_id ");
//		sql.append("                         AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                         AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                         AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                         AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                         AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                         AND pca.promotion_cust_attr_id = ");
//		sql.append("                             pcad.promotion_cust_attr_id ");
//		sql.append("                         AND pca.status = 1 ");
//		sql.append("                         AND AT.status = 1 ");
//		sql.append("                         AND atv.status = 1 ");
//		sql.append("                         AND atd.status = 1 ");
//		sql.append("                         AND atvd.status = 1 ");
//		sql.append("                         AND pcad.status = 1 ");
//		sql.append("                         AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                         AND AT.value_type = 5 ");
//		sql.append("                         AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                         AND atv.table_id = ? ");
//		sql.append("                  GROUP  BY pca.promotion_program_id) TTTT10 ");
//		sql.append("              ON TTTT10.promotion_program_id = pp.promotion_program_id, ");
//
//		sql.append("       promotion_program_detail ppd, ");
//		sql.append("       promotion_shop_map psm ");
//		sql.append("       LEFT JOIN promotion_customer_map pcm ");
//		sql.append("              ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("WHERE  1 = 1 ");
//		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       AND pp.promotion_program_id = ppd.promotion_program_id ");
//		sql.append("       AND psm.shop_id IN ( ");
//		sql.append(strListShop + " )");
//		sql.append("       AND pp.status = 1 ");
//		sql.append("       AND psm.status = 1 ");
//		sql.append("       AND Date(pp.from_date) <= Date('now', 'localtime') ");
//		sql.append("       AND Ifnull (Date(pp.to_date) >= Date('now', 'localtime'), 1) ");
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN (SELECT Count(*) ");
//		sql.append("                     FROM   promotion_customer_map pcmm ");
//		sql.append("                     WHERE  pcmm.promotion_shop_map_id = ");
//		sql.append("                            psm.promotion_shop_map_id) = 0 ");
//		sql.append("                 THEN ");
//		sql.append("               1 ");
//		sql.append("               ELSE pcm.customer_id = ? ");
//		sql.append("                    AND pcm.status = 1 ");
//		sql.append("             END ) ) NEW_PROMOTION on NEW_PROMOTION.product_id = p.product_id ");

		// dk where
		sql.append(" WHERE  1 = 1 ");
		sql.append("       AND PR_.price > 0 ");

		if (hasProductCode) {
			sql.append("AND upper(p.product_code) LIKE upper(?) escape '^' ");
			listParams.add(productCode);
		}
//		if (hasProgrameCode) {
//			sql.append("AND upper(NEW_PROMOTION.code) LIKE upper(?) escape '^' ");
//			listParams.add(progameCode);
//		}
		if (hasProductName) {
			sql.append("AND upper(p.product_name_text) LIKE upper(?) escape '^' ");
			listParams.add(productName);
		}
		sql.append("ORDER BY tt DESC, product_code ASC, product_name ASC");

		/**
		 * sql lay tong so luong dong
		 */
		String getCountProductList = " select count(*) as total_row from ("
				+ sql.toString() + ") ";

		// khoi tao tham so cho sql lay danh sach san pham
		String[] paramsGetListProduct = new String[] {};
		paramsGetListProduct = listParams
				.toArray(new String[listParams.size()]);

		ListFindProductSaleOrderDetailViewDTO listResult = new ListFindProductSaleOrderDetailViewDTO();
		Cursor c = null;
		Cursor cTmp = null;
		//long startTime = System.currentTimeMillis();
		try {
			// get total row first
			int total = 0;
			if (isGetTotalItem) {
				cTmp = rawQuery(getCountProductList, paramsGetListProduct);
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					listResult.totalObject = total;
				}
			}
			// end
			c = rawQuery(sql + ext,
					paramsGetListProduct);

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						FindProductSaleOrderDetailViewDTO orderJoinTableDTO = new FindProductSaleOrderDetailViewDTO();
						orderJoinTableDTO
								.initSaleOrderDetailObjectFromGetProductStatement(c);
						listResult.listObject.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}

		} finally {
			if (c != null) {
				c.close();
			}
			if (isGetTotalItem && cTmp != null) {
				cTmp.close();
			}
		}

//		Log.v("endTime", String.valueOf(System.currentTimeMillis() - startTime));
		return listResult;
	}



	/**
	 * delete chi tiet don hang truoc 1 thang
	 * @author banghn
	 * @return
	 */
	public long deleteOldSaleOrderDetail() {
		long success = -1;
		try {
			String startOfMonth = DateUtils
					.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, -2);
			String[] params = {startOfMonth};
			mDB.beginTransaction();
			//xoa don hang detail giu lai trong 3 thang hien tai
			StringBuffer sqlDel = new StringBuffer();
			sqlDel.append("  ?  > dayInOrder(ORDER_DATE) ");
			delete(sqlDel.toString(), params);

			success = 1;
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("deleteOldSaleOrderDetail", "fail", e);
			success = -1;
		} finally {
			mDB.endTransaction();
		}
		return success;
	}



	 /**
	 * Lay detail cua tra thuong keyshop theo tung sp
	 * @author: Tuanlt11
	 * @param orderDetailDTO
	 * @param customerId
	 * @throws Exception
	 * @return: void
	 * @throws:
	*/
	private void getPromoDetailKeyShopOfOrderDetail(OrderDetailViewDTO orderDetailDTO, long customerId) throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append(" SELECT sopd.*,KS.KS_ID PROGRAM_ID ");
		sql.append(" FROM sale_order_promo_detail sopd,  ks ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND sopd.sale_order_detail_id = ? ");
		params.add(String.valueOf(orderDetailDTO.orderDetailDTO.salesOrderDetailId));
		sql.append(" AND sopd.PROGRAM_CODE = ks.KS_CODE ");
		sql.append(" AND sopd.PROGRAM_TYPE = ? ");
		params.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleOrderPromoDetailDTO promoDetailDTO = new SaleOrderPromoDetailDTO();
						promoDetailDTO.parseDataFromCursor(c);
						orderDetailDTO.listPromoDetail.add(promoDetailDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
			}
		}
	}

}