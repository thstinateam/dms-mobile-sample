/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * Danh sach khach hang tham gia chuong trinh trung bay
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class ListCustomerAttentProgrameDTO implements Serializable{
	private static final long serialVersionUID = 1334103252923500874L;
	private ArrayList<CustomerAttentProgrameDTO> listCustomer = new ArrayList<CustomerAttentProgrameDTO>();
	private int totalSize;
	/**
	 * @return the listCustomer
	 */
	public ArrayList<CustomerAttentProgrameDTO> getListCustomer() {
		return listCustomer;
	}
	/**
	 * @param listCustomer the listCustomer to set
	 */
	public void setListCustomer(ArrayList<CustomerAttentProgrameDTO> listCustomer) {
		this.listCustomer = listCustomer;
	}
	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}
	/**
	 * @param total the totalSize to set
	 */
	public void setTotalSize(int total) {
		this.totalSize = total;
	}
	
}
