package com.ths.dmscore.dto.view;

import com.ths.dmscore.util.ObjectItemForSpinner;

public interface ObjectAdapterForSpinner {
	public ObjectItemForSpinner castObjectToObjectItem(Object object);
}


