/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.model.SynDataModelService;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;

/**
 *  Controller xu ly dong bo
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class SynDataController extends AbstractController {

	static volatile SynDataController instance;

	protected SynDataController() {
	}

	public static SynDataController getInstance() {
		if (instance == null) {
			instance = new SynDataController();
		}
		return instance;
	}

	@Override
	public void handleViewEvent(final ActionEvent e) {
		if (e.isUsingAsyntask) {
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {
					SynDataModelService.getInstance().handleControllerEvent(
							SynDataController.this, e);
					GlobalBaseActivity base = null;
					if (e.sender instanceof Activity) {
						base = (GlobalBaseActivity) e.sender;
					} else if (e.sender instanceof Fragment) {
						base = (GlobalBaseActivity) ((Fragment) e.sender).getActivity();
					}
					if (e.request != null && base != null) {
						base.addProcessingRequest(e.request, e.isBlockRequest);
					}
					return null;
				}
			};
			task.execute();
		} else {
			SynDataModelService.getInstance().handleControllerEvent(
					SynDataController.this, e);
		}
	}

	@Override
	public void handleModelEvent(final ModelEvent modelEvent) {
		if (modelEvent.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
			final ActionEvent e = modelEvent.getActionEvent();
			HTTPRequest request = e.request;
			if (e.sender != null && (request == null || (request != null && request.isAlive()))) {
				if (e.sender instanceof GlobalBaseActivity) {
					final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
					if (sender == null || sender.isFinished)
						return;
					sender.runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.isFinished)
								return;
							sender.handleModelViewEvent(modelEvent);
						}
					});
				} else if (e.sender instanceof BaseFragment) {
					final BaseFragment sender = (BaseFragment) e.sender;
					if (sender == null || sender.getActivity() == null || sender.isFinished) {
						return;
					}
					sender.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.getActivity() == null || sender.isFinished) {
								return;
							}
							sender.handleModelViewEvent(modelEvent);
						}
					});
				}
			} else {
				modelEvent.setIsSendLog(false);
				handleErrorModelEvent(modelEvent);
			}
		}
	}

	@Override
	public void handleErrorModelEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		super.handleErrorModelEvent(modelEvent);
	}

	@Override
	public boolean handleSwitchFragment(ActionEvent e) {
		return false;
	}

	@Override
	public void handleSwitchActivity(ActionEvent e) {

	}
}
