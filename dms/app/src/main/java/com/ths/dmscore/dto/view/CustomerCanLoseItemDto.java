package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

public class CustomerCanLoseItemDto implements Serializable {
	private static final long serialVersionUID = 1L;
	public String cusCode;
	public String cusName;
	public String cusAdd;
	public String cusLastApproveOrder;
	public String cusLastOrder;

	public CustomerCanLoseItemDto() {
	}

	public void initDataFromCursor(Cursor c) {
		try {
			if (c == null) {
				throw new Exception("Cursor is empty");
			}

			cusCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			cusName = CursorUtil.getString(c, "CUSTOMER_NAME");
			cusAdd = CursorUtil.getString(c, "ADDRESS");
			if (StringUtil.isNullOrEmpty(cusAdd)) {
					cusAdd = CursorUtil.getString(c, "STREET");
			}
//			cusAdd = StringUtil.isNullOrEmpty(c.getString(c.getColumnIndexOrThrow("STREET"))) ? "" : c.getString(c
//					.getColumnIndexOrThrow("STREET"));
//			cusAdd = CursorUtil.getString(c, "STREET");
			cusLastApproveOrder = CursorUtil.getString(c, "LAST_APPROVE_ORDER");
			cusLastOrder = CursorUtil.getString(c, "LAST_ORDER");
		} catch (Exception e) {
		}

	}

}
