package com.ths.dmscore.view.listener;

import android.location.LocationListener;

import com.ths.dmscore.util.locating.Locating;

/**
 *  lang nghe cac su kien dinh vi co time out (interface)
 *  @author: AnhND
 *  @version: 1.0
 *  @since: 1.0
 */
public interface LocatingListener extends LocationListener{
	public void onTimeout (Locating locating);
}