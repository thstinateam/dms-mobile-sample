/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SynDataController;
import com.ths.dmscore.dto.syndata.SynDataDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.lib.sqllite.download.DownloadFile;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.lib.sqllite.sync.SynDataTableDAO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.main.LoginView;
import com.ths.dmscore.dto.DBVersionDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.syndata.SynDataTableDTO;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Model dong bo
 *
 * @author: ThuatTQ
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class SynDataModelService extends AbstractModelService {

	protected static volatile SynDataModelService instance;

	protected SynDataModelService() {
	}

	public static SynDataModelService getInstance() {
		if (instance == null) {
			instance = new SynDataModelService();
		}
		return instance;
	}

	public void onReceiveData(HTTPMessage mes) {

		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		String msgText = mes.getDataText();
		model.setDataText(msgText);
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SynDataController.getInstance().handleErrorModelEvent(model);
			return;
		}

		// chung thuc thoi gian sai
		if (!StringUtil.isNullOrEmpty(msgText)
				&& msgText.contains(Constants.EXPIRED_TIMESTAMP)) {
			model.setModelCode(ErrorConstants.ERROR_EXPIRED_TIMESTAMP);
			SynDataController.getInstance().handleErrorModelEvent(model);
			return;
		}

		switch (mes.getAction()) {
		case ActionEventConstant.ACTION_GET_LINK_SQL_FILE: {
			JSONObject json;

			try {
				json = new JSONObject((String) mes.getDataText());
				//MyLog.d("SynData", DateUtils.now()
				//		+ ": Respone url db : " + json.toString());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					DBVersionDTO glo = new DBVersionDTO();
					// parse va luu tru cac gia tri
					glo.parseGetLinkDB(result.getJSONObject("response"));
					model.setModelData(glo);
					SynDataController.getInstance().handleModelEvent(model);
				} else {
					//MyLog.d("SynData", DateUtils.now()
					//		+ ": Error sync data : " + json.toString());
					model.setModelMessage(result.getString("errorMessage"));
					SynDataController.getInstance()
							.handleErrorModelEvent(model);
				}

			} catch (JSONException e) {
				MyLog.d("SynData", DateUtils.now()
						+ ": Exception get sql file : " + e.toString());
				 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
			} catch (Exception e) {
				MyLog.d("SynData", DateUtils.now()
						+ ": Exception get sql file : " + e.toString());
				 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
			}
			break;
		}
		case ActionEventConstant.ACTION_SYN_SYNDATA:
		case ActionEventConstant.ACTION_SYN_SYNDATA_MAX: {
			try {
				String dataText = (String) mes.getDataText();
				JSONObject json = new JSONObject(dataText);
				JSONObject result = json.getJSONObject("result");
				JSONObject response = result.getJSONObject("response");
				// get response & error code
				int errCode = result.getInt("errorCode");
				if (ErrorConstants.ERROR_CODE_SUCCESS == errCode) {
					int type = response.getInt("type");
					if (SynDataDTO.TYPE_FILE == type) {
						String url = response.getString("rowData");
						String fileName = StringUtil
								.getFileNameRromURLString(url);
						MyLog.e("SYNDATA", "Get file dong bo du lieu: " + url);
						downloadFileUpdateAndUnzip(url, fileName);

						// doc file va tien hanh update dong bo
						File file = new File(ExternalStorage.getPathSynData(), fileName);
						if (file.exists()) {
							executeSynDataFile(model, file, response);
						} else {
							ServerLogger.sendLog(
									"SYNDATA_FILE_NOT_FOUND",
									"File not exists: "
											+ file.getAbsolutePath()
											+ "  - Respones: " + dataText,
									TabletActionLogDTO.LOG_EXCEPTION);
							model.setDataText("Exception sync res  : File not exists");
							model.setModelCode(ErrorConstants.ERROR_NOT_FOUND);
							SynDataController.getInstance()
									.handleErrorModelEvent(model);
							GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
						}
						file = null;
					} else {
						MyLog.e("SYNDATA", "Dong bo du lieu theo rowdata");
						executeSynDataRows(model, result);
					}
				} else {
					// log loi len server truong hop dong bo tra ve loi
					if (errCode != ErrorConstants.ERROR_SESSION_RESET) {
						ServerLogger.sendLog("SYNDATA_ERROR",
								"Param: " + model.getParams()
										+ "  - Respones: " + dataText,
								TabletActionLogDTO.LOG_SERVER);
					}
					model.setDataText("Exception sync res: " + dataText);
					model.setModelCode(errCode);
					SynDataController.getInstance()
							.handleErrorModelEvent(model);
					GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
				}
			} catch (JSONException e) {
				MyLog.e("SYNDATA_ERROR JSONException", e);
				ServerLogger.sendLog("SYNDATA_ERROR", "JSONException: " + VNMTraceUnexceptionLog.getReportFromThrowable(e) + " || Param: " + model.getParams() + " || DataText: " + model.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}  catch (DownloadFile.DMSDownloadException e) {
				MyLog.e("SYNDATA_ERROR DMSDownloadException", e);
				ServerLogger.sendLog("SYNDATA_ERROR", "DMSDownloadException: " + VNMTraceUnexceptionLog.getReportFromThrowable(e) + " || Param: " + model.getParams() + " || DataText: " + model.getDataText(), TabletActionLogDTO.LOG_EXCEPTION);
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			} catch (Exception e) {
				MyLog.e("SYNDATA_ERROR", e);
				ServerLogger.sendLog("SYNDATA_ERROR", MyLog.printStackTrace(e), TabletActionLogDTO.LOG_EXCEPTION);
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}
			break;
		}case ActionEventConstant.ACTION_SYN_PROMOTION: {
			try {
				String dataText = (String) mes.getDataText();
				JSONObject json = new JSONObject(dataText);
				JSONObject result = json.getJSONObject("result");
				JSONObject response = result.getJSONObject("response");
				// get response & error code
				int errCode = result.getInt("errorCode");
				if (ErrorConstants.ERROR_CODE_SUCCESS == errCode) {
					long promotionId = (Long) model.getActionEvent().userData;
					clearOldDataPromotion(promotionId);
					int type = response.getInt("type");
					if (SynDataDTO.TYPE_FILE == type) {
						String url = response.getString("rowData");
						String fileName = StringUtil.getFileNameRromURLString(url);
						MyLog.e("SYN_PROMOTION", "Get file dong bo du lieu: " + url);
						downloadFileUpdateAndUnzip(url, fileName);

						// doc file va tien hanh update dong bo
						File file = new File(ExternalStorage.getPathSynData(),
								fileName);
						if (file.exists()) {
							executeSynProgrameDataFile(model, file, response);
						} else {
							ServerLogger.sendLog("SYN_PROMOTION", "File not exists: "
									+ file.getAbsolutePath() + "  - Respones: " + dataText,
									TabletActionLogDTO.LOG_CLIENT);
							model.setDataText("Exception sync res  : File not exists");
							model.setModelCode(ErrorConstants.ERROR_COMMON);
							SynDataController.getInstance()
									.handleErrorModelEvent(model);
							GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
						}
						file = null;
					} else {
						executeSynDataRows(model, result);
					}
				} else {
					// log loi len server truong hop dong bo tra ve loi
					if (errCode != ErrorConstants.ERROR_SESSION_RESET) {
						ServerLogger.sendLog("SYN_PROMOTION",
								"Param: " + model.getParams()+ "  - Respones: " + dataText,
								TabletActionLogDTO.LOG_SERVER);
					}
					model.setDataText("Exception sync res: " + dataText);
					model.setModelMessage(result.getString("errorMessage"));
					model.setModelCode(errCode);
					SynDataController.getInstance().handleErrorModelEvent(model);
					GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
				}
			} catch (Exception e) {
				MyLog.e("SYN_PROMOTION", e);
				ServerLogger.sendLog("SYN_PROMOTION",
						VNMTraceUnexceptionLog.getReportFromThrowable(e),
						TabletActionLogDTO.LOG_EXCEPTION);
				model.setIsSendLog(false);
				model.setModelMessage(e.getMessage());
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}
			break;
		}
		default:
			break;
		}
	}

	public void onReceiveError(HTTPResponse response) {
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		MyLog.d("Syndata", DateUtils.now() + "- onReceiveError: "
				+ response.getDataText() + " ErrorCode: " + response.getErrorCode());
		ModelEvent model = new ModelEvent();
		String msgText = response.getDataText();
		model.setDataText(msgText);
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		// chung thuc thoi gian sai
		if (!StringUtil.isNullOrEmpty(msgText)
				&& msgText.contains(Constants.EXPIRED_TIMESTAMP)) {
			model.setModelCode(ErrorConstants.ERROR_EXPIRED_TIMESTAMP);
		} else {
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
		}
		model.setModelMessage(response.getErrMessage());
		SynDataController.getInstance().handleErrorModelEvent(model);

	}

	@Override
	public Object requestHandleModelData(ActionEvent e) throws Exception {
		Object data = null;
		switch (e.action) {
		case ActionEventConstant.ACTION_SYN_SYNDATA_MAX:
		case ActionEventConstant.ACTION_SYN_SYNDATA:
			data = requestSynData(e);
			break;
		case ActionEventConstant.ACTION_GET_LINK_SQL_FILE:
			data = requestGetLinkSqlFile(e);
			break;
		case ActionEventConstant.ACTION_SYN_PROMOTION:
			data = requestSynPromotionData(e);
			break;
		default:
			break;
		}
		return data;
	}

	/**
	 * Dong bo du lieu voi RowData tu server tra ve Moi lan tra la N dong du
	 * lieu dong goi kieu json
	 *
	 * @author: BANGHN
	 * @param model
	 * @param result
	 * @throws Exception
	 */
	private void executeSynDataRows(ModelEvent model, JSONObject result)
			throws Exception {
		int errCode = result.getInt("errorCode");
		model.setModelCode(errCode);
		if (GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_CANCELED) {
			return;
		}
		if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_EXECUTING);
			JSONObject response = result.getJSONObject("response");
			// lay cac tham so tra ve
			SynDataDTO synDataDTO = new SynDataDTO();
			synDataDTO
					.setLastLogId_update(response.getLong("lastLogId_update"));
			synDataDTO.setMaxDBLogId(response.getLong("maxDBLogId"));
			synDataDTO.setState(response.getString("state"));

			// neu co kem theo Script thi chay Script truoc
			if (response.has("dailyFirstSynScript")) {
				String scripts = response.getString("dailyFirstSynScript");
				if (!StringUtil.isNullOrEmpty(scripts)) {
					ArrayList<String> listScript = new ArrayList<String>();
					try {
						JSONArray json = new JSONArray(scripts);
						if (json != null) {
							for (int i = 0; i < json.length(); i++) {
								listScript.add(json.getString(i));
							}
						}
					} catch (JSONException e1) {
					}
					SynDataTableDAO synDataDAO = new SynDataTableDAO();
					synDataDAO.executeDBScript(listScript);
				}
			}

			JSONArray rowData = null;

			try {
				rowData = response.getJSONArray("rowData");
			} catch (JSONException ex) {
				// TODO: handle exception
			}
			if (rowData != null && rowData.length() > 0) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
				for (int i = 0, size = rowData.length(); i < size; i++) {
					synDataDTO.getRowData().add(
							mapper.readValue(rowData.get(i).toString(),
									SynDataTableDTO.class));
				}
			}
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			Editor prefsPrivateEditor = sharedPreferences.edit();
			long lastLogId = Long
					.parseLong(String.valueOf(model.getActionEvent().userData));
			boolean isUpdateSuccess = true;
			// save data to DB.
			if (synDataDTO.getRowData() != null
					&& synDataDTO.getRowData().size() > 0) {

				SynDataTableDAO dao = new SynDataTableDAO(
						(List<SynDataTableDTO>) synDataDTO.getRowData());
				try {
					dao.synData(lastLogId, synDataDTO.getLastLogId_update());
				} catch (Exception ex) {
					isUpdateSuccess = false;
					//MyLog.d("SYN", DateUtils.now()
					//		+ "- Insert SynData failure " + ex.toString());
				}
			}
			if (isUpdateSuccess) {
				// lay role, shop dang dang nhap
				String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
				// cap nhat lastLog id
				if (synDataDTO.getLastLogId_update() > 0){
					prefsPrivateEditor.putString(roleShop + LoginView.LAST_LOG_ID,
							Long.toString(synDataDTO.getLastLogId_update()));
				}
				prefsPrivateEditor.commit();
				//MyLog.d("SYN DATA",
				//		DateUtils.now() + " Cap nhat lastLog ok........ "
				//				+ synDataDTO.getLastLogId_update());
				// Goi xu ly len tan tren
				model.setModelData(synDataDTO);
				model.setIsSendLog(false);
				SynDataController.getInstance().handleModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			} else {
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}

		} else {
			model.setIsSendLog(false);
			//MyLog.d("SynData : ",
			//		DateUtils.now() + " - " + model.getDataText());
			if (errCode != ErrorConstants.ERROR_SESSION_RESET) {
				ServerLogger.sendLog(model.getParams(), model.getDataText(),
						false, TabletActionLogDTO.LOG_SERVER);
			}
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SynDataController.getInstance().handleErrorModelEvent(model);
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
		}
	}

	/**
	 * Dong bo bang cach doc du lieu tu file File chua: - Thong tin lastLogId,
	 * trang thai - N rows Data, moi row la M dong du lieu
	 *
	 * @author: BANGHN
	 * @param model
	 * @param file
	 *            : File sau khi download tu server
	 * @throws Exception
	 */
	private void executeSynDataFile(ModelEvent model, File file, JSONObject response) throws Exception {
		// xem nhu co file roi la thanh cong, neu qua trinh syn loi thi tra ve
		// loi
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_EXECUTING);
		// Get the text file
		if (file.exists()) {
			// try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			SynDataDTO synDataDTO = new SynDataDTO();
			synDataDTO.setLastLogId_update(response.getLong("lastLogId_update"));
			synDataDTO.setMaxDBLogId(response.getLong("maxDBLogId"));
			synDataDTO.setState(response.getString("state"));
			// luu lastlog_id vao share preference
			// SharedPreferences sharedPreferences = GlobalInfo
			// .getInstance()
			// .getActivityContext()
			// .getSharedPreferences(
			// GlobalInfo.getInstance().getActivityContext()
			// .getPackageName(), Context.MODE_PRIVATE);
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			Editor prefsPrivateEditor = sharedPreferences.edit();

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			boolean isUpdateSuccess = true;
			long lastLogId = Long.parseLong((String) model.getActionEvent().userData);

			// uu luong khoan maxid o mot dong
			long totalline = GlobalUtil.countLineOfFile(file);
			long tmpLogId = (long) ((double) (synDataDTO.getLastLogId_update() - lastLogId) / (double) totalline);
			long lineIndex = 0;
			while ((line = br.readLine()) != null) {
				lineIndex++;
				synDataDTO.getRowData().add(mapper.readValue(line, SynDataTableDTO.class));
				SynDataTableDAO dao = new SynDataTableDAO((List<SynDataTableDTO>) synDataDTO.getRowData());
				try {
					dao.synData(lastLogId, synDataDTO.getLastLogId_update());
				} catch (Exception ex) {
					isUpdateSuccess = false;
					MyLog.d("SYN", DateUtils.now() + "- Insert SynData failure " + ex.toString());
				}
				Bundle b = new Bundle();
				b.putLong(IntentConstants.INTENT_LAST_LOG_ID, lastLogId + (tmpLogId * lineIndex));
				b.putLong(IntentConstants.INTENT_MAX_LOG_ID, synDataDTO.getMaxDBLogId());
				((GlobalBaseActivity) GlobalInfo.getInstance().getActivityContext()).sendBroadcast(ActionEventConstant.ACTION_SYN_PERCENT, b);
				synDataDTO.getRowData().clear();
			}
			br.close();
			if (isUpdateSuccess) {
				// lay role va shop dang dang nhap
				String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
				// cap nhat lastLog id
				if (synDataDTO.getLastLogId_update() > 0)
					prefsPrivateEditor.putString(roleShop
							+ LoginView.LAST_LOG_ID,
							Long.toString(synDataDTO.getLastLogId_update()));
				prefsPrivateEditor.commit();
				// Goi xu ly len tan tren
				model.setModelData(synDataDTO);
				model.setIsSendLog(false);
				SynDataController.getInstance().handleModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			} else {
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}
		} else {
			// MyLog.logToFile("SynData", DateUtils.now()
			// + ": Exception sync res  : File not exists");
			model.setDataText("Exception sync res  : File not exists");
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SynDataController.getInstance().handleErrorModelEvent(model);
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
		}
	}

	/**
	 * Download .zip file specified by url, then unzip it to a folder in
	 * external storage.
	 *
	 * @param url
	 * @throws Exception 
	 */
	private void downloadFileUpdateAndUnzip(String url, String fileName) throws Exception {
		File zipDir = ExternalStorage.getPathSynData();
		File zipFile = new File(ExternalStorage.getPathSynData(), fileName
				+ ".zip");
		File outputDir = ExternalStorage.getPathSynData();

		try {
			DownloadFile.downloadWithURLConnection(url, zipFile, zipDir);
			DownloadFile.unzipFile(GlobalInfo.getInstance().getAppContext(),
					zipFile, outputDir);
		} catch (Exception e) {
			MyLog.e("downloadFileUpdateAndUnzip", e);
			throw e;
		} finally {
			if (zipFile != null && zipFile.exists()) {
				zipFile.delete();
			}
		}
	}

	/**
	 * Request dong bo du lieu luc nhan cap nhat
	 *
	 * @param actionEvent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent requestSynData(ActionEvent actionEvent) throws Exception {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		Vector<String> info = (Vector<String>) actionEvent.viewData;
		re = sendHttpRequestSyndata("synDataController/getDataFromServer", info, actionEvent);
		return new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Request lay link download file DB
	 *
	 * @author : BangHN since : 11:56:17 AM
	 */
	@SuppressWarnings("unchecked")
	public Object requestGetLinkSqlFile(ActionEvent actionEvent)
			throws Exception {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		Vector<String> info = (Vector<String>) actionEvent.viewData;
		re = sendHttpRequest("synDataController/createSQLiteFile", info, actionEvent,
				NetworkTimeout.getREAD_TIME_OUT_DOWNLOAD_FILE());
		return  new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

//	/**
//	 * Request Finish Execute Daily first synchronize script
//	 *
//	 * @param actionEvent
//	 * @return
//	 */
//	public HTTPRequest requestUpdateLastDailyFirstSynScriptExecutionDate(ActionEvent actionEvent) {
//		HTTPRequest re = null;
//		try {
//			@SuppressWarnings("unchecked")
//			Vector<String> info = (Vector<String>) actionEvent.viewData;
//			re = sendHttpRequest("synDataController/updateStaffSynDataStatus", info, actionEvent);
//
//		} catch (Exception ex) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//		return re;
//	}

	/**
	 * clear detail chuong trinh km sai
	 * @author: banghn
	 * @param promotionProgrameId
	 * @throws Exception
	 */
	private void clearOldDataPromotion(long promotionProgrameId) throws Exception{
		StringBuilder sql = new StringBuilder();
		//Xoa bang map
		sql.append(" DELETE FROM GROUP_MAPPING ");
		sql.append(" WHERE PROMO_GROUP_ID IN (SELECT PGP.PRODUCT_GROUP_ID ");
		sql.append(" 					FROM PRODUCT_GROUP PGP ");
		sql.append(" 					WHERE PGP.PROMOTION_PROGRAM_ID = ? ); ");

		//Xoa cac chi tiet
		sql.append(" DELETE FROM GROUP_LEVEL_DETAIL ");
		sql.append(" WHERE GROUP_LEVEL_ID IN ( ");
		sql.append(" 			SELECT GLL.GROUP_LEVEL_ID ");
		sql.append(" 			FROM GROUP_LEVEL GLL ");
		sql.append(" 			WHERE GLL.PRODUCT_GROUP_ID IN (SELECT PGP.PRODUCT_GROUP_ID ");
		sql.append(" 							FROM PRODUCT_GROUP PGP ");
		sql.append(" 							WHERE PGP.PROMOTION_PROGRAM_ID = ? ) ");
		sql.append(" 			); ");

		//Xoa cac muc
		sql.append(" DELETE FROM GROUP_LEVEL ");
		sql.append(" WHERE PRODUCT_GROUP_ID IN (SELECT PGP.PRODUCT_GROUP_ID ");
		sql.append(" 		FROM PRODUCT_GROUP PGP ");
		sql.append(" 		WHERE PGP.PROMOTION_PROGRAM_ID = ? ); ");

		//Xoa cac nhom
		sql.append(" DELETE FROM PRODUCT_GROUP ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ?; ");

		//Xoa chuong trinh
		sql.append(" DELETE FROM PROMOTION_PROGRAM ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ? ");

		String[] query = sql.toString().split(";");
		String[] params = { "" + promotionProgrameId };
		SQLUtils.getInstance().executeDBScripts(query, params);

		//Xoa cac bang map
		sql.setLength(0);
		sql.append(" DELETE FROM PROMOTION_CUSTOMER_MAP ");
		sql.append(" WHERE PROMOTION_SHOP_MAP_ID IN ( ");
		sql.append(" SELECT PROMOTION_SHOP_MAP_ID FROM PROMOTION_SHOP_MAP ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ? ");
		sql.append(" AND SHOP_ID = ? ");
		sql.append(" ) ");
		sql.append(" AND SHOP_ID = ?; ");

		sql.append(" DELETE FROM PROMOTION_STAFF_MAP ");
		sql.append(" WHERE PROMOTION_SHOP_MAP_ID IN ( ");
		sql.append(" SELECT PROMOTION_SHOP_MAP_ID FROM PROMOTION_SHOP_MAP ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ? ");
		sql.append(" AND SHOP_ID = ? ");
		sql.append(" ) ");
		sql.append(" AND SHOP_ID = ?; ");

		sql.append(" DELETE FROM PROMOTION_SHOP_MAP ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ? ");
		sql.append(" AND SHOP_ID = ? AND SHOP_ID = ?");

		query = sql.toString().split(";");
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		params = new String[] { "" + promotionProgrameId, shopId, shopId };
		SQLUtils.getInstance().executeDBScripts(query, params);
	}

	/**
	 * Dong bo du lieu promotion tu file
	 * @author: banghn
	 * @throws Exception
	 */
	private void executeSynProgrameDataFile(ModelEvent model, File file, JSONObject response) throws Exception {
		//xem nhu co file roi la thanh cong, neu qua trinh syn loi thi tra ve loi
		model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_EXECUTING);
		// Get the text file
		if (file.exists()) {
			SynDataDTO synDataDTO = new SynDataDTO();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			boolean isUpdateSuccess = true;
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				synDataDTO.getRowData().add(
						mapper.readValue(line, SynDataTableDTO.class));
				SynDataTableDAO dao = new SynDataTableDAO(
						(List<SynDataTableDTO>) synDataDTO.getRowData());
				try {
					dao.synData();
				} catch (Exception ex) {
					isUpdateSuccess = false;
					ServerLogger.sendLog("SYNDATA",
							VNMTraceUnexceptionLog.getReportFromThrowable(ex),
							true, TabletActionLogDTO.LOG_CLIENT);
				}
				synDataDTO.getRowData().clear();
			}
			br.close();
			if (isUpdateSuccess) {
				// Goi xu ly len tan tren
				model.setModelData(synDataDTO);
				model.setIsSendLog(false);
				SynDataController.getInstance().handleModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}else{
				model.setIsSendLog(false);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				SynDataController.getInstance().handleErrorModelEvent(model);
				GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
			}
		} else {
			model.setDataText("Exception sync res  : File not exists");
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SynDataController.getInstance().handleErrorModelEvent(model);
			GlobalInfo.getInstance().setStateSynData(GlobalInfo.SYNDATA_NONE);
		}
	}

	/**
	 * Request get thong tin dong bo chuong trinh km sai
	 * @author : BangHN since : 11:56:17 AM
	 */
	@SuppressWarnings("unchecked")
	public Object requestSynPromotionData(ActionEvent actionEvent) throws Exception {
		HTTPRequest re = null;
		actionEvent.type = ActionEvent.TYPE_ONLINE;
		Vector<String> info = (Vector<String>) actionEvent.viewData;
		re = sendHttpRequest("promotionProgrameController/getPromotionPrograme", info, actionEvent,
				NetworkTimeout.getREAD_TIME_OUT_DOWNLOAD_FILE());
		return  new ModelEvent(actionEvent, re,
				ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
}
