/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ths.dmscore.dto.view.AttachDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.FEEDBACK_STAFF_TABLE;
import com.ths.dmscore.lib.sqllite.db.FEED_BACK_TABLE;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Thong tin dia ban
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class FeedBackDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 4857905436420700937L;
	// nvbh
	public static final String FEEDBACK_TYPE_AP_NVBH = "FEEDBACK_TYPE_NVBH";
	// gsnpp
	public static final String FEEDBACK_TYPE_AP_GSNPP = "FEEDBACK_TYPE_GSNPP";
	// xoa
	public static final int FEEDBACK_STATUS_DELETE = 0;
	// tiep nhan
	public static final int FEEDBACK_STATUS_CREATE = 0;
	// user nhan feedback da xu ly
	public static final int FEEDBACK_STATUS_STAFF_DONE = 1;
	// nguoi quan ly da xu ly
	public static final int FEEDBACK_STATUS_STAFF_OWNER_DONE = 2;
	// feed back type 1
	public static final int FEEDBACK_TYPE_SERVICE = 0;
	// feed back type 2
	public static final int FEEDBACK_TYPE_PRODUCT = 1;
	// feed back type 2
	public static final int FEEDBACK_TYPE_PROGRAME_HTTM = 2;
	// feed back type 2
	public static final int FEEDBACK_TYPE_DISPLAY = 3;
	// feed back type 2
	public static final int FEEDBACK_TYPE_SHELF = 4;
    // feed back type 2
	public static final int FEEDBACK_TYPE_CTTB = 5;
	// feed back ve chuong trinh trung bay
	public static final int FEEDBACK_TYPE_ABOUT_DISPLAY = 6;
	// feed back ho tro NVBH: gom cac danh gia ve ki nang NVBH
	public static final int FEEDBACK_TYPE_SUPPORT_NVBH = 7;
	// feed back tich luy ve doanh so: gom cac danh gia ve cai
	//thien trung bay , phan anh khach hang
	public static final int FEEDBACK_TYPE_AMOUNT = 8;
	// feed back type 9 (SKU)
	public static final int FEEDBACK_TYPE_SKU = 9;

	// nhan xet chung
	public static final int FEEDBACK_TYPE_GENERAL = 10;
	// phan phoi va trung bay
	public static final int FEEDBACK_TYPE_DISTRIBUTION_DISPLAY = 11;
	// dich vu khach hang
	public static final int FEEDBACK_TYPE_SERVICE_CUSTOMER = 12;
	// quan he khach hang
	public static final int FEEDBACK_TYPE_RELATIONSHIP_CUSTOMER = 13;
	// ky nang NVBH
	public static final int FEEDBACK_TYPE_SKILL_NVBH = 14;
	// ky nang GSNPP
	public static final int FEEDBACK_TYPE_SKILL_GSNPP = 15;
	// van de khac
	public static final int FEEDBACK_TYPE_OTHER_PROBLEM = 16;

	// id bang
	private long feedBackId;
	// id NVBH
	private int staffId;
	private ArrayList<Integer> arrStaffId = new ArrayList<Integer>();

	// dung de kiem tra chon shop_id de luu cho feedback (TBHV)
	private ArrayList<String> arrStaffShopId = new ArrayList<String>();
	private String customerShopIdForRequest;

	private ArrayList<String> arrFeedBackId = new ArrayList<String>();
	// id KH
	private String customerId;
	// id Shop
	private String shopId;
	// id training Plan Detail
	private String trainingPlanDetailId;
	// Ma KH
	private String customerCode;
	// Ten KH
	private String customerName;
	// trang thai
	private int status;
	// noi dung phan anh
	private String content;
	// nguoi tao
	private String userUpdate;
	// ap param name
	private String apParamName;
	// ngay tao
	private String createDate;
	// ngay cap nhat
	private String updateDate;
	// ngay thuc hien
	private String doneDate;
	// num return
	private String numReturn;
	// ngay nhac nho
	private String remindDate;
	// loai phan anh
	private int type;
	// loai van de do nvbh tao hay gsnpp tao
	private String type_ap;
	// da xoa
	private int isDeleted;
	// supper staff id
	private long createUserId;
	// so dien thoai nha
	private String houseNumber;
	// street
	private String street;
	// address
	private String address;
	private FeedbackStaffDTO feedbackStaffDTO = new FeedbackStaffDTO();
	private List<AttachDTO> lstAttach = null;
	public FeedBackDTO() {
		super(TableType.FEEDBACK_TABLE);
	}

	public void initDataFromCursor(Cursor c) {
		setFeedBackId(CursorUtil.getLong(c, "FEEDBACK_ID"));
		setStaffId(CursorUtil.getInt(c, "STAFF_ID"));
		setFeedbackType(CursorUtil.getInt(c, "TYPE"));
		setCreateUserId(CursorUtil.getLong(c, "CREATE_USER_ID"));
		setStatus(CursorUtil.getInt(c, "RESULT", 1, 1));
		setContent(CursorUtil.getString(c, "CONTENT"));
		setApParamName(CursorUtil.getString(c, "AP_PARAM_NAME"));
		setRemindDate(CursorUtil.getString(c, "REMIND_DATE"));
		setDoneDate(CursorUtil.getString(c, "DONE_DATE"));
		setCreateDate(CursorUtil.getString(c, "CREATE_DATE"));
		setCustomerId(CursorUtil.getString(c, "CUSTOMER_ID"));
//		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
//		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		setHouseNumber(CursorUtil.getString(c, "HOUSE_NUMBER"));
		setStreet(CursorUtil.getString(c, "STREET"));
		setAddress(CursorUtil.getString(c, "ADDRESS"));
		getFeedbackStaffDTO().feedBackStaffId = CursorUtil.getLong(c, "FEEDBACK_STAFF_ID");
		getFeedbackStaffDTO().staffId = CursorUtil.getLong(c, "STAFF_ID");
		getFeedbackStaffDTO().doneDate = CursorUtil.getString(c, "DONE_DATE");
		getFeedbackStaffDTO().updateDate = CursorUtil.getString(c, "UPDATE_DATE");
		getFeedbackStaffDTO().updateUser = CursorUtil.getString(c, "USER_UPDATE");
		getFeedbackStaffDTO().result = CursorUtil.getInt(c, "RESULT");
		getFeedbackStaffDTO().createDate = CursorUtil.getString(c, "CREATE_DATE");
	}

	/**
	 * Phat sinh ra cau lenh sql chuyen len server
	 *
	 * @author: TamPQ
	 * @return: ArrayList<JSONArray>
	 * @throws:
	 */
	public JSONObject generateFeedbackSql(String remindDate) {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, getFeedBackId(), null));
//			if (!StringUtil.isNullOrEmpty(customerId)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CUSTOMER_ID, customerId, null));
//			}
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CONTENT, getContent(), null));
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, getStatus(), null));
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.TYPE, getFeedbackType(), null));
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CREATE_DATE, getCreateDate(), null));
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.SHOP_ID, shopId, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.IS_DELETED, getIsDeleted(), null));
			if (!StringUtil.isNullOrEmpty(remindDate)) {// nhac nho
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.REMIND_DATE, remindDate, null));
				// detailPara.put(GlobalUtil.getJsonColumn(
				// FEED_BACK_TABLE.DONE_DATE, DATA_VALUE.sysdate.toString(),
				// DATA_TYPE.SYSDATE.toString()));
			}
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CREATE_USER_ID, String.valueOf(getCreateUserId()), null));
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.SHOP_ID, shopId, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return feedbackJson;
	}

	/**
	 *
	 * generate sql insert to server
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateFeedbackSql() {
		JSONObject feedbackJson = new JSONObject();

		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, getFeedBackId(), null));
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STAFF_ID, staffId, null));
//			if (!StringUtil.isNullOrEmpty(customerId)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CUSTOMER_ID, customerId, null));
//			}
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, getStatus(), null));
			if (!StringUtil.isNullOrEmpty(getContent())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CONTENT, getContent(), null));
			}
//			if (!StringUtil.isNullOrEmpty(userUpdate)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_USER, userUpdate, null));
//			}
			if (!StringUtil.isNullOrEmpty(getCreateDate())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CREATE_DATE, getCreateDate(), null));
			}
//			if (!StringUtil.isNullOrEmpty(updateDate)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_DATE, updateDate, null));
//			}
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.TYPE, getFeedbackType(), null));
			if (!StringUtil.isNullOrEmpty(getRemindDate())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.REMIND_DATE, getRemindDate(), null));
			}
//			if (!StringUtil.isNullOrEmpty(doneDate)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.DONE_DATE, doneDate, null));
//			}
//			if (!StringUtil.isNullOrEmpty(shopId)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.SHOP_ID, shopId, null));
//			}
			detailPara
					.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CREATE_USER_ID, String.valueOf(getCreateUserId()), null));
//			if (!StringUtil.isNullOrEmpty(trainingPlanDetailId)) {
//				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.TRAINING_PLAN_DETAIL_ID,
//						String.valueOf(trainingPlanDetailId), null));
//			}

			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return feedbackJson;
	}

	public JSONObject generateUpdateFeedbackSql() {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			if (getFeedbackStaffDTO().updateUser != null) {
				detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.UPDATE_USER, getFeedbackStaffDTO().updateUser, null));
			}
			if (getFeedbackStaffDTO().updateDate != null) {
				detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.UPDATE_DATE, getFeedbackStaffDTO().updateDate, null));
			}
			if (getFeedbackStaffDTO().result != 0) {
				detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.RESULT, getFeedbackStaffDTO().result, null));
			}
			if (getFeedbackStaffDTO().updateUser != null) {
				detailPara.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.DONE_DATE, getFeedbackStaffDTO().doneDate, null));
			}
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_ID, getFeedbackStaffDTO().feedBackStaffId, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	/**
	 *
	 * generate sql update feedback status
	 *
	 * @param feedBackIdValue
	 * @param feedBackStatusValue
	 * @param doneDateValue
	 * @return
	 * @return: JSONObject
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public JSONObject generateUpdateFeedBackSql(String feedBackIdValue, String feedBackStatusValue, String doneDateValue) {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
//			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.DONE_DATE, doneDateValue, null));
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, feedBackStatusValue, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, feedBackIdValue, null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	/**
	 *
	 * generalte sql update content for feedback
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateContentFeedbackSql() {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			if (!StringUtil.isNullOrEmpty(getUpdateDate())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_DATE, getUpdateDate(), null));
			}
			if (!StringUtil.isNullOrEmpty(getUserUpdate())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_USER, getUserUpdate(), null));
			}
			if (!StringUtil.isNullOrEmpty(getRemindDate())) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.REMIND_DATE, getRemindDate(), null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.CONTENT, getContent(), null));

			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, this.getFeedBackId(), null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	/**
	 * general sql delete feedback out of db server Mo ta chuc nang cua ham
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generalSqlDeleteFeedBackOutOfDB() {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, String.valueOf(this.getFeedBackId()), null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	public JSONObject generateDeleteFeedbackSql(FeedBackDTO dto) {
		JSONObject feedbackJson = new JSONObject();
		try {
			feedbackJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			feedbackJson.put(IntentConstants.INTENT_TABLE_NAME, FEED_BACK_TABLE.FEED_BACK_TABLE);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.STATUS, getStatus(), null));
			if (getUpdateDate() != null) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_DATE, getUpdateDate(), null));
			}
			if (getUserUpdate() != null) {
				detailPara.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.UPDATE_USER, getUserUpdate(), null));
			}
			feedbackJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(FEED_BACK_TABLE.FEEDBACK_ID, this.getFeedBackId(), null));
			feedbackJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return feedbackJson;
	}

	/**
	 *
	 * init data with cursor
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDataWithCursor(Cursor c) {
		setFeedBackId(CursorUtil.getLong(c, FEED_BACK_TABLE.FEEDBACK_ID, -1, 0));
//		staffId = CursorUtil.getInt(c, FEED_BACK_TABLE.STAFF_ID, -1, 0);
//		customerId = CursorUtil.getString(c, FEED_BACK_TABLE.CUSTOMER_ID);
		setStatus(CursorUtil.getInt(c, FEED_BACK_TABLE.STATUS, -1, 0));
		setContent(CursorUtil.getString(c, FEED_BACK_TABLE.CONTENT));
//		shopId = CursorUtil.getString(c, FEED_BACK_TABLE.SHOP_ID);
		setUserUpdate(CursorUtil.getString(c, FEED_BACK_TABLE.UPDATE_USER));
		setCreateDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "CREATE_DATE")));
		setUpdateDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "UPDATE_DATE")));
		setDoneDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "DONE_DATE")));
		setRemindDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "REMIND_DATE")));
		setFeedbackType(CursorUtil.getInt(c, FEED_BACK_TABLE.TYPE, -1, 0));
		setCreateUserId(CursorUtil.getLong(c, FEED_BACK_TABLE.CREATE_USER_ID, -1, 0));
	}

	public String getTypeTitle(int type) {
		String title = "";
		if (type == FeedBackDTO.FEEDBACK_TYPE_SERVICE) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_SERVICE);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_PRODUCT) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_PRODUCT);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_PROGRAME_HTTM) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_PROGRAM);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_DISPLAY) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_DISPLAY);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_SHELF) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_SHELF);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_CTTB) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_CTTB);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_SUPPORT_NVBH) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_SUPPORT_STAFF);
		} else if (type == FeedBackDTO.FEEDBACK_TYPE_AMOUNT) {
			title = StringUtil.getString(R.string.TEXT_FEEDBACK_TYPE_AMOUNT);
		}
		return title;
	}

	/**
	 *
	 * create object with params
	 *
	 * @author: HaiTC3
	 * @param feedabackId
	 * @param staffId
	 * @param customerId
	 * @param status
	 * @param desc
	 * @param userUpdate
	 * @param createDate
	 * @param updateDate
	 * @param type
	 * @param parentStaffId
	 * @return: void
	 * @throws:
	 */
	public void initDateForTrainingResult(long feedabackId, int staffId, String customerId, int status, String desc,
			String userUpdate, String createDate, String updateDate, int type, long parentStaffId, String remainDate,
			String shopId, String trainpdId) {
		this.setFeedBackId(feedabackId);
		this.setStaffId(staffId);
		this.setCustomerId(customerId);
		this.setStatus(status);
		this.setContent(desc);
//		this.userUpdate = userUpdate;
		this.setCreateDate(createDate);
//		this.updateDate = updateDate;
		this.setFeedbackType(type);
		this.setTrainingPlanDetailId(trainpdId);
		// not have is_send and remind dayInOrder, done_date, is_delete
		this.setCreateUserId(parentStaffId);
		this.setRemindDate(remainDate);
		this.setShopId(shopId);
	}

	public long getFeedBackId() {
		return feedBackId;
	}

	public void setFeedBackId(long feedBackId) {
		this.feedBackId = feedBackId;
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public ArrayList<Integer> getArrStaffId() {
		return arrStaffId;
	}

	public void setArrStaffId(ArrayList<Integer> arrStaffId) {
		this.arrStaffId = arrStaffId;
	}

	public ArrayList<String> getArrStaffShopId() {
		return arrStaffShopId;
	}

	public void setArrStaffShopId(ArrayList<String> arrStaffShopId) {
		this.arrStaffShopId = arrStaffShopId;
	}

	public String getCustomerShopIdForRequest() {
		return customerShopIdForRequest;
	}

	public void setCustomerShopIdForRequest(String customerShopIdForRequest) {
		this.customerShopIdForRequest = customerShopIdForRequest;
	}

	public ArrayList<String> getArrFeedBackId() {
		return arrFeedBackId;
	}

	public void setArrFeedBackId(ArrayList<String> arrFeedBackId) {
		this.arrFeedBackId = arrFeedBackId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getTrainingPlanDetailId() {
		return trainingPlanDetailId;
	}

	public void setTrainingPlanDetailId(String trainingPlanDetailId) {
		this.trainingPlanDetailId = trainingPlanDetailId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserUpdate() {
		return userUpdate;
	}

	public void setUserUpdate(String userUpdate) {
		this.userUpdate = userUpdate;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getDoneDate() {
		return doneDate;
	}

	public void setDoneDate(String doneDate) {
		this.doneDate = doneDate;
	}

	public String getNumReturn() {
		return numReturn;
	}

	public void setNumReturn(String numReturn) {
		this.numReturn = numReturn;
	}

	public String getRemindDate() {
		return remindDate;
	}

	public void setRemindDate(String remindDate) {
		this.remindDate = remindDate;
	}

	public int getFeedbackType() {
		return type;
	}

	public void setFeedbackType(int type) {
		this.type = type;
	}

	public String getType_ap() {
		return type_ap;
	}

	public void setType_ap(String type_ap) {
		this.type_ap = type_ap;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(long createUserId) {
		this.createUserId = createUserId;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public FeedbackStaffDTO getFeedbackStaffDTO() {
		return feedbackStaffDTO;
	}

	public void setFeedbackStaffDTO(FeedbackStaffDTO feedbackStaffDTO) {
		this.feedbackStaffDTO = feedbackStaffDTO;
	}

	public List<AttachDTO> getLstAttach() {
		return lstAttach;
	}

	public void setLstAttach(List<AttachDTO> lstAttach) {
		this.lstAttach = lstAttach;
	}
}
