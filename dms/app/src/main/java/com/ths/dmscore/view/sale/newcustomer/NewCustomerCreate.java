/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import java.util.ArrayList;
import java.util.List;

import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.AreaItem;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.CustomerType;
import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.dto.view.ListCustomerAttributeViewDTO;
import com.ths.dmscore.dto.view.NewCustomerItem;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Man hinh tao moi khach hang
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 9:30:41 AM Jan 26, 2015
 */
public class NewCustomerCreate extends BaseFragment implements
		OnItemSelectedListener, OnTouchListener, OnDateSetListener {

	private final int ACTION_ORDER_LIST = 1;
	private final int ACTION_PATH = 2;
	private final int ACTION_CUS_LIST = 3;
	private final int ACTION_CREATE_CUSTOMER = 9;

	public static final int REQUEST_EDIT_CUSTOMER = -1;
	public static final int REQUEST_CREATE_CUSTOMER = -2;

	private static final int ACTION_INSERT_CUSTOMER = -1;

	private static final int ACTION_UPDATE_CUSTOMER = -2;

	private static final int ACTION_CANCEL_CUSTOMER = -3;

	private static final int REQUEST_VIEW_CUSTOMER = -4;
	private final int ACTION_CLOSE_MAP_DIALOG = 4;

	private final int ACTION_SET_DATE_TIME = 5;
	private final int ACTION_SHOW_MAP = 6;

	// ten khach hang
	private TextView tvCustomerName;
	// input ten khach hang
	private VNMEditTextClearable edCustomerName;
	// textView Loai khach hang
	private TextView tvType;
	// spinner loai khach hang
	private Spinner spinnerType;
	// textView so dien thoai co dinh
	//private TextView tvPhone;
	// input so dien thoai co dinh
	private VNMEditTextClearable edPhone;
	// input email
	private VNMEditTextClearable edEmail;
	private VNMEditTextClearable edFax;
	// textView so dien thoai di dong
	//private TextView tvMobilePhone;
	// input so dien thoai di dong
	private VNMEditTextClearable edMobilePhone;
	// textview nguoi lien he
	//private TextView tvContactName;
	// input nguoi lien he
	private VNMEditTextClearable edContactName;
	// textview Tinh/ thanh pho
	private TextView tvProvine;
	// spinner chon tinh/thanh pho
	private Spinner spinnerProvine;
	// TextView quan/huyen
	private TextView tvDistrict;
	// spinner chon quan/huyen
	private Spinner spinnerDistrict;
	// TextView phuong xa
	private TextView tvPrecinct;
	// spinner chon phuong/xa
	private Spinner spinnerPrecinct;
	// input so nha
	private VNMEditTextClearable edHouseNumber;
	// input duong
	private VNMEditTextClearable edStreet;
	private VNMEditTextClearable edBirthDate;
	// textview thong bao nhap sai
	private TextView tvError;

	// for map fragment use
	public Button btSave;
	public Button btCancel;
	private boolean isEditMode;
	//private TextView tvEmail;
	//private TextView tvFax;
	//private TextView tvHouseNumber;
	//private TextView tvStreet;
	private TextView tvMap;

	private CreateCustomerInfoDTO dto;
	// user info
	// user id
	private String userId;
	// shopID
	private String shopId;
	// customer id
	private String customerId;
	// user name
	private String userName;
	private int typeRequest;
	private boolean isOldCustomer;

	private MapViewEventDialog mapDialog;

	// vi tri doi thu
	private ArrayList<AbstractExtraInfoView> listExtra;
	private TableLayout tableExtra;
	private ListCustomerAttributeViewDTO customerInfo;
	private long fromControl;
	private long mapControl;
	private double lat;
	private double lng;
	private String fromView;
	private Spinner spinnerTypePosition;
	private TextView tvMapName;
	private Object fromViewObject;

	public static NewCustomerCreate getInstance(Bundle data) {
		NewCustomerCreate f = new NewCustomerCreate();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getArguments();
		if (b != null) {
			customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID, "");
			fromView = b.getString(IntentConstants.INTENT_IS_VIEW_ALL, ListCustomerCreatedView.class.getName());
			Class<?> clazz;
			try {
				clazz = Class.forName(fromView);
				fromViewObject = clazz.newInstance();
			} catch (ClassNotFoundException e) {
				MyLog.e("NewCustomerCreate", "ClassNotFoundException", e);
			} catch (java.lang.InstantiationException e) {
				MyLog.e("NewCustomerCreate", "InstantiationException", e);
			} catch (IllegalAccessException e) {
				MyLog.e("NewCustomerCreate", "IllegalAccessException", e);
			}
		}
		if (!StringUtil.isNullOrEmpty(customerId)) {
			if (b != null) {
				isEditMode = b.getBoolean(
						IntentConstants.INTENT_IS_EDIT, false);
				isOldCustomer = b.getBoolean(IntentConstants.INTENT_OLD_CUSTOMER, false);
			} else {
				isEditMode = false;
				isOldCustomer = false;
			}
			
			
			if (isEditMode || isOldCustomer) {
				typeRequest = REQUEST_EDIT_CUSTOMER;
			} else {
				typeRequest = REQUEST_VIEW_CUSTOMER;
			}
			
		} else {
			isEditMode = true;
			typeRequest = REQUEST_CREATE_CUSTOMER;
		}
		mapDialog = new MapViewEventDialog(parent, this, StringUtil.getString(R.string.TEXT_CUSTOMER_LOCATION), ACTION_CLOSE_MAP_DIALOG);
		userId = String.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId());
		userName = GlobalInfo.getInstance().getProfile().getUserData().getUserName();
		shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		listExtra = new ArrayList<AbstractExtraInfoView>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		PriUtils.getInstance().genPriHashMapForForm(PriForm.NVBH_THEMMOIKH_DANHSACH);
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_new_customer_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		initView(v);
		if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
			initHeaderMenu();
		}
		initHeaderText();
		
		if (isEditMode) {
			parent.reStartLocatingWithWaiting();
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (dto == null) {
//			if (!StringUtil.isNullOrEmpty(customerId)) {
				getCustomerInfo();
//			}
			getExtraCustomerInfo();
		} else if (dto != null) {

		}
	}

	/**
	 * khoi tao view
	 *
	 * @author: dungdq3
	 * @since: 4:08:07 PM Jan 27, 2015
	 * @return: void
	 * @throws:
	 * @param view
	 */
	private void initView(View view) {

		TableLayout table = (TableLayout) view.findViewById(R.id.table);
		tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);
		edCustomerName = (VNMEditTextClearable) view
				.findViewById(R.id.edCustomerName);
		tvType = (TextView) view.findViewById(R.id.tvType);
		spinnerType = (Spinner) view.findViewById(R.id.spinnerType);
		//tvPhone = (TextView) view.findViewById(R.id.tvPhone);
		edPhone = (VNMEditTextClearable) view.findViewById(R.id.edPhone);
		// edEmail = (VNMEditTextClearable) view.findViewById(R.id.edEmail);
		edEmail = (VNMEditTextClearable) PriUtils.getInstance().findViewById(
				view, R.id.edEmail, PriControl.NVBH_THEMMOIKHACHHANG_EMAIL,
				PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
//		tvEmail = (TextView) PriUtils.getInstance().findViewById(view,
//				R.id.tvEmail, PriControl.NVBH_THEMMOIKHACHHANG_EMAIL);

		edFax = (VNMEditTextClearable) PriUtils.getInstance().findViewById(
				view, R.id.edFax, PriControl.NVBH_THEMMOIKHACHHANG_FAX,
				PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
//		tvFax = (TextView) PriUtils.getInstance().findViewById(view,
//				R.id.tvFax, PriControl.NVBH_THEMMOIKHACHHANG_FAX);

//		tvMobilePhone = (TextView) view.findViewById(R.id.tvMobilePhone);
		edMobilePhone = (VNMEditTextClearable) view
				.findViewById(R.id.edMobilePhone);
//		tvContactName = (TextView) view.findViewById(R.id.tvContactName);
		edContactName = (VNMEditTextClearable) view
				.findViewById(R.id.edContactName);
		tvProvine = (TextView) view.findViewById(R.id.tvProvine);
		spinnerProvine = (Spinner) view.findViewById(R.id.spinnerProvine);
		tvDistrict = (TextView) view.findViewById(R.id.tvDistrict);
		spinnerDistrict = (Spinner) view.findViewById(R.id.spinnerDistrict);
		tvPrecinct = (TextView) view.findViewById(R.id.tvPrecinct);
		spinnerPrecinct = (Spinner) view.findViewById(R.id.spinnerPrecinct);
		spinnerTypePosition = (Spinner) view.findViewById(R.id.spinnerTypePosition);
		edBirthDate = (VNMEditTextClearable) view.findViewById(R.id.edBirthDate);
		edBirthDate.setOnTouchListener(this);
		edBirthDate.setIsHandleDefault(false);
		edBirthDate.setFocusable(false);
//		tvHouseNumber = (TextView) PriUtils.getInstance().findViewById(view,
//				R.id.tvHouseNumber, PriControl.NVBH_THEMMOIKHACHHANG_SONHA);
		edHouseNumber = (VNMEditTextClearable) PriUtils.getInstance()
				.findViewById(view, R.id.edHouseNumber,
						PriControl.NVBH_THEMMOIKHACHHANG_SONHA,
						PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
//		tvStreet = (TextView) PriUtils.getInstance().findViewById(view,
//				R.id.tvStreet, PriControl.NVBH_THEMMOIKHACHHANG_TENDUONG);
		tvMapName = (TextView) view.findViewById(R.id.tvMapName);
		tvMap = (TextView) PriUtils.getInstance().findViewById(view,
				R.id.tvMap, PriControl.NVBH_THEMMOIKHACHHANG_BANDO_KH);
		edStreet = (VNMEditTextClearable) PriUtils.getInstance().findViewById(
				view, R.id.edStreet, PriControl.NVBH_THEMMOIKHACHHANG_TENDUONG,
				PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		btSave = (Button) view.findViewById(R.id.btSave);
		btCancel = (Button) view.findViewById(R.id.btCancel);
		tvError = (TextView) view.findViewById(R.id.tvError);

		tableExtra = (TableLayout) view.findViewById(R.id.tableExtra);
		tableExtra.removeAllViews();

		// set event for spinners
		spinnerDistrict.setOnItemSelectedListener(this);
		spinnerPrecinct.setOnItemSelectedListener(this);
		spinnerProvine.setOnItemSelectedListener(this);
		spinnerType.setOnItemSelectedListener(this);
		spinnerTypePosition.setOnItemSelectedListener(this);

		// array of view
		if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
			TextView[] arrTextView = new TextView[] { tvCustomerName, tvType,
					tvProvine, tvDistrict, tvPrecinct, tvMapName};
			setRequireSymbolTextViews(arrTextView);
		}


		// hide keyboard
		table.setOnTouchListener(this);
		btSave.setOnClickListener(this);
		btCancel.setOnClickListener(this);
		tvMap.setOnClickListener(this);

		// khong ko cho chinh sua neu la view mode
		if (!isEditMode) {
			// an nut luu
			if(!isOldCustomer) {
				btSave.setVisibility(View.GONE);
			}
			// disiable toan bo view
			// spinner
			spinnerType.setEnabled(isEditMode);
			spinnerProvine.setEnabled(isEditMode || isOldCustomer);
			spinnerDistrict.setEnabled(isEditMode || isOldCustomer);
			spinnerPrecinct.setEnabled(isEditMode || isOldCustomer);
			spinnerTypePosition.setEnabled(isEditMode);

			// edit text
			edCustomerName.setEnabled(isEditMode);
			edCustomerName.setImageClearVisibile(isEditMode);
			
			edPhone.setEnabled(isEditMode || isOldCustomer);
			edPhone.setImageClearVisibile(isEditMode || isOldCustomer);
			
			edEmail.setEnabled(isEditMode);
			edEmail.setImageClearVisibile(isEditMode);
			
			edMobilePhone.setEnabled(isEditMode || isOldCustomer);
			edMobilePhone.setImageClearVisibile(isEditMode || isOldCustomer);
			
			edContactName.setEnabled(isEditMode || isOldCustomer);
			edContactName.setImageClearVisibile(isEditMode || isOldCustomer);
			
			edBirthDate.setEnabled(isEditMode);
			edBirthDate.setImageClearVisibile(isEditMode);
			
			edHouseNumber.setEnabled(isEditMode || isOldCustomer);
			edHouseNumber.setImageClearVisibile(isEditMode || isOldCustomer);
			
			edStreet.setEnabled(isEditMode || isOldCustomer);
			edStreet.setImageClearVisibile(isEditMode || isOldCustomer);
			
			edFax.setEnabled(isEditMode);
			edFax.setImageClearVisibile(isEditMode);
		}
	}

	/**
	 * show header text
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initHeaderText() {
		// TODO Auto-generated method stub
		if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
			String title = StringUtil.getString(R.string.TITLE_VIEW_CREATE_CUSTOMER);
			parent.setTitleName(title);
//			setTitleHeaderView(title);
		} else if (!(fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView)) {
			String title = StringUtil.getString(R.string.TITLE_VIEW_CUSTOMER_INFO);
			parent.setTitleName(title);
//			setTitleHeaderView(title);
		}
	}

	/**
	 * get data on db
	 *
	 * @author: duongdt3
	 * @since: 09:07:03 6 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void getCustomerInfo() {
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID, userId);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEvent(data, ActionEventConstant.GET_CREATE_CUSTOMER_INFO,
				SaleController.getInstance());
	}

	/**
	 * init header menu
	 *
	 * @author: duongdt3
	 * @since: 08:56:46 03/01/2014
	 * @update: 08:56:46 03/01/2014
	 * @return: void
	 */
	private void initHeaderMenu() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(PriForm.NVBH_THEMMOIKH_DANHSACH, new MenuTab(
				R.string.TEXT_MENU_ADD_CUSTOMER_NEW,
				R.drawable.icon_add_new, ACTION_CREATE_CUSTOMER,
				PriForm.NVBH_THEMMOIKH_DANHSACH), new MenuTab(
				R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
				ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
				new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
						ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
				new MenuTab(R.string.TEXT_MENU_CUS_LIST,
						R.drawable.menu_customer_icon, ACTION_CUS_LIST,
						PriForm.NVBH_BANHANG_DSKH));
	}

	/**
	 * set dau * cho cac TextView thong tin bat buot
	 *
	 * @author: duongdt3
	 * @since: 14:33:32 6 Jan 2014
	 * @return: void
	 * @throws:
	 * @param arrTextView
	 */
	private void setRequireSymbolTextViews(TextView[] arrTextView) {
		for (TextView tv : arrTextView) {
			String text = tv.getText().toString();
			int color = ImageUtil.getColor(R.color.RED);
			// them *: vao
			StringBuilder sb = new StringBuilder(text);
			sb.append(Constants.STR_SPACE);
			sb.append(StringUtil.getColorText("*", color));
//			text += " " + StringUtil.getColorText("*", color);

			tv.setText(StringUtil.getHTMLText(sb.toString()));
		}
	}

	/**
	 * lay danh sach du lieu dia ban voi id dia ban cha + loai dia ban
	 *
	 * @author: duongdt3
	 * @since: 15:55:46 6 Jan 2014
	 * @return: void
	 * @throws:
	 * @param type
	 * @param parentAreaId
	 */
	protected void requestDataArea(int type, long parentAreaId) {
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putInt(IntentConstants.INTENT_AREA_TYPE, type);
		data.putLong(IntentConstants.INTENT_PARRENT_AREA_ID, parentAreaId);
		handleViewEvent(data,
				ActionEventConstant.GET_CREATE_CUSTOMER_AREA_INFO,
				SaleController.getInstance());
	}

	@Override
	public void onItemSelected(AdapterView<?> adap, View v, int pos, long arg3) {
		// TODO Auto-generated method stub
		if (adap == spinnerDistrict) {
			if (this.dto.currentIndexDistrict != pos) {
				this.dto.setCurrentDistrict(pos);
				// request lai danh sach xa
				 requestDataArea(CUSTOMER_TABLE.AREA_TYPE_PRECINCT,
				 this.dto.curentIdDistrict);
			}
		} else if (adap == spinnerPrecinct) {
			if (this.dto.currentIndexPrecinct != pos) {
				this.dto.setCurrentPrecinct(pos);
			}
		}  else if (adap == spinnerTypePosition) {
			if (this.dto.currentIndexTypePosition != pos) {
				this.dto.setCurrentTypePosition(pos);
			}
		} else if (adap == spinnerProvine) {
			if (this.dto.currentIndexProvince != pos) {
				this.dto.setCurrentProvince(pos);
				// request lai danh sach huyen
				 requestDataArea(CUSTOMER_TABLE.AREA_TYPE_DISTRICT,
				 this.dto.curentIdProvine);
			}
		} else if (adap == spinnerType) {
			if (this.dto.currentIndexType != pos) {
				// vi co the bi thay doi C2 hay khong
				if (pos >= 0 && pos < listCusTypeCurrent.size()) {
					int typeId = listCusTypeCurrent.get(pos).typeId;
					this.dto.setCurrentType(pos, typeId);
					dto.cusInfo.setCustomerTypeId(typeId);
				}
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> adap) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		GlobalUtil.forceHideKeyboardInput(parent, v);
		if (v == edBirthDate) {
			if (!edBirthDate.onTouchEvent(arg1)) {
				parent.showDatePickerDialog(edBirthDate.getText().toString(), false, eventSetBirthDate);
			}
		}
		return true;
	}

	/**
	 * Set thong tin KH
	 *
	 * @author: duongdt3
	 * @since: 16:37:29 4 Jan 2014
	 * @return: void
	 * @throws:
	 * @param dto
	 */
	public void setCustomerInfo(CreateCustomerInfoDTO dto) {
		this.dto = dto;

		if (dto != null) {
			// neu co thong tin KH
			if (dto.cusInfo != null) {
				// ten
				edCustomerName.setText(dto.cusInfo.getCustomerName());
				// dien thoai ban
				edPhone.setText(dto.cusInfo.getPhone());
				// email
				edEmail.setText(dto.cusInfo.email);
				// fax
				// dien thoai di dong
				edMobilePhone.setText(dto.cusInfo.getMobilephone());
				// nguoi lien he
				edContactName.setText(dto.cusInfo.getContactPerson());
				// so nha
				edHouseNumber.setText(dto.cusInfo.getHouseNumber());
				//birth dayInOrder
				if (StringUtil.isNullOrEmpty(dto.cusInfo.birthDate)) {
					edBirthDate.setText("");
				} else{
					edBirthDate.setText(dto.cusInfo.birthDate);
				}
				// duong
				edStreet.setText(dto.cusInfo.getStreet());
				edFax.setText(dto.cusInfo.fax);
				lat = dto.cusInfo.lat;
				lng = dto.cusInfo.lng;
				if(mapDialog != null){
					mapDialog.showLocationFlag(dto.cusInfo.lat, dto.cusInfo.lng, !isEditMode);
				}
			}

			boolean isHaveC2Type = true;

			// Danh sach loai khach hang
			int indexC2Remove = renderListCusType(isHaveC2Type);
			if (indexC2Remove >= 0) {
				// neu vi tri hien tai nam duoi C2, ma c2 da bi remove => tru
				// index di 1
				if (dto.currentIndexType > indexC2Remove) {
					dto.currentIndexType--;
				}
			}
			spinnerType.setSelection(dto.currentIndexType);

			// Danh sach tinh
			renderListProvince();
			spinnerProvine.setSelection(dto.currentIndexProvince);

			// Danh sach huyen
			renderListDicstrict();
			spinnerDistrict.setSelection(dto.currentIndexDistrict);

			// Danh sach xa
			renderListPrecinct();
			spinnerPrecinct.setSelection(dto.currentIndexPrecinct);
			
			// Danh sach loai vi tri
			renderListTypePosition();
			spinnerTypePosition.setSelection(dto.currentIndexTypePosition);
		}
	}

	List<CustomerType> listCusTypeCurrent = new ArrayList<CustomerType>();
	boolean isHaveC2Current = true;

	/**
	 * hien thi danh sach loai khach hang
	 *
	 * @author: duongdt38
	 * @since: 09:00:58 8 Jan 2014
	 * @return: int
	 * @throws:
	 * @param isHaveC2
	 * @return
	 */
	int renderListCusType(boolean isHaveC2) {
		int indexC2 = -1;
		// Danh sach loai khach hang
		if (dto.listCusType != null) {
			int sizeCusType = dto.listCusType.size();
			listCusTypeCurrent.clear();
			for (int i = 0; i < sizeCusType; i++) {
				CustomerType item = dto.listCusType.get(i);
				// neu ko render c2 => object_type = 4
				if (isHaveC2 || (!isHaveC2 && item.objectType != 4)) {
					listCusTypeCurrent.add(item);
				} else {
					indexC2 = i;
				}
			}

			int sizeCurrent = listCusTypeCurrent.size();
			if (sizeCurrent > 0) {
				String[] arrCusType = new String[sizeCurrent];
				for (int i = 0; i < sizeCurrent; i++) {
					arrCusType[i] = listCusTypeCurrent.get(i).typeName;
				}
//				SpinnerAdapter adapterCusType = new SpinnerAdapter(parent,
//						R.layout.simple_spinner_item, arrCusType);
                ArrayAdapter adapterCusType = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrCusType);
                adapterCusType.setDropDownViewResource(R.layout.simple_spinner_dropdown);
				spinnerType.setAdapter(adapterCusType);
			}
		}

		isHaveC2Current = isHaveC2;
		return indexC2;
	}

	/**
	 * Hien thi danh sach huyen
	 *
	 * @author: duongdt3
	 * @since: 09:01:10 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListDicstrict() {
		// Danh sach huyen
		if (dto.listDistrict != null) {
			int sizeDistrict = dto.listDistrict.size();
			String[] arrDistrict = new String[sizeDistrict];

			for (int i = 0; i < sizeDistrict; i++) {
				AreaItem item = dto.listDistrict.get(i);
				arrDistrict[i] = item.areaName;
			}
//			SpinnerAdapter adapterDistrict = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, arrDistrict);
            ArrayAdapter adapterDistrict = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrDistrict);
            adapterDistrict.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spinnerDistrict.setAdapter(adapterDistrict);
		}
	}

	/**
	 * Hien thi danh sach tinh
	 *
	 * @author: duongdt3
	 * @since: 09:01:25 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListProvince() {
		// Danh sach tinh
		if (dto.listProvine != null) {
			int sizeProvine = dto.listProvine.size();
			String[] arrProvine = new String[sizeProvine];

			for (int i = 0; i < sizeProvine; i++) {
				AreaItem item = dto.listProvine.get(i);
				arrProvine[i] = item.areaName;
			}
//			SpinnerAdapter adapterProvine = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, arrProvine);
			ArrayAdapter adapterProvine = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrProvine);
			adapterProvine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spinnerProvine.setAdapter(adapterProvine);

		}
	}

	/**
	 * Hien thi danh sach xa
	 *
	 * @author: duongdt3
	 * @since: 09:01:35 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListPrecinct() {
		// Danh sach xa
		if (dto.listPrecinct != null) {
			int sizePrecinct = dto.listPrecinct.size();
			String[] arrPrecinct = new String[sizePrecinct];

			for (int i = 0; i < sizePrecinct; i++) {
				AreaItem item = dto.listPrecinct.get(i);
				arrPrecinct[i] = item.areaName;
			}
//			SpinnerAdapter adapterPrecinct = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, arrPrecinct);
            ArrayAdapter adapterPrecinct = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrPrecinct);
            adapterPrecinct.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spinnerPrecinct.setAdapter(adapterPrecinct);
		}
	}
	
	void renderListTypePosition() {
		// Danh sach xa
		if (dto.listTypePosition != null) {
			int size = dto.listTypePosition.size();
			String[] arr = new String[size];
			
			for (int i = 0; i < size; i++) {
				ApParamDTO item = dto.listTypePosition.get(i);
				arr[i] = item.getApParamName();
			}
//			SpinnerAdapter adapter = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, arr);
            ArrayAdapter adapter = new ArrayAdapter(parent, R.layout.simple_spinner_item, arr);
            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spinnerTypePosition.setAdapter(adapter);
		}
	}

	/**
	 * check cac input thong tin cho Khach hang
	 *
	 * @author: duongdt3
	 * @since: 08:34:36 7 Jan 2014
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	private boolean checkInputInfo() {
		boolean isVaild = true;
		String strError = "";
		// remove khoang trang
		edCustomerName.setText(edCustomerName.getText().toString().trim());
		edContactName.setText(edContactName.getText().toString().trim());
		edHouseNumber.setText(edHouseNumber.getText().toString().trim());
		edStreet.setText(edStreet.getText().toString().trim());

		// check
		if (edCustomerName.getText().toString().isEmpty()) {
			// kiem tra ten rong
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_CUSTOMER_NAME_NOT_NULL);
		} else if (!StringUtil.isCustomerNameContainValidChars(edCustomerName
				.getText().toString())) {
			// kiem tra ten co chua ky tu dac biet
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_CUSTOMER_NAME_NOT_VALID);
//		} else if (edContactName.getText().toString().isEmpty()) {
//			// kiem tra ten lien he rong
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_CONTACT_NAME_NOT_NULL);
//		} else if (edMobilePhone.getText().toString().isEmpty()) {
//			// kiem tra dien thoai di dong rong
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_MOBILE_NOT_NULL);
//		} else if (edPhone.getText().toString().isEmpty()) {
//			// kiem tra dien thoai ban rong
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_PHONE_NOT_NULL);
//		} else if (!StringUtil.isCustomerNameContainValidChars(edContactName
//				.getText().toString())) {
//			// kiem tra ten lien he co chua ky tu dac biet
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_CONTACT_NAME_NOT_VALID);
//		} else if (!StringUtil
//				.isNullOrEmpty(edHouseNumber.getText().toString())
//				&& !StringUtil.isHouseNumberContainValidChars(edHouseNumber
//						.getText().toString())) {
//			// kiem tra so nha co gia tri + chua ky tu dac biet
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_HOUSE_NUMBER_NOT_VALID);
//		} else if (!StringUtil.isNullOrEmpty(edStreet.getText().toString())
//				&& !StringUtil.isStreetContainValidChars(edStreet.getText()
//						.toString())) {
//			// kiem tra duong co gia tri + chua ky tu dac biet
//			isVaild = false;
//			strError = StringUtil
//					.getString(R.string.TEXT_NOTIFY_HOUSE_STREET_NOT_VALID);
		} else if (dto.cusInfo.lat <= 0 || dto.cusInfo.lng <= 0) {
			// kiem tra khong co vi tri KH
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_CUSTOMER_LOCATION_NOT_NULL);
		} else if (dto.currentIndexType < 0) {
			// kiem tra khong co loai khach hang
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_CUSTOMER_TYPE_NOT_NULL);
		} else if (dto.currentIndexPrecinct < 0) {
			// kiem tra khong co dia chi
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_PRECINCT_NOT_NULL);
		} else if (dto.currentIndexDistrict < 0) {
			// kiem tra khong co dia chi
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_DISTRICT_NOT_NULL);
		} else if (dto.currentIndexProvince < 0) {
			// kiem tra khong co dia chi
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_PROVINCE_NOT_NULL);
		} else if (!StringUtil.isNullOrEmpty(edBirthDate.getText().toString()) && DateUtils.isCompareWithToDate(edBirthDate.getText().toString()) > 0) {
			// kiem tra ngay sinh lon hon hien tai
			isVaild = false;
			strError = StringUtil
					.getString(R.string.TEXT_NOTIFY_CUSTOMER_BIRTHDATE_NOT_GREATER_TODAY);
		} else {
			for (int i = 0, size = listExtra.size(); i < size; i++) {
				if (customerInfo.listAttribute.get(i).mandatory == 1) {
					AbstractExtraInfoView v = listExtra.get(i);
					if (!v.isCheckData()) {
						isVaild = false;
						v.requestFocusView();
						strError = StringUtil
								.getStringAndReplace(
										R.string.TEXT_REQUIRED_INPUT,
										v.getName());
						break;
					}
				}

			}
		}
		tvError.setText(strError);
		return isVaild;
	}

	/**
	 * set danh sach dia ban theo loai render vao spinner
	 *
	 * @author: duongdt3
	 * @since: 17:14:16 6 Jan 2014
	 * @return: void
	 * @throws:
	 * @param type
	 * @param arrayArea
	 */
	public void setArrayAreaInfo(int type, ArrayList<AreaItem> arrayArea) {
		switch (type) {
		case CUSTOMER_TABLE.AREA_TYPE_PRECINCT:
			// list xa
			dto.listPrecinct = arrayArea;
			dto.setCurrentPrecinct(-1);
			renderListPrecinct();
			break;
		case CUSTOMER_TABLE.AREA_TYPE_DISTRICT:
			// list huyen
			dto.listDistrict = arrayArea;
			dto.setCurrentDistrict(-1);
			renderListDicstrict();
			break;
		default:
			break;
		}
	}

	/**
	 * Cap nhat thong tin khach hang
	 *
	 * @author: duongdt3
	 * @since: 17:17:34 4 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void sendRequestInsertCustomerToDB() {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_CUSTOMER, dto.cusInfo);
		handleViewEvent(data, ActionEventConstant.ACTION_CREATE_CUSTOMER,
				SaleController.getInstance());
	}

	/**
	 * cap nhat thong tin khach hang
	 *
	 * @author: duongdt3
	 * @since: 14:37:10 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void sendRequestUpdateCustomerToDB() {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putSerializable(IntentConstants.INTENT_CUSTOMER, dto.cusInfo);
		handleViewEvent(data, ActionEventConstant.ACTION_UPDATE_CUSTOMER,
				SaleController.getInstance());
	}

	/**
	 * Hoi khi tao khach hang
	 *
	 * @author: duongdt3
	 * @param info
	 * @since: 14:41:34 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmCreateCustomer() {
		GlobalUtil.showDialogConfirm(this, parent, StringUtil
				.getString(R.string.TEXT_NOTIFY_ACTION_CREATE_CUSTOMER),
				StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_INSERT_CUSTOMER, StringUtil
						.getString(R.string.TEXT_BUTTON_DENY),
				ACTION_CANCEL_CUSTOMER, null);
	}

	/**
	 * Hoi khi cap nhat khach hang
	 *
	 * @author: duongdt3
	 * @param info
	 * @since: 14:41:34 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	private void confirmUpdateCustomer() {
		GlobalUtil.showDialogConfirm(this, parent, StringUtil
				.getString(R.string.TEXT_NOTIFY_ACTION_UPDATE_CUSTOMER),
				StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_UPDATE_CUSTOMER, StringUtil
						.getString(R.string.TEXT_BUTTON_DENY),
				ACTION_CANCEL_CUSTOMER, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_CREATE_CUSTOMER_INFO: {
			dto = (CreateCustomerInfoDTO) modelEvent.getModelData();
			if (dto != null) {
				if (typeRequest == REQUEST_CREATE_CUSTOMER) {
					dto.cusInfo = new CustomerDTO();
				}

				setCustomerInfo(dto);
			}
			break;
		}
		case ActionEventConstant.GET_CREATE_CUSTOMER_AREA_INFO: {
			ArrayList<AreaItem> arrayArea = (ArrayList<AreaItem>) modelEvent
					.getModelData();
			int type = ((Bundle) modelEvent.getActionEvent().viewData)
					.getInt(IntentConstants.INTENT_AREA_TYPE);
			setArrayAreaInfo(type, arrayArea);
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_CUSTOMER: {
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_MESSAGE_UPDATE_CUSTOMER_SUCCESS));
			ListCustomerCreatedView fragment = (ListCustomerCreatedView) findFragmentByTag(GlobalUtil
					.getTag(ListCustomerCreatedView.class));
			if (fragment != null) {
				fragment.receiveBroadcast(
						ActionEventConstant.BROADCAST_CREATE_OR_UPDATE_CUSTOMER,
						null);
			}
			if(isOldCustomer) {
				GlobalInfo.setChangeInfoCurrentCustomer(true);
			}
			GlobalUtil.popBackStack(parent);
			break;
		}
		case ActionEventConstant.ACTION_CREATE_CUSTOMER: {
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_MESSAGE_CREATE_CUSTOMER_SUCCESS));
			ListCustomerCreatedView fragment = (ListCustomerCreatedView) findFragmentByTag(GlobalUtil
					.getTag(ListCustomerCreatedView.class));
			if (fragment != null) {
				fragment.receiveBroadcast(
						ActionEventConstant.BROADCAST_CREATE_OR_UPDATE_CUSTOMER,
						null);
			}
			GlobalUtil.popBackStack(parent);
			break;
		}
		case ActionEventConstant.GET_EXTRA_CUSTOMER_INFO: {
			customerInfo = (ListCustomerAttributeViewDTO) modelEvent.getModelData();
			tableExtra.removeAllViews();
			TableRow row = null;
			int numItem = 4;
			int marginBig = (int) 5;
			int marginRightBig = (int) parent.getResources().getDimension(R.dimen.margin_right_big);
			for (int i = 0, size = customerInfo.listAttribute.size(); i < size; i += numItem) {
				
				LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				lp.setMargins(0, marginBig, 0, marginBig);
				row = new TableRow(parent);
				row.setLayoutParams(lp);
				
				for (int j = 0; j < numItem; j++) {
					TableRow.LayoutParams lpItem = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT);
					lpItem.weight = 1;
					//set margin right for left items
					if (j < numItem - 1) {
						lpItem.rightMargin = marginRightBig;
					}
					//on list item
					if (i + j < size) {
						CustomerAttributeViewDTO attr = customerInfo.listAttribute.get(i + j);
						if (attr.type == CustomerAttributeViewDTO.TEXT
								|| attr.type == CustomerAttributeViewDTO.NUMBER
								|| attr.type == CustomerAttributeViewDTO.DATE) {
							TextExtraInfoView text = new TextExtraInfoView(parent,
									this, ACTION_SET_DATE_TIME);
							text.renderLayout(attr, isEditMode, fromView);
							text.setLayoutParams(lpItem);
							listExtra.add(text);
							row.addView(text);
						} else if (attr.type == CustomerAttributeViewDTO.SELECT) {
							SpinnerSingleChoiceExtraInfoView spinnerSingle = new SpinnerSingleChoiceExtraInfoView(
									parent);
							spinnerSingle.renderLayout(attr, isEditMode, fromView);
							listExtra.add(spinnerSingle);
							spinnerSingle.setLayoutParams(lpItem);
							row.addView(spinnerSingle);
						} else if (attr.type == CustomerAttributeViewDTO.MULTISELECT) {
							SpinnerMultiChoiceExtraInfoView spinnerMulti = new SpinnerMultiChoiceExtraInfoView(
									parent);
							spinnerMulti.renderLayout(attr, isEditMode, fromView);
							spinnerMulti.setLayoutParams(lpItem);
							listExtra.add(spinnerMulti);
							row.addView(spinnerMulti);
						} else if (attr.type == CustomerAttributeViewDTO.LOCATION) {
							MapExtraInfoView map = new MapExtraInfoView(
									parent, this, ACTION_SHOW_MAP);
							map.renderLayout(attr, isEditMode, fromView);
							map.setLayoutParams(lpItem);
							listExtra.add(map);
							row.addView(map);
						}
					} else{
						//out size list item, add empty TextView
						TextView text = new TextView(parent);
						text.setLayoutParams(lpItem);
						row.addView(text);
					}
				}
				tableExtra.addView(row);
			}
			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}

		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW: {
			if (this.isVisible() && !isEditMode) {
				getCustomerInfo();
				getExtraCustomerInfo();
			}
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			if (mapDialog != null) {
				mapDialog.updateNewGPSPosition();
			}
			break;
		default: {
			super.receiveBroadcast(action, extras);
			break;
		}
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_INSERT_CUSTOMER:
		case ACTION_UPDATE_CUSTOMER: {
			sendRequestCreateOrUpdateWhenVailInput();
		}
			break;
		case ACTION_PATH:
			handleSwitchFragment(null,
					ActionEventConstant.GO_TO_CUSTOMER_ROUTE,
					SaleController.getInstance());
			break;
		case ACTION_ORDER_LIST:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER,
					SaleController.getInstance());
			break;
		case ACTION_CUS_LIST:
			handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST,
					SaleController.getInstance());
			break;
		case ACTION_CLOSE_MAP_DIALOG:
			double[] cusLatlng = {-1 , -1};
			if (data != null) {
				cusLatlng = (double[]) data;
				for(AbstractExtraInfoView view : listExtra) {
					if(view.tag == mapControl){
						if(view instanceof MapExtraInfoView){
							MapExtraInfoView map = (MapExtraInfoView) view;
							double lat = cusLatlng[0];
							double lng = cusLatlng[1];
							StringBuilder sb = new StringBuilder();
							if (lat > 0 && lng > 0) {
								sb.append(String.valueOf(lat));
								sb.append(",");
								sb.append(String.valueOf(lng));
							}
							map.setLocation(sb.toString());
						}
					}
				}
				if(mapControl == -1) {
					lat = cusLatlng[0];
					lng = cusLatlng[1];
					dto.cusInfo.lat = cusLatlng[0];
					dto.cusInfo.lng = cusLatlng[1];
				}
			}
			break;
		case ACTION_SET_DATE_TIME:
			fromControl = Long.parseLong(data.toString());
			parent.showDatePickerDialog(data.toString(), true, this);
			break;
		case ACTION_SHOW_MAP:
			mapControl = Long.parseLong(data.toString());
			String location = String.valueOf(control.getTag());
			if (!StringUtil.isNullOrEmpty(location) && !"null".equals(location)) {
				try {
					String[] loca = location.split(",");
					double lat = Double.parseDouble(loca[0]);
					double lng = Double.parseDouble(loca[1]);
					mapDialog.show();
					mapDialog.showLocationFlag(lat, lng, !isEditMode);
				} catch (Exception e) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_ERROR_MAP_LOCATION));
				}
			} else {
				mapDialog.show();
				mapDialog.showLocationFlag(-1, -1, !isEditMode);
			}
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	/**
	 * Goi yeu cau them, cap nhat
	 *
	 * @author: duongdt3
	 * @since: 17:25:29 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void sendRequestCreateOrUpdateWhenVailInput() {
		// cap nhat vi tri
		String dateNow = DateUtils.now();
		getInfoFromView();
		if (typeRequest == REQUEST_CREATE_CUSTOMER) {
			// tao thi them nguoi tao, ngay tao, shop_id cua NHBH hien tai,
			// status = 2 => cho xu ly
			dto.cusInfo.setShopId(shopId);
			dto.cusInfo.staffId = Long.valueOf(userId);
			dto.cusInfo.setCreateDate(dateNow);
			dto.cusInfo.setCreateUser(userName);
			dto.cusInfo.setStatus(NewCustomerItem.STATE_CUSTOMER_WAIT_APPROVED);

			// send request to DB
			sendRequestInsertCustomerToDB();
		} else if (typeRequest == REQUEST_EDIT_CUSTOMER) {
			// sua thi them nguoi sua, ngay sua
			dto.cusInfo.setUpdateDate(dateNow);
			dto.cusInfo.setUpdateUser(userName);

			// send request to DB
			sendRequestUpdateCustomerToDB();
		}
	}

	@Override
	public void onClick(View v) {
		if(v == btSave) {
			updateCustomerToDB();
		} else if (v == btCancel) {
			GlobalUtil.popBackStack(parent);
		} else if (v == tvMap){
			// MO popup ban do
			mapControl = -1;
			mapDialog.show();
			mapDialog.showLocationFlag(lat, lng, !isEditMode);
		} else{
			super.onClick(v);
		}
	}

	/**
	 * Check & Update customer to DB
	 *
	 * @author: duongdt3
	 * @since: 10:10:08 7 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void updateCustomerToDB() {
		// check dieu kien
		boolean isVaild = checkInputInfo();
		if (isVaild) {
			// tam thoi ko can quan tam toi vi tri
			if (typeRequest == REQUEST_CREATE_CUSTOMER) {
				confirmCreateCustomer();
			} else if (typeRequest == REQUEST_EDIT_CUSTOMER) {
				confirmUpdateCustomer();
			}
		}
	}

	@Override
	public void onDestroyView() {
		try {
			 MapFragment fragment = (MapFragment) parent.getFragmentManager().findFragmentById(
                         R.id.ggMap);
			 if (fragment != null) {
				 getFragmentManager().beginTransaction().remove(fragment).commit();
			 }

		} catch (Exception e) {
			// TODO: handle exception
		}
		super.onDestroyView();
	}
	/**
	 * lay thong tin bo vao object
	 *
	 * @author: dungdq3
	 * @since: 8:19:47 AM Jan 27, 2015
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("unchecked")
	private void getInfoFromView() {
		//id
		dto.cusInfo.setCustomerTypeId((int)dto.curentIdType);
		dto.cusInfo.areaId = (int)dto.curentIdPrecinct;
		dto.cusInfo.typePostitionId = dto.curentIdTypePosition;
		//text
		dto.cusInfo.birthDate = edBirthDate.getText().toString();
		dto.cusInfo.setCustomerName(edCustomerName.getText().toString());
		dto.cusInfo.setMobilephone(edMobilePhone.getText().toString());
		dto.cusInfo.setPhone(edPhone.getText().toString());
		dto.cusInfo.email = edEmail.getText().toString();
		dto.cusInfo.fax = edFax.getText().toString();
		dto.cusInfo.setHouseNumber(edHouseNumber.getText().toString());
		dto.cusInfo.setStreet(edStreet.getText().toString());
		dto.cusInfo.setContactPerson(edContactName.getText().toString());
		dto.cusInfo.setAddress(spinnerPrecinct.getSelectedItem().toString()
				+ ", " + spinnerDistrict.getSelectedItem().toString() + ", "
				+ spinnerProvine.getSelectedItem().toString());
		dto.cusInfo.staffId = Long.parseLong(userId);
		for(AbstractExtraInfoView v : listExtra) {
			if(v instanceof SpinnerMultiChoiceExtraInfoView) {
				ArrayList<CustomerAttributeDetailViewDTO> listDetail = (ArrayList<CustomerAttributeDetailViewDTO>) v.getDataFromView();
				for(CustomerAttributeDetailViewDTO detail : listDetail){
					detail.status = 1;
					detail.createDate = DateUtils.now();
					detail.createUser = GlobalInfo.getInstance().getProfile()
							.getUserData().getUserCode();
					detail.updateDate = DateUtils.now();
					detail.updateUser = GlobalInfo.getInstance().getProfile()
							.getUserData().getUserCode();
					dto.cusInfo.listCusAttrDetail.add(detail);
				}
			} else {
				CustomerAttributeDetailViewDTO detail = (CustomerAttributeDetailViewDTO) v
						.getDataFromView();
				detail.status = 1;
				detail.createDate = DateUtils.now();
				detail.createUser = GlobalInfo.getInstance().getProfile()
						.getUserData().getUserCode();
				detail.updateDate = DateUtils.now();
				detail.updateUser = GlobalInfo.getInstance().getProfile()
						.getUserData().getUserCode();
				dto.cusInfo.listCusAttrDetail.add(detail);
			}
		}
	}

	/**
	 * lay thong tin dong cua khach hang
	 *
	 * @author: dungdq3
	 * @since: 4:08:31 PM Jan 27, 2015
	 * @return: void
	 * @throws:
	 */
	private void getExtraCustomerInfo() {
		// TODO Auto-generated method stub
		parent.showLoadingDialog();
		// create request
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_STAFF_ID, userId);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEvent(data, ActionEventConstant.GET_EXTRA_CUSTOMER_INFO,
				SaleController.getInstance());
	}

	/**
	 * set dayInOrder time
	 *
	 * @author: dungdq3
	 * @since: 7:49:25 PM Jan 28, 2015
	 * @return: void
	 * @throws:
	 * @param dayOfMonth
	 * @param monthOfYear
	 * @param year
	 */
	public void setDateTime(int dayOfMonth, int monthOfYear, int year) {
		// TODO Auto-generated method stub
		String sDay = String.valueOf(dayOfMonth);
		String sMonth = String.valueOf(monthOfYear + 1);
		if (dayOfMonth < 10) {
			sDay = "0" + sDay;
		}
		if (monthOfYear + 1 < 10) {
			sMonth = "0" + sMonth;
		}
		String strDate = new StringBuilder()
				// Month is 0 based so add 1
				.append(sDay).append("/").append(sMonth).append("/")
				.append(year).toString();
//		if (DateUtils.compareWithNow(strDate, DateUtils.DATE_FORMAT_DATE_VN) != -1) {
			for(AbstractExtraInfoView view : listExtra) {
				if(view.tag == fromControl){
					if(view instanceof TextExtraInfoView){
						TextExtraInfoView text = (TextExtraInfoView) view;
						text.setDate(strDate);
					}
				}
			}
//		} else {
//			GlobalUtil
//					.showDialogConfirm(
//							this,
//							activity,
//							StringUtil
//									.getString(R.string.TEXT_ERROR_DATE_LESS_THAN_OR_EQUAL_NOW),
//							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
//							-100, null, false);
//		}
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		setDateTime(dayOfMonth, monthOfYear, year);
	}
	
	private OnDateSetListener eventSetBirthDate = new OnDateSetListener() {
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			String sDay = String.valueOf(dayOfMonth);
			String sMonth = String.valueOf(monthOfYear + 1);
			if (dayOfMonth < 10) {
				sDay = "0" + sDay;
			}
			if (monthOfYear + 1 < 10) {
				sMonth = "0" + sMonth;
			}
			String strDate = new StringBuilder()
					// Month is 0 based so add 1
					.append(sDay).append("/").append(sMonth).append("/")
					.append(year).toString();
			edBirthDate.setText(strDate);
		}
	};
}
