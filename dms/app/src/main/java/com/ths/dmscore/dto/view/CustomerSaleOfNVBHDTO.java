/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.CursorUtil;

public class CustomerSaleOfNVBHDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public long customerId;
	public String customerCode;
	public String customerName;

	public double amountApprovedPre;
	public double amountApproved;
	public double amount;

	public long quantityApprovedPre;
	public long quantityApproved;
	public long quantity;

	public CustomerSaleOfNVBHDTO() {
		customerId = 0;
		customerCode = Constants.STR_BLANK;
		customerName = Constants.STR_BLANK;
		amountApprovedPre = 0;
		amountApproved = 0;
		amount = 0;
		quantityApprovedPre = 0;
		quantityApproved = 0;
		quantity = 0;
	}

	public void initDataWithCursor(Cursor c) {
		customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		
		amountApprovedPre = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_AMOUNT_APPROVED_PRE");
		amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_AMOUNT_APPROVED");
		amount = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_AMOUNT");

		quantityApprovedPre = CursorUtil.getLong(c, "MONTH_QUANTITY_APPROVED_PRE");
		quantityApproved = CursorUtil.getLong(c, "MONTH_QUANTITY_APPROVED");
		quantity = CursorUtil.getLong(c, "MONTH_QUANTITY");
	}
}
