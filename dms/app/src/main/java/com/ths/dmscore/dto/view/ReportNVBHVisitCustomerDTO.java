/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin bao cao cua NVBH ghe tham khach hang trong ngay
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class ReportNVBHVisitCustomerDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ghe tham khach hang dau tien trong tuyen
	public static final int CUSTOMER_IN_PLAN = 0;
	// ghe tham khach hang dau tien ngoai tuyen
	public static final int CUSTOMER_OUT_PLAN = 1;

	// id NVBH
	public String staffId;
	// ma NVBH
	public String staffCode;
	// ten NVBH
	public String staffName;
	// ma khach hang ghe tham dau tien trong ngay
	public String customerCodeStartVisit;
	// ten khach hang ghe tham dau tien trong ngay
	public String customerNameStartVisit;
	// thoi gian ghe tham khach hang dau tien trong ngay
	public String timeStartVisit;
	// so khach hang da ghe tham middle
	public int numCustomerVisitedMiddle;
	// so khach hang max can ghe tham middle
	public int maxNumCustomerVisitMiddle;
	// so khach hang da ghe tham end time
	public int numCustomerVisitedEnd;
	// so khach hang max can ghe tham end
	public int maxNumCustomerVisitEnd;
	// so khach hang da ghe tham hien tai
	public int numCustomerCurrentVisited;
	// so khach hang da ghe tham hien tai trong tuyen
	public int numCustomerCurrentVisitedInPlan;
	// tong so khach hang can phai ghe tham trong tuyen - trong ngay
	public int numTotalCustomerVisit;
	// hight light middle infor
	public boolean isHighLightMiddle = false;
	// hight light end info
	public boolean isHighLightEnd = false;
	// high light current info
	public boolean isHighLightCurrent = false;
	// high light NVBH info
	public boolean isHighLightNVBH = false;
	// vi tri hien tai cua NVBH
	public double latPosition;
	public double lngPosition;
	// number customer order start
	public int numCusOrderStart = 0;
	// number customer order middle
	public int numCusOrderMiddle = 0;
	// number customer order end fullDate
	public int numCusOrderEnd = 0;
	// number customer order current
	public int numCusOrderCurrent = 0;

	// ghe tham khach hang dau tien trong ngay (1: rong tuyen / 2: ngoai tuyen)
	public int isOr;

	public ReportNVBHVisitCustomerDTO() {
		staffId = "0";
		staffCode = Constants.STR_BLANK;
		staffName = Constants.STR_BLANK;
		customerCodeStartVisit = Constants.STR_BLANK;
		customerNameStartVisit = Constants.STR_BLANK;
		timeStartVisit = Constants.STR_BLANK;
		numCustomerVisitedMiddle = 0;
		maxNumCustomerVisitMiddle = 0;
		numCustomerVisitedEnd = 0;
		maxNumCustomerVisitEnd = 0;
		numCustomerCurrentVisited = 0;
		numCustomerCurrentVisitedInPlan = 0;
		numTotalCustomerVisit = 0;
	}

	/**
	 * 
	 * init object with cursor data
	 * 
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 21, 2013
	 */
	public void initObjectWithCursor(Cursor c) {
		staffId = CursorUtil.getString(c, STAFF_TABLE.STAFF_ID);
		staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		staffName = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		customerCodeStartVisit = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE);
		customerNameStartVisit = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
		numTotalCustomerVisit = CursorUtil.getInt(c, "TOTAL_CUSTOMER_VISIT");
		timeStartVisit = CursorUtil.getString(c, "START_TIME");
		isOr = CursorUtil.getInt(c, "IS_OR");
		numCustomerVisitedMiddle = CursorUtil.getInt(c, "LESSMIDDLE");
		numCustomerVisitedEnd = CursorUtil.getInt(c, "LESSEND");
		numCustomerCurrentVisited = CursorUtil.getInt(c, "LESSNOW_TOTAL_VISITED");
		numCustomerCurrentVisitedInPlan = CursorUtil.getInt(c, "LESSNOW_TOTAL_VISITED_IN_PLAN");
		latPosition = CursorUtil.getDouble(c, "LAT");
		lngPosition = CursorUtil.getDouble(c, "LNG");
		numCusOrderMiddle = CursorUtil.getInt(c, "NUMMIDDLE");
		numCusOrderEnd = CursorUtil.getInt(c, "NUMEND");
		numCusOrderCurrent = CursorUtil.getInt(c, "NUMCURRENT");
	}
}
