package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.RptKPIInDateDTO;

/**
 * dto theo ngay cho man hinh bao cao
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class DynamicReportDateRowDTO extends AbstractDynamicReportRowDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	// mang ds cac column ngang
	public ArrayList<RptKPIInDateDTO> lstColumn;
	
	public DynamicReportDateRowDTO() {
		super();
		lstColumn = new ArrayList<RptKPIInDateDTO>();
	}
	
	@Override
	public Object clone() {
		// TODO Auto-generated method stub
		ArrayList<RptKPIInDateDTO> lstColumnClone = new ArrayList<RptKPIInDateDTO>();
		for (RptKPIInDateDTO date : lstColumn) {
			lstColumnClone.add(date.clone());
		}
		return lstColumnClone;
	}
}
