package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class StaffInfoDTO implements Serializable {
	public int totalCusCanLose;
	public ArrayList<SaleInMonthItemDto> arrSaleProgress;
	public ArrayList<CustomerCanLoseItemDto> arrCusCanLose;

	public StaffInfoDTO() {
		arrSaleProgress = new ArrayList<SaleInMonthItemDto>();
		arrCusCanLose = new ArrayList<CustomerCanLoseItemDto>();
	}
}
