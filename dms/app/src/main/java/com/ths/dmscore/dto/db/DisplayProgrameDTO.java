/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * DTO chuong trinh trung bay
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class DisplayProgrameDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 2320630590495338540L;
	// Id cua bang
	public long displayProgrameId;
	// ma CTTB
	public String displayProgrameCode;
	// ten CTTB
	public String displayProgrameName;
	// loai CTTB
	public String displayProgrameType;
	// trang thai
	public int status;
	// tu ngay
	public String fromDate;
	// den ngay
	public String toDate;
	// truong nay chua dung
	public float objTarget;
	// quan he: 1: OR, 0 : AND
	public int relation;
	// ngay tao
	public String createDate;
	// ngay update
	public String updateDate;
	// nguoi tao
	public String createUser;
	// nguoi update
	public String updateUser;
	// ma nganh hang
	public String cat;
	// % ti le fino
	public float percentFino;

	public int quantity;

	public int countCustomerNotComplete; // so luong khach hang chua dat - dung
											// cho nv giam sat

	public DisplayProgrameDTO() {
		super(TableType.DISPLAY_PROGRAME_TABLE);
	}
	
	public DisplayProgrameDTO addAll(long id, String name, String code) {		
		this.displayProgrameId = id;
		this.displayProgrameCode = code;
		this.displayProgrameName = name;
		return new DisplayProgrameDTO();
	}

	/**
	 * Mo ta chuc nang cua ham
	 * 
	 * @author: SoaN
	 * @param c
	 * @return
	 * @return: DisplayProgrameDTO
	 * @throws:
	 */

	public void initDisplayProgrameObject(Cursor c) {

		displayProgrameId = CursorUtil.getLong(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_ID);
		displayProgrameCode = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_CODE);
		displayProgrameName = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_NAME);
		fromDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.FROM_DATE));
		toDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.TO_DATE));
		quantity = CursorUtil.getInt(c, "SOSUAT");
		displayProgrameType = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DESCRIPTION);

	}
}
