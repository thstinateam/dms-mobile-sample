/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

/**
 *  Luu gia ca san pham
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class PriceDTO extends AbstractTableDTO {
	// ma gia
	public int priceId ;
	// ma san pham
	public int productId ; 
	// hieu luc tu ngay
	public String fromDate ;
	// hieu luc den ngay
	public String toDate ; 
	// 1: hoat dong, 0: ngung
	public int status ; 
	// nguoi tao
	public String createUser ; 
	// nguoi cap nhat
	public String updateUser ; 
	// ngay tao
	public String createDate ; 
	// ngay cap nhat
	public String updateDate ;
	// gia
	public float price ;
	// gia chua vat
	public float priceNotVat;
	// gia
	public float packagePrice;
	// gia chua vat
	public float packagePriceNotVat;
	// vat
	public float vat;
	
	public int customerTypeId;
	public int shopChannel;
	public int shopId;
	
	public PriceDTO(){
		super(TableType.PRICE_TABLE);
	}
}
