package com.ths.dmscore.dto.db;

import java.util.Date;

@SuppressWarnings("serial")
public class SynTabledataLogDTO extends AbstractTableDTO{
	private Date LAST_SYNDATE;
	private Integer ID;
	private Integer STAFF_ID;
	private Integer LAST_STATUS;
	
	public SynTabledataLogDTO(){
		super(TableType.SYN_TABLEDATA_LOG_TABLE);
	}

	public Date getLAST_SYNDATE() {
		return LAST_SYNDATE;
	}

	public void setLAST_SYNDATE(Date lAST_SYNDATE) {
		LAST_SYNDATE = lAST_SYNDATE;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Integer getSTAFF_ID() {
		return STAFF_ID;
	}

	public void setSTAFF_ID(Integer sTAFF_ID) {
		STAFF_ID = sTAFF_ID;
	}

	public Integer getLAST_STATUS() {
		return LAST_STATUS;
	}

	public void setLAST_STATUS(Integer lAST_STATUS) {
		LAST_STATUS = lAST_STATUS;
	}

}
