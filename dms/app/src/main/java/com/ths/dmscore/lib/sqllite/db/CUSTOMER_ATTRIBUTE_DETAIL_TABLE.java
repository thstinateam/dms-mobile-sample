/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;

/**
 * Luu thong tin khach hang
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CUSTOMER_ATTRIBUTE_DETAIL_TABLE extends ABSTRACT_TABLE {
	public static final String CUSTOMER_ATTRIBUTE_DETAIL_ID = "CUSTOMER_ATTRIBUTE_DETAIL_ID";
	public static final String CUSTOMER_ATTRIBUTE_ID = "CUSTOMER_ATTRIBUTE_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String VALUE = "VALUE";
	public static final String CUSTOMER_ATTRIBUTE_ENUM_ID = "CUSTOMER_ATTRIBUTE_ENUM_ID";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "CUSTOMER_ATTRIBUTE_DETAIL";

	public CUSTOMER_ATTRIBUTE_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { CUSTOMER_ATTRIBUTE_DETAIL_ID,
				CUSTOMER_ATTRIBUTE_ID, CUSTOMER_ID, VALUE,
				CUSTOMER_ATTRIBUTE_ENUM_ID, STATUS, CREATE_DATE, CREATE_USER,
				UPDATE_DATE, UPDATE_USER, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public long insert(CustomerAttributeDetailViewDTO dto, long customerID){
		dto.customerID = customerID;
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * gen content de insert
	 * 
	 * @author: dungdq3
	 * @since: 5:37:02 PM Jan 28, 2015
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @param customerID 
	 * @return
	 */
	private ContentValues initDataRow(CustomerAttributeDetailViewDTO dto) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(CUSTOMER_ID, dto.customerID);
		editedValues.put(CUSTOMER_ATTRIBUTE_DETAIL_ID, dto.detailID);
		editedValues.put(VALUE, dto.value);
		editedValues.put(CUSTOMER_ATTRIBUTE_ENUM_ID, dto.enumID);
		editedValues.put(CUSTOMER_ATTRIBUTE_ID, dto.attrID);
		editedValues.put(STATUS, dto.status);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(CREATE_USER, dto.createUser);
		return editedValues;
	}

	/**
	 * update detail
	 * 
	 * @author: dungdq3
	 * @since: 10:24:44 AM Jan 29, 2015
	 * @return: long
	 * @throws:  
	 * @param detail
	 * @param customerId
	 * @return
	 */
	public long update(CustomerAttributeDetailViewDTO detail, String customerId) {
		// TODO Auto-generated method stub
		ContentValues value = initUpdateDataRow(detail);
		String[] params = { String.valueOf(detail.detailID) };
		return update(value, CUSTOMER_ATTRIBUTE_DETAIL_ID + " = ?", params);
	}

	/**
	 * generate json update
	 * 
	 * @author: dungdq3
	 * @since: 10:50:26 AM Jan 29, 2015
	 * @return: ContentValues
	 * @throws:  
	 * @param detail
	 * @return
	 */
	private ContentValues initUpdateDataRow(
			CustomerAttributeDetailViewDTO dto) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(VALUE, dto.value);
		editedValues.put(CUSTOMER_ATTRIBUTE_ENUM_ID, dto.enumID);
		editedValues.put(STATUS, dto.status);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(UPDATE_USER, dto.updateUser);
		return editedValues;
	}
	
	protected long delete(long customerId) {
		String[] params = { "" + customerId };
		return delete(CUSTOMER_ID + " = ?", params);
	}
}
