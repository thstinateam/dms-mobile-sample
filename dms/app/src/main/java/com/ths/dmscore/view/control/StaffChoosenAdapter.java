/**
 * Copyright 2013 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.view.supervisor.routed.StaffListView;
import com.ths.dms.R;

/**
 *  Adapter cho man hinh chon nv
 *  @author: Tuanlt11
 *  @version: 1.0
 *  @since: 1.0
 */
public class StaffChoosenAdapter extends ArrayAdapter<StaffChoosenDTO> {

	Context m_conContext;
	List<StaffChoosenDTO> listDTO = null;
	int resourceLayoutId;
	boolean[] lstCheckStaff; // mang luu lai thong tin chon nhan vien
	StaffListView listener;
	int indexChoose = 0; // nhan vien da duoc chon

	public StaffChoosenAdapter(Context context, int resourceLayoutId,
			ArrayList<StaffChoosenDTO> lstStaff, int indexChoose) {
		super(context, resourceLayoutId, lstStaff);
		m_conContext = context;
		this.listDTO = lstStaff;
		this.resourceLayoutId = resourceLayoutId;
		lstCheckStaff = new boolean[lstStaff.size()];

		for (int i = 0; i < lstStaff.size(); i++) {
			StaffChoosenDTO item = lstStaff.get(i);
			if (i== indexChoose) {
				lstCheckStaff[i] = true;
				item.isSelected = true;
			} else {
				if(item.isSelected && indexChoose != 0)
					lstCheckStaff[i] = true;
				else
				{
					lstCheckStaff[i] = false;
					item.isSelected = false;
				}
			}

		}
	}

	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row = convertView;
		StaffChoosenItemRow cell = null;
		final RadioButton rdChooseStaff;
		TextView tvStaffName;
		if (row == null) {
			LayoutInflater vi = (LayoutInflater) m_conContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = vi.inflate(R.layout.layout_choose_staff_item, parent, false);
			cell = new StaffChoosenItemRow(m_conContext, row);
			row.setTag(cell);
		} else {
			cell = (StaffChoosenItemRow) row.getTag();
		}
		rdChooseStaff = (RadioButton)row.findViewById(R.id.rdChooseStaff);
		tvStaffName = (TextView)row.findViewById(R.id.tvStaff);

		rdChooseStaff.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (((RadioButton) v).isChecked()) {
					lstCheckStaff[position] = true;
					removeChecked(position);
					if(listener != null)
					{
						listener.onEvent(listDTO.get(position));
					}
				} else {
					lstCheckStaff[position] = false;
					listDTO.get(position).isSelected = false;

				}

			}
		});



		tvStaffName.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				if (!rdChooseStaff.isChecked()) {
					for (int i = 0; i < lstCheckStaff.length; i++) {
						lstCheckStaff[i] = false;
						listDTO.get(i).isSelected = false;
					}
					lstCheckStaff[position] = true;
					listDTO.get(position).isSelected = true;
					listener.onEvent(listDTO.get(position));
				}

			}
		});
		rdChooseStaff.setChecked(lstCheckStaff[position]);
		cell.populateFrom(listDTO.get(position),row);

		return row;
	}

	/**
	*  Ham xoa cac radio button da duoc chon truoc do
	*  @author: Tuanlt11
	*  @return: void
	*  @throws:
	*/
	public void removeChecked(int positionChecked){
		for(int i = 0; i < listDTO.size() ; i++)
		{
			if(i != positionChecked)
				listDTO.get(i).isSelected = false;
			else{
				listDTO.get(i).isSelected = true;
			}
		}
		notifyDataSetChanged();
	}

	public void setListener(StaffListView listener){
		this.listener = listener;
	}


}
