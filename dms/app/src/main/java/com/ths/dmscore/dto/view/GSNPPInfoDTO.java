/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * GSNPP info , use for function CTTB of NPP (module TBHV)
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class GSNPPInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public long staffId;
	public String staffCode;
	public String staffName;

	public GSNPPInfoDTO() {
		staffId = 0;
		staffCode = Constants.STR_BLANK;
		staffName = Constants.STR_BLANK;
	}

	public void initObjectWithCursor(Cursor c) {
		staffId = CursorUtil.getLong(c, STAFF_TABLE.STAFF_ID);
		staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		staffName = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
	}

}
