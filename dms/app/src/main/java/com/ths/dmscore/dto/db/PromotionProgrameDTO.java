/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 *  Luu thong tin chuong trinh khuyen mai
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class PromotionProgrameDTO extends AbstractTableDTO{
	public static final int PROGRAME_PROMOTION_AUTO = 0;
	public static final int PROGRAME_PROMOTION_MANUAL = 1;
	public static final int PROGRAME_DISPLAY_COMPENSATION = 2;
	public static final int PROGRAME_PROMOTION_CANCEL = 4;
	public static final int PROGRAME_PROMOTION_CHANGE = 3;
	public static final int PROGRAME_PROMOTION_RETURN = 5;
	public static final int PROGRAME_PROMOTION_ORDER = 6;
	public static final int PROGRAME_PROMOTION_ACCUMULATION = 7;
	public static final int PROGRAME_PROMOTION_NEW_OPEN = 8;
	// tam de tra thuong key shop type = 6
	public static final int PROGRAME_PROMOTION_KEYSHOP = 6;

	// id chuong trinh khuyen mai
	private int PROMOTION_PROGRAM_ID ;
	// ma chuong trinh
	private String PROMOTION_PROGRAM_CODE ;
	// ten chuong trinh
	private String PROMOTION_PROGRAM_NAME ;
	// trang thai 1: hieu luc, 0: het hieu luc
	private int STATUS ;
	// ma phan loai chuong trinh khuyen mai
	private String TYPE ;
	// dang khuyen mai
	private String PRO_FORMAT ;
	// tu ngay
	private String FROM_DATE ;
	// den ngay
	private String TO_DATE ;
	// quan he 1: or, 0: and
	private int RELATION ;
	// id NPP
	private int SHOP_ID ;
	// nguoi tao
	private String CREATE_USER ;
	// nguoi cap nhat
	private String UPDATE_USER ;
	// ngay tao
	private String CREATE_DATE ;
	// ngay cap nhat
	private String UPDATE_DATE ;
	// co tinh boi so khuyen mai hay khong? 1: co tinh, 0: khong tinh
	private int MULTIPLE ;
	// co tinh toi uu hay khong? 1: co tinh, 0: khong tinh
	private int recursive;
	// mo ta
	private String DESCRIPTION;
	//cot validate CTKM
	private String MD5_VALID_CODE;

	public static final int RELATION_AND = 0;
    public static final int RELATION_OR = 1;

    // kiem tra CTKM co hop le hay khong
    private boolean isInvalid = false;

    //Cho phep sua KM hay ko?
    private int isEdited;
    //CTKM co cau hinh co so suat hay khong?
	private boolean isHasQuantityReceived;
	public PromotionProgrameDTO(){
		super(TableType.PROMOTION_PROGRAME_TABLE);
	}

	/**
	 *
	*  init promotioin programe dto for request get promotion programe detail
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void initDataForRequestGetPromotionProgrameDetail(Cursor c){
		setPROMOTION_PROGRAM_CODE(CursorUtil.getString(c, "PROMOTION_PROGRAM_CODE"));
		setPROMOTION_PROGRAM_NAME(CursorUtil.getString(c, "PROMOTION_PROGRAM_NAME"));
		setTYPE(CursorUtil.getString(c, "TYPE"));
		setFROM_DATE(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "FROM_DATE")));
		setTO_DATE(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "TO_DATE")));
		setDESCRIPTION(CursorUtil.getString(c, "DESCRIPTION"));
	}

	public int getPROMOTION_PROGRAM_ID() {
		return PROMOTION_PROGRAM_ID;
	}

	public void setPROMOTION_PROGRAM_ID(int pROMOTION_PROGRAM_ID) {
		PROMOTION_PROGRAM_ID = pROMOTION_PROGRAM_ID;
	}

	public String getPROMOTION_PROGRAM_CODE() {
		return PROMOTION_PROGRAM_CODE;
	}

	public void setPROMOTION_PROGRAM_CODE(String pROMOTION_PROGRAM_CODE) {
		PROMOTION_PROGRAM_CODE = pROMOTION_PROGRAM_CODE;
	}

	public String getPROMOTION_PROGRAM_NAME() {
		return PROMOTION_PROGRAM_NAME;
	}

	public void setPROMOTION_PROGRAM_NAME(String pROMOTION_PROGRAM_NAME) {
		PROMOTION_PROGRAM_NAME = pROMOTION_PROGRAM_NAME;
	}

	public int getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(int sTATUS) {
		STATUS = sTATUS;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getPRO_FORMAT() {
		return PRO_FORMAT;
	}

	public void setPRO_FORMAT(String pRO_FORMAT) {
		PRO_FORMAT = pRO_FORMAT;
	}

	public String getFROM_DATE() {
		return FROM_DATE;
	}

	public void setFROM_DATE(String fROM_DATE) {
		FROM_DATE = fROM_DATE;
	}

	public String getTO_DATE() {
		return TO_DATE;
	}

	public void setTO_DATE(String tO_DATE) {
		TO_DATE = tO_DATE;
	}

	public int getRELATION() {
		return RELATION;
	}

	public void setRELATION(int rELATION) {
		RELATION = rELATION;
	}

	public int getSHOP_ID() {
		return SHOP_ID;
	}

	public void setSHOP_ID(int sHOP_ID) {
		SHOP_ID = sHOP_ID;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public int getMULTIPLE() {
		return MULTIPLE;
	}

	public void setMULTIPLE(int mULTIPLE) {
		MULTIPLE = mULTIPLE;
	}

	public int getRecursive() {
		return recursive;
	}

	public void setRecursive(int recursive) {
		this.recursive = recursive;
	}

	public String getDESCRIPTION() {
		return DESCRIPTION;
	}

	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}

	public String getMD5_VALID_CODE() {
		return MD5_VALID_CODE;
	}

	public void setMD5_VALID_CODE(String mD5_VALID_CODE) {
		MD5_VALID_CODE = mD5_VALID_CODE;
	}

	public boolean isInvalid() {
		return isInvalid;
	}

	public void setInvalid(boolean isInvalid) {
		this.isInvalid = isInvalid;
	}

	public int getIsEdited() {
		return isEdited;
	}

	public void setIsEdited(int isEdited) {
		this.isEdited = isEdited;
	}

	public boolean isHasQuantityReceived() {
		return isHasQuantityReceived;
	}

	public void setHasQuantityReceived(boolean isHasQuantityReceived) {
		this.isHasQuantityReceived = isHasQuantityReceived;
	}
}
