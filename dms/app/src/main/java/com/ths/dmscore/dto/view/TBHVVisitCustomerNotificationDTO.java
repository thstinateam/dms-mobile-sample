package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua class
 *
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVVisitCustomerNotificationDTO implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	public ArrayList<TBHVVisitCustomerNotificationItem> arrList = new ArrayList<TBHVVisitCustomerNotificationDTO.TBHVVisitCustomerNotificationItem>();
	public Hashtable<String, ShopParamDTO> listParam = new Hashtable<String, ShopParamDTO>();
	public String lessColumn1;
	public String lessColumn2;
	public String lessColumn3;
	public String lessColumn4;

	public TBHVVisitCustomerNotificationDTO() {
	}

	public TBHVVisitCustomerNotificationItem newTBHVVisitCustomerNotificationItem() {
		return new TBHVVisitCustomerNotificationItem();
	}

	public class TBHVVisitCustomerNotificationItem implements Serializable {
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		public String nvbhShopCode;
		public long nvbhShopId;
		public int gsnppStaffId;
		public String gsnppStaffCode;
		public String gsnppStaffName;
		public int numNvbh;
		// public int numNvbh_1200_plan;
		// public int numNvbh_1600_plan;
		// public int numNvbh_now_plan;
		public int numNvbh_right_930;
		public int numNvbh_right_1200;
		public int numNvbh_right_1600;
		public int numNvbh_right_now;
		public String less9h30;
		public String less12h00;
		public String less16h00;
		public String lessnow;
//		public String time9h30;
//		public String time1200;
//		public String time1600;
		public ArrayList<NVBH> arrNVBH = new ArrayList<TBHVVisitCustomerNotificationDTO.TBHVVisitCustomerNotificationItem.NVBH>();
//		public String time9h30_1;
//		public String time1200_1;
//		public String time1600_1;
//		public String time9h30_desc;
//		public String time1200_desc;
//		public String time1600_desc;
		public int gsnppObjectType;// loai gs

		public TBHVVisitCustomerNotificationItem() {

		}

		public class NVBH implements Serializable {
			/**
			 *
			 */
			private static final long serialVersionUID = 1L;
			public int staffId;
			public int num930;
			public int num1200;
			public int num1600;
			public int numNow;
			public int numCusPlan;
			public int num_1200_plan;
			public int num_1600_plan;
			public int num_now_plan;
		}

		/**
		 * Mo ta muc dich cua ham
		 *
		 * @author: TamPQ
		 * @param c
		 * @return: voidvoid
		 * @throws:
		 */
		public void initData(Cursor c) {
			try {
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
				String timeNow = sdf.format(cal.getTime());

				if (c == null) {
					throw new Exception("Cursor is empty");
				}
				nvbhShopId = CursorUtil.getLong(c, "NVBH_SHOP_ID");
				gsnppStaffId = CursorUtil.getInt(c, "GS_STAFF_ID");
				gsnppStaffCode = CursorUtil.getString(c, "GS_STAFF_CODE");
				gsnppStaffName = CursorUtil.getString(c, "GS_STAFF_NAME");
				nvbhShopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
				numNvbh = CursorUtil.getInt(c, "NUM_NVBH");
				gsnppObjectType = CursorUtil.getInt(c, "GS_OBJECT_TYPE");
				numNvbh_right_930 = CursorUtil.getInt(c, "NUM930NUM");
				
				String nvbhListStr = null;
				String num1200ListStr = null;
				String num1600ListStr = null;
				String numNowListStr = null;
				String numCusPlanStr = null;
				nvbhListStr = CursorUtil.getString(c, "STAFF_LIST");
				num1200ListStr = CursorUtil.getString(c, "NUM1200_LIST");
				num1600ListStr = CursorUtil.getString(c, "NUM1600_LIST");
				numNowListStr = CursorUtil.getString(c, "NUM_NOW_LIST");
				numCusPlanStr = CursorUtil.getString(c, "TOTAL_CUS_PLAN_LIST");

				String[] nvbhList = nvbhListStr.split(",");
				String[] num1200List = num1200ListStr.split(",");
				String[] num1600List = num1600ListStr.split(",");
				String[] numNowList = numNowListStr.split(",");
				String[] numCusPlanList = numCusPlanStr.split(",");

				for (int i = 0; i < nvbhList.length; i++) {
					NVBH nvbh = new NVBH();
					if (!StringUtil.isNullOrEmpty(nvbhList[i])) {
						nvbh.staffId = Integer.parseInt(nvbhList[i]);
					}
					if (!StringUtil.isNullOrEmpty(num1200List[i])) {
						nvbh.num1200 = Integer.parseInt(num1200List[i]);
					}
					if (!StringUtil.isNullOrEmpty(num1600List[i])) {
						nvbh.num1600 = Integer.parseInt(num1600List[i]);
					}
					if (!StringUtil.isNullOrEmpty(numNowList[i])) {
						nvbh.numNow = Integer.parseInt(numNowList[i]);
					}
					if (!StringUtil.isNullOrEmpty(numCusPlanList[i])) {
						nvbh.numCusPlan = Integer.parseInt(numCusPlanList[i]);
					}
					nvbh.num_1200_plan = (int) Math.ceil((double) nvbh.numCusPlan / (double) 2);

					long temp2 = DateUtils.getDistanceMinutesFrom2Hours("08:00", timeNow);
					long temp3 = DateUtils.getDistanceMinutesFrom2Hours("08:00", listParam.get("DT_END").value);
					nvbh.num_now_plan = StringUtil.calcularPercent((double) temp2, (double) temp3);
					nvbh.num_1600_plan = nvbh.numCusPlan;
					arrNVBH.add(nvbh);
				}

				for (NVBH nvbh : arrNVBH) {
					if (nvbh.numCusPlan <= 0 || nvbh.num1200 >= nvbh.num_1200_plan) {
						numNvbh_right_1200++;
					}
					if (nvbh.numCusPlan <= 0 || nvbh.num1600 >= nvbh.num_1600_plan) {
						numNvbh_right_1600++;
					}
					int temp = (int) (((double) nvbh.numNow / (double) nvbh.numCusPlan) * 100);
					if (nvbh.numCusPlan <= 0 || temp >= nvbh.num_now_plan) {
						numNvbh_right_now++;
					}
				}

			} catch (Exception e) {
			}
		}
	}
}
