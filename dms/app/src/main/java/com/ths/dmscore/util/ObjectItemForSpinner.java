package com.ths.dmscore.util;

public class ObjectItemForSpinner {
	
	public static final int TYPE_OK = 1;	
	public static final int TYPE_ALL = 2;	
	public static final int TYPE_INFO = 3;
	public static final int TYPE_NONE = 4;
	public static final int INDEX_ALL = -1;
	public static final int INDEX_OK = -2;
	public static final int INDEX_NONE = -3;	
	
	private Object realObject;
	private String field;
	private int type;
	private int index;
	public boolean isSelected;
	
	public ObjectItemForSpinner(Object object, String field,int _index, int _type) {
		// TODO Auto-generated constructor stub
		this.realObject = object;
		this.field = field;	
		this.type = _type;
		this.setIndex(_index);
		isSelected = false;
	}
	
	public ObjectItemForSpinner(Object object, String field) {
		// TODO Auto-generated constructor stub
		this.realObject = object;
		this.field = field;		
		isSelected = false;
	}
	
	public ObjectItemForSpinner(int _type) {
		this.type = _type;
		if (_type == TYPE_ALL) {
			this.index = INDEX_ALL;
		}else if (_type == TYPE_NONE) {
			this.index = INDEX_NONE;
		} 
	}
	
	public Object getRealObject() {
		return realObject;
	}
	public void setRealObject(Object realObject) {
		this.realObject = realObject;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
