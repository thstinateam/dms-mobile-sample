package com.ths.dmscore.view.listener;
/**
 * Lang nghe su kien show keyboard
 * @author banghn
 */
public interface OnSoftKeyboardShownListener {
	void onSoftKeyboardShown(boolean isShowing);
}
