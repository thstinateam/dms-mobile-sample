package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CollumnMapping;
import com.ths.dmscore.dto.db.DebitDetailDTO;
import com.ths.dmscore.dto.db.PaymentDetailDTO;
import com.ths.dmscore.lib.sqllite.db.DEBIT_DETAIL_TEMP_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Dong chi tiet cong no khach hang
 * 
 * @author: ThangNV31
 * @version: 1.0 
 */
public class CustomerDebitDetailItem implements Serializable {
	private static final long serialVersionUID = -8329115273169106131L;
	public DebitDetailDTO debitDetailDTO;
	public String payReceiveNumber;
	public String payDate;
	public int index;
	public PaymentDetailDTO currentPaymentDetailDTO;
	public boolean isCheck;
	public boolean isWouldPay;
	public long payAmout;
	//0 phieu thu, 1 phieu chi
	public int receiptType;
	//don da tra
	public boolean hasPaid = false;
	public int numDebitDetail;
	//aproved cua pay_received
	public int approved;

	public CustomerDebitDetailItem() {
		currentPaymentDetailDTO = new PaymentDetailDTO();
	}

	public void initFromCursor(Cursor c) {
		CollumnMapping mapping = new CollumnMapping();
		debitDetailDTO = new DebitDetailDTO();
		mapping.map(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_NUMBER, "ORDER_NUMBER");
		mapping.map(DEBIT_DETAIL_TEMP_TABLE.CREATE_DATE, "ORDER_DATE");
		mapping.map("RETURN_DATE", "RETURN_DATE");
		mapping.map("PAY_DATE", "PAY_DATE");
		mapping.map("FROM_OBJECT_ID", "FROM_OBJECT_ID");
		debitDetailDTO.initData(c, mapping);
		payReceiveNumber = CursorUtil.getString(c, "PAY_RECEIVED_NUMBER");
		payDate = CursorUtil.getString(c, "PAY_DATE");
		payAmout = CursorUtil.getLong(c, "PAY_AMOUNT");
		currentPaymentDetailDTO = new PaymentDetailDTO();
		currentPaymentDetailDTO.paymentDetailID  = CursorUtil.getLong(c, "PAYMENT_DETAIL_ID");
		receiptType = CursorUtil.getInt(c, "RECEIPT_TYPE");
		currentPaymentDetailDTO.discount = CursorUtil.getInt(c, "PAYMENT_DISCOUNT");
		approved = CursorUtil.getInt(c, "APPROVED");
		
	}
}