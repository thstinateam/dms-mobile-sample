/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.EquipmentHistoryDTO;

/**
 * EQUIP_HISTORY_TABLE.java
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  16:21:15 26-12-2014
 */
public class EQUIP_HISTORY_TABLE extends ABSTRACT_TABLE {
	
	public static final String EQUIP_HISTORY_ID = "EQUIP_HISTORY_ID";
	public static final String EQUIP_ID = "EQUIP_ID";
	public static final String STATUS = "STATUS";
	public static final String USAGE_STATUS = "USAGE_STATUS";
	public static final String HEALTH_STATUS = "HEALTH_STATUS";
	public static final String TRADE_STATUS = "TRADE_STATUS";
	public static final String TRADE_TYPE = "TRADE_TYPE";
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	public static final String OBJECT_ID = "OBJECT_ID";
	public static final String OBJECT_CODE = "OBJECT_CODE";
	// id nhan vien bao mat
	public static final String STAFF_ID = "STAFF_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String FORM_ID = "FORM_ID";
	public static final String FORM_TYPE = "FORM_TYPE";
	public static final String STOCK_NAME = "STOCK_NAME";
	public static final String TABLE_NAME_FORM = "TABLE_NAME";
	public static final String TABLE_NAME = "EQUIP_HISTORY";

	public EQUIP_HISTORY_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { EQUIP_HISTORY_ID, EQUIP_ID, STATUS,
				USAGE_STATUS, HEALTH_STATUS, TRADE_STATUS, TRADE_TYPE,
				OBJECT_TYPE, OBJECT_ID, OBJECT_CODE, STAFF_ID, CREATE_DATE,
				CREATE_USER, FORM_ID, FORM_TYPE, STOCK_NAME, TABLE_NAME_FORM,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((EquipmentHistoryDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * Inser thiet bi bao mat mobile
	 * @author: hoanpd1
	 * @since: 09:50:05 19-12-2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long insertEquipHistory(EquipmentHistoryDTO dto) throws Exception {
		TABLE_ID idTable = new TABLE_ID(mDB);
		dto.equipHistoryId = idTable.getMaxIdTime(TABLE_NAME);
		return insert(dto);
	}
	
	/**
	 * @author: hoanpd1
	 * @since: 16:28:19 26-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private ContentValues initDataRow(EquipmentHistoryDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(EQUIP_HISTORY_ID, dto.equipHistoryId);
		editedValues.put(EQUIP_ID, dto.equipId);
		editedValues.put(STATUS, dto.status);
		editedValues.put(USAGE_STATUS, dto.usageStatus);
		editedValues.put(HEALTH_STATUS, dto.healthStatus);
		editedValues.put(TRADE_STATUS, dto.tradeStatus);
		editedValues.put(TRADE_TYPE, dto.tradeStatus);
		editedValues.put(OBJECT_TYPE, dto.objecType);
		editedValues.put(OBJECT_ID, dto.objectId);
		editedValues.put(OBJECT_CODE, dto.objectCode);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(FORM_ID, dto.formId);
		editedValues.put(FORM_TYPE, dto.formType);
		editedValues.put(STOCK_NAME, dto.stockName);
		editedValues.put(TABLE_NAME_FORM, dto.tableName);
		return editedValues;
	}
	
}
