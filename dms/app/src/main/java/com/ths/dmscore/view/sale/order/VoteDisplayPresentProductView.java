/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;
import java.util.Vector;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerStockHistoryDTO;
import com.ths.dmscore.dto.view.DisplayPresentProductInfo;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.OnImageTakingPopupListener;
import com.ths.dmscore.view.main.LoginView;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.ListCustomerDisplayProgrameScoreDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.dto.view.VoteDisplayPresentProductViewDTO;
import com.ths.dmscore.dto.view.VoteDisplayProductDTO;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.CustomGalleryImageAdapter;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.CustomGallery;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dms.R;

/**
 * vote display present product
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class VoteDisplayPresentProductView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener, OnClickListener,
		OnItemSelectedListener, OnImageTakingPopupListener {
	// back action
	public static final int BACK_ACTION = 0;
	public final int SAVE_VOTE_DISPLAY_ACTION = 1;
	public final int CANCEL_SAVE_VOTE_DISPLAY_ACTION = 2;

	// action chup hinh trung bay sau khi cham CT
	public final int ACTION_AGREE_TAKE_IMAGE_DP = 3;
	public final int ACTION_DEPLAY_TAKE_IMAGE_DP = 4;

	// table list product
	DMSTableView tbProductPromotionList;
	// control select programe code
	Spinner spPromotionProgrameCode;
	// customer level
	TextView tvCustomerLevel;
	// programe name
	TextView tvPromotionProgrameName;
	// number product
	TextView tvNumberProduct;
	// button save
	Button btSave;
	// number item on page of table
	public static final int LIMIT_ROW_PER_PAGE = 10;
	// the first load product list
	boolean isFirstLoadProduct = false;
	// change programe code
	boolean isChangeDisplayProgramCode = false;

	// customer id
	public CustomerListItem customerListObject;

	// current diplay programe code
	String displayProgramCode;
	// current diplay programe id
	String displayProgramId;

	// list display program to vote
	public VoteDisplayPresentProductViewDTO modelViewData = new VoteDisplayPresentProductViewDTO();

	// current promotion selected
	int currentSelected = -1;

	int page = 0;
	String startTime = Constants.STR_BLANK;
	boolean isLoadData = false;
	boolean callRemoveMenuClose = false;
	// current display programe id
	String currentDisplayProgrameID = Constants.STR_BLANK;
	private double lat;
	private double lng;
	// quan ly hinh anh chuong trinh trung bay
	private RelativeLayout llGallery;
	private ImageView ivCapture;
	private CustomGallery gallery;
	CustomGalleryImageAdapter thumbAdap;

	public static VoteDisplayPresentProductView getInstance(Bundle data) {
		VoteDisplayPresentProductView instance = new VoteDisplayPresentProductView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_vote_display_present_product_view, container, false);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_CHAMCTTB);

		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		initView(v);
		initThumbnailGallery(v);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_VOTE_DISPLAY_PROGRAME));
		this.customerListObject = (CustomerListItem) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM);
		this.isFirstLoadProduct = true;
		modelViewData.listProductHasChangeVote = new ArrayList<VoteDisplayProductDTO>();
		if (!this.isLoadData
				|| this.modelViewData.listDisplayProgrameInfo.size() <= 0) {
			this.getVoteDisplayProgrameProductView();
		} else {
			this.renderDisplayProgramInfo();
		}
		this.startTime = DateUtils.now();
		return v;
	}

	/**
	 * Khoi tao view hien thi anh cttb, chup anh cttb
	 *
	 * @author: banghn
	 * @param view
	 */
	private void initThumbnailGallery(View view) {
		llGallery = (RelativeLayout) view.findViewById(R.id.llGallery);
		llGallery.setVisibility(View.VISIBLE);
		ivCapture = (ImageView) view.findViewById(R.id.ivCapture);
		ivCapture.setOnClickListener(this);
		gallery = (CustomGallery) view.findViewById(R.id.gallery);

		gallery.setSpacing(5);
		gallery.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				arg0.getParent().requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});

		// align gallery hinh anh cttb
		alignGallery();
	}

	/**
	 * Tinh toan align lai gallery hinh anh chup trung bay khi menu left hien
	 * full hoac khong
	 *
	 * @author: banghn
	 */
	private void alignGallery() {
		boolean isLeftMenuShow = ((SalesPersonActivity)parent).isShowMenu;
		// distance icon in right menu
		BitmapDrawable bd = (BitmapDrawable) parent.getResources().getDrawable(R.drawable.menu_customer_icon);
		int widthIcon = bd.getBitmap().getWidth();
		// distance padding
		int padding = 20;
		DisplayMetrics metrics = new DisplayMetrics();
		parent.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		// set gallery to left side
		MarginLayoutParams mlp = (MarginLayoutParams) gallery.getLayoutParams();
		int marginLeft = 0;
		if (isLeftMenuShow) {
			marginLeft = -(metrics.widthPixels / 2
					+ ivCapture.getLayoutParams().width - widthIcon * 2 - padding * 2);
		} else {
			marginLeft = -(metrics.widthPixels / 2
					+ ivCapture.getLayoutParams().width + widthIcon * 2 - padding);
		}
		mlp.setMargins(marginLeft, mlp.topMargin, mlp.rightMargin, mlp.bottomMargin);
	}

	/**
	 * Luong dinh vi
	 *
	 * @author : TamPQ since : 1.0
	 */
	public void processWaitingPosition() {
		lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();

		// check dinh vi cua khach hang trong tuyen
		if (lat <= 0 || lng <= 0 && isFirstLoadProduct) {// &&
														 // customerListObject.isOr
														 // == 0) {
			parent.reStartLocatingWithWaiting();
		}
	}

	/**
	 * init view
	 * @author: HaiTC3
	 * @param v
	 * @return: void
	 * @throws:
	 */
	public void initView(View v) {
		tbProductPromotionList = (DMSTableView) v.findViewById(R.id.tbProductPromotionList);
		tbProductPromotionList.setListener(this);
		tbProductPromotionList.setNumItemsPage(LIMIT_ROW_PER_PAGE);
		spPromotionProgrameCode = (Spinner) v.findViewById(R.id.spPromotionProgrameCode);
		spPromotionProgrameCode.setOnItemSelectedListener(this);
		tvCustomerLevel = (TextView) v.findViewById(R.id.tvCustomerLevel);
		tvNumberProduct = (TextView) v.findViewById(R.id.tvNumberProduct);
		tvPromotionProgrameName = (TextView) v.findViewById(R.id.tvPromotionProgrameName);
		btSave = (Button) v.findViewById(R.id.btSave);
		btSave.setOnClickListener(this);
	}

	/**
	 * update layout after get display program info
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderDisplayProgramInfo() {
		String[] arrPromotion = new String[this.modelViewData.listDisplayProgrameInfo.size()];
		for (int i = 0, size = this.modelViewData.listDisplayProgrameInfo.size(); i < size; i++) {
			arrPromotion[i] = this.modelViewData.listDisplayProgrameInfo.get(i).displayProgramCode;
		}
		SpinnerAdapter adapterLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrPromotion);
		this.spPromotionProgrameCode.setAdapter(adapterLine);
	}

	/**
	 * update layout for display promotion info
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderProgrameInfo() {
		DisplayPresentProductInfo currentDisplayPrograme = this.modelViewData.listDisplayProgrameInfo
				.get(this.spPromotionProgrameCode.getSelectedItemPosition());
		tvPromotionProgrameName.setText(currentDisplayPrograme.displayProgramName);
		tvCustomerLevel.setText(String.valueOf(currentDisplayPrograme.joinLevel));
	}

	/**
	 * render layout after get data from db
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderLayout(boolean refreshImage) {
		if (isFirstLoadProduct) {
			VoteDisplayPresentProductRow header = new VoteDisplayPresentProductRow(parent);
			tbProductPromotionList.addHeader(header);
		} else if (isChangeDisplayProgramCode) {
		}
		tbProductPromotionList.clearAllData();
		int pos = 1
				+ Constants.NUM_ITEM_PER_PAGE
				* (tbProductPromotionList.getPagingControl().getCurrentPage() - 1);
		int posGroup = 1;
		if (this.modelViewData.listProductDisplay.size() > 0) {
			for (VoteDisplayProductDTO dto : this.modelViewData.listProductDisplay) {
				VoteDisplayPresentProductRow row = null;
				if (dto.isGroup) {
					row = new VoteDisplayPresentProductRow(parent);
					row.setListener(this);
					row.renderTitle(posGroup, dto);
					posGroup++;
				} else {
					row = new VoteDisplayPresentProductRow(parent);
					row.setListener(this);
					row.renderLayout(pos, dto);
					row.setTag(Integer.valueOf(pos));
					pos++;
				}
				row.setClickable(true);
				// row.setTag(Integer.valueOf(pos));
				int num = this.getNumberVoteForProduct(dto);
				if (num > 0) {
					row.etVote.setText(String.valueOf(num));
				}

				tbProductPromotionList.addRow(row);
			}
		} else {
			tbProductPromotionList.showNoContentRow(StringUtil.getString(R.string.TEXT_NOTIFY_PRODUCT_NULL));
		}


		if (refreshImage) {
			// renderLayoutImage();
			initMediaGallery();
		}
	}

	/**
	 * Khoi tao gallery hinh anh chup CTTB
	 * @author: banghn
	 */
	private void initMediaGallery() {
		if (thumbAdap == null) {
			thumbAdap = new CustomGalleryImageAdapter(parent,
					VoteDisplayPresentProductView.this,
					modelViewData.listImageDisplay);
			thumbAdap.isShowBackground(false);
			gallery.setAdapter(thumbAdap);
		} else {
			thumbAdap.notifyDataSetChanged();
			gallery.setSelection(0);
		}
		gallery.setTotalItem(modelViewData.listImageDisplay.size());
		gallery.setSpanGallery(((SalesPersonActivity)parent).isShowMenu);
	}

	/**
	 * lay thong tin cho man hinh cham trung bay hien dien mat hang
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void getVoteDisplayProgrameProductView() {
		if (!parent.isShowProgressDialog()) {
			this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}
		Bundle data = new Bundle();

		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());

		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerListObject.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_STAFF_ID, staffId);
		String page = " limit " + (0 * LIMIT_ROW_PER_PAGE) + ","
				+ LIMIT_ROW_PER_PAGE;
		data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE, this.displayProgramCode);
		data.putString(IntentConstants.INTENT_PAGE, page);
		data.putBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM, true);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putInt(IntentConstants.INTENT_OBJECT_TYPE, MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		handleViewEvent(data, ActionEventConstant.GET_VOTE_DISPLAY_PROGRAME_VIEW, SaleController.getInstance());
	}

	/**
	 *
	 * get list vote display product DTO
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void getListVoteDisplayProductDTO(int pageNumber, boolean isGetCount) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		// get current display programe code
		DisplayPresentProductInfo currentDisplayPrograme = this.modelViewData.listDisplayProgrameInfo
				.get(this.spPromotionProgrameCode.getSelectedItemPosition());
		displayProgramCode = currentDisplayPrograme.displayProgramCode;

		// send request get list vote product
		Bundle data = new Bundle();
		String page = " limit " + (pageNumber * LIMIT_ROW_PER_PAGE) + ","
				+ LIMIT_ROW_PER_PAGE;

		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		data.putString(IntentConstants.INTENT_STAFF_ID, staffId);

		data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, currentDisplayPrograme.displayProgrameID);
		data.putString(IntentConstants.INTENT_LEVEL_CODE, currentDisplayPrograme.joinLevel);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerListObject.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_PAGE, page);
		data.putBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM, isGetCount);
		data.putInt(IntentConstants.INTENT_OBJECT_TYPE, MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(data, ActionEventConstant.GET_LIST_VOTE_DISPLAY_PRODUCT, SaleController.getInstance());
	}

	@Override
	protected void setTitleHeaderView(String title) {
		// TODO Auto-generated method stub
		super.setTitleHeaderView(title);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_VOTE_DISPLAY_PROGRAME_VIEW: {
			this.modelViewData = (VoteDisplayPresentProductViewDTO) modelEvent.getModelData();
			if (this.modelViewData != null
					&& this.modelViewData.listDisplayProgrameInfo.size() > 0) {
				// da co cham trung bay truoc do
				if (this.modelViewData.numDisplayVoted > 0) {
					parent.removeMenuCloseCustomer();
				}
				currentSelected = 0;
				renderDisplayProgramInfo();
				renderLayoutListProduct(true);

				// update show display program info
				DisplayPresentProductInfo currentDisplayPrograme = this.modelViewData.listDisplayProgrameInfo.get(0);
				tvPromotionProgrameName.setText(currentDisplayPrograme.displayProgramName);
				tvCustomerLevel.setText(String.valueOf(currentDisplayPrograme.joinLevel));

				this.isLoadData = true;
			}
			this.parent.closeProgressDialog();
			// TamPQ: them luong dinh vi
			processWaitingPosition();
			break;
		}
		case ActionEventConstant.GET_LIST_VOTE_DISPLAY_PRODUCT: {
			VoteDisplayPresentProductViewDTO result = (VoteDisplayPresentProductViewDTO) modelEvent.getModelData();
			Bundle b = (Bundle) actionEvent.viewData;
			boolean isGetCount = b.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);
			if (isGetCount) {
				this.modelViewData.totalProductDisplay = result.totalProductDisplay;
				//this.modelViewData.listImageDisplay = result.listImageDisplay;
				this.modelViewData.listImageDisplay.clear();
				for (MediaItemDTO item : result.listImageDisplay) {
					this.modelViewData.listImageDisplay.add(item);
				}
			}

			this.modelViewData.listProductDisplay = result.listProductDisplay;

			this.renderLayoutListProduct(isGetCount);
			this.parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.SAVE_VOTE_DISPLAY_PRESENT_PRODUCT: {
			parent.closeProgressDialog();
			// save to action log
			ActionLogDTO action = new ActionLogDTO();
			action.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			action.aCustomer.customerId = this.customerListObject.aCustomer.customerId;
			action.aCustomer.lat = this.customerListObject.aCustomer.lat;
			action.aCustomer.lng = this.customerListObject.aCustomer.lng;
			action.objectId = this.currentDisplayProgrameID;
			action.objectType = "2";
			action.startTime = this.startTime;
			action.isOr = this.customerListObject.isOr;
			action.routingId = this.customerListObject.routingId;
			action.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			parent.requestInsertActionLog(action);
			requestInsertLogKPI(HashMapKPI.NVBH_CHAMTRUNGBAY, modelEvent.getActionEvent());
			continueAfterVoteDP();

			if (!this.callRemoveMenuClose) {
				parent.removeMenuCloseCustomer();
				this.callRemoveMenuClose = true;
			}

			break;
		}
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER:
			// parent.closeProgressDialog();
			getImageListCTTB();
			// continueAfterVoteDP();
			break;
		case ActionEventConstant.ACTION_LOAD_LIST_IMAGE_CTTB:
			parent.closeProgressDialog();
			ArrayList<MediaItemDTO> model = (ArrayList<MediaItemDTO>) modelEvent.getModelData();
			modelViewData.listImageDisplay.clear();
			for (MediaItemDTO item : model) {
				modelViewData.listImageDisplay.add(item);
			}
			initMediaGallery();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Tiep tuc luon cham trung bay sau khi chup anh trung bay
	 *
	 * @author: BANGHN
	 */
	private void continueAfterVoteDP() {
		// go to next display programe or go to remain product view
		if (this.modelViewData.listDisplayProgrameInfo.size() > 0) {

			this.modelViewData.listDisplayProgrameInfo.remove(this.spPromotionProgrameCode.getSelectedItemPosition());
		}

		if (this.modelViewData.listDisplayProgrameInfo.size() > 0) {
			currentSelected = -1;
			this.renderDisplayProgramInfo();
		} else {
			CustomerListItem lastVisitCustomer = GlobalInfo.getInstance().getProfile().getLastVisitCustomer();
			lastVisitCustomer.isHaveDisplayProgramNotYetVote = false;
			gotoNextView();
		}
	}

	/**
	 * Upload hinh chup cham trung bay
	 *
	 * @author: BANGHN
	 */
	public void updateTakenPhoto() {
		// upload
		DisplayPresentProductInfo curentDP = modelViewData.listDisplayProgrameInfo
				.get(this.spPromotionProgrameCode.getSelectedItemPosition());

		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		viewData.add(IntentConstants.INTENT_STAFF_ID);
		viewData.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));

		viewData.add(IntentConstants.INTENT_SHOP_ID);
		if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId())) {
			viewData.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}

		if (parent.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(parent.takenPhoto.getName());
			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(parent.takenPhoto.getAbsolutePath());
		}

		// tao doi tuong mediaItem de insert row du lieu
		MediaItemDTO dto = new MediaItemDTO();

		try {
			dto.objectId = customerListObject.aCustomer.customerId;
			dto.objectType = MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE;
			dto.monthSeq = modelViewData.monthSeq;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = parent.takenPhoto.getAbsolutePath();
			dto.thumbUrl = parent.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			dto.displayProgrameId = Long.parseLong(curentDP.displayProgrameID);
			dto.fileSize = parent.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
//			dto.type = 1;
			dto.status = 1;
			if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId())) {
				dto.shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			}
			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(String.valueOf(dto.objectId));
			viewData.add(IntentConstants.INTENT_MONTH_SEQ);
			viewData.add(String.valueOf(dto.monthSeq));
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));

			viewData.add(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
			viewData.add(curentDP.displayProgrameID);
			viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
			viewData.add(this.customerListObject.aCustomer.customerCode);
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
//			viewData.add(IntentConstants.INTENT_TYPE);
//			viewData.add("1");
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}

		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	/**
	 * Update setting bo qua thong bao chup anh trung bay lan sau
	 *
	 * @author: BANGHN
	 * @param isIgnore
	 *            : bo qua?
	 */
	private void updateSetting(boolean isIgnore) {
//		SharedPreferences sharedPreferences = parent.getSharedPreferences(LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
		SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
		Editor prefsPrivateEditor = sharedPreferences.edit();
		prefsPrivateEditor.putBoolean(LoginView.VNM_IGNORE_MSG_TAKE_PICTURE_DP, isIgnore);
		prefsPrivateEditor.commit();
	}

	/**
	 *
	 * render layout cho d/s san pham cua chuong trinh trung bay
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void renderLayoutListProduct(boolean refreshImage) {
		// paging
		if (this.modelViewData.listProductDisplay.size() > 0) {
			tbProductPromotionList.getPagingControl().setVisibility(View.VISIBLE);
			if (tbProductPromotionList.getPagingControl().totalPage < 0
					|| isChangeDisplayProgramCode) {
				tbProductPromotionList.setTotalSize(this.modelViewData.totalProductDisplay,1);
			}
		} else {
			tbProductPromotionList.getPagingControl().setVisibility(View.GONE);
		}

		renderLayout(refreshImage);
		if (isChangeDisplayProgramCode) {
			isChangeDisplayProgramCode = false;
		}
		if (isFirstLoadProduct) {
			isFirstLoadProduct = false;
		}
		this.btSave.setEnabled(true);
	}

	/**
	 *
	 * lay index cua chuong trinh trung bay tiep theo
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getNextSelectedDisplay() {
		int kq = -1;
		for (int i = 0, size = this.modelViewData.listDisplayProgrameInfo.size(); i < size; i++) {
			if (!this.modelViewData.listDisplayProgrameInfo.get(i).isVoted) {
				kq = i;
				break;
			}
		}
		return kq;
	}

	/**
	 *
	 * hien thi man hinh kiem tra hang hon
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void gotoNextView() {
		CustomerListView frag = (CustomerListView) this.findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
		if (frag != null) {
			frag.isBackFromPopStack = true;
		}
		GlobalUtil.popBackStack(parent);
		if (customerListObject.isHaveEquip) {
			gotoTakePhotoEquipment(customerListObject);
		}else if(customerListObject.isExistKeyShop){
			goToVoteKeyShop(customerListObject);
		} else if (customerListObject.isHaveSaleOrder) {
			gotoRemainProductView(
			// neu co don dat hang & chua dc kiem hang ton
					this.customerListObject.aCustomer.getCustomerId(),
					this.customerListObject.aCustomer.getCustomerCode(),
					this.customerListObject.aCustomer.getCustomerName(),
					this.customerListObject.aCustomer.getStreet(),
					this.customerListObject.aCustomer.getCustomerTypeId(),
					this.customerListObject.aCustomer.cashierStaffID,
					this.customerListObject.aCustomer.deliverID);
		} else {
			gotoCreateOrder(this.customerListObject);
		}
	}

	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoTakePhotoEquipment(CustomerListItem listItem) {
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, listItem);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_TAKE_PHOTO_EQUIPMENT, SaleController.getInstance());
	}
	
	/**
	 * Toi man hinh cham key shop
	 * @author: Tuanlt11
	 * @param item
	 * @return: void
	 * @throws:
	*/
	private void goToVoteKeyShop(CustomerListItem item) {
		String customerId = item.aCustomer.getCustomerId();
		String customerCode = item.aCustomer.getCustomerCode();
		String customerName = item.aCustomer.getCustomerName();
		String customerAddress = item.aCustomer.getStreet();
		int customerTypeId = item.aCustomer.getCustomerTypeId();
		int isOr = item.isOr;
		String cashierID = item.aCustomer.cashierStaffID;
		String deliverID = item.aCustomer.deliverID;
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliverID);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, item);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_VOTE_KEY_SHOP, SaleController.getInstance());
	}

	/**
	 *
	 * go to remain product view
	 *
	 * @author: HaiTC3
	 * @param customerId
	 * @param customerCode
	 * @param customerName
	 * @return: void
	 * @throws:
	 */
	private void gotoRemainProductView(String customerId, String customerCode,
			String customerName, String customerAddress, int customerTypeId, String cashierID, String deliveryID) {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(this.customerListObject.isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliveryID);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, customerListObject);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_REMAIN_PRODUCT_VIEW, SaleController.getInstance());
	}

	/**
	 * go to create order view
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	private void gotoCreateOrder(CustomerListItem dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, dto.aCustomer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, dto.aCustomer.getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, dto.aCustomer.getStreet());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, dto.aCustomer.getCustomerCode());
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "0");
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, dto.aCustomer.getCustomerTypeId());
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				dto.aCustomer.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID,
				dto.aCustomer.deliverID);
		bundle.putSerializable(IntentConstants.INTENT_SUGGEST_ORDER_LIST, null);
		bundle.putString(IntentConstants.INTENT_IS_OR, String.valueOf(dto.isOr));
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_NEW);
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, dto.routingId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * check vote display product exits in list save TMP
	 *
	 * @author: HaiTC3
	 * @param currentProduct
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean isExitsProductInListSaveTMP(VoteDisplayProductDTO currentProduct) {
		boolean kq = false;
		if (this.modelViewData.listProductHasChangeVote != null) {
			for (int i = 0, size = modelViewData.listProductHasChangeVote.size(); i < size; i++) {
				VoteDisplayProductDTO selectedProduct = modelViewData.listProductHasChangeVote.get(i);
				if (currentProduct.productCode.equals(selectedProduct.productCode)) {
					kq = true;
					break;
				}
			}
		}
		return kq;
	}

	/**
	 * get index of vote display product in list vote display product save TMP
	 * @author: HaiTC3
	 * @param product
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getIndexProductInProductListSaveTMP(VoteDisplayProductDTO product) {
		int kq = -1;
		if (this.isExitsProductInListSaveTMP(product)) {
			for (int i = 0, size = modelViewData.listProductHasChangeVote.size(); i < size; i++) {
				VoteDisplayProductDTO selectedProduct = modelViewData.listProductHasChangeVote.get(i);
				if (product.productCode.equals(selectedProduct.productCode)) {
					kq = i;
					break;
				}
			}
		}
		return kq;
	}

	/**
	 * get vote value for vote display product dto
	 * @author: HaiTC3
	 * @param product
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getNumberVoteForProduct(VoteDisplayProductDTO product) {
		int kq = 0;
		for (int i = 0, size = modelViewData.listProductHasChangeVote.size(); i < size; i++) {
			VoteDisplayProductDTO productSelected = modelViewData.listProductHasChangeVote.get(i);
			if (product.productCode.equals(productSelected.productCode)) {
				kq = productSelected.voteNumber;
			}
		}
		return kq;
	}

	/**
	 * validate vote number user input
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean validateVoteNumber() {
		boolean kq = true;
		if (this.modelViewData != null
				&& this.modelViewData.listProductDisplay != null
				&& this.modelViewData.listProductDisplay.size() > 0) {
			for (int i = 0, size = tbProductPromotionList.getChildCount(); i < size; i++) {
				VoteDisplayPresentProductRow row = (VoteDisplayPresentProductRow) tbProductPromotionList.getListChildRow().get(i);
				VoteDisplayProductDTO voteDto = modelViewData.listProductDisplay.get(i);
				if (!voteDto.isGroup) {
					String voteNumber = row.etVote.getText().toString();

					if (!StringUtil.isNullOrEmpty(voteNumber)) {
						if (!StringUtil.isValidateIntergeNonNegativeInput(voteNumber)) {
							kq = false;
							row.etVote.requestFocus();
							break;
						}
					}
				}
			}
		}
		return kq;
	}

	/**
	 * save list product display present we have change vote number
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public boolean saveTMPVoteDisplayCurrent() {
		boolean kq = true;
		if (!validateVoteNumber()) {
			GlobalUtil
					.showDialogConfirm(
							this.parent,
							StringUtil.getString(R.string.TEXT_NOTIFY_INPUT_VOTE_NUMBER_INCORRECT),
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
							0, "", 0, null);
			return false;
		}
		if (this.modelViewData != null
				&& this.modelViewData.listProductDisplay != null
				&& this.modelViewData.listProductDisplay.size() > 0) {
			for (int i = 0, size = tbProductPromotionList.getListChildRow().size(); i < size; i++) {
				VoteDisplayProductDTO voteDto = modelViewData.listProductDisplay.get(i);
				if (!voteDto.isGroup) {
					VoteDisplayPresentProductRow row = (VoteDisplayPresentProductRow) tbProductPromotionList
							.getListChildRow().get(i);
					String voteNumber = "";
					if (!StringUtil.isNullOrEmpty(row.etVote.getText()
							.toString())) {
						try {
							voteNumber = GlobalUtil.calRealOrder(row.etVote
									.getText().toString(),
									this.modelViewData.listProductDisplay
											.get(i).convfact)
									+ "";
						} catch (Exception e) {
						}
					}
					// vote number = 0
					if (!StringUtil.isNullOrEmpty(voteNumber)) {
						// chua ton tai trong ds luu tam thoi
						if (!this.isExitsProductInListSaveTMP(this.modelViewData.listProductDisplay.get(i))) {
							this.modelViewData.listProductDisplay.get(i).voteNumber = Integer.valueOf(voteNumber);
							modelViewData.listProductHasChangeVote.add(this.modelViewData.listProductDisplay.get(i));
						} else { // da ton tai trong d/s luu tam thoi
							// co thay doi gia tri cham trung bay
							if (this.getIndexProductInProductListSaveTMP(this.modelViewData.listProductDisplay.get(i)) >= 0) {
								// update value number product for org list dto
								int index = getIndexProductInProductListSaveTMP(this.modelViewData.listProductDisplay.get(i));
								VoteDisplayProductDTO dto = modelViewData.listProductHasChangeVote.get(index);
								dto.voteNumber = Integer.valueOf(voteNumber);
								modelViewData.listProductHasChangeVote.set(index, dto);
							}
						}
					} else {
						if (this.isExitsProductInListSaveTMP(this.modelViewData.listProductDisplay.get(i))) {
							if (this.getIndexProductInProductListSaveTMP(this.modelViewData.listProductDisplay.get(i)) >= 0) {
								int index = this.getIndexProductInProductListSaveTMP(this.modelViewData.listProductDisplay.get(i));
								modelViewData.listProductHasChangeVote.remove(index);
							}
						}
					}
				}
			}
		}
		return kq;
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.SAVE_VOTE_DISPLAY_PRESENT_PRODUCT:
			this.btSave.setEnabled(true);
			// Log.v("insert voted display", "ERROR");
			// gotoRemainProductView();
			break;
		case ActionEventConstant.GET_LIST_VOTE_DISPLAY_PRODUCT:
			this.modelViewData.totalProductDisplay = 0;
			this.modelViewData.listProductDisplay = new ArrayList<VoteDisplayProductDTO>();
			this.modelViewData.listProductHasChangeVote = new ArrayList<VoteDisplayProductDTO>();
			Bundle b = (Bundle) actionEvent.viewData;
			boolean isGetCount = b.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);
			this.renderLayout(isGetCount);
			break;
		case ActionEventConstant.GET_VOTE_DISPLAY_PROGRAME_VIEW:
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER:
			parent.closeProgressDialog();
			this.btSave.setEnabled(true);
			super.handleErrorModelViewEvent(modelEvent);
			break;
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER:
			parent.closeProgressDialog();
			continueAfterVoteDP();
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 *
	 * request save vote display to server and local
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void requestSaveVoteDisplay() {
		if (modelViewData.listDisplayProgrameInfo.size() > 0
				&& spPromotionProgrameCode.getSelectedItemPosition() >= 0
				&& spPromotionProgrameCode.getSelectedItemPosition() < modelViewData.listDisplayProgrameInfo
						.size()) {
			this.parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
			Bundle newData = new Bundle();

			String staffId = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritId());
			DisplayPresentProductInfo currentDisplayPrograme = this.modelViewData.listDisplayProgrameInfo
					.get(this.spPromotionProgrameCode.getSelectedItemPosition());

			ListCustomerDisplayProgrameScoreDTO listDisplayProductVoted = new ListCustomerDisplayProgrameScoreDTO();

			for (int i = 0, size = this.modelViewData.listProductHasChangeVote
					.size(); i < size; i++) {
				VoteDisplayProductDTO dto = this.modelViewData.listProductHasChangeVote
						.get(i);
				CustomerStockHistoryDTO displayProductDTO = new CustomerStockHistoryDTO();
				displayProductDTO.customerId = Long
						.valueOf(this.customerListObject.aCustomer
								.getCustomerId());
				displayProductDTO.staffId = Integer.valueOf(staffId);
				displayProductDTO.productId = Integer.valueOf(dto.productID);
				displayProductDTO.objectType = Integer
						.valueOf(CustomerStockHistoryDTO.TYPE_CTTB);
				displayProductDTO.objectId = Integer
						.valueOf(currentDisplayPrograme.displayProgrameID);
				displayProductDTO.result = Integer.valueOf(dto.voteNumber);
				displayProductDTO.createDate = DateUtils.now();
				listDisplayProductVoted.getListCusDTO().add(displayProductDTO);
			}

			this.currentDisplayProgrameID = currentDisplayPrograme.displayProgrameID;
			newData.putSerializable(
					IntentConstants.INTENT_LIST_DISPLAY_PROGRAM_PRODUCT,
					listDisplayProductVoted);
			handleViewEvent(newData,
					ActionEventConstant.SAVE_VOTE_DISPLAY_PRESENT_PRODUCT,
					SaleController.getInstance());
		} else {
			continueAfterVoteDP();
			if (!this.callRemoveMenuClose) {
				parent.removeMenuCloseCustomer();
				this.callRemoveMenuClose = true;
			}
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		if (eventType == SAVE_VOTE_DISPLAY_ACTION) {
			this.requestSaveVoteDisplay();
		} else if (eventType == CANCEL_SAVE_VOTE_DISPLAY_ACTION) {
			// don't do any thing
		} else if (eventType == ACTION_AGREE_TAKE_IMAGE_DP) {
			checkDistanceAndTakeImage();
		} else if (eventType == ACTION_DEPLAY_TAKE_IMAGE_DP) {
			continueAfterVoteDP();
		}
	}

	/**
	 * Kiem tra khoang cach hop le va thuc hien chup anh
	 * @author: banghn
	 */
	private void checkDistanceAndTakeImage(){
		LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile()
				.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
				.getProfile().getMyGPSInfo().getLongtitude());
		if (checkDistance()) {
			parent.takenPhoto = GlobalUtil.takePhoto(parent,
					GlobalBaseActivity.RQ_TAKE_PHOTO_VOTE_DP);
		} else {
			//gui log len server phuc vu dieu tra khong chup anh TB
			ServerLogger.sendLog("VoteDisplayPresentProductView",
					"Khoang cach xa hon cho phep : CUS="
							+ this.customerListObject.aCustomer.customerCode + " (Lat, Lng)="
							+ myLoc.lat +"," + myLoc.lng,
							true, TabletActionLogDTO.LOG_CLIENT);

			//thong bao len view va tiep tuc
			SpannableObject msg = new SpannableObject();
			msg.addSpan(StringUtil
					.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_1),
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.NORMAL);

			msg.addSpan(
					this.customerListObject.aCustomer.customerCode
							+ "-"
							+ this.customerListObject.aCustomer.customerName,
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.BOLD);

			msg.addSpan(StringUtil
					.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_2),
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.NORMAL);
			// dong dialog waitting dang luong chup anh
			parent.closeProgressDialog();
			// show msg thong bao
			parent.showDialog(msg.getSpan());
//			continueAfterVoteDP();
		}
	}


	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		if (this.saveTMPVoteDisplayCurrent()) {
			getListVoteDisplayProductDTO((tbProductPromotionList.getPagingControl().getCurrentPage() - 1), false);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
		if (action == ActionEventConstant.SHOW_KEYBOARD_CUSTOM) {
			parent.showKeyboardCustom(control);
		}
	}

	@Override
	public void onClick(View v) {
		parent.hideKeyboardCustom();
		if (v == btSave) {
			GlobalUtil.forceHideKeyboard(parent);
			if (!checkDistance()) {
				ActionLogDTO al = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				String mess = StringUtil
						.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_1)
						+ " "
						+ al.aCustomer.customerCode
						+ " - "
						+ al.aCustomer.customerName
						+ " "
						+ StringUtil
								.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_2);

				GlobalUtil.showDialogConfirm(parent, mess,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
						false);
			} else {
				if (this.saveTMPVoteDisplayCurrent()) {
					saveVoteDisplayPresentProductToDB();
				}
			}
		} else if (v == ivCapture) {
			if (modelViewData.listDisplayProgrameInfo.size() > 0
					&& spPromotionProgrameCode.getSelectedItemPosition() >= 0
					&& spPromotionProgrameCode.getSelectedItemPosition() < modelViewData.listDisplayProgrameInfo.size()) {
				checkDistanceAndTakeImage();
			}
		}
	}

	/**
	 * save vote display present product to Db local
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void saveVoteDisplayPresentProductToDB() {
		if (this.modelViewData.listProductHasChangeVote.size() <= 0
				&& modelViewData.listProductDisplay.size() > 0) {
			GlobalUtil.showDialogConfirm(this.parent,
					StringUtil.getString(R.string.TEXT_NOTIFY_PLEASE_INPUT_VOTE),
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
					Constants.STR_BLANK, 0, null);
		} else if (modelViewData.listDisplayProgrameInfo.size() > 0
				&& (modelViewData.listImageDisplay == null || modelViewData.listImageDisplay
						.size() <= 0)) {
			parent.showDialog(StringUtil
					.getString(R.string.NEED_TAKE_PHOTO_TO_VOTE_DISPLAY));
		} else {
			GlobalUtil.showDialogConfirm(this, this.parent,
					StringUtil.getString(R.string.TEXT_MESSAGE_CONFIRM_SAVE_VOTE_DISPLAY),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					this.SAVE_VOTE_DISPLAY_ACTION,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					this.CANCEL_SAVE_VOTE_DISPLAY_ACTION, null);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (currentSelected != arg2 && this.isLoadData
				&& this.modelViewData != null
				&& this.modelViewData.listDisplayProgrameInfo.size() > 0) {
			currentSelected = arg2;

			// change display programe info
			renderProgrameInfo();

			isChangeDisplayProgramCode = true;
			// reset list product has change vote number
			modelViewData.listProductHasChangeVote = new ArrayList<VoteDisplayProductDTO>();

			// get list vote display product
			this.getListVoteDisplayProductDTO(0, true);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				currentSelected = -1;
				if (this.modelViewData.listDisplayProgrameInfo.size() <= 0) {
					this.getVoteDisplayProgrameProductView();
				} else {
					this.renderDisplayProgramInfo();
				}
			}
			break;
		case ActionEventConstant.ACTION_ADJUST_GALLERY:
			alignGallery();
			gallery.setSpanGallery(((SalesPersonActivity)parent).isShowMenu);
			thumbAdap.notifyDataSetChanged();
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onImageTakingPopupEvent(int eventType, Object data) {
		// TODO Auto-generated method stub

	}

	/**
	 * Lay ds hinh anh trung bay
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void getImageListCTTB() {
		parent.showProgressDialog(StringUtil.getString(R.string.loading), true);

		Bundle bundle = new Bundle();
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		bundle.putString(IntentConstants.INTENT_STAFF_ID, staffId);

		bundle.putInt(IntentConstants.INTENT_OBJECT_TYPE,
				MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		bundle.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID,
				this.modelViewData.listDisplayProgrameInfo
						.get(this.spPromotionProgrameCode
								.getSelectedItemPosition()).displayProgrameID);
		bundle.putString(IntentConstants.INTENT_OBJECT_ID,
				this.customerListObject.aCustomer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(bundle, ActionEventConstant.ACTION_LOAD_LIST_IMAGE_CTTB, SaleController.getInstance());

	}

	/**
	 * Chup hinh tiep
	 * @author: Nguyen Thanh Dung
	 * @return: voidvoid
	 * @throws:
	 */
	public void takeNextImage() {
		//DisplayPresentProductInfo object = modelViewData.listDisplayProgrameInfo
		//		.get(this.spPromotionProgrameCode.getSelectedItemPosition());
		// chup hinh trung bay
		//checkNoticeMsgAndTakeImageDisplayPrograme(object);

		//kiem tra kc va chup anh trung bay
		checkDistanceAndTakeImage();
	}

	 /**
	 * check khoang cach thoa dieu kien de CTB
	 * @author: Tuanlt11
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	private boolean checkDistance(){
		ActionLogDTO action = GlobalInfo.getInstance().getProfile().getActionLogVisitCustomer();
		if (action != null) {
			LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			LatLng cusLoc = new LatLng(this.customerListObject.aCustomer.lat,
					this.customerListObject.aCustomer.lng);
			if (GlobalUtil.getDistanceBetween(myLoc, cusLoc) <= modelViewData.shopDistance) {
				return true;
			}
		}
		return false;
	}
}
