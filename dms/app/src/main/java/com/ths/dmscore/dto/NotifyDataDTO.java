package com.ths.dmscore.dto;

import java.io.Serializable;

public class NotifyDataDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String lastTimeHaveLoc = null;
	public int numDataNeedSync = 0;
	
	@Override
	public String toString() {
		return String.format("NotifyDataDTO {lastTimeHaveLoc: %s, numDataNeedSync: %s}", 
				lastTimeHaveLoc, numDataNeedSync);
	}
}
