package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

public class ListCustomerDTO {
	private int totalItem;
	private List<CustomerListDTO> listCusDTO = null;

	public ListCustomerDTO() {
		this.totalItem = 0;
		listCusDTO = new ArrayList<CustomerListDTO>();
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public List<CustomerListDTO> getListCusDTO() {
		return listCusDTO;
	}

	public void setListCusDTO(List<CustomerListDTO> listCusDTO) {
		this.listCusDTO = listCusDTO;
	}
}
