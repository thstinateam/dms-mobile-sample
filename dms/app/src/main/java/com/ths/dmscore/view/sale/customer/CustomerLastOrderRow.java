package com.ths.dmscore.view.sale.customer;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.view.SaleOrderCustomerDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

/**
 * Dong thong tin don hang gan nhat cua khach hang
 * @author : BangHN
 * since : 2:15:44 PM
 * version :
 */
public class CustomerLastOrderRow extends DMSTableRow implements OnClickListener{
	TextView tvNum;
	TextView tvOrderCode;//so don hang
	TextView tvDate;//ngay
	TextView tvSKU;//sku
	TextView tvMoney;//thanh tien

	public CustomerLastOrderRow(Context context) {
		super(context, R.layout.layout_customer_last_order_row, GlobalInfo
				.getInstance().isSysShowPrice() ? null
				: new int[] { R.id.tvMoney });
		setOnClickListener(this);
		tvNum = (TextView) findViewById(R.id.tvNum);
		tvOrderCode = (TextView) findViewById(R.id.tvOrderCode);
		tvDate = (TextView) findViewById(R.id.tvDate);
		tvSKU = (TextView) findViewById(R.id.tvSKU);
		tvMoney = (TextView) findViewById(R.id.tvMoney);
	}


	public void setDataRow(SaleOrderCustomerDTO dto, int index){
		tvNum.setText("" + index);
		tvOrderCode.setText(dto.getSaleOder().orderNumber);
		tvDate.setText(dto.getSaleOder().orderDate);
		display(tvMoney, dto.getSaleOder().getTotal());
		tvSKU.setText("" + dto.getSKU());
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
