package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import android.database.Cursor;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;

public class GSNPPTrainingResultDayReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double amountMonth;
	public double amount;
	public int isNew;
	public int isOn;
	public int isOr;
	public double score;
	public double distance;
	public ArrayList<GSNPPTrainingResultReportDayItem> listResult = new ArrayList<GSNPPTrainingResultReportDayItem>();

	public enum VISIT_STATUS {
		NONE_VISIT, VISITED, VISITING
	}

	public class GSNPPTrainingResultReportDayItem implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public int stt;
		public String custCode;
		public int custId;
		public String custName;
		public String custAddr;
		public double amountMonth;
		public double amount;
		public double score;
		public int isNew;
		public int isOn;
		public int isOr;
		public double lat;
		public double lng;
		public int channelTypeId;
		public String startTime;
		public String endTime;
		public double tpdScore;
		public VISIT_STATUS visit = VISIT_STATUS.NONE_VISIT;
		public long visitActLogId;
		public boolean isHaveDisplayProgramNotYetVote;
		public ArrayList<String> displayProgramId;
		public ArrayList<String> displayProgramIdCaptured;

		public void initFromCursor(Cursor c, GSNPPTrainingResultDayReportDTO dto, int stt2) {
			stt = stt2;
			custCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			custId = CursorUtil.getInt(c, "CUSTOMER_ID");
			custName = CursorUtil.getString(c, "CUSTOMER_NAME");
			custAddr = CursorUtil.getString(c, "ADDRESS");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			isNew = CursorUtil.getInt(c, "IS_NEW");
			isOn = CursorUtil.getInt(c, "IS_ON");
			isOr = CursorUtil.getInt(c, "IS_OR");
			score = CursorUtil.getDouble(c, "SCORE");
			lat = CursorUtil.getDouble(c, "LAT");
			lng = CursorUtil.getDouble(c, "LNG");
			startTime = CursorUtil.getString(c, "AL_START_TIME");
			endTime = CursorUtil.getString(c, "AL_END_TIME");
			tpdScore = CursorUtil.getDouble(c, "TPD_SCORE");
			visitActLogId = CursorUtil.getLong(c, "AL_ID");
			if (!StringUtil.isNullOrEmpty(startTime)) {
				if (!StringUtil.isNullOrEmpty(endTime)) {
					visit = VISIT_STATUS.VISITED;
				} else {
					visit = VISIT_STATUS.VISITING;
				}
			}
			channelTypeId = CursorUtil.getInt(c, "CHANNEL_TYPE_ID");
			dto.listResult.add(this);
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.isNew += isNew;
			dto.isOr += isOr;
			dto.isOn += isOn;
			// dto.score += score;
			dto.score = tpdScore;

			String displayProgrameId = null;
			displayProgrameId = CursorUtil.getString(c, "DISPLAY_PROGRAM_ID");
			String displayProgrameIdCaptured = null;
			displayProgrameIdCaptured = CursorUtil.getString(c, "DISPLAY_PROGRAM_ID_CAPTURED");
			if (!StringUtil.isNullOrEmpty(displayProgrameId)) {
				displayProgramId = new ArrayList<String>();
				displayProgramId.addAll(Arrays.asList(displayProgrameId
						.split(",")));
			}
			if (!StringUtil.isNullOrEmpty(displayProgrameIdCaptured)) {
				displayProgramIdCaptured = new ArrayList<String>();
				displayProgramIdCaptured.addAll(Arrays.asList(displayProgrameIdCaptured
						.split(",")));
			}
			if (displayProgramId == null || displayProgramId.size() == 0 || dto.isOr == 1) {
				// khong co CTTB, khong chup hinh
				isHaveDisplayProgramNotYetVote = false;
			} else {
				if(displayProgramIdCaptured == null || displayProgramIdCaptured.size() < displayProgramId.size())
					// co CTTB, nhung chua chup du hinh TB
					isHaveDisplayProgramNotYetVote = true;
				else
					isHaveDisplayProgramNotYetVote = false;
			}
		}

	}

	public GSNPPTrainingResultReportDayItem newStaffTrainResultReportItem() {
		return new GSNPPTrainingResultReportDayItem();
	}
}
