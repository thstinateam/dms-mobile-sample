package com.ths.dmscore.view.sale.statistic;

import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.sale.customer.PostFeedbackView;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;
import com.ths.dms.R;

/**
 * CustomerListFragment
 *
 * @author : TamPQ since : 9:48:52 AM version :
 */
public class NoteListView extends BaseFragment implements
		OnEventControlListener, OnClickListener, VinamilkTableListener,
		OnItemSelectedListener, DMSColSortManager.OnSortChange {

	private GlobalBaseActivity parent;
	private static final String ALL = StringUtil.getString(R.string.TEXT_ALL);
	private static final String NOT_DONE = StringUtil
			.getString(R.string.TEXT_PROBLEM_CREATE_NEW);
	private static final String DONE = StringUtil
			.getString(R.string.TEXT_PROBLEM_HAS_DONE);
	private static final String GSNPP = StringUtil
			.getString(R.string.TEXT_PROBLEM_HAS_APPROVED);

	private static final int ACTION_OK = 0;
	private static final int ACTION_CANCEL = 1;
	private static final int ACTION_DELETE = 2;
	private static final int ACTION_CLOSE = 3;
	private static final int ACTION_DONE = 4;
	private static final int ACTION_ROW_CLICK = 5;
	// khach hang chua phat sinh doanh so
	private static final int ACTION_MENU_NEED_DONE = 6;
	// mat hang trong tam
	private static final int ACTION_MENU_MHTT = 7;
	// thong ke chung
	private static final int ACTION_MENU_GENERAL_STATISTICS = 8;
	private static final int ACTION_DONE_OK = 9;
	private static final int ACTION_DONE_CANCEL = 10;
	//CTTB
	private final int MENU_REPORT_DISPLAY_PROGRESS 	 = 11;

	Spinner spStatus;
	Spinner spTypeProblem;
	private DMSTableView tbNoteList;
	private CustomerFeedBackDto dto = new CustomerFeedBackDto();
	Vector<ApParamDTO> vTypeProblem;

	private String[] arrChoose = new String[] { ALL, NOT_DONE, DONE, GSNPP };
	private String[] arrChooseTypeProblem;
	private boolean isUpdateData = false;
	private int curSelection = -1;
	private int curSelectionTypeProblem = -1;
	// alert popup problem detail
	AlertDialog alertFollowProblemDetail;
	// popup problem detail
	SalePopupProblemDetailView salePopupProblemDetail;
	View currentRow;
	Button btAddNote;
	private CustomerDTO customer;

	public static NoteListView newInstance(Bundle data) {
		NoteListView f = new NoteListView();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		parent = (GlobalBaseActivity) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_note_list_fragment, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		int channelObjectType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
		if( channelObjectType == UserDTO.TYPE_STAFF)
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_CANTHUCHIEN);
		else if(channelObjectType == UserDTO.TYPE_SUPERVISOR)
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_CANTHUCHIEN);
		else{
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.TBHV_CANTHUCHIEN);
		}
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_NOTE_LIST));
		PriUtils.getInstance().findViewByIdGone(view, R.id.tvTypeProblem, PriHashMap.PriControl.CANTHUCHIEN_LOAIVANDE);
		PriUtils.getInstance().findViewByIdGone(view, R.id.tvStatus, PriHashMap.PriControl.CANTHUCHIEN_TRANGTHAI);
		spTypeProblem = (Spinner) PriUtils.getInstance().findViewByIdGone(view, R.id.spTypeProblem, PriHashMap.PriControl.CANTHUCHIEN_LOAIVANDE);
		spStatus = (Spinner) PriUtils.getInstance().findViewByIdGone(view, R.id.spStatus, PriHashMap.PriControl.CANTHUCHIEN_TRANGTHAI);
//		SpinnerAdapter adapterChoose = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrChoose);
		ArrayAdapter adapterChoose = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrChoose);
		adapterChoose.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		spStatus.setAdapter(adapterChoose);
		spStatus.setSelection(0);
		curSelection = spStatus.getSelectedItemPosition();
		PriUtils.getInstance().setOnItemSelectedListener(spStatus, this);
		tbNoteList = (DMSTableView) view.findViewById(R.id.tbNoteList);
		tbNoteList.setListener(this);
		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
						.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
						.addInfoCaseUnSensitive(4, SortActionConstants.TYPE)
						.addInfoCaseUnSensitive(5, SortActionConstants.FROM_DATE)
						.addInfoCaseUnSensitive(6, SortActionConstants.TO_DATE)
						.build();
//		 khoi tao header table
		initHeaderTable(tbNoteList, new NoteListRow(parent, this, 0, null), lstSort, this);
		getListTypeProblem();
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	/**
	 * Lay danh sach feedback can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:47:59 21-05-2015
	 * @return: void
	 * @throws:
	 * @param page
	 * @param isGetTotalPage
	 */
	private void getNoteList(int page, int isGetTotalPage) {
		if (!parent.isShowProgressDialog()) {
			parent.showLoadingDialog();
		}
		Bundle bundle = new Bundle();
		if (curSelection <= 0) {// tat ca
			bundle.putString(IntentConstants.INTENT_STATE, "");// tat ca
		} else {
			bundle.putString(IntentConstants.INTENT_STATE, getStatus(arrChoose[curSelection]));
		}
		if (curSelectionTypeProblem <= 0) {// tat ca
			bundle.putString(IntentConstants.INTENT_TYPE, "");
		} else {
			bundle.putString(IntentConstants.INTENT_TYPE,
					((ApParamDTO) vTypeProblem.elementAt(curSelectionTypeProblem - 1)).getApParamCode());
		}
		bundle.putString(IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, "");
		bundle.putString(IntentConstants.INTENT_DONE_DATE, "");
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);
		//add sort info
		bundle.putSerializable(IntentConstants.INTENT_SORT_DATA, tbNoteList.getSortInfo());
		handleViewEvent(bundle, ActionEventConstant.NOTE_LIST_VIEW, SaleController.getInstance());
	}

	/**
	 * Lay danh sach cac loai van de can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:33:37 21-05-2015
	 * @return: void
	 * @throws:
	 */
	private void getListTypeProblem() {
		if (!parent.isShowProgressDialog()) {
			parent.showLoadingDialog();
		}
		handleViewEvent(new Bundle(), ActionEventConstant.GET_LIST_TYPE_PROBLEM_NVBH_GSNPP, SaleController.getInstance());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		customer = (CustomerDTO) getArguments().getSerializable(
				IntentConstants.INTENT_CUSTOMER);
	}

	/**
	 * getStatus
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private String getStatus(String status) {
		String d = "";
		if (status.equals(NOT_DONE)) {
			d = "0";
		} else if (status.equals(DONE)) {
			d = "1";
		} else if (status.equals(GSNPP)) {
			d = "2";
		} else {
			d = ""; // tat ca
		}
		return d;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_LIST_TYPE_PROBLEM_NVBH_GSNPP:
			vTypeProblem = (Vector<ApParamDTO>) modelEvent.getModelData();
			if (vTypeProblem != null && vTypeProblem.size() > 0) {
				arrChooseTypeProblem = new String[vTypeProblem.size() + 1];
				arrChooseTypeProblem[0] = StringUtil.getString(R.string.TEXT_ALL);
				for (int i = 1, size = vTypeProblem.size() + 1; i < size; i++) {
					arrChooseTypeProblem[i] = ((ApParamDTO) vTypeProblem
							.elementAt(i - 1)).getApParamName();
				}
//				SpinnerAdapter adapterChoose = new SpinnerAdapter(parent,R.layout.simple_spinner_item, arrChooseTypeProblem);
                ArrayAdapter adapterChoose = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrChooseTypeProblem);
                adapterChoose.setDropDownViewResource(R.layout.simple_spinner_dropdown);
				spTypeProblem.setAdapter(adapterChoose);
				spTypeProblem.setSelection(0);
				curSelectionTypeProblem = spTypeProblem.getSelectedItemPosition();
				PriUtils.getInstance().setOnItemSelectedListener(spTypeProblem, this);
			}
			getNoteList(1, 1);
			break;
		case ActionEventConstant.NOTE_LIST_VIEW:
			parent.closeProgressDialog();
			CustomerFeedBackDto tempDto = (CustomerFeedBackDto) modelEvent
					.getModelData();
			if (isUpdateData) {
				isUpdateData = false;
				dto.currentPage = -1;
			}
			if (dto == null) {
				dto = tempDto;
			} else {
				dto.arrItem = tempDto.arrItem;
				if (tempDto.totalFeedBack >= 0) {
					dto.totalFeedBack = tempDto.totalFeedBack;
				}
			}
			renderLayout();
			break;
		case ActionEventConstant.UPDATE_FEEDBACK:
			parent.closeProgressDialog();
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_UPDATE_FEEDBACK_SUCC));
			if(dto.arrItem.size() > 1) {
				getNoteList(dto.currentPage, 0);
			} else {
				dto.currentPage = -1;
				getNoteList(1, 1);
			}
			break;
		case ActionEventConstant.DELETE_FEEDBACK:
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_DELETE_FEEDBACK_SUCC));
			dto.currentPage = -1;
			getNoteList(1, 1);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		parent.closeProgressDialog();
		switch (e.action) {
		case ActionEventConstant.DELETE_FEEDBACK:
			View v = (View) e.userData;
			v.setEnabled(true);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * renderLayout
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		if (dto.currentPage <= 0) {
			tbNoteList.setTotalSize(dto.totalFeedBack,1);
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();
		}else{
			tbNoteList.setTotalSize(dto.totalFeedBack,dto.currentPage);
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();
		}
		tbNoteList.clearAllData();
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbNoteList.getPagingControl().getCurrentPage() - 1);
		if (dto.arrItem != null && dto.arrItem.size() > 0) {
			for (int i = 0, s = dto.arrItem.size(); i < s; i++) {
				NoteListRow row = new NoteListRow(parent, this,
						ACTION_ROW_CLICK, dto.arrItem.get(i));
				PriUtils.getInstance().setOnClickListener(row.cbDone, this);
				row.cbDone.setTag(dto.arrItem.get(i));
				row.render(pos, dto.arrItem.get(i));
				pos++;
				tbNoteList.addRow(row);
			}
		} else {
			tbNoteList.showNoContentRow();
		}
	}

	/**
	 * updateFeedbackRow
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void updateFeedbackRow(FeedBackDTO item) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_DTO, item);
		e.sender = this;
		e.action = ActionEventConstant.UPDATE_FEEDBACK;
		e.viewData = bundle;
		SaleController.getInstance().handleViewEvent(e);
	}

	private void resetAllValue() {
		spStatus.setSelection(0);
		curSelection = spStatus.getSelectedItemPosition();
		if (dto != null) {
			dto.currentPage = -1;
		}
		getListTypeProblem();
	}

	/**
	 *
	 * display popup problem detail
	 *
	 * @param dto
	 * @return: void
	 * @throws:
	 * @author: HieuNH
	 * @date: Nov 7, 2012
	 */
	private void showFollowProblemDetail(FeedBackDTO dto) {
		if (alertFollowProblemDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			salePopupProblemDetail = new SalePopupProblemDetailView(parent,
					this, ACTION_CLOSE, ACTION_DELETE, ACTION_DONE);
			build.setView(salePopupProblemDetail.viewLayout);
			alertFollowProblemDetail = build.create();
			Window window = alertFollowProblemDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		salePopupProblemDetail.renderLayoutWithObject(dto);
		alertFollowProblemDetail.show();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.cbNote:
			CheckBox cb = (CheckBox) v;
			if (cb.isChecked()) {
				cb.setEnabled(false);
				FeedBackDTO item = (FeedBackDTO) cb.getTag();
				Vector<Object> vt;
				vt = new Vector<Object>();
				vt.add(item);
				vt.add(cb);
				dto.currentPage = tbNoteList.getPagingControl()
						.getCurrentPage();
				GlobalUtil
						.showDialogConfirmCanBackAndTouchOutSide(
								this,
								parent,
								StringUtil
										.getString(R.string.TEXT_CONFIRM_UPDATE_PROBLEM_DONE_2),
								StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
								ACTION_DONE_OK, StringUtil
										.getString(R.string.TEXT_BUTTON_CANCEL),
								ACTION_DONE_CANCEL, vt, false, false);

			}
			break;
		case R.id.btAddNote:
			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
			b.putInt(IntentConstants.INTENT_FROM,
					PostFeedbackView.FROM_NOTE_LIST_VIEW);
			ActionEvent e = new ActionEvent();
			e.action = ActionEventConstant.POST_FEEDBACK;
			e.sender = this;
			e.viewData = b;
			SaleController.getInstance().handleSwitchFragment(e);
			break;
		default:
			break;
		}
	}

	/**
	 * HieuNH go to man hinh khach hang chua phat sinh doanh so trong thang cua
	 * NVBH
	 */
	private void goToCustomerNotPSDSInMonthSaleView() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.ACTION_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH_SALE;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(int eventType, View control, Object data) {
		ActionEvent e = new ActionEvent();
		Vector<Object> vt;
		View v;
		FeedBackDTO item;
		switch (eventType) {
		case ActionEventConstant.GO_TO_CUSTOMER_LIST_DO_NOT_AMOUNT_IN_MONTH:
			goToCustomerNotPSDSInMonthSaleView();
			break;
		case MENU_REPORT_DISPLAY_PROGRESS:
			e.action = ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY;
			e.sender = this;
			e.viewData = new Bundle();
			UserController.getInstance().handleSwitchFragment(e);
			break;
		case ACTION_MENU_MHTT:
			e.action = ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW;
			e.viewData = new Bundle();
			e.sender = this;
			UserController.getInstance().handleSwitchFragment(e);
			break;
		case ACTION_MENU_NEED_DONE:
			e.action = ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW;
			e.viewData = new Bundle();
			e.sender = this;
			UserController.getInstance().handleSwitchFragment(e);
			break;
		case ACTION_MENU_GENERAL_STATISTICS:
			e.action = ActionEventConstant.GO_TO_GENERAL_STATISTICS;
			e.sender = this;
			e.viewData = new Bundle();
			UserController.getInstance().handleSwitchFragment(e);
			break;
		case ACTION_OK:
			vt = (Vector<Object>) data;
			item = (FeedBackDTO) vt.elementAt(0);
			v = (View) vt.elementAt(1);
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();

			item.setStatus(FeedBackDTO.FEEDBACK_STATUS_DELETE);
			item.setUpdateDate(DateUtils.now());
			item.setUserUpdate(GlobalInfo.getInstance().getProfile().getUserData().getUserName());
			parent.showLoadingDialog();
			e.sender = this;
			e.action = ActionEventConstant.DELETE_FEEDBACK;
			e.viewData = item;
			e.userData = v;
			SaleController.getInstance().handleViewEvent(e);
			break;
		case ACTION_CANCEL:
			vt = (Vector<Object>) data;
			v = (View) vt.elementAt(1);
			v.setEnabled(true);
			break;
		case ACTION_DELETE:
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();
			currentRow.setEnabled(false);
			vt = new Vector<Object>();
			vt.add(data);
			vt.add(currentRow);
			GlobalUtil
					.showDialogConfirmCanBackAndTouchOutSide(
							this,
							parent,
							StringUtil
									.getString(R.string.TEXT_CONFIRM_DELETE_PROBLEM_DONE),
							StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
							ACTION_OK,
							StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
							ACTION_CANCEL, vt, false, false);
			if (this.alertFollowProblemDetail.isShowing()) {
				this.alertFollowProblemDetail.dismiss();
			}
			break;
		case ACTION_CLOSE:
			if (this.alertFollowProblemDetail.isShowing()) {
				this.alertFollowProblemDetail.dismiss();
			}
			break;
		case ACTION_DONE:
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();
			currentRow.setEnabled(false);
			vt = new Vector<Object>();
			vt.add(data);
			vt.add(currentRow);

			GlobalUtil
					.showDialogConfirmCanBackAndTouchOutSide(
							this,
							parent,
							StringUtil
									.getString(R.string.TEXT_CONFIRM_UPDATE_PROBLEM_DONE_2),
							StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
							ACTION_DONE_OK, StringUtil
									.getString(R.string.TEXT_BUTTON_CANCEL),
									ACTION_CANCEL, vt, false, false);
			if (this.alertFollowProblemDetail.isShowing()) {
				this.alertFollowProblemDetail.dismiss();
			}
			break;
		case ACTION_DONE_OK: {
			vt = (Vector<Object>) data;
			item = (FeedBackDTO) vt.elementAt(0);
			item.getFeedbackStaffDTO().doneDate = DateUtils.now();
			item.getFeedbackStaffDTO().updateDate = DateUtils.now();
			item.getFeedbackStaffDTO().updateUser = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getUserCode());
			item.getFeedbackStaffDTO().result = FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE;
			updateFeedbackRow(item);
			break;
		}
		case ACTION_DONE_CANCEL: {
			vt = (Vector<Object>) data;
			CheckBox cb = (CheckBox) vt.elementAt(1);
			cb.setEnabled(true);
			cb.setChecked(false);
			this.renderLayout();
		}
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbNoteList) {
			dto.currentPage = tbNoteList.getPagingControl().getCurrentPage();
			getNoteList(dto.currentPage, 1);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		if(action == ACTION_ROW_CLICK){
			this.currentRow = control;
			showFollowProblemDetail((FeedBackDTO) data);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spStatus) {
			if (curSelection != spStatus.getSelectedItemPosition()) {
				curSelection = spStatus.getSelectedItemPosition();
				dto.currentPage = -1;
				getNoteList(1, 1);
			}
		} else if (arg0 == spTypeProblem) {
			if (curSelectionTypeProblem != spTypeProblem
					.getSelectedItemPosition()) {
				curSelectionTypeProblem = spTypeProblem
						.getSelectedItemPosition();
				dto.currentPage = -1;
				getNoteList(1, 1);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// cau request du lieu man hinh
				isUpdateData = true;
				tbNoteList.resetSortInfo();
				resetAllValue();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getNoteList(dto.currentPage, 1);
	}

}
