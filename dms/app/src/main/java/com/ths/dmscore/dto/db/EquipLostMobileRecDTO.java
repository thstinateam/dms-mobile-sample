/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.EQUIP_LOST_MOBILE_REC_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * DTO luu bien ban bao mat tren Mobile
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 19:30:21 26-12-2014
 */
public class EquipLostMobileRecDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	// ID bien ban bao mat tren mobile
	public long recordLostMobileRecId;
	// ma mobile bao mat
	public String coderecordLostMobileRec;
	// id thiet bi
	public long equipId;
	// id kho khach hang
	public long stockId;
	// ma kho
	public String stockCode;
	public String stockName;
	public String address;
	// trang thai phieu bao mat
	public int recordStatus;
	// id nguoi bao mat
	public int recordStaffId;
	// ngay mat
	public String dateLost;
	// ngay het phat sinh doanh so
	public String dateLastSales;
	//
	public int equipLostRecId;
	public String createUser;
	public String createDate;
	public String updateUser;
	public String updateDate;
	
	/**
	 * insert bao mat mobile
	 * @author: hoanpd1
	 * @since: 12:21:32 30-12-2014
	 * @return: JSONObject
	 * @throws:  
	 * @param item
	 * @return
	 */
	public JSONObject generateInsertEquipLostMobileRec(EquipLostMobileRecDTO item) {
		JSONObject jsonInsert = new JSONObject();
		try {
			// Insert
			jsonInsert.put(IntentConstants.INTENT_TYPE, TableAction.INSERT); 
			jsonInsert.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_LOST_MOBILE_REC_TABLE.TABLE_NAME);
			// ds params
			JSONArray jsonDetail = new JSONArray();
			// Khi insert thi insert cac truong sau
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.EQUIP_LOST_MOBILE_REC_ID, item.recordLostMobileRecId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.CODE, item.coderecordLostMobileRec, null));  
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.EQUIP_ID, item.equipId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.STOCK_ID, item.stockId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.STOCK_CODE, item.stockCode, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.RECORD_STATUS, item.recordStatus, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.REPORT_STAFF_ID, item.recordStaffId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.LAST_ARISING_SALES_DATE, item.dateLastSales, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.LOST_DATE, item.dateLost, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.EQUIP_LOST_REC_ID, item.equipLostRecId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.CREATE_USER, item.createUser, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.CREATE_DATE, item.createDate, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.STOCK_NAME, item.stockName, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_LOST_MOBILE_REC_TABLE.ADDRESS, item.address, null));

			jsonInsert.put(IntentConstants.INTENT_LIST_PARAM, jsonDetail);

		} catch (JSONException e) {
			MyLog.e("UnexceptionLog",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return jsonInsert;
	}
}
