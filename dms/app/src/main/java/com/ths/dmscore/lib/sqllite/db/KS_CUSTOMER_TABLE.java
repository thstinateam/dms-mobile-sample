package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;

/**
 * Table dang ki keyshop cua kh
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KS_CUSTOMER_TABLE extends ABSTRACT_TABLE {
	// id khach hang
	public static final String KS_CUSTOMER_ID = "KS_CUSTOMER_ID";
	public static final String KS_ID = "KS_ID";
	public static final String CYCLE_ID = "CYCLE_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String AMOUNT_DISCOUNT = "AMOUNT_DISCOUNT";
	public static final String OVER_AMOUNT_DISCOUNT = "OVER_AMOUNT_DISCOUNT";
	public static final String DISPLAY_MONEY = "DISPLAY_MONEY";
	public static final String SUPPORT_MONEY = "SUPPORT_MONEY";
	public static final String ODD_DISCOUNT = "ODD_DISCOUNT";
	public static final String ADD_MONEY = "ADD_MONEY";
	public static final String TOTAL_REWARD_MONEY = "TOTAL_REWARD_MONEY";
	public static final String TOTAL_REWARD_MONEY_DONE = "TOTAL_REWARD_MONEY_DONE";
	public static final String STATUS = "STATUS";
//	public static final String APPROVAL = "APPROVAL";
	public static final String REWARD_TYPE = "REWARD_TYPE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	public static final String TABLE_NAME = "KS_CUSTOMER";

	public KS_CUSTOMER_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { ADD_MONEY, AMOUNT_DISCOUNT,
				CREATE_DATE, CREATE_USER, CUSTOMER_ID, CYCLE_ID, DISPLAY_MONEY,
				KS_CUSTOMER_ID, KS_ID, ODD_DISCOUNT, OVER_AMOUNT_DISCOUNT,
				REWARD_TYPE, SHOP_ID, STATUS, SUPPORT_MONEY,
				TOTAL_REWARD_MONEY, TOTAL_REWARD_MONEY_DONE, UPDATE_DATE,
				UPDATE_USER,CREATE_USER_ID,SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public long insert(RegisterKeyShopItemDTO dto) {
		ContentValues value = initDataRowInsert(dto);
		return insert(null, value);
	}

	public long update(RegisterKeyShopItemDTO dto) {
		ContentValues value = initDataRowUpdate(dto);
		String[] params = { "" + dto.ksCustomerItem.ksCustomerId };
		return update(value, KS_CUSTOMER_ID + " = ?", params);
	}

	private ContentValues initDataRowInsert(RegisterKeyShopItemDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(KS_CUSTOMER_ID, dto.ksCustomerItem.ksCustomerId);
		editedValues.put(KS_ID, dto.ksCustomerItem.ksId);
		editedValues.put(CYCLE_ID, dto.ksCustomerItem.cycleId);
		editedValues.put(SHOP_ID, dto.ksCustomerItem.shopId);
		editedValues.put(CUSTOMER_ID, dto.ksCustomerItem.customerId);
		editedValues.put(STATUS, KSCustomerDTO.STATE_NEW);
//		editedValues.put(APPROVAL, KSCustomerDTO.STATE_NEW);
		editedValues.put(CREATE_USER_ID, dto.ksCustomerItem.createUserId);
		editedValues.put(CREATE_DATE, dto.ksCustomerItem.createDate);
		editedValues.put(CREATE_USER, dto.ksCustomerItem.createUser);
		return editedValues;
	}

	private ContentValues initDataRowUpdate(RegisterKeyShopItemDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, dto.ksCustomerItem.status);
		editedValues.put(UPDATE_DATE, dto.ksCustomerItem.updateDate);
		editedValues.put(UPDATE_USER, dto.ksCustomerItem.updateUser);
		return editedValues;
	}

	public long update(KSCustomerDTO dto) {
		ContentValues value = initDataRowUpdate(dto);
		String[] params = { "" + dto.ksCustomerId };
		return update(value, KS_CUSTOMER_ID + " = ?", params);
	}

	private ContentValues initDataRowUpdate(KSCustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(TOTAL_REWARD_MONEY_DONE, dto.totalRewardMoneyDone);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(UPDATE_USER, dto.updateUser);
		return editedValues;
	}

}
