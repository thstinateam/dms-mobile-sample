package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class TotalStatisticsItem {
	public String productId;
	public String productCode;
	public String productName;
	public String productPromotion;
	public String quantity;
	public String available_quantity;

	public TotalStatisticsItem() {
	}

	public void initDataFromCursor(Cursor c) {
		try {
			if (c == null) {
				throw new Exception("Cursor is empty");
			}
			productCode = CursorUtil.getString(c, "PRODUCT_CODE");
			productName = CursorUtil.getString(c, "PRODUCT_NAME");
			quantity = CursorUtil.getString(c, "QUANTITY", "0", "0");
			available_quantity = CursorUtil.getString(c, "AVAILABLE_QUANTITY", "0", "0");
		} catch (Exception e) {
		}
	}
}
