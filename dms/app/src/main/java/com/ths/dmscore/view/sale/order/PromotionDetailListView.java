/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Popup ds CTKM man hinh them hang
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PromotionDetailListView extends ScrollView implements OnClickListener {

	private DMSTableView tbPromotionList;
	private GlobalBaseActivity parent;
	public View viewLayout;
	// listtener
	protected BaseFragment listener;
	private View tvTittle;


	public PromotionDetailListView(Context context, BaseFragment listener) {
		super(context);
		parent = (GlobalBaseActivity) context;
		this.listener = listener;
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_promotion_detail_view, null);
		tbPromotionList = (DMSTableView) viewLayout.findViewById(R.id.tbPromotionList);
		tbPromotionList.addHeader(new PromotionDetailListRow(parent));
		tvTittle = (TextView) viewLayout.findViewById(R.id.tvTittle);
		tvTittle.setOnClickListener(this);
	}

	 /**
	 * render layout ds CTKM
	 * @author: Tuanlt11
	 * @param lstPromotionPrograme
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(ArrayList<PromotionProgrameDTO> lstPromotionPrograme){
		tbPromotionList.clearAllData();
		for(int i = 0, size = lstPromotionPrograme.size(); i < size; i ++){
			PromotionDetailListRow row = new PromotionDetailListRow(parent);
			row.renderLayout(i+1, lstPromotionPrograme.get(i));
			tbPromotionList.addRow(row);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		listener.onEvent(ActionEventConstant.ACTION_CLOSE_POPUP, this, null);
	}
}
