package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

public class ListStaffDTO implements Serializable {
	private static final long serialVersionUID = -3272126982422119739L;
	public int totalList;
	public String totalofTotal;
	public String listStaff;

	public ArrayList<StaffItem> arrList;

	public ListStaffDTO() {
		arrList = new ArrayList<ListStaffDTO.StaffItem>();
		listStaff = "";
	}

	public void addItemAll() {
		arrList.add(0, new StaffItem("", StringUtil.getString(R.string.TEXT_ALL), ""));
	}
		
	public void addItem(Cursor c) {
		StaffItem item = new StaffItem();
//		if (c.getColumnIndex("STAFF_ID") > -1) {
//			item.id = c.getString(c.getColumnIndex("STAFF_ID"));
//		}
//		if (c.getColumnIndex("STAFF_NAME") > -1) {
//			item.name = c.getString(c.getColumnIndex("STAFF_NAME"));
//		}
//		if (c.getColumnIndex("STAFF_CODE") > -1) {
//			item.code = c.getString(c.getColumnIndex("STAFF_CODE"));
//		}
//		if (c.getColumnIndex("LAT") > -1) {
//			item.lat = c.getDouble(c.getColumnIndex("LAT"));
//		}
//		if (c.getColumnIndex("LNG") > -1) {
//			item.lng = c.getDouble(c.getColumnIndex("LNG"));
//		}
//		if (c.getColumnIndex("NVBH_STAFF_ID") > -1) {
//			item.nvbhId = c.getString(c.getColumnIndex("NVBH_STAFF_ID"));
//		}
//		if (c.getColumnIndex("NVBH_SHOP_CODE") > -1) {
//			item.nvbhShopCode = c.getString(c.getColumnIndex("NVBH_SHOP_CODE"));
//		}
//		if (c.getColumnIndex("NVBH_SHOP_ID") > -1) {
//			item.nvbhShopId = c.getString(c.getColumnIndex("NVBH_SHOP_ID"));
//		}
		item.id = CursorUtil.getString(c, "STAFF_ID");
		item.name = CursorUtil.getString(c, "STAFF_NAME");
		item.code = CursorUtil.getString(c, "STAFF_CODE");
		item.lat = CursorUtil.getDouble(c, "LAT");
		item.lng = CursorUtil.getDouble(c, "LNG");
		item.nvbhId = CursorUtil.getString(c, "NVBH_STAFF_ID");
		item.nvbhShopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
		item.nvbhShopId = CursorUtil.getString(c, "NVBH_SHOP_ID");
		arrList.add(item);
	}
	
	
	
	public void parrseStaffFromCursor(Cursor c) {
		StaffItem item = new StaffItem();
//		if (c.getColumnIndex("STAFF_ID") > -1) {
//			item.id = c.getString(c.getColumnIndex("STAFF_ID"));
//		}
//		if (c.getColumnIndex("STAFF_NAME") > -1) {
//			item.name = c.getString(c.getColumnIndex("STAFF_NAME"));
//		}
//		if (c.getColumnIndex("STAFF_CODE") > -1) {
//			item.code = c.getString(c.getColumnIndex("STAFF_CODE"));
//		}
//		if (c.getColumnIndex("NVBH_SHOP_CODE") > -1) {
//			item.nvbhShopCode = c.getString(c.getColumnIndex("NVBH_SHOP_CODE"));
//		}
		item.id = CursorUtil.getString(c, "STAFF_ID");
		item.name = CursorUtil.getString(c, "STAFF_NAME");
		item.code = CursorUtil.getString(c, "STAFF_CODE");
		item.nvbhShopCode = CursorUtil.getString(c, "NVBH_SHOP_CODE");
		arrList.add(item);
	}
	
	
	
	
	

	public StaffItem newStaffItem() {
		return new StaffItem();
	}

	public class StaffItem implements Serializable {
		private static final long serialVersionUID = -4391030998624085453L;
		public String id;
		public String name;
		public String code;
		public double lat;
		public double lng;
		public String nvbhId;
		public String nvbhShopCode = "";
		public String nvbhShopId;

		public StaffItem() {
			id = "3/7";
			name = "0/0";
		}

		public StaffItem(String id, String name, String code) {
			this.id = id;
			this.name = name;
			this.code = code;
		}
	}
}
