/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.SaleProductInfoDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

/**
 * hien thi 1 row trong man hinh thong ke don tong trong ngay cua VanSales
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SaleStatisticsInDayVanSalesRow extends DMSTableRow implements
		OnClickListener {
	// stt
	TextView tvSTT;
	// pma mat hang
	TextView tvProductCode;
	// Ten mat hang
	TextView tvProductName;
	// nganh hang
	TextView tvSectorsProduct;
	// Quy cach
	TextView tvConvfact;
	// number ton kho dau ngay
	TextView tvEarlyDay;
	// number da ban
	TextView tvSolded;
	// number con lai
	TextView tvRemain;
	// number ton kho dau ngay quy doi
	TextView tvEarlyDayConvert;
	// number da ban quy doi
	TextView tvSoldedConvert;
	// number con lai quy doi
	TextView tvRemainConvert;
	// total amount quy doi
	TextView tvTotalAmount;
	// data to render layout for row
	FindProductSaleOrderDetailViewDTO myData;

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 */
	public SaleStatisticsInDayVanSalesRow(Context context,
			SaleStatisticsInDayVanSalesView lis) {
		super(context, R.layout.layout_sale_statistics_in_day_van_sales_row,
				GlobalInfo.getInstance().isSysShowPrice() ? 1.3 : 1.2,
				GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {R.id.tvTotalAmount});
		setListener(lis);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvSectorsProduct = (TextView) findViewById(R.id.tvSectorsProduct);
		tvConvfact = (TextView) findViewById(R.id.tvConvfact);
		tvEarlyDay = (TextView) findViewById(R.id.tvEarlyDay);
		tvSolded = (TextView) findViewById(R.id.tvSolded);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
		tvEarlyDayConvert = (TextView) findViewById(R.id.tvEarlyDayConvert );
		tvSoldedConvert  = (TextView) findViewById(R.id.tvSoldedConvert );
		tvRemainConvert  = (TextView) findViewById(R.id.tvRemainConvert );
		tvTotalAmount = (TextView) findViewById(R.id.tvTotalAmount);
	}

	/**
	 * init layout for row
	 * @author: hoanpd1
	 * @since: 10:11:34 09-04-2015
	 * @return: void
	 * @throws:
	 * @param position
	 * @param item
	 * @param isRowSum
	 */
	public void renderLayout(int position, SaleProductInfoDTO item, boolean isRowSum) {
		if (isRowSum) {
			showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT, tvProductCode,tvProductName,tvSectorsProduct, tvConvfact);
			display(tvEarlyDay, Constants.STR_BLANK);
			display(tvSolded, Constants.STR_BLANK);
			display(tvRemain, Constants.STR_BLANK);

			display(tvEarlyDayConvert, item.numTotalProductRemainFirstDay);
			display(tvSoldedConvert, item.numberTotalProduct);
			display(tvRemainConvert, item.numTotalProductRemain);
			display(tvTotalAmount,item.sumTotalAmountSold);
		} else {
			display(tvSTT, position);
			display(tvProductCode, item.productInfo.productCode);
			display(tvProductName, item.productInfo.productName);
			display(tvSectorsProduct, item.productInfo.categoryCode);
			display(tvConvfact, item.productInfo.convfact);

			display(tvEarlyDay, item.numProductRemainFirstDayFormat);
			display(tvSolded, item.numberProductFormat);
			display(tvRemain, item.numProductRemainFormat);

			display(tvEarlyDayConvert, item.numProductRemainFirstDay);
			display(tvSoldedConvert, item.numberProduct);
			display(tvRemainConvert, item.numProductRemain);
			display(tvTotalAmount,item.totalAmountSold);
		}
	}

	@Override
	public void onClick(View v) {

	}
}
