package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.StaffDTO;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.STAFF_POSITION_LOG_TABLE;

public class GsnppRouteSupervisionItem implements Serializable {
	private static final long serialVersionUID = 3956267061401849473L;
	public StaffDTO aStaff = new StaffDTO();
	// co nv con hay ko?
	public boolean haveStaffChild;
	public int numTotalCus;
	public int numVisited;
	public int numRightPlan;
	public int numWrongPlan;
	public int numLessThan2Min;
	public int numOnTime;
	public int numMoreThan30Min;
	public double lat;
	public double lng;
	public String updateTime;
	public String visitingCusName;
	public String lessThan2MinList;
	public String trainingDate;
	public String moreThan30MinList;
	public String visitTime;

	public GsnppRouteSupervisionItem() {
	}

	public void initDataFromCursor(Cursor c) {
		if (c == null) {
			return;
		}
		aStaff.staffId = CursorUtil.getInt(c, "STAFF_ID");
		aStaff.staffCode = CursorUtil.getString(c, "STAFF_CODE");
		aStaff.name = CursorUtil.getString(c, "NAME");
		aStaff.shopId = CursorUtil.getInt(c, "SHOP_ID");
		aStaff.mobile = CursorUtil.getString(c, "MOBILE");
		lat = CursorUtil.getDouble(c, "LAT");
		lng = CursorUtil.getDouble(c, "LNG");
		numTotalCus = CursorUtil.getInt(c, "NUM_TOTAL_CUS");
		numVisited = CursorUtil.getInt(c, "TOTAL_VISITED");
		numRightPlan = CursorUtil.getInt(c, "NUM_RIGHT_PLAN");
		numWrongPlan = CursorUtil.getInt(c, "NUM_WRONG_PLAN");
		numLessThan2Min = CursorUtil.getInt(c, "NUM_LESTTHAN_2MIN");
		numMoreThan30Min = CursorUtil.getInt(c, "NUM_MORETHAN_30MIN");
		numOnTime = CursorUtil.getInt(c, "NUM_ON_TIME");
		lessThan2MinList = CursorUtil.getString(c, "LESSTHAN_2_MIN_LIST");
		moreThan30MinList = CursorUtil.getString(c, "MORETHAN_30_MIN_LIST");
		int temp = CursorUtil.getInt(c, "VISIT_TIME");
		visitTime = String.valueOf(temp / 60);
	}

	/**
	 * Lay thong tin ds vi tri NVBH trong 2 ngay
	 *
	 * @author: TruongHN
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initListPostSalePersons(Cursor c) {
		try {
			if (c == null) {
				throw new Exception("Cursor is empty");
			}
			aStaff.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
			aStaff.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
			aStaff.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
			aStaff.shopId = CursorUtil.getInt(c, STAFF_TABLE.SHOP_ID);
			aStaff.mobile = CursorUtil.getString(c, STAFF_TABLE.MOBILE_PHONE);
			aStaff.phone = CursorUtil.getString(c, STAFF_TABLE.PHONE);

			if (StringUtil.isNullOrEmpty(aStaff.mobile)) {
				aStaff.mobile = aStaff.phone;
			}
			if (StringUtil.isNullOrEmpty(aStaff.mobile)) {
				aStaff.mobile = "";
			}
			lat = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LAT);
			lng = CursorUtil.getDouble(c, STAFF_POSITION_LOG_TABLE.LNG);
			updateTime = CursorUtil.getString(c, "DATE_TIME");

			String start_time = CursorUtil.getString(c, "START_TIME");
			String end_time = CursorUtil.getString(c, "END_TIME");
			if (!StringUtil.isNullOrEmpty(start_time)
					&& StringUtil.isNullOrEmpty(end_time)) {
				visitingCusName = CursorUtil.getString(c, "CUS_NAME");
			}
			trainingDate = CursorUtil.getString(c, "TRAINING_DATE");

		} catch (Exception e) {
		}
	}
	
}
