/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.AreaDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;

/**
 *  Thong tin dia ban
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class AREA_TABLE extends ABSTRACT_TABLE {
	// ma dia ban
	public static final String AREA_CODE = "AREA_CODE";
	// ten dia ban
	public static final String AREA_NAME = "AREA_NAME";
	// loai dia ban
	public static final String TYPE = "TYPE";
	// ma dia ban cha
	public static final String PARENT_CODE = "PARENT_CODE";
	// ma tinh
	public static final String PROVINCE = "PROVINCE";
	// ten tinh
	public static final String PROVINCE_NAME = "PROVINCE_NAME";
	// ma huyen
	public static final String DISTRICT = "DISTRICT";
	// ten huyen
	public static final String DISTRICT_NAME = "DISTRICT_NAME";
	// ma xa
	public static final String PRECINCT = "PRECINCT";
	// ten xa
	public static final String PRECINCT_NAME = "PRECINCT_NAME"; 
	// trang thai 0: het hieu luc, 1: hieu luc
	public static final String STATUS = "STATUS"; 
	// ma vung
	public static final String CENTER_CODE = "CENTER_CODE";
	// thoi gian update
	public static final String UPDATE_TIME = "UPDATE_TIME";
	// thoi gian tao
	public static final String CREATE_TIME = "CREATE_TIME";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER"; 
	
	private static final String TABLE_AREA = "AREA";
	
	public AREA_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_AREA;
		this.columns = new String[] {AREA_CODE, AREA_NAME ,TYPE,PARENT_CODE,PROVINCE,PROVINCE_NAME,
				DISTRICT, DISTRICT_NAME,PRECINCT, PRECINCT_NAME ,STATUS, CENTER_CODE, UPDATE_TIME,
				CREATE_TIME,UPDATE_USER,CREATE_USER,SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((AreaDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(AreaDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}
	
	/**
	 * Update 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		AreaDTO area = (AreaDTO)dto;
		ContentValues value = initDataRow(area);
		String[] params = { "" + area.areaCode };
		return update(value, AREA_CODE + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(AREA_CODE + " = ?", params);
	}
	
	public long delete(AbstractTableDTO dto) {
		AreaDTO area = (AreaDTO)dto;
		String[] params = { "" + area.areaCode };
		return delete(AREA_CODE + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public AreaDTO getRowById(String id) {
		AreaDTO dto = null;
		Cursor c = null;
		try {
			String[]params = {id};
			c = query(
					AREA_CODE + " = ?" , params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initAreaDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private AreaDTO initAreaDTOFromCursor(Cursor c) {
		AreaDTO areDTO = new AreaDTO();
		areDTO.areaCode = (CursorUtil.getString(c, AREA_CODE));
		areDTO.areaName = (CursorUtil.getString(c, AREA_NAME));
		areDTO.centerCode = (CursorUtil.getString(c, CENTER_CODE));
		areDTO.createTime = (CursorUtil.getString(c, CREATE_TIME));
		areDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		areDTO.district = (CursorUtil.getString(c, DISTRICT));
		areDTO.districtName = (CursorUtil.getString(c, DISTRICT_NAME));
		areDTO.parentCode = (CursorUtil.getString(c, PARENT_CODE));
		areDTO.precinct = (CursorUtil.getString(c, PRECINCT));
		areDTO.precinctName = (CursorUtil.getString(c, PRECINCT_NAME));
		areDTO.province = (CursorUtil.getString(c, PROVINCE));
		areDTO.provinceName = (CursorUtil.getString(c, PROVINCE_NAME));
		
		areDTO.status = (CursorUtil.getInt(c, STATUS));
		areDTO.type = (CursorUtil.getString(c, TYPE));
		areDTO.updateTime = (CursorUtil.getString(c, UPDATE_TIME));
		areDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		
		return areDTO;
	}

	/**
	 * 
	 * lay tat ca cac dong cua CSDL
	 * @author: TruongHN
	 * @return: Vector<AreaDTO>
	 * @throws:
	 */
	public Vector<AreaDTO> getAllRow() {
		Vector<AreaDTO> v = new Vector<AreaDTO>();
		Cursor c = null;
		try {
			c = query(null,
					null, null, null, null);
			if (c != null) {
				AreaDTO area;
				if (c.moveToFirst()) {
					do {
						area = initAreaDTOFromCursor(c);
						v.addElement(area);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(AreaDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(AREA_CODE, dto.areaCode);
		editedValues.put(AREA_NAME, dto.areaName);
		editedValues.put(TYPE, dto.type);
		editedValues.put(PARENT_CODE, dto.parentCode);
		editedValues.put(PROVINCE , dto.province);
		editedValues.put(PROVINCE_NAME , dto.provinceName);
		editedValues.put(DISTRICT , dto.district);
		editedValues.put(DISTRICT_NAME , dto.districtName);
		editedValues.put(PRECINCT  , dto.precinct);
		editedValues.put(PRECINCT_NAME  , dto.precinctName);
		editedValues.put(STATUS , dto.status);
		editedValues.put(CENTER_CODE  , dto.centerCode);
		editedValues.put(UPDATE_TIME  , dto.updateTime);
		editedValues.put(CREATE_TIME  , dto.createTime);
		editedValues.put(UPDATE_USER  , dto.updateUser);
		editedValues.put(CREATE_USER   , dto.createUser);
		

		return editedValues;
	}
}
