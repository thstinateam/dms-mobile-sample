/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.AP_PARAM_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Thong tin cau hinh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class ApParamDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -7899858695746027312L;
	// id
	private long apParamId;
	// ma tham so cau hinh
	private String apParamCode;
	// gia tri tham so cau hinh
	private String value;
	// trang thai 0: het hieu luc, 1: dang hieu luc
	private int status;
	// ghi chu
	private String description;
	// name
	private String apParamName;
	// TYPE
	private String type;

	public static final String PRIORITY_NOW = "A";
	public static final String PRIORITY_IN_DAY = "B";
	public static final String PRIORITY_OUT_DAY = "C";

	// loai chu ki cho bang report
	public static final int PERIOD_YEAR = 400006;
	public static final int PERIOD_QUARTER = 400005;
	public static final int PERIOD_MONTH = 400004;
	public static final int PERIOD_WEEK = 400003;
	public static final int PERIOD_DAY = 400002;

	// ket qua tong hop cho bang RPT_SALE_IN_MONTH
	// tong hop don da duyet
	public static final int SYS_CAL_APPROVED = 1;
	  // tong hop don chua duyet
	public static final int SYS_CAL_UNAPPROVED_PENDING = 3;
	  // tong hop ca hai
	public static final int SYS_CAL_UNAPPROVED = 2;
	//tong hop theo route hay nhan vien hoac shop
	public static final int SYS_SALE_ROUTE = 2;
	public static final int SYS_SALE_STAFF = 1;
	public static final int SYS_SALE_SHOP = 3;
	
	//Cau hinh lay theo order_date hay approved_date
	public static final int SYS_CAL_ORDER_DATE = 1;
	public static final int SYS_CAL_APPROVED_DATE = 2;

	public ApParamDTO() {
		super(TableType.AP_PARAM_TABLE);
	}

	/**
	 * Init lay danh sach cac loai van de can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:33:37 21-05-2015
	 * @return: void
	 * @throws:
	 */
	public void initObjectWithCursor(Cursor c) {
		setApParamId(CursorUtil.getLong(c, AP_PARAM_TABLE.AP_PARAM_ID));
		setApParamCode(CursorUtil.getString(c, AP_PARAM_TABLE.AP_PARAM_CODE));
		setValue(CursorUtil.getString(c, AP_PARAM_TABLE.VALUE));
		setApParamType(CursorUtil.getString(c, AP_PARAM_TABLE.TYPE));
		setStatus(CursorUtil.getInt(c, AP_PARAM_TABLE.STATUS));
		setDescription(CursorUtil.getString(c, AP_PARAM_TABLE.DESCRIPTION));
		setApParamName(CursorUtil.getString(c, AP_PARAM_TABLE.AP_PARAM_NAME));
	}
	
	@Override
	public String toString() {
		return "apParamCode: " + getApParamCode() + " apParamName: " + getApParamName() + " type: " + getApParamType() + " value: " + getValue() + " status: " + getStatus();
	}

	public long getApParamId() {
		return apParamId;
	}

	public void setApParamId(long apParamId) {
		this.apParamId = apParamId;
	}

	public String getApParamCode() {
		return apParamCode;
	}

	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApParamName() {
		return apParamName;
	}

	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}

	public String getApParamType() {
		return type;
	}

	public void setApParamType(String type) {
		this.type = type;
	}
}
