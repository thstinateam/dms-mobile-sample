/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import android.content.Intent;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.controller.UserController;
import com.ths.dms.R;

/**
 * GCMCheckRoleActivity.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 10:14:50 AM Feb 10, 2014
 */
public class GCMCheckRoleActivity extends GlobalBaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_main);
		int loginState = GlobalInfo.getInstance().getProfile().getUserData().getLoginState();
		Intent resultIntent = null;
		// da dang nhap
		UserDTO userDTO = null;
		if (loginState != 0) {
			if (userDTO == null) {
				userDTO = GlobalInfo.getInstance().getProfile().getUserData();
			}
			if (userDTO != null) {

				Bundle b = new Bundle();
				b.putString(IntentConstants.INTENT_TIME, userDTO.getServerDate());
				ActionEvent e = new ActionEvent();
				e.sender = this;
				e.viewData = b;
				if (userDTO.getInheritSpecificType() == 5) {// NVGS
					e.action = ActionEventConstant.GO_TO_SUPERVISOR_VIEW;
				} else if (userDTO.getInheritSpecificType() == 7) {// TBHV
					e.action = ActionEventConstant.GO_TO_TBHV_VIEW;
				} else if (userDTO.getInheritSpecificType() == 1) {// NVBH
					e.action = ActionEventConstant.GO_TO_SALE_PERSON_VIEW;
				}
				// e.action = ActionEventConstant.GO_TO_SALE_PERSON_VIEW;
				UserController.getInstance().handleSwitchActivity(e);
			}
		} else {
			resultIntent = new Intent(getApplicationContext(), LoginView.class);
			startActivity(resultIntent);
		}
	}
}
