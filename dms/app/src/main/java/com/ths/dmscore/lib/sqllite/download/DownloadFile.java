package com.ths.dmscore.lib.sqllite.download;

/**
 * ZipDownloader
 * 
 * A simple app to demonstrate downloading and unpacking a .zip file
 * as a background task.
 * 
 * Copyright (c) 2011 Michael J. Portuesi (http://www.jotabout.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.network.http.NetworkTimeout;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.lib.network.http.HTTPClient;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * Utility methods for downloading files.
 * 
 * @author portuesi
 *
 */
public class DownloadFile {
	
    public static final int BUFFER_SIZE = 8 * 1024;
    
    static long fileSize = 0;
    
	/**
	 * Download a file from a URL somewhere.  The download is atomic;
	 * that is, it downloads to a temporary file, then renames it to
	 * the requested file name only if the download successfully
	 * completes.
	 * 
	 * Returns TRUE if download succeeds, FALSE otherwise.
	 * 
	 * @param url				Source URL
	 * @param output			Path to output file
	 * @param tmpDir			Place to put file download in progress
	 */
	public static void download( String url, File output, File tmpDir ) {
		InputStream is = null;
		OutputStream os = null;
		File tmp = null;
		try {
			MyLog.d("DownloadDB", "Downloading url :" + url);
			tmp = File.createTempFile( "download", ".tmp", tmpDir );
			is = new URL(url).openStream();
			os = new BufferedOutputStream( new FileOutputStream( tmp ) );
			copyStream( is, os );
			tmp.renameTo( output );
			tmp = null;
		} catch ( IOException e ) {		
			MyLog.e("DownloadDB", "Loi download file db ", e);
			throw new RuntimeException( e );
		} catch (Exception e) {
			MyLog.e("download", "fail", e);
			throw new RuntimeException( e );
		}finally {
			if ( tmp != null ) { try { tmp.delete(); tmp = null; } catch (Exception ignore) {;} }
			if ( is != null  ) { try { is.close();   is = null;  } catch (Exception ignore) {;} }
			if ( os != null  ) { try { os.close();   os = null;  } catch (Exception ignore) {;} }
		}
	}
	
	public static class DMSDownloadException extends Exception{
		private static final long serialVersionUID = 1L;
		public DMSDownloadException(String detailMessage, Throwable throwable) {
			super(detailMessage, throwable);
		}
	}
	
	/**
	 * Download file with urlConnection
	 * @author : BangHN
	 * since : 1.0
	 * @throws Exception 
	 */
	public static void downloadWithURLConnection(String url, File output, File tmpDir) throws DMSDownloadException, Exception {
		BufferedOutputStream os = null;
		BufferedInputStream is = null;
		File tmp = null;
		try {
			MyLog.d("downloadWithURLConnection", "Downloading url :" + url);
			tmp = File.createTempFile( "download", ".tmp", tmpDir );
			URL urlDownload = new URL(url);
			URLConnection cn = urlDownload.openConnection();
			cn.addRequestProperty("session", HTTPClient.getSessionID());
			cn.setConnectTimeout(NetworkTimeout.getCONNECT_TIME_OUT_DOWNLOAD_FILE());
			cn.setReadTimeout(NetworkTimeout.getREAD_TIME_OUT_DOWNLOAD_FILE());
			cn.connect();
			fileSize = cn.getContentLength();
			if (fileSize < 0) {
				//default 4MB
				fileSize = 4 * 1024 * 1024;
			}
			MyLog.d("downloadWithURLConnection", "fileSize  :" + fileSize);
			is = new BufferedInputStream(cn.getInputStream());
			os = new BufferedOutputStream( new FileOutputStream( tmp ) );
			copyStream( is, os );
			tmp.renameTo( output );
			tmp = null;
			MyLog.d("downloadWithURLConnection", "Downloaded pass url :" + DateUtils.now());
		} catch (MalformedURLException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e);
		} catch (FileNotFoundException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_RESOURCE_IN_SERVER), e);
		} catch (SocketTimeoutException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_TIME_OUT_CONNECT_SERVER), e);
		} catch (UnknownHostException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e);
		} catch (ConnectException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECTION), e);
		} catch (IOException e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e);
		} catch (Exception e) {
			MyLog.e("downloadWithURLConnection", DateUtils.now() + " Exception : " + e.getMessage());
			throw new DMSDownloadException(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON), e);
		} finally {
			MyLog.d("downloadWithURLConnection", "Downloaded url :" + DateUtils.now());
			if ( tmp != null ) { try { tmp.delete(); tmp = null; } catch (Exception ignore) {;} }
			if ( is != null  ) { try { is.close();   is = null;  } catch (Exception ignore) {;} }
			if ( os != null  ) { try { os.close();   os = null;  } catch (Exception ignore) {;} }
		}
	}
	
	
	/**
	 * Download file su dung httpclient request
	 * @param url
	 * @param output
	 * @param tmpDir
	 */
	public static void downloadWithHTTPClient( String url, File output, File tmpDir ){
		File tmp = null;
		HttpClient httpclient = new DefaultHttpClient();
		BufferedOutputStream os = null;
		BufferedInputStream is = null;
		try {
			MyLog.d("DownloadDB", "Downloading url :" + url);
			//MyLog.logToFile("DownloadDB", "Downloading url :" + url);
			//MyLog.logToFile("DownloadDB", "Downloading url :" + DateUtils.now());
			
			tmp = File.createTempFile("download", ".tmp", tmpDir);
			HttpGet httpget = new HttpGet(url);
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				is = new BufferedInputStream(entity.getContent());
				os = new BufferedOutputStream(
						new FileOutputStream(tmp));

				copyStream(is, os );
				tmp.renameTo( output );
				tmp = null;
				httpclient.getConnectionManager().shutdown();
			}
		} catch ( IOException e ) {		
			MyLog.e("DownloadDB", "Loi download file db ", e);
			throw new RuntimeException( e );
		} catch (Exception e) {
			MyLog.e("DownloadDB", "Loi download file db ", e);
			throw new RuntimeException( e );
		}finally {
			if ( tmp != null ) { try { tmp.delete(); tmp = null; } catch (Exception ignore) {;} }
			if ( is != null  ) { try { is.close();   is = null;  } catch (Exception ignore) {;} }
			if ( os != null  ) { try { os.close();   os = null;  } catch (Exception ignore) {;} }
		}
		MyLog.d("DownloadDB", "Downloaded url :" + DateUtils.now());
	}
	
	
	
	/**
	 * Copy from one stream to another.  Throws IOException in the event of error
	 * (for example, SD card is full)
	 * 
	 * @param is		Input stream.
	 * @param os		Output stream.
	 */
	public static void copyStream( InputStream is, OutputStream os ) throws IOException {
		byte[] buffer = new byte[ BUFFER_SIZE ];
		copyStream( is, os, buffer, BUFFER_SIZE );
    }
	
	public static void copyStream( BufferedInputStream is, BufferedOutputStream os ) throws IOException {
		byte[] buffer = new byte[ BUFFER_SIZE ];
		copyStream( is, os, buffer, BUFFER_SIZE );
	}

	/**
	 * Copy from one stream to another.  Throws IOException in the event of error
	 * (for example, SD card is full)
	 * 
	 * @param is			Input stream.
	 * @param os			Output stream.
	 * @param buffer		Temporary buffer to use for copy.
	 * @param bufferSize	Size of temporary buffer, in bytes.
	 */
	public static void copyStream( BufferedInputStream is, BufferedOutputStream os,
								      byte[] buffer, int bufferSize ) throws IOException {
		int sizeDownloaded = 0;
		int percent = 0;
		try {
			for (;;) {
				int count = is.read( buffer, 0, bufferSize );
				sizeDownloaded += count;
				percent = (int) ((double) (sizeDownloaded) / (double) (fileSize) * 100);
				if (percent > 98) {
					percent = 98;
				}
				((GlobalBaseActivity) GlobalInfo.getInstance()
						.getActivityContext())
						.updateProgressPercentDialog(percent);
				if ( count == -1 ) { break; }
				os.write( buffer, 0, count );
			}
			os.flush();
		} catch ( IOException e ) {
			throw e;
		}
	}
	public static void copyStream( InputStream is, OutputStream os,
			byte[] buffer, int bufferSize ) throws IOException {
		try {
			for (;;) {
				int count = is.read( buffer, 0, bufferSize );
				if ( count == -1 ) { break; }
				os.write( buffer, 0, count );
			}
			os.flush();
		} catch ( IOException e ) {
			throw e;
		}
	}
	
	//////////////////////////////////////////////////////////////////////////
	// Zip Extraction
	//////////////////////////////////////////////////////////////////////////

	/**
	 * Unpack .zip file.
	 * 
	 * @param zipFile
	 * @param destination
	 */
	public static void unzipFile(Context context, File zipFile, File destination ) {
		DecompressZip decomp = new DecompressZip( zipFile.getPath(), 
				 destination.getPath() + File.separator );
		decomp.unzip(context);
		
		//copySqlFileFromAnotherDir(context);
		
	}
	
	
//	public static void copySqlFileFromAnotherDir(Context context) {
//		// TODO Auto-generated method stub
//		// File (or directory) to be moved
//		
//		File src = new File("/mnt/sdcard/demo.db");
//		Log.e("PhucNT4",
//				"file database exist " + src.exists() + " name "
//						+ src.getName());
//		// Destination directory
//		String database = ExternalStorage.getDatabasePath(context).getAbsolutePath()+"/"+Constants.DATABASE_NAME;
//		File dst = new File(database);
//		Log.e("PhucNT4", "folder database exist " + dst.exists());
//		
//		try {
//			copy(src, dst);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	public static void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}

	
}
