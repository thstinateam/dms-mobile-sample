/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.EquipAttachFileDTO;
import com.ths.dmscore.dto.db.EquipLostMobileRecDTO;
import com.ths.dmscore.dto.db.EquipmentFormHistoryDTO;
import com.ths.dmscore.dto.db.EquipmentHistoryDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedBackDetailDTO;
import com.ths.dmscore.dto.db.FeedbackStaffCustomerDTO;
import com.ths.dmscore.dto.db.FeedbackStaffDTO;
import com.ths.dmscore.dto.db.GSNPPTrainingPlanDTO;
import com.ths.dmscore.dto.db.ListProductDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.ComboboxDisplayProgrameDTO;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListFeedbackDTO;
import com.ths.dmscore.dto.view.DisplayProgrameModel;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.dto.view.EquipStatisticRecordDTO;
import com.ths.dmscore.dto.view.FollowProblemItemDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.ListFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ListStaffDTO;
import com.ths.dmscore.dto.view.OfficeDocumentListDTO;
import com.ths.dmscore.dto.view.PhotoThumbnailListDto;
import com.ths.dmscore.dto.view.ReviewsObjectDTO;
import com.ths.dmscore.dto.view.ShopProblemDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentImageViewDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPMessage;
import com.ths.dmscore.lib.network.http.HTTPResponse;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.StaffCustomerDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.ReviewsStaffViewDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dms.R;

/**
 * Tang xu ly model cua NVGS
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class SupervisorModel extends AbstractModelService {
	protected static volatile SupervisorModel instance;

	protected SupervisorModel() {
	}

	public static SupervisorModel getInstance() {
		if (instance == null) {
			instance = new SupervisorModel();
		}
		return instance;
	}

	public void onReceiveData(HTTPMessage mes) {
		ActionEvent actionEvent = (ActionEvent) mes.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(mes.getDataText());
		model.setCode(mes.getCode());
		model.setParams(((HTTPResponse) mes).getRequest().getDataText());
		model.setActionEvent(actionEvent);
		if (StringUtil.isNullOrEmpty((String) mes.getDataText())) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			SupervisorController.getInstance().handleErrorModelEvent(model);
			return;
		}
		switch (mes.getAction()) {
		case ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE:
			JSONObject json;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				int errCode = result.getInt("errorCode");
				model.setModelCode(errCode);
				if (errCode == ErrorConstants.ERROR_CODE_SUCCESS) {
					SupervisorController.getInstance().handleModelEvent(model);
				} else {
					model.setModelMessage(result.getString("errorMessage"));
					SupervisorController.getInstance().handleErrorModelEvent(model);
				}
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				UserController.getInstance().handleErrorModelEvent(model);
			}
			break;
		default:
			int errCodeDefault = ErrorConstants.ERROR_COMMON;
			try {
				json = new JSONObject((String) mes.getDataText());
				JSONObject result = json.getJSONObject("result");
				errCodeDefault = result.getInt("errorCode");
				model.setModelCode(errCodeDefault);
				model.setModelData(actionEvent.userData);
			} catch (Exception e) {
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			} finally {
				// thanh cong, that bai khong gui tra kq ve view nua
				if (errCodeDefault == ErrorConstants.ERROR_CODE_SUCCESS) {
					// TH mac dinh la request create/update/delete du lieu
					updateLog(actionEvent, LogDTO.STATE_SUCCESS);
					model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
					// UserController.getInstance().handleModelEvent(model);
				} else if (errCodeDefault == ErrorConstants.ERROR_UNIQUE_CONTRAINTS) {
					// request loi trung khoa -- khong thuc hien goi lai len
					// server nua
					// UserController.getInstance().handleErrorModelEvent(model);
					updateLog(actionEvent, LogDTO.STATE_UNIQUE_CONTRAINTS);
				} else {
					// ghi log loi len server
					updateLog(actionEvent, LogDTO.STATE_FAIL);
					// UserController.getInstance().handleErrorModelEvent(model);
				}
			}
		}
	}

	public void onReceiveError(HTTPResponse response) {
		ActionEvent actionEvent = (ActionEvent) response.getUserData();
		ModelEvent model = new ModelEvent();
		model.setDataText(response.getDataText());
		model.setParams(((HTTPResponse) response).getRequest().getDataText());
		model.setActionEvent(actionEvent);

		if (GlobalUtil.checkActionSave(actionEvent.action) && actionEvent.logData != null) {
			// xu ly chung cho cac request
			LogDTO log = (LogDTO) actionEvent.logData;
			if (LogDTO.STATE_NONE.equals(log.state)) {
				updateLog(actionEvent, LogDTO.STATE_NEW);
				// log.state = LogDTO.STATE_NEW;
				// SQLUtils.getInstance().insert(log);
			} else {
				// log.state = LogDTO.STATE_FAIL;
				// SQLUtils.getInstance().update(log);
				updateLog(actionEvent, LogDTO.STATE_FAIL);
			}
			// luu thanh cong
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			SupervisorController.getInstance().handleModelEvent(model);
		} else {
			model.setModelCode(ErrorConstants.ERROR_NO_CONNECTION);
			model.setModelMessage(response.getErrMessage());
			SupervisorController.getInstance().handleErrorModelEvent(model);
		}

	}

	@Override
	public Object requestHandleModelData(ActionEvent e) throws Exception {
		Object data = null;
		switch (e.action) {
		case ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_AMOUNT_REPORT: {
			data = getSupervisorCatAmountReport(e);
			break;
		}
		// start postFeedback dungdq
		case ActionEventConstant.POST_FEEDBACK:
			data = postFeedBack(e);
			break;
		case ActionEventConstant.ACTION_LOAD_LIST_CUSTOMER:
			data = getGsnppCustomerList(e);
			break;
		// end postFeedback dungdq
		case ActionEventConstant.GET_IMAGE_LIST_SEARCH:
			data = getListAlbum(e);
			break;
		case ActionEventConstant.GET_GSNPP_ROUTE_SUPERVISION:
			data = getListSaleRoadmapSupervisor(e);
			break;
		case ActionEventConstant.ACC_SALE_PROG_REPORT:
			data = getAccSaleProgReport(e);
			break;
		// bao cao tien do ban MHTT den ngay //
		case ActionEventConstant.PROG_REPOST_SALE_FOCUS:
			data = getProgRepostSaleFocus(e);
			break;
		case ActionEventConstant.DIS_PRO_COM_PROG_REPORT:
			data = getDisProComProReport(e);
			break;
		case ActionEventConstant.GET_REPORT_PROGRESS_DATE:
			data = getProgressDateReport(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_VISITED: {
			data = getCustomerVisited(e);
			break;
		}
		case ActionEventConstant.GET_REPORT_PROGRESS_DATE_DETAIL:
			data = getProgressDateDetailReport(e);
			break;
		case ActionEventConstant.GET_LIST_NVBH:
			data = getListNVBH(e);
			break;
		case ActionEventConstant.GET_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH:
			data = getCustomerNotPsdsInMonthReport(e);
			break;
		case ActionEventConstant.GET_LIST_VISIT:
			data = getListVisit(e);
			break;
		case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY: {
			data = getProgReportProDispDetailSale(e);
			break;
		}
		case ActionEventConstant.GET_LIST_EQUIPMENT:
			data = getListEquipment(e);
			break;
		case ActionEventConstant.GET_COUNT_CABINET_STAFF:
			data = getCountCabinetStaff(e);
			break;
		case ActionEventConstant.GET_CABINET_STAFF:
			data = getCabinetStaff(e);
			break;
		case ActionEventConstant.STAFF_DIS_PRO_COM_PROG_REPORT: {
			data = getStaffDisProComProReport(e);
			break;
		}
		// bao cao tien do CTTB chi tiet NVBH ngay
		case ActionEventConstant.PROG_REPOST_PRO_DISP_DETAIL_SALE: {
			data = getProgReportProDispDetailSale(e);
			break;
		}
		case ActionEventConstant.GET_LIST_PROMOTION_PROGRAME: {
			data = requestGetListPromotionPrograme(e);
			break;
		}
		case ActionEventConstant.GET_LIST_CUSTOMER_ATTENT_PROGRAME: {
			data = getListCustomerAttentPrograme(
					e);
			break;
		}
		case ActionEventConstant.GET_LIST_COMBOBOX_DISPLAY_PROGRAME: {
			data = getListComboboxDisplayPrograme(e);
			break;
		}
		case ActionEventConstant.GET_LIST_DISPLAY_PROGRAM: {
			data = requestGetListDisplayPrograme(e);
			break;
		}
		case ActionEventConstant.STAFF_INFORMATION: {
			data = requestStaffInfo(e);
			break;
		}
		case ActionEventConstant.GSNPP_TRAINING_RESULT_ACC_REPORT: {
			data = getAccTrainResultReport(e);
			break;
		}
		case ActionEventConstant.GSNPP_TRAINING_PLAN_DAY_REPORT: {
			data = getGsnppTrainingResultDayReport(e);
			break;
		}
		case ActionEventConstant.GO_TO_FOLLOW_LIST_PROBLEM: {
			data = getFollowlistProblem(e);
			break;
		}
		case ActionEventConstant.GO_TO_KEY_SHOP_LIST: {
			data = getListKeyshop(e);
			break;
		}
		case ActionEventConstant.GET_LIST_COMBOBOX_FOLLOW_PROBLEM: {
			data = getComboboxFollowlistProblem(e);
			break;
		}
		case ActionEventConstant.GSNPP_TRAINING_PLAN_FOR_TBHV:
		case ActionEventConstant.GSNPP_TRAINING_PLAN: {
//			SuperviorModel.getInstance().getPlanTrainResultReportDTO(e);
			data = getGsnppTrainingPlan(e);
			break;
		}
		case ActionEventConstant.GET_REVIEWS_INFO_DONE: {
			data = getReviewsDoneOfSTAFF(e);
			break;
		}
		case ActionEventConstant.UPDATE_OR_INSERT_REVIEWS_TO_DB: {
			data = updateReviewsToDBAndServer(e);
			break;
		}
		case ActionEventConstant.GET_LIST_PRODUCT_ADD_REVIEWS_STAFF: {
			data = getListProductToAddReviewsStaff(e);
			break;
		}
		case ActionEventConstant.UPDATE_GSNPP_FOLLOW_PROBLEM_DONE: {
			data = updateGSNPPFollowProblemDone(e);
			break;
		}
		case ActionEventConstant.GET_WRONG_PLAN_CUSTOMER: {
			data = getWrongCustomer(e);
			break;
		}
		case ActionEventConstant.GET_LIST_HISTORY: {
			data = getListHistory(e);
			break;
		}
		case ActionEventConstant.GET_HISTORY_UPDATED_LOCATION: {
			data = getCustomerHistoryUpdateLocation(e);
			break;
		}
		case ActionEventConstant.GET_LIST_POSITION_SALE_PERSONS:
			data = getListPositionSalePersons(e);
			break;
		case ActionEventConstant.UPDATE_EXCEPTION_ORDER_DATE:
			data = requestUpdateExceptionOrderDate(e);
			break;
		case ActionEventConstant.GET_LIST_PROBLEM_OF_GSNPP_NEED_DO_IT:
			data = getListProblemOfGSNPP(e);
			break;
		case ActionEventConstant.ACTION_UPDATE_CHANGE_PROBLEM_STATUS:
			data = requestUpdateFeedbackStatusOfGSNPP(e);
			break;
		case ActionEventConstant.GET_LESS_THAN_2_MINS:
			data = requestGetLessThan2Mins(e);
			break;
		case ActionEventConstant.GO_TO_GSNPP_IMAGE_LIST:
			data = getGsnppImageList(e);
			break;
		case ActionEventConstant.GO_TO_LIST_ALBUM_USER:
			data = getListAlbumUser(e);
			break;
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_USER:
			data = getAlbumDetailUser(e);
			break;
		case ActionEventConstant.GET_PRODUCT_LIST:
			data = getListProductStorage(e);
			break;
		case ActionEventConstant.GSNPP_GET_TIME_OF_HEADER_DEFINE_APPARAM:
			data = getListTimeDefineApparamForReportVisitCustomer(e);
			break;
		case ActionEventConstant.GSNPP_GET_LIST_REPORT_NVBH_VISIT_CUSTOMER:
			data = getListReportNVBHVisitCustomer(e);
			break;
		case ActionEventConstant.GSNPP_GET_LIST_SALE_FOR_ATTENDANCE:
			data = getListSaleForAttendance(e);
			break;
		case ActionEventConstant.GO_TO_LIST_ALBUM_PROGRAME:
			data = getListAlbumPrograme(e);
			break;
		case ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME:
			data = getAlbumDetailPrograme(e);
			break;
		case ActionEventConstant.GET_LIST_DOCUMENT: {
			data = requestGetListDocument(e);
			break;
		}
		case ActionEventConstant.GET_LIST_DOCUMENT_DETAIL: {
			data = requestGetListDocumentDetail(e);
			break;
		}
		case ActionEventConstant.GET_APPARAM:
			data = getListApparam(e);
			break;
		case ActionEventConstant.GET_SHOPPARAM_ALLOW_CLEAR_POSITION:
			data = getShopParamAllowClearPosition(e);
			break;
		case ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT:{
			data = getCustomerInfoVisit(e);
			break;
		}
		case ActionEventConstant.GET_CUSTOMER_BY_ID: {
			data = getCustomerById(e);
			break;
		}
		case ActionEventConstant.GET_LIST_COMBOBOX: {
			data = requestGetListCombobox(e);
			break;
		}
		case ActionEventConstant.GET_LIST_COMBOBOX_SHOP_STAFF: {
			data = requestListComboboxShopStaff(e);
			break;
		}
		case ActionEventConstant.GET_DATA_COMBOBOX_IMAGE_LIST_SUPERVISOR: {
			data = requestDataComboxListImageOfSupervisor(e);
			break;
		}
		case ActionEventConstant.GET_LIST_SHOP_PROBLEM:
			data = getListShopProblem(e);
			break;
		case ActionEventConstant.GET_CUSTOMER_LIST_FOR_POST_FEEDBACK: {
			data = getCustomerListFeedback(e);
			break;
		}case ActionEventConstant.GET_TYPE_FEEDBACK:
			data = requestGetTypeFeedback(e);
			break;
		case ActionEventConstant.GET_LIST_STAFF:
			data = getListStaffFeedback(e);
			break;
		case ActionEventConstant.GO_TO_REPORT_STAFF:
			data = getReportStaff(e);
			break;
		case ActionEventConstant.GO_TO_REPORT_LIST_STAFF:
			data = getReportListStaff(e);
			break;
		case ActionEventConstant.GET_REPORT_KPI_SUPERVISOR:
			data = getReportKPISuperVisor(e);
			break;
		case ActionEventConstant.ACTION_GET_COMBOBOX_CTHTTM:
			data = getComboboxCTHTTM(e);
			break;
		case ActionEventConstant.SUP_GET_REPORT_ASO:
		data = getReportSupAso(e);
			break;
		case ActionEventConstant.ACTION_SAVE_REPORT_LOST_DEVICE_INFOR: {
			data = requestSaveReportLostDevice(e);
			break;
		}
		case ActionEventConstant.GET_CUSTOMER_LIST_REPORT_LOST:
			data = getListCustomerManageEquipment(e);
			break;
		case ActionEventConstant.GET_EQUIPMENT_LOST_REPORT_ERROR:
			data = getEquipmentLostReportError(e);
			break;
		case ActionEventConstant.ACTION_GET_LIST_INVENTORY_EQUIPMENT: {
			data = getListInventoryEquipment(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_REPORT_LOST_DEVICE: {
			data = getReportLostDevice(e);
			break;
		}
		case ActionEventConstant.GET_CUSTOMER_LIST_INVENTORY_EQUIPMENT: {
			data = getListCustomerInventoryEquipment(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_INVENTORY_DEVICE: {
			data = getListInventoryDevice(e);
			break;
		}
		case ActionEventConstant.ACTION_SAVE_INVENTORY_DEVICE: {
			data = requestUpdateInventoryDevice(e);
			break;
		}
		case ActionEventConstant.INSERT_MEDIA_INVENTORY_DEVICE: {
			data = requestInsertMediaInventory(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_PHOTO_INVENTORY_EQUIPMENT_INFO: {
			data = getPhotoInventoryEquipmentInfo(e);
			break;
		}
		case ActionEventConstant.ACTION_GET_LIST_IMAGE_EQUIPMENT: {
			data = getListImageOfInventoryEquipment(e);
			break;
		}
		default:// test
			// UserModel.getInstance().requestTest(e);
			break;
		}
		return data;
	}

	/**
	 * @author: duongdt3
	 * @time: 6:55:46 AM Oct 23, 2015
	 * @param e
	 * @return
	 * @throws Exception 
	*/
	private Object getReportSupAso(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportSupAso(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getListSaleRoadmapSupervisor
	 *
	 * @author: TamPQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getListSaleRoadmapSupervisor(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		String today = bundle.getString(IntentConstants.INTENT_DAY);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);

		Object dto = SQLUtils.getInstance().getGsnppRouteSupervision(staffId, today,
				shopId, sortInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * llay danh sach bao cao tien do ban hang luy ke den ngay
	 *
	 * @author: HieuNQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getAccSaleProgReport(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int staffId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_STAFF_ID));
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		int sysSaleRoute = GlobalInfo.getInstance().getSysSaleRoute();
		Object dto = SQLUtils.getInstance().getAccSaleProgReport(staffId, shopId, sysSaleRoute);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach bao cao tien do ban MHTT den ngay
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getProgRepostSaleFocus(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getProgReportSalesFocus(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 * Lay ds hinh anh tim kiem
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListAlbum(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getListAlbumGSNPP(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach bao cao tien do chung CTTB ngay
	 *
	 * @author: HieuNQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getDisProComProReport(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getDisProComProReport(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * getWrongCustomer
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getCustomerVisited(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getCustomerVisited(staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach bao cao ngay cua nhan vien giam sat
	 *
	 * @param e
	 */
	public ModelEvent getProgressDateReport(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getProgressDateReport(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach bao cao ngay cua nhan vien giam sat
	 *
	 * @param e
	 */
	public ModelEvent getProgressDateDetailReport(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getProgressDateDetailReport(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach vieng tham khach hang cua nhan vien ban hang
	 *
	 * @param e
	 */
	public ModelEvent getListVisit(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int staffId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_STAFF_ID));
		int customerId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_CUSTOMER_ID));
		Object dto = SQLUtils.getInstance().getListVisit(staffId, customerId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach thiet bi
	 *
	 * @param e
	 */
	public ModelEvent getListEquipment(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int staffOwnerId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_STAFF_OWNER_ID));
		String shopId = viewInfo.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getListEquipment(staffOwnerId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay count danh sach theo doi tu nhan vien
	 *
	 * @param e
	 */
	public ModelEvent getCountCabinetStaff(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int staffId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_SHOP_ID));
		int isAll = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_IS_ALL));
		Object dto = SQLUtils.getInstance().getCountCabinetStaff(shopId, staffId,
				isAll);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach theo doi tu nhan vien
	 *
	 * @param e
	 */
	public ModelEvent getCabinetStaff(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int staffId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_STAFF_ID));
		int shopId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_SHOP_ID));
		int isAll = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_IS_ALL));
		String page = viewInfo.getString(IntentConstants.INTENT_PAGE);
		Object dto = SQLUtils.getInstance().getCabinetStaff(shopId, staffId, isAll,
				page);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach bao cao khach hang chua psds trong thang
	 *
	 * @param e
	 */
	public ModelEvent getCustomerNotPsdsInMonthReport(ActionEvent e)
			throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int shopId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo
				.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String visit_plan = viewInfo
				.getString(IntentConstants.INTENT_VISIT_PLAN);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);
		int page = viewInfo.getInt(IntentConstants.INTENT_PAGE);
		int sysCalUnApproved = viewInfo.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		String listStaff = viewInfo.getString(IntentConstants.INTENT_LIST_STAFF);
		DMSSortInfo sortInfo = (DMSSortInfo) viewInfo.getSerializable(IntentConstants.INTENT_SORT_DATA);
		Object dto = SQLUtils.getInstance().getCustomerNotPsdsInMonthReport(shopId,
				staffOwnerId, visit_plan, staffId, page, sysCalUnApproved, listStaff, sortInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach bao cao khach hang chua psds trong thang
	 *
	 * @param e
	 */
	public ModelEvent getCountCustomerNotPsdsInMonthReport(ActionEvent e)
			throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int shopId = Integer.parseInt(viewInfo
				.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo
				.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String visit_plan = viewInfo
				.getString(IntentConstants.INTENT_VISIT_PLAN);
		String staffId = viewInfo.getString(IntentConstants.INTENT_STAFF_ID);

		Object dto = SQLUtils.getInstance().getCountCustomerNotPsdsInMonthReport(
				shopId, staffOwnerId, visit_plan, staffId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * HieuNH lay danh sach NVBH
	 *
	 * @param e
	 */
	public ModelEvent getListNVBH(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		int shopId = Integer.parseInt(viewInfo.getString(IntentConstants.INTENT_SHOP_ID));
		String staffOwnerId = viewInfo.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		boolean orderStaffCode = viewInfo.getBoolean(IntentConstants.INTENT_ORDER);
		boolean isHaveAll = viewInfo.getBoolean(IntentConstants.INTENT_IS_ALL);
		ListStaffDTO dto;
		if (!orderStaffCode) {
			dto = SQLUtils.getInstance().getListNVBH(shopId, staffOwnerId);
		} else {
			dto = SQLUtils.getInstance().getListNVBHOrderCode(shopId, staffOwnerId, isHaveAll);
		}
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 *
	 * lay danh sach Bao cao tien do CTTB theo NVBH ngay
	 *
	 * @author: HieuNQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getStaffDisProComProReport(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getStaffDisProComProReportDTO(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach bao cao tien do CTTB chi tiet NVBH ngay
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getProgReportProDispDetailSale(ActionEvent e)
			throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportDisplayProgressDetailDay(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 * Lay danh sach khuyen mai
	 * @author: hoanpd1
	 * @since: 14:13:25 24-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param event
	 * @return
	 * @throws Exception
	 */
	public ModelEvent requestGetListPromotionPrograme(ActionEvent event)
			throws Exception {
		Bundle bundle = (Bundle) event.viewData;
		Object dto = SQLUtils.getInstance().getListSupervisorPromotionPrograme(bundle);
		boolean checkLoadMore = bundle.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		if (checkLoadMore == false) {
			event.tag = 11;
		}
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach khach hang tham gia chuong trinh
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListCustomerAttentPrograme(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;
		String extPage = data.getString(IntentConstants.INTENT_PAGE);
		long displayProgrameId = data
				.getLong(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String displayProgrameCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String customerCode = data
				.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String customerName = data
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		Object dto = SQLUtils.getInstance().getListCustomerAttentPrograme(extPage,
				displayProgrameCode, displayProgrameId, customerCode,
				customerName, staffId, checkPagging);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Lay danh sach loai CT va danh sach nganh hang
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListComboboxDisplayPrograme(ActionEvent event)
			throws Exception {
		// TODO Auto-generated method stub
		ComboboxDisplayProgrameDTO result = new ComboboxDisplayProgrameDTO();
		result.listDepartPrograme = SQLUtils.getInstance()
				.getListDepartDisplayPrograme();
		return new ModelEvent(event, result.listDepartPrograme, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds chuong trinh khuyen mai
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestGetListDisplayPrograme(ActionEvent event)
			throws Exception {
		Bundle ext = (Bundle) event.viewData;
		boolean repareReset = ext.getBoolean(IntentConstants.INTENT_CHECK_PAGGING);
		DisplayProgrameModel result = null;
		result = SQLUtils.getInstance().getListSuperVisorDisplayPrograme(ext);
		if (!repareReset) {
			event.tag = 11;
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS,"");
	}

	public ModelEvent getAccTrainResultReport(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getAccTrainResultReportDTO(staffId,
				shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay bao cao luy ke doanh so theo nganh cua tung NVBH
	 *
	 * @author: DungNT19
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getSupervisorCatAmountReport(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getSupervisorCatAmountReport(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getStaffTrainResultReportDTO
	 *
	 * @author: HieuNQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getGsnppTrainingResultDayReport(ActionEvent e)
			throws Exception {
		Bundle b = (Bundle) e.viewData;
		long staffTrainId = b
				.getLong(IntentConstants.INTENT_STAFF_TRAIN_DETAIL_ID);
		int shopId = b.getInt(IntentConstants.INTENT_SHOP_ID);
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		Object dto = SQLUtils.getInstance().getGsnppTrainingResultDayReport(
				staffTrainId, shopId, staffId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: HieuNQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getGsnppTrainingPlan(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getGsnppTrainingPlan(staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getGsnppCustomerList
	 *
	 * @author: TrungHQM
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getGsnppCustomerList(ActionEvent actionEvent)
			throws Exception {

		Bundle bundle = (Bundle) actionEvent.viewData;
		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		String code = bundle.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String name = bundle.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		String visit_plan = bundle.getString(IntentConstants.INTENT_VISIT_PLAN);
		int isGetTotalPage = bundle
				.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		Object dto = SQLUtils.getInstance().getGsnppCustomerList(staffId, name, code,
				null, visit_plan, page, isGetTotalPage);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay thong tin cac danh gia da thuc hien doi voi NVBH tren khach hang
	 * trong ngay
	 *
	 * @author: HaiTC3
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getReviewsDoneOfSTAFF(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReviewsInfoOfStaff(b);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * cap nhat du lieu xuong DB local, DB server
	 *
	 * @author: HaiTC3
	 * @param actionEvent
	 * @return: void
	 * @throws:
	 */
	public ModelEvent updateReviewsToDBAndServer(ActionEvent actionEvent)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);

		int result = 0;
		Bundle data = (Bundle) actionEvent.viewData;
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String supperStaffId = data
				.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		ReviewsStaffViewDTO reviewsInfo = (ReviewsStaffViewDTO) data
				.getSerializable(IntentConstants.INTENT_REVIEWS_INFO);

		reviewsInfo = SQLUtils.getInstance().updateIdForTrainingResult(shopId,
				supperStaffId, staffId, reviewsInfo);

		result = SQLUtils.getInstance().insertAndUpdateReviewsStaffDTO(
				reviewsInfo);
		if (result > 0) {
			JSONArray listSql = reviewsInfo.generalSQLInsertAndUpdateToServer();
			String logId = GlobalUtil.generateLogId();
			actionEvent.userData = result;
			actionEvent.logData = logId;
			// send to server
			Vector<Object> para = new Vector<Object>();
			para.add(IntentConstants.INTENT_LIST_SQL);
			para.add(listSql);
			para.add(IntentConstants.INTENT_MD5);
			para.add(StringUtil.md5(listSql.toString()));
			para.add(IntentConstants.INTENT_LOG_ID);
			para.add(logId);
//			para.add(IntentConstants.INTENT_STAFF_ID_PARA);
//			para.add(GlobalInfo.getInstance().getProfile().getUserData().id);
			para.add(IntentConstants.INTENT_IMEI_PARA);
			para.add(GlobalInfo.getInstance().getDeviceIMEI());
			sendHttpRequestOffline("queryController/executeSql", para, actionEvent);

			if (reviewsInfo.feedBackSKU.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
				if (reviewsInfo.listSKU.size() > 0) {
					logId = GlobalUtil.generateLogId();
					actionEvent.logData = logId;
					Vector<Object> para1 = new Vector<Object>();

					para1.add(IntentConstants.INTENT_SHOP_ID);
					para1.add(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritShopId());
					para1.add(IntentConstants.INTENT_STAFF_ID);
					para1.add(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritId());

					para1.add(IntentConstants.INTENT_LIST_STAFF);
					para1.add(staffId);
					para1.add(StringUtil
							.getString(R.string.TEXT_FEEDBACK_TITLE));
					para1.add(StringUtil
							.getString(R.string.TEXT_FEEDBACK_TITLE_CONTENT));
					para1.add(IntentConstants.INTENT_CONTENT);

					String loaiVanDe = reviewsInfo.feedBackSKU.feedBack.getApParamName();
					StringBuilder sb = new StringBuilder();
					for (FeedBackDetailDTO dto : reviewsInfo.listSKU) {
						sb.append(dto.productCode);
						sb.append("; ");
					}
					String noiDung = sb.toString();
					String guiDi = loaiVanDe + ": " + noiDung;
					para1.add(guiDi);
					// sendHttpRequest("gcm.broadcastMessage", para1,
					// actionEvent);
				}
			}
			// gui tin nhan GCM
			for (ReviewsObjectDTO reviewDTO : reviewsInfo.listReviewsObject) {
				if (reviewDTO.currentStateOfFeedback == FeedBackDetailDTO.STATE_NEW_INSERT) {
					logId = GlobalUtil.generateLogId();
					actionEvent.logData = logId;
					Vector<Object> para1 = new Vector<Object>();

					para1.add(IntentConstants.INTENT_SHOP_ID);
					para1.add(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritShopId());
					para1.add(IntentConstants.INTENT_STAFF_ID);
					para1.add(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritId());

					para1.add(IntentConstants.INTENT_LIST_STAFF);
					para1.add(staffId);
					para1.add(StringUtil
							.getString(R.string.TEXT_FEEDBACK_TITLE));
					para1.add(StringUtil
							.getString(R.string.TEXT_FEEDBACK_TITLE_CONTENT));
					para1.add(IntentConstants.INTENT_CONTENT);
					String loaiVanDe = reviewDTO.feedBack.getApParamName();
					String noiDung = reviewDTO.feedBack.getContent();
					String guiDi = loaiVanDe + ": " + noiDung;
					para1.add(guiDi);
					// sendHttpRequest("gcm.broadcastMessage", para1,
					// actionEvent);
				}
			}
			// ket thuc gui tin GCM

			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			model.setModelData(actionEvent.userData);
		} else {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			model.setIsSendLog(false);
			ServerLogger.sendLog("method updateReviewsToDBAndServer error: ",
					"sql save review info to db local incorrect",
					TabletActionLogDTO.LOG_CLIENT);
		}
		return model;
	}

	/**
	 * requestStaffInfo
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent requestStaffInfo(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		int sysSaleRoute = bundle.getInt(IntentConstants.INTENT_SYS_SALE_ROUTE);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		int isLoadSaleInMonth = bundle
				.getInt(IntentConstants.INTENT_LOAD_SALE_IN_MONTH);
		int isGetTotalPage = bundle
				.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		Object dto = SQLUtils.getInstance().getStaffInformation(staffId,sysSaleRoute, page,
				isGetTotalPage, isLoadSaleInMonth, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Ham lay danh sach theo doi khac phuc cua GSNPP
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getFollowlistProblem(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) event.viewData;
		Object dto = SQLUtils.getInstance().getListFollowProblem(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getComboboxFollowlistProblem(ActionEvent event)
			throws Exception {
		// TODO Auto-generated method stub
		Object dto = SQLUtils.getInstance().getComboboxListFollowProblem();
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: ThanhNN8
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent updateGSNPPFollowProblemDone(ActionEvent e) throws Exception {
		// gan vao send offline
		long returnCode = -1;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle b = (Bundle) e.viewData;
		FollowProblemItemDTO dto = (FollowProblemItemDTO) b.getSerializable(IntentConstants.INTENT_DATA);
		returnCode = SQLUtils.getInstance().updateGSNPPFollowProblemDone(dto);
		// send to server
		if (returnCode != -1) {
			JSONObject sqlPara = dto.generateUpdateFollowProblemSql(dto);
			JSONArray jarr = new JSONArray();
			jarr.put(sqlPara);
			genJsonSendToServer(e, jarr);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(dto);
		return model;
	}

	/**
	 *
	 * lay d/s san pham them vao danh gia NVBH
	 *
	 * @author: HaiTC3
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListProductToAddReviewsStaff(ActionEvent event) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(event);
		Bundle data = (Bundle) event.viewData;
		ListFindProductSaleOrderDetailViewDTO result = SQLUtils.getInstance().getListProductToAddReviewsStaff(data);
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * getWrongCustomer
	 *
	 * @author: TamPQ
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getWrongCustomer(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;

		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);

		Object dto = SQLUtils.getInstance().getWrongCustomerList(staffId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * getListHistory trong man hinh chi tiet theo doi va giam sat
	 *
	 * @author: HieuNH
	 * @param event
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getListHistory(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;

		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String productId = b.getString(IntentConstants.INTENT_PRODUCT_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);

		Object dto = SQLUtils.getInstance().getListHistory(staffId, customerId,
				productId, shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach lich su cap nhat vi tri cua khach hang
	 *
	 * @author banghn
	 * @param e
	 */
	public ModelEvent getCustomerHistoryUpdateLocation(ActionEvent e)
			throws Exception {
		Bundle b = (Bundle) e.viewData;
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		Object dto = SQLUtils.getInstance().getCustomerHistoryUpdateLocation(
				customerId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getListSaleRoadmapSupervisor
	 *
	 * @author: TamPQ
	 * @param dto
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getListPositionSalePersons(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		int staffId = bundle.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);

		Object dto = SQLUtils.getInstance().getListPositionSalePersons(staffId,
				shopId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * update exception order dayInOrder: Cho phep dat ghe tham ngoai le
	 *
	 * @author: BangHN
	 * @param e
	 * @return
	 * @return: HTTPRequest
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ModelEvent requestUpdateExceptionOrderDate(ActionEvent e)
			throws Exception {
		e.type = ActionEvent.TYPE_ONLINE;
		e.isNeedCheckTimeServer = false;
		Bundle b = (Bundle) e.viewData;
		StaffCustomerDTO staffCusDto = (StaffCustomerDTO) b.getSerializable(IntentConstants.INTENT_STAFF_DTO);
		// cap nhat gia tri exception oder dayInOrder trong visit_plan
		long success = SQLUtils.getInstance().updateExceptionOrderDate(staffCusDto);
		if (success > 0) {
			JSONObject sqlUpdateExceptionDate = staffCusDto.generateInserOrUpdateExceptionOrderDate();
			JSONArray jarr = new JSONArray();
			jarr.put(sqlUpdateExceptionDate);
			genJsonSendToServer(e, jarr);
		}
		return new ModelEvent(e, success, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list track and fix problem of gsnpp
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public ModelEvent getListProblemOfGSNPP(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getListTrackAndFixProblemOfGSNPP(b);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * request update feedback status from GSNPP
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public ModelEvent requestUpdateFeedbackStatusOfGSNPP(ActionEvent e)
			throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		String feedBackId = data.getString(IntentConstants.INTENT_FEED_BACK_ID);
		String feedBackStatus = data
				.getString(IntentConstants.INTENT_FEED_BACK_STATUS);
		String doneDate = data.getString(IntentConstants.INTENT_DONE_DATE);

		long result = SQLUtils.getInstance().requestUpdateFeedbackStatus(
				feedBackId, feedBackStatus, doneDate);
		if (result == 1) {
			FeedBackDTO feedBack = new FeedBackDTO();
			JSONObject sqlUpdate = feedBack.generateUpdateFeedBackSql(
					feedBackId, feedBackStatus, doneDate);
			JSONArray listSQL = new JSONArray();
			listSQL.put(sqlUpdate);
			genJsonSendToServer(e, listSQL);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
			model.setIsSendLog(false);
			ServerLogger.sendLog("method requestUpdateFeedbackStatusOfGSNPP error: ",
					"save note to db local incorrect", TabletActionLogDTO.LOG_CLIENT);
		}
		model.setModelData(String.valueOf(result));
		return model;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: voidvoid
	 * @throws:
	 */
	public ModelEvent requestGetLessThan2Mins(ActionEvent e) throws Exception {
		Bundle b = (Bundle) e.viewData;
		String lessThan2MinList = b
				.getString(IntentConstants.INTENT_STRING_LIST);
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		Object dto = SQLUtils.getInstance().requestGetLessThan2Mins(shopId, staffId,
				lessThan2MinList);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * Danh sach hinh anh khach hang GSNPP
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 * @date: Jan 8, 2013
	 */
	public ModelEvent getGsnppImageList(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getSupervisorImageList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds hinh anh cua album
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws:
	 */

	public ModelEvent getAlbumDetailUser(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String type = String.valueOf(data
				.getInt(IntentConstants.INTENT_ALBUM_TYPE));
		String numTop = String.valueOf(data
				.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE));
		String page = String.valueOf(data.getInt(IntentConstants.INTENT_PAGE));
		PhotoThumbnailListDto result = new PhotoThumbnailListDto();
		ArrayList<PhotoDTO> listPhoto = SQLUtils.getInstance()
				.getSupervisorAlbumDetailUser(customerId, type, numTop, page,
						shopId);
		result.getAlbumInfo().getListPhoto().addAll(listPhoto);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds album cua user
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */

	public ModelEvent getListAlbumUser(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getAlbumUserInfoCTTB(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds san pham cua shop ma gsnpp quan ly
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */

	public ModelEvent getListProductStorage(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		ListProductDTO dto =   SQLUtils.getInstance().getSupervisorProductList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay d/s thoi gian cho header cua table trong canh bao ghe tham tren tuyen
	 * cua NVBH trong table apparam
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Jan 21, 2013
	 */
	public ModelEvent getListTimeDefineApparamForReportVisitCustomer(ActionEvent e)
			throws Exception {
		Bundle b = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getListDefineHeaderTable(b);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay ds cham cong cac nhan vien cua nhan vien giam sat
	 *
	 * @author: Nguyen Thanh Dung
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */

	public ModelEvent getListSaleForAttendance(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getSupervisorAttendanceList(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * get list report NVBH visit customer
	 *
	 * @param e
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Jan 21, 2013
	 */
	public ModelEvent getListReportNVBHVisitCustomer(ActionEvent e)
			throws Exception {
		Bundle b = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getListReportVisitCustomerInDay(b);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay danh sach hinh anh chuong trinh cua khach hang
	 *
	 * @author ThanhNN
	 * @param e
	 * @throws Exception
	 */
	public ModelEvent getListAlbumPrograme(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle bundle = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getAlbumProgrameInfo(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Lay ds hinh anh cua chuong trinh
	 *
	 * @author: thanhnn
	 * @param e
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */

	public ModelEvent getAlbumDetailPrograme(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String type = String.valueOf(data
				.getInt(IntentConstants.INTENT_ALBUM_TYPE));
		String numTop = String.valueOf(data
				.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE));
		String page = String.valueOf(data.getInt(IntentConstants.INTENT_PAGE));
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String programeCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		PhotoThumbnailListDto result = new PhotoThumbnailListDto();
		ArrayList<PhotoDTO> listPhoto = SQLUtils.getInstance()
				.getAlbumDetailPrograme(customerId, type, numTop, page,
						programeCode, fromDate, toDate, shopId);
		result.getAlbumInfo().getListPhoto().addAll(listPhoto);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent requestGetListDocument(ActionEvent event) throws Exception {
		// TODO Auto-generated method stub
		Bundle bundle = (Bundle) event.viewData;
		OfficeDocumentListDTO result = new OfficeDocumentListDTO();
		int type = bundle.getInt(IntentConstants.INTENT_DOCUMENT_TYPE);
		String fromDate = bundle
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = bundle
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		String ext = bundle.getString(IntentConstants.INTENT_PAGE);
		boolean checkLoadMore = bundle.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		boolean isGetType = bundle.getBoolean(
				IntentConstants.INTENT_IS_GET_LIST_TYPE, false);
		result = SQLUtils.getInstance().getListDocument(type, fromDate, toDate,
				ext, checkLoadMore, shopId);
		if (isGetType) {
			result.listType = SQLUtils.getInstance().getListDocumentType();
		}
		return new ModelEvent(event, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * chi tiet cong van
	 *
	 * @author: YenNTH
	 * @param event
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent requestGetListDocumentDetail(ActionEvent event)
			throws Exception {
		// TODO Auto-generated method stub
		Bundle bundle = (Bundle) event.viewData;
		long officeDocumentId = bundle
				.getLong(IntentConstants.INTENT_OFFICE_DOCUMENT_ID);
		Object dto =   SQLUtils.getInstance().getListDocumentDetail(officeDocumentId);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @param: Tham so cua ham
	 * @return: Ket qua tra ve
	 * @throws Exception
	 * @throws: Ngoai le do ham dua ra (neu co)
	 */
	public ModelEvent getListApparam(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;
		Object dto =  SQLUtils.getInstance().getListApparam(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * shop param cho phep xoa vi tri
	 *
	 * @author: DungNX
	 * @param: ActionEvent
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getShopParamAllowClearPosition(ActionEvent e)
			throws Exception {
		// TODO Auto-generated method stub
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getShopParamAllowClearPosition(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Theo doi va khac phuc cac van de cua nhan vien
	 *
	 * @author: dungdq3
	 * @since: 1:32:02 PM Feb 10, 2014
	 * @return: void
	 * @throws:
	 * @param e
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent postFeedBack(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		boolean insertSuccess = false;
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bd = (Bundle) e.viewData;
		FeedBackDTO feedbackDTO = (FeedBackDTO) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_DTO);
		ArrayList<FeedbackStaffDTO> feedbackStaffDTO = (ArrayList<FeedbackStaffDTO>) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_STAFF_DTO);
		ArrayList<FeedbackStaffCustomerDTO> feedbackStaffCustomerDTO = (ArrayList<FeedbackStaffCustomerDTO>) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_STAFF_CUSTOMER_DTO);
		ArrayList<MediaItemDTO> lstMediaAttach = (ArrayList<MediaItemDTO>) bd
				.getSerializable(IntentConstants.INTENT_FEEDBACK_ATTACH_DTO);
		insertSuccess = SQLUtils.getInstance().gsnppPostFeedBack(feedbackDTO,feedbackStaffDTO,feedbackStaffCustomerDTO, lstMediaAttach);
		// send to server
		if (insertSuccess) {
			JSONArray jarr = new JSONArray();
			JSONObject sqlPara = feedbackDTO.generateFeedbackSql(feedbackDTO.getRemindDate());
			jarr.put(sqlPara);
			for(int i = 0, size = feedbackStaffDTO.size(); i < size ; i++){
				FeedbackStaffDTO item = feedbackStaffDTO.get(i);
				JSONObject sql = item.generateFeedbackStaffSql();
				jarr.put(sql);
			}

			for(int i = 0, size = feedbackStaffCustomerDTO.size(); i < size ; i++){
				FeedbackStaffCustomerDTO item = feedbackStaffCustomerDTO.get(i);
				JSONObject sql = item.generateFeedbackStaffCustomerSql();
				jarr.put(sql);
			}
			genJsonSendToServer(e, jarr);

			for (MediaItemDTO dto : lstMediaAttach) {
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;
				File file = new File(dto.url);
				String fileName = file.getName();

				Vector<String> viewData = new Vector<String>();
				viewData.add(IntentConstants.INTENT_STAFF_ID);
				viewData.add(String.valueOf(dto.staffId));
				viewData.add(IntentConstants.INTENT_SHOP_ID);
				viewData.add(String.valueOf(dto.shopId));
				viewData.add(IntentConstants.INTENT_FILE_NAME);
				viewData.add(fileName);
				viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
				viewData.add(dto.url);
				viewData.add(IntentConstants.INTENT_OBJECT_ID);
				viewData.add(String.valueOf(dto.objectId));
				viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
				viewData.add(String.valueOf(dto.objectType));
				viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
				viewData.add(String.valueOf(dto.mediaType));
				viewData.add(IntentConstants.INTENT_URL);
				viewData.add(String.valueOf(dto.url));
				viewData.add(IntentConstants.INTENT_THUMB_URL);
				viewData.add(String.valueOf(dto.thumbUrl));
				viewData.add(IntentConstants.INTENT_CREATE_DATE);
				viewData.add(String.valueOf(dto.createDate));
				viewData.add(IntentConstants.INTENT_CREATE_USER);
				viewData.add(String.valueOf(dto.createUser));
				viewData.add(IntentConstants.INTENT_LAT);
				viewData.add(String.valueOf(dto.lat));
				viewData.add(IntentConstants.INTENT_LNG);
				viewData.add(String.valueOf(dto.lng));
				viewData.add(IntentConstants.INTENT_FILE_SIZE);
				viewData.add(String.valueOf(dto.fileSize));
				viewData.add(IntentConstants.INTENT_STATUS);
				viewData.add(String.valueOf(dto.status));
				viewData.add(IntentConstants.INTENT_ROUTING_ID);
				viewData.add(Constants.STR_BLANK + dto.routingId);
				viewData.add(IntentConstants.INTENT_IMAGE_ID);
				viewData.add(Long.toString(dto.id));
				viewData.add(IntentConstants.INTENT_CYCLE_ID);
				viewData.add(Constants.STR_BLANK + dto.cycleId);
				httpMultiPartRequestAttachFile(viewData, dto.url, e);
			}

			//tam bo GCM bao co nhac nho
			// start send content GCM to server: dungdq
			/*String logId = GlobalUtil.generateLogId();
			e.logData = logId;
			Vector<Object> paraGCM = new Vector<Object>();

			StringBuffer sb = new StringBuffer();
			for (int i : feedbackDTO.arrStaffId) {
				sb.append(i);
				sb.append(";");
			}
			paraGCM.add(IntentConstants.INTENT_STAFF_ID);
			paraGCM.add(feedbackDTO.createUserId);

			paraGCM.add(IntentConstants.INTENT_LIST_STAFF);
			paraGCM.add(sb);
			paraGCM.add(StringUtil.getString(R.string.FEEDBACK_TITLE));
			paraGCM.add(StringUtil.getString(R.string.FEEDBACK_TITLE_CONTENT));
			paraGCM.add(IntentConstants.INTENT_CONTENT);
			String loaiVanDe = feedbackDTO.apParamName;
			String noiDung = feedbackDTO.content;
			String guiDi = loaiVanDe + ": " + noiDung;
			paraGCM.add(guiDi);
			sendHttpRequest("gcm.broadcastMessage", paraGCM, e);*/

			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		}else{
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		model.setModelData(insertSuccess);
		return model;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @param actionEvent
	 * @return: void
	 * @throws Exception
	 * @throws:
	 */
	public ModelEvent getCustomerInfoVisit(ActionEvent actionEvent)
			throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		Object dto = SQLUtils.getInstance().getCustomerInfoVisitTraining(bundle);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * getPlanTrainResultReportDTO
	 *
	 * @author: HieuNQ
	 * @param dto
	 * @throws Exception
	 * @return:
	 * @throws:
	 */
	public ModelEvent getPlanTrainResultReportDTO(ActionEvent e) {
		Bundle b = (Bundle) e.viewData;
		int staffId = b.getInt(IntentConstants.INTENT_STAFF_ID);
		GSNPPTrainingPlanDTO dto = SQLUtils.getInstance().getPlanTrainResultReportDTO(staffId);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");

	}

	/**
	 * lay thong tin khach hang qua id
	 * @author: hoanpd1
	 * @since: 14:54:21 11-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getCustomerById(ActionEvent actionEvent)
			throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);
		Object dto = SQLUtils.getInstance().getCustomerById(customerId);
		return new ModelEvent(actionEvent, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	/**
	 * lay danh sach shop
	 * @author: hoanpd1
	 * @since: 17:20:11 23-03-2015
	 * @return: Object
	 * @throws:
	 * @param e
	 * @return
	 */
	private ModelEvent requestGetListCombobox(ActionEvent actionEvent) throws Exception{
		Bundle bundle = (Bundle) actionEvent.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String productInfoCode = bundle.getString(IntentConstants.INTENT_PRODUCT_CAT);
		ComboboxDisplayProgrameDTO comboboxModel = new ComboboxDisplayProgrameDTO();
		comboboxModel.listShop = SQLUtils.getInstance().requestGetListCombobox(shopId);
		comboboxModel.listTypePrograme = SQLUtils.getInstance()
				.getListTypeDisplayPrograme();
		comboboxModel.listDepartPrograme = SQLUtils.getInstance()
				.getListDepartDisplayPrograme();
		comboboxModel.listSubcat = SQLUtils.getInstance().getListSubcat(productInfoCode);
		return new ModelEvent(actionEvent, comboboxModel, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Danh sach shop va NV
	 * @author: hoanpd1
	 * @since: 10:17:22 26-03-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 */
	private ModelEvent requestListComboboxShopStaff(ActionEvent actionEvent)throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffOwnerId = bundle.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		boolean isSatffOwnerId = bundle.getBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID);
		ListComboboxShopStaffDTO comboboxModel = (ListComboboxShopStaffDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
		if (bundle.getBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST)) {
			comboboxModel = new ListComboboxShopStaffDTO();
			comboboxModel.listShop = SQLUtils.getInstance().requestGetListCombobox(shopId);
			if(!comboboxModel.listShop.isEmpty()){
				shopId = String.valueOf(comboboxModel.listShop.get(0).shopId);
			}else {
				shopId ="";
			}
		}
		String listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
		comboboxModel.listStaffId = listStaffId;
		comboboxModel.listStaff = SQLUtils.getInstance().getListStaffInfo(shopId, staffOwnerId, listStaffId, isSatffOwnerId);
		return new ModelEvent(actionEvent, comboboxModel, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * lay du lieu combobox List image of supervisor
	 * @author: hoanpd1
	 * @since: 15:29:02 13-04-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 */
	private ModelEvent requestDataComboxListImageOfSupervisor(ActionEvent actionEvent)throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffOwnerId = bundle.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		boolean isStaffOwnerId = bundle.getBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID);
		ListComboboxShopStaffDTO comboboxModel = (ListComboboxShopStaffDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
		if (bundle.getBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST)) {
			comboboxModel = new ListComboboxShopStaffDTO();
			comboboxModel.listShop = SQLUtils.getInstance().requestGetListCombobox(shopId);
			if(!comboboxModel.listShop.isEmpty()){
				shopId = String.valueOf(comboboxModel.listShop.get(0).shopId);
			}else {
				shopId ="";
			}
		}
	    comboboxModel.listDisplayProgram = SQLUtils.getInstance().getListDisplayProgrameImageOfSupervisor(shopId);

		String listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
		//lay ca giam sat.
		if(isStaffOwnerId){
			if(!StringUtil.isNullOrEmpty(listStaffId)){
				listStaffId  = listStaffId.concat(","+staffOwnerId);
			}else {
				listStaffId = staffOwnerId;
			}
		}
		comboboxModel.listStaffId = listStaffId;
		comboboxModel.listStaff = SQLUtils.getInstance().getListStaffInfo(shopId, staffOwnerId, listStaffId, isStaffOwnerId);
//		comboboxModel.listCycle = SQLUtils.getInstance().getListCycleOfSupervisor();
//		comboboxModel.curentCycle = SQLUtils.getInstance().currentCycle();
//		comboboxModel.listDisplayProgramThreeCycle = SQLUtils.getInstance().getListDisplayProgrameThreeCycle(shopId);
		return new ModelEvent(actionEvent, comboboxModel, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay ds shop cho ds van de
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	private ModelEvent getListShopProblem(ActionEvent e) throws Exception {
		// TODO Auto-generated method stub
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		ArrayList<ShopProblemDTO> dto = SQLUtils.getInstance().getListShopProblem(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 *
	 * lay danh sach khach hang thuoc GSNPP
	 *
	 * @author: HoanPD1
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public ModelEvent getCustomerListFeedback(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle data = (Bundle) e.viewData;
		CustomerListFeedbackDTO dto = SQLUtils.getInstance().getCustomerListFeedback(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay loai van de
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent requestGetTypeFeedback(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		int from = bundle.getInt(IntentConstants.INTENT_FROM);
		Object dto =  SQLUtils.getInstance().requestGetTypeFeedback(from);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}


	 /**
	 * Lay ds nhan vien tao van de
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	public ModelEvent getListStaffFeedback(ActionEvent e) throws Exception {
		Bundle bundle = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getListStaffFeedback(bundle);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay danh sach key shop role GSBH
	 * @author: yennth16
	 * @since: 11:46:01 10-07-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param event
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getListKeyshop(ActionEvent event) throws Exception {
		Bundle data = (Bundle) event.viewData;
		Object dto = SQLUtils.getInstance().getKeyShopList(data);
		return new ModelEvent(event, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Bao cao key shop role NVBH
	 * @author: yennth16
	 * @since: 16:20:53 20-07-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getReportStaff(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportKeyShopStaff(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Bao cao key shop role NVBH
	 * @author: yennth16
	 * @since: 16:20:53 20-07-2015
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getReportListStaff(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getListReportKeyShopStaff(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Lay bao cao kpi cua gs
	 *
	 * @author: Tuanlt11
	 * @param e
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getReportKPISuperVisor(ActionEvent e) throws Exception {
		Bundle viewInfo = (Bundle) e.viewData;
		Object dto = SQLUtils.getInstance().getReportKPISuperVisor(viewInfo);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	 /**
	 * Lay ds ct httm
	 * @author: Tuanlt11
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	*/
	private ModelEvent getComboboxCTHTTM(ActionEvent actionEvent)throws Exception {
		Bundle bundle = (Bundle) actionEvent.viewData;
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffOwnerId = bundle.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		boolean isStaffOwnerId = bundle.getBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID);
		ListComboboxShopStaffDTO comboboxModel = (ListComboboxShopStaffDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
		if (bundle.getBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST)) {
			comboboxModel = new ListComboboxShopStaffDTO();
			comboboxModel.listShop = SQLUtils.getInstance().requestGetListCombobox(shopId);
			if(!comboboxModel.listShop.isEmpty()){
				shopId = String.valueOf(comboboxModel.listShop.get(0).shopId);
			}else {
				shopId ="";
			}
		}
//	    comboboxModel.listDisplayProgram = SQLUtils.getInstance().getListDisplayProgrameImageOfSupervisor(shopId);

		String listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
		//lay ca giam sat.
		if(isStaffOwnerId){
			if(!StringUtil.isNullOrEmpty(listStaffId)){
				listStaffId  = listStaffId.concat(","+staffOwnerId);
			}else {
				listStaffId = staffOwnerId;
			}
		}
		comboboxModel.listStaffId = listStaffId;
		comboboxModel.listStaff = SQLUtils.getInstance().getListStaffInfo(shopId, staffOwnerId, listStaffId, isStaffOwnerId);
		comboboxModel.listCycle = SQLUtils.getInstance().getListCycleOfSupervisor();
		comboboxModel.curentCycle = SQLUtils.getInstance().currentCycle();
		comboboxModel.listDisplayProgramThreeCycle = SQLUtils.getInstance().getListDisplayProgrameThreeCycle(shopId);
		return new ModelEvent(actionEvent, comboboxModel, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}

	/**
	 * Luu bao mat thiet bi
	 * @author: hoanpd1
	 * @since: 18:24:42 18-12-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public ModelEvent requestSaveReportLostDevice(ActionEvent e) throws Exception {

		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		Bundle bundle = (Bundle) e.viewData;
		EquipLostMobileRecDTO equipLostMobileRec = (EquipLostMobileRecDTO) bundle.getSerializable(IntentConstants.INTENT_EQUIP_LOST_MOBILE_REC_DATA);
		EquipmentFormHistoryDTO equipFromHistory = (EquipmentFormHistoryDTO) bundle.getSerializable(IntentConstants.INTENT_EQUIP_FORM_HISTORY);
		EquipmentHistoryDTO equipHistory = (EquipmentHistoryDTO) bundle.getSerializable(IntentConstants.INTENT_EQUIP_HISTORY);
		try {
			long success = SQLUtils.getInstance().requestSaveReportLostDevice(bundle);

			if (success > 0) {
				JSONArray jarr = new JSONArray();
				JSONObject sqlInsertEquipLostMobileRec = equipLostMobileRec.generateInsertEquipLostMobileRec (equipLostMobileRec);
				jarr.put(sqlInsertEquipLostMobileRec);
				JSONObject sqlInsertEquipFromHistory = equipFromHistory.generateInsertEquipFromHistory (equipFromHistory);
				jarr.put(sqlInsertEquipFromHistory);
				JSONObject sqlInsertEquipHistory = equipHistory.generateInsertEquipHistory (equipHistory);
				jarr.put(sqlInsertEquipHistory);
				JSONObject sqlUpdateEquip = equipHistory.generateUpdateEquip(equipHistory);
				jarr.put(sqlUpdateEquip);
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_LOG_TYPE);
				para.add("REPORT_LOST_EQUIPMENT");
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			} else {
				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.MESSAGE_NO_CUSTOMER_INFO));
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
		}
		return model;
	}
	
	/**
	 * danh sach khach hang dang muon thiet bi
	 * @author: hoanpd1
	 * @since: 15:23:24 25-12-2014
	 * @return: void
	 * @throws:
	 * @param actionEvent
	 */
	public ModelEvent getListCustomerManageEquipment(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);

		Bundle bundle = (Bundle) actionEvent.viewData;
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String code = bundle.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String name = bundle.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		// String address = bundle
		// .getString(IntentConstants.INTENT_CUSTOMER_ADDRESS);
		int page = bundle.getInt(IntentConstants.INTENT_PAGE);
		String visit_plan = bundle.getString(IntentConstants.INTENT_VISIT_PLAN);
		String isGetWrongPlan = bundle.getString(IntentConstants.INTENT_GET_WRONG_PLAN);
		String ownerId = bundle.getString(IntentConstants.INTENT_USER_ID);
		String listStaff = bundle.getString(IntentConstants.INTENT_LIST_STAFF);
		boolean getWrongPlan = false;
		if (isGetWrongPlan.equals("1")) {
			getWrongPlan = true;
		}
		int isGetTotalPage = bundle.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);

		CustomerListDTO dto = SQLUtils.getInstance()
				.getListCustomerManageEquipment(ownerId, staffId, shopId, name, code,
						null, visit_plan, getWrongPlan, page, listStaff, isGetTotalPage);

		if (dto != null) {
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			StringBuffer buff = new StringBuffer();
			buff.append("staffId: ");
			buff.append(staffId);
			buff.append("/shopId: ");
			buff.append(shopId);
			buff.append("/name: ");
			buff.append(name);
			buff.append("/visit_plan: ");
			buff.append(visit_plan);
			buff.append("/isGetWrongPlan: ");
			buff.append(isGetWrongPlan);
			ServerLogger
					.sendLog(
							"method getListCustomerManageEquipment(Danh sach khach hang dang quan ly thiet bi): ",
							buff.toString(), TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		return model;
	}
	
	/**
	 * danh sach khach hang dang quan ly thiet bi
	 * @author dungnt19
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getListCustomerInventoryEquipment(ActionEvent actionEvent) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(actionEvent);
		
		Bundle bundle = (Bundle) actionEvent.viewData;
		CustomerListDTO dto = SQLUtils.getInstance().getListCustomerEventoryEquipment(bundle);
		
		if (dto != null) {
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog(
					"method getListCustomerInventoryEquipment(Danh sach khach hang dang quan ly thiet bi): ", "", TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelData(dto);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
		}
		return model;
	}
	
	/**
	 * @author: DungNX
	 * @param e
	 * @return: void
	 * @throws:
	*/
	public ModelEvent getEquipmentLostReportError(ActionEvent e)
			throws Exception {
		Bundle data = (Bundle) e.viewData;
		List<EquipInventoryDTO> result = null;
		result = SQLUtils.getInstance().getEquipmentLostReportError(data);
		return new ModelEvent(e, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay danh sach quan ly thiet bi
	 * @author: hoanpd1
	 * @since: 15:27:17 27-11-2014
	 * @return: void
	 * @throws:
	 * @param e
	 */
	public ModelEvent getReportLostDevice(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		EquipStatisticRecordDTO dto = SQLUtils.getInstance()
				.getReportLostDevice(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds cac chuong trinh kiem ke thiet bi
	 * @author: dungnt19
	 * @param e
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getListInventoryEquipment(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		CustomerListDTO dto = SQLUtils.getInstance().getListInventoryEquipment(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds thiet bi cua KH
	 * @author: dungnt19
	 * @return
	 * @throws Exception
	 */
	public ModelEvent getListInventoryDevice(ActionEvent e) throws Exception {
		Bundle data = (Bundle) e.viewData;
		EquipStatisticRecordDTO dto = SQLUtils.getInstance().getSupervisorListInventoryDevice(data);
		return new ModelEvent(e, dto, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Cap nhat kiem ke thiet bi
	 * @author: dungnt19
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 */
	public ModelEvent requestUpdateInventoryDevice(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);

		Bundle bundle = (Bundle) e.viewData;
		EquipStatisticRecordDTO listPeriod = (EquipStatisticRecordDTO) bundle.getSerializable(IntentConstants.INTENT_LIST_INVENTORY_DEVICE);
		try {
			long success = SQLUtils.getInstance().insertInventoryDevice(bundle);

			if (success > 0) {
				JSONArray jarr = new JSONArray();
				for (EquipInventoryDTO productCompetitor : listPeriod.listEquipInventory) {
					JSONObject sqlPara = productCompetitor.generateInsertEquipLostMobileRec();
					jarr.put(sqlPara);
				}
				String logId = GlobalUtil.generateLogId();
				e.logData = logId;

				Vector<Object> para = new Vector<Object>();
				para.add(IntentConstants.INTENT_LIST_SQL);
				para.add(jarr.toString());
				para.add(IntentConstants.INTENT_MD5);
				para.add(StringUtil.md5(jarr.toString()));
				para.add(IntentConstants.INTENT_LOG_ID);
				para.add(logId);
				para.add(IntentConstants.INTENT_IMEI_PARA);
				para.add(GlobalInfo.getInstance().getDeviceIMEI());
				sendHttpRequestOffline("queryController/executeSql", para, e);

				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
			} else {
				model.setModelData(success);
				model.setModelCode(ErrorConstants.ERROR_COMMON);
				model.setModelMessage(StringUtil.getString(R.string.MESSAGE_NO_CUSTOMER_INFO));
			}
		} catch (Exception ex) {
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(ex.getMessage());
		}

		return model;
	}

	/**
	 * Luu hinh anh cua kiem ke thiet bi
	 * @author: dungnt19
	 * @return: ModelEvent
	 * @throws:
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ModelEvent requestInsertMediaInventory(ActionEvent e) throws Exception {
		ModelEvent model = new ModelEvent();
		model.setActionEvent(e);
		EquipAttachFileDTO equipAttachFileDTO = (EquipAttachFileDTO) e.userData;

		long equipAttachFileId = SQLUtils.getInstance().insertMediaInventory(
				equipAttachFileDTO);
		if (equipAttachFileId != -1) {
			String logId = GlobalUtil.generateLogId();
			e.logData = logId;
			Vector<String> data = (Vector<String>) e.viewData;
			data.add(IntentConstants.INTENT_EQUIP_ATTACH_FILE_ID);
			data.add(Long.toString(equipAttachFileId));
			data.add(IntentConstants.INTENT_LOG_ID);
			data.add(logId);
			String imgPath = data.get(
					data.lastIndexOf(IntentConstants.INTENT_TAKEN_PHOTO) + 1)
					.toString();

			if (StringUtil.isNullOrEmpty(imgPath)) {
			} else {
				httpMultiPartRequestInventory(data, imgPath, e);
			}
			model.setModelCode(ErrorConstants.ERROR_CODE_SUCCESS);
		} else {
			ServerLogger.sendLog("Insert mot hinh anh bi that bai "
					+ equipAttachFileDTO.idEquipAttachFile,
					TabletActionLogDTO.LOG_CLIENT);
			model.setIsSendLog(false);
			model.setModelCode(ErrorConstants.ERROR_COMMON);
			model.setModelMessage(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
		}
		return model;
	}

	/**
	 * Lay thong tin man hinh chup hinh thiet bi kiem ke
	 * @author: dungnt19
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getPhotoInventoryEquipmentInfo(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		TakePhotoEquipmentViewDTO result = SQLUtils.getInstance().getPhotoInventoryEquipmentInfo(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
	
	/**
	 * Lay ds hinh anh cua thiet bi kiem  ke
	 * @author: dungnt19
	 * @param actionEvent
	 * @return
	 * @throws Exception
	 * @return: ModelEvent
	 * @throws:
	 */
	public ModelEvent getListImageOfInventoryEquipment(ActionEvent actionEvent) throws Exception {
		Bundle data = (Bundle) actionEvent.viewData;
		TakePhotoEquipmentImageViewDTO result = SQLUtils.getInstance().getListImageOfInventoryEquipment(data);
		return new ModelEvent(actionEvent, result, ErrorConstants.ERROR_CODE_SUCCESS, "");
	}
}
