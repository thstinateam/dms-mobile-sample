/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;

/**
 * PRODUCT_GROUP_TABLE.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class SALE_ORDER_PROMOTION_TABLE extends ABSTRACT_TABLE {

	public static final String SALE_ORDER_PROMOTION_ID = "SALE_ORDER_PROMOTION_ID";
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	public static final String PROMOTION_PROGRAM_CODE = "PROMOTION_PROGRAM_CODE";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String PROMOTION_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String PROMOTION_LEVEL = "PROMOTION_LEVEL";
	public static final String QUANTITY_RECEIVED = "QUANTITY_RECEIVED";
	public static final String QUANTITY_RECEIVED_MAX = "QUANTITY_RECEIVED_MAX";
	public static final String NUM_RECEIVED = "NUM_RECEIVED";
	public static final String NUM_RECEIVED_MAX = "NUM_RECEIVED_MAX";
	public static final String AMOUNT_RECEIVED = "AMOUNT_RECEIVED";
	public static final String AMOUNT_RECEIVED_MAX = "AMOUNT_RECEIVED_MAX";
	public static final String PROMOTION_DETAIL = "PROMOTION_DETAIL";
	public static final String EXCEED_UNIT = "EXCEED_UNIT";
	public static final String EXCEED_OBJECT = "EXCEED_OBJECT";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "SALE_ORDER_PROMOTION";
	public SALE_ORDER_PROMOTION_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SALE_ORDER_PROMOTION_ID, SALE_ORDER_ID, SHOP_ID,
				ORDER_DATE, PROMOTION_PROGRAM_ID, PROMOTION_PROGRAM_CODE, PRODUCT_GROUP_ID, 
				PROMOTION_LEVEL_ID, PROMOTION_LEVEL, QUANTITY_RECEIVED, QUANTITY_RECEIVED_MAX, STAFF_ID,
				NUM_RECEIVED, NUM_RECEIVED_MAX, AMOUNT_RECEIVED, AMOUNT_RECEIVED_MAX, PROMOTION_DETAIL, EXCEED_UNIT, EXCEED_OBJECT,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		SaleOrderPromotionDTO sDto = (SaleOrderPromotionDTO) dto;
		return insert(null, sDto.initInsertOrderSalePromotion());
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:39:17 15 Sep 2014
	 * @return: int
	 * @throws:  
	 * @param saleOrderId
	 * @return
	 */
	public int deleteAllDetailOfOrder(long saleOrderId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(saleOrderId));

		return delete(SALE_ORDER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}

	/**
	 * @author: DungNX
	 * @param saleOrderID
	 * @return
	 * @throws Exception
	 * @return: List<SaleOrderPromotionDTO>
	 * @throws:
	*/
	public ArrayList<SaleOrderPromotionDTO> getSaleOrderPromotionBySaleOrderID(long saleOrderID) throws Exception{
		ArrayList<SaleOrderPromotionDTO> result = new ArrayList<SaleOrderPromotionDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    SALE_ORDER_PROMOTION	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND SALE_ORDER_ID = ?	");
		paramsObject.add("" + saleOrderID);
//		sqlObject.append("	    AND STAFF_ID = ?	");
//		paramsObject.add();
//		sqlObject.append("	    AND SHOP_ID = ?	");
//		paramsObject.add();		
		
		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						SaleOrderPromotionDTO item = new SaleOrderPromotionDTO();
						item.initFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		
		return result;
	}
}
