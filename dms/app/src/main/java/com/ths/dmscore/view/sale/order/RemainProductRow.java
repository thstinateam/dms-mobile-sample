package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.graphics.Typeface;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.RemainProductViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Row cua man hinh kiem ton
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RemainProductRow extends DMSTableRow implements OnFocusChangeListener {

	public static final int ACTION_CLICK_SDH = 0;
	public static final int ACTION_CLICK_MKH = 1;
	public static final int ACTION_CLICK_DELETE = 2;
	public static final int ACTION_CLICK_SELECT = 3;
	public static final int ACTION_CLICK_SHOW_PROMOTION_DETAIL = 4;
	TextView tvSTT;
	public TextView tvMMH;
	TextView tvTMH;
	EditText edRemainQuanlity;
	LinearLayout llRemain;

	public RemainProductRow(Context context, VinamilkTableListener listen) {
		super(context, R.layout.remain_product_row);
		setListener(listen);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvMMH = (TextView) findViewById(R.id.tvMMH);
		tvTMH = (TextView) findViewById(R.id.tvTMH);
		edRemainQuanlity = (EditText) findViewById(R.id.edRemainQuanlity);
		edRemainQuanlity.setOnFocusChangeListener(this);
		GlobalUtil.setFilterInputConvfact(edRemainQuanlity, Constants.MAX_LENGHT_QUANTITY);

		llRemain =  (LinearLayout) findViewById(R.id.llRemain);

		//an ban phim ao
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			edRemainQuanlity.setInputType(InputType.TYPE_NULL);
			edRemainQuanlity.setOnClickListener(this);
		}
	}

	 /**
	  * render layout
	 * @author: Tuanlt11
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(int position, RemainProductViewDTO item) {
		tvSTT.setText("" + position);
		tvMMH.setText(item.getPRODUCT_CODE());
		tvTMH.setText(item.getPRODUCT_NAME());
//		String str ="";
//		if (item.sign==4 || item.sign==3  ){
//
//			str="*";
//
//		}else if (item.sign==6 ){
//			str="* GS";
//		}
//		else if (item.sign==2 ){
//			str="GS";
//		}
//
//		 if(!StringUtil.isNullOrEmpty(str)){
//			 StringUtil.superScript(tvMMH, item.getPRODUCT_CODE(), str, ImageUtil.getColor(R.color.RED));
//		 }

		edRemainQuanlity.setText(item.REMAIN_NUMBER);

//		if(StringUtil.isNullOrEmpty(item.getPromotionProgrameDetailId())){
//
//		}
//		if (item.getHAS_REMAIN()==0){
//			setHightlight();
//		}
	}
//
//	private void setHightlight() {
//		// TODO Auto-generated method stub
//		tvSTT.setTextColor(getResources().getColor(R.color.RED));
//		tvTMH.setTextColor(getResources().getColor(R.color.RED));
//		tvMMH.setTextColor(getResources().getColor(R.color.RED));
//	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v == edRemainQuanlity) {
			if (listener != null) {
				listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
						v, null);
			}
		}

		if (v == this && context != null){
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity)context);
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (v != null && v instanceof EditText && hasFocus) {
			EditText et = ((EditText)v);
			et.setSelection(et.getText().length());

			if (listener != null) {
				listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
			}
		}
	}

	/**
	 * Render header cho table Roi vao truong hop dac biet khi header la
	 * edittext, phai set lai kich thuoc
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void renderHeader() {
		llRemain.setBackgroundColor(ImageUtil
				.getColor(R.color.TABLE_HEADER_BG));
		llRemain.setPadding(GlobalUtil.dip2Pixel(0), GlobalUtil.dip2Pixel(0),
				GlobalUtil.dip2Pixel(0), GlobalUtil.dip2Pixel(0));
		llRemain.removeView(edRemainQuanlity);
		// tb ko phai la textview
		TextView textView = new TextView(context);
		textView.setLayoutParams(edRemainQuanlity.getLayoutParams());
		if (edRemainQuanlity.getTag() != null) {
			textView.setText(edRemainQuanlity.getTag().toString());
		}
		textView.setTextSize(tvSTT.getTextSize());
		textView.setTextColor(ImageUtil.getColor(R.color.BLACK));
		textView.setTypeface(null, Typeface.BOLD);
		textView.setGravity(Gravity.CENTER);
		llRemain.addView(textView);
	}
}
