/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Bang luu log request
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class LOG_TABLE extends ABSTRACT_TABLE{
	// id cua bang
	public static final String LOG_ID = "LOG_ID";
	// gia tri (cau lenh excute)
	public static final String VALUE = "VALUE";
	// userId
	public static final String USER_ID = "USER_ID";
	// trang thai log
	public static final String STATE = "STATE";
	// trang thai syn
	public static final String SYN_STATE = "SYN_STATE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay update
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// table name - tam thoi chua dung
	public static final String TABLE_NAME = "TABLE_NAME";
	// table id - tam thoi chua dung
	public static final String TABLE_ID = "TABLE_ID";
	// table type
	public static final String TABLE_TYPE = "TABLE_TYPE";
	// co can kiem tra thoi gian truoc khi thuc hien request hay khong? : 0: khong can, 1: can kiem tra
	public static final String NEED_CHECK_DATE = "NEED_CHECK_DATE";

	private static final String TABLE_LOG = "LOG_TABLE";

	/**
	 * tao va mo mot CSDL
	 * @author: BangHN
	 * @return: SQLiteUtil
	 * @throws:
	 */
	public LOG_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_LOG;
		this.columns = new String[] {LOG_ID,VALUE,USER_ID,STATE, SYN_STATE,
				CREATE_DATE, UPDATE_DATE, CREATE_USER, UPDATE_USER, TABLE_NAME, TABLE_TYPE, TABLE_ID, NEED_CHECK_DATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}



	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((LogDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		ContentValues editedValues = new ContentValues();
		LogDTO log = (LogDTO)dto;
		editedValues.put(STATE, log.state);
		editedValues.put(UPDATE_DATE, log.updateDate);
		editedValues.put(UPDATE_USER, log.updateUser);
		String[] params = { "" + log.logId };
		return update(editedValues, LOG_ID + " = ?", params);
	}


	@Override
	protected long delete(AbstractTableDTO dto) {
		LogDTO log = (LogDTO)dto;
		String[] params = { log.logId};
		return delete(LOG_ID + " = ?", params);
	}

	/**
	 * init du lieu cho mot row (log)
	 * @author : BangHN
	 * since : 9:50:41 AM
	 */
	private ContentValues initDataRow(LogDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(LOG_ID, dto.logId);
		editedValues.put(VALUE, dto.value);
		editedValues.put(USER_ID, dto.userId);
		editedValues.put(STATE, dto.state);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(NEED_CHECK_DATE, dto.needCheckDate);
		editedValues.put(TABLE_TYPE, dto.tableType);
		if (!StringUtil.isNullOrEmpty(dto.tableId)){
			editedValues.put(TABLE_ID, dto.tableId);
		}
		editedValues.put(TABLE_NAME, dto.tableName);
		return editedValues;
	}



	private LogDTO initLogDTOFromCursor(Cursor c) {
		LogDTO logDTO = new LogDTO();

		logDTO.logId = CursorUtil.getString(c, LOG_ID);
		logDTO.value = CursorUtil.getString(c, VALUE);
		logDTO.userId = CursorUtil.getString(c, USER_ID);
		logDTO.state = CursorUtil.getString(c, STATE);
		logDTO.createDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, CREATE_DATE));
		logDTO.createUser = CursorUtil.getString(c, CREATE_USER);
		logDTO.tableType = CursorUtil.getInt(c, TABLE_TYPE);
		logDTO.tableId = CursorUtil.getString(c, TABLE_ID);
		logDTO.tableName= CursorUtil.getString(c, TABLE_NAME);
		logDTO.needCheckDate = CursorUtil.getInt(c, NEED_CHECK_DATE);

		return logDTO;
	}

	/**
	*  Lay ds log tao moi hoac xu ly that bai
	*  @author: TruongHN
	*  @return: ArrayList<LogDTO>
	*  @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheck() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		//String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		int sysMaxdayApprove = GlobalInfo.getInstance().getSysMaxdayApprove();
		int sysMaxdayReturn = GlobalInfo.getInstance().getSysMaxdayReturn();
		//tra don la min sysMaxdayReturn va sysMaxdayApprove
		sysMaxdayReturn = Math.min(sysMaxdayApprove, sysMaxdayReturn);
		// cong them so ngay nghi vao
		try {
			sysMaxdayReturn = SQLUtils.getInstance().addCurrentWorkingDaysOfMonth(DateUtils.convertToDate(DateUtils.now(), DateUtils.DATE_FORMAT_NOW), sysMaxdayReturn);
		} catch (Exception e) {
			// TODO: handle exception
		}
		String timeCheckToServer = "-" + sysMaxdayReturn + " fullDate";
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" STATE != ? and STATE != ? and STATE != ? and STATE != ?") ;
			strBuffer.append(" and STATE != ? and STATE != ? and STATE != ? and ");
			// truong hop nhung log != log don hang thi lay nhung log co ngay tao <= 1 so voi ngay hien tai
//			strBuffer.append(" ((dayInOrder(CREATE_DATE) >= dayInOrder('now', '-1 fullDate') ");
//			strBuffer.append("       AND table_type != ?) ");
//			// truong hop log don hang thi lay nhung don hang tao co min la han tra va han duyet
//			strBuffer.append(" OR (dayInOrder(create_date) >= dayInOrder('now', 'localtime', ? ) ");
//			strBuffer.append("       AND table_type = ?)) ");
			// lay nhung log thoa man dieu kien min han tra, han duyet
			strBuffer.append(" dayInOrder(CREATE_DATE) >=dayInOrder('now', 'localtime', ? ) ");

			String[] params = { LogDTO.STATE_SUCCESS,
					LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS,
					LogDTO.STATE_ORDER_DELETED, LogDTO.STATE_IMAGE_DELETED,
					LogDTO.STATE_WRONG_FORMAT, LogDTO.STATE_OTHER_NOT_RETRY,timeCheckToServer };
			c = query(strBuffer.toString(), params, null, null, CREATE_DATE
					+ " asc");

			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getLogNeedToCheck", e.getMessage());
			}
		}

		return v;
	}
	/**
	 *  Lay ds log tao moi hoac xu ly that bai
	 *  Chi lay nhung request can thiet, sau do thuc hien dong bo ve cho nhanh
	 *  @author: TruongHN
	 *  @return: ArrayList<LogDTO>
	 *  @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckBeforeSync() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and ") ;
			strBuffer.append(" (TABLE_TYPE = ? or TABLE_TYPE = ? or TABLE_TYPE = ? ) and dayInOrder(CREATE_DATE) >= dayInOrder(?) ");

			String[] params = { LogDTO.STATE_SUCCESS,
					LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS,
					LogDTO.STATE_ORDER_DELETED, LogDTO.STATE_WRONG_FORMAT,
					LogDTO.STATE_OTHER_NOT_RETRY, String.valueOf(LogDTO.TYPE_NORMAL),
					String.valueOf(LogDTO.TYPE_ORDER),
					String.valueOf(LogDTO.TYPE_POSITION), dateNow };
			c = query(strBuffer.toString(), params, null, null, CREATE_DATE + " asc");

			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getLogNeedToCheck", e.getMessage());
			}
		}

		return v;
	}

	/**
	 *  Lay ds log tao moi hoac xu ly that bai
	 *  Chi lay nhung request can thiet, sau do thuc hien dong bo ve cho nhanh
	 *  @author: TruongHN
	 *  @return: ArrayList<LogDTO>
	 *  @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckForLogin() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		Cursor c = null;
		try {
			StringBuffer  strBuffer = new StringBuffer();
			strBuffer.append("SELECT * ");
			strBuffer.append("FROM   [log_table] ");
			strBuffer.append("WHERE  state = 0 ");
			strBuffer.append("       AND ( table_type = 0 ");
			strBuffer.append("              OR table_type = 3 ) ");
			strBuffer.append("       AND Date(create_date) < Date(?) ");
			strBuffer.append("UNION ");
			strBuffer.append("SELECT * ");
			strBuffer.append("FROM   [log_table] ");
			strBuffer.append("WHERE  state != 2 ");
			strBuffer.append("       AND state != 3 ");
			strBuffer.append("       AND state != 4 ");
			strBuffer.append("       AND state != 5 ");
			strBuffer.append("       AND state != 7 ");
			strBuffer.append("       AND state != 8 ");
			strBuffer.append("       AND ( table_type = 0 ");
			strBuffer.append("              OR table_type = 3  or table_type = 4 ) ");
			strBuffer.append("       AND Date(create_date) >= Date(?) ");
			String[] params = {dateNow, dateNow};
			c=  rawQuery(strBuffer.toString(), params);

			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getLogNeedToCheck", e.getMessage());
			}
		}

		return v;
	}


	/**
	 *  Lay ds log chua duoc xu ly trong vong 10 ngay
	 *  @author: BangHN
	 *  @return: ArrayList<LogDTO>
	 *  @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToSynBefore10Days() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and ") ;
			strBuffer.append(" TABLE_TYPE != 1 AND ");
			strBuffer.append(" dayInOrder(CREATE_DATE) >= dayInOrder('now', 'localtime', '-10 fullDate') ");

			String[] params = { LogDTO.STATE_SUCCESS,
					LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS,
					LogDTO.STATE_ORDER_DELETED, LogDTO.STATE_WRONG_FORMAT,
					LogDTO.STATE_OTHER_NOT_RETRY };
			c = query(strBuffer.toString(), params, null, null, CREATE_DATE + " asc");
			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getLogNeedToCheck", e.getMessage());
			}
		}
		return v;
	}


	/**
	 *  Lay so luong request anh chua gui hoac loi
	 *  @author: BangHN
	 *  @return: ArrayList<LogDTO>
	 *  @throws:
	 */
	public ArrayList<String> getNumLogImageNotSynOrError() {
		ArrayList<String> v = new ArrayList<String>();
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append("SELECT count(1) as NUM, STATE ");
			strBuffer.append("FROM   LOG_TABLE ");
			strBuffer.append("WHERE  TABLE_TYPE = 2 ");
			strBuffer.append("       AND ( STATE = 0 ");
			strBuffer.append("              OR STATE = 6 ) ");
			strBuffer.append("       AND DATE(CREATE_DATE) > DATE('NOW', 'LOCALTIME', '-1 DAY') ");
			strBuffer.append("group by STATE ");
			strBuffer.append("order by STATE ");

			String[] params = {};
			c=  rawQuery(strBuffer.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String temp = "State " + CursorUtil.getString(c, "STATE");
						temp += ": " + CursorUtil.getString(c, "NUM");
						v.add(temp);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				MyLog.i("getNumLogImageNotSynOrError", e.getMessage());
			}
		}
		return v;
	}

	/**
	*  Lay tat ca log thuoc ve position
	*  @author: TruongHN
	*  @return: ArrayList<LogDTO>
	*  @throws:
	 */
	public ArrayList<LogDTO> getLogPosition() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		Cursor c = null;
		try {
			c = query(STATE + " != "  + LogDTO.STATE_SUCCESS + " and "
			+ STATE + " != " + LogDTO.STATE_INVALID_TIME  + " and "
			+ STATE + " != " + LogDTO.STATE_UNIQUE_CONTRAINTS + " and "
			+ STATE + " != " + LogDTO.STATE_ORDER_DELETED + " and "
			+ TABLE_TYPE  + " = " + LogDTO.TYPE_POSITION,
			null, null, null, CREATE_DATE + " asc");
			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getLogPosition", e.getMessage());
			}
		}

		return v;
	}


	/**
	*  Cap nhat cac log cung ngay ve trang thai close
	*  @author: TruongHN
	*  @param dto
	*  @return: long
	*  @throws:
	 */
	public long updateLogsWithStateClose(AbstractTableDTO dto) {
		ContentValues editedValues = new ContentValues();
		LogDTO log = (LogDTO)dto;
		editedValues.put(STATE, LogDTO.STATE_INVALID_TIME);
		editedValues.put(UPDATE_DATE, log.updateDate);
		editedValues.put(UPDATE_USER, log.createUser);

		String date = "";
		int index = log.createDate.indexOf(" ");
		if (index > -1){
			date = log.createDate.substring(0, index);
		}else{
			date = log.createDate;
		}

		String[] params = { "%" + date + "%" };
		return update(editedValues, CREATE_DATE + " like ? and ( STATE = 0 or STATE  = 1)" , params);
	}

	/**
	*  Cap nhat log trang thai close
	*  @author: TruongHN
	*  @param dto
	*  @return: long
	*  @throws:
	 */
	public long updateLogWithStateClose(AbstractTableDTO dto) {
		ContentValues editedValues = new ContentValues();
		LogDTO log = (LogDTO)dto;
		editedValues.put(STATE, LogDTO.STATE_INVALID_TIME);
		editedValues.put(UPDATE_DATE, log.updateDate);
		editedValues.put(UPDATE_USER, log.updateUser);
		String[] params = { "" + log.logId };
		return update(editedValues, LOG_ID + " = ?", params);
	}



	/**
	 * delete log truoc 15 ngay
	 * @author banghn
	 * @return
	 */
	public long deleteOldLog(){
		long success  = -1;
		try{
			mDB.beginTransaction();
			//String sqlDelete = "Delete from LOG_TABLE where dayInOrder(CREATE_DATE) < dayInOrder('now','-15 fullDate','localtime')";
			//xoa du lieu truoc do toi da 2 thang hoac truoc 15 ngay nhung state = 2
			//(giu lai record nao chua thanh cong de dieu tra trong 2 thang)
			String startOfMonth = DateUtils
					.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, -1);
			String[] params = {startOfMonth};

			StringBuffer  sqlDelete = new StringBuffer();
			sqlDelete.append("DELETE FROM log_table ");
			sqlDelete.append("WHERE  1 = 1 ");
			sqlDelete.append("       AND ( SUBSTR(create_date, 0, 11) < ? ");
			sqlDelete.append("              OR ( SUBSTR(create_date, 0, 11) < Date('now', '-10 fullDate', 'localtime') ");
			sqlDelete.append("                   AND state = 2 ) ) ");
			execSQL(sqlDelete.toString(), params);
			success = 1;
			mDB.setTransactionSuccessful();
		}catch (Exception e) {
			MyLog.e("deleteOldLog", "fail", e);
			success = -1;
		}finally{
			mDB.endTransaction();
		}
		return success;
	}

	/**
	*  Lay ds don hang co trong log
	*  @author: TruongHN
	*  @return: ArrayList<String>
	*  @throws:
	 */
	public ArrayList<LogDTO> getOrderInLog() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		Cursor c = null;
		try {
			String []param = {DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE)};
			// co TH xoa don hang khoi ds, nhung trong log_table van con, nen phai loai ra
			c = query(TABLE_TYPE + " = "  + LogDTO.TYPE_ORDER + " AND STATE != " + LogDTO.STATE_ORDER_DELETED +
					" AND dayInOrder("+ CREATE_DATE + ") >= ? ",
					param, null, null, CREATE_DATE + " asc");
			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				MyLog.i("getOrderInLog", e.getMessage());
			}
		}

		return v;
	}

	/**
	 * Cap nhat trang thai dong bo don hang
	 * @author: TruongHN
	 * @param id
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public long updateState(String saleOrderId, int synState) {
		ContentValues value = new ContentValues();
		value.put(STATE, synState);
		String[] params = { "" + saleOrderId };
		return update(value, TABLE_ID + " = ? ", params);
	}

	/**
	 *  Lay ds log thuc hien chot ngay
	 *  @author: ThangNV31
	 *  @return: ArrayList<LogDTO>
	 *  @throws:
	 */
	public ArrayList<LogDTO> getLogNeedToCheckBeforeDateLockSync() {
		ArrayList<LogDTO> v = new ArrayList<LogDTO>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and ") ;
			strBuffer.append(" (TABLE_TYPE = ? or TABLE_TYPE = ? or TABLE_TYPE = ? ) and dayInOrder(CREATE_DATE) >= dayInOrder(?) ");

			String[] params = { LogDTO.STATE_SUCCESS,
					LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS,
					LogDTO.STATE_ORDER_DELETED, LogDTO.STATE_WRONG_FORMAT,
					LogDTO.STATE_OTHER_NOT_RETRY, String.valueOf(LogDTO.TYPE_PAYMENT),
					String.valueOf(LogDTO.TYPE_ORDER),
					String.valueOf(LogDTO.TYPE_POSITION), dateNow };
			c = query(strBuffer.toString(), params, null, null, CREATE_DATE + " asc");

			if (c != null) {
				LogDTO LogDTO;
				if (c.moveToFirst()) {
					do {
						LogDTO = initLogDTOFromCursor(c);
						v.add(LogDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally{
			try{
				if (c != null) {
					c.close();
				}
			}catch (Exception e) {
				MyLog.i("getLogNeedToCheck before dayInOrder lock", e.getMessage());
			}
		}

		return v;
	}

	/**
	 * Kiem tra xem duoc phep thuc hien chot ngay hay chua
	 *
	 * @author: ThangNV31
	 * @return: boolean
	 * @throws:
	 */
	public boolean checkDBLogForDateLock() {
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		Cursor c = null;
		boolean hasLogNotSynYet = false;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ? and ");
			strBuffer.append(" (TABLE_TYPE = ? or TABLE_TYPE = ?) and dayInOrder(CREATE_DATE) >= dayInOrder(?) ");

			String[] params = { LogDTO.STATE_SUCCESS, LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS, LogDTO.STATE_ORDER_DELETED,
					LogDTO.STATE_WRONG_FORMAT, LogDTO.STATE_OTHER_NOT_RETRY, String.valueOf(LogDTO.TYPE_PAYMENT), String.valueOf(LogDTO.TYPE_ORDER), dateNow };
			c = query(strBuffer.toString(), params, null, null, CREATE_DATE + " asc");

			if (c != null) {
				if (c.moveToFirst()) {
					hasLogNotSynYet = true;
				}
			}
		} catch (Exception e) {
			MyLog.w("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.i("check DB Log For DateLock", e.getMessage());
			}
		}

		return !hasLogNotSynYet;
	}

	/**
	 * get notify sync data info
	 * @return
	 */
	public int getNotifySyncData() {
		int numDataNeedSync = 0;
		int sysMaxdayApprove = GlobalInfo.getInstance().getSysMaxdayApprove();
		int sysMaxdayReturn = GlobalInfo.getInstance().getSysMaxdayReturn();
		// tra don la min sysMaxdayReturn va sysMaxdayApprove
		sysMaxdayReturn = Math.min(sysMaxdayApprove, sysMaxdayReturn);
		// cong them so ngay nghi vao
		try {
			sysMaxdayReturn = SQLUtils
					.getInstance()
					.addCurrentWorkingDaysOfMonth(
							DateUtils.convertToDate(DateUtils.now(),
									DateUtils.DATE_FORMAT_NOW), sysMaxdayReturn);
		} catch (Exception e) {
		}
		String timeCheckToServer = "-" + sysMaxdayReturn + " fullDate";
		Cursor c = null;
		try {
			StringBuffer strBuffer = new StringBuffer();
			strBuffer.append(" select count(*) COUNT_DATA from LOG_TABLE ");
			strBuffer
					.append(" WHERE TABLE_TYPE != ? and STATE != ? and STATE != ? and STATE != ? and STATE != ?");
			strBuffer.append(" and STATE != ? and STATE != ? and STATE != ? ");
			// lay nhung log thoa man dieu kien min han tra, han duyet
			strBuffer
					.append(" and dayInOrder(CREATE_DATE) >=dayInOrder('now', 'localtime', ?) ");

			String[] params = { String.valueOf(LogDTO.TYPE_LOG), LogDTO.STATE_SUCCESS,
					LogDTO.STATE_INVALID_TIME, LogDTO.STATE_UNIQUE_CONTRAINTS,
					LogDTO.STATE_ORDER_DELETED, LogDTO.STATE_IMAGE_DELETED,
					LogDTO.STATE_WRONG_FORMAT, LogDTO.STATE_OTHER_NOT_RETRY,
					timeCheckToServer };
			c = rawQuery(strBuffer.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					numDataNeedSync = CursorUtil.getInt(c, "COUNT_DATA");
				}
			}
		} catch (Exception e) {
			MyLog.e("getLogNeedToCheck", "fail", e);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.i("getLogNeedToCheck", e.getMessage());
			}
		}

		return numDataNeedSync;
	}
}
