/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.dto.db.ProductDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin mot sku khi duoc ban, su dung cho man hinh thong ke don tong
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SaleProductInfoDTO {
	// thong tin cua san pham
	public ProductDTO productInfo;
	// so luong san pham
	public long numberProduct;
	// number product format
	public String numberProductFormat;
	public String numberProductFormatApproved;
	// tong tien da ban
	public double totalAmountSold;
	public double totalAmountSoldApproved;
	// so luong san pham ton kho dau ngay
	public long numProductRemainFirstDay;
	// so luong san pham ton kho dau ngay format
	public String numProductRemainFirstDayFormat;
	// so luong san pham con lai
	public long numProductRemain;
	// so luong san pham con lai format
	public String numProductRemainFormat;
	// so luong san pham dat them
	public long numProductAddOrder;
	// so luong san pham dat them format
	public String numProductAddOrderFormat;
	public int tongQuyDoi;

	// kho quy doi
	public long numTotalProductRemainFirstDay;
	public long numberTotalProduct;
	public long numTotalProductRemain;
	public double sumTotalAmountSold;


	/**
	 * struct
	 */
	public SaleProductInfoDTO() {
		productInfo = new ProductDTO();
		numberProduct = 0;
		totalAmountSold = 0;
		totalAmountSoldApproved = 0;
		numberProductFormat = "0/0";
		numberProductFormatApproved = "0/0";
		numProductRemainFirstDay = 0;
		numProductRemainFirstDayFormat = "0/0";
		numProductRemain = 0;
		numProductRemainFormat = "0/0";
		numProductAddOrder = 0;
		numProductAddOrderFormat = "0/0";
		tongQuyDoi = 0;
		numTotalProductRemainFirstDay = 0;
		numberTotalProduct = 0;
		numTotalProductRemain = 0;
		sumTotalAmountSold = 0;
	}

	/**
	 *
	 * init data for pre sale from cursor
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 19, 2012
	 */
	public void initDataPreSaleFromCursor(Cursor c) {
		productInfo.initDataFromCursor(c);
		numberProduct = CursorUtil.getInt(c, "PRODUCT_NUMBER") - CursorUtil.getInt(c, "DA_TRA");
		int daDuyet = 0;
		daDuyet = CursorUtil.getInt(c, "DA_DUYET");
		int daTra = 0;
		daTra = CursorUtil.getInt(c, "DA_TRA");

		tongQuyDoi = daDuyet - daTra;
		long a = numberProduct / productInfo.convfact;
		long b = numberProduct % productInfo.convfact;
		numberProductFormat = Long.toString(a) + "/" + Long.toString(b);

		a = tongQuyDoi / productInfo.convfact;
		b = tongQuyDoi % productInfo.convfact;
		numberProductFormatApproved = Long.toString(a) + "/" + Long.toString(b);

		daDuyet = CursorUtil.getInt(c, "TONG_DA_DUYET");
		daTra = CursorUtil.getInt(c, "TONG_DA_TRA");

		totalAmountSoldApproved = daDuyet - daTra;
		totalAmountSold = CursorUtil.getDouble(c, "AMOUNT_TOTAL") -  CursorUtil.getInt(c, "TONG_DA_TRA");
	}

	/**
	 *
	 * init data for van sale from cursor
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 17, 2013
	 */
	public void initDataVanSaleFromCursor(Cursor c) {
		productInfo.initDataFromCursor(c);
		long a = 0;
		long b = 0;
		//So luong con lai
		numProductRemain = CursorUtil.getInt(c, "TOTAL_PRODUCT_REMAIN");
		a = numProductRemain / productInfo.convfact;
		b = numProductRemain % productInfo.convfact;
		numProductRemainFormat = Long.toString(a) + "/" + Long.toString(b);
		//so da ban
		numberProduct = CursorUtil.getInt(c, "TOTAL_PRODUCT_SOLD");
		a = numberProduct / productInfo.convfact;
		b = numberProduct % productInfo.convfact;
		numberProductFormat = Long.toString(a) + "/" + Long.toString(b);

		// so luong ton kho dau ki
		numProductRemainFirstDay = numProductRemain + numberProduct;
		a = numProductRemainFirstDay / productInfo.convfact;
		b = numProductRemainFirstDay % productInfo.convfact;
		numProductRemainFirstDayFormat = Long.toString(a) + "/" + Long.toString(b);

		//so ban them
		numProductAddOrder = CursorUtil.getInt(c, "TOTAL_PRODUCT_ADD_ORDER");
		a = numProductAddOrder / productInfo.convfact;
		b = numProductAddOrder % productInfo.convfact;
		numProductAddOrderFormat = Long.toString(a) + "/" + Long.toString(b);
		//tong tien da ban
		totalAmountSold = CursorUtil.getDouble(c, "TOTAL_TOTAL_AMOUNT_SOLD",1, GlobalInfo.getInstance().getSysNumRounding());

		long daDuyet = 0;
		daDuyet = CursorUtil.getInt(c, "DA_DUYET");
		long daTra = 0;
		daTra = CursorUtil.getInt(c, "DA_TRA");

		long soLuongDuyet = daDuyet - daTra;
		a = soLuongDuyet / productInfo.convfact;
		b = soLuongDuyet % productInfo.convfact;

		numberProductFormatApproved = Long.toString(a) + "/" + Long.toString(b);

	}

}
