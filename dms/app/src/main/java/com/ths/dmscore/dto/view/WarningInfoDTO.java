/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

/**
 *  warning information
 *  @author: HaiTC3
 *  @version: 1.0
 *  @since: 1.0
 */
public class WarningInfoDTO {
	public String warningContent;
	
	public WarningInfoDTO(){
		warningContent = "";
	}
}
