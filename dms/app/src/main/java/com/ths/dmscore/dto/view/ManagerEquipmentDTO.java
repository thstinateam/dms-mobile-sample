package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class ManagerEquipmentDTO {
	public int totalList;	
	public String totalofTotal;
	
	public ArrayList<ManagerEquipmentItem> arrList;
	
	public ManagerEquipmentDTO(){
		arrList = new ArrayList<ManagerEquipmentDTO.ManagerEquipmentItem>();		
	}
	
	public void addItem(Cursor c){
		ManagerEquipmentItem item = new ManagerEquipmentItem();	
		item.id = CursorUtil.getString(c, "id");
		item.maNVBH = CursorUtil.getString(c, "maNVBH");
		item.nvbh = CursorUtil.getString(c, "nvbh");
		item.soThietBi = CursorUtil.getInt(c, "soThietBi");
		item.khongDat = CursorUtil.getInt(c, "khongDat");		
		arrList.add(item);
	}
	
	public class ManagerEquipmentItem{
		public String id;
		public String maNVBH;
		public String nvbh;	
		public int soThietBi;
		public int khongDat;		
		
		public ManagerEquipmentItem(){
			id = "1";
			maNVBH = "maNVBH";
			nvbh = "maNVBH";
			soThietBi = 1;
			khongDat = 1;					
		}
	}
}
