/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;

/**
 * SortActionConstants.java
 * @author: duongdt3
 * @version: 1.0
 * @since:  11:24:23 26 Mar 2015
 */
public class SortActionConstants {
	public static final int CODE = 1;
	public static final int NAME = 2;
	public static final int VISIT_STATE = 3;
	public static final int DAY_SEQ = 4;
	public static final int QUANITY = 5;
	public static final int AMOUNT = 6;
	public static final int CAT = 7;
	public static final int CONVFACT = 8;
	public static final int PRICE = 9;
	public static final int AMOUNT_PLAN = 10;
	public static final int AMOUNT_DONE = 11;
	public static final int AMOUNT_APPROVED = 12;
	public static final int AMOUNT_REMAIN = 13;
	public static final int QUANITY_PLAN = 14;
	public static final int QUANITY_DONE = 15;
	public static final int QUANITY_APPROVED = 16;
	public static final int QUANITY_REMAIN = 17;
	public static final int PRODUCT_CODE = 18;
	public static final int PRODUCT_NAME = 19;
	public static final int PRODUCT_STOCK_TOTAL = 20;
	public static final int PACKAGE_PRICE = 21;
	public static final int AVAILABLE_QUANTITY = 22;
	public static final int DATE = 23;
	public static final int PROMOTION = 24;
	public static final int IMAGE_COUNT = 25;
	public static final int LEVEL = 26;
	public static final int TYPE = 27;
	public static final int CODE_PROGRAM = 28;
	public static final int NAME_PROGRAM = 29;
	public static final int ADDRESS = 30;
	public static final int PERIOD = 31;
	public static final int KPI = 32;
	public static final int NUM_IMAGE = 33;
	public static final int FROM_DATE = 34;
	public static final int TO_DATE = 35;
	public static final int STAFF = 36;
	public static final int STATE = 37;
	public static final int CONTENT = 38;
	public static final int SUB_CAT = 39;
	public static final int RESULT = 40;
	public static final int REWARD = 41;
	public static final int AMOUNT_APPROVED_PRE = 42;
	public static final int QUANITY_APPROVED_PRE = 43;
	public static final int STAFF_CODE = 44;
	public static final int STAFF_NAME = 45;
	public static final int ROUTE_CODE = 46;
	public static final int ROUTE_NAME = 47;
	public static final int TOTAL = 48;
	
}
