/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class CycleInfo {
	public long cycleId;
	public String cycleCode;
	public String cycleName; 
	public String year;
	public int num;
	public String beginDate;
	public String endDate;
	public int status;
	
	public void initData(Cursor c){
		cycleId = CursorUtil.getLong(c, "CYCLE_ID");
		cycleCode = CursorUtil.getString(c, "CYCLE_CODE");
		cycleName = CursorUtil.getString(c, "CYCLE_NAME");
		year = CursorUtil.getString(c, "YEAR");
		num = CursorUtil.getInt(c, "NUM");
		beginDate = CursorUtil.getString(c, "BEGIN_DATE");
		endDate = CursorUtil.getString(c, "END_DATE");
		status = CursorUtil.getInt(c, "STATUS");
	}
}
