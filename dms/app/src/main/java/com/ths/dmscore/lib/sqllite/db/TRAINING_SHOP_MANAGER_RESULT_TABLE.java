/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.TrainingShopManagerResultDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.TBHVFollowProblemDTO;
import com.ths.dmscore.dto.view.TBHVFollowProblemItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * luu tru cac danh gia ve NVBH, ghi nhan gop y cua khach hang, ghi nhan cac SKU
 * can ban lan sau. Cac thong tin duoc ghi nhan trong qua trinh huan luyen NVBH
 * tren mot khach hang cu the
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class TRAINING_SHOP_MANAGER_RESULT_TABLE extends ABSTRACT_TABLE {

	// id bang
	public static final String ID = "ID";
	// ma training plan detail
	public static final String TRAINING_PLAN_DETAIL_ID = "TRAINING_PLAN_DETAIL_ID";
	// xac dinh loai du lieu cua dong tuong ung
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// chi co y nghia voi object type = 3
	public static final String STATUS = "STATUS";
	//
	public static final String REMIND_DATE = "REMIND_DATE";
	// ngay thuc hien
	public static final String DONE_DATE = "DONE_DATE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// noi dung
	public static final String CONTENT = "CONTENT";

	// ten table
	public static final String TABLE_NAME = "TRAINING_SHOP_MANAGER_RESULT";

	public TRAINING_SHOP_MANAGER_RESULT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { ID, TRAINING_PLAN_DETAIL_ID, OBJECT_TYPE,
				STATUS, REMIND_DATE, DONE_DATE, CREATE_DATE, UPDATE_DATE,
				CONTENT, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		ContentValues value = initDataRow((TrainingShopManagerResultDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * insert with TrainingResultDTO
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long insert(TrainingShopManagerResultDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		TrainingShopManagerResultDTO dtoDisplay = (TrainingShopManagerResultDTO) dto;
		ContentValues value = initDataRow(dtoDisplay);
		String[] params = { String.valueOf(dtoDisplay.ID) };
		return update(value, ID + " = ?", params);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		TrainingShopManagerResultDTO dtoDisplay = (TrainingShopManagerResultDTO) dto;
		String[] params = { "" + dtoDisplay.ID };
		return delete(ID + " = ?", params);
	}

	/**
	 *
	 * init data for row in DB
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDataRow(TrainingShopManagerResultDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(ID, dto.ID);
		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.trainingPlanDetailID);
		editedValues.put(OBJECT_TYPE, dto.objectType);
		editedValues.put(STATUS, dto.status);
		editedValues.put(REMIND_DATE, dto.remindDate);
		editedValues.put(DONE_DATE, dto.doneDate);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(CONTENT, dto.content);
		editedValues.put(SYN_STATE, dto.synState);
		return editedValues;
	}


	/**
	 *
	 * Lay ds van de theo GSNPP cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: FollowProblemDTO
	 * @throws:
	 */
	public TBHVFollowProblemDTO getListProblemOfTBHV(Bundle data) throws Exception {
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		String parentShopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> listShopId = shopTable.getShopRecursiveReverse(parentShopId);
		String strListShop = TextUtils.join(",", listShopId);

		String extPage = data.getString(IntentConstants.INTENT_PAGE);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID_PARA);
		String status = data.getString(IntentConstants.INTENT_STATE);
		String typeProblem = data.getString(IntentConstants.INTENT_TYPE_PROBLEM);
		String parentStaffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);

		String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		String listStaff = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);

		TBHVFollowProblemDTO result = new TBHVFollowProblemDTO();
		List<String> stringParams = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT FB.feedback_id   ID, ");
		sql.append("       FB.staff_id, ");
		sql.append("       S.staff_code, ");
		sql.append("       S.staff_name, ");
		sql.append("       FB.type, ");
		sql.append("       C.customer_code, ");
		sql.append("       C.customer_name, ");
		sql.append("       C.housenumber, ");
		sql.append("       C.street, ");
		sql.append("       FB.content       CONTENT, ");
		sql.append("       FB.create_date   CREATE_DATE, ");
		sql.append("       FB.remind_date   REMIND_DATE, ");
		sql.append("       FB.done_date     DONE_DATE, ");
		sql.append("       FB.num_return     NUM_RETURN, ");
		sql.append("       FB.status, ");
		sql.append("       (select ap_param_name from ap_param where ap_param_code = fb.type and type in ('FEEDBACK_TYPE_TBHV') and status = 1) DESCRIPTION ");
		sql.append("FROM   feedback FB, staff s ");
		sql.append("       LEFT JOIN customer C ");
		sql.append("              ON FB.customer_id = C.customer_id ");
		sql.append("WHERE  FB.staff_id = S.staff_id ");
		sql.append("       and fb.shop_id IN (" + strListShop + ") ");
		sql.append("       and fb.create_user_id = ? ");
		stringParams.add(parentStaffId);
		sql.append("       AND (C.shop_id is null OR (C.shop_id is not null AND C.shop_id IN (" + strListShop + ")))  ");
		//gs phai do ASM quan ly truc tiep
		sql.append("       and s.status = 1 ");
		sql.append("       and s.staff_id in ( ");
		sql.append(listStaff);
		sql.append("       ) ");

		if (!StringUtil.isNullOrEmpty(status)) {
			sql.append(" AND FB.STATUS = ?"); // loc theo status
			stringParams.add(status);
			// stringParams.add(status);
		} else {
			sql.append(" AND FB.STATUS IN (1,2,3)");
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sql.append(" AND DATE(FB.CREATE_DATE) >= DATE(?)");
			stringParams.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sql.append(" AND DATE(FB.CREATE_DATE) <= DATE(?)");
			stringParams.add(toDate);
		}

		if (!StringUtil.isNullOrEmpty(staffId)) {
			sql.append(" AND FB.STAFF_ID = ?");// loc theo GSNPP nao
			stringParams.add(staffId);
		}
		if (!StringUtil.isNullOrEmpty(typeProblem)) {
			sql.append("       AND FB.TYPE = ? ");
			stringParams.add(typeProblem);
		}
		sql.append(" ORDER BY FB.STATUS, FB.REMIND_DATE, FB.DONE_DATE desc, FB.CREATE_DATE desc, S.STAFF_NAME");

		String requestGetFollowProblemList = sql.toString();
		Cursor c = null;
		String[] params = new String[stringParams.size()];
		for (int i = 0, length = stringParams.size(); i < length; i++) {
			params[i] = stringParams.get(i);
		}

		Cursor cTmp = null;
		String getCountFollowProblemList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ requestGetFollowProblemList + ") ";

		try {
			if (!checkPagging) {
				cTmp = rawQuery(getCountFollowProblemList, params);
				if (cTmp != null) {
					cTmp.moveToFirst();
					result.total = cTmp.getInt(0);
				}
			}
			c = rawQuery(requestGetFollowProblemList + extPage, params);
			if (c != null) {
				if (c.moveToFirst()) {
					List<TBHVFollowProblemItemDTO> listFollow = new ArrayList<TBHVFollowProblemItemDTO>();
					do {
						TBHVFollowProblemItemDTO note = new TBHVFollowProblemItemDTO();
						note.initDateWithCursor(c);
						listFollow.add(note);
					} while (c.moveToNext());
					result.list = listFollow;
				} else {
					result.list = new ArrayList<TBHVFollowProblemItemDTO>();
				}
			}
		} catch (Exception e) {
			// return null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * Update status cua van de
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(STATUS, dto.status);
			String[] params = { String.valueOf(dto.id) };
			returnCode = update(editedValues, ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 *
	 * lay combobox trang thai cho man hinh theo doi khac phuc cua TBHV
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getComboboxProblemStatusOfTBHV() {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		return result;
	}

	/**
	 *
	*  get list reviews of tbhv
	*  @author: HaiTC3
	*  @param data
	*  @return
	*  @return: List<TrainingShopManagerResultDTO>
	*  @throws:
	 */
	public List<TrainingShopManagerResultDTO> getListReviewsOfTBHV(Bundle data) {
		List<TrainingShopManagerResultDTO> listReviews = new ArrayList<TrainingShopManagerResultDTO>();
		String trainingPlanDetailId = data.getString(IntentConstants.INTENT_TRAINING_DETAIL_ID);
		if (data != null) {
			if (data.getString(IntentConstants.INTENT_TRAINING_DETAIL_ID) != null) {
				trainingPlanDetailId = data
						.getString(IntentConstants.INTENT_TRAINING_DETAIL_ID);
			}
		}
		StringBuffer  sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT * ");
		sqlQuery.append("FROM   training_shop_manager_result ");
		sqlQuery.append("WHERE  training_plan_detail_id = ? ");
		sqlQuery.append("       AND status = 1 ");
		sqlQuery.append("       AND ( Date(create_date) IS NULL ");
		sqlQuery.append("              OR Date(create_date) = (SELECT Date('NOW', 'localtime')) ) ");

		Cursor c = null;
		String[] paramsList = new String[] {trainingPlanDetailId};

		try {
			c = this.rawQuery(sqlQuery.toString(), paramsList);
			if (c != null && c.moveToFirst()) {
				do {
					TrainingShopManagerResultDTO item = new TrainingShopManagerResultDTO();
					item.parserDataFromCursor(c);
					listReviews.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listReviews;
	}
}