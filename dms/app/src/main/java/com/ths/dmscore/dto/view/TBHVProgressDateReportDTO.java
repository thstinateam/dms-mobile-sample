package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 *
 *  DTO cua man hinh bao cao ngay cua TBHV
 *  @author: Nguyen Huu Hieu
 *  @version: 1.1
 *  @since: 1.0
 */
public class TBHVProgressDateReportDTO implements Serializable {
	// noi dung field
	private static final long serialVersionUID = 1L;
	public int totalList;
	public String totalofTotal;

	public ArrayList<TBHVProgressDateReportItem> arrList;

	public TBHVProgressDateReportDTO() {
		arrList = new ArrayList<TBHVProgressDateReportDTO.TBHVProgressDateReportItem>();
	}

	public void addItem(Cursor c, int sysCurrencyDevide) {
		TBHVProgressDateReportItem item = new TBHVProgressDateReportItem();
		item.idNPP = CursorUtil.getString(c, "ID_NPP");
		item.tenNPP = CursorUtil.getString(c, "TEN_NPP");
		item.tenGSNPP = CursorUtil.getString(c, "TEN_GSNPP");
		item.maNPP = CursorUtil.getString(c, "MA_NPP");
		item.maGSNPP = CursorUtil.getString(c, "MA_GSNPP");
		item.idGSNPP = CursorUtil.getString(c, "ID_GSNPP");
		item.shopSpecificType = CursorUtil.getInt(c, "SHOP_SPECIFIC_TYPE");
		item.tenNV = CursorUtil.getString(c, "TEN_NV");
		item.maNV = CursorUtil.getString(c, "MA_NV");
		item.idNV = CursorUtil.getString(c, "ID_NV");

		item.amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_PLAN");
		item.amountDone = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT");
		item.amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_APPROVED");
		item.amountPending = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_PENDING");
		item.amountRemain = item.amountPlan - item.amountDone;
		if (item.amountRemain < 0) {
			item.amountRemain = 0.0;
		}

		item.quantityPlan = CursorUtil.getInt(c, "DAY_QUANTITY_PLAN");
		item.quantityDone = CursorUtil.getInt(c, "DAY_QUANTITY");
		item.quantityApproved = CursorUtil.getInt(c, "DAY_QUANTITY_APPROVED");
		item.quantityPending = CursorUtil.getInt(c, "DAY_QUANTITY_PENDING");
		item.quantityRemain = item.quantityPlan - item.quantityDone;
		if (item.quantityRemain < 0) {
			item.quantityRemain = 0;
		}
//		if (c.getColumnIndex("SKU_THUC_HIEN_AMOUNT") > -1) {
//			float skuSum = c.getFloat(c.getColumnIndex("SKU_THUC_HIEN_AMOUNT"));
//			float skuCount = c
//					.getFloat(c.getColumnIndex("SKU_THUC_HIEN_COUNT"));
//			item.skuThucHien = skuSum / skuCount;
//			skuSum = c.getFloat(c.getColumnIndex("SKU_KE_HOACH_SUM"));
//			skuCount = c.getFloat(c.getColumnIndex("SKU_KE_HOACH_COUNT"));
//			item.skuPlan = skuSum / skuCount;
//		}else{
//			item.skuPlan = c.getFloat(c.getColumnIndex("SKU_KE_HOACH"));
//			item.skuThucHien = c.getFloat(c.getColumnIndex("SKU_THUC_HIEN"));
//		}
		arrList.add(item);
	}

	public class TBHVProgressDateReportItem implements Serializable {
		// noi dung field
		private static final long serialVersionUID = 1L;
		public String idNPP;
		public String idGSNPP;
		public String tenNPP;
		public String maNPP;
		public String tenGSNPP;
		public String maGSNPP;
		public String tenNV;
		public String maNV;
		public String idNV;
		public int shopSpecificType;// loai don vi
		public float skuPlan;
		public float skuThucHien;
		public Double amountPlan;
		public Double amountDone;
		public Double amountApproved;
		public Double amountPending;

		public Double amountRemain;
		public int quantityPlan;
		public int quantityDone;
		public int quantityApproved;
		public int quantityRemain;
		public int quantityPending;
		public String id;
		public int type = ApParamDTO.SYS_SALE_STAFF;
		public TBHVProgressDateReportItem() {
			tenNPP = "";
			tenGSNPP = "";
			skuPlan = 0;
			skuThucHien = 0;
			amountPlan = 0.0;
			amountDone = 0.0;
			amountApproved = 0.0;
			amountRemain = 0.0;
			amountPending = 0.0;
			quantityPlan = 0;
			quantityDone = 0;
			quantityApproved = 0;
			quantityRemain = 0;
			quantityPending = 0;
		}
	}
}
