/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;

/**
 * Menu item dang: Icon + text, action
 * @author : BangHN since : 11:29:38 AM version :
 */
public class MenuItem extends LinearLayout implements View.OnClickListener {

	public OnEventControlListener listener;
	public View view;
	// icon
	public ImageView ivIconMenu;
	// icon phan cach
	public ImageView ivSeparate;
	// text menu
	public TextView tvTitle;
	private boolean isMenuSelected = false;
	private int action;

	public MenuItem(Context context, int iconRscId, final int action) {
		super(context);
		initMenuItem(context, "", iconRscId, action);
	}


	public MenuItem(Context context, String text, int iconRscId, final int action) {
		super(context);
		initMenuItem(context, text, iconRscId, action);
	}

	public MenuItem(final int action, int menuID, Context context, String text) {
		super(context);
		initMenuItem(action, menuID, context, text);
	}

	public void setSeparateVisible(int visible){
		ivSeparate.setVisibility(visible);
	}

	private void initMenuItem(Context context, String text, int iconRscId, final int action) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.layout_menu_bar_item, this);
		ivIconMenu = (ImageView) view.findViewById(R.id.ivIconMenu);
		ivSeparate = (ImageView) view.findViewById(R.id.ivSeparate);
		tvTitle = (TextView) view.findViewById(R.id.tvTitle);

		this.action = action;
		if(StringUtil.isNullOrEmpty(text)){
			tvTitle.setVisibility(View.GONE);
		}else{
			tvTitle.setText(text);
			tvTitle.setVisibility(View.VISIBLE);
		}

		ivIconMenu.setImageResource(iconRscId);
		view.setOnClickListener(this);
		tvTitle.setOnClickListener(this);
	}

	 /**
	 * Tao menu dong
	 * @author: dungdq3
	 * @param action
	 * @param menuID
	 * @param context
	 * @param text
	 * @return: void
	 * @throws:
	*/
	private void initMenuItem(final int action, final int menuID, Context context, String text) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.layout_menu_bar_item, this);
		ivIconMenu = (ImageView) view.findViewById(R.id.ivIconMenu);
		ivSeparate = (ImageView) view.findViewById(R.id.ivSeparate);
		tvTitle = (TextView) view.findViewById(R.id.tvTitle);
		
		this.action = action;
		if(StringUtil.isNullOrEmpty(text)){
			tvTitle.setVisibility(View.GONE);
		}else{
			tvTitle.setText(text);
			tvTitle.setVisibility(View.VISIBLE);
		}

		ivIconMenu.setVisibility(GONE);
		view.setOnClickListener(this);
		tvTitle.setOnClickListener(this);
	}

	public void setOnEventControlListener(OnEventControlListener lis) {
		listener = lis;
	}
	
	/**
	 * set menu is selected
	 * @author: duongdt3
	 * @since: 15:09:37 24 Mar 2015
	 * @return: void
	 * @throws:  
	 * @param b
	 */
	public void setMenuSelected(boolean isMenuSelected) {
		this.isMenuSelected = isMenuSelected;
		if (this.isMenuSelected) {
			this.setBackgroundResource(R.drawable.bg_vnm_header);
		}
	}

	@Override
	public void onClick(View v) {
		//chi goi event khi khong phai menu nay dang duoc chon
		if(!MenuItem.this.isMenuSelected){
			listener.onEvent(action, MenuItem.this, null);
		}
	}
}
