/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.content.Context;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dms.R;

/**
 * Row muc chuong trinh
 * @author: yennth16
 * @version: 1.0
 * @since:  15:36:38 13-07-2015
 */
public class KeyShopLevelRow extends DMSTableRow {
	public TextView tvStt;
	public TextView tvKeyshopLevelCode;
	public TextView tvKeyshopLevelName;
	public TextView tvAmount;
	public TextView tvQuantity;

	public KeyShopLevelRow(Context context) {
		super(context, R.layout.layout_key_shop_level, GlobalInfo.getInstance()
				.isSysShowPrice() ? null : new int[] { R.id.tvAmount });
		setOnClickListener(this);
		tvStt = (TextView) findViewById(R.id.tvStt);
		tvKeyshopLevelCode = (TextView) findViewById(R.id.tvKeyshopLevelCode);
		tvKeyshopLevelName = (TextView) findViewById(R.id.tvKeyshopLevelName);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
	}

	/**
	 * render
	 * @author: yennth16
	 * @since: 15:42:49 13-07-2015
	 * @return: void
	 * @throws:
	 * @param historyItem
	 */
	public void render(KSLevelDTO item, int position) {
		tvStt.setText("" + position);
		tvKeyshopLevelCode.setText(item.ksLevelCode);
		tvKeyshopLevelName.setText(item.name);
		long amountDone = 0;
		long quantityDone = 0;
		amountDone = item.amount;
		quantityDone = item.quantity;
		display(tvAmount, amountDone);
		display(tvQuantity, quantityDone);
	}
}