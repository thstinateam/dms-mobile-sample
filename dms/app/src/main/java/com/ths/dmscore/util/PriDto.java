package com.ths.dmscore.util;

import java.io.Serializable;

import android.view.View;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class PriDto implements Serializable {
	private static final long serialVersionUID = -6712891558827211963L;
	public View control;
	PriControlStatus controlStatus;
	
	public PriDto(View control, int status){
		this.control = control;
		controlStatus = new PriControlStatus();
		controlStatus.status = status;
	}
}
