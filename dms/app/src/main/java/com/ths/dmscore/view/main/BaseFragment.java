/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.AbstractController;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.db.ProductDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.RightHeaderMenuBar;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.sale.order.ProductInfoDetailView;
import com.ths.dmscore.view.sale.order.PromotionDetailListView;
import com.ths.dmscore.view.supervisor.keyshop.KeyShopDetailView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Base fragment
 * Xu ly cac phan chung: header, handle du lieu tu controller...
 * @author banghn
 */
public class BaseFragment extends Fragment implements OnClickListener, OnEventControlListener {
	//activity chua view
	protected GlobalBaseActivity parent;
	//view root chua header & view fragment con
	LinearLayout viewRoot;
	//header trong fragment
	protected View rlHeader;
	//header title
	private TextView tvTitle;
	//menu icon trong view fragment
	public RightHeaderMenuBar menuBar;
	//kiem tra fragment duoc finish, detroy chua
	public boolean isFinished = false;
	// dialog product detail view
	AlertDialog alertPromotionDetail;

	//PromotionProgrameDetailView promotionDetailView;

	// dialog product detail view
	AlertDialog alertProductInfoDetail;

	ProductInfoDetailView productInfoDetailView;

	//current level manager fragment
	private int managerLevelCurrent = 0;
	private boolean isManagerFragment = false;
	private int switchAction = 0;
	// dialog product ds detail CTKM view
	AlertDialog alertPromotionDetailList;
	PromotionDetailListView promotionDetailListView;
	// MH chi tiet van de
	KeyShopDetailView keyShopDetailView;
		// dialog product detail view
	AlertDialog alertKeyShopDetail;
	private boolean isSysShowPrice = GlobalInfo.getInstance().isSysShowPrice();
	private int isSysCurrencyDivide = GlobalInfo.getInstance().getSysCurrencyDivide();
	private String sysCurrency = GlobalInfo.getInstance().getSysCurrency();
	
	// bien do log KPI
	//public Calendar startTimeKPI = null;
	//public Calendar endTimeKPI;
	// bien de kiem tra fragment phai duoc load dau tien hay ko

	BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int action = intent.getExtras().getInt(Constants.ACTION_BROADCAST);
			int hasCode = intent.getExtras().getInt(
					Constants.HASHCODE_BROADCAST);
			if (hasCode != BaseFragment.this.hashCode()) {
				receiveBroadcast(action, intent.getExtras());

				// xu li GsnppTrainingResultDayReportView trong backstack
//				GsnppTrainingResultDayReportView cus = (GsnppTrainingResultDayReportView)
//						findFragmentByTag(GlobalUtil.getTag(GsnppTrainingResultDayReportView.class));
//				if (cus != null) {
//					cus.receiveBroadcast(action, intent.getExtras());
//				}
			}
		}
	};
	public boolean isZooming;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			IntentFilter filter = new IntentFilter(SalesPersonActivity.VNM_ACTION);
			parent.registerReceiver(receiver, filter);
		} catch (Exception e) {
		}
	}

	@Override
	public void onAttach(Activity activity) {
		this.parent = (GlobalBaseActivity) activity;
		super.onAttach(activity);
	}

	@Override
	public void onResume() {
		isFinished = false;
		// kiem tra status cua form va ds cac control cua form
		int formStatus = PriUtils.getInstance().getFormStatus();
		if(formStatus != 1){
			// neu status cua form != 1 thi chuyen sang man hinh khac / hoac back ra man hinh truoc
			// remove all view khi khong co quyen truy cap
			wrongPri(1);
		}
		super.onResume();

		if (parent != null) {
			parent.hideKeyboardCustom();
		}
	}

	@Override
	public void onDestroyView() {
		//GlobalUtil.nullViewDrawablesRecursive(viewRoot);
		//isFinished = true;
		try {
			getActivity().unregisterReceiver(receiver);
		} catch (Exception e) {
		}
		super.onDestroyView();
	}

	@Override
	public void onDestroy() {
		isFinished = true;
		try {
			getActivity().unregisterReceiver(receiver);
		} catch (Exception e) {
		}
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_fragment_base, null,
				false);
		viewRoot = (LinearLayout) view.findViewById(R.id.llMain);
		viewRoot.addView(container);
		isFinished = false;
		if (container != null){
			container.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					GlobalUtil.forceHideKeyboard(parent, true);
					return false;
				}
			});
		}
		initHeaderview();
		return view;
	}

	/**
	 * Khoi tao add header view
	 * @author : BangHN
	 * since : 4:30:53 PM
	 */
	private void initHeaderview() {
		LayoutInflater inflater = (LayoutInflater) parent
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		rlHeader = inflater.inflate(R.layout.layout_fragment_header, null);
		viewRoot.addView(rlHeader, 0);
		tvTitle = (TextView)rlHeader.findViewById(R.id.tvTitle);
		rlHeader.setVisibility(View.GONE);
	}

	/**
	 * An header
	 * @author: yennth16
	 * @since: 11:19:33 18-05-2015
	 * @return: void
	 * @throws:
	 */
	public void hideHeaderview() {
		//rlHeader.setVisibility(View.GONE);
	}


	/**
	 * set title header view (fragment)
	 * @author : BangHN
	 * since : 4:33:48 PM
	 */
	protected void setTitleHeaderView(String title) {
		if(tvTitle != null){
			tvTitle.setText(title);
			tvTitle.setSelected(true);
		}
	}

	protected void setTitleHeaderView(SpannableObject title) {
		if (tvTitle != null) {
			tvTitle.setText(title.getSpan());
			tvTitle.setSelected(true);
		}
	}
	protected void setTitleHeaderView(SpannableString title) {
		if (tvTitle != null) {
			tvTitle.setText(title);
			tvTitle.setSelected(true);
		}
	}

	/**
	 * Send broadcast toi cac fragment khac
	 * @author : BangHN
	 * since : 4:29:24 PM
	 */
	public void sendBroadcast(int action, Bundle bundle) {
		Intent intent = new Intent(SalesPersonActivity.VNM_ACTION);
		bundle.putInt(Constants.ACTION_BROADCAST, action);
		bundle.putInt(Constants.HASHCODE_BROADCAST, intent.getClass()
				.hashCode());
		intent.putExtras(bundle);
		parent.sendBroadcast(intent, GlobalBaseActivity.BROACAST_PERMISSION);
	}


	/**
	 * Nhan broadcast tu cac view fragment khac
	 * @author : BangHN
	 * since : 4:29:39 PM
	 */
	protected void receiveBroadcast(int action, Bundle extras) {
		// TODO Auto-generated method stub
//		GsnppTrainingResultDayReportView cus = (GsnppTrainingResultDayReportView) getFragmentManager()
//				.findFragmentByTag(GsnppTrainingResultDayReportView.TAG);
//		if (cus != null) {
//			cus.receiveBroadcast(action, extras);
//		}
	}


	/**
	 * Nhan handle modle tu controller
	 * @author : BangHN
	 * since : 4:29:55 PM
	 */
	@SuppressWarnings("unchecked")
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_PROMOTION_DETAIL_KEYSHOP:
			KeyShopItemDTO item = (KeyShopItemDTO) modelEvent.getModelData();
			showKeyShopDetail(item);
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL: {//hien thi thong tin chi tiet khuyen mai
			ArrayList<PromotionProgrameDTO> promotionInfo = (ArrayList<PromotionProgrameDTO>) modelEvent
					.getModelData();
			if(promotionInfo!= null && promotionInfo.size() == 1){
				PromotionProgrameDTO promotion = promotionInfo.get(0);
				showPromotionDetailView(promotion);
			}else{
				showPromotionDetailListView(promotionInfo);
			}
			parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.GO_TO_PRODUCT_INFO_DETAIL: {//hien thi thong tin chi tiet san pham
			ProductDTO promotionInfo = (ProductDTO) modelEvent
					.getModelData();
			showProductInfoDetailView(promotionInfo);
			break;
		}
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER: {
			// tam thoi chua lam j ca
			break;
		}
		default:
			break;
		}
	}


	/**
	 * Xu ly luong loi khi nhan tu controller
	 * @author : BangHN
	 * since : 4:30:11 PM
	 */
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER: {
			// neu dong bo online ma thoi gian bi loi, thi thong bao loi va hien thi setting
			if(modelEvent.getModelData() == null){
				return;
			}
			int res = (Integer)modelEvent.getModelData();
			if (res == ModelEvent.MODEL_RESULT_FAIL_TIME_ONLINE){
				AlertDialog alertDialog = null;
				try {
					alertDialog = new AlertDialog.Builder(parent).create();
					alertDialog.setMessage(StringUtil.getString(R.string.ERROR_TIME_OFFLINE_INVALID));
					alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
									// hien thi man hinh setting
									startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), "");
									return;

								}
							});
					alertDialog.show();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
				}
			}
			break;
		}
		case ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL:{
			parent.closeProgressDialog();
			break;
		}
		default:
			parent.handleErrorModelViewEvent(modelEvent);
			break;
		}

	}


	/**
	 * Enable menu bar icon
	 * @author : TamPQ
	 * since : 4:30:24 PM
	 */
	public void enableMenuBar(OnEventControlListener listener) {
		menuBar = new RightHeaderMenuBar(parent);
		menuBar.setOnEventControlListener(listener);
		LinearLayout llMenuBar = (LinearLayout) rlHeader
				.findViewById(R.id.ll_menubar);
		llMenuBar.removeAllViews();
		llMenuBar.addView(menuBar);
	}

	/**
	 * Add icon menu
	 * @author : TamPQ
	 * since : 4:30:39 PM
	 */
	public void addMenuItem(int drawableResId, int action) {
		menuBar.addMenuItem(drawableResId, action, View.VISIBLE);
	}

//	public void addMenuItem(String text, int drawableResId, int action) {
//		menuBar.addMenuItem(text, drawableResId, action, View.VISIBLE);
//	}

	public int addMenuItemPri(String text, int drawableResId, int action, PriHashMap.PriForm menu) {
		return menuBar.addMenuItem(text, drawableResId, action, View.VISIBLE, menu.getFormName());
	}

	public int addMenuItemPri(String text, int drawableResId, int action, PriHashMap.PriForm menu, int separateVisible) {
		return menuBar.addMenuItem(text, drawableResId, action, separateVisible, menu.getFormName());
	}

	public void addMenuItem(int action, String text, int menuID) {
		menuBar.addMenuItem(text, action, menuID);
	}

	public void addMenuItem(int focus, MenuTab ...tabs) {
		int numRemoved = 0;
		for (MenuTab menuTab : tabs) {
			numRemoved += addMenuItemPri(menuTab.text, menuTab.drawableResId, menuTab.action, menuTab.menu);

		}
		if ((tabs.length - focus) - numRemoved > 0) {
			setMenuItemFocus((tabs.length - focus) - numRemoved);
		} else {
			// remove all view khi khong co quyen truy cap
			if(this.getView() != null && this.getView() instanceof ViewGroup){
				ViewGroup g = (ViewGroup) this.getView();
				g.removeAllViews();
			}

			// thong bao khong co quyen vao menu
			parent.showDialog(StringUtil.getString(R.string.TEXT_PRI_WRONG_TAB));
		}
	}

	int focus = -1;

	public void addMenuItem(PriHashMap.PriForm menuFocus, MenuTab... tabs) {
		// dmscore ko add menu
//		int numRemoved = 0;
//		boolean add = true;
//
//		for (int i = 0, count = tabs.length; i < count; i++) {
//			MenuTab menuTab = tabs[i];
//			if (add) {
//				numRemoved += addMenuItemPri(menuTab.text,
//						menuTab.drawableResId, menuTab.action, menuTab.menu,
//						menuTab.separateVisible);
//				if(menuFocus == menuTab.menu){
//					add = false;
//				}
//			} else {
//				addMenuItemPri(menuTab.text,
//						menuTab.drawableResId, menuTab.action, menuTab.menu,
//						menuTab.separateVisible);
//			}
//			if (focus == -1 && menuFocus == menuTab.menu) {
//				if (PriUtils.getInstance().checkMenu(menuFocus.getFormName()) == PriUtils.ENABLE) {
//					focus = i + 1;
//				} else {
//
//				}
//			}
//		}
//		if (focus - numRemoved > 0) {
//			setMenuItemFocus(focus - numRemoved);
//		} else {
//			// remove all view khi khong co quyen truy cap
//			wrongPri(2);
//		}
	}

	public class MenuTab{
		public String text;
		public int drawableResId;
		public int action;
		public PriHashMap.PriForm menu;
		public int separateVisible;

		public MenuTab(String text, int drawableResId, int action, PriHashMap.PriForm menu){
			this.text = text;
			this.drawableResId = drawableResId;
			this.action = action;
			this.menu = menu;
			this.separateVisible = View.VISIBLE;
		}

		public MenuTab(int stringId, int drawableResId, int action, PriHashMap.PriForm menu){
			init(stringId, drawableResId, action, menu, View.VISIBLE);
		}

		public MenuTab(int stringId, int drawableResId, int action, PriHashMap.PriForm menu, int separateVisible){
			init(stringId, drawableResId, action, menu, separateVisible);
		}

		private void init(int stringId, int drawableResId, int action,
						  PriHashMap.PriForm menu, int separateVisible) {
			this.text = StringUtil.getString(stringId);
			this.drawableResId = drawableResId;
			this.action = action;
			this.menu = menu;
			this.separateVisible = separateVisible;
		}
	}

	/**
	 * Add menu icon kem theo co hien thi dau ngan cach hay khong (GONE || VISIBLE)
	 * @author : BangHN
	 * since : 9:20:11 AM
	 */
	public void addMenuItem(int drawableResId, int action, int separateVisible) {
		menuBar.addMenuItem(drawableResId, action, separateVisible);
	}

	public void addMenuItem(String text, int drawableResId, int action, int separateVisible) {
		menuBar.addMenuItem(text, drawableResId, action, separateVisible);
	}

	public void setMenuItemFocus(int index){
		menuBar.setMenuItemFocus(index);
	}

	public void removeAllMenuItem(){
		if(menuBar != null)
			menuBar.removeAllMenuItem();
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
//		if(promotionDetailView != null && v == promotionDetailView.btClosePopupPromotionDetail){
//			alertPromotionDetail.dismiss();
//		}
		if(productInfoDetailView != null && v == productInfoDetailView.btCloseProductInfoDetail){
			alertProductInfoDetail.dismiss();
		}
		if (keyShopDetailView != null && keyShopDetailView.btCloseKeyshopDetail == v) {
			if (alertKeyShopDetail != null) {
				alertKeyShopDetail.dismiss();
			}
		}
	}


	/**
	 * show thong tin chi tiet chuong trinh khuyen mai
	 * @author: HaiTC3
	 * @param program
	 * @return: void
	 * @throws:
	 */
	public void showPromotionDetailView(PromotionProgrameDTO program) {
		if (alertPromotionDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
//			promotionDetailView = new PromotionProgrameDetailView(parent,
//					this);
//			build.setView(promotionDetailView.viewLayout);
			alertPromotionDetail = build.create();
			Window window = alertPromotionDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
	//	promotionDetailView.updateLayout(program);
		alertPromotionDetail.show();
	}


	 /**
	 * Show ds CTKM chi tiet
	 * @author: Tuanlt11
	 * @param program
	 * @return: void
	 * @throws:
	*/
	public void showPromotionDetailListView(ArrayList<PromotionProgrameDTO> program) {
		if (alertPromotionDetailList == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			promotionDetailListView = new PromotionDetailListView(parent,this);
			build.setView(promotionDetailListView.viewLayout);
			alertPromotionDetailList = build.create();
			Window window = alertPromotionDetailList.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		promotionDetailListView.renderLayout(program);
		alertPromotionDetailList.show();
	}

	/**
	 * Hien thi popup chi tiet san pham
	 * @author: ThanhNN8
	 * @param product
	 * @return: void
	 * @throws:
	 */
	public void showProductInfoDetailView(ProductDTO product) {
		if (alertProductInfoDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			productInfoDetailView = new ProductInfoDetailView(parent,
					this);
			build.setView(productInfoDetailView.viewLayout);
			alertProductInfoDetail = build.create();
			Window window = alertProductInfoDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		productInfoDetailView.updateLayout(product);
		alertProductInfoDetail.show();
	}

	/**
	 * Request get thong tin chi tiet chuong trinh
	 * @author: HaiTC3
	 * @param promotionCode
	 * @return: void
	 * @throws:
	 */
	public void requestGetPromotionDetail(String promotionCode,String promotionId) {
		// go to promotion detail
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle dto = new Bundle();
		dto.putString(IntentConstants.INTENT_PROMOTION_CODE, promotionCode);
		dto.putString(IntentConstants.INTENT_PROMOTION_ID, promotionId);
		dto.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(dto, ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL, SaleController.getInstance());
	}

	/**
	 * Request lay thong tin chi tiet cua san pham
	 * @author: ThanhNN8
	 * @param productId
	 * @return: void
	 * @throws:
	 */
	public void requestGetProductInfoDetail(String productId) {
		// go to promotion detail
		Bundle dto = new Bundle();
		dto.putString(IntentConstants.INTENT_PRODUCT_ID, productId);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = dto;
		e.action = ActionEventConstant.GO_TO_PRODUCT_INFO_DETAIL;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * Remove fragment ra khoi stack
	 * @author: PhucNT
	 * @param promotionCode
	 * @return: void
	 * @throws:
	 */
	public void removeFragmentFromBackStack() {
		// go to promotion detail
		FragmentManager fm = this.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		fm.popBackStack(this.getClass().getName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
		ft.commit();
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
//		if (eventType == PromotionProgrameDetailView.CLOSE_POPUP_DETAIL_PROMOTION)
//			alertPromotionDetail.dismiss();
//		else
		if(eventType == ActionEventConstant.ACTION_CLOSE_POPUP){
			if(alertPromotionDetailList!= null)
				alertPromotionDetailList.dismiss();
		}
	}

	/**
	 * Lay thoi gian hien tai tren server
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void getCurrentDateTimeServer(ActionEvent actionEventBeforeGetTime) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putBoolean(IntentConstants.INTENT_IS_SYNC, false);
		e.viewData = bundle;
		e.sender = this;
		e.userData = actionEventBeforeGetTime;
		e.action = ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER;
		UserController.getInstance().handleViewEvent(e);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}


	public void closePopup() {

	}

	/**
	 * Ghi log KPI
	 * @author: banghn
	 * @param kpi
	 * @param startTimeKPI
	 */
	public void requestInsertLogKPI(HashMapKPI kpi, Calendar startTimeKPI) {
		int valueKey = GlobalInfo.getInstance().getHashMapKPI().get(kpi.ordinal());
		Calendar endTimeKPI = Calendar.getInstance();
		if (startTimeKPI != null
				&& endTimeKPI != null
				&& GlobalInfo.getInstance().getProfile().getUserData().getEnableClientLog() == 1
				&& valueKey < GlobalInfo.getInstance().getAllowRequestLogKPINumber()
				&& GlobalInfo.getInstance().getStateSynData() != GlobalInfo.SYNDATA_EXECUTING) {
			GlobalUtil.requestInsertLogKPI(kpi, startTimeKPI, endTimeKPI, "");
		}
	}

	/**
	 * Ghi log KPI
	 * @author: banghn
	 * @param kpi
	 * @param e
	 */
	public void requestInsertLogKPI(HashMapKPI kpi, ActionEvent e){
		int valueKey = GlobalInfo.getInstance().getHashMapKPI().get(kpi.ordinal());
		Calendar endTimeKPI = Calendar.getInstance();
		if (e.startTimeKPI != null
				&& endTimeKPI != null
				&& GlobalInfo.getInstance().getProfile().getUserData().getEnableClientLog() == 1
				&& valueKey < GlobalInfo.getInstance().getAllowRequestLogKPINumber()) {
			String kpiNote = "";

			//them thoi gian cho khi start AsynTask
			long waitTime = e.startTimeFromBootActive - e.startTimeFromBoot;
			if (waitTime != 0) {
				kpiNote += "WAIT_TIME: " + waitTime;
			}

			GlobalUtil.requestInsertLogKPI(kpi, e.startTimeKPI, endTimeKPI, kpiNote);
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	protected void wrongPri(int from){
		// from = 1: form chua phan quyen
		// from = 2: tab chua phan quyen
		if(this.getView() != null && this.getView() instanceof ViewGroup){
			ViewGroup g = (ViewGroup) this.getView();
			View header = g.findViewById(R.id.rl_header);
			for (int i = 0; i < g.getChildCount(); i++) {
				if (g.getChildAt(i) != header) {
					g.removeViewAt(i);
				}
			}
		}
		if (from == 1) {
			// thong bao khong co quyen vao menu
			parent.showDialog(StringUtil.getString(R.string.TEXT_PRI_WRONG_FORM));
			parent.closeProgressDialog();
		} else if(from ==2){
			// thong bao khong co quyen vao menu
			parent.showDialog(StringUtil.getString(R.string.TEXT_PRI_WRONG_TAB));
			parent.closeProgressDialog();
		}
	}

	/**
	 * switch fragment
	 *
	 * @author: dungdq3
	 * @since: 8:57:15 AM Oct 7, 2014
	 * @return: void
	 * @throws:
	 * @param b
	 * @param action
	 * @param abstractController:
	 */
	protected void handleSwitchFragment(Bundle b, int action, AbstractController controller) {
		Bundle bundle = (b == null ? new Bundle() : b);
		boolean isValid = true;

		//check manager fragment
		if (isManagerFragment) {
			//check same action
			if (action == switchAction) {
				//chua het level manager view cho phep thi cho chuyen view
				if (!isOutManagerLevel()) {
					isValid = true;
					// tang level cho view sau
					bundle.putInt(IntentConstants.INTENT_MANAGER_LEVEL, managerLevelCurrent + 1);
				} else{
					isValid = false;
				}
			}

			MyLog.d("handleSwitchFragment isManagerFragment", "isValid: " + isValid);
		}

		//check before send action
		if (isValid) {
			//send action
			ActionEvent e = new ActionEvent();
			e.sender = this;
			e.viewData = bundle;
			e.action = action;
			controller.handleSwitchFragment(e);
		}
	}

	/**
	 * get bundle request from view
	 * @author: duongdt3
	 * @since: 15:45:16 3 Apr 2015
	 * @return: Bundle
	 * @throws:
	 * @param b
	 * @return
	 */
	private Bundle getBundleRequest(Bundle b){
		Bundle bundle = (b == null ? new Bundle() : b);
		//add system var
		bundle.putInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED,
				GlobalInfo.getInstance().getSysCalUnapproved());
		bundle.putInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE,
				GlobalInfo.getInstance().getSysCurrencyDivide());
		return bundle;
	}

	/**
	 * handle model event
	 *
	 * @author: dungdq3
	 * @since: 9:00:28 AM Oct 7, 2014
	 * @return: void
	 * @throws:
	 * @param b
	 * @param action
	 * @param abs:
	 */
	protected void handleViewEvent(Bundle b, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = getBundleRequest(b);
		e.action = action;
		abs.handleViewEvent(e);
	}

	protected void handleViewEvent(Vector vt, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = vt;
		e.action = action;
		abs.handleViewEvent(e);
	}

	/**
	 * handle model event with tag
	 *
	 * @author: dungdq3
	 * @since: 9:00:28 AM Oct 7, 2014
	 * @return: void
	 * @throws:
	 * @param b
	 * @param action
	 * @param abs:
	 */
	protected void handleViewEventWithTag(Bundle b, int action, AbstractController abs, int tag) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = getBundleRequest(b);
		e.action = action;
		e.tag = tag;
		abs.handleViewEvent(e);
	}

	 /**
	 * Xu li event tu view ma ko xai asyntaks
	 * @author: Tuanlt11
	 * @param b
	 * @param action
	 * @param abs
	 * @param tag
	 * @return: void
	 * @throws:
	*/
	protected void handleViewEventWithOutAsyntask(Bundle b, int action, AbstractController abs) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = getBundleRequest(b);
		e.action = action;
		e.isUsingAsyntask = false;
		abs.handleViewEvent(e);
	}

	 /**
	 * Xu li event tu view xuong co
	 * Co check them thoi gian server hay ko
	 * @author: Tuanlt11
	 * @param b
	 * @param action
	 * @param abs
	 * @param isCheckTimeServer
	 * @return: void
	 * @throws:
	*/
	protected void handleViewEvent(Bundle b, int action, AbstractController abs, boolean isCheckTimeServer) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = getBundleRequest(b);
		e.action = action;
		e.isNeedCheckTimeServer = isCheckTimeServer;
		abs.handleViewEvent(e);
	}

	/**
	 * init with sort feature
	 * @author: duongdt3
	 * @since: 11:21:17 26 Mar 2015
	 * @return: void
	 * @throws:
	 * @param tb
	 * @param header
	 * @param lstSort
	 * @param defaultSortColSeq so thu tu cot mac dinh (bat dau tu 1)
	 * @param defaultSortType {DMSSortInfo.ASC_TYPE, DMSSortInfo.DESC_TYPE}
	 * @param sortListener
	 */
	protected void initHeaderTable(DMSTableView tb, DMSTableRow header,
								   SparseArray<DMSColSortInfo> lstSort, DMSColSortManager.OnSortChange sortListener){
		initHeaderTable(tb, header, null, lstSort, sortListener);
	}

	/**
	 * init with sort feature with sort + list title
	 * @author: duongdt3
	 * @since: 10:01:45 27 Mar 2015
	 * @return: void
	 * @throws:
	 * @param tb
	 * @param header
	 * @param lstTitle
	 * @param lstSort
	 * @param sortListener
	 */
	protected void initHeaderTable(DMSTableView tb, DMSTableRow header,
			ArrayList<String> lstTitle, SparseArray<DMSColSortInfo> lstSort, DMSColSortManager.OnSortChange sortListener){
		tb.addHeader(header, lstTitle, lstSort, sortListener);
	}

	 /**
	 * Khoi tao header table
	 * @author: Tuanlt11
	 * @param tb
	 * @param header
	 * @return: void
	 * @throws:
	*/
	protected void initHeaderTable(DMSTableView tb, DMSTableRow header){
		tb.addHeader(header);
	}

	 /**
	 * Khoi tao header table voi list string
	 * @author: Tuanlt11
	 * @param tb
	 * @param header
	 * @param lstTitle
	 * @return: void
	 * @throws:
	*/
	protected void initHeaderTable(DMSTableView tb, DMSTableRow header,ArrayList<String> lstTitle) {
		tb.addHeader(header,lstTitle);
	}

	/**
	 * get TAG cua fragment
	 * @author: duongdt3
	 * @since: 15:26:00 16 Jan 2015
	 * @return: String
	 * @throws:
	 * @return
	 */
	public String getTAG(){
		String tag = GlobalUtil.getTag(this.getClass());
		//add tag level number
		if (isManagerFragment) {
			tag += "_" + managerLevelCurrent;
		}
		MyLog.d("Fragment getTAG", tag);
		return tag;
	}

	/**
	 * tim fragment tu tag
	 * @author: duongdt3
	 * @since: 08:29:35 24 Mar 2015
	 * @return: Fragment
	 * @throws:
	 * @param tag
	 * @return
	 */
	public Fragment findFragmentByTag (String tag){
		Fragment frag = null;
		frag = parent.getFragmentManager().findFragmentByTag(tag);
		return frag;
	}

	/**
	 * check het level manager view
	 * @author: duongdt3
	 * @since: 15:02:12 24 Mar 2015
	 * @return: boolean
	 * @throws:
	 * @return
	 */
	protected boolean isOutManagerLevel(){
		boolean isOutManagerLevel = (managerLevelCurrent >= GlobalInfo.getInstance().getManagerViewLevel());
		return isOutManagerLevel;
	}

	/**
	 * set manager view mode
	 * @author: duongdt3
	 * @since: 15:20:45 25 Mar 2015
	 * @return: void
	 * @throws:
	 */
	public void setManagerFragmentMode() {
		this.isManagerFragment = true;
		//manager fragmnent need init info
		Bundle args = getArguments();
		if (args != null) {
			managerLevelCurrent = args.getInt(IntentConstants.INTENT_MANAGER_LEVEL, 1);
			switchAction = args.getInt(IntentConstants.INTENT_SWITCH_ACTION, 0);
			MyLog.d("Fragment setManagerFragment", "have getArguments managerLevelCurrent:" + managerLevelCurrent);
		} else{
			managerLevelCurrent = 1;
			switchAction = 0;
			MyLog.d("Fragment setManagerFragment", "not have getArguments managerLevelCurrent:" + managerLevelCurrent);
		}
	}

	/**
	 * @return the managerLevelCurrent
	 */
	public int getManagerLevelCurrent() {
		return managerLevelCurrent;
	}

	/**
	 * @return the isManagerFragmentMode
	 */
	public boolean isManagerFragmentMode() {
		return isManagerFragment;
	}

	/**
	 * @return the switchAction (action show this)
	 */
	public int getSwitchAction() {
		return switchAction;
	}

	/**
	 * Lay thong tin ct keyshop
	 *
	 * @author: Tuanlt11
	 * @param promotionCode
	 * @param promotionId
	 * @return: void
	 * @throws:
	 */
	public void requestGetPromotionDetailKeyShop(String keyshopId) {
		// go to promotion detail
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle dto = new Bundle();
		dto.putString(IntentConstants.INTENT_PROMOTION_ID, keyshopId);
		dto.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		handleViewEvent(dto, ActionEventConstant.GET_PROMOTION_DETAIL_KEYSHOP,
				SaleController.getInstance());
	}

	/**
	 * start activity with try catch
	 * @param intent
	 * @param actionName
	 */
	public void startActivity(Intent intent, String actionName) {
		try {
			GlobalUtil.startActivityOtherAppFromFragment(this, intent);
		} catch (ActivityNotFoundException e) {
			String note = StringUtil.getString(R.string.TEXT_NOT_ACTIVITY_FOUND, actionName);
			parent.showDialog(note);
			ServerLogger.sendLog(actionName + " not found activity "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		} catch (Exception e) {
			String note = StringUtil.getString(R.string.TEXT_START_ACTIVITY_FAIL, actionName);
			parent.showDialog(note);
			ServerLogger.sendLog(actionName + " fail "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 * start activity for result with try catch
	 * @param intent
	 * @param requestCode
	 * @param actionName
	 */
	public void startActivityForResult(Intent intent, int requestCode, String actionName) {
		try {
			GlobalUtil.startActivityForResultFromFragment(this, intent, requestCode);
		} catch (ActivityNotFoundException e) {
			String note = StringUtil.getString(R.string.TEXT_NOT_ACTIVITY_FOUND, actionName);
			parent.showDialog(note);
			ServerLogger.sendLog(actionName + " not found activity "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		} catch (Exception e) {
			String note = StringUtil.getString(R.string.TEXT_START_ACTIVITY_FAIL, actionName);
			parent.showDialog(note);
			ServerLogger.sendLog(actionName + " fail "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 * Hien thi pop up chi tiet van de
	 * @author: yennth16
	 * @since: 11:43:05 10-07-2015
	 * @return: void
	 * @throws:
	 * @param dto
	 */
	private void showKeyShopDetail(KeyShopItemDTO dto) {
		if (alertKeyShopDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			keyShopDetailView = new KeyShopDetailView(parent, this);
			build.setView(keyShopDetailView.viewLayout);
			alertKeyShopDetail = build.create();
			Window window = alertKeyShopDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		keyShopDetailView.updateLayout(dto);
		alertKeyShopDetail.show();
	}

	protected boolean isChangeSysConfig(){
		boolean isChangeSysConfig = false;
		boolean isSysShowPriceTemp = GlobalInfo.getInstance().isSysShowPrice();
		int isSysCurrencyDivideTemp = GlobalInfo.getInstance().getSysCurrencyDivide();
		String sysCurrencyTemp = GlobalInfo.getInstance().getSysCurrency();
		
		if (isSysShowPriceTemp != isSysShowPrice 
				|| isSysCurrencyDivideTemp != isSysCurrencyDivide 
				|| !sysCurrencyTemp.equals(sysCurrency)) {
			isChangeSysConfig = true;
			isSysShowPrice = isSysShowPriceTemp;
			isSysCurrencyDivide = isSysCurrencyDivideTemp;
			sysCurrency = sysCurrencyTemp; 
		} else{
			isChangeSysConfig = false;
		}
		return isChangeSysConfig;
	}
	
	/**
	 * check khoang cach thoa dieu kien de tao don hang
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	protected boolean checkDistanceProcessAfterVisit() {
		boolean isDistancePass = true;
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if (action != null) {
			GlobalInfo.VistConfig visitConfig = GlobalInfo.getInstance().getVisitConfig();
			boolean isTooFar = true;
			LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			LatLng cusLoc = new LatLng(action.aCustomer.lat,action.aCustomer.lng);
			isTooFar = (GlobalUtil.getDistanceBetween(myLoc, cusLoc) > action.aCustomer.shopDTO.distanceOrder);
			boolean isVisited = !StringUtil.isNullOrEmpty(action.endTime);
			isDistancePass = (visitConfig != null && visitConfig.isAllowProcessAfterVisit(isTooFar, action.isOr, isVisited));
		}
		return isDistancePass;
	}
}
