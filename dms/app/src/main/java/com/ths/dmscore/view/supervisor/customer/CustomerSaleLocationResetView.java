/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.customer;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;

import com.ths.dmscore.dto.SpinnerItemDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.CustomerPositionLogDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;
import com.ths.dmscore.dto.view.CustomerUpdateLocationDTO;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.VNMSpinnerTextAdapter;
import com.ths.dms.R;
import com.ths.map.BaseFragmentMapView.MarkerAnchor;
import com.ths.map.CustomFragmentMapView;
import com.viettel.maps.base.LatLng;

/**
 * man hinh reset vi tri cua khach hang trong module gs npp
 * CustomerSaleLocationResetView.java
 * @author: hoanpd1
 * @version: 1.0
 * @since:  09:32:39 06-03-2015
 */
public class CustomerSaleLocationResetView extends CustomFragmentMapView implements OnItemSelectedListener {

	// ds lich su cap nhat (dung cho spinner)
	Vector<SpinnerItemDTO> listHistoryText = new Vector<SpinnerItemDTO>();
	ArrayList<CustomerUpdateLocationDTO> listHistoryUpdateLocation;
	VNMSpinnerTextAdapter adapterLine;
	// action xu ly xoa vi tri
	public static final int ACTION_CLEAR_OK = 11;
	public static final int ACTION_CLEAR_CANCEL = 12;
	Bundle savedInstanceState;
	// thong tin user
	CustomerDTO customerInfo;
	//check xem di vao chuc nang xao vi tri khach hang hay la xem lich su cap nhat
	boolean isUpdatePositionCus;
	// view header
	CustomerSaleLocationResetHeaderView header;
	// vi tri hien tai cua khach hang
	double cusLat, cusLng;
	// vi tri nhan vien
	double staffLat, staffLng;
	// lan dau khoi tao view ?
	boolean isFirstInitView = false;
//	OverlayViewLayer overlayMarkerCustomer;
//	private Object marker;
//	HashMap<Integer, Marker> listMarker = new HashMap<Integer, Marker>();
	ArrayList<LatLng> arrayPosition = new ArrayList<LatLng>();
	private String shopId;

	public static CustomerSaleLocationResetView newInstance(Bundle data) {
		CustomerSaleLocationResetView f = new CustomerSaleLocationResetView();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		customerInfo = (CustomerDTO) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER);
		shopId = getArguments().getString(IntentConstants.INTENT_SHOP_ID);
		isUpdatePositionCus = getArguments().getBoolean(IntentConstants.INTENT_UPDATE_POSITION_CUSTOMER);
		if (customerInfo != null) {
			cusLat = customerInfo.getLat();
			cusLng = customerInfo.getLng();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.savedInstanceState = savedInstanceState;
		View v = super.onCreateView(inflater, container, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriForm.GSNPP_DSKHACHHANG_CAPNHATVITRI);
		hideHeaderview();
		header = new CustomerSaleLocationResetHeaderView(parent);
		// enable menu bar
		parent.setTitleName(StringUtil.getString(R.string.TITLE_GSNPP_CUSTOMER_SALE_UPDATE_LOCATION));
		double myLat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		double myLng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (cusLat > 0 && cusLng > 0) {
			showMarkerInMap(cusLat, cusLng, 0, "", R.drawable.icon_map_position_blue, MarkerAnchor.BOTTOM_CENTER);
			moveTo(new LatLng(cusLat, cusLng));
			arrayPosition.add(new LatLng(cusLat, cusLng));
		} else if (myLat > 0 && myLng > 0) {
			moveTo(new LatLng(myLat, myLng));
		} else {
			moveTo(new LatLng(Constants.LAT_VNM_TOWNER, Constants.LNG_VNM_TOWNER));
		}

		// lat, lng cua nhan vien giam sat
		staffLat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
		staffLng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
		if (staffLat <= 0 || staffLng <= 0) {
			parent.reStartLocatingWithWaiting();
		}

		drawMarkerMyPosition();
		// init du lieu dau
		setCustomerLocationHeader();
		//getShopParam();
		getHistoryUpdateLocation();
		// show marker vi tri hien tai cua khach hang

		//  lay thong tin shop_param
		getShopParam();
		return v;
	}

	/**
	 * Lay danh sach lich su cap nhat vi tri cua khach hang
	 *
	 * @author: hoanpd1
	 * @since: 09:35:55 06-03-2015
	 * @return: void
	 * @throws:
	 */
	private void getHistoryUpdateLocation() {
		isFirstInitView = true;
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, String.valueOf(customerInfo.customerId));
		handleViewEvent(b, ActionEventConstant.GET_HISTORY_UPDATED_LOCATION, SupervisorController.getInstance());
	}

	/**
	 *
	 * Hien thi header thong tin khach hang tren ban do
	 * @author: hoanpd1
	 * @since: 09:35:42 06-03-2015
	 * @return: void
	 * @throws:
	 */
	@SuppressWarnings("deprecation")
	private void setCustomerLocationHeader() {
		if (!isFirstInitView) {
			LinearLayout llHeader;
			llHeader = new LinearLayout(rlMainMapView.getContext());

			llHeader.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			llHeader.setOrientation(LinearLayout.HORIZONTAL);
			llHeader.removeView(header);
			header = new CustomerSaleLocationResetHeaderView(parent);
			header.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			header.setCustomerInfo(customerInfo);
			llHeader.addView(header);
			llHeader.setGravity(Gravity.CENTER | Gravity.TOP);
			header.btReset.setOnClickListener(this);

			rlMainMapView.addView(llHeader);
			isFirstInitView = true;
		}else{
			header.setCustomerInfo(customerInfo);
		}
		mapHelper.invalidate();
	}

	/**
	 * Hien thi marker toa do hien tai
	 *
	 * @author: hoanpd1
	 * @since: 09:36:07 06-03-2015
	 * @return: void
	 * @throws:
	 * @param lat
	 * @param lng
	 */
	private void showMarkerInMap(double lat, double lng, int index,
			String name, int drawable, MarkerAnchor markerAnchor) {
		super.addMarkerToMap(lat, lng, drawable, markerAnchor, "", name, 0, 0);
	}



	/**
	 * clear vi tri cua khach hang
	 *
	 * @author: hoanpd1
	 * @since: 09:36:28 06-03-2015
	 * @return: void
	 * @throws:
	 */
	private void resetCustomerLocation() {
		parent.showLoadingDialog();
		Bundle bundle = new Bundle();
//		customerInfo.setLng(Constants.LAT_LNG_NULL);
//		customerInfo.setLat(Constants.LAT_LNG_NULL);
		customerInfo.setLng(-1);
		customerInfo.setLat(-1);
		cusLat = customerInfo.getLat();
		cusLng = customerInfo.getLng();
		// du lieu insert bang position log
		CustomerPositionLogDTO logPosition = new CustomerPositionLogDTO();
		logPosition.createDate = DateUtils.now();
		logPosition.customerId = customerInfo.customerId;
		logPosition.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
//		logPosition.lat = -1;
//		logPosition.lng = -1;
		logPosition.lat = Constants.LAT_LNG_NULL;
		logPosition.lng = Constants.LAT_LNG_NULL;

		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER_ID, customerInfo);
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER_POSITION_LOG, logPosition);
		handleViewEvent(bundle, ActionEventConstant.UPDATE_CUSTOMER_LOATION, SaleController.getInstance());

	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_CUSTOMER_BY_ID:{
			customerInfo = (CustomerDTO) modelEvent.getModelData();
			if(customerInfo != null){
				cusLat = customerInfo.getLat();
				cusLng = customerInfo.getLng();
				setCustomerLocationHeader();
				getHistoryUpdateLocation();
				getShopParam();
			}
			break;
		}
		case ActionEventConstant.GET_SHOPPARAM_ALLOW_CLEAR_POSITION:
			ShopParamDTO shopparam = (ShopParamDTO) modelEvent.getModelData();
			if(shopparam==null || shopparam.status != 1){
				if (header!=null) {
					header.btReset.setVisibility(View.INVISIBLE);
				}
			} else {
				if (header!=null) {
					if(customerInfo.getLat() > 0 && customerInfo.getLng() > 0)
						header.btReset.setVisibility(View.VISIBLE);
					else{
						header.btReset.setVisibility(View.INVISIBLE);
					}
				}
			}
			break;
		case ActionEventConstant.UPDATE_CUSTOMER_LOATION:
			parent.showDialog(StringUtil.getString(R.string.TEXT_DELETE_CUSTOMER_POSITION_SUCCESS));
			header.setButtonResetVisible(View.INVISIBLE);
//			 TransactionProcessManager.getInstance().resetChecking();
			isUpdatePositionCus = false;
			getHistoryUpdateLocation();
			break;
		case ActionEventConstant.GET_HISTORY_UPDATED_LOCATION:
			listHistoryUpdateLocation = (ArrayList<CustomerUpdateLocationDTO>) modelEvent.getModelData();
			// list toa do cap nhat du lieu
			ArrayList<LatLng> listUpdatePosition = new ArrayList<LatLng>();
			int size = listHistoryUpdateLocation.size();
			if (listHistoryUpdateLocation != null && size > 0) {
				clearAllMarkerOnMap();
				listHistoryText = new Vector<SpinnerItemDTO>();
				arrayPosition.add(new LatLng(staffLat, staffLng));
				SpinnerItemDTO spinnerItem;
				for (int i = 0; i < size; i++) {
					spinnerItem = new SpinnerItemDTO();
					spinnerItem.name = listHistoryUpdateLocation.get(i).staffSale.name;
					spinnerItem.content = listHistoryUpdateLocation.get(i).positionLog.createDate;
					double customerLat = listHistoryUpdateLocation.get(i).positionLog.getLat();
					double customerLng = listHistoryUpdateLocation.get(i).positionLog.getLng();
					String createDate = listHistoryUpdateLocation.get(i).positionLog.createDate;

					listHistoryText.add(spinnerItem);
						if (i > 0) {
							mapHelper.addMarker(customerLat, customerLng,
									R.drawable.icon_map_position_pink, MarkerAnchor.BOTTOM_CENTER, "",
									createDate, R.color.TRANSPARENT, R.color.RED);
						} else {
							// da xoa diem hien tai
							if (cusLat <= 0 || cusLng <= 0) {
								mapHelper.addMarker(customerLat, customerLng,
										R.drawable.icon_map_position_pink, MarkerAnchor.BOTTOM_CENTER, "",
										createDate, R.color.TRANSPARENT, R.color.RED);
							} else {
								mapHelper.addMarker(customerLat, customerLng,
										R.drawable.icon_map_position_blue, MarkerAnchor.BOTTOM_CENTER, "",
										"", R.color.TRANSPARENT, R.color.TRANSPARENT);
							}
						}

					LatLng cusLo = new LatLng(customerLat,customerLng);
					listUpdatePosition.add(cusLo);
					if (customerLat > 0 && customerLng > 0) {
						arrayPosition.add(new LatLng(customerLat, customerLng));
					}
				}
				drawMarkerMyPosition();
				// khoi tao spinner
				adapterLine = new VNMSpinnerTextAdapter(getActivity(), R.layout.simple_spinner_item, listHistoryText);
				adapterLine.setHint(StringUtil.getString(R.string.HISTOY_UPDATED_LOCATION));
				header.spHistoryUpdate.setOnItemSelectedListener(this);
				header.spHistoryUpdate.setAdapter(adapterLine);
//				header.spHistoryUpdate.setSelection(0);
				adapterLine.notifyDataSetChanged();
				header.spHistoryUpdate.setVisibility(View.VISIBLE);

			} else {
				header.spHistoryUpdate.setVisibility(View.GONE);
			}
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.UPDATE_CUSTOMER_LOATION:
			parent.showDialog(modelEvent.getModelMessage());
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		if (v == header.btReset) {
			GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this, parent,
					StringUtil.getString(R.string.TEXT_CONFIRM_DELETE_CUSTOMER_POSITION),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_CLEAR_OK,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					ACTION_CLEAR_CANCEL, null, false, false);
		}
		super.onClick(v);
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_CLEAR_OK:
			if (GlobalUtil.checkNetworkAccess()) {
				resetCustomerLocation();
			} else {
				parent.showDialog(StringUtil.getString(R.string.TEXT_NETWORK_DISABLE));
			}
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.ACTION_UPDATE_POSITION:
			double lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			double lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			if (lat < 0 && lng < 0) {
				return;
			} else {
				drawMarkerMyPosition();
			}
			break;
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
//				isFirstInitView = false;
				getCustomerByID(customerInfo.getCustomerId());
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if(isFirstInitView){
			if(!isUpdatePositionCus){
				super.fitBounds(arrayPosition);
			}
			isFirstInitView = false;
		}else {
			if (arg1 == null) {
				if (arg2 >= 0 && listHistoryUpdateLocation != null
						&& listHistoryUpdateLocation.size() > 0) {
					LatLng pointerCenter = new LatLng(listHistoryUpdateLocation.get(arg2).positionLog.lat, listHistoryUpdateLocation.get(arg2).positionLog.lng);
					moveTo(pointerCenter);
				}

			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	/**
	 * Lay TT KH = ID
	 *
	 * @author: hoanpd1
	 * @since: 09:36:43 06-03-2015
	 * @return: void
	 * @throws:
	 * @param customerId
	 */
	private void getCustomerByID(String customerId) {
		parent.showLoadingDialog();
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		handleViewEventWithOutAsyntask(bundle, ActionEventConstant.GET_CUSTOMER_BY_ID,
				SupervisorController.getInstance());
	}

	/**
	 * Lay danh sach lich su cap nhat vi tri cua khach hang
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void getShopParam() {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		handleViewEvent(b,  ActionEventConstant.GET_SHOPPARAM_ALLOW_CLEAR_POSITION, SupervisorController.getInstance());
	}

}
