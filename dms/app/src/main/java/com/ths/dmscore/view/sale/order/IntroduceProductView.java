/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.commonsware.cwac.cache.SimpleWebImageCache;
import com.commonsware.cwac.thumbnail.ThumbnailAdapter;
import com.commonsware.cwac.thumbnail.ThumbnailBus;
import com.commonsware.cwac.thumbnail.ThumbnailMessage;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.MediaLogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.HorizontalListView;
import com.ths.dmscore.view.control.MediaGalleryAdapter;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.view.CustomerListDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.IntroduceProductDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 *
 * Mo ta cho class
 *
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class IntroduceProductView extends BaseFragment implements
		OnEventControlListener, OnClickListener, VinamilkTableListener {
	// frame image view full
	private FrameLayout flImageViewFull;
	// man hinh video
	private ImageView imgViewFull;
	private TextView tvProductName;
	private int selectIndex;
	private LinearLayout llGallery;
	private WebView wvIntroduce;
	// category for list location search
	// category adapter
	MediaGalleryAdapter mediaAdapter;
	HorizontalListView gallery;
	private ThumbnailAdapter categoryThumbs = null;
	// main activity
	private GlobalBaseActivity parent;
	// define id of avatar on row
	private static final int[] IMAGE_IDS = { R.id.imgViewMedia,
			R.id.imgViewBoder };
	// productId
	private String productId;
	// image hien tai
	MediaItemDTO curentImage = null;

	// dialog product detail view
	AlertDialog alertProductDetail;
	boolean isFirstShowSelectCustomer = false;
	// table list customer
	DMSTableView tbCustomerList;
	// button close popup
	Button btClosePopup;
	// button skip popup
	Button btSkipPopup;
	public CustomerListDTO cusDto;// cusList
	// id insert log db
	long mediaLogId = -1;
	int INTENT_RECORD_VIDEO = 1001;

	private Calendar startTimeKPI;

	// category list
	public Vector<MediaItemDTO> mediaList = new Vector<MediaItemDTO>();

	public static IntroduceProductView newInstance(Bundle data) {
		// if (instance == null) {
		IntroduceProductView instance = new IntroduceProductView();
		// Supply index input as an argument.
		instance.setArguments(data);
		// }
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.layout_introduce_product, null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DSSANPHAM_CHITIETSANPHAM);

		// get Date
		Bundle data = (Bundle) getArguments();
		productId = data.getString(IntentConstants.INTENT_PRODUCT_ID);
		String productCode = data
				.getString(IntentConstants.INTENT_PRODUCT_CODE);
		String productName = data
				.getString(IntentConstants.INTENT_PRODUCT_NAME);
		flImageViewFull = (FrameLayout) view.findViewById(R.id.flImageViewFull);
		imgViewFull = (ImageView) view.findViewById(R.id.imgViewFull);
		imgViewFull.setOnClickListener(this);
		tvProductName = (TextView) view.findViewById(R.id.tvProductName);
		tvProductName.setText(productCode + " - " + productName);
		llGallery = (LinearLayout) view.findViewById(R.id.llGallery);
		llGallery.setVisibility(View.VISIBLE);
		gallery = (HorizontalListView) view.findViewById(R.id.glCategoryView);
		wvIntroduce = (WebView) view.findViewById(R.id.wvIntroduce);
		wvIntroduce.setBackgroundColor(Color.TRANSPARENT);

//		setTitleHeaderView(StringUtil
//				.getString(R.string.TITLE_VIEW_INTRODUCE_PRODUCT));
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_INTRODUCE_PRODUCT));
		// Request information for view
		RequestIntroduceProduct(productId);// change productId
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			parent = (GlobalBaseActivity) activity;
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}

	}

	/**
	 * Goi du lieu len server lay thong tin man hinh gioi thieu san pham
	 *
	 * @author: ThanhNN8
	 * @param i
	 * @return: void
	 * @throws:
	 */
	private void RequestIntroduceProduct(String productId) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_PRODUCT_ID, productId);
		ActionEvent e = new ActionEvent();
		e.viewData = data;
		e.sender = this;
		e.action = ActionEventConstant.GET_INTRODUCE_PRODUCT;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 *
	 * bo xung comment cho ThanhNN: play video from path
	 *
	 * @author: BangHN
	 * @param path
	 * @return: void
	 * @throws:
	 */
	private void openMeida(String path, int mediaType) {
		// call service
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		if (mediaType == MediaItemDTO.MEDIA_IMAGE) {
			intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
			startActivity(intent, StringUtil.getString(R.string.TEXT_VIEW_IMAGE));
		} else {
			intent.setDataAndType(Uri.parse(path), "video/mp4");
			startActivityForResult(intent, INTENT_RECORD_VIDEO, StringUtil.getString(R.string.TEXT_VIEW_VIDEO));
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INTENT_RECORD_VIDEO) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));

			MediaLogDTO mediaLog = new MediaLogDTO();
			mediaLog.id = mediaLogId;
			mediaLog.endTime = DateUtils.now();

			ActionEvent e = new ActionEvent();
			e.viewData = mediaLog;
			e.sender = this;
			e.action = ActionEventConstant.UPDATE_ENDTIME_MEDIA_LOG;
			SaleController.getInstance().handleViewEvent(e);
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	private String getDataSource(String path) throws IOException {
		MediaItemDTO mdiDTO = (MediaItemDTO) mediaList.get(selectIndex);
		URL url = new URL(path);
		URLConnection cn = url.openConnection();
		cn.connect();
		InputStream stream = cn.getInputStream();
		if (stream == null) {
			throw new RuntimeException("stream is null");
		}
		File dir = new File(ExternalStorage.ROOT_SDCARD_DIR + "PRODUCTS/");
		dir.mkdirs();
		File temp;
		if (mdiDTO.mediaType == MediaItemDTO.MEDIA_IMAGE) {
			// temp = File.createTempFile("mediaplayertmp", ".jpg", dir);
			temp = new File(dir, StringUtil.md5(mdiDTO.url) + ".jpg");
		} else {
			// temp = File.createTempFile("mediaplayertmp", ".mp4", dir);
			temp = new File(dir, StringUtil.md5(mdiDTO.url) + ".mp4");
		}
		temp.deleteOnExit();
		String tempPath = temp.getAbsolutePath();
		FileOutputStream out = new FileOutputStream(temp);
		byte buf[] = new byte[128];
		try {
			do {
				int numread = stream.read(buf);
				if (numread <= 0)
					break;
				out.write(buf, 0, numread);
			} while (true);
			
		} catch (IOException ex) {
			MyLog.e(this.getTAG(), "error: " + ex.getMessage(), ex);
			throw ex;
		} finally {
			out.close();
			stream.close();
		}
		return tempPath;
	}

	/**
	 * Background task to download and unpack .zip file in background.
	 */
	private class DownloadTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			startTimeKPI = Calendar.getInstance();
			parent.showProgressDialog(StringUtil.getString(R.string.TEXT_MESSAGE_DOWNLOADING_PRODUCT_INFOR));
		}

		@Override
		protected String doInBackground(String... params) {
			String url = (String) params[0];
			try {
				String path = getDataSource(url);
				publishProgress(path);
			} catch (Exception e) {
				return null;
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);
			if (values != null && IntroduceProductView.this != null
					&& IntroduceProductView.this.isVisible()) {
				MediaItemDTO mdiDTO = (MediaItemDTO) mediaList.get(selectIndex);
				mdiDTO.sdCardPath = values[0];
				updateClientThumnailUrl(mdiDTO);
				parent.closeProgressDialog();
				openMeida(mdiDTO.sdCardPath, mdiDTO.mediaType);
				if (mdiDTO.mediaType == MediaItemDTO.MEDIA_IMAGE) {
					requestInsertLogKPI(HashMapKPI.NVBH_XEMHINHANHSANPHAM, startTimeKPI);
				} else {
					requestInsertLogKPI(HashMapKPI.NVBH_XEMVIDEOSANPHAM, startTimeKPI);
				}
			} else {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_MESSAGE_ERROR_DOWNLOAD_VIDEO_PRODUCT));
			}

		}

		/**
		 * Update url client for database
		 *
		 * @author: ThanhNN8
		 * @param mdiDTO
		 * @return: void
		 * @throws:
		 */
		private void updateClientThumnailUrl(MediaItemDTO mdiDTO) {
			Bundle data = new Bundle();
			data.putSerializable(IntentConstants.INTENT_DATA, mdiDTO);
			ActionEvent e = new ActionEvent();
			e.viewData = data;
			e.sender = this;
			e.action = ActionEventConstant.UPDATE_CLIENT_THUMNAIL_URL;
			SaleController.getInstance().handleViewEvent(e);
		}

		@Override
		protected void onPostExecute(String result) {
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		case ActionEventConstant.ACTION_WATCH_PRODUCT_VIDEO:
			alertProductDetail.dismiss();
			insertMediaLog(data);
			break;
		default:
			break;
		}
	}

	/**
	 * insert medialog vao db
	 *
	 * @author: ToanTT
	 * @param i
	 * @return: void
	 * @throws:
	 */
	private void insertMediaLog(Object data) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		MediaLogDTO mediaLog = new MediaLogDTO();
		mediaLog.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		mediaLog.mediaId = curentImage.media_id;
		mediaLog.productId = Long.valueOf(productId);
		mediaLog.startTime = DateUtils.now();
		mediaLog.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLatitude();
		mediaLog.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLongtitude();
		mediaLog.createUser = String.valueOf(GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritId());
		mediaLog.createDate = DateUtils.now();
		if (data != null) {
			CustomerListItem item = (CustomerListItem) data;
			mediaLog.customerId = item.aCustomer.customerId;
			mediaLog.isVisitPlan = item.isVisitPlan;
			mediaLog.visitStatus = MediaLogDTO.isVisited(item.visitStatus);
			mediaLog.distance = GlobalUtil.getDistanceBetween(new LatLng(
					mediaLog.lat, mediaLog.lng), new LatLng(item.aCustomer.lat,
					item.aCustomer.lng));
		} else {
			mediaLog.customerId = -1;
			mediaLog.isVisitPlan = -1;
			mediaLog.visitStatus = -1;
			mediaLog.distance = -1;
		}

		ActionEvent e = new ActionEvent();
		e.viewData = mediaLog;
		e.sender = this;
		e.action = ActionEventConstant.INSERT_MEDIA_LOG;
		SaleController.getInstance().handleViewEvent(e);
	}

	@Override
	public void onClick(View v) {
		if (v == imgViewFull && curentImage != null) {
			if (curentImage.mediaType == MediaItemDTO.MEDIA_IMAGE) {
				if(!StringUtil.isNullOrEmpty(curentImage.url))
					loadMediaItem(StringUtil.md5(curentImage.url) + ".jpg");
			} else {
				getSelectCustomer();
			}
		} else if (v == btClosePopup) {
			alertProductDetail.dismiss();
		} else if (v == btSkipPopup) {
			alertProductDetail.dismiss();
			insertMediaLog(null);
		}
	}

	/**
	 * Lay danh sach khach hang do nhan vien quan ly trong pham vi quy dinh
	 *
	 * @author: ThanhNN8
	 * @param i
	 * @return: void
	 * @throws:
	 */
	private void getSelectCustomer() {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		isFirstShowSelectCustomer = true;
		Bundle data = new Bundle();
		data.putInt(IntentConstants.INTENT_ROLE_TYPE, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritSpecificType());
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		data.putDouble(IntentConstants.INTENT_POSITION_LAT, GlobalInfo
				.getInstance().getProfile().getMyGPSInfo().getLatitude());
		data.putDouble(IntentConstants.INTENT_POSITION_LONG, GlobalInfo
				.getInstance().getProfile().getMyGPSInfo().getLongtitude());
		data.putString(
				IntentConstants.INTENT_SHOP_ID,
				GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		ActionEvent e = new ActionEvent();
		e.viewData = data;
		e.sender = this;
		e.action = ActionEventConstant.ACTION_LOAD_LIST_SELECT_CUSTOMER;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * show dialog select customer to watch product video
	 *
	 * @author: ToanTT
	 * @param data
	 * @return: void
	 * @throws:
	 */
	private void loadMediaItem(String mediaName) {
		// neu ton tai cache truoc do roi
		if (GlobalUtil.isFileExistsInDirectory(new File(
				ExternalStorage.ROOT_SDCARD_DIR + "PRODUCTS/"), mediaName)) {
			// xem offline, file ton tai duoi may tinh bang
			openMeida(ExternalStorage.ROOT_SDCARD_DIR + "PRODUCTS/"
					+ mediaName, curentImage.mediaType);
		} else if (!GlobalUtil.checkNetworkAccess()) {
			// truong hop khong co mang
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_NETWORK_DISABLE_PRODUCT_INFO));
		} else {
			// download moi ve
			new DownloadTask().execute(GlobalInfo.getInstance()
					.getServerImageProductVNM() + curentImage.url);
		}
	}

	/**
	 * show dialog select customer to watch product video
	 *
	 * @author: ToanTT
	 * @param data
	 * @return: void
	 * @throws:
	 */
	private void showSelectCustomerProductVideo() {
		if (alertProductDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			LayoutInflater inflater = this.parent.getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_select_customer_for_product_video, null);

			tbCustomerList = (DMSTableView) view
					.findViewById(R.id.tbCustomerListView);
			btClosePopup = (Button) view.findViewById(R.id.btClosePopup);
			btClosePopup.setOnClickListener(this);
			btSkipPopup = (Button) view.findViewById(R.id.btSkipPopup);
			btSkipPopup.setOnClickListener(this);

			initHeaderTable(tbCustomerList, new SelectCustomerForProductVideo(
						parent));

			build.setView(view);
			alertProductDetail = build.create();

			Window window = alertProductDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		if (isFirstShowSelectCustomer) {
			int pos = 1;
			for (int i = 0, size = cusDto.getCusList().size(); i < size; i++) {
				CustomerListItem dto = cusDto.getCusList().get(i);
				SelectCustomerForProductVideo row = new SelectCustomerForProductVideo(
						parent);
				row.setClickable(true);
				row.setTag(dto);
				row.renderLayout(pos, dto);
				row.setListener(this);
				pos++;
				tbCustomerList.addView(row);
			}
		}
		alertProductDetail.show();
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_INTRODUCE_PRODUCT:
			IntroduceProductDTO inDTO = (IntroduceProductDTO) modelEvent
					.getModelData();
			initMediaGallery(inDTO.getListMedia());
			renderLayout(inDTO);
			parent.closeProgressDialog();
			requestInsertLogKPI(HashMapKPI.NVBH_CHITIETSANPHAM, modelEvent.getActionEvent());
			break;
		case ActionEventConstant.UPDATE_CLIENT_THUMNAIL_URL:
			// update link client thanh cong
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_LOAD_LIST_SELECT_CUSTOMER:
			CustomerListDTO tempDTO = (CustomerListDTO) modelEvent
					.getModelData();
			if (tempDTO != null && tempDTO.getCusList().size() > 0) {
				cusDto = CustomerListItem.orderDistanceCusomter(tempDTO);
				showSelectCustomerProductVideo();
				if (isFirstShowSelectCustomer) {
					isFirstShowSelectCustomer = false;
				}
			} else {
				insertMediaLog(null);
			}
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.INSERT_MEDIA_LOG:
			parent.closeProgressDialog();
			mediaLogId = ((Long) modelEvent.getModelData()).longValue();
			loadMediaItem(StringUtil.md5(curentImage.url) + ".mp4");
			break;
		case ActionEventConstant.UPDATE_ENDTIME_MEDIA_LOG:
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * render giao dien cho man hinh
	 *
	 * @author: ThanhNN8
	 * @param modelData
	 * @return: void
	 * @throws:
	 */
	private void renderLayout(IntroduceProductDTO modelData) {
		String htmlContent = modelData.getHtmlContextIntroduce();
		wvIntroduce.getSettings().setSupportZoom(false);
		if (htmlContent != null) {
			StringBuilder regul = new StringBuilder();
			regul.append("<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><body>");
			regul.append(htmlContent);
			regul.append("</body></html>");
			wvIntroduce.loadDataWithBaseURL(null, regul.toString(),
					"text/html", "UTF-8", null);
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.INSERT_MEDIA_LOG:
			mediaLogId = ((Long) modelEvent.getModelData()).longValue();
			loadMediaItem(StringUtil.md5(curentImage.url) + ".mp4");
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 *
	 * bo xung comment cho THanhNN : khoi tao media cho gallery
	 *
	 * @author: HaiTC3
	 * @param listDTO
	 * @return: void
	 * @throws:
	 */
	private void initMediaGallery(List<MediaItemDTO> listDTO) {
		mediaList.clear();
		int size = listDTO.size();
		if (size == 0) {
			if (mediaAdapter != null) {
				mediaAdapter.notifyDataSetChanged();
			}
			if (categoryThumbs != null) {
				categoryThumbs.notifyDataSetChanged();
			}
			imgViewFull.setVisibility(View.GONE);
			flImageViewFull.setVisibility(View.GONE);
			return;
		} else {
			imgViewFull.setVisibility(View.VISIBLE);
			flImageViewFull.setVisibility(View.VISIBLE);
		}
		for (int i = 0; i < size; i++) {
			MediaItemDTO media = listDTO.get(i);
			mediaList.add(media.clone());
		}
		mediaAdapter = new MediaGalleryAdapter(parent,
				R.layout.layout_media_item, mediaList, true);
		try {
			ThumbnailBus thumbBus = new ThumbnailBus();
			categoryThumbs = new ThumbnailAdapter(parent, mediaAdapter,
					new SimpleWebImageCache<ThumbnailBus, ThumbnailMessage>(
							null, null, 101, thumbBus), IMAGE_IDS);
		} catch (Exception e1) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}
		gallery.setAdapter(categoryThumbs);
		gallery.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				selectIndex = position;
				MediaItemDTO mdiDTO = (MediaItemDTO) mediaList.get(position);
				// to do handler event
				showImage(mdiDTO);
				changeImageClick();
			}
		});
		// set gia tri 0 mac dinh ban dau
		if (mediaList.size() > 0) {
			selectIndex = 0;
			MediaItemDTO mdiDTO = (MediaItemDTO) mediaList.get(0);
			changeImageClick();
			// to do handler event
			showImage(mdiDTO);
		}
	}

	/**
	 * Thay doi lai danh sach hinh anh khi chon 1 item
	 *
	 * @author: ThanhNN8
	 * @return: void
	 * @throws:
	 */
	protected void changeImageClick() {
		for (MediaItemDTO mdiDTO : mediaList) {
			mdiDTO.isSelected = false;
		}
		// Send message update
		mediaList.elementAt(selectIndex).isSelected = true;
		categoryThumbs.notifyDataSetChanged();
	}

	/**
	 *
	 * Hien thi full hinh anh hoac mo trinh xem video cua he thong
	 *
	 * @author: ThanhNN8
	 * @param mdiDTO
	 * @return: void
	 * @throws:
	 */
	private void showImage(MediaItemDTO mdiDTO) {
		curentImage = mdiDTO;
		if (mdiDTO.mediaType == 1) { // neu la video
			imgViewFull.setImageResource(R.drawable.videofull);
		} else { // neu la hinh anh
			showFullImage(GlobalInfo.getInstance().getServerImageProductVNM()
					+ mdiDTO.url);
		}
	}

	/**
	 * Ham hien thi full hinh anh
	 *
	 * @author: ThanhNN8
	 * @param url
	 * @return: void
	 * @throws:
	 */
	private void showFullImage(String url) {
		ImageUtil.getImageFromURL(url, parent, imgViewFull);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				RequestIntroduceProduct(productId);
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}
}
