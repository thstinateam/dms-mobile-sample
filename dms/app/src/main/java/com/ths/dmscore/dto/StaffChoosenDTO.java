/**
 * Copyright 2013 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  DTO cho man hinh chon nhan vien chup hinh
 *  @author: Tuanlt11
 *  @version: 1.0
 *  @since: 1.0
 */
public class StaffChoosenDTO  implements Serializable {
	private static final long serialVersionUID = 7051400898265877359L;
	public long statffId; // id nv
	public String staffName; // ten nv
	public String staffCode; // ma nv
	public int roleType ; // role type
	public boolean isSelected = false ; // bien kiem tra dang chon hay ko
	public long staffOwnerId;

	/**
	 * 
	 * init data
	 * 
	 * @author: YenNTH
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		statffId = CursorUtil.getLong(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		roleType = CursorUtil.getInt(c, "ROLE_TYPE");
		// chi co roleType = 2
		if(roleType == 1){
			roleType = 2;
		}
		staffOwnerId = CursorUtil.getLong(c, "STAFF_OWNER_ID");
	}
	
}
