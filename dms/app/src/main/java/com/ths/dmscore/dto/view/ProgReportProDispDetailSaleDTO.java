package com.ths.dmscore.dto.view;

import java.util.ArrayList;


public class ProgReportProDispDetailSaleDTO {
	
	public long RemainSaleTotal;

	public ArrayList<ProgReportProDispDetailSaleRowDTO> listItem;
	
	// tong so item
	public int totalItem = 0;

	public  ProgReportProDispDetailSaleDTO(){
		listItem = new ArrayList<ProgReportProDispDetailSaleRowDTO>();
	}
	public void addItem(ProgReportProDispDetailSaleRowDTO c) {
		listItem.add(c);
		RemainSaleTotal +=c.RemainSale;
	}
}
