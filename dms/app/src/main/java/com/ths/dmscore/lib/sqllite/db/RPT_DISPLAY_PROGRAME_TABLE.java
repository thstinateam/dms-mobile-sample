package com.ths.dmscore.lib.sqllite.db;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.DisplayProgrameLvDTO;
import com.ths.dmscore.dto.view.DisProComProgReportDTO;
import com.ths.dmscore.dto.view.DisplayPresentProductInfo;
import com.ths.dmscore.dto.view.DisplayProgramLevelForProgramDTO;
import com.ths.dmscore.dto.view.ProgReportProDispDetailSaleRowDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailCustomerDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailDayDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailDayRowDTO;
import com.ths.dmscore.dto.view.SupervisorReportDisplayProgressStaffDTO;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportDTO;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportItem;
import com.ths.dmscore.dto.view.TBHVProgReportProDispDetailSaleDTO;
import com.ths.dmscore.dto.view.TBHVProgReportProDispDetailSaleRowDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.GSNPPInfoDTO;
import com.ths.dmscore.dto.view.ProgReportProDispDetailSaleDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressDetailCustomerRowDTO;
import com.ths.dmscore.dto.view.ReportDisplayProgressResult;
import com.ths.dmscore.dto.view.SupervisorReportDisplayProgressDTO;
import com.ths.dmscore.dto.view.SupervisorReportDisplayProgressItem;
import com.ths.dmscore.dto.view.SupervisorReportDisplayProgressStaffItem;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportItemResult;
import com.ths.dmscore.dto.view.TBHVDisProComProgReportNPPDTO;
import com.ths.dmscore.dto.view.TBHVReportDisplayProgressDetailDayViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;
/**
 *
 * @author: TRUNGHQM
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_DISPLAY_PROGRAME_TABLE extends ABSTRACT_TABLE {

	// id cua bang
	public static final String DISPLAY_PROGRAME_DETAIL_ID = "DISPLAY_PROGRAME_DETAIL_ID";

	public static final String ID = "ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String PARENT_STAFF_ID = "PARENT_STAFF_ID";
	public static final String STAFF_CODE = "STAFF_CODE";
	public static final String STAFF_NAME = "STAFF_NAME";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
	public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
	public static final String CUSTOMER_ADDRESS = "CUSTOMER_ADDRESS";
	public static final String DISPLAY_PROGRAME_ID = "DISPLAY_PROGRAME_ID";
	public static final String DISPLAY_PROGRAME_CODE = "DISPLAY_PROGRAME_CODE";
	public static final String DP_SHORT_CODE = "DP_SHORT_CODE";
	public static final String DISPLAY_PROGRAME_NAME = "DISPLAY_PROGRAME_NAME";
	public static final String DP_SHORT_NAME = "DP_SHORT_NAME";
	public static final String DISPLAY_PROGRAME_LEVEL = "DISPLAY_PROGRAME_LEVEL";
	public static final String AMOUNT_PLAN = "AMOUNT_PLAN";
	public static final String AMOUNT = "AMOUNT";
	public static final String AMOUNT_FINO = "AMOUNT_FINO";
	public static final String AMOUNT_OTHER = "AMOUNT_OTHER";
	public static final String FINO_PERCENTAGE = "FINO_PERCENTAGE";
	public static final String RESULT = "RESULT";
	public static final String MONTH = "MONTH";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String SYN_STATE = "SYN_STATE";
	public static final String FROM_DATE = "FROM_DATE";
	public static final String TO_DATE = "TO_DATE";

	public RPT_DISPLAY_PROGRAME_TABLE(SQLiteDatabase _mDB) {
		this.mDB = _mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * get list display programe level code
	 *
	 * @param shopId
	 * @param staffId
	 * @return
	 */
	public ArrayList<String> getSupervisorArrayLevelCode(String shopId,
			String lstStaffId) {
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		String monthNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_MONTH_YEAR);
		ArrayList<String> ret = new ArrayList<String>();
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT  distinct DPL.LEVEL_CODE ");
		var1.append("        FROM   display_program_level DPL, ");
		var1.append("               rpt_display_program RPT_DP ");
		var1.append("        WHERE  DPL.display_program_id = RPT_DP.display_program_id ");
		var1.append("               AND RPT_DP.shop_id = ? ");
		params.add(shopId);
		var1.append("               AND RPT_DP.staff_id IN ( ");
		var1.append(lstStaffId);
		var1.append("               ) ");
		var1.append("               AND RPT_DP.status = 1 ");
		var1.append("               AND substr(RPT_DP.month, 1, 7) = ? ");
		params.add(monthNow);
		var1.append("               AND substr(RPT_DP.from_date, 1, 10) <= ? ");
		params.add(dateNow);
		var1.append("               AND Ifnull(substr(RPT_DP.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
//		var1.append("        GROUP  BY DPL.display_program_id ");
		var1.append("        ORDER  BY DPL.LEVEL_CODE ");

		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c.moveToFirst()) {
				do {
					ret.add(CursorUtil.getString(c, "LEVEL_CODE"));
				} while (c.moveToNext());
			}
		} finally {

			try {
				if (c != null)
					c.close();
			} catch (Exception ex) {
			}
		}


		return ret;
	}

	/**
	 * Hoan get DTO for DisProComProgReport
	 *
	 * @param parentStaffId
	 * @return
	 * @throws Exception
	 */
	public SupervisorReportDisplayProgressDTO getDisProComProReportDTO(
			Bundle data) throws Exception {
		SupervisorReportDisplayProgressDTO dto = new SupervisorReportDisplayProgressDTO();

		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String parentShopId = data.getString(IntentConstants.INTENT_PARENT_SHOP_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		if (shopId == null) {
			shopId = "";
		}

		//khong co shop thi tien hanh lay ds shop
		if (StringUtil.isNullOrEmpty(shopId)) {
			ArrayList<ShopDTO> lstShop = SQLUtils.getInstance().requestGetListCombobox(parentShopId);
			dto.lstShop = lstShop;

			if (dto.lstShop != null && !dto.lstShop.isEmpty()) {
				shopId = String.valueOf(dto.lstShop.get(0).shopId);
			}
		}

		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		String monthNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_MONTH_YEAR);
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		String listStaffID = staffTable.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		ArrayList<String> params = new ArrayList<String>();
		Cursor c = null;
		try {
			// /lay danh sach level cua cttb
			dto.arrLevelCode = getSupervisorArrayLevelCode(shopId, listStaffID);
			for (int i = 0; i < dto.arrLevelCode.size(); i++) {
				ReportDisplayProgressResult rs = new ReportDisplayProgressResult();
				dto.totalItem.arrLevelCode.add(rs);
			}
			StringBuffer var1 = new StringBuffer();
			var1.append("SELECT display_programe_id, ");
			var1.append("       display_programe_code, ");
			var1.append("       display_programe_name, ");
			var1.append("       from_date, ");
			var1.append("       to_date, ");
			var1.append("       Group_concat(num_join)                            NUM_JOIN, ");
			var1.append("       Group_concat(num_non_complete)                    NUM_NON_COMPLETE, ");
			var1.append("       Group_concat(DISTINCT display_program_level_code) LEVEL_CODE ");
			var1.append("FROM   (SELECT RPT_DP.display_program_id         DISPLAY_PROGRAME_ID, ");
			var1.append("               RPT_DP.display_program_code       DISPLAY_PROGRAME_CODE, ");
			var1.append("               RPT_DP.display_program_name       DISPLAY_PROGRAME_NAME, ");
			var1.append("               RPT_DP.display_program_level_code DISPLAY_PROGRAM_LEVEL_CODE, ");
			var1.append("               RPT_DP.from_date                  FROM_DATE, ");
			var1.append("               RPT_DP.to_date                    TO_DATE, ");
			var1.append("               Count(RPT_DP.result)              NUM_JOIN, ");
			var1.append("               Sum(CASE RPT_DP.result ");
			var1.append("                     WHEN 0 THEN 1 ");
			var1.append("                     ELSE 0 ");
			var1.append("                   END)                          NUM_NON_COMPLETE ");
			var1.append("        FROM   rpt_display_program RPT_DP ");
			var1.append("        WHERE  1 = 1 ");
			var1.append("               AND RPT_DP.shop_id = ? ");
			params.add(shopId);
			var1.append("               AND RPT_DP.staff_id IN( ");
			var1.append(listStaffID);
			var1.append("               ) ");
			var1.append("               AND substr(RPT_DP.from_date, 1, 10) <= ? ");
			params.add(dateNow);
			var1.append("               AND Ifnull(substr(RPT_DP.to_date, 1, 10) >= ?, 1) ");
			params.add(dateNow);
			var1.append("               AND substr(RPT_DP.month, 1, 7) = ? ");
			params.add(monthNow);
			var1.append("               AND RPT_DP.status = 1 ");
			if(!GlobalInfo.getInstance().isSysShowPrice()){
				var1.append("               AND RPT_DP.display_program_type <> ? ");
				params.add(ReportDisplayProgressDetailDayRowDTO.TYPE_AMOUNT + "");
			}
			var1.append("               AND RPT_DP.status = 1 ");
			var1.append("        GROUP  BY rpt_dp.display_program_id, ");
			var1.append("                  rpt_dp.display_program_level_id ");
//			var1.append("        ORDER  BY rpt_dp.display_program_id, ");
			var1.append("        ORDER  BY  RPT_DP.display_program_level_code) ");
			var1.append("GROUP  BY display_programe_id ");
			
			var1.append("ORDER  BY display_programe_code, ");
			var1.append("          display_programe_name ");

			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;
			if (c.moveToFirst()) {
				do {
					SupervisorReportDisplayProgressItem item = new SupervisorReportDisplayProgressItem();
					item.programId = CursorUtil.getString(c, DISPLAY_PROGRAME_ID);
					item.programCode = CursorUtil.getString(c, DISPLAY_PROGRAME_CODE);
					item.programName = CursorUtil.getString(c, DISPLAY_PROGRAME_NAME);
					String from_date = CursorUtil.getString(c, FROM_DATE);
					String to_date = CursorUtil.getString(c, TO_DATE);
					try {
						if (!StringUtil.isNullOrEmpty(from_date))
							item.dateFromTo = sfd.format(sfs.parse(from_date));
						if (!StringUtil.isNullOrEmpty(to_date))
							item.dateFromTo += " - "
									+ sfd.format(sfs.parse(to_date));
					} catch (Exception e) {
						item.dateFromTo = "";
						MyLog.e("getDisProComProReportDTO", "fail", e);
					}
					dto.arrList.add(item);


					String numJoin = CursorUtil.getString(c, "NUM_JOIN");
					String numNonComplete = CursorUtil.getString(c, "NUM_NON_COMPLETE");
					String levelCode = CursorUtil.getString(c, "LEVEL_CODE");
					String[] numJoinArray = numJoin.split(",");
					String[] numNonCompleteArray = numNonComplete.split(",");
					String[] rptLevelCodeArray = levelCode.split(",");
					ArrayList<String> levelCodeArray = getArrayLevelCodeOfDisplayPrograme(item.programId);
					for (int i = 0, size =dto.arrLevelCode.size() ; i < size; i++) {
						ReportDisplayProgressResult rs = new ReportDisplayProgressResult();
						item.arrLevelCode.add(rs);
						boolean isExist = false;
						for(int j = 0, levelSize = levelCodeArray.size(); j < levelSize; j++){
							if(levelCodeArray.get(j).equals(dto.arrLevelCode.get(i))){
								isExist = true;
								break;
							}
						}
						if(!isExist)
							levelCodeArray.add(i, dto.arrLevelCode.get(i));
					}
					for (int i = 0, size = dto.arrLevelCode.size(); i < size; i++) {
						ReportDisplayProgressResult rs = item.arrLevelCode
								.get(i);
						for (int j = 0; j < rptLevelCodeArray.length; j++) {
							if (levelCodeArray.get(i).equals(
									rptLevelCodeArray[j])) {
								rs.joinNumber = Integer
										.parseInt(numJoinArray[j]);
								rs.resultNumber = Integer
										.parseInt(numNonCompleteArray[j]);
								rs.levelCode = levelCodeArray.get(i);
								item.itemResultTotal.joinNumber += rs.joinNumber;
								item.itemResultTotal.resultNumber += rs.resultNumber;
								item.itemResultTotal.levelCode = levelCodeArray
										.get(i);

								dto.totalItem.arrLevelCode.get(i).joinNumber += rs.joinNumber;
								dto.totalItem.arrLevelCode.get(i).resultNumber += rs.resultNumber;
								dto.totalItem.arrLevelCode.get(i).levelCode = levelCodeArray
										.get(i);

								dto.totalItem.itemResultTotal.joinNumber += rs.joinNumber;
								dto.totalItem.itemResultTotal.resultNumber += rs.resultNumber;
							}
						}
					}
				} while (c.moveToNext());
			}
			int numPlanDate = new EXCEPTION_DAY_TABLE(mDB)
					.getPlanWorkingDaysOfMonth(new Date(), shopId);
			int numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB)
					.getCurrentWorkingDaysOfMonth(shopId);
			dto.standardProgress = StringUtil.calPercentUsingRound(numPlanDate, numPlanDateDone);
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 *
	 * lay danh sach id cac chuong trinh trung bay duoc tong hop trong bang rpt
	 * display program
	 *
	 * @author: HaiTC3
	 * @param shopId
	 * @param staffId
	 * @return
	 * @return: String
	 * @throws:
	 * @since: Feb 22, 2013
	 */
	public String getListDisplayProgramHaveRPTDisplayProgramTable(
			String shopId, String staffId) {
		String listDis = "";
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT RDP.display_program_id DISPLAY_PROGRAME_ID ");
			sqlQuery.append("FROM   rpt_display_program RDP, ");
			sqlQuery.append("       display_program_level DPL ");
			sqlQuery.append("       LEFT JOIN staff s ");
			sqlQuery.append("              ON RDP.staff_id = s.staff_id ");
			sqlQuery.append("WHERE  Date(from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("       AND ifnull(Date('now', 'localtime') <= Date(to_date), 1)  ");
			sqlQuery.append("       AND Date(RDP.month, 'start of month') = ");
			sqlQuery.append("           Date('now', 'localtime','start of month') ");
			sqlQuery.append("       AND s.status > 0 ");
			sqlQuery.append("       AND DPL.status = 1 ");
			sqlQuery.append("       AND DPL.display_program_id = RDP.display_program_id ");
			sqlQuery.append("       AND RDP.parent_staff_id = ? ");
			sqlQuery.append("       AND RDP.shop_id = ? ");
			sqlQuery.append("GROUP  BY RDP.display_program_id ");

			String[] params = new String[] { staffId, shopId };
			c = this.rawQuery(sqlQuery.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						listDis += CursorUtil.getString(c, "DISPLAY_PROGRAME_ID");
						listDis += ",";
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			listDis = "";
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		if (!StringUtil.isNullOrEmpty(listDis)) {
			listDis = listDis.substring(0, listDis.length() - 1);
		}
		return listDis;

	}

	/**
	 * get report by level code of display programe
	 *
	 * @param item
	 * @param arrLevel
	 * @param parentStaffId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getArrayLevelCodeOfDisplayPrograme(
			String programeId) throws Exception {
		ArrayList<String> result = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT Group_concat(DISTINCT level_code) LEVEL_CODE ");
		var1.append("FROM   (SELECT DPL.display_program_id, ");
		var1.append("               DPL.level_code LEVEL_CODE ");
		var1.append("        FROM   display_program_level DPL ");
		var1.append("        WHERE  DPL.display_program_id = ? ");
		var1.append("               AND DPL.status = 1 ");
		var1.append("        ORDER  BY DPL.level_code) ");
		var1.append("GROUP  BY display_program_id ");
		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					new String[] { programeId });
			if (c.moveToFirst()) {
				String levelCode = CursorUtil.getString(c, "LEVEL_CODE");
				String[] levelCodeArray = levelCode.split(",");
				for (int i = 0, size = levelCodeArray.length; i < size; i++) {
					result.add(levelCodeArray[i]);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		return result;
	}

	/**
	 * Lay Level cho man hinh :01-06. Bao cao tien do chung CTTB ngay (TBHV)
	 *
	 * @author: BangHN
	 * @param parentStaffId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<String> getTBHVArrayLevelCodeVNM() throws Exception {
		ArrayList<String> ret = new ArrayList<String>();
		Cursor c = null;

//		try {
//			StringBuffer var1 = new StringBuffer();
//			var1.append("SELECT Max(num) NUM_LEVEL ");
//			var1.append("FROM   (SELECT Count(DISTINCT DPL.level_code) NUM ");
//			var1.append("        FROM   display_program_level DPL, ");
//			var1.append("               rpt_display_program RPT_DP ");
//			var1.append("        WHERE  DPL.display_program_id = RPT_DP.display_program_id ");
//			var1.append("               AND RPT_DP.status = 1 ");
//			var1.append("               AND Date(RPT_DP.month, 'start of month') = ");
//			var1.append("                   Date('now', 'localtime', 'start of month' ");
//			var1.append("                   ) ");
//			var1.append("               AND Date(RPT_DP.from_date) <= Date('now', 'localtime') ");
//			var1.append("               AND Ifnull(Date(RPT_DP.to_date) >= Date('now', 'localtime'), 1) ");
//			var1.append("        GROUP  BY DPL.display_program_id) ");
//			c = this.rawQuery(var1.toString(), null);
//			int maxLevel = 0;
//			if (c.moveToFirst()) {
//				maxLevel = CursorUtil.getInt(c, "NUM_LEVEL");
//			}
//			for (int i = 0; i < maxLevel; i++) {
//				ret.add(Constants.LEVEL_CODE_ARRAY[i]);
//			}
//		} catch (Exception e) {
//			throw e;
//		} finally {
//			try {
//				if (c != null) {
//					c.close();
//				}
//			} catch (Exception e) {
//				// TODO: handle exception
//			}
//		}
		return ret;
	}

	/**
	 * Hoan2 get DTO for TBHVDisProComProgReport 01-04. Bao cao tien do chung
	 * CTTB ngay (TBHV)
	 *
	 * @author: HoanPD1
	 * @param nvbhShopId
	 * @return
	 * @throws Exception
	 */
	public TBHVDisProComProgReportDTO getTBHVDisProComProReportDTO(Bundle data) throws Exception {

		Cursor c = null;
		Cursor ctotal = null;
		String displayProgrameCode = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAME_CODE);
		String parentShopId = data
				.getString(IntentConstants.INTENT_PARENT_SHOP_ID);
		String displayProgrameId = data
				.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		ArrayList<String> params = new ArrayList<String>();
		boolean isGetCountTotal = false;// data.getBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable
				.getShopRecursiveReverse(parentShopId);
		String shopStr = TextUtils.join(",", shopList);
		String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		String listStaff = PriUtils.getInstance().getListSupervisorOfASMIncludeHistory(staffIdASM);

		TBHVDisProComProgReportDTO dto = new TBHVDisProComProgReportDTO();
		if (StringUtil.isNullOrEmpty(displayProgrameId)) {
			dto.listDisplayProgrameInfo = this
					.getListDisplayProgrameForTBHV(parentShopId);
			if (dto.listDisplayProgrameInfo.size() > 0) {
				displayProgrameCode = dto.listDisplayProgrameInfo.get(0).displayProgramCode;
				dto.arrLevelCodeInDB = getArrayLevelCodeOfDisplayPrograme(dto.listDisplayProgrameInfo
						.get(0).displayProgrameID);
			}
		} else {
			dto.arrLevelCodeInDB = getArrayLevelCodeOfDisplayPrograme(displayProgrameId);
		}
		try {
			// /lay danh sach level cua cttb
			dto.arrLevelCodeInView = getTBHVArrayLevelCodeVNM();

			StringBuffer var1 = new StringBuffer();
			var1.append("SELECT DISTINCT S.shop_id                         NPP_ID, ");
			var1.append("                S.shop_code                       NPP_CODE, ");
			var1.append("                S.shop_name                       NPP_NAME, ");
			var1.append("                NPP_LIST.staff_id                 GSNPP_ID, ");
			var1.append("                NPP_LIST.staff_code               GSNPP_CODE, ");
			var1.append("                NPP_LIST.staff_name               GSNPP_NAME, ");
			var1.append("                RPT_DP.display_program_name       DP_NAME, ");
			var1.append("                RPT_DP.display_program_code       DP_CODE, ");
			var1.append("                RPT_DP.display_program_level_code DP_LEVEL, ");
			var1.append("                Count(RPT_DP.staff_id)            NUM_JOIN, ");
			var1.append("                Sum(CASE RPT_DP.result ");
			var1.append("                      WHEN 0 THEN 1 ");
			var1.append("                      ELSE 0 ");
			var1.append("                    END)                          NUM_NON_COMPLETE ");
			var1.append("FROM   (SELECT GS.staff_id   AS STAFF_ID, ");
			var1.append("               GS.staff_code AS STAFF_CODE, ");
			var1.append("               GS.staff_name AS STAFF_NAME ");
//			var1.append("        FROM   staff GS ");
			var1.append("       FROM (SELECT s.staff_id, ");
			var1.append("                         s.staff_code, ");
			var1.append("                         s.staff_name, ");
			var1.append("                         s.mobilephone, ");
			var1.append("                         s.shop_id ");
			var1.append("                  FROM   staff s ");
			var1.append("                  UNION ");
			var1.append("                  SELECT sh.staff_id AS staff_id, ");
			var1.append("                         sh.staff_code, ");
			var1.append("                         sh.staff_name, ");
			var1.append("                         sh.mobilephone, ");
			var1.append("                         sh.shop_id ");
			var1.append("                  FROM   staff_history sh ");
			var1.append("                  WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var1.append("                  AND Date(sh.from_date) >= Date('now', 'localtime','start of month') ");
			var1.append("                         AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			var1.append("                                                   'start of month') ");
			var1.append("                                OR sh.to_date IS NULL ) ");
			var1.append("                         AND sh.status = 1) GS ");
			var1.append("        WHERE  GS.staff_id IN ( ");
			var1.append(listStaff);
			var1.append("       		) ");
			var1.append("       ) NPP_LIST, ");
			var1.append("       shop S, ");
			var1.append("       rpt_display_program RPT_DP ");
			var1.append("WHERE  1 = 1 ");
			var1.append("       AND RPT_DP.shop_id = S.shop_id ");
			var1.append("       AND RPT_DP.shop_id IN ( ");
			var1.append(shopStr);
			var1.append("       	) ");
			//var1.append("       and NPP_LIST.shop_id = S.shop_id ");
			var1.append("       AND NPP_LIST.staff_id = RPT_DP.parent_staff_id ");
			var1.append("       AND Date(RPT_DP.from_date) <= Date('now', 'localtime') ");
			var1.append("       AND ( Date(RPT_DP.to_date) >= Date('now', 'localtime') ");
			var1.append("              OR Date(RPT_DP.to_date) IS NULL ) ");
			var1.append("       AND Date(RPT_DP.month, 'start of month') = ");
			var1.append("           Date('now', 'localtime', 'start of month' ");
			var1.append("           ) ");
			// chon xem theo ct
			if (!StringUtil.isNullOrEmpty(displayProgrameCode)) {
				var1.append("       AND  RPT_DP.display_program_code like ?");
				params.add(displayProgrameCode);
			}

			var1.append("GROUP  BY S.shop_id, ");
			var1.append("          S.shop_code, ");
			var1.append("          S.shop_name, ");
			var1.append("          NPP_LIST.staff_id, ");
			var1.append("          NPP_LIST.staff_code, ");
			var1.append("          NPP_LIST.staff_name, ");
			var1.append("          dp_name, ");
			var1.append("          dp_code, ");
			var1.append("          dp_level ");
			var1.append("ORDER  BY npp_code, ");
			var1.append("          gsnpp_name, ");
			var1.append("          dp_code, ");
			var1.append("          dp_name ");

			/*String sqlQueryGetTotal = " select count(*) as total_row from ("
					+ var1.toString() + ") ";

			// get total
			int total = 0;
			if (isGetCountTotal) {
				ctotal = rawQuery(sqlQueryGetTotal,
						params.toArray(new String[params.size()]));
				if (ctotal != null) {
					ctotal.moveToFirst();
					total = ctotal.getInt(0);
					dto.totalItem = total;
				}
			}*/

			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			for (int i = 0; i < dto.arrLevelCodeInView.size(); i++) {
				TBHVDisProComProgReportItemResult rs = dto
						.newDisProComProgReportItemResult();
				dto.arrResultTotal.add(rs);
			}
			if (c != null && c.moveToFirst()) {
				do {
					TBHVDisProComProgReportItem item = dto
							.newDisProComProgReportItem();
					item.initWithCursor(c, dto);
					dto.arrList.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			MyLog.e("getTBHVDisProComProReportDTO", "fail", e);
			dto = null;
		} finally {
			try {
				 if (ctotal != null) {
				 ctotal.close();
				 }
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		if (dto != null) {
			int numPlanDate = new EXCEPTION_DAY_TABLE(mDB)
					.getPlanWorkingDaysOfMonth(new Date(), parentShopId);
			int numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB)
					.getCurrentWorkingDaysOfMonth(parentShopId);
			dto.progressStandarPercent = Math.floor(numPlanDate > 0 ? numPlanDateDone * 100.0
					/ numPlanDate : 0);
		}

		return dto;
	}

	/**
	 *
	 * get list display programe for tbhv
	 *
	 * @param data
	 * @return
	 * @return: ArrayList<DisplayPresentProductInfo>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 30, 2012
	 */
	public ArrayList<DisplayPresentProductInfo> getListDisplayProgrameForTBHV(
			String parentShopId) throws Exception{
		ArrayList<DisplayPresentProductInfo> result = new ArrayList<DisplayPresentProductInfo>();
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listParentShopId = shopTB
				.getShopRecursive(parentShopId);
		ArrayList<String> listShopId = shopTB
				.getShopRecursiveReverse(parentShopId);
		String strListParentShop = TextUtils.join(",", listParentShopId);
		String strListShop = TextUtils.join(",", listShopId);
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT RPT_DP.display_program_code AS DISPLAY_PROGRAM_CODE, ");
			sqlQuery.append("                RPT_DP.display_program_name AS DISPLAY_PROGRAM_NAME,  ");
			sqlQuery.append("				 RPT_DP.display_program_id as   DISPLAY_PROGRAM_ID ");
			sqlQuery.append("FROM   rpt_display_program RPT_DP ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND RPT_DP.shop_id IN ( ");
			sqlQuery.append(strListParentShop + ", " + strListShop + ") ");
			sqlQuery.append("       AND Date(RPT_DP.from_date) <= Date('NOW', 'LOCALTIME') ");
			sqlQuery.append("       AND Ifnull(Date(RPT_DP.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");
			sqlQuery.append("       AND Date(RPT_DP.month, 'start of month') = ");
			sqlQuery.append("           Date('now','localtime', 'start of month' ");
			sqlQuery.append("           ) ");
			sqlQuery.append("ORDER  BY RPT_DP.display_program_code ");

			String params[] = new String[] {};
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c.moveToFirst()) {
				do {
					DisplayPresentProductInfo item = new DisplayPresentProductInfo();
					item.initDisplayPresentProductInfo(c);
					result.add(item);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get list display programe for NPP
	 *
	 * @param shopId
	 * @return
	 * @return: ArrayList<DisplayPresentProductInfo>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 31, 2012
	 */
	public ArrayList<DisplayPresentProductInfo> getListDisplayProgrameForNPP(
			String shopId) {
		ArrayList<DisplayPresentProductInfo> result = new ArrayList<DisplayPresentProductInfo>();
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT RPT_DP.[display_program_id] DISPLAY_PROGRAM_ID, ");
			sqlQuery.append("                RPT_DP.[display_program_code] DISPLAY_PROGRAM_CODE, ");
			sqlQuery.append("                RPT_DP.[display_program_name] DISPLAY_PROGRAM_NAME ");
			sqlQuery.append("FROM   rpt_display_program RPT_DP ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND RPT_DP.shop_id = ? ");
			sqlQuery.append("       AND Date(RPT_DP.from_date) <= Date('NOW', 'LOCALTIME') ");
			sqlQuery.append("       AND Ifnull(Date(RPT_DP.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");
			sqlQuery.append("       AND Date(RPT_DP.month, 'start of month') = ");
			sqlQuery.append("           Date('now', 'localtime', 'start of month' ");
			sqlQuery.append("           ) ");
			sqlQuery.append("ORDER  BY RPT_DP.display_program_code ");

			String params[] = new String[] { shopId };
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c.moveToFirst()) {
				do {
					DisplayPresentProductInfo item = new DisplayPresentProductInfo();
					item.initDisplayPresentProductInfo(c);
					result.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			result = null;
			// TODO: handle exception
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get list GSNPP of NPP
	 *
	 * @param shopId
	 * @return
	 * @return: ArrayList<GSNPPInfoDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 31, 2012
	 */
	public ArrayList<GSNPPInfoDTO> getListGSNPPOfNPP(String shopId) {
		ArrayList<GSNPPInfoDTO> result = new ArrayList<GSNPPInfoDTO>();
		Cursor c = null;
		try {
			// StringBuffer sqlQuery = new StringBuffer();
			// sqlQuery.append("SELECT DISTINCT ST.staff_id   STAFF_ID, ");
			// sqlQuery.append("                ST.staff_code STAFF_CODE, ");
			// sqlQuery.append("                ST.name       NAME ");
			// sqlQuery.append("FROM   rpt_display_programe RPT_DP, ");
			// sqlQuery.append("       staff ST, ");
			// sqlQuery.append("       staff ST2 ");
			// sqlQuery.append("WHERE  1 = 1 ");
			// sqlQuery.append("       AND RPT_DP.shop_id = ? ");
			// sqlQuery.append("       AND ST.staff_id = RPT_DP.parent_staff_id ");
			// sqlQuery.append("       AND ST.status = 1 ");
			// sqlQuery.append("       AND ST2.staff_id = RPT_DP.staff_id ");
			// sqlQuery.append("       AND ST2.status = 1 ");
			// sqlQuery.append("       AND Date(RPT_DP.from_date) <= Date('now', 'localtime') ");
			// sqlQuery.append("       AND Ifnull(Date(RPT_DP.to_date) >= Date('now', 'localtime'), 1) ");
			// sqlQuery.append("       AND Date(RPT_DP.month, 'start of month') = ");
			// sqlQuery.append("           Date('now', 'start of month', 'localtime' ");
			// sqlQuery.append("           ) ");
			// sqlQuery.append("GROUP  BY ST.staff_id, ");
			// sqlQuery.append("          ST.staff_code, ");
			// sqlQuery.append("          ST.name ");
			// sqlQuery.append("ORDER  BY ST.staff_code ");
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT ST.staff_id   STAFF_ID, ");
			sqlQuery.append("                ST.staff_code STAFF_CODE, ");
			sqlQuery.append("                ST.staff_name STAFF_NAME ");
			sqlQuery.append("FROM   rpt_display_program RPT_DP, ");
			sqlQuery.append("       staff ST, ");
			sqlQuery.append("       staff ST2 ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND RPT_DP.shop_id = ? ");
			sqlQuery.append("       AND ST.staff_id = RPT_DP.parent_staff_id ");
			sqlQuery.append("       AND ST.status = 1 ");
			sqlQuery.append("       AND ST2.staff_id = RPT_DP.staff_id ");
			sqlQuery.append("       AND ST2.status = 1 ");
			sqlQuery.append("       AND Date(RPT_DP.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("       AND Ifnull(Date(RPT_DP.to_date) >= Date('now', 'localtime'), 1) ");
			sqlQuery.append("       AND Date(RPT_DP.month, 'start of month') = ");
			sqlQuery.append("           Date('now', 'localtime','start of month' ");
			sqlQuery.append("           ) ");
			sqlQuery.append("GROUP  BY ST.staff_id, ");
			sqlQuery.append("          ST.staff_code, ");
			sqlQuery.append("          ST.staff_name ");
			sqlQuery.append("ORDER  BY ST.staff_code ");

			String params[] = new String[] { shopId };
			c = this.rawQuery(sqlQuery.toString(), params);
			if (c.moveToFirst()) {
				do {
					GSNPPInfoDTO item = new GSNPPInfoDTO();
					item.initObjectWithCursor(c);
					result.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			result = null;
			// TODO: handle exception
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 * Lay bao cao tien do CTTB cua tung NVBH
	 *
	 * @author: Nguyen Thanh Dung
	 * @param bundle
	 * @return
	 */
	public ReportDisplayProgressDetailCustomerDTO getReportDisplayProgressDetailCustomer(
			Bundle data) throws Exception {
		ReportDisplayProgressDetailCustomerDTO dto = new ReportDisplayProgressDetailCustomerDTO();
		String rptStDisplayProgramId = data
				.getString(IntentConstants.INTENT_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);

		ArrayList<String> paramsGroup = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT D_PDG.display_dp_group_code, ");
		var1.append("       D_PDG.display_dp_group_name, ");
		var1.append("       D_PDG.type, ");
		var1.append("       ( CASE D_PDG.type ");
		var1.append("           WHEN 1 THEN RPT_DP.amount_plan ");
		var1.append("           WHEN 2 THEN RPT_DP.quantity_plan ");
		var1.append("           WHEN 3 THEN RPT_DP.num_sku_plan ");
		var1.append("         END ) PLAN, ");
		var1.append("       ( CASE D_PDG.type ");
		var1.append("           WHEN 1 THEN RPT_DP.amount  ");
		var1.append("           WHEN 2 THEN RPT_DP.quantity ");
		var1.append("           WHEN 3 THEN RPT_DP.num_sku ");
		var1.append("         END ) ACTUAL ");
		var1.append("FROM   display_product_group D_PDG, ");
		var1.append("       rpt_display_program RPT_DP ");
		var1.append("WHERE  1 = 1 ");
		var1.append("       AND D_PDG.display_program_id = RPT_DP.display_program_id ");
		var1.append("       AND RPT_DP.rpt_display_program_id = ? ");
		paramsGroup.add(rptStDisplayProgramId);
		var1.append("       AND RPT_DP.status = 1 ");
		var1.append("       AND D_PDG.status = 1 ");
		var1.append("       AND Date(RPT_DP.month, 'start of month') = ");
		var1.append("           Date('now', 'localtime', 'start of month' ");
		var1.append("           ) ");
		var1.append("       AND D_PDG.type IN ( 1, 2, 3 ) ");
		var1.append("GROUP  BY D_PDG.type ");
		var1.append("ORDER  BY D_PDG.type ");

		StringBuffer sqlQuery = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sqlQuery.append("SELECT RPT_DPD.display_pd_group_code DISPLAY_DP_GROUP_CODE, ");
		sqlQuery.append("       RPT_DPD.display_pd_group_name DISPLAY_DP_GROUP_NAME, ");
		sqlQuery.append("       ( CASE RPT_DPD.display_pd_group_type ");
		sqlQuery.append("           WHEN 1 THEN RPT_DPD.PLAN ");
		sqlQuery.append("           ELSE RPT_DPD.PLAN ");
		sqlQuery.append("         END )                       PLAN, ");
		sqlQuery.append("       ( CASE RPT_DPD.display_pd_group_type ");
		sqlQuery.append("           WHEN 1 THEN RPT_DPD.actual ");
		sqlQuery.append("           ELSE RPT_DPD.actual ");
		sqlQuery.append("         END )                       ACTUAL, ");
		sqlQuery.append("       RPT_DPD.display_pd_group_type TYPE ");
		sqlQuery.append("FROM   rpt_display_program_dtl RPT_DPD ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND RPT_DPD.rpt_display_program_id = ? ");
		params.add(rptStDisplayProgramId);
		sqlQuery.append("       AND Date(RPT_DPD.month, 'start of month') = ");
		sqlQuery.append("           Date('now', 'localtime', 'start of month') ");
		sqlQuery.append("ORDER  BY RPT_DPD.display_pd_group_type ");

		Cursor c = null;
		Cursor groupC = null;
		try {
			groupC = rawQuery(var1.toString(),
					paramsGroup.toArray(new String[paramsGroup.size()]));
			ArrayList<ReportDisplayProgressDetailCustomerRowDTO> groupList = new ArrayList<ReportDisplayProgressDetailCustomerRowDTO>();
			if (groupC.moveToFirst()) {
				do {
					ReportDisplayProgressDetailCustomerRowDTO item = new ReportDisplayProgressDetailCustomerRowDTO();
					item.parseReportDisplayProgressDetailCustomerRow(groupC);
					item.isHeader = true;
					switch (item.type) {
					case ReportDisplayProgressDetailCustomerRowDTO.TYPE_AMOUNT: {
						item.groupCode = StringUtil.getReportUnitTitle(StringUtil.getString(R.string.TEXT_GROUP_DS));
						break;
					}
					case ReportDisplayProgressDetailCustomerRowDTO.TYPE_QUANTITY: {
						item.groupCode = StringUtil.getString(R.string.TEXT_GROUP_SL);
						break;
					}
					case ReportDisplayProgressDetailCustomerRowDTO.TYPE_SKU: {
						item.groupCode = StringUtil.getString(R.string.TEXT_REQUIRED_MH);
						break;
					}
					default:
						break;
					}
					groupList.add(item);
				} while (groupC.moveToNext());
			}

			c = rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			ArrayList<ReportDisplayProgressDetailCustomerRowDTO> arrayList = new ArrayList<ReportDisplayProgressDetailCustomerRowDTO>();
			if (c.moveToFirst()) {
				do {
					ReportDisplayProgressDetailCustomerRowDTO item = new ReportDisplayProgressDetailCustomerRowDTO();
					item.parseReportDisplayProgressDetailCustomerRow(c);
					arrayList.add(item);
				} while (c.moveToNext());
			}

			for (ReportDisplayProgressDetailCustomerRowDTO group : groupList) {
				dto.listItem.add(group);
				for (ReportDisplayProgressDetailCustomerRowDTO item : arrayList) {
					if (group.type == item.type) {
						dto.listItem.add(item);
					}
				}
			}

			int numPlanDate = new EXCEPTION_DAY_TABLE(mDB)
			.getPlanWorkingDaysOfMonth(new Date(), shopId);
			int numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB)
					.getCurrentWorkingDaysOfMonth(shopId);
			dto.standardProgress = StringUtil.calPercentUsingRound(numPlanDate, numPlanDateDone);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (groupC != null) {
					groupC.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * get report by level code of display programe
	 *
	 * @param item
	 * @param arrLevel
	 * @param staffId
	 * @return
	 */
	public ArrayList<DisProComProgReportDTO.DisProComProgReportItemResult> getArrLevelCodeOfCTTB(
			DisProComProgReportDTO.DisProComProgReportItem item, ArrayList<String> arrLevel,
			int staffId) {
		ArrayList<DisProComProgReportDTO.DisProComProgReportItemResult> ret = new ArrayList<DisProComProgReportDTO.DisProComProgReportItemResult>();
		for (int i = 0; i < arrLevel.size(); i++) {
			String levelCode = arrLevel.get(i);
			DisProComProgReportDTO.DisProComProgReportItemResult rs = item
					.NewDisProComProgReportItemResult();
			// tinh dat/tham gia
			String sqlQuery = "select count(*) JOIN_NUMBER,"
					+ " sum(case when RPT_DISPLAY_PROGRAME.RESULT = 0 then 1 else 0 end) RESULT_NUMBER"
					+ " from RPT_DISPLAY_PROGRAME"
					+ " left join staff on RPT_DISPLAY_PROGRAME.staff_id = staff.staff_id"
					+ " where DISPLAY_PROGRAME_ID = ?"
					+ " and DISPLAY_PROGRAME_LEVEL = ?"
					+ " and PARENT_STAFF_ID = ?"
					+ " and dayInOrder(RPT_DISPLAY_PROGRAME.MONTH, 'start of month') = dayInOrder('now', 'localtime','start of month')"
					+ " and staff.status > 0"
					+ " group by DISPLAY_PROGRAME_ID, DISPLAY_PROGRAME_LEVEL";
			Cursor c = null;
			try {
				c = this.rawQuery(sqlQuery, new String[] {
						item.programId, levelCode, "" + staffId });
				if (c.moveToFirst()) {
					rs.joinNumber = CursorUtil.getInt(c, "JOIN_NUMBER");
					rs.resultNumber = CursorUtil.getInt(c, "RESULT_NUMBER");
				}
			} catch (Exception e) {
				MyLog.e("getArrLevelCodeOfCTTB", "fail", e);
			} finally{
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
			// sum lai
			item.itemResultTotal.joinNumber += rs.joinNumber;
			item.itemResultTotal.resultNumber += rs.resultNumber;
			ret.add(rs);
		}
		return ret;
	}

	/**
	 * get list display programe level code
	 *
	 * @param staffId
	 * @return
	 */
	public ArrayList<String> getArrayLevelCodeVNM(int staffId) {
		ArrayList<String> ret = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("select DISPLAY_PROGRAME_LEVEL from RPT_DISPLAY_PROGRAM");
		sqlQuery.append(" group by DISPLAY_PROGRAME_LEVEL");
		sqlQuery.append(" order by DISPLAY_PROGRAME_LEVEL");
		Cursor c = this.rawQuery(sqlQuery.toString(), null);
		if (c.moveToFirst()) {
			do {
				ret.add(CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL"));
			} while (c.moveToNext());
		}
		if (c != null) {
			c.close();
		}
		return ret;
	}

	/**
	 * lay ds level code cua cac CTTB cho MH bao cao CTTB
	 *
	 * @param staffId
	 * @return
	 */
	public ArrayList<String> getSupervisorArrayLevelCodeCTTB(int staffId) {
		ArrayList<String> ret = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("select DPL.LEVEL_CODE from RPT_DISPLAY_PROGRAM RDP, display_program_level DPL ");
		sqlQuery.append("WHERE Date(from_date) <= Date('now', 'localtime') ");
		sqlQuery.append(" AND ifnull(Date('now', 'localtime') <= Date(to_date),1) ");
		sqlQuery.append(" AND Date(RDP.month, 'start of month') = Date('now','localtime', 'start of month') ");
		sqlQuery.append(" and RDP.parent_staff_id = ? ");
		sqlQuery.append(" AND DPL.display_program_level_id = RDP.display_programe_level AND DPL.status = 1 ");
		sqlQuery.append(" group by DPL.LEVEL_CODE");
		sqlQuery.append(" order by DPL.LEVEL_CODE");
		String[] params = new String[] { "" + staffId };
		Cursor c = this.rawQuery(sqlQuery.toString(), params);
		if (c.moveToFirst()) {
			do {
				ret.add(CursorUtil.getString(c, "LEVEL_CODE"));
			} while (c.moveToNext());
		}
		if (c != null) {
			c.close();
		}
		return ret;
	}

	/**
	 *
	 * lay d/s chuong trinh trung bay, trong moi chuong trinh trung bay co 1 d/s
	 * cac level tuong ung
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: ArrayList<DisplayProgramLevelForProgramDTO>
	 * @throws:
	 * @since: Feb 19, 2013
	 */
	public ArrayList<DisplayProgramLevelForProgramDTO> getListDisplayProgramLevel(
			String listDisProIdShow) {
		ArrayList<DisplayProgramLevelForProgramDTO> listResult = new ArrayList<DisplayProgramLevelForProgramDTO>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DPL.display_program_id, ");
		sqlQuery.append("       DPL.level_code, ");
		sqlQuery.append("       DPL.amount ");
		sqlQuery.append("FROM   display_program_level DPL ");
		sqlQuery.append("WHERE  status = 1 ");
		sqlQuery.append(" AND DPL.display_program_id IN ( ");
		sqlQuery.append(listDisProIdShow + ") ");
		sqlQuery.append("GROUP  BY DPL.display_program_id, ");
		sqlQuery.append("          DPL.level_code, ");
		sqlQuery.append("          DPL.amount ");
		sqlQuery.append("ORDER  BY DPL.display_program_id, ");
		sqlQuery.append("          DPL.amount DESC ");

		String[] params = new String[] {};
		Cursor c = this.rawQuery(sqlQuery.toString(), params);
		ArrayList<DisplayProgrameLvDTO> listLevelNotFormat = new ArrayList<DisplayProgrameLvDTO>();
		if (c.moveToFirst()) {
			do {
				DisplayProgrameLvDTO item = new DisplayProgrameLvDTO();
				item.parseWithCursor(c);
				listLevelNotFormat.add(item);
			} while (c.moveToNext());
		}
		if (c != null) {
			c.close();
		}

		// tao danh sach gom cac chuong trinh trung bay va moi chuong trinh co 1
		// d/s cac level khac nhau
		DisplayProgramLevelForProgramDTO newItemDPLevel = new DisplayProgramLevelForProgramDTO();
		for (int i = 0, size = listLevelNotFormat.size(); i < size; i++) {
			if (i != 0
					&& newItemDPLevel.displayProgramId != listLevelNotFormat
							.get(i).dislayProgramId) {
				newItemDPLevel.maxDisProLevel = newItemDPLevel.listDisProLevel
						.size();
				listResult.add(newItemDPLevel);
				newItemDPLevel = new DisplayProgramLevelForProgramDTO();
			}
			newItemDPLevel.displayProgramId = listLevelNotFormat.get(i).dislayProgramId;
			newItemDPLevel.listDisProLevel.add(listLevelNotFormat.get(i));
		}
		// bo xung item cuoi cung vao d/s
		newItemDPLevel.maxDisProLevel = newItemDPLevel.listDisProLevel.size();
		listResult.add(newItemDPLevel);

		return listResult;
	}

	/**
	 * Lay Level cho man hinh :01-04. Bao cao tien do chung CTTB ngay (TBHV)
	 *
	 * @author: HoanPD1
	 * @param staffIdGS
	 * @return
	 */
	public ArrayList<String> getTBHVArrayLevelCodeVNM(String displayProgrameCode) {
		ArrayList<String> ret = new ArrayList<String>();
		Cursor c = null;
		// StringBuffer sqlQuery = new StringBuffer();
		// sqlQuery.append("SELECT DISTINCT ( CASE Ifnull(display_programe_level, '1') ");
		// sqlQuery.append("                    WHEN '' THEN 'A' ");
		// sqlQuery.append("                    WHEN '1' THEN 'A' ");
		// sqlQuery.append("                    WHEN '2' THEN 'B' ");
		// sqlQuery.append("                    WHEN '3' THEN 'C' ");
		// sqlQuery.append("                    WHEN '4' THEN 'D' ");
		// sqlQuery.append("                    WHEN '5' THEN 'E' ");
		// sqlQuery.append("                    ELSE display_programe_level ");
		// sqlQuery.append("                  END ) DISPLAY_PROGRAME_LEVEL ");
		// sqlQuery.append("FROM   rpt_display_program ");
		// sqlQuery.append("where 1=1 and DISPLAY_PROGRAME_CODE = ?");
		// sqlQuery.append("GROUP  BY display_programe_level ");
		// sqlQuery.append("ORDER  BY display_programe_level ");
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT rdp.display_programe_level ");
		sqlQuery.append("FROM   rpt_display_program rdp, ");
		sqlQuery.append("       display_program_level dpl ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND rdp.display_programe_code = ? ");
		sqlQuery.append("       AND rdp.display_programe_level = dpl.level_code ");
		sqlQuery.append("GROUP  BY rdp.display_programe_level ");
		sqlQuery.append("ORDER  BY rdp.display_programe_level ");

		String params[] = new String[] { displayProgrameCode };
		c = this.rawQuery(sqlQuery.toString(), params);
		if (c.moveToFirst()) {
			do {
				ret.add(CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL"));
			} while (c.moveToNext());
		}
		if (c != null) {
			c.close();
		}
		return ret;
	}

	/**
	 * get DTO for report display programe of staff
	 *
	 * @param staffId
	 * @param proId
	 * @return
	 */
	public DisProComProgReportDTO getStaffDisProComProReportDTO(int staffId,
			int shopId, int proId) {
		Cursor c = null;
		DisProComProgReportDTO dto = new DisProComProgReportDTO();
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT STAFF_ID, ");
			sqlQuery.append("       STAFF_CODE, ");
			sqlQuery.append("       STAFF_NAME, ");
			sqlQuery.append("       display_programe_id DP_ID, ");
			sqlQuery.append("       Group_concat(level)            LEVEL, ");
			sqlQuery.append("       Group_concat (num_join)        NUM_JOIN, ");
			sqlQuery.append("       Group_concat(num_non_complete) NUM_NON_COMPLETE ");
			sqlQuery.append("FROM   (SELECT RDP.staff_id STAFF_ID, ");
			sqlQuery.append("               RDP.staff_code STAFF_CODE, ");
			sqlQuery.append("               RDP.staff_name STAFF_NAME, ");
			sqlQuery.append("               rdp.display_program_id display_programe_id, ");
			sqlQuery.append("               RDP.display_program_level_code LEVEL, ");
			sqlQuery.append("               Count(RDP.customer_id)       NUM_JOIN, ");
			sqlQuery.append("               Sum(CASE RDP.result ");
			sqlQuery.append("                     WHEN 1 THEN 0 ");
			sqlQuery.append("                     ELSE 1 ");
			sqlQuery.append("                   end)                  NUM_NON_COMPLETE ");
			sqlQuery.append("        FROM   rpt_display_program RDP ");
			sqlQuery.append("               LEFT JOIN staff s");
			sqlQuery.append("                      ON RDP.staff_id = s.staff_id ");
			sqlQuery.append("        WHERE  Date(from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("               AND ifnull(Date('now', 'localtime') <= Date(to_date),1) ");
			sqlQuery.append("               AND Date(RDP.month, 'start of month') = ");
			sqlQuery.append("                   Date('now', 'localtime','start of month') ");
			sqlQuery.append("               AND rdp.parent_staff_id = ? ");
			sqlQuery.append("               AND rdp.shop_id = ? ");
			sqlQuery.append("               AND rdp.display_program_id = ? ");
			sqlQuery.append("        GROUP  BY RDP.staff_id, ");
			sqlQuery.append("                  RDP.staff_code, ");
			sqlQuery.append("                  RDP.staff_name, ");
			sqlQuery.append("                  RDP.display_program_level_code ");
			sqlQuery.append("        ORDER  BY RDP.staff_code, RDP.staff_name) ");
			sqlQuery.append("GROUP  BY staff_code ");

			String[] params = new String[] { String.valueOf(staffId),
					String.valueOf(shopId), String.valueOf(proId) };
			c = this.rawQuery(sqlQuery.toString(), params);
			dto.arrayLevel = getListDisplayProgramLevel(this
					.getListDisplayProgramHaveRPTDisplayProgramTable(
							String.valueOf(shopId), String.valueOf(staffId)));
			int maxLevel = 0;
			for (int i = 0, size = dto.arrayLevel.size(); i < size; i++) {
				if (maxLevel < dto.arrayLevel.get(i).maxDisProLevel) {
					maxLevel = dto.arrayLevel.get(i).maxDisProLevel;
				}
			}
			// set max level display
			dto.maxLevelDisPlay = maxLevel;

			for (int i = 0; i < maxLevel; i++) {
				DisProComProgReportDTO.DisProComProgReportItemResult rs = dto
						.newDisProComProgReportItemResult();
				dto.arrResultTotal.add(rs);
			}
			if (c != null && c.moveToFirst()) {
				do {
					DisProComProgReportDTO.DisProComProgReportItem item = dto
							.newDisProComProgReportItem();
					item.initWithCursor(c, dto);
					dto.arrList.add(item);
				} while (c.moveToNext());
			}
			// return dto;
		} catch (Exception e) {
			dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;

	}

	/**
	 * lay du lieu man hinh : 01-07.Tien do CTTB theo NPP ngay (TBHV)
	 *
	 * @author: HoanPD1
	 * @param staffIdGS
	 * @param proId
	 * @return
	 * @throws Exception
	 */

	public TBHVDisProComProgReportNPPDTO getTBHVDisProComProReportNPP(
			Bundle data) throws Exception {
		Cursor c = null;
		TBHVDisProComProgReportNPPDTO dto = new TBHVDisProComProgReportNPPDTO();
		String strShopId = Constants.STR_BLANK;
		try {
			String strGSNPPId = Constants.STR_BLANK;
			String strCTTBCode = Constants.STR_BLANK;
			String strCTTBId = Constants.STR_BLANK;
			boolean isLoadListGSNPPAndListDisPro = false;
			if (data.getString(IntentConstants.INTENT_STAFF_ID) != null) {
				strGSNPPId = data.getString(IntentConstants.INTENT_STAFF_ID);
			}
			if (data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE) != null) {
				strCTTBCode = data
						.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE);
			}
			if (data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID) != null) {
				strCTTBId = data
						.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
			}
			if (data.getString(IntentConstants.INTENT_SHOP_ID) != null) {
				strShopId = data.getString(IntentConstants.INTENT_SHOP_ID);
			}
			isLoadListGSNPPAndListDisPro = data
					.getBoolean(IntentConstants.INTENT_IS_OR);

			// /lay danh sach level cua cttb
			dto.arrLevelCodeInView = getTBHVArrayLevelCodeVNM();

			if (isLoadListGSNPPAndListDisPro) {
				// GET LIST GSNPP
				dto.listGSNPPInfo = this.getListGSNPPOfNPP(strShopId);

				// get list CTTB
				dto.listDisplayProgrameInfo = this
						.getListDisplayProgrameForNPP(strShopId);
				int index = 0;
				for(int i = 0; i < dto.listDisplayProgrameInfo.size(); i++){
					if(strCTTBCode.equals(dto.listDisplayProgrameInfo.get(i).displayProgramCode)){
						index = i;
						break;
					}
				}
				dto.arrLevelInDB = getArrayLevelCodeOfDisplayPrograme(dto.listDisplayProgrameInfo
						.get(index).displayProgrameID);
			} else {
				dto.arrLevelInDB = getArrayLevelCodeOfDisplayPrograme(strCTTBId);
			}

			ArrayList<String> listParams = new ArrayList<String>();
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT RPT_DP.staff_id              STAFF_ID, ");
			sqlQuery.append("                RPT_DP.staff_code            STAFF_CODE, ");
			sqlQuery.append("                RPT_DP.staff_name            STAFF_NAME, ");
			sqlQuery.append("                ST.staff_id                  STAFF_OWNER_ID, ");
			sqlQuery.append("                ST.staff_code                STAFF_OWNER_CODE, ");
			sqlQuery.append("                ST.staff_name                STAFF_OWNER_NAME, ");
			sqlQuery.append("                RPT_DP.display_program_code DP_CODE, ");
			sqlQuery.append("                RPT_DP.display_program_name DP_NAME, ");
			sqlQuery.append("                RPT_DP.display_program_level_code DP_LEVEL, ");
			sqlQuery.append("                Count(RPT_DP.staff_id)    NUM_JOIN, ");
			sqlQuery.append("                Sum(CASE RPT_DP.RESULT ");
			sqlQuery.append("                      WHEN 0 THEN 1 ");
			sqlQuery.append("                      ELSE 0 ");
			sqlQuery.append("                    end)                     NUM_NON_COMPLETE ");
			sqlQuery.append("FROM   rpt_display_program RPT_DP, ");
//			sqlQuery.append("       staff ST ");
			sqlQuery.append("       (SELECT s.staff_id, ");
			sqlQuery.append("               s.staff_code, ");
			sqlQuery.append("               s.staff_name, ");
			sqlQuery.append("               s.mobilephone, ");
			sqlQuery.append("               s.shop_id, ");
			sqlQuery.append("               s.staff_type_id, ");
			sqlQuery.append("               s.status ");
			sqlQuery.append("        FROM   staff s ");
			sqlQuery.append("        UNION ");
			sqlQuery.append("        SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("               sh.staff_code, ");
			sqlQuery.append("               sh.staff_name, ");
			sqlQuery.append("               sh.mobilephone, ");
			sqlQuery.append("               sh.shop_id, ");
			sqlQuery.append("               sh.staff_type_id, ");
			sqlQuery.append("               sh.status ");
			sqlQuery.append("        FROM   staff_history sh ");
			sqlQuery.append("        WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("               AND Date(sh.from_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("               AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("                      OR sh.to_date IS NULL ) ");
			sqlQuery.append("               AND sh.status = 1) ST ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND RPT_DP.shop_id = ? ");
			listParams.add(strShopId);
			sqlQuery.append("       AND ST.staff_id = RPT_DP.parent_staff_id ");
			sqlQuery.append("       AND ST.shop_id = RPT_DP.shop_id ");
			sqlQuery.append("       AND ST.status = 1 ");

			if (!StringUtil.isNullOrEmpty(strGSNPPId)) {
				sqlQuery.append("       AND RPT_DP.parent_staff_id = ? ");
				listParams.add(strGSNPPId);
			} else {
				//neu lay tat ca, thi phai lay nhung GS duoc ASM quan ly truc tiep
				String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
				String listStaff = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);
				sqlQuery.append("       AND RPT_DP.parent_staff_id IN ( ");
				sqlQuery.append(listStaff);
				sqlQuery.append(" 		) ");
			}

			if (!StringUtil.isNullOrEmpty(strCTTBCode)) {
				sqlQuery.append("       AND RPT_DP.display_program_code = ? ");
				listParams.add(strCTTBCode);
			}

			sqlQuery.append("       AND Date(RPT_DP.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("       AND Ifnull(Date(RPT_DP.to_date) >= Date('now', 'localtime'), 1) ");
			sqlQuery.append("       AND Date(RPT_DP.month, 'start of month') = ");
			sqlQuery.append("           Date('now', 'localtime', 'start of month' ");
			sqlQuery.append("           ) ");
			sqlQuery.append("GROUP  BY RPT_DP.staff_id, ");
			sqlQuery.append("          RPT_DP.staff_code, ");
			sqlQuery.append("          RPT_DP.staff_name, ");
			sqlQuery.append("          ST.staff_id, ");
			sqlQuery.append("          ST.staff_code, ");
			sqlQuery.append("          ST.staff_name, ");
			sqlQuery.append("          RPT_DP.display_program_code, ");
			sqlQuery.append("          RPT_DP.display_program_name, ");
			sqlQuery.append("          RPT_DP.display_program_level_code ");
			sqlQuery.append("ORDER  BY RPT_DP.staff_code, ");
			sqlQuery.append("          RPT_DP.staff_name ");
			String[] params = listParams.toArray(new String[listParams.size()]);

			c = this.rawQuery(sqlQuery.toString(), params);
			// get liver ABCD
			for (int i = 0; i < dto.arrLevelCodeInView.size(); i++) {
				TBHVDisProComProgReportItemResult rs = new TBHVDisProComProgReportItemResult();
				dto.arrResultTotal.add(rs);
			}
			if (c != null && c.moveToFirst()) {
				do {
					TBHVDisProComProgReportItem item = new TBHVDisProComProgReportItem();
					item.initWithCursorForNPP(c, dto);
					dto.arrList.add(item);
				} while (c.moveToNext());
			}
			// return dto;
		} catch (Exception e) {
			dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		
		if (dto != null) {
			int numPlanDate = new EXCEPTION_DAY_TABLE(mDB).getPlanWorkingDaysOfMonth(new Date(), strShopId);
			int numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB).getCurrentWorkingDaysOfMonth(strShopId);
			dto.progressStandarPercent = Math.floor(numPlanDate > 0 ? numPlanDateDone * 100.0 / numPlanDate : 0);
		}
		return dto;

	}

	/**
	 * get DTO for report display programe of staff
	 *
	 * @param parentStaffId
	 * @param proId
	 * @return
	 * @throws Exception
	 */
	public SupervisorReportDisplayProgressStaffDTO getStaffDisProComProReportDTO(Bundle data) throws Exception {
		SupervisorReportDisplayProgressStaffDTO dto = new SupervisorReportDisplayProgressStaffDTO();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String programeId = data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffID = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		List<String> params = new ArrayList<String>();
		Cursor c = null;
		ArrayList<String> levelCodeArray = getArrayLevelCodeOfDisplayPrograme(programeId);

		for (int i = 0; i < levelCodeArray.size(); i++) {
			dto.arrLevelCode.add(levelCodeArray.get(i));

			ReportDisplayProgressResult rs = new ReportDisplayProgressResult();
			dto.totalItem.arrLevelCode.add(rs);
		}
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT STAFF_ID, STAFF_CODE, STAFF_NAME, ");
			sqlQuery.append("GROUP_CONCAT(NUM_JOIN) NUM_JOIN, ");
			sqlQuery.append("GROUP_CONCAT(NUM_NON_COMPLETE) NUM_NON_COMPLETE, ");
			sqlQuery.append("GROUP_CONCAT(DISTINCT DISPLAY_PROGRAM_LEVEL_CODE) LEVEL_CODE ");
			sqlQuery.append("FROM ( ");
			sqlQuery.append("	SELECT  ");
			sqlQuery.append("	RPT_ST_DP.STAFF_ID STAFF_ID, ");
			sqlQuery.append("	RPT_ST_DP.STAFF_CODE STAFF_CODE, ");
			sqlQuery.append("	RPT_ST_DP.STAFF_NAME STAFF_NAME,     ");
			sqlQuery.append("	RPT_ST_DP.DISPLAY_PROGRAM_LEVEL_CODE DISPLAY_PROGRAM_LEVEL_CODE, ");
			sqlQuery.append("	Count(RPT_ST_DP.RESULT) NUM_JOIN, ");
			sqlQuery.append("	Sum(CASE RPT_ST_DP.RESULT WHEN 0 THEN 1 ELSE 0 END) NUM_NON_COMPLETE ");
			sqlQuery.append("FROM   rpt_display_program RPT_ST_DP ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("	AND RPT_ST_DP.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("	AND RPT_ST_DP.staff_id IN ");
			sqlQuery.append("(" + listStaffID + ")");
			sqlQuery.append("	AND RPT_ST_DP.DISPLAY_PROGRAM_ID = ? ");
			params.add(programeId);
			sqlQuery.append("	AND Date(RPT_ST_DP.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("	AND Ifnull(Date(RPT_ST_DP.to_date) >= Date('now', 'localtime'), 1) ");
			sqlQuery.append("	AND Date(RPT_ST_DP.month, 'start of month') = Date('now', 'localtime', 'start of month') ");
			sqlQuery.append("	AND RPT_ST_DP.status = 1     ");
			sqlQuery.append("GROUP  BY rpt_ST_dp.STAFF_ID, rpt_ST_dp.DISPLAY_PROGRAM_LEVEL_ID ");
			sqlQuery.append("ORDER  BY RPT_ST_DP.DISPLAY_PROGRAM_ID, RPT_ST_DP.DISPLAY_PROGRAM_LEVEL_CODE ");
			sqlQuery.append(") ");
			sqlQuery.append("GROUP BY STAFF_ID ");
			sqlQuery.append("ORDER  BY STAFF_CODE, STAFF_NAME ");

			c = this.rawQuery(sqlQuery.toString(), params.toArray(new String[params.size()]));

			if (c != null && c.moveToFirst()) {
				do {
					SupervisorReportDisplayProgressStaffItem item = dto.newStaffDisProComProgReportItem();
					item.staffId = CursorUtil.getInt(c, STAFF_ID);
					item.staffCode = CursorUtil.getString(c, STAFF_CODE);
					item.staffName = CursorUtil.getString(c, STAFF_NAME);

					dto.listItem.add(item);

					String numJoin = CursorUtil.getString(c, "NUM_JOIN");
					String numNonComplete = CursorUtil.getString(c, "NUM_NON_COMPLETE");
					String levelCode = CursorUtil.getString(c, "LEVEL_CODE");
					String[] numJoinArray = numJoin.split(",");
					String[] numNonCompleteArray = numNonComplete.split(",");
					String[] rptLevelCodeArray = levelCode.split(",");
					///

					for (int i = 0; i < dto.arrLevelCode.size(); i++) {
						ReportDisplayProgressResult rs = new ReportDisplayProgressResult();
						for(int j = 0; j <rptLevelCodeArray.length ; j++){
							if(levelCodeArray.get(i).equals(rptLevelCodeArray[j])){
								rs.joinNumber = Integer.parseInt(numJoinArray[j]);
								rs.resultNumber = Integer.parseInt(numNonCompleteArray[j]);

								item.itemResultTotal.joinNumber += rs.joinNumber;
								item.itemResultTotal.resultNumber += rs.resultNumber;

								dto.totalItem.arrLevelCode.get(i).joinNumber += rs.joinNumber;
								dto.totalItem.arrLevelCode.get(i).resultNumber += rs.resultNumber;

								dto.totalItem.itemResultTotal.joinNumber += rs.joinNumber;
								dto.totalItem.itemResultTotal.resultNumber += rs.resultNumber;
							}
						}

						item.arrLevelCode.add(rs);
					}
				} while (c.moveToNext());
			}
			// return dto;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;

	}

	/**
	 * Lay bao cao tien do CTTB cua tung NVBH
	 *
	 * @author: TRUNGHQM
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public ReportDisplayProgressDetailDayDTO getProgReportProDispDetailSaleDTO(
			Bundle data) throws Exception {
		ReportDisplayProgressDetailDayDTO dto = new ReportDisplayProgressDetailDayDTO();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String programeId = data.getString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, null);
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = data.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		ArrayList<String> levelCodeArray = new ArrayList<String>();
		if (!StringUtil.isNullOrEmpty(programeId)) {
			levelCodeArray = getArrayLevelCodeOfDisplayPrograme(programeId);
		}
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffID = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);

		ArrayList<String> params = new ArrayList<String>();
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("	SELECT RPT_ST_DP.RPT_DISPLAY_PROGRAM_ID RPT_DISPLAY_PROGRAM_ID, ");
		sqlQuery.append("	RPT_ST_DP.CUSTOMER_ID CUSTOMER_ID, ");
		sqlQuery.append("	RPT_ST_DP.CUSTOMER_CODE CUSTOMER_CODE, ");
		sqlQuery.append("	RPT_ST_DP.CUSTOMER_NAME CUSTOMER_NAME, ");
		sqlQuery.append("	RPT_ST_DP.CUSTOMER_ADDRESS CUSTOMER_ADDRESS, ");
		sqlQuery.append("	RPT_ST_DP.DISPLAY_PROGRAM_CODE DISPLAY_PROGRAM_CODE, ");
		sqlQuery.append("	RPT_ST_DP.DISPLAY_PROGRAM_NAME DISPLAY_PROGRAM_NAME, ");
		sqlQuery.append("	RPT_ST_DP.DISPLAY_PROGRAM_LEVEL_CODE DISPLAY_PROGRAME_LEVEL_CODE, ");
		sqlQuery.append("	RPT_ST_DP.RESULT RESULT, ");
		sqlQuery.append("	RPT_ST_DP.DISPLAY_PROGRAM_TYPE DISPLAY_PROGRAM_TYPE, ");
		sqlQuery.append("	RPT_ST_DP.QUANTITY QUANTITY, ");
		sqlQuery.append("	RPT_ST_DP.QUANTITY_PLAN QUANTITY_PLAN, ");
		sqlQuery.append("	RPT_ST_DP.AMOUNT AMOUNT, ");
		sqlQuery.append("	RPT_ST_DP.AMOUNT_PLAN AMOUNT_PLAN, ");
		sqlQuery.append("	(SELECT DESCRIPTION FROM AP_PARAM AP WHERE AP.STATUS = 1 AND AP.AP_PARAM_CODE = 'DISPLAY_PROGR_TYPE' AND AP.VALUE = RPT_ST_DP.DISPLAY_PROGRAM_TYPE) DISPLAY_PROGRAM_TYPE_CODE, ");
		sqlQuery.append("	(CASE RPT_ST_DP.DISPLAY_PROGRAM_TYPE WHEN 2 THEN RPT_ST_DP.QUANTITY_PLAN WHEN 1 THEN RPT_ST_DP.AMOUNT_PLAN ELSE RPT_ST_DP.AMOUNT_PLAN||'/'||RPT_ST_DP.QUANTITY_PLAN END) PLAN, ");
		sqlQuery.append("	(CASE RPT_ST_DP.DISPLAY_PROGRAM_TYPE WHEN 2 THEN RPT_ST_DP.QUANTITY WHEN 1 THEN RPT_ST_DP.AMOUNT ELSE RPT_ST_DP.AMOUNT ||'/'||RPT_ST_DP.QUANTITY END) ACTUAL ");
		sqlQuery.append("FROM   rpt_display_program RPT_ST_DP ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("	AND RPT_ST_DP.staff_id = ? ");
		params.add(staffId);
		//Giam sat
		if(!StringUtil.isNullOrEmpty(programeId)) {
			sqlQuery.append("	AND RPT_ST_DP.DISPLAY_PROGRAM_ID = ? ");
			params.add(programeId);
		}

		if(!StringUtil.isNullOrEmpty(shopId)) {
			sqlQuery.append("	AND RPT_ST_DP.SHOP_ID = ? ");
			params.add(shopId);
		}
		sqlQuery.append("	AND substr(RPT_ST_DP.from_date, 1, 7) <= ? ");
		params.add(dateNow);
		sqlQuery.append("	AND ifnull(substr(RPT_ST_DP.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
		sqlQuery.append("	AND substr(RPT_ST_DP.month, 1, 7) = ? ");
		params.add(dateNow.substring(0, 7));
		sqlQuery.append("	AND RPT_ST_DP.status = 1     ");
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			sqlQuery.append("	AND RPT_ST_DP.staff_id IN     ");
			sqlQuery.append("(" +    listStaffID + ")");
		} else {
//			sqlQuery.append("	GROUP BY   RPT_ST_DP.staff_id, RPT_ST_DP.DISPLAY_PROGRAM_ID   ");
		}

		//get total sql, without order + paging
		String totalSql = "";
		if(isGetTotalPage) {
			totalSql = StringUtil.getCountSql(sqlQuery.toString(), "NUM_ROW");
		}

		//add order sql
		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
		defaultOrderByStr.append("ORDER  BY RPT_ST_DP.RESULT ");

		if(!StringUtil.isNullOrEmpty(programeId)) {
			defaultOrderByStr.append(", RPT_ST_DP.DISPLAY_PROGRAM_LEVEL_CODE ");
		}
		defaultOrderByStr.append(", RPT_ST_DP.CUSTOMER_CODE ");

		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF){
			String orderByStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
			.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
			.addMapper(SortActionConstants.CODE_PROGRAM, "DISPLAY_PROGRAM_CODE")
			.addMapper(SortActionConstants.NAME_PROGRAM, "DISPLAY_PROGRAM_NAME")
			.addMapper(SortActionConstants.LEVEL, "DISPLAY_PROGRAME_LEVEL_CODE")
			.addMapper(SortActionConstants.TYPE, "DISPLAY_PROGRAM_TYPE_CODE")
			.addMapper(SortActionConstants.AMOUNT_PLAN, "PLAN")
			.addMapper(SortActionConstants.AMOUNT, "ACTUAL")
			.defaultOrderString(defaultOrderByStr.toString())
			.build(sortInfo);
			sqlQuery.append(orderByStr);
		}else{
			// giam sat, quan ly
			String orderByStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
			.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
			.addMapper(SortActionConstants.ADDRESS, "CUSTOMER_ADDRESS")
			.addMapper(SortActionConstants.LEVEL, "DISPLAY_PROGRAME_LEVEL_CODE")
			.addMapper(SortActionConstants.TYPE, "DISPLAY_PROGRAM_TYPE_CODE")
			.addMapper(SortActionConstants.AMOUNT_PLAN, "PLAN")
			.addMapper(SortActionConstants.AMOUNT, "ACTUAL")
			.defaultOrderString(defaultOrderByStr.toString())
			.build(sortInfo);
			sqlQuery.append(orderByStr);
		}
		//add paging sql
		String sqlPaging = StringUtil.getPagingSql(Constants.NUM_ITEM_PER_PAGE, page);
		sqlQuery.append(sqlPaging);

		Cursor totalC = null;
		try {
			if(isGetTotalPage) {
				totalC = rawQueries(totalSql, params);
				if (totalC.moveToFirst()) {
					dto.totalSize = CursorUtil.getInt(totalC, "NUM_ROW");
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (totalC != null) {
					totalC.close();
				}
			} catch (Exception e) {
			}
		}

		Cursor c = null;
		try {
			c = rawQueries(sqlQuery.toString(), params);
			ArrayList<ReportDisplayProgressDetailDayRowDTO> arrayList = new ArrayList<ReportDisplayProgressDetailDayRowDTO>();
			if (c.moveToFirst()) {
				do {
					ReportDisplayProgressDetailDayRowDTO detailDto = new ReportDisplayProgressDetailDayRowDTO();
					detailDto.parseReportDisplayProgressDetailDayRow(c);
					for(int i = 0, size = levelCodeArray.size(); i < size; i++) {
						if(detailDto.displayLevel.equals(levelCodeArray.get(i))) {
							detailDto.displayLevel = levelCodeArray.get(i);
						}
					}
					arrayList.add(detailDto);
				} while (c.moveToNext());
				dto.listItem = arrayList;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	public ProgReportProDispDetailSaleDTO getProgReportProDispDetailSaleDTO(
			int staffId, String displayProgrameCode, int checkAll, String page) {
		ProgReportProDispDetailSaleDTO dto = new ProgReportProDispDetailSaleDTO();
		String sqlQuery = "select RDP.CUSTOMER_CODE, RDP.CUSTOMER_NAME, RDP.CUSTOMER_ADDRESS, RDP.AMOUNT_PLAN, RDP.AMOUNT_REMAIN, RDP.DISPLAY_PROGRAME_LEVEL, RDP.AMOUNT, RDP.RESULT"
				+ " from RPT_DISPLAY_PROGRAM as RDP"
				+ " where RDP.STAFF_ID = ?"
				+ " and RDP.DISPLAY_PROGRAME_CODE = ?"
				+ " and dayInOrder(RDP.MONTH, 'start of month') = dayInOrder('now', 'localtime','start of month')";
		if (checkAll == 0) {
			sqlQuery += " and (RDP.RESULT = 0 OR RDP.RESULT is null)";
		}
		sqlQuery += " group by RDP.CUSTOMER_CODE, RDP.CUSTOMER_NAME";
		Cursor c = null;
		Cursor cTmp = null;
		try {
			ArrayList<ProgReportProDispDetailSaleRowDTO> arrayList = new ArrayList<ProgReportProDispDetailSaleRowDTO>();
			long totalRemain = Long.valueOf(0);
			String[] param = new String[] { String.valueOf(staffId),
					displayProgrameCode };
			String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
					+ sqlQuery + ") ";

			// tinh tong so item
			if (checkAll == 1) {
				cTmp = rawQuery(getCountProductList, param);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					dto.totalItem = total;
				}
				c = this.rawQuery(sqlQuery + page, param);
			} else {
				c = this.rawQuery(sqlQuery, param);
			}

			if (c.moveToFirst()) {
				do {
					ProgReportProDispDetailSaleRowDTO detailDto = this
							.initForDetailSaleFromCursor(c);
					totalRemain += Math.round(detailDto.RemainSale / 1000.0);
					arrayList.add(detailDto);
				} while (c.moveToNext());
				dto.listItem = arrayList;
				dto.RemainSaleTotal = totalRemain;
			}
		} catch (Exception e) {
			MyLog.e("getProgReportProDispDetailSaleDTO", "fail", e);
			dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (checkAll == 1 && cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Khoi tao du lieu cho man hinh thong ke chi tiet
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: ProgReportProDispDetailSaleRowDTO
	 * @throws:
	 */
	private ProgReportProDispDetailSaleRowDTO initForDetailSaleFromCursor(
			Cursor c) {
		// TODO Auto-generated method stub
		ProgReportProDispDetailSaleRowDTO detailDto = new ProgReportProDispDetailSaleRowDTO();
		detailDto.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		detailDto.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		detailDto.CustomerAddress = CursorUtil.getString(c, "CUSTOMER_ADDRESS");
		detailDto.level = CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL");
		detailDto.amountPlan = CursorUtil.getLong(c, "AMOUNT_PLAN");
		detailDto.RemainSale = CursorUtil.getLong(c, "AMOUNT_REMAIN");
		if (detailDto.RemainSale < 0) {
			detailDto.RemainSale = 0;
		}
		detailDto.result = CursorUtil.getInt(c, "RESULT");
		return detailDto;
	}

	/**
	 *
	 * Lay so luong khach hang chua dat cua mot chuong trinh trung bay
	 *
	 * @author: ThanhNN8
	 * @param displayProgrameCode
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int countCustomerNotComplete(String displayProgrameCode,
			boolean notComplete) {
		int result = 0;
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select count(RDP.CUSTOMER_ID) AS TOTALCUSTOMER");
		sqlQuery.append(" from RPT_DISPLAY_PROGRAME as RDP, CUSTOMER AS C");
		sqlQuery.append(" where RDP.DISPLAY_PROGRAME_CODE = ? and RDP.CUSTOMER_ID = c.CUSTOMER_ID and c.STATUS = 1");
		sqlQuery.append(" and strftime('%Y/%m', 'now','localtime') = strftime('%Y/%m',RDP.MONTH)");
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			sqlQuery.append("	AND RDP.parent_staff_id = ?     ");
		} else {
			sqlQuery.append("	GROUP BY   RDP.staff_id, RDP.CUSTOMER_ID   ");

		}
		if (notComplete) {
			sqlQuery.append(" and RDP.RESULT = 0");
		}
		try {
			Cursor c = this.rawQuery(sqlQuery.toString(),
					new String[] { displayProgrameCode, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()) });
			if (c.moveToFirst()) {
				result = CursorUtil.getInt(c, "TOTALCUSTOMER");
			}
			if (c != null) {
				c.close();
			}
		} catch (Exception e) {
			MyLog.e("countCustomerNotComplete", "fail", e);
			return 0;
		}
		return result;
	}

	/**
	 * lay du lieu man hinh : 01-08.Tien do CTTB chi tiet NVBH ngay (TBHV)
	 * @author: BangHN
	 * @param staffId
	 * @param displayProgrameId
	 * @param checkAll
	 * @return
	 * @throws Exception
	 */
	public TBHVReportDisplayProgressDetailDayViewDTO getTBHVReportDisplayProgressDetailDayDTO(int staffId,
			String displayProgrameId, String displayProgrameCode, String displayProgrameLevel, int checkAll, int page) throws Exception {
		TBHVReportDisplayProgressDetailDayViewDTO dto = new TBHVReportDisplayProgressDetailDayViewDTO();
		//levelcode cua cttb
		ArrayList<String> levelCode = getArrayLevelCodeOfDisplayPrograme(displayProgrameId);
		StringBuffer getTotalCount = new StringBuffer();
		Cursor c = null;
		Cursor cTotalRow = null;
		StringBuffer  sql = new StringBuffer();
		sql.append("SELECT RDP.customer_code, ");
		sql.append("       RDP.customer_name, ");
		sql.append("       RDP.customer_address, ");
		sql.append("       RDP.display_program_level_code                DISPLAY_PROGRAME_LEVEL_CODE, ");
		sql.append("       RDP.display_program_code, ");
		sql.append("       RDP.RPT_DISPLAY_PROGRAM_ID, ");
		sql.append("       RDP.amount, ");
		sql.append("       RDP.amount_plan, ");
		sql.append("       RDP.result, ");
		//sql.append("       RDP.display_program_type                         DISPLAY_PROGRAM_TYPE, ");
		sql.append("       (SELECT description ");
		sql.append("        FROM   ap_param AP ");
		sql.append("        WHERE  AP.status = 1 ");
		sql.append("               AND AP.ap_param_code = 'DISPLAY_PROGR_TYPE' ");
		sql.append("               AND AP.value = RDP.display_program_type) ");
		sql.append("       DISPLAY_PROGRAM_TYPE_CODE, ");
		sql.append("       ( CASE RDP.display_program_type ");
		sql.append("           WHEN 2 THEN RDP.quantity_plan ");
		sql.append("           ELSE RDP.amount_plan / 1000 ");
		sql.append("         end )                                          PLAN, ");
		sql.append("       ( CASE RDP.display_program_type ");
		sql.append("           WHEN 2 THEN RDP.quantity ");
		sql.append("           ELSE RDP.amount / 1000 ");
		sql.append("         end )                                          ACTUAL ");
		sql.append("FROM   rpt_display_program RDP ");
		sql.append("WHERE  RDP.[display_program_code] = ? ");
		sql.append("       AND RDP.staff_id = ? and RDP.status = 1 ");
		sql.append("       AND Date(RDP.month, 'start of month') = ");
		sql.append("           Date('now', 'localtime', 'start of month') ");
		sql.append("GROUP  BY RDP.customer_code, ");
		sql.append("          RDP.customer_name ");
		sql.append("ORDER  BY RDP.result, ");
		sql.append("          RDP.display_program_level_code, ");
		sql.append("          RDP.display_program_code, ");
		sql.append("          RDP.customer_code ");

		getTotalCount.append("  select count(*) as TOTAL_ROW from (" + sql.toString() + ")");

		sql.append(" limit " + Integer.toString(Constants.NUM_ITEM_PER_PAGE));
		sql.append(" offset " + Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));


		ArrayList<ReportDisplayProgressDetailDayRowDTO> arrayList = new ArrayList<ReportDisplayProgressDetailDayRowDTO>();
		try {

			int totalRemain = Integer.valueOf(0);
			c = this.rawQuery(sql.toString(), new String[] { displayProgrameCode, "" + staffId });
			if (c.moveToFirst()) {
				do {
					ReportDisplayProgressDetailDayRowDTO rowDTO = new ReportDisplayProgressDetailDayRowDTO();
					rowDTO.parseReportDisplayProgressDetailDayRow(c);
					for(int i = 0; i < levelCode.size(); i++){
						if(rowDTO.displayLevel.equals(levelCode.get(i))){
							rowDTO.displayLevel = levelCode.get(i);
						}
					}
					arrayList.add(rowDTO);
				} while (c.moveToNext());
				dto.listItem = arrayList;
				dto.remainSaleTotal = totalRemain;
			}

			cTotalRow = rawQuery(getTotalCount.toString(), new String[] { displayProgrameCode, "" + staffId });
			if (cTotalRow != null) {
				if (cTotalRow.moveToFirst()) {
					dto.totalSize = cTotalRow.getInt(0);
				}
			}
		} catch (Exception e) {
			MyLog.e("getTBHVReportDisplayProgressDetailDayDTO", "fail", e);
			dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * lay du lieu man hinh : 01-08.Tien do CTTB chi tiet NVBH ngay (TBHV)
	 *
	 * @author: HoanPD1
	 * @param staffId
	 * @param displayProgrameId
	 * @param checkAll
	 * @return
	 */
	public TBHVProgReportProDispDetailSaleDTO getTBHVProgReportProDispDetailSaleDTO(
			int staffId, String displayProgrameCode,
			String displayProgrameLevel, int checkAll, int page) {
		TBHVProgReportProDispDetailSaleDTO dto = new TBHVProgReportProDispDetailSaleDTO();
		// String getTotalCount = "";
		StringBuffer getTotalCount = new StringBuffer();
		Cursor c = null;
		Cursor cTotalRow = null;
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT RDP.customer_code, ");
		sqlQuery.append("       RDP.customer_name, ");
		sqlQuery.append("       RDP.customer_address, ");
		sqlQuery.append("       CT.housenumber  || ' ' || ");
		sqlQuery.append("       street housenumber, ");
		//sqlQuery.append("       RDP.display_program_level DISPLAY_PROGRAME_LEVEL, ");
		sqlQuery.append("       RDP.display_program_code, ");
		sqlQuery.append("       RDP.amount_plan, ");
		sqlQuery.append("       RDP.amount_remain, ");
		sqlQuery.append("       RDP.amount, ");
		sqlQuery.append("       RDP.result ");
		sqlQuery.append("FROM   rpt_display_program RDP, customer CT ");
		sqlQuery.append("WHERE  RDP.display_programe_code = ? ");
		sqlQuery.append("       AND RDP.staff_id = ? ");
		sqlQuery.append("       AND RDP.customer_id = CT.customer_id ");
		sqlQuery.append("       AND Date(RDP.month, 'start of month') = ");
		sqlQuery.append("           Date('now', 'localtime','start of month') ");
		if (checkAll == 0) {
			sqlQuery.append("       AND RDP.amount = 0 ");
		}
		sqlQuery.append("GROUP  BY RDP.customer_code, ");
		sqlQuery.append("          RDP.customer_name ");
		sqlQuery.append("ORDER  BY RDP.customer_code, ");
		sqlQuery.append("          RDP.customer_name ");

		if (checkAll == 1) {

			getTotalCount.append("  select count(*) as TOTAL_ROW from ("
					+ sqlQuery.toString() + ")");
			// sqlQuery.append(" limit " + Constants.NUM_ITEM_PER_PAGE
			// + " offset " + page);

			sqlQuery.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlQuery.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));

		}

		ArrayList<TBHVProgReportProDispDetailSaleRowDTO> arrayList = new ArrayList<TBHVProgReportProDispDetailSaleRowDTO>();
		try {

			int totalRemain = Integer.valueOf(0);
			c = this.rawQuery(sqlQuery.toString(), new String[] {
					displayProgrameCode, "" + staffId });
			if (c.moveToFirst()) {
				do {
					TBHVProgReportProDispDetailSaleRowDTO detailDto = this
							.initTBHVForDetailSaleFromCursor(c);
					totalRemain += Math.round(detailDto.remainSale / 1000.0);
					arrayList.add(detailDto);
				} while (c.moveToNext());
				dto.listItem = arrayList;
				dto.remainSaleTotal = totalRemain;
			}
			if (checkAll == 1) {
				cTotalRow = rawQuery(getTotalCount.toString(), new String[] {
						displayProgrameCode, "" + staffId });
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.total = cTotalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("getTBHVProgReportProDispDetailSaleDTO", "fail", e);
			dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Khoi tao du lieu cho man hinh thong ke chi tiet
	 *
	 * @author: HoanPD1
	 * @param c
	 * @return
	 * @return: ProgReportProDispDetailSaleRowDTO
	 * @throws:
	 */
	private TBHVProgReportProDispDetailSaleRowDTO initTBHVForDetailSaleFromCursor(
			Cursor c) {
		// TODO Auto-generated method stub
		TBHVProgReportProDispDetailSaleRowDTO detailDto = new TBHVProgReportProDispDetailSaleRowDTO();
		detailDto.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		detailDto.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		detailDto.customerAddress = CursorUtil.getString(c, "CUSTOMER_ADDRESS");
		if (StringUtil.isNullOrEmpty(detailDto.customerAddress)) {
			detailDto.customerAddress = CursorUtil.getString(c, "housenumber");
		}
		detailDto.displayProgLevel = CursorUtil.getString(c, "DISPLAY_PROGRAME_LEVEL_CODE");
		detailDto.amountPlan = CursorUtil.getLong(c, "AMOUNT_PLAN");
		detailDto.remainSale = CursorUtil.getLong(c, "AMOUNT_REMAIN");
		detailDto.amount = CursorUtil.getLong(c, "AMOUNT");
		detailDto.result = CursorUtil.getInt(c, "RESULT");
		return detailDto;
	}

}
