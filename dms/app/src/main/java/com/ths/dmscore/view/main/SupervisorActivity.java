package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.view.supervisor.customer.CustomerSaleList;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.MenuAndSubMenu;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dms.R;

/**
 * Activity chinh: cua nhan vien giam sat nha phan phoi
 *
 * @author : BangHN since : 1.0
 */
public class SupervisorActivity extends RoleActivity implements
		OnItemClickListener {
	private static final int ACTION_FINISH_VISIT_CUS_OK = 13;
	private static final int ACTION_FINISH_VISIT_CUS_CANCEL = 14;
	// confirm show camera
	public static final int CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR = 15;

	boolean isShowMenu = false;
	boolean isShowTextInMenu = false;
	//public ReviewsStaffViewItems reviewsStaffViewItems;

	// kiem tra da luu truoc khi ket thuc
	public boolean allowSaveResult = false;
	public boolean isFinish = false;

	boolean isTakePhotoFromMenuCloseCustomer = false;
	boolean isSaveActionLogAndCloseCustomerAfterTakePhoto = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState,PriHashMap.PriMenu.GSNPP_MENU, PriHashMap.PriForm.GSNPP_MENU);
//		PriUtils.getInstance().genPriHashMapForMenu(PriHashMap.PriMenu.GSNPP_MENU, PriHashMap.PriForm.GSNPP_MENU);

		// check time
		serverDate = getIntent().getExtras().getString(
				IntentConstants.INTENT_TIME);
		validateTimeClientServer();

//		showHideMenuText(isShowMenu);
//		gotoReportProgressDate();// bao cao theo ngay

		// TamPQ : xu ly trang thai ghe tham neu thoat dot ngot ( force close)
		checkVisitFromActionLog();

		// check va gui toan bo log dang con ton dong
		TransactionProcessManager.getInstance().startChecking(
				TransactionProcessManager.SYNC_NORMAL);
//		reviewsStaffViewItems=new ReviewsStaffViewItems();

		// lay thong tin cho xoa vi tri
		// truong hop dang nhap lan dau ma khong dong bo db
//		readAllowResetLocation();

	}

	/**
	 * kiem tra duoi DB lan login truoc co dang ghe tham khach hang nao ko
	 *
	 * @author TamPQ
	 */
	private void checkVisitFromActionLog() {
		Vector<String> v = new Vector<String>();
		v.add(IntentConstants.INTENT_STAFF_ID);
		v.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		v.add(IntentConstants.INTENT_SHOP_ID);
		v.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId()));

		handleViewEvent(v, ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG, SaleController.getInstance());
	}

	/**
	 * Khoi tao menu chuong trinh
	 *
	 * @author : BangHN since : 4:41:56 PM
	 */
	protected void initMenuGroups() {
		lstMenuItems = new ArrayList<MenuItemDTO>();

		// menu va submenu Tong Quan
		MenuAndSubMenu listTongQuan = new MenuAndSubMenu();
		MenuItemDTO item1 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_SUMMARIZE),
				R.drawable.menu_report_icon);
		// submenu tien do ngay
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_PROGRESS_SALE_DAY),
				R.drawable.icon_calendar);
		// submenu tien do thang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_PROGRESS_SALE_MONTH),
				R.drawable.icon_accumulated);
		// submenu mat hang trong tam
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_FOCUS_PRODUCT),
				R.drawable.icon_list_star);
		// submenu CTTB
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_DISPLAY_PROGRAM),
//				R.drawable.icon_reminders);
		// submenu kh chua psds
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_CUSTOMER_NOT_PSDS),
				R.drawable.icon_order);
		// submenu sl theo nganh hang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_QUANTITY_FOLLOW_CATEGORY),
				R.drawable.icon_categories);
		// submenu ds theo nganh hang
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_AMOUNT_FOLLOW_CATEGORY),
				R.drawable.icon_categories);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KEY_SHOP),
				R.drawable.icon_report_keyshop);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KPI),
				R.drawable.icon_kpi);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_SUP_ASO),
				R.drawable.icon_kpi);
		item1.setItems(listTongQuan.getListSubMenu());


		// menu va sub Giam sat Lo trinh
		MenuAndSubMenu listGSLoTrinh = new MenuAndSubMenu();
		MenuItemDTO item2 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_MENU_MONITORING),
				R.drawable.menu_customer_icon);
		item2.menuIndex = MENU_INDEX_SELECTED;
		// submenu GS Huan luyen NVBH
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_ROUTE_MANAGE),
				R.drawable.menu_customer_icon);
		// submenu Xem vi tri
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_VIEW_POSITION),
				R.drawable.icon_map);
		// submenu Cham cong
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_TIMEKEEPING),
				R.drawable.icon_clock);
		// submenu Di tuyen
		listGSLoTrinh.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_ON_ROUTE),
				R.drawable.icon_task);
		item2.setItems(listGSLoTrinh.getListSubMenu());

		// menu va submenu danh sach khach hang
		MenuAndSubMenu listDSHA = new MenuAndSubMenu();
		MenuItemDTO item7 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_SELECT_CUSTOMER_NAME),
				R.drawable.icon_customer_list);
		// submenu DSKH
		listDSHA.addSubMenu(StringUtil.getString(R.string.TEXT_CUSTOMER_LIST_VIEW),
				R.drawable.menu_customer_icon);
		listDSHA.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_LIST_ORDER),
				R.drawable.icon_order);
		// submenu Hinh Anh
		listDSHA.addSubMenu(StringUtil.getString(R.string.TEXT_IMAGE_CUSTOMER_LIST_VIEW),
				R.drawable.menu_picture_icon);
		item7.setItems(listDSHA.getListSubMenu());

		// menu va submenu Huan luyen
		MenuAndSubMenu listHuanLuyen = new MenuAndSubMenu();
		MenuItemDTO item4 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_TRAINING_SALEMAN),
				R.drawable.icon_training);
		// submenu Ke Hoach Huan luyen
		listHuanLuyen.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_TABLE_PLAN),
				R.drawable.icon_calendar);
		// submenu Bao cao luy ke thang
		listHuanLuyen.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_MENU_REPORT_MONTH),
				R.drawable.icon_accumulated);
		// submenu Doanh so ke hoach
//		listHuanLuyen.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CUS_LIST),
//				R.drawable.menu_customer_icon);
		item4.setItems(listHuanLuyen.getListSubMenu());

		MenuItemDTO item6 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_PROBLEMS_MANAGE),
				R.drawable.menu_problem_icon);
		// menu va submenu giao van de, can thuc hien
		MenuAndSubMenu listTheoDoiKhacPhuc = new MenuAndSubMenu();
		// submenu Can thuc hien
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_TAKE_PROBLEM),
				R.drawable.icon_feedback);
		// submenu Theo doi khac phuc
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_NEED_DO_IT),
				R.drawable.icon_feedback);
		item6.setItems(listTheoDoiKhacPhuc.getListSubMenu());

		// menu va sub menu Danh muc
		MenuAndSubMenu listDanhMuc = new MenuAndSubMenu();
		MenuItemDTO item5 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_CATEGORY),
				R.drawable.icon_category);
		// submenu San pham
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_PRODUCT),
				R.drawable.icon_product_list);
		// submenu CTKM
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TITLE_VIEW_LIST_PROMOTION),
				R.drawable.menu_promotion_icon);
		// submenu CTTB
//		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CTTB),
//				R.drawable.menu_manage_icon);
		// submenu Tim Kiem Hinh Anh
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_SEARCH_IMAGE),
				R.drawable.icon_image_search);
		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_LIST_KEY_SHOP),
				R.drawable.icon_document);
		// submenu Cong van
//		listDanhMuc.addSubMenu(StringUtil.getString(R.string.TEXT_OFFICE_DOCUMENT),
//				R.drawable.icon_document);
		item5.setItems(listDanhMuc.getListSubMenu());

		MenuItemDTO item3 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_DEVICES_MANAGE),
				R.drawable.menu_manage_icon);

		MenuItemDTO item9 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_DYNAMIC_REPORT),
				R.drawable.icon_product_list);
		
		MenuAndSubMenu listDevice = new MenuAndSubMenu();
		MenuItemDTO item10 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_MENU_DEVICE),
				R.drawable.icon_device);
		listDevice.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_LOST), R.drawable.icon_device_report_lost);
		listDevice.addSubMenu(StringUtil.getString(R.string.TEXT_INVENTORY_DEVICE), R.drawable.icon_device_inventory);
		item10.setItems(listDevice.getListSubMenu());

		item1.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_TONGQUAN.getMenuName();
		item2.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_GIAMSAT.getMenuName();
		item3.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_QUANLYTHIETBI.getMenuName();
		item4.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_HUANLUYEN.getMenuName();
		item5.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_DANHMUC.getMenuName();
		item6.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_THEODOIKHACPHUC.getMenuName();
		item7.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_KHACHHANG.getMenuName();
		item9.controlOrdinal = PriHashMap.PriMenu.GSNPP_MENU_KPI.getMenuName();
		item10.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_DEVICE.getMenuName();

		lstMenuItems.add(item1);
		lstMenuItems.add(item2);
//		lstMenuItems.add(item4);
		lstMenuItems.add(item7);
		lstMenuItems.add(item6);
		lstMenuItems.add(item5);
//		lstMenuItems.add(item8);
//		lstMenuItems.add(item9);
//		lstMenuItems.add(item3);
		lstMenuItems.add(item10);

		initHashActionbar();

//		lstMenuItemsToCheck.addAll(lstMenuItems);
//		PriUtils.getInstance().checkMenu(lstMenuItems);
		setSelectMenu(item1.menuIndex);
	}

	@Override
	protected void initHashActionbar() {
		hashActionbar = new SparseArray<List<MenuTab>>();

		int index = 0;
		// Menu tien do ban hang
		List<MenuTab> listTiendobanhang = new ArrayList<MenuTab>();
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_REPORT_PROGRESS_DATE, PriForm.GSNPP_BAOCAOTIENDONGAY));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACC_SALE_PROG_REPORT, PriForm.GSNPP_BAOCAOTIENDOLUYKE));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.PROG_REPOST_SALE_FOCUS, PriForm.GSNPP_BAOCAOCTTT));
//		listTiendobanhang.add(new MenuTab(ActionEventConstant.DIS_PRO_COM_PROG_REPORT, PriForm.GSNPP_BAOCAOCTTB));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH, PriForm.GSNPP_BAOCAOCHUAPSDS));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_QUANTITY_REPORT, PriForm.GSNPP_BAOCAOSANLUONGNGANH));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_AMOUNT_REPORT, PriForm.GSNPP_BAOCAODOANHSONGANH));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_KEY_SHOP, PriForm.GSNPP_BAOCAOKEYSHOP));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_KPI_SUPVERVISOR, PriForm.GSNPP_BAOCAOKPI));
		listTiendobanhang.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_ASO_SUPVERVISOR, PriForm.GSNPP_BAOCAO_ASO));
		hashActionbar.put(index, listTiendobanhang);
		index ++;

		// Giam sat lo trinh
		List<MenuTab> listGiamsatlotrinh = new ArrayList<MenuTab>();
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.GET_GSNPP_ROUTE_SUPERVISION, PriForm.GSNPP_GSLOTRINH));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.SUPERVISE_STAFF_POSITION, PriForm.GSNPP_VITRINHANVIEN));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.GSNPP_GET_LIST_SALE_FOR_ATTENDANCE, PriForm.GSNPP_CHAMCONG));
		listGiamsatlotrinh.add(new MenuTab(ActionEventConstant.GO_TO_REPORT_VISIT_CUSTOMER_ON_PLAN, PriForm.GSNPP_DITUYEN));
		hashActionbar.put(index, listGiamsatlotrinh);
		index ++;


		// Huan luyen
//		List<MenuTab> listHuanluyen = new ArrayList<MenuTab>();
//		listHuanluyen.add(new MenuTab(ActionEventConstant.GSNPP_TRAINING_PLAN, PriForm.GSNPP_HUANLUYEN_KEHOACH));
//		listHuanluyen.add(new MenuTab(ActionEventConstant.GSNPP_TRAINING_RESULT_ACC_REPORT, PriForm.GSNPP_HUANLUYEN_LUYKE));
//		listHuanluyen.add(new MenuTab(ActionEventConstant.ACTION_LOAD_LIST_CUSTOMER, PriForm.GSNPP_HUANLUYEN_DANHSACHKH));
//		hashActionbar.put(index, listHuanluyen);
//		index ++;

		// Danh sach diem ban
		List<MenuTab> listDsDiemban = new ArrayList<MenuTab>();
		listDsDiemban.add(new MenuTab(ActionEventConstant.GO_TO_CUSTOMER_SALE_LIST, PriForm.GSNPP_DSKHACHHANG));
		listDsDiemban.add(new MenuTab(ActionEventConstant.GO_TO_LIST_ORDER, PriHashMap.PriForm.GSNPP_DSDONHANG));
		listDsDiemban.add(new MenuTab(ActionEventConstant.GO_TO_GSNPP_IMAGE_LIST, PriForm.GSNPP_DSHINHANH));
		hashActionbar.put(index, listDsDiemban);
		index ++;

		List<MenuTab> listTheodoikhacphuc = new ArrayList<MenuTab>();
		listTheodoikhacphuc.add(new MenuTab(ActionEventConstant.GO_TO_FOLLOW_LIST_PROBLEM, PriForm.GSNPP_GIAOVANDE));
		listTheodoikhacphuc.add(new MenuTab(ActionEventConstant.NOTE_LIST_VIEW, PriForm.GSNPP_CANTHUCHIEN));
		hashActionbar.put(index, listTheodoikhacphuc);
		index ++;

		// Danh muc
		List<MenuTab> listDanhmuc = new ArrayList<MenuTab>();
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_PRODUCT_LIST, PriForm.GSNPP_DSSANPHAM));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GET_LIST_PROMOTION_PROGRAME, PriForm.GSNPP_CTKM));
//		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_DISPLAY_PROGRAM, PriForm.GSNPP_CTTB));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_SEARCH_IMAGE, PriForm.GSNPP_TIMKIEMHINHANH));
		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_KEY_SHOP_LIST_VIEW, PriForm.TBHV_DANHSACHCLB));
//		listDanhmuc.add(new MenuTab(ActionEventConstant.GO_TO_DOCUMENT, PriForm.GSNPP_DANHMUC_CONGVAN));
		hashActionbar.put(index, listDanhmuc);
		index ++;

		// Lich su huan luyen moi
//		List<MenuTab> listHuanluyenNew = new ArrayList<MenuTab>();
//		listHuanluyenNew.add(new MenuTab(
//				ActionEventConstant.GO_TO_GSNPP_TRAINING_HISTORY,
//				PriForm.GSNPP_LICHSUHUANLUYEN_LICHSUHUANLUYEN));
//		hashActionbar.put(7, listHuanluyenNew);

		// KPI
//		List<MenuTab> listKPI = new ArrayList<MenuTab>();
//		listKPI.add(new MenuTab(ActionEventConstant.GO_TO_KPI, PriForm.GSNPP_DANHSACHKPI));
//		hashActionbar.put(index, listKPI);
//		index ++;

		// Quan li thiet bi
//		List<MenuTab> listQuanlithietbi = new ArrayList<MenuTab>();
//		listQuanlithietbi.add(new MenuTab(ActionEventConstant.ACTION_MANAGER_EQUIPMENT, PriForm.GSNPP_QUANLYTHIETBI));
//		hashActionbar.put(index, listQuanlithietbi);
//		index ++;
		
		// thiet bi
		List<MenuTab> listThietbi = new ArrayList<MenuTab>();
		listThietbi.add(new MenuTab(ActionEventConstant.GO_TO_CUSTOMER_LIST_REPORT_LOST, PriForm.NVBH_CUSTOMER_LIST_DEVICE_REPORT_LOST));
		listThietbi.add(new MenuTab(ActionEventConstant.GO_TO_CUSTOMER_LIST_INVENTORY_DEVICE, PriForm.NVBH_LIST_CUSTOMER_DEVICE_INVENTORY));
		hashActionbar.put(index, listThietbi);
		index++;

	}


	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG:
			ActionLogDTO action = (ActionLogDTO) modelEvent.getModelData();
			if (action != null) {
				GlobalInfo.getInstance().getProfile()
						.setActionLogVisitCustomer(action);
				// fixbug 0012403: Loi tu dong ket thuc ghe tham KH khi thoat ung dung hoac force close ung dung
				// fix khong tu dong ket thuc, ma hien len thong tin ghe tham
				initMenuVisitCustomer(action.aCustomer.customerCode, action.aCustomer.customerName);
//				removeMenuCloseCustomer();
				// requestUpdateActionLog("0", null, null, this);
			}
			break;
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER: {
			if (this.isSaveActionLogAndCloseCustomerAfterTakePhoto) {
				this.isSaveActionLogAndCloseCustomerAfterTakePhoto = false;
				this.closeProgressDialog();
			} else {
				super.handleModelViewEvent(modelEvent);
			}
			break;
		}
		case ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT:
			CustomerListItem item = (CustomerListItem) modelEvent.getModelData();
			if (item != null) {
				GlobalInfo.getInstance().getProfile().setLastVisitCustomer(item);
			}
			break;
		case ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION:
			ApParamDTO result = (ApParamDTO) modelEvent.getModelData();
			if (result != null
					&& !StringUtil.isNullOrEmpty(result.getApParamCode())) {
				try {
					GlobalInfo.getInstance().setAllowResetLocation(
							Integer.parseInt(result.getApParamCode()));
				} catch (Exception ex) {
					// TODO: handle exception
//					showDialog("asdasd");
				}
			} else {
				GlobalInfo.getInstance().setAllowResetLocation(-1);
			}
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER: {
			if (this.isSaveActionLogAndCloseCustomerAfterTakePhoto) {
				this.isSaveActionLogAndCloseCustomerAfterTakePhoto = false;
				this.closeProgressDialog();
			} else {
				super.handleErrorModelViewEvent(modelEvent);
			}
			break;
		}
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (isValidateMenu(arg2))
			return;

		// TamPQ: giai phong TrainingReviewStaff de phan biet luong Hinh anh WW
		// voi DS Hinh anh
		setTrainingReviewStaffBundle(null);
		notifyChangeSelection(arg2);
		showDetails(lstMenuItems.get(arg2).menuIndex);
	}

	/**
	 * Thay doi selection
	 *
	 * @author: TamPQ
	 * @param arg2
	 * @return: voidvoid
	 * @throws:
	 */
	private void notifyChangeSelection(int index) {
		for (int i = 0; i < lstMenuItems.size(); i++) {
			if (i == index)
				lstMenuItems.get(i).setSelected(true);
			else
				lstMenuItems.get(i).setSelected(false);
		}
		lstMenuAdapter.notifyDataSetChanged();

		// xu li khi qua cac tab ngoai huan luyen thi bo menu tan khach hang
		// dungnx
		if (index != 3) {
			ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			if ((actionLog != null && actionLog.isVisited())
					|| (actionLog != null && actionLog.isOr == 1)) {
				endVisitCustomerBar();
			}
		}
	}

	@Override
	protected void getActionWhenClickMenu(int action) {
		// TODO Auto-generated method stub
		handleSwitchFragment(null, action, SupervisorController.getInstance());
	}

	/**
	 * validate khi nhan menu chinh
	 *
	 * @author : BangHN since : 1.0
	 */
	private boolean isValidateMenu(int index) {
		boolean isOk = false;
		FragmentManager fm = getFragmentManager();
		BaseFragment fg;
		if (lstMenuItems.get(index).isSelected()) {
			switch (index) {
//			case 0:
//				fg = (BaseFragment) fm
//						.findFragmentByTag(GlobalUtil.getTag(ReportProgressDateView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
//			case 1:
//				fg = (BaseFragment) fm
//						.findFragmentByTag(GlobalUtil.getTag(GsnppRouteSupervisionView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
//			case 2:
//				fg = (BaseFragment) fm
//						.findFragmentByTag(GlobalUtil.getTag(ManagerEquipmentView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
//			case 3:
//				fg = (BaseFragment) fm
//						.findFragmentByTag(GlobalUtil.getTag(GSNPPTrainingPlanView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
//			case 4:
//				fg = (BaseFragment) fm
//						.findFragmentByTag(GlobalUtil.getTag(SuperVisorPromotionProgramView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
//			case 5:
//				fg = (BaseFragment) fm.findFragmentByTag(GlobalUtil.getTag(FollowProblemView.class));
//				if (fg != null && fg.isVisible()) {
//					isOk = true;
//				}
//				break;
			default:// 6
				fg = (BaseFragment) fm.findFragmentByTag(GlobalUtil.getTag(CustomerSaleList.class));
				if (fg != null && fg.isVisible()) {
					isOk = true;
				}
				break;
			}
		}
		return isOk;
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() <= 1) {
			// finish();
			GlobalUtil.showDialogConfirm(this,
					StringUtil.getString(R.string.TEXT_CONFIRM_EXIT),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					CONFIRM_EXIT_APP_OK,
					StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
					CONFIRM_EXIT_APP_CANCEL, null);
		}
//		else if (fm.getBackStackEntryCount() > 1) {
//			int handleBack = -1;
//			if (GlobalUtil.getTag(GSNPPPostFeedbackView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				GSNPPPostFeedbackView view = (GSNPPPostFeedbackView) getFragmentManager()
//						.findFragmentByTag(GlobalUtil.getTag(GSNPPPostFeedbackView.class));
//				if (view != null) {
//					handleBack = view.onBackPressed();
//				}
//			} else if (GlobalUtil.getTag(SupervisorInventoryDeviceView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				SupervisorInventoryDeviceView view = (SupervisorInventoryDeviceView) getFragmentManager()
//						.findFragmentByTag(GlobalUtil.getTag(SupervisorInventoryDeviceView.class));
//				if (view != null) {
//					handleBack = view.onBackPressed();
//				}
//			}
//			// dinh nghia cac luong xu ly su kien back dac biet
//			if (handleBack == -1) {
//				GlobalInfo.getInstance().popCurrentTag();
//				super.onBackPressed();
//			}
//		}
		else {
			GlobalInfo.getInstance().popCurrentTag();
			super.onBackPressed();
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case MENU_FINISH_VISIT:
			CustomerListItem item = GlobalInfo.getInstance().getProfile()
					.getLastVisitCustomer();
			if (item == null || !item.isHaveDisplayProgramNotYetVote) {
				ActionLogDTO action = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				String endTime;
				if (action != null && action.aCustomer != null) {
					endTime = DateUtils.getVisitEndTime(action.aCustomer);
					SpannableObject textConfirmed = new SpannableObject();
					textConfirmed.addSpan(StringUtil
							.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.NORMAL);
					if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
						textConfirmed
						.addSpan(
								" "
										+ action.aCustomer.customerCode,
										ImageUtil.getColor(R.color.WHITE),
										android.graphics.Typeface.BOLD);
					}
					textConfirmed.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
					if (!StringUtil.isNullOrEmpty(action.aCustomer.customerName)) {
						textConfirmed.addSpan(action.aCustomer.customerName,
								ImageUtil.getColor(R.color.WHITE),
								android.graphics.Typeface.BOLD);
					}
					textConfirmed.addSpan(" "+StringUtil.getString(R.string.TEXT_IN)+" ",
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.NORMAL);
					textConfirmed.addSpan(
							DateUtils.getVisitTime(action.startTime, endTime),
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
					textConfirmed.addSpan(StringUtil
							.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.NORMAL);
					GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this,
							textConfirmed.getSpan(),
							StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
							ACTION_FINISH_VISIT_CUS_OK,
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
							ACTION_FINISH_VISIT_CUS_CANCEL, null, true, true);
				}
			} else {
				this.showDialog(StringUtil
						.getString(R.string.TEXT_REQUIRE_TAKE_PHOTO_DISPLAY_BEFORE_OLD_CUSTOMER));
			}
			//			endVitCustomer();
			break;
		case CONFIRM_EXIT_APP_OK:
//			endVitCustomer();
//			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
			TransactionProcessManager.getInstance().cancelTimer();
			//xu ly thoat ung dung
			processExitApp();

//			finish();
			break;
		case CONFIRM_EXIT_APP_CANCEL:
			break;
		case ACTION_FINISH_VISIT_CUS_OK:
			// requestUpdateActionLog("0", null, null, this);
			// int arg2 = (Integer) data;
			// notifyChangeSelection(arg2);
			// showDetails(arg2);
			// break;
			if (trainingReviewStaff != null) {
				isFinish = true;
				CustomerDTO cus = (CustomerDTO) trainingReviewStaff
						.getSerializable(IntentConstants.INTENT_CUSTOMER_OBJECT);
//				if (!allowSaveResult) {
//					this.reviewsStaffViewItems.trainingResultItems
//							.remove(cus.customerId);
//
//				}
				CustomerListItem cusItem = new CustomerListItem();
				cusItem.aCustomer = cus;
				requestUpdateActionLog("0", "0", cusItem, this);

				sendBroadcast(ActionEventConstant.END_VISIT_WORKWITH,
						new Bundle());
			} else {
				requestUpdateActionLog("0", "0", null, this);
			}
			if (data != null) {
				int arg2 = (Integer) data;
				notifyChangeSelection(arg2);
				showDetails(lstMenuItems.get(arg2).menuIndex);
			}
			break;
		case ACTION_FINISH_VISIT_CUS_CANCEL:
			break;
		case MENU_FINISH_VISIT_CLOSE:
			this.showPopupConfirmCustomerClose();
			break;
		case CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR:
			// show camera
			this.isTakePhotoFromMenuCloseCustomer = true;
			this.takenPhoto = GlobalUtil.takePhoto(this,
					RQ_TAKE_PHOTO);
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		// TODO Auto-generated method stub
		switch (action) {
//		case ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION:
//			break;
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			// lay lai thong tin customer info
//			readAllowResetLocation();
			getCustomerInfoVisit();
			break;
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	public void getCustomerInfoVisit() {
		ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
		.getActionLogVisitCustomer();
		if (actionLog != null && actionLog.aCustomer != null && GlobalInfo.getInstance().getTrainingStaffId() != -1
				&& GlobalInfo.getInstance().getTrainngDetailId() != -1) {
			Bundle bundle = new Bundle();
			bundle.putString(IntentConstants.INTENT_STAFF_ID, "" + GlobalInfo.getInstance().getTrainingStaffId());
			bundle.putString(IntentConstants.INTENT_STAFF_TRAIN_DETAIL_ID, "" + GlobalInfo.getInstance().getTrainngDetailId());
			bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, "" + actionLog.aCustomer.customerId);
			handleViewEvent(bundle, ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT, SupervisorController.getInstance());
		}
	}

	/**
	 *
	 * show popup before show camera when click button "dong cua"
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void showPopupConfirmCustomerClose() {
		GlobalUtil
				.showDialogConfirm(
						this,
						getString(R.string.TEXT_NOTIFY_DISPLAY_CAMERA_CLICK_BUTTON_CUSTOMER_CLOSE_THE_DOOR),
						"OK", CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR, "", 0,
						null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			String filePath = "";
			switch (requestCode) {
			case RQ_TAKE_PHOTO: {
				if (this.isTakePhotoFromMenuCloseCustomer) {
					this.isTakePhotoFromMenuCloseCustomer = false;
					this.isSaveActionLogAndCloseCustomerAfterTakePhoto = true;
					if (resultCode == RESULT_OK && takenPhoto != null) {
						filePath = takenPhoto.getAbsolutePath();
						ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
								this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
						validator.setDataIntent(data);
						if (validator.execute()) {
//							this.saveCustomerCloseDoorAndGotoCustomerList();
							requestUpdateActionLog("1", null, null, this);
							updateTakenPhoto();
						}
					}
				} else {
					super.onActivityResult(requestCode, resultCode, data);
				}
				break;
			}
			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;

			}
		} catch (Exception ex) {
			ServerLogger.sendLog("ActivityState", "GlobalBaseActivity : "
					+ VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 *
	 * update image to db and to server
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void updateTakenPhoto() {
		this.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		viewData.add(IntentConstants.INTENT_STAFF_ID);
		viewData.add(Integer.toString(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId()));

		viewData.add(IntentConstants.INTENT_SHOP_ID);
		if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId())) {
			viewData.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		} else {
			viewData.add("1");
		}

		if (this.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(this.takenPhoto.getName());

			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(this.takenPhoto.getAbsolutePath());
		}

		MediaItemDTO dto = new MediaItemDTO();

		try {
			dto.objectId = Long.parseLong(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerId());
			dto.objectType = 0;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = this.takenPhoto.getAbsolutePath();
			dto.thumbUrl = this.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
//			dto.type = 1;
			dto.status = 1;
			dto.fileSize = this.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId())) {
				dto.shopId = Integer.valueOf(GlobalInfo.getInstance()
						.getProfile().getUserData().getInheritShopId());
			} else {
				dto.shopId = 1;
			}

			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerId());
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));
			viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
			viewData.add(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerCode());
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
//			viewData.add(IntentConstants.INTENT_TYPE);
//			viewData.add("1");
		} catch (NumberFormatException e1) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}

		CustomerListItem lastVisitCustomer = GlobalInfo.getInstance()
				.getProfile().getLastVisitCustomer();
		if (lastVisitCustomer != null) {
			lastVisitCustomer.visitStatus = VISIT_STATUS.VISITED_CLOSED;
			lastVisitCustomer.isHaveDisplayProgramNotYetVote = false;
		}
		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	/**
	 * Doc cau hinh cho phep reset vi tri
	 *
	 * @author: DungNX
	 */
	private void readAllowResetLocation() {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_CODE, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopCode());
		handleViewEventWithOutAsyntask(b, ActionEventConstant.ACTION_READ_ALLOW_RESET_LOCATION, UserController.getInstance());
	}

}
