/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * product info (industry of product)
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class PRODUCT_INFO_TABLE extends ABSTRACT_TABLE {
	public static final String PRODUCT_INFO_ID = "PRODUCT_INFO_ID";
	public static final String PRODUCT_INFO_CODE = "PRODUCT_INFO_CODE";
	public static final String PRODUCT_INFO_NAME = "PRODUCT_INFO_NAME";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String STATUS = "STATUS";
	public static final String TYPE = "TYPE";
	public static final String PRODUCT_INFO_NAME_TABLE = "PRODUCT_INFO";

	public PRODUCT_INFO_TABLE(SQLiteDatabase mDB) {
		this.tableName = PRODUCT_INFO_NAME_TABLE;
		this.mDB = mDB;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public ArrayList<DisplayProgrameItemDTO> getListDepartDisplayPrograme() throws Exception{
		ArrayList<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		var1.append(" SELECT PRODUCT_INFO_CODE ");
		var1.append(" FROM PRODUCT_INFO ");
		var1.append(" WHERE TYPE = ?  ");
		params.add("1");
		var1.append(" 		AND STATUS = 1 ");
		var1.append(" ORDER BY PRODUCT_INFO_CODE ");
		Cursor c = null;
		try {
			// get total row first
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO dto = initComboBoxDisProItemFromCursor(c);
						result.add(dto);
					} while (c.moveToNext());
				}
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return result;
	}

	/**
	 * Tao du lieu combobox cho nganh hang
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: ComboBoxDisplayProgrameItemDTO
	 * @throws:
	 */
	private DisplayProgrameItemDTO initComboBoxDisProItemFromCursor(Cursor c) {
		// TODO Auto-generated method stub
		DisplayProgrameItemDTO result = new DisplayProgrameItemDTO();
		result.value = (CursorUtil.getString(c, PRODUCT_INFO_CODE));
		result.name = (CursorUtil.getString(c, PRODUCT_INFO_CODE));
		return result;
	}

	/**
	 * Danh sach nganh hang con
	 * @author: yennth16
	 * @param productInfoCode
	 * @since: 10:33:00 19-06-2015
	 * @return: List<DisplayProgrameItemDTO>
	 * @throws:
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DisplayProgrameItemDTO> getListSubcat(String productInfoCode) throws Exception{
		ArrayList<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ");
		var1.append("    pi.product_info_code ");
		var1.append("FROM ");
		var1.append("    PRODUCT_INFO PI, ");
		var1.append("    PRODUCT_INFO_MAP PIM ");
		var1.append("WHERE 1=1 ");
		var1.append("    and PI.TYPE = ? ");
		params.add("2");
		var1.append("    AND pim.object_type = 12 ");
		if(!StringUtil.isNullOrEmpty(productInfoCode)){
			var1.append("    AND pim.from_object_id in (select p.product_info_id from product_info p  ");
			var1.append("    where p.status = 1 and p.type = 1  and p.product_info_code like upper(?)) ");
			params.add(productInfoCode);
		}else{
			var1.append("    AND pim.from_object_id in (select p.product_info_id from product_info p where p.status = 1 and p.type = 1) ");
		}
		var1.append("    AND pim.to_object_id = pi.product_info_id ");
		var1.append("    AND PI.STATUS = 1 ");
		var1.append("GROUP BY pi.product_info_code ");
		var1.append("ORDER BY ");
		var1.append("    PRODUCT_INFO_CODE ");
		Cursor c = null;
		try {
			// get total row first
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameItemDTO dto = initComboBoxDisProItemFromCursor(c);
						result.add(dto);
					} while (c.moveToNext());
				}
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return result;
	}

}
