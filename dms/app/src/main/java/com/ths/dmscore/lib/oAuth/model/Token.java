package com.ths.dmscore.lib.oAuth.model;

import java.io.Serializable;

/**
 * Represents an OAuth token (either request or access token) and its secret
 * 
 * @author Pablo Fernandez
 */
public class Token implements Serializable {
	private static final long serialVersionUID = 1187699856140168577L;
	private final String token;
	private final String secret;
	private final String rawResponse;
	private final String tokenType;

	/**
	 * Default constructor
	 * 
	 * @param token
	 *            token value
	 * @param secret
	 *            token secret
	 */
	public Token(String token, String secret) {
		this(null, token, secret, null);
	}

	public Token(String tokenType, String token, String secret, String rawResponse) {
		this.token = token;
		this.secret = secret;
		this.rawResponse = rawResponse;
		this.tokenType = tokenType;
	}

	public String getToken() {
		return token;
	}

	public String getSecret() {
		return secret;
	}

	public String getTokenType() {
		return tokenType;
	}

	public String getRawResponse() {
		if (rawResponse == null) {
			throw new IllegalStateException("This token object was not constructed by scribe and does not have a rawResponse");
		}
		return rawResponse;
	}

	@Override
	public String toString() {
		return String.format("Token[%s, %s, %s]", tokenType, token, secret);
	}
}
