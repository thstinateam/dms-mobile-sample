/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Mo ta muc dich cua lop (interface)
 * 
 * @author: HoanPD1
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVProgReportProDispDetailSaleRowDTO {
	// ma khach hang
	public String customerCode;
	// teen khach hang
	public String customerName;
	// dia chi
	public String customerAddress;
	// doanh so con lai
	public long remainSale;
	// dinh muc
	public long amountPlan;
	//doanh so thuc hien
	public long amount;
	// muc
	public String displayProgLevel;
	// dat, ko dat
	public int result;

	public TBHVProgReportProDispDetailSaleRowDTO() {
		customerCode = "";
		customerName = "";
		customerAddress = "";
		remainSale = 0;
	}

	public TBHVProgReportProDispDetailSaleRowDTO(Cursor c) {
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		customerAddress = CursorUtil.getString(c, "CUSTOMER_ADD");
		remainSale = CursorUtil.getLong(c, "REMAIN_SALE");
	}
}
