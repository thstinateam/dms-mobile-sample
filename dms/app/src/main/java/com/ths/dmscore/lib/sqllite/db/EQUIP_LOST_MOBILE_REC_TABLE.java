/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.EquipLostMobileRecDTO;

/**
 * Bao mat mobile
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  19:32:24 18-12-2014
 */
public class EQUIP_LOST_MOBILE_REC_TABLE extends ABSTRACT_TABLE {
	// id bien ban bao mat
	public static final String EQUIP_LOST_MOBILE_REC_ID = "EQUIP_LOST_MOBILE_REC_ID";
	// ma bao mat
	public static final String CODE = "CODE";
	// id thiet bi
	public static final String EQUIP_ID = "EQUIP_ID";
	// id khach hang
	public static final String STOCK_ID = "STOCK_ID";
	// ma kho khach hang
	public static final String STOCK_CODE = "STOCK_CODE";
	// trang thai cua phieu
	public static final String RECORD_STATUS = "RECORD_STATUS";
	// id nhan vien bao mat
	public static final String REPORT_STAFF_ID = "REPORT_STAFF_ID";
	// nagy cuoi thiet bi phat sinh doanh so
	public static final String LAST_ARISING_SALES_DATE = "LAST_ARISING_SALES_DATE";
	// ngay mat
	public static final String LOST_DATE = "LOST_DATE";
	// id phieu bao mat
	public static final String EQUIP_LOST_REC_ID = "EQUIP_LOST_REC_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String STOCK_NAME = "STOCK_NAME";
	public static final String ADDRESS = "ADDRESS";

	public static final String TABLE_NAME = "EQUIP_LOST_MOBILE_REC";

	public EQUIP_LOST_MOBILE_REC_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { EQUIP_LOST_MOBILE_REC_ID, CODE, EQUIP_ID,
				STOCK_ID, STOCK_CODE, RECORD_STATUS, REPORT_STAFF_ID,
				LAST_ARISING_SALES_DATE, LOST_DATE, EQUIP_LOST_REC_ID,
				CREATE_DATE, CREATE_USER, UPDATE_USER, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((EquipLostMobileRecDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @author: hoanpd1
	 * @since: 08:25:25 19-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param dto
	 * @return
	 */
	private ContentValues initDataRow(EquipLostMobileRecDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(EQUIP_LOST_MOBILE_REC_ID, dto.recordLostMobileRecId);
		editedValues.put(CODE, dto.coderecordLostMobileRec);
		editedValues.put(EQUIP_ID, dto.equipId);
		editedValues.put(STOCK_ID, dto.stockId);
		editedValues.put(STOCK_CODE, dto.stockCode);
		editedValues.put(RECORD_STATUS, dto.recordStatus);
		editedValues.put(REPORT_STAFF_ID, dto.recordStaffId);
		editedValues.put(LAST_ARISING_SALES_DATE, dto.dateLastSales);
		editedValues.put(LOST_DATE, dto.dateLost);
		editedValues.put(EQUIP_LOST_REC_ID, dto.equipLostRecId);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(STOCK_NAME, dto.stockName);
		editedValues.put(ADDRESS, dto.address);
		return editedValues;
	}
	
	/**
	 * Inser thiet bi bao mat mobile
	 * @author: hoanpd1
	 * @since: 09:50:05 19-12-2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public long insertLostDevice(EquipLostMobileRecDTO dto) throws Exception {
		TABLE_ID idTable = new TABLE_ID(mDB);
		dto.recordLostMobileRecId = idTable.getMaxIdTime(TABLE_NAME);
		dto.coderecordLostMobileRec = "ELMR_" + dto.recordLostMobileRecId;
		return insert(dto);
	}

	/**
	 * @author: DungNX
	 * @param equipLostMobileID
	 * @return
	 * @return: long
	 * @throws:
	*/
	public long updateRejectStatus(String equipLostMobileID) {
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(RECORD_STATUS, 3);
			String[] params = { equipLostMobileID };
			int result = update(editedValues, EQUIP_LOST_MOBILE_REC_ID + " = ?", params);
			return result;
		} catch (Exception e) {
			return -1;
		}
	}
}
