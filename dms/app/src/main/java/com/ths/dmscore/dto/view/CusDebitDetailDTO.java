package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.PaymentDetailDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.dto.db.PayReceivedDTO;

/**
 * Mo ta muc dich cua class
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class CusDebitDetailDTO {
	public ArrayList<CusDebitDetailItem> arrList = new ArrayList<CusDebitDetailDTO.CusDebitDetailItem>();
	public boolean isAllUncheck = true;
	public long payNowTotal;
	public int customerId;
	public long debitId;
	public long totalPay;
	public long totalDebit;
	public long totalDiscount;

	public PayReceivedDTO payReceivedDto;
	public ArrayList<PaymentDetailDTO> arrPaymentDetailDto;

	public CusDebitDetailDTO() {

	}

	public CusDebitDetailItem newCusDebDetailItem() {
		return new CusDebitDetailItem();
	}

	public class CusDebitDetailItem {
		public long debitDetailId;
		public String orderCode;
		public String orderNumber;
		public String orderDate;
		public long totalDebit;
		public long paidAmount;
		// dung luu vao bang PAYMENT_DETAIL
		public long paidAmountItemDetail;
		public long remainingAmount;
//		public long thanhToan;
		public String receipt;
		public String paidDate;
		public int index;
		public boolean isCheck;
		public boolean isWouldPay;
		// Chiet khau
		public long discount;

		public CusDebitDetailItem() {
		}

		/**
		 * Mo ta muc dich cua ham
		 * 
		 * @author: TamPQ
		 * @param c
		 * @return: voidvoid
		 * @throws:
		 */
		public void initData(Cursor c) {
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
				debitDetailId = CursorUtil.getLong(c, "DEBIT_DETAIL_ID");
				orderNumber = CursorUtil.getString(c, "ORDER_NUMBER");
				orderDate = CursorUtil.getString(c, "ORDER_DATE");
				totalDebit = CursorUtil.getLong(c, "TOTAL");
				paidAmount = CursorUtil.getLong(c, "TOTAL_PAY");
				remainingAmount = CursorUtil.getLong(c, "REMAIN");
				receipt = CursorUtil.getString(c, "PAY_RECEIVED_NUMBER");
				paidDate = CursorUtil.getString(c, "PAY_DATE");
				discount = Long.parseLong(CursorUtil.getString(c, "DISCOUNT"));
			} catch (Exception e) {
			}
		}
	}

//	/**
//	 * Mo ta muc dich cua ham
//	 * 
//	 * @author: TamPQ
//	 * @return
//	 * @return: JSONArrayvoid
//	 * @throws:
//	 */
//	public JSONArray generatePayDebtSql() {
//		JSONArray listSql = new JSONArray();
//
////		DebitDTO debit = new DebitDTO();
//		// update DEBIT table
//		
//		// yeu cau GP: vansale khong gui truc tiep debit len server nua!
//
////		listSql.put(debit.generateUpdateForPayDebt(this));
//
//		// insert PAY_RECEIVED table
//		listSql.put(payReceivedDto.generateInsertForPayDebt());
//		for (int i = 0; i < arrList.size(); i++) {
//			if (arrList.get(i).isWouldPay) {
//				PaymentDetailDTO paymentDetail = arrPaymentDetailDto.get(i);
////				DebitDetailDTO debitDetail = new DebitDetailDTO();
//				// update DEBIT_DETAIL table
////				listSql.put(debitDetail.generateUpdateForPayDebt(arrList.get(i)));
//
//				// insert PAYMENT_DETAIL table
//				listSql.put(paymentDetail.generateInsertForPayDebt());
//			}
//		}
//
//		return listSql;
//	}
}
