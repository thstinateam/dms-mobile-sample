/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.customer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

/**
 * thong tin chung
 * @author : BangHN
 * since   : 1.0
 * version : 1.0
 */
public class CustomerLocationUpdateHeaderView extends LinearLayout {

	TextView tvCustomerName;
	TextView tvCustomerAddress;
	Button btUpdate;
	LinearLayout llUpdate;
	Button btUseMyPosition;

	/**
	 * @param context
	 * @param attrs
	 */
	public CustomerLocationUpdateHeaderView(Context context) {
		super(context);
		initView(context);
	}


	/**
	 * @param context
	 * @param attrs
	 */
	public CustomerLocationUpdateHeaderView(Context context, AttributeSet attrs) {
		super(context);
		initView(context);
	}

	/**
	 * Init header
	 * @author : BangHN
	 * since : 1.0
	 */
	private void initView(Context mContext) {
		LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.layout_customer_location_update_header, this);
		tvCustomerAddress = (TextView)view.findViewById(R.id.tvCustomerAddress);
		tvCustomerName = (TextView)view.findViewById(R.id.tvCustomerName);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			btUpdate = (Button) PriUtils.getInstance().findViewByIdGone(view, R.id.btUpdate, PriHashMap.PriControl.NVBH_BANHANG_VITRI_CAPNHATVITRI);
			btUseMyPosition = (Button) view.findViewById(R.id.btUseMyPosition);
			llUpdate = (LinearLayout) PriUtils.getInstance().findViewByIdGone(view, R.id.llUpdate, PriHashMap.PriControl.NVBH_BANHANG_VITRI_CAPNHATVITRI);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR
				|| GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			btUpdate = (Button) PriUtils.getInstance().findViewByIdGone(view, R.id.btUpdate, PriHashMap.PriControl.GSNPP_VITRI_CAPNHATVITRI);
			llUpdate = (LinearLayout) PriUtils.getInstance().findViewByIdGone(view, R.id.llUpdate, PriHashMap.PriControl.GSNPP_VITRI_CAPNHATVITRI);
		}
	}

	/**
	 * Set thong tin header cua khach hang
	 * @author : BangHN
	 * since : 1.0
	 */
	public void setCustomerInfo(CustomerDTO customer){
		if(customer != null){
			int typeUser = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
			tvCustomerName.setText(customer.getShortCode() + " - " + customer.getCustomerName());
			tvCustomerAddress.setText(customer.address);
			if(typeUser == UserDTO.TYPE_STAFF && (customer.getLat() <= 0 ||customer.getLng() <= 0)){
				PriUtils.getInstance().setStatus(llUpdate, PriUtils.ENABLE);
			}else{
				PriUtils.getInstance().setStatus(llUpdate, PriUtils.INVISIBLE);
			}
		}
	}

}
