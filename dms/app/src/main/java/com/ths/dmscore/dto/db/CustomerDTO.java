/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_ATTRIBUTE_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * Luu thong tin khach hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerDTO extends AbstractTableDTO implements Serializable {
	private static final long serialVersionUID = -8997929507990163917L;
	// id khach hang
	public long customerId;
	// ma khach hang
	public String customerCode;
	// ma rut gon 3 ky tu
	private String shortCode;
	// ten khach hang
	public String customerName;
	// full address: dia chi fullDate du
	public String address;
	// id NPP
	private String shopId;
	// truong nay chua dung
	private String country;
	// ma vung
	private String areaCode;
	// duong
	public String street;
	// so nha
	public String houseNumber;
	// dien thoai
	public String phone;
	// ngay tao
	private String createDate;
	// ngay cap nhat
	private String updateDate;
	// truong nay chua dung
	private String postalCode;
	// nguoi lien he
	private String contactPerson;
	// truong nay chua dung
	private String taxCode;
	// di dong
	public String mobilePhone;
	// khu vuc 1: thanh thi, 2: nong thon
	private int location;
	// loai kenh
	public int channelTypeId;
	// do trung thanh
	private int loyalty;
	// nguoi tao
	private String createUser;
	// nguoi cap nhat
	private String updateUser;
	// loai chuong trinh tham gia
	private String dislay;
	// private String tuyen;
	// private String trang_thai;
	// don hang cuoi cung duoc duyet
	//private String lastApproveOrder;
	// don hang cuoi cung tao
	private String lastOrder;
	// status
	private int status;
	// lat
	public double lat;
	// lng
	public double lng;
	// cong no cho phep toi da
	public long maxDebitAmount;
	// so ngay toi da cho phep no
	public String maxDebitDate;
	// kiem tra xem khach hang co dang duoc ghe tham khong
	// dung cho GSNPP
	public boolean isVisiting;
	// doi tuong shop cua customer
	public ShopDTO shopDTO=new ShopDTO();

	public String deliverID;
	public String cashierStaffID;

	// area id
	public int areaId;
	public long staffId;
	public String nameText;
	public String orderSource;
	public String email;
	public String fax;

	public int applyDebitLimited;
	public long typePostitionId;

	public ArrayList<CustomerAttributeDetailViewDTO> listCusAttrDetail;
	public String birthDate;

	public CustomerDTO() {
		super(TableType.CUSTOMER);
		listCusAttrDetail = new ArrayList<CustomerAttributeDetailViewDTO>();
	}

	public String getCustomerId() {
		return Long.toString(customerId);
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAreaCde() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getMobilephone() {
		return mobilePhone;
	}

	public void setMobilephone(String mobilephone) {
		this.mobilePhone = mobilephone;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getCustomerTypeId() {
		return channelTypeId;
	}

	public void setCustomerTypeId(int customerTypeId) {
		this.channelTypeId = customerTypeId;
	}

	public void setCashierStaffID(String cashierStaffID) {
		this.cashierStaffID = cashierStaffID;
	}
	public void setDeliverID(String deliverID) {
		this.deliverID = deliverID;
	}

	public int getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getDislay() {
		return dislay;
	}

	public void setDislay(String dislay) {
		this.dislay = dislay;
	}

//	public String getLastApproverder() {
//		return this.lastApproveOrder;
//	}
//
//	public void setLastApproverder(String lastApproveOrder) {
//		this.lastApproveOrder = lastApproveOrder;
//	}

	public String getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(String lastOrder) {
		this.lastOrder = lastOrder;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	/**
	 * @return the maxDebitAmount
	 */
	public long getMaxDebitAmount() {
		return maxDebitAmount;
	}

	/**
	 * @param maxDebitAmount
	 *            the maxDebitAmount to set
	 */
	public void setMaxDebitAmount(long maxDebitAmount) {
		this.maxDebitAmount = maxDebitAmount;
	}

	/**
	 * @return the maxDebitDate
	 */
	public String getMaxDebitDate() {
		return maxDebitDate;
	}

	/**
	 * @param maxDebitDate
	 *            the maxDebitDate to set
	 */
	public void setMaxDebitDate(String maxDebitDate) {
		this.maxDebitDate = maxDebitDate;
	}

	public boolean isHaveLocation(){
		return lat > 0 && lng > 0;
	}
	
	/**
	 * Khoi tao thong tin sau khi query database
	 *
	 * @author : BangHN since : 1.0
	 */
	public void initLogDTOFromCursor(Cursor c) {
		setCustomerId(CursorUtil.getLong(c, "CUSTOMER_ID"));
		setCustomerCode(CursorUtil.getString(c, "CUSTOMER_CODE"));
		setCustomerName(CursorUtil.getString(c, "CUSTOMER_NAME"));
		setShopId(CursorUtil.getString(c, "SHOP_ID"));
		address = CursorUtil.getString(c, "ADDRESS");
		setStreet(CursorUtil.getString(c, "STREET"));
		setHouseNumber(CursorUtil.getString(c, "HOUSENUMBER"));
		setPhone(CursorUtil.getString(c, "PHONE"));
		setContactPerson(CursorUtil.getString(c, "CONTACT_NAME"));
		setMobilephone(CursorUtil.getString(c, "MOBIPHONE"));
		setLat(CursorUtil.getDouble(c, "LAT"));
		setLng(CursorUtil.getDouble(c, "LNG"));
	}

	/**
	 * Generate cau lenh update
	 *
	 * @author: TruongHN
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateFromOrderSql() {
		JSONObject json = new JSONObject();
		try {
			// update customer set LAST_ORDER = sysdate where customer_id =
			// 87484

			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					CUSTOMER_TABLE.CUSTOMER_TABLE);

			JSONArray params = new JSONArray();
			if (StringUtil.isNullOrEmpty(lastOrder)) {
				params.put(GlobalUtil.getJsonColumn(STAFF_CUSTOMER_TABLE.LAST_ORDER, "", DATA_TYPE.NULL.toString()));
			} else {
				// ds params
				params.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAST_ORDER, lastOrder, null));
			}

			// khong cap nhat lastApproveOrder nua
//			if (!StringUtil.isNullOrEmpty(lastApproveOrder)) {
//				params.put(GlobalUtil.getJsonColumn(
//						CUSTOMER_TABLE.LAST_APPROVE_ORDER, lastApproveOrder,
//						null);
//			}

			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID,
					customerId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
		}
		return json;
	}

	/**
	 * Generate cau lenh update last order after delete
	 *
	 * @author: PhucNT
	 * @param lastOrder2
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateLastOrder(String lastOrder2) {
		JSONObject json = new JSONObject();
		try {
			// update customer set LAST_ORDER = sysdate where customer_id =
			// 87484

			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					CUSTOMER_TABLE.CUSTOMER_TABLE);

			// ds params
			JSONArray params = new JSONArray();
			if (StringUtil.isNullOrEmpty(lastOrder2)) {
				params.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAST_ORDER,
						"", DATA_TYPE.NULL.toString()));
			} else {
				params.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAST_ORDER,
						lastOrder2, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID,
					customerId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
		}
		return json;
	}

	/**
	 * generate cau lenh update location trong customerinfo
	 *
	 * @author : BangHN since : 1.0
	 */
	public JSONObject generateUpdateLocationSql() {
		JSONObject updateLocationJson = new JSONObject();
		try {
			updateLocationJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			updateLocationJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_TABLE.CUSTOMER_TABLE);

			// ds params
			JSONArray detailPara = new JSONArray();
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAT, lat, null));
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LNG, lng, null));
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAT, lat, lat == Constants.LAT_LNG_NULL ? DATA_TYPE.NULL.toString(): null));
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LNG, lng, lng == Constants.LAT_LNG_NULL ? DATA_TYPE.NULL.toString(): null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAT, lat, BigDecimal.valueOf(lat).compareTo(BigDecimal.valueOf(Constants.LAT_LNG_NULL)) == 0 ? DATA_TYPE.NULL.toString(): null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LNG, lng, BigDecimal.valueOf(lng).compareTo(BigDecimal.valueOf(Constants.LAT_LNG_NULL)) == 0 ? DATA_TYPE.NULL.toString(): null));
			updateLocationJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
			updateLocationJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
		}

		return updateLocationJson;
	}

	public void parseCustomerInfo(Cursor c) {
		setCustomerId(CursorUtil.getLong(c, "CUSTOMER_ID"));
		setCustomerCode(CursorUtil.getString(c, "SHORT_CODE"));
		setCustomerName(CursorUtil.getString(c, "CUSTOMER_NAME"));
		setAddress(CursorUtil.getString(c, "ADDRESS"));
		setStreet(CursorUtil.getString(c, "STREET"));
		setHouseNumber(CursorUtil.getString(c, "HOUSENUMBER"));
		setShopId(CursorUtil.getString(c, "SHOP_ID"));
		staffId = CursorUtil.getLong(c, "STAFF_ID");
	}


	/**
	 * JSON xoa KH
	 *
	 * @author: dungdq3
	 * @since: 1:45:59 PM Sep 22, 2014
	 * @return: JSONObject
	 * @throws:
	 * @return:
	 * @throws JSONException
	 */
	public JSONObject generateDeleteCustomerSql() throws JSONException {
		JSONObject deleteCustomerJson = new JSONObject();
		deleteCustomerJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
		deleteCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_TABLE.CUSTOMER_TABLE);
		// ds params
		JSONArray detailPara = new JSONArray();
		// ...them thuoc tinh
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.STATUS, status, null));
		deleteCustomerJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

		// ds where params --> insert khong co menh de where
		JSONArray wheres = new JSONArray();
		wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
		deleteCustomerJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		return deleteCustomerJson;
	}


	public void initCreateCustomerDTOFromCursor(Cursor c) {
		customerId = CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID);
		customerName = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
		areaId = (int) CursorUtil.getLong(c, CUSTOMER_TABLE.AREA_ID);
		typePostitionId = CursorUtil.getLong(c, CUSTOMER_TABLE.SALE_POSITION_ID);
		String tempBirthDate = CursorUtil.getString(c, CUSTOMER_TABLE.BIRTHDAY);
		birthDate = DateUtils.convertDateOneFromFormatToAnotherFormat(tempBirthDate, DateUtils.DATE_STRING_YYYY_MM_DD, DateUtils.DATE_STRING_DD_MM_YYYY);
		channelTypeId = (int)CursorUtil.getLong(c, CUSTOMER_TABLE.CHANNEL_TYPE_ID);
		orderSource = CursorUtil.getString(c, CUSTOMER_TABLE.ORDER_SOURCE);
		houseNumber = CursorUtil.getString(c, CUSTOMER_TABLE.HOUSENUMBER);
		street = CursorUtil.getString(c, CUSTOMER_TABLE.STREET);
		phone = CursorUtil.getString(c, CUSTOMER_TABLE.PHONE);
		email = CursorUtil.getString(c, CUSTOMER_TABLE.EMAIL);
		fax = CursorUtil.getString(c, CUSTOMER_TABLE.FAX);
		contactPerson = CursorUtil.getString(c, CUSTOMER_TABLE.CONTACT_NAME);
		mobilePhone = CursorUtil.getString(c, CUSTOMER_TABLE.MOBIPHONE);
		lat = CursorUtil.getDouble(c, CUSTOMER_TABLE.LAT);
		lng = CursorUtil.getDouble(c, CUSTOMER_TABLE.LNG);
	}

	/**
	 * generate json tao kh
	 *
	 * @author: dungdq3
	 * @since: 5:52:15 PM Jan 26, 2015
	 * @return: JSONObject
	 * @throws:
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateCreateCustomerSql() throws JSONException {
		JSONObject insertCustomerJson = new JSONObject();
		insertCustomerJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
		insertCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_TABLE.CUSTOMER_TABLE);
		// ds params
		JSONArray detailPara = new JSONArray();
		// ...them thuoc tinh
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_CODE, customerCode, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_NAME, customerName, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.SALE_POSITION_ID, typePostitionId, typePostitionId > 0 ? null : DATA_TYPE.NULL.toString()));
		String bDay = DateUtils.convertDateOneFromFormatToAnotherFormat(birthDate, DateUtils.DATE_STRING_DD_MM_YYYY, DateUtils.DATE_STRING_YYYY_MM_DD);
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.BIRTHDAY, bDay, !StringUtil.isNullOrEmpty(bDay) ? null : DATA_TYPE.NULL.toString()));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.SHOP_ID, shopId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.AREA_ID, areaId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.HOUSENUMBER, houseNumber, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.STREET, street, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.PHONE, phone, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CREATE_DATE, createDate, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CONTACT_NAME, contactPerson, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.MOBIPHONE, mobilePhone, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CHANNEL_TYPE_ID, channelTypeId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CREATE_USER, createUser, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.STATUS, status, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CREATE_USER_ID, staffId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.EMAIL, email, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.FAX, fax, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.ADDRESS, address, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.NAME_TEXT, nameText, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAT, lat, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LNG, lng, null));
		insertCustomerJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		return insertCustomerJson;
	}

	public JSONObject generateUpdateCustomerSql() throws JSONException {
		JSONObject updateCustomerJson = new JSONObject();
		updateCustomerJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
		updateCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_TABLE.CUSTOMER_TABLE);

		// ds params
		JSONArray detailPara = new JSONArray();
		// ...them thuoc tinh
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_NAME, customerName, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.AREA_ID, areaId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.SALE_POSITION_ID, typePostitionId, typePostitionId > 0 ? null : DATA_TYPE.NULL.toString()));
		String bDay = DateUtils.convertDateOneFromFormatToAnotherFormat(birthDate, DateUtils.DATE_STRING_DD_MM_YYYY, DateUtils.DATE_STRING_YYYY_MM_DD);
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.BIRTHDAY, bDay, !StringUtil.isNullOrEmpty(bDay) ? null : DATA_TYPE.NULL.toString()));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.HOUSENUMBER, houseNumber, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.STREET, street, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.PHONE, phone, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.UPDATE_DATE, updateDate, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CONTACT_NAME, contactPerson, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.MOBIPHONE, mobilePhone, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CHANNEL_TYPE_ID, channelTypeId, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.UPDATE_USER, updateUser, null));

		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.ADDRESS, address, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.EMAIL, email, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.FAX, fax, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.NAME_TEXT, nameText, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.STATUS, status, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LAT, lat, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.LNG, lng, null));
		updateCustomerJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

		// ds where params --> insert khong co menh de where
		JSONArray wheres = new JSONArray();
		wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
		updateCustomerJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		return updateCustomerJson;
	}

	public JSONObject generateDeleteDetail() throws JSONException {
		JSONObject deleteCustomerJson = new JSONObject();
		deleteCustomerJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
		deleteCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_ATTRIBUTE_DETAIL_TABLE.TABLE_NAME);

		JSONArray wheres = new JSONArray();
		wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_TABLE.CUSTOMER_ID, customerId, null));
		deleteCustomerJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		return deleteCustomerJson;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
}
