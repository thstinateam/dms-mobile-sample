package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.GSNPPTrainingPlanDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * Giam sat huan luyen ngay DTO
 * 
 * @author : TamPQ
 * @version: 1.1
 * @since : 1.0
 */
public class TBHVTrainingPlanDTO implements Serializable {
	private static final long serialVersionUID = 7197926043590160322L;
	// public ArrayList<TBHVGsnppTrainingPlanItem> spinerGsnppList = new
	// ArrayList<TBHVGsnppTrainingPlanDTO.TBHVGsnppTrainingPlanItem>(); // ds
	// gsnpp thuoc quyen quan ly
	public GSNPPTrainingPlanDTO trainingPlanOfGsnppDto = new GSNPPTrainingPlanDTO();
	public ListStaffDTO spinnerStaffList = new ListStaffDTO();
	public int spinnerItemSelected = -1;
	public Hashtable<String, TBHVTrainingPlanItem> tbhvTrainingPlan = new Hashtable<String, TBHVTrainingPlanItem>();

	public class TBHVTrainingPlanItem implements Serializable {
		private static final long serialVersionUID = -8114737159804243036L;
		public Date date;// dayInOrder
		public String dateString;// dayInOrder String
		public int staffId;// staff Id
		public String staffName;// staff Name
		public long trainDetailId;// train Detail Id
		public double score;// score
		public int shopId;// shopId
		public String shopCode;// shopCode
		public String shopName;// shopName
		public int nvbh_staff_id;// nvbh_staff_id
		public String nvbh_staff_name;// nvbh_staff_name
		public int nvbh_trainig_plan_detail_id;// nvbh_trainig_plan_detail_id
		public String staffCode;// staffCode

		/**
		 * init data
		 * 
		 * @author: TamPQ
		 * @return: void
		 * @throws:
		 */
		public void initFromCursor(Cursor c) {
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;
			trainDetailId = CursorUtil.getInt(c, "TRAINING_PLAN_DETAIL_ID");
			staffId = CursorUtil.getInt(c, "GSNPP_STAFF_ID");
			staffName = CursorUtil.getString(c, "GSNPP_STAFF_NAME");
			staffCode = CursorUtil.getString(c, "GSNPP_STAFF_CODE");
			try {
				date = sfs.parse(CursorUtil.getString(c, "TRAINING_DATE"));
				dateString = sfd.format(date);
			} catch (ParseException e) {
			}

		}

	}

	public TBHVTrainingPlanDTO() {
	}

	/**
	 * init TBHVGsnppTrainingPlanItem dto
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public TBHVTrainingPlanItem newTBHVGsnppTrainingPlanItem() {
		return new TBHVTrainingPlanItem();
	}

	/**
	 * is Existed In List
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public boolean isExistedInList(int staffId, ArrayList<TBHVTrainingPlanItem> list) {
		boolean isExisted = false;
		for (int i = 0; i < list.size(); i++) {
			if (staffId == list.get(i).staffId) {
				isExisted = true;
				break;
			}
		}
		return isExisted;
	}
	
}
