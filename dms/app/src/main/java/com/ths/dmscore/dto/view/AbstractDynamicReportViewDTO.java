/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * AbstractDynamicReportViewDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  9:03:32 AM Feb 11, 2015
 */
public abstract class AbstractDynamicReportViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	public int totalReportRow;
	
	public ArrayList<AbstractDynamicReportRowDTO> lstReport;

	public AbstractDynamicReportViewDTO() {
		// TODO Auto-generated constructor stub
		totalReportRow = 0;
		lstReport = new ArrayList<AbstractDynamicReportRowDTO>();
	}
	
	public abstract void setData();

}
