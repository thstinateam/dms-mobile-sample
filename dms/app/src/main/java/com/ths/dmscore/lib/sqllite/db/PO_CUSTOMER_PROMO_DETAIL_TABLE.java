package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class PO_CUSTOMER_PROMO_DETAIL_TABLE extends ABSTRACT_TABLE {

	// id
	public static final String PO_CUSTOMER_PROMO_DETAIL_ID = "PO_CUSTOMER_PROMO_DETAIL_ID";
	public static final String PO_CUSTOMER_DETAIL_ID = "PO_CUSTOMER_DETAIL_ID";
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// % chiet khau
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// So tien chiet khau
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	public static final String MAX_AMOUNT_FREE = "MAX_AMOUNT_FREE";
	//public static final String MAX_QUANTITY_FREE_CK = "MAX_QUANTITY_FREE_CK";
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public static final String IS_OWNER = "IS_OWNER";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String STATUS = "STATUS";


	public static final String TABLE_NAME = "PO_CUSTOMER_PROMO_DETAIL";

	public PO_CUSTOMER_PROMO_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PO_CUSTOMER_PROMO_DETAIL_ID, PO_CUSTOMER_DETAIL_ID, PROGRAM_TYPE, PROGRAM_CODE, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				MAX_AMOUNT_FREE, IS_OWNER, SYN_STATE, STAFF_ID, PO_CUSTOMER_ID, PROGRAME_TYPE_CODE, SHOP_ID, ORDER_DATE, PRODUCT_GROUP_ID, GROUP_LEVEL_ID,STATUS
				/*,MAX_QUANTITY_FREE_CK*/
				};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		SaleOrderPromoDetailDTO promoDto = (SaleOrderPromoDetailDTO) dto;
		return insert(null, initDataRow(promoDto));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(SaleOrderPromoDetailDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
		if(dto.isOwner >=0)
			editedValues.put(IS_OWNER, dto.isOwner);
		editedValues.put(MAX_AMOUNT_FREE, dto.maxAmountFree);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(PROGRAM_TYPE, dto.programType);
		if(dto.poDetailId > 0) {
			editedValues.put(PO_CUSTOMER_DETAIL_ID, dto.poDetailId);
		}
		editedValues.put(PO_CUSTOMER_PROMO_DETAIL_ID, dto.poPromoDetailId);
		editedValues.put(PO_CUSTOMER_ID, dto.poId);
		editedValues.put(STAFF_ID, dto.staffId);
		if (!StringUtil.isNullOrEmpty(dto.programTypeCode)) {
			editedValues.put(PROGRAME_TYPE_CODE, dto.programTypeCode);
		}
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(ORDER_DATE, dto.orderDate);
		editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
		editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);

		return editedValues;
	}

	/**
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 08:18:37 26 Aug 2014
	 * @return: int
	 * @throws:
	 * @param saleOrderId
	 * @return
	 */
	public int deleteAllPromoDetailOfPO(long poId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(poId));

		return delete(PO_CUSTOMER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}
}
