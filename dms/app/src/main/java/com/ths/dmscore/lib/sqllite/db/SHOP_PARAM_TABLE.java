/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ShopParamDTO;

/**
 * cac thong tin dac biet ve shop (khai bao thoi gian cham cong, di tuyen)
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class SHOP_PARAM_TABLE extends ABSTRACT_TABLE {

	// id cua table
	public static final String SHOP_PARAM_ID = "SHOP_PARAM_ID";
	// id cua shop khai bao
	public static final String SHOP_ID = "SHOP_ID";
	// type param
	public static final String TYPE = "TYPE";
	// code param
	public static final String CODE = "CODE";
	// name param
	public static final String NAME = "NAME";
	// description
	public static final String DESCRIPTION = "DESCRIPTION";
	// status
	public static final String STATUS = "STATUS";
	// value
	public static final String VALUE = "VALUE";
	// create_date
	public static final String CREATE_DATE = "CREATE_DATE";
	// create_user
	public static final String CREATE_USER = "CREATE_USER";
	// update dayInOrder
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// update user
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String TABLE_NAME = "SHOP_PARAM";

	public SHOP_PARAM_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SHOP_PARAM_ID, SHOP_ID, TYPE, CODE, NAME,
				DESCRIPTION, STATUS, CREATE_DATE, CREATE_USER, UPDATE_DATE,
				UPDATE_USER };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 *
	 * get list time define header for table
	 *
	 * @param data
	 * @return
	 * @return: ArrayList<ActionLogDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 21, 2013
	 */
	public ArrayList<ShopParamDTO> getListTimeHeader(Bundle data) throws Exception{
		ArrayList<ShopParamDTO> listTimeDefine = new ArrayList<ShopParamDTO>();
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		//String strListShop = TextUtils.join(",", listShopId);
		boolean isHaveTimeHeader = false;
		for(String shop:listShopId){
			StringBuilder sqlRequest = new StringBuilder();
			ArrayList<String> params = new ArrayList<String>();
			sqlRequest.append("SELECT * ");
			sqlRequest.append("FROM   shop_param sp ");
			sqlRequest.append("WHERE  sp.status = 1 ");
			sqlRequest.append("       AND sp.CODE IN ('DT_START', 'DT_MIDDLE', 'DT_END' )");
			sqlRequest.append("       AND sp.shop_id = ?");
			params.add(shop);
			sqlRequest.append(" limit 3");

			Cursor c = null;
			try {
				c = rawQueries(sqlRequest.toString(), params);
				if (c != null) {
					if (c.moveToFirst()) {
//						isHaveTimeHeader = true;
						do {
							ShopParamDTO object = new ShopParamDTO();
							object.initObjectWithCursor(c);
							listTimeDefine.add(object);
						} while (c.moveToNext());
					}
				}
			} finally {
				if (c != null) {
					try {
						c.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			}
			// lay du 3 tham so di tuyen
			if(listTimeDefine.size() == 3) {
				isHaveTimeHeader = true;
			}
			if(isHaveTimeHeader){
				break;
			}
		}

		return listTimeDefine;
	}

	/**
	 * Lay ds params
	 *
	 * @author: Nguyen Thanh Dung
	 * @param ext
	 * @return
	 * @return: List<ApParamDTO>
	 * @throws:
	 */
	public List<ShopParamDTO> getListParamForTakeAttendance(Bundle ext) throws Exception{
		String shopId = ext.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		ArrayList<ShopParamDTO> listParam = new ArrayList<ShopParamDTO>();

		boolean isHaveParam = false;
		for (String shop : listShopId) {
			StringBuilder sqlRequest = new StringBuilder();
			ArrayList<String> params = new ArrayList<String>();
			sqlRequest.append("SELECT * ");
			sqlRequest.append("FROM   shop_param sp ");
			sqlRequest.append("WHERE  sp.status = 1 ");
			sqlRequest.append("       AND sp.CODE IN ('CC_START','CC_END', 'CC_DISTANCE' )");
			sqlRequest.append("       AND sp.shop_id = ? ");
			params.add(shop);
			sqlRequest.append(" limit 3");

			Cursor c = null;
			ShopParamDTO param1 = new ShopParamDTO();
			ShopParamDTO param2 = new ShopParamDTO();
			ShopParamDTO param3 = new ShopParamDTO();
			try {
				c = rawQueries(sqlRequest.toString(), params);
				if (c != null) {
					if (c.moveToFirst()) {
						isHaveParam = true;
						do {
							ShopParamDTO object = new ShopParamDTO();
							object.initObjectWithCursor(c);

							if (object.code.contains("CC_START") == true) {
								param1 = object;
							} else if (object.code.contains("CC_END")) {
								param2 = object;
							} else if (object.code.contains("CC_DISTANCE")) {
								param3 = object;
							}
						} while (c.moveToNext());
					}
				}
			} finally {
				if (c != null) {
					try {
						c.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}

				if(!isHaveParam){
					continue;
				}
			}

			listParam.add(param1);
			listParam.add(param2);
			listParam.add(param3);

			break;
		}

		return listParam;
	}

	/**
	 * shop param cho phep xoa vi tri
	 *
	 * @author: DungNX
	 * @param: Bundle
	 * @return: ApParamDTO
	 * @throws:
	 */
	public ShopParamDTO getShopParamAllowClearPosition(Bundle data) throws Exception {
		ShopParamDTO dto = new ShopParamDTO();
		dto.status = 1;// default la cho phep xoa vi tri
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> lstShop = new SHOP_TABLE(mDB).getShopRecursive(shopId);
		StringBuilder sqlRequest = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sqlRequest.append("SELECT * ");
		sqlRequest.append("FROM   shop_param sp ");
		sqlRequest.append("WHERE ");
		sqlRequest.append("       sp.type = 'ALLOW_CLEAR_POSITION'");
		sqlRequest.append("       AND sp.shop_id = ?");
		params.add(shopId);
		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					dto.initObjectWithCursor(c);
				}else{
					// truong hop ko co du lieu thi lay len shop cha
					if(lstShop.size() > 0){
						lstShop.remove(0);
					}
					if(lstShop.size() > 0){
						shopId = lstShop.get(0);
						data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
						dto = getShopParamAllowClearPosition(data);
					}
				}
			}
		}finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}
		return dto;
	}

	/**
	 * get shop config in SHOP_PARAM
	 * @author: duongdt3
	 * @since: 14:41:13 4 Apr 2015
	 * @return: List<ShopParamDTO>
	 * @throws:
	 * @param pShopId
	 * @param codes
	 * @return
	 * @throws Exception
	 */
	public List<ShopParamDTO> getShopParams(ArrayList<String> lstShopId, String ... pCodes) throws Exception{
		String strListShop = TextUtils.join(",", lstShopId);
		List<ShopParamDTO> dto = new ArrayList<ShopParamDTO>();
		if (lstShopId != null && !lstShopId.isEmpty() && pCodes != null && pCodes.length > 0) {
			//add 'TYPE' de tao list str
			for (int i = 0, size = pCodes.length; i < size; i++) {
				String code = pCodes[i];
				pCodes[i] = "'" + code + "'";
			}
			String strListCode = TextUtils.join(",", pCodes);

			//ArrayList<String> arrShopId = shopTable.getShopRecursive(shopId);
			StringBuilder sqlRequest = new StringBuilder();
			ArrayList<String> params = new ArrayList<String>();
			sqlRequest.append("	SELECT * FROM (");
			sqlRequest.append("		SELECT SHOP_PARAM_ID, SHOP_ID, TYPE, CODE, NAME, DESCRIPTION, STATUS, ");
			sqlRequest.append("			VALUE, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, (CASE  ");
			for (int i = 0, size = lstShopId.size(); i < size; i++) {
				String shopId = lstShopId.get(i);
				sqlRequest.append("		WHEN sp.shop_id = ? THEN " + i + " ");
				params.add(shopId);
			}
			sqlRequest.append("			END) STT_SHOP ");
			sqlRequest.append("		FROM   shop_param sp ");
			sqlRequest.append("		WHERE ");
			sqlRequest.append("      	1=1 and sp.CODE IN ( ");
			sqlRequest.append(strListCode);
			sqlRequest.append(" 		)");
			sqlRequest.append("      	AND sp.SHOP_ID IN ( ");
			sqlRequest.append(strListShop);
			sqlRequest.append(" 		)");
			//tranh trung 1 shop nhieu dong cung type
			sqlRequest.append(" 	GROUP BY sp.SHOP_ID, sp.CODE ");
			sqlRequest.append("	)	");
			sqlRequest.append("	ORDER BY CODE asc, STT_SHOP asc	");

			Cursor c = null;
			String typeCurrent = "";
			String typeWatingCurrent = "";
			try {
				ShopParamDTO waitingShopParamDto = null;
				c = rawQueries(sqlRequest.toString(), params);
				if (c != null) {
					if (c.moveToFirst()) {
						do {
							ShopParamDTO shopParamDto = new ShopParamDTO();
							shopParamDto.initObjectWithCursor(c);
							//new type
							if (!typeCurrent.equals(shopParamDto.code)) {
								//add waiting dto
								if (waitingShopParamDto != null && !typeWatingCurrent.equals(shopParamDto.code)) {
									dto.add(waitingShopParamDto);
									waitingShopParamDto = null;
								}

								//hop le
								if (shopParamDto.status != 0) {
									typeCurrent = shopParamDto.code;
									dto.add(shopParamDto);
									waitingShopParamDto = null;
								} else{
									typeWatingCurrent = shopParamDto.code;
									waitingShopParamDto = shopParamDto;
								}
							}
						} while (c.moveToNext());

						//add waiting dto
						if (waitingShopParamDto != null) {
							dto.add(waitingShopParamDto);
						}
					}
				}
			} finally {
				if (c != null) {
					try {
						c.close();
					} catch (Exception e2) {
					}
				}
			}
		}

		return dto;

	}
}
