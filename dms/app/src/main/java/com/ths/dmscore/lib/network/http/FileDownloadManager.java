package com.ths.dmscore.lib.network.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

import android.os.AsyncTask;
import android.os.Environment;

import com.ths.dmscore.util.MyLog;

public class FileDownloadManager extends AsyncTask<String, Integer, Long>{
	private static String fileName = "VNMILK";
	InputStream is;
	FileOutputStream fos;
	int count = 0;
	@Override
	protected Long doInBackground(String... params) {
		try {
	        URL url = new URL(params[0].toString());
	        HttpURLConnection c = (HttpURLConnection) url.openConnection();
	        c.setRequestMethod("GET");
	        c.setDoOutput(true);
	        c.connect();

	        String PATH = Environment.getExternalStorageDirectory()
	                + "/VNMDownload/";
	        
	        MyLog.v("FileDownloadManager", "PATH: " + PATH);
	        File file = new File(PATH);
	        file.mkdirs();
	        File outputFile = new File(file, fileName);
	        fos = new FileOutputStream(outputFile);

	        is = c.getInputStream();

	        byte[] buffer = new byte[1024];
	        int len = 0;
	        while ((len = is.read(buffer)) != -1) {
	            fos.write(buffer, 0, len);
	            count += len;
	            publishProgress(count);
	        }
	    } catch (FileNotFoundException e) {
	    	MyLog.d("FileDownloadManager", "FileNotFoundException: " + e);
	    } catch (IOException e) {
	        MyLog.d("FileDownloadManager", "IOException: " + e);
	    } catch (Exception e) {
	    	MyLog.d("FileDownloadManager", "Exception: " + e);
	    } finally{
	    	if(is != null){
	    		try {
					is.close();
				} catch (IOException e) {
					MyLog.e("FileDownloadManager", "fail", e);
				}
	    	}
	    	if(fos != null){
	    		try {
					fos.close();
				} catch (IOException e) {
					MyLog.e("FileDownloadManager", "fail", e);
				}
	    	}
	    }
		return (long) count;
	}

	
	@Override
	protected void onProgressUpdate(Integer... values) {
		MyLog.d("FileDownloadManager", "Downloaded : " + Arrays.toString(values));
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Long result) {
		MyLog.d("FileDownloadManager", "onPostExecute : " + result);
		super.onPostExecute(result);
	}
	
}
