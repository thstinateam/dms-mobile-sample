package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.RPT_CTTL_DETAIL_PAY_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class RptCttlDetailPayDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public long rptCttlDetailPayId;
	public long rptCttlPayId;
	public long productId;
	public double quantityPromotion;
	public long amountPromotion;
	public long staffId;
	public long shopId;
	public String createDate;
	public double quantityBegin;
	public long amountBegin;
	//Tich luy cua sp quy doi?
	public boolean isConvertProduct;

	/**
	 * gen json promotion detail
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:31:25 25 Aug 2014
	 * @return: Vector<?>
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonRptCttlDetailPay() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, RPT_CTTL_DETAIL_PAY_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.RPT_CTTL_DETAIL_PAY_ID, rptCttlDetailPayId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.RPT_CTTL_PAY_ID, rptCttlPayId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.PRODUCT_ID, productId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.QUANTITY_PROMOTION, quantityPromotion, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.AMOUNT_PROMOTION, amountPromotion, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.SHOP_ID, shopId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.STAFF_ID, staffId, null));
			params.put(GlobalUtil.getJsonColumn(RPT_CTTL_DETAIL_PAY_TABLE.CREATE_DATE, createDate, null));

			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * Khoi tao tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		rptCttlDetailPayId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.RPT_CTTL_DETAIL_PAY_ID);
		rptCttlPayId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.RPT_CTTL_PAY_ID);
		quantityPromotion = CursorUtil.getDouble(c, RPT_CTTL_DETAIL_PAY_TABLE.QUANTITY_PROMOTION);
		amountPromotion = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.AMOUNT_PROMOTION);
		productId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.PRODUCT_ID);
		createDate = CursorUtil.getString(c, RPT_CTTL_DETAIL_PAY_TABLE.CREATE_DATE);
		shopId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.SHOP_ID);
		staffId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_PAY_TABLE.STAFF_ID);
	}

	/**
	 * Clone ra 1 doi tuong co gia tri tuong tu
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param cDetailPay
	 */
	public RptCttlDetailPayDTO clone() {
		RptCttlDetailPayDTO result = new RptCttlDetailPayDTO();
		result.productId = productId;
		result.quantityPromotion = quantityPromotion;
		result.amountPromotion = amountPromotion;
		result.staffId = staffId;
		result.shopId = shopId;
		result.createDate = createDate;
		result.quantityBegin = quantityBegin;
		result.amountBegin = amountBegin;
		result.isConvertProduct = isConvertProduct;

		return result;
	}

	/**
	 * Reset gia tri da su dung
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void resetValueUsing() {
		quantityPromotion = 0;
		amountPromotion = 0;
	}
}