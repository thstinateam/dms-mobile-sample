/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.DisplayShopMapDTO;
import com.ths.dmscore.dto.view.DisplayProgrameModel;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;

/**
 * Luu CTTT ap dung cho NPP nao
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class DISPLAY_SHOP_MAP_TABLE extends ABSTRACT_TABLE {
	// id CTTB
	public static final String DISPLAY_PROGRAM_ID = "DISPLAY_PROGRAM_ID";
	// code CTTB
	public static final String DISPLAY_PROGRAM_CODE = "DISPLAY_PROGRAM_CODE";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// trang thai
	public static final String STATUS = "STATUS";
	// id bang
	public static final String DISPLAY_SHOP_MAP_ID = "DISPLAY_SHOP_MAP_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay cap nhat
	public static final String FROM_DATE = "FROM_DATE";
	// nguoi cap nhat
	public static final String TO_DATE = "TO_DATE";

	private static final String TABLE_DISPLAY_SHOP_MAP = "TABLE_DISPLAY_SHOP_MAP";

	public DISPLAY_SHOP_MAP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_DISPLAY_SHOP_MAP;
		this.columns = new String[] { DISPLAY_PROGRAM_ID, DISPLAY_PROGRAM_CODE, SHOP_ID, STATUS,
				DISPLAY_SHOP_MAP_ID, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((DisplayShopMapDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(DisplayShopMapDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		DisplayShopMapDTO disDTO = (DisplayShopMapDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.displayProgramId };
		return update(value, DISPLAY_PROGRAM_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(DISPLAY_PROGRAM_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		DisplayShopMapDTO cusDTO = (DisplayShopMapDTO) dto;
		String[] params = { "" + cusDTO.displayProgramId };
		return delete(DISPLAY_PROGRAM_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public DisplayShopMapDTO getRowById(String id) {
		DisplayShopMapDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(DISPLAY_PROGRAM_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private DisplayShopMapDTO initLogDTOFromCursor(Cursor c) {
		DisplayShopMapDTO dpDetailDTO = new DisplayShopMapDTO();

		dpDetailDTO.displayProgramId = (CursorUtil.getInt(c, DISPLAY_PROGRAM_ID));
		dpDetailDTO.displayProgrameCode = (CursorUtil.getString(c, DISPLAY_PROGRAM_CODE));
		dpDetailDTO.shopId = (CursorUtil.getInt(c, SHOP_ID));
		dpDetailDTO.status = (CursorUtil.getInt(c, STATUS));
		dpDetailDTO.displayShopMapId = (CursorUtil.getInt(c, DISPLAY_SHOP_MAP_ID));
		dpDetailDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dpDetailDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		dpDetailDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dpDetailDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dpDetailDTO.fromDate = (CursorUtil.getString(c, FROM_DATE));
		dpDetailDTO.toDate = (CursorUtil.getString(c, TO_DATE));

		return dpDetailDTO;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayPrdogrameLvDTO>
	 * @throws:
	 */
	public Vector<DisplayShopMapDTO> getAllRow() {
		Vector<DisplayShopMapDTO> v = new Vector<DisplayShopMapDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				DisplayShopMapDTO DisplayPrdogrameLvDTO;
				if (c.moveToFirst()) {
					do {
						DisplayPrdogrameLvDTO = initLogDTOFromCursor(c);
						v.addElement(DisplayPrdogrameLvDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow DisplayShopMapDTO", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(DisplayShopMapDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(DISPLAY_PROGRAM_ID, dto.displayProgramId);
		editedValues.put(DISPLAY_PROGRAM_CODE, dto.displayProgrameCode);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STATUS, dto.status);
		editedValues.put(DISPLAY_SHOP_MAP_ID, dto.displayShopMapId);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(FROM_DATE, dto.fromDate);
		editedValues.put(TO_DATE, dto.toDate);

		return editedValues;
	}

	/**
	 *
	 * Lay ds CTTB cho chuc nang hinh cua NVBH & cua KH cua NV
	 *
	 * @author: Nguyen Thanh Dung
	 * @param data
	 * @return
	 * @return: DisplayProgrameModel
	 * @throws:
	 */
	public DisplayProgrameModel getListDisplayProgrameImage(Bundle data) throws Exception {
		DisplayProgrameDTO orderJoinTableDTO = new DisplayProgrameDTO();
		boolean checkPagging = data.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		Cursor c = null;

		StringBuffer  var1 = new StringBuffer();
		var1.append("select ");
		var1.append("       DISTINCT ks.KS_ID      DISPLAY_PROGRAM_ID, ");
		var1.append("                ks.KS_CODE    DISPLAY_PROGRAM_CODE, ");
		var1.append("                ks.NAME       DISPLAY_PROGRAM_NAME ");
		var1.append("from ks ks, ks_shop_map ksm ");
		var1.append("where 1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		params.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");

		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM (" + var1.toString() + ") ";
		var1.append(" ORDER BY ks.KS_CODE");

		DisplayProgrameModel modelData = new DisplayProgrameModel();
		ArrayList<DisplayProgrameDTO> list = new ArrayList<DisplayProgrameDTO>();
		Cursor cTmp = null;
		try {
			// get total row first
			if (!checkPagging) {
				cTmp = rawQuery(getCountProductList, params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null && cTmp.moveToFirst()) {
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(var1.toString(), params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					do {
						orderJoinTableDTO = new DisplayProgrameDTO();
						orderJoinTableDTO.initDisplayProgrameObject(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 * Danh sach CHuong trinh CB cua shop trong man hinh danh sach hinh anh role GSNPP
	 * @author: hoanpd1
	 * @since: 15:35:10 13-04-2015
	 * @return: DisplayProgrameModel
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DisplayProgrameDTO> getListDisplayProgrameImageOfSupervisor(String shopId) throws Exception {
		ArrayList<DisplayProgrameDTO> listDisplayProgram = new ArrayList<DisplayProgrameDTO>();
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		Cursor c = null;

		StringBuffer  var1 = new StringBuffer();
		var1.append("select ");
		var1.append("       DISTINCT ks.KS_ID      DISPLAY_PROGRAM_ID, ");
		var1.append("                ks.KS_CODE    DISPLAY_PROGRAM_CODE, ");
		var1.append("                ks.NAME       DISPLAY_PROGRAM_NAME ");
		var1.append("from ks ks, ks_shop_map ksm ");
		var1.append("where 1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		var1.append("      and  ? <= ks.TO_CYCLE_ID");
		params.add(""+cycleId);
		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ORDER BY ks.KS_CODE");

		try {
			// end
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameDTO item = new DisplayProgrameDTO();
    					item.initDisplayProgrameObject(c);
    					listDisplayProgram.add(item);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return listDisplayProgram;
	}

	/**
	 * Lay danh sach CLB thuoc 3 chu ky: chu ky hien tai + 2 chu ky truoc
	 * @author: yennth16
	 * @since: 10:00:03 23-07-2015
	 * @return: ArrayList<DisplayProgrameDTO>
	 * @throws:
	 * @param shopId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<DisplayProgrameDTO> getListDisplayProgrameThreeCycle(String shopId) throws Exception {
		ArrayList<DisplayProgrameDTO> listDisplayProgram = new ArrayList<DisplayProgrameDTO>();
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTB.getShopParentByShopTypeRecursive(shopId);
		String idShopList = TextUtils.join(",", shopIdArray);
		long fromCycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), -2);
		long toCycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		Cursor c = null;

		StringBuffer  var1 = new StringBuffer();
		var1.append("select ");
		var1.append("       DISTINCT ks.KS_ID      DISPLAY_PROGRAM_ID, ");
		var1.append("                ks.KS_CODE    DISPLAY_PROGRAM_CODE, ");
		var1.append("                ks.NAME       DISPLAY_PROGRAM_NAME ");
		var1.append("from ks ks, ks_shop_map ksm ");
		var1.append("where 1 = 1 ");
		var1.append("      and ks.status = 1 ");
		var1.append("      AND  (( ? >= ks.FROM_CYCLE_ID  AND  ? <= ks.TO_CYCLE_ID )  ");
		params.add(""+fromCycleId);
		params.add(""+toCycleId);
		var1.append("      OR    ( ? <= ks.TO_CYCLE_ID and ks.TO_CYCLE_ID <= ? ) ");
		params.add(""+fromCycleId);
		params.add(""+toCycleId);
		var1.append("      OR    ( ? >= ks.FROM_CYCLE_ID and ks.FROM_CYCLE_ID>= ? )) ");
		params.add(""+toCycleId);
		params.add(""+fromCycleId);

//		var1.append("      and  ? >= ks.FROM_CYCLE_ID ");
//		params.add(""+fromCycleId);
//		var1.append("      and  ? <= ks.TO_CYCLE_ID");
//		params.add(""+toCycleId);

		var1.append("      and ksm.status = 1 ");
		var1.append("      and ks.KS_ID = ksm.KS_ID ");
		var1.append(" 	   and ksm.SHOP_ID IN ");
		var1.append("       (").append(idShopList).append(")");
		var1.append(" ORDER BY ks.KS_CODE");

		try {
			// end
			c = rawQueries(var1.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						DisplayProgrameDTO item = new DisplayProgrameDTO();
    					item.initDisplayProgrameObject(c);
    					listDisplayProgram.add(item);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return listDisplayProgram;
	}
}
