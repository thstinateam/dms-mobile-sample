package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.StaffPositionLogDTO;

@SuppressWarnings("serial")
public class StaffPositionDTO implements Serializable{
	public ArrayList<StaffPositionLogDTO> itemList;
	
	public StaffPositionDTO(){
		itemList = new ArrayList<StaffPositionLogDTO>();
	}
}
