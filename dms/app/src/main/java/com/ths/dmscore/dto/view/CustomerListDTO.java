package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomerListDTO implements Serializable {
	private static final long serialVersionUID = 6819581830458875596L;
	private int totalCustomer;
	private ArrayList<CustomerListItem> cusList;
	private double distance;
	private int curPage = -1;
	private List<EquipStatisticRecordDTO> listInventoryRecord = new ArrayList<EquipStatisticRecordDTO>();
	
	public CustomerListDTO() {
		setTotalCustomer(0);
		setCusList(new ArrayList<CustomerListItem>());
	}

	public int getTotalCustomer() {
		return totalCustomer;
	}

	public void setTotalCustomer(int totalCustomer) {
		this.totalCustomer = totalCustomer;
	}

	public ArrayList<CustomerListItem> getCusList() {
		return cusList;
	}

	public void setCusList(ArrayList<CustomerListItem> cusList) {
		this.cusList = cusList;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getCurPage() {
		return curPage;
	}

	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}

	public List<EquipStatisticRecordDTO> getListInventoryRecord() {
		return listInventoryRecord;
	}

	public void setListInventoryRecord(List<EquipStatisticRecordDTO> listInventoryRecord) {
		this.listInventoryRecord = listInventoryRecord;
	}
}
