/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;

public class HistoryItemDTO {
	public String STT;
	public String ngayBan;
	public Double amountPlan;
	public Double amountDone;
	public Double amountApproved;
	public Double amountPending;

	public long quantityPlan;
	public long quantityDone;
	public long quantityApproved;
	public long quantityPending;
	public String sku;
	public void initDateWithCursor(Cursor c) {
		sku = CursorUtil.getString(c, "SKU");
		amountDone = CursorUtil.getDouble(c, "AMOUNT",1, GlobalInfo.getInstance().getSysNumRounding());
		amountApproved = CursorUtil.getDouble(c, "AMOUNT_APPROVED",1, GlobalInfo.getInstance().getSysNumRounding());
		amountPending = CursorUtil.getDouble(c, "AMOUNT_PENDING",1, GlobalInfo.getInstance().getSysNumRounding());
		quantityDone = CursorUtil.getLong(c, "QUANTITY");
		quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
		quantityPending = CursorUtil.getLong(c, "QUANTITY_PENDING");
	}
}
