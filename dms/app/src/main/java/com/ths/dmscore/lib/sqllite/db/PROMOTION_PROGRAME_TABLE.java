/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.util.LongSparseArray;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PromotionProductConvDtlDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.RptCttlDTO;
import com.ths.dmscore.dto.db.RptCttlDetailDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.dto.db.PromotionProductConvertDTO;
import com.ths.dmscore.dto.db.PromotionProductOpenDTO;
import com.ths.dmscore.dto.view.PromotionProgrameModel;
import com.ths.dmscore.dto.view.TBHVPromotionProgrameDTO;

/**
 * Luu thong tin chuong trinh khuyen mai
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class PROMOTION_PROGRAME_TABLE extends ABSTRACT_TABLE {
	// id chuong trinh khuyen mai
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	// ma chuong trinh
	public static final String PROMOTION_PROGRAM_CODE = "PROMOTION_PROGRAM_CODE";
	// ten chuong trinh
	public static final String PROMOTION_PROGRAM_NAME = "PROMOTION_PROGRAM_NAME";
	// trang thai 1: hieu luc, 0: het hieu luc
	public static final String STATUS = "STATUS";
	// ma phan loai chuong trinh khuyen mai
	public static final String TYPE = "TYPE";
	// dang khuyen mai
	public static final String PRO_FORMAT = "PRO_FORMAT";
	// tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// den ngay
	public static final String TO_DATE = "TO_DATE";
	// quan he 1: or, 0: and
	public static final String RELATION = "RELATION";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// khao sat lai?
	public static final String CUSTOMER_TYPE = "CUSTOMER_TYPE";
	// khao sat lai?
	public static final String AREA_CODE = "AREA_CODE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// co tinh boi so khuyen mai hay khong? 1: co tinh, 0: khong tinh
	public static final String MULTIPLE = "MULTIPLE";
	// cho phep tinh toi uu hay khong? 1: co, 0: khong co
	public static final String RECURSIVE = "RECURSIVE";
	//Cho phep sua KM hay ko?
	public static final String IS_EDITED = "IS_EDITED";

	private static final String TABLE_PROMOTION_PROGRAME = "PROMOTION_PROGRAM";
	// mo ta chuong trinh khuyen mai
	private static final String DESCRIPTION = "DESCRIPTION";
	public static final String MD5_VALID_CODE = "MD5_VALID_CODE";

	public PROMOTION_PROGRAME_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_PROMOTION_PROGRAME;
		this.columns = new String[] { PROMOTION_PROGRAM_ID,
				PROMOTION_PROGRAM_CODE, PROMOTION_PROGRAM_NAME, STATUS, TYPE,
				PRO_FORMAT, FROM_DATE, TO_DATE, RELATION, SHOP_ID,
				CUSTOMER_TYPE, AREA_CODE, CREATE_USER, UPDATE_USER,
				CREATE_DATE, UPDATE_DATE, MULTIPLE, RECURSIVE, IS_EDITED, MD5_VALID_CODE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PromotionProgrameDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PromotionProgrameDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PromotionProgrameDTO disDTO = (PromotionProgrameDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.getPROMOTION_PROGRAM_ID() };
		return update(value, PROMOTION_PROGRAM_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(PROMOTION_PROGRAM_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PromotionProgrameDTO pro = (PromotionProgrameDTO) dto;
		String[] params = { "" + pro.getPROMOTION_PROGRAM_ID() };
		return delete(PROMOTION_PROGRAM_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public PromotionProgrameDTO getRowById(String id) {
		PromotionProgrameDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(PROMOTION_PROGRAM_ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public PromotionProgrameDTO getPromotionProgrameFromProgrameCode(
			String promotionCode) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(promotionCode);

		String sqlQuery = " Select distinct (promotion_program_id) as PROMOTION_PROGRAM_ID, "
				+ "PROMOTION_PROGRAM_CODE as PROMOTION_PROGRAM_CODE, PROMOTION_PROGRAM_NAME as PROMOTION_PROGRAM_NAME,"
				+ "  STATUS as STATUS,TYPE as TYPE, PRO_FORMAT as PRO_FORMAT, FROM_DATE as FROM_DATE, TO_DATE as TO_DATE, RELATION as RELATION, "
				+ "CUSTOMER_TYPE as CUSTOMER_TYPE , MULTIPLE as MULTIPLE , RECURSIVE as RECURSIVE "
				+ " From promotion_programe "
				+ " Where status = 1 and "
				+ " PROMOTION_PROGRAM_CODE = ? and "
				+ " IFNULL(DATE(FROM_DATE) <= DATE('NOW', 'localtime'),0) and "
				+ " (IFNULL(DATE(TO_DATE) >= DATE('NOW', 'localtime'),0) OR TO_DATE IS NULL OR TO_DATE = '') ";

		PromotionProgrameDTO dto = null;
		Cursor c = null;

		try {
			c = rawQuery(sqlQuery, params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTO(c);
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e) {
				}
			}
		}
		return dto;
	}

	/**
	 *
	 * Lay thong tin chuong trinh KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param promotionId
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws:
	 */
	public PromotionProgrameDTO getPromotionProgrameFromProgrameId(
			String promotionId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(promotionId);

		String sqlQuery = " Select distinct (PROMOTION_PROGRAM_ID) as PROMOTION_PROGRAM_ID, "
				+ "PROMOTION_PROGRAM_CODE as PROMOTION_PROGRAM_CODE, PROMOTION_PROGRAM_NAME as PROMOTION_PROGRAM_NAME,"
				+ "  STATUS as STATUS,TYPE as TYPE, PRO_FORMAT as PRO_FORMAT, FROM_DATE as FROM_DATE, TO_DATE as TO_DATE, RELATION as RELATION, "
				+ " MULTIPLE as MULTIPLE , RECURSIVE as RECURSIVE "
				+ " From PROMOTION_PROGRAM "
				+ " Where status = 1 and "
				+ " PROMOTION_PROGRAM_ID = ? and "
				+ " IFNULL(DATE(FROM_DATE) <= DATE('NOW', 'localtime'),0) and "
				+ " (IFNULL(DATE(TO_DATE) >= DATE('NOW', 'localtime'),0) OR TO_DATE IS NULL OR TO_DATE = '') ";
		//

		PromotionProgrameDTO dto = null;
		Cursor c = null;

		try {
			c = rawQuery(sqlQuery, params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTO(c);
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e) {
				}
			}
		}
		return dto;
	}

	private PromotionProgrameDTO initDTO(Cursor c) {
		PromotionProgrameDTO dpDetailDTO = new PromotionProgrameDTO();
		dpDetailDTO.setPROMOTION_PROGRAM_ID(CursorUtil.getInt(c, PROMOTION_PROGRAM_ID));
		dpDetailDTO.setPROMOTION_PROGRAM_CODE(CursorUtil.getString(c, PROMOTION_PROGRAM_CODE));
		dpDetailDTO.setPROMOTION_PROGRAM_NAME(CursorUtil.getString(c, PROMOTION_PROGRAM_NAME));
		dpDetailDTO.setSTATUS(CursorUtil.getInt(c, STATUS));
		dpDetailDTO.setTYPE((CursorUtil.getString(c, TYPE)));
		dpDetailDTO.setPRO_FORMAT((CursorUtil.getString(c, PRO_FORMAT)));
		dpDetailDTO.setFROM_DATE(CursorUtil.getString(c, FROM_DATE));
		dpDetailDTO.setTO_DATE(CursorUtil.getString(c, TO_DATE));
		dpDetailDTO.setIsEdited(CursorUtil.getInt(c, IS_EDITED));
		dpDetailDTO.setMD5_VALID_CODE(CursorUtil.getString(c, MD5_VALID_CODE));

		return dpDetailDTO;
	}

	/**
	 *
	 * khoi tao doi tuong CTKM co kiem tra CTKM co hop le theo policy moi hay
	 * khong
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws:
	 * @since: May 20, 2013
	 */
	private PromotionProgrameDTO initDTOHasCheckValidCTKM(Cursor c) {
		PromotionProgrameDTO resultObject = initDTO(c);
		//Check thuoc tinh dong
		if (resultObject != null) {
			int COKHAIBAO1 = 0;
			int COKHAIBAO2 = 0;
			int COKHAIBAO3 = 0;
			int COKHAIBAO4 = 0;
			int COKHAIBAO5 = 0;
			int COKHAIBAO6 = 0;
			int COKHAIBAO7 = 0;
			int COKHAIBAO8 = 0;
			int COKHAIBAO9 = 0;
			int COKHAIBAO10 = 0;

			COKHAIBAO1 = CursorUtil.getInt(c, "COKHAIBAO1");
			COKHAIBAO2 = CursorUtil.getInt(c, "COKHAIBAO2");
			COKHAIBAO3 = CursorUtil.getInt(c, "COKHAIBAO3");
			COKHAIBAO4 = CursorUtil.getInt(c, "COKHAIBAO4");
			COKHAIBAO5 = CursorUtil.getInt(c, "COKHAIBAO5");
			COKHAIBAO6 = CursorUtil.getInt(c, "COKHAIBAO6");
			COKHAIBAO7 = CursorUtil.getInt(c, "COKHAIBAO7");
			COKHAIBAO8 = CursorUtil.getInt(c, "COKHAIBAO8");
			COKHAIBAO9 = CursorUtil.getInt(c, "COKHAIBAO9");
			COKHAIBAO10 = CursorUtil.getInt(c, "COKHAIBAO10");
			boolean result = false;
			if (COKHAIBAO1 == 0) {
				result = true;
			} else {
				result = COKHAIBAO2 > 0 ? true : false;
			}
			if (result) {
				if (COKHAIBAO3 == 0) {
					result = true;
				} else {
					result = COKHAIBAO4 > 0 ? true : false;
				}
			} else {
				resultObject.setInvalid(false);
			}
			if (result) {
				boolean result1 = false;
				if (COKHAIBAO5 == 0) {
					result1 = true;
				} else {
					result1 = COKHAIBAO6 == COKHAIBAO5 ? true : false;
				}

				boolean result2 = false;
				if (COKHAIBAO7 == 0) {
					result2 = true;
				} else {
					result2 = COKHAIBAO8 == COKHAIBAO7 ? true : false;
				}


				boolean result3 = false;
				if (COKHAIBAO9 == 0) {
					result3 = true;
				} else {
					result3 = COKHAIBAO10 > 0 ? true : false;
				}
				result = result1 && result2 && result3;
			} else {
				resultObject.setInvalid(false);
			}
			resultObject.setInvalid(result);

			//CTKM co so suat hay khong?
			String quantityMaxShop = null;
			String quantityMaxStaff = null;
			String quantityMaxCustomer = null;
			//CTKM ko cho phep sua so suat
			int isQuantityMaxEdit = 0;
			quantityMaxShop = CursorUtil.getString(c, "QUANTITY_MAX_SHOP");
			quantityMaxStaff = CursorUtil.getString(c, "QUANTITY_MAX_STAFF");
			quantityMaxCustomer = CursorUtil.getString(c, "QUANTITY_MAX_CUSTOMER");
			isQuantityMaxEdit = CursorUtil.getInt(c, "IS_QUANTITY_MAX_EDIT");

			if (!StringUtil.isNullOrEmpty(quantityMaxShop)
					|| !StringUtil.isNullOrEmpty(quantityMaxStaff)
					|| !StringUtil.isNullOrEmpty(quantityMaxCustomer)
					|| isQuantityMaxEdit == 1
					) {
				resultObject.setHasQuantityReceived(true);
			} else {
				resultObject.setHasQuantityReceived(false);
			}

		}
		return resultObject;
	}

	/**
	 *
	 * parse data to check CTKM invalid ?
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return
	 * @return: boolean
	 * @throws:
	 * @since: May 24, 2013
	 */
//	public boolean checkPromotionProgramInvalid(Cursor c) {
//		boolean kq = false;
//		// lay cac bien de kiem tra san pham co thuoc CTKM hay khong
//		int COKHAIBAO1 = 0;
//		int COKHAIBAO2 = 0;
//		int COKHAIBAO3 = 0;
//		int COKHAIBAO4 = 0;
//		int COKHAIBAO5 = 0;
//		int COKHAIBAO6 = 0;
//		int COKHAIBAO7 = 0;
//		int COKHAIBAO8 = 0;
//		int COKHAIBAO9 = 0;
//		int COKHAIBAO10 = 0;
//
//		if (c.getColumnIndex("COKHAIBAO1") >= 0) {
//			COKHAIBAO1 = c.getInt(c.getColumnIndex("COKHAIBAO1"));
//		}
//		if (c.getColumnIndex("COKHAIBAO2") >= 0) {
//			COKHAIBAO2 = c.getInt(c.getColumnIndex("COKHAIBAO2"));
//		}
//		if (c.getColumnIndex("COKHAIBAO3") >= 0) {
//			COKHAIBAO3 = c.getInt(c.getColumnIndex("COKHAIBAO3"));
//		}
//		if (c.getColumnIndex("COKHAIBAO4") >= 0) {
//			COKHAIBAO4 = c.getInt(c.getColumnIndex("COKHAIBAO4"));
//		}
//		if (c.getColumnIndex("COKHAIBAO5") >= 0) {
//			COKHAIBAO5 = c.getInt(c.getColumnIndex("COKHAIBAO5"));
//		}
//		if (c.getColumnIndex("COKHAIBAO6") >= 0) {
//			COKHAIBAO6 = c.getInt(c.getColumnIndex("COKHAIBAO6"));
//		}
//		if (c.getColumnIndex("COKHAIBAO7") >= 0) {
//			COKHAIBAO7 = c.getInt(c.getColumnIndex("COKHAIBAO7"));
//		}
//		if (c.getColumnIndex("COKHAIBAO8") >= 0) {
//			COKHAIBAO8 = c.getInt(c.getColumnIndex("COKHAIBAO8"));
//		}
//		if (c.getColumnIndex("COKHAIBAO9") >= 0) {
//			COKHAIBAO9 = c.getInt(c.getColumnIndex("COKHAIBAO9"));
//		}
//		if (c.getColumnIndex("COKHAIBAO10") >= 0) {
//			COKHAIBAO10 = c.getInt(c.getColumnIndex("COKHAIBAO10"));
//		}
//		boolean result = false;
//		if (COKHAIBAO1 == 0) {
//			result = true;
//		} else {
//			result = COKHAIBAO2 > 0 ? true : false;
//		}
//		if (result) {
//			if (COKHAIBAO3 == 0) {
//				result = true;
//			} else {
//				result = COKHAIBAO4 > 0 ? true : false;
//			}
//		} else {
//			kq = false;
//		}
//		if (result) {
//			boolean result1 = false;
//			if (COKHAIBAO5 == 0) {
//				result1 = true;
//			} else {
//				result1 = COKHAIBAO6 == COKHAIBAO5 ? true : false;
//			}
//
//			boolean result2 = false;
//			if (COKHAIBAO7 == 0) {
//				result2 = true;
//			} else {
//				result2 = COKHAIBAO8 == COKHAIBAO7 ? true : false;
//			}
//			boolean result3 = false;
//			if (COKHAIBAO9 == 0) {
//				result3 = true;
//			} else {
//				result3 = COKHAIBAO10 > 0 ? true : false;
//			}
//			result = result1 && result2 && result3;
//		} else {
//			kq = false;
//		}
//		kq = result;
//		return kq;
//	}

	private PromotionProgrameDTO initDTOFromCursor(Cursor c) {
		PromotionProgrameDTO dpDetailDTO = new PromotionProgrameDTO();

		dpDetailDTO.setPROMOTION_PROGRAM_ID((CursorUtil.getInt(c, PROMOTION_PROGRAM_ID)));
		dpDetailDTO.setPROMOTION_PROGRAM_CODE((CursorUtil.getString(c, PROMOTION_PROGRAM_CODE)));
		dpDetailDTO.setPROMOTION_PROGRAM_NAME((CursorUtil.getString(c, PROMOTION_PROGRAM_NAME)));
		dpDetailDTO.setSTATUS((CursorUtil.getInt(c, STATUS)));
		dpDetailDTO.setTYPE((CursorUtil.getString(c, TYPE)));
		dpDetailDTO.setPRO_FORMAT((CursorUtil.getString(c, PRO_FORMAT)));

		dpDetailDTO.setFROM_DATE((CursorUtil.getString(c, FROM_DATE)));
		dpDetailDTO.setTO_DATE((CursorUtil.getString(c, TO_DATE)));
		dpDetailDTO.setRELATION((CursorUtil.getInt(c, RELATION)));
		dpDetailDTO.setSHOP_ID((CursorUtil.getInt(c, SHOP_ID)));

		dpDetailDTO.setCREATE_USER((CursorUtil.getString(c, CREATE_USER)));
		dpDetailDTO.setUPDATE_USER((CursorUtil.getString(c, UPDATE_USER)));
		dpDetailDTO.setCREATE_DATE((CursorUtil.getString(c, CREATE_DATE)));
		dpDetailDTO.setUPDATE_DATE((CursorUtil.getString(c, UPDATE_DATE)));
		dpDetailDTO.setMULTIPLE((CursorUtil.getInt(c, MULTIPLE)));
		dpDetailDTO.setIsEdited(CursorUtil.getInt(c, IS_EDITED));

		return dpDetailDTO;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayPrdogrameLvDTO>
	 * @throws:
	 */
	public Vector<PromotionProgrameDTO> getAllRow() {
		Vector<PromotionProgrameDTO> v = new Vector<PromotionProgrameDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		}
		if (c != null) {
			PromotionProgrameDTO DisplayPrdogrameLvDTO;
			if (c.moveToFirst()) {
				do {
					DisplayPrdogrameLvDTO = initDTOFromCursor(c);
					v.addElement(DisplayPrdogrameLvDTO);
				} while (c.moveToNext());
			}
		}
		try {
			if (c != null) {
				c.close();
			}
		} catch (Exception e2) {
			// TODO: handle exception
		}
		return v;

	}

	private ContentValues initDataRow(PromotionProgrameDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(PROMOTION_PROGRAM_ID, dto.getPROMOTION_PROGRAM_ID());
		editedValues.put(PROMOTION_PROGRAM_CODE, dto.getPROMOTION_PROGRAM_CODE());
		editedValues.put(PROMOTION_PROGRAM_NAME, dto.getPROMOTION_PROGRAM_NAME());
		editedValues.put(STATUS, dto.getSTATUS());
		editedValues.put(TYPE, dto.getTYPE());
		editedValues.put(PRO_FORMAT, dto.getPRO_FORMAT());

		editedValues.put(FROM_DATE, dto.getFROM_DATE());
		editedValues.put(TO_DATE, dto.getTO_DATE());
		editedValues.put(RELATION, dto.getRELATION());
		editedValues.put(SHOP_ID, dto.getSHOP_ID());
		editedValues.put(CREATE_USER, dto.getCREATE_USER());
		editedValues.put(UPDATE_USER, dto.getUPDATE_USER());
		editedValues.put(CREATE_DATE, dto.getCREATE_DATE());
		editedValues.put(UPDATE_DATE, dto.getUPDATE_DATE());
		editedValues.put(MULTIPLE, dto.getMULTIPLE());
		editedValues.put(IS_EDITED, dto.getIsEdited());

		return editedValues;
	}


	/**
	 *
	 * Lay ds chuong trinh KM cua GSNPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param staffId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: PromotionProgrameModel
	 * @throws:
	 */
	public PromotionProgrameModel getListSupervisorPromotionPrograme(Bundle data) throws Exception {
		boolean checkLoadMore = data.getBoolean(IntentConstants.INTENT_CHECK_PAGGING);
		int staffOwnerId = data.getInt(IntentConstants.INTENT_PARENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String ext = data.getString(IntentConstants.INTENT_PAGE);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}


		STAFF_TABLE tbStaff = new STAFF_TABLE(mDB);
		String listStaffId = tbStaff.getListStaffOfSupervisor(String.valueOf(staffOwnerId), shopId);
		ArrayList<String> params = new ArrayList<String>();

		StringBuilder sqlQuery = new StringBuilder();
//		sqlQuery.append("SELECT DISTINCT PP.promotion_program_code, ");
//		sqlQuery.append("       PP.promotion_program_name, ");
//		sqlQuery.append("       Strftime('%d/%m/%Y', PSM.from_date) AS FROM_DATE, ");
//		sqlQuery.append("       Strftime('%d/%m/%Y', PSM.to_date)   AS TO_DATE, ");
//		sqlQuery.append("       PP.description ");
//		sqlQuery.append("FROM   promotion_program PP, ");
//		sqlQuery.append("       promotion_shop_map PSM ");
//		sqlQuery.append("WHERE  Ifnull (Date(PP.to_date) >= Date('now', 'localtime'), 1) ");
//		sqlQuery.append("       AND PP.status = 1 AND PSM.status = 1 ");
//		sqlQuery.append("	AND PSM.SHOP_ID IN ( ");
//		sqlQuery.append(idListString);
//		sqlQuery.append(") ");
//		sqlQuery.append("       AND PSM.promotion_program_id = PP.promotion_program_id ");
//		sqlQuery.append("       AND Ifnull(Date(PSM.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");
//		sqlQuery.append("ORDER  BY PSM.from_date DESC, CASE WHEN PSM.to_date IS NULL THEN 1 ELSE 0 END,");
//		sqlQuery.append("          PSM.to_date, ");
//		sqlQuery.append("          PP.promotion_program_code, ");
//		sqlQuery.append("          PP.promotion_program_name ");

		sqlQuery.append("	SELECT	");
		sqlQuery.append("	    PP.promotion_program_code,	");
		sqlQuery.append("	    PP.promotion_program_name,	");
		sqlQuery.append("	    Strftime('%d/%m/%Y',	");
		sqlQuery.append("	    PSM.from_date) AS FROM_DATE,	");
		sqlQuery.append("	    Strftime('%d/%m/%Y',	");
		sqlQuery.append("	    PSM.to_date)   AS TO_DATE,	");
		sqlQuery.append("	    PP.description	");
		sqlQuery.append("	FROM	");
		sqlQuery.append("	    promotion_program PP,	");
		sqlQuery.append("	    promotion_shop_map PSM	");
		sqlQuery.append("	LEFT JOIN	");
		sqlQuery.append("	    promotion_staff_map pstm	");
		sqlQuery.append("	        ON pstm.promotion_shop_map_id = psm.promotion_shop_map_id	");
		sqlQuery.append("	WHERE	");
		sqlQuery.append("	    PP.status = 1	");
		sqlQuery.append("	    AND PSM.status = 1	");
		sqlQuery.append("	    AND PSM.SHOP_ID IN (	");
		sqlQuery.append(idShopList);
		sqlQuery.append("	    )	");
		sqlQuery.append("	    AND PSM.promotion_program_id = PP.promotion_program_id	");
		sqlQuery.append("	    AND (	");
		sqlQuery.append("	        CASE	");
		sqlQuery.append("	            WHEN (	");
		sqlQuery.append("	                SELECT	");
		sqlQuery.append("	                    Count(1)	");
		sqlQuery.append("	                FROM	");
		sqlQuery.append("	                    promotion_staff_map pstmm	");
		sqlQuery.append("	                WHERE	");
		sqlQuery.append("	                    pstmm.promotion_shop_map_id = PSM.promotion_shop_map_id	");
		sqlQuery.append("	                    AND pstmm.status = 1	");
		sqlQuery.append("	            ) = 0              THEN 1	");
		sqlQuery.append("	            ELSE pstm.staff_id in (	");
		sqlQuery.append(listStaffId);
		sqlQuery.append("	            )	");
		sqlQuery.append("	            AND pstm.status = 1	");
		sqlQuery.append("	        END	");
		sqlQuery.append("	    )	");
		sqlQuery.append("	    AND Ifnull(Date(PSM.to_date) >= Date('NOW', 'LOCALTIME'), 1)	");
		sqlQuery.append("	    AND Ifnull (Date(PP.to_date) >= Date('now', 'localtime'), 1)	");

		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
		defaultOrderByStr.append("	ORDER  BY	");
		defaultOrderByStr.append("	    PSM.from_date DESC,	");
		defaultOrderByStr.append("	    CASE	");
		defaultOrderByStr.append("	        WHEN PP.to_date IS NULL THEN 1	");
		defaultOrderByStr.append("	        ELSE 0	");
		defaultOrderByStr.append("	    END,	");
		defaultOrderByStr.append("	    PSM.to_date,	");
		defaultOrderByStr.append("	    PP.promotion_program_code,	");
		defaultOrderByStr.append("	    PP.promotion_program_name	");

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "PP.promotion_program_code")
				.addMapper(SortActionConstants.NAME, "PP.promotion_program_name")
				.addMapper(SortActionConstants.DATE, "PSM.from_date")
				.defaultOrderString(defaultOrderByStr.toString())
				.build(sortInfo);
				//add order string
		sqlQuery.append(orderByStr);

		String queryGetListProductForOrder = sqlQuery.toString();
		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ queryGetListProductForOrder + ") ";

		PromotionProgrameModel modelData = new PromotionProgrameModel();
		List<PromotionProgrameDTO> list = new ArrayList<PromotionProgrameDTO>();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			// get total row first
			if (checkLoadMore == false) {
				cTmp = rawQuery(getCountProductList,
						params.toArray(new String[params.size()]));
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			c = rawQuery(queryGetListProductForOrder + ext,
					params.toArray(new String[params.size()]));

			if (c != null) {

				if (c.moveToFirst()) {
					PromotionProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this
								.initPromotionProgrameObjectFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 *
	 * Lay ds chuong trinh KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopId
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: PromotionProgrameModel
	 * @throws:
	 */
	public TBHVPromotionProgrameDTO getListTBHVPromotionPrograme(String shopId,
			String ext, boolean checkLoadMore) throws Exception {

		Cursor c = null;
		Cursor cTmp = null;
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT DISTINCT PP.promotion_program_code, ");
		sqlQuery.append("                PP.promotion_program_name, ");
		sqlQuery.append("                Strftime('%d/%m/%Y', PP.from_date) AS FROM_DATE, ");
		sqlQuery.append("                Strftime('%d/%m/%Y', PP.to_date)   AS TO_DATE, ");
		sqlQuery.append("                Trim(PP.description)               AS DESCRIPTION ");
		sqlQuery.append("FROM   promotion_program PP, ");
		sqlQuery.append("       promotion_shop_map PSM ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND PP.status = 1 ");
		sqlQuery.append("       AND PSM.status = 1 ");
		sqlQuery.append("       AND Ifnull(Date(PSM.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");
		sqlQuery.append("       AND Ifnull(Date(PP.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");
		sqlQuery.append("       AND PSM.promotion_program_id = PP.promotion_program_id ");
		sqlQuery.append("       AND PSM.shop_id IN ( ");
		sqlQuery.append(idShopList + ") ");

		// String queryGetListProductForOrder = sqlQuery.toString();
		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ sqlQuery.toString() + ") ";
		sqlQuery.append("GROUP  BY PP.promotion_program_code, ");
		sqlQuery.append("          PP.promotion_program_name, ");
		sqlQuery.append("          PP.from_date, ");
		sqlQuery.append("          PP.to_date ");
		sqlQuery.append("ORDER  BY PP.from_date DESC, CASE WHEN PP.to_date IS NULL THEN 1 ELSE 0 END,");
		sqlQuery.append("          PP.to_date, ");
		sqlQuery.append("          PP.promotion_program_code, ");
		sqlQuery.append("          PP.promotion_program_name ");

		// ArrayList<String> params = new ArrayList<String>();
		String[] params = new String[] {};
		// params.add("");

		TBHVPromotionProgrameDTO modelData = new TBHVPromotionProgrameDTO();
		List<PromotionProgrameDTO> list = new ArrayList<PromotionProgrameDTO>();
		modelData.setModelData(list);
		try {
			// get total row first
			if (checkLoadMore == false) {
				// cTmp = rawQuery(getCountProductList, params.toArray(new
				// String[params.size()]));
				cTmp = this.rawQuery(getCountProductList.toString(), params);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
			// end
			// c = rawQuery(queryGetListProductForOrder + ext,
			// params.toArray(new String[params.size()]));
			c = this.rawQuery(sqlQuery.toString() + " " + ext, params);

			if (c != null) {
				if (c.moveToFirst()) {
					PromotionProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this
								.initPromotionProgrameObjectFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				 if (cTmp != null) {
				 cTmp.close();
				 }
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return modelData;
	}

	/**
	 *
	 * Khoi tao doi tuong chuong trinh KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws:
	 */
	private PromotionProgrameDTO initPromotionProgrameObjectFromGetStatement(
			Cursor c) {
		PromotionProgrameDTO dto = new PromotionProgrameDTO();
		dto.setPROMOTION_PROGRAM_ID(CursorUtil.getInt(c, PROMOTION_PROGRAM_ID));
		dto.setPROMOTION_PROGRAM_CODE(CursorUtil.getString(c, PROMOTION_PROGRAM_CODE));
		dto.setPROMOTION_PROGRAM_NAME(CursorUtil.getString(c, PROMOTION_PROGRAM_NAME));
		dto.setFROM_DATE(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, FROM_DATE)));
		dto.setTO_DATE(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, TO_DATE)));
		if (dto.getTO_DATE() == null) {
			dto.setTO_DATE("");
		}
		// DESCRIPTION
		dto.setDESCRIPTION(CursorUtil.getString(c, DESCRIPTION));

		return dto;
	}

	/**
	 *
	 * get promotion programe detail
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws:
	 */
	public ArrayList<PromotionProgrameDTO> getPromotionProgrameDetail(Bundle data) throws Exception {
		ArrayList<PromotionProgrameDTO> lstPromotion = new ArrayList<PromotionProgrameDTO>();
//		String promotionCode = data
//				.getString(IntentConstants.INTENT_PROMOTION_CODE);
		String promotionId = data
				.getString(IntentConstants.INTENT_PROMOTION_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		String strListShop = TextUtils.join(",", listShopId);

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT pp.promotion_program_code                AS PROMOTION_PROGRAM_CODE, ");
		sql.append("       pp.promotion_program_name                AS PROMOTION_PROGRAM_NAME, ");
		sql.append("       type                                     AS TYPE, ");
		sql.append("       Strftime('%d/%m/%Y', Date(psm.from_date)) AS FROM_DATE, ");
		sql.append("       Strftime('%d/%m/%Y', Date(psm.to_date))   AS TO_DATE, ");
		sql.append("       pp.description                           AS DESCRIPTION ");
		sql.append("FROM   promotion_program pp, ");
		sql.append("       promotion_shop_map psm ");
		sql.append("WHERE  pp.promotion_program_id In ( ");
		sql.append(promotionId);
		sql.append(" ) ");
//		sql.append("       AND pp.status = 1 ");
		sql.append("       AND pp.promotion_program_id = psm.promotion_program_id ");
		sql.append("       AND psm.shop_id IN ( ");
		sql.append(strListShop + ") ");
		sql.append("GROUP BY pp.promotion_program_id ");
//		sql.append("       AND psm.status = 1 ");
//		sql.append("       AND dayInOrder(psm.from_date) <= dayInOrder('now', 'localtime') ");
//		sql.append("       AND Ifnull(Date(psm.to_date) >= Date('NOW', 'LOCALTIME'), 1) ");


		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), null);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						PromotionProgrameDTO promotionDetail = new PromotionProgrameDTO();
						promotionDetail
								.initDataForRequestGetPromotionProgrameDetail(c);
						lstPromotion.add(promotionDetail);
					} while (c.moveToNext());

				}
			}

		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return lstPromotion;
	}

	/**
	 *
	 * Lay id cua chuong trinh khuyen mai
	 *
	 * @author: Nguyen Thanh Dung
	 * @param productId
	 *            : ma san pham
	 * @param shopId
	 *            : id cua shop
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long getPromotionProgramID(String productId, String shopId) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("SELECT   distinct(pp.promotion_program_id) ");
		sqlQuery.append(" FROM   promotion_program pp, PROMOTION_PROGRAM_DETAIL ppd ");
		sqlQuery.append(" WHERE 1 = 1");
		sqlQuery.append(" and pp.promotion_program_id = ppd.promotion_program_id");
		sqlQuery.append(" and pp.shop_id = ?");
		sqlQuery.append(" and ppd.product_id = ?");
		sqlQuery.append(" and pp.status = 1");
		sqlQuery.append(" and dayInOrder(pp.from_date) <= dayInOrder('now','localtime')");
		sqlQuery.append(" and (pp.to_date is null or dayInOrder(pp.to_date) >= dayInOrder('now', 'localtime'))");

		ArrayList<String> params = new ArrayList<String>();
		params.add(shopId);
		params.add(productId);

		long promotionProgramId = 0;
		Cursor c = null;
		try {
			c = rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					promotionProgramId = CursorUtil.getLong(c, "PROMOTION_PROGRAM_ID");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return promotionProgramId;
	}

	/**
	 *
	 * lay doi tuong CTKM voi co them cac thuoc tinh dung de check CTKM co hop
	 * le hay khong
	 *
	 * @author: HaiTC3
	 * @param productId
	 * @param idListShop
	 * @param customerId
	 * @param customerTypeId
	 * @return
	 * @return: PromotionProgrameDTO
	 * @throws Exception
	 * @throws:
	 * @since: May 20, 2013
	 */
//	public PromotionProgrameDTO getPromotionObjectWithCheckInvalid(
//			String productId, String idListShop, String customerId,
//			String customerTypeId, String staffId) {
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
//		PromotionProgrameDTO promotionObject = new PromotionProgrameDTO();
//		StringBuffer sql = new StringBuffer();
//		ArrayList<String> params = new ArrayList<String>();
//		sql.append("SELECT DISTINCT pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
//		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
//		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
//		sql.append("                pp.status                      STATUS, ");
//		sql.append("                pp.type                        TYPE, ");
//		sql.append("                pp.pro_format                  PRO_FORMAT, ");
//		sql.append("                pp.from_date                   FROM_DATE, ");
//		sql.append("                pp.to_date                     TO_DATE, ");
//		sql.append("                pp.is_edited                   IS_EDITED, ");
////		sql.append("                pp.relation                    RELATION, ");
////		sql.append("                pp.multiple                    MULTIPLE, ");
////		sql.append("                pp.recursive                   RECURSIVE, ");
//		//Thuoc tinh dong loai khach hang
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca ");
//		sql.append("                                            WHERE  pca.object_type = 2 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  1 = 1 ");
//		sql.append("                                                   AND pca.object_type = 2 ");
//		sql.append("                                                   AND pcad.object_id = ? ");
//		params.add(customerTypeId);
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
//		//Thuoc tinh dong muc doanh so
//		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca ");
//		sql.append("                                            WHERE  pca.object_type = 3 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad, ");
//		sql.append("                                                   sale_level_cat slc, ");
//		sql.append("                                                   customer_cat_level ccl ");
//		sql.append("                                            WHERE  1 = 1 ");
//		sql.append("                                                   AND pca.object_type = 3 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND slc.status = 1 ");
//		sql.append("                                                   AND ccl.customer_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
//		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
//		//Thuoc tinh dong cac gia tri don le: 1: chuoi, 2: so, 3: ngay
//		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND ( CASE ");
//		sql.append("                                                           WHEN AT.value_type = 2 THEN atv.value IS NOT NULL AND Ifnull(Cast(atv.value AS INTEGER) >= pca.from_value, 1) ");
//		sql.append("                                                                                       AND Ifnull(Cast(atv.value AS INTEGER) <= pca.to_value, 1) ");
//		sql.append("                                                           WHEN AT.value_type = 1 THEN Trim(atv.value) = Trim(pca.from_value) ");
//		sql.append("                                                           WHEN AT.value_type = 3 THEN atv.value IS NOT NULL ");
//		sql.append("                                                                                       AND Ifnull(Date(atv.value) >= Date(pca.from_value), 1) ");
//		sql.append("                                                                                       AND Ifnull(Date(atv.value) <= Date(pca.to_value), 1) ");
//		sql.append("                                                         END ) ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
//		//Thuoc tinh dong dang select
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type = 4 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv, ");
//		sql.append("                                                   attribute_detail atd, ");
//		sql.append("                                                   attribute_value_detail atvd, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND atd.status = 1 ");
//		sql.append("                                                   AND atvd.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND AT.value_type = 4 ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                                   AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
//		//Thuoc tinh dong dang multi select
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type = 5 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv, ");
//		sql.append("                                                   attribute_detail atd, ");
//		sql.append("                                                   attribute_value_detail atvd, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND atd.status = 1 ");
//		sql.append("                                                   AND atvd.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND AT.value_type = 5 ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                                   AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");
//		//Check customer map & staff map
//		sql.append("                                                   ,(SELECT Count(1) ");
//		sql.append("                                                       FROM promotion_staff_map pstmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//		sql.append("                                                   (SELECT Count(1)                      ");
//		sql.append("                                                       FROM promotion_staff_map pstmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//		params.add(staffId);
//		sql.append("                                                   (SELECT Count(1) ");
//		sql.append("                                                       FROM promotion_customer_map pcmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//		sql.append("                                                   (SELECT Count(1) ");
//		sql.append("                                                        FROM promotion_customer_map pcmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//		params.add(customerId);
////		sql.append("FROM   promotion_program pp, ");
//		sql.append("       FROM       (select * from promotion_program ");
//		sql.append("                      where 1 = 1 ");
//		sql.append("                      AND substr(from_date, 1, 10) <= ? ");
//		sql.append("                      AND Ifnull (substr(to_date, 1, 10) >= ?, 1) ");
//		sql.append("                      and status = 1 ");
//		sql.append("                      and type not in ('ZV23') ");
//		sql.append("                      )pp,  ");
//		params.add(dateNow);
//		params.add(dateNow);
//		sql.append("			PRODUCT_GROUP PG,	");
//		sql.append("			GROUP_LEVEL GLP,	");
//		sql.append("			GROUP_LEVEL GLC,	");
//		sql.append("			GROUP_LEVEL_DETAIL GLD,	");
//		sql.append("       promotion_shop_map psm ");
////		sql.append("       LEFT JOIN promotion_customer_map pcm ");
////		sql.append("              ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
////		sql.append("       LEFT JOIN promotion_staff_map pstm ");
////		sql.append("              ON pstm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("		WHERE	");
//		sql.append("			1 = 1	");
//		sql.append("			AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID	");
//		sql.append("			AND PG.PRODUCT_GROUP_ID = GLP.PRODUCT_GROUP_ID	");
//		sql.append("			AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
//		sql.append("			AND GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
//		sql.append("			AND PG.GROUP_TYPE = 1	");
//		sql.append("			AND GLP.HAS_PRODUCT = 1	");
//		sql.append("			AND GLC.HAS_PRODUCT = 1	");
//		sql.append("			AND PP.STATUS = 1	");
//		sql.append("			AND PG.STATUS = 1	");
//		sql.append("			AND GLP.STATUS = 1	");
//		sql.append("			AND GLC.STATUS = 1	");
//		sql.append("			AND GLD.STATUS = 1	");
//		sql.append("       AND GLD.product_id = ? ");
//		params.add(productId);
//		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
//		sql.append("       AND psm.shop_id IN ( " + idListShop);
//		sql.append("       ) ");
//		sql.append("       AND psm.status = 1 ");
//		sql.append("       AND substr(psm.from_date, 1, 10) <= ? ");
//		sql.append("       AND Ifnull (substr(psm.to_date, 1, 10) >= ?, 1) ");
//		sql.append("       AND substr(pp.from_date, 1, 10) <= ? ");
//		sql.append("       AND Ifnull (substr(pp.to_date, 1, 10) >= ?, 1) ");
//		params.add(dateNow);
//		params.add(dateNow);
//		params.add(dateNow);
//		params.add(dateNow);
//
//		//Lay ds CT co KM loai KH cua KH
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO1 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO2 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM chuoi, so ngay
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO3 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO4 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM chuoi, so ngay
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO5 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO6 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM dang select
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO7 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO8 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM dang select
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO9 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO10 > 0 ");
//		sql.append("              END) ");
//
//		//Check staff map & customer map
//		sql.append("              AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//		sql.append("              AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//
////		sql.append("       AND ( CASE ");
////		sql.append("               WHEN (SELECT Count(1) ");
////		sql.append("                     FROM   promotion_customer_map pcmm ");
////		sql.append("                     WHERE  pcmm.promotion_shop_map_id = ");
////		sql.append("                            psm.promotion_shop_map_id and pcmm.status = 1) = 0 ");
////		sql.append("                 THEN 1 ");
////		sql.append("               ELSE pcm.customer_id = ? AND pcm.status = 1 END ) ");
////		params.add(customerId);
////		sql.append("       AND ( CASE ");
////		sql.append("               WHEN (SELECT Count(1) ");
////		sql.append("                     FROM   promotion_staff_map pstmm ");
////		sql.append("                     WHERE  pstmm.promotion_shop_map_id = psm.promotion_shop_map_id and pstmm.status = 1) = 0 ");
////		sql.append("               THEN 1 ");
////		sql.append("               ELSE pstm.staff_id = ? AND pstm.status = 1 END ) ");
////		params.add(staffId);
//
//		Cursor c = null;
//		try {
//			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
//			if (c != null) {
//				if (c.moveToFirst()) {
//					promotionObject = initDTOHasCheckValidCTKM(c);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			try {
//				if (c != null) {
//					c.close();
//				}
//			} catch (Exception e2) {
//				// TODO: handle exception
//			}
//		}
//		return promotionObject;
//	}

	public ArrayList<LongSparseArray<ArrayList<PromotionProgrameDTO>>> getArrPromotionObjectWithCheckInvalid(
			String productId, String idListShop, String customerId,
			String customerTypeId, String staffId, String orderType) throws Exception {
		ArrayList<LongSparseArray<ArrayList<PromotionProgrameDTO>>> lstPromotion = new ArrayList<LongSparseArray<ArrayList<PromotionProgrameDTO>>>();
		LongSparseArray<ArrayList<PromotionProgrameDTO>> arPromo = new LongSparseArray<ArrayList<PromotionProgrameDTO>>();
		LongSparseArray<ArrayList<PromotionProgrameDTO>> arPromoClone = new LongSparseArray<ArrayList<PromotionProgrameDTO>>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT 			pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
		sql.append("                pp.status                      STATUS, ");
		sql.append("                pp.type                        TYPE, ");
//		sql.append("                pp.pro_format                  PRO_FORMAT, ");
//		sql.append("                pp.from_date                   FROM_DATE, ");
//		sql.append("                pp.to_date                     TO_DATE, ");
		sql.append("                pp.is_edited                   IS_EDITED, ");
		sql.append("                pp.md5_valid_code              MD5_VALID_CODE, ");
		sql.append("                GLD.product_id                 PRODUCT_ID, ");
		sql.append("                PP.COKHAIBAO1       AS COKHAIBAO1 ");
		sql.append("                ,PP.COKHAIBAO2       AS COKHAIBAO2 ");
		sql.append("                ,PP.COKHAIBAO3       AS COKHAIBAO3 ");
		sql.append("                ,PP.COKHAIBAO4       AS COKHAIBAO4 ");
		sql.append("                ,PP.COKHAIBAO5       AS COKHAIBAO5 ");
		sql.append("                ,PP.COKHAIBAO6       AS COKHAIBAO6 ");
		sql.append("                ,PP.COKHAIBAO7       AS COKHAIBAO7 ");
		sql.append("                ,PP.COKHAIBAO8       AS COKHAIBAO8 ");
		sql.append("                ,PP.COKHAIBAO9       AS COKHAIBAO9 ");
		sql.append("                ,PP.COKHAIBAO10       AS COKHAIBAO10 ");
		sql.append("                ,PP.QUANTITY_MAX_SHOP       AS QUANTITY_MAX_SHOP ");
		sql.append("                ,PP.QUANTITY_MAX_STAFF       AS QUANTITY_MAX_STAFF ");
		sql.append("                ,PP.QUANTITY_MAX_CUSTOMER       AS QUANTITY_MAX_CUSTOMER ");
		sql.append("                ,PP.IS_QUANTITY_MAX_EDIT       AS IS_QUANTITY_MAX_EDIT ");

		sql.append("                FROM ");

		sql.append("(SELECT 		pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
		sql.append("                pp.status                      STATUS, ");
		sql.append("                pp.type                        TYPE, ");
//		sql.append("                pp.pro_format                  PRO_FORMAT, ");
//		sql.append("                pp.from_date                   FROM_DATE, ");
//		sql.append("                pp.to_date                     TO_DATE, ");
		sql.append("                pp.is_edited                   IS_EDITED, ");
		sql.append("                pp.md5_valid_code              MD5_VALID_CODE, ");
		sql.append("                psm.QUANTITY_MAX			   QUANTITY_MAX_SHOP, ");
		sql.append("                psm.IS_QUANTITY_MAX_EDIT	   IS_QUANTITY_MAX_EDIT, ");

		//Thuoc tinh dong loai khach hang
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 2 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 2 ");
		sql.append("                                                   AND pcad.object_id = ? ");
		params.add(customerTypeId);
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad, ");
		sql.append("                                                   sale_level_cat slc, ");
		sql.append("                                                   customer_cat_level ccl ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND slc.status = 1 ");
		sql.append("                                                   AND ccl.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_detail CAD ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND ( CASE ");
		sql.append("                                                           WHEN CA.type = 2 THEN CAD.value IS NOT NULL AND Ifnull(Cast(CAD.value AS INTEGER) >= pca.from_value, 1) ");
		sql.append("                                                                                       AND Ifnull(Cast(CAD.value AS INTEGER) <= pca.to_value, 1) ");
		sql.append("                                                           WHEN CA.type = 1 THEN Trim(CAD.value) = Trim(pca.from_value) ");
		sql.append("                                                           WHEN CA.type = 3 THEN CAD.value IS NOT NULL ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) >= substr(pca.from_value, 1, 10), 1) ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) <= substr(pca.to_value, 1, 10), 1) ");
		sql.append("                                                         END ) ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = CAD.customer_attribute_enum_id ");
		sql.append("                                                   AND CAD.customer_attribute_detail_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");

//		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                                                   ,(SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//			sql.append("                                                   (SELECT Count(1)                      ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//			params.add(staffId);
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                        FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//			params.add(customerId);
//		}

		sql.append("                                                   ,(SELECT sum(quantity_max) ");
		sql.append("                                                       FROM promotion_staff_map pstmm ");
		sql.append("                                                       WHERE ");
		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("                                                           AND pstmm.status = 1) QUANTITY_MAX_STAFF, ");
		sql.append("                                                   (SELECT sum(quantity_max) ");
		sql.append("                                                       FROM promotion_customer_map pcmm ");
		sql.append("                                                       WHERE ");
		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("                                                          AND pcmm.status = 1) QUANTITY_MAX_CUSTOMER  ");

//		sql.append("FROM   promotion_program pp, ");
		sql.append("       FROM       (select * from promotion_program ");
		sql.append("                      where 1 = 1 ");
		sql.append("                      AND substr(from_date, 1, 10) <= ? ");
		sql.append("                      AND Ifnull (substr(to_date, 1, 10) >= ?, 1) ");
		sql.append("                      and status = 1 ");
		sql.append("                      and type not in ('ZV23') ");
		sql.append("                      )pp,  ");
		params.add(dateNow);
		params.add(dateNow);
		sql.append("       promotion_shop_map psm ");
		sql.append("		WHERE	");
		sql.append("			1 = 1	");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append("       AND psm.shop_id IN ( " + idListShop);
		sql.append("       ) ");
		sql.append("       AND psm.status = 1 ");
		sql.append("       AND substr(psm.from_date, 1, 10) <= ? ");
		sql.append("       AND Ifnull (substr(psm.to_date, 1, 10) >= ?, 1) ");
		params.add(dateNow);
		params.add(dateNow);

		//Lay ds CT co KM loai KH cua KH
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO1 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO2 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO3 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO4 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO5 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO6 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO7 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO8 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO9 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO10 > 0 ");
		sql.append("              END) ");

		//Check staff map & customer map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("              AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//			sql.append("              AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//		}

		sql.append("GROUP BY 		      ");
//		sql.append("				pp.promotion_program_code,      ");
		sql.append("                pp.promotion_program_id        ");
//		sql.append("                pp.promotion_program_name,      ");
//		sql.append("                pp.status,                      ");
//		sql.append("                pp.type,                        ");
//		sql.append("                pp.pro_format,                  ");
//		sql.append("                pp.from_date,                   ");
//		sql.append("                pp.to_date,                     ");
//		sql.append("                pp.is_edited                    ");
		sql.append("                ) PP, ");
		sql.append("			PRODUCT_GROUP PG,	");
		sql.append("			GROUP_LEVEL GLP,	");
		sql.append("			GROUP_LEVEL GLC,	");
		sql.append("			GROUP_LEVEL_DETAIL GLD	");
		sql.append("		WHERE	");
		sql.append("			1 = 1	");
		sql.append("			AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID	");
		sql.append("			AND PG.PRODUCT_GROUP_ID = GLP.PRODUCT_GROUP_ID	");
		sql.append("			AND GLC.PARENT_GROUP_LEVEL_ID = GLP.GROUP_LEVEL_ID	");
		sql.append("			AND GLC.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID	");
		sql.append("			AND PG.GROUP_TYPE = 1	");
		sql.append("			AND GLP.HAS_PRODUCT = 1	");
		sql.append("			AND GLC.HAS_PRODUCT = 1	");
		sql.append("			AND PP.STATUS = 1	");
		sql.append("			AND PG.STATUS = 1	");
		sql.append("			AND GLP.STATUS = 1	");
		sql.append("			AND GLC.STATUS = 1	");
		sql.append("			AND GLD.STATUS = 1	");
		sql.append("       AND GLD.product_id in ( ");
		sql.append(productId);
		sql.append("       ) ");
		sql.append("GROUP BY 		      ");
//		sql.append("				pp.promotion_program_code,      ");
		sql.append("                pp.promotion_program_id,        ");
//		sql.append("                pp.promotion_program_name,      ");
//		sql.append("                pp.status,                      ");
//		sql.append("                pp.type,                        ");
//		sql.append("                pp.pro_format,                  ");
//		sql.append("                pp.from_date,                   ");
//		sql.append("                pp.to_date,                     ");
//		sql.append("                pp.is_edited,              ");
		sql.append("                GLD.PRODUCT_ID                 ");

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						if (c.getColumnIndex("PRODUCT_ID") >= 0) {
							long proId = CursorUtil.getLong(c, "PRODUCT_ID");
							PromotionProgrameDTO promotionObject = new PromotionProgrameDTO();
							promotionObject = initDTOHasCheckValidCTKM(c);
							if (arPromo.get(proId) == null) {
								ArrayList<PromotionProgrameDTO> lstPromotionProgrameDTO = new ArrayList<PromotionProgrameDTO>();
								ArrayList<PromotionProgrameDTO> lstPromotionProgrameDTOClone = new ArrayList<PromotionProgrameDTO>();
								lstPromotionProgrameDTO.add(promotionObject);
								lstPromotionProgrameDTOClone.add(promotionObject);
								arPromo.put(proId, lstPromotionProgrameDTO);
								arPromoClone.put(proId, lstPromotionProgrameDTOClone);
							}else{
								ArrayList<PromotionProgrameDTO> lstPromotionProgrameDTO = arPromo.get(proId);
								lstPromotionProgrameDTO.add(promotionObject);
								ArrayList<PromotionProgrameDTO> lstPromotionProgrameDTOClone = arPromoClone.get(proId);
								lstPromotionProgrameDTOClone.add(promotionObject);
							}
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getArrPromotionObjectWithCheckInvalid", "fail", e);
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		lstPromotion.add(arPromo);
		lstPromotion.add(arPromoClone);
		return lstPromotion;
	}

	/**
	 *
	 * kiem tra CTKM co hop le voi Khach hang nay hay khong
	 *
	 * @author: HaiTC3
	 * @param listShopId
	 * @param customerId
	 * @param customerTypeId
	 * @return
	 * @return: boolean
	 * @throws:
	 * @since: May 24, 2013
	 */
//	public boolean checkPromotionInValid(String promotionProgramId,
//			String listShopId, String customerId, String customerTypeId, String staffId, boolean checkExpire) {
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
//		boolean result = false;
//		StringBuffer sql = new StringBuffer();
//		ArrayList<String> params = new ArrayList<String>();
//		sql.append("SELECT			pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
//		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
//		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
//		//Thuoc tinh dong loai khach hang
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca ");
//		sql.append("                                            WHERE  pca.object_type = 2 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  1 = 1 ");
//		sql.append("                                                   AND pca.object_type = 2 ");
//		sql.append("                                                   AND pcad.object_id = ? ");
//		params.add(customerTypeId);
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
//		//Thuoc tinh dong muc doanh so
//		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca ");
//		sql.append("                                            WHERE  pca.object_type = 3 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad, ");
//		sql.append("                                                   sale_level_cat slc, ");
//		sql.append("                                                   customer_cat_level ccl ");
//		sql.append("                                            WHERE  1 = 1 ");
//		sql.append("                                                   AND pca.object_type = 3 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND slc.status = 1 ");
//		sql.append("                                                   AND ccl.customer_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
//		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
//		//Thuoc tinh dong cac gia tri don le: 1: chuoi, 2: so, 3: ngay
//		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND AT.value_type IN ( 1, 2, 3 ) ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND ( CASE ");
//		sql.append("                                                           WHEN AT.value_type = 2 THEN atv.value IS NOT NULL AND Ifnull(Cast(atv.value AS INTEGER) >= pca.from_value, 1) ");
//		sql.append("                                                                                       AND Ifnull(Cast(atv.value AS INTEGER) <= pca.to_value, 1) ");
//		sql.append("                                                           WHEN AT.value_type = 1 THEN Trim(atv.value) = Trim(pca.from_value) ");
//		sql.append("                                                           WHEN AT.value_type = 3 THEN atv.value IS NOT NULL ");
//		sql.append("                                                                                       AND Ifnull(Date(atv.value) >= Date(pca.from_value), 1) ");
//		sql.append("                                                                                       AND Ifnull(Date(atv.value) <= Date(pca.to_value), 1) ");
//		sql.append("                                                         END ) ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
//		//Thuoc tinh dong dang select
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type = 4 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv, ");
//		sql.append("                                                   attribute_detail atd, ");
//		sql.append("                                                   attribute_value_detail atvd, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND atd.status = 1 ");
//		sql.append("                                                   AND atvd.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND AT.value_type = 4 ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                                   AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
//		//Thuoc tinh dong dang multi select
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND AT.value_type = 5 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
//		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
//		sql.append("                                            FROM   promotion_cust_attr pca, ");
//		sql.append("                                                   attribute AT, ");
//		sql.append("                                                   attribute_value atv, ");
//		sql.append("                                                   attribute_detail atd, ");
//		sql.append("                                                   attribute_value_detail atvd, ");
//		sql.append("                                                   promotion_cust_attr_detail pcad ");
//		sql.append("                                            WHERE  pca.object_type = 1 ");
//		sql.append("                                                   AND pca.status = 1 ");
//		sql.append("                                                   AND AT.status = 1 ");
//		sql.append("                                                   AND atv.status = 1 ");
//		sql.append("                                                   AND atd.status = 1 ");
//		sql.append("                                                   AND atvd.status = 1 ");
//		sql.append("                                                   AND pcad.status = 1 ");
//		sql.append("                                                   AND AT.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND AT.value_type = 5 ");
//		sql.append("                                                   AND atv.table_name = 'CUSTOMER' ");
//		sql.append("                                                   AND atv.table_id = ? ");
//		params.add(customerId);
//		sql.append("                                                   AND pca.object_id = AT.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atv.attribute_id ");
//		sql.append("                                                   AND AT.attribute_id = atd.attribute_id ");
//		sql.append("                                                   AND atv.attribute_value_id = atvd.attribute_value_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = atvd.attribute_detail_id ");
//		sql.append("                                                   AND atd.attribute_detail_id = pcad.object_id ");
//		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
//		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");
//
//		//Check staff map & customer map
//		sql.append("                                                   ,(SELECT Count(1) ");
//		sql.append("                                                       FROM promotion_staff_map pstmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//		sql.append("                                                   (SELECT Count(1)                      ");
//		sql.append("                                                       FROM ");
//		sql.append("                                                           promotion_staff_map pstmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//		params.add(staffId);
//		sql.append("                                                   (SELECT Count(1) ");
//		sql.append("                                                       FROM promotion_customer_map pcmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//		sql.append("                                                   (SELECT Count(1) ");
//		sql.append("                                                        FROM promotion_customer_map pcmm ");
//		sql.append("                                                       WHERE ");
//		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//		params.add(customerId);
//
//		sql.append("FROM   promotion_program pp, ");
//		sql.append("       promotion_shop_map psm ");
//		sql.append("		WHERE	");
//		sql.append("			1 = 1	");
//		sql.append("       AND pp.promotion_program_id = ? ");
//		params.add(promotionProgramId);
//		sql.append("			AND PP.STATUS = 1	");
//		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
//		//Co check han ap dung CTKM hay ko?
//		if(checkExpire) {
//			sql.append("       AND substr(psm.from_date, 1, 10) <= ? ");
//			sql.append("       AND Ifnull (substr(psm.to_date, 1, 10) >= ?, 1) ");
//			sql.append("       AND substr(pp.from_date, 1, 10) <= ? ");
//			sql.append("       AND Ifnull (substr(pp.to_date, 1, 10) >= ?, 1) ");
//			params.add(dateNow);
//			params.add(dateNow);
//			params.add(dateNow);
//			params.add(dateNow);
//		}
//		sql.append("       AND psm.shop_id IN ( " + listShopId);
//		sql.append("       ) ");
//		sql.append("       AND pp.status = 1 ");
//		sql.append("       AND psm.status = 1 ");
//
//		//Lay ds CT co KM loai KH cua KH
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO1 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO2 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM chuoi, so ngay
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO3 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO4 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM chuoi, so ngay
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO5 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO6 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM dang select
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO7 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO8 > 0 ");
//		sql.append("              END) ");
//
//		//Lay ds CT co KM dang select
//		sql.append("       AND ( CASE ");
//		sql.append("               WHEN COKHAIBAO9 = 0 ");
//		sql.append("               THEN 1 ");
//		sql.append("              ELSE COKHAIBAO10 > 0 ");
//		sql.append("              END) ");
//
//		//Check customer map & staff map
//		sql.append("              AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//		sql.append("              AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//
//		sql.append("GROUP BY ");
//		sql.append("         pp.promotion_program_id ");
//
//		Cursor cApplyNPP = null;
//		try {
//			cApplyNPP = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
//			if (cApplyNPP != null) {
//				if (cApplyNPP.moveToFirst()) {
//					result = checkPromotionProgramInvalid(cApplyNPP);
//				}
//			}
//		} catch (Exception e) {
//			MyLog.w("",	 e.toString());
//		} finally {
//			try {
//				if (cApplyNPP != null) {
//					cApplyNPP.close();
//				}
//			} catch (Exception e2) {
//				// TODO: handle exception
//			}
//		}
//		return result;
//	}


	/**
	 * Lay ds CTKM dang chay
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: int
	 * @throws:
	 */

	public int getNumPromotionProgrameRunning(String shopId) {
		StringBuilder sqlQuery = new StringBuilder();
		// sqlQuery.append("SELECT DISTINCT PP.PROMOTION_PROGRAM_CODE, PP.PROMOTION_PROGRAM_NAME, StrfTime('%d/%m/%Y', PP.FROM_DATE) as FROM_DATE, StrfTime('%d/%m/%Y', PP.TO_DATE) as TO_DATE, TRIM(PP.DESCRIPTION) AS DESCRIPTION  FROM PROMOTION_PROGRAM PP ");
		// sqlQuery.append(" WHERE (dayInOrder(PP.TO_DATE) >= (SELECT dayInOrder('now','localtime')) OR PP.TO_DATE is null)");
		// sqlQuery.append(" AND (PP.STATUS=1) AND (PP.SHOP_ID IN (SELECT SHOP_ID FROM SHOP SH WHERE SH.PARENT_SHOP_ID = ?))");
		// sqlQuery.append(" GROUP BY PP.PROMOTION_PROGRAM_CODE, PP.PROMOTION_PROGRAM_NAME, FROM_DATE, TO_DATE");

		sqlQuery.append("SELECT DISTINCT PP.promotion_program_code, ");
		sqlQuery.append("                PP.promotion_program_name, ");
		sqlQuery.append("                Strftime('%d/%m/%Y', PP.from_date) AS FROM_DATE, ");
		sqlQuery.append("                Strftime('%d/%m/%Y', PP.to_date)   AS TO_DATE, ");
		sqlQuery.append("                Trim(PP.description)               AS DESCRIPTION ");
		sqlQuery.append("FROM   promotion_program PP, ");
		sqlQuery.append("       promotion_shop_map PSM ");
		sqlQuery.append("WHERE  Ifnull (Date(PP.to_date) >= Date('now', 'localtime'), 1) ");
		sqlQuery.append("       AND ( PP.status = 1 ) ");
		sqlQuery.append("       AND PSM.promotion_program_id = PP.promotion_program_id ");
		sqlQuery.append("       AND PSM.status = 1 ");
		sqlQuery.append("       AND Date(PSM.from_date) <= Date('now', 'localtime') ");
		sqlQuery.append("       AND Ifnull (Date(PSM.to_date) >= Date('now', 'localtime'), 1) ");
		sqlQuery.append("       AND ( PSM.shop_id IN (SELECT shop_id ");
		sqlQuery.append("                             FROM   shop SH ");
		sqlQuery.append("                             WHERE  SH.parent_shop_id = ?) ) ");
		sqlQuery.append("GROUP  BY PP.promotion_program_code, ");
		sqlQuery.append("          PP.promotion_program_name, ");
		sqlQuery.append("          from_date, ");
		sqlQuery.append("          to_date ");

		String queryGetListProductForOrder = sqlQuery.toString();
		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ queryGetListProductForOrder + ") ";
		ArrayList<String> params = new ArrayList<String>();
		params.add(shopId);

		Cursor cTmp = null;
		int total = 0;
		try {
			cTmp = rawQuery(getCountProductList,
					params.toArray(new String[params.size()]));
			if (cTmp != null) {
				cTmp.moveToFirst();
				total = cTmp.getInt(0);
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return total;
	}

	/**
	 * Lay ds CTKM cho don hang
	 *
	 * @author: TruongHN
	 * @param shopId
	 * @return: ArrayList<PromotionProgrameDTO>
	 * @throws Exception
	 * @throws:
	 */
	public ArrayList<PromotionProgrameDTO> getListPromotionForOrder2(String shopIdList, String customerId, String staffId, String customerTypeId, String orderType) throws Exception {
		String dateNow = DateUtils.now();
		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT DISTINCT pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
		sql.append("                pp.status                      STATUS, ");
		sql.append("                pp.type                        TYPE, ");
//		sql.append("                pp.pro_format                  PRO_FORMAT, ");
//		sql.append("                pp.from_date                   FROM_DATE, ");
//		sql.append("                pp.to_date                     TO_DATE, ");
		sql.append("                pp.is_edited                   IS_EDITED, ");
		sql.append("                pp.md5_valid_code              MD5_VALID_CODE, ");
		//Thuoc tinh dong loai khach hang
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 2 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 2 ");
		sql.append("                                                   AND pcad.object_id = ? ");
		params.add(customerTypeId);
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad, ");
		sql.append("                                                   sale_level_cat slc, ");
		sql.append("                                                   customer_cat_level ccl ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND slc.status = 1 ");
		sql.append("                                                   AND ccl.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_detail CAD ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND ( CASE ");
		sql.append("                                                           WHEN CA.type = 2 THEN CAD.value IS NOT NULL AND Ifnull(Cast(CAD.value AS INTEGER) >= pca.from_value, 1) ");
		sql.append("                                                                                       AND Ifnull(Cast(CAD.value AS INTEGER) <= pca.to_value, 1) ");
		sql.append("                                                           WHEN CA.type = 1 THEN Trim(CAD.value) = Trim(pca.from_value) ");
		sql.append("                                                           WHEN CA.type = 3 THEN CAD.value IS NOT NULL ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) >= substr(pca.from_value, 1, 10), 1) ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) <= substr(pca.to_value, 1, 10), 1) ");
		sql.append("                                                         END ) ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = CAD.customer_attribute_enum_id ");
		sql.append("                                                   AND CAD.customer_attribute_detail_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");

		//Check staff map & customer map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                                                   ,(SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//			sql.append("                                                   (SELECT Count(1)                      ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//			params.add(staffId);
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                        FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//			params.add(customerId);
//		}

		sql.append("                                                   , psm.quantity_max QUANTITY_MAX_SHOP ");
		sql.append("                                                   , psm.is_quantity_max_edit IS_QUANTITY_MAX_EDIT ");

		sql.append("                                                   ,(SELECT sum(quantity_max) ");
		sql.append("                                                       FROM promotion_staff_map pstmm ");
		sql.append("                                                       WHERE ");
		sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("                                                           AND pstmm.status = 1) QUANTITY_MAX_STAFF, ");
		sql.append("                                                   (SELECT sum(quantity_max) ");
		sql.append("                                                       FROM promotion_customer_map pcmm ");
		sql.append("                                                       WHERE ");
		sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("                                                          AND pcmm.status = 1) QUANTITY_MAX_CUSTOMER  ");
//		params.add(customerId);
		sql.append("FROM   promotion_program pp,  ");
		sql.append("       PRODUCT_GROUP PG,  ");
		sql.append("       GROUP_LEVEL GL, ");
		sql.append("       promotion_shop_map psm ");
		// Kiem tra xem co ap dung toi muc KH ko?
//		sql.append("       left join promotion_customer_map pcm on pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//		sql.append("       LEFT JOIN promotion_staff_map pstm ON pstm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND psm.shop_id in ( ");
		sql.append(shopIdList);
		sql.append(") ");
		sql.append("	   AND PP.TYPE NOT IN ('ZV23') ");
		sql.append("       AND PP.PROMOTION_PROGRAM_ID = PG.PROMOTION_PROGRAM_ID ");
		sql.append("       AND PG.PRODUCT_GROUP_ID = GL.PRODUCT_GROUP_ID ");
		//Nhom sp ban & khong co sp
		sql.append("       AND PG.GROUP_TYPE = 1 AND GL.HAS_PRODUCT = 0  ");
		sql.append("       AND PP.STATUS = 1 AND PG.STATUS = 1 AND GL.STATUS = 1 ");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append("       AND psm.status = 1 ");
		sql.append("       AND substr(psm.from_date, 1, 10) <= substr(?, 1, 10) ");
		sql.append("       AND Ifnull(substr(psm.to_date, 1, 10) >= substr(?, 1, 10), 1) ");
		sql.append("       AND substr(pp.from_date, 1, 10) <= substr(?, 1, 10) ");
		sql.append("       AND Ifnull(substr(pp.to_date, 1, 10) >= substr(?, 1, 10), 1) ");
		params.add(dateNow);
		params.add(dateNow);
		params.add(dateNow);
		params.add(dateNow);

		//Check customer map & staff map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("       AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//			sql.append("       AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//		}

		sql.append("       GROUP BY PP.PROMOTION_PROGRAM_ID ");
		sql.append("       ORDER BY PP.PROMOTION_PROGRAM_CODE ");

		ArrayList<PromotionProgrameDTO> listPromotion = new ArrayList<PromotionProgrameDTO>();
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				PromotionProgrameDTO promotionObject;
				if (c.moveToFirst()) {
					do {
						promotionObject = initDTOHasCheckValidCTKM(c);
						listPromotion.add(promotionObject);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getListPromotionForOrder", e.getMessage());
			}
		}

		return listPromotion;
	}

	/**
	 * Lay ds CT tich luy
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<PromotionProgrameDTO>
	 * @throws:
	 * @param shopIdList
	 * @param customerId
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<PromotionProgrameDTO> getListAccumulationPromotion(String shopIdList, String customerId, String staffId, String customerTypeId, String orderType) throws Exception {
		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT 			pp.promotion_program_code      PROMOTION_PROGRAM_CODE, ");
		sql.append("                pp.promotion_program_id        PROMOTION_PROGRAM_ID, ");
		sql.append("                pp.promotion_program_name      PROMOTION_PROGRAM_NAME, ");
		sql.append("                pp.from_date			       FROM_DATE, ");
		sql.append("                pp.to_date					   TO_DATE, ");
		sql.append("                pp.status                      STATUS, ");
		sql.append("                pp.type                        TYPE, ");
		sql.append("                pp.is_edited                   IS_EDITED, ");
		sql.append("                pp.md5_valid_code              MD5_VALID_CODE, ");
		//Thuoc tinh dong loai khach hang
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 2 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO1, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 2 ");
		sql.append("                                                   AND pcad.object_id = ? ");
		params.add(customerTypeId);
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO2, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca ");
		sql.append("                                            WHERE  pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO3, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   promotion_cust_attr_detail pcad, ");
		sql.append("                                                   sale_level_cat slc, ");
		sql.append("                                                   customer_cat_level ccl ");
		sql.append("                                            WHERE  1 = 1 ");
		sql.append("                                                   AND pca.object_type = 3 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND slc.status = 1 ");
		sql.append("                                                   AND ccl.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   AND pcad.object_id = slc.sale_level_cat_id ");
		sql.append("                                                   AND slc.sale_level_cat_id = ccl.sale_level_cat_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO4, ");
		sql.append("                                 (SELECT Count (promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO5, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_detail CAD ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND CA.type IN ( 1, 2, 3 ) ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND ( CASE ");
		sql.append("                                                           WHEN CA.type = 2 THEN CAD.value IS NOT NULL AND Ifnull(Cast(CAD.value AS INTEGER) >= pca.from_value, 1) ");
		sql.append("                                                                                       AND Ifnull(Cast(CAD.value AS INTEGER) <= pca.to_value, 1) ");
		sql.append("                                                           WHEN CA.type = 1 THEN Trim(CAD.value) = Trim(pca.from_value) ");
		sql.append("                                                           WHEN CA.type = 3 THEN CAD.value IS NOT NULL ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) >= substr(pca.from_value, 1, 10), 1) ");
		sql.append("                                                                                       AND Ifnull(substr(CAD.value, 1, 10) <= substr(pca.to_value, 1, 10), 1) ");
		sql.append("                                                         END ) ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO6, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO7, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 4 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = CAD.customer_attribute_enum_id ");
		sql.append("                                                   AND CAD.customer_attribute_detail_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO8, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO9, ");
		sql.append("                                 (SELECT Count (pca.promotion_cust_attr_id) NUM_CONDITION ");
		sql.append("                                            FROM   promotion_cust_attr pca, ");
		sql.append("                                                   customer_attribute CA, ");
		sql.append("                                                   customer_attribute_enum CAE, ");
		sql.append("                                                   customer_attribute_detail CAD, ");
		sql.append("                                                   promotion_cust_attr_detail pcad ");
		sql.append("                                            WHERE  pca.object_type = 1 ");
		sql.append("                                                   AND pca.status = 1 ");
		sql.append("                                                   AND CA.status = 1 ");
		sql.append("                                                   AND CAE.status = 1 ");
		sql.append("                                                   AND CAD.status = 1 ");
		sql.append("                                                   AND pcad.status = 1 ");
		sql.append("                                                   AND CA.type = 5 ");
		sql.append("                                                   AND CAD.customer_id = ? ");
		params.add(customerId);
		sql.append("                                                   AND pca.object_id = CA.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAD.customer_attribute_id ");
		sql.append("                                                   AND CA.customer_attribute_id = CAE.customer_attribute_id ");
		sql.append("                                                   AND CAE.customer_attribute_enum_id = pcad.object_id ");
		sql.append("                                                   AND pca.promotion_cust_attr_id = pcad.promotion_cust_attr_id ");
		sql.append("                                                   and pca.promotion_program_id = pp.promotion_program_id) COKHAIBAO10 ");

		//Check staff map & customer map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("                                                   ,(SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1) VALUE01, ");
//			sql.append("                                                   (SELECT Count(1)                      ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_staff_map pstmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                           pstmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pstmm.status = 1 AND pstmm.staff_id = ?) VALUE02, ");
//			params.add(staffId);
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                       FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                          AND pcmm.status = 1) VALUE03,  ");
//			sql.append("                                                   (SELECT ");
//			sql.append("                                                           Count(1) ");
//			sql.append("                                                        FROM ");
//			sql.append("                                                           promotion_customer_map pcmm ");
//			sql.append("                                                       WHERE ");
//			sql.append("                                                          pcmm.promotion_shop_map_id = psm.promotion_shop_map_id ");
//			sql.append("                                                           AND pcmm.status = 1 AND pcmm.customer_id = ?) VALUE04 ");
//			params.add(customerId);
//		}
		sql.append("FROM   promotion_program pp, ");
		sql.append("       promotion_shop_map psm ");
		sql.append("		WHERE	1 = 1	");
		sql.append("		AND pp.promotion_program_id in (SELECT distinct promotion_program_id from ");
		sql.append("		 rpt_cttl where 1 = 1 and shop_id in (");
		sql.append(shopIdList);
		sql.append("		 )");
		sql.append("		 and customer_id = ? ");
		params.add(customerId);
		sql.append("		 )");
//		sql.append("			AND PP.STATUS = 1	");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append("       AND psm.shop_id IN ( " + shopIdList);
		sql.append("       ) ");
		sql.append("       AND pp.status = 1 ");
		sql.append("       AND psm.status = 1 ");

		//Lay ds CT co KM loai KH cua KH
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO1 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO2 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO3 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO4 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM chuoi, so ngay
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO5 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO6 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO7 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO8 > 0 ");
		sql.append("              END) ");

		//Lay ds CT co KM dang select
		sql.append("       AND ( CASE ");
		sql.append("               WHEN COKHAIBAO9 = 0 ");
		sql.append("               THEN 1 ");
		sql.append("              ELSE COKHAIBAO10 > 0 ");
		sql.append("              END) ");

		//Check customer map & staff map
		//Sua: chi apply cho vansale, presale ko check: nghiep vu kiem tra tra so suat truoc khi luu
//		if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)) {
//			sql.append("              AND ((VALUE01 = 0 AND VALUE02 = 0) OR VALUE02 > 0) ");
//			sql.append("              AND ((VALUE03 = 0 AND VALUE04 = 0) OR VALUE04 > 0) ");
//		}

		sql.append("GROUP BY pp.promotion_program_id ");
		sql.append("ORDER BY PP.PROMOTION_PROGRAM_CODE ");

//		sql.append("SELECT PP.* ");
//		sql.append("FROM   promotion_program pp  ");
//		sql.append("WHERE  1 = 1 ");
//		sql.append("	   AND pp.status = 1 ");
//		sql.append("		AND pp.promotion_program_id in (SELECT distinct promotion_program_id from ");
//		sql.append("		 rpt_cttl where 1 = 1 and shop_id in (");
//		sql.append(shopIdList);
//		sql.append("		 )");
//		sql.append("		 and customer_id = ? ");
//		params.add(customerId);
//		sql.append("		 )");
//		sql.append("       ORDER BY PP.PROMOTION_PROGRAM_CODE ");

		ArrayList<PromotionProgrameDTO> listPromotion = new ArrayList<PromotionProgrameDTO>();
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				PromotionProgrameDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initDTOHasCheckValidCTKM(c);
						listPromotion.add(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.i("getListAccumulationOrder", e.getMessage());
			}
		}

		return listPromotion;
	}

	/**
	 *
	 * Khoi tao doi tuong chuong trinh KM
	 *
	 * @author: TruongHN
	 * @param c
	 * @return: PromotionProgrameDTO
	 * @throws:
	 */
	private PromotionProgrameDTO initPromotionPrograme(Cursor c) {
		PromotionProgrameDTO dto = new PromotionProgrameDTO();
		dto.setPROMOTION_PROGRAM_CODE(CursorUtil.getString(c, PROMOTION_PROGRAM_CODE));
		dto.setPROMOTION_PROGRAM_ID(CursorUtil.getInt(c, PROMOTION_PROGRAM_ID));
		dto.setPROMOTION_PROGRAM_NAME(CursorUtil.getString(c, PROMOTION_PROGRAM_NAME));
		dto.setSTATUS(CursorUtil.getInt(c, STATUS));
		dto.setTYPE(CursorUtil.getString(c, TYPE));
		dto.setIsEdited(CursorUtil.getInt(c, IS_EDITED));
		dto.setFROM_DATE(CursorUtil.getString(c, FROM_DATE));
		dto.setTO_DATE(CursorUtil.getString(c, TO_DATE));
		dto.setDESCRIPTION(CursorUtil.getString(c, DESCRIPTION));
		return dto;
	}

	/**
	 * Lay thong tin don hang tich luy
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: OrderViewDTO
	 * @throws:
	 * @param pROMOTION_PROGRAM_ID2
	 * @return
	 * @throws Exception
	 */
	public OrderViewDTO getAccumulationOrder(long promotionProgramId, String customerId, String idShopList, long saleOrderId) throws Exception {
		OrderViewDTO result = new OrderViewDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		//Lay thong tin tong tich luy cua ct
		StringBuilder sqlOrder = new StringBuilder();
		ArrayList<String> paramsOrder = new ArrayList<String>();
		sqlOrder.append("SELECT * FROM ");
		sqlOrder.append("       RPT_CTTL RPT_AP ");
		sqlOrder.append("WHERE 1 = 1 ");
		sqlOrder.append("AND RPT_AP.PROMOTION_PROGRAM_ID = ? ");
		paramsOrder.add(String.valueOf(promotionProgramId));
		sqlOrder.append("AND RPT_AP.CUSTOMER_ID = ? ");
		paramsOrder.add(customerId);
		sqlOrder.append("AND RPT_AP.SHOP_ID IN ( ");
		sqlOrder.append(idShopList);
		sqlOrder.append(") ");
		Cursor cOrder = null;
		RptCttlDTO rptCttlDTO = new RptCttlDTO();
		try {
			cOrder = rawQuery(sqlOrder.toString(), paramsOrder.toArray(new String[paramsOrder.size()]));
			if (cOrder != null) {
				if (cOrder.moveToFirst()) {
					rptCttlDTO.initFromCursor(cOrder);
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
			throw e;
		} finally {
			try {
				if (cOrder != null) {
					cOrder.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		//Lay thong tin cac sp tich luy
		StringBuilder sqlOrderDetail = new StringBuilder();
		ArrayList<String> paramsOrderDetail = new ArrayList<String>();
		sqlOrderDetail.append("SELECT * FROM ");
		sqlOrderDetail.append("       RPT_CTTL_DETAIL RPT_AP_DETAIL ");
		sqlOrderDetail.append("WHERE 1 = 1 ");
		sqlOrderDetail.append("AND RPT_AP_DETAIL.RPT_CTTL_ID = ? ");
		paramsOrderDetail.add(String.valueOf(rptCttlDTO.rptCttlId));
		Cursor cOrderDetail = null;
		ArrayList<RptCttlDetailDTO> listRptDetail = new ArrayList<RptCttlDetailDTO>();
		try {
			cOrderDetail = rawQuery(sqlOrderDetail.toString(), paramsOrderDetail.toArray(new String[paramsOrderDetail.size()]));
			if (cOrderDetail != null) {
				if (cOrderDetail.moveToFirst()) {
					do {
						RptCttlDetailDTO rptCttlDetailDTO = new RptCttlDetailDTO();
						rptCttlDetailDTO.initFromCursor(cOrderDetail);
						listRptDetail.add(rptCttlDetailDTO);
					} while (cOrderDetail.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cOrderDetail != null) {
					cOrderDetail.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		//Tien KM con du cua nhung ngay truoc
//		result.orderInfo.discount = Math.round(rptCttlDTO.balancePromotion);

		//Lay balance promotion
		ArrayList<String> cttpPayIdList = new ArrayList<String>();
		StringBuilder sqlBalance = new StringBuilder();
		ArrayList<String> paramsBalance = new ArrayList<String>();
		sqlBalance.append("SELECT * FROM ");
		sqlBalance.append("       RPT_CTTL_PAY RPT_AP_PAY ");
		sqlBalance.append("WHERE 1 = 1 ");
		sqlBalance.append("AND RPT_AP_PAY.PROMOTION_PROGRAM_ID = ? ");
		paramsBalance.add(String.valueOf(promotionProgramId));
		sqlBalance.append("AND RPT_AP_PAY.CUSTOMER_ID = ? ");
		paramsBalance.add(customerId);
		sqlBalance.append("AND RPT_AP_PAY.SHOP_ID IN ( ");
		sqlBalance.append(idShopList);
		sqlBalance.append(") ");
		sqlBalance.append("AND SUBSTR(RPT_AP_PAY.CREATE_DATE, 1, 10) = ? ");
		paramsBalance.add(dateNow);
		sqlBalance.append("AND RPT_AP_PAY.SALE_ORDER_ID != ? ");
		paramsBalance.add(String.valueOf(saleOrderId));
		//Lay nhung don chua duyet hoac da duyet trong ngay
//		sqlBalance.append("AND RPT_AP_PAY.APPROVED IN (0, 1) ");
//		sqlBalance.append("ORDER BY AMOUNT_PROMOTION DESC ");
		Cursor cBalance = null;
		try {
			cBalance = rawQuery(sqlBalance.toString(), paramsBalance.toArray(new String[paramsBalance.size()]));
			if (cBalance != null) {
				if (cBalance.moveToFirst()) {
//					boolean isLast = true;
//					result.orderInfo.discount = 0;
//					long amountPromotion = 0;
//					long actuallyPromotion = 0;

					do {
						RptCttlPayDTO rptCttlPayDto = new RptCttlPayDTO();
						rptCttlPayDto.initFromCursor(cBalance);

						//Giam tru cac gia tri da tinh
						rptCttlDTO.totalQuantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", rptCttlDTO.totalQuantity)))
								.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", rptCttlPayDto.totalQuantityPayPromotion))).doubleValue();
						rptCttlDTO.totalAmount = (new BigDecimal(StringUtil.decimalFormatSymbols("#", rptCttlDTO.totalAmount)))
								.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", rptCttlPayDto.totalAmountPayPromotion))).longValue();

//						rptCttlDTO.totalQuantity -= rptCttlPayDto.totalQuantityPayPromotion;
//						rptCttlDTO.totalAmount -= rptCttlPayDto.totalAmountPayPromotion;
						cttpPayIdList.add(String.valueOf(rptCttlPayDto.rptCttlPayId));
						//Lay tien KM con du
//						if(isLast) {
//							isLast = false;
//						if(rptCttlPayDto.amountPromotion > result.orderInfo.discount) {
//							amountPromotion = rptCttlPayDto.amountPromotion;
////							result.orderInfo.discount = rptCttlPayDto.amountPromotion;
//						}
//						actuallyPromotion += rptCttlPayDto.actuallyPromotion;
//						result.orderInfo.discount -= rptCttlPayDto.actuallyPromotion;
//						}
					} while (cBalance.moveToNext());

					//Lam tron 2 chu so
//					rptCttlDTO.totalQuantity = StringUtil.roundDoubleDown("#.##", rptCttlDTO.totalQuantity);
//					result.orderInfo.discount = amountPromotion - actuallyPromotion;
//					if(result.orderInfo.discount < 0) {
//						result.orderInfo.discount = 0;
//					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cBalance != null) {
					cBalance.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		//Lay thong tin cac sp tich luy
		StringBuilder sqlDetailPay = new StringBuilder();
		ArrayList<String> paramsDetailPay = new ArrayList<String>();
		sqlDetailPay.append("SELECT * FROM ");
		sqlDetailPay.append("       RPT_CTTL_DETAIL_PAY RPT_AP_DETAIL_PAY ");
		sqlDetailPay.append("WHERE 1 = 1 ");
		sqlDetailPay.append("AND RPT_AP_DETAIL_PAY.RPT_CTTL_PAY_ID IN ( ");
		sqlDetailPay.append(TextUtils.join(",", cttpPayIdList));
		sqlDetailPay.append(") ");
//		paramsDetailPay.add(String.valueOf(rptCttlDTO.rptCttlId));
		Cursor cDetailPay = null;
		try {
			cDetailPay = rawQuery(sqlDetailPay.toString(), paramsDetailPay.toArray(new String[paramsDetailPay.size()]));
			if (cDetailPay != null) {
				if (cDetailPay.moveToFirst()) {
					do {
						int productId = CursorUtil.getInt(cDetailPay, RPT_CTTL_DETAIL_PAY_TABLE.PRODUCT_ID);
						double quantityPayPromotion = CursorUtil.getDouble(cDetailPay, RPT_CTTL_DETAIL_PAY_TABLE.QUANTITY_PROMOTION);
						long amountPayPromotion = CursorUtil.getLong(cDetailPay, RPT_CTTL_DETAIL_PAY_TABLE.AMOUNT_PROMOTION);

						for(RptCttlDetailDTO rptCttlDetail : listRptDetail) {
							if(rptCttlDetail.productId == productId) {
								rptCttlDetail.quantity = (new BigDecimal(StringUtil.decimalFormatSymbols("#.##", rptCttlDetail.quantity)))
										.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.##", quantityPayPromotion))).doubleValue();
								rptCttlDetail.amount = (new BigDecimal(StringUtil.decimalFormatSymbols("#", rptCttlDetail.amount)))
										.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#", amountPayPromotion))).longValue();

//								rptCttlDetail.quantity -= quantityPayPromotion;
//								rptCttlDetail.amount -= amountPayPromotion;
								break;
							}
						}

					} while (cDetailPay.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cDetailPay != null) {
					cDetailPay.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		//Chuyen hoa thanh sale order
		result.orderInfo.setAmount(rptCttlDTO.totalAmount);
//		result.orderInfo.totalDetail = rptCttlDTO.totalQuantity;

		for(RptCttlDetailDTO rptCttlDetail : listRptDetail) {
			OrderDetailViewDTO detailView = new OrderDetailViewDTO();
			SaleOrderDetailDTO orderDetail = new SaleOrderDetailDTO();
			detailView.orderDetailDTO = orderDetail;

			orderDetail.productId = rptCttlDetail.productId;
			orderDetail.quantity = rptCttlDetail.quantity;
			orderDetail.setAmount(rptCttlDetail.amount);

			if(orderDetail.quantity < 0 || orderDetail.getAmount() < 0) {
				orderDetail.quantity = 0;
				orderDetail.setAmount(0);
			}

			//Gia duoc tinh tu bang rpt detail
			orderDetail.quantityBegin = rptCttlDetail.quantityBegin;
			orderDetail.amountBegin = rptCttlDetail.amountBegin;
			orderDetail.priceBegin = rptCttlDetail.priceBegin;

			result.listBuyOrders.add(detailView);
		}

		return result;
	}

	/**
	 * Lay ds sp quy doi
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<PromotionProductConvertDTO>
	 * @throws:
	 * @param promotionProgramId
	 * @return
	 * @throws Exception
	 */
	public ArrayList<PromotionProductConvertDTO> getListProductConvertGroup(int promotionProgramId) throws Exception {
		ArrayList<PromotionProductConvertDTO> result = new ArrayList<PromotionProductConvertDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT * FROM ");
		sql.append("       PROMOTION_PRODUCT_CONVERT PPC, PROMOTION_PRODUCT_CONV_DTL PPCD ");
		sql.append("WHERE 1 = 1 ");
		sql.append("AND PPC.PROMOTION_PROGRAM_ID = ? ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("AND PPC.PROMOTION_PRODUCT_CONVERT_ID = PPCD.PROMOTION_PRODUCT_CONVERT_ID ");
		sql.append("AND PPC.STATUS = 1 AND PPCD.STATUS = 1 ");
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						PromotionProductConvDtlDTO convertDetailDto = new PromotionProductConvDtlDTO();
						convertDetailDto.initFromCursor(c);

						boolean isExist = false;
						PromotionProductConvertDTO productConvertGroup = new PromotionProductConvertDTO();
						for(PromotionProductConvertDTO productConvertDto: result) {
							if(productConvertDto.promotionProductConvertId == convertDetailDto.promotionProductConvertId) {
								productConvertGroup = productConvertDto;
								isExist = true;
								break;
							}
						}

						//Chua co nhom
						if(!isExist) {
//							productConvertGroup = new PromotionProductConvertDTO();
							productConvertGroup.promotionProductConvertId = convertDetailDto.promotionProductConvertId;
							result.add(productConvertGroup);
						}

						//Sp goc
						if(convertDetailDto.isSourceProduct == 1) {
							productConvertGroup.sourceProduct = convertDetailDto;
						//sp qui doi
						} else {
							productConvertGroup.listConvertProduct.add(convertDetailDto);
						}
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		return result;
	}

	/**
	 * Lay ds sp mo moi
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<PromotionProductConvertDTO>
	 * @throws:
	 * @param promotionProgramId
	 * @return
	 */
	public boolean isPromotionNewOpen(int promotionProgramId, String customerId, String idShopList, OrderViewDTO orderView) {
		boolean result = false;
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<PromotionProductOpenDTO> listProductOpen = new ArrayList<PromotionProductOpenDTO>();
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("SELECT PPO.* FROM ");
		sql.append("	PROMOTION_PRODUCT_OPEN PPO, ");
		sql.append("	RPT_LAST_ORDER_PRODUCT RPT_LOP ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("	AND PPO.PROMOTION_PROGRAM_ID = ? ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("	AND RPT_LOP.CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append("	AND RPT_LOP.SHOP_ID IN ( ");
		sql.append(idShopList);
		sql.append("	) ");
		sql.append("	AND PPO.PROMOTION_PROGRAM_ID = RPT_LOP.PROMOTION_PROGRAM_ID ");
		sql.append("	AND PPO.PRODUCT_ID = RPT_LOP.PRODUCT_ID ");
		sql.append("	AND PPO.PRODUCT_ID NOT IN ( ");
		sql.append("	SELECT SOD.PRODUCT_ID FROM SALE_ORDER SO, SALE_ORDER_DETAIL SOD ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("	AND SO.SALE_ORDER_ID = SOD.SALE_ORDER_ID ");
		sql.append("	AND SO.CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append("	AND SOD.IS_FREE_ITEM = 0 ");
		//Don hien tai la presale thi sp ko con la mo moi trong Don presale da duyet hoac don hang vansale chua tra
		if (orderView.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			sql.append("	AND ((SO.ORDER_TYPE = 'IN' AND (SO.APPROVED = 1 OR (SO.APPROVED = 0 AND SO.APPROVED_STEP = 1)) AND SO.TYPE = 1) OR (SO.ORDER_TYPE = 'SO' AND SO.TYPE = 1 AND SO.APPROVED IN (0, 1))) ");
			//don vansale thi sp ko con la mo moi trong nhung don
		} else if (orderView.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			sql.append("	AND SO.TYPE = 1 AND SO.APPROVED IN (0, 1) ");
		}
		sql.append("	AND SUBSTR(SO.CREATE_DATE, 1, 10) = ?) ");
		params.add(dateNow);
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						PromotionProductOpenDTO productOpen = new PromotionProductOpenDTO();
						productOpen.initFromCursor(c);
						listProductOpen.add(productOpen);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		//Duyet qua ds sp mo moi con lai
		for(PromotionProductOpenDTO productOpen : listProductOpen) {
			//Kiem tra sp ban co sp mo moi thoa dk
			for(OrderDetailViewDTO product : orderView.listBuyOrders) {
				if(productOpen.productId == product.orderDetailDTO.productId) {
					if((productOpen.quantity > 0 && product.orderDetailDTO.quantity >= productOpen.quantity) ||
							(productOpen.amount > 0 && product.orderDetailDTO.getAmount() >= productOpen.amount)) {
						result = true;
						break;
					}
				}
			}
		}

		return result;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: boolean
	 * @throws:
	 * @param pROMOTION_PROGRAM_ID2
	 * @return
	 */
	@SuppressLint("DefaultLocale")
	public boolean validatePromotionPrograme(int promotionProgramId, String promotionProgramCode, String md5ValidCode) {
		boolean result = false;
		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("	SELECT GROUP_CONCAT(ID_UPDATE_DATE, '') ID_UPDATE_DATE	");
		sql.append("	FROM	");
		sql.append("	    ( SELECT 1 AS ID, ID || IFNULL(UPDATE_DATE, '') ID_UPDATE_DATE	");
		sql.append("	    FROM	");
		sql.append("	        ( SELECT	");
		sql.append("	            1 AS TABLE_NAME,	");
		sql.append("	            PGP.PRODUCT_GROUP_ID AS ID,	");
		sql.append("	            Strftime('%d/%m/%Y %H:%M:%S', PGP.UPDATE_DATE) AS UPDATE_DATE	");
		sql.append("	        FROM PRODUCT_GROUP PGP	");
		sql.append("	        WHERE PGP.PROMOTION_PROGRAM_ID = ? AND PGP.STATUS = 1 ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("	        UNION	");
		sql.append("	        SELECT	");
		sql.append("	            2 AS TABLE_NAME,	");
		sql.append("	            GLL.GROUP_LEVEL_ID AS ID,	");
		sql.append("	            Strftime('%d/%m/%Y %H:%M:%S', GLL.UPDATE_DATE) AS UPDATE_DATE	");
		sql.append("	        FROM GROUP_LEVEL GLL	");
		sql.append("	        WHERE GLL.PRODUCT_GROUP_ID IN (	");
		sql.append("	                SELECT PGP.PRODUCT_GROUP_ID	");
		sql.append("	                FROM PRODUCT_GROUP PGP	");
		sql.append("	                WHERE PGP.PROMOTION_PROGRAM_ID = ?  AND PGP.STATUS = 1 ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("	            )  AND GLL.STATUS = 1 ");
		sql.append("	        UNION	");
		sql.append("	        SELECT	");
		sql.append("	            3 AS TABLE_NAME,	");
		sql.append("	            GLDL.GROUP_LEVEL_DETAIL_ID AS ID,	");
		sql.append("	            Strftime('%d/%m/%Y %H:%M:%S', GLDL.UPDATE_DATE) AS UPDATE_DATE	");
		sql.append("	        FROM GROUP_LEVEL_DETAIL GLDL	");
		sql.append("	        WHERE	");
		sql.append("	            GLDL.GROUP_LEVEL_ID IN (	");
		sql.append("	                SELECT GLL.GROUP_LEVEL_ID	");
		sql.append("	                FROM GROUP_LEVEL GLL	");
		sql.append("	                WHERE GLL.PRODUCT_GROUP_ID IN (	");
		sql.append("	                        SELECT PGP.PRODUCT_GROUP_ID	");
		sql.append("	                        FROM PRODUCT_GROUP PGP	");
		sql.append("	                        WHERE PGP.PROMOTION_PROGRAM_ID = ?	AND PGP.STATUS = 1 ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("	                    )	");
		sql.append("	                AND GLL.STATUS = 1 ) AND GLDL.STATUS = 1 ");
		sql.append("	            UNION	");
		sql.append("	            SELECT	");
		sql.append("	                4 AS TABLE_NAME,	");
		sql.append("	                GMG.GROUP_MAPPING_ID AS ID,	");
		sql.append("	                Strftime('%d/%m/%Y %H:%M:%S', GMG.UPDATE_DATE) AS UPDATE_DATE	");
		sql.append("	            FROM	");
		sql.append("	                GROUP_MAPPING GMG	");
		sql.append("	            WHERE	");
		sql.append("	                GMG.PROMO_GROUP_ID IN (	");
		sql.append("	                    SELECT PGP.PRODUCT_GROUP_ID	");
		sql.append("	                    FROM PRODUCT_GROUP PGP	");
		sql.append("	                    WHERE PGP.PROMOTION_PROGRAM_ID = ?	AND PGP.STATUS = 1 ");
		params.add(String.valueOf(promotionProgramId));
		sql.append("	                ) AND GMG.STATUS = 1	");
		sql.append("	            ) ");
		sql.append("	        ORDER BY TABLE_NAME, ID	");
		sql.append("	    )	");
		sql.append("	GROUP BY ID	");
		Cursor c = null;
		String idUpdateDate = "";
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					idUpdateDate = CursorUtil.getString(c, "ID_UPDATE_DATE");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}

		try {
			String hash = StringUtil.generateHash(idUpdateDate, promotionProgramCode.toLowerCase());

			if (StringUtil.isNullOrEmpty(md5ValidCode)
					|| "null".equals(md5ValidCode) || hash.equals(md5ValidCode)) {
				result = true;
			}
		} catch (NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException e) {
		}

		return result;
	}

	/**
	 * Lay ds chuong trinh KM
	 * @author: hoanpd1
	 * @since: 17:06:12 24-03-2015
	 * @return: PromotionProgrameModel
	 * @throws:
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public PromotionProgrameModel getListPromotionPrograme(Bundle bundle)
			throws Exception {
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);

		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}

		String ext = bundle.getString(IntentConstants.INTENT_PAGE);
		boolean checkLoadMore = bundle.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);
		int staffOwnerId = bundle.getInt(IntentConstants.INTENT_PARENT_STAFF_ID);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);
//		STAFF_TABLE staff = new STAFF_TABLE(mDB);
//		String listStaffId = staff.getListStaffOfSupervisor(String.valueOf(staffOwnerId), shopId);
		ArrayList<String> params = new ArrayList<String>();
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("	SELECT	");
		sqlQuery.append("	    PP.promotion_program_code,	");
		sqlQuery.append("	    PP.promotion_program_name,	");
		sqlQuery.append("	    Strftime('%d/%m/%Y',	");
		sqlQuery.append("	    PSM.from_date) AS FROM_DATE,	");
		sqlQuery.append("	    Strftime('%d/%m/%Y',	");
		sqlQuery.append("	    PSM.to_date)   AS TO_DATE,	");
		sqlQuery.append("	    PP.description	");
		sqlQuery.append("	FROM	");
		sqlQuery.append("	    promotion_program PP,	");
		sqlQuery.append("	    promotion_shop_map PSM	");
//		sqlQuery.append("	LEFT JOIN	");
//		sqlQuery.append("	    promotion_staff_map pstm	");
//		sqlQuery.append("	        ON pstm.promotion_shop_map_id = psm.promotion_shop_map_id	");
		sqlQuery.append("	WHERE	");
		sqlQuery.append("	    PP.status = 1	");
		sqlQuery.append("	    AND PSM.status = 1	");
		sqlQuery.append("	    AND PSM.SHOP_ID IN (	");
		sqlQuery.append(idShopList);
		sqlQuery.append("	    )	");
		sqlQuery.append("	    AND PSM.promotion_program_id = PP.promotion_program_id	");
//		sqlQuery.append("	    AND (	");
//		sqlQuery.append("	        CASE	");
//		sqlQuery.append("	            WHEN (	");
//		sqlQuery.append("	                SELECT	");
//		sqlQuery.append("	                    Count(1)	");
//		sqlQuery.append("	                FROM	");
//		sqlQuery.append("	                    promotion_staff_map pstmm	");
//		sqlQuery.append("	                WHERE	");
//		sqlQuery.append("	                    pstmm.promotion_shop_map_id = PSM.promotion_shop_map_id	");
//		sqlQuery.append("	                    AND pstmm.status = 1	");
//		sqlQuery.append("	            ) = 0              THEN 1	");
//		sqlQuery.append("	            ELSE pstm.staff_id = ?	");
//		params.add(String.valueOf(staffOwnerId));
//		sqlQuery.append("	            AND pstm.status = 1	");
//		sqlQuery.append("	        END	");
//		sqlQuery.append("	    )	");
		sqlQuery.append("	    AND Ifnull(Date(PSM.to_date) >= Date('NOW', 'LOCALTIME'), 1)	");
		sqlQuery.append("	    AND Ifnull (Date(PP.to_date) >= Date('now', 'localtime'), 1)	");


		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
		defaultOrderByStr.append("	ORDER  BY	");
		defaultOrderByStr.append("	    PSM.from_date DESC,	");
		defaultOrderByStr.append("	    CASE	");
		defaultOrderByStr.append("	        WHEN PP.to_date IS NULL THEN 1	");
		defaultOrderByStr.append("	        ELSE 0	");
		defaultOrderByStr.append("	    END,	");
		defaultOrderByStr.append("	    PSM.to_date,	");
		defaultOrderByStr.append("	    PP.promotion_program_code,	");
		defaultOrderByStr.append("	    PP.promotion_program_name	");

		String orderByStr = new DMSSortQueryBuilder()
		.addMapper(SortActionConstants.CODE, "promotion_program_code")
		.addMapper(SortActionConstants.NAME, "promotion_program_name")
		.defaultOrderString(defaultOrderByStr.toString())
		.build(sortInfo);
		//add order string
		sqlQuery.append(orderByStr);

		String queryGetListProductForOrder = sqlQuery.toString();
		String getCountProductList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ queryGetListProductForOrder + ") ";

		PromotionProgrameModel modelData = new PromotionProgrameModel();
		List<PromotionProgrameDTO> list = new ArrayList<PromotionProgrameDTO>();

		Cursor cTmp = null;
		try {
			// get total row first
			if (checkLoadMore == false) {
				cTmp = rawQueries(getCountProductList, params);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				modelData.setTotal(total);
			}
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e1) {
				throw e1;
			}
		}
		Cursor c = null;
		try {
			// end
			c = rawQueries(queryGetListProductForOrder + ext, params);
			if (c != null) {
				if (c.moveToFirst()) {
					PromotionProgrameDTO orderJoinTableDTO = null;
					do {
						orderJoinTableDTO = this.initPromotionProgrameObjectFromGetStatement(c);
						list.add(orderJoinTableDTO);
					} while (c.moveToNext());
				}
			}
			modelData.setModelData(list);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				throw e2;
			}
		}
		return modelData;
	}

}
