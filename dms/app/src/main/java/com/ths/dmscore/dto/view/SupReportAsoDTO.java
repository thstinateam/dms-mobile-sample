/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

public class SupReportAsoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public String routeCode;
	public String routeName;
	public String staffName;
	public long numTarget;
	public long done;
	public long remain;
	public double percent;
	public long totalPlan;
	public long totalDone;
	public String typeName;
	public long staffId;
	public long routeId;
	public long shopId;
	public String staffCode;

	public SupReportAsoDTO() {
		routeCode = Constants.STR_BLANK;
		routeName = Constants.STR_BLANK;
		staffCode = Constants.STR_BLANK;
		staffName = Constants.STR_BLANK;
		typeName = Constants.STR_BLANK;
		numTarget = 0;
		done = 0;
		remain = 0;
		percent = 0;
		totalPlan = 0;
		totalDone = 0;
		staffId = 0;
		routeId = 0;
		shopId = 0;
	}

	public void initDataWithCursor(Cursor c) {
		routeId = CursorUtil.getLong(c, "ROUTING_ID");
		routeCode = CursorUtil.getString(c, "ROUTING_CODE");
		routeName = CursorUtil.getString(c, "ROUTING_NAME");
		staffId = CursorUtil.getLong(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		shopId = CursorUtil.getLong(c, "SHOP_ID");
		int type = CursorUtil.getInt(c, "OBJECT_TYPE");
		typeName = type == 1 ? StringUtil.getString(R.string.TEXT_ASO_TYPE_SKU)
				: (type == 2 ? StringUtil.getString(R.string.TEXT_ASO_TYPE_SUB_CAT) : "");
		numTarget = CursorUtil.getLong(c, "PLAN");
		done = CursorUtil.getLong(c, "DONE");
		remain = StringUtil.calRemain(numTarget, done);
		percent = StringUtil.calPercentUsingRound(numTarget, done);
		totalPlan = CursorUtil.getLong(c, "TOTAL_PLAN");
		totalDone = CursorUtil.getLong(c, "TOTAL_DONE");
	}
}
