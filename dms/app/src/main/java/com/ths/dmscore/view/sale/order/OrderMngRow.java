/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.view.SaleOrderViewDTO;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 *  Row trong danh sach don hang
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class OrderMngRow extends DMSTableRow {
	public static final int ACTION_CLICK_SDH = 0;
	public static final int ACTION_CLICK_MKH = 1;
	public static final int ACTION_CLICK_DELETE = 2;
	public static final int ACTION_CLICK_SELECT = 3;
	public static final int ACTION_VIEW_ORDER = 4;
	public static final int ACTION_RETURN_ORDER = 5;
	// textview STT
	private TextView tvSTT;
	// textview Ngay
	private TextView tvDay;
	// textview Khach hang
	private TextView tvMKH;
	// textview so don hang
	private TextView tvSDH;
	// textview thanh tien
	private TextView tvTT;
	// Linear action
	private LinearLayout llReturnOrder;
	// tong san luong
	private TextView tvQuantity;

	// Button btnDelete;

	private ImageView ivDelete;
	//private ImageView ivViewOrder;
	private TextView tvDescript;
	private ImageView ivReturnOrder;
	private LinearLayout llAction;
	boolean isVsible;

	private SaleOrderViewDTO saleOrderDTO;

	public OrderMngRow(Context context, VinamilkTableListener listen, boolean isVisible) {
		super(context, R.layout.mng_order_item, GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {R.id.tvTT},
				isVisible ? null : new int[] {R.id.llAction,R.id.ivDelete,R.id.llReturnOrder,R.id.ivReturnOrder});
		listener = listen;
		this.isVsible = isVisible;
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvDay = (TextView) findViewById(R.id.tvDay);
		tvMKH = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvMKH, PriHashMap.PriControl.NVBH_DSDONHANG_CHITIETKHACHHANG);
		PriUtils.getInstance().setOnClickListener(tvMKH, this);
		tvSDH = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvSDH, PriHashMap.PriControl.NVBH_DSDONHANG_CHITIETDONHANG);
		PriUtils.getInstance().setOnClickListener(tvSDH, this);

		tvTT = (TextView) findViewById(R.id.tvTT);
		tvDescript = (TextView) findViewById(R.id.tvDescript);
		//ivViewOrder = (ImageView) PriUtils.getInstance().findViewById(this, R.id.ivViewOrder, PriHashMap.PriControl.NVBH_DSDONHANG_XEMDONGOC);
		//PriUtils.getInstance().setOnClickListener(ivViewOrder, this);
		llAction = (LinearLayout) findViewById(R.id.llAction);
		ivDelete = (ImageView) PriUtils.getInstance().findViewById(this, R.id.ivDelete, PriHashMap.PriControl.NVBH_DSDONHANG_XOADONHANG);
		llReturnOrder = (LinearLayout) findViewById(R.id.llReturnOrder);
		ivReturnOrder = (ImageView) PriUtils.getInstance().findViewById(this, R.id.ivReturnOrder, PriHashMap.PriControl.NVBH_DSDONHANG_TRADONHANG);
		if(isVisible){
			PriUtils.getInstance().setOnClickListener(ivDelete, this);
			PriUtils.getInstance().setOnClickListener(ivReturnOrder, this);
		}
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
	}

	/**
	 * render row
	 *
	 * @author: dungdq3
	 * @since: 1:44:32 PM Apr 6, 2015
	 * @return: void
	 * @throws:
	 * @param position
	 * @param item
	 */
	public void renderLayout(int position, SaleOrderViewDTO item) {
		saleOrderDTO = item;
		display(tvSTT, position);
		tvDay.setText(DateUtils.formatDate(DateUtils.parseDateFromString(
				item.getOrderDate(), DateUtils.DATE_FORMAT_NOW),
				DateUtils.defaultDateFormat_3));
		if (item.customer != null){
			if (!StringUtil.isNullOrEmpty(item.customer.customerCode) && !StringUtil.isNullOrEmpty(item.customer.customerName)) {
				tvMKH.setText(item.customer.customerCode + '-' + item.customer.customerName.trim());
			} else if(!StringUtil.isNullOrEmpty(item.customer.customerCode)){
				tvMKH.setText(item.customer.customerCode);
			}
		}else{
			tvMKH.setText("");
		}
		tvSDH.setText(item.getOrderNumber());

		// bo sung *
		// Chi hien thi cho don hang trong ngay
		if ( !StringUtil.isNullOrEmpty(item.priorityCode) &&
				(ApParamDTO.PRIORITY_IN_DAY.equals(item.priorityCode) || ApParamDTO.PRIORITY_NOW.equals(item.priorityCode)) &&
				DateUtils.compareWithNow(item.saleOrder.createDate, "yyyy-MM-dd") == 0){
			SpannableObject objTotal = new SpannableObject();
			objTotal.addSpan("*", ImageUtil.getColor(R.color.RED), android.graphics.Typeface.BOLD);
			tvSDH.append(objTotal.getSpan());
		}

//		if (item.saleOrder.fromPOCustomerId > 0) {
//			PriUtils.getInstance().setStatus(ivViewOrder, PriUtils.ENABLE);
//		} else {
//			PriUtils.getInstance().setStatus(ivViewOrder, PriUtils.INVISIBLE);
//		}

		// ivReturnOrder chi xuat hien khi don vansale chua tra va cung ngay chot
		// va chua chot ngay valsale
 		LockDateDTO stockLock = GlobalInfo.getInstance().getLockDateValsale();
		int dateBetween = DateUtils
				.daysBetween(
						DateUtils.convertToDate(
								DateUtils
										.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD),
								DateUtils.DATE_STRING_YYYY_MM_DD),
						// DateUtils.convertToDate(GlobalInfo.getInstance()
						// .getShopLockDto().lockDate,
						DateUtils.convertToDate(item.saleOrder.maxReturn,
								DateUtils.DATE_STRING_YYYY_MM_DD));
		if (item.saleOrder.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)
				// don tra
				&& item.saleOrder.type != 2
				// don goc da tra
				&& item.saleOrder.type != 0 && item.saleOrder.approved != 3
				&& item.saleOrder.approved != 2 //Khac don sai KM
//				&& dateBetween <= GlobalInfo.getInstance().getSysMaxdayReturn()
				&& dateBetween >=0
				&& (stockLock == null || stockLock .vanLock != 1)) {
			if(isVsible){
				PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.ENABLE);
			}
		} else {
			if(isVsible){
				PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.INVISIBLE);
			}
		}
		if(isVsible){
			llReturnOrder.setVisibility(View.VISIBLE);
		}

		if(item.getTotal()>0.0){
			//String money= StringUtil.decimalFormatSymbols("#.##",item.getTotal());
			display(tvTT, item.getTotal());
		}else{
			display(tvTT, 0);
		}
		tvQuantity.setText(Constants.STR_BLANK + item.saleOrder.quantity);
		if (item.saleOrder.approved != 2 && item.saleOrder.approved != -1){//2: don hang tra ve
			if(isVsible){
				PriUtils.getInstance().setStatus(ivDelete, PriUtils.INVISIBLE);
			}
		}
		String synState = String.valueOf(item.saleOrder.synState);
		if(item.saleOrder.approved == 0 || (item.saleOrder.approved == 1 && !synState.equals(LogDTO.STATE_SUCCESS) )) {//Don hang da chuyen len server
			if(synState.equals(LogDTO.STATE_NEW)) {// Chua gui
				tvDescript.setText(StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND));
			} else if(synState.equals(LogDTO.STATE_FAIL)) {// Dang gui
				tvDescript.setText(StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND));
			} else if(synState.equals(LogDTO.STATE_SUCCESS)) {
				if (DateUtils.compareWithNow(item.saleOrder.orderDate, "dd/MM/yyyy") == -1){
					tvDescript.setText(StringUtil.getString(R.string.TEXT_OVERDATE_NOT_APROVE));
				}else{
					tvDescript.setText(StringUtil.getString(R.string.TEXT_WAITING_PROCESS));
				}
				//setBgOrderNonError();
			} else if(synState.equals(LogDTO.STATE_INVALID_TIME)) {//Sai thoi gian
				tvDescript.setText(StringUtil.getString(R.string.TEXT_ORDER_WRONG));
				if(isVsible){
					PriUtils.getInstance().setStatus(ivDelete, PriUtils.ENABLE);
					PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.INVISIBLE);
				}
				setBgOrderError();
			} else if(synState.equals(LogDTO.STATE_UNIQUE_CONTRAINTS)) {//Duplicate don hang
				tvDescript.setText(StringUtil.getString(R.string.TEXT_ORDER_WRONG));
				if(isVsible){
					PriUtils.getInstance().setStatus(ivDelete, PriUtils.ENABLE);
					PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.INVISIBLE);
				}
				setBgOrderError();
			}
		}else if (item.saleOrder.approved == 1){
			tvDescript.setText(StringUtil.getString(R.string.TEXT_SUCCESS));
		} else if (item.saleOrder.approved == 2){
			tvDescript.setText(item.getDescription());
			// don hang tu choi, can cap nhat
			setBackgroundRowByColor(ImageUtil.getColor(R.color.OGRANGE));
		} else if (item.saleOrder.approved == 3){
			tvDescript.setText(StringUtil.getString(R.string.TEXT_BUTTON_CANCEL));
			// set mau do
			setTextColorForRow(ImageUtil.getColor(R.color.RED));
			if(isVsible){
				//PriUtils.getInstance().setStatus(ivDelete, PriUtils.ENABLE);
			}
		} else if (item.saleOrder.approved == 4){
			// don huy sau chot
			tvDescript.setText(StringUtil.getString(R.string.TEXT_CANCEL_AFTER_LOCK));
			// set mau do
			setTextColorForRow(ImageUtil.getColor(R.color.RED));
			if(isVsible){
				//PriUtils.getInstance().setStatus(ivDelete, PriUtils.ENABLE);
				PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.INVISIBLE);
			}
		} else if (item.saleOrder.approved == -1) {// don hang draft
			tvDescript.setText(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_UNFINISH));
			setBackgroundRowByColor(ImageUtil.getColor(R.color.ORDER_NOT_FINISH));
			//PriUtils.getInstance().setStatus(ivViewOrder, PriUtils.INVISIBLE);
			if(isVsible){
				PriUtils.getInstance().setStatus(ivReturnOrder, PriUtils.INVISIBLE);
			}
		}else {
			tvDescript.setText(item.getDescription());
		}

		if(DateUtils.compare(item.saleOrder.maxApproved, DateUtils.now(), DateUtils.DATE_STRING_YYYY_MM_DD) == -1) {
			if(isVsible){
				PriUtils.getInstance().setStatus(ivDelete, PriUtils.INVISIBLE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		super.onClick(v);
		if (v == tvSDH){
			listener.handleVinamilkTableRowEvent(ACTION_CLICK_SDH, v, saleOrderDTO);
		} else if (v == tvMKH){
			listener.handleVinamilkTableRowEvent(ACTION_CLICK_MKH, v, saleOrderDTO);
		} else if (v == ivDelete){
			listener.handleVinamilkTableRowEvent(ACTION_CLICK_DELETE, v, saleOrderDTO);
		} else if (v == ivReturnOrder){
			listener.handleVinamilkTableRowEvent(ACTION_RETURN_ORDER, v, saleOrderDTO);
		}
//		else if (v == ivViewOrder){
//			listener.handleVinamilkTableRowEvent(ACTION_VIEW_ORDER, v, saleOrderDTO);
//		} 
		
	}

	/**
	 *
	*  Update background cho don hang loi
	*  @author: Nguyen Thanh Dung
	*  @return: void
	*  @throws:
	 */
	private void setBgOrderError() {
		tvSDH.setTextColor( ImageUtil.getColor(R.color.TITLE_LIGHT_BLACK_COLOR));
		tvMKH.setTextColor( ImageUtil.getColor(R.color.TITLE_LIGHT_BLACK_COLOR));
		setBackgroundRowByColor(ImageUtil.getColor(R.color.RED));
	}
}
