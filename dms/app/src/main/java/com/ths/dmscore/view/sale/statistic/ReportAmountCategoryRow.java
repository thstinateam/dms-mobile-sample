/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.dto.view.SupervisorReportStaffSaleCat;
import com.ths.dmscore.dto.view.SupervisorReportStaffSaleItemDTO;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Bao cao tien do doanh so nganh row
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 16:47:47 07-04-2015
 */
public class ReportAmountCategoryRow extends DMSTableRow implements
		OnClickListener {

	public static final int ACTION_CLICK_NPP_CODE = 0;
	public static final int ACTION_CLICK_GSNPP_NAME = 1;

	public static final int OBJECT_TYPE_USE_TBHV = 0;
	public static final int OBJECT_TYPE_USE_NPP = 1;
	// view
	// public View view;
	// ten NPP
	public TextView tvCustomerCode;
	// ten GSNPP
	public TextView tvCustomerName;
	// layout level
	public LinearLayout llCatList;
	// 0: report from TBHV sumary , 1: report from TBHV detail
	public int typeScreen = -1;
	SupervisorReportStaffSaleItemDTO data;
	private TableRow row;
	// view header
	private View vItemHeader;
	// ten nganh hang
	private TextView tvTitle;
	// ke hoach
	private TextView tvPlan;
	// thuc hien
	private TextView tvDone;
	// tien do
	private TextView tvProgress;

	private int action;

	public ReportAmountCategoryRow(Context context,
			ReportAmountCategoryView listener, int type) {
		super(context, R.layout.layout_report_category_row);
		this.typeScreen = type;
		setListener(listener);
		setOnClickListener(this);
		tvCustomerCode = (TextView) findViewById(R.id.tvCustomerCode);
		tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
		llCatList = (LinearLayout) findViewById(R.id.llCatList);

	}

	/**
	 * Khoi tao item nganh hang
	 * 
	 * @author: hoanpd1
	 * @since: 09:24:31 07-04-2015
	 * @return: void
	 * @throws:
	 * @param isHighlight
	 *            : co to mau background hay ko
	 */
	public void initializeHeader(boolean isHighlight) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		vItemHeader = inflater.inflate(R.layout.layout_gsnpp_report_staff_sale_header_item, null);
		tvTitle = (TextView) vItemHeader.findViewById(R.id.tvTitle);
		tvPlan = (TextView) vItemHeader.findViewById(R.id.tvPlan);
		tvDone = (TextView) vItemHeader.findViewById(R.id.tvDone);
		tvProgress = (TextView) vItemHeader.findViewById(R.id.tvProgress);
		if (isHighlight) {
			tvPlan.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_COLUM_REPORT_REMAIN));
			tvDone.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_COLUM_REPORT_REMAIN));
			tvProgress.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_COLUM_REPORT_REMAIN));
		}
		llCatList.addView(vItemHeader);
	}

	/**
	 * render data row
	 * 
	 * @author: hoanpd1
	 * @since: 09:16:57 07-04-2015
	 * @return: void
	 * @throws:
	 * @param item
	 * @param standarPercentAlow
	 * @param isSum
	 */
	public void render(SupervisorReportStaffSaleItemDTO item, double standarPercentAlow, boolean isSum) {
		this.data = item;
		if (isSum) {
			showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvCustomerCode, tvCustomerName);
		} else {
			tvCustomerCode.setText(item.objectCode);
			tvCustomerName.setText(item.objectName);
		}

		for (int i = 0, size = item.listFocusProductItem.size(); i < size; i++) {
			if (i % 2 == 1) {
				initializeHeader(true);
			} else {
				initializeHeader(false);
			}
			tvTitle.setVisibility(View.GONE);
			SupervisorReportStaffSaleCat rs = item.listFocusProductItem.get(i);
			display(tvPlan, rs.amountPlan);
			display(tvDone, rs.amount);
			displayPercent(tvProgress, rs.progressAmount);
			tvPlan.setTextColor(ImageUtil.getColor(R.color.BLACK));
			tvDone.setTextColor(ImageUtil.getColor(R.color.BLACK));
			setTextColorPercent(tvProgress, rs.progressAmount, standarPercentAlow, ImageUtil.getColor(R.color.RED));
			tvPlan.setTypeface(null, Typeface.NORMAL);
			tvDone.setTypeface(null, Typeface.NORMAL);
			tvProgress.setTypeface(null, Typeface.NORMAL);
		}
	}

	@Override
	public void onClick(View paramView) {
		// TODO Auto-generated method stub
		if (paramView == row && context != null) {
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
		} else if (paramView == tvCustomerName && listener != null) {
			listener.handleVinamilkTableRowEvent(action, paramView, data);
		}

	}

}
