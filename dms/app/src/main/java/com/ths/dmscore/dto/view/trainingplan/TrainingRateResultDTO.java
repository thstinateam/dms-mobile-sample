/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO.TableAction;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SUP_TRAINING_CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * tieu chi danh gia
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:54:26 PM Nov 14, 2014
 */
public class TrainingRateResultDTO implements Serializable {
	private static final long serialVersionUID = 7399752069339049364L;
	// ma TBHV
	private long subStaffID;
	private long supTrainingPlanID;
	// id cua kq danh gia
	private long supTrainingCustomerID;
	// thang huan luyen
	private String trainingMonth;
	// ma khach hang danh gia
	private long customerID;
	// ghi chu trong tieu chi
	private String note;
	// true: new -> insert, false: update 
	private boolean isNew;
	private TrainingRateDTO trainingRateDTO;
	// danh sach cac tieu chuan
	private ArrayList<TrainingRateDetailResultlDTO> listStandard;
	

	public TrainingRateResultDTO() {
		// TODO Auto-generated constructor stub
		subStaffID = 0;
		trainingMonth = Constants.STR_BLANK;
		customerID = 0;
		supTrainingCustomerID = 0;
		listStandard = new ArrayList<TrainingRateDetailResultlDTO>();
		trainingRateDTO = new TrainingRateDTO();
		supTrainingPlanID = 0;
		isNew = true;
		note = Constants.STR_BLANK;
	}

	public long getSubStaffID() {
		return subStaffID;
	}

	public void setSubStaffID(long subStaffID) {
		this.subStaffID = subStaffID;
	}

	public String getTrainingMonth() {
		return trainingMonth;
	}

	public void setTrainingMonth(String trainingMonth) {
		this.trainingMonth = trainingMonth;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public long getCustomerID() {
		return customerID;
	}

	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}

	public long getSupTrainingCustomerID() {
		return supTrainingCustomerID;
	}

	public void setSupTrainingCustomerID(long subTrainingCustomerID) {
		this.supTrainingCustomerID = subTrainingCustomerID;
	}

	public ArrayList<TrainingRateDetailResultlDTO> getListStandard() {
		return listStandard;
	}

	public void setListStandard(ArrayList<TrainingRateDetailResultlDTO> listStandard) {
		this.listStandard = listStandard;
	}
	
	/**
	 * khoi tao du lieu tu cursor
	 * 
	 * @author: dungdq3
	 * @since: 10:37:48 AM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 */
	public void initFromCursor(Cursor c){
		trainingRateDTO.initFromCursor(c);
		supTrainingCustomerID = CursorUtil.getLong(c, SUP_TRAINING_CUSTOMER_TABLE.SUP_TRAINING_CUSTOMER_ID);
		note = CursorUtil.getString(c, SUP_TRAINING_CUSTOMER_TABLE.NOTE);
		TrainingRateDetailResultlDTO rateDetaiResultlDTO = new TrainingRateDetailResultlDTO();
		rateDetaiResultlDTO.initFromCursor(c);
		rateDetaiResultlDTO.setTrainingRateID(trainingRateDTO.getTrainingRateID());
		if(rateDetaiResultlDTO.getTrainingRateDetailID() > 0)
			listStandard.add(rateDetaiResultlDTO);
	}

	public TrainingRateDTO getTrainingRateDTO() {
		return trainingRateDTO;
	}

	public void setTrainingRateDTO(TrainingRateDTO trainingRateDTO) {
		this.trainingRateDTO = trainingRateDTO;
	}
	
	/**
	 * kich thuoc list
	 * 
	 * @author: dungdq3
	 * @since: 3:25:08 PM Nov 17, 2014
	 * @return: int
	 * @throws:  
	 * @return
	 */
	public int getSizeOfList(){
		return listStandard.size();
	}
	
	/**
	 * lay object cuoi cung trong list tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 4:32:43 PM Nov 17, 2014
	 * @return: TrainingRateDetailResultlDTO
	 * @throws:  
	 * @return
	 */
	public TrainingRateDetailResultlDTO getLastItem(){
		TrainingRateDetailResultlDTO resultDTO = null;
		if(listStandard.size() > 0)
			resultDTO = listStandard.get(listStandard.size()-1);
		return resultDTO;
	}

	/**
	 * add them 1 item moi vao list
	 * 
	 * @author: dungdq3
	 * @since: 4:39:21 PM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 * @param rateResultDTO
	 */
	public void addRateDetailResult(TrainingRateDetailResultlDTO rateResultDTO) {
		// TODO Auto-generated method stub
		rateResultDTO.setTrainingRateID(trainingRateDTO.getTrainingRateID());
		listStandard.add(rateResultDTO);
	}

	/**
	 * xoa object tai vi tri duoc chon
	 * 
	 * @author: dungdq3
	 * @since: 4:44:23 PM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 * @param indexDelete
	 */
	public void removeObject(int indexDelete) {
		// TODO Auto-generated method stub
		listStandard.remove(indexDelete);
	}
	
	public void removeObject(TrainingRateDetailResultlDTO dto) {
		int i = 0;
		boolean isHave = false;
		for (TrainingRateDetailResultlDTO resultDTO : listStandard) {
			if(resultDTO.getOrderNumber() == dto.getOrderNumber()) {
				isHave = true;
				break;
			}
			i++;
		}
		if(isHave)
			listStandard.remove(i);
	}

	public long getSupTrainingPlanID() {
		return supTrainingPlanID;
	}

	public void setSupTrainingPlanID(long supTrainingPlanID) {
		this.supTrainingPlanID = supTrainingPlanID;
	}

	/**
	 * get training rate id
	 * 
	 * @author: dungdq3
	 * @since: 5:32:12 PM Nov 17, 2014
	 * @return: String
	 * @throws:  
	 * @return
	 */
	public long getTrainingRateID() {
		// TODO Auto-generated method stub
		return trainingRateDTO.getTrainingRateID();
	}
	
	public JSONObject generateTrainingRate() throws JSONException{
		JSONObject json = null;
		if(isNew){
			json = generateInsertTrainingRate();
		} else {
			json = generateUpdateTrainingRate();
		}
		return json;
	}
	
	public JSONObject generateInsertTrainingRate() throws JSONException {
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_CUSTOMER_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.SUP_TRAINING_CUSTOMER_ID, supTrainingCustomerID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.SUP_TRAINING_PLAN_ID, supTrainingPlanID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.TRAINING_RATE_ID, trainingRateDTO.getTrainingRateID(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.CUSTOMER_ID, customerID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.NOTE, note, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.CREATE_DATE, trainingRateDTO.getCreateDate(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.CREATE_USER, trainingRateDTO.getCreateUser(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.CREATE_STAFF_ID, subStaffID, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
			throw e;
		}

		return trainingRateJson;
	}
	
	public JSONObject generateUpdateTrainingRate() throws JSONException {
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_CUSTOMER_TABLE.TABLE_NAME);
			
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.NOTE, note, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.UPDATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_CUSTOMER_TABLE.SUP_TRAINING_CUSTOMER_ID, supTrainingCustomerID, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	public boolean checkSaveListStandard(){
		boolean flag = true;
		if (listStandard != null && listStandard.size() > 0) {
			TrainingRateDetailResultlDTO dto = listStandard.get(listStandard
					.size() - 1);
			if (StringUtil.isNullOrEmpty(dto.getFullStandardName()))
				flag = false;
		}
		return flag;
	}
}
