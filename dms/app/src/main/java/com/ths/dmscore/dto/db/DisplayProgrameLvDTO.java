/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_LEVEL_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Muc cua chuong trinh trung bay
 * 
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class DisplayProgrameLvDTO extends AbstractTableDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ma CTTB
	public String displayProgrameCode;
	// muc CTTB
	public String levelCode;
	// so tien cua muc
	public int amount;
	// tu ngay
	public String fromDate;
	// den ngay
	public String toDate;
	// trang thai
	public int status;
	// long diplay program id
	public long dislayProgramId;

	public DisplayProgrameLvDTO() {
		super(TableType.DISPLAY_PROGRAME_LEVEL_TABLE);
	}

	/**
	 * 
	 * parse data with cursor
	 * 
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 * @since: Feb 19, 2013
	 */
	public void parseWithCursor(Cursor c) {
		this.dislayProgramId = CursorUtil.getLong(c, DISPLAY_PROGRAME_LEVEL_TABLE.DISPLAY_PROGRAM_ID, 0);
		this.levelCode = CursorUtil.getString(c, DISPLAY_PROGRAME_LEVEL_TABLE.LEVEL_CODE, "");
		this.fromDate = CursorUtil.getString(c, DISPLAY_PROGRAME_LEVEL_TABLE.FROM_DATE, "");
		this.toDate = CursorUtil.getString(c, DISPLAY_PROGRAME_LEVEL_TABLE.TO_DATE, "");
	}
}
