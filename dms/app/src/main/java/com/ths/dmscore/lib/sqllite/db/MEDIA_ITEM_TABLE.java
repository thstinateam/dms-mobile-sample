/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.db.TakePhotoEquipmentDTO;
import com.ths.dmscore.dto.view.ListAlbumUserDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentImageViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.dto.view.ImageListItemDTO;
import com.ths.dmscore.dto.view.VoteKeyShopImageViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 *
 * Table cho media item
 *
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class MEDIA_ITEM_TABLE extends ABSTRACT_TABLE {
	// id media
	public static final String MEDIA_ITEM_ID = "MEDIA_ITEM_ID";
	// ma khach hang / ma chuong trinh
	public static final String OBJECT_ID = "OBJECT_ID";
	// url cua hinh anh
	public static final String URL = "URL";
	// display programe id
//	public static final String DISPLAY_PROGRAME_ID = "DISPLAY_PROGRAM_ID";
	// thumnail cua hinh anh
	public static final String THUMB_URL = "THUMB_URL";
	// title cua hinh anh
	public static final String TITLE = "TITLE";
	// loai hinh anh (trung bay, khach hang, cua san pham, ...)
	public static final String MEDIA_TYPE = "MEDIA_TYPE";
	// mo ta hinh anh
	public static final String DESCRIPTION = "DESCRIPTION";
	// kich thuoc hinh anh
	public static final String FILE_SIZE = "FILE_SIZE";
	// chieu rong cua hinh anh
	public static final String WIDTH = "WIDTH";
	// chieu cao cua hinh anh
	public static final String HEIGHT = "HEIGHT";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// lat
	public static final String LAT = "LAT";
	// lng
	public static final String LNG = "LNG";
	// loai thuc the chua hinh anh
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// url tren client
	public static final String CLIENT_THUMB_URL = "CLIENT_THUMB_URL";
	// staff id
	public static final String STAFF_ID = "STAFF_ID";
	// table name
	public static final String TABLE_MEDIA_ITEM = "MEDIA_ITEM";
	// status
	public static final String STATUS = "STATUS";
	// display programe id
	public static final String DISPLAY_PROGRAM_ID = "DISPLAY_PROGRAM_ID";
	// type
//	public static final String TYPE = "TYPE";
	// shopId
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CYCLE_ID = "CYCLE_ID";
	public static final String ROUTING_ID = "ROUTING_ID";
//	public static final String MONTH_SEQ = "MONTH_SEQ";

	public MEDIA_ITEM_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_MEDIA_ITEM;
		this.columns = new String[] { MEDIA_ITEM_ID, OBJECT_ID, URL, THUMB_URL,
				TITLE, MEDIA_TYPE, DESCRIPTION, FILE_SIZE, WIDTH, HEIGHT,
				CREATE_DATE, UPDATE_DATE, CREATE_USER, UPDATE_USER, LAT, LNG,
				OBJECT_TYPE, STAFF_ID, STATUS, DISPLAY_PROGRAM_ID,
				SHOP_ID,ROUTING_ID, CYCLE_ID };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		MediaItemDTO mediaDTO = (MediaItemDTO) dto;
		ContentValues value = initDataRow(mediaDTO);
		return insert(null, value);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		MediaItemDTO mediaDTO = (MediaItemDTO) dto;
		ContentValues value = initDataRow(mediaDTO);
		String[] params = { "" + mediaDTO.id };
		return update(value, MEDIA_ITEM_ID + " = ?", params);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		MediaItemDTO mediaDTO = (MediaItemDTO) dto;
		String[] params = { "" + mediaDTO.id };
		return delete(MEDIA_ITEM_ID + " = ?", params);
	}

	private ContentValues initDataRow(MediaItemDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(MEDIA_ITEM_ID, dto.id);
		editedValues.put(OBJECT_ID, dto.objectId);
		editedValues.put(URL, dto.url);
		editedValues.put(THUMB_URL, dto.thumbUrl);
		editedValues.put(TITLE, dto.title);
		editedValues.put(MEDIA_TYPE, dto.mediaType);
		editedValues.put(FILE_SIZE, dto.fileSize);
		editedValues.put(WIDTH, dto.width);
		editedValues.put(HEIGHT, dto.height);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(DISPLAY_PROGRAM_ID, dto.displayProgrameId);
		editedValues.put(LAT, dto.lat);
		editedValues.put(LNG, dto.lng);
		editedValues.put(OBJECT_TYPE, dto.objectType);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(STATUS, dto.status);
//		editedValues.put(DISPLAY_PROGRAME_ID, dto.displayProgrameId);
//		editedValues.put(TYPE, dto.type);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(CYCLE_ID, dto.cycleId);
		editedValues.put(ROUTING_ID, dto.routingId);
//		editedValues.put(MONTH_SEQ, dto.monthSeq);
		return editedValues;
	}

	public MediaItemDTO getRowById(String id) {
		MediaItemDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(MEDIA_ITEM_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	/**
	 * Lay ds hinh anh cua CTTB
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	 */
	public ArrayList<MediaItemDTO> getListImageCTTB(String customerId,
			int objectType, String displayProgrameId, String shopId, String staffId) throws Exception {
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<MediaItemDTO> dto = null;
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT mi.url                 URL, ");
		sqlObject
				.append("                mi.MEDIA_ITEM_ID       MEDIA_ITEM_ID, ");
		sqlObject.append("                mi.thumb_url           THUMB_URL, ");
		sqlObject
				.append("                mi.create_date          CREATE_DATE, ");
		// sqlObject.append("                mi.object_type         OBJECT_TYPE, ");
		sqlObject
				.append("                mi.display_program_id DISPLAY_PROGRAM_ID ");
		sqlObject.append("FROM   media_item mi ");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add("" + customerId);
		sqlObject.append("       AND mi.object_type = ? ");
		params.add("" + objectType);
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append("       AND mi.status = 1 ");
//		sqlObject.append("       AND mi.type = 1 ");
		sqlObject
				.append("       AND Date(mi.create_date) = Date('NOW', 'localtime') ");
		sqlObject.append("       AND mi.display_program_id = ? ");
		params.add("" + displayProgrameId);
		sqlObject.append("       AND mi.shop_id = ? ");

		params.add("" + shopId);
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add("" + staffId);
		sqlObject.append(" order by datetime(mi.create_date) desc ");

		Cursor c = null;
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null) {
				dto = new ArrayList<MediaItemDTO>();
				if (c.moveToFirst()) {
					do {
						MediaItemDTO item = initMediaCTTB(c);
						dto.add(item);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}

		}
		return dto;
	}

	/**
	 *
	 * @author: Tuanlt11
	 * @param c
	 * @return
	 * @return: MediaItemDTO
	 * @throws:
	 */
	private MediaItemDTO initMediaCTTB(Cursor c) {
		MediaItemDTO mediaDTO = new MediaItemDTO();
		mediaDTO.id = (CursorUtil.getLong(c, MEDIA_ITEM_ID));
		mediaDTO.url = (CursorUtil.getString(c, URL));
		mediaDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		mediaDTO.thumbUrl = (CursorUtil.getString(c, THUMB_URL));
		mediaDTO.displayProgrameId = CursorUtil.getLong(c, DISPLAY_PROGRAM_ID);

		return mediaDTO;
	}

	/**
	 *
	 * Ham gan gia tri cho dto from ket qua cua cau lenh sql
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: MediaItemDTO
	 * @throws:
	 */
	private MediaItemDTO initLogDTOFromCursor(Cursor c) {
		MediaItemDTO mediaDTO = new MediaItemDTO();
		mediaDTO.id = (CursorUtil.getLong(c, MEDIA_ITEM_ID));
		mediaDTO.objectId = (CursorUtil.getLong(c, OBJECT_ID));
		mediaDTO.url = (CursorUtil.getString(c, URL));
		mediaDTO.thumbUrl = (CursorUtil.getString(c, THUMB_URL));
		mediaDTO.title = (CursorUtil.getString(c, TITLE));
		mediaDTO.mediaType = (CursorUtil.getInt(c, MEDIA_TYPE));
		mediaDTO.fileSize = (CursorUtil.getLong(c, FILE_SIZE));
		mediaDTO.width = (CursorUtil.getInt(c, WIDTH));
		mediaDTO.height = (CursorUtil.getInt(c, HEIGHT));
		mediaDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		mediaDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		mediaDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		mediaDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		mediaDTO.lat = (CursorUtil.getFloat(c, LAT));
		mediaDTO.lng = (CursorUtil.getFloat(c, LNG));
		mediaDTO.objectType = (CursorUtil.getInt(c, OBJECT_TYPE));
		mediaDTO.staffId = (CursorUtil.getInt(c, STAFF_ID));
		mediaDTO.status = CursorUtil.getInt(c, STATUS);
		mediaDTO.displayProgrameId = CursorUtil.getLong(c, DISPLAY_PROGRAM_ID);
//		mediaDTO.type = CursorUtil.getInt(c, TYPE);
		mediaDTO.shopId = CursorUtil.getInt(c, SHOP_ID);

		return mediaDTO;
	}

	/**
	 *
	 * Ham lay danh sach media ho tro cho man hinh gioi thieu san pham
	 *
	 * @author: ThanhNN8
	 * @param c
	 * @return
	 * @return: MediaItemDTO
	 * @throws:
	 */
	private MediaItemDTO initForListMedia(Cursor c) {
		MediaItemDTO mediaDTO = new MediaItemDTO();
		mediaDTO.id = (CursorUtil.getLong(c, MEDIA_ITEM_ID));
		mediaDTO.objectId = (CursorUtil.getLong(c, OBJECT_ID));
		mediaDTO.url = CursorUtil.getString(c, URL);
		mediaDTO.thumbUrl = CursorUtil.getString(c, THUMB_URL);
		mediaDTO.mediaType = (CursorUtil.getInt(c, MEDIA_TYPE));
		mediaDTO.sdCardPath = (CursorUtil.getString(c, CLIENT_THUMB_URL));
		mediaDTO.media_id = (CursorUtil.getLong(c, "MEDIA_ID"));
		return mediaDTO;
	}

	/**
	 * Lay danh sach media theo productId
	 *
	 * @author: ThanhNN8
	 * @update: ToanTT
	 * @param productId
	 * @return
	 * @return: List<MediaItemDTO>
	 * @throws:
	 */
	public List<MediaItemDTO> getListMediaOfProduct(String productId) {
		List<MediaItemDTO> result = new ArrayList<MediaItemDTO>();
		StringBuffer var1 = new StringBuffer();

		var1.append("	select *, null as MEDIA_ID ");
		var1.append("	from media_item ");
		var1.append("	where object_id = ? ");
		var1.append("	and object_type = 3 ");
		var1.append("	and media_type = 0 ");
		var1.append("	and status = 1 ");
		var1.append("	union ");
		//sua media_id thanh media_item_id de insert vao medialog
		var1.append("	select mi.*, mi.media_item_id as MEDIA_ID ");
		var1.append("	from media_item mi ");
		var1.append("	where 1 = 1 ");
//		var1.append("	and m.media_item_id = mi.media_item_id ");
//		var1.append("	and m.status = 1 ");
//		var1.append("	and mm.object_type = 1 ");
		var1.append("	and mi.object_id = ?  ");
//		var1.append("	and mm.status = 1 ");
		var1.append("	and mi.status = 1 ");
//		var1.append("	and mi.object_type = 7 ");
		var1.append("	and mi.object_type = 3 ");
		var1.append("	and mi.media_type = 1 ");
		var1.append("	order by media_type desc ");

		String[] params = { productId, productId };
		Cursor c = null;
		try {
			c = rawQuery(var1.toString(), params);
			if (c != null) {
				MediaItemDTO mediaItemDTO;
				if (c.moveToFirst()) {
					do {
						mediaItemDTO = initForListMedia(c);
						result.add(mediaItemDTO);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {

			}
		}
		return result;
	}

	public int updateClientThumbnailUrl(String clientThumbnailUrl, long mediaId) {
		ContentValues args = new ContentValues();
		args.put(CLIENT_THUMB_URL, clientThumbnailUrl);
		return mDB.update(TABLE_MEDIA_ITEM, args,
				MEDIA_ITEM_ID + "=" + mediaId, null);
	}

	/**
	 * Lay thong tin album cua user
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param shopId
	 * @return: void
	 * @throws:
	 */

	public ListAlbumUserDTO getAlbumUserInfo(String customerId, String shopId) throws Exception {
		String startOfTwoPreviousMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -2);
		String[] params = { customerId, shopId, startOfTwoPreviousMonth };
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("select *, count(*) NUM_IMAGE from ( ");
		sqlObject.append("SELECT url, ");
		sqlObject.append("       thumb_url THUMB_URL, ");
		sqlObject.append("       object_type OBJECT_TYPE ");
		sqlObject.append("FROM   media_item mi ");
		sqlObject.append("WHERE  object_id = ? ");
		sqlObject.append(" and shop_id = ? ");
//		sqlObject.append(" and type = 1");
		sqlObject.append(" and status = 1");
		sqlObject.append("       AND object_type IN ( 0, 1, 2 ) ");
		sqlObject.append("       AND media_type = 0 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  ?");
		sqlObject.append(" order by object_type, datetime(create_date))  ");
		sqlObject.append("GROUP  BY object_type ");

		Cursor c = null;
		ListAlbumUserDTO data = new ListAlbumUserDTO();
		for (int i = 0; i < 3; i++) {
			AlbumDTO dto = new AlbumDTO();
			dto.setAlbumType(i);
			data.getListAlbum().add(dto);
		}
		try {
			c = rawQuery(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						int object_type = CursorUtil.getInt(c, "OBJECT_TYPE");
						data.getListAlbum().get(object_type).setThumbUrl(CursorUtil.getString(c, THUMB_URL));
						data.getListAlbum().get(object_type).setNumImage(CursorUtil.getInt(c, "NUM_IMAGE"));
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return data;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param page
	 * @param shopId
	 * @return
	 * @return: ArrayList<PhotoDTO>
	 * @throws:
	 */

	public ArrayList<PhotoDTO> getAlbumDetailUser(String customerId,
			String type, String numTop, String page, String shopId) {
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT media_item_id, ");
		sqlObject.append("       thumb_url, ");
		sqlObject.append("       url, ");
		sqlObject
				.append("       Strftime('%d/%m/%Y - %H:%M', mi.create_date) AS CREATE_DATE, ");
		sqlObject.append("       lat, ");
		sqlObject.append("       lng, ");
		sqlObject
				.append("       st.staff_name                             AS STAFF_NAME, ");
		sqlObject
				.append("       st.staff_code                             AS STAFF_CODE, ");
		sqlObject.append("       mi.create_user  as CREATE_USER ");
		sqlObject.append("FROM   media_item mi ");
		sqlObject.append("       LEFT JOIN staff st ");
		sqlObject.append("              ON mi.staff_id = st.staff_id ");
		sqlObject.append("WHERE  object_id = ? ");
		sqlObject.append("       AND object_type = ? ");
//		sqlObject.append("       AND mi.type = 1 ");
		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND mi.media_type = 0 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  dayInOrder('NOW', 'localtime','start of month','-2 month')");

		ArrayList<String> params = new ArrayList<String>();
		params.add(customerId);
		params.add(type);

		if (!StringUtil.isNullOrEmpty(shopId)) {
			sqlObject.append("       AND mi.shop_id = ? ");
			params.add(shopId);
		}

		sqlObject.append(" ORDER BY datetime(mi.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add(numTop);
		int offset = Integer.parseInt(page) * Integer.parseInt(numTop);
		params.add(String.valueOf(offset));

		Cursor c = null;
		ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					PhotoDTO photoDTO = initPhotoInfo(c);
					listPhoto.add(photoDTO);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			MyLog.e("getAlbumDetailUser", "fail", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}

		return listPhoto;
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return
	 * @return: PhotoDTO
	 * @throws:
	 */

	private PhotoDTO initPhotoInfo(Cursor c) {
		PhotoDTO photoDTO = new PhotoDTO();
		photoDTO.thumbUrl = CursorUtil.getString(c, THUMB_URL);
		photoDTO.fullUrl = CursorUtil.getString(c, URL);
		photoDTO.createdTime = CursorUtil.getString(c, CREATE_DATE);
		photoDTO.mediaId = CursorUtil.getString(c, MEDIA_ITEM_ID);
		photoDTO.lat = CursorUtil.getDouble(c, LAT);
		photoDTO.lng = CursorUtil.getDouble(c, LNG);
		photoDTO.staffCode = CursorUtil.getString(c, "STAFF_CODE");
		photoDTO.staffName = CursorUtil.getString(c, "STAFF_NAME");
		if (StringUtil.isNullOrEmpty(photoDTO.staffName)) {
			photoDTO.staffName = "";
		}

		photoDTO.createUser = CursorUtil.getString(c, CREATE_USER);
		return photoDTO;
	}

	/**
	 * Request upload photo
	 *
	 * @author: PhucNT
	 * @param mediaDTO
	 * @return: void
	 * @throws:
	 */
	public int updateNewLink(MediaItemDTO mediaDTO) {
		// TODO Auto-generated method stub
		ContentValues args = new ContentValues();
		args.put(THUMB_URL, mediaDTO.thumbUrl);
		args.put(URL, mediaDTO.url);
		return mDB.update(TABLE_MEDIA_ITEM, args, MEDIA_ITEM_ID + "="
				+ mediaDTO.id, null);
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param lastMediaId
	 * @param lastCreatedTime
	 * @return
	 * @return: ArrayList<PhotoDTO>
	 * @throws:
	 */

	public ArrayList<PhotoDTO> getSupervisorAlbumDetailUser(String customerId,
			String type, String numTop, String page, String shopId) {
		StringBuffer sqlObject = new StringBuffer();
		sqlObject
				.append("SELECT mi.media_item_id                                AS MEDIA_ITEM_ID, ");
		sqlObject
				.append("       mi.thumb_url                                    AS THUMB_URL, ");
		sqlObject
				.append("       url                                          AS URL, ");
		sqlObject
				.append("       Strftime('%d/%m/%Y - %H:%M', MI.create_date) AS CREATE_DATE, ");
		sqlObject
				.append("       mi.lat                                          AS LAT, ");
		sqlObject
				.append("       mi.lng                                          AS LNG, ");
		sqlObject
				.append("       st.staff_name                                AS STAFF_NAME, ");
		sqlObject
				.append("       st.staff_code                                AS STAFF_CODE, ");
		sqlObject
				.append("       MI.create_user                               AS CREATE_USER ");
		sqlObject.append("FROM   media_item mi ");
		sqlObject.append("       LEFT JOIN staff st ");
		sqlObject.append("              ON mi.staff_id = st.staff_id ");
		sqlObject.append("WHERE  object_id = ? ");
		sqlObject.append("       AND object_type = ? ");
//		sqlObject.append("       AND mi.type = 1 ");
		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND mi.media_type = 0 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  dayInOrder('NOW', 'localtime','start of month','-2 month')");

		ArrayList<String> params = new ArrayList<String>();
		params.add(customerId);
		params.add(type);
		if (!StringUtil.isNullOrEmpty(shopId)) {
			sqlObject.append("       AND mi.shop_id = ? ");
			params.add(shopId);
		}

		sqlObject.append(" ORDER BY datetime(mi.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add(numTop);
		int offset = Integer.parseInt(page) * Integer.parseInt(numTop);
		params.add(String.valueOf(offset));

		Cursor c = null;
		ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					PhotoDTO photoDTO = initPhotoInfo(c);
					listPhoto.add(photoDTO);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				c.close();
			}
		}

		return listPhoto;
	}

	/**
	 * ham lay danh sach album theo chuong trinh, ngay chup
	 *
	 * @author thanhnn
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param lastMediaId
	 * @param lastCreatedTime
	 * @param programeId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public ArrayList<PhotoDTO> getAlbumDetailPrograme(String customerId,
			String type, String numTop, String page, String programeCode,
			String fromDate, String toDate, String shopId) throws Exception {
		ArrayList<String> params = new ArrayList<String>();
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		// ST
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT mi.media_item_id                             AS MEDIA_ITEM_ID, ");
		var1.append("       mi.thumb_url                                 AS THUMB_URL, ");
		var1.append("       mi.url                                       AS URL, ");
		var1.append("       Strftime('%d/%m/%Y - %H:%M', MI.create_date) AS CREATE_DATE, ");
		var1.append("       mi.lat                                       AS LAT, ");
		var1.append("       mi.lng                                       AS LNG, ");
		var1.append("       st.staff_name                                AS STAFF_NAME, ");
		var1.append("       st.staff_code                                AS STAFF_CODE, ");
		var1.append("       MI.create_user                               AS CREATE_USER, ");
		var1.append("       MI.display_program_id                        AS DISPLAY_PROGRAME_ID ");
		var1.append("FROM   (SELECT * ");
		var1.append("        FROM   media_item ");
		var1.append("        WHERE  object_type IN ( 0, 1, 2 ) ");
		var1.append("               AND status = 1 ");
//		var1.append("               AND type = 1 ");
		var1.append("               AND Date(create_date) >= Date('now', 'localtime', ");
		var1.append("                                        'start of month', ");
		var1.append("                                        '-2 month') ");
		var1.append("               AND media_type = 0 ");
		var1.append("               AND shop_id = ? ");
		params.add(shopId);
		var1.append("               AND object_id = ? ");
		params.add(customerId);
		var1.append("        UNION ");
		var1.append("        SELECT mi.* ");
		var1.append("        FROM   media_item mi ");
		var1.append("               JOIN ks dp ");
		var1.append("                 ON mi.display_program_id = dp.ks_id ");
		var1.append("               JOIN ks_shop_map ksm  ON ksm.ks_id = dp.ks_id ");
		var1.append("        WHERE  mi.object_type = 4 ");
		var1.append("               AND mi.status = 1 ");
		var1.append("               AND dp.status = 1 ");
		var1.append("               AND ksm.status = 1 ");
		var1.append("               AND Date(mi.create_date) >= Date('NOW', 'localtime', ");
		var1.append("                                           'start of month', ");
		var1.append("                                           '-2 month') ");
		var1.append("      and  ? >= dp.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		var1.append("      and  ? <= dp.TO_CYCLE_ID");
		params.add(""+cycleId);
		var1.append(" AND ksm.shop_id IN ");
		var1.append("       (").append(idShopList).append(")");
//		var1.append("               AND Ifnull(Date(dp.from_date) <= Date('NOW', 'localtime'), 0) ");
//		var1.append("               AND ( dp.to_date IS NULL ");
//		var1.append("                      OR Date(dp.to_date) >= Date('NOW', 'localtime', ");
//		var1.append("                                             'start of month', ");
//		var1.append("                                             '-2 month') ) ");
		if (!StringUtil.isNullOrEmpty(programeCode) && !programeCode.equalsIgnoreCase("0")
				&& !programeCode.equalsIgnoreCase("-1")) {
			var1.append(" and dp.ks_id =  ?");
			params.add(programeCode);
		}
		var1.append("               AND mi.media_type = 0 ");
		var1.append("               AND mi.shop_id = ? ");
		params.add(shopId);
		var1.append("               AND object_id = ?) MI ");
		params.add(customerId);
		var1.append("       LEFT JOIN staff st ");
		var1.append("              ON mi.staff_id = st.staff_id ");
		var1.append("WHERE  1 = 1 ");
		if (Integer.valueOf(type) == MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE) {
			var1.append("       AND mi.object_type = 4 ");
		} else {
			var1.append("       AND mi.object_type in (0, 1, 2, 4) ");
		}
		var1.append("       AND mi.media_type = 0 ");
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			var1.append(" and dayInOrder(mi.create_date) >=  dayInOrder(?)");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			var1.append(" and dayInOrder(mi.create_date) <=  dayInOrder(?)");
			params.add(toDate);
		}
		var1.append("ORDER  BY Datetime(MI.create_date) DESC ");

		var1.append(" limit ? offset ?");
		params.add(numTop);
		int offset = Integer.parseInt(page) * Integer.parseInt(numTop);
		params.add(String.valueOf(offset));

		Cursor c = null;
		ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
		try {
			c = rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					PhotoDTO photoDTO = initPhotoInfo(c);
					listPhoto.add(photoDTO);
				} while (c.moveToNext());
			}
		}  finally {
			if (c != null) {
				c.close();
			}
		}

		return listPhoto;
	}

	/**
	 * ham lay danh sach album theo chuong trinh, ngay chup cho chuc nang gsnpp
	 *
	 * @author thanhnn
	 * @param customerId
	 * @param type
	 * @param numTop
	 * @param lastMediaId
	 * @param lastCreatedTime
	 * @param programeId
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public ArrayList<PhotoDTO> getSuperVisorAlbumDetailPrograme(
			String customerId, String type, String numTop, String page,
			String programeCode, String fromDate, String toDate) {
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject
				.append("SELECT mi.media_item_id                                AS MEDIA_ITEM_ID, ");
		sqlObject
				.append("       mi.thumb_url                                    AS THUMB_URL, ");
		sqlObject
				.append("       mi.url                                          AS URL, ");
		sqlObject
				.append("       Strftime('%d/%m/%Y - %H:%M', MI.create_date) AS CREATE_DATE, ");
		sqlObject
				.append("       mi.lat                                          AS LAT, ");
		sqlObject
				.append("       mi.lng                                          AS LNG, ");
		sqlObject
				.append("       st.staff_name                                AS STAFF_NAME, ");
		sqlObject
				.append("       st.staff_code                                AS STAFF_CODE, ");
		sqlObject
				.append("       MI.create_user                               AS CREATE_USER ");
		sqlObject
				.append("FROM   media_item mi left join display_shop_map dp ON mi.display_program_id = dp.display_program_id and dp.status = 1 ");
		sqlObject.append("       LEFT JOIN staff st ");
		sqlObject.append("              ON mi.staff_id = st.staff_id ");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add(customerId);
//		sqlObject.append("       AND mi.type = 1 ");
		sqlObject.append("       AND mi.status = 1 ");

		if (Integer.valueOf(type) == 4) {
			sqlObject.append("       AND mi.object_type = 4 ");
		} else {
			sqlObject.append("       AND mi.object_type in (0, 1, 2, 4) ");
		}
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject
				.append(" and case when mi.object_type = 4 then dp.status = 1 else 1 end ");

		// add them status cho display_shop_map
		sqlObject.append(" and dp.status = 1 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  dayInOrder('NOW', 'localtime','start of month','-2 month')");

		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) >=  dayInOrder(?)");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) <=  dayInOrder(?)");
			params.add(toDate);
		}
		if (!StringUtil.isNullOrEmpty(programeCode)) {
			sqlObject.append(" and dp.display_program_code =  ?");
			params.add(programeCode);
		}

		sqlObject.append(" ORDER BY datetime(MI.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add(numTop);
		int offset = Integer.parseInt(page) * Integer.parseInt(numTop);
		params.add(String.valueOf(offset));

		Cursor c = null;
		ArrayList<PhotoDTO> listPhoto = new ArrayList<PhotoDTO>();
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					PhotoDTO photoDTO = initPhotoInfo(c);
					listPhoto.add(photoDTO);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				c.close();
			}
		}

		return listPhoto;
	}

	/**
	 *
	 * Lay hinh anh cho 1 CTTB
	 *
	 * @author: TRUNGHQM
	 * @param programeCode
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return: ArrayList<AlbumDTO>
	 * @throws:
	 */
	public ArrayList<AlbumDTO> getAlbumProgrameInfo(String programeCode,
			String customerId, String fromDate, String toDate, String shopId) throws Exception {
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("select *, count(*) NUM_IMAGE from ( ");
		sqlObject.append("SELECT distinct mi.url, ");
		sqlObject.append("       mi.thumb_url THUMB_URL, ");
		sqlObject.append("       mi.object_type OBJECT_TYPE, ");
		sqlObject.append("       mi.display_program_id DISPLAY_PROGRAM_ID, ");
		sqlObject
				.append("       dp.display_program_code DISPLAY_PROGRAM_CODE ");
		sqlObject
				.append("FROM   media_item mi join display_shop_map dp on mi.display_program_id = dp.display_program_id ");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add(customerId);
		sqlObject.append("       AND mi.object_type = 5 ");
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append(" and mi.status = 1");
//		sqlObject.append(" and mi.type = 1 ");
		// add them status cho display_shop_map
		sqlObject.append(" and dp.status = 1 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  dayInOrder('NOW', 'localtime','start of month','-2 month')");

		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) >=  dayInOrder(?)");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) <=  dayInOrder(?)");
			params.add(toDate);
		}
		if (!StringUtil.isNullOrEmpty(shopId)) {
			sqlObject.append(" and mi.shop_id =  ?");
			params.add(shopId);
			sqlObject.append(" and dp.shop_id IN ");
			sqlObject.append("       (").append(idShopList)
			.append(")");
//			params.add(shopId);

		}
		if (!StringUtil.isNullOrEmpty(programeCode)) {
			sqlObject.append(" and dp.display_program_code =  ?");
			params.add(programeCode);
		}
		sqlObject
				.append(" order by dp.display_program_code, dayInOrder(mi.create_date))  ");
		sqlObject.append(" GROUP  BY display_program_code ");

		Cursor c = null;
		ArrayList<AlbumDTO> data = new ArrayList<AlbumDTO>();
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					int index = 0;
					do {
						int object_type = 0;
						AlbumDTO dto = new AlbumDTO();
						data.add(dto);
						if (c.getColumnIndex("OBJECT_TYPE") >= 0) {
							object_type = c.getInt(c
									.getColumnIndex("OBJECT_TYPE"));
						}
						dto.setAlbumType(object_type);
						data.get(index).setThumbUrl(CursorUtil.getString(c, THUMB_URL));
						data.get(index).setNumImage(CursorUtil.getInt(c, "NUM_IMAGE"));
						data.get(index).setDisplayProgrameId(CursorUtil.getLong(c, "DISPLAY_PROGRAM_ID"));
						data.get(index)
								.setAlbumTitle(
										c.getString(c
												.getColumnIndex("DISPLAY_PROGRAM_CODE")));
						index++;
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return data;
	}

	/**
	 * Lay ds hinh anh cua CTTB
	 *
	 * @author: Tuanlt11
	 * @param programeCode
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @return: ArrayList<AlbumDTO>
	 * @throws:
	 */
	public ArrayList<AlbumDTO> getAlbumProgramInfoCTTB(String programeCode,
			String customerId, String fromDate, String toDate, String shopId,
			String staffId) throws Exception {
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfTwoPreviousMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, -2);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		ArrayList<String> params = new ArrayList<String>();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("select *, count(*) NUM_IMAGE from ( ");
		sqlObject.append("SELECT distinct mi.url, ");
		sqlObject.append("       mi.thumb_url THUMB_URL, ");
		sqlObject.append("       mi.object_type OBJECT_TYPE, ");
		sqlObject.append("       mi.display_program_id DISPLAY_PROGRAM_ID, ");
		sqlObject
				.append("       dp.ks_code DISPLAY_PROGRAM_CODE ");
		sqlObject
				.append("FROM   media_item mi join ks dp ON mi.display_program_id = dp.ks_id  ");
		sqlObject.append(" JOIN ks_shop_map ksm  ON ksm.ks_id = dp.ks_id ");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add(customerId);
		sqlObject.append("       AND mi.object_type = 4 ");
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append(" and mi.status = 1");
//		sqlObject.append(" and mi.type = 1 ");
		// add them status cua display_programe_shop_map
		sqlObject.append(" and dp.status = 1 ");
		sqlObject.append(" and ksm.status = 1 ");
		// add kiem tra create_date > 2 thang truoc
		sqlObject
				.append(" and dayInOrder(mi.create_date) >=  ? ");
		params.add(startOfTwoPreviousMonth);
		sqlObject.append("      and  ? >= dp.FROM_CYCLE_ID ");
		params.add(""+cycleId);
		sqlObject.append("      and  ? <= dp.TO_CYCLE_ID");
		params.add(""+cycleId);
		sqlObject.append(" AND ksm.shop_id IN ");
		sqlObject.append("       (").append(idShopList).append(")");
//		sqlObject.append("              AND IFNULL(DATE(dp.from_date) <=  ?,0) ");
//		params.add(dateNow);
//		sqlObject.append("            AND ( dp.to_date IS NULL ");
//		sqlObject.append("            OR DATE(dp.to_date) >= ? ) ");
//		params.add(startOfTwoPreviousMonth);

		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) >=  dayInOrder(?)");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sqlObject.append(" and dayInOrder(mi.create_date) <=  dayInOrder(?)");
			params.add(toDate);
		}
		if (!StringUtil.isNullOrEmpty(shopId)) {
			sqlObject.append(" and mi.shop_id =  ?");
			params.add(shopId);
		}
		if (!StringUtil.isNullOrEmpty(programeCode)) {
			sqlObject.append(" and dp.ks_code =  ?");
			params.add(programeCode);
		}
		sqlObject
				.append(" order by dp.ks_code, datetime(mi.create_date))  ");
		sqlObject.append(" GROUP  BY display_program_code ");

		Cursor c = null;
		ArrayList<AlbumDTO> data = new ArrayList<AlbumDTO>();
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					int index = 0;
					do {
						int object_type = 0;
						AlbumDTO dto = new AlbumDTO();
						data.add(dto);
						if (c.getColumnIndex("OBJECT_TYPE") >= 0) {
							object_type = c.getInt(c
									.getColumnIndex("OBJECT_TYPE"));
						}
						dto.setAlbumType(object_type);
						data.get(index).setThumbUrl(CursorUtil.getString(c, THUMB_URL));
						data.get(index).setNumImage(CursorUtil.getInt(c, "NUM_IMAGE"));
						data.get(index).setDisplayProgrameId(CursorUtil.getLong(c, "DISPLAY_PROGRAM_ID"));
						data.get(index).setAlbumTitle(CursorUtil.getString(c, "DISPLAY_PROGRAM_CODE"));
						index++;
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		return data;
	}

	/**
	 *
	 * image cong van
	 *
	 * @author: YenNTH
	 * @param officeDocumentId
	 * @return
	 * @return: MediaItemDTO
	 * @throws:
	 */
	public MediaItemDTO getImageOfficeDocument(long officeDocumentId) {
		ArrayList<String> params = new ArrayList<String>();
		MediaItemDTO dto = null;
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT mi.url              as   URL, ");
		sqlObject.append("                mi.id               as   ID, ");
		sqlObject
				.append("                mi.thumb_url        as   THUMB_URL, ");
		sqlObject
				.append("                mi.create_date      as    CREATE_DATE, ");
		sqlObject
				.append("                mi.display_programe_id as DISPLAY_PROGRAME_ID ");
		sqlObject.append("FROM   media_item mi ");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add("" + officeDocumentId);
		sqlObject.append("       AND mi.object_type = ? ");
		params.add("" + 6);

		Cursor c = null;
		try {
			c = rawQuery(sqlObject.toString(),
					params.toArray(new String[params.size()]));
			if (c != null) {
				dto = new MediaItemDTO();
				if (c.moveToFirst()) {
					do {
						dto = initMediaCTTB(c);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return dto;

	}

	/**
	 * Xoa du lieu hinh anh luu qua 2 thang
	 * giu lai hinh anh san pham
	 * @author: banghn
	 * @return
	 */
	public long deleteOldMediaItem(){
		long result = -1;
		try {
			String startOf2MonthAgo = DateUtils
					.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, -2);
			String userId = "" + GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			mDB.beginTransaction();
			// xoa action tu thang truoc
			StringBuffer sqlDel = new StringBuffer();
			sqlDel.append(" CREATE_DATE < ?");
			sqlDel.append(" AND OBJECT_TYPE IN (0, 1, 2, 4, 5)");
			sqlDel.append(" AND MEDIA_ITEM_ID <> ifnull((SELECT Max(MEDIA_ITEM_ID) ");
			sqlDel.append("     FROM   MEDIA_ITEM ");
			sqlDel.append("     WHERE  STAFF_ID = ?),1)");

			String[] params = { startOf2MonthAgo, userId };

			delete(sqlDel.toString(), params);
			result = 1;
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("deleteOldMediaItem", "fail", e);
			result = -1;
		} finally {
			mDB.endTransaction();
		}
		return result;
	}

	 /**
	 * Lay ds hinh anh cua keyshop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	*/
	public VoteKeyShopImageViewDTO getListImageOfKeyShop(Bundle b) throws Exception {
		VoteKeyShopImageViewDTO image = new VoteKeyShopImageViewDTO();
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		long keyShopId = b.getLong(IntentConstants.INTENT_KEYSHOP_ID);
		int numTop = b.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<String> totalParams = new ArrayList<String>();
		StringBuffer sqlTotalObject = new StringBuffer();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT mi.url                 URL, ");
		sqlObject.append("                mi.MEDIA_ITEM_ID       MEDIA_ITEM_ID, ");
		sqlObject.append("                mi.thumb_url           THUMB_URL, ");
		sqlObject.append("                mi.create_date         CREATE_DATE, ");
		sqlObject.append("                mi.display_program_id  DISPLAY_PROGRAM_ID, ");
		sqlObject.append("                Mi.thumb_url                                 AS THUMB_URL, ");
		sqlObject.append("                Mi.url                                       AS URL, ");
		sqlObject.append("                S.staff_code                                AS STAFF_CODE, ");
		sqlObject.append("                S.staff_name                                AS STAFF_NAME, ");
		sqlObject.append("                S.staff_id                                  AS STAFF_ID, ");
		sqlObject.append("                Mi.lat                                       AS LAT_MEDIA, ");
		sqlObject.append("                Mi.lng                                       AS LNG_MEDIA, ");
		sqlObject.append("                Mi.create_user                               AS CREATE_USER, ");
		sqlObject.append("                Strftime('%d/%m/%Y - %H:%M', Mi.create_date) AS CREATE_DATE ");
		sqlObject.append("FROM   media_item mi, ");
//		sqlObject.append("	     CYCLE CY,	");
		sqlObject.append("	     KS_CUSTOMER KSC,	");
		sqlObject.append("	     KS, 	");
		sqlObject.append("	     Staff s 	");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add("" + customerId);
		totalParams.add("" + customerId);
		sqlObject.append("       AND ksc.customer_id = mi.object_id ");
		sqlObject.append("       AND mi.object_type = ? ");
		params.add("" +  MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		totalParams.add("" +  MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append("       AND mi.status = 1 ");
//		sqlObject.append("       AND Date(mi.create_date) = Date('NOW', 'localtime') ");
		sqlObject.append("       AND mi.shop_id = ? ");
		params.add("" + shopId);
		totalParams.add("" + shopId);
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add("" + staffId);
		totalParams.add("" + staffId);
		sqlObject.append("       AND mi.STAFF_ID = s.staff_id ");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("	    AND KSC.STATUS = ?	");
//		sqlObject.append("	    AND KSC.APPROVAL = ?	");
		params.add(""+KSCustomerDTO.STATE_ACTIVE);
		totalParams.add(""+KSCustomerDTO.STATE_ACTIVE);
		sqlObject.append("	    AND KSC.KS_ID = KS.KS_ID	");
//		sqlObject.append("	    AND KSC.CYCLE_ID >= KS.FROM_CYCLE_ID	");
//		sqlObject.append("	    AND KSC.CYCLE_ID <= KS.TO_CYCLE_ID	");
//		sqlObject.append("      AND CY.CYCLE_ID = KSC.CYCLE_ID ");
		sqlObject.append("       AND KS.KS_ID = mi.display_program_id ");
		sqlObject.append("       AND KS.KS_ID = ?");
		params.add("" + keyShopId);
		totalParams.add("" + keyShopId);
//		sqlObject.append("       AND CY.STATUS = 1 ");
//		sqlObject.append("       AND SUBSTR(CY.BEGIN_DATE,1,10) <= SUBSTR(?,1,10) ");
//		params.add(dateNow);
//		sqlObject.append("       AND SUBSTR(CY.END_DATE,1,10) >= SUBSTR(?,1,10) ");
//		params.add(dateNow);
		if (isGetTotalPage) {
			sqlTotalObject.append("select count(*) as TOTAL_ROW from ("
					+ sqlObject + ")");
		}
		sqlObject.append(" order by datetime(mi.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add("" + numTop);
		int offset = page * numTop;
		params.add(String.valueOf(offset));

		Cursor c = null;
		Cursor cTotal = null;
		try {
			if (isGetTotalPage) {
				cTotal = rawQueries(sqlTotalObject.toString(), totalParams);
				if (cTotal != null) {
					if (cTotal.moveToFirst()) {
						image.totalImage = CursorUtil.getInt(cTotal,
								"TOTAL_ROW");
					}
				}
			}
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						image.lstImage.add(item.mediaItem);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotal != null) {
					cTotal.close();
				}
			} catch (Exception e) {
			}
		}
		return image;
	}

	/**
	 * Lay ds hinh anh cua keyshop
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	*/
	public ArrayList<KeyShopDTO> validateListImageOfKeyShop(Bundle b) throws Exception {
		ArrayList<KeyShopDTO> lstKeyShop = new ArrayList<KeyShopDTO>();
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		ArrayList<String> lstId = b.getStringArrayList(IntentConstants.INTENT_LIST_KEYSHOP_ID);
		String strId= TextUtils.join(",", lstId);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("	SELECT count(MI.OBJECT_ID) AS TAKEN_PHOTO_NUM, KS.MIN_PHOTO_NUM, KS.NAME, KS.KS_CODE ");
		sqlObject.append("	FROM KS LEFT JOIN	MEDIA_ITEM mi   	");
		sqlObject.append("	ON  	 mi.object_id = ? ");
		params.add("" + customerId);
		sqlObject.append("       AND mi.object_type = ? ");
		params.add("" +  MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND substr(mi.create_date, 1, 10) = substr(?, 1, 10) ");
		params.add(dateNow);
		sqlObject.append("       AND mi.shop_id = ? ");
		params.add("" + shopId);
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add("" + staffId);
		sqlObject.append("       AND KS.KS_ID = mi.display_program_id ");
		sqlObject.append("	WHERE  1 = 1 ");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("       AND KS.KS_ID IN (");
		sqlObject.append(strId);
		sqlObject.append("       )");
		sqlObject.append(" group by KS.KS_ID ");
		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						int numItemTaken = CursorUtil.getInt(c, "MIN_PHOTO_NUM");
						int takenPhotoNum = CursorUtil.getInt(c, "TAKEN_PHOTO_NUM");
						if(takenPhotoNum < numItemTaken){
							KeyShopDTO item = new KeyShopDTO();
							item.initDataFromCursor(c);
							item.takenPhotoNum = takenPhotoNum;
							lstKeyShop.add(item);
						}
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;
	}
	
	/**
	 * Lay ds hinh anh cua thiet bi
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	 */
	public TakePhotoEquipmentImageViewDTO getListImageOfEquipment(Bundle b) throws Exception {
		TakePhotoEquipmentImageViewDTO image = new TakePhotoEquipmentImageViewDTO();
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		long equipId = b.getLong(IntentConstants.INTENT_EQUIP_ID);
		int numTop = b.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<String> totalParams = new ArrayList<String>();
		StringBuffer sqlTotalObject = new StringBuffer();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT mi.url                 URL, ");
		sqlObject.append("                mi.MEDIA_ITEM_ID       MEDIA_ITEM_ID, ");
		sqlObject.append("                mi.thumb_url           THUMB_URL, ");
		sqlObject.append("                mi.create_date         CREATE_DATE, ");
		sqlObject.append("                mi.display_program_id  DISPLAY_PROGRAM_ID, ");
		sqlObject.append("                Mi.thumb_url                                 AS THUMB_URL, ");
		sqlObject.append("                Mi.url                                       AS URL, ");
		sqlObject.append("                S.staff_code                                AS STAFF_CODE, ");
		sqlObject.append("                S.staff_name                                AS STAFF_NAME, ");
		sqlObject.append("                S.staff_id                                  AS STAFF_ID, ");
		sqlObject.append("                Mi.lat                                       AS LAT_MEDIA, ");
		sqlObject.append("                Mi.lng                                       AS LNG_MEDIA, ");
		sqlObject.append("                Mi.create_user                               AS CREATE_USER, ");
		sqlObject.append("                Strftime('%d/%m/%Y - %H:%M', Mi.create_date) AS CREATE_DATE ");
		sqlObject.append("FROM   media_item mi, ");
		sqlObject.append("	     Staff s 	");
		sqlObject.append("WHERE  mi.object_id = ? ");
		params.add(String.valueOf(customerId));
		totalParams.add(String.valueOf(customerId));
		sqlObject.append("AND  mi.display_program_id = ? ");
		params.add(String.valueOf(equipId));
		totalParams.add(String.valueOf(equipId));
		sqlObject.append("       AND mi.object_type = ? ");
		params.add(String.valueOf(MediaItemDTO.TYPE_DEVICE_IMAGE));
		totalParams.add(String.valueOf(MediaItemDTO.TYPE_DEVICE_IMAGE));
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND mi.shop_id = ? ");
		params.add(String.valueOf(shopId));
		totalParams.add(String.valueOf(shopId));
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add(String.valueOf(staffId));
		totalParams.add(String.valueOf(staffId));
		sqlObject.append("       AND mi.STAFF_ID = s.staff_id ");
		if (isGetTotalPage) {
			sqlTotalObject.append("select count(*) as TOTAL_ROW from (" + sqlObject + ")");
		}
		sqlObject.append(" order by datetime(mi.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add("" + numTop);
		int offset = page * numTop;
		params.add(String.valueOf(offset));
		
		Cursor c = null;
		Cursor cTotal = null;
		try {
			if (isGetTotalPage) {
				cTotal = rawQueries(sqlTotalObject.toString(), totalParams);
				if (cTotal != null) {
					if (cTotal.moveToFirst()) {
						image.totalImage = CursorUtil.getInt(cTotal, "TOTAL_ROW");
					}
				}
			}
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						image.lstImage.add(item.mediaItem);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotal != null) {
					cTotal.close();
				}
			} catch (Exception e) {
			}
		}
		return image;
	}
	
	/**
	 * Lay ds hinh anh cua thiet bi
	 * @author: Tuanlt11
	 * @param b
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	*/
	public ArrayList<TakePhotoEquipmentDTO> validateListImageOfEquipment(Bundle b) throws Exception {
		ArrayList<TakePhotoEquipmentDTO> lstKeyShop = new ArrayList<TakePhotoEquipmentDTO>();
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		ArrayList<String> lstId = b.getStringArrayList(IntentConstants.INTENT_LIST_KEYSHOP_ID);
		String strId= TextUtils.join(",", lstId);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("	SELECT count(MI.OBJECT_ID) AS TAKEN_PHOTO_NUM, KS.CODE ");
		sqlObject.append("	FROM EQUIPMENT KS LEFT JOIN	MEDIA_ITEM mi   	");
		sqlObject.append("	ON  	 mi.object_id = ? ");
		params.add(String.valueOf(customerId));
		sqlObject.append("       AND mi.object_type = ? ");
		params.add(String.valueOf(MediaItemDTO.TYPE_DEVICE_IMAGE));
		sqlObject.append("       AND mi.media_type = 0 ");
		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND substr(mi.create_date, 1, 10) = substr(?, 1, 10) ");
		params.add(dateNow);
		sqlObject.append("       AND mi.shop_id = ? ");
		params.add(String.valueOf(shopId));
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add(String.valueOf(staffId));
		sqlObject.append("       AND KS.EQUIP_ID = mi.display_program_id ");
		sqlObject.append("	WHERE  1 = 1 ");
		sqlObject.append("	    AND KS.STATUS = 1	");
		sqlObject.append("       AND KS.EQUIP_ID IN (");
		sqlObject.append(strId);
		sqlObject.append("       )");
		sqlObject.append(" group by KS.EQUIP_ID ");
		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						int takenPhotoNum = CursorUtil.getInt(c, "TAKEN_PHOTO_NUM");
						if(takenPhotoNum <= 0) {
							TakePhotoEquipmentDTO item = new TakePhotoEquipmentDTO();
							item.initDataFromCursor(c);
							item.takenPhotoNum = takenPhotoNum;
							lstKeyShop.add(item);
						}
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstKeyShop;
	}
}
