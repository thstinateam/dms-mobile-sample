package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * dto theo ngay cho man hinh bao cao
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class DynamicReportDateViewDTO extends AbstractDynamicReportViewDTO implements Serializable {
	// mang ds row cua bao cao ngay
	public ArrayList<DynamicReportDateRowDTO> lstReportDate;
	
	public DynamicReportDateViewDTO() {
		lstReportDate = new ArrayList<DynamicReportDateRowDTO>();
		lstReport.addAll(lstReportDate);
	}

	@Override
	public void setData() {
		// TODO Auto-generated method stub
		lstReport.addAll(lstReportDate);
	}
}
