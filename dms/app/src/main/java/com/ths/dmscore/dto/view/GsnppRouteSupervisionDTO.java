package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.ShopDTO;

@SuppressWarnings("serial")
public class GsnppRouteSupervisionDTO implements Serializable{
	public int totalList;
	public ArrayList<GsnppRouteSupervisionItem> itemList;
	//Danh sach don vi
	public ArrayList<ShopDTO> listShop;
	public GsnppRouteSupervisionDTO(){
		itemList = new ArrayList<GsnppRouteSupervisionItem>();
		listShop = new ArrayList<ShopDTO>();
	}
}
