/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.db.StaffDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * ReportKeyshopItemDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  17:38:00 21-07-2015
 */
public class ReportKeyshopItemDTO {
	public double amountRegister;
	public double amountDone;
	public double quantityRegister;
	public double quantityDone;
	public StaffDTO staff = new StaffDTO();
	public KeyShopItemDTO ksShop = new KeyShopItemDTO();
	public int countCustomerRegister = 0;
	public int countDone = 0;
	public double percent;
	
	
	/**
	 * initData
	 * @author: yennth16
	 * @since: 11:03:01 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initData(Cursor c){
		ksShop.ksId = CursorUtil.getLong(c, "KS_ID");
		ksShop.ksCode = CursorUtil.getString(c, "KS_CODE");
		ksShop.name = CursorUtil.getString(c, "NAME");
		amountRegister = CursorUtil.getDouble(c, "TOTAL_AMOUNT_TARGET");
		amountDone = CursorUtil.getDouble(c, "TOTAL_AMOUNT");
		quantityRegister = CursorUtil.getDouble(c, "TOTAL_QUANTITY_TARGET");
		quantityDone = CursorUtil.getDouble(c, "TOTAL_QUANTITY");
		staff.staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staff.name = CursorUtil.getString(c, "STAFF_NAME");
		staff.staffId = CursorUtil.getInt(c, "STAFF_ID");
		staff.shopId =  CursorUtil.getInt(c, "SHOP_ID");
		
	}

}
