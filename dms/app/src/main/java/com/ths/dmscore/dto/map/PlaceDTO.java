/**
 * HieuNH
 */
package com.ths.dmscore.dto.map;

import java.io.Serializable;
import java.util.Vector;

import com.ths.dmscore.constants.Constants;


public class PlaceDTO implements Serializable {
	private static final long serialVersionUID = -3184077635227748424L;
	public String id = Constants.STR_BLANK;
	public String name = Constants.STR_BLANK;
	public String nameMayor = Constants.STR_BLANK;
	public String address = Constants.STR_BLANK;
	@SuppressWarnings("rawtypes")
	public Vector phone = new Vector();
	public String avatarUrl = Constants.STR_BLANK;
	
	// thong tin toa do dia diem lat, lng
	public GeomDTO geom = new GeomDTO();
	
	public PlaceDTO() {
		// init category
		// HaiTC
		// if (KunKunInfo.avatarCategoryGroup.size() == 0){
		// KunKunInfo.initAvatarCategoryGroup();
		// }
	}
}
