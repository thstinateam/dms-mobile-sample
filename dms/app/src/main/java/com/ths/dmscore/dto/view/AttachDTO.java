/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *
 * Mo ta cho class
 *
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class AttachDTO implements Serializable{
	private static final long serialVersionUID = 19667997719851047L;
	public long attachId;
	public String url;
	public String title;

	public void initDataWithCursor(Cursor c) {
		attachId = CursorUtil.getLong(c, "ATTACH_ID");
		url = CursorUtil.getString(c, "URL");
		title = CursorUtil.getString(c, "TITLE");
	}
}
