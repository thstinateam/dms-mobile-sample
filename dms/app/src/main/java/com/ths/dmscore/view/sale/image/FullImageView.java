/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.image;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.commonsware.cwac.cache.CacheBase.DiskCachePolicy;
import com.commonsware.cwac.thumbnail.ThumbnailBus;
import com.google.android.gms.maps.MapFragment;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.image.zoom.ImageViewTouch;
import com.ths.universalimageloader.ImageGalleryAdapter;
import com.ths.universalimageloader.ImagePagerAdapter;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.MenuItem;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.newcustomer.MapViewEventDialog;
import com.ths.dms.R;
import com.ths.image.zoom.ZoomViewPaper;
import com.ths.image.zoom.ZoomViewPaper.OnPageChangeListener;

/**
 * Man hinh xem chi tiet anh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class FullImageView extends BaseFragment implements DiskCachePolicy,
		OnTouchListener, ImagePagerAdapter.OnPagerListener, OnPageChangeListener,
		ImageGalleryAdapter.OnGalleryListener, OnItemClickListener {
	private static final int SHOW_POSITION = 0;
	public static final int ACTION_GET_MORE_PHOTO = 1;
	// public ImageViewTouch image;
	ThumbnailBus thumbBus = new ThumbnailBus();
	// cahe hinh anh
	public boolean isZooming;
	public ImageViewTouch imageTouch;
	protected int currentSelected;
	// text view hien thi so thu tu cua hinh dang xem
	// private TextView tvNumberImage;
	// thanh bar o tren dau trong man hinh
	// private RelativeLayout headerBar;
	private RelativeLayout rootView;
	// header thong tin hinh anh
	// private ImageHeader ihImageHeader;
	AlbumDTO albumDTO;
	private AlertDialog alertPromotionDetail;
	private boolean isChangedIndex;
	// private PagerContainer mContainer;
	// private ViewPager thumbnailViewPager;
	// private ThumbnailPagerAdapter thumbnailViewAdapter;
	private LinearLayout llGallery;
	private ImageGalleryAdapter galleryAdapter;
	private Gallery gallery;
	private boolean isThumnailClick = false;// co bien nay de thumbnail adapter

	// -- Danh sach Photo
	public ArrayList<PhotoDTO> listPhotosDTO;
	// -- Danh sach url anh
	public ArrayList<String> imageUrls;
	private ZoomViewPaper mPager;
	// -- Adaper for pager
	private ImagePagerAdapter pagerAdapter;

	private GestureDetector gestureDetector;
	private MapViewEventDialog mapDialog;

	// ko notify nhieu lan, se bi sai
	// lech hinh

	public static FullImageView newInstance(Bundle bundle) {
		FullImageView f = new FullImageView();
		f.setArguments(bundle);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// Get data
		Bundle data = getArguments();
		AlbumDTO albumDTO = (AlbumDTO) data
				.getSerializable(IntentConstants.INTENT_ALBUM_INFO);
		listPhotosDTO = albumDTO.getListPhoto();
		getListUrlsFromListPhoto(listPhotosDTO);
		currentSelected = data.getInt(IntentConstants.INTENT_ALBUM_INDEX_IMAGE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.view_image_full,
				null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_IMAGE_FULL_04_03_03));
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			PriUtils.getInstance().genPriHashMapForForm(
					PriHashMap.PriForm.NVBH_DSHINHANH_CHITIETHINHANH_CHITIET);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance()
					.genPriHashMapForForm(
							PriHashMap.PriForm.GSNPP_DSHINHANH_CHITIETHINHANH_CHITIET);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			PriUtils.getInstance()
					.genPriHashMapForForm(
							PriHashMap.PriForm.TBHV_DSHINHANH_CHITIETHINHANH_CHITIET);
		}

		// Setup header
		rootView = (RelativeLayout) view.findViewById(R.id.lnViewImage);
		initHeaderView(rootView, savedInstanceState);
        if(rlHeader != null){
            rlHeader.setVisibility(View.VISIBLE);
        }

//		Display currentDisplay = ((WindowManager) parent.getBaseContext()
//				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
//		int dw = currentDisplay.getWidth();
//		int dh = currentDisplay.getHeight();

		// Khoi tao du lieu
		initData();

		// Khoi tao pager
		initPager(view);

		updateSelectedInfoImage(currentSelected);


		initThumbnailGallery(view);
		return view;

	}

	/**
	 * Khoi tao data
	 *
	 * @author: Tamvnm
	 * @param
	 * @return: void
	 * @throws:
	 */
	private void initData() {
		if (listPhotosDTO == null) {
			listPhotosDTO = new ArrayList<PhotoDTO>();
		}

		if (imageUrls == null) {
			imageUrls = new ArrayList<String>();
		}

		gestureDetector = new GestureDetector(parent, new GestureListener());
	}

	/**
	 * Khoi tao Pager
	 *
	 * @author: Tamvnm
	 * @param
	 * @return: void
	 * @throws:
	 */
	private void initPager(View view) {
		pagerAdapter = new ImagePagerAdapter(parent, this, imageUrls);
		pagerAdapter.setOntouchListener(this);
		pagerAdapter.setPagerListener(this);

		mPager = (ZoomViewPaper) view.findViewById(R.id.myfivepanelpager);
		mPager.setFragment(this);
		mPager.setAdapter(pagerAdapter);
		mPager.setCurrentItem(currentSelected);
		mPager.setOnPageChangeListener(this);
	}

	/**
	 * init Thumbnail Gallery
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void initThumbnailGallery(View view) {
		llGallery = (LinearLayout) view.findViewById(R.id.llGallery);
		llGallery.setVisibility(View.VISIBLE);

		gallery = (Gallery) view.findViewById(R.id.gallery);
		galleryAdapter = new ImageGalleryAdapter(parent, imageUrls);
		gallery.setAdapter(galleryAdapter);
		gallery.setSelection(currentSelected);
		gallery.setCallbackDuringFling(false);
		gallery.setOnItemClickListener(this);


	}

	private ArrayList<String> getListUrlsFromListPhoto(
			ArrayList<PhotoDTO> listPhotosDTO) {
		if (imageUrls == null) {
			imageUrls = new ArrayList<String>();
		} else {
			imageUrls.clear();
		}

		for (PhotoDTO photoDTO : listPhotosDTO) {
			imageUrls.add(photoDTO.fullUrl);
		}
		return imageUrls;
	}

	/**
	 * process Thumbnail View
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void processThumbnailView() {
		if (llGallery.getVisibility() == View.INVISIBLE) {
			llGallery.setVisibility(View.VISIBLE);
		} else if (llGallery.getVisibility() == View.VISIBLE) {
			llGallery.setVisibility(View.INVISIBLE);
		}
	}

	protected void updateSelectedInfoImage(int position) {
		currentSelected = position;
		isChangedIndex = true;
		PhotoDTO item = listPhotosDTO.get(currentSelected);
		String time = DateUtils.parseDateFromSqlLite(item.createdTime);
		// intit title
		String title = null;
		if (!StringUtil.isNullOrEmpty(item.staffCode)) {
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
				title = StringUtil
						.getString(R.string.TITLE_VIEW_IMAGE_FULL_04_03_03)
						+ " ("
						+ item.createUser
						+ StringUtil.getString(R.string.TEXT_TIME_TAKE_PHOTO) +": "
						+ time
						+ ")";
			} else {
				StringBuffer buffer = new StringBuffer();
				buffer.append(item.staffCode);
				if (!StringUtil.isNullOrEmpty(item.staffName)) {
					buffer.append(" - ").append(item.staffName);
				}
				title = StringUtil.getString(R.string.TITLE_VIEW_IMAGE_FULL)
						+ " (" + buffer.toString() + StringUtil.getString(R.string.TEXT_TIME_TAKE_PHOTO) +": "
						+ time + ")";
			}
		} else {
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
				title = StringUtil
						.getString(R.string.TITLE_VIEW_IMAGE_FULL_04_03_03)
						+ " ("
						+ item.createUser
						+ StringUtil.getString(R.string.TEXT_TIME_TAKE_PHOTO) +": "
						+ time
						+ ")";
			} else {
				title = StringUtil.getString(R.string.TITLE_VIEW_IMAGE_FULL)
						+ " (" + item.createUser + StringUtil.getString(R.string.TEXT_TIME_TAKE_PHOTO) +": " + time
						+ ")";
			}
		}
		SpannableString span = new SpannableString(title);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			span.setSpan(
					new StyleSpan(android.graphics.Typeface.BOLD),
					StringUtil.getString(
							R.string.TITLE_VIEW_IMAGE_FULL_04_03_03).length(),
					title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		} else {
			span.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
					StringUtil.getString(R.string.TITLE_VIEW_IMAGE_FULL)
							.length(), title.length(),
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}

		setTitleHeaderView(span);

		// set position Image
	}

	/**
	 * tao thanh tool bar zoom hinh anh, va bao xau
	 *
	 * @author: PhucNT
	 * @param header
	 * @return: void
	 * @throws:
	 */
	private void initHeaderView(View header, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// ihImageHeader = (ImageHeader)
		// header.findViewById(R.id.ihImageHeader);
		// ihImageHeader.setFragment(this);
		// //ihImageHeader.setLayoutMap(m_loMain);
		// m_loMain = ihImageHeader.getLayoutMap();
		// initMap(savedInstanceState);
		// setMapPostion( 10.784875,106.68323);
	}

	// public void visibleHeader() {
	// // TODO Auto-generated method stub
	// if (ihImageHeader != null) {
	// if (ihImageHeader.getVisibility() == View.VISIBLE) {
	// ihImageHeader.setVisibility(View.GONE);
	//
	// } else {
	// ihImageHeader.setVisibility(View.VISIBLE);
	// ihImageHeader.visibleInTime();
	//
	// }
	// }
	//
	// }

	// /**
	// * Xu ly luu hinh anh
	// * @author: PhucNT4
	// * @return: void
	// * @throws:
	// */
	// private void processSaveImage() {
	// // TODO Auto-generated method stub
	// try {
	// File filePath = getImageDownloadPath();
	//
	// if (filePath == null) {
	// Toast.makeText(this, StringUtil.getString(R.string.SAVE_IMAGE_FAIL),
	// Toast.LENGTH_LONG).show();
	// } else {
	// FileOutputStream file = new FileOutputStream(filePath);
	// // BitmapDrawable bd = (BitmapDrawable)
	// cache.get(imListPicture.get(currentSelected).getImageUrl());
	// BitmapDrawable bd = (BitmapDrawable)
	// cache.get(listPhotosDTO.get(currentSelected).fullUrl);
	// if (bd != null && bd.getBitmap() != null) {
	// Bitmap bitmap = bd.getBitmap();
	// bitmap.compress(Bitmap.CompressFormat.JPEG, 100, file);
	//
	// file.flush();
	// file.close();
	// Toast.makeText(this,
	// StringUtil.getString(R.string.SAVE_IMAGE_SUCCESSFUL), Toast.LENGTH_LONG)
	// .show();
	// sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"
	// + Environment.getExternalStorageDirectory())));
	// }
	// }
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		// case ActionEventConstant.ACTION_CLICK:
		//
		// break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onDestroyView() {
		try {
			 MapFragment fragment = (MapFragment) parent.getFragmentManager().findFragmentById(
                        R.id.ggMap);
			 if (fragment != null) {
				 getFragmentManager().beginTransaction().remove(fragment).commit();
			 }
		} catch (Exception e) {
			Log.e("FullImageView detroy map", "error", e);
		}
		/*
		 * if (cache != null) { GlobalUtil.nullViewDrawablesRecursive(rootView);
		 * alertPromotionDetail = null; cache.recycleAllBitmaps(true);
		 * cache.getBus().unregister(onCache); cache.stopDownload(); cache =
		 * null; HashMapManager.getInstance().clearAllHashMap(); }
		 */
		super.onDestroyView();
	}

	/*
	 * private class MyPagerAdapter extends PagerAdapter {
	 *
	 * public int getCount() { return listPhotosDTO.size(); }
	 *
	 * public Object instantiateItem(View collection, int position) { // View
	 * row = ((ZoomViewPaper) collection).getChildAt(position); // if (row ==
	 * null) { LayoutInflater inflater = (LayoutInflater)
	 * collection.getContext().getSystemService(
	 * Context.LAYOUT_INFLATER_SERVICE);
	 *
	 * int resId = R.layout.item_view_image_full;
	 *
	 * View row = inflater.inflate(resId, null); MyLog.e("PhucNT4",
	 * "instantiateItem " + position);
	 *
	 * imageTouch = (ImageViewTouch) row.findViewById(R.id.imageView1);
	 * imageTouch.setFragment(FullImageView.this);
	 * imageTouch.setOnTouchListener(FullImageView.this); Bitmap bm =
	 * BitmapFactory.decodeResource(getResources(), R.drawable.image_loading);
	 * imageTouch.setImageBitmapReset(bm, true); if
	 * (!StringUtil.isNullOrEmpty(listPhotosDTO.get(position).fullUrl)) {
	 *
	 * // listPhotosDTO.get(position).fullUrl = ServerPath.AVATAR_PATH // +
	 * listPhotosDTO.get(position).fullUrl; listPhotosDTO.get(position).fullUrl
	 * = listPhotosDTO.get(position).fullUrl;
	 * notifyLoadImage(listPhotosDTO.get(position), position, imageTouch); }
	 *
	 * ((ZoomViewPaper) collection).addView(row);
	 *
	 * // get more if (position == listPhotosDTO.size() - 3) { getMorePhotos();
	 * }
	 *
	 * return row;
	 *
	 * }
	 *
	 * @Override public void destroyItem(View arg0, int arg1, Object arg2) {
	 * ((ZoomViewPaper) arg0).removeView((View) arg2); MyLog.e("BigPhotoCache",
	 * "  destroyItem destroyItem  " + arg1); }
	 *
	 * @Override public void finishUpdate(View arg0) {
	 *
	 * }
	 *
	 * @Override public boolean isViewFromObject(View arg0, Object arg1) {
	 * return arg0 == ((View) arg1);
	 *
	 * }
	 *
	 * @Override public void restoreState(Parcelable arg0, ClassLoader arg1) { }
	 *
	 * @Override public Parcelable saveState() { return null; }
	 *
	 * @Override public void startUpdate(View arg0) {
	 *
	 * }
	 *
	 * }
	 */
	public void performZoom() {
		// TODO Auto-generated method stub
		// if (image.getScale()==1)
		// ((ZoomViewPager)v).onTouchEvent(event);
		// return false;
	}

	public void getMorePhotos() {
		Bundle bundle = new Bundle();
		sendBroadcast(ActionEventConstant.GET_MORE_PHOTOS, bundle);
	}


	public String getUrlPath(PhotoDTO dto) {
		if (dto != null && !StringUtil.isNullOrEmpty(dto.fullUrl)) {
			if (dto.fullUrl.contains(ExternalStorage.SDCARD_PATH)) {
				return dto.fullUrl;
			} else {
				return ServerPath.IMAGE_PATH + dto.fullUrl;
			}
		}
		return "";
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		if (control instanceof MenuItem) {
			if (eventType == SHOW_POSITION) {
				showPositionImage();
			}
		} else if (eventType == ACTION_GET_MORE_PHOTO) {
			getMorePhotos();
		} else {
			super.onEvent(eventType, control, data);
		}
	}

	/**
	 *
	 * show thong tin chi tiet chuong trinh khuy
	 *
	 * @param program
	 * @return: void
	 * @throws:
	 */
	public void showPositionImage() {
		PhotoDTO dto = listPhotosDTO.get(currentSelected);
		if(mapDialog == null){
			mapDialog = new MapViewEventDialog(parent, this, StringUtil.getString(R.string.TEXT_POSITION_IN_MAP), 0);
		}
		mapDialog.showLocationFlag(dto.lat,  dto.lng, true);
		mapDialog.show();
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		if (action == ActionEventConstant.GET_RESULT_MORE_PHOTOS) {
			albumDTO = (AlbumDTO) bundle
					.getSerializable(IntentConstants.INTENT_ALBUM_INFO);
			listPhotosDTO = albumDTO.getListPhoto();
			pagerAdapter.notifyDataSetChanged();
			galleryAdapter.notifyDataSetChanged();
			/* thumnailThumbs.notifyDataSetChanged(); */
		}
	}

	@Override
	public boolean eject(File cachedFile) {
		// TODO Auto-generated method stub
		return true;
	}

	public void closePopup() {
		alertPromotionDetail.dismiss();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// float pointX = 0;
		// float pointY = 0;
		// int tolerance = 5;
		// switch (event.getAction()) {
		// case MotionEvent.ACTION_MOVE:
		// return false; // This is important, if you return TRUE the
		// // action of swipe will not take place.
		// case MotionEvent.ACTION_DOWN:
		// pointX = event.getX();
		// pointY = event.getY();
		// break;
		// case MotionEvent.ACTION_UP:
		// case MotionEvent.ACTION_CANCEL:
		// boolean sameX = pointX + tolerance > event.getX() && pointX -
		// tolerance < event.getX();
		// boolean sameY = pointY + tolerance > event.getY() && pointY -
		// tolerance < event.getY();
		// if (sameX && sameY) {
		// processThumbnailView();
		// }
		// }
		// return false;

		/*
		 * long time1 = 0, time2 = 0; switch (event.getAction()) { case
		 * MotionEvent.ACTION_MOVE: return false; // This is important, if you
		 * return TRUE the case MotionEvent.ACTION_DOWN: time1 =
		 * System.currentTimeMillis(); break; case MotionEvent.ACTION_UP: time2
		 * = System.currentTimeMillis(); if ((time2 - time1) >= 1000) {
		 * processThumbnailView(); } break; }
		 *
		 * return false;
		 */
		return gestureDetector.onTouchEvent(event);
	}

	@Override
	public void onResume() {
		super.onResume();
		// enable menu bar
		enableMenuBar(this);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			addMenuItemPri("", R.drawable.icon_map, SHOW_POSITION,
					PriHashMap.PriForm.NVBH_DSHINHANH_CHITIETHINHANH_VITRI);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			addMenuItemPri(
					"",
					R.drawable.icon_map,
					SHOW_POSITION,
					PriHashMap.PriForm.GSNPP_DSHINHANH_CHITIETHINHANH_CHITIET_VITRIHINHANH);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			addMenuItemPri(
					"",
					R.drawable.icon_map,
					SHOW_POSITION,
					PriHashMap.PriForm.TBHV_HINHANH_DANHSACHHINHANH_ALBUM_CHITIETHINHANH_VITRI);
		}
	}

	public class GestureListener extends SimpleOnGestureListener {

		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onDoubleTap(MotionEvent e) {
			return false;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			return false;
		}

		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			processThumbnailView();
			return false;
		}
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		getMorePhotos();
	}

	@Override
	public void onPageScrolled(int position, double positionOffset,
			int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int position) {
		updateSelectedInfoImage(position);
		if (isThumnailClick) {
			isThumnailClick = false;
		} else {
			currentSelected = position;
			gallery.setSelection(currentSelected);
		}

	}

	@Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view,
			final int position, long id) {
		if (currentSelected != position) {
			updateSelectedInfoImage(position);
			isThumnailClick = true;
			currentSelected = position;
			mPager.setCurrentItem(currentSelected);
		}

	}

}