package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cua man hinh bao cao luy ke den ngay
 *
 * @author hieunq1
 *
 */
public class AccSaleProgReportDTO {
	public int totalList;
	// ngay lam viec trong thang
	public int monthSalePlan;
	// ngay da lam viec trong thang
	public int soldSalePlan;
	// percent ngay lam viec trong thang
	public double perSalePlan;
	// ke hoach ban hang trong thang
	public double planTotal;
	// doanh so da ban trong thang
	public double soldTotal;
	// doanh so da duyet trong thang
	public double duyet;
	// doanh so con lai
	public double remainTotal;
	// san luong
	// ke hoach ban hang trong thang
	public long quantityPlanTotal;
	// san luong da ban trong thang
	public long quantitySoldTotal;
	// san luong da duyet trong thang
	public long quantityDuyet;
	// san luong con lai
	public long quantityRemainTotal;
	// percent so tien da ban
	public double perSoldTotal;
	// percent san luong da ban
	public double perQuantityTotal;
	public int numCycle = 0;
	public String beginDate = "";
	public String endDate = "";

	public ArrayList<AccSaleProgReportItem> arrList;

	public AccSaleProgReportDTO() {
		arrList = new ArrayList<AccSaleProgReportItem>();
	}

	/**
	 * add item
	 *
	 * @param c
	 */
	public void addItem(AccSaleProgReportItem c) {
		arrList.add(c);
		planTotal += c.moneyPlan;
		soldTotal += c.moneySold;
		duyet += c.duyet;
		remainTotal += c.moneyRemain;
		perSoldTotal = StringUtil.calPercentUsingRound(planTotal, soldTotal);

		quantityPlanTotal += c.quantityPlan;
		quantitySoldTotal += c.quantitySold;
		quantityDuyet += c.quantityDuyet;
		quantityRemainTotal += c.quantityRemain;

		perQuantityTotal = StringUtil.calPercentUsingRound(quantityPlanTotal, quantitySoldTotal);
	}

	public AccSaleProgReportItem newAccSaleProgReportItem() {
		return new AccSaleProgReportItem();
	}

	/**
	 * DTO row
	 *
	 * @author hieunq1
	 *
	 */
	public class AccSaleProgReportItem {
		public String staffCode;
		public String staffName;
		// doanh so ke hoach
		public double moneyPlan;
		// doanh so da ban
		public double moneySold;
		// doanh so da duyet
		public double duyet;
		// doanh so con lai
		public double moneyRemain;

		// san luong ke hoach
		public long quantityPlan;
		// san luong da ban
		public long quantitySold;
		// san luong da duyet
		public long quantityDuyet;
		// san luong con lai
		public long quantityRemain;
		// % san luong
		public double quantityPercent;

		// phan tram da ban / ke hoach thang
		public double percent;
		// sku khach hang
		public double sku_kh;
		// sku chi tieu
		public double sku_target;
		// diem thang
		public double score;
		public String mobile;
		public int staffId;

		public AccSaleProgReportItem() {
			staffCode = "";
			staffName = "";
			moneyPlan = 0;
			moneySold = 0;
			duyet = 0;
			percent = 0;
			moneyRemain = 0;
			sku_kh = 0;
			sku_target = 0;
			score = 0;
		}

		public AccSaleProgReportItem(Cursor c) {
			staffCode = CursorUtil.getString(c, "staff_code");
			staffName = CursorUtil.getString(c, "name");
			moneyPlan = CursorUtil.getDouble(c, "plan_amount");
			moneySold = CursorUtil.getDouble(c, "sold_amount");
			duyet = CursorUtil.getDouble(c, "duyet");
			if (moneyPlan > 0) {
				percent = moneySold / moneyPlan;
				percent *= 100;
			}
			moneyRemain = moneyPlan - moneySold;
			sku_kh = 11;
			sku_target = 14;
			score = 2;
		}

		/**
		 *
		 * parse data with cursor
		 *
		 * @author: HaiTC3
		 * @param c
		 * @return: void
		 * @throws:
		 * @since: Jan 29, 2013
		 */
		public void parseDataFromCursor(Cursor c) {
			staffId = CursorUtil.getInt(c, "OBJECT_ID");
			staffCode = CursorUtil.getString(c, "OBJECT_CODE");
			staffName = CursorUtil.getString(c, "OBJECT_NAME");
			mobile = CursorUtil.getString(c, "MOBILE");

			moneyPlan = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_AMOUNT_PLAN");
			moneySold = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_AMOUNT");
			duyet = CursorUtil.getDoubleUsingSysConfig(c, "DUYET");
			percent = StringUtil.calPercentUsingRound(moneyPlan, moneySold);
			moneyRemain = StringUtil.calRemainUsingRound(moneyPlan, moneySold);

			quantityPlan = CursorUtil.getLong(c, "MONTH_QUANTITY_PLAN");
			quantitySold = CursorUtil.getLong(c, "MONTH_QUANTITY");
			quantityDuyet = CursorUtil.getLong(c, "DUYET_SL");
			quantityRemain = StringUtil.calRemain(quantityPlan, quantitySold);
			quantityPercent = StringUtil.calPercentUsingRound(quantityPlan, quantitySold);

//			sku_target = StringUtil.getDoubleFromCursor(c, "MONTH_SKU_PLAN");
//			sku_kh = c.getDouble(c.getColumnIndex("MONTH_SKU"));
//			score = c.getDouble(c.getColumnIndex("MONTH_SCORE"));
		}
	}

}
