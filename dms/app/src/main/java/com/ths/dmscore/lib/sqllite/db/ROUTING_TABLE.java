package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Mo ta muc dich cua class
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class ROUTING_TABLE extends ABSTRACT_TABLE {
	// ID bang
	public static final String ROUTING_ID = "ROUTING_ID";
	// Ma tuyen
	public static final String ROUTING_CODE = "ROUTING_CODE";
	// Ten tuyen
	public static final String ROUTING_NAME = "ROUTING_NAME";
	// ID NPP
	public static final String SHOP_ID = "SHOP_ID";
	// 0: ngung hoat dong, 1:hoat dong
	public static final String STATUS = "STATUS";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay update
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";

	public static final String ROUTING_TABLE = "ROUTING_TABLE";

	public ROUTING_TABLE() {
		this.tableName = ROUTING_TABLE;
		this.columns = new String[] { ROUTING_ID, ROUTING_CODE, ROUTING_NAME, SHOP_ID, STATUS, CREATE_DATE,
				CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public ROUTING_TABLE(SQLiteDatabase mDB) {
		this.tableName = ROUTING_TABLE;
		this.columns = new String[] { ROUTING_ID, ROUTING_CODE, ROUTING_NAME, SHOP_ID, STATUS, CREATE_DATE,
				CREATE_USER, UPDATE_DATE, UPDATE_USER, SYN_STATE };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

}
