/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;

/**
 * Define cac tham so khi truyen nhan giua cac activity/client-server
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public interface IntentConstants {
	public static final String INTENT_VISIT_TYPE = "visitType";
	public static final String INTENT_USER_NAME = "userName";
	public static final String INTENT_MONTH_SEQ = "monthSeq";
	public static final String INTENT_USER_ID = "userId"; // intent user id
	public static final String INTENT_ID = "id";
	public static final String INTENT_CCONTENT_TEST = "test_content";
	public static final String INTENT_DISCRIPTION_TEST = "test_discription";
	public static final String INTENT_TIME = "time";
	public static final String INTENT_REQUEST_METHOD = "requestMethod";
	public static final String INTENT_AUTH_DATA = "authData";
	public static final String INTENT_LOGIN_PASSWORD = "password";
	public static final String INTENT_LOGIN_PHONE_MODEL = "loginPhoneModel";
	public static final String INTENT_SIM_SERIAL = "serialSim";
	public static final String INTENT_LOGIN_PLATFORM = "platform";
	public static final String INTENT_LOGIN_IS_REMEMBER = "isRemember";
	public static final String INTENT_VERSION_APP = "softVersion";
	public static final String INTENT_VERSION_DB = "dbVersion";
	public static final String INTENT_DATA = "data";
	public static final String INTENT_SEGMENTS = "SEGMENTS";
	public static final String INTENT_INDEX = "INDEX";
	public static final String INTENT_POINT = "POINT";
	public static final String INTENT_POSITION_LAT = "position.lat";
	public static final String INTENT_POSITION_LONG = "position.long";
	public static final String INTENT_CONTENT = "content";
	public static final String INTENT_CHECK_IN_SHARE = "isShared";

	public static final String INTENT_MAP_SHOW_MENU = "isShow";
	public static final String INTENT_COUNTRY_ID = "countryId";
	public static final String INTENT_DISTRICT_ID = "districtId";
	public static final String INTENT_PROVINCE_ID = "provinceId";
	public static final String INTENT_WARD_ID = "wardId";
	public static final String INTENT_LOCATION_ID = "locationId";
	public static final String INTENT_LOCATION_TEXT = "locationText";
	public static final String INTENT_LOCATION_NAME = "locationName";
	public static final String INTENT_LOCATION_ADDRESS = "locationAddress";
	public static final String INTENT_LOCATION_DANHMUC = "locationDanhMuc";
	public static final String INTENT_LOCATION_PHONENUMBER = "locationPhoneNumber";
	public static final String INTENT_LOCATION_CATEGORY_ID = "locationGroupCategoryId";

	public static final String INTENT_SHOW_MAP_VIEW_TYPE = "typeMap";
	public static final String INTENT_MAP_TITLE = "INTENT_MAP_TITLE";
	public static final String INTENT_SCORE_RULEID = "scoreRuleId";

	public static final String INTENT_GLOBAL_BUNDLE = "INTENT_GLOBAL_BUNDLE";
	public static final String INTENT_SESSION_ID = "sessionID";
	public static final String INTENT_LAT = "lat";
	public static final String INTENT_LNG = "lng";
	public static final String INTENT_PAGE = "page";
	public static final String INTENT_NUMTOP = "numTop";

	public static final String INTENT_STAFF_ID = "staffId";
	public static final String INTENT_SALE_TYPE_CODE = "saleTypeCode";
	public static final String INTENT_STAFF_CODE = "staffCode";
	public static final String INTENT_STAFF_NAME = "staffName";
	public static final String INTENT_STAFF_TYPE = "staffTypePreSalesVanSales";
	public static final String INTENT_IS_VIEW_ALL = "isViewAll";
	public static final String INTENT_STAFF_OWNER_ID = "staffOwnerId";
	public static final String INTENT_STAFF_OWNER_CODE = "staffOwnerCode";
	public static final String INTENT_STAFF_OWNER_NAME = "staffOwnerName";
	public static final String INTENT_SHOP_ID = "shopId";
	public static final String INTENT_PARENT_SHOP_ID = "parentShopId";

	public static final String INTENT_STAFF_TRAIN_DETAIL_ID = "staffTrainId";
	public static final String INTENT_STAFF_TRAIN_SCORE = "staffTrainScore";
	public static final String INTENT_STAFF_TRAIN_DATE = "staffTrainDate";

	public static final String INTENT_VISIT_PLAN = "line";
	public static final String INTENT_CUSTOMER_CODE = "cusCode";
	public static final String INTENT_CUSTOMER_ID = "cusID";
	public static final String INTENT_CUSTOMER_NAME = "cusName";
	public static final String INTENT_TYPE_LINE = "typeLine";
	public static final String INTENT_ORDER = "order";
	public static final String INTENT_ORDER_TYPE = "ORDER_TYPE";
	public static final String INTENT_ORDER_ID = "orderId";
	public static final String INTENT_STATE = "state";
	public static final String INTENT_STATUS = "status";
	public static final String INTENT_TYPE_PROBLEM = "typeProblem";
	public static final String INTENT_QUERY_SQLItE = "INTENT_QUERY_SQLItE";
	public static final String INTENT_QUERY_ORACLE = "INTENT_QUERY_ORACLE";
	public static final String INTENT_SQL = "sql";
	public static final String INTENT_PARAMS = "params";

	public static final String INTENT_NAME = "name";
	public static final String INTENT_TYPE = "type";
	public static final String INTENT_VALUE = "value";
	public static final String INTENT_MIN_SCORE_NOTIFICATION = "minScoreNotification";
	public static final String INTENT_OPERATOR = "operator";
	public static final String INTENT_TABLE_NAME = "tableName";
	public static final String INTENT_LIST_PARAM = "listParam";
	public static final String INTENT_LIST_WHERE_PARAM = "listWhereParam";
	public static final String INTENT_LIST_SQL = "listSql";
	public static final String INTENT_LIST_DECLARE = "listDeclare";
	public static final String INTENT_STAFF_ID_PARA = "staff_id";
	public static final String INTENT_IMEI_PARA = "IMEI";
	public static final String INTENT_LOG_ID = "logId";
	public static final String INTENT_LOG_TYPE = "logType";
	public static final String INTENT_COLUMN = "column";
	public static final String INTENT_KEY = "key";
	public static final String INTENT_MD5 = "md5Data";
	// module order
	public static final String INTENT_PRODUCT_ID = "productId";
	public static final String INTENT_PRODUCT_CODE = "productCode";
	public static final String INTENT_PRODUCT_NAME = "productName";
	public static final String INTENT_CTKM_CODE = "ctkmCode";
	public static final String INTENT_INDUSTRY = "industry";
	public static final String INTENT_SUB_INDUSTRY = "subIndustry";
	public static final String INTENT_DISPLAY_PROGRAM_CODE = "programCode";
	public static final String INTENT_DISPLAY_PROGRAM_NAME = "programName";
	public static final String INTENT_DISPLAY_PROGRAM_ID = "displayProgramId";
	public static final String INTENT_DISPLAY_PROGRAM_LEVEL = "displayProgramLevel";
	public static final String INTENT_LIST_DISPLAY_PROGRAM_PRODUCT = "displayProgramProduct";
	public static final String INTENT_LIST_PRODUCT_NOT_IN = "listProductNotIn";
	public static final String INTENT_DISPLAY_TYPE = "typeDisplay";
	public static final String INTENT_DISPLAY_DEPART = "departDisplay";

	// module promotion
	public static final String INTENT_PROMOTION_CODE = "promotionCode";
	public static final String INTENT_PROMOTION_NAME = "promotionName";
	public static final String INTENT_PROMOTION_ID = "promotionId";

	// THUATTQ SYN DATA
	public static final String INTENT_SYN_BEGINDATE = "beginDate";
	public static final String INTENT_SYN_ENDDATE = "endDate";
	public static final String INTENT_SYN_CURRENTDATE = "currentDate";
	public static final String INTENT_SYN_CURRENT_LOCATION_DATE = "currentLocationDate";

	public static final String INTENT_SYN_STAFFID = "staffId";
	public static final String INTENT_SYN_LAST_SYNDATE = "lastSynDate";
	public static final String INTENT_SYN_LAST_SYNSTATUS = "lastSynStatus";
	public static final String INTENT_SYN_INHERITID = "inheritId";

	public static final String INTENT_FIND_ORDER_FROM_DATE = "FROM_DATE";
	public static final String INTENT_FIND_ORDER_TO_DATE = "TO_DATE";
	public static final String INTENT_FIND_ORDER_CUSTOMER_CODE = "CUSTOMER_CODE";
	public static final String INTENT_FIND_ORDER_CUSTOMER_NAME = "CUSTOMER_NAME";
	public static final String INTENT_FIND_ORDER_TYPEROUTE = "TYPEROUTE";
	public static final String INTENT_FIND_ORDER_BILL_CATEGORY = "ORDERBILLCATEGORY";
	public static final String INTENT_FIND_ORDER_STATUS = "STATUS";
	public static final String INTENT_FIND_ORDER_STAFFID = "STAFFID";
	public static final String INTENT_FIND_ORDER_SHOP_ID = "SHOP_ID";
	public static final String INTENT_FIND_ORDER_BAPPROVED = "BAPPROVED";
	public static final String INTENT_FROM_ORDER_MNG = "FROM_ORDER_MNG";
	public static final String INTENT_LAST_LOG_ID = "lastLogId";
	public static final String INTENT_MAX_LOG_ID = "maxLogId";
	public static final String INTENT_GET_MAX_ID_DATA = "getMaxIdData";
	public static final String INTENT_CUSTOMER_ADDRESS = "customer add";
	public static final String INTENT_PRODUCT_CAT = "product cat";
	public static final String INTENT_PRODUCT_SUB_CAT = "product sub cat";
	public static final String INTENT_CUSTOMER = "customerDTO";
	public static final String INTENT_IS_ALL = "isAll";

	public static final String INTENT_DESCR = "descr";
	public static final String INTENT_PRODUCTHASPROMOTIONDTO = "ProductHasPromotionDTO";
	public static final String INTENT_SALE_ORDER_ID = "saleOrderId";
	public static final String INTENT_PRODUCTS_ADD_ORDER_LIST = "productAddOrderList";

	public static final String INTENT_NOTE_OBJECT = "noteObject";
	public static final String INTENT_TRANSFER_ORDER_LIST = "transferListOrder";
	public static final String INTENT_SUGGEST_ORDER_LIST = "suggestListOrder";
	public static final String INTENT_REMIND_DATE = "remind_date";
	public static final String INTENT_FEEDBACK_DTO = "feedbackdto";
	public static final String INTENT_DONE_DATE = "done_date";
	public static final String INTENT_IMEI = "deviceIMEI";
	public static final String INTENT_CHANGE_PRODUCT_PROMOTION = "listProductChange";
	public static final String INTENT_PRODUCT_PROMOTION_SELECTED = "productSelected";
	public static final String INTENT_PRODUCT_PROMOTION_OLD_SELECTED = "oldProductSelected";
	public static final String INTENT_PRIORITY_CODE = "priorityCode";
	public static final String INTENT_YEAR = "year";
	public static final String INTENT_MONTH = "month";
	public static final String INTENT_DAY = "fullDate";
	public static final String INTENT_HOUR = "hour";
	public static final String INTENT_MINUTE = "minute";
	public static final String INTENT_SECOND = "second";
	public static final String INTENT_UPDATE_EDITED_ORDER = "editOrder";
	public static final String INTENT_CUSTOMER_LIST_ITEM = "CustomerListItem";
	public static final String INTENT_IS_FROM_CREATE_ORDER = "isFromCreateOrder";
	public static final String INTENT_STAFF_DTO = "staffDTO";
	public static final String INTENT_IS_OR = "isOr";
	public static final String INTENT_IS_SYNC = "isSync";
	public static final String INTENT_CURRENT_TIME = "currentTime";
	public static final String INTENT_IS_GOT_COUNT = "gotCount";
	public static final String INTENT_TRAINING_DETAIL_ID = "trainingDetailId";
	public static final String INTENT_CUSTOMER_OBJECT_TRAINING = "customerObjectTraining";
	public static final String INTENT_REVIEWS_INFO = "ReviewsStaffInfo";

	public static final String INTENT_ARRAY_IMAGE_PHOTOS = "arrays_photo";
	public static final String INTENT_TIP_ID = "tip_id";
	public static final String INTENT_ALBUM_INFO = "album_info";
	public static final String INTENT_MAX_IMAGE_PER_PAGE = "max_image_per_page";
	public static final String INTENT_ALBUM_TYPE = "album_type";
	public static final String INTENT_ARRAY_PHOTOS_DTO = "array_photos_dto";
	public static final String INTENT_HAS_REMAIN_PRODUCT = "hasRemainProduct";
	public static final String INTENT_LAST_MEDIA_ID = "last_media_id";
	public static final String INTENT_LAST_CREATED_TIME = "last_created_time";
	public static final String INTENT_ALBUM_INDEX_IMAGE = "album_index_image";
	public static final String INTENT_TAKEN_PHOTO = "taken_photo";
	public static final String INTENT_PHOTO_URL = "photo_url";
	public static final String INTENT_OBJECT_TYPE = "object_type";
	public static final String INTENT_GET_WRONG_PLAN = "getWrongPlan";
	// common check pagging
	public static final String INTENT_CHECK_PAGGING = "check_pggging";

	public static final String INTENT_STAFF_PHONE = "staff_phone";

	public static final String INTENT_CHECK_COMBOBOX = "check_request_combobox";

	public static final String INTENT_IS_GET_NUM_TOTAL_ITEM = "isGetCount";

	public static final String INTENT_IMAGE_ID = "id";
	public static final String INTENT_FILE_NAME = "fileName";
	public static final String INTENT_EXECUTE_UPLOAD_PHOTO = "executeUpload";
	public static final String INTENT_MEDIA_ITEM_OBJECT = "mediaItemObject";
	public static final String INTENT_OBJECT_ID = "objectId";
	public static final String INTENT_OBJECT_TYPE_IMAGE = "objectType";
	public static final String INTENT_MEDIA_TYPE = "mediaType";
	public static final String INTENT_URL = "url";
	public static final String INTENT_THUMB_URL = "thumbUrl";
	public static final String INTENT_CREATE_DATE = "createDate";
	public static final String INTENT_CREATE_USER = "createUser";
	public static final String INTENT_FILE_SIZE = "fileSize";
	public static final String INTENT_CUSTOMER_STREET = "customerStreet";
	public static final String INTENT_GET_TOTAL_PAGE = "getTotalPage";
	public static final String INTENT_LOAD_SALE_IN_MONTH = "loadSaleInMonth";
	public static final String INTENT_ARRAY_STRING_LIST = "array string list";
	public static final String INTENT_GET_LIST_LOG = "get list log";
	public static final String INTENT_SALE_ROAD_MAP = "sale_road_map";
	public static final String INTENT_STAFF_LIST = "staff_list";
	public static final String INTENT_ROLE_TYPE = "roleType";
	public static final String INTENT_NOTIFY_INACTIVE = "notifyInactive";
	public static final String INTENT_SERVER_DATE = "serverDate";
	public static final String INTENT_HAVE_RETURN_ORDER = "haveReturnOrder";
	public static final String INTENT_NUM_RETURN_ORDER = "numReturnOrder";
	public static final String INTENT_SHOP_CODE = "shop_code";
	public static final String INTENT_SHOP_NAME = "shop_name";
	public static final String INTENT_NVBH_STAFF_ID = "nvbh_staff_id";
	public static final String INTENT_NVBH_STAFF_NAME = "nvbh_staff_name";
	public static final String INTENT_LIST_NPP_GSNPP = "listNpp_GSNPP";
	public static final String INTENT_PERCENT_PROGRESS_REPORT_MONTH = "percent";
	public static final String INTENT_CURRENT_REPORT_MONTH_CELL_DTO = "currentReportMonth";
	public static final String INTENT_DATA_REPORT_PROGRESS_SALES_FOCUS = "reportProgressSaleFocus";
	public static final String INTENT_INDEX_REPORT_PROGRESS_SALES_FOCUS = "reportProgressIndexSaleFocus";
	public static final String INTENT_DAY_OF_WEEK = "dayOfWeek";
	public static final String INTENT_NVBH_TRAIN_DETAIL_ID = "nvbh_trainig_plan_detail_id";
	public static final String INTENT_TBHV_TRAINING_PLAN_TODAY_ITEM = "tbhv gsnpp trainig plan item";
	public static final String INTENT_TBHV_LIST_TRAINING_SHOP_OBJECT = "listTrainingShopManagerResult";
	public static final String INTENT_TBHV_SELECTED_GSNPP_BEFORE_SCREEN = "selectedGSNPPBeforeScreen";
	public static final String INTENT_TBHV_GSNPP_TRAINING_PLAN_ROW_ITEM = "tbhv gsnpp trainig plan item row";
	public static final String INTENT_VISIT_PLAN_DTO = "INTENT_VISIT_PLAN_DTO";
	public static final String INTENT_DISPLAY_PROGRAME_CODE = "tbhvDisProCode";
	public static final String INTENT_TBHV_DISPROCOM_PROG_REPORT_ITEM = "TBHVDisProComProgReportItem";
	public static final String INTENT_TBHV_CTTB_CODE = "CodeCTTBTBHV";
	public static final String INTENT_TBHV_LIST_CTTB_NPP = "ListCTTBNPP";
	public static final String INTENT_TBHV_CURRENT_CTTB_NPP_SELECTED = "CTTBCurrentNPPSelected";
	public static final String INTENT_TBHV_GSNPP_TRAINING_PLAN_SPINNER = "gsnpp training plan spinner";
	public static final String INTENT_GSNPP_STAFF_ID = "gsnpp staff id";
	public static final String INTENT_TYPE_PROBLEM_GSNPP = "typeProblemGSNPP";
	public static final String INTENT_CREATE_USER_STAFF_ID = "createUserStaffId";
	public static final String INTENT_GET_LIST_PROBLEM_TYPE = "getListPRoblemType";
	public static final String INTENT_GSNPP = "gsnpp";
	public static final String INTENT_GSNPP_TRAINING_PLAN_TODAY_ITEM = "gsnpp training plan";
	public static final String INTENT_FEED_BACK_ID = "feedBackId";
	public static final String INTENT_FEED_BACK_STATUS = "feedBackStatus";
	public static final String INTENT_EXIT = "intentExitApp";
	public static final String INTENT_TBHV_ROUTE_SUPERVISION = "tbhvRouteSupervisionDto";
	public static final String OLD_PASS = "oldPassword";
	public static final String NEW_PASS = "newPassword";
	public static final String INTENT_STRING_LIST = "string list";
	public static final String FROM_CUSTOMER_LIST_IMAGE = "ListAlbumUserView";
	public static final String FROM_LIST_ALBUM_DETAIL = "ListAlbumDetail";
	public static final String FROM_VIEW_IMAGE_FULL = "ViewImageFull";
	public static final String INTENT_TBHV_IS_TRAINED_TODAY = "trainedtoday";
	public static final String INTENT_PERCENT_PROGRESS = "percentProgress";
	public static final String INTENT_CUSTOMER_TYPE_ID = "cusTypeID";
	public static final String INTENT_CODE_NPP = "codeNPP";
	public static final String INTENT_FROM = "FROM";
	public static final String INTENT_SENDER = "sender";
	public static final String INTENT_LIST_NVBH = "listNvbh";
	public static final String INTENT_NPP_CODE = "npp code";
	public static final String INTENT_START_TIME_COMPARE = "startTimeCompare";
	public static final String INTENT_MIDDLE_TIME_COMPARE = "middleTimeCompare";
	public static final String INTENT_END_TIME_COMPARE = "endTimeCompare";
	public static final String INTENT_LIST_GSNPP = "listGSNPP";
	public static final String INTENT_LESS_THAN_2_MINS = "less than";
	public static final String INTENT_DTO = "dto";
	public static final String INTENT_DEBIT_ID = "debit id";
	public static final String INTENT_LIST_NPP = "list_npp";
	public static final String INTENT_CUSTOMER_OBJECT = "customerObjectTraining";
	public static final String INTENT_IS_SEARCH = "isSearch";
	public static final String INTENT_DISPLAY_PROGRAM_MODEL = "displayProgrameModel";
	public static final String INTENT_LEVEL_CODE = "levelCode";
	public static final String INTENT_ARRAY_TYPE = "arraytype";
	public static final String INTENT_PROGRESS = "progress";
	public static final String INTENT_STAFF = "staff";
	public static final String INTENT_GET_LIST_STAFF = "lstStaffId";
	public static final String INTENT_LIST_STAFF = "lstStaffId";
	public static final String INTENT_DOCUMENT_TYPE = "type";
	public static final String INTENT_OFFICE_DOCUMENT_ID = "officeDocumentId";
	public static final String INTENT_PARENT_STAFF_ID = "parentStaffID";
	public static final String INTENT_REGID = "registrationKey";
	public static final String INTENT_DISPLAY_PROGRAME = "display programe";
	public static final String INTENT_IS_GET_LIST_TYPE = "is_get_list_type";
	public static final String INTENT_IS_FROM_ROUTE_VIEW = "isFromRouteView";
	public static final String INTENT_CASHIER_STAFF_ID = "cashierStaffId";
	public static final String INTENT_DELIVERY_ID = "deliveryId";
	public static final String INTENT_GET_LIST_GS = "getListGS";
	public static final String INTENT_IS_VIEW_PO = "isViewPO";
	public static final String INTENT_FROM_SUPERVISOR = "supervisor";
	public static final String INTENT_STOCK_DTO = "stockDTO";
	public static final String INTENT_LIST_SHOP_ID = "listShopId";
	public static final String INTENT_SHOP_ID_OWNER = "shopIdOwner";
	public static final String INTENT_INDEX_PARENT = "indexParent";
	public static final String INTENT_INDEX_CHILD = "indexChild";
	public static final String INTENT_TABLE_ROW = "tableRow";
	public static final String CUSTOMER_DEBIT_DETAIL_DTO = "CustomerDebitDetailDTO";
	public static final String CUSTOMER_DEBIT_DETAIL_ITEM = "CustomerDebitDetailItem";
	public static final String INTENT_SUP_TRAINING_PLAN_ID = "supTrainingPlanId";
	public static final String INTENT_PRI_UTIL = "priUtil";
	public static final String INTENT_IS_SEND = "isSend";
	public static final String INTENT_LIST_STANDARD = "listStandard";
	public static final String INTENT_ITEM_REPORT_LOST_DEVICE = "reportLostDevice";
	public static final String INTENT_ID_PERIOD = "idPeriod";
	public static final String INTENT_LIST_INVENTORY_DEVICE = "listInventoryDevice";
	public static final String INTENT_ITEM_INVENTORY_DEVICE = "inventoryDevice";
	public static final String INTENT_EQUIP_ATTACH_FILE_ID = "equipAttachFileID";
	public static final String INTENT_EQUIP_LOST_MOBILE_REC_DATA = "equipLostMobileRec";
	public static final String INTENT_EQUIP_FORM_HISTORY = "equipFormHistory";
	public static final String INTENT_EQUIP_HISTORY = "equipHistory";

	// them role, shopProfile
	public static final String INTENT_ROLE_ID = "roleId";
	public static final String INTENT_SHOP_PROFILE_ID = "shopProfileId";

	// Bao cao dong
	public static final String INTENT_SEARCH = "search";
	public static final String INTENT_LIST_ID_ROW_CHOOSE = "lstIDRowChoose";
	public static final String INTENT_TYPE_REPORT_ROW_CHOOSE = "typeRowId";
	public static final String INTENT_TYPE_REPORT_COLUMN_CHOOSE = "typeColumnId";
	public static final String INTENT_TYPE_REPORT_SUB_ROW_CHOOSE = "typeSubRowId";
	public static final String INTENT_LEAVE_JOB = "leaveJob";
	public static final String INTENT_DYNAMIC_OBJECT_COLUMN = "dynamicObjectColumn";
	public static final String INTENT_DYNAMIC_OBJECT_ROW = "dynamicObjectRow";
	public static final String INTENT_FROM_DATE = "fromDate";
	public static final String INTENT_TO_DATE = "toDate";
	public static final String INTENT_FROM_DATE_SQL = "fromDateSQL";
	public static final String INTENT_TO_DATE_SQL = "toDateSQL";
	public static final String INTENT_QUANTITY_ORDER = "quantityOrder";
	public static final String INTENT_QUANTITY_PLAN_ORDER = "quantityPlanOrder";
	public static final String INTENT_KPI_ID = "kpiId";
	public static final String INTENT_TYPE_KPI = "typeKpi";
	public static final String INTENT_TYPE_KPI_ID = "typeKpiID";
	public static final String INTENT_DYNAMIC_OBJECT_CLOUMN_STRING = "dynamicObjectColumnString";
	public static final String INTENT_DYNAMIC_OBJECT_ROW_STRING  = "dynamicObjectRowString";
	public static final String INTENT_REPORT_NAME= "reportName";
	public static final String INTENT_DYNAMIC_REPORT_DATA  = "dynamicReportData";
	public static final String INTENT_COLUMN_ID  = "columnId";
	public static final String INTENT_NUM_ITEM_IN_PAGE  = "numItemInPage";
	public static final String INTENT_IS_EDIT  = "isEdit";
	public static final String INTENT_AREA_TYPE  = "areaType";
	public static final String INTENT_PARRENT_AREA_ID  = "parrentAreaId";
	public static final String INTENT_STATUS_CUSTOMER  = "statusCustomer";
	public static final String INTENT_STATUS_CUSTOMER_OF_LINE  = "statusCustomerOfLine";
	public static final String INTENT_REPORT_TEMPLATE_ID  = "reportTemplateId";
	public static final String INTENT_UPDATE_POSITION_CUSTOMER  = "updatePositionCustomer";
	public static final String INTENT_CUSTOMER_POSITION_LOG  = "customerPositionLog";
	public static final String INTENT_LIST_SHOP_AND_STAFF  = "listShopAndStaff";
	public static final String INTENT_SYS_CAL_UNAPPROVED  = "sysCalUnApproved";
	public static final String INTENT_MANAGER_LEVEL = "managerLevel";
	public static final String INTENT_SWITCH_ACTION = "intentSwitchAction";
	public static final String INTENT_IS_REQUEST_FIRST = "isRequestFirst";
	public static final String INTENT_INHERIT_ID = "inheritID";
	public static final String INTENT_LIST_SHOP = "lstShopInfo";
	public static final String INTENT_SORT_DATA = "sortData";
	public static final String INTENT_SYS_CURRENCY_DIVIDE = "sysCurrencyDivide";
	public static final String PROMOTION_PROGRAME_ID = "promotionProgrameId";
	public static final String INTENT_DATA_LIST_PRODUCT_REMAIN = "lstProductRemain";
	public static final String INTENT_SYS_SALE_ROUTE = "sysSaleRoute";
	public static final String INTENT_IS_STAFF_OWNER_ID = "isStaffOwnerId";
	public static final String INTENT_ROUTING_ID = "routingId";
	public static final String INTENT_FEEDBACK_STAFF_DTO = "feedbackstaffdto";
	public static final String INTENT_FEEDBACK_STAFF_CUSTOMER_DTO = "feedbackstaffcustomerdto";
	public static final String INTENT_ORG_ID= "orgId";
	public static final String INTENT_KEYSHOP_ID= "keyShopId";
	public static final String INTENT_LIST_KEYSHOP_ID= "lstKeyShopId";
	public static final String INTENT_MIN_NUM_TAKEN_KEYSHOP= "minNumTaken";
	public static final String INTENT_LIST_LEVEL_OF_KEY_SHOP= "lstLevelOfKeyShop";
	public static final String INTENT_CYCLE_ID= "cycleId";
	public static final String INTENT_FROM_CYCLE_ID= "fromCycleId";
	public static final String INTENT_TO_CYCLE_ID= "toCycleId";
	public static final String INTENT_IS_TAKE_PHOTO= "isTakePhoto";
	public static final String INTENT_REPORT_RESULT= "result";
	public static final String INTENT_REPORT_AWARD= "award";
	public static final String INTENT_KS_ID = "ksId";

	public static final String RADIUS_OF_POSITION_FUSED = "radiusOfPositionFused";
	public static final String FAST_INTERVAL_FUSED_POSITION = "fastIntervalFusedPosition";
	public static final String INTERVAL_FUSED_POSITION = "intervalFusedPosition";
	public static final String FUSED_POSTION_PRIORITY = "fusedPostionPriority";
	public static final String FUSED_POSTION_RESTART = "fusedPostionRestart";
	public static final String INTENT_FEEDBACK_ATTACH_DTO = "feedBackStaffAttach";
	public static final String INTENT_REGISTER_APPROVED_KEYSHOP = "registerApprovedKeyshop";
	public static final String INTENT_FORCE_CREATE_FILE = "forceCreateFile";
	public static final String INTENT_GET_UNPLAN = "intentGetUnplan";
	public static final String INTENT_GET_SHOP_DATA = "intentGetShopData";
	public static final String INTENT_ROUTING_CODE = "routingCode";
	public static final String INTENT_TITLE = "title";
	public static final String INTENT_IS_NEED_GEN_PRICE = "isNeedGenPrice";
	
	public static final String INTENT_STATISTIC_RECORD_TYPE = "statisticRecordType";
	public static final String INTENT_STATISTIC_OBJECT_TYPE = "statisticObjectType";

	public static final String INTENT_LAST_ORDINAL_REMAIN = "lastOrdinalRemain";
	public static final String INTENT_EQUIP_ID = "equipId";
	public static final String INTENT_EQUIP_STATISTIC_RECORD_ID = "equipStatisticRecordId";
	public static final String INTENT_EQUIP_STATISTIC_RECORD = "equipStatisticRecord";
	
	public static final String INTENT_FROM_VIEW = "fromView";
	public static final String INTENT_OLD_CUSTOMER = "oldCustomer";
}
