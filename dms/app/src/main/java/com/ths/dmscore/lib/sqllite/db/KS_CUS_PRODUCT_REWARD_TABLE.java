package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.KSCusProductRewardDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Table sp tra thuong cho kh
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KS_CUS_PRODUCT_REWARD_TABLE extends ABSTRACT_TABLE {
	// id khach hang
	public static final String KS_CUS_PRODUCT_REWARD_ID = "KS_CUS_PRODUCT_REWARD_ID";
	public static final String KS_CUSTOMER_ID = "KS_CUSTOMER_ID";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String PRODUCT_NUM = "PRODUCT_NUM";
	public static final String PRODUCT_NUM_DONE = "PRODUCT_NUM_DONE";
	public static final String STATUS = "STATUS";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	public static final String KS_ID = "KS_ID";
	public static final String TABLE_NAME = "KS_CUS_PRODUCT_REWARD";

	public KS_CUS_PRODUCT_REWARD_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { KS_CUS_PRODUCT_REWARD_ID, KS_CUSTOMER_ID,
				PRODUCT_ID, PRODUCT_NUM, PRODUCT_NUM_DONE, UPDATE_DATE,
				UPDATE_USER, CREATE_USER_ID,KS_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public long update(KSCusProductRewardDTO dto) {
		ContentValues value = initDataRowUpdate(dto);
		String[] params = { "" + dto.ksCusProductRewardId };
		return update(value, KS_CUSTOMER_ID + " = ?", params);
	}

	private ContentValues initDataRowUpdate(KSCusProductRewardDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PRODUCT_NUM_DONE, dto.productNumDone);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(UPDATE_USER, dto.updateUser);
		return editedValues;
	}

}
