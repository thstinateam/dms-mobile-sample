package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.KeyShopDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopViewDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffDTO;
import com.ths.dmscore.dto.view.VoteKeyShopImageViewDTO;
import com.ths.dmscore.dto.view.VoteKeyShopViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dms.R;

/**
 * Man hinh chup hinh key shop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class VoteKeyShopView extends BaseFragment implements OnItemSelectedListener,OnItemClickListener {
	TextView tvKeyShop;
	Spinner spKeyShop;
	Button btTakeImage; // button chup anh
	Button btRegisterKeyShop;// buttong dang ki keyshop
	Button btEnd;// button dong
	GridView gvImageView; // ds hinh anh
	private int indexKeyShop = -1;// index spinner key shop
	public CustomerListItem customerItem; // thong tin kh
	VoteKeyShopViewDTO keyShopViewDTO = new VoteKeyShopViewDTO();
	private ImageAdapter thumbs = null;// adapter image
	private int numTopLoaded = 0;
	private boolean isAbleGetMore = true;
	private boolean isFirst = false;
	private TextView tvMinPhotoNum;// so luong hinh anh toi thieu phai chup
	private static final int SAVE_VOTE_KEYSHOP_ACTION = 1;
	private static final int CANCEL_SAVE_VOTE_KEYSHOP_ACTION = 2;
	LinearLayout llImage;
	TextView tvNoDataResult;
	private TextView tvNumPhoto;// so hinh anh da chup
	private boolean isGetTotal = true;
	// dialog product detail view
	RegisterKeyShopView keyShopView;
	// dto cho man hinh keyshop
	private RegisterKeyShopViewDTO registerKeyShopDTO;// dang ky keyshop
	boolean isUpdateRegisterKeyShop = false;// bien kiem tra dang o trang thai update keyshop hay ko
	long ksId = 0; // CT Keyshop hien tai
	String cycleId = "0"; // cycleId hien tai dang chon
	private long fromCycleId;// tu chu ki
	private long toCycleId; // den chu ki
	// dialog product detail view
	ReportListCustomerView reportView; // man hinh bao cao ct httm khach hang tham gias

	public static VoteKeyShopView getInstance(Bundle data) {
		VoteKeyShopView instance = new VoteKeyShopView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.customerItem = (CustomerListItem) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_vote_key_shop_view, container, false);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_CHAMKEYSHOP);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VOTE_KEY_SHOP));
		initView(v);
		//parent.removeMenuFinishCustomer();
		if(!isFirst){
			getListKeyShop();
			getReportListCustomer();
			isFirst = true;
		}
		else{
			gvImageView.setAdapter(thumbs);
			renderLayout();
		}
		return v;
	}

	 /**
	 * Khoi tao view
	 * @author: Tuanlt11
	 * @param v
	 * @return: void
	 * @throws:
	*/
	public void initView(View v) {
		tvKeyShop = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvKeyShop, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_TITLE);
		spKeyShop = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spKeyShop, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_SPINNERKEYSHOP);
		PriUtils.getInstance().setOnItemSelectedListener(spKeyShop, this);
		btRegisterKeyShop = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btRegisterKeyShop, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_DANGKYKEYSHOP);
		PriUtils.getInstance().setOnClickListener(btRegisterKeyShop, this);
		btTakeImage = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btTakeImage, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_CHUPHINHKEYSHOP);
		PriUtils.getInstance().setOnClickListener(btTakeImage, this);
		btEnd = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btEnd, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_KETTHUC);
		PriUtils.getInstance().setOnClickListener(btEnd, this);
		gvImageView = (GridView) PriUtils.getInstance().findViewById(v, R.id.gvImageView, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_GRIDHINHANH, PriUtils.CONTROL_GRIDVIEW);
		PriUtils.getInstance().setOnItemClickListener(gvImageView, this);
		tvMinPhotoNum = (TextView)v.findViewById(R.id.tvMinPhotoNum);
		llImage = (LinearLayout)v.findViewById(R.id.llImage);
		tvNoDataResult = (TextView)v.findViewById(R.id.tvNoDataResult);
		tvNumPhoto = (TextView)v.findViewById(R.id.tvNumPhoto);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		if(arg0 == spKeyShop){
			if(indexKeyShop != position){
				indexKeyShop = position;
				resetAllValue();
				setMinNumPhoto();
				getListImageOfKeyShop(true);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btEnd) {
			GlobalUtil.showDialogConfirm(this, this.parent,
					StringUtil.getString(R.string.TEXT_MESSAGE_CONFIRM_SAVE_VOTE_KEYSHOP),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					SAVE_VOTE_KEYSHOP_ACTION,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					CANCEL_SAVE_VOTE_KEYSHOP_ACTION, null);
		} else if( v == btTakeImage){
			checkDistanceAndTakeImage();
		}else if(v == btRegisterKeyShop){
			getAllListKeyShop();
//			showDialogRegisterKeyShop();
		}else {
			super.onClick(v);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		AlbumDTO album = new AlbumDTO();
		album.setListPhoto(keyShopViewDTO.image.lstImage);
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, album);
		bundle.putInt(IntentConstants.INTENT_ALBUM_INDEX_IMAGE, arg2);
		bundle.putString(IntentConstants.INTENT_FROM, GlobalUtil.getTag(VoteKeyShopView.class));
		handleSwitchFragment(bundle, ActionEventConstant.ACTION_LOAD_IMAGE_FULL, UserController.getInstance());

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.ACTION_GET_LIST_KEY_SHOP:{
			VoteKeyShopViewDTO dto = (VoteKeyShopViewDTO)modelEvent.getModelData();
			keyShopViewDTO.lstKeyShop = dto.lstKeyShop;
			keyShopViewDTO.shopDistance = dto.shopDistance;
			keyShopViewDTO.image.totalImage = dto.image.totalImage;
			keyShopViewDTO.image.lstImage.clear();
			keyShopViewDTO.image.lstImage.addAll(dto.image.lstImage);
			int size = dto.image.lstImage.size();
			numTopLoaded = size;
			if (size < Constants.NUM_LOAD_MORE_IMAGE) {
				isAbleGetMore = false;
			}
			if(keyShopViewDTO.lstKeyShop.size() > 0){
				renderLayout();
			} else{
				gotoNextView();
			}
			requestInsertLogKPI(HashMapKPI.NVBH_CHUPHINHKEYSHOP, modelEvent.getActionEvent());
			parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.ACTION_GET_LIST_IMAGE_KEY_SHOP:
			VoteKeyShopImageViewDTO temp = (VoteKeyShopImageViewDTO)modelEvent.getModelData();
//			keyShopViewDTO.lstImage.clear();
			if(isGetTotal)
				keyShopViewDTO.image.totalImage = temp.totalImage;
			keyShopViewDTO.image.lstImage.addAll(temp.lstImage);
			int size = temp.lstImage.size();
			numTopLoaded += size;
			if (size < Constants.NUM_LOAD_MORE_IMAGE) {
				isAbleGetMore = false;
			}
			renderLayoutImage();
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER:
			resetAllValue();
			getListImageOfKeyShop(true);
			break;
		case ActionEventConstant.ACTION_CHECK_LIST_IMAGE_KEY_SHOP:
			@SuppressWarnings("unchecked")
			ArrayList<KeyShopDTO> result = (ArrayList<KeyShopDTO> )modelEvent.getModelData();
			if(result == null || result.isEmpty()){
				gotoNextView();
			}else{
				String programList = getListCodeNameKeyShop(result);
				SpannableObject obj = new SpannableObject();
				obj.addSpan(StringUtil.getString(R.string.TEXT_MISS_IMAGE_KEYSHOP),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				obj.addSpan(programList,
						ImageUtil.getColor(R.color.RED),
						android.graphics.Typeface.NORMAL);
				GlobalUtil.showDialogConfirm(parent, obj.getSpan(),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
						false);
			}
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_GET_ALL_LIST_KEYSHOP:
			registerKeyShopDTO = (RegisterKeyShopViewDTO)modelEvent.getModelData();
			showDialogRegisterKeyShop();
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP:
			RegisterKeyShopViewDTO dtoTemp = (RegisterKeyShopViewDTO)modelEvent.getModelData();
			registerKeyShopDTO.lstItem = dtoTemp.lstItem;
			registerKeyShopDTO.lstCycle = dtoTemp.lstCycle;
			showDialogRegisterKeyShop();
			parent.closeProgressDialog();
			if(isUpdateRegisterKeyShop){
				parent.showDialog(StringUtil.getString(R.string.TEXT_REGISTER_KEYSHOP_SUCCESS));
				isUpdateRegisterKeyShop = false;
				GlobalUtil.forceHideKeyboard(parent);
			}
			break;
		case ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP:
			isUpdateRegisterKeyShop = true;
			getLevelOfKeyShop(ksId, cycleId,fromCycleId,toCycleId);
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_GET_REPORT_CUSTOMER:
			ReportKeyshopStaffDTO dto = (ReportKeyshopStaffDTO)modelEvent.getModelData();
			showDialogReportCustomer(dto);
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	 /**
	 * Lay ds key shop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getListKeyShop(){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, true);
		data.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, Constants.NUM_LOAD_MORE_IMAGE);
		data.putInt(IntentConstants.INTENT_PAGE, numTopLoaded
				/ Constants.NUM_LOAD_MORE_IMAGE);
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LIST_KEY_SHOP, SaleController.getInstance());
	}

	 /**
	 * Lay ds hinh anh cua Key Shop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getListImageOfKeyShop(boolean isGetTotal){
		this.isGetTotal = isGetTotal;
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putLong(IntentConstants.INTENT_KEYSHOP_ID,keyShopViewDTO.lstKeyShop.get(indexKeyShop).ksId);
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotal);
		data.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, Constants.NUM_LOAD_MORE_IMAGE);
		data.putInt(IntentConstants.INTENT_PAGE, numTopLoaded
				/ Constants.NUM_LOAD_MORE_IMAGE);
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LIST_IMAGE_KEY_SHOP, SaleController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				indexKeyShop = -1;
				resetAllValue();
				getListKeyShop();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	 /**
	 * render layout
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderLayout(){
		int size = keyShopViewDTO.lstKeyShop.size();
		String[] strAdap = new String[size];
		//add text all neu truong hop co hon 2 nganh hang
		int i = 0;
		for (KeyShopDTO dto : keyShopViewDTO.lstKeyShop) {
			strAdap[i] = dto.name;
			i++;
		}
//		SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, strAdap);
        ArrayAdapter adapterNPP = new ArrayAdapter(parent, R.layout.simple_spinner_item, strAdap);
        adapterNPP.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		spKeyShop.setAdapter(adapterNPP);
		spKeyShop.setSelection(0);
		indexKeyShop = 0;
		setMinNumPhoto();
		renderLayoutImage();
	}

	 /**
	 * render hinh anh
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderLayoutImage(){
		if(thumbs == null){
			thumbs = new ImageAdapter(parent, keyShopViewDTO.image.lstImage);
			gvImageView.setAdapter(thumbs);
		}
		thumbs.notifyDataSetChanged();
		if(keyShopViewDTO.image.lstImage.size() == 0){
			llImage.setVisibility(View.GONE);
			tvNoDataResult.setVisibility(View.VISIBLE);
		}else{
			tvNoDataResult.setVisibility(View.GONE);
			llImage.setVisibility(View.VISIBLE);
		}
		SpannableObject obj = new SpannableObject();
		obj.addSpan(StringUtil.getString(R.string.TEXT_NOTIFY_TOTAL) + " ",
				ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.NORMAL);
		obj.addSpan(String.valueOf(keyShopViewDTO.image.totalImage),
				ImageUtil.getColor(R.color.RED),
				android.graphics.Typeface.BOLD);
		obj.addSpan(" " + StringUtil.getString(R.string.TEXT_PHOTO_1),
				ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.NORMAL);
		tvNumPhoto.setText(obj.getSpan());
	}

	 /**
	 * Lay ds hinh anh
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getMorePhoto() {
		if (isAbleGetMore) {
			parent.showLoadingDialog();
			getListImageOfKeyShop(false);
		}
	}

	 /**
	 * Di toi man hinh tiep theo
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void gotoNextView() {
		parent.removeMenuCloseCustomer();
		CustomerListView frag = (CustomerListView) this.findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
		if (frag != null) {
			frag.isBackFromPopStack = true;
		}
		GlobalUtil.popBackStack(parent);
//		parent.initMenuVisitCustomer(customerItem.aCustomer.customerCode,
//				customerItem.aCustomer.customerName);
		if (customerItem.isHaveSaleOrder) {
			gotoRemainProductView(
					// neu co don dat hang & chua dc kiem hang ton
					this.customerItem.aCustomer.getCustomerId(),
					this.customerItem.aCustomer.getCustomerCode(),
					this.customerItem.aCustomer.getCustomerName(),
					this.customerItem.aCustomer.getStreet(),
					this.customerItem.aCustomer.getCustomerTypeId(),
					this.customerItem.aCustomer.cashierStaffID,
					this.customerItem.aCustomer.deliverID);
		} else {
			gotoCreateOrder(this.customerItem);
		}
	}


	 /**
	 * Toi man hinh kiem ton
	 * @author: Tuanlt11
	 * @param customerId
	 * @param customerCode
	 * @param customerName
	 * @param customerAddress
	 * @param customerTypeId
	 * @param cashierID
	 * @param deliveryID
	 * @return: void
	 * @throws:
	*/
	private void gotoRemainProductView(String customerId, String customerCode,
			String customerName, String customerAddress, int customerTypeId, String cashierID, String deliveryID) {
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		b.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		b.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, customerAddress);
		b.putString(IntentConstants.INTENT_IS_OR, String.valueOf(this.customerItem.isOr));
		b.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		b.putString(IntentConstants.INTENT_CASHIER_STAFF_ID, cashierID);
		b.putString(IntentConstants.INTENT_DELIVERY_ID, deliveryID);
		b.putSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM, customerItem);
		handleSwitchFragment(b, ActionEventConstant.GO_TO_REMAIN_PRODUCT_VIEW, SaleController.getInstance());
	}


	 /**
	 * Toi man hinh dat hang
	 * @author: Tuanlt11
	 * @param dto
	 * @return: void
	 * @throws:
	*/
	private void gotoCreateOrder(CustomerListItem dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, dto.aCustomer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, dto.aCustomer.getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS, dto.aCustomer.getStreet());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, dto.aCustomer.getCustomerCode());
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "0");
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, dto.aCustomer.getCustomerTypeId());
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				dto.aCustomer.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID,
				dto.aCustomer.deliverID);
		bundle.putSerializable(IntentConstants.INTENT_SUGGEST_ORDER_LIST, null);
		bundle.putString(IntentConstants.INTENT_IS_OR, String.valueOf(dto.isOr));
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_NEW);
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, dto.routingId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * Upload hinh chup cham trung bay
	 *
	 * @author: BANGHN
	 */
	public void updateTakenPhoto() {

		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		viewData.add(IntentConstants.INTENT_STAFF_ID);
		viewData.add(Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));

		viewData.add(IntentConstants.INTENT_SHOP_ID);
		if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId())) {
			viewData.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}

		if (parent.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(parent.takenPhoto.getName());
			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(parent.takenPhoto.getAbsolutePath());
		}

		// tao doi tuong mediaItem de insert row du lieu
		MediaItemDTO dto = new MediaItemDTO();

		try {
			dto.objectId = customerItem.aCustomer.customerId;
			dto.objectType = MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = parent.takenPhoto.getAbsolutePath();
			dto.thumbUrl = parent.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			dto.displayProgrameId = keyShopViewDTO.lstKeyShop.get(indexKeyShop).ksId;
			dto.fileSize = parent.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			dto.routingId = customerItem.routingId;
//			dto.type = 1;
			dto.status = 1;
			if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId())) {
				dto.shopId = Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			}
			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(String.valueOf(dto.objectId));
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));

			viewData.add(IntentConstants.INTENT_DISPLAY_PROGRAM_ID);
			viewData.add(""+keyShopViewDTO.lstKeyShop.get(indexKeyShop).ksId);
			viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
			viewData.add(this.customerItem.aCustomer.customerCode);
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
			viewData.add(IntentConstants.INTENT_ROUTING_ID);
			viewData.add(Constants.STR_BLANK + dto.routingId);
		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}

		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	/**
	 * Kiem tra khoang cach hop le va thuc hien chup anh
	 * @author: banghn
	 */
	private void checkDistanceAndTakeImage(){
		LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile()
				.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
				.getProfile().getMyGPSInfo().getLongtitude());
		if (checkDistance()) {
			parent.takenPhoto = GlobalUtil.takePhoto(this,
					GlobalBaseActivity.RQ_TAKE_PHOTO_VOTE_DP);
		} else {
			ServerLogger.sendLog("VoteKeyShopView",
					"Khoang cach xa hon cho phep : CUS="
							+ this.customerItem.aCustomer.customerCode + " (Lat, Lng)="
							+ myLoc.lat +"," + myLoc.lng,
							true, TabletActionLogDTO.LOG_CLIENT);

			//thong bao len view va tiep tuc
			SpannableObject msg = new SpannableObject();
			msg.addSpan(StringUtil
					.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_1),
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.NORMAL);

			msg.addSpan(
					this.customerItem.aCustomer.customerCode
							+ "-"
							+ this.customerItem.aCustomer.customerName,
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.BOLD);

			msg.addSpan(StringUtil
					.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_2),
					ImageUtil.getColor(R.color.WHITE),
					android.graphics.Typeface.NORMAL);
			// dong dialog waitting dang luong chup anh
			parent.closeProgressDialog();
			// show msg thong bao
			parent.showDialog(msg.getSpan());
//			continueAfterVoteDP();
		}
	}

	/**
	 * check khoang cach thoa dieu kien de CTB
	 * @author: Tuanlt11
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	private boolean checkDistance(){
		ActionLogDTO action = GlobalInfo.getInstance().getProfile().getActionLogVisitCustomer();
		if (action != null) {
			LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile()
					.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
					.getProfile().getMyGPSInfo().getLongtitude());
			LatLng cusLoc = new LatLng(this.customerItem.aCustomer.lat,
					this.customerItem.aCustomer.lng);
			if (GlobalUtil.getDistanceBetween(myLoc, cusLoc) <= keyShopViewDTO.shopDistance) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			String filePath = "";
			switch (requestCode) {
			case GlobalBaseActivity.RQ_TAKE_PHOTO_VOTE_DP: {
				if (resultCode == Activity.RESULT_OK && parent.takenPhoto != null) {
					filePath = parent.takenPhoto.getAbsolutePath();
					ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
							parent, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
					validator.setDataIntent(data);
					if (validator.execute()) {
						updateTakenPhoto();
					}
				} else {
					parent.closeProgressDialog();
				}
				break;
			}

			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;

			}
		} catch (Exception ex) {
			ServerLogger.sendLog("ActivityState",
					VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	 /**
	 * clear data
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void resetAllValue(){
		gvImageView.smoothScrollToPosition(0);
		numTopLoaded = 0;
		isAbleGetMore = true;
		isGetTotal = true;
		keyShopViewDTO.image.lstImage.clear();
	}

	 /**
	 * Kiem tra so luong hinh anh cua keyshop tham gia
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void validateListImageOfKeyShop(){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putStringArrayList(IntentConstants.INTENT_LIST_KEYSHOP_ID,getListIdKeyShop());
//		data.putInt(IntentConstants.INTENT_MIN_NUM_TAKEN_KEYSHOP,keyShopViewDTO.minNumTaken);
		handleViewEvent(data, ActionEventConstant.ACTION_CHECK_LIST_IMAGE_KEY_SHOP, SaleController.getInstance());
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case SAVE_VOTE_KEYSHOP_ACTION:
			validateListImageOfKeyShop();
			break;
		case ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP:{
			ksId = ((Bundle)data).getLong(IntentConstants.INTENT_KEYSHOP_ID);
			cycleId = ((Bundle)data).getString(IntentConstants.INTENT_CYCLE_ID);
			fromCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_FROM_CYCLE_ID);
			toCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_TO_CYCLE_ID);
			getLevelOfKeyShop(ksId,cycleId,fromCycleId,toCycleId);
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP:
			@SuppressWarnings("unchecked")
			ArrayList<RegisterKeyShopItemDTO> lstItemClone = (ArrayList<RegisterKeyShopItemDTO>) ((Bundle) data)
					.getSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP);
			cycleId = ((Bundle) data).getString(IntentConstants.INTENT_CYCLE_ID);
			ksId = ((Bundle)data).getLong(IntentConstants.INTENT_KEYSHOP_ID);
			fromCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_FROM_CYCLE_ID);
			toCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_TO_CYCLE_ID);
			updateRegisterKeyShop(lstItemClone,cycleId);
			break;
		default:
			break;
		}
	}

	 /**
	 * Lay ds Id cua keyshop
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<String>
	 * @throws:
	*/
	private ArrayList<String> getListIdKeyShop() {
		ArrayList<String> lstId = new ArrayList<String>();
		for (int i = 0, size = keyShopViewDTO.lstKeyShop.size(); i < size; i++) {
			lstId.add(keyShopViewDTO.lstKeyShop.get(i).ksId + "");
		}
		return lstId;
	}

	/**
	 * Lay ds code + name cua keyshop
	 * @author: Tuanlt11
	 * @return
	 * @return: ArrayList<String>
	 * @throws:
	*/
	private String getListCodeNameKeyShop(ArrayList<KeyShopDTO> lstKeyshop) {
		StringBuilder builder = new StringBuilder();
		for (KeyShopDTO keyShopDTO : lstKeyshop) {
			builder.append(String.format("\n%s - %s: %d/%d", keyShopDTO.ksCode, keyShopDTO.name, 
					keyShopDTO.takenPhotoNum, keyShopDTO.minPhotoNum));
		}
		return builder.toString();
	}

	 /**
	 * set so luong hinh anh toi thieu phai chup cho moi keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void setMinNumPhoto() {
		if (keyShopViewDTO.lstKeyShop.size() > 0) {
			SpannableObject strNotice = new SpannableObject();
			strNotice.addSpan(StringUtil
					.getString(R.string.TEXT_NOTIFY_NUM_PHOTO_MIN_KEYSHOP),
					ImageUtil.getColor(R.color.BLACK), Typeface.NORMAL);
			strNotice.addSpan(Constants.STR_SPACE + keyShopViewDTO.lstKeyShop.get(indexKeyShop).minPhotoNum,
					ImageUtil.getColor(R.color.BLACK), Typeface.BOLD);
			tvMinPhotoNum.setText(strNotice.getSpan());
		}
	}

	public class ImageAdapter extends BaseAdapter {

		private ArrayList<PhotoDTO> list = new ArrayList<PhotoDTO>();
		public ImageAdapter(Context c,ArrayList<PhotoDTO> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup pt) {
			View row = convertView;
			ViewHolder holder = null;

			if (convertView == null) {
				LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) parent)
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = layout.inflate(R.layout.layout_album_detail_user, pt, false);
				holder = new ViewHolder();
				holder.imageView = (ImageView) row
						.findViewById(R.id.imgAlbumImage);
				holder.staffName = (TextView) row
						.findViewById(R.id.tvStaffName);
				holder.staffName.setVisibility(View.GONE);
				holder.titleAlbum = (TextView) row
						.findViewById(R.id.tvAlbumName);
				holder.titleAlbum.setTextColor(getResources().getColor(
						R.color.COLOR_TEXT));
				holder.titleAlbum.setTypeface(null,
						android.graphics.Typeface.NORMAL);
                if(GlobalUtil.getInstance().checkIsTablet(parent)){
                    holder.titleAlbum.setTextSize(GlobalUtil.dip2Pixel(16));
                }else{
                    holder.titleAlbum.setTextSize(GlobalUtil.dip2Pixel(7));
                }
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			holder.imageView.setImageResource(R.drawable.album_default);
			if (!StringUtil.isNullOrEmpty(list.get(position).thumbUrl)) {
				if (list.get(position).thumbUrl.contains(ExternalStorage.SDCARD_PATH)){
					ImageUtil.loadImage(list.get(position).thumbUrl, holder.imageView);
				}
				else{
					ImageUtil.loadImage(ServerPath.IMAGE_PATH +  list.get(position).thumbUrl, holder.imageView);
				}

			}
			holder.titleAlbum.setText(list.get(position).createdTime);
			if (position == numTopLoaded - 1) {
				getMorePhoto(); // lay them hinh anh
			}

			return row;
		}
	}

	public static class ViewHolder {
		public ImageView imageView;
		public ImageView imageViewBg;
		public TextView titleAlbum;
		public TextView staffName;
	}

	 /**
	 * Lay tat ca key shop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getAllListKeyShop(){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putInt(IntentConstants.INTENT_REGISTER_APPROVED_KEYSHOP, GlobalInfo.getInstance().getRegisterApprovedKeyshop());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_ALL_LIST_KEYSHOP, SaleController.getInstance());
	}

	 /**
	 * Lay cac muc cua keyshop
	 * @author: Tuanlt11
	 * @param ksId
	 * @param cycleId
	 * @return: void
	 * @throws:
	*/
	private void getLevelOfKeyShop(long ksId, String cycleId, long fromCycleId, long toCycleId){
		if(!parent.isShowProgressDialog())
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putLong(IntentConstants.INTENT_KEYSHOP_ID, ksId);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		data.putLong(IntentConstants.INTENT_FROM_CYCLE_ID, fromCycleId);
		data.putLong(IntentConstants.INTENT_TO_CYCLE_ID, toCycleId);
		data.putInt(IntentConstants.INTENT_REGISTER_APPROVED_KEYSHOP, GlobalInfo.getInstance().getRegisterApprovedKeyshop());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP, SaleController.getInstance());
	}

	 /**
	 * hien thi man hinh dang ky keyshop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void showDialogRegisterKeyShop() {
		if (keyShopView == null) {
			keyShopView = new RegisterKeyShopView(parent, this,
					StringUtil.getString(R.string.TEXT_REGISTER_KEY_SHOP_FULL));
		}
		boolean isFirst = false;
		if (!keyShopView.isShowing()) {
			keyShopView.show();
			keyShopView.getWindow().clearFlags(
	                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
	                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
//			keyShopView.getWindow().setSoftInputMode(
//	                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			isFirst = true;
		}
		keyShopView.renderLayout(registerKeyShopDTO,isFirst);
	}

	 /**
	 * Dang ki keyshop
	 * @author: Tuanlt11
	 * @param cycleId
	 * @return: void
	 * @throws:
	*/
	private void updateRegisterKeyShop(ArrayList<RegisterKeyShopItemDTO> lstItemClone, String cycleId){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP, lstItemClone);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		handleViewEvent(data, ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP, SaleController.getInstance());
	}

	 /**
	 * Lay ds thong tin ct httm ma khach hang tham gia
	 * @author: Tuanlt11
	 * @param page
	 * @return: void
	 * @throws:
	*/
	private void getReportListCustomer() {
		if (!parent.isShowProgressDialog())
			parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_REPORT_CUSTOMER,
				SaleController.getInstance());
	}

	 /**
	 * Hien thi man hinh bao cao ct dang ki cua kh
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void showDialogReportCustomer(ReportKeyshopStaffDTO dto) {
		if (reportView == null) {
			reportView = new ReportListCustomerView(parent, this,
					StringUtil.getString(R.string.TEXT_REPORT_KEY_SHOP));
		}
		if (!reportView.isShowing()) {
			reportView.show();
		}
		reportView.renderLayout(dto.listItem);
	}

}
