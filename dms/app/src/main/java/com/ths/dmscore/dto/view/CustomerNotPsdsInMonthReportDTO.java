package com.ths.dmscore.dto.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.ROUTING_CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 *
 * dto dung cho man hinh Khach hang chua PSDS cua TBHV
 *
 * @author: Nguyen Huu Hieu
 * @version: 1.1
 * @since: 1.0
 */
public class CustomerNotPsdsInMonthReportDTO {
	public int totalList;

	public ArrayList<String> listExcepTionDay;
	public ArrayList<CustomerNotPsdsInMonthReportItem> arrList;

	public CustomerNotPsdsInMonthReportDTO() {
		arrList = new ArrayList<CustomerNotPsdsInMonthReportDTO.CustomerNotPsdsInMonthReportItem>();
	}

	public void addItem(Cursor c) {
		CustomerNotPsdsInMonthReportItem item = new CustomerNotPsdsInMonthReportItem();
		item.numVisitPlan = 0;
		item.customerId = CursorUtil.getString(c, "customerId");
		item.staffId = CursorUtil.getString(c, "staffId");
		item.maKh = CursorUtil.getString(c, "MaKH");
		item.tenKh = CursorUtil.getString(c, "TenKH");
		item.diaChi = CursorUtil.getString(c, "ADDRESS");
		if(item.diaChi.equals(Constants.STR_BLANK)){
			item.diaChi = CursorUtil.getString(c, "SoNha") + " " +  CursorUtil.getString(c, "Duong");
		}
		item.lossDistribution = CursorUtil.getInt(c, "LOSS_DISTRIBUTION");
		item.nvbh = CursorUtil.getString(c, "NVBH");
		item.tuyen = CursorUtil.getString(c, "TUYEN");
		if (!StringUtil.isNullOrEmpty(item.tuyen)
				&& item.tuyen.charAt(0) == ',') {
			item.tuyen = item.tuyen.substring(1);
		}

		int startWeek = 0;
		// int weekInterval = 0;
		String startDateMonth = Constants.STR_BLANK;
		String endDateMonth = Constants.STR_BLANK;
		Integer week1 = null;
		Integer week2 = null;
		Integer week3 = null;
		Integer week4 = null;
		String exceptionDay = Constants.STR_BLANK;
		String tuyenList = Constants.STR_BLANK;
		// get start week
		startWeek = CursorUtil.getInt(c, ROUTING_CUSTOMER_TABLE.START_WEEK, 0);
		tuyenList = CursorUtil.getString(c, "TUYEN_LIST");
		startDateMonth = DateUtils.convertDateOneFromFormatToAnotherFormat(CursorUtil.getString(c, "START_DATE_MONTH"), DateUtils.DATE_FORMAT_DATE, DateUtils.DATE_FORMAT_YEAR_MONTH_DATE);
		endDateMonth = DateUtils.convertDateOneFromFormatToAnotherFormat(CursorUtil.getString(c, "END_DATE_MONTH"), DateUtils.DATE_FORMAT_DATE, DateUtils.DATE_FORMAT_YEAR_MONTH_DATE);

		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		if (c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK1) >= 0) {
			if (!StringUtil.isNullOrEmpty(c.getString(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK1)))) {
				week1 = c.getInt(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK1));
			}
		}
		if (c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK2) >= 0) {
			if (!StringUtil.isNullOrEmpty(c.getString(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK2)))) {
				week2 = c.getInt(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK2));
			}
		}
		if (c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK3) >= 0) {
			if (!StringUtil.isNullOrEmpty(c.getString(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK3)))) {
				week3 = c.getInt(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK3));
			}
		}
		if (c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK4) >= 0) {
			if (!StringUtil.isNullOrEmpty(c.getString(c
					.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK4)))) {
				week4 = c
						.getInt(c.getColumnIndex(ROUTING_CUSTOMER_TABLE.WEEK4));
			}
		}
		arrayList.add(week1);
		arrayList.add(week2);
		arrayList.add(week3);
		arrayList.add(week4);
		item.slgt = c.getString(c.getColumnIndex("SLGT"));
		item.slgtThucTe = Integer.parseInt(item.slgt);

		// tinh so lan ghe tham theo ke hoach
		int countVisitPlan = 0;

		if (!StringUtil.isNullOrEmpty(startDateMonth)
				&& !StringUtil.isNullOrEmpty(endDateMonth)) {
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(new Date(startDateMonth));
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(new Date(endDateMonth));
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"yyyy-MM-dd");
			while (startDate.compareTo(endDate) <= 0) {
				if (((week1 != null || week2 != null || week3 != null || week4 != null) && (arrayList
						.get(startWeek - 1) == null || (arrayList
						.get(startWeek - 1) != null && arrayList.get(
						startWeek - 1).equals(0))))
						|| exceptionDay.contains(simpleDateFormat
								.format(startDate.getTime()))) {
					if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						startWeek++;
						if (startWeek == 5) {
							startWeek = 1;
						}
					}
					startDate.add(Calendar.DATE, 1);
					continue;
				}
				if (tuyenList.contains(String.valueOf(startDate
						.get(Calendar.DAY_OF_WEEK)))) {
					countVisitPlan++;
					for (int i = 0; i < listExcepTionDay.size(); i++) {
						if (listExcepTionDay.get(i).contains(simpleDateFormat
								.format(startDate.getTime()))) {
							item.numVisitPlan ++;
						}
						
					}
				}
				if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					startWeek++;
					if (startWeek == 5) {
						startWeek = 1;
					}
				}
				startDate.add(Calendar.DATE, 1);
			}
		}
		countVisitPlan = countVisitPlan - item.numVisitPlan;
		item.slgt += "/" + countVisitPlan;

		String format = GlobalInfo.getInstance().getSysDateFormat();
		String stOrderDate = CursorUtil.getString(c, "ORDER_DATE");
		Date orderDate = DateUtils.parseDateFromString(stOrderDate,DateUtils.DATE_FORMAT_DATE);
		if(!StringUtil.isNullOrEmpty(orderDate)){
			item.ngayDhCuoi = DateUtils.formatDate(orderDate, format);
		}
		item.dsThangTruoc = CursorUtil.getDouble(c, "DSThangTruoc",1,GlobalInfo.getInstance().getSysNumRounding());
		arrList.add(item);
	}

	/**
	 *
	 * tinh so lan ghe tham theo ke hoach trong thang cua mot khach hang, tinh
	 * tu ngay dau thang den ngay cuoi thang
	 *
	 * @author: HaiTC3
	 * @param visitPlan
	 * @param startWeek
	 * @param weekInterval
	 * @return
	 * @return: int
	 * @throws:
	 * @since: Feb 21, 2013
	 */
	public int getNumberTimesVisitPlanInMonth(String visitPlan, int startWeek,
			int weekInterval) {
		int count = 0;
		// tinh cac thu can di trong tuyen
		int T2 = 0;
		int T3 = 0;
		int T4 = 0;
		int T5 = 0;
		int T6 = 0;
		int T7 = 0;
		int CN = 0;
		String[] listT = visitPlan.split(",");
		for (int i = 0, size = listT.length; i < size; i++) {
			if ("T2".equals(listT[i])) {
				T2 = 1;
			} else if ("T3".equals(listT[i])) {
				T3 = 1;
			} else if ("T4".equals(listT[i])) {
				T4 = 1;
			} else if ("T5".equals(listT[i])) {
				T5 = 1;
			} else if ("T6".equals(listT[i])) {
				T6 = 1;
			} else if ("T7".equals(listT[i])) {
				T7 = 1;
			} else if ("CN".equals(listT[i])) {
				CN = 1;
			}
		}
		// tinh so ngay trong thang hien tai
		Calendar calendar = Calendar.getInstance();
		calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);
		int numDay = calendar.getActualMaximum(Calendar.DATE);
		for (int i = 1; i <= numDay; i++) {
			calendar.set(year, month, i);
			if (checkDayOff(i, month, year) == false) {
				int currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
				int weekOfDayInYear = calendar.get(Calendar.WEEK_OF_YEAR);
				if (currentDayOfWeek == 1 && CN == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 2 && T2 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 3 && T3 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 4 && T4 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 5 && T5 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 6 && T6 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				} else if (currentDayOfWeek == 7 && T7 == 1) {
					if (weekOfDayInYear >= startWeek
							&& ((weekOfDayInYear - startWeek) % weekInterval) == 0) {
						count++;
					}
				}
			}
		}
		return count;
	}

	/**
	 * Kiem tra ngay nghi trong nam
	 *
	 * @author:
	 * @param day
	 * @param month
	 * @param year
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean checkDayOff(int day, int month, int year) {

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			for (int i = 0; i < listExcepTionDay.size(); i++) {
				calendar.setTime(format.parse(listExcepTionDay.get(i)));
				int dayOff = calendar.get(Calendar.DAY_OF_MONTH);
				int monthOff = calendar.get(Calendar.MONTH);
				int yearOff = calendar.get(Calendar.YEAR);
				if (dayOff == day && monthOff == month && yearOff == year)
					return true;
			}
		} catch (Exception ex) {

		}
		return false;
	}

	public class CustomerNotPsdsInMonthReportItem {
		public String customerId;
		public String staffId;
		public String maKh;
		public String tenKh;
		public String diaChi;
		public String nvbh;
		public String tuyen;
		public String slgt;
		public int lossDistribution;
		public int slgtThucTe;
		public String ngayDhCuoi;
		public double dsThangTruoc;
		public int numVisitPlan;

		public CustomerNotPsdsInMonthReportItem() {
			customerId = "";
			staffId = "";
			maKh = "";
			tenKh = "";
			diaChi = "";
			nvbh = "";
			tuyen = "";
			slgt = "";
			dsThangTruoc = 0.0;
			ngayDhCuoi = "";
			numVisitPlan = 0;

		}
	}
}
