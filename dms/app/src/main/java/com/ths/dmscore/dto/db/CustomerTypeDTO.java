/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  Thong tin loai khach hang
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class CustomerTypeDTO extends AbstractTableDTO {
	// id cua bang
	public int customerTypeId ;
	// ma loai khach hang
	public String customerTypeName ; 
	// ten loai khach hang
	public String customerTypeCode ;
	// trang thai
	public int status ; 
	// ghi chu
	public String note ; 
	// nguoi tao
	public String createUser ; 
	// ngay tao
	public String createDate ; 
	
	public CustomerTypeDTO(){
		super(TableType.CUSTOMER_TYPE_TABLE);
	}
	
	
	/**
	 * Khoi tao thong tin sau khi query database 
	 * @author : BangHN
	 * since : 1.0
	 */
	public void initCustomerTypeDTOFromCursor(Cursor c) {
		customerTypeId = CursorUtil.getInt(c, "CUSTOMER_TYPE_ID");
		customerTypeCode = CursorUtil.getString(c, "CUSTOMER_TYPE_CODE");
		customerTypeName = CursorUtil.getString(c, "CUSTOMER_TYPE_NAME");
	}
}
