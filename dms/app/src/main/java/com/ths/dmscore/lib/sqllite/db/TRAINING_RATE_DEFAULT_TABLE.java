/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * 
 * TRANING_RATE_DEFAULT_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:40:18 17-11-2014
 */
public class TRAINING_RATE_DEFAULT_TABLE extends ABSTRACT_TABLE {
	public static final String TRAINING_RATE_DEFAULT_ID = "TRAINING_RATE_DEFAULT_ID";
	public static final String SHORT_NAME = "SHORT_NAME";
	public static final String FULL_NAME = "FULL_NAME";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_NAME = "TRAINING_RATE_DEFAULT";

	public TRAINING_RATE_DEFAULT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { TRAINING_RATE_DEFAULT_ID, SHORT_NAME,
				FULL_NAME, ORDER_NUMBER, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}
