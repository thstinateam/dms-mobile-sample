/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Thong tin chuong trinh khach hang dang tham gia
 * 
 * @author : BangHN since : 1.0 version : 1.0
 */
@SuppressWarnings("serial")
public class CustomerProgrameDTO implements Serializable {
	public int customerId;// id khach hang
	public int staffId;// id nhan vien
	public int displayProgrameId;// id chuong trinh khuyen mai
	public String displayProgrameName;// name chuong trinh khuyen mai
	public String displayProgrameCode;// ma code chuong trinh
	public String levelCode;// muc tham gia
	public String status;// trang thai chuong tring
	public String fromDate; // ngay bat dau chuong trinh
	public String toDate;// ngay ket thuc chuong trinh
	public String cat;// nganh hang chuong trinh
	public long amount = 0;// doanh so dat duoc
	public long amountPlan = 0;// doanh so chi tieu
	public long quantity = 0;// san luong dat duoc
	public long quantityPlan = 0;// san luong chi tieu
	public long result = 0;// con lai
	public long amountRemain = 0; // doanh so con lai

	public CustomerProgrameDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Parse mot cursor sau khi query db -> dto
	 * 
	 * @author : BangHN since : 1.0
	 */
	public void initCustomerProgrameDTO(Cursor c) {
		displayProgrameId = (CursorUtil.getInt(c, "display_programe_id"));
		displayProgrameCode = (CursorUtil.getString(c, "display_programe_code"));
		displayProgrameName = (CursorUtil.getString(c, "display_programe_name"));
		cat = (CursorUtil.getString(c, "cat"));
		levelCode = (CursorUtil.getString(c, "display_programe_level"));
		amount = (CursorUtil.getInt(c, "amount"));
		amountPlan = (CursorUtil.getInt(c, "amount_plan"));
		result = (CursorUtil.getInt(c, "result"));
		amountRemain = (CursorUtil.getLong(c, "AMOUNT_REMAIN"));
		fromDate = (CursorUtil.getString(c, "from_date"));
		toDate = (CursorUtil.getString(c, "to_date"));
		quantity = (CursorUtil.getInt(c, "quantity"));
		quantityPlan = (CursorUtil.getInt(c, "quantity_plan"));
	}

}
