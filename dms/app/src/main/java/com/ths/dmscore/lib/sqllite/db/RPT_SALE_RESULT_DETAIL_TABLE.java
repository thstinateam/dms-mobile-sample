package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.GSNPPTrainingResultDayReportDTO;
import com.ths.dmscore.dto.view.GSNPPTrainingResultDayReportDTO.GSNPPTrainingResultReportDayItem;
import com.ths.dmscore.dto.view.TBHVTrainingPlanDayResultReportDTO;
import com.ths.dmscore.dto.view.TBHVTrainingPlanDayResultReportDTO.TBHVTrainingPlanDayResultReportItem;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;

public class RPT_SALE_RESULT_DETAIL_TABLE extends ABSTRACT_TABLE {

	public RPT_SALE_RESULT_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	/**
	 * get Gsnpp Training Result Day Report
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public GSNPPTrainingResultDayReportDTO getGsnppTrainingResultDayReport(long staffTrainId, int shopId, int staffId) {
		GSNPPTrainingResultDayReportDTO dto = new GSNPPTrainingResultDayReportDTO();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer var1 = new StringBuffer();
		String startOfMonth = DateUtils
				.getFirstDateOfNumberPreviousMonthWithFormat(
						DateUtils.DATE_FORMAT_DATE, 0);
		int supervisorId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT ");
		var1.append("	CUS_LIST.*, AL.*, ");
		var1.append("	group_concat(distinct CTTB.DISPLAY_PROGRAM_ID) AS DISPLAY_PROGRAM_ID, ");
		var1.append("	group_concat(distinct CTTB.DISPLAY_PROGRAM_CODE) AS DISPLAY_PROGRAM_CODE, ");
		var1.append("	group_concat(distinct mi.DISPLAY_PROGRAM_ID) AS DISPLAY_PROGRAM_ID_CAPTURED, ");
		var1.append("	group_concat(mi.MEDIA_ITEM_ID) AS MEDIA_ITEM_ID ");
		var1.append("FROM   (SELECT rssd.*, ");
		var1.append("               CUSTOMER.CUSTOMER_CODE, ");
		var1.append("               CUSTOMER.CUSTOMER_ID, ");
		var1.append("               CUSTOMER.CUSTOMER_NAME, ");
		var1.append("               ifnull(CUSTOMER.HOUSENUMBER,'') HOUSENUMBER, ");
		var1.append("        		( CASE WHEN CUSTOMER.ADDRESS IS NOT NULL THEN CUSTOMER.ADDRESS ");
		var1.append("               	ELSE ( ifnull(CUSTOMER.housenumber,'') ||' '|| ifnull(CUSTOMER.street,'')) ");      
		var1.append("          			END ) AS ADDRESS, "); 
		var1.append("               CUSTOMER.LAT, ");
		var1.append("               CUSTOMER.LNG, ");
		var1.append("               ifnull(CUSTOMER.STREET,'') STREET, ");
		var1.append("               CUSTOMER.CHANNEL_TYPE_ID, ");
		var1.append("               rssd.SCORE AS SCORE, ");
		var1.append("               TRAINING_PLAN_DETAIL.SCORE  AS TPD_SCORE");
		var1.append("        FROM   RPT_STAFF_SALE_DETAIL rssd, ");
		var1.append("               CUSTOMER, ");
		var1.append("               TRAINING_PLAN_DETAIL ");
		var1.append("        WHERE  rssd.TRAINING_PLAN_DETAIL_ID = ? ");
		param.add( String.valueOf(staffTrainId));
		var1.append("               AND rssd.SHOP_ID = ? ");
		param.add("" + shopId);
		var1.append("               AND rssd.TRAINING_PLAN_DETAIL_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID ");
		var1.append("               AND TRAINING_PLAN_DETAIL.STATUS IN ( 0,1 ) ");
		var1.append("               AND rssd.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID ");
		var1.append("               AND CUSTOMER.STATUS = 1) CUS_LIST ");
		var1.append("       LEFT JOIN (SELECT ACTION_LOG.ACTION_LOG_ID   AS AL_ID, ");
		var1.append("                         ACTION_LOG.STAFF_ID        AS AL_STAFF_ID, ");
		var1.append("                         ACTION_LOG.CUSTOMER_ID     AS AL_CUSTOMER_ID_1, ");
		var1.append("                         ACTION_LOG.LAT             AS AL_LAT, ");
		var1.append("                         ACTION_LOG.LNG             AS AL_LNG, ");
		var1.append("                         MIN(ACTION_LOG.START_TIME) AS AL_START_TIME, ");
		var1.append("                         ACTION_LOG.END_TIME        AS AL_END_TIME ");
		var1.append("                  FROM   ACTION_LOG, ");
		var1.append("                         STAFF, ");
		var1.append("                         CHANNEL_TYPE ");
		var1.append("                  WHERE  ACTION_LOG.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("                         and ACTION_LOG.object_type in (0,1) ");
		var1.append("                         AND STAFF.STAFF_TYPE_ID = CHANNEL_TYPE.CHANNEL_TYPE_ID ");
		var1.append("                         AND CHANNEL_TYPE.OBJECT_TYPE = 5 ");
		var1.append("                         AND CHANNEL_TYPE.TYPE = 2 ");
		var1.append("                         AND STAFF.STATUS = 1 ");
		var1.append("                         AND ACTION_LOG.SHOP_ID = ? ");
		param.add("" + shopId);
		var1.append("                         AND DATE(?) = DATE(ACTION_LOG.START_TIME) ");
		param.add(date_now);
		var1.append("                  GROUP  BY CUSTOMER_ID) AL ");
		var1.append("              ON CUS_LIST.CUSTOMER_ID = AL.AL_CUSTOMER_ID_1 ");
		var1.append("		LEFT JOIN ");
		var1.append("			(");

		// cttb
		var1.append("               SELECT DCM.CUSTOMER_ID          AS CUSTOMER_ID1, ");
		var1.append("               DP.DISPLAY_PROGRAM_ID  AS DISPLAY_PROGRAM_ID, ");
		var1.append("               DP.display_program_code AS DISPLAY_PROGRAM_CODE ");
		var1.append("               FROM display_program AS DP, ");
		var1.append("               display_staff_map AS DSM, ");
		var1.append("               display_program_level AS DPL, ");
		var1.append("               display_customer_map AS DCM ");
		var1.append("               WHERE DP.display_program_id = DSM.display_program_id ");
		var1.append("               AND DP.display_program_id = DPL.display_program_id ");
		var1.append("               AND DSM.display_staff_map_id = DCM.display_staff_map_id ");
		var1.append("               AND DPL.display_program_level_id = DCM.display_program_level_id ");
		var1.append("               AND DATE(DP.from_date) <= Date(?) ");
		param.add(date_now);
		var1.append("               AND IFNULL(DATE(DP.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		var1.append("               AND DP.STATUS = 1 ");
		var1.append("               AND DCM.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND DSM.SHOP_ID = ? ");
		param.add(String.valueOf(shopId));
		var1.append("               AND DSM.STAFF_ID = ? AND DATE(DSM.MONTH, 'start of month') = ? ");
		param.add(String.valueOf(staffId));
		param.add(startOfMonth);
		var1.append("               AND DSM.STATUS = 1 ");
		var1.append("               AND DPL.STATUS = 1 ");
		var1.append("               AND DATE(DCM.from_date) <= Date(?) ");
		param.add(date_now);
		var1.append("               AND IFNULL(DATE(DCM.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		var1.append("               AND DCM.STATUS = 1 ");
//		var1.append("                                 AND DP.DISPLAY_PROGRAM_ID NOT IN (SELECT ");
//		var1.append("                                     OBJECT_ID AS DISPLAY_PROGRAME_ID ");
//		var1.append("                                                                    FROM ");
//		var1.append("                                     ACTION_LOG ");
//		var1.append("                                                                    WHERE ");
//		var1.append("                                     OBJECT_TYPE = 2 ");
//		var1.append("                                     AND STAFF_ID = ? ");
//		param.add(String.valueOf(staffId));
//		var1.append("                                     AND DATE(START_TIME) <= DATE(?) ");
//		param.add(date_now);
//		var1.append("                                     AND CUSTOMER_ID = DCM.CUSTOMER_ID ");
//		var1.append("                                     AND (DATE(END_TIME) >= DATE(?) ");
//		param.add(date_now);
//		var1.append("                                          OR END_TIME IS NULL ");
//		var1.append("                                         ))) ");

		var1.append("		)	 CTTB ");
		// end cttb
		var1.append("		ON CTTB.CUSTOMER_ID1 = CUS_LIST.CUSTOMER_ID ");
		var1.append("		left join ( ");
		var1.append("			 select MEDIA_ITEM_ID, DISPLAY_PROGRAM_ID, OBJECT_ID  ");
		var1.append("			 from media_item ");
		var1.append("			 where DATE(CREATE_DATE) >= DATE(?) and STAFF_ID = ? ");
		param.add(date_now);
		param.add(String.valueOf(supervisorId));
		var1.append("			 ) mi on mi.DISPLAY_PROGRAM_ID =  CTTB.DISPLAY_PROGRAM_ID and mi.OBJECT_ID = CUS_LIST.CUSTOMER_ID ");
		var1.append("	group by CUS_LIST.CUSTOMER_ID ");
		var1.append("	ORDER  BY AMOUNT_PLAN DESC, ");
		var1.append("          AMOUNT DESC ");

		Cursor c = null;
		try {
			c = this.rawQueries(var1.toString(), param);
			if (c != null && c.moveToFirst()) {
				int stt = 0;
				do {
					stt++;
					GSNPPTrainingResultReportDayItem i = dto.newStaffTrainResultReportItem();
					i.initFromCursor(c, dto, stt);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.e("getGsnppTrainingResultDayReport", "fail", ex);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		
		StringBuffer scoreSql = new StringBuffer();
		scoreSql.append("  SELECT * FROM TRAINING_PLAN_DETAIL WHERE TRAINING_PLAN_DETAIL_ID = ? ");
		Cursor cScore = null;
		try {
			cScore = rawQuery(scoreSql.toString(), new String[] { String.valueOf(staffTrainId) });
			if (cScore != null) {
				if (cScore.moveToFirst()) {
					if (dto != null) {
						dto.score = CursorUtil.getDouble(cScore, "SCORE");
					}
				}
			}
		} catch (Exception e) {
		} finally {
			if (cScore != null) {
				cScore.close();
			}
		}
		
		Cursor cDistance = null;
		String getShopDistance = " select DISTANCE_TRAINING from SHOP where SHOP_ID = ?";
		try {
			cDistance = rawQuery(getShopDistance, new String[] { String.valueOf(shopId) });
			if (cDistance != null) {
				if (cDistance.moveToFirst()) {
					if (dto != null) {
						dto.distance = cDistance.getDouble(0);
					}
				}
			}
		} catch (Exception e) {
		} finally {
			if (cDistance != null) {
				cDistance.close();
			}
		}

		return dto;
	}

	/**
	 * get Day Training Supervision
	 * 
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public TBHVTrainingPlanDayResultReportDTO getDayTrainingSupervision(int staffTrainId, int shopId) {
		TBHVTrainingPlanDayResultReportDTO dto = new TBHVTrainingPlanDayResultReportDTO();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT RPT_STAFF_SALE_DETAIL.*, ");
		var1.append("       CUSTOMER.CUSTOMER_CODE, ");
		var1.append("       SUBSTR(CUSTOMER.CUSTOMER_CODE, 1, 3) AS CUSCODE, ");
		var1.append("       CUSTOMER.CUSTOMER_ID, ");
		var1.append("       CUSTOMER.CUSTOMER_NAME, ");
		var1.append("       CUSTOMER.HOUSENUMBER, ");
		var1.append("       CUSTOMER.LAT, ");
		var1.append("       CUSTOMER.LNG, ");
		var1.append("       CUSTOMER.STREET, ");
		var1.append("       ( CASE WHEN CUSTOMER.ADDRESS IS NOT NULL THEN CUSTOMER.ADDRESS ");
		var1.append("              ELSE ( ifnull(CUSTOMER.housenumber,'') ||' '|| ifnull(CUSTOMER.street,'')) ");      
		var1.append("        END ) AS ADDRESS "); 
		var1.append("FROM   RPT_STAFF_SALE_DETAIL, ");
		var1.append("       CUSTOMER, ");
		var1.append("       TRAINING_PLAN, ");
		var1.append("       TRAINING_PLAN_DETAIL ");
		var1.append("WHERE  TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID = ? ");
		var1.append("       AND TRAINING_PLAN_DETAIL.TRAINING_PLAN_ID = ");
		var1.append("           TRAINING_PLAN.TRAINING_PLAN_ID ");
		var1.append("       AND TRAINING_PLAN.STATUS = 1 ");
		var1.append("       AND TRAINING_PLAN_DETAIL.STATUS IN ( 0,1 ) ");
		var1.append("       AND RPT_STAFF_SALE_DETAIL.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID ");
		var1.append("       AND RPT_STAFF_SALE_DETAIL.TRAINING_PLAN_DETAIL_ID = ");
		var1.append("           TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID ");
		var1.append("       AND CUSTOMER.STATUS = 1 ");
		var1.append("ORDER  BY CUSCODE ASC ");

		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(), new String[] { String.valueOf(staffTrainId) });
			if (c != null && c.moveToFirst()) {
				int stt = 0;
				do {
					stt++;
					TBHVTrainingPlanDayResultReportItem i = dto.newStaffTrainResultReportItem();
					i.initFromCursor(c, dto, stt);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
		} finally {
			if (c != null) {
				c.close();
			}
		}
		
		StringBuffer scoreSql = new StringBuffer();
		scoreSql.append("  SELECT * FROM TRAINING_PLAN_DETAIL WHERE TRAINING_PLAN_DETAIL_ID = ? ");
		Cursor cScore = null;
		try {
			cScore = rawQuery(scoreSql.toString(), new String[] { String.valueOf(staffTrainId) });
			if (cScore != null) {
				if (cScore.moveToFirst()) {
					if (dto != null) {
						dto.score = CursorUtil.getDouble(cScore, "SCORE");
					}
				}
			}
		} catch (Exception e) {
		} finally {
			if (cScore != null) {
				cScore.close();
			}
		}

		return dto;

	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param bundle
	 * @return
	 * @return: CustomerListItem
	 * @throws:
	*/
	public CustomerListItem getGsnppTrainingResultDayReport(Bundle bundle) {
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String startOfMonth = DateUtils
				.getFirstDateOfNumberPreviousMonthWithFormat(
						DateUtils.DATE_FORMAT_DATE, 0);
		int supervisorId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		
		String staffTrainId = bundle.getString(IntentConstants.INTENT_STAFF_TRAIN_DETAIL_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = bundle.getString(IntentConstants.INTENT_CUSTOMER_ID);

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		
		var1.append("SELECT ");
		var1.append("	CUS_LIST.*, AL.*, ");
		var1.append("	group_concat(CTTB.DISPLAY_PROGRAM_ID) AS DISPLAY_PROGRAM_ID, ");
		var1.append("	group_concat(CTTB.DISPLAY_PROGRAM_CODE) AS DISPLAY_PROGRAM_CODE, ");
		var1.append("	group_concat(mi.DISPLAY_PROGRAM_ID) AS DISPLAY_PROGRAM_ID_CAPTURED, ");
		var1.append("	group_concat(mi.MEDIA_ITEM_ID) AS MEDIA_ITEM_ID ");
		var1.append("FROM   (SELECT rssd.*, ");
		var1.append("               CUSTOMER.CUSTOMER_CODE, ");
		var1.append("               CUSTOMER.CUSTOMER_ID, ");
		var1.append("               CUSTOMER.CUSTOMER_NAME, ");
		var1.append("               ifnull(CUSTOMER.HOUSENUMBER,'') HOUSENUMBER, ");
		var1.append("        		( CASE WHEN CUSTOMER.ADDRESS IS NOT NULL THEN CUSTOMER.ADDRESS ");
		var1.append("               	ELSE ( ifnull(CUSTOMER.housenumber,'') ||' '|| ifnull(CUSTOMER.street,'')) ");      
		var1.append("          			END ) AS ADDRESS, "); 
		var1.append("               CUSTOMER.LAT, ");
		var1.append("               CUSTOMER.LNG, ");
		var1.append("               ifnull(CUSTOMER.STREET,'') STREET, ");
		var1.append("               CUSTOMER.CHANNEL_TYPE_ID, ");
		var1.append("               rssd.SCORE AS SCORE, ");
		var1.append("               TRAINING_PLAN_DETAIL.SCORE  AS TPD_SCORE");
		var1.append("        FROM   RPT_STAFF_SALE_DETAIL rssd, ");
		var1.append("               CUSTOMER, ");
		var1.append("               TRAINING_PLAN_DETAIL ");
		var1.append("        WHERE  rssd.TRAINING_PLAN_DETAIL_ID = ? ");
		param.add( String.valueOf(staffTrainId));
		var1.append("               AND rssd.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               AND CUSTOMER.CUSTOMER_ID = ? ");
		param.add(customerId);
		var1.append("               AND rssd.TRAINING_PLAN_DETAIL_ID = TRAINING_PLAN_DETAIL.TRAINING_PLAN_DETAIL_ID ");
		var1.append("               AND TRAINING_PLAN_DETAIL.STATUS IN ( 0,1 ) ");
		var1.append("               AND rssd.CUSTOMER_ID = CUSTOMER.CUSTOMER_ID ");
		var1.append("               AND CUSTOMER.STATUS = 1) CUS_LIST ");
		var1.append("       LEFT JOIN (SELECT ACTION_LOG.ACTION_LOG_ID   AS AL_ID, ");
		var1.append("                         ACTION_LOG.STAFF_ID        AS AL_STAFF_ID, ");
		var1.append("                         ACTION_LOG.CUSTOMER_ID     AS AL_CUSTOMER_ID_1, ");
		var1.append("                         ACTION_LOG.LAT             AS AL_LAT, ");
		var1.append("                         ACTION_LOG.LNG             AS AL_LNG, ");
		var1.append("                         MIN(ACTION_LOG.START_TIME) AS AL_START_TIME, ");
		var1.append("                         ACTION_LOG.END_TIME        AS AL_END_TIME ");
		var1.append("                  FROM   ACTION_LOG, ");
		var1.append("                         STAFF, ");
		var1.append("                         CHANNEL_TYPE ");
		var1.append("                  WHERE  ACTION_LOG.STAFF_ID = STAFF.STAFF_ID ");
		var1.append("                         AND STAFF.STAFF_TYPE_ID = CHANNEL_TYPE.CHANNEL_TYPE_ID ");
		var1.append("                         AND CHANNEL_TYPE.OBJECT_TYPE = 5 ");
		var1.append("                         AND CHANNEL_TYPE.TYPE = 2 ");
		var1.append("                         AND STAFF.STATUS = 1 ");
		var1.append("                         AND ACTION_LOG.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("                         AND DATE(?) = DATE(ACTION_LOG.START_TIME) ");
		param.add(date_now);
		var1.append("                  GROUP  BY CUSTOMER_ID) AL ");
		var1.append("              ON CUS_LIST.CUSTOMER_ID = AL.AL_CUSTOMER_ID_1 ");
		var1.append("		LEFT JOIN ");
		var1.append("			(");

		// cttb
		var1.append("               SELECT DCM.CUSTOMER_ID          AS CUSTOMER_ID1, ");
		var1.append("               DP.DISPLAY_PROGRAM_ID  AS DISPLAY_PROGRAM_ID, ");
		var1.append("               DP.display_program_code AS DISPLAY_PROGRAM_CODE ");
		var1.append("               FROM display_program AS DP, ");
		var1.append("               display_staff_map AS DSM, ");
		var1.append("               display_program_level AS DPL, ");
		var1.append("               display_customer_map AS DCM ");
		var1.append("               WHERE DP.display_program_id = DSM.display_program_id ");
		var1.append("               AND DP.display_program_id = DPL.display_program_id ");
		var1.append("               AND DSM.display_staff_map_id = DCM.display_staff_map_id ");
		var1.append("               AND DPL.display_program_level_id = DCM.display_program_level_id ");
		var1.append("               AND DATE(DP.from_date) <= Date(?) ");
		param.add(date_now);
		var1.append("               AND IFNULL(DATE(DP.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		var1.append("               AND DP.STATUS = 1 ");
		var1.append("               AND DCM.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               AND DSM.SHOP_ID = ? ");
		param.add(shopId);
		var1.append("               AND DSM.STAFF_ID = ? AND DATE(DSM.MONTH, 'start of month') = ? ");
		param.add(staffId);
		param.add(startOfMonth);
		var1.append("               AND DSM.STATUS = 1 ");
		var1.append("               AND DPL.STATUS = 1 ");
		var1.append("               AND DATE(DCM.from_date) <= Date(?) ");
		param.add(date_now);
		var1.append("               AND IFNULL(DATE(DCM.to_date) >=  Date(?), 1) ");
		param.add(date_now);
		var1.append("               AND DCM.STATUS = 1 ");
		var1.append("                                 AND DP.DISPLAY_PROGRAM_ID NOT IN (SELECT ");
		var1.append("                                     OBJECT_ID AS DISPLAY_PROGRAME_ID ");
		var1.append("                                                                    FROM ");
		var1.append("                                     ACTION_LOG ");
		var1.append("                                                                    WHERE ");
		var1.append("                                     OBJECT_TYPE = 2 ");
		var1.append("                                     AND STAFF_ID = ? ");
		param.add(staffId);
		var1.append("                                     AND SHOP_ID = ? ");
		param.add(shopId);
		var1.append("                                     AND DATE(START_TIME) <= DATE(?) ");
		param.add(date_now);
		var1.append("                                     AND CUSTOMER_ID = DCM.CUSTOMER_ID ");
		var1.append("                                     AND (DATE(END_TIME) >= DATE(?) ");
		param.add(date_now);
		var1.append("                                          OR END_TIME IS NULL ");
		var1.append("                                         ))) ");

		var1.append("			 CTTB ");
		// end cttb
		var1.append("		ON CTTB.CUSTOMER_ID1 = CUS_LIST.CUSTOMER_ID ");
		var1.append("		left join ( ");
		var1.append("			 select MEDIA_ITEM_ID, DISPLAY_PROGRAM_ID, OBJECT_ID  ");
		var1.append("			 from media_item ");
		var1.append("			 where DATE(CREATE_DATE) >= DATE(?) and STAFF_ID = ? ");
		param.add(date_now);
		param.add(String.valueOf(supervisorId));
		var1.append("			 ) mi on mi.DISPLAY_PROGRAM_ID =  CTTB.DISPLAY_PROGRAM_ID and mi.OBJECT_ID = CUS_LIST.CUSTOMER_ID ");
		var1.append("	group by CUS_LIST.CUSTOMER_ID ");
		var1.append("	ORDER  BY AMOUNT_PLAN DESC, ");
		var1.append("          AMOUNT DESC ");

		GSNPPTrainingResultReportDayItem item = null;
		Cursor c = null;
		try {
			c = this.rawQueries(var1.toString(), param);
			if (c != null && c.moveToFirst()) {
				GSNPPTrainingResultDayReportDTO dto = new GSNPPTrainingResultDayReportDTO();
				item = dto.newStaffTrainResultReportItem();
				item.initFromCursor(c, dto, 0);
			}
		} catch (Exception ex) {
			MyLog.e("getGsnppTrainingResultDayReport", "fail", ex);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		
		CustomerListItem cusItem = null;
		if (item != null) {
			cusItem = new CustomerListItem();
			cusItem.aCustomer.customerId = item.custId;
			cusItem.aCustomer.customerName = item.custName;
			cusItem.aCustomer.customerCode = item.custCode;
			cusItem.aCustomer.lat = item.lat;
			cusItem.aCustomer.lng = item.lng;
			cusItem.isOr = item.isOr;
			cusItem.visitStartTime = item.startTime;
			cusItem.visitEndTime = item.endTime;
			cusItem.visitActLogId = item.visitActLogId;
			cusItem.displayProgramId = item.displayProgramId;
			cusItem.displayProgramIdCaptured = item.displayProgramIdCaptured;
			cusItem.isHaveDisplayProgramNotYetVote = item.isHaveDisplayProgramNotYetVote;
		}
		return cusItem;
	}
}
