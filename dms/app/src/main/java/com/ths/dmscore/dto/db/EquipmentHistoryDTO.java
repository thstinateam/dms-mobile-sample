/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.EQUIPMENT_TABLE;
import com.ths.dmscore.lib.sqllite.db.EQUIP_HISTORY_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * DTO luu lich su thiet bi
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 19:14:20 26-12-2014
 */
public class EquipmentHistoryDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;

	// id lich su thiet bi
	public long equipHistoryId;
	// id thiet bi
	public long equipId;
	// trang thai lich su
	public int status;
	// tinh trang thiet bi
	public int usageStatus;
	// t
	public String healthStatus;
	// trang giao dich
	public int tradeStatus;
	//loai giao dich
	public int tradeType;
	// loai  object
	public int objecType;
	// id kho ( khach hang, NPP, VNM)
	public long objectId;
	// ma kho
	public String objectCode;
	// nhan vien
	public long staffId;
	// id bien ban
	public long recordId;// danh cho khi cap nhat bang equipment bao mat tu mobile
	public String createUser;
	public String createDate;
	public String updateUser;
	public String updateDate;

	public long formId;
	public int formType;
	public String stockName;
	public String tableName;

	/**
	 * insert lich su thiet bi 
	 * @author: hoanpd1
	 * @since: 12:23:12 30-12-2014
	 * @return: JSONObject
	 * @throws:  
	 * @param item
	 * @return
	 */
	public JSONObject generateInsertEquipHistory(EquipmentHistoryDTO item) {
		JSONObject jsonInsert = new JSONObject();
		try {
			// Insert
			jsonInsert.put(IntentConstants.INTENT_TYPE, TableAction.INSERT); 
			jsonInsert.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_HISTORY_TABLE.TABLE_NAME);
			// ds params
			JSONArray jsonDetail = new JSONArray();
			// Khi insert thi insert cac truong sau
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.EQUIP_HISTORY_ID, item.equipHistoryId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.EQUIP_ID, item.equipId, null));  
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.STATUS, item.status, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.USAGE_STATUS, item.usageStatus, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.HEALTH_STATUS, item.healthStatus, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.TRADE_STATUS, item.tradeStatus, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.TRADE_TYPE, item.tradeType, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.OBJECT_TYPE, item.objecType, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.OBJECT_ID, item.objectId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.STAFF_ID, item.staffId, null));
			if (!StringUtil.isNullOrEmpty(item.objectCode)) {
				jsonDetail
						.put(GlobalUtil.getJsonColumn(
								EQUIP_HISTORY_TABLE.OBJECT_CODE,
								item.objectCode, null));
			}
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.CREATE_USER, item.createUser, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.CREATE_DATE, item.createDate, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.FORM_ID, item.formId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_HISTORY_TABLE.FORM_TYPE, item.formType, null));
			if (!StringUtil.isNullOrEmpty(item.stockName)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(
						EQUIP_HISTORY_TABLE.STOCK_NAME, item.stockName, null));
			}
			if (!StringUtil.isNullOrEmpty(item.tableName)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(
						EQUIP_HISTORY_TABLE.TABLE_NAME_FORM, item.tableName,
						null));
			}
			jsonInsert.put(IntentConstants.INTENT_LIST_PARAM, jsonDetail);

		} catch (JSONException e) {
			MyLog.e("UnexceptionLog",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return jsonInsert;
	}
	
	
	/**
	 * update trang thai thiet bi
	 * @author: hoanpd1
	 * @since: 12:24:52 30-12-2014
	 * @return: JSONObject
	 * @throws:  
	 * @param item
	 * @return
	 */
	public JSONObject generateUpdateEquip(EquipmentHistoryDTO item) {
		JSONObject jsonUpdate = new JSONObject(); 
		try {
			// Update
			jsonUpdate.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE); 
			jsonUpdate.put(IntentConstants.INTENT_TABLE_NAME, EQUIPMENT_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			// ...them thuoc tinh		
			detailPara.put(GlobalUtil.getJsonColumn(EQUIPMENT_TABLE.TRADE_STATUS, item.tradeStatus, null));// 0 la trang thang dang giao dich
			detailPara.put(GlobalUtil.getJsonColumn(EQUIPMENT_TABLE.TRADE_TYPE, item.tradeType, null));// 4 bao mat tu mobile
			detailPara.put(GlobalUtil.getJsonColumn(EQUIPMENT_TABLE.UPDATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(EQUIPMENT_TABLE.UPDATE_USER,item.updateUser , null));
			jsonUpdate.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(EQUIPMENT_TABLE.EQUIP_ID, item.equipId, null));
			jsonUpdate.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			MyLog.e("UnexceptionLog",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}

		return jsonUpdate;
	}
}
