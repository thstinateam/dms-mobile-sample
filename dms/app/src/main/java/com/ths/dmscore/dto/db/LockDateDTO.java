package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.STOCK_LOCK_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class LockDateDTO extends AbstractTableDTO {
	// id
	public long stockLockID;
	// vanLock, mac dinh chua co dong nao xem nhu khoa
	public int vanLock = 1;
	// id nhan vien
	public long staffId;
	// id NPP
	public long shopId;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;

	// dung cho chot ngay
	// kiem tra co don DP trong ngay moi cho chot
	public boolean hasDP = false;

	public LockDateDTO() {
		super(TableType.STOCK_LOCK_TABLE);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initFromCursor(Cursor c) {
		stockLockID = CursorUtil.getLong(c, STOCK_LOCK_TABLE.STOCK_LOCK_ID);
		vanLock = CursorUtil.getInt(c, STOCK_LOCK_TABLE.VAN_LOCK, -1);
		staffId = CursorUtil.getLong(c, STOCK_LOCK_TABLE.STAFF_ID);
		shopId = CursorUtil.getLong(c, STOCK_LOCK_TABLE.SHOP_ID);
		updateDate = CursorUtil.getString(c, STOCK_LOCK_TABLE.UPDATE_DATE);
	}

	/**
	 * gen cau Json update trang thai kho
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateUpdateStockState() {
		JSONObject dateLockJson = new JSONObject();
		try {
			dateLockJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			dateLockJson.put(IntentConstants.INTENT_TABLE_NAME, STOCK_LOCK_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.VAN_LOCK, this.vanLock, null));
			if (updateDate != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.UPDATE_DATE, updateDate, null));
			}
			if (updateUser != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.UPDATE_USER, updateUser, null));
			}
			dateLockJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.STAFF_ID, this.staffId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.SHOP_ID, this.shopId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.CREATE_DATE, this.createDate, null));
			dateLockJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
		}
		return dateLockJson;
	}

	/**
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws Exception
	 * @throws:
	*/
	public JSONObject generateInsertLockDateVansale() throws Exception {
		JSONObject dateLockJson = new JSONObject();
		try {
			dateLockJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERTORUPDATE);
			dateLockJson.put(IntentConstants.INTENT_TABLE_NAME, STOCK_LOCK_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.STOCK_LOCK_ID, stockLockID, null));
			detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.VAN_LOCK, this.vanLock, null));
			detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.STAFF_ID, this.staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.SHOP_ID, this.shopId, null));

			if (createDate != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.CREATE_DATE, createDate, null));
			}
			if (createUser != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.CREATE_USER, createUser, null));
			}
			if (updateDate != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.UPDATE_DATE, updateDate, null));
			}
			if (updateUser != null) {
				detailPara.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.UPDATE_USER, updateUser, null));
			}
			dateLockJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_LOCK_TABLE.STOCK_LOCK_ID, this.stockLockID, null));
			dateLockJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (Exception e) {
			throw e;
		}
		return dateLockJson;
	}
}
