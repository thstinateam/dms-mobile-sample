/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * MapExtraInfoView.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  11:33:02 AM Feb 3, 2015
 */
public class MapExtraInfoView extends AbstractExtraInfoView implements View.OnClickListener {

	private TextView tv;
	private LinearLayout llMap;
	private ImageView imgMap;
	private CustomerAttributeViewDTO attrDTO;
	private final BaseFragment listener;
	private final int actionShowMap;
	private String location;
	
	public MapExtraInfoView(Context context, BaseFragment listen, int ACTION_SHOW_MAP) {
		super(context, R.layout.layout_map_extra_info_view);
		// TODO Auto-generated constructor stub
		tv = (TextView) arrView.get(0);
		llMap = (LinearLayout) arrView.get(1);
		imgMap = (ImageView) llMap.findViewById(R.id.imgMap);
		imgMap.setOnClickListener(this);
		listener = listen;
		actionShowMap = ACTION_SHOW_MAP;
	}

	@Override
	public Object getDataFromView() {
		// TODO Auto-generated method stub
		CustomerAttributeDetailViewDTO detail = new CustomerAttributeDetailViewDTO();
		detail.attrID = attrDTO.customerAttributeID;
		detail.value = location;
		detail.detailID = attrDTO.attributeDetailID;
		return detail;
	}

	@Override
	public void renderLayout(CustomerAttributeViewDTO dto, boolean isEdit, String fromView) {
		// TODO Auto-generated method stub
		attrDTO = dto;
		if (dto.mandatory == 1) {
			Class<?> clazz;
			Object fromViewObject = null;
			try {
				clazz = Class.forName(fromView);
				fromViewObject = clazz.newInstance();
			} catch (ClassNotFoundException e) {
				MyLog.e("MapExtraInfoView", "ClassNotFoundException", e);
			} catch (java.lang.InstantiationException e) {
				MyLog.e("MapExtraInfoView", "InstantiationException", e);
			} catch (IllegalAccessException e) {
				MyLog.e("MapExtraInfoView", "IllegalAccessException", e);
			}
			if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
				setSymbol("*", dto.name, ImageUtil.getColor(R.color.RED), tv);
			} else {
				tv.setText(dto.name);
			}
		} else if (dto.mandatory == 0) {
			tv.setText(dto.name);
		}
		tag = dto.customerAttributeID;
		this.location = dto.enumValue;
		imgMap.setTag(dto.enumValue);
	}

	@Override
	public boolean isCheckData() {
		// TODO Auto-generated method stub
		return !StringUtil.isNullOrEmpty(location);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return attrDTO.name;
	}
	
	/**
	 * set location
	 * 
	 * @author: dungdq3
	 * @since: 2:16:26 PM Feb 3, 2015
	 * @return: void
	 * @throws:  
	 * @param location
	 */
	public void setLocation(String location){
		this.location = location;
		imgMap.setTag(location);
	}
	

	@Override
	public void onClick(View v) {
		if(v == imgMap) {
			listener.onEvent(actionShowMap, imgMap, attrDTO.customerAttributeID);
		}
	}

}
