/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.db.RPT_STAFF_SALE_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.view.SupervisorReportStaffSaleViewDTO;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Bao cao doanh so theo nganh role NVBH
 *
 * @author: hoanpd1
 * @version: 1.0
 * @since: 16:47:33 07-04-2015
 */
public class ReportAmountCategoryView extends BaseFragment implements
		VinamilkTableListener {

	// thong ke chung
	private static final int ACTION_MENU_GENERAL_STATISTICS = 1;
	// mat hang trong tam
	private static final int ACTION_MENU_MHTT = 2;
	// can thuc hien
	private static final int ACTION_MENU_NEED_DONE = 3;
	// CTTB
	private final int ACTION_MENU_REPORT_DISPLAY_PROGRESS = 4;
	// Bao cao doanh so nganh hang
	private final int ACTITON_MENU_REPORT_AMOUNT_CATEGORY = 5;
	// Bao cao san luong nganh hang
	private final int ACTITON_MENU_REPORT_QUANTITY_CATEGORY = 6;

	// so ngay ban hang ke hoach
	TextView tvPlanDate;
	// so ngay ban hang da qua
	TextView tvSoldDate;
	// Tien do
	TextView tvProgress;

	// bang bao cao tien do ban hang trong tam
	private DMSTableView tbReportCategory;
	// flag check load data the first
	public boolean isFirst = true;
	// report infor of screen
	private SupervisorReportStaffSaleViewDTO reportDataScreen = new SupervisorReportStaffSaleViewDTO();
	// private LinearLayout llCatHeaderList;
	String shopId = "";
	String staffId = "";
	String staffName = "";

	// from view
	int FROM_GSNPP = 0;
	int FROM_TBHV = 1;
	int isFrom = 0;
	// tong hop theo nhan vien hay route
	int sysSaleRoute;

	public static ReportAmountCategoryView getInstance(Bundle data) {
		ReportAmountCategoryView instance = new ReportAmountCategoryView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_report_category_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_BAOCAODOANHSONGANH);

		tvPlanDate = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvPlanDate, PriHashMap.PriControl.GSNPP_DOANHSONGANH_NGAYBANHANGKEHOACH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvPlanDateTitle, PriHashMap.PriControl.GSNPP_DOANHSONGANH_NGAYBANHANGKEHOACH);
		tvSoldDate = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldDate, PriHashMap.PriControl.GSNPP_DOANHSONGANH_NGAYBANHANGDAQUA);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldDateTitle, PriHashMap.PriControl.GSNPP_DOANHSONGANH_NGAYBANHANGDAQUA);
		tvProgress = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvProgress, PriHashMap.PriControl.GSNPP_DOANHSONGANH_TIENDO);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvProgressTitle, PriHashMap.PriControl.GSNPP_DOANHSONGANH_TIENDO);

		tbReportCategory = (DMSTableView) v.findViewById(R.id.tbReportCategory);
		tbReportCategory.setListener(this);

		Bundle arg = (Bundle) getArguments();
		// TBHV
		staffId = arg.getString(IntentConstants.INTENT_OBJECT_ID);
		// GSNPP
		if (StringUtil.isNullOrEmpty(staffId)) {
			isFrom = FROM_GSNPP;
			staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		} else {
			isFrom = FROM_TBHV;
		}
		shopId = arg.getString(IntentConstants.INTENT_SHOP_ID);
		if (StringUtil.isNullOrEmpty(shopId)) {
			shopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		}
		sysSaleRoute = GlobalInfo.getInstance().getSysSaleRoute();
		this.setTitleForScreen();

		if (isFirst) {
			requestReportCategory();
		} else {
			renderLayout();
		}

		return v;
	}

	/**
	 * request get report progress sales focus info
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void requestReportCategory() {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_STAFF_ID, staffId);
		data.putInt(IntentConstants.INTENT_SYS_SALE_ROUTE, sysSaleRoute);
		handleViewEvent(data, ActionEventConstant.GET_REPORT_CATEGORY, SaleController.getInstance());
	}

	/**
	 * set title screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void setTitleForScreen() {
		SpannableObject obj = new SpannableObject();
		// Title for GSNPP
		String title = StringUtil.getString(R.string.TITLE_VIEW_GSNPP_REPORT_CAT_AMOUNT);
		obj.addSpan(title, ImageUtil.getColor(R.color.BLACK), android.graphics.Typeface.NORMAL);
		obj.addSpan(DateUtils.defaultDateFormat.format(new Date()), ImageUtil.getColor(R.color.BLACK), android.graphics.Typeface.BOLD);
		hideHeaderview();
		parent.setTitleName(obj);
	}

	/**
	 * render data for screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		// render header infor
		tvPlanDate.setText(String.valueOf(this.reportDataScreen.numPlanDate));
		tvSoldDate.setText(String.valueOf(this.reportDataScreen.numPlanDateDone));
		StringUtil.displayPercent(tvProgress, reportDataScreen.progressSales);
		// create header for table
		tbReportCategory.clearAllDataAndHeader();
		LayoutInflater vi = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (isFirst) {
			isFirst = false;
		}
		// bo isFirst do khi refresh thi load lai header
		ReportAmountCategoryRow row;// = new SupervisorReportStaffRow(parent,
									 // this, 0);
		ArrayList<String> lstTitle = new ArrayList<String>();
			lstTitle.add(StringUtil.getString(R.string.TEXT_TB_CUSTOMER_CODE));
			lstTitle.add(StringUtil.getString(R.string.TEXT_CUSTOMER_NAME_SHORT));
		ReportAmountCategoryRow header = new ReportAmountCategoryRow(parent, this, 0);
		for (int i = 0, size = RPT_STAFF_SALE_TABLE.getInstance().getCAT_LIST().length; i < size; i++) {
			lstTitle.add(RPT_STAFF_SALE_TABLE.getInstance().getCAT_LIST()[i] + " - " + RPT_STAFF_SALE_TABLE.getInstance().getCAT_NAME_LIST()[i]);
			lstTitle.add(StringUtil.getString(R.string.TEXT_KH));
			lstTitle.add(StringUtil.getString(R.string.TEXT_DONE_SHORT));
			lstTitle.add(StringUtil.getString(R.string.TEXT_PERCENT));
			if (i % 2 == 1) {
				header.initializeHeader(true);
			} else {
				header.initializeHeader(false);
			}
		}
		initHeaderTable(tbReportCategory, header, lstTitle);
		for (int i = 0, size = reportDataScreen.listFocusInfoRow.size(); i < size; i++) {
			row = new ReportAmountCategoryRow(parent, this, 0);
			row.render(reportDataScreen.listFocusInfoRow.get(i), reportDataScreen.progressSales, false);
			row.setListener(this);
			tbReportCategory.addRow(row);
		}

		row = new ReportAmountCategoryRow(parent, this, 0);
		row.render(reportDataScreen.objectReportTotal, reportDataScreen.progressSales, true);
		tbReportCategory.addRow(row);
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
		case ActionEventConstant.GET_REPORT_CATEGORY:
			if (modelEvent.getModelData() != null) {
				reportDataScreen = (SupervisorReportStaffSaleViewDTO) modelEvent.getModelData();
				this.renderLayout();
			}
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		this.parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
			case ActionEventConstant.GET_LIST_COMBOBOX: {
				this.parent.closeProgressDialog();
				break;
			}
			case ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_AMOUNT_REPORT:
				this.parent.closeProgressDialog();
				break;
			default:
				super.handleErrorModelViewEvent(modelEvent);
				break;
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_MENU_GENERAL_STATISTICS:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW, UserController.getInstance());
			break;
		case ACTION_MENU_MHTT:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW, UserController.getInstance());
			break;
		case ACTION_MENU_NEED_DONE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW, UserController.getInstance());
			break;
		case ACTION_MENU_REPORT_DISPLAY_PROGRESS:
			handleSwitchFragment(null, ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY, UserController.getInstance());
			break;
		case ACTITON_MENU_REPORT_AMOUNT_CATEGORY:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REPORT_AMOUNT_CATEGORY, UserController.getInstance());
			break;
		case ACTITON_MENU_REPORT_QUANTITY_CATEGORY:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REPORT_QUANTITY_CATEGORY, UserController.getInstance());
			break;
		default:
			break;
		}
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				reportDataScreen = null;
				requestReportCategory();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}

	}

}
