package com.ths.dmscore.view.graphchart;

import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.ths.dmscore.dto.view.ChartDTO;

/**
 * pager cho man hinh chart
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class CycleChartPagerAdapter extends PagerAdapter {

	List<ChartDTO>  lstCycleChart;
	Context context;
	// ten cac cot mau tuong ung
	private String[] annotation;
	// mau ve chart
	private int[] lstColorChart;

	// co show cot kh hay ko
	public boolean isShowPlan = true;
	// co show cot thuc hien hay ko
	public boolean isShowReality = true;

	/**
	 * Co show cac cot quantity plan, plan hay ko
	 * 
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void setShowColumn(boolean isShowPlan, boolean isShowReality) {
		this.isShowPlan = isShowPlan;
		this.isShowReality = isShowReality;
	}
	
	public CycleChartPagerAdapter(Context context, String[] annotation,
			int[] lstColorChart, List<ChartDTO>  lstCycleChart) {
		this.lstCycleChart = lstCycleChart;
		this.lstColorChart = lstColorChart;
		this.annotation = annotation;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return lstCycleChart.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0.equals(arg1);
	}
	
	@Override
	public Object instantiateItem(ViewGroup view, int position) {
		CycleChartView chart = new CycleChartView(context);
		ChartDTO item = lstCycleChart.get(position);
		renderLayout(chart,item);
		((ViewPager) view).addView(chart, 0);
		chart.repaint();
//		LayoutInflater inflater = LayoutInflater.from(context);
//		View imageLayout = inflater.inflate(R.layout.customer_programe_display_row,
//				view, false);
//		((ViewPager) view).addView(chart, 0);
		return chart;
	}
	
	 /**
	 * render noi dung
	 * @author: Tuanlt11
	 * @param chart
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(CycleChartView chart,ChartDTO item){
		chart.drawChart(item.name, annotation,lstColorChart, item);
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}
	

}
