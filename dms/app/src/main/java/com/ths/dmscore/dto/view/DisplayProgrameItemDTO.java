/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * 
 * Mo ta cho class
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class DisplayProgrameItemDTO implements Serializable {
	private static final long serialVersionUID = -5428407327054686466L;
	public String id;
	public String value;
	public String name;
	public String fullName;	
}
