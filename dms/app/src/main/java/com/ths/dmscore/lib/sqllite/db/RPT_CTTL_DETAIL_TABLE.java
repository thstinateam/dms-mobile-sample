package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Chua thong tin ket qua tra thuong KM tich luy
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_CTTL_DETAIL_TABLE extends ABSTRACT_TABLE {
	public static final String RPT_CTTL_DETAIL_ID = "RPT_CTTL_DETAIL_ID";
	public static final String RPT_CTTL_ID = "RPT_CTTL_ID";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String QUANTITY = "QUANTITY";
	public static final String AMOUNT = "AMOUNT";
	public static final String QUANTITY_PAY_PROMOTION = "QUANTITY_PAY_PROMOTION";
	public static final String AMOUNT_PAY_PROMOTION = "AMOUNT_PAY_PROMOTION";

	public static final String TABLE_NAME = "RPT_CTTL_DETAIL";

	public RPT_CTTL_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { RPT_CTTL_DETAIL_ID, RPT_CTTL_ID,
				PRODUCT_ID, QUANTITY, AMOUNT, QUANTITY_PAY_PROMOTION,
				AMOUNT_PAY_PROMOTION, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}