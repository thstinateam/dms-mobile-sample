/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.constants;

/**
 * Danh sach cac column can order
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  8:05:45 AM Feb 24, 2015
 */
public class ConstantOrderColumn {
	//fullDate la comments
	public static final String SUB_CAT_NAME = "SUB_CAT_NAME";
	public static final String PRODUCT_CODE = "PRODUCT_CODE";
	public static final String PRODUCT_INFO_NAME = "PRODUCT_INFO_NAME";
	public static final String PRODUCT_INFO_CODE = "PRODUCT_INFO_CODE";
	public static final String PRODUCT_NAME = "PRODUCT_NAME";
	public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
	public static final String CUSTOMER_NAME = "CUSTOMER_NAME";
	public static final String ADDRESS = "ADDRESS";
	public static final String SHOP_NAME = "sh.SHOP_NAME";
	public static final String SHOP_CODE = "sh.SHOP_CODE";
	public static final String VALUE_SUM = "VALUE_SUM";
	public static final String VALUE_PLAN_SUM = "VALUE_PLAN_SUM";
	public static final String ROW_NAME = "ROW_NAME";
}
