/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util.guard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ths.dmscore.util.MyLog;

public class SystemDialogReceiver extends BroadcastReceiver {

	private static final String SYSTEM_DIALOG_REASON_KEY = "reason";
	private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
//	private static final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
//	private static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent != null && Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(intent.getAction())) {
			String dialogType = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
			MyLog.i("SystemDialogReceiver", "dialogType: " + dialogType);
			if (dialogType != null && dialogType.equals(SYSTEM_DIALOG_REASON_RECENT_APPS)) {
				Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
				context.sendBroadcast(closeDialog);
			}
		}
	}
//	 WindowManager.LayoutParams windowManagerParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY , 
//	            WindowManager.LayoutParams. FLAG_DIM_BEHIND, PixelFormat.TRANSLUCENT);
//
//	 WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
//
//	    LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
//	    View myView = inflater.inflate(R.layout.youractivity, null);
//
//	 wm.addView(myView, windowManagerParams);
	//android.permission.SYSTEM_ALERT_WINDOW
}
