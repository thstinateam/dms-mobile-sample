/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingIssueDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSupTrainingIssueDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.dto.UserDTO;

/**
 * 
 * SUP_TRANING_ISSUE_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:39:48 17-11-2014
 */
public class SUP_TRAINING_ISSUE_TABLE extends ABSTRACT_TABLE {
	public static final String SUP_TRAINING_ISSUE_ID = "SUP_TRAINING_ISSUE_ID";
	public static final String SUP_TRAINING_PLAN_ID = "SUP_TRAINING_PLAN_ID";
	public static final String ISSUE = "ISSUE";
	public static final String JOB_CONTENT = "JOB_CONTENT";
	public static final String EXECUTE_STAFF_ID = "EXECUTE_STAFF_ID";
	public static final String EXECUTE_ROLE = "EXECUTE_ROLE";
	public static final String DUE_DATE = "DUE_DATE";
	public static final String COMPLETE_DATE = "COMPLETE_DATE";
	public static final String STATUS = "STATUS";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_STAFF_ID = "CREATE_STAFF_ID";
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_NAME = "SUP_TRAINING_ISSUE";

	public SUP_TRAINING_ISSUE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SUP_TRAINING_ISSUE_ID,
				SUP_TRAINING_PLAN_ID, ISSUE, JOB_CONTENT, EXECUTE_STAFF_ID,
				EXECUTE_ROLE, DUE_DATE, COMPLETE_DATE, STATUS, CREATE_USER, CREATE_STAFF_ID,
				UPDATE_USER, CREATE_DATE, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * lay danh sach van de
	 * 
	 * @author: dungdq3
	 * @since: 8:56:27 AM Nov 19, 2014
	 * @return: ListSupTrainingIssueDTO
	 * @throws:  
	 * @param data
	 * @return
	 * @throws Exception 
	 */
	public ListSupTrainingIssueDTO getListIssue(Bundle data) throws Exception {
		// TODO Auto-generated method stub
		ListSupTrainingIssueDTO dto = new ListSupTrainingIssueDTO();
		Cursor c = null;
		
		long supTrainingPlanID = data.getLong(IntentConstants.INTENT_SUP_TRAINING_PLAN_ID, 0);
		int createStafID = data.getInt(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		int page = data.getInt(IntentConstants.INTENT_PAGE, 1);
		boolean isGetTotalPage = data.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, true);
		
		ArrayList<String> param = new ArrayList<String>();

		StringBuffer  varname1 = new StringBuffer();
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			varname1.append(" SELECT sti.issue as ISSUE, ");
			varname1.append("       sti.job_content as JOB_CONTENT, ");
			varname1.append("       sti.sup_training_issue_id as SUP_TRAINING_ISSUE_ID, ");
			varname1.append("       sti.execute_staff_id as EXECUTE_STAFF_ID, ");
			varname1.append("       sti.execute_role as EXECUTE_ROLE, ");
			varname1.append("       Strftime('%d/%m/%Y', sti.due_date) as DUE_DATE_VIEW, ");
			varname1.append("       sti.due_date as DUE_DATE, ");
//			varname1.append("       Strftime('%d/%m/%Y', sti.complete_date) as COMPLETE_DATE_VIEW, ");
			varname1.append("       sti.complete_date as COMPLETE_DATE, ");
			varname1.append("       sti.status as STATUS ");
			varname1.append(" FROM   sup_training_issue sti ");
			varname1.append(" WHERE  1 = 1 ");
			varname1.append("       AND sti.create_staff_id = ? ");
			param.add(String.valueOf(createStafID));
			varname1.append("       AND sti.sup_training_plan_id = ? ");
			param.add(String.valueOf(supTrainingPlanID));
			varname1.append("       AND sti.status in (1,2,3,4) ");
			varname1.append(" order by sti.status, sti.due_date, sti.complete_date ");
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			varname1.append(" SELECT sti.issue as ISSUE, ");
			varname1.append("       sti.job_content as JOB_CONTENT, ");
			varname1.append("       sti.sup_training_issue_id as SUP_TRAINING_ISSUE_ID, ");
			varname1.append("       sti.execute_staff_id as EXECUTE_STAFF_ID, ");
			varname1.append("       sti.execute_role as EXECUTE_ROLE, ");
			varname1.append("       Strftime('%d/%m/%Y', sti.due_date) as DUE_DATE_VIEW, ");
			varname1.append("       sti.due_date as DUE_DATE, ");
//			varname1.append("       Strftime('%d/%m/%Y', sti.complete_date) as COMPLETE_DATE, ");
			varname1.append("       sti.complete_date as COMPLETE_DATE, ");
			varname1.append("       sti.status as STATUS ");
			varname1.append(" FROM   sup_training_issue sti, sup_training_plan stp ");
			varname1.append(" WHERE  1 = 1 ");
//			varname1.append("       AND sti.create_staff_id = ( ");
//			varname1.append("							SELECT staff_owner_id ");
//			varname1.append("							FROM   staff ");
//			varname1.append("							WHERE  staff_id = ? ) ");
//			param.add(String.valueOf(createStafID));
			
			varname1.append("		AND sti.create_staff_id IN (SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			varname1.append("				AND PS.staff_id = ? AND PS.STATUS = 1 ");
			param.add(String.valueOf(createStafID));
			varname1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
			
			varname1.append("       AND sti.sup_training_plan_id = ? ");
			param.add(String.valueOf(supTrainingPlanID));
			varname1.append("       AND sti.sup_training_plan_id = stp.sup_training_plan_id ");
//			varname1.append("       AND stp.is_send = 1 ");
			varname1.append("       AND sti.status in (1,2,3,4) ");
			varname1.append(" order by sti.status, sti.due_date, sti.complete_date ");
		}
		
		if (isGetTotalPage) {
			StringBuffer total = new StringBuffer(); 
			total.append("select count(*) as TOTAL from (");
			total.append(varname1);
			total.append(") ");
			try {

				c = rawQueries(total.toString(), param);
				if (c != null) {
					if (c.moveToFirst()) {
						dto.setTotalItem(c.getInt(0));
					}
				}
			} catch (Exception e) {
				throw e;
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception ex) {
				}
			}
		}
		
		varname1.append(" limit "
				+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
		varname1.append(" offset "
				+ Integer
						.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));

		try {

			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SupTrainingIssueDTO issue = new SupTrainingIssueDTO();
						issue.setSupTrainingPlanID(supTrainingPlanID);
						issue.initFromCursor(c);
						dto.addItem(issue);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}
		return dto;
	}

	/**
	 * luu issue
	 * 
	 * @author: dungdq3
	 * @since: 10:00:34 AM Nov 19, 2014
	 * @return: long
	 * @throws:  
	 * @param issue2
	 * @return
	 * @throws Exception 
	 */
	public long saveIssue(SupTrainingIssueDTO issue) throws Exception {
		// TODO Auto-generated method stub
		long returnCode = -1;
		try {
			// chua luu nhung da xoa
			returnCode = insert(issue);
		} catch (Exception e) {
			throw e;
		}
		return returnCode;
	}

	/**
	 * insert issue
	 * 
	 * @author: dungdq3
	 * @since: 10:02:48 AM Nov 19, 2014
	 * @return: long
	 * @throws:  
	 * @param issue2
	 * @return
	 */
	private long insert(SupTrainingIssueDTO issue) {
		// TODO Auto-generated method stub
		ContentValues cv = generateDataInsert(issue);
		return insert(null, cv);
	}

	/**
	 * generate data for insert issue
	 * 
	 * @author: dungdq3
	 * @since: 10:04:06 AM Nov 19, 2014
	 * @return: ContentValues
	 * @throws:  
	 * @param issue2
	 * @return
	 */
	private ContentValues generateDataInsert(SupTrainingIssueDTO issue) {
		// TODO Auto-generated method stub
		ContentValues editedValues = new ContentValues();
		editedValues.put(SUP_TRAINING_ISSUE_ID, issue.getSupTrainingIssueID());
		editedValues.put(SUP_TRAINING_PLAN_ID, issue.getSupTrainingPlanID());
		editedValues.put(ISSUE, issue.getIssue());
		editedValues.put(JOB_CONTENT, issue.getJobContent());
		editedValues.put(EXECUTE_STAFF_ID, issue.getExcuteStaffID());
		editedValues.put(EXECUTE_ROLE, issue.getExcuteRole());
		editedValues.put(DUE_DATE, issue.getDueDate());
		editedValues.put(STATUS, issue.getStatus());
		editedValues.put(CREATE_STAFF_ID, issue.getCreateStaffID());
		editedValues.put(CREATE_DATE, issue.getCreateDate());
		return editedValues;
	}

	/**
	 * xoa issue
	 * 
	 * @author: dungdq3
	 * @since: 5:20:42 PM Nov 19, 2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 */
	public long delete(SupTrainingIssueDTO dto) {
		// TODO Auto-generated method stub
		String[] param = {String.valueOf(dto.getSupTrainingIssueID())};
		return delete(SUP_TRAINING_ISSUE_ID + " = ? ", param);
	}

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 8:45:20 AM Nov 20, 2014
	 * @return: long
	 * @throws:  
	 * @param dto
	 * @return
	 */
	public long update(SupTrainingIssueDTO dto) {
		// TODO Auto-generated method stub
		ContentValues cv = new ContentValues();
		cv.put(STATUS, dto.getStatus());
		cv.put(JOB_CONTENT, dto.getJobContent());
		cv.put(ISSUE, dto.getIssue());
		cv.put(UPDATE_DATE, dto.getUpdateDate());
		cv.put(UPDATE_USER, dto.getUpdateUser());
		cv.put(EXECUTE_ROLE, dto.getExcuteRole());
		cv.put(COMPLETE_DATE, dto.getCompleteDate());
		cv.put(DUE_DATE, dto.getDueDate());
		String[] param = {String.valueOf(dto.getSupTrainingIssueID())};
		return update(cv, SUP_TRAINING_ISSUE_ID + " = ? ", param);
	}
}
