/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.CustomerVisitedViewDTO;
import com.ths.dmscore.dto.view.CustomerVisitedViewDTO.CustomerVisitedDTO;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.sale.statistic.GeneralStatisticsView;
import com.ths.dms.R;

public class PopupCustomerVisitedDetailView extends LinearLayout implements
		OnClickListener {

	// OnEventControlListener
	public OnEventControlListener listener;
	int actionClose;
	// context
	GeneralStatisticsView context;
	Context parent;
	// root layout
	public View viewLayout;
	Button btClose;

	// table ds khach hang da ghe tham
	// public cho generalStatistics dung
	public DMSTableView tbCustomerList;
	public int currentPage = -1;

	/**
	 * @param context
	 * @param attrs
	 */
	public PopupCustomerVisitedDetailView(GeneralStatisticsView context,
			Context parent, OnEventControlListener listener, int actionClose) {
		super(parent);
		this.context = context;
		this.parent = parent;
		// this.parent = (SalesPersonActivity) context;

		this.listener = listener;
		this.actionClose = actionClose;
		LayoutInflater inflater = (LayoutInflater) parent.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		viewLayout = inflater.inflate(
				R.layout.layout_custom_customer_visited_detail_nvbh_view, this, false);

		tbCustomerList = (DMSTableView) viewLayout
				.findViewById(R.id.tbCustomerVisitedList);
		tbCustomerList.setListener(context);
		// tbCustomerList.setTotalSize(1);
		// tbCustomerList.getPagingControl().setVisibility(View.GONE);

		btClose = (Button) viewLayout.findViewById(R.id.btClose);
		btClose.setOnClickListener(this);
	}

	/**
	 *
	 * render layout for popup
	 *
	 * @param object
	 * @return: void
	 * @throws:
	 * @author: DungNX
	 * @date: DEC 10, 2012
	 */
	public void renderLayoutWithObject(CustomerVisitedViewDTO dto) {
		if (currentPage != -1) {
//			tbCustomerList.setTotalSize(dto.totalCustomer);
//			tbCustomerList.getPagingControl().setCurrentPage(currentPage);
		} else {
			tbCustomerList.setTotalSize(dto.totalCustomer, 1);
			currentPage = tbCustomerList.getPagingControl().getCurrentPage();
		}
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE * (tbCustomerList.getPagingControl().getCurrentPage() - 1);

		tbCustomerList.clearAllDataAndHeader();
		CustomerVisitedDetailRow header = new CustomerVisitedDetailRow(parent);
		header.initHeaderAmount();
		tbCustomerList.addHeader(header);
		for (CustomerVisitedDTO item : dto.arrWrong) {
			CustomerVisitedDetailRow customerVisitedDetailRow = new CustomerVisitedDetailRow(
					parent);
			customerVisitedDetailRow.render(item, pos);
			pos++;
			tbCustomerList.addRow(customerVisitedDetailRow);
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (listener != null) {
			if (v == btClose) {
				listener.onEvent(actionClose, v, null);
			}
		}
	}

}
