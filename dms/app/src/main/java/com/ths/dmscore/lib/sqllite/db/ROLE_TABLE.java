/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.RoleDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.CursorUtil;

/**
 *  Luu cac quyen trong he thong
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ROLE_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String ROLE_ID = "ROLE_ID";
	// ma quyen 
	public static final String ROLE_CODE = "ROLE_CODE";
	// trang thai 1: hieu luc, 0: khong hieu luc
	public static final String STATUS = "STATUS";
	
	private static final String ROLE_TABLE = "ROLE";
	
	public ROLE_TABLE(SQLiteDatabase mDB) {
		this.tableName = ROLE_TABLE;
		this.columns = new String[] {ROLE_ID, ROLE_CODE ,STATUS, SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((RoleDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(RoleDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}
	
	/**
	 * Update 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		RoleDTO dtoRole = (RoleDTO)dto;
		ContentValues value = initDataRow(dtoRole);
		String[] params = { "" + dtoRole.roleId };
		return update(value, ROLE_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(ROLE_ID + " = ?", params);
	}
	
	public long delete(AbstractTableDTO dto) {
		RoleDTO dtoRole = (RoleDTO)dto;
		String[] params = { "" + dtoRole.roleId };
		return delete(ROLE_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * @author: TruongHN
	 * @param id
	 * @return: RoleDTO
	 * @throws:
	 */
	public RoleDTO getRowById(String id) {
		RoleDTO dto = null;
		Cursor c = null;
		try {
			String[]params = {id};
			c = query(
					ROLE_ID + " = ?" , params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private RoleDTO initDTOFromCursor(Cursor c) {
		RoleDTO dto = new RoleDTO();
		dto.roleId = (CursorUtil.getInt(c, ROLE_ID));
		dto.roleCode = (CursorUtil.getString(c, ROLE_CODE));
		dto.status = (CursorUtil.getInt(c, STATUS));
		
		return dto;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 * @author: TruongHN
	 * @return: Vector<FeedBackDTO>
	 * @throws:
	 */
	public Vector<RoleDTO> getAllRow() {
		Vector<RoleDTO> v = new Vector<RoleDTO>();
		Cursor c = null;
		try {
			c = query(null,
					null, null, null, null);
			if (c != null) {
				RoleDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initDTOFromCursor(c);
						v.addElement(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(RoleDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(ROLE_ID, dto.roleId);
		editedValues.put(ROLE_CODE, dto.roleCode);
		editedValues.put(STATUS, dto.status);

		return editedValues;
	}
}
