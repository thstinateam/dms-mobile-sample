/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PRODUCT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Thong tin san pham
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class ProductDTO extends AbstractTableDTO {
	public static final String PRODUCT_EMPTY = "0/0";

	// id san pham
	public int productId;
	// ten san pham
	public String productName;
	// trang thai 1: hoat dong, 0: ngung
	public int status;
	// don vi tinh nho nhat (hop)
	public String uom1;
	// package (thung)
	public String uom2;
	// gia tri quy doi tu UOM2 --> UOM1
	public int convfact;
	// ma nganh hang
	public String categoryCode;
	// ma nganh hang con
	public String subcategoryCode;
	public String subCategoryId;
	// thuoc tinh cua mat hang
	public String brand;
	// thuoc tinh cua mat hang
	public String flavour;
	// ton kho an toan - chua dung
	public float safetyStock;
	// hoa hong - chua dung
	public String commission;
	// ngay tao
	public String createDate;
	// ngay cap nhat
	public String udpateDate;
	// thuoc tinh cua mat hang
	public float volumn;
	// thuoc tinh cua mat hang
	public float netweight;
	// thuoc tinh cua mat hang
	public float grossWeight;
	// thuoc tinh cua mat hang
	public String packing;
	public int productTypeId;
	// ma mat hang
	public String productCode;
	// nguoi tao
	public String createUser;
	// nguoi cap nhat
	public String udpateUser;
	// loai san pham
	public String productType;
	// sub cat cua sang pham
	public String subCat;
	// don gia
	public double unitPrice;
	// don gia
	public double packagePrice;
	// ton kho
	public String inventory;
	// tong hang
	public int totalProduct;

	// has promotion
	public boolean isHasPromotion;
	// has focus programe
	public boolean isHasFocus;
	public boolean isUOM2;

	public int isHasImage;

	public int isHasVideo;
	// id nganh hang
	public int catId;

	public ProductDTO() {
		super(TableType.PRODUCT);
	}

	public void initDataFromCursor(Cursor c) {
		productId = CursorUtil.getInt(c, "PRODUCT_ID");
		productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		productName = CursorUtil.getString(c, "PRODUCT_NAME");
		uom1 = CursorUtil.getString(c, "UOM1");
		uom2 = CursorUtil.getString(c, "UOM2");
		categoryCode = CursorUtil.getString(c, "CATEGORY_CODE");
		subcategoryCode = CursorUtil.getString(c, "SUB_CATEGORY_CODE");
		unitPrice = CursorUtil.getDouble(c, "PRICE");
		packagePrice = CursorUtil.getDouble(c, "PACKAGE_PRICE");
		convfact = CursorUtil.getInt(c, "CONVFACT");
		convfact = convfact > 0 ? convfact : 1;
		if (c.getColumnIndex("AVAILABLE_QUANTITY") >= 0) {
			totalProduct = CursorUtil.getInt(c, "AVAILABLE_QUANTITY");
			int b = totalProduct % convfact;
			inventory = GlobalUtil.formatNumberProductFlowConvfact(totalProduct, convfact);
			if (b == 0) {
				isUOM2 = true;
			}
		} else {
			totalProduct = 0;
			inventory = PRODUCT_EMPTY;
		}
		isHasFocus = CursorUtil.getInt(c, "TT") == 1 ? true : false;
		isHasPromotion = CursorUtil.getInt(c, "HAS_PROMOTION") == 1 ? true
				: false;
		isHasImage = CursorUtil.getInt(c, "IMAGE");
		isHasVideo = CursorUtil.getInt(c, "VIDEO");

	}

	/**
	 *
	 * init data cho popup chi tiet san pham
	 *
	 * @author: HaiTC3
	 * @param cursor
	 * @return: void
	 * @throws:
	 * @since: Feb 22, 2013
	 */
	public void initDataWithCursor(Cursor cursor) {
		productCode = CursorUtil.getString(cursor, PRODUCT_TABLE.PRODUCT_CODE);
		productName = CursorUtil.getString(cursor, PRODUCT_TABLE.PRODUCT_NAME);
		convfact = CursorUtil.getInt(cursor, PRODUCT_TABLE.CONVFACT);
		convfact = convfact > 0 ? convfact : 1;
		categoryCode = CursorUtil.getString(cursor, PRODUCT_TABLE.CATEGORY_CODE);
		uom1 = (CursorUtil.getString(cursor, PRODUCT_TABLE.UOM1));
		uom2 = (CursorUtil.getString(cursor, PRODUCT_TABLE.UOM2));
	}
}
