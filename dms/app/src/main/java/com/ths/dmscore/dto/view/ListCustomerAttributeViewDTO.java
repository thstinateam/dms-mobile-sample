/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * ListCustomerAttributeViewDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:15:26 PM Jan 27, 2015
 */
public class ListCustomerAttributeViewDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public ArrayList<CustomerAttributeViewDTO> listAttribute;
	
	public ListCustomerAttributeViewDTO() {
		// TODO Auto-generated constructor stub
		listAttribute = new ArrayList<CustomerAttributeViewDTO>();
	}
	
	
	public void addCustomerAttribute(CustomerAttributeViewDTO cusAttr, CustomerAttributeEnumViewDTO cusEnum){
		int posExist = -1;
		for(int i = 0, size = listAttribute.size(); i < size; i++){
			if(listAttribute.get(i).customerAttributeID == cusAttr.customerAttributeID){
				posExist = i;
				break;
			}
		}
		
		if(posExist > -1){ // da ton tai
			listAttribute.get(posExist).addAttributeEnum(cusEnum);
		} else if (posExist == -1){
			cusAttr.addAttributeEnum(cusEnum);
			listAttribute.add(cusAttr);
		}
	}
}
