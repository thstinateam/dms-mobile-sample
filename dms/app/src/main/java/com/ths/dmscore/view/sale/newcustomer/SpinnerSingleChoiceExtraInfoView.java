/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dms.R;
import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;

/**
 * SpinnerSingleChoiceExtraInfoView.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  3:54:44 PM Jan 27, 2015
 */
public class SpinnerSingleChoiceExtraInfoView extends AbstractExtraInfoView implements OnItemSelectedListener {

	private TextView tv;
	private Spinner sp;
	private CustomerAttributeViewDTO attrDTO;
	
	public SpinnerSingleChoiceExtraInfoView(Context context) {
		super(context, R.layout.layout_spinner_extra_info_view);
		// TODO Auto-generated constructor stub
		tv = (TextView) arrView.get(0);
		sp = (Spinner) arrView.get(1);
		sp.setOnItemSelectedListener(this);
	}

	@Override
	public Object getDataFromView() {
		// TODO Auto-generated method stub
		CustomerAttributeDetailViewDTO detail = new CustomerAttributeDetailViewDTO();
		int id = attrDTO.getListEnum().get(sp.getSelectedItemPosition()).enumID;
		detail.attrID = attrDTO.customerAttributeID;
		detail.enumID = id;
		detail.detailID = attrDTO.attributeDetailID;
		return detail;
	}

	@Override
	public void renderLayout(CustomerAttributeViewDTO dto, boolean isEdit, String fromView) {
		// TODO Auto-generated method stub
		attrDTO = dto;
		if (dto.mandatory == 1) {
			Class<?> clazz;
			Object fromViewObject = null;
			try {
				clazz = Class.forName(fromView);
				fromViewObject = clazz.newInstance();
			} catch (ClassNotFoundException e) {
				MyLog.e("SpinnerSingleChoiceExtraInfoView", "ClassNotFoundException", e);
			} catch (java.lang.InstantiationException e) {
				MyLog.e("SpinnerSingleChoiceExtraInfoView", "InstantiationException", e);
			} catch (IllegalAccessException e) {
				MyLog.e("SpinnerSingleChoiceExtraInfoView", "IllegalAccessException", e);
			}
			if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
				setSymbol("*", dto.name, ImageUtil.getColor(R.color.RED), tv);
			} else {
				tv.setText(dto.name);
			}
		} else if (dto.mandatory == 0) {
			tv.setText(dto.name);
		}
//		SpinnerAdapter adap = new SpinnerAdapter(con,
//				R.layout.simple_spinner_item, dto.toArrayString());
		ArrayAdapter adap = new ArrayAdapter(con, R.layout.simple_spinner_item, dto.toArrayString());
		adap.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		sp.setAdapter(adap);
		sp.setSelection(0);
		sp.setEnabled(isEdit);
		if (dto.enumID > 0) {
			for (int i = 0, size = dto.listEnum.size(); i < size; i++) {
				if (dto.listEnum.get(i).enumID == dto.enumID) {
					sp.setSelection(i);
				}
			}
		}
		tag = dto.customerAttributeID;
	}

	@Override
	public void onItemSelected(AdapterView<?> adap, View v, int pos,
			long arg3) {
		// TODO Auto-generated method stub
		if(adap == sp) {
			
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isCheckData() {
		return true;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return attrDTO.name;
	}

}
