package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.RptCttlDetailPayDTO;

/**
 * Chua thong tin ket qua tra thuong KM tich luy detail
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_CTTL_DETAIL_PAY_TABLE extends ABSTRACT_TABLE {

	public static final String RPT_CTTL_DETAIL_PAY_ID = "RPT_CTTL_DETAIL_PAY_ID";
	public static final String RPT_CTTL_PAY_ID = "RPT_CTTL_PAY_ID";
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String QUANTITY_PROMOTION = "QUANTITY_PROMOTION";
	public static final String AMOUNT_PROMOTION = "AMOUNT_PROMOTION";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CREATE_DATE = "CREATE_DATE";

	public static final String TABLE_NAME = "RPT_CTTL_DETAIL_PAY";

	public RPT_CTTL_DETAIL_PAY_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { RPT_CTTL_DETAIL_PAY_ID, RPT_CTTL_PAY_ID,
				PRODUCT_ID, QUANTITY_PROMOTION, AMOUNT_PROMOTION, STAFF_ID, SHOP_ID, CREATE_DATE,
				SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value =  initDataRow((RptCttlDetailPayDTO) dto);
		return insert(null, value);
	}
	
	private ContentValues initDataRow(RptCttlDetailPayDTO dto) {
		ContentValues editedValues = new ContentValues();
		
		editedValues.put(RPT_CTTL_DETAIL_PAY_ID, dto.rptCttlDetailPayId);
		editedValues.put(RPT_CTTL_PAY_ID, dto.rptCttlPayId);
		editedValues.put(PRODUCT_ID, dto.productId);
		editedValues.put(QUANTITY_PROMOTION, dto.quantityPromotion);
		editedValues.put(AMOUNT_PROMOTION, dto.amountPromotion);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CREATE_DATE, dto.createDate);
		
		return editedValues;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel
	 * .vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
}