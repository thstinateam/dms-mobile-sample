/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PaymentDetailDTO;

/**
 * Bang cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class PAYMENT_DETAIL_TEMP_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String PAYMENT_DETAIL_ID = "PAYMENT_DETAIL_ID";
	//
	public static final String DEBIT_DETAIL_ID = "DEBIT_DETAIL_ID";
	//
	public static final String PAY_RECEIVED_ID = "PAY_RECEIVED_ID";
	//
	public static final String AMOUNT = "AMOUNT";
	//
	public static final String PAYMENT_TYPE = "PAYMENT_TYPE";
	//
	public static final String STATUS = "STATUS";
	//
	public static final String DISCOUNT = "DISCOUNT";
	//
	public static final String PAY_DATE = "PAY_DATE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String PAYMENT_STATUS = "PAYMENT_STATUS";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";

	// ten bang
	public static final String TABLE_PAYMENT_TEMP_DETAIL = "PAYMENT_DETAIL_TEMP";
	public static final String STAFF_ID = "STAFF_ID";
	// chi dung cho bang tam tren server
	public static final String FOR_OBJECT_ID = "FOR_OBJECT_ID";
	public static final String TABLE_PAYMENT_DETAIL_TEMP = "PAYMENT_DETAIL_TEMP";

	public PAYMENT_DETAIL_TEMP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_PAYMENT_TEMP_DETAIL;
		this.columns = new String[] { PAYMENT_DETAIL_ID, DEBIT_DETAIL_ID, PAY_RECEIVED_ID, AMOUNT, PAYMENT_TYPE,
				STATUS, PAY_DATE, DISCOUNT, PAYMENT_STATUS, SHOP_ID, CUSTOMER_ID };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PaymentDetailDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PaymentDetailDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * 
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PaymentDetailDTO disDTO = (PaymentDetailDTO) dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.paymentDetailID };
		return update(value, PAYMENT_DETAIL_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * 
	 * @author: TruongHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(PAYMENT_DETAIL_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PaymentDetailDTO paramDTO = (PaymentDetailDTO) dto;
		String[] params = { String.valueOf(paramDTO.paymentDetailID) };
		return delete(PAYMENT_DETAIL_ID + " = ?", params);
	}

	private ContentValues initDataRow(PaymentDetailDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PAYMENT_DETAIL_ID, dto.paymentDetailID);
//		editedValues.put(DEBIT_DETAIL_ID, dto.debitDetailID);
		editedValues.put(PAY_RECEIVED_ID, dto.payReceivedID);
		editedValues.put(AMOUNT, dto.amount);
		editedValues.put(PAYMENT_TYPE, dto.paymentType);
		editedValues.put(STATUS, dto.status);
		editedValues.put(PAY_DATE, dto.payDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(DISCOUNT, dto.discount);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(FOR_OBJECT_ID, dto.saleOrderId);
		
		editedValues.put(PAYMENT_STATUS, dto.paymentStatus);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(SHOP_ID, dto.shopId);
		return editedValues;
	}

	public int updateStatus(long paymentDetailID, int i) {
		// TODO Auto-generated method stub
		ContentValues value = new ContentValues();
		value.put(STATUS, i);
		String[] params = { "" + paymentDetailID };
		return update(value, PAYMENT_DETAIL_ID + " = ?", params);
	}

}
