package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.AccSaleProgReportDTO;
import com.ths.dmscore.dto.view.AccSaleProgReportDTO.AccSaleProgReportItem;
import com.ths.dmscore.dto.view.CabinetStaffDTO;
import com.ths.dmscore.dto.view.HistoryItemDTO;
import com.ths.dmscore.dto.view.ListStaffDTO;
import com.ths.dmscore.dto.view.ManagerEquipmentDTO;
import com.ths.dmscore.dto.view.ProgressDateDetailReportDTO;
import com.ths.dmscore.dto.view.ProgressDateReportDTO;
import com.ths.dmscore.dto.view.ProgressReportSalesFocusDTO;
import com.ths.dmscore.dto.view.ProgressReportSalesFocusDTO.InfoProgressEmployeeDTO;
import com.ths.dmscore.dto.view.ProgressReportSalesFocusDTO.ReportProductFocusItem;
import com.ths.dmscore.dto.view.ReportFocusProductItem;
import com.ths.dmscore.dto.view.ReportProgressMonthCellDTO;
import com.ths.dmscore.dto.view.ReportProgressMonthViewDTO;
import com.ths.dmscore.dto.view.ReportSalesFocusEmployeeInfo;
import com.ths.dmscore.dto.view.SupervisorReportStaffSaleItemDTO;
import com.ths.dmscore.dto.view.SupervisorReportStaffSaleViewDTO;
import com.ths.dmscore.dto.view.TBHVCustomerNotPSDSReportDTO;
import com.ths.dmscore.dto.view.TBHVManagerEquipmentDTO;
import com.ths.dmscore.dto.view.TBHVProgressDateReportDTO;
import com.ths.dmscore.dto.view.TBHVProgressDateReportDTO.TBHVProgressDateReportItem;
import com.ths.dmscore.dto.view.TBHVProgressReportSalesFocusViewDTO;
import com.ths.dmscore.dto.view.VisitDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dms.R;

/**
 *
 * table summary all report for gsnpp & tbhv
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class RPT_STAFF_SALE_TABLE extends ABSTRACT_TABLE {

	// id khach hang
	public static final String STAFF_ID = "STAFF_ID";
	public static final String STAFF_NAME = "STAFF_NAME";
	public static final String STAFF_CODE = "STAFF_CODE";
	public static final String PARENT_STAFF_ID = "PARENT_STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	// doanh so ke hoach thang
	public static final String MONTH_AMOUNT_PLAN = "MONTH_AMOUNT_PLAN";
	// doanh so thuc hien trong thang
	public static final String MONTH_AMOUNT = "MONTH_AMOUNT";
	// doanh so thuc hien trong thang
	public static final String MONTH_AMOUNT_APPROVED = "MONTH_AMOUNT_APPROVED";
	// diem trong thang
	public static final String MONTH_SCORE = "MONTH_SCORE";
	// doanh so ke hoach ngay
	public static final String DAY_AMOUNT_PLAN = "DAY_AMOUNT_PLAN";
	// doanh so thuc hien trong ngay
	public static final String DAY_AMOUNT = "DAY_AMOUNT";
	// doanh so thuc hien trong ngay
	public static final String DAY_AMOUNT_APPROVED = "DAY_AMOUNT_APPROVED";
	// diem trong ngay
	public static final String DAY_SCORE = "DAY_SCORE";
	// sku ke hoach thang
	public static final String MONTH_SKU_PLAN = "MONTH_SKU_PLAN";
	// sku thuc hien trong than
	public static final String MONTH_SKU = "MONTH_SKU";
	// doanh so ke hoach thang mat hang trong tam 1
	public static final String FOCUS1_AMOUNT_PLAN = "FOCUS1_AMOUNT_PLAN";
	// doanh so dat dc trong thang mat hang trong tam 1
	public static final String FOCUS1_AMOUNT = "FOCUS1_AMOUNT";
	// doanh so ke hoach thang mat hang trong tam 2
	public static final String FOCUS2_AMOUNT_PLAN = "FOCUS2_AMOUNT_PLAN";
	// doanh so dat dc trong thang mat hang trong tam 2
	public static final String FOCUS2_AMOUNT = "FOCUS2_AMOUNT";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String TYPE = "TYPE";
	public static final String MONTH_AMOUNT_PENDING = "MONTH_AMOUNT_PENDING";
	public static final String DAY_AMOUNT_PENDING = "DAY_AMOUNT_PENDING";
	public static final String DAY_QUANTITY = "DAY_QUANTITY";
	public static final String DAY_QUANTITY_PENDING = "DAY_QUANTITY_PENDING";
	public static final String DAY_QUANTITY_PLAN = "DAY_QUANTITY_PLAN";
	public static final String DAY_QUANTITY_APPROVED = "DAY_QUANTITY_APPROVED";
	public static final String MONTH_QUANTITY = "MONTH_QUANTITY";
	public static final String MONTH_QUANTITY_PENDING = "MONTH_QUANTITY_PENDING";
	public static final String MONTH_QUANTITY_PLAN = "MONTH_QUANTITY_PLAN";
	public static final String MONTH_QUANTITY_APPROVED = "MONTH_QUANTITY_APPROVED";

	private volatile String[] CAT_LIST = new String[] { "A", "B", "C", "D",
			"E", "F" };
	private volatile String[] CAT_NAME_LIST = new String[] { "A", "B", "C", "D",
		"E", "F" };

	public static final String RPT_STAFF_SALE_TABLE = "RPT_STAFF_SALE";

	public RPT_STAFF_SALE_TABLE() {
		this.tableName = RPT_STAFF_SALE_TABLE;
		this.columns = new String[] { STAFF_ID, STAFF_NAME, STAFF_CODE,
				PARENT_STAFF_ID, SHOP_ID, MONTH_AMOUNT_PLAN, MONTH_AMOUNT, MONTH_AMOUNT_APPROVED,
				MONTH_SCORE, DAY_AMOUNT_PLAN, DAY_AMOUNT, DAY_AMOUNT_APPROVED, DAY_SCORE,
				MONTH_SKU_PLAN, MONTH_SKU, FOCUS1_AMOUNT_PLAN, FOCUS1_AMOUNT,
				FOCUS2_AMOUNT_PLAN, FOCUS2_AMOUNT, CREATE_DATE, UPDATE_DATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public RPT_STAFF_SALE_TABLE(SQLiteDatabase mDB) {
		this.tableName = RPT_STAFF_SALE_TABLE;
		this.columns = new String[] { STAFF_ID, STAFF_NAME, STAFF_CODE,
				PARENT_STAFF_ID, SHOP_ID, MONTH_AMOUNT_PLAN, MONTH_AMOUNT, MONTH_AMOUNT_APPROVED,
				MONTH_SCORE, DAY_AMOUNT_PLAN, DAY_AMOUNT, DAY_AMOUNT_APPROVED, DAY_SCORE,
				MONTH_SKU_PLAN, MONTH_SKU, FOCUS1_AMOUNT_PLAN, FOCUS1_AMOUNT,
				FOCUS2_AMOUNT_PLAN, FOCUS2_AMOUNT, CREATE_DATE, UPDATE_DATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	static volatile RPT_STAFF_SALE_TABLE instance;

	public static RPT_STAFF_SALE_TABLE getInstance() {
		if (instance == null) {
			instance = new RPT_STAFF_SALE_TABLE();
		}
		return instance;
	}
	
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 *
	 * lay thong tin bao cao tien do ban mat hang trong tam
	 *
	 * @author: HoanPD1
	 * @param staffIdGS
	 * @return
	 * @return: ProgressReportSalesFocusDTO
	 * @throws:
	 */
	public ProgressReportSalesFocusDTO getProgReportSalesFocus(Bundle data) throws Exception {
		ProgressReportSalesFocusDTO dto = new ProgressReportSalesFocusDTO();
		Cursor cTotal = null;
		Cursor c = null;
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String dateNow= DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		StringBuffer sqlQuery = new StringBuffer();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffID = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> lstShop = shopTB.getShopRecursive(shopId);
		String idShopList = TextUtils.join(",", lstShop);
		String startOfCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		long cycleId = new EXCEPTION_DAY_TABLE(mDB).getCycleId(new Date(), 0);
		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		if(!StringUtil.isNullOrEmpty(idShopListReverse)){
			idShopList = idShopList + "," + idShopListReverse;
		}
		ArrayList<String> params = new ArrayList<String>();
		try {
			sqlQuery.append(" SELECT STAFF_CODE,");
			sqlQuery.append(" 		 STAFF_ID as STAFF_ID,");
			sqlQuery.append(" 		 STAFF_NAME,");
			sqlQuery.append(" 		 SALE_TYPE_CODE,");
			sqlQuery.append(" 		 MOBILEPHONE,");
			sqlQuery.append(" 		 PHONE,");
			sqlQuery.append(" 		 sum(AMOUNT) AMOUNT,");
			sqlQuery.append(" 		 sum(AMOUNT_APPROVED) AMOUNT_APPROVED,");
			sqlQuery.append(" 		 sum(AMOUNT_PENDING) AMOUNT_PENDING,");
			sqlQuery.append(" 		 sum(AMOUNT_PLAN) AMOUNT_PLAN,");
			sqlQuery.append(" 		 sum(QUANTITY) QUANTITY,");
			sqlQuery.append(" 		 sum(QUANTITY_APPROVED) QUANTITY_APPROVED,");
			sqlQuery.append(" 		 sum(QUANTITY_PENDING) QUANTITY_PENDING,");
			sqlQuery.append(" 		 sum(QUANTITY_PLAN) QUANTITY_PLAN,");
			sqlQuery.append("        TYPE_FOCUS_PRODUCT, ");
			sqlQuery.append("        AP_VALUE ");
			sqlQuery.append(" FROM  ");
			sqlQuery.append(" 		(SELECT S.staff_id, S.staff_code STAFF_CODE,");
			sqlQuery.append(" 		 S.staff_name STAFF_NAME,");
			sqlQuery.append(" 		 S.mobilephone MOBILEPHONE,");
			sqlQuery.append(" 		 S.sale_type_code SALE_TYPE_CODE,");
			sqlQuery.append(" 		 S.phone PHONE,");
			sqlQuery.append(" 		 AMOUNT AMOUNT,");
			sqlQuery.append(" 		 AMOUNT_APPROVED AMOUNT_APPROVED,");
			sqlQuery.append(" 		 AMOUNT_PENDING AMOUNT_PENDING,");
			sqlQuery.append(" 		 AMOUNT_PLAN AMOUNT_PLAN,");
			sqlQuery.append(" 		 QUANTITY QUANTITY,");
			sqlQuery.append(" 		 QUANTITY_APPROVED QUANTITY_APPROVED,");
			sqlQuery.append(" 		 QUANTITY_PENDING QUANTITY_PENDING,");
			sqlQuery.append(" 		 QUANTITY_PLAN QUANTITY_PLAN,");
			sqlQuery.append("        FCMP.TYPE TYPE_FOCUS_PRODUCT, ");
			sqlQuery.append("        AP.VALUE AP_VALUE ");
			sqlQuery.append(" 		FROM  rpt_sale_in_month RSM");
			//union voi staff_history de lay nhan vien da nghi/chuyen shop
			sqlQuery.append("       LEFT JOIN (SELECT * FROM (SELECT s.staff_id, ");
			sqlQuery.append("                         s.sale_type_code, ");
			sqlQuery.append("                         s.staff_code, ");
			sqlQuery.append("                         s.staff_name, ");
			sqlQuery.append("                         s.mobilephone MOBILEPHONE, ");
			sqlQuery.append("                         s.phone PHONE , ");
			sqlQuery.append("                         s.shop_id, ");
			sqlQuery.append("                         s.status ");
			sqlQuery.append("                  FROM   staff s WHERE S.STATUS = 1 ");
			sqlQuery.append("                  UNION ");
			sqlQuery.append("                  SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("                         ST.sale_type_code, ");
			sqlQuery.append("                         sh.staff_code, ");
			sqlQuery.append("                         sh.staff_name, ");
			sqlQuery.append("                         ST.mobilephone AS MOBILEPHONE, ");
			sqlQuery.append("                         ST.PHONE AS PHONE, ");
			sqlQuery.append("                         sh.shop_id, ");
			sqlQuery.append("                         sh.status ");
			sqlQuery.append("                  FROM   staff_history sh ");
			sqlQuery.append("                		  JOIN staff st ON ST.STAFF_ID = SH.STAFF_ID ");
			sqlQuery.append("                  WHERE  Date(sh.from_date) <= Date(?) ");
			params.add(dateNow);
//			sqlQuery.append("                  AND  Date(sh.from_date) >= Date('now', 'localtime','start of month' ) ");
			sqlQuery.append("                  AND  Date(sh.from_date) >= Date(?) ");
			params.add(startOfCycle);
			sqlQuery.append("                         AND (Date(sh.to_date) >= Date(?) or sh.to_date is null ) ");
			params.add(startOfCycle);
			sqlQuery.append("                         AND sh.status = 1) GROUP BY STAFF_ID) s ");
			sqlQuery.append("  on RSM.object_id = s.staff_id");
			sqlQuery.append("       ,FOCUS_CHANNEL_MAP_PRODUCT FCMP ");
			sqlQuery.append("	    ,FOCUS_CHANNEL_MAP FCM ");
			sqlQuery.append("       ,FOCUS_SHOP_MAP FSM ");
			sqlQuery.append("       ,FOCUS_PROGRAM FP ");
			sqlQuery.append("       ,PRODUCT P ");
			sqlQuery.append("       ,AP_PARAM AP ");
			sqlQuery.append("       ,PRODUCT_INFO PI ");
			sqlQuery.append("       ,CYCLE CY  ");
			sqlQuery.append(" WHERE RSM.object_id IN (");
			sqlQuery.append(listStaffID);
			sqlQuery.append(" )");
			sqlQuery.append(" and RSM.object_type = ? ");
			params.add(ProgressReportSalesFocusDTO.TYPE_STAFF + "");
			sqlQuery.append(" and RSM.PRODUCT_ID = FCMP.PRODUCT_ID ");
			sqlQuery.append(" and s.status = 1 ");
			sqlQuery.append(" and s.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append(" and RSM.shop_id = ? ");
			params.add(shopId);
//			sqlQuery.append("       AND Strftime('%Y/%m', RSM.month) = ");
//			sqlQuery.append("           Strftime('%Y/%m', ?) ");
//			params.add(startOfCycle);
			sqlQuery.append("       AND RSM.cycle_id = ? ");
			params.add(String.valueOf(cycleId));
			sqlQuery.append("       AND RSM.cycle_id = cy.cycle_id ");
			sqlQuery.append("       AND FP.STATUS = 1 ");
			sqlQuery.append("       AND FP.FOCUS_PROGRAM_ID = FCM.FOCUS_PROGRAM_ID ");
			sqlQuery.append("       AND FCM.SALE_TYPE_CODE = s.sale_type_code ");
			sqlQuery.append("       AND FCM.STATUS = 1 ");
			sqlQuery.append("       AND FP.FOCUS_PROGRAM_ID = FSM.FOCUS_PROGRAM_ID ");
			sqlQuery.append("       AND substr(FP.FROM_DATE, 1, 10) <= substr(?, 1, 10) ");
			params.add(dateNow);
			sqlQuery.append("       AND Ifnull (substr(FP.TO_DATE, 1, 10) >= substr(?, 1, 10), 1) ");
			params.add(dateNow);
			sqlQuery.append("       AND FSM.SHOP_ID IN ( ");
			sqlQuery.append(idShopList + ") ");
			sqlQuery.append("       AND FSM.STATUS = 1 ");
			sqlQuery.append("       AND FCMP.FOCUS_CHANNEL_MAP_ID = FCM.FOCUS_CHANNEL_MAP_ID ");
			sqlQuery.append("       AND P.PRODUCT_ID = FCMP.PRODUCT_ID ");
			sqlQuery.append("       AND P.STATUS = 1 ");
			sqlQuery.append("       AND P.CAT_ID = PI.PRODUCT_INFO_ID ");
			sqlQuery.append("       AND PI.STATUS = 1 ");
			sqlQuery.append("       AND PI.TYPE = 1 ");
			sqlQuery.append("       AND AP.TYPE = 'FOCUS_PRODUCT_TYPE' ");
			sqlQuery.append("       AND AP.AP_PARAM_CODE = FCMP.TYPE) ");
			sqlQuery.append(" GROUP BY TYPE_FOCUS_PRODUCT, STAFF_ID");
			sqlQuery.append(" ORDER BY STAFF_CODE, AP_VALUE");
			
			cTotal = this.rawQuery(sqlQuery.toString(), params.toArray(new String[params.size()]));
			c = this.rawQuery(sqlQuery.toString(), params.toArray(new String[params.size()]));
			if (cTotal.moveToFirst()) {
				do {
					dto.arrMMTTText.add(CursorUtil.getString(cTotal,
							"TYPE_FOCUS_PRODUCT"));
					ReportProductFocusItem fItem = dto
							.newReportProductFocusItem();
					dto.arrRptFocusItemTotal.add(fItem);
				} while (cTotal.moveToNext());
			}
			if (c.moveToFirst()) {
				do {
					InfoProgressEmployeeDTO item = dto
							.newInfoProgressEmployeeDTO();
					int staffIdTemp = CursorUtil.getInt(c,"STAFF_ID");
					boolean isExist = false;
					int index = 0;
					for (InfoProgressEmployeeDTO temp : dto.listProgressSalesStaff) {
						if (temp.staffId == staffIdTemp) {
							isExist = true;
							break;
						}
						index++;
					}
					// truong hop dto chua add nhan vien vao
					if(!isExist)
						dto.addItem(item);
					else{
						// nhan vien da ton tai trong mang dto
						item = dto.listProgressSalesStaff.get(index);
					}
					item.staffId = CursorUtil.getInt(c,"STAFF_ID");
					item.staffCode = CursorUtil.getString(c,"STAFF_CODE");
					item.staffName = CursorUtil.getString(c,"STAFF_NAME");
					item.staffPhone = CursorUtil.getString(c,STAFF_TABLE.PHONE);
					item.staffMobile =  CursorUtil.getString(c,STAFF_TABLE.MOBILE_PHONE);
					item.saleTypeCode =  CursorUtil.getString(c,"SALE_TYPE_CODE");
					ReportProductFocusItem fItem = dto
							.newReportProductFocusItem();
					fItem.amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PLAN");
					fItem.amountDone = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT");
					fItem.amountPending = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_PENDING");
					fItem.amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "AMOUNT_APPROVED");
					fItem.amountPercent = StringUtil.calPercentUsingRound(fItem.amountPlan, fItem.amountDone);
					fItem.quantityPlan = CursorUtil.getLong(c, "QUANTITY_PLAN");
					fItem.quantityDone = CursorUtil.getLong(c, "QUANTITY");
					fItem.quantityPending = CursorUtil.getLong(c, "QUANTITY_PENDING");
					fItem.quantityApproved = CursorUtil.getLong(c, "QUANTITY_APPROVED");
					fItem.quantityPercent = StringUtil.calPercentUsingRound(fItem.quantityPlan, fItem.quantityDone);
					fItem.typeFocusProduct = CursorUtil.getString(c, "TYPE_FOCUS_PRODUCT");
					item.arrRptFocusItem.add(fItem);
					for (int i = 0, size = dto.arrMMTTText.size(); i < size; i++) {
						ReportProductFocusItem fItemTT = dto.arrRptFocusItemTotal
								.get(i);
						// neu cung loai MHTT thi moi add vao dong tong
						if (fItem.typeFocusProduct.equals(dto.arrMMTTText
								.get(i))) {
							fItemTT.amountPlan += fItem.amountPlan;
							fItemTT.amountDone += fItem.amountDone;
							fItemTT.amountPending += fItem.amountPending;
							fItemTT.amountApproved += fItem.amountApproved;
							fItemTT.amountPercent = StringUtil
									.calPercentUsingRound(fItemTT.amountPlan,
											fItemTT.amountDone);
							fItemTT.quantityPlan += fItem.quantityPlan;
							fItemTT.quantityDone += fItem.quantityDone;
							fItemTT.quantityPending += fItem.quantityPending;
							fItemTT.quantityApproved += fItem.quantityApproved;
							fItemTT.quantityPercent = StringUtil
									.calPercentUsingRound(fItemTT.quantityPlan,
											fItemTT.quantityDone);
						}

					}

				} while (c.moveToNext());
			}
			dto.planDay = new EXCEPTION_DAY_TABLE(mDB)
					.getPlanWorkingDaysOfMonth(new Date(), shopId);
			dto.pastDay = new EXCEPTION_DAY_TABLE(mDB)
					.getCurrentWorkingDaysOfMonth(shopId);
			dto.progress = StringUtil.calPercentUsingRound(dto.planDay,
					dto.pastDay);
			// return dto;
		} finally {
			try {
				if (cTotal != null) {
					cTotal.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * get DTO for AccSaleProgReport
	 *
	 * @param staffId
	 * @return
	 * @throws Exception
	 */
	public AccSaleProgReportDTO getAccSaleProgReportDTO(int staffId, String shopId, int sysSaleRoute) throws Exception {
		AccSaleProgReportDTO dto = new AccSaleProgReportDTO();
		Cursor c = null;
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffID = staff.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		String dateNow = DateUtils.now();
		String startOfCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		try {
			StringBuffer sqlQuery = new StringBuffer();
			List<String> params = new ArrayList<String>();
			sqlQuery.append("SELECT st.OBJECT_ID          OBJECT_ID, ");
			sqlQuery.append("       st.OBJECT_CODE        OBJECT_CODE, ");
			sqlQuery.append("       st.OBJECT_NAME        OBJECT_NAME, ");
//			sqlQuery.append("       s.mobilephone                MOBILEPHONE, ");
			sqlQuery.append("       rpt_staff_sale.month_amount_plan MONTH_AMOUNT_PLAN, ");
			sqlQuery.append("       rpt_staff_sale.month_quantity_plan MONTH_QUANTITY_PLAN, ");
			sqlQuery.append("       cy.num NUM_CYCLE, ");
			sqlQuery.append("       cy.BEGIN_DATE BEGIN_DATE, ");
			sqlQuery.append("       cy.END_DATE END_DATE, ");
			if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED) {
				sqlQuery.append("       rpt_staff_sale.month_amount      MONTH_AMOUNT, ");
				sqlQuery.append("       rpt_staff_sale.month_quantity      MONTH_QUANTITY, ");
			} else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED) {
				sqlQuery.append("       rpt_staff_sale.month_amount_approved      MONTH_AMOUNT, ");
				sqlQuery.append("       rpt_staff_sale.month_quantity_approved      MONTH_QUANTITY, ");
			}
			sqlQuery.append("       rpt_staff_sale.month_amount_approved    DUYET, ");
			sqlQuery.append("       rpt_staff_sale.month_quantity_approved    DUYET_SL ");


//			sqlQuery.append("       ,rpt_staff_sale.month_sku_plan    MONTH_SKU_PLAN, ");
//			sqlQuery.append("       rpt_staff_sale.month_sku         MONTH_SKU, ");
//			sqlQuery.append("       rpt_staff_sale.month_score       MONTH_SCORE ");
			sqlQuery.append("FROM   rpt_staff_sale ");
			sqlQuery.append("       JOIN CYCLE CY ON rpt_staff_sale.cycle_id = CY.cycle_id ");
			//union voi staff_history de lay nhan vien da nghi/chuyen shop
			sqlQuery.append("       JOIN ");
			if (sysSaleRoute == ApParamDTO.SYS_SALE_ROUTE) {
				sqlQuery.append("       (SELECT routing_id       AS OBJECT_ID,  ");
				sqlQuery.append("               routing_code     AS OBJECT_CODE,  ");
				sqlQuery.append("               routing_name     AS OBJECT_NAME  ");
				sqlQuery.append("        FROM ROUTING  ");
				sqlQuery.append("        WHERE 1=1  ");
				sqlQuery.append("              and status =1  ");
				sqlQuery.append("              and shop_id = ? ");
				params.add(shopId);
			} else if (sysSaleRoute == ApParamDTO.SYS_SALE_STAFF) {
				sqlQuery.append("     (SELECT * ");
				sqlQuery.append("      FROM(SELECT s.staff_id            AS OBJECT_ID, ");
				sqlQuery.append("                  s.staff_code			 AS OBJECT_CODE, ");
				sqlQuery.append("                  s.staff_name			 AS OBJECT_NAME, ");
				sqlQuery.append("                  s.shop_id             AS SHOP_ID ");
				sqlQuery.append("           FROM   staff s, ");
				sqlQuery.append("                  staff_type st ");
				sqlQuery.append("           WHERE  1=1 ");
//				sqlQuery.append("                  AND st.staff_type_id = s.staff_type_id ");
//				sqlQuery.append("                  AND s.status = 1 ");
				sqlQuery.append("                  AND st.status = 1 ");
				sqlQuery.append("                  AND s.staff_id in ("
						+ listStaffID + " ) ");
				sqlQuery.append("                  AND s.staff_id <> ? ");
				params.add("" + staffId);
				sqlQuery.append("                  AND s.shop_id = ? ");
				params.add(shopId);
//				sqlQuery.append("                  AND st.SPECIFIC_TYPE = ? ");
//				params.add(staffType);
				sqlQuery.append("          UNION ");
				sqlQuery.append("           SELECT sh.staff_id AS staff_id, ");
				sqlQuery.append("                  sh.staff_code, ");
				sqlQuery.append("                  sh.staff_name, ");
				sqlQuery.append("                  sh.shop_id ");
				sqlQuery.append("           FROM staff_history sh ");
				sqlQuery.append("           WHERE 1=1 ");
				sqlQuery.append("                 AND sh.staff_id in ("
						+ listStaffID + " ) ");
				sqlQuery.append("                 AND sh.shop_id = ? ");
				params.add(shopId);
				sqlQuery.append("                 AND sh.staff_id <> ? ");
				params.add("" + staffId);
				sqlQuery.append("                 AND sh.status =1 ");
				sqlQuery.append("                 AND substr(sh.from_date, 1, 10) <= ? ");
				params.add(dateNow);
				sqlQuery.append("                 AND substr(sh.from_date,1,10) >= substr(?,1,10) ");
				params.add(startOfCycle);
				sqlQuery.append("                 AND (substr(sh.to_date,1,10) >= substr(?,1,10) ");
				params.add(startOfCycle);
				sqlQuery.append("                      OR sh.to_date is null) ) ");
			} else if (sysSaleRoute == ApParamDTO.SYS_SALE_SHOP) {
				sqlQuery.append("       (SELECT shop_id       AS OBJECT_ID,  ");
				sqlQuery.append("               shop_code     AS OBJECT_CODE,  ");
				sqlQuery.append("               shop_name     AS OBJECT_NAME  ");
				sqlQuery.append("        FROM SHOP  ");
				sqlQuery.append("        WHERE 1=1  ");
				sqlQuery.append("              and status =1  ");
				sqlQuery.append("              and shop_id = ? ");
				params.add(shopId);
			}
			sqlQuery.append("     GROUP BY OBJECT_ID) st ");
			sqlQuery.append("              ON rpt_staff_sale.object_id = st.OBJECT_ID ");

//			sqlQuery.append("       LEFT JOIN (SELECT * FROM (SELECT s.staff_id, ");
//			sqlQuery.append("                         s.staff_code, ");
//			sqlQuery.append("                         s.staff_name, ");
//			sqlQuery.append("                         s.mobilephone, ");
//			sqlQuery.append("                         s.shop_id ");
//			sqlQuery.append("                  FROM   staff s WHERE S.STATUS = 1 ");
//			sqlQuery.append("                  and s.staff_id in ( ");
//			sqlQuery.append(listStaffID);
//			sqlQuery.append("					) ");
//			sqlQuery.append("                  UNION ");
//			sqlQuery.append("                  SELECT sh.staff_id AS staff_id, ");
//			sqlQuery.append("                         sh.staff_code, ");
//			sqlQuery.append("                         sh.staff_name, ");
//			sqlQuery.append("                         st.mobilephone as mobilephone, ");
//			sqlQuery.append("                         sh.shop_id ");
//			sqlQuery.append("                  FROM   staff_history sh ");
//			sqlQuery.append("                  		JOIN staff st ON ST.STAFF_ID = SH.STAFF_ID ");
//			sqlQuery.append("                  WHERE  substr(sh.from_date, 1, 10) <= substr(?, 1, 10) ");
//			params.add(dateNow);
//			sqlQuery.append("                  and sh.staff_id in ( ");
//			sqlQuery.append(listStaffID);
//			sqlQuery.append("					) ");
//			sqlQuery.append("                  AND  Date(sh.from_date) >= Date(?) ");
//			params.add(startOfCycle);
//			sqlQuery.append("                         AND (Date(sh.to_date) >= Date(?) ");
//			params.add(startOfCycle);
//			sqlQuery.append("                                                or sh.to_date is null ) ");
//			sqlQuery.append("                         AND sh.status = 1) GROUP BY STAFF_ID) s ");
//			sqlQuery.append("              ON rpt_staff_sale.object_id = s.staff_id ");
//			sqlQuery.append("WHERE  rpt_staff_sale.staff_id = ? ");
//			params.add(String.valueOf(staffId));
			//sqlQuery.append("WHERE  s.staff_id IN ( " + PriUtils.getInstance().getListStaffOfSupervisor(String.valueOf(staffId)) +" ) ");
			// sqlQuery.append("       AND s.status > 0 ");
//			sqlQuery.append(" WHERE s.shop_id = ? ");
//			params.add(shopId);
			sqlQuery.append("  WHERE  1 = 1    AND rpt_staff_sale.shop_id = ? ");
			params.add(shopId);
//			sqlQuery.append("       AND rpt_staff_sale.object_id = ? ");
//			params.add(String.valueOf(staffId));
//			sqlQuery.append("       AND rpt_staff_sale.object_id = s.staff_id ");
			sqlQuery.append("       AND rpt_staff_sale.object_type = ? ");
			params.add("" + sysSaleRoute);
//			sqlQuery.append("       AND substr(rpt_staff_sale.SALE_DATE, 1, 7) = ");
//			sqlQuery.append("           substr(?, 1, 7) ");
//			params.add(dateNow);
			sqlQuery.append("       AND Date(CY.BEGIN_DATE) <= Date(?) ");
			params.add(dateNow);
			sqlQuery.append("       AND Date(CY.END_DATE) >= Date(?) ");
			params.add(dateNow);
			sqlQuery.append("       AND CY.STATUS = 1 ");
			sqlQuery.append("ORDER  BY st.OBJECT_NAME ");

			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));

			if (c.moveToFirst()) {
				do {
					AccSaleProgReportItem item = dto.newAccSaleProgReportItem();
					item.parseDataFromCursor(c);
					dto.addItem(item);
				} while (c.moveToNext());
			}
			EXCEPTION_DAY_TABLE edt = new EXCEPTION_DAY_TABLE(mDB);
			dto.monthSalePlan = edt.getPlanWorkingDaysOfMonth(new Date(), shopId);
			dto.soldSalePlan = edt.getCurrentWorkingDaysOfMonth(shopId);
			dto.perSalePlan = StringUtil.calPercentUsingRound(dto.monthSalePlan,dto.soldSalePlan);
			dto.beginDate = edt.getFirstDayOfOffsetCycle(new Date(),0);
			dto.endDate = edt.getEndDayOfOffsetCycle(new Date(),0);
			dto.numCycle = edt.getNumCycle(new Date(),0);
			// return dto;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	public double getMinScoreNotification() {
		StringBuffer sqlQuery = new StringBuffer();
		double minScoreNotification = 2.5;
		sqlQuery.append("select  ap.ap_param_code as ap_param_code from ap_param ap where type like 'MIN_SCORE_NOTIFICATION' and status = 1");
		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(), null);

			if (c != null && c.moveToFirst()) {
				do {
					minScoreNotification = CursorUtil.getDouble(c, "ap_param_code");
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return minScoreNotification;
	}

	/**
	 * Lay danh sach bao cao ngay cho GS
	 * @author: hoanpd1
	 * @since: 10:01:28 28-03-2015
	 * @return: ProgressDateReportDTO
	 * @throws:
	 * @param bundle
	 * @return
	 * @throws Exception
	 */
	public ProgressDateReportDTO getProgressDateReport(Bundle bundle)
			throws Exception {
		ProgressDateReportDTO dto = new ProgressDateReportDTO();
		int sysCurrencyDivide = bundle.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
		int sysCalUnApproved = bundle.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
//		String objectType = bundle.getString(IntentConstants.INTENT_OBJECT_TYPE);
		int sysSaleRoute = bundle.getInt(IntentConstants.INTENT_SYS_SALE_ROUTE);
		String staffOwnerId = bundle.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
//		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_MONTH_YEAR);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		DMSSortInfo sortInfo = (DMSSortInfo) bundle.getSerializable(IntentConstants.INTENT_SORT_DATA);

//		String staffOwnerId = data.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
//		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
//		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffType = bundle.getString(IntentConstants.INTENT_STAFF_TYPE);
//		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
//		int sysSaleRoute = data.getInt(IntentConstants.INTENT_SYS_SALE_ROUTE);
//		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
//		STAFF_TABLE staff = new STAFF_TABLE(mDB);
//		String listStaff = staff.getListStaffOfSupervisor(staffId, shopId);

		dto.minScoreNotification = getMinScoreNotification();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(" SELECT st.OBJECT_ID                        AS OBJECT_ID, ");
		sqlQuery.append("        st.OBJECT_CODE                      AS OBJECT_CODE, ");
		sqlQuery.append("        st.OBJECT_NAME                      AS OBJECT_NAME, ");
		sqlQuery.append("        rss.day_cust_order_plan           AS DAY_CUST_ORDER_PLAN, ");
		sqlQuery.append("        rss.day_cust_order                AS DAY_CUST_ORDER, ");
		sqlQuery.append("        rss.day_amount_plan               AS DAY_AMOUNT_PLAN, ");
		sqlQuery.append("        rss.day_amount_approved           AS DAY_AMOUNT_APPROVED, ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlQuery.append(" 	 rss.day_amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sqlQuery.append(" 	 rss.day_amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlQuery.append(" 	 rss.day_amount ");
		}else {
			sqlQuery.append(" 	 rss.day_amount ");
		}
		sqlQuery.append("                                          AS DAY_AMOUNT, ");
		sqlQuery.append("        rss.day_quantity_plan             AS DAY_QUANTITY_PLAN, ");
		sqlQuery.append("        rss.day_quantity_approved         AS DAY_QUANTITY_APPROVED, ");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sqlQuery.append(" 	 rss.day_quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sqlQuery.append(" 	 rss.day_quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlQuery.append(" 	 rss.day_quantity ");
		}else {
			sqlQuery.append(" 	 rss.day_quantity ");
		}
		sqlQuery.append("                                          AS DAY_QUANTITY ");
		sqlQuery.append(" FROM rpt_staff_sale rss, ");
		if (sysSaleRoute == ApParamDTO.SYS_SALE_ROUTE) {
			sqlQuery.append("       (SELECT routing_id       AS OBJECT_ID,  ");
			sqlQuery.append("               routing_code     AS OBJECT_CODE,  ");
			sqlQuery.append("               routing_name     AS OBJECT_NAME  ");
			sqlQuery.append("        FROM ROUTING  ");
			sqlQuery.append("        WHERE 1=1  ");
			sqlQuery.append("              and status =1  ");
			sqlQuery.append("              and shop_id = ? ");
			params.add(shopId);
		} else if (sysSaleRoute == ApParamDTO.SYS_SALE_STAFF) {
			sqlQuery.append("     (SELECT * ");
			sqlQuery.append("      FROM(SELECT s.staff_id            AS OBJECT_ID, ");
			sqlQuery.append("                  s.staff_code			 AS OBJECT_CODE, ");
			sqlQuery.append("                  s.staff_name			 AS OBJECT_NAME, ");
			sqlQuery.append("                  s.shop_id             AS SHOP_ID ");
			sqlQuery.append("           FROM   staff s, ");
			sqlQuery.append("                  staff_type st ");
			sqlQuery.append("           WHERE  1=1 ");
			sqlQuery.append("                  AND st.staff_type_id = s.staff_type_id ");
//			sqlQuery.append("                  AND s.status = 1 ");
			sqlQuery.append("                  AND st.status = 1 ");
			sqlQuery.append("                  AND s.staff_id in ("
					+ listStaffId + " ) ");
			sqlQuery.append("                  AND s.staff_id <> ? ");
			params.add(staffOwnerId);
			sqlQuery.append("                  AND s.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("                  AND st.SPECIFIC_TYPE = ? ");
			params.add(staffType);
			sqlQuery.append("          UNION ");
			sqlQuery.append("           SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("                  sh.staff_code, ");
			sqlQuery.append("                  sh.staff_name, ");
			sqlQuery.append("                  sh.shop_id ");
			sqlQuery.append("           FROM staff_history sh ");
			sqlQuery.append("           WHERE 1=1 ");
			sqlQuery.append("                 AND sh.staff_id in ("
					+ listStaffId + " ) ");
			sqlQuery.append("                 AND sh.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("                 AND sh.staff_id <> ? ");
			params.add(staffOwnerId);
			sqlQuery.append("                 AND sh.status =1 ");
			sqlQuery.append("                 AND substr(sh.from_date, 1, 10) <= ? ");
			params.add(date_now);
			sqlQuery.append("                 AND substr(sh.from_date,1,10) >= substr(?,1,10) ");
			params.add(startOfCycle);
			sqlQuery.append("                 AND (substr(sh.to_date,1,10) >= substr(?,1,10) ");
			params.add(startOfCycle);
			sqlQuery.append("                      OR sh.to_date is null) ) ");
		} else if (sysSaleRoute == ApParamDTO.SYS_SALE_SHOP) {
			sqlQuery.append("       (SELECT shop_id       AS OBJECT_ID,  ");
			sqlQuery.append("               shop_code     AS OBJECT_CODE,  ");
			sqlQuery.append("               shop_name     AS OBJECT_NAME  ");
			sqlQuery.append("        FROM SHOP  ");
			sqlQuery.append("        WHERE 1=1  ");
			sqlQuery.append("              and status =1  ");
			sqlQuery.append("              and shop_id = ? ");
			params.add(shopId);
		}
		sqlQuery.append("     GROUP BY OBJECT_ID) st ");
//		sqlQuery.append("   JOIN staff s ON rss.OBJECT_ID = s.staff_id ");
		sqlQuery.append(" WHERE 1=1 ");
//		sqlQuery.append("       AND s.status = 1 ");
		sqlQuery.append("   	AND rss.object_id = st.object_id ");
		sqlQuery.append("     	AND rss.object_type = ? ");
		params.add("" + sysSaleRoute);
		//sqlQuery.append("     	AND DAY_AMOUNT > 0 ");
		sqlQuery.append("     	AND rss.shop_id = ? ");
		params.add("" + shopId);
//		sqlQuery.append("     	AND rss.OBJECT_ID in ( " + listStaffId + " ) ");
//		sqlQuery.append("     	AND substr(rss.sale_date,1,7) =  substr(?, 1,7) ");
		sqlQuery.append("     	AND substr(rss.sale_date, 1, 10) =  ? ");
		params.add(date_now);
		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   ORDER BY st.object_code, st.object_name ";

		String orderByStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "object_code")
				.addMapper(SortActionConstants.NAME, "object_name")
				.defaultOrderString(defaultOrderByStr)
				.build(sortInfo);
				//add order string
		sqlQuery.append(orderByStr);
		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c, sysCurrencyDivide);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * HieuNH Lay danh sach bao cao ngay cua NVGS
	 *
	 * @param staffOwnerId
	 *            : id cua NVGS
	 * @return
	 * @throws Exception
	 */
	public ProgressDateDetailReportDTO getProgressDateDetailReport(Bundle bundle) throws Exception {
		ProgressDateDetailReportDTO dto = new ProgressDateDetailReportDTO();
//		StringBuffer sqlQuery = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		int sysSaleRoute = bundle.getInt(IntentConstants.INTENT_SYS_SALE_ROUTE);
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		int sysCurrencyDivide = bundle.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
		int sysCalUnApproved = bundle.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		String from = bundle.getString(IntentConstants.INTENT_FROM);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		
		Class<?> clazz = Class.forName(from);
		Object fromObject = clazz.newInstance();
		
		StringBuffer  sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT C.customer_id                  AS CUSTOMER_ID, ");
		sqlQuery.append("       C.short_code       			   AS CUSTOMER_CODE, ");
		sqlQuery.append("       C.customer_name                AS CUSTOMER_NAME, ");
		sqlQuery.append("       ifnull(C.ADDRESS, ");
		sqlQuery.append("              ( ifnull(C.HOUSENUMBER,'') ");
		sqlQuery.append("              || ' ' || ifnull(C.STREET,'') ) )       AS CUSTOMER_ADDRESS, ");
//		if (fromObject instanceof ReportProgressDateView) {
//			sqlQuery.append("       rsrd.DAY_AMOUNT_PLAN               AS AMOUNT_PLAN, ");
//			sqlQuery.append("       rsrd.DAY_AMOUNT_APPROVED           AS AMOUNT_APPROVED, ");
//			if (sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED) {
//				sqlQuery.append(" 	 rsrd.DAY_AMOUNT_APPROVED ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
//				sqlQuery.append(" 	 rsrd.DAY_AMOUNT_PENDING ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
//				sqlQuery.append(" 	 rsrd.DAY_AMOUNT ");
//			} else {
//				sqlQuery.append(" 	 rsrd.DAY_AMOUNT ");
//			}
//			sqlQuery.append("                                      AS AMOUNT, ");
//			sqlQuery.append("       rsrd.DAY_QUANTITY_PLAN             AS QUANTITY_PLAN, ");
//			sqlQuery.append("       rsrd.DAY_QUANTITY_APPROVED         AS QUANTITY_APPROVED, ");
//			if (sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED) {
//				sqlQuery.append(" 	 rsrd.DAY_QUANTITY_APPROVED ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
//				sqlQuery.append(" 	 rsrd.DAY_QUANTITY_PENDING ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
//				sqlQuery.append(" 	 rsrd.DAY_QUANTITY ");
//			} else {
//				sqlQuery.append(" 	 rsrd.DAY_QUANTITY ");
//			}
//			sqlQuery.append("       			                   AS QUANTITY, ");
//		} else if (fromObject instanceof AccSaleProgReportView) {
//			sqlQuery.append("       rsrd.MONTH_AMOUNT_PLAN               AS AMOUNT_PLAN, ");
//			sqlQuery.append("       rsrd.MONTH_AMOUNT_APPROVED           AS AMOUNT_APPROVED, ");
//			if (sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED) {
//				sqlQuery.append(" 	 rsrd.MONTH_AMOUNT_APPROVED ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
//				sqlQuery.append(" 	 rsrd.MONTH_AMOUNT_PENDING ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
//				sqlQuery.append(" 	 rsrd.MONTH_AMOUNT ");
//			} else {
//				sqlQuery.append(" 	 rsrd.MONTH_AMOUNT ");
//			}
//			sqlQuery.append("                                      AS AMOUNT, ");
//			sqlQuery.append("       rsrd.MONTH_QUANTITY_PLAN             AS QUANTITY_PLAN, ");
//			sqlQuery.append("       rsrd.MONTH_QUANTITY_APPROVED         AS QUANTITY_APPROVED, ");
//			if (sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED) {
//				sqlQuery.append(" 	 rsrd.MONTH_QUANTITY_APPROVED ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
//				sqlQuery.append(" 	 rsrd.MONTH_QUANTITY_PENDING ");
//			} else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
//				sqlQuery.append(" 	 rsrd.MONTH_QUANTITY ");
//			} else {
//				sqlQuery.append(" 	 rsrd.MONTH_QUANTITY ");
//			}
//			sqlQuery.append("       			                   AS QUANTITY, ");
//		}
		sqlQuery.append("       RSRD.is_or                     AS IS_NT ");
		sqlQuery.append("FROM  rpt_staff_sale_detail RSRD, ");
		sqlQuery.append("      customer C ");
		sqlQuery.append("      JOIN CYCLE CY ON RSRD.cycle_id = CY.cycle_id ");
		sqlQuery.append("WHERE RSRD.customer_id = C.customer_id ");
		//sqlQuery.append("      AND AMOUNT > 0 ");
		sqlQuery.append("      AND RSRD.object_id = ? ");
		params.add(staffId);
		sqlQuery.append("      AND RSRD.object_type = ? ");
		params.add("" + sysSaleRoute);
		sqlQuery.append("      AND RSRD.shop_id = ? ");
		params.add(shopId);
		sqlQuery.append("       AND CY.STATUS = 1 ");
//		if(fromObject instanceof AccSaleProgReportView) {
////			sqlQuery.append("      AND substr(RSRD.sale_date, 1, 7) = substr(?, 1, 7) ");
////			params.add(dateNow);
//			sqlQuery.append("       AND Date(CY.BEGIN_DATE) <= Date(?) ");
//			params.add(dateNow);
//			sqlQuery.append("       AND Date(CY.END_DATE) >= Date(?) ");
//			params.add(dateNow);
//		} else if (fromObject instanceof ReportProgressDateView) {
//			sqlQuery.append("      AND substr(RSRD.sale_date, 1, 10) = substr(?, 1, 10) ");
//			params.add(dateNow);
//		}
		sqlQuery.append("ORDER BY RSRD.is_or, ");
		sqlQuery.append("         C.short_code, ");
		sqlQuery.append("         c.customer_name");


		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(),params);
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c,sysCurrencyDivide);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		dto.beginDate = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(),0);
		dto.endDate = new EXCEPTION_DAY_TABLE(mDB).getEndDayOfOffsetCycle(new Date(),0);
		dto.numCycle = new EXCEPTION_DAY_TABLE(mDB).getNumCycle(new Date(),0);
		return dto;
	}

	/**
	 * HieuNH Lay danh sach bao cao ngay cua TBHV
	 *
	 * @param staffOwnerId
	 *            : id cua TBHV
	 * @return
	 * @throws Exception
	 */
	public TBHVProgressDateReportDTO getTBHVProgressDateDetailReport(
			String shopId, String staffOwnerId,int sysCurrencyDevide) throws Exception {
		// SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		// ArrayList<String> listShopId = shopTB.getShopRecursive(shopId);
		// String strListShop = TextUtils.join(",", listShopId);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		TBHVProgressDateReportDTO dto = new TBHVProgressDateReportDTO();
		StringBuffer sqlQuery = new StringBuffer();
		List<String> params = new ArrayList<String>();
		String[] lstGS = staffOwnerId.split(",");
		for (String itemGS : lstGS) {
			sqlQuery.delete(0, sqlQuery.length());
			params.clear();
			sqlQuery.append("SELECT rss.day_amount_plan      DAY_AMOUNT_PLAN, ");
			sqlQuery.append("       rss.day_amount           DAY_AMOUNT, ");
			sqlQuery.append("       rss.day_amount_approved  DAY_AMOUNT_APPROVED, ");
			sqlQuery.append("       rss.day_amount_pending   DAY_AMOUNT_PENDING, ");
			sqlQuery.append("       rss.day_quantity_plan  DAY_QUANTITY_PLAN, ");
			sqlQuery.append("       rss.day_quantity       DAY_QUANTITY, ");
			sqlQuery.append("       rss.day_quantity_approved       DAY_QUANTITY_APPROVED, ");
			sqlQuery.append("       rss.day_quantity_approved       DAY_QUANTITY_APPROVED, ");
			sqlQuery.append("       stNVBH.staff_name       TEN_NV, ");
			sqlQuery.append("       stGSNPP.staff_name      TEN_GSNPP ");
			// sqlQuery.append("       rss.day_sku_plan    SKU_KE_HOACH, ");
			// sqlQuery.append("       rss.day_sku         SKU_THUC_HIEN ");
			sqlQuery.append("FROM   rpt_staff_sale rss, ");
			sqlQuery.append("       staff stNVBH,");
			sqlQuery.append("       staff stGSNPP ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND stGSNPP.staff_id = ? ");
			params.add(itemGS);
			sqlQuery.append("       AND rss.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("       AND rss.object_type = ? ");
			params.add(ApParamDTO.SYS_SALE_STAFF + "");
			sqlQuery.append("       AND stNVBH.staff_id = rss.object_id ");
			sqlQuery.append("       AND stNVBH.status = 1 ");
			sqlQuery.append("       AND substr(rss.sale_date,1,10) = ? ");
			params.add(date_now);
			if (!StringUtil.isNullOrEmpty(itemGS)) {
				STAFF_TABLE staff = new STAFF_TABLE(mDB);
				String listStaff = staff.getListStaffOfSupervisor(itemGS,
						shopId);
				sqlQuery.append(" AND stNVBH.staff_id IN ( ");
				sqlQuery.append(listStaff);
				sqlQuery.append(" ) ");
			}
			sqlQuery.append("ORDER  BY TEN_NV, ");
			sqlQuery.append("          TEN_GSNPP ");

			Cursor c = null;
			try {
				c = this.rawQuery(sqlQuery.toString(),
						params.toArray(new String[params.size()]));

				if (c != null && c.moveToFirst()) {
					do {
						dto.addItem(c,sysCurrencyDevide);
					} while (c.moveToNext());
				}
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return dto;
	}

	/**
	 * HieuNH Lay bao ca ngay cua TBHV
	 *
	 * @param inheritShopId
	 *            : id cua TBHV
	 * @return
	 */
	public TBHVProgressDateReportDTO getTBHVProgressDateReport(Bundle bundle) throws Exception {
		TBHVProgressDateReportDTO dto = new TBHVProgressDateReportDTO();

		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		int sysCurrencyDevide = bundle.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
		// lay ds nhung nhan vien o duoi staffId truyen vao mot cap
		STAFF_TABLE staffTB = new STAFF_TABLE(this.mDB);
		String[] lstStaff = staffId.split(",");
		StringBuffer listGs = new StringBuffer();
		for (int i = 0, size = lstStaff.length; i < size; i++) {
			if (i == 0)
				listGs.append(staffTB.getListStaffManagerOfChildShop(
						lstStaff[i], shopId));
			else {
				listGs.append(",");
				listGs.append(staffTB.getListStaffManagerOfChildShop(
						lstStaff[i], shopId));
			}
		}
		// lay ds shop con duoi mot cap
		ArrayList<String> listShopChild = staffTB.getListShopChild(shopId);
		String strListShop = TextUtils.join(",", listShopChild);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		StringBuffer sqlQuery = new StringBuffer();
		List<String> params = new ArrayList<String>();
		// params.add("" + shopId);

		sqlQuery.append("SELECT sh.shop_name             TEN_NPP, ");
		sqlQuery.append("       sh.shop_code             MA_NPP, ");
		sqlQuery.append("       sh.shop_id             	 ID_NPP, ");
		sqlQuery.append("       (Select st.specific_type from shop_type st where sh.shop_type_id = st.shop_type_id) SHOP_SPECIFIC_TYPE, ");
		sqlQuery.append("       st1.staff_name           TEN_GSNPP, ");
		sqlQuery.append("       st1.staff_code           MA_GSNPP, ");
		sqlQuery.append("       st1.staff_id             ID_GSNPP, ");
		sqlQuery.append("       rss.day_amount_plan      DAY_AMOUNT_PLAN, ");
		sqlQuery.append("       rss.day_amount           DAY_AMOUNT, ");
		sqlQuery.append("       rss.day_amount_approved  DAY_AMOUNT_APPROVED, ");
		sqlQuery.append("       rss.day_amount_pending   DAY_AMOUNT_PENDING, ");
		sqlQuery.append("       rss.day_quantity_plan  DAY_QUANTITY_PLAN, ");
		sqlQuery.append("       rss.day_quantity       DAY_QUANTITY, ");
		sqlQuery.append("       rss.day_quantity_approved       DAY_QUANTITY_APPROVED, ");
		sqlQuery.append("       rss.day_quantity_pending       DAY_QUANTITY_PENDING ");
//		sqlQuery.append("       Sum(rss.day_sku_plan)    SKU_KE_HOACH_SUM, ");
//		sqlQuery.append("       Count(st.staff_id)       SKU_KE_HOACH_COUNT, ");
//		sqlQuery.append("       Sum(rss.day_sku)         SKU_THUC_HIEN_AMOUNT, ");
//		sqlQuery.append("       Count(rss.PARENT_STAFF_ID) SKU_THUC_HIEN_COUNT ");
		sqlQuery.append("FROM   rpt_staff_sale rss, ");
		sqlQuery.append("       shop sh, ");
		sqlQuery.append("       staff st1, ");
		sqlQuery.append("       org_temp OT ");
		sqlQuery.append("WHERE  rss.shop_id = sh.shop_id ");
		sqlQuery.append("       and rss.object_id = sh.shop_id  ");
		sqlQuery.append("       and ot.shop_id = sh.shop_id  ");
		sqlQuery.append("       and rss.object_type = ? ");
		params.add(ApParamDTO.SYS_SALE_SHOP + "");
		sqlQuery.append("       and ot.staff_id = st1.staff_id  ");
		sqlQuery.append("       AND sh.shop_id IN ( ");
		sqlQuery.append(strListShop + " ) ");
		sqlQuery.append("       AND st1.staff_id IN ( ");
		sqlQuery.append(listGs.toString());
		sqlQuery.append("       )");
		sqlQuery.append("       AND st1.status = 1 ");
		sqlQuery.append("       AND sh.status = 1 ");
		sqlQuery.append("       AND substr(rss.sale_date,1,10) = ? ");
		params.add(date_now);
		sqlQuery.append("GROUP  BY sh.shop_name, ");
		sqlQuery.append("          st1.staff_name ");
		sqlQuery.append("ORDER  BY ma_npp, ");
		sqlQuery.append("          ten_gsnpp ");
		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));

			if (c != null && c.moveToFirst()) {
				do {
					boolean isExist = false;
					String idNPP = CursorUtil.getString(c, "ID_NPP");
					String tenGSNPP = CursorUtil.getString(c, "TEN_GSNPP");
					String idGSNPP = CursorUtil.getString(c, "ID_GSNPP");
					String maGSNPP = CursorUtil.getString(c, "MA_GSNPP");
					for(int i = 0, size = dto.arrList.size(); i < size; i++){
						TBHVProgressDateReportItem item = dto.arrList.get(i);
						if(idNPP.equals(item.idNPP) && !idGSNPP.equals(item.idGSNPP)){
							item.tenGSNPP = item.tenGSNPP + "\n" + tenGSNPP;
							item.maGSNPP = item.maGSNPP + "," + maGSNPP;
							item.idGSNPP = item.idGSNPP + "," + idGSNPP;
							isExist = true;
						}
					}
					if(!isExist)
						dto.addItem(c, sysCurrencyDevide);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * HieuNH Lay danh sach not PSDS thuoc TBHV
	 *
	 * @param staffOwnerId
	 *            : id cua TBHV
	 * @return
	 */
	public TBHVCustomerNotPSDSReportDTO getTBHVNotPSDSReport(String shopId) throws Exception {
		TBHVCustomerNotPSDSReportDTO dto = new TBHVCustomerNotPSDSReportDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = DateUtils.getFirstDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_FORMAT_DATE, 0);

		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursiveReverse(shopId);
		String strListShop = TextUtils.join(",", listShopId);
		String staffIdASM = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";
		String listGs = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("select * from (SELECT GS.staff_id                     AS STAFF_ID, ");
		sqlQuery.append("       GS.staff_code                   AS STAFF_CODE, ");
		sqlQuery.append("       GS.staff_name                   AS TEN_GSNPP, ");
		sqlQuery.append("       SH.shop_id                      AS ID_NPP, ");
		sqlQuery.append("       GS.staff_id STAFF_OWNER_ID            , ");
		sqlQuery.append("       SH.shop_code                    AS MA_NPP, ");
		sqlQuery.append("       SH.shop_name                    AS TEN_NPP, ");
		sqlQuery.append("       Count(NVBH.staff_id)            AS NUM_NVBH, ");
		sqlQuery.append("       (SELECT Sum(sodiemban) ");
		sqlQuery.append("        FROM   (SELECT s.staff_id, ");
		sqlQuery.append("                       Count(DISTINCT( a.customer_id ))SoDiemBan ");
		sqlQuery.append("                FROM   (SELECT * ");
		sqlQuery.append("                        FROM   routing_customer ");
		sqlQuery.append("                        WHERE  julianday(dayInOrder('now', 'localtime', 'weekday 0', '-6 fullDate')) - julianday(dayInOrder(START_DATE, 'weekday 0', '-6 fullDate')) >= 0 ");
		sqlQuery.append("                               AND status = 1 ");
		sqlQuery.append("		 						AND (monday or tuesday or wednesday or thursday or friday or saturday or sunday)) a, ");
		sqlQuery.append("                       visit_plan b, ");
		sqlQuery.append("                       routing c, ");
		sqlQuery.append("                       staff s, ");
		sqlQuery.append("                       shop sh, ");
		sqlQuery.append("                       customer cu ");
		sqlQuery.append("                WHERE  1 = 1 ");
		sqlQuery.append("                       AND b.routing_id = a.routing_id ");
		sqlQuery.append("                       AND c.routing_id = a.routing_id ");
		sqlQuery.append("                       AND b.staff_id = s.staff_id ");
		sqlQuery.append("                       AND a.customer_id = cu.customer_id ");
		sqlQuery.append("                       AND s.shop_id = sh.shop_id ");
		sqlQuery.append("                       AND sh.status = 1 ");
		sqlQuery.append("                       AND a.status = 1 ");
		sqlQuery.append("                       AND b.status = 1 ");
		sqlQuery.append("                       AND s.status = 1 ");
		sqlQuery.append("                       AND c.status = 1 ");
		sqlQuery.append("                       AND cu.status = 1 ");
		sqlQuery.append("                       AND cu.shop_id = NVBH.shop_id ");
		sqlQuery.append("                       AND ( ? >= ");
		params.add(dateNow);
		sqlQuery.append("                             Date(b.from_date) ");
		sqlQuery.append("                             AND ( b.to_date IS NULL ");
		sqlQuery.append("                                    OR ? <= ");
		params.add(dateNow);
		sqlQuery.append("                                       Date(b.to_date) ) ) ");
		sqlQuery.append("                       AND s.staff_id IN ");
		sqlQuery.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1)) ");
		sqlQuery.append("                GROUP  BY s.staff_id)) TONG_SO_KH, ");
		sqlQuery.append("       (SELECT Count(tb1.staff_id) ");
		sqlQuery.append("        FROM   (SELECT s.staff_id, ");
		sqlQuery.append("                       s.staff_name NVBH, ");
		sqlQuery.append("                       a.customer_id ");
		sqlQuery.append("                FROM   (SELECT * ");
		sqlQuery.append("                        FROM   routing_customer ");
		sqlQuery.append("                        WHERE  julianday(dayInOrder('now', 'localtime', 'weekday 0', '-6 fullDate')) - julianday(dayInOrder(START_DATE, 'weekday 0', '-6 fullDate')) >= 0 ");
		sqlQuery.append("		 						AND (monday or tuesday or wednesday or thursday or friday or saturday or sunday)) a, ");
		sqlQuery.append("                       visit_plan b, ");
		sqlQuery.append("                       routing c, ");
		sqlQuery.append("                       staff s, ");
		sqlQuery.append("                       shop sh, ");
		sqlQuery.append("                       customer cu ");
		sqlQuery.append("                WHERE  1 = 1 ");
		sqlQuery.append("                       AND b.routing_id = a.routing_id ");
		sqlQuery.append("                       AND c.routing_id = a.routing_id ");
		sqlQuery.append("                       AND b.staff_id = s.staff_id ");
		sqlQuery.append("                       AND a.customer_id = cu.customer_id ");
		sqlQuery.append("                       AND s.shop_id = sh.shop_id ");
		sqlQuery.append("                       AND sh.status = 1 ");
		sqlQuery.append("                       AND a.status = 1 ");
		sqlQuery.append("                       AND b.status = 1 ");
		sqlQuery.append("                       AND s.status = 1 ");
		sqlQuery.append("                       AND c.status = 1 ");
		sqlQuery.append("                       AND cu.status = 1 ");
		sqlQuery.append("                       AND cu.shop_id = NVBH.shop_id ");
		sqlQuery.append("                       AND ( ? >= ");
		params.add(dateNow);
		sqlQuery.append("                             Date(b.from_date) ");
		sqlQuery.append("                             AND ( b.to_date IS NULL ");
		sqlQuery.append("                                    OR ? <= ");
		params.add(dateNow);
		sqlQuery.append("                                       Date(b.to_date) ) ) ");
		sqlQuery.append("			AND b.staff_id IN (SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))  ");
		sqlQuery.append("                       AND b.staff_id IN (SELECT staff_id ");
		sqlQuery.append("                                          FROM   staff a, ");
		sqlQuery.append("                                                 channel_type b ");
		sqlQuery.append("                                          WHERE ");
		sqlQuery.append("                           a.staff_type_id = b.channel_type_id ");
		sqlQuery.append("                           AND a.status = 1 ");
		sqlQuery.append("                           AND b.status = 1 ");
		sqlQuery.append("                           AND a.shop_id = NVBH.shop_id ");
		sqlQuery.append("                                         )) tb1 ");
		sqlQuery.append("               LEFT JOIN staff_customer tb2 ");
		sqlQuery.append("                      ON ( tb1.staff_id = tb2.staff_id ");
		sqlQuery.append("                           AND tb1.customer_id = tb2.customer_id ) ");
		sqlQuery.append("        WHERE  1 = 1 ");
		sqlQuery.append("               AND ( Date(tb2.last_approve_order) IS NULL ");
		sqlQuery.append("                      OR tb2.last_approve_order = '' ");
		sqlQuery.append("                      OR Date(tb2.last_approve_order) < ? ");
		params.add(startOfMonth);
		sqlQuery.append("                          ) ");
		sqlQuery.append("               AND ( tb2.last_order IS NULL ");
		sqlQuery.append("                      OR tb2.last_order = '' ");
		sqlQuery.append("                      OR Date(tb2.last_order) < ? ");
		params.add(dateNow);
		sqlQuery.append("                                                 )) ");
		sqlQuery.append("                                       KHONG_PSDS ");
		sqlQuery.append("FROM   staff NVBH, ");
		sqlQuery.append("       channel_type CH, ");
		sqlQuery.append("       shop SH, ");
		sqlQuery.append("       staff GS ");
		sqlQuery.append("WHERE  NVBH.staff_type_id = CH.channel_type_id ");
		sqlQuery.append("       AND CH.type = 2 ");
		sqlQuery.append("       AND CH.object_type IN ( 1, 2 ) ");
		sqlQuery.append("       AND NVBH.shop_id = SH.shop_id ");
		sqlQuery.append("       AND GS.staff_id IN ( ");
		sqlQuery.append(listGs);
		sqlQuery.append("       ) ");
		sqlQuery.append("       AND GS.status = 1 ");
		sqlQuery.append("       AND SH.status = 1 ");
		sqlQuery.append("       AND NVBH.status = 1 ");
		sqlQuery.append("       AND NVBH.shop_id IN ( ");
		sqlQuery.append(strListShop);
		sqlQuery.append(" )");
		sqlQuery.append("       AND NVBH.staff_id IN ");
		sqlQuery.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlQuery.append("GROUP  BY SH.shop_id, GS.staff_id  ");
		sqlQuery.append("ORDER  BY SH.shop_code, ");
		sqlQuery.append("          GS.staff_name ) TTTT where TTTT.KHONG_PSDS > 0");

		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(), params);

			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * HieuNH lay danh sach cac lan ghe tham khach hang cua NVBH
	 *
	 * @param staffId
	 *            : id NVBH
	 * @param customerIddddd
	 *            : id Khach Hang
	 * @return
	 * @throws Exception
	 */
	public VisitDTO getListVisit(int staffId, int customerId) throws Exception {
		VisitDTO dto = new VisitDTO();
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT Strftime('%d/%m/%Y : %H:%M', al.start_time) THOI_GIAN, ");
		sqlQuery.append("       al.object_type                              TRANG_THAI ");
		sqlQuery.append("FROM   action_log al ");
		sqlQuery.append("WHERE  ( al.object_type = 0 ");
		sqlQuery.append("          OR al.object_type = 1 ) ");
		sqlQuery.append("       AND staff_id NOT IN (SELECT staff_id ");
		sqlQuery.append("                            FROM   action_log ");
		sqlQuery.append("                            WHERE  customer_id = ? ");
		params.add("" + customerId);
		sqlQuery.append("                                   AND substr(al.start_time,1,10) = substr(start_time,1,10) ");
		sqlQuery.append("                                   AND object_type = 4) ");
		sqlQuery.append("       AND al.staff_id = ? ");
		params.add("" + staffId);
		sqlQuery.append("       AND al.customer_id = ? ");
		params.add("" + customerId);
		sqlQuery.append("       AND substr(al.start_time,1,10) >= substr(?, 1, 10) ");
		params.add(startOfMonth);
		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * HieuNH lay danh sach thiet bi
	 *
	 * @param staffIdGS
	 *            : id NVBH
	 * @param customerId
	 *            : id Khach Hang
	 * @return
	 * @throws Exception
	 */
	public ManagerEquipmentDTO getListEquipment(int staffOwnerId, String shopId) throws Exception {
		ManagerEquipmentDTO dto = new ManagerEquipmentDTO();
		List<String> params = new ArrayList<String>();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String strListStaffId = staff.getListStaffOfSupervisor(staffOwnerId +"", shopId);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ab.staff_id        id, ");
		var1.append("       ab.staff_code      maNVBH, ");
		var1.append("       ab.staff_name      nvbh, ");
		var1.append("       ab.total           soThietBi, ");
		var1.append("       ab.totalpass, ");
		var1.append("       ( ab.total - CASE ");
		var1.append("                      WHEN ab.totalpass IS NULL THEN 0 ");
		var1.append("                      ELSE ab.totalpass ");
		var1.append("                    END ) AS khongDat ");
		var1.append("FROM   ((SELECT dt.staff_id, ");
		var1.append("               s.staff_name, ");
		var1.append("               s.staff_code, ");
		var1.append("               dt.customer_id, ");
		var1.append("               Count (dt.customer_id) AS total ");
		var1.append("        FROM   display_tools dt, ");
		var1.append("               staff s, ");
		var1.append("               customer c, ");
		var1.append("               product p, ");
		var1.append("               product_info pi ");
		var1.append("        WHERE  dt.staff_id = s.staff_id ");
		var1.append("               AND s.staff_id in ( " +  strListStaffId + " ) ");
		var1.append("               AND dt.shop_id = ? ");
		params.add(shopId);
		var1.append("               AND s.shop_id = ? ");
		params.add(shopId);
		var1.append("               AND c.shop_id = ? ");
		params.add(shopId);
		var1.append("               AND dt.customer_id = c.customer_id ");
		var1.append("               AND c.status > 0 ");
		var1.append("               AND s.status > 0 ");
		var1.append("               AND dt.product_id = p.product_id ");
		var1.append("               AND p.status = 1 ");
		var1.append("               AND pi.status = 1 ");
		var1.append("               AND p.cat_id = pi.product_info_id ");
		var1.append("               AND pi.product_info_code = 'Z' ");
		var1.append("               AND Strftime('%m', dt.in_month) = (SELECT Strftime('%m', ");
		var1.append("                   Date('now', 'localtime'))) ");
		var1.append("        GROUP  BY dt.staff_id) a ");
		var1.append("        LEFT OUTER JOIN (SELECT a.staff_id, ");
		var1.append("                                Count (*) AS totalpass ");
		var1.append("                         FROM   (SELECT dt.staff_id, ");
		var1.append("                                        dt.customer_id, ");
		var1.append("                                        dt.amount, ");
		var1.append("                                        Sum (sim.amount) ");
		var1.append("                                 FROM   display_tools dt, ");
		var1.append("                                        display_tools_product dtp, ");
		var1.append("                                        rpt_sale_in_month sim ");
		var1.append("                                 WHERE  1 = 1 ");
		var1.append("                                        AND dt.product_id = dtp.tool_id ");
		var1.append("                                        AND Strftime('%m', dt.in_month) = ( ");
		var1.append("                                            SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime' ");
		var1.append("                                            ))) ");
		var1.append("                                        AND sim.product_id = dtp.product_id ");
		var1.append("                                        AND sim.customer_id = dt.customer_id ");
		var1.append("                                        AND sim.PARENT_STAFF_ID = ? ");
		params.add(String.valueOf(staffOwnerId));
		var1.append("                                        AND Strftime('%m', sim.month) = (SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime'))) ");
		var1.append("                                 GROUP  BY dt.staff_id, ");
		var1.append("                                           dt.customer_id, ");
		var1.append("                                           dt.amount ");
		var1.append("                                 HAVING Sum (sim.amount) >= dt.amount) a ");
		var1.append("                         GROUP  BY a.staff_id) b ");
		var1.append("                     ON a.staff_id = b.staff_id) ab ");
		var1.append("ORDER  BY khongdat DESC, ");
		var1.append("          sothietbi DESC, ");
		var1.append("          manvbh, ");
		var1.append("          nvbh ");

		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 *
	 * Lay ds thiet bi theo NVBH cua TBHV
	 *
	 * @author: Nguyen Huu Hieu
	 * @param nvbhShopId
	 * @return
	 * @return: ManagerEquipmentDTO
	 * @throws:
	 */
	public TBHVManagerEquipmentDTO getListEquipmentTBHV(int parentShopId) {
		TBHVManagerEquipmentDTO dto = new TBHVManagerEquipmentDTO();
		List<String> params = new ArrayList<String>();
		params.add("" + parentShopId);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ab.shop_id         id, ");
		var1.append("       ab.shop_code       maNVBH, ");
		var1.append("       ab.shop_name       nvbh, ");
		var1.append("       ab.total           soThietBi, ");
		var1.append("       ab.totalpass, ");
		var1.append("       ( ab.total - CASE ");
		var1.append("                      WHEN ab.totalpass IS NULL THEN 0 ");
		var1.append("                      ELSE ab.totalpass ");
		var1.append("                    END ) AS khongDat ");
		var1.append("FROM   ((SELECT sh.shop_id, ");
		var1.append("               sh.shop_name, ");
		var1.append("               sh.shop_code, ");
		var1.append("               dt.customer_id, ");
		var1.append("               Count (dt.customer_id) AS total ");
		var1.append("        FROM   display_tools dt, ");
		var1.append("               staff s, ");
		var1.append("               customer c, ");
		var1.append("               product p, ");
		var1.append("               product_info pi, ");
		var1.append("               shop sh ");
		var1.append("        WHERE  dt.staff_id = s.staff_id ");
		var1.append("               AND dt.shop_id = sh.shop_id ");
		var1.append("               AND sh.parent_shop_id = ? ");
		var1.append("               AND dt.customer_id = c.customer_id ");
		var1.append("               AND c.status = 1 ");
		var1.append("               AND s.status > 0 ");
		var1.append("               AND dt.product_id = p.product_id ");
		var1.append("               AND p.cat_id = pi.product_info_id ");
		var1.append("               AND pi.status = 1 ");
		var1.append("               AND pi.product_info_code = 'Z' ");
		var1.append("               AND Strftime('%m', dt.in_month) = (SELECT Strftime('%m', ");
		var1.append("                   Date('now', 'localtime'))) ");
		var1.append("        GROUP  BY dt.shop_id) a ");
		var1.append("        LEFT OUTER JOIN (SELECT a.shop_id, ");
		var1.append("                                Count (*) AS totalpass ");
		var1.append("                         FROM   (SELECT dt.shop_id, ");
		var1.append("                                        dt.customer_id, ");
		var1.append("                                        dt.amount, ");
		var1.append("                                        Sum (sim.amount) ");
		var1.append("                                 FROM   display_tools dt, ");
		var1.append("                                        display_tools_product dtp, ");
		var1.append("                                        rpt_sale_in_month sim ");
		var1.append("                                 WHERE  1 = 1 ");
		var1.append("                                        AND dt.product_id = dtp.tool_id ");
		var1.append("                                        AND Strftime('%m', dt.in_month) = ( ");
		var1.append("                                            SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime' ");
		var1.append("                                            ))) ");
		var1.append("                                        AND sim.product_id = dtp.product_id ");
		var1.append("                                        AND sim.customer_id = dt.customer_id ");
		var1.append("                                        AND Strftime('%m', sim.month) = (SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime'))) ");
		var1.append("                                 GROUP  BY dt.shop_id, ");
		var1.append("                                           dt.customer_id, ");
		var1.append("                                           dt.amount ");
		var1.append("                                 HAVING Sum (sim.amount) >= dt.amount) a ");
		var1.append("                         GROUP  BY a.shop_id) b ");
		var1.append("                     ON a.shop_id = b.shop_id) ab ");
		var1.append("ORDER  BY khongdat DESC, ");
		var1.append("          sothietbi DESC, ");
		var1.append("          manvbh, ");
		var1.append("          nvbh ");
		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 *
	 * Lay ds thiet bi theo NVBH cua NPP
	 *
	 * @author: Nguyen Thanh Dung
	 * @param shopId
	 * @return
	 * @return: ManagerEquipmentDTO
	 * @throws:
	 */
	public TBHVManagerEquipmentDTO getListEquipment(String shopId) {
		TBHVManagerEquipmentDTO dto = new TBHVManagerEquipmentDTO();
		List<String> params = new ArrayList<String>();
		params.add(shopId);

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT ab.staff_id id, ab.staff_code maNVBH, ab.name nvbh,");
		sqlQuery.append(" ab.total soThietBi,  ab.totalpass,");
		sqlQuery.append(" (ab.total - case when ab.totalpass is null then 0 else ab.totalpass end) AS khongDat");
		sqlQuery.append(" FROM");
		sqlQuery.append(" ((SELECT dt.staff_id, s.name, s.staff_code, dt.customer_id, COUNT (dt.customer_id) AS total");
		sqlQuery.append(" FROM display_tools dt, staff s, customer c, product p");
		sqlQuery.append(" WHERE dt.staff_id = s.staff_id AND dt.shop_id = ?");
		sqlQuery.append(" AND dt.customer_id = c.customer_id AND c.status = 1");
		sqlQuery.append(" AND dt.product_id = p.product_id AND p.status = 1");
		sqlQuery.append(" AND p.category_code = 'Z'");
		sqlQuery.append(" AND strftime('%m', dt.in_month) = (select strftime('%m', dayInOrder('now', 'localtime')))");
		sqlQuery.append(" GROUP BY dt.staff_id) a");
		sqlQuery.append(" left outer join");
		sqlQuery.append(" (SELECT a.staff_id, COUNT (*) AS totalpass");
		sqlQuery.append(" FROM");
		sqlQuery.append(" (SELECT dt.staff_id, dt.customer_id, dt.amount, SUM (sim.amount)");
		sqlQuery.append(" FROM display_tools dt,  display_tools_product dtp, sale_in_month sim");
		sqlQuery.append(" WHERE 1 = 1");
		sqlQuery.append(" AND dt.product_id = dtp.tool_id");
		sqlQuery.append(" AND strftime('%m', dt.in_month) = (select strftime('%m', dayInOrder('now', 'localtime')))");
		sqlQuery.append(" AND sim.product_id = dtp.product_id");
		sqlQuery.append(" AND sim.customer_id = dt.customer_id");
		sqlQuery.append(" AND strftime('%m', sim.month) = (select strftime('%m', dayInOrder('now', 'localtime')))");
		sqlQuery.append(" GROUP BY dt.staff_id, dt.customer_id, dt.amount HAVING SUM (sim.amount) >= dt.amount) a");
		sqlQuery.append(" GROUP BY a.staff_id) b on a.staff_id = b.staff_id) ab");
		sqlQuery.append(" order by khongDat desc,  soThietBi desc, maNVBH, nvbh");
		// Cursor c = this.rawQuery(sqlQuery.toString(),params.toArray(new
		// String[params.size()]));
		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * HieuNH lay count danh sach tu nhan vien
	 *
	 * @param staffId
	 *            : id NVBH
	 * @param customerId
	 *            : id Khach Hang
	 * @return
	 */
	public int getCountCabinetStaff(int shopId, int staffId, int isAll) {
		int count = 0;
		List<String> params = new ArrayList<String>();
		params.add("" + staffId);
		params.add("" + shopId);
		params.add("" + staffId);
		params.add("" + shopId);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ab.customer_id, ");
		var1.append("       Round(ab.amount / 1000.0)    amount, ");
		var1.append("       Round(ab.amountcus / 1000.0) amountcus, ");
		var1.append("       ( amount - CASE ");
		var1.append("                    WHEN ab.amountcus IS NULL THEN 0 ");
		var1.append("                    ELSE amountcus ");
		var1.append("                  END )             AS amountre ");
		var1.append("FROM   ((SELECT dt.customer_id, ");
		var1.append("               dt.amount AS amount ");
		var1.append("        FROM   display_tools dt, ");
		var1.append("               customer c ");
		var1.append("        WHERE  Strftime('%m', dt.in_month) = (SELECT Strftime('%m', ");
		var1.append("                                                     Date('now', 'localtime'))) ");
		var1.append("               AND dt.staff_id = ? ");
		var1.append("               AND dt.shop_id = ? ");
		var1.append("               AND c.customer_id = dt.customer_id ");
		var1.append("               AND c.status = 1) a ");
		var1.append("        LEFT OUTER JOIN (SELECT * ");
		var1.append("                         FROM   (SELECT dt.customer_id, ");
		var1.append("                                        dt.amount, ");
		var1.append("                                        Sum (sim.amount) AS amountcus ");
		var1.append("                                 FROM   display_tools dt, ");
		var1.append("                                        display_tools_product dtp, ");
		var1.append("                                        rpt_sale_in_month sim ");
		var1.append("                                 WHERE  1 = 1 ");
		var1.append("                                        AND dt.product_id = dtp.tool_id ");
		var1.append("                                        AND Strftime('%m', dt.in_month) = ( ");
		var1.append("                                            SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime' ");
		var1.append("                                            ))) ");
		var1.append("                                        AND sim.product_id = dtp.product_id ");
		var1.append("                                        AND sim.customer_id = dt.customer_id ");
		var1.append("                                        AND Strftime('%m', sim.month) = (SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime')) ");
		var1.append("                                                                        ) ");
		var1.append("                                        AND dt.staff_id = ? ");
		var1.append("                                        AND dt.shop_id = ? ");
		var1.append("                                 GROUP  BY dt.customer_id, ");
		var1.append("                                           dt.amount) a) b ");
		var1.append("                     ON a.customer_id = b.customer_id ) ab ");
		if (isAll == 0) {// khong lay tat ca
			var1.append(" WHERE amountre > 0");
		}
		Cursor c = null;
		try {
			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				count = c.getCount();
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return count;

	}

	/**
	 * HieuNH lay danh sach tu nhan vien
	 *
	 * @param staffId
	 *            : id NVBH
	 * @param customerId
	 *            : id Khach Hang
	 * @return
	 */
	public CabinetStaffDTO getCabinetStaff(int shopId, int staffId, int isAll,
			String page) {
		CabinetStaffDTO dto = new CabinetStaffDTO();
		List<String> params = new ArrayList<String>();
		params.add("" + staffId);
		params.add("" + shopId);
		params.add("" + staffId);
		params.add("" + shopId);

		StringBuffer  var1 = new StringBuffer();
		var1.append("SELECT ab.customer_id                      id, ");
		var1.append("       ab.customer_code                    MA_KH, ");
		var1.append("       ab.customer_name                    TEN_KH, ");
		var1.append("       ab.housenumber                      SO_NHA, ");
		var1.append("       ab.street                           DUONG, ");
		var1.append("       Round(ab.amount / 1000.0)           KE_HOACH, ");
		var1.append("       Round(ab.amountcus / 1000.0)        THUC_HIEN, ");
		var1.append("       ( Round(ab.amount / 1000.0) - CASE ");
		var1.append("                                       WHEN ab.amountcus IS NULL THEN 0 ");
		var1.append("                                       ELSE Round(ab.amountcus / 1000.0) ");
		var1.append("                                     END ) AS CON_LAI ");
		var1.append("FROM   ((SELECT dt.customer_id, ");
		var1.append("               dt.amount AS amount, ");
		var1.append("               c.customer_code, ");
		var1.append("               c.customer_name, ");
		var1.append("               c.housenumber, ");
		var1.append("               c.street ");
		var1.append("        FROM   display_tools dt, ");
		var1.append("               customer c ");
		var1.append("        WHERE  Strftime('%m', dt.in_month) = (SELECT Strftime('%m', ");
		var1.append("                                                     Date('now', 'localtime'))) ");
		var1.append("               AND dt.staff_id = ? ");
		var1.append("               AND dt.shop_id = ? ");
		var1.append("               AND c.customer_id = dt.customer_id ");
		var1.append("               AND c.status = 1) a ");
		var1.append("        LEFT OUTER JOIN (SELECT * ");
		var1.append("                         FROM   (SELECT dt.customer_id, ");
		var1.append("                                        dt.amount, ");
		var1.append("                                        Sum (sim.amount) AS amountcus ");
		var1.append("                                 FROM   display_tools dt, ");
		var1.append("                                        display_tools_product dtp, ");
		var1.append("                                        rpt_sale_in_month sim ");
		var1.append("                                 WHERE  1 = 1 ");
		var1.append("                                        AND dt.product_id = dtp.tool_id ");
		var1.append("                                        AND Strftime('%m', dt.in_month) = ( ");
		var1.append("                                            SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime' ");
		var1.append("                                            ))) ");
		var1.append("                                        AND sim.product_id = dtp.product_id ");
		var1.append("                                        AND sim.customer_id = dt.customer_id ");
		var1.append("                                        AND Strftime('%m', sim.month) = (SELECT ");
		var1.append("                                            Strftime('%m', ");
		var1.append("                                            Date('now', 'localtime')) ");
		var1.append("                                                                        ) ");
		var1.append("                                        AND dt.staff_id = ? ");
		var1.append("                                        AND dt.shop_id = ? ");
		var1.append("                                 GROUP  BY dt.customer_id, ");
		var1.append("                                           dt.amount) a) b ");
		var1.append("                     ON a.customer_id = b.customer_id ) ab ");
		if (isAll == 0) {// khong lay tat ca
			var1.append(" WHERE CON_LAI > 0");
		}
		var1.append(" order by CON_LAI desc,  KE_HOACH desc, THUC_HIEN desc, MA_KH, TEN_KH");
		var1.append(page);
		Cursor c = null;

		try {
			c = this.rawQuery(var1.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * HieuNH lay danh sach NPP cua TBHV
	 *
	 * @param parentShopId
	 *
	 * @return
	 */
	public ArrayList<ShopDTO> getListNPP(String shopId) throws Exception {
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listShopId = shopTB.getShopRecursiveReverse(shopId);
		String strListShop = TextUtils.join(",", listShopId);

		ArrayList<ShopDTO> listShop = new ArrayList<ShopDTO>();
		ArrayList<String> params = new ArrayList<String>();
		// params.add("" + parentShopId);
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT sh.shop_name             SHOP_NAME, ");
		sqlQuery.append("       sh.shop_code             SHOP_CODE, ");
		sqlQuery.append("       sh.shop_id             	 SHOP_ID ");
		sqlQuery.append("FROM   rpt_staff_sale rss, ");
		sqlQuery.append("       shop sh, ");
		sqlQuery.append("       staff st, ");
		sqlQuery.append("       staff st1 ");
		sqlQuery.append("WHERE  rss.shop_id = sh.shop_id ");
		sqlQuery.append("       AND sh.parent_shop_id IN ( ");
		sqlQuery.append(strListShop + " ) ");
		sqlQuery.append("       AND st.staff_id = rss.staff_id ");
		sqlQuery.append("       AND st.staff_id = rss.staff_id ");
		sqlQuery.append("       AND rss.PARENT_STAFF_ID = st1.staff_id ");
		//lay danh sach giam sat NPP duoc phan quyen
		String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		sqlQuery.append("       AND st1.staff_id IN ( " + PriUtils.getInstance().getListSupervisorOfASM(staffIdASM) + " )");
		sqlQuery.append("       AND st.status = 1 ");
		sqlQuery.append("       AND st1.status = 1 ");
		sqlQuery.append("       AND st.staff_id IN ");
		sqlQuery.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.PARENT_STAFF_ID = st1.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlQuery.append("       AND Date(rss.create_date, 'start of month') = ");
		sqlQuery.append("           Date('now','localtime', 'start of month') ");
		sqlQuery.append("GROUP  BY sh.shop_name ");
		sqlQuery.append("ORDER  BY SHOP_NAME ");

		// params.add(strListShop);
		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c != null && c.moveToFirst()) {
				do {
					ShopDTO item = new ShopDTO();
					item.parseDataFromCusor(c);
					listShop.add(item);
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return listShop;
	}

	/**
	 * HieuNH lay danh sach NVGS cua NPP
	 *
	 * @param shopId
	 *
	 * @return
	 */
	public ListStaffDTO getListNVGSOfTBHVReportPSDS(String shopId) {
		ListStaffDTO dto = new ListStaffDTO();
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		String staffIdASM = GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + "";

		String listGs = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM );
		sqlQuery.append("SELECT GS.STAFF_ID          AS STAFF_ID, ");
		sqlQuery.append("       GS.STAFF_CODE        AS STAFF_CODE, ");
		sqlQuery.append("       GS.STAFF_NAME        AS STAFF_NAME, ");
		sqlQuery.append("       SH.SHOP_ID                  AS NVBH_SHOP_ID, ");
		sqlQuery.append("       SH.SHOP_CODE          AS NVBH_SHOP_CODE, ");
		sqlQuery.append("       COUNT(NVBH.STAFF_ID) AS NUM_NVBH ");
		sqlQuery.append("FROM   STAFF NVBH, ");
		sqlQuery.append("       CHANNEL_TYPE CH, ");
		sqlQuery.append("       SHOP SH, ");
		sqlQuery.append("       STAFF GS, ");
		sqlQuery.append("       CHANNEL_TYPE GS_CH ");
		sqlQuery.append("WHERE  NVBH.STAFF_TYPE_ID = CH.CHANNEL_TYPE_ID ");
		sqlQuery.append("       AND CH.TYPE = 2 ");
		sqlQuery.append("       AND CH.OBJECT_TYPE IN ( 1, 2 ) ");
		sqlQuery.append("       AND NVBH.SHOP_ID = SH.SHOP_ID ");
		sqlQuery.append("       AND NVBH.STATUS = 1 ");
		sqlQuery.append("       AND SH.STATUS = 1 ");
		sqlQuery.append("       AND NVBH.SHOP_ID = ? ");
		sqlQuery.append("       AND GS.STAFF_ID IN (");
		sqlQuery.append(listGs);
		sqlQuery.append("       )");
		params.add(shopId);
		sqlQuery.append("       AND NVBH.STAFF_ID IN ");
		sqlQuery.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.PARENT_STAFF_ID = GS.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlQuery.append("       AND GS.STATUS = 1 ");
		sqlQuery.append("GROUP  BY SH.SHOP_ID, ");
		sqlQuery.append("          GS.STAFF_ID ");
		sqlQuery.append("ORDER  BY SH.SHOP_CODE, ");
		sqlQuery.append("          GS.STAFF_NAME ");

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				if (c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.parrseStaffFromCursor(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * HieuNH lay danh sach NVGS cua NPP
	 *
	 * @param inheritShopId
	 *
	 * @return
	 */
	public ListStaffDTO getListNVGSOfTBHVReportDate(Bundle bundle) throws Exception {
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String parentStaffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
		ListStaffDTO dto = new ListStaffDTO();
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		STAFF_TABLE table = new STAFF_TABLE(mDB);
		String listGs = table.getListStaffManagerOfShop(parentStaffId, shopId);
		sqlQuery.append("SELECT distinct st1.staff_name STAFF_NAME, ");
		sqlQuery.append("       st1.staff_code, ");
		sqlQuery.append("       st1.staff_id ");
		sqlQuery.append("FROM    staff st1 ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND st1.staff_id IN ( ");
		sqlQuery.append(listGs);
		sqlQuery.append("       ) ");
		sqlQuery.append("		and st1.status = 1 ");
		sqlQuery.append("GROUP  BY st1.staff_code, st1.staff_name ");

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				if (c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.parrseStaffFromCursor(c);
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * Lay danh sach NVBH duoi quyen
	 * @author: hoanpd1
	 * @since: 10:50:14 05-03-2015
	 * @return: ListStaffDTO
	 * @throws:
	 * @param shopId
	 * @param staffOwnerId
	 * @param isHaveAll
	 * @return
	 * @throws Exception
	 */
	public ListStaffDTO getListNVBHOrderCode(int shopId, String staffOwnerId,
			boolean isHaveAll) throws Exception {
		ListStaffDTO dto = new ListStaffDTO();
		ArrayList<String> params = new ArrayList<String>();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffId = staff.getListStaffOfSupervisor(staffOwnerId, String.valueOf(shopId));
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append(" SELECT st.STAFF_ID STAFF_ID, ");
		sqlQuery.append("        st.STAFF_NAME STAFF_NAME, ");
		sqlQuery.append("        st.STAFF_CODE STAFF_CODE ");
		sqlQuery.append(" FROM STAFF st, STAFF_TYPE S ");
		sqlQuery.append(" WHERE 1=1 ");
		sqlQuery.append("       AND st.STATUS = 1 ");
		sqlQuery.append("       AND st.staff_type_id = s.staff_type_id ");
		sqlQuery.append("       AND s.status = 1 ");
		sqlQuery.append("       AND s.SPECIFIC_TYPE = ? ");
		params.add(String.valueOf(UserDTO.TYPE_STAFF));
		if (!StringUtil.isNullOrEmpty(staffOwnerId)) {
			sqlQuery.append(" and st.STAFF_ID IN ( " + listStaffId +" ) ");
			dto.listStaff = listStaffId;
		}
		sqlQuery.append(" ORDER BY st.STAFF_CODE");

		Cursor c = null;
		try {
			c = this.rawQueries(sqlQuery.toString(),params);
			if (c != null && c.moveToFirst()) {
				if (isHaveAll && c.getCount() > 1) {
					dto.addItemAll();
				}
				do {
					dto.addItem(c);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 * HieuNH lay danh sach lich su mua hang cua NVBH ban cho Khach hang
	 *
	 * @param nvbhShopId
	 * @param staffOwnerId
	 *            : id NVGS
	 * @return
	 */
	public Vector<HistoryItemDTO> getListHistory(String staffId,
			String customerId, String productId, String shopId) throws Exception {
		Vector<HistoryItemDTO> dto = new Vector<HistoryItemDTO>();
		HistoryItemDTO item;
		List<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);

		sqlQuery.append("SELECT P.product_code        SKU, ");
		sqlQuery.append("       p.product_id, ");
		sqlQuery.append("       FB.feedback_id, ");
		sqlQuery.append("       SIM.amount AMOUNT, ");
		sqlQuery.append("       SIM.amount_approved AMOUNT_APPROVED,");
		sqlQuery.append("       SIM.amount_pending AMOUNT_PENDING,");
		sqlQuery.append("       SIM.quantity QUANTITY,");
		sqlQuery.append("       SIM.quantity_approved QUANTITY_APPROVED,");
		sqlQuery.append("       SIM.quantity_pending QUANTITY_PENDING ");
		sqlQuery.append("FROM   feedback FB, ");
		sqlQuery.append("       feedback_staff FBS, ");
		sqlQuery.append("       feedback_staff_product FBD ");
		sqlQuery.append("       LEFT JOIN (SELECT Sum(RSIM.amount) AMOUNT, ");
		sqlQuery.append("                         Sum(RSIM.amount_approved) AMOUNT_APPROVED,");
		sqlQuery.append("                         Sum(RSIM.amount_pending) AMOUNT_PENDING,");
		sqlQuery.append("                         Sum(RSIM.quantity) QUANTITY,");
		sqlQuery.append("                         Sum(RSIM.quantity_approved) QUANTITY_APPROVED,");
		sqlQuery.append("                         Sum(RSIM.quantity_pending) QUANTITY_PENDING,");
		sqlQuery.append("                         RSIM.product_id ");
		sqlQuery.append("                  FROM   rpt_sale_primary_month RSIM ");
		sqlQuery.append("                  WHERE  1 = 1 ");
		if (!StringUtil.isNullOrEmpty(staffId)) {
			sqlQuery.append("                         AND RSIM.staff_id = ? ");
			params.add(staffId);
		}
		// ds cac kh
		if (!StringUtil.isNullOrEmpty(customerId)) {
			sqlQuery.append("                  AND RSIM.customer_id IN ( ");
			sqlQuery.append(customerId);
			sqlQuery.append("                  ) ");
		}

		sqlQuery.append("                  AND RSIM.shop_id IN ( ");
		sqlQuery.append(idShopListReverse);
		sqlQuery.append("                  ) ");

		sqlQuery.append("                         AND Date(RSIM.affect_date, 'start of month') = (SELECT ");
		sqlQuery.append("                             Date('now', 'localtime','start of month')) ");
		sqlQuery.append("                  GROUP  BY RSIM.product_id) SIM ");
		sqlQuery.append("              ON FBD.product_id = SIM.product_id, ");
		sqlQuery.append("       product P ");
		sqlQuery.append("WHERE  FB.feedback_id = FBS.feedback_id ");
		sqlQuery.append("       AND FBS.feedback_staff_id = fbd.feedback_staff_id ");
		sqlQuery.append("       AND FBD.product_id = P.product_id ");
		if (!StringUtil.isNullOrEmpty(staffId)) {
			sqlQuery.append("       AND FBS.staff_id = ? ");
			params.add(staffId);
		}
		/*if (!StringUtil.isNullOrEmpty(customerId)) {
			sqlQuery.append("       AND FB.customer_id = ? ");
			params.add(customerId);
		}*/
//		sqlQuery.append("       AND FB.shopId = ? ");
//		params.add(shopId);

		sqlQuery.append("GROUP  BY sku ");
		sqlQuery.append("ORDER  BY SKU DESC ");

		Cursor c = null;
		try {
			c = this.rawQuery(sqlQuery.toString(),
					params.toArray(new String[params.size()]));
			if (c != null && c.moveToFirst()) {
				do {
					item = new HistoryItemDTO();
					item.initDateWithCursor(c);
					dto.addElement(item);
				} while (c.moveToNext());
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return dto;
	}

	/**
	 *
	 * get report progress in month of TBHV
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: ReportProgressMonthViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public ReportProgressMonthViewDTO getReportProgressInMonthOfTBHV(Bundle data) throws Exception {
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String inheritID = data.getString(IntentConstants.INTENT_INHERIT_ID);
		int sysCurrencyDivide = data.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE);

//		ArrayList<String> listShopId = shopTB.getShopChildByShopTypeRecursive(shopId);
//		String strListShop = TextUtils.join(",", listShopId);
//		String listSuperVisor = staff.getListStaffManagerOfChildShop(inheritID, shopId);
		ReportProgressMonthViewDTO result = new ReportProgressMonthViewDTO();
//		String dateNow = DateUtils.now();

		STAFF_TABLE staffTB = new STAFF_TABLE(this.mDB);
		String[] lstStaff = inheritID.split(",");
		StringBuffer listGs = new StringBuffer();
		for (int i = 0, size = lstStaff.length; i < size; i++) {
			if (i == 0)
				listGs.append(staffTB.getListStaffManagerOfChildShop(
						lstStaff[i], shopId));
			else {
				listGs.append(",");
				listGs.append(staffTB.getListStaffManagerOfChildShop(
						lstStaff[i], shopId));
			}
		}
		// lay ds shop con duoi mot cap
		ArrayList<String> listShopChild = staffTB.getListShopChild(shopId);
		String strListShop = TextUtils.join(",", listShopChild);
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		ArrayList<String> listParams = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT sh.shop_id                                     STAFF_OWNER_ID, ");
		sqlQuery.append("       sh.shop_name                                   STAFF_OWNER_NAME, ");
		sqlQuery.append("       sh.shop_code                                   STAFF_OWNER_CODE, ");
		sqlQuery.append("       st1.staff_id                                    STAFF_ID, ");
		sqlQuery.append("       st1.staff_name                                  STAFF_NAME, ");
		sqlQuery.append("       st1.staff_code                                  STAFF_CODE, ");
		sqlQuery.append("       Sum(rss.month_amount_plan)                     MONTH_AMOUNT_PLAN, ");
		sqlQuery.append("       Sum(rss.month_quantity_plan)                     MONTH_QUANTITY_PLAN, ");

		if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sqlQuery.append("       rss.month_amount      MONTH_AMOUNT, ");
			sqlQuery.append("       rss.month_quantity      MONTH_QUANTITY, ");
		} else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED) {
			sqlQuery.append("       rss.month_amount_approved      MONTH_AMOUNT, ");
			sqlQuery.append("       rss.month_quantity_approved      MONTH_QUANTITY, ");
		}
		sqlQuery.append("       rss.month_amount_approved    MONTH_AMOUNT_APPROVED, ");
		sqlQuery.append("       rss.month_quantity_approved    MONTH_QUANTITY_APPROVED, ");

//		sqlQuery.append("       ( Sum (rss.month_sku) / Count (rss.staff_id) ) MONTH_SKU, ");
//		sqlQuery.append("       Ifnull(NEW_SS.PLAN, 0)                         MONTH_SKU_PLAN, ");
		sqlQuery.append("       Ifnull(CASE ");
		sqlQuery.append("                WHEN ( Sum(rss.month_amount_plan) = 0 ");
		sqlQuery.append("                       AND Sum(rss.month_amount) > 0 ) THEN 1 ");
		sqlQuery.append("                ELSE Sum(rss.month_amount) / Sum(rss.month_amount_plan) ");
		sqlQuery.append("              END, 0)                                 PERCENT ");
		sqlQuery.append("FROM   rpt_staff_sale rss, ");
		sqlQuery.append("       shop sh, ");
		sqlQuery.append("       staff st1, ");
		sqlQuery.append("       org_temp OT, ");
		sqlQuery.append("       cycle CY ");
		sqlQuery.append("WHERE  rss.shop_id = sh.shop_id ");
		sqlQuery.append("       and rss.object_id = sh.shop_id  ");
		sqlQuery.append("       and ot.shop_id = sh.shop_id  ");
		sqlQuery.append("       and rss.object_type = ? ");
		listParams.add(ApParamDTO.SYS_SALE_SHOP + "");
		sqlQuery.append("       and ot.staff_id = st1.staff_id  ");
		sqlQuery.append("       AND sh.shop_id IN ( ");
		sqlQuery.append(strListShop + " ) ");
		sqlQuery.append("       AND st1.staff_id IN ( ");
		sqlQuery.append(listGs.toString());
		sqlQuery.append("       )");
		sqlQuery.append("       AND st1.status = 1 ");
		sqlQuery.append("       AND sh.status = 1 ");
//		sqlQuery.append("       AND substr(rss.sale_date, 1, 7) = substr(?, 1, 7) ");
//		listParams.add(date_now);
		sqlQuery.append("       AND Date(CY.BEGIN_DATE) <= Date(?) ");
		listParams.add(date_now);
		sqlQuery.append("       AND Date(CY.END_DATE) >= Date(?) ");
		listParams.add(date_now);
		sqlQuery.append("       AND CY.STATUS = 1 ");
		sqlQuery.append("       AND RSS.cycle_id = CY.cycle_id ");
		sqlQuery.append("GROUP  BY sh.shop_name, ");
		sqlQuery.append("          st1.staff_name ");
		sqlQuery.append("ORDER  BY STAFF_OWNER_CODE, ");
		sqlQuery.append("          STAFF_OWNER_NAME ");

		Cursor c = null;
		boolean isExists = false;
		try {
			c = this.rawQueries(sqlQuery.toString(), listParams);
			if (c != null && c.moveToFirst()) {
				do {
					isExists = false;
					ReportProgressMonthCellDTO item = new ReportProgressMonthCellDTO();
					item.initReportProgressMonthObject(c, sysCurrencyDivide);
					for(ReportProgressMonthCellDTO dto : result.listReportProgessMonthDTO) {
						if(dto.staffOwnerID == item.staffOwnerID && dto.staffID != item.staffID) {
							dto.staffName = dto.staffName + "\n" + item.staffName;
							dto.staffCode = dto.staffCode + "\n" +item.staffCode;
							dto.listStaffID = dto.listStaffID + "," +item.staffID;
							isExists = true;
						}
					}
					if(!isExists)
						result.listReportProgessMonthDTO.add(item);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		// number fullDate plan sale in month
		result.numDaySalePlan = new EXCEPTION_DAY_TABLE(mDB)
				.getPlanWorkingDaysOfMonth(new Date(), shopId);
		// number fullDate sold in month
		result.numDaySoldPlan = new EXCEPTION_DAY_TABLE(mDB)
				.getCurrentWorkingDaysOfMonth(shopId);
		// percent
		result.progessSold = StringUtil.calPercentUsingRound(result.numDaySalePlan, result.numDaySoldPlan);
		return result;
	}

	/**
	 *
	 * get report progress in month of TBHV detail NPP
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: ReportProgressMonthViewDTO
	 * @throws Exception
	 * @throws:
	 */
	public ReportProgressMonthViewDTO getReportProgressInMonthOfTBHVDetailNPP(
			Bundle data) throws Exception {
		String staffOwenerId = "";
		String shopId = "";
		int sysCurrencyDivide = 1;
		if (data != null) {
			sysCurrencyDivide = data.getInt(IntentConstants.INTENT_SYS_CURRENCY_DIVIDE, 1);
			if (data.getString(IntentConstants.INTENT_STAFF_OWNER_ID) != null) {
				staffOwenerId = data
						.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
			}
			if (data.getString(IntentConstants.INTENT_SHOP_ID) != null) {
				shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
			}
		}

		ReportProgressMonthViewDTO result = new ReportProgressMonthViewDTO();

		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		StringBuffer sqlQuery = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		String[] lstGS = staffOwenerId.split(",");
		for (String itemGS : lstGS) {
			sqlQuery.delete(0, sqlQuery.length());
			params.clear();
			sqlQuery.append("SELECT DISTINCT RPT_SS.object_id          STAFF_ID, ");
//			sqlQuery.append("                RPT_SS.newName        STAFF_NAME, ");
//			sqlQuery.append("                RPT_SS.staff_code        STAFF_CODE, ");
			sqlQuery.append("                stGSNPP.staff_id              STAFF_OWNER_ID, ");
			sqlQuery.append("                stGSNPP.staff_name                  STAFF_OWNER_NAME, ");
			sqlQuery.append("                stNVBH.staff_name                  STAFF_NAME, ");
			sqlQuery.append("                stNVBH.staff_code                  STAFF_CODE, ");
//			sqlQuery.append("                stNVBH.staff_id                  STAFF_ID, ");
			sqlQuery.append("                stGSNPP.staff_code            STAFF_OWNER_CODE, ");
			sqlQuery.append("                RPT_SS.month_amount_plan MONTH_AMOUNT_PLAN, ");
			sqlQuery.append("                RPT_SS.month_quantity_plan MONTH_QUANTITY_PLAN, ");
			if(GlobalInfo.getInstance().getSysCalUnapproved() == 2) {
				sqlQuery.append("       RPT_SS.month_amount      MONTH_AMOUNT, ");
				sqlQuery.append("       RPT_SS.month_quantity      MONTH_QUANTITY, ");
			} else if(GlobalInfo.getInstance().getSysCalUnapproved() == 1) {
				sqlQuery.append("       RPT_SS.month_amount_approved      MONTH_AMOUNT, ");
				sqlQuery.append("       RPT_SS.month_quantity_approved      MONTH_QUANTITY, ");
			}
			sqlQuery.append("       RPT_SS.month_amount_approved    MONTH_AMOUNT_APPROVED, ");
			sqlQuery.append("       RPT_SS.month_quantity_approved    MONTH_QUANTITY_APPROVED ");
			// sqlQuery.append("       rss.day_sku_plan    SKU_KE_HOACH, ");
			// sqlQuery.append("       rss.day_sku         SKU_THUC_HIEN ");
			sqlQuery.append("FROM   rpt_staff_sale RPT_SS, ");
			sqlQuery.append("       staff stNVBH,");
			sqlQuery.append("       staff stGSNPP, ");
			sqlQuery.append("       cycle CY ");
			sqlQuery.append("WHERE  1 = 1 ");
			sqlQuery.append("       AND stGSNPP.staff_id = ? ");
			params.add(itemGS);
			sqlQuery.append("       AND RPT_SS.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("       AND RPT_SS.object_type = ? ");
			params.add(ApParamDTO.SYS_SALE_STAFF + "");
//			sqlQuery.append("       AND stNVBH.staff_id = RPT_SS.staff_id ");
			sqlQuery.append("       AND stNVBH.staff_id = RPT_SS.object_id ");
			sqlQuery.append("       AND stNVBH.status = 1 ");
//			sqlQuery.append("       AND substr(RPT_SS.sale_date, 1, 7) = substr(?, 1, 7) ");
//			params.add(date_now);
			sqlQuery.append("       AND RPT_SS.cycle_id = CY.cycle_id ");
			sqlQuery.append("       AND Date(CY.BEGIN_DATE) <= Date(?) ");
			params.add(date_now);
			sqlQuery.append("       AND Date(CY.END_DATE) >= Date(?) ");
			params.add(date_now);
			sqlQuery.append("       AND CY.STATUS = 1 ");
			if (!StringUtil.isNullOrEmpty(itemGS)) {
				STAFF_TABLE staff = new STAFF_TABLE(mDB);
				String listStaff = staff.getListStaffOfSupervisor(itemGS,
						shopId);
				sqlQuery.append(" AND stNVBH.staff_id IN ( ");
				sqlQuery.append(listStaff);
				sqlQuery.append(" ) ");
			}
			sqlQuery.append("ORDER  BY STAFF_NAME, ");
			sqlQuery.append("          STAFF_OWNER_NAME ");

			Cursor c = null;

			double totalAmountPlan = 0;
			double totalAmountDone = 0;
			double totalDuyet = 0;
			long totalQuantityPlan = 0;
			long totalQuantityDone = 0;
			long totalQuantityDuyet = 0;
			try {
				c = this.rawQueries(sqlQuery.toString(), params);
				if (c != null && c.moveToFirst()) {
					do {
						ReportProgressMonthCellDTO item = new ReportProgressMonthCellDTO();
						item.initReportProgressMonthObject(c, sysCurrencyDivide);
						totalAmountPlan += item.amountPlan;
						totalAmountDone += item.amountDone;
						totalDuyet += item.duyet;
						totalQuantityPlan += item.quantityPlan;
						totalQuantityDone += item.quantityDone;
						totalQuantityDuyet += item.quantityDuyet;
						result.listReportProgessMonthDTO.add(item);
					} while (c.moveToNext());
				}
			} finally {
				try {
					if (c != null) {
						c.close();
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			// row total
			result.totalReportObject.amountPlan = totalAmountPlan;
			result.totalReportObject.amountDone = totalAmountDone;
			result.totalReportObject.duyet = totalDuyet;
			result.totalReportObject.quantityPlan = totalQuantityPlan;
			result.totalReportObject.quantityDone = totalQuantityDone;
			result.totalReportObject.quantityDuyet = totalQuantityDuyet;
			result.totalReportObject.amountRemain = (totalAmountPlan - totalAmountDone) >= 0 ? (totalAmountPlan - totalAmountDone)
					: 0;
			result.totalReportObject.quantityRemain = (totalQuantityPlan - totalQuantityDone) >= 0 ? (totalQuantityPlan - totalQuantityDone)
					: 0;
			if (totalAmountDone <= 0) {
				result.totalReportObject.progressAmountDone = 0;
			} else {
				result.totalReportObject.progressAmountDone = (int) (totalAmountDone * 100 / (totalAmountPlan <= 0 ? totalAmountDone
						: totalAmountPlan));
			}

			if (totalQuantityDone <= 0) {
				result.totalReportObject.progressQuantityDone = 0;
			} else {
				result.totalReportObject.progressQuantityDone = (int) (totalQuantityDone * 100 / (totalQuantityPlan <= 0 ? totalQuantityDone
						: totalQuantityPlan));
			}

		}


		return result;
	}

	/**
	 *
	 * exec query sql get report progress sales focus info
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: TBHVReportProgressSalesFocusView
	 * @throws:
	 */
	public TBHVProgressReportSalesFocusViewDTO getReportProgressSalesFocusInfo(
			Bundle data) throws Exception{
		TBHVProgressReportSalesFocusViewDTO result = new TBHVProgressReportSalesFocusViewDTO();
		Cursor c = null;
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(this.mDB);
		ArrayList<String> listParentShopId = shopTB.getShopRecursive(shopId);
		ArrayList<String> listChildShopId = shopTB
				.getShopRecursiveReverse(shopId);
		String strListParentShop = TextUtils.join(",", listParentShopId);
		String strListChildShop = TextUtils.join(",", listChildShopId);

		String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		String listGs = PriUtils.getInstance().getListSupervisorOfASMIncludeHistory(staffIdASM);


		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT S.shop_id                      STAFF_OWNER_ID, ");
			sqlQuery.append("                S.shop_name                    STAFF_OWNER_NAME, ");
			sqlQuery.append("                S.shop_code                    STAFF_OWNER_CODE, ");
			sqlQuery.append("                st.staff_id                    STAFF_ID, ");
			sqlQuery.append("                st.staff_name                        STAFF_NAME, ");
			sqlQuery.append("                st.staff_code                  STAFF_CODE, ");
			sqlQuery.append("                Sum(RPT_SS.focus1_amount_plan) FOCUS1_AMOUNT_PLAN, ");
			sqlQuery.append("                Sum(RPT_SS.focus1_amount)      FOCUS1_AMOUNT, ");
			sqlQuery.append("                Sum(RPT_SS.focus2_amount_plan) FOCUS2_AMOUNT_PLAN, ");
			sqlQuery.append("                Sum(RPT_SS.focus2_amount)      FOCUS2_AMOUNT, ");
			sqlQuery.append("                ifnull(case  when (Sum(RPT_SS.focus1_amount_plan) = 0 and Sum(RPT_SS.focus1_amount) > 0) then 1 else Sum(RPT_SS.focus1_amount) / Sum(RPT_SS.focus1_amount_plan) end, 0) PERCENT1, ");
			sqlQuery.append("                ifnull(case  when (Sum(RPT_SS.focus2_amount_plan) = 0 and Sum(RPT_SS.focus2_amount) > 0) then 1 else Sum(RPT_SS.focus2_amount) / Sum(RPT_SS.focus2_amount_plan) end, 0) PERCENT2 ");
			sqlQuery.append("FROM   shop S, ");
			sqlQuery.append("       rpt_staff_sale RPT_SS, ");
//			sqlQuery.append("       staff ST ");
//			lay luon nhan gsnpp da nghi/chuyen tuyen
			sqlQuery.append("        (SELECT s.staff_id, ");
			sqlQuery.append("                         s.staff_code, ");
			sqlQuery.append("                         s.staff_name, ");
			sqlQuery.append("                         s.mobilephone, ");
			sqlQuery.append("                         s.shop_id ");
			sqlQuery.append("                  FROM   staff s ");
			sqlQuery.append("                  UNION ");
			sqlQuery.append("                  SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("                         sh.staff_code, ");
			sqlQuery.append("                         sh.staff_name, ");
			sqlQuery.append("                         sh.mobilephone, ");
			sqlQuery.append("                         sh.shop_id ");
			sqlQuery.append("                  FROM   staff_history sh ");
			sqlQuery.append("                  WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("                    Date(sh.from_date) >= Date('now', 'localtime','start of month') ");
			sqlQuery.append("                         AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                                   'start of month') ");
			sqlQuery.append("                                OR sh.to_date IS NULL ) ");
			sqlQuery.append("                         AND sh.status = 1) ST ");
			sqlQuery.append("WHERE  S.shop_id = RPT_SS.shop_id ");
			sqlQuery.append("       AND ST.status = 1 ");
			sqlQuery.append("       AND (S.parent_shop_id IN ( ");
			sqlQuery.append(strListParentShop + " ) OR S.parent_shop_id IN ( ");
			sqlQuery.append(strListChildShop + " ))");
			sqlQuery.append("       AND ST.staff_id = rpt_ss.parent_staff_id ");

			//gs phai thuoc quyen quan ly truc tiep tu ASM
			sqlQuery.append("       AND ST.staff_id IN ( ");
			sqlQuery.append(listGs);
			sqlQuery.append("       ) ");

			sqlQuery.append("       AND ( Date(RPT_SS.create_date) IS NULL ");
			sqlQuery.append("              OR ( Date(RPT_SS.create_date, 'start of month') = ");
			sqlQuery.append("                       Date('now', 'localtime','start of month') ) ) ");
			sqlQuery.append("GROUP  BY S.shop_id, ");
			sqlQuery.append("          S.shop_name, ");
			sqlQuery.append("          S.shop_code, ");
			sqlQuery.append("          st.staff_id, ");
			sqlQuery.append("          st.staff_name, ");
			sqlQuery.append("          st.staff_code ");
			sqlQuery.append("ORDER  BY STAFF_OWNER_CODE, ");
			sqlQuery.append("          STAFF_NAME ,PERCENT1, PERCENT2 ");

			String[] param = new String[] {};

			c = this.rawQuery(sqlQuery.toString(), param);

			if (c != null && c.moveToFirst()) {
				boolean isCreateArrMMTTT = false;

				do {
					if (!isCreateArrMMTTT) {
						for (int i = 1; i < Integer.MAX_VALUE; i++) {
							int idx = c.getColumnIndex("FOCUS" + i + "_AMOUNT");
							if (idx >= 0) {
								result.arrMMTTText.add(String.valueOf(i));
								result.objectReportTotal.listFocusProductItem.add(new ReportFocusProductItem());
							} else {
								break;
							}
						}
						isCreateArrMMTTT = true;
					}
					ReportSalesFocusEmployeeInfo item = new ReportSalesFocusEmployeeInfo();

					item.parseDataFromCursor(c);

					for (int i = 0; i < result.arrMMTTText.size(); i++) {
						String number = result.arrMMTTText.get(i);
						ReportFocusProductItem fItem = new ReportFocusProductItem();
						fItem.parseDataFromCursor(c, number);
						fItem.remain = fItem.amountPlan > fItem.amount ? fItem.amountPlan
								- fItem.amount
								: (long) 0;
						fItem.progress = (int) (fItem.amountPlan > 0 ? fItem.amount
								* 100 / fItem.amountPlan
								: 100.0);
						result.objectReportTotal.listFocusProductItem.get(i).amountPlan += fItem.amountPlan;
						result.objectReportTotal.listFocusProductItem.get(i).amount += fItem.amount;
						item.listFocusProductItem.add(fItem);
					}
					result.listFocusInfoRow.add(item);
				} while (c.moveToNext());
			}

			// update total row
			for (int i = 0, size = result.objectReportTotal.listFocusProductItem
					.size(); i < size; i++) {
				ReportFocusProductItem TMP = result.objectReportTotal.listFocusProductItem
						.get(i);
				TMP.remain = TMP.amountPlan > TMP.amount ? TMP.amountPlan
						- TMP.amount : 0;
				TMP.progress = (int) (TMP.amountPlan > 0 ? TMP.amount * 100
						/ TMP.amountPlan : 100.0);
			}

			// get plan dayInOrder, plan dayInOrder done, progress percent sold
			result.numPlanDate = new EXCEPTION_DAY_TABLE(mDB)
					.getPlanWorkingDaysOfMonth(new Date(), shopId);
			result.numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB)
					.getCurrentWorkingDaysOfMonth(shopId);
			result.progressSales = Math.floor((result.numPlanDate > 0 ? (double)result.numPlanDateDone
					* 100 / result.numPlanDate
					: 0));
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return result;
	}

	/**
	 *
	 * get report progress sales focus detail info from DB
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: TBHVProgressReportSalesFocusViewDTO
	 * @throws:
	 */
	public TBHVProgressReportSalesFocusViewDTO getReportProgressSalesFocusDetailInfo(
			Bundle data) {
		String staffOwnerId = "";
		String shopId = "";
		if (data != null) {
			if (data.getString(IntentConstants.INTENT_STAFF_OWNER_ID) != null) {
				staffOwnerId = data
						.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
			}
			if (data.getString(IntentConstants.INTENT_SHOP_ID) != null) {
				shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
			}
		}

		TBHVProgressReportSalesFocusViewDTO result = new TBHVProgressReportSalesFocusViewDTO();
		Cursor c = null;
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT DISTINCT ST.staff_id STAFF_OWNER_ID, ");
			sqlQuery.append("                ST.staff_name     STAFF_OWNER_NAME, ");
			sqlQuery.append("                RPT_SS.* ,");
			sqlQuery.append("                ifnull(case  when (RPT_SS.FOCUS1_AMOUNT_PLAN = 0 and RPT_SS.FOCUS1_AMOUNT > 0) then 1 else RPT_SS.FOCUS1_AMOUNT / RPT_SS.FOCUS1_AMOUNT_PLAN end, 0) PERCENT1, ");
			sqlQuery.append("                ifnull(case  when (RPT_SS.FOCUS2_AMOUNT_PLAN = 0 and RPT_SS.FOCUS2_AMOUNT > 0) then 1 else RPT_SS.FOCUS2_AMOUNT / RPT_SS.FOCUS2_AMOUNT_PLAN end, 0) PERCENT2 ");
			sqlQuery.append("FROM  ");
			sqlQuery.append("       (SELECT s.staff_id, ");
			sqlQuery.append("               s.staff_code, ");
			sqlQuery.append("               s.staff_name, ");
			sqlQuery.append("               s.mobilephone, ");
			sqlQuery.append("               s.shop_id, ");
			sqlQuery.append("               s.staff_type_id, ");
			sqlQuery.append("               s.status ");
			sqlQuery.append("        FROM   staff s ");
			sqlQuery.append("        UNION ");
			sqlQuery.append("        SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("               sh.staff_code, ");
			sqlQuery.append("               sh.staff_name, ");
			sqlQuery.append("               sh.mobilephone, ");
			sqlQuery.append("               sh.shop_id, ");
			sqlQuery.append("               sh.staff_type_id, ");
			sqlQuery.append("               sh.status ");
			sqlQuery.append("        FROM   staff_history sh ");
			sqlQuery.append("        WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("               AND Date(sh.from_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("               AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("                      OR sh.to_date IS NULL ) ");
			sqlQuery.append("               AND sh.status = 1) ST, ");
			sqlQuery.append("       (select rss.*, st.staff_name NEWNAME from rpt_staff_sale rss ,   ");
			sqlQuery.append("       (SELECT s.staff_id, ");
			sqlQuery.append("               s.staff_code, ");
			sqlQuery.append("               s.staff_name, ");
			sqlQuery.append("               s.mobilephone, ");
			sqlQuery.append("               s.shop_id, ");
			sqlQuery.append("               s.staff_type_id, ");
			sqlQuery.append("               s.status ");
			sqlQuery.append("        FROM   staff s ");
			sqlQuery.append("        UNION ");
			sqlQuery.append("        SELECT sh.staff_id AS staff_id, ");
			sqlQuery.append("               sh.staff_code, ");
			sqlQuery.append("               sh.staff_name, ");
			sqlQuery.append("               sh.mobilephone, ");
			sqlQuery.append("               sh.shop_id, ");
			sqlQuery.append("               sh.staff_type_id, ");
			sqlQuery.append("               sh.status ");
			sqlQuery.append("        FROM   staff_history sh ");
			sqlQuery.append("        WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			sqlQuery.append("               AND Date(sh.from_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("               AND ( Date(sh.to_date) >= Date('now', 'localtime', ");
			sqlQuery.append("                                         'start of month') ");
			sqlQuery.append("                      OR sh.to_date IS NULL ) ");
			sqlQuery.append("               AND sh.status = 1) ST ");
			sqlQuery.append("       where rss.staff_id = st.staff_id and st.status = 1 and rss.shop_id = st.shop_id) RPT_SS ");
			sqlQuery.append("WHERE  RPT_SS.shop_id = ? ");
			sqlQuery.append("       AND ST.status = 1 ");
			sqlQuery.append("       AND ST.staff_id = RPT_SS.parent_staff_id ");
			sqlQuery.append("       AND ST.shop_id = RPT_SS.shop_id ");
			sqlQuery.append("       AND ( Date(RPT_SS.create_date) IS NULL ");
			sqlQuery.append("              OR ( Date(RPT_SS.create_date, 'start of month') = ");
			sqlQuery.append("                       Date('now', 'localtime','start of month') ) ) ");
			if (!StringUtil.isNullOrEmpty(staffOwnerId)) {
				sqlQuery.append("       AND RPT_SS.parent_staff_id = ? ");
			} else{
				//neu lay tat ca, thi phai lay nhung GS duoc ASM quan ly truc tiep
				String staffIdASM = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
				String listStaff = PriUtils.getInstance().getListSupervisorOfASM(staffIdASM);
				sqlQuery.append("       AND RPT_SS.parent_staff_id IN ( ");
				sqlQuery.append(listStaff);
				sqlQuery.append("       ) ");
			}

			sqlQuery.append("ORDER  BY staff_owner_name, ");
			sqlQuery.append("          RPT_SS.staff_name, PERCENT1, PERCENT2 ");

			String[] paramsList = new String[] {};
			ArrayList<String> listParams = new ArrayList<String>();
			if (!StringUtil.isNullOrEmpty(staffOwnerId)) {
				listParams.add(shopId);
				listParams.add(staffOwnerId);
			} else {
				listParams.add(shopId);
			}
			paramsList = listParams.toArray(new String[listParams.size()]);

			c = this.rawQuery(sqlQuery.toString(), paramsList);

			if (c != null && c.moveToFirst()) {
				boolean isCreateArrMMTTT = false;

				do {
					if (!isCreateArrMMTTT) {
						for (int i = 1; i < Integer.MAX_VALUE; i++) {
							int idx = c.getColumnIndex("FOCUS" + i + "_AMOUNT");
							if (idx >= 0) {
								result.arrMMTTText.add(String.valueOf(i));
								result.objectReportTotal.listFocusProductItem
										.add(new ReportFocusProductItem());
							} else {
								break;
							}
						}
						isCreateArrMMTTT = true;
					}
					ReportSalesFocusEmployeeInfo item = new ReportSalesFocusEmployeeInfo();

					item.parseDataFromCursor(c);
					if (c.getColumnIndex("NEWNAME") >= 0) {
						String newName = c.getString(c
								.getColumnIndex("NEWNAME"));
						if (!StringUtil.isNullOrEmpty(newName)) {
							item.staffName = newName;
						}
					}

					for (int i = 0; i < result.arrMMTTText.size(); i++) {
						String number = result.arrMMTTText.get(i);
						ReportFocusProductItem fItem = new ReportFocusProductItem();
						fItem.parseDataFromCursor(c, number);
						fItem.remain = fItem.amountPlan > fItem.amount ? fItem.amountPlan
								- fItem.amount
								: (long) 0;
						fItem.progress = (int) (fItem.amountPlan > 0 ? (double) fItem.amount
								* 100 / (double) fItem.amountPlan
								: 100.0);
						result.objectReportTotal.listFocusProductItem.get(i).amountPlan += fItem.amountPlan;
						result.objectReportTotal.listFocusProductItem.get(i).amount += fItem.amount;
						item.listFocusProductItem.add(fItem);
					}
					result.listFocusInfoRow.add(item);
				} while (c.moveToNext());
			}

			// update total row
			for (int i = 0, size = result.objectReportTotal.listFocusProductItem
					.size(); i < size; i++) {
				ReportFocusProductItem TMP = result.objectReportTotal.listFocusProductItem
						.get(i);
				TMP.remain = TMP.amountPlan > TMP.amount ? TMP.amountPlan
						- TMP.amount : 0;
				TMP.progress = (int) (TMP.amountPlan > 0 ? (double) TMP.amount
						* 100 / (double) TMP.amountPlan : 100.0);
			}

		} catch (Exception e) {
			result = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return result;
	}

	/**
	 * Lay du lieu bao cao tien do luy ke theo nganh hang
	 * @author: hoanpd1
	 * @since: 10:14:56 07-04-2015
	 * @return: SupervisorReportStaffSaleViewDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public SupervisorReportStaffSaleViewDTO getSupervisorCatAmountReport(Bundle data)
			throws Exception {
		SupervisorReportStaffSaleViewDTO result = new SupervisorReportStaffSaleViewDTO();
		Cursor c = null;
		// phai khoi tao CAT_LIST cho nganh hang dong
		genCatList();
		String staffOwnerId = data.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String staffType = data.getString(IntentConstants.INTENT_STAFF_TYPE);
		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		int sysSaleRoute = data.getInt(IntentConstants.INTENT_SYS_SALE_ROUTE);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfCycle = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaff = staff.getListStaffOfSupervisor(staffId, shopId);
		ArrayList<String> params = new ArrayList<String>();
		try {
			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append(" SELECT st.OBJECT_ID                                                    AS OBJECT_ID, ");
			sqlQuery.append("        st.OBJECT_CODE                                                  AS OBJECT_CODE, ");
			sqlQuery.append("        st.OBJECT_NAME                                                  AS OBJECT_NAME, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.amount_plan,0))            			 AS CONCAT_AMOUNT_PLAN, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.amount,0))                              AS CONCAT_AMOUNT, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.amount_approved,0))                     AS CONCAT_AMOUNT_APPROVED, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.amount_pending,0))                      AS CONCAT_AMOUNT_PENDING, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.quantity_plan,0))         				 AS CONCAT_QUANTITY_PLAN, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.quantity,0))            				 AS CONCAT_QUANTITY, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.quantity_approved,0))  				 AS CONCAT_QUANTITY_APPROVED, ");
			sqlQuery.append("        Group_concat(Ifnull(rsc.quantity_pending,0))      				 AS CONCAT_QUANTITY_PENDING, ");
			sqlQuery.append("        Group_concat(Ifnull(pi.product_info_code,''))                   AS CONCAT_PRODUCT_INFO_CODE, ");
			sqlQuery.append("        Group_concat(Ifnull(pi.product_info_name,''))                   AS CONCAT_PRODUCT_INFO_NAME ");
			sqlQuery.append(" FROM rpt_staff_category rsc, ");
			if (sysSaleRoute == ApParamDTO.SYS_SALE_ROUTE) {
				sqlQuery.append("       (SELECT routing_id       AS OBJECT_ID,  ");
				sqlQuery.append("               routing_code     AS OBJECT_CODE,  ");
				sqlQuery.append("               routing_name     AS OBJECT_NAME  ");
				sqlQuery.append("        FROM ROUTING  ");
				sqlQuery.append("        WHERE 1=1  ");
				sqlQuery.append("              and status =1  ");
				sqlQuery.append("              and shop_id = ? ");
				params.add(shopId);
			} else if (sysSaleRoute == ApParamDTO.SYS_SALE_STAFF) {
				sqlQuery.append("     (SELECT * ");
				sqlQuery.append("      FROM(SELECT s.staff_id            AS OBJECT_ID, ");
				sqlQuery.append("                  s.staff_code			 AS OBJECT_CODE, ");
				sqlQuery.append("                  s.staff_name			 AS OBJECT_NAME, ");
				sqlQuery.append("                  s.shop_id             AS SHOP_ID ");
				sqlQuery.append("           FROM   staff s, ");
				sqlQuery.append("                  staff_type st ");
				sqlQuery.append("           WHERE  1=1 ");
				sqlQuery.append("                  AND st.staff_type_id = s.staff_type_id ");
//				sqlQuery.append("                  AND s.status = 1 ");
				sqlQuery.append("                  AND st.status = 1 ");
				sqlQuery.append("                  AND s.staff_id in ("
						+ listStaff + " ) ");
				sqlQuery.append("                  AND s.staff_id <> ? ");
				params.add(staffOwnerId);
				sqlQuery.append("                  AND s.shop_id = ? ");
				params.add(shopId);
				sqlQuery.append("                  AND st.SPECIFIC_TYPE = ? ");
				params.add(staffType);
				sqlQuery.append("          UNION ");
				sqlQuery.append("           SELECT sh.staff_id AS staff_id, ");
				sqlQuery.append("                  sh.staff_code, ");
				sqlQuery.append("                  sh.staff_name, ");
				sqlQuery.append("                  sh.shop_id ");
				sqlQuery.append("           FROM staff_history sh ");
				sqlQuery.append("           WHERE 1=1 ");
				sqlQuery.append("                 AND sh.staff_id in ("
						+ listStaff + " ) ");
				sqlQuery.append("                 AND sh.shop_id = ? ");
				params.add(shopId);
				sqlQuery.append("                 AND sh.staff_id <> ? ");
				params.add(staffOwnerId);
				sqlQuery.append("                 AND sh.status =1 ");
				sqlQuery.append("                 AND substr(sh.from_date, 1, 10) <= ? ");
				params.add(dateNow);
				sqlQuery.append("                 AND substr(sh.from_date,1,10) >= substr(?,1,10) ");
				params.add(startOfCycle);
				sqlQuery.append("                 AND (substr(sh.to_date,1,10) >= substr(?,1,10) ");
				params.add(startOfCycle);
				sqlQuery.append("                      OR sh.to_date is null) ) ");
			} else if (sysSaleRoute == ApParamDTO.SYS_SALE_SHOP) {
				sqlQuery.append("       (SELECT shop_id       AS OBJECT_ID,  ");
				sqlQuery.append("               shop_code     AS OBJECT_CODE,  ");
				sqlQuery.append("               shop_name     AS OBJECT_NAME  ");
				sqlQuery.append("        FROM SHOP  ");
				sqlQuery.append("        WHERE 1=1  ");
				sqlQuery.append("              and status =1  ");
				sqlQuery.append("              and shop_id = ? ");
				params.add(shopId);
			}
			sqlQuery.append("     GROUP BY OBJECT_ID) st ");
			sqlQuery.append("  JOIN product_info pi ");
			sqlQuery.append("     ON rsc.product_info_id = pi.product_info_id ");
			sqlQuery.append(" WHERE 1 = 1 ");
			sqlQuery.append("       AND rsc.object_id = st.object_id ");
			sqlQuery.append("       AND rsc.status = 1 ");
//			sqlQuery.append("       AND pi.type = 1 ");
			sqlQuery.append("       AND rsc.object_type = ? ");
			params.add(sysSaleRoute+"");
			sqlQuery.append("       AND substr(rsc.create_date,1,10) = substr(?,1,10) ");
			params.add(startOfCycle);
			sqlQuery.append("       AND rsc.amount > 0 ");
			sqlQuery.append("       AND rsc.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append(" GROUP BY rsc.object_id ");
			sqlQuery.append(" ORDER BY st.object_code, ");
			sqlQuery.append("          st.object_name");

			c = this.rawQueries(sqlQuery.toString(), params);

			SupervisorReportStaffSaleItemDTO totalItem = new SupervisorReportStaffSaleItemDTO();
			totalItem.staffName = StringUtil.getString(R.string.TEXT_HEADER_TABLE_SUM);
			if (c.moveToFirst()) {
				do {
					SupervisorReportStaffSaleItemDTO item = new SupervisorReportStaffSaleItemDTO();
					item.parseDataFromCursor(c, sysCalUnApproved);
					result.listFocusInfoRow.add(item);

					totalItem.addAmountFrom(item);
				} while (c.moveToNext());
			}

			result.objectReportTotal = totalItem;

			result.numPlanDate = new EXCEPTION_DAY_TABLE(mDB).getPlanWorkingDaysOfMonth(new Date(), shopId);
			result.numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB).getCurrentWorkingDaysOfMonth(shopId);
			result.progressSales = StringUtil.calPercentUsingRound(result.numPlanDate , result.numPlanDateDone);
			result.beginDate = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(),0);
			result.endDate = new EXCEPTION_DAY_TABLE(mDB).getEndDayOfOffsetCycle(new Date(),0);
			result.numCycle = new EXCEPTION_DAY_TABLE(mDB).getNumCycle(new Date(),0);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * Lay bao cao tien do luy ke theo nganh hang
	 *
	 * @author: DungNX
	 * @param ext
	 * @return
	 * @return: SupervisorReportStaffSaleViewDTO
	 * @throws:
	 */
	public SupervisorReportStaffSaleViewDTO getTBHVTotalCatAmountReport(Bundle data)
			throws Exception {
		// phai khoi tao CAT_LIST cho nganh hang dong
		genCatList();
		String staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId()
				+ "";
		String listGs = PriUtils.getInstance().getListSupervisorOfASMIncludeHistory(staffId);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SupervisorReportStaffSaleViewDTO result = new SupervisorReportStaffSaleViewDTO();
		Cursor c = null;
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		try {
			var1.append("SELECT SHOP_ID SHOP_ID, ");
			var1.append("       SHOP_NAME SHOP_NAME, ");
			var1.append("       SHOP_CODE SHOP_CODE, ");
			var1.append("       STAFF_ID STAFF_ID, ");
			var1.append("       STAFF_CODE STAFF_CODE, ");
			var1.append("       STAFF_NAME STAFF_NAME, ");
			var1.append("       Group_concat(Cast(Ifnull(amount_plan, 0) AS INT)) CONCAT_AMOUNT_PLAN, ");
			var1.append("       Group_concat(Cast(Ifnull(amount, 0) AS INT))      CONCAT_AMOUNT, ");
			var1.append("       Group_concat(Ifnull(product_info_code, '')) ");
			var1.append("       CONCAT_PRODUCT_INFO_CODE, ");
			var1.append("       Group_concat(Ifnull(product_info_name, '')) ");
			var1.append("       CONCAT_PRODUCT_INFO_NAME ");
			var1.append("FROM   (SELECT sh.shop_id, ");
			var1.append("               sh.shop_name, ");
			var1.append("               sh.shop_code, ");
			var1.append("               gs.[staff_id]        STAFF_ID, ");
			var1.append("               gs.[staff_name]      STAFF_NAME, ");
			var1.append("               gs.[staff_code]      STAFF_CODE, ");
			var1.append("               Sum(rsc.amount_plan) AMOUNT_PLAN, ");
			var1.append("               Sum(rsc.amount)      AMOUNT, ");
			var1.append("               pi.product_info_code PRODUCT_INFO_CODE, ");
			var1.append("               pi.product_info_name PRODUCT_INFO_NAME ");
			var1.append("        FROM   rpt_staff_category rsc, ");
			// var1.append("               staff nv, ");//lay luon lich su
			var1.append("       		(SELECT s.staff_id, ");
			var1.append("                         s.staff_code, ");
			var1.append("                         s.staff_name, ");
			var1.append("                         s.mobilephone, ");
			var1.append("                         s.shop_id ");
			var1.append("                  FROM   staff s ");
			var1.append("                  UNION ");
			var1.append("                  SELECT sh.staff_id AS staff_id, ");
			var1.append("                         sh.staff_code, ");
			var1.append("                         sh.staff_name, ");
			var1.append("                         sh.mobilephone, ");
			var1.append("                         sh.shop_id ");
			var1.append("                  FROM   staff_history sh ");
			var1.append("                  WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var1.append("                  AND  Date(sh.from_date) >= Date('now', 'localtime','start of month' ) ");
			var1.append("                         AND (Date(sh.to_date) >= Date('now', 'localtime', ");
			var1.append("                                                  'start of month') or sh.to_date is null ) ");
			var1.append("                         AND sh.status = 1) nv, ");
			var1.append("       		(SELECT s.staff_id, ");
			var1.append("                         s.staff_code, ");
			var1.append("                         s.staff_name, ");
			var1.append("                         s.mobilephone, ");
			var1.append("                         s.shop_id ");
			var1.append("                  FROM   staff s ");
			var1.append("                  UNION ");
			var1.append("                  SELECT sh.staff_id AS staff_id, ");
			var1.append("                         sh.staff_code, ");
			var1.append("                         sh.staff_name, ");
			var1.append("                         sh.mobilephone, ");
			var1.append("                         sh.shop_id ");
			var1.append("                  FROM   staff_history sh ");
			var1.append("                  WHERE  Date(sh.from_date) <= Date('now', 'localtime') ");
			var1.append("                  AND  Date(sh.from_date) >= Date('now', 'localtime','start of month' ) ");
			var1.append("                         AND (Date(sh.to_date) >= Date('now', 'localtime', ");
			var1.append("                                                  'start of month') or sh.to_date is null ) ");
			var1.append("                         AND sh.status = 1) gs, ");
			// var1.append("               staff gs, ");
			var1.append("               shop sh ");
			var1.append("               JOIN product_info pi ");
			var1.append("                      ON rsc.product_info_id = pi.product_info_id ");
			var1.append("        WHERE  1 = 1 ");
			var1.append("       		AND rsc.shop_id = sh.shop_id ");
			var1.append("               AND sh.parent_shop_id = ? ");
			params.add(shopId);
			var1.append("               AND rsc.status = 1 ");
			var1.append("               AND rsc.[staff_id] = nv.[staff_id] ");
			var1.append("               AND rsc.[shop_id] = nv.[shop_id] ");
			var1.append("               AND rsc.[shop_id] = gs.[shop_id] ");
			var1.append("       		AND rsc.status = 1 ");
			var1.append("               AND gs.staff_id in (  ");
			var1.append(listGs);
			var1.append("               )  ");
			var1.append("               and nv.staff_id in  ");
			var1.append("			(SELECT PS.STAFF_ID FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			var1.append("				AND PS.PARENT_STAFF_ID = gs.staff_id AND PS.STATUS = 1 ");
			var1.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
//			var1.append("               and pi.type = 1  ");
			var1.append("               AND Date(rsc.create_date, 'start of month') = ");
			var1.append("                   Date('now', 'localtime', 'start of month') ");
			var1.append("        GROUP  BY sh.shop_id, gs.staff_id, pi.product_info_code, rsc.PRODUCT_INFO_ID  ) ");
			var1.append("GROUP  BY shop_code, ");
			var1.append("          staff_name ");

			c = this.rawQuery(var1.toString(), params.toArray(new String[params.size()]));

			SupervisorReportStaffSaleItemDTO totalItem = new SupervisorReportStaffSaleItemDTO();
			totalItem.staffName = StringUtil.getString(R.string.TEXT_HEADER_TABLE_SUM);
			if (c.moveToFirst()) {
				do {
					SupervisorReportStaffSaleItemDTO item = new SupervisorReportStaffSaleItemDTO();
					item.parseTBHVDataFromCursor(c);
					result.listFocusInfoRow.add(item);

					totalItem.addAmountFrom(item);
				} while (c.moveToNext());
			}

			result.objectReportTotal = totalItem;

			result.numPlanDate = new EXCEPTION_DAY_TABLE(mDB).getPlanWorkingDaysOfMonth(new Date(), shopId);
			result.numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB).getCurrentWorkingDaysOfMonth(shopId);
			result.progressSales = Math.floor(result.numPlanDate > 0 ? result.numPlanDateDone
					* 100.0 / result.numPlanDate
					: 0);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 * khoi tao ds nganh hang
	 *
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	private void genCatList() throws Exception{
		Cursor c = null;
		try {
			StringBuffer var1 = new StringBuffer();
			var1.append(" SELECT group_concat(PRODUCT_INFO_CODE) CONCAT_PRODUCT_INFO_CODE,  group_concat(PRODUCT_INFO_NAME) CONCAT_PRODUCT_INFO_NAME ");
			var1.append(" FROM   product_info ");
			var1.append(" WHERE  type = 1 and status = 1 ");
			var1.append(" GROUP BY TYPE ");

			String catString = "";
			String catNameString = "";
			c = this.rawQuery(var1.toString(), null);
			if (c.moveToFirst()) {
				catString = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_CODE");
				catNameString = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_NAME");
			}

			if (!StringUtil.isNullOrEmpty(catString)) {
				setCAT_LIST(catString.split(","));
				setCAT_NAME_LIST(catNameString.split(","));
			} else {
				setCAT_LIST(new String[]{""});
				setCAT_NAME_LIST(new String[]{""});
			}

		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

	}

	/**
	 * Danh sach khach hang theo nganh hang
	 * @author: hoanpd1
	 * @since: 17:57:22 07-04-2015
	 * @return: ReportCategoryViewDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public SupervisorReportStaffSaleViewDTO getReportCategory(Bundle data)
			throws Exception {
		SupervisorReportStaffSaleViewDTO result = new SupervisorReportStaffSaleViewDTO();
		Cursor c = null;
		// phai khoi tao CAT_LIST cho nganh hang dong
		genCatList();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		ArrayList<String> params = new ArrayList<String>();
		try {
			StringBuffer  sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT st.OBJECT_ID                                                    AS OBJECT_ID, ");
			sqlQuery.append("       st.OBJECT_CODE                                                  AS OBJECT_CODE, ");
			sqlQuery.append("       st.OBJECT_NAME                                                  AS OBJECT_NAME, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.amount_plan,0) AS INT))            AS CONCAT_AMOUNT_PLAN, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.amount,0) AS INT))                 AS CONCAT_AMOUNT, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.amount_approved,0) AS INT))        AS CONCAT_AMOUNT_APPROVED, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.amount_pending,0) AS INT))         AS CONCAT_AMOUNT_PENDING, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.quantity_plan,0) AS INT))          AS CONCAT_QUANTITY_PLAN, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.quantity,0) AS INT))               AS CONCAT_QUANTITY, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.quantity_approved,0) AS INT))      AS CONCAT_QUANTITY_APPROVED, ");
			sqlQuery.append("       Group_concat(Cast(Ifnull(rsc.quantity_pending,0) AS INT))       AS CONCAT_QUANTITY_PENDING, ");
			sqlQuery.append("       Group_concat(Ifnull(pi.product_info_code,''))                   AS CONCAT_PRODUCT_INFO_CODE, ");
			sqlQuery.append("       Group_concat(Ifnull(pi.product_info_name,''))                   AS CONCAT_PRODUCT_INFO_NAME ");
			sqlQuery.append("FROM rpt_staff_category rsc, ");
			sqlQuery.append("     (select ct.customer_id             AS OBJECT_ID, ");
			sqlQuery.append("             ct.short_code              AS OBJECT_CODE, ");
			sqlQuery.append("             ct.customer_name           AS OBJECT_NAME ");
			sqlQuery.append("      from visit_plan vp, ");
			sqlQuery.append("           routing rt, ");
			sqlQuery.append("           routing_customer rtc, ");
			sqlQuery.append("           customer ct ");
			sqlQuery.append("      where 1=1 ");
			sqlQuery.append("            and vp.status in (0,1) ");
			sqlQuery.append("            and rt.status = 1 ");
			sqlQuery.append("            and rtc.status in (0,1) ");
			sqlQuery.append("            and ct.status = 1 ");
			sqlQuery.append("            and vp.staff_id = ? ");
			params.add(staffId);
			sqlQuery.append("            and vp.routing_id = rt.routing_id ");
			sqlQuery.append("            and rt.routing_id = rtc.routing_id ");
			sqlQuery.append("            and rtc.customer_id  = ct.customer_id ");
			sqlQuery.append("            and substr(vp.from_date,1,10) <= ? ");
			params.add(dateNow);
			sqlQuery.append("            and (substr(vp.to_date,1,10) >= ? or substr(vp.to_date,1,10) is null) ");
			params.add(dateNow);
			sqlQuery.append("            and substr(rtc.start_date,1,10) <= ? ");
			params.add(dateNow);
			sqlQuery.append("            and (substr(rtc.end_date,1,10) >= ? or substr(rtc.end_date,1,10) is null) ");
			params.add(dateNow);
			sqlQuery.append("      group by object_id )st ");
			sqlQuery.append("JOIN product_info pi ");
			sqlQuery.append("    ON rsc.product_info_id = pi.product_info_id ");
			sqlQuery.append("WHERE 1 = 1 ");
			sqlQuery.append("      AND rsc.customer_id = st.OBJECT_ID ");
			sqlQuery.append("      AND rsc.status = 1 ");
//			sqlQuery.append("      AND pi.type = 1 ");
			sqlQuery.append("      AND rsc.object_type  = 4 ");
			sqlQuery.append("      AND substr(rsc.create_date, 1, 10) = ? ");
			params.add(startOfMonth);
			sqlQuery.append("      AND rsc.amount > 0 ");
			sqlQuery.append("      AND rsc.shop_id = ? ");
			params.add(shopId);
			sqlQuery.append("GROUP BY rsc.object_id ");
			sqlQuery.append("ORDER BY st.OBJECT_CODE, ");
			sqlQuery.append("         st.OBJECT_NAME");
			c = this.rawQueries(sqlQuery.toString(), params);

			SupervisorReportStaffSaleItemDTO totalItem = new SupervisorReportStaffSaleItemDTO();
			totalItem.staffName = StringUtil.getString(R.string.TEXT_HEADER_TABLE_SUM);
			if (c.moveToFirst()) {
				do {
					SupervisorReportStaffSaleItemDTO item = new SupervisorReportStaffSaleItemDTO();
					item.parseDataFromCursor(c, sysCalUnApproved);
					result.listFocusInfoRow.add(item);

					totalItem.addAmountFrom(item);
				} while (c.moveToNext());
			}

			result.objectReportTotal = totalItem;

			result.numPlanDate = new EXCEPTION_DAY_TABLE(mDB).getPlanWorkingDaysOfMonth(new Date(), shopId);
			result.numPlanDateDone = new EXCEPTION_DAY_TABLE(mDB).getCurrentWorkingDaysOfMonth(shopId);

			result.progressSales = Math.floor(result.numPlanDate > 0 ? result.numPlanDateDone
					* 100.0 / result.numPlanDate
					: 0);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	public String[] getCAT_LIST() {
		return CAT_LIST;
	}

	public void setCAT_LIST(String[] cAT_LIST) {
		CAT_LIST = cAT_LIST;
	}

	public String[] getCAT_NAME_LIST() {
		return CAT_NAME_LIST;
	}

	public void setCAT_NAME_LIST(String[] cAT_NAME_LIST) {
		CAT_NAME_LIST = cAT_NAME_LIST;
	}
}
