package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PROMOTION_PRODUCT_OPEN_TABLE;
import com.ths.dmscore.util.CursorUtil;

public class PromotionProductOpenDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public long promotionProductOpenId;
	public long productId;
	public long promotionProgramId;
	public long quantity;
	public long amount;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public int status;
	
	/** Khoi tao tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		promotionProgramId = CursorUtil.getLong(c, PROMOTION_PRODUCT_OPEN_TABLE.PROMOTION_PROGRAM_ID);
		productId = CursorUtil.getLong(c, PROMOTION_PRODUCT_OPEN_TABLE.PRODUCT_ID);
		quantity = CursorUtil.getLong(c, PROMOTION_PRODUCT_OPEN_TABLE.QUANTITY);
		amount = CursorUtil.getLong(c, PROMOTION_PRODUCT_OPEN_TABLE.AMOUNT);
	}
}