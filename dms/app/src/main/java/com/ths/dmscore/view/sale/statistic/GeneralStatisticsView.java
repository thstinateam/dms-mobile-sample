/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.AttendanceDTO;
import com.ths.dmscore.dto.view.CustomerVisitedViewDTO;
import com.ths.dmscore.dto.view.GeneralStatisticsInfoViewDTO;
import com.ths.dmscore.dto.view.ReportInfoDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.salestatistics.PopupCustomerVisitedDetailView;
import com.ths.dms.R;

/**
 *
 * UI for general statistics view for application
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class GeneralStatisticsView extends BaseFragment implements
		OnEventControlListener, OnClickListener, VinamilkTableListener {

	// thong ke chung
	private static final int ACTION_MENU_GENERAL_STATISTICS = 1;
	// mat hang trong tam
	private static final int ACTION_MENU_MHTT = 2;
	// can thuc hien
	private static final int ACTION_MENU_NEED_DONE = 3;
	// CTTB
	private final int MENU_REPORT_DISPLAY_PROGRESS = 4;
	// Bao cao doanh so nganh hang
	private final int ACTITON_MENU_REPORT_AMOUNT_CATEGORY = 5;
	// Bao cao san luong nganh hang
	private final int ACTITON_MENU_REPORT_QUANTITY_CATEGORY = 6;
	// thong ke chi tiet
	private final int ACTION_GO_TO_REPORT_STAFF_SALE_DETAIL = 7;

	private final int ACTION_CUSTOMER_VISITED_DETAIL = 8;
	private final int ACTION_CLOSE_CUSTOMER_VISITED_DETAIL = 9;

	// action refresh on header top right
	public static final int REFRESH_ACTION = 0;
	// table cham cong va thoi gian ghe tham trong ngay
	DMSTableView tbTime;
	// table general statistics for fullDate
	DMSTableView tbStatisticsListDay;
	// table general statistics for month
	DMSTableView tbStatisticsListMonth;
	// bao cao theo nganh hang
	DMSTableView tbReportCat;

	// number fullDate sale plan
	private TextView tvNumDaySalePlan;
	// num fullDate sold
	private TextView tvNumDaySold;
	// percent sold
	private TextView tvSoldPercent;

	// linner layout screen
	LinearLayout llScreen;

	// text header for table report in month
	TextView tvHeaderTableReportMonth;
	// text header for table report cat
	TextView tvHeaderReportCat;

	// general statistics info view
	GeneralStatisticsInfoViewDTO generalStatisticsInfo = new GeneralStatisticsInfoViewDTO();

	boolean isLoadData = false;
	// title for header of report month
	String headerTableReportMonth = Constants.STR_BLANK;

	// alert popup visit customer detail
	AlertDialog alertCustomerVisitedDetail;
	PopupCustomerVisitedDetailView popupCustomerVisitedDetailView;
	CustomerVisitedViewDTO customerVisitedViewDTO;
	private static String strQuantity = "DS";
	private static String strAmount = "SL";

	/**
	 *
	 * method get instance
	 *
	 * @author: HaiTC3
	 * @param index
	 * @return
	 * @return: GeneralStatisticsView
	 * @throws:
	 */
	public static GeneralStatisticsView getInstance(Bundle data) {
		GeneralStatisticsView instance = new GeneralStatisticsView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_general_statistics_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_THONGKECHUNG_THONGKE);
		// init 2 table statistics list for screen
		initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_GENERAL_STATISTICS));
		if (!this.isLoadData) {
			getGeneralStatisticsReportInfo();
		} else {
			llScreen.setVisibility(View.VISIBLE);
			this.renderLayout();
		}
		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initHeadermenu();
	}

	/**
	 *
	 * init header for menu
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	private void initHeadermenu() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(
				PriHashMap.PriForm.NVBH_THONGKECHUNG_THONGKE,
//				new MenuTab(R.string.TEXT_HEADER_MENU_QUANTITY_CATEGORY, R.drawable.icon_list_check,
//						ACTITON_MENU_REPORT_QUANTITY_CATEGORY, PriForm.NVBH_NGANHHANG_SANLUONG),
//				new MenuTab(R.string.TEXT_HEADER_MENU_AMOUNT_CATEGORY, R.drawable.icon_list_check,
//						ACTITON_MENU_REPORT_AMOUNT_CATEGORY, PriForm.NVBH_NGANHHANG_DOANHSO),
				new MenuTab(R.string.TEXT_HEADER_MENU_CTTB, R.drawable.icon_list_check,
						MENU_REPORT_DISPLAY_PROGRESS, PriHashMap.PriForm.NVBH_BAOCAOCTTB),
//				new MenuTab(R.string.TEXT_HEADER_MENU_NEED_DONE, R.drawable.icon_order,
//						ACTION_MENU_NEED_DONE, PriForm.NVBH_THONGKECHUNG_CANTHUCHIEN),
				new MenuTab(R.string.TEXT_HEADER_MENU_MHTT, R.drawable.icon_list_star,
						ACTION_MENU_MHTT, PriHashMap.PriForm.NVBH_BAOCAOMHTT),
				new MenuTab(R.string.TEXT_HEADER_MENU_GENERAL_STATISTICS, R.drawable.icon_reminders,
						ACTION_MENU_GENERAL_STATISTICS, PriHashMap.PriForm.NVBH_THONGKECHUNG_THONGKE));
	}

	/**
	 *
	 * get list customer visit detail
	 *
	 * @param dto
	 * @return: void
	 * @throws:
	 * @author: DungNX
	 * @date: Nov 7, 2012
	 */
	private void getListCustomerVisited(int page, int isGetTotalPage){
		this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_PAGE, page);
		bundle.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotalPage);
		bundle.putInt(
				IntentConstants.INTENT_STAFF_ID,
				GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId());
		bundle.putString(
				IntentConstants.INTENT_SHOP_ID,
				GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId());
		handleViewEvent(bundle, ActionEventConstant.GET_LIST_CUSTOMER_VISITED, SaleController.getInstance());
	}

	/**
	 * lay thong tin bao cao theo ngay va theo thang
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void getGeneralStatisticsReportInfo() {
		parent.showLoadingDialog();
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_STAFF_ID, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(bundle, ActionEventConstant.GET_GENERAL_STATISTICS_REPORT_INFO, SaleController.getInstance());
	}

	/**
	 * init 2 table for list statistics for fullDate and month
	 *
	 * @author: HaiTC3
	 * @param v
	 * @return: void
	 * @throws:
	 */
	public void initView(View v) {
		tbStatisticsListDay = (DMSTableView) v
				.findViewById(R.id.tbStatisticsListDay);
		initHeaderTable(tbStatisticsListDay, new GeneralStatisticsReportRowDay(
				parent, null));
		tbTime = (DMSTableView) v
				.findViewById(R.id.tbTime);
		tbStatisticsListMonth = (DMSTableView) v
				.findViewById(R.id.tbStatisticsListMonth);

		tvHeaderTableReportMonth = (TextView) v
				.findViewById(R.id.tvHeaderTableReportMonth);
		tvHeaderReportCat = (TextView) v
				.findViewById(R.id.tvHeaderReportCat);
		// truong hop ko co gia  thi show title la san luong thoi
		if(!GlobalInfo.getInstance().isSysShowPrice())
			tvHeaderReportCat.setText(StringUtil.getString(R.string.TEXT_HEADER_REPORT_QUANTITY_CAT));
		tbReportCat = (DMSTableView) v
				.findViewById(R.id.tbReportCat);

		initHeaderTable(tbStatisticsListMonth, new GeneralStatisticsReportRowMonth(
				parent, null));
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String dateNow = formatter.format(currentDate.getTime());
		headerTableReportMonth = StringUtil
				.getString(R.string.REPORT_SALE_IN_MONTH);
		headerTableReportMonth += " " + dateNow;
		tvHeaderTableReportMonth.setText(headerTableReportMonth);

		llScreen = (LinearLayout) v.findViewById(R.id.llScreen);
		llScreen.setVisibility(View.GONE);

		tvNumDaySalePlan = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySalePlan, PriHashMap.PriControl.NVBH_THONGKE_NGAYBANHANGKEHOACH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySalePlanTitle, PriHashMap.PriControl.NVBH_THONGKE_NGAYBANHANGKEHOACH);
		tvNumDaySold = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySold, PriHashMap.PriControl.NVBH_THONGKE_NGAYBANHANGDAQUA);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySoldTitle, PriHashMap.PriControl.NVBH_THONGKE_NGAYBANHANGDAQUA);
		tvSoldPercent = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldPercent, PriHashMap.PriControl.NVBH_THONGKE_TIENDOCHUAN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldPercentTitle, PriHashMap.PriControl.NVBH_THONGKE_TIENDOCHUAN);
	}

	/**
	 *
	 * init layout for screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		// report time
		ArrayList<String> listTitle = new ArrayList<String>();
		listTitle.add(StringUtil.getString(R.string.TEXT_TAKE_ATTENDANCE));
		if (generalStatisticsInfo.listInfo.size() >= 3) {
			String description1 = generalStatisticsInfo.listInfo.get(0).getDescription();
			String description2 = generalStatisticsInfo.listInfo.get(1).getDescription();
			StringBuilder sb =  new StringBuilder();
			sb.append(description1);
			sb.append(" - ");
			sb.append(description2);
			listTitle.add(sb.toString());
			sb = new StringBuilder();
			sb.append(StringUtil.getString(R.string.TEXT_ATTENDANCE_AFTER));
			sb.append(Constants.STR_SPACE);
			sb.append(description2);
			listTitle.add(sb.toString());
		} else {
			listTitle.add(Constants.STR_BLANK);
			listTitle.add(Constants.STR_BLANK);
		}

		listTitle.add(StringUtil.getString(R.string.TEXT_TAKE_ATTENDANCE_TIME));

		initHeaderTable(tbTime, new GeneralStatisticsReportRowTime(parent, null), listTitle);

		tbTime.clearAllData();
		GeneralStatisticsReportRowTime rowTime = new GeneralStatisticsReportRowTime(
				parent, this);

		rowTime.setClickable(true);
		rowTime.renderLayout(generalStatisticsInfo);
		rowTime.setAction(ACTION_CUSTOMER_VISITED_DETAIL);
		tbTime.addRow(rowTime);

		tbTime.setTotalSize(1,1);
//		tbTime.addRow(rowTime);

//		tbTime.setTotalSize(1);
//		tbTime.getPagingControl().setVisibility(View.GONE);
//		tbTime.addData(getActivity(), generalStatisticsInfo, ACTION_CUSTOMER_VISITED_DETAIL);

		// end report time

		int pos = 1;
		// update layout for header screen
		if (this.generalStatisticsInfo != null) {
			tvNumDaySalePlan.setText(String
					.valueOf(this.generalStatisticsInfo.numberDayPlan));
			tvNumDaySold.setText(String
					.valueOf(this.generalStatisticsInfo.numberDaySold));
			StringUtil.displayPercent(tvSoldPercent, this.generalStatisticsInfo.progressSold);
		}
		tvHeaderTableReportMonth.setText(headerTableReportMonth);
		// list general statistics for fullDate
		tbStatisticsListDay.clearAllData();
		for (final ReportInfoDTO dto : this.generalStatisticsInfo.listReportDate) {
			GeneralStatisticsReportRowDay row = new GeneralStatisticsReportRowDay(
					parent, this);
			row.setTag(Integer.valueOf(pos));
			row.renderLayout(pos, dto);
			if (strAmount.equals(dto.code) || strQuantity.equals(dto.code)) {
				SpannableObject obj = new SpannableObject();
				obj.addSpan(row.tvContent.getText().toString(),
						ImageUtil.getColor(R.color.BLACK),
						Typeface.NORMAL, null);

				SpannableString sText = obj.getSpan();
				row.tvContent.setText(sText);
//				row.setAction(ACTION_GO_TO_REPORT_STAFF_SALE_DETAIL);
			}
			pos++;
			// co cau hinh san luong moi add vao
			if (strAmount.equals(dto.code)) {
				if (GlobalInfo.getInstance().isSysShowPrice())
					tbStatisticsListDay.addRow(row);
			}else{
				tbStatisticsListDay.addRow(row);
			}

		}
		if (this.generalStatisticsInfo.listReportDate.size() == 0) {
			tbStatisticsListDay.showNoContentRow();
		}

		tbStatisticsListDay.setTotalSize(1,1);

		// list general statistics for month
		tbStatisticsListMonth.clearAllData();
		for (final ReportInfoDTO dto : this.generalStatisticsInfo.listReportMonth) {
			GeneralStatisticsReportRowMonth row = new GeneralStatisticsReportRowMonth(
					parent, this);

			row.setClickable(true);

			row.setTag(Integer.valueOf(pos));
			row.renderLayout(pos, dto, generalStatisticsInfo.progressSold);
			pos++;

			// co cau hinh san luong moi add vao
			if (ReportInfoDTO.TYPE_AMOUNT.equals(dto.code)) {
				if (GlobalInfo.getInstance().isSysShowPrice())
					tbStatisticsListMonth.addRow(row);
			} else {
				tbStatisticsListMonth.addRow(row);
			}
		}
		tbStatisticsListMonth.setTotalSize(1,1);

		// list general statistics for month
		tbReportCat.clearAllDataAndHeader();
		initHeaderTable(tbReportCat, new GeneralStatisticsReportCatRow(
					parent, this));

		ReportInfoDTO sumItem = new ReportInfoDTO();
		ReportInfoDTO dtoPSDS = new ReportInfoDTO();
		for ( ReportInfoDTO dto : this.generalStatisticsInfo.listReportCat) {
			if (dto.reportType == ReportInfoDTO.REPORT_DETAIL) {
				GeneralStatisticsReportCatRow row = new GeneralStatisticsReportCatRow(
						parent, this);

				row.setClickable(true);

				row.renderLayout(dto, generalStatisticsInfo.progressSold);
				sumItem.amountApproved += dto.amountApproved;
				sumItem.amount += dto.amount;
				sumItem.amountPending += dto.amountPending;
				sumItem.amountPlan += dto.amountPlan;
				sumItem.quantityApproved += dto.quantityApproved;
				sumItem.quantity += dto.quantity;
				sumItem.quantityPending += dto.quantityPending;
				sumItem.quantityPlan += dto.quantityPlan;
				tbReportCat.addRow(row);
			}else{
				dtoPSDS = dto;
			}
		}
		if (this.generalStatisticsInfo.listReportCat.size() > 1) {
			GeneralStatisticsReportCatRow rowSum = new GeneralStatisticsReportCatRow(
					parent, this);
			rowSum.renderSum(sumItem, generalStatisticsInfo.progressSold);
			tbReportCat.addRow(rowSum);
		}
		GeneralStatisticsReportCatRow row = new GeneralStatisticsReportCatRow(
				parent, this);
		row.setClickable(true);
		row.renderLayout(dtoPSDS, generalStatisticsInfo.progressSold);
		tbReportCat.addRow(row);




	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_GENERAL_STATISTICS_REPORT_INFO:
			llScreen.setVisibility(View.VISIBLE);
			generalStatisticsInfo = (GeneralStatisticsInfoViewDTO) modelEvent
					.getModelData();
			this.isLoadData = true;

			boolean checkAttendance=true;
			// shop khong co toa do
			if (generalStatisticsInfo.shopPosition.lat <= 0
					&& generalStatisticsInfo.shopPosition.lng <= 0) {
				checkAttendance = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_TAKE_ATTENDANCE_SHOP_HAVE_NO_POSITION));
			}

			// shop chua khai bao thoi gian cham cong
			if(generalStatisticsInfo.listInfo.size()<3){
				checkAttendance = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_TAKE_ATTENDANCE_SHOP_HAVE_NO_TIME));
			} else {
				for(ApParamDTO temp :generalStatisticsInfo.listInfo){
					if (StringUtil.isNullOrEmpty(temp.getDescription())
							|| StringUtil.isNullOrEmpty(temp.getValue())) {
						checkAttendance = false;
						parent.showDialog(StringUtil.getString(R.string.TEXT_TAKE_ATTENDANCE_SHOP_HAVE_NO_TIME));
						break;
					}
				}
			}

			if(checkAttendance){
				this.checkAttendance();
			}

			this.renderLayout();
			this.parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_LIST_CUSTOMER_VISITED:
			if (customerVisitedViewDTO != null
					&& customerVisitedViewDTO.totalCustomer > 0) {
				customerVisitedViewDTO.arrWrong = ((CustomerVisitedViewDTO) modelEvent
						.getModelData()).arrWrong;
			} else {
				customerVisitedViewDTO = (CustomerVisitedViewDTO) modelEvent
						.getModelData();
			}
			showCustomerVisitedDetail();
			this.parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 *
	 * display popup visit customer detail
	 *
	 * @param dto
	 * @return: void
	 * @throws:
	 * @author: DungNX
	 * @date: Nov 7, 2012
	 */
	private void showCustomerVisitedDetail() {
		if (alertCustomerVisitedDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			popupCustomerVisitedDetailView = new PopupCustomerVisitedDetailView(this, parent,
					this, ACTION_CLOSE_CUSTOMER_VISITED_DETAIL);
			build.setView(popupCustomerVisitedDetailView.viewLayout);
			alertCustomerVisitedDetail = build.create();
			Window window = alertCustomerVisitedDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		GlobalUtil.forceHideKeyboard(this.parent);
		if (customerVisitedViewDTO != null) {
			popupCustomerVisitedDetailView
					.renderLayoutWithObject(customerVisitedViewDTO);
		}
		if (!alertCustomerVisitedDetail.isShowing()) {
			alertCustomerVisitedDetail.show();
		}
	}

	/**
	 *
	 * thuc hien cham cong
	 *
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	private void checkAttendance(){
		if(generalStatisticsInfo.attendanceDTO.size()==0){
			return;
		}
		// thuc hien cham cong
		// cham cong duoc luu vao generalStatisticsInfo.attendance
		AttendanceDTO attDTO = new AttendanceDTO();
		boolean validated=false;
		if (generalStatisticsInfo.attendanceDTO.size() > 0) {
			for (AttendanceDTO attTemp : generalStatisticsInfo.attendanceDTO) {
				if (!StringUtil.isNullOrEmpty(attTemp.time1)) {
					SimpleDateFormat formatter = new SimpleDateFormat(
							DateUtils.DATE_FORMAT_ATTENDANCE);
					Date createDate = new Date();
					Date startDate = new Date();
					Date endDate = new Date();

					String startTime = attTemp.time1.split(" ")[0] + " "
							+ generalStatisticsInfo.listInfo.get(0).getValue();
					String endTime = attTemp.time1.split(" ")[0] + " "
							+ generalStatisticsInfo.listInfo.get(1).getValue();

					try {
						createDate = formatter.parse(attTemp.time1);
						startDate = formatter.parse(startTime);
						endDate = formatter.parse(endTime);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
					}

					// Trong thoi gian cho phep
					if (createDate.compareTo(startDate) >= 0
							&& createDate.compareTo(endDate) <= 0) {
						// Neu cham cong chua hop le thi moi lay record
						// nay
						if (StringUtil.isNullOrEmpty(attDTO.time1)
								|| (attDTO.onTime == false && attTemp.distance1 < attDTO.distance1)) {
							attDTO.distance1 = attTemp.distance1;
							attDTO.time1 = attTemp.time1;
							attDTO.position1.lat = attTemp.position1.lat;
							attDTO.position1.lng = attTemp.position1.lng;

							if (attDTO.distance1 > Double
									.parseDouble(generalStatisticsInfo.listInfo
											.get(2).getValue())) {
								attDTO.onTime = false;
							} else {
								attDTO.onTime = true;
							}
							validated = true;
						} else {
						}
						// Sau thoi gian cho phep & chua co log hop le
						// trong khoang thoi gian cho phep
					} else if (createDate.compareTo(endDate) > 0
							&& attDTO.onTime == false) {
						if (attTemp.distance1 <= Double
								.parseDouble(generalStatisticsInfo.listInfo
										.get(2).getValue())
								&& StringUtil.isNullOrEmpty(attDTO.time2)) {
							attDTO.time2 = attTemp.time1;
							attDTO.position2.lat = attTemp.position1.lat;
							attDTO.position2.lng = attTemp.position1.lng;
							attDTO.distance2 = attTemp.distance1;
							if(!validated){
								attDTO.time1="";
							}
							validated = true;
						}
					} else if (!validated) {// Truoc thoi gian cho phep
						attDTO.time1 = "";
						attDTO.time2 = "";
						attDTO.position2.lat = -1;
						attDTO.position2.lng = -1;
						attDTO.distance2 = -1;
					}
				}
			}
		}
		if (validated) {
			generalStatisticsInfo.attendance = attDTO;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		super.handleErrorModelViewEvent(modelEvent);
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_GENERAL_STATISTICS_REPORT_INFO:
			this.parent.closeProgressDialog();
			break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case REFRESH_ACTION:
			refreshScreen();
			break;
		case ACTION_MENU_MHTT:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW, UserController.getInstance());
			break;
		case ACTION_MENU_NEED_DONE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW, UserController.getInstance());
			break;
		case MENU_REPORT_DISPLAY_PROGRESS:
			handleSwitchFragment(null, ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY, UserController.getInstance());
			break;
		case ACTION_CLOSE_CUSTOMER_VISITED_DETAIL:{
			if (this.alertCustomerVisitedDetail.isShowing()) {
				this.alertCustomerVisitedDetail.dismiss();
			}
			break;
		}
		case ACTITON_MENU_REPORT_AMOUNT_CATEGORY:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REPORT_AMOUNT_CATEGORY, UserController.getInstance());
			break;
		case ACTITON_MENU_REPORT_QUANTITY_CATEGORY:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REPORT_QUANTITY_CATEGORY, UserController.getInstance());
			break;
		default:
			break;
		}
	}


	/**
	 *
	 * refresh screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void refreshScreen() {
		// TODO refresh screen
		getGeneralStatisticsReportInfo();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				customerVisitedViewDTO = null;
				if (popupCustomerVisitedDetailView!=null) {
					popupCustomerVisitedDetailView.currentPage = -1;
				}
				getGeneralStatisticsReportInfo();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		if (control == popupCustomerVisitedDetailView.tbCustomerList) {
			popupCustomerVisitedDetailView.currentPage = popupCustomerVisitedDetailView.tbCustomerList
					.getPagingControl().getCurrentPage();
			getListCustomerVisited(popupCustomerVisitedDetailView.currentPage,1);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub
		switch (action) {
		case ACTION_CUSTOMER_VISITED_DETAIL: {
			// if (customerVisitedViewDTO != null
			// && popupCustomerVisitedDetailView != null
			// && customerVisitedViewDTO.totalCustomer > 0) {
			// getListCustomerVisited(
			// popupCustomerVisitedDetailView.currentPage, 0);
			// } else {
			getListCustomerVisited(1, 1);
			if (popupCustomerVisitedDetailView != null) {
				popupCustomerVisitedDetailView.currentPage = -1;
			}
			// }
			break;
		}case ACTION_GO_TO_REPORT_STAFF_SALE_DETAIL: {
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REPORT_STAFF_SALE_DETAIL, SaleController.getInstance());
			break;
		}case ActionEventConstant.GO_TO_CUSTOMER_LIST_DO_NOT_AMOUNT_IN_MONTH:
			handleSwitchFragment(null, ActionEventConstant.ACTION_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH_SALE, SaleController.getInstance());
			break;

		default:
			break;
		}

	}

}
