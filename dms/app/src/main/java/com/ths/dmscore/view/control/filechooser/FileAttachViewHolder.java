/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.util.TextDrawable.ColorGenerator;
import com.ths.dmscore.util.TextDrawable.TextDrawable;
import com.ths.dmscore.dto.view.filechooser.FileAttachEvent;
import com.ths.dms.R;

public class FileAttachViewHolder implements View.OnClickListener {
	static ColorGenerator colorGenerator = ColorGenerator.getMATERIAL();
	private final ImageView ivFileType;
	private final TextView tvFileName;
	private final ImageView ivDelete;
	public View view;
	int pos = -1;
	public FileInfo info;
	FileAttachEvent fileAttachEvent;
	boolean isCanRemoveFile;

	public FileAttachViewHolder(Context context, ViewGroup parent,
			FileAttachEvent fileChooseEvent, boolean isCanRemoveFile) {
		this.isCanRemoveFile = isCanRemoveFile;
		this.fileAttachEvent = fileChooseEvent;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		view = inflater.inflate(R.layout.item_file_attach, parent, false);
		view.setOnClickListener(this);
		ivFileType = (ImageView) view.findViewById(R.id.ivFileType);
		tvFileName = (TextView) view.findViewById(R.id.tvFileName);
		ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
		ivDelete.setOnClickListener(this);
		view.setOnClickListener(this);
		if (isCanRemoveFile) {
			ivDelete.setVisibility(View.VISIBLE);
		} else{
			ivDelete.setVisibility(View.GONE);
		}
	}

	public void render(FileInfo info, int pos) {
		this.pos = pos;
		this.info = info;
		String key = this.info.extension.toUpperCase();
		int color = colorGenerator.getColor(key);
		if (key.length() > 3) {
			key = key.substring(0, 3);
		}
		TextDrawable drawable = TextDrawable.builder().buildRect(key, color);
		ivFileType.setImageDrawable(drawable);
		tvFileName.setText(info.fileName);
	}

	@Override
	public void onClick(View view) {
		if (ivDelete == view) {
			fileAttachEvent.onRemoveAttach(this.pos);
		} else if(this.view == view){
			fileAttachEvent.onOpenFile(this.info);
		}
	}
}