/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.graphchart;

import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;

import android.content.Context;
import android.util.AttributeSet;

import com.ths.dmscore.dto.view.ChartDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

/**
 * LineChartView.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 11:41:37 AM Oct 3, 2014
 */
public final class LineChartView extends
		AbstractChartView {

	private XYMultipleSeriesDataset xYDataset;

	public LineChartView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

	public LineChartView(
			Context context,
			AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

	public LineChartView(
			Context context,
			AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		xYDataset = new XYMultipleSeriesDataset();
	}

//	public void drawChart(String titleChart, String columnName, String rowName,
//			int[] listColor, String[] annotation, List<double[]> listData, String[] lstColumnName) {
//		setData(annotation,listData,lstColumnName);
//		buildRenderer(listColor);
//		setRenderSetting(titleChart, columnName, rowName);
//		chartView = ChartFactory.getLineChartView(con, xYDataset, renderer);
//		addView(chartView);
//	}
	
	 /**
	 * Ve chart
	 * @author: Tuanlt11
	 * @param titleChart
	 * @param columnName
	 * @param rowName
	 * @param listColor
	 * @param annotation
	 * @param lstValues
	 * @param lstColumnName
	 * @return: void
	 * @throws:
	*/
	public void drawChart(String titleChart, String columnName, String rowName,
			int[] listColor, String[] annotation, List<ChartDTO> lstValues, String[] lstColumnName) {
//		if ((listColor.length != x.length)
//				|| (listColor.length != annotation.length)
//				|| (annotation.length != x.length)
//				|| (listData.size() != annotation.length)) {
//			return;
//		}
		setDataValues(annotation,lstValues,lstColumnName);
		buildRenderer(listColor);
		setPanLimit(true, false);
		setRenderSetting(titleChart, columnName, rowName);
		chartView = ChartFactory.getLineChartView(con, xYDataset, renderer);
		addView(chartView);
	}
	
	/**
	 * Ve du lieu theo cot y
	 * 
	 * @author: dungdq3
	 * @since: 2:27:14 PM Oct 3, 2014
	 * @return: void
	 * @throws:
	 * @param x
	 * @param annotation
	 * @param listData
	 *            :
	 * @param lstIndexChoose : danh sach cac vi tri tuong ung de ve
	 */
	private void buildDataSet(String[] annotation, List<ChartDTO> listData) {
		int length = annotation.length;
		renderer.setXAxisMax(length + 1);
		arrMaxValues = new double[length];
		for (int i = 0; i < length; i++) {
			XYSeries series = new XYSeries(annotation[i], 0);
			double max = 0;
			for(int j = 0, size = listData.size(); j < size; j++){
				double[] yValues = listData.get(j).value;
				double maxTemp = yValues[0];
				series.add(j+1,yValues[i]);
				if (maxTemp < yValues[i])
					maxTemp = yValues[i];
				if(max < maxTemp)
					max = maxTemp;
			}
			arrMaxValues[i] = max;
			xYDataset.addSeries(series);
		}

		findMaxNumber();
	}
	
	/**
	 * set du lieu de render
	 * 
	 * @author: Tuanlt11
	 * @param listData
	 * @param annotation
	 * @param lstColumnName
	 * @param lstIndexChoose
	 * @return: void
	 * @throws:
	 */
//	public void setData(String[] annotation, List<double[]> listData,
//			String[] lstColumnName) {
//		buildDataSet(annotation, listData);
//		renderer.setXLabels(0);
//		for (int i = 0, size = lstColumnName.length; i < size; i++) {
//			renderer.addXTextLabel(i + 1, lstColumnName[i]);
//		}
//
//	}
	
	 /**
	 * set gia tri cho cot x
	 * @author: Tuanlt11
	 * @param annotation
	 * @param listData
	 * @param lstColumnName
	 * @return: void
	 * @throws:
	*/
	public void setDataValues(String[] annotation, List<ChartDTO> listData,
			String[] lstColumnName) {
		buildDataSet(annotation, listData);
		renderer.setXLabels(0);
		int index = 1;
		int position = 0;// vi tri cua mang ten
		int size = lstColumnName.length;
		while (position < size) {
			if (isShowPlan) {
				renderer.addXTextLabel(index, lstColumnName[position] + "-"
						+ StringUtil.getString(R.string.TEXT_KH));
				index++;
			}
			if (isShowReality) {
				renderer.addXTextLabel(index, lstColumnName[position] + "-"
						+ StringUtil.getString(R.string.TEXT_ACTION));
				index++;
			}
			position++;
		}
		
	}

}
