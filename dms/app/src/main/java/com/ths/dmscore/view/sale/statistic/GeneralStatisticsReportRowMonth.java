/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.dto.view.ReportInfoDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Mo ta muc dich cua lop (interface)
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class GeneralStatisticsReportRowMonth extends DMSTableRow {
	TextView tvContent;
	TextView tvQuota;
	TextView tvAttain;
	TextView tvDuyet;
	TextView tvProgress;
	TextView tvRemain;
//	LinearLayout llEndTable;
//	TextView tvTotalMathNVBH;
//	TextView tvNumCustomerDoNotAmount;

	public GeneralStatisticsReportRowMonth(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_general_statistics_row_month);
		tvContent = (TextView) findViewById(R.id.tvContent);
		tvQuota = (TextView) findViewById(R.id.tvQuota);
		tvAttain = (TextView) findViewById(R.id.tvAttain);
		tvDuyet = (TextView) findViewById(R.id.tvDuyet);
		tvProgress = (TextView) findViewById(R.id.tvProgress);
		tvRemain = (TextView) findViewById(R.id.tvRemain);
//		llEndTable = (LinearLayout) findViewById(R.id.llEndTable);
//		tvTotalMathNVBH = (TextView) findViewById(R.id.tvTotalMathNVBH);
		listener = listen;
//		tvNumCustomerDoNotAmount = (TextView) view
//				.findViewById(R.id.tvNumCustomerDoNotAmount);
//		tvNumCustomerDoNotAmount = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvNumCustomerDoNotAmount,
//				PriHashMap.PriControl.NVBH_THONGKE_CHUAPSDS );
	}

	/**
	 *
	 * render layout for row with a GeneralStatisticsViewDTO
	 *
	 * @author: HaiTC3
	 * @since: 4:51:47 PM | Jun 11, 2012
	 * @param position
	 * @param GeneralStatisticsViewDTO
	 *            item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, ReportInfoDTO item, double percentFinished) {
		tvContent.setText(item.content);
//		if(!StringUtil.isNullOrEmpty(item.code)){
//			if(item.code.equals("CATEGORY")){
//				tvContent.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
//			}
//		}
		if (item.reportType == ReportInfoDTO.REPORT_DETAIL) {
			if (ReportInfoDTO.TYPE_AMOUNT.equals(item.code)) {
				display(tvQuota, item.amountPlan);
				display(tvAttain, item.amount);
				display(tvDuyet, item.amountApproved);
				display(tvRemain, item.amountRemain);
				displayPercent(tvProgress, item.amountProgress);
				if (item.amountProgress < percentFinished) {
					setTextColor(ImageUtil.getColor(R.color.RED), tvProgress);
				}
			}else{
				display(tvQuota, item.quantityPlan);
				display(tvAttain, item.quantity);
				display(tvDuyet, item.quantityApproved);
				display(tvRemain, item.quantityRemain);
				displayPercent(tvProgress, item.quantityProgress);
				if (item.quantityProgress < percentFinished) {
					setTextColor(ImageUtil.getColor(R.color.RED), tvProgress);
				}
			}

//			llEndTable.setVisibility(View.GONE);
		} else {


//			tvQuota.setVisibility(View.GONE);
//			tvAttain.setVisibility(View.GONE);
//			tvDuyet.setVisibility(View.GONE);
//			tvProgress.setVisibility(View.GONE);
//			tvRemain.setVisibility(View.GONE);
//			tvContent.setVisibility(View.GONE);
//			llEndTable.setVisibility(View.VISIBLE);
//			tvTotalMathNVBH.setVisibility(View.VISIBLE);
//			tvNumCustomerDoNotAmount.setVisibility(View.VISIBLE);

			// display math sale
			String mathSale = context.getString(R.string.TEXT_MATH_SALE_2) + " ";
			mathSale += String.valueOf(item.startSale);
//			showRowSum(mathSale,tvContent, tvQuota);
			// display cusomter do not have amount
			String titleCustomerDoNotAmount = context
					.getString(R.string.TEXT_TITLE_CUSOTMER_DO_NOT_AMOUNT)
					+ " ";
			String str = String.valueOf(item.numCustomerDoNotAmount) + "/"
					+ String.valueOf(item.numCustomerVisitPlanInMonth);

			// update textView number item

			SpannableObject obj = new SpannableObject();
			obj.addSpan(titleCustomerDoNotAmount,
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			obj.addSpan(str, ImageUtil.getColor(R.color.COLOR_LINK),
					Typeface.BOLD, item);

			// init clickable
//			obj.setTextView(tvProgress);

//			tvProgress.setText(sText);
			showRowSum(obj.getSpan(),tvContent, tvQuota, tvAttain, tvDuyet, tvRemain, tvProgress);
			PriUtils.getInstance().setOnClickListener(tvProgress, this);

		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvProgress:
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_CUSTOMER_LIST_DO_NOT_AMOUNT_IN_MONTH, v, null);
			break;

		default:
			super.onClick(v);
			break;
		}
	}
}
