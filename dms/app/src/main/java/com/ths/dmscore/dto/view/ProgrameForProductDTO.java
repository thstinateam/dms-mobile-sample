/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin chuong trinh cho san pham
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class ProgrameForProductDTO {
	public long programId;
	public String programeCode;
	public String programeName;
	// ten cua Chuong trinh: CTTB, CTKM (huy, doi, tra, khuyen mai tay)
	public String programeTypeName;
	public String programeLevel;
	public int programeType;
	// check CTKM co hop le hay khong dua vao cach tinh CTKM moi. Neu la CTTB thi luon luon true
	public boolean isCTKMInvalid = false;
	public String type;

	public ProgrameForProductDTO() {
		programeCode = "";
		programeName = "";
		programeTypeName = "";
		programeType = 0;
		type = "";
	}

	/**
	 *
	 * khoi tao object voi cursor
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initPrograme(Cursor c) {
		// ma chuong trinh
		programId = CursorUtil.getLong(c, "PROMOTION_PROGRAM_ID");
		// ma chuong trinh
		programeCode = CursorUtil.getString(c, "PROGRAM_CODE");
		// ten chuong trinh
		programeName = CursorUtil.getString(c, "PROGRAM_NAME");
		// muc chuong trinh
		programeLevel = CursorUtil.getString(c, "LEVEL_CODE");
		// loai chuong trinh
		programeType = CursorUtil.getInt(c, "PROGRAM_TYPE");
		// loai CTKM (huy, doi , tra, nomal), CTTB
		type = CursorUtil.getString(c, "TYPE", "TB");
		if(type.equals("TB")){
			isCTKMInvalid = true;
		}
		else{
			int COKHAIBAO1 = 0;
			int COKHAIBAO2 = 0;
			int COKHAIBAO3 = 0;
			int COKHAIBAO4 = 0;
			int COKHAIBAO5 = 0;
			int COKHAIBAO6 = 0;
			int COKHAIBAO7 = 0;
			int COKHAIBAO8 = 0;
			int COKHAIBAO9 = 0;
			int COKHAIBAO10 = 0;

			COKHAIBAO1 = CursorUtil.getInt(c, "COKHAIBAO1");
			COKHAIBAO2 = CursorUtil.getInt(c, "COKHAIBAO2");
			COKHAIBAO3 = CursorUtil.getInt(c, "COKHAIBAO3");
			COKHAIBAO4 = CursorUtil.getInt(c, "COKHAIBAO4");
			COKHAIBAO5 = CursorUtil.getInt(c, "COKHAIBAO5");
			COKHAIBAO6 = CursorUtil.getInt(c, "COKHAIBAO6");
			COKHAIBAO7 = CursorUtil.getInt(c, "COKHAIBAO7");
			COKHAIBAO8 = CursorUtil.getInt(c, "COKHAIBAO8");
			COKHAIBAO9 = CursorUtil.getInt(c, "COKHAIBAO9");
			COKHAIBAO10 = CursorUtil.getInt(c, "COKHAIBAO10");
			boolean result = false;
			if (COKHAIBAO1 == 0) {
				result = true;
			} else {
				result = COKHAIBAO2 > 0 ? true : false;
			}
			if (result) {
				if (COKHAIBAO3 == 0) {
					result = true;
				} else {
					result = COKHAIBAO4 > 0 ? true : false;
				}
			} else {
				isCTKMInvalid = false;
			}
			if (result) {
				boolean result1 = false;
				if (COKHAIBAO5 == 0) {
					result1 = true;
				} else {
					result1 = COKHAIBAO6 == COKHAIBAO5 ? true : false;
				}

				boolean result2 = false;
				if (COKHAIBAO7 == 0) {
					result2 = true;
				} else {
					result2 = COKHAIBAO8 == COKHAIBAO7 ? true : false;
				}

				boolean result3 = false;
				if (COKHAIBAO9 == 0) {
					result3 = true;
				} else {
					result3 = COKHAIBAO10 > 0 ? true : false;
				}
				result = result1 && result2 && result3;
			} else {
				isCTKMInvalid = false;
			}
			isCTKMInvalid = result;
		}
		if (type.equals("ZH")) {
			programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL;
		} else if (type.equals("ZD")) {
			programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE;
		} else if (type.equals("ZT")) {
			programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN;
		}
		programeTypeName = CursorUtil.getString(c, "AP_PARAM_NAME");
	}
}
