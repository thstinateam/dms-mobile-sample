/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Mo ta cho class
 *
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class FollowProblemDTO {
	public List<FollowProblemItemDTO> list = new ArrayList<FollowProblemItemDTO>();
	public ComboboxFollowProblemDTO comboboxDTO;
	public int total = 0;
}
