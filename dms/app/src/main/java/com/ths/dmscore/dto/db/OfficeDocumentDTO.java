/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

/**
 * 
 * dto cong van
 * 
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class OfficeDocumentDTO extends AbstractTableDTO {
	// id cong van
	public long officeDocumentId;
	// ma cong van
	public String docCode;
	// ten cong van
	public String docName;
	// loai cong van
	public int type;
	// ngay tao
	public String createDate;
	// nguoi tao
	public String createUser;
	// ngay cap nhat
	public String updateDate;
	// nguoi cap nhat
	public String updateUser;
	// from dayInOrder
	public String fromDate;
	// to dayInOrder
	public String toDate;
	//url hinh anh cong van
	public String url;
	//status
	public int status;
	/**
	 * init data office document
	 * @author: YenNTH
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDocumentData(Cursor c) {
		officeDocumentId = CursorUtil.getLong(c, "OFFICE_DOCUMENT_ID");
		docCode= CursorUtil.getString(c, "DOC_CODE");
		docName= CursorUtil.getString(c, "DOC_NAME");
		type=CursorUtil.getInt(c, "TYPE");
		fromDate= DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "FROM_DATE"));
		toDate= DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, "TO_DATE"));
		url= CursorUtil.getString(c, "URL");

	}
}
