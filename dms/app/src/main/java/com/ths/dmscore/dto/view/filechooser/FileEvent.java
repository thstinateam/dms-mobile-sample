package com.ths.dmscore.dto.view.filechooser;

public interface FileEvent {
        void onFileChoose(FileInfo info);
        void onFileNameSelected(FileInfo info);
}