/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

@SuppressWarnings("serial")
public class StockTransDTO extends AbstractTableDTO {
	public int stockTransLotId;
	public int stockTransId;
	public int stockTransDetailId;
	public String stockTransDate;
	public int productId;
	public String lot;
	public int quantity;
	public int fromOwnerId;
	public int toOwnerId;
	public int fromOwnerType;
	public int toOwnerType;
	public String createUser;
	public String createDate;
	
	public StockTransDTO() {
		super(TableType.STOCK_TRANS_LOT_TABLE);
	}

}