/*
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.android.gcm.GCMRegistrar;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ServerPath;
import com.ths.dms.R;

/**
 * Dang ky GCM len server
 * @author dungdq
 */
public class GCMRegisterAsync extends AsyncTask<SharedPreferences, Void, Void> {

	private final Context con;

	public GCMRegisterAsync(Context context) {
		con = context;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(SharedPreferences... params) {
		// TODO Auto-generated method stub
		String regId = GCMRegistrar.getRegistrationId(con
				.getApplicationContext());

		checkNotNull(ServerPath.SERVER_HTTP, "SERVER_URL");
		checkNotNull(Constants.SENDER_ID, "SENDER_ID");
		GCMRegistrar.checkManifest(this.con.getApplicationContext());
		if (regId.trim().length()==0) {
			Intent registrationIntent = new Intent(
					"com.google.android.c2dm.intent.REGISTER");
			registrationIntent.putExtra("app",
					PendingIntent.getBroadcast(con, 0, new Intent(), 0));
			registrationIntent.putExtra("sender", Constants.SENDER_ID);
			con.startService(registrationIntent);
		}
		return null;
	}


	private void checkNotNull(Object reference, String name) {
		if (reference == null) {
			throw new NullPointerException(con.getString(
					R.string.TEXT_REGISTER_GCM_FALSE, name));
		}
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
	}
}
