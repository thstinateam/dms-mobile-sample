/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Danh sach cac tieu chi
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  10:14:58 AM Nov 17, 2014
 */
public class ListTrainingRateDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// so luong phan tu
	private int totalItem;
	// danh sach cac tieu chi
	private ArrayList<TrainingRateDTO> listCriteria;

	public ListTrainingRateDTO() {
		// TODO Auto-generated constructor stub
		totalItem = 0;
		listCriteria = new ArrayList<TrainingRateDTO>();
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public ArrayList<TrainingRateDTO> getListCriteria() {
		return listCriteria;
	}

	public void setListCriteria(ArrayList<TrainingRateDTO> listCriteria) {
		this.listCriteria = listCriteria;
	}
	
	/**
	 * lay kich thuoc danh sach tieu chi
	 * 
	 * @author: dungdq3
	 * @since: 2:04:59 PM Nov 17, 2014
	 * @return: int
	 * @throws:  
	 * @return
	 */
	public int getSizeListCriteria(){
		return listCriteria.size();
	}
	
	/**
	 * lay tieu chi tai vi tri
	 * 
	 * @author: dungdq3
	 * @since: 2:02:37 PM Nov 17, 2014
	 * @return: TrainingRateDTO
	 * @throws:  
	 * @param index
	 * @return
	 */
	public TrainingRateDTO getTrainingRate(int index) {
		TrainingRateDTO rateDTO = null;
		if(listCriteria.size() > 0 && listCriteria.size() > index - 1)
			rateDTO = listCriteria.get(index - 1);
		return rateDTO;
	}
	
	/**
	 * add item
	 * 
	 * @author: dungdq3
	 * @since: 10:41:01 AM Nov 21, 2014
	 * @return: void
	 * @throws:  
	 * @param item
	 */
	public void addItem(TrainingRateDTO item){
		listCriteria.add(item);
	}

}
