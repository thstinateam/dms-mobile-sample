package com.ths.dmscore.view.sale.order;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.EquipInventoryDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentImageViewDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Man hinh chup hinh cho thiet bi kiem ke
 * @author: dungnt19
 * @version: 1.0
 * @since: 1.0
 */
public class PhotoInventoryEquipmentView extends BaseFragment implements OnItemClickListener {
	Button btTakeImage; // button chup anh
	GridView gvImageView; // ds hinh anh
	public CustomerListItem customerItem; // thong tin kh
	TakePhotoEquipmentViewDTO keyShopViewDTO = new TakePhotoEquipmentViewDTO();
	private ImageAdapter thumbs = null;// adapter image
	private int numTopLoaded = 0;
	private boolean isAbleGetMore = true;
	private boolean isFirst = false;
	LinearLayout llImage;
	TextView tvNoDataResult;
//	private TextView tvNumPhoto;// so hinh anh da chup
	private TextView tvTotalImage;// so hinh anh da chup
	private TextView tvTimeInventory;// so hinh anh da chup
	private TextView tvNumImage;// so hinh anh da chup
	private boolean isGetTotal = true;
	// dialog product detail view
	RegisterKeyShopView keyShopView;
	// dto cho man hinh keyshop
	boolean isUpdateRegisterKeyShop = false;// bien kiem tra dang o trang thai update keyshop hay ko
	long ksId = 0; // CT Keyshop hien tai
	// dialog product detail view
	ReportListCustomerView reportView; // man hinh bao cao ct httm khach hang tham gias
	private EquipInventoryDTO itemInventory;

	public static PhotoInventoryEquipmentView getInstance(Bundle data) {
		PhotoInventoryEquipmentView instance = new PhotoInventoryEquipmentView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		customerItem = (CustomerListItem) getArguments().getSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM);
		itemInventory = (EquipInventoryDTO) getArguments().getSerializable(IntentConstants.INTENT_ITEM_REPORT_LOST_DEVICE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_photo_inventory_equipment_view, container, false);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_KIEMKE_CHUPHINHTHIETBI);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_PHOTO_INVENTORY_EQUIPMENT));
		initView(v);
		//parent.removeMenuFinishCustomer();
		if(!isFirst){
			getPhotoInventoryEquipmentInfo();
			getListImageOfInventoryEquipment(true);
			isFirst = true;
		}
		else{
			gvImageView.setAdapter(thumbs);
			renderLayoutImage();
		}
		return v;
	}

	 /**
	 * Khoi tao view
	 * @author: dungnt19
	 * @param v
	 * @return: void
	 * @throws:
	*/
	public void initView(View v) {
		btTakeImage = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btTakeImage, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_CHUPHINHKEYSHOP);
		PriUtils.getInstance().setOnClickListener(btTakeImage, this);
		gvImageView = (GridView) PriUtils.getInstance().findViewById(v, R.id.gvImageView, PriHashMap.PriControl.NVBH_BANHANG_CHAMKEYSHOP_GRIDHINHANH, PriUtils.CONTROL_GRIDVIEW);
		PriUtils.getInstance().setOnItemClickListener(gvImageView, this);
		llImage = (LinearLayout)v.findViewById(R.id.llImage);
		tvNoDataResult = (TextView)v.findViewById(R.id.tvNoDataResult);
		tvTotalImage = (TextView)v.findViewById(R.id.tvTotalImage);
		tvTimeInventory = (TextView)v.findViewById(R.id.tvTimeInventory);
		tvNumImage = (TextView)v.findViewById(R.id.tvNumImage);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if( v == btTakeImage){
			checkDistanceAndTakeImage();
		}else {
			super.onClick(v);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		AlbumDTO album = new AlbumDTO();
		album.setListPhoto(keyShopViewDTO.image.lstImage);
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, album);
		bundle.putInt(IntentConstants.INTENT_ALBUM_INDEX_IMAGE, arg2);
		bundle.putString(IntentConstants.INTENT_FROM, GlobalUtil.getTag(VoteKeyShopView.class));
		handleSwitchFragment(bundle, ActionEventConstant.ACTION_LOAD_IMAGE_FULL, UserController.getInstance());

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.ACTION_GET_PHOTO_INVENTORY_EQUIPMENT_INFO:{
			TakePhotoEquipmentViewDTO dto = (TakePhotoEquipmentViewDTO)modelEvent.getModelData();
			keyShopViewDTO.shopDistance = dto.shopDistance;
			parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.ACTION_GET_LIST_IMAGE_EQUIPMENT: {
			TakePhotoEquipmentImageViewDTO temp = (TakePhotoEquipmentImageViewDTO)modelEvent.getModelData();
			if(isGetTotal) {
				keyShopViewDTO.image.totalImage = temp.totalImage;
				
				keyShopViewDTO.image.lstImage.addAll(itemInventory.getListPhotoTaken());
			}
			keyShopViewDTO.image.lstImage.addAll(temp.lstImage);
			int size = temp.lstImage.size();
			numTopLoaded += size;
			if (size < Constants.NUM_LOAD_MORE_IMAGE) {
				isAbleGetMore = false;
			}
			
			renderLayoutImage();
			parent.closeProgressDialog();
			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	 /**
	 * Lay thong tin chup hinh thiet bi kiem ke
	 * @author: dungnt19
	 * @return: void
	 * @throws:
	*/
	private void getPhotoInventoryEquipmentInfo(){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		handleViewEvent(data, ActionEventConstant.ACTION_GET_PHOTO_INVENTORY_EQUIPMENT_INFO, SupervisorController.getInstance());
	}

	 /**
	 * Lay ds hinh anh cua thiet bi kiem
	 * @author: dungnt19
	 * @return: void
	 * @throws:
	*/
	private void getListImageOfInventoryEquipment(boolean isGetTotal){
		this.isGetTotal = isGetTotal;
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, this.customerItem.aCustomer.getCustomerId());
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putLong(IntentConstants.INTENT_EQUIP_ID, itemInventory.idEquip);
		data.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotal);
		data.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, Constants.NUM_LOAD_MORE_IMAGE);
		data.putInt(IntentConstants.INTENT_PAGE, numTopLoaded / Constants.NUM_LOAD_MORE_IMAGE);
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LIST_IMAGE_EQUIPMENT, SupervisorController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				resetAllValue();
				getPhotoInventoryEquipmentInfo();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	private void renderLayoutImage(){
		if(thumbs == null){
			thumbs = new ImageAdapter(parent, keyShopViewDTO.image.lstImage);
			gvImageView.setAdapter(thumbs);
		}
		thumbs.notifyDataSetChanged();
		if(keyShopViewDTO.image.lstImage.size() == 0){
			llImage.setVisibility(View.GONE);
			tvNoDataResult.setVisibility(View.VISIBLE);
		}else{
			tvNoDataResult.setVisibility(View.GONE);
			llImage.setVisibility(View.VISIBLE);
		}
		
		tvTotalImage.setText(String.valueOf(keyShopViewDTO.image.totalImage + itemInventory.getListPhotoTaken().size()));
		tvNumImage.setText(String.valueOf(itemInventory.getListPhotoTaken().size()));
		tvTimeInventory.setText(String.valueOf(itemInventory.preTimes + 1));
	}

	 /**
	 * Lay ds hinh anh load more
	 * @author: dungnt19
	 * @return: void
	 * @throws:
	*/
	private void getMorePhoto() {
		if (isAbleGetMore) {
			parent.showLoadingDialog();
			getListImageOfInventoryEquipment(false);
		}
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param filePath
	 */
	public void updateTakenPhoto(String filePath) {
		if(itemInventory != null) {
			PhotoDTO photo = new PhotoDTO();
			photo.thumbUrl = filePath;
			photo.fullUrl = filePath;
			photo.createdTime = DateUtils.formatNow(DateUtils.defaultDateFormat_3);
			photo.fullCreatedTime = DateUtils.now();
			photo.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
			photo.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude();
			photo.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude();
			itemInventory.getListPhotoTaken().add(0, photo);
			
			keyShopViewDTO.image.lstImage.add(0, photo);
			renderLayoutImage();
		}
	}
	

	private void checkDistanceAndTakeImage() {
		LatLng myLoc = new LatLng(GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLatitude(), GlobalInfo.getInstance().getProfile().getMyGPSInfo().getLongtitude());
		LatLng cusLoc = new LatLng(this.customerItem.aCustomer.lat, this.customerItem.aCustomer.lng);
		if (GlobalUtil.getDistanceBetween(myLoc, cusLoc) <= keyShopViewDTO.shopDistance) {
			parent.takenPhoto = GlobalUtil.takePhoto(this, GlobalBaseActivity.RQ_TAKE_PHOTO_INVENTORY_DV);
			
			if(parent.takenPhoto == null) {
				parent.showDialog(StringUtil.getString(R.string.TEXT_CANNOT_CREATE_FILE_TO_TAKE_IMAGE));
			}
		} else {
			// gui log len server phuc vu dieu tra khong chup anh TB
			ServerLogger.sendLog("InventoryDeviceView", "Khoang cach xa hon cho phep : CUS="
					+ this.customerItem.aCustomer.customerCode
					+ " (Lat, Lng)=" + myLoc.lat + "," + myLoc.lng, true, TabletActionLogDTO.LOG_CLIENT);

			// thong bao len view va tiep tuc
			SpannableObject msg = new SpannableObject();
			msg.addSpan(StringUtil.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_1), ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);

			msg.addSpan(this.customerItem.aCustomer.customerCode.substring(0, 3)
					+ "-" + this.customerItem.aCustomer.customerName, ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.BOLD);

			msg.addSpan(StringUtil.getString(R.string.ERROR_DISTANCE_TAKE_IMAGE_DP_2), ImageUtil.getColor(R.color.WHITE), android.graphics.Typeface.NORMAL);
			// dong dialog waitting dang luong chup anh
			parent.closeProgressDialog();
			// show msg thong bao
			parent.showDialog(msg.getSpan());
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		String filePath = "";
		//Co file vat ly nhung take photo null		
		if (parent.takenPhoto == null) {
			// if (takenPhoto != null) {
			SharedPreferences sharedPreferences = GlobalInfo
					.getInstance().getDmsPrivateSharePreference();
			String timeSaveImage = sharedPreferences.getString(
					GlobalBaseActivity.TIME_SAVE_IMAGE_LIST, "");
			String fileName = Constants.TEMP_IMG + "_"
					+ timeSaveImage + ".jpg";
			parent.takenPhoto = new File(ExternalStorage.getTakenPhotoPath(parent), fileName);
			
			ServerLogger.sendLog("InventoryDeviceView", "RQ_TAKE_PHOTO_INVENTORY_DV, takenPhoto == null: " + fileName,
					TabletActionLogDTO.LOG_EXCEPTION);
		}
		switch (requestCode) {
		case GlobalBaseActivity.RQ_TAKE_PHOTO_INVENTORY_DV:
			if (resultCode == Activity.RESULT_OK && parent.takenPhoto != null && parent.takenPhoto.exists()) {
				filePath = parent.takenPhoto.getAbsolutePath();
				ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(parent, filePath,
						Constants.MAX_FULL_IMAGE_HEIGHT);
				validator.setDataIntent(data);
				if (validator.execute()) {
					MyLog.e("TakePhoto", "....result taking photo : OK");
					updateTakenPhoto(filePath);				
				}
			} else {
				//Xoa file rac
				if(parent.takenPhoto != null && parent.takenPhoto.exists()) {
					parent.takenPhoto.delete();
				}
			}
			break;
		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}

	 /**
	 * clear data
	 * @author: dungnt19
	 * @return: void
	 * @throws:
	*/
	private void resetAllValue(){
		gvImageView.smoothScrollToPosition(0);
		numTopLoaded = 0;
		isAbleGetMore = true;
		isGetTotal = true;
		keyShopViewDTO.image.lstImage.clear();
	}

	public class ImageAdapter extends BaseAdapter {

		private ArrayList<PhotoDTO> list = new ArrayList<PhotoDTO>();
		public ImageAdapter(Context c,ArrayList<PhotoDTO> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup pt) {
			View row = convertView;
			ViewHolder holder = null;

			if (convertView == null) {
				LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) parent)
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = layout.inflate(R.layout.layout_album_detail_user, pt, false);
				holder = new ViewHolder();
				holder.imageView = (ImageView) row
						.findViewById(R.id.imgAlbumImage);
				holder.staffName = (TextView) row
						.findViewById(R.id.tvStaffName);
				holder.staffName.setVisibility(View.GONE);
				holder.titleAlbum = (TextView) row
						.findViewById(R.id.tvAlbumName);
				holder.titleAlbum.setTextColor(getResources().getColor(
						R.color.BLACK));
				holder.titleAlbum.setTypeface(null,
						android.graphics.Typeface.NORMAL);
				holder.titleAlbum.setTextSize(GlobalUtil.dip2Pixel(16));
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			holder.imageView.setImageResource(R.drawable.album_default);
			if (!StringUtil.isNullOrEmpty(list.get(position).thumbUrl)) {
				if (list.get(position).thumbUrl.contains(ExternalStorage.SDCARD_PATH)){
					ImageUtil.loadImage(list.get(position).thumbUrl, holder.imageView);
				}
				else{
					ImageUtil.loadImage(ServerPath.IMAGE_PATH +  list.get(position).thumbUrl, holder.imageView);
				}

			}
			holder.titleAlbum.setText(list.get(position).createdTime);
			if (position == numTopLoaded - 1) {
				getMorePhoto(); // lay them hinh anh
			}

			return row;
		}
	}

	public static class ViewHolder {
		public ImageView imageView;
		public ImageView imageViewBg;
		public TextView titleAlbum;
		public TextView staffName;
	}
}
