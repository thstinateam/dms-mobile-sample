package com.ths.dmscore.dto.view;

import java.util.ArrayList;


public class ReportDisplayProgressDetailCustomerDTO {
	public ArrayList<ReportDisplayProgressDetailCustomerRowDTO> listItem;
	public double standardProgress;

	public  ReportDisplayProgressDetailCustomerDTO(){
		listItem = new ArrayList<ReportDisplayProgressDetailCustomerRowDTO>();
	}
	public void addItem(ReportDisplayProgressDetailCustomerRowDTO c) {
		listItem.add(c);
	}
}
