package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.StaffDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class CustomerListItem implements Serializable {
	private static final long serialVersionUID = 3565307269873797326L;
	public String cusPlan; // tuyen ban hang
	public boolean isTodayOrdered = false;// dat hang 4 da dat hang
	public boolean isTodayVoted = false;// cham hien dien : 2 cham trong ngay
										// roi
	public boolean isTodayCheckedRemain = false;// kiem hang : 3 kiem roi
	public boolean isHaveDisplayProgramNotYetVote = false;
	public boolean isHaveSaleOrder = false; // neu co sale_order trong 2 thang
											// gan fullDate thi se co Kiem hang ton
	public int isOr = 0; // 0: KH trong tuyen, 1: KH ngoai tuyen
	public int numUpdatePosition = 0; // so lan cap nhat vi tri cua khach hang
	public String lastUpdatePosition; // ngay cap nhat vi tri gan fullDate
	public Double visitedLat;
	public Double visitedLng;
	public String visitStartTime;
	public String visitStartTimeShort;
	public String visitEndTime = "";
	public boolean isTooFarShop = false;
	public CustomerDTO aCustomer = new CustomerDTO();
	public StaffDTO staffSale;// nhan vien ban hang
	public String seqInDayPlan = "";
	public String currentPlan = "";// tuyen dang chon
	public String exceptionOrderDate;
	public int routingCustomerId;
	public String draftSaleOrderId;
	public double cusDistance = -1;
	public int weekInterval;
	public int startWeek;
	public String fromDate;
	public String toDate;
	public long staffCustomerId;
	public long visitActLogId;
	public double distanceOrder;
	public int isVisitPlan = 0;// 1: KH ngoai tuyen, 0: KH trong tuyen
	public long routingId = 0;
	public boolean isExistKeyShop = false;// co ton tai key shop hay ko
	public boolean isHaveEquip = false;// co thiet bi hay ko

	// dung cho GS bat buoc chup hinh TB
	public ArrayList<String> displayProgramId;
	public ArrayList<String> displayProgramIdCaptured;
	
	// thong tin KH dang ky thiet bi
	public long numInventoryEquipment;
	public String lastInventoryDate;

	public enum VISIT_STATUS {
		NONE_VISIT, VISITED_FINISHED, VISITING, VISITED_CLOSED
	}

	public VISIT_STATUS visitStatus = VISIT_STATUS.NONE_VISIT;

	public void initDataFromCursor(Cursor c, double shopDistance, String currentPlan) throws Exception {
		try {
			this.currentPlan = currentPlan;
			if (c == null) {
				throw new Exception("Cursor is empty");
			}
			routingId = CursorUtil.getLong(c, "ROUTING_ID");
			aCustomer.deliverID = CursorUtil.getString(c, "DELIVER_ID");
			aCustomer.cashierStaffID = CursorUtil.getString(c, "CASHIER_STAFF_ID");
			aCustomer.customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
			aCustomer.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			aCustomer.setShortCode(CursorUtil.getString(c, "SHORT_CODE"));
			aCustomer.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
			distanceOrder = shopDistance;
			aCustomer.mobilePhone = CursorUtil.getString(c, "MOBIPHONE");
			aCustomer.phone = CursorUtil.getString(c, "PHONE");
			aCustomer.address = CursorUtil.getString(c, "ADDRESS");
			if (!StringUtil.isNullOrEmpty(aCustomer.address)) {
				aCustomer.street = aCustomer.address;
			} else {
				aCustomer.street = CursorUtil.getString(c, "STREET");
				if (StringUtil.isNullOrEmpty(aCustomer.street)) {
					aCustomer.street = "";
				}
			}
			aCustomer.lat = CursorUtil.getDouble(c, "LAT");
			aCustomer.lng = CursorUtil.getDouble(c, "LNG");
			visitedLat = CursorUtil.getDouble(c, "VISITED_LAT");
			visitedLng = CursorUtil.getDouble(c, "VISITED_LNG");
			cusPlan = CursorUtil.getString(c, "CUS_PLAN");
			if (!StringUtil.isNullOrEmpty(cusPlan)
					&& cusPlan.substring(0, 1).equals(",")) {
				cusPlan = cusPlan.substring(1, cusPlan.length());
			}
			visitStartTime = CursorUtil.getString(c, "START_TIME");
			visitStartTimeShort = CursorUtil.getString(c, "VISIT_START_TIME_SHORT");
			if (StringUtil.isNullOrEmpty(visitStartTimeShort)) {
				visitStartTimeShort = "";
			}
			visitEndTime = CursorUtil.getString(c, "END_TIME");
			String visited = "";
			visited = CursorUtil.getString(c, "VISIT");
			if (visited.equals("0") && !StringUtil.isNullOrEmpty(visitEndTime)) {
				visitStatus = VISIT_STATUS.VISITED_FINISHED;
			} else if (visited.equals("0")
					&& StringUtil.isNullOrEmpty(visitEndTime)) {
				visitStatus = VISIT_STATUS.VISITING;
			} else if (visited.equals("1")) {
				visitStatus = VISIT_STATUS.VISITED_CLOSED;
			}
			String action = CursorUtil.getString(c, "ACTION");
			{
				if (action.indexOf('2') > -1) {
					isTodayVoted = true;
				}
				if (action.indexOf('3') > -1) {
					isTodayCheckedRemain = true;
				}
				if (action.indexOf('4') > -1) {
					isTodayOrdered = true;
				}
//				if (action.indexOf('6') > -1) {
//					isInventoryDevice = true;
//				}
			}

			String order = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "IS_ORDER")) ? ""
					: CursorUtil.getString(c, "IS_ORDER");
			if (order.equals("4")) {
				isTodayOrdered = true;
			}
			String vote = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "IS_VOTE")) ? ""
					: CursorUtil.getString(c, "IS_VOTE");
			if (vote.equals("2")) {
				isTodayVoted = true;
			}
			String checkRemainProduct = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "IS_CHECK")) ? ""
					: CursorUtil.getString(c, "IS_CHECK");
			if (checkRemainProduct.equals("3")) {
				isTodayCheckedRemain = true;
			}
			// ST
			String displayProgrameCode = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "DISPLAY_PROGRAM_CODE")) ? ""
						: CursorUtil.getString(c, "DISPLAY_PROGRAM_CODE");
			if (!StringUtil.isNullOrEmpty(displayProgrameCode)) {
				isHaveDisplayProgramNotYetVote = true;
			}

			String saleOrderId = CursorUtil.getString(c, "SALE_ORDER_ID");
			if (!StringUtil.isNullOrEmpty(saleOrderId)) {
				isHaveSaleOrder = true;
			}
			draftSaleOrderId = CursorUtil.getString(c, "DRAFT_SALE_ORDER_ID");

			isOr = checkInVisitPlan();
				seqInDayPlan = StringUtil.isNullOrEmpty(CursorUtil.getString(c, "DAY_SEQ")) ? ""
						: CursorUtil.getString(c, "DAY_SEQ");
			updateCustomerDistance(shopDistance);
			exceptionOrderDate = CursorUtil.getString(c, "EXCEPTION_ORDER_DATE");

			routingCustomerId = CursorUtil.getInt(c, "ROUTING_CUSTOMER_ID");
			weekInterval = CursorUtil.getInt(c, "WEEK_INTERVAL");
			startWeek = CursorUtil.getInt(c, "START_WEEK");
			fromDate = CursorUtil.getString(c, "FROM_DATE");
			toDate = CursorUtil.getString(c, "TO_DATE");
			staffCustomerId = CursorUtil.getLong(c, "STAFF_CUSTOMER_ID");
			aCustomer.channelTypeId = CursorUtil.getInt(c, "CHANNEL_TYPE_ID");
			this.visitActLogId = Long
					.parseLong((StringUtil.isNullOrEmpty(CursorUtil.getString(c, "VISIT_ACT_LOG_ID")) ? "0"
							: CursorUtil.getString(c, "VISIT_ACT_LOG_ID")));
			if(CursorUtil.getInt(c, "KEYSHOP_COUNT") > 0){
				isExistKeyShop = true;
			}
			
			if(CursorUtil.getInt(c, "EQUIP_ID") > 0) {
				isHaveEquip = true;
			}
			
			this.staffSale = new StaffDTO();
			this.staffSale.shopId = CursorUtil.getInt(c, STAFF_TABLE.SHOP_ID);
			this.staffSale.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
			this.staffSale.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
			this.staffSale.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);

		} catch (Exception ex) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public void updateCustomerDistance(double shopDistance) {
		isTooFarShop = false;
		LatLng myLatLng = new LatLng(GlobalInfo.getInstance().getProfile()
				.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
				.getProfile().getMyGPSInfo().getLongtitude());
		LatLng cusLatLng = new LatLng(aCustomer.getLat(), aCustomer.getLng());
		if (myLatLng.lat > 0 && myLatLng.lng > 0 && cusLatLng.lat > 0
				&& cusLatLng.lng > 0) {
			cusDistance = GlobalUtil.getDistanceBetween(myLatLng, cusLatLng);
			if (cusDistance > shopDistance) {
				isTooFarShop = true;
			}
		} else {
			cusDistance = -1;
			isTooFarShop = true;
		}

		 //HARD-CORE KHONG CHECK KHOANG CACH GHE THAM
//		 isTooFarShop = false;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	public boolean canOrderException() {
		boolean canOrderException = false;
		Calendar calendar = Calendar.getInstance();
		String sToday = DateUtils.defaultDateFormat.format(calendar.getTime());
		if (!StringUtil.isNullOrEmpty(exceptionOrderDate)
				&& sToday.equals(exceptionOrderDate)) {
			canOrderException = true;
		}
		return canOrderException;
	}

	private int checkInVisitPlan() {
		int isOr;
		if (cusPlan!= null && cusPlan.indexOf(DateUtils.getCurrentLine()) != -1) {
			isOr = 0; // trong tuyen
			isVisitPlan = 0;
		} else {
			isOr = 1; // ngoai tuyen
			isVisitPlan = 1;
		}
		return isOr;
	}

	public boolean isVisit() {
		return visitStatus != VISIT_STATUS.NONE_VISIT;
	}

	/**
	 * Parse thong tin khach hang cua mot nhan vien ban hang trong tuyen Su dung
	 * cho NVGS NPP giam sat vi tri khach hang
	 *
	 * @author banghn
	 * @throws Exception
	 */
	public void initDataCustomerSaleItem(Cursor c) throws Exception {
		this.staffSale = new StaffDTO();
		this.aCustomer.setCustomerCode(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE));
		this.aCustomer.setCustomerName(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME));
		this.aCustomer.setCustomerId(CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID));
		this.aCustomer.address = CursorUtil.getString(c, "ADDRESS");
		this.aCustomer.setStreet(CursorUtil.getString(c, "STREET"));
		if (StringUtil.isNullOrEmpty(this.aCustomer.address)) {
			this.aCustomer.address = this.aCustomer.street;
		}
		this.staffSale.shopId = CursorUtil.getInt(c, STAFF_TABLE.SHOP_ID);
		this.staffSale.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
		this.staffSale.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		this.staffSale.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		this.aCustomer.setLat(CursorUtil.getDouble(c, "LAT"));
		this.aCustomer.setLng(CursorUtil.getDouble(c, "LNG"));
		this.numUpdatePosition = CursorUtil.getInt(c, "NUM_UPDATE");
		this.lastUpdatePosition = CursorUtil.getString(c, "LAST_UPDATE");
		this.staffCustomerId = CursorUtil.getLong(c, "STAFF_CUSTOMER_ID");

		this.exceptionOrderDate = CursorUtil.getString(c, "EXCEPTION_ORDER_DATE");
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param c
	 * @return: voidvoid
	 * @throws:
	 */
	public void initDataFromNoPSDS(Cursor c) {
		this.aCustomer.setCustomerCode(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE));
		this.aCustomer.setCustomerName(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME));
		this.aCustomer.setCustomerId(CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID));
		this.aCustomer.setStreet(CursorUtil.getString(c, "ADDRESS"));

	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: ToanTT
	 * @param c
	 * @return: voidvoid
	 * @throws:
	 */
	public boolean initDataFromSelectCustomer(Cursor c, double lat, double lng) {
		double cusLat = CursorUtil.getDouble(c, (CUSTOMER_TABLE.LAT));
		double cusLng = CursorUtil.getDouble(c, (CUSTOMER_TABLE.LNG));
		if (cusLat > 0 && cusLng > 0) {
			cusDistance = GlobalUtil.getDistanceBetween(new LatLng(lat, lng), new LatLng(cusLat, cusLng));
			if (cusDistance <= Constants.DISTANCE_SELECT_CUSTOMER) {
				this.aCustomer.setCustomerCode(CursorUtil.getString(c, (CUSTOMER_TABLE.CUSTOMER_CODE), ""));
				this.aCustomer.setCustomerName(CursorUtil.getString(c, (CUSTOMER_TABLE.CUSTOMER_NAME), ""));
				this.aCustomer.setCustomerId(CursorUtil.getLong(c, (CUSTOMER_TABLE.CUSTOMER_ID), 0));
				this.aCustomer.setStreet(CursorUtil.getString(c, (CUSTOMER_TABLE.STREET), ""));
				this.aCustomer.setLat(cusLat);
				this.aCustomer.setLng(cusLng);
				this.cusPlan = CursorUtil.getString(c, ("TUYEN"), "");
				if (!StringUtil.isNullOrEmpty(cusPlan)) {
					if (cusPlan.substring(0, 1).equals(",")) {
						cusPlan = cusPlan.substring(1, cusPlan.length());
					}
					checkInVisitPlan();
				} else {
					isOr = 1;
					isVisitPlan = 1;
				}
				this.visitEndTime = CursorUtil.getString(c, "VISIT_END_TIME", "");
				String visit = CursorUtil.getString(c, "IS_VISIT", "");
				if (!StringUtil.isNullOrEmpty(visit)) {
					if ((visit.equals("0") && !StringUtil
							.isNullOrEmpty(visitEndTime)) || visit.equals("1")) {
						visitStatus = VISIT_STATUS.VISITED_FINISHED;
					} else if (visit.equals("0")
							&& StringUtil.isNullOrEmpty(visitEndTime)) {
						visitStatus = VISIT_STATUS.VISITING;
					}
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * sap xep theo k/c den kh tang dan
	 *
	 * @author: ToanTT
	 * @param data
	 * @return: void
	 * @throws:
	 */
	public static CustomerListDTO orderDistanceCusomter(CustomerListDTO tempDTO) {
		CustomerListItem shiftItem = new CustomerListItem();
		for (int i = 0; i < tempDTO.getCusList().size() - 1; i++) {
			for (int j = i + 1; j < tempDTO.getCusList().size(); j++) {
				if (tempDTO.getCusList().get(i).cusDistance > tempDTO.getCusList().get(j).cusDistance) {
					shiftItem = CustomerListItem
							.cloneSelectCustomer(tempDTO.getCusList().get(i));
					tempDTO.getCusList().set(i, CustomerListItem
							.cloneSelectCustomer(tempDTO.getCusList().get(j)));
					tempDTO.getCusList().set(j,
							CustomerListItem.cloneSelectCustomer(shiftItem));
				}
			}
		}
		return tempDTO;
	}

	public void initDataFromCursorSupervisor(Cursor c) throws Exception {
		try {
			if (c == null) {
				throw new Exception("Cursor is empty");
			}

			this.aCustomer.setCustomerCode(CursorUtil.getString(c, "CUSTOMER_CODE"));
			this.aCustomer.setCustomerId(CursorUtil.getLong(c, "CUSTOMER_ID"));
			this.aCustomer.setCustomerName(CursorUtil.getString(c, "CUSTOMER_NAME"));
			this.aCustomer.setStreet(CursorUtil.getString(c, "STREET"));

		} catch (Exception ex) {
			throw ex;
		}
	}

	public static CustomerListItem cloneSelectCustomer(CustomerListItem cusItem) {
		CustomerListItem cloneDto = new CustomerListItem();
		cloneDto.aCustomer.customerId = cusItem.aCustomer.customerId;
		cloneDto.aCustomer.customerCode = cusItem.aCustomer.customerCode;
		cloneDto.aCustomer.customerName = cusItem.aCustomer.customerName;
		cloneDto.aCustomer.setStreet(cusItem.aCustomer.getStreet());
		cloneDto.aCustomer.lat = cusItem.aCustomer.lat;
		cloneDto.aCustomer.lng = cusItem.aCustomer.lng;
		cloneDto.isOr = cusItem.isOr;
		cloneDto.visitStatus = cusItem.visitStatus;
		cloneDto.cusDistance = cusItem.cusDistance;
		cloneDto.isVisitPlan = cusItem.isVisitPlan;
		return cloneDto;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	public void checkCaptureDisplayProgram(){
		if (displayProgramId == null || displayProgramId.size() == 0 || isOr == 1) {
			// khong co CTTB, khong chup hinh
			isHaveDisplayProgramNotYetVote = false;
		} else {
			if(displayProgramIdCaptured == null || displayProgramIdCaptured.size() < displayProgramId.size())
				// co CTTB, nhung chua chup du hinh TB
				isHaveDisplayProgramNotYetVote = true;
			else
				isHaveDisplayProgramNotYetVote = false;
		}
	}
	
	/**
	 * Parse thong tin khach hang cua mot nhan vien ban hang trong tuyen Su dung
	 * cho NVGS NPP giam sat vi tri khach hang
	 *
	 * @author banghn
	 * @throws Exception
	 */
	public void initDataCustomerInventoryEquipment(Cursor c, double shopDistance) throws Exception {
		this.staffSale = new StaffDTO();
		this.aCustomer.setCustomerCode(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE));
		this.aCustomer.setShortCode(CursorUtil.getString(c, CUSTOMER_TABLE.SHORT_CODE));
		this.aCustomer.setCustomerName(CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME));
		this.aCustomer.setCustomerId(CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID));
		this.aCustomer.address = CursorUtil.getString(c, "ADDRESS");
		this.aCustomer.setStreet(CursorUtil.getString(c, "STREET"));
		if (StringUtil.isNullOrEmpty(this.aCustomer.address)) {
			this.aCustomer.address = this.aCustomer.street;
		}
		this.staffSale.shopId = CursorUtil.getInt(c, STAFF_TABLE.SHOP_ID);
		this.staffSale.staffId = CursorUtil.getInt(c, STAFF_TABLE.STAFF_ID);
		this.staffSale.staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		this.staffSale.name = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		this.aCustomer.setLat(CursorUtil.getDouble(c, "LAT"));
		this.aCustomer.setLng(CursorUtil.getDouble(c, "LNG"));
		this.numUpdatePosition = CursorUtil.getInt(c, "NUM_UPDATE");
		this.lastUpdatePosition = CursorUtil.getString(c, "LAST_UPDATE");
		this.staffCustomerId = CursorUtil.getLong(c, "STAFF_CUSTOMER_ID");
		this.numInventoryEquipment = CursorUtil.getLong(c, "NUM_INVENTORY_EQUIPMENT");
		this.lastInventoryDate = CursorUtil.getString(c, "LAST_INVENTORY_DATE");

		this.exceptionOrderDate = CursorUtil.getString(c, "EXCEPTION_ORDER_DATE");
		distanceOrder = shopDistance;
		updateCustomerDistance(shopDistance);
	}
}
