/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.GROUP_LEVEL_DETAIL_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 * ProductGroupDTO.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class GroupLevelDetailDTO extends AbstractTableDTO {
	public static final int VALUE_TYPE_QUANTITY = 1;
	public static final int VALUE_TYPE_AMOUNT = 2;
	public static final int VALUE_TYPE_PERCENT = 3;
	
	private static final long serialVersionUID = 1L;
	public long groupLevelDetailId;
	public long groupLevelId;
	public long productId;
	public String oum;
	public int valueType;
	public long value;
	public int isRequired;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public long usedAmount;
	
	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		groupLevelDetailId = CursorUtil.getLong(c, GROUP_LEVEL_DETAIL_TABLE.GROUP_LEVEL_DETAIL_ID);
		groupLevelId = CursorUtil.getLong(c, GROUP_LEVEL_DETAIL_TABLE.GROUP_LEVEL_ID);
		productId = CursorUtil.getLong(c, GROUP_LEVEL_DETAIL_TABLE.PRODUCT_ID);
		valueType = CursorUtil.getInt(c, GROUP_LEVEL_DETAIL_TABLE.VALUE_TYPE);
		value = CursorUtil.getLong(c, GROUP_LEVEL_DETAIL_TABLE.VALUE);
		isRequired = CursorUtil.getInt(c, GROUP_LEVEL_DETAIL_TABLE.IS_REQUIRED);
	}
}
