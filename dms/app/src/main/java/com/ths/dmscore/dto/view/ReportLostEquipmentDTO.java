/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * DTO bao mat thiet bi
 * 
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  10:45:35 26-11-2014
 */
public class ReportLostEquipmentDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int totalItem;
	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}
	public ArrayList<EquipInventoryDTO> listItem = new ArrayList<EquipInventoryDTO>();
	
}
