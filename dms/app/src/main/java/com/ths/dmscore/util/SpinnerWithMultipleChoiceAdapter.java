
package com.ths.dmscore.util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.ObjectAdapterForSpinner;
import com.ths.dms.R;
/**
 * Spinner With Checkboxes
 * 
 * @author : TRUNGHQM 
 */
public class SpinnerWithMultipleChoiceAdapter extends ArrayAdapter<ObjectItemForSpinner>
		implements OnCheckedChangeListener, OnClickListener {

	public interface OnSpinnerEventListener {
		void onSpninerBtnOK(List<Integer> listIndex);
	}

	public MyListener l;
	public interface MyListener {
		public void onSelect();
	}
	
	
	private Context context;
	private boolean isAllSelected;
	private List<ViewHolder> listViewHolderCurrent = new ArrayList<ViewHolder>();
	OnSpinnerEventListener spinnerEventListener;
	private static int idResource = R.layout.layout_for_textview_all;
	List<ObjectItemForSpinner> arrObjectItem;	
	
	public static List<ObjectItemForSpinner> getListObjectItem2(List<DisplayProgrameItemDTO> listData,
			String fieldName) {
		List<ObjectItemForSpinner> result = new ArrayList<ObjectItemForSpinner>();
		ObjectItemForSpinner itemAll = new ObjectItemForSpinner(ObjectItemForSpinner.TYPE_ALL);
		result.add(itemAll);
		for (int i = 0; i < listData.size(); i++) {
			ObjectItemForSpinner itemTemp = new ObjectItemForSpinner(listData.get(i), listData.get(i).fullName,
					i, ObjectItemForSpinner.TYPE_INFO);
//			LogFile.logToFile("Item field " + i + " : " + itemTemp.getField());
			result.add(itemTemp);
		}
		return result;
		
	}

	public SpinnerWithMultipleChoiceAdapter(Context _context,
			List<ObjectItemForSpinner> lisItem,
			OnSpinnerEventListener spinnerEventListener) {
		super(_context, idResource, lisItem);
		this.spinnerEventListener = spinnerEventListener;
		// TODO Auto-generated constructor stub
		arrObjectItem = lisItem;
		context = _context;
		
		isAllSelected = true;
		int size = lisItem.size();
		List<Integer> lstIndex = new ArrayList<Integer>();
		if (size > 1) {
			for(int i = 1; i < size; i++){
				if(lisItem.get(i).isSelected){
					lstIndex.add(i);
				} else{
					isAllSelected = false;
				}
			}
			
			if (isAllSelected) {
				lisItem.get(0).isSelected = true;
			}
		}
		spinnerEventListener.onSpninerBtnOK(lstIndex);
	}

	private String getTextAllLabel(){
		StringBuilder strSelectNV = new StringBuilder();
		boolean notThingSelected = true;
		boolean allSelected = true;
		int size = arrObjectItem.size();
		if (size > 1) {
			for (int i = 1; i < size; i++) {
				if (arrObjectItem.get(i).isSelected) {
					notThingSelected = false;
					ObjectItemForSpinner item = arrObjectItem.get(i);
					if (StringUtil.isNullOrEmpty(strSelectNV.toString())) {
						strSelectNV.append(item.getField());
					} else {
						strSelectNV.append(", " + item.getField());
					}
				} else{
					allSelected = false;
				}
			}
		} else{
			notThingSelected = true;
			allSelected = false;
		}
		
		if (notThingSelected) {
			strSelectNV.setLength(0);
			strSelectNV.append("");
		} else if (allSelected){
			strSelectNV.setLength(0);
			strSelectNV.append(StringUtil.getString(R.string.TEXT_ALL));
		}
		
		return strSelectNV.toString();
	}
	
	TextView tvAll = null;
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inlflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			View view = inlflater.inflate(R.layout.layout_for_textview_all, parent, false);
			tvAll = (TextView) view.findViewById(R.id.tvAll);	
		} else{
			tvAll = (TextView) convertView;
		}
		String textAll = getTextAllLabel();
		tvAll.setText(textAll);
		return tvAll;
	}

	@Override
	public View getDropDownView(final int position, View convertView,
			ViewGroup parent) {

		// TODO Auto-generated method stub
		ObjectItemForSpinner item = arrObjectItem.get(position);
		LayoutInflater inlflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = (convertView == null ? null
				: (ViewHolder) convertView.getTag());

		isCodeCheck = true;

		View view = null;
		if (holder == null) {
			holder = new ViewHolder();
			view = inlflater.inflate(R.layout.layout_spinner_item, parent,
					false);
			holder.cb = (CheckBox) view.findViewById(R.id.cbItem);
			holder.tvData = (TextView) view.findViewById(R.id.tvData);
			holder.cb.setOnCheckedChangeListener(this);
			holder.tvData.setOnClickListener(this);
			holder.tvData.setClickable(true);
			// add holder to tag
			view.setTag(holder);
		} else {
			view = convertView;
		}

		holder.cb.setTag(position);

		holder.tvData.setTag(holder);
		if (position > 0) {
			String text = item.getField();
//			LogFile.logToFile("Vi tri : " + position +" : " + text);
			holder.tvData.setText(text);
			holder.cb.setVisibility(View.VISIBLE);
//			holder.cb.setChecked(itemCheckMappingTemp[position - 1]);
			holder.cb.setChecked(arrObjectItem.get(position).isSelected);
			holder.tvData.setVisibility(View.VISIBLE);
			holder.tvData.setOnClickListener(this);
			// add Info
			listViewHolderCurrent.add(holder);

		} else{
			// holder.cb.setSelected(isAllSelected);
			holder.tvData.setText(R.string.TEXT_ALL);
			holder.cb.setChecked(isAllSelected);
			holder.cb.setVisibility(View.VISIBLE);
			holder.tvData.setVisibility(View.VISIBLE);
			listViewHolderCurrent.add(holder);
		}

		isCodeCheck = false;
		return view;
	}

	class ViewHolder {
		TextView tvData;
		CheckBox cb;
	}

	boolean isCodeCheck = false;

	public void selectItemAtIndex(int index, boolean isChecked) {
		if (index == 0) {
			// check lai gia tri cua cb All
			isAllSelected = isChecked;
		} else {
			arrObjectItem.get(index).isSelected = isChecked;
		}

		int size = arrObjectItem.size();
		if (!isCodeCheck) {
			if (index == 0) {
				// set lai mang temp bang gia tri cua cb All
				if (size > 1) {
					for(int i = 1; i < arrObjectItem.size(); i++){
						arrObjectItem.get(i).isSelected = isChecked;
					}
				}

				for (int i = 0; i < listViewHolderCurrent.size(); i++) {
					ViewHolder holder = listViewHolderCurrent.get(i);
					int indexObject = (Integer) holder.cb.getTag();
					// set check Info item
					if (indexObject != 0) {
						// tranh mat check Tat ca
						isCodeCheck = true;
						holder.cb.setChecked(isChecked);
						isCodeCheck = false;
					}
				}
			} else {
				// set lai mang temp bang gia tri cua cb hien tai
				arrObjectItem.get(index).isSelected = isChecked;
				// set lai gia tri cua cb tat ca
				// neu isSelect = false
				boolean isAll = true;
				if (size > 1) {
					for(int i = 1; i < arrObjectItem.size(); i++){
						if(arrObjectItem.get(i).isSelected == false){
							isAll = false;
							break;
						}
					}
				}
				isAllSelected = isAll;
				
				if (isChecked == false) {
					// tim cb All
					for (int i = 0; i < listViewHolderCurrent.size(); i++) {
						ViewHolder holder = listViewHolderCurrent.get(i);
						int indexObject = (Integer) holder.cb.getTag();
						// tim thay cb All
						if (indexObject == 0) {
							if (holder.cb.isChecked()) {
								isCodeCheck = true;
								holder.cb.setChecked(false);
								isCodeCheck = false;
							}
							break;
						}
					}
				} else {
					if (isAll) {
						for (int i = 0; i < listViewHolderCurrent.size(); i++) {
							ViewHolder holder = listViewHolderCurrent.get(i);
							int indexObject = (Integer) holder.cb.getTag();
							// tim thay cb All
							if (indexObject == 0) {
								isCodeCheck = true;
								holder.cb.setChecked(true);
								isCodeCheck = false;
								break;
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

		int index = (Integer) buttonView.getTag();
		selectItemAtIndex(index, isChecked);
		if (spinnerEventListener != null) {
			List<Integer> result = new ArrayList<Integer>();
			int size = arrObjectItem.size();
			if (size > 1) {
				for(int i = 1; i < size; i++){
					if(arrObjectItem.get(i).isSelected){
						result.add(i);
					}
				}
			}
			spinnerEventListener.onSpninerBtnOK(result);
		}

		if (l != null) {
			l.onSelect();
		}
		
		if (tvAll != null) {
			String textAll = getTextAllLabel();
			if (textAll != null && !textAll.equals(tvAll.getText().toString())) {
				tvAll.setText(textAll);
				notifyDataSetChanged();
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.tvData || v.getId() == R.id.cbItem) {
			ViewHolder vHolder = (ViewHolder) v.getTag();
			if (vHolder != null) {
				boolean selected = vHolder.cb.isChecked();
				// change selected when click on text
				vHolder.cb.setChecked(!selected);
			}
		}
	}

	

	public static List<ObjectItemForSpinner> getListObjectItemEx(
			Object[] listData,
			ObjectAdapterForSpinner adapterForGSNPP) {
		
		List<ObjectItemForSpinner> result = new ArrayList<ObjectItemForSpinner>();
		ObjectItemForSpinner itemAll = new ObjectItemForSpinner(ObjectItemForSpinner.TYPE_ALL);
		result.add(itemAll);
		for (int i = 0; i < listData.length; i++) {
			ObjectItemForSpinner itemTemp = adapterForGSNPP.castObjectToObjectItem(listData[i]);
			itemTemp.setIndex(i);
			itemTemp.setType(ObjectItemForSpinner.TYPE_INFO);
			result.add(itemTemp);
		}
		return result;
	}	
}
