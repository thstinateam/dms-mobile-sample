/**
 * Copyright 2011 THSe. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.lib.network.http;

import com.ths.dmscore.lib.oAuth.model.OAuthRequest;
import com.ths.dmscore.lib.oAuth.builder.ServiceBuilder;
import com.ths.dmscore.lib.oAuth.builder.api.VT_Api;
import com.ths.dmscore.lib.oAuth.model.Token;
import com.ths.dmscore.lib.oAuth.model.Verb;
import com.ths.dmscore.lib.oAuth.oauth.OAuthService;

/**
 * OAuth request
 * 
 * @author: HieuNH
 * @version: 1.1
 * @since: 1.0
 */
public class OAuthRequestManager {
	public static final int TOKEN_INVALID = 401;
	public static final int TIMED_OUT = 500;
	public static final int SERVICE_UNAVAILABLE = 503;
	public static final int GATE_WAY_TIME_OUT = 504;

	// OAUTH2
	private static volatile String CLIENT_ID = "ThangNV31";
	private static volatile String CLIENT_SECRET = "1192d23f3d8470e0deab0a51d5946f9c";
	private static volatile String GRANT_TYPE = "client_credentials";
	private static volatile String RESPONSE_TYPE = "token";

	static volatile OAuthRequestManager instance;
	OAuthService service;
	public Token accessToken = null;

	private static volatile String serverPathOauth = "";

	public OAuthRequestManager() {
		initOAuth();
	}

	public static OAuthRequestManager getInstance() {
		if (instance == null || instance.accessToken == null) {
			instance = new OAuthRequestManager();
		}
		return instance;
	}

	public static boolean reInit() {
		try {
			instance = new OAuthRequestManager();
			if (instance == null) {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	private void initOAuth() {
		service = new ServiceBuilder().provider(VT_Api.class).apiKey(getCLIENT_ID()).apiSecret(getCLIENT_SECRET()).grantType(getGRANT_TYPE()).responseType(getRESPONSE_TYPE())
				.build();
		Token requestToken = service.getAccessToken(null, null);
		accessToken = requestToken;
	}

	public OAuthRequest signRequest(String requestUrl, String method) {
		if (accessToken == null) {
			return null;
		} else {
			OAuthRequest request;
			if (HTTPRequest.POST.equals(method)) {
				request = new OAuthRequest(Verb.POST, requestUrl);
			} else {
				request = new OAuthRequest(Verb.GET, requestUrl);
			}
			service.signRequest(accessToken, request);
			return request;
		}
	}

	public static void setOAuthInfo(String serverPath, String clientID, String clientSecret) {
		setServerPathOauth(serverPath);
		setCLIENT_ID(clientID);
		setCLIENT_SECRET(clientSecret);
	}

	public static String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public static void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public static String getCLIENT_SECRET() {
		return CLIENT_SECRET;
	}

	public static void setCLIENT_SECRET(String cLIENT_SECRET) {
		CLIENT_SECRET = cLIENT_SECRET;
	}

	public static String getGRANT_TYPE() {
		return GRANT_TYPE;
	}

	public static void setGRANT_TYPE(String gRANT_TYPE) {
		GRANT_TYPE = gRANT_TYPE;
	}

	public static String getRESPONSE_TYPE() {
		return RESPONSE_TYPE;
	}

	public static void setRESPONSE_TYPE(String rESPONSE_TYPE) {
		RESPONSE_TYPE = rESPONSE_TYPE;
	}

	public static String getServerPathOauth() {
		return serverPathOauth;
	}

	public static void setServerPathOauth(String serverPathOauth) {
		OAuthRequestManager.serverPathOauth = serverPathOauth;
	}
}
