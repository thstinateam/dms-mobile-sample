/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view.trainingplan;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SUP_TRAINING_CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.SUP_TRAINING_RESULT_TABLE;
import com.ths.dmscore.lib.sqllite.db.TRAINING_RATE_DETAIL_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * TrainingRateDetailDTO.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 4:03:24 PM Nov 14, 2014
 */
public class TrainingRateDetailResultlDTO implements Serializable {
	private static final long serialVersionUID = -8896404785707569613L;
	// ma tieu chi
	private long trainingRateID;
	// ma tieu chuan
	private long trainingRateDetailID;
	private long subTrainingCustomerID;
	private long subTrainingResultID;
	// ten viet tat de hien thi len tab
	private String shortStandardName;
	// ten fullDate du
	private String fullStandardName;
	// so thu tu de hien thi
	private int orderNumber;
	// code tablet sinh de web group data tong hop
	private String code;
	// ket qua cham: 0: chua cham, 1: ko ap dung, 2: dat, 3: ko dat
	private int result;
	// 0: tieu chuan do nhan vien tao; 1: tieu chuan mac dinh
	private int defaultRate;
	// da luu hay chua: true: da luu, false: moi tao chua luu
	private boolean isSaved;
	// nguoi tao
	private String createUser;
	// ngay tao
	private String createDate;
	// nguoi cap nhat
	private String updateUser;
	// ngay cap nhat
	private String updateDate;
	// true: da xoa, false: chua xoa
	private boolean isDeleted;
	// true: insert, false: update
	private boolean isInsert;

	public TrainingRateDetailResultlDTO() {
		// TODO Auto-generated constructor stub
		trainingRateID = 0;
		trainingRateDetailID = 0;
		orderNumber = 0;
		subTrainingCustomerID = 0;
		result = 0;
		defaultRate = 0;
		isSaved = false;
		isDeleted = false;
		isInsert = true;
		code = Constants.STR_BLANK;
		shortStandardName = Constants.STR_BLANK;
		fullStandardName = Constants.STR_BLANK;
		createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		createDate = DateUtils.now();
		updateDate = Constants.STR_BLANK;
		updateUser = Constants.STR_BLANK;
	}

	public String getShortStandardName() {
		return shortStandardName;
	}

	public void setShortStandardName(String shortName) {
		this.shortStandardName = shortName;
	}

	public String getFullStandardName() {
		return fullStandardName;
	}

	public void setFullStandardName(String fullName) {
		this.fullStandardName = fullName;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public long getTrainingRateID() {
		return trainingRateID;
	}

	public void setTrainingRateID(long trainingRateID) {
		this.trainingRateID = trainingRateID;
	}

	public long getTrainingRateDetailID() {
		return trainingRateDetailID;
	}

	public void setTrainingRateDetailID(long trainingRateDetailID) {
		this.trainingRateDetailID = trainingRateDetailID;
	}

	public long getSubTrainingCustomerID() {
		return subTrainingCustomerID;
	}

	public void setSubTrainingCustomerID(long subTrainingCustomerID) {
		this.subTrainingCustomerID = subTrainingCustomerID;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getDefaultRate() {
		return defaultRate;
	}

	public void setDefaultRate(int defaultRate) {
		this.defaultRate = defaultRate;
	}

	/**
	 * khoi tao data tu cursor
	 * 
	 * @author: dungdq3
	 * @since: 2:44:27 PM Nov 17, 2014
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		subTrainingCustomerID = CursorUtil.getLong(c, SUP_TRAINING_CUSTOMER_TABLE.SUP_TRAINING_CUSTOMER_ID);
		subTrainingResultID = CursorUtil.getLong(c, SUP_TRAINING_RESULT_TABLE.SUP_TRAINING_RESULT_ID);
		trainingRateDetailID = CursorUtil.getLong(c, SUP_TRAINING_RESULT_TABLE.TRAINING_RATE_DETAIL_ID);
		shortStandardName = CursorUtil.getString(c, "shortStandardName");
		fullStandardName = CursorUtil.getString(c, "fullStandardName");
		orderNumber = CursorUtil.getInt(c, "standardOrderNum");
		defaultRate = CursorUtil.getInt(c, "defaultRate");
		result = CursorUtil.getInt(c, "result");
		// do tieu chuan nay da duoc luu trong db
		isSaved = true;
	}

	public long getSubTrainingResultID() {
		return subTrainingResultID;
	}

	public void setSubTrainingResultID(long subTrainingResultID) {
		this.subTrainingResultID = subTrainingResultID;
	}

	public boolean isSaved() {
		return isSaved;
	}

	public void setSaved(boolean isSaved) {
		this.isSaved = isSaved;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	/**
	 * generate json
	 * 
	 * @author: dungdq3
	 * @param jarr 
	 * @since: 3:15:53 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException
	 */
	public void generateJSONObject(JSONArray jsonArray) throws JSONException {
		// TODO Auto-generated method stub
		if (!isSaved && !isDeleted) {
			JSONObject json = generateInsert();
			jsonArray.put(json);
		} else if(isSaved) { // da luu
			// chua xoa thi update
			if(!isDeleted){
				JSONObject json = generateUpdate();
				jsonArray.put(json);
//			} else { // da xoa
//				JSONObject json = generateDelete();
//				jsonArray.put(json);
////				if(result > 0){
//					JSONObject jsonResult = generateDeleteResult();
//					jsonArray.put(jsonResult);
//				}
			}
		}
	}
	
	/**
	 * xoa kq tuong ung
	 * 
	 * @author: dungdq3
	 * @since: 5:18:25 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateDeleteResult() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_RESULT_TABLE.TABLE_NAME);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.TRAINING_RATE_DETAIL_ID, trainingRateDetailID, null));
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	/**
	 * genarate json insert
	 * 
	 * @author: dungdq3
	 * @since: 3:33:02 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateInsert() throws JSONException{
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.INSERT);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, TRAINING_RATE_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.TRAINING_RATE_DETAIL_ID, trainingRateDetailID, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.TRAINING_RATE_ID, trainingRateID, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CODE, code, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.SHORT_NAME, shortStandardName, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.FULL_NAME, fullStandardName, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.ORDER_NUMBER, orderNumber, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.IS_DEFAULT_RATE, defaultRate, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CREATE_DATE, createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CREATE_USER, createUser, null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			isSaved = true;
		} catch (JSONException e) {
			throw e;
		}
		return trainingRateJson;
	}
	
	/**
	 * generate json delete
	 * 
	 * @author: dungdq3
	 * @since: 3:34:49 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateDelete() throws JSONException{
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.DELETE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, TRAINING_RATE_DETAIL_TABLE.TABLE_NAME);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.TRAINING_RATE_DETAIL_ID, trainingRateDetailID, null));
			wheres.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}
	
	/**
	 * generate update
	 * 
	 * @author: dungdq3
	 * @since: 3:32:34 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException
	 */
	public JSONObject generateUpdate() throws JSONException{
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, TRAINING_RATE_DETAIL_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			if(!StringUtil.isNullOrEmpty(shortStandardName)){
				detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.SHORT_NAME, shortStandardName, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.FULL_NAME, fullStandardName, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.ORDER_NUMBER, orderNumber, null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.UPDATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.TRAINING_RATE_DETAIL_ID, trainingRateDetailID, null));
			wheres.put(GlobalUtil.getJsonColumn(TRAINING_RATE_DETAIL_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	/**
	 * generate json result
	 * 
	 * @author: dungdq3
	 * @since: 3:38:56 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateJSONObjectResult() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject json = null;
		if (isInsert) {
			json = generateInsertResult();
		} else { // da luu
			json = generateUpdateResult();
		}
		return json;
	}

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 3:46:06 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateUpdateResult() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_RESULT_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.RESULT, result, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.UPDATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.SUP_TRAINING_RESULT_ID, subTrainingResultID, null));
			wheres.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			throw e;
		}
		
		return trainingRateJson;
	}

	/**
	 * generate insert result
	 * 
	 * @author: dungdq3
	 * @since: 3:41:42 PM Nov 18, 2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateInsertResult() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject trainingRateJson = new JSONObject();
		try {
			trainingRateJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.INSERT);
			trainingRateJson.put(IntentConstants.INTENT_TABLE_NAME, SUP_TRAINING_RESULT_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.SUP_TRAINING_CUSTOMER_ID, subTrainingCustomerID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.SUP_TRAINING_RESULT_ID, subTrainingResultID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.TRAINING_RATE_DETAIL_ID, trainingRateDetailID, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.CREATE_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId(), null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.RESULT, result, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.CREATE_DATE, createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(SUP_TRAINING_RESULT_TABLE.CREATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode(), null));
			trainingRateJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
			throw e;
		}
		return trainingRateJson;
	}

	public boolean isInsert() {
		return isInsert;
	}

	public void setInsert(boolean isInsert) {
		this.isInsert = isInsert;
	}

	/**
	 * tao code cho tieu chuan
	 * 
	 * @author: dungdq3
	 * @since: 4:28:51 PM Nov 18, 2014
	 * @return: void
	 * @throws:  
	 */
	public void generateCode() {
		// TODO Auto-generated method stub
		String staffID = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		code = staffID + DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_FILE_EXPORT);
	}

}
