/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerSaleOfNVBHDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

public class NVBHReportCustomerSaleRow extends DMSTableRow implements
		OnClickListener {
	// stt
	private TextView tvSTT;
	private TextView tvCustomerCode;
	private TextView tvCustomerName;
	
	public TextView tvAmountApprovedPre;
	public TextView tvAmountApproved;
	public TextView tvAmount;

	public TextView tvQuantityApprovedPre;
	public TextView tvQuantityApproved;
	public TextView tvQuantity;
	private TextView tvAmountInfo;
	private VinamilkTableListener listenerTable;
	private CustomerSaleOfNVBHDTO dto;

	public NVBHReportCustomerSaleRow(Context context, VinamilkTableListener listenerTable) {
		super(context, R.layout.layout_nvbh_report_customer_sale_row, 1.0, GlobalInfo.getInstance().isSysShowPrice() ? null : new int[]{R.id.llAmount});
		this.listenerTable = listenerTable;
		this.setOnClickListener(this);
		this.initViewControlNomalRow();
	}

	/**
	 *
	 * init view control for nomal row (not total row)
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void initViewControlNomalRow() {
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvCustomerCode = (TextView) findViewById(R.id.tvCustomerCode);
		tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
		
		tvAmountApprovedPre = (TextView) findViewById(R.id.tvAmountApprovedPre);
		tvAmountApproved = (TextView) findViewById(R.id.tvAmountApproved);
		tvAmount = (TextView) findViewById(R.id.tvAmount);

		tvQuantityApprovedPre = (TextView) findViewById(R.id.tvQuantityApprovedPre);
		tvQuantityApproved = (TextView) findViewById(R.id.tvQuantityApproved);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
		tvAmountInfo = (TextView) findViewById(R.id.tvAmountInfo);
		
		String currency = StringUtil.getReportUnitTitle();
		String amountInfo = StringUtil.getString(R.string.TEXT_SALES_) + " - " + currency;
		display(tvAmountInfo, amountInfo);
	}

	/**
	 *
	 * render layout for row product info
	 *
	 * @param dto
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void renderLayout(CustomerSaleOfNVBHDTO dto, int stt) {
		this.dto = dto;
		tvCustomerCode.setOnClickListener(this);
		tvSTT.setText(String.valueOf(stt));
		tvCustomerCode.setText(dto.customerCode);
		tvCustomerName.setText(dto.customerName);

		display(tvAmountApprovedPre, dto.amountApprovedPre);
		display(tvAmountApproved, dto.amountApproved);
		display(tvAmount, dto.amount);
		
		display(tvQuantityApprovedPre, dto.quantityApprovedPre);
		display(tvQuantityApproved, dto.quantityApproved);
		display(tvQuantity, dto.quantity);
	}

	@Override
	public void onClick(View v) {
		if (tvCustomerCode == v) {
			listenerTable.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_CUSTOMER_INFO, this, dto);
		}
	}
}
