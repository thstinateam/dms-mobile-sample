package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.ShopLockDTO;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class SHOP_LOCK_TABLE extends ABSTRACT_TABLE {

	// id cua table
	public static final String SHOP_LOCK_ID = "SHOP_LOCK_ID";
	// id cua shop khai bao
	public static final String LOCK_DATE = "LOCK_DATE_1";
	public static final String LOCKED_DATE = "LOCK_DATE";
	// type param
	public static final String CREATE_DATE = "CREATE_DATE";
	// code param
	public static final String CREATE_USER = "CREATE_USER";
	// id cua shop khai bao
	public static final String SHOP_ID = "SHOP_ID";
	// id cua shop khai bao
	public static final String STAFF_LOCK = "STAFF_LOCK";
	// id cua shop khai bao
	public static final String SHOP_LOCK = "SHOP_LOCK";
	// id cua shop khai bao
	public static final String SYN_STATE = "SYN_STATE";

	// table name
	public static final String TABLE_NAME = "SHOP_LOCK";

	public SHOP_LOCK_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SHOP_LOCK_ID, LOCK_DATE, CREATE_DATE,
				CREATE_USER, SHOP_ID, STAFF_LOCK, SHOP_LOCK, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param data
	 * @return
	 * @return: StockLockDTO
	 * @throws:
	*/
	public ShopLockDTO getShopLock(String shopId){
		ShopLockDTO shopLock = new ShopLockDTO();
//		shopLock.lockDate = DateUtils.now();
		String date_now = DateUtils
				.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
//		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		
		StringBuilder sqlRequest = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		sqlRequest.append("SELECT * , DATE(sl.LOCK_DATE, '+1 fullDate') LOCK_DATE_1 ");
		sqlRequest.append("FROM   SHOP_LOCK sl ");
		sqlRequest.append("WHERE  1=1 ");
//		sqlRequest.append("       AND sl.staff_id = ? ");
//		params.add(staffId);
		sqlRequest.append("       AND sl.shop_id = ? "); 
		params.add(shopId);
		sqlRequest.append("       AND DATE(sl.LOCK_DATE, '+1 fullDate') = DATE(?) ");
		params.add(date_now);

		Cursor c = null;
		try {
			c = rawQueries(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					shopLock.initFromCursor(c);
				}
			} 
		} catch (Exception e) {
			MyLog.e("getShopLock", "fail", e);
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return shopLock;
	}
}
