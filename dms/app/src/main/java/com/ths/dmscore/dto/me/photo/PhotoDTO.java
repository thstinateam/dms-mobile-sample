/**
 * Copyright 2011 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.me.photo;

import java.io.Serializable;



/**
 *  Thong tin cua hinh anh
 *  @author: HaiTC
 *  @version: 1.0
 *  @since: 1.0
 */
public class PhotoDTO implements Serializable{
	private static final long serialVersionUID = 4685894007184421609L;
	public String mediaId;
	public String thumbUrl;
	public String fullUrl;
	public String createdTime;
	public double lat;
	public double lng;
	public String createUser;
	public String staffName;
	public String staffCode;
	public String customerCode;
	public String customerName;
	public String localUrl;
	public String fullCreatedTime;
	public PhotoDTO(){
		mediaId = "";
		thumbUrl = "";
		fullUrl="";
		createdTime="";
		lat = 0;
		lng = 0;
		staffName = "";
		customerCode = "";
		customerName = "";
		fullCreatedTime = "";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null){
		    return false;
		}

		if (this.getClass() != o.getClass()){
		    return false;
		}
		
		return mediaId==((PhotoDTO)o).mediaId;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
