package com.ths.dmscore.lib.oAuth.model;

public enum SignatureType
{
  Header,
  QueryString
}
