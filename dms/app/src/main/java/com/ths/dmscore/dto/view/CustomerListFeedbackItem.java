package com.ths.dmscore.dto.view;

import com.ths.dmscore.dto.db.CustomerDTO;

/**
 * DTO cho row khach hang hien thi
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerListFeedbackItem {
	public CustomerDTO customer = new CustomerDTO();
	public boolean isCheck = false;

	@Override
	public Object clone()  {
		CustomerListFeedbackItem p = new CustomerListFeedbackItem();
		p.customer = this.customer;
		p.isCheck = this.isCheck;
		return p;
	}
}
