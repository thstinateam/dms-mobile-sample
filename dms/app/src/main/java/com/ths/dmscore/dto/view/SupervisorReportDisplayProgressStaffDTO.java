package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cua man hinh bao cao chuong trinh trung bay NVBH
 * @author hieunq1
 *
 */
public class SupervisorReportDisplayProgressStaffDTO {
	public ArrayList<SupervisorReportDisplayProgressStaffItem> listItem = new ArrayList<SupervisorReportDisplayProgressStaffItem>();
	public ArrayList<SupervisorReportDisplayProgressItem> listPrograme = new ArrayList<SupervisorReportDisplayProgressItem>();
	public ArrayList<String> arrLevelCode = new ArrayList<String>();
	public SupervisorReportDisplayProgressStaffItem totalItem = new SupervisorReportDisplayProgressStaffItem();
	public double standardProgress;

	public SupervisorReportDisplayProgressStaffItem newStaffDisProComProgReportItem() {
		return new SupervisorReportDisplayProgressStaffItem();
	}
	
	public void addItem(SupervisorReportDisplayProgressStaffItem item) {
		listItem.add(item);
	}
	
}
