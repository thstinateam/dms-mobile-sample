/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.network.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import android.os.AsyncTask;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.lib.oAuth.model.OAuthRequest;
import com.ths.dmscore.lib.oAuth.model.Response;
import com.ths.dmscore.lib.oAuth.utils.Preconditions;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 *  asyn task luon dong bo du lieu tu server ve tablet
 *  clone source from: HttpAsyncTask
 *  @author: HaiTC3
 *  @version: 1.1
 *  @since: 1.0
 */
public class HttpAsyncTaskSynData extends AsyncTask<Void, Void, Void> {
	private static int CONNECT_TIMEOUT = 120000; // miliseconds: 2phut: thoi gian request
	private static int TIME_OUT = 360000;//miliseconds: 3phut: thoi gian response
	private HTTPRequest request;
	private HTTPResponse response;
	private boolean isSuccess;
	private static final String LOG_TAG = "HttpAsyncTaskSynData";
	private int readTimeout = TIME_OUT;
	private int connectTimeout = CONNECT_TIMEOUT;
	
	public HttpAsyncTaskSynData(HTTPRequest re) {
		this.request = re;
		this.readTimeout = TIME_OUT;
	}
	public HttpAsyncTaskSynData(HTTPRequest re, int timeout) {
		this.request = re;
		this.readTimeout = timeout;
	}
	
	public HttpAsyncTaskSynData(HTTPRequest re, int connnectTimeOut, int readTimeOut) {
		this.request = re;
		this.connectTimeout = connnectTimeOut;
		this.readTimeout = readTimeOut;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		if (request == null || request.isAlive() == false) {
			if (request == null) {
				MyLog.i(LOG_TAG, "Request null");
			}else {
				MyLog.i(LOG_TAG, "Request NOT alive");
			}
			return null;
		}		
		
		int countRetry = 0;
		final int NUM_RETRY = 1;
		boolean isRetry = false;
		do {
			isRetry = false;
			countRetry++;
			HttpURLConnection connection = null;				
			isSuccess = true;
			//bug sometime response code = -1
			System.setProperty("http.keepAlive", "false");
			try {
				response = new HTTPResponse(request);
				OAuthRequest oAuthRequest = OAuthRequestManager.getInstance().signRequest(request.getUrl(), request.getMethod());//HieuNH add OAuthRequest
				if(oAuthRequest != null){					
					oAuthRequest.createConnection();
					connection = oAuthRequest.getConnection();			 	
					
					oAuthRequest.setConnectTimeout(connectTimeout, TimeUnit.MILLISECONDS);
					oAuthRequest.setReadTimeout(readTimeout, TimeUnit.MILLISECONDS);
					if (request.getContentType() != null) {
						connection.setRequestProperty("Content-type", request.contentType);
					}
					if (HTTPRequest.POST.equals(request.getMethod())){					
						DataSupplier.Data data = new DataSupplier.Data();
						request.getNextPart(data);
						oAuthRequest.addPayload(data.buffer);
					}				
					if (HTTPClient.getSessionID() != null) {
						oAuthRequest.addHeader("Cookie", HTTPClient.getSessionID());
						connection.setRequestProperty("Cookie", HTTPClient.getSessionID());
					}
					
					Response response1 = oAuthRequest.doSend();
					// Hai - edit
					if(response1.getCode() != ErrorConstants.ERROR_CODE_SUCCESS){
						response.setDataText(response1.getBody());
					}else{
						response.setDataText(this.getStreamContentsSynData(response1.getStream()));
					}
					
					response.setCode(response1.getCode());
					if(response.getCode() == OAuthRequestManager.TOKEN_INVALID 
							|| response.getCode() == OAuthRequestManager.TIMED_OUT
							|| response.getCode() == OAuthRequestManager.SERVICE_UNAVAILABLE 
							|| response.getCode() == OAuthRequestManager.GATE_WAY_TIME_OUT ){	
						if (response.getCode() == OAuthRequestManager.GATE_WAY_TIME_OUT || 
							response.getCode() == OAuthRequestManager.SERVICE_UNAVAILABLE){
							isSuccess = false;
							response.setError(HTTPClient.ERR_TIME_OUT, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER),"");
						}else{
							response.setCode(OAuthRequestManager.TIMED_OUT);
						}
						OAuthRequestManager.getInstance().accessToken = null;						
					}				
					if (response.getDataText() == null && response.getDataBinary() == null && request.isAlive()) {
						isSuccess = false;
						isRetry = true;
						StringBuffer strBuffer = new StringBuffer();						
						strBuffer.append("ResponseCode: " + connection.getResponseCode());
						strBuffer.append("/ResponseMsg: " + connection.getResponseMessage());
						response.setError(HTTPClient.ERR_UNKNOWN, strBuffer.toString());
					}
				}else{
					response.setCode(OAuthRequestManager.TIMED_OUT);//vi request dang bi timeout
				}
				
			} catch (MalformedURLException e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Exception : " + e.getMessage());
				isSuccess = false;
				response.setError(HTTPClient.ERR_INVALID_URL, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "MalformedURLException - " + e.getMessage() + "/" + e.toString());
			} catch (FileNotFoundException e) {
				// TODO: handle exception
				isSuccess = false;
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.i(LOG_TAG, "FileNotFoundException - " + e.getMessage() + "/" + e.toString());
			} catch (SocketTimeoutException e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Exception : SocketTimeoutException " + e.getMessage());
				isSuccess = false;
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "FileNotFoundException - " + e.getMessage() + "/" + e.toString());
			} catch (UnknownHostException e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " UnknownHostException : " + e.getMessage());
				isSuccess = false;
				isRetry = true;
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "FileNotFoundException - " + e.getMessage() + "/" + e.toString());
			} catch (ConnectException e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				isSuccess = false;
				isRetry = true;
				response.setError(HTTPClient.ERR_NO_CONNECTION, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECT_SERVER), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "IOException - " + e.getMessage() + "/" + e.toString());
			}  catch (IOException e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " IOException : " + e.getMessage());
				isSuccess = false;
				isRetry = true;
				response.setError(HTTPClient.ERR_UNKNOWN, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECTION), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "IOException - " + e.getMessage() + "/" + e.toString());
			} catch (Exception e) {
				//MyLog.logToFile("HttpAsyncTask", DateUtils.now() + " Throwable : " + e.getMessage());
				isSuccess = false;
				isRetry = false;
				response.setError(HTTPClient.ERR_UNKNOWN, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_NO_CONNECTION), e.getMessage() + "/" + e.toString());
				MyLog.e(LOG_TAG, "Throwable - " + e.getMessage() + "/" + e.toString());
			} finally {						
				if (connection != null){
					MyLog.i(LOG_TAG, "disconnect");
					connection.disconnect();
				}
			}
		}while (countRetry <= NUM_RETRY && isRetry);
		
		HTTPListenner listenner = null;
		boolean isAlive = true;
		if (response != null) {
			listenner = response.getObserver();
			isAlive = response.request.isAlive();
		}
		if (listenner != null && isAlive) {
			//if(!GlobalUtil.checkActionSave(request.getAction())){
				if (isSuccess) {
					listenner.onReceiveData(response);
				} else {
					listenner.onReceiveError(response);
				}
			//}
		}
		return null;
	}
	
	

	/**
	 * lay noi dung cho chuc nang dong bo du lieu va ghi xuong file o fullDate
	 * @author: HaiTC3
	 * @param is
	 * @return
	 * @return: String
	 * @throws:
	 */
	public String getStreamContentsSynData(InputStream is) {
		String result;
		Preconditions.checkNotNull(is, "Cannot get String from a null object");
		FileOutputStream fop = null;
		File file;

		try {
			MyLog.i("SYNDATA", "HTTP: getStreamContentsSynData : 1");
			String fileName = Constants.TEMP_SYNDATA_FILE + "_"
					+ DateUtils.getCurrentDateTimeWithFormat(null) + ".txt";
			GlobalUtil.setPathSynDataFileName(fileName);
			file = new File(ExternalStorage.getPathSynData(),
					fileName);
			MyLog.i("SYNDATA", "HTTP: getStreamContentsSynData : 2" + fileName);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			else{
				file.delete();
				file.createNewFile();
			}
			fop = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int read;
			while ((read = is.read(buf)) > 0) {
				fop.write(buf, 0, read);
				MyLog.i("SYNDATA", "HTTP: getStreamContentsSynData : read buffer");
			}
			MyLog.i("SYNDATA", "HTTP: getStreamContentsSynData : read buffer DONE");
			fop.flush();
			result = ErrorConstants.RESPONSE_STRING_SYNDATA_SUCCESS;
		} catch (IOException ioe) {
			//MyLog.logToFile("SYNDATA", "HTTP: getStreamContentsSynData : " + ioe.toString());
			MyLog.d("SYNDATA", "HTTP: getStreamContentsSynData : " + ioe.toString());
			
			result = ErrorConstants.RESPONSE_STRING_SYNDATA_ERROR;
			throw new IllegalStateException(
					"Error while reading response body", ioe);
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				MyLog.e("getStreamContentsSynData", "fail", e);
			}
		}
		return result;
	}
}
