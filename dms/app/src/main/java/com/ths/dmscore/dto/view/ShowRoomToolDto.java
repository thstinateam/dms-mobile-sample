package com.ths.dmscore.dto.view;

public class ShowRoomToolDto {
	public String toolCode;
	public String toolName;
	public boolean isPassed;

	public ShowRoomToolDto() {
		toolCode = "A01";
		toolName = "Tu lanh";
		isPassed = false;
	}
}
