package com.ths.dmscore.global;

/**
 *  Thong tin cau hinh build
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ServerPath {
	
	//1. server DEV - ThienHai
	public static final String SERVER_TYPE = "TEST";
	public static final String SERVER_HTTP = "http://124.158.10.63:8380/";
	public static final String SERVER_HTTPS = "http://124.158.10.63:8380/";
	public static final String IMAGE_PRODUCT_VNM = "http://124.158.10.63:9999/image/ProductImage/";
	public static final String SERVER_PATH_OAUTH = "http://124.158.10.63:8380/";
	public static final String IMAGE_PATH = "http://124.158.10.63:9999/thienhai";
	public static final String SERVER_PATH_SYN_DATA = SERVER_PATH_OAUTH + "synchronizer";
	public static final String CV_IMAGE_PATH = "http://124.158.10.63:9999/TP/";
	public static final String SERVER_PATH_NATIVE_LIB = "http://124.158.10.63:9999/nativelibs/";
	public static final String SERVER_HTTP_CHART = "http://124.158.10.63:9090/";
    public static final String SERVER_HTTPS_CHART = "http://124.158.10.63:9090/";

	//1. server DEV - TungLH2
//	public static final String SERVER_TYPE = "TEST";
//	public static final String SERVER_HTTP = "http://192.168.9.131:8380/";
//	public static final String SERVER_HTTPS = "http://192.168.9.131:8380/";
//	public static final String IMAGE_PRODUCT_VNM = "http://192.168.9.131:9999/thienhai/upload/ProductImage/";
//	public static final String SERVER_PATH_OAUTH = "http://192.168.9.131:8380/";
//	public static final String IMAGE_PATH = "http://192.168.9.131:9999/";
//	public static final String SERVER_PATH_SYN_DATA = SERVER_PATH_OAUTH + "synchronizer";
//	public static final String CV_IMAGE_PATH = "http://192.168.9.131:9999/TP/";
//	public static final String SERVER_PATH_NATIVE_LIB = "http://192.168.9.131:9999/nativelibs/";

}
