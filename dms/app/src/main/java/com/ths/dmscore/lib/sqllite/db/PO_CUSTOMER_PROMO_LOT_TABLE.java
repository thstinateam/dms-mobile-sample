package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.util.StringUtil;


/**
 * Luu thong tin KM cho don PO
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class PO_CUSTOMER_PROMO_LOT_TABLE extends ABSTRACT_TABLE {

	// id
	public static final String PO_CUSTOMER_PROMO_LOT_ID = "PO_CUSTOMER_PROMO_LOT_ID";
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	public static final String PO_CUSTOMER_DETAIL_ID = "PO_CUSTOMER_DETAIL_ID";
	// 0: Km tu dong, 1:KM bang tay,2: cham chung bay, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	// PROGRAM_TYPE = 0,1,3,4,5 thi ma la CTKM; PROGRAM_TYPE = 2 --> ma la CTTB
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// % chiet khau
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// So tien chiet khau
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	//public static final String MAX_QUANTITY_FREE_CK = "MAX_QUANTITY_FREE_CK";
	// 1. La khuyen mai cua chinh sp, 0. La khuyen mai do chia deu (tu don hang, nhom)
	public static final String IS_OWNER = "IS_OWNER";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String PO_CUSTOMER_LOT_ID = "PO_CUSTOMER_LOT_ID";

	public static final String TABLE_NAME = "PO_CUSTOMER_PROMO_LOT";

	public PO_CUSTOMER_PROMO_LOT_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PO_CUSTOMER_PROMO_LOT_ID, PO_CUSTOMER_ID, PROGRAM_TYPE, PROGRAM_CODE, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				 IS_OWNER, SYN_STATE, STAFF_ID, PO_CUSTOMER_DETAIL_ID, PROGRAME_TYPE_CODE, SHOP_ID, ORDER_DATE,PO_CUSTOMER_LOT_ID
				/*,MAX_QUANTITY_FREE_CK*/
				};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	protected long insert(AbstractTableDTO dto, long poCustomerLotId,  long poCustomerPromoLotId ) {
		SaleOrderPromoDetailDTO promoDto = (SaleOrderPromoDetailDTO) dto;
		return insert(null, initDataRow(promoDto, poCustomerLotId,poCustomerPromoLotId));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(SaleOrderPromoDetailDTO dto, long poCustomerLotId, long poCustomerPromoLotId) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(PO_CUSTOMER_PROMO_LOT_ID, poCustomerPromoLotId);
		editedValues.put(DISCOUNT_AMOUNT, dto.discountAmount);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercent);
		if(dto.isOwner >=0)
			editedValues.put(IS_OWNER, dto.isOwner);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(PROGRAM_TYPE, dto.programType);
		if (!StringUtil.isNullOrEmpty(dto.programTypeCode)) {
			editedValues.put(PROGRAME_TYPE_CODE, dto.programTypeCode);
		}
		editedValues.put(PO_CUSTOMER_DETAIL_ID, dto.poDetailId);
		editedValues.put(PO_CUSTOMER_ID, dto.poId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(ORDER_DATE, dto.orderDate);
		editedValues.put(PO_CUSTOMER_LOT_ID, poCustomerLotId);

		return editedValues;
	}

	 /**
	 * Xoa tat ca po_promo_lot
	 * @author: Tuanlt11
	 * @param saleOrderId
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int deleteAllDetailOfOrder(long poCustomerId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(poCustomerId));

		return delete(PO_CUSTOMER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}
}
