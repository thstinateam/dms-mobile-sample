/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.stock;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.AsyncTaskUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Man trang thai kho
 *
 * @author: ThangNV31
 * @version: 1.0
 * @since: 1.0
 */
public class LockDateValView extends BaseFragment implements OnClickListener, OnEventControlListener {

	public static final int CONFIRM_LOCK_DATE_OK = 1;
	public static final int CONFIRM_LOCK_DATE_CANCEL = 2;
	public static final int CONFIRM_DRAFT_ORDER = 3;
	public static final int ACTION_IMAGE = 4;
	private static final int ACTION_CUS_LIST = 5;
	private static final int ACTION_PATH = 6;
	private static final int ACTION_ORDER_LIST = 7;
	private static final int ACTION_CREATE_CUSTOMER = 8;
	private static final int ACTION_LOCK_DATE = 9;

	private TextView tvLockDate; // Trang thai kho
	private Button btLockDate; // Mo kho
	private int lockDateState; // Trang thai
	private boolean hasDP = false;

	public static LockDateValView newInstance(Bundle data) {
		LockDateValView f = new LockDateValView();
		f.setArguments(data);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.layout_lock_date_view, null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriForm.NVBH_CHOTKHO);
		initView(view);
		getLockDateState();
		return view;
	}

	 /**
	 * Khoi tao view
	 * @author: Tuanlt11
	 * @param v
	 * @return: void
	 * @throws:
	*/
	private void initView(View v) {
//		setTitleHeaderView(StringUtil.getString(R.string.TITLE_VIEW_STOCK_STATE));
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_STOCK_STATE));
		this.tvLockDate = (TextView) v.findViewById(R.id.tvLockDate);
		tvLockDate.setText(StringUtil.getString(R.string.TEXT_LOCK_DATE_VAL) + ": " + DateUtils.getCurrentDate());
		tvLockDate.setVisibility(View.VISIBLE);
		this.btLockDate = (Button) v.findViewById(R.id.btLockDate);
		this.btLockDate.setOnClickListener(this);
	}
	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		addMenuItem(PriForm.NVBH_CHOTKHO,
		new MenuTab(R.string.TEXT_PHOTO,
				R.drawable.menu_picture_icon, ACTION_IMAGE, PriForm.NVBH_DSHINHANH),
		new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
				R.drawable.icon_lock, ACTION_LOCK_DATE, PriForm.NVBH_CHOTKHO),
		new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
				ACTION_CREATE_CUSTOMER, PriForm.NVBH_THEMMOIKH_DANHSACH),
		new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
				ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
		new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
				ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
		new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
				ACTION_CUS_LIST,PriForm.NVBH_BANHANG_DSKH));
	}
	/**
	 * cap nhat giao dien
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		if (lockDateState == 1) {
			// da chot ngay
			btLockDate.setVisibility(View.GONE);
			tvLockDate.setText(StringUtil
					.getString(R.string.TEXT_STOCK_LOCK_STATUS)
					+ StringUtil.getString(R.string.TEXT_STOCK_LOCK_CLOSE));
		} else if (!hasDP) {
			// khong co xuat hang vansale
			// (khong co don DP)
			btLockDate.setVisibility(View.GONE);
			tvLockDate.setText(StringUtil
					.getString(R.string.TEXT_STOCK_LOCK_STATUS)
					+ StringUtil.getString(R.string.TEXT_STOCK_LOCK_CLOSE));
		} else {
			btLockDate.setVisibility(View.VISIBLE);
			tvLockDate.setText(StringUtil
					.getString(R.string.TEXT_STOCK_LOCK_STATUS)
					+ StringUtil.getString(R.string.TEXT_STOCK_LOCK_OPEN));
		}
	}

	/**
	 * lay trang thai kho
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	private void getLockDateState(){
		parent.showLoadingDialog();
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_STAFF_ID, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(bundle, ActionEventConstant.ACTION_GET_LOCK_DATE_STATE, SaleController.getInstance());
	}

	/**
	 * cap nhat trang thai kho
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param state
	 */
	private void insertLockDateState(int state) {
		Bundle bundle = new Bundle();
		LockDateDTO dto = new LockDateDTO();
		dto.shopId = Integer.parseInt(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
		dto.vanLock = state;
		dto.createDate = DateUtils.now();
		dto.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		bundle.putSerializable(IntentConstants.INTENT_STOCK_DTO, dto);
		handleViewEvent(bundle, ActionEventConstant.ACTION_INSERT_LOCK_DATE, SaleController.getInstance());
	}

	/**
	 * kiem tra xem duoc phep thuc hien chot ngay hay chua
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param state
	 */
	private void checkDBLogForDateLock() {
		handleViewEvent(new Bundle(), ActionEventConstant.ACTION_CHECK_DB_LOG_FOR_DATE_LOCK, SaleController.getInstance());
	}

	/**
	 * request fullDate du lieu len server de chot ngay
	 *
	 * @author: ThangNV31
	 * @return: void
	 */
	private void startUpSynDataForDateLock() {
		int numAsyncTaskActive = AsyncTaskUtil.getNumAsyncTaskActive();
		if (numAsyncTaskActive >= 3 || GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_EXECUTING) {
			// ghi log
			ServerLogger.sendLog("SynData", "Luong cap nhat tam chua xu ly (active, stateSyn): " + numAsyncTaskActive + ", "
					+ GlobalInfo.getInstance().getStateSynData(), false, TabletActionLogDTO.LOG_CLIENT);
			parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
		} else {
			parent.setIsCanceledUpdateData(false);
			parent.showProgressDialog(StringUtil.getString(R.string.updating));
			if (GlobalUtil.checkNetworkAccess()) {
				TransactionProcessManager.getInstance().resetChecking(TransactionProcessManager.SYNC_FROM_DATE_LOCK);
			} else {
				parent.closeProgressDialog();
				parent.showDialog(StringUtil.getString(R.string.MESSAGE_SYN_FAIL_WHEN_CHECK_DB_FOR_DATE_LOCK));
			}
		}
	}

	/**
	 * request fullDate du lieu len server de chot ngay
	 *
	 * @author: ThangNV31
	 * @return: void
	 */
	private void sendRequestDateLockToServer() {
		int numAsyncTaskActive = AsyncTaskUtil.getNumAsyncTaskActive();
		if (numAsyncTaskActive >= 3 || GlobalInfo.getInstance().getStateSynData() == GlobalInfo.SYNDATA_EXECUTING) {
			// ghi log
			ServerLogger.sendLog("SynData", "Luong cap nhat tam chua xu ly (active, stateSyn): " + numAsyncTaskActive + ", "
					+ GlobalInfo.getInstance().getStateSynData(), false, TabletActionLogDTO.LOG_CLIENT);
			parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
		} else {
			parent.showProgressDialog(StringUtil.getString(R.string.updating));
			if (GlobalUtil.checkNetworkAccess()) {
//				ActionEvent e = new ActionEvent();
//				e.action = ActionEventConstant.ACTION_SEND_REQUEST_DATE_LOCK_TO_SERVER;
//				SaleController.getInstance().handleViewEvent(e);
				this.insertLockDateState(1);
			} else {
				parent.closeProgressDialog();
				parent.showDialog(StringUtil.getString(R.string.MESSAGE_SYN_FAIL_WHEN_CHECK_DB_FOR_DATE_LOCK));
			}
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		parent.closeProgressDialog();
		switch (actionEvent.action) {
//		case ActionEventConstant.ACTION_SEND_REQUEST_DATE_LOCK_TO_SERVER:
//			this.insertLockDateState(1);
//			break;
		case ActionEventConstant.ACTION_GET_LOCK_DATE_STATE:
			LockDateDTO dto = (LockDateDTO) modelEvent.getModelData();
			this.lockDateState = dto.vanLock;
			this.hasDP = dto.hasDP;
			GlobalInfo.getInstance().setLockDateValsale(dto);
			renderLayout();
			break;
		case ActionEventConstant.ACTION_INSERT_LOCK_DATE:
			parent.showDialog(StringUtil.getString(R.string.TEXT_LOCK_DATE_SUCCESS));
			getLockDateState();
			break;
		case ActionEventConstant.ACTION_CHECK_DB_LOG_FOR_DATE_LOCK:
			parent.closeProgressDialog();
			boolean canExecuteDateLock = (Boolean) modelEvent.getModelData();
			if (canExecuteDateLock) {
				this.sendRequestDateLockToServer();
			} else {
				parent.showDialog(StringUtil.getString(R.string.MESSAGE_SYN_FAIL_WHEN_CHECK_DB_FOR_DATE_LOCK));
			}
			break;
		case ActionEventConstant.GET_ORDER_DRAFT_TYPE:
			String orderType = (String) modelEvent.getModelData();
			if(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderType)){
				// thong bao co don hang vansale dang tao
				GlobalUtil.showDialogConfirm(this, parent,  StringUtil.getString(R.string.TEXT_CONFIRM_EDIT_VAN_SALE),
						StringUtil.getString(R.string.TEXT_BUTTON_AGREE), CONFIRM_DRAFT_ORDER, null, true);
			} else {
				// khong phai don tam la vansale cho chot
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_QUESTION_LOCK_STOCK), StringUtil.getString(R.string.TEXT_BUTTON_AGREE), CONFIRM_LOCK_DATE_OK,
						StringUtil.getString(R.string.TEXT_BUTTON_CANCEL), CONFIRM_LOCK_DATE_CANCEL, null);
			}
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		parent.closeProgressDialog();
		switch (actionEvent.action) {
		case ActionEventConstant.ACTION_GET_LOCK_DATE_STATE: {
			parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_HAPPEN));
			this.lockDateState = -1;
			renderLayout();
			break;
		}
		case ActionEventConstant.ACTION_INSERT_LOCK_DATE: {
			parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_HAPPEN));
			this.lockDateState = -1;
			renderLayout();
			break;
		}
		case ActionEventConstant.GET_ORDER_DRAFT_TYPE:
			parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_HAPPEN));
			break;
		case ActionEventConstant.ACTION_CHECK_DB_LOG_FOR_DATE_LOCK: {
			parent.showDialog(StringUtil.getString(R.string.MESSAGE_ERROR_WHEN_CHECK_DB_FOR_DATE_LOCK));
			break;
		}
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btLockDate) {
			if (StringUtil.isNullOrEmpty(parent.draftOrderId)) {
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_QUESTION_LOCK_STOCK), StringUtil.getString(R.string.TEXT_BUTTON_AGREE), CONFIRM_LOCK_DATE_OK,
						StringUtil.getString(R.string.TEXT_BUTTON_CANCEL), CONFIRM_LOCK_DATE_CANCEL, null);
			} else {
				getDraftOrderType();
			}
		}
	}

	/**
	 * lay type don tam
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void getDraftOrderType() {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_SALE_ORDER_ID, parent.draftOrderId);
		handleViewEvent(bundle, ActionEventConstant.GET_ORDER_DRAFT_TYPE, SaleController.getInstance());
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				getLockDateState();
			}
			break;
		case ActionEventConstant.ACTION_FINISH_SYN_DATA_FOR_DATE_LOCK:
			if (this.isVisible()) {
				this.checkDBLogForDateLock();
			}
			break;
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case CONFIRM_LOCK_DATE_OK: {
			this.startUpSynDataForDateLock();
			break;
		}
		case ACTION_PATH:
			handleSwitchFragment(null,ActionEventConstant.GO_TO_CUSTOMER_ROUTE, UserController.getInstance());
			break;
		case ACTION_ORDER_LIST:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER, UserController.getInstance());
			break;
		case ACTION_CREATE_CUSTOMER:{
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
			break;
		}
		case ACTION_CUS_LIST:
			handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
			break;
		case ACTION_IMAGE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
			break;
		default:
			break;
		}
	}
}
