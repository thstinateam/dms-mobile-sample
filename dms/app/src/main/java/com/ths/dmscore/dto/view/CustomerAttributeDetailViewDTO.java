/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_ATTRIBUTE_DETAIL_TABLE;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.GlobalUtil;

/**
 * CustomerAttributeDetailViewDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  4:49:39 PM Jan 28, 2015
 */
public class CustomerAttributeDetailViewDTO implements Serializable {

	private static final long serialVersionUID = -65924836441154719L;
	
	public long attrID;
	public long detailID;
	public String value;
	public int enumID;
	public int status;
	public long customerID;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;

	public CustomerAttributeDetailViewDTO() {
		// TODO Auto-generated constructor stub
		value = Constants.STR_BLANK;
		enumID = 0;
		detailID = 0;
		status = 0;
	}

	/**
	 * generate data
	 * 
	 * @author: dungdq3
	 * @since: 7:01:30 PM Jan 28, 2015
	 * @return: Vector<?>
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateJSON() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject insertCustomerJson = new JSONObject();
		insertCustomerJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.INSERT);
		insertCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_ATTRIBUTE_DETAIL_TABLE.TABLE_NAME);
		// ds params
		JSONArray detailPara = new JSONArray();
		// ...them thuic tinh		
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ID, customerID, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_DETAIL_ID, detailID, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_ID, attrID, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.STATUS, status, null));
		if(!StringUtil.isNullOrEmpty(value)){
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.VALUE, value, null));
		} else if (enumID > 0) {
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_ENUM_ID, enumID, null));
		}
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CREATE_DATE, createDate, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CREATE_USER, createUser, null));
		insertCustomerJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		return insertCustomerJson;
	}

	/**
	 * 
	 * 
	 * @author: dungdq3
	 * @since: 10:59:21 AM Jan 29, 2015
	 * @return: Vector<?>
	 * @throws:  
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject generateJSONUpdate() throws JSONException {
		// TODO Auto-generated method stub
		JSONObject insertCustomerJson = new JSONObject();
		insertCustomerJson.put(IntentConstants.INTENT_TYPE, AbstractTableDTO.TableAction.UPDATE);
		insertCustomerJson.put(IntentConstants.INTENT_TABLE_NAME, CUSTOMER_ATTRIBUTE_DETAIL_TABLE.TABLE_NAME);
		// ds params
		JSONArray detailPara = new JSONArray();
		// ...them thuoc tinh		
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.STATUS, status, null));
		if(!StringUtil.isNullOrEmpty(value)){
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.VALUE, value, null));
		} else if (enumID > 0) {
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_ENUM_ID, enumID, null));
		}
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.UPDATE_DATE, updateDate, null));
		detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.UPDATE_USER, updateUser, null));
		insertCustomerJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		JSONArray wheres = new JSONArray();
		wheres.put(GlobalUtil.getJsonColumn(CUSTOMER_ATTRIBUTE_DETAIL_TABLE.CUSTOMER_ATTRIBUTE_DETAIL_ID, detailID, null));
		insertCustomerJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		return insertCustomerJson;
	}

}
