/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *
 * dto cua 3 textview de thuc hien autoComplete
 *
 * @author: HieuNH6
 * @version: 1.1
 * @since: 1.0
 */
public class AutoCompleteFindProductSaleOrderDetailViewDTO implements Serializable {
	private static final long serialVersionUID = 4913222926975871612L;
	// ma san pham
	public ArrayList<String> strInputOrderCode = new ArrayList<String>();
	// ten san pham
	public ArrayList<String> strInputOrderName = new ArrayList<String>();
	// ma CTKM
	public ArrayList<String> strInputCTKMCode = new ArrayList<String>();
	// ten san pham khong dau
	public ArrayList<String> strInputOrderNameTextUnSigned = new ArrayList<String>();
	//danh sach loai nganh hang
	public ArrayList<DisplayProgrameItemDTO> lstCat = new ArrayList<DisplayProgrameItemDTO>() ;
	//danh sach loai nganh hang con
	public ArrayList<DisplayProgrameItemDTO> lstSubcat = new ArrayList<DisplayProgrameItemDTO>();

	public AutoCompleteFindProductSaleOrderDetailViewDTO() {

	}

	/**
	 *
	 * init with cursor
	 *
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 9, 2013
	 */
	public void init(Cursor c) {
		strInputOrderCode.add(CursorUtil.getString(c, "PRODUCT_CODE"));
		strInputOrderName.add(CursorUtil.getString(c, "PRODUCT_NAME"));
		strInputOrderNameTextUnSigned.add(CursorUtil.getString(c, "PRODUCT_NAME_TEXT"));
		strInputCTKMCode.add(CursorUtil.getString(c, "PROMOTION_CODE"));
	}
}
