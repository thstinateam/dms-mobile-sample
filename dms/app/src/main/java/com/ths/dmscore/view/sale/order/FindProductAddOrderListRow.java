/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 *
 * display row product when research
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class FindProductAddOrderListRow extends DMSTableRow implements
		OnClickListener, TextWatcher, OnFocusChangeListener {
	// STT
	TextView tvSTT;
	// code
	TextView tvMMH;
	// Name
	public TextView tvMHName;
	// inventory
	TextView tvStockTotal;
	TextView tvStockTotalReal;
	// convfact
	TextView tvConvfactName;
	TextView tvConvfact;
	// Image view CTKM
	ImageView ivCTKM;
	// number
	EditText etNumber;
	LinearLayout llEditText;
	// linerlayout CTKM
	LinearLayout llCTKM;
	// linner layout detail CT
	LinearLayout llCTDetail;
	// image button CT
	ImageView imbtCTDetail;
	// text view CT code
	TextView tvCTDetail;

	// data to render layout for row
	FindProductSaleOrderDetailViewDTO myData;
	TextView tvPackagePrice;// gia thung
	TextView tvPrice;// gia le

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 * @param isShowPrice
	 */
	public FindProductAddOrderListRow(Context context, View aRow, boolean isShowPrice) {
		super(context, R.layout.layout_find_product_add_orderlist_row,(isShowPrice ? 1.2 : 1.0),
				(isShowPrice ? null : new int[] {R.id.tvPrice,R.id.tvPackagePrice,R.id.imbtCTDetail}));
		//set onClick
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvMMH = (TextView) findViewById(R.id.tvMMH);
		tvMHName = (TextView) findViewById(R.id.tvMHName);
		tvPackagePrice = (TextView) findViewById(R.id.tvPackagePrice);
		tvPrice = (TextView) findViewById(R.id.tvPrice);
//		tvCTDetail = (TextView) findViewById(R.id.tvCTDetail);
		tvCTDetail = (TextView) PriUtils.getInstance().findViewById(this, R.id.tvCTDetail, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_CHITIETCHUONGTRINHTRATHUONG);
		PriUtils.getInstance().setOnClickListener(tvCTDetail, this);
		tvCTDetail.setBackgroundColor(ImageUtil.getColor(R.color.TABLE_HEADER_BG));
		tvCTDetail.setTypeface(null, Typeface.BOLD);
		tvConvfactName = (TextView) findViewById(R.id.tvConvfactName);
		tvConvfact = (TextView) findViewById(R.id.tvConvfact);
		tvStockTotal = (TextView) findViewById(R.id.tvStockTotal);
		tvStockTotalReal = (TextView) findViewById(R.id.tvStockTotalReal);
		ivCTKM = (ImageView) PriUtils.getInstance().findViewById(this, R.id.ivCTKM, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_CHITIETCHUONGTRINHKHUYENMAI);
		PriUtils.getInstance().setOnClickListener(ivCTKM, this);

		etNumber = (EditText) findViewById(R.id.etNumber);
		etNumber.addTextChangedListener(this);
		GlobalUtil.setFilterInputConvfact(etNumber, Constants.MAX_LENGHT_QUANTITY);
		llEditText = (LinearLayout) findViewById(R.id.llEditText);
		llCTKM = (LinearLayout) findViewById(R.id.llCTKM);
		llCTDetail = (LinearLayout) findViewById(R.id.llCTDetail);
		myData = new FindProductSaleOrderDetailViewDTO();
		imbtCTDetail = (ImageView) PriUtils.getInstance().findViewById(this, R.id.imbtCTDetail, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_CHITIETCHUONGTRINHTRATHUONG);
		PriUtils.getInstance().setOnClickListener(imbtCTDetail, this);

		//chon custom, thi an ban phim ao
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			etNumber.setInputType(InputType.TYPE_NULL);
			etNumber.setOnFocusChangeListener(this);
			etNumber.setOnClickListener(this);
		}
	}

	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position,
			FindProductSaleOrderDetailViewDTO item, boolean isRequestFocus) {
		this.myData = item;
		display(tvSTT, String.valueOf(position));
		String messageOrder = Constants.STR_BLANK;
		if (item.mhTT == 1 && item.gsnppRequest == 1) {
			messageOrder = "*,GS";
		} else if (item.mhTT == 1) {
			messageOrder = "*";
		} else if (item.gsnppRequest == 1) {
			messageOrder = "GS";
		}
		if (!StringUtil.isNullOrEmpty(messageOrder)) {
			SpannableStringBuilder content = new SpannableStringBuilder(
					item.productCode);
			SpannableStringBuilder superScript = StringUtil.superScript(
					messageOrder, ImageUtil.getColor(R.color.RED));
			content.append(superScript);
			tvMMH.setText(content, BufferType.SPANNABLE);
		} else {
			SpannableStringBuilder content = new SpannableStringBuilder(
					item.productCode);
			tvMMH.setText(content, BufferType.SPANNABLE);
		}
		display(tvMHName, item.productName);
		// truong hop co id trong bang gia thi moi hien thi len
		// ko thi ko hien thi
		if(item.saleOrderDetail.priceId > 0){
			display(tvPrice, item.saleOrderDetail.price);
			display(tvPackagePrice, item.saleOrderDetail.packagePrice);
		}
		else{
			display(tvPrice, "");
			display(tvPackagePrice, "");
		}


		// hien thi ten quy cach
		display(tvConvfactName, item.uom2.trim());

		// hien thi so luong ton kho co the dat hang da format
		display(tvStockTotal, item.available_quantity_format);
		display(tvStockTotalReal, item.quantity_format);

		if (item.hasPromotionProgrameCode) {
			ivCTKM.setVisibility(View.VISIBLE);
		} else {
			ivCTKM.setVisibility(View.INVISIBLE);
		}

		if (item.hasSelectPrograme) {
			if (imbtCTDetail != null) {
				imbtCTDetail.setVisibility(View.GONE);
			}
			tvCTDetail.setVisibility(View.VISIBLE);
			tvCTDetail.setText(item.saleOrderDetail.programeCode);
		} else {
			if (imbtCTDetail != null) {
				imbtCTDetail.setVisibility(View.VISIBLE);
			}
			tvCTDetail.setVisibility(View.GONE);
		}

		if (!StringUtil.isNullOrEmpty(item.numProduct)
				&& !item.numProduct.equals("0")) {
			etNumber.setText(String.valueOf(item.numProduct));
		}

		if (item.quantity <= 0 || item.available_quantity <= 0) {
			this.updateColorForTextRow(ImageUtil.getColor(R.color.RED));
		}
		if (isRequestFocus) {
			 etNumber.requestFocus();
		}
		display(tvConvfact, item.convfact);

		tvCTDetail.setBackgroundColor(ImageUtil.getColor(R.color.TRANSPARENT));
		tvCTDetail.setTextAppearance(getContext(), R.style.TextViewVinamilkTable);
		tvCTDetail.setTypeface(null, Typeface.BOLD);

	}

	/**
	 *
	 * set color for all text in row
	 *
	 * @param color
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 23, 2012
	 */
	public void updateColorForTextRow(int color) {
		setTextColor(color,
				tvSTT, tvMMH, tvMHName, tvPrice,
				tvPackagePrice, tvStockTotal, tvStockTotalReal,
				tvConvfactName, etNumber, tvCTDetail, tvConvfact);
	}

	/**
	 *
	 * update colum programe after select programe for product
	 *
	 * @author: HaiTC3
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void updateLayout(FindProductSaleOrderDetailViewDTO item) {
		if (item.hasSelectPrograme) {
			if (imbtCTDetail != null) {
				imbtCTDetail.setVisibility(View.GONE);
			}
			tvCTDetail.setVisibility(View.VISIBLE);
			tvCTDetail.setText(item.saleOrderDetail.programeCode);
		} else {
			if (imbtCTDetail != null) {
				imbtCTDetail.setVisibility(View.VISIBLE);
			}
			tvCTDetail.setVisibility(View.GONE);
		}
	}

	@Override
	public void onClick(View v) {
		if(listener != null){
			if (v == ivCTKM) {
				listener.handleVinamilkTableRowEvent(
						ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL,
						ivCTKM, myData);
			} else if (v == imbtCTDetail || v == tvCTDetail) {
				listener.handleVinamilkTableRowEvent(
						ActionEventConstant.GO_TO_CT_DETAIL, imbtCTDetail, myData);
			} else if (v == this && context != null) {
				GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
			} else if (v == etNumber) {
				listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
			}
		}else
			super.onClick(v);
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		int numProductRealOrder = GlobalUtil.calRealOrder(etNumber.getText()
				.toString().trim(), this.myData.convfact);
		if (this.myData.quantity > 0 && this.myData.available_quantity > 0) {
			if (numProductRealOrder > this.myData.available_quantity) {
				this.updateColorForTextRow(ImageUtil.getColor(R.color.OGRANGE));
			} else {
				this.updateColorForTextRow(ImageUtil.getColor(R.color.BLACK));
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

	}

	@Override
	public void onFocusChange(View v, boolean isFocused) {
		if (v == etNumber) {
			if (isFocused) {
				if (listener != null) {
					listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v, null);
				}
			} else {
				changeQuantityFormat();
			}
		}
	}
	
	public void changeQuantityFormat() {
		String realOrder = etNumber.getText().toString();
		String textQuantity = GlobalUtil.getTextQuantity(realOrder);
		if (!StringUtil.isNullOrEmpty(realOrder) && !StringUtil.isNullOrEmpty(textQuantity) && !realOrder.equals(textQuantity)) {
			display(etNumber, textQuantity);
		}
	}

}
