/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;

/**
 * OrderDTO.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  4:05:13 PM Feb 13, 2015
 */
public class OrderDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public boolean orderAscOrDesc;
	public String columnOrder;
	
	public OrderDTO() {
		// TODO Auto-generated constructor stub
		orderAscOrDesc = false;
		columnOrder = Constants.STR_BLANK;
	}
	
	public OrderDTO(boolean order, String orderName) {
		// TODO Auto-generated constructor stub
		orderAscOrDesc = order;
		columnOrder = orderName;
	}
}
