/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Mo ta cho class
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVFollowProblemDTO {
	public List<TBHVFollowProblemItemDTO> list;
	public ComboboxFollowProblemDTO comboboxDTO;
	public int total = 0;
	
	/**
	 * 
	 */
	public TBHVFollowProblemDTO() {
		list = new ArrayList<TBHVFollowProblemItemDTO>();
		list.add(new TBHVFollowProblemItemDTO());
		list.add(new TBHVFollowProblemItemDTO());
		list.add(new TBHVFollowProblemItemDTO());
		comboboxDTO = new ComboboxFollowProblemDTO();
		total = 0;
	}
}
