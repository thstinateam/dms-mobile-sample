/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * AbstractDynamicReportRowDTO.java
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 9:18:50 AM Feb 11, 2015
 */
public abstract class AbstractDynamicReportRowDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	// id theo dong
	public int objectId;
	// id staff quan li
	public int staffId;
	// shop thuoc
	public int shopId;
	// mang ds cac column ngang

	public AbstractDynamicReportRowDTO() {
		// TODO Auto-generated constructor stub
		objectId = 0;
		staffId = 0;
		shopId = 0;
	}
	
	public abstract Object clone();

}
