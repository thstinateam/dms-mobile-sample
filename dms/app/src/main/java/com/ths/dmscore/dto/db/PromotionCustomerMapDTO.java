/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_CUSTOMER_MAP;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Thong tin luu tru cua customer promotion map
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 2.1
 */
@SuppressWarnings("serial")
public class PromotionCustomerMapDTO extends AbstractTableDTO{
	// id shop promotin
	public long promotionCustomerMapId ;
	// id CTKM
	public long promotionShopMapd ;
	// id shop
	public int shopId ;
	//customer id
	public long customerId;
	//customer type id
	public int customerTypeId;
	// so luong toi da
	public int quantityMax ;
	public String quantityMaxString ;
	// so luong thuc nhan
	public int quantityReceived;
	// so luong thuc nhan truoc duyet
	public int quantityReceivedTotal;
	// status
	public int status ;
	//type
	public int type;
	//nguoi tao
	public String createUser ;
	// nguoi cap nhat
	public String updateUser ;
	// ngay tao
	public String createDate ;
	// ngay cap nhat
	public String updateDate ;
	public double amountMax;
	public double amountReceived;
	public double amountReceivedTotal; //so tien nhan duoc truoc duyet
	public int numMax; // so luong toi da duoc nhan
	public int numReceived; // so luong da nhan duoc
	public int numReceivedTotal; // so luong nhan duoc truoc duyet
	private String amountMaxString;
	private String numMaxString;

	public PromotionCustomerMapDTO(){
		super(TableType.PROMOTION_CUSTOMER_MAP_TABLE);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		promotionCustomerMapId = CursorUtil.getLong(c, PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID, -1);
		promotionShopMapd = CursorUtil.getLong(c, PROMOTION_CUSTOMER_MAP.PROMOTION_SHOP_MAP_ID, -1);
		shopId = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.SHOP_ID, -1);
		customerId = CursorUtil.getLong(c, PROMOTION_CUSTOMER_MAP.CUSTOMER_ID, -1);
		customerTypeId = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.CUSTOMER_TYPE_ID, -1);
		quantityMax = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.QUANTITY_MAX, -1);
		quantityMaxString = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.QUANTITY_MAX);
		if(StringUtil.isNullOrEmpty(quantityMaxString)) {
			quantityMax = Integer.MAX_VALUE;
		}
		quantityReceived = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, -1);
		quantityReceivedTotal = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED_TOTAL, 0);
		status = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.STATUS, -1);
		type = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.TYPE, -1);
		createUser = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.CREATE_USER);
		updateUser = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.UPDATE_USER);
		createDate = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.CREATE_DATE);
		updateDate = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.UPDATE_DATE);
		amountMax = CursorUtil.getDouble(c, PROMOTION_CUSTOMER_MAP.AMOUNT_MAX, -1);
		amountMaxString = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.AMOUNT_MAX);
		if(StringUtil.isNullOrEmpty(amountMaxString)) {
			amountMax = Double.MAX_VALUE;
		}
		amountReceived = CursorUtil.getDouble(c, PROMOTION_CUSTOMER_MAP.AMOUNT_RECEIVED, -1);
		amountReceivedTotal = CursorUtil.getDouble(c, PROMOTION_CUSTOMER_MAP.AMOUNT_RECEIVED_TOTAL, 0);
		numMax = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.NUM_MAX, -1);
		numMaxString = CursorUtil.getString(c, PROMOTION_CUSTOMER_MAP.NUM_MAX);
		if(StringUtil.isNullOrEmpty(numMaxString)) {
			numMax = Integer.MAX_VALUE;
		}
		numReceived = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.NUM_RECEIVED, -1);
		numReceivedTotal = CursorUtil.getInt(c, PROMOTION_CUSTOMER_MAP.NUM_RECEIVED_TOTAL, 0);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateDescreasePromotionSqlVansale() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*-1",
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
					this.promotionCustomerMapId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInscreasePromotionSqlVansale() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*+1",
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(
					PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
					this.promotionCustomerMapId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

}
