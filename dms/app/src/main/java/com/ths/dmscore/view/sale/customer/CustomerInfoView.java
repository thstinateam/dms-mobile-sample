/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.customer;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.view.CustomerInfoDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.sale.order.RegisterKeyShopView;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.CustomerCatAmountPopupViewDTO;
import com.ths.dmscore.dto.view.CustomerSaleSKUDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.dto.view.RegisterKeyShopViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Thong tin co ban khach hang
 *
 * @author : BangHN since : 9:48:52 AM version :
 */
public class CustomerInfoView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener {

	public static final int UPDATE_LOCATION_ACTION = 1;
	public static final int END_UPDATE_ACTION = 2;
	public static final int FEEDBACK_ACTION = 3;
	public static final int GIVE_PROMOTION_ACTION = 4;
	public static final int CHECK_POINT_ACTION = 5;
	public static final int MENU_REVIEW = 10;
	public static final int MENU_LIST_FEEDBACKS = 11;
	public static final int MENU_MAP = 12;
	public static final int MENU_IMAGE = 13;
	public static final int MENU_INFO_DETAIL = 14;
	public static final int MENU_UPDATE_LOCAION = 15;
	public static final int MENU_CUSTOMER_SALE = 16;

	// layout chua toan bo customer info
	LinearLayout llContent;
//	// layout chua menu icon
//	LinearLayout llIconMenu;
	// title chuong trinh khach hang dang tham
	// gia
	LinearLayout llProgrameDisplay;
	// ten khach hang
	TextView tvCustomerName;
	// ma code khach hang
	// TextView tvCustomerCode;
	// dia chi khach hang
	TextView tvCustomerAddress;
	// phone
	TextView tvCustomerPhone;
	// mobile phone
	TextView tvCustomerMobile;
	// loai cua hang
	TextView tvCustomerType;
	// nguoi lien he
	TextView tvCustomerContact;
	// Imageview sang man hinh thong tin thuoc tinh khach hang
	TextView tvInforDynamic;
	// sku
	TextView tvSKU;
	// so lan dat trong thang
	TextView tvOrderInMonth;
	// binh quan 2 thang truoc
	TextView tvAverageSales;
	// doanh so trong thang
	TextView tvSalesInMonth;
	// doanh so trong 3 thang
	TextView tvSalesLast3Month;
	// title 5 don hang gan nhat
	TextView tvLastSalesGroup;
	// doanh so thang thu 1
	TextView tvMonthFirst;
	// doanh so thang thu 2
	TextView tvMonthSecond;
	// doanh so thang thu 3
	TextView tvMonthThird;
	// textview dang ky keyshop
	TextView tvRegisterKeyShop;

	private AlertDialog alertDialog;
	// doanh so gan fullDate nhat
	private DMSTableView tbLastSalesInfo;
	CustomerInfoDTO customerInfoDTO = new CustomerInfoDTO();

	// lan dau init view
	private boolean isFirstInitView = true;
	// id khach hang
	String customerId;
	// trang hien tai
	int currentPage = 1;
	// so luong moi trang
	int numTop = 10;

	// bien dung de kiem tra co phai di tu man hinh tao don hang hay khong
	public boolean isFromCreateOrder = false;
	private boolean isFromSupervisor = false;
	private TextView tvMonthFirstQuantity;

	private TextView tvMonthSecondQuantity;

	private TextView tvMonthThirdQuantity;
	//binh quan san luong 2 thang truoc
	private TextView tvAverageQuantity;
	//sang luong thuc hien trong thang
	private TextView tvQuantityInMonth;

	private TextView tvSKUQuantity;

	private TextView tvOrderInMonthQuantity;

	private TextView tvQuantityLast3Month;

	private LinearLayout llSaleInfo;
	private TextView tvAmountInfo;
	private TextView tvImage;
	private TextView tvPosition;
	// dto cho man hinh keyshop
	private RegisterKeyShopViewDTO registerKeyShopDTO;// dang ky keyshop
	// dialog product detail view
	RegisterKeyShopView keyShopView;
	long ksId = 0; // CT Keyshop hien tai
	String cycleId = "0"; // cycleId hien tai dang chon
	private long fromCycleId;// tu chu ki
	private long toCycleId; // den chu ki
	boolean isUpdateRegisterKeyShop = false;// bien kiem tra dang o trang thai update keyshop hay ko

	private boolean requestShowQuantityPopup;
	public static CustomerInfoView newInstance(Bundle data) {
		CustomerInfoView f = new CustomerInfoView();
		f.setArguments(data);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getArguments();
		if (args != null) {
			customerId = args.getString(IntentConstants.INTENT_CUSTOMER_ID);
			isFromSupervisor = args.getBoolean(
					IntentConstants.INTENT_FROM_SUPERVISOR, false);
			MyLog.i("Customer", "Customer code : " + customerId);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_customer_info, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		llContent = (LinearLayout) v.findViewById(R.id.llContent);
		llContent.setVisibility(View.GONE);
		llProgrameDisplay = (LinearLayout) v
				.findViewById(R.id.llProgrameDisplay);
		tbLastSalesInfo = (DMSTableView) v
				.findViewById(R.id.tbLastSalesInfo);
		tbLastSalesInfo.setListener(this);
		llSaleInfo = (LinearLayout) v.findViewById(R.id.llSaleInfo);
		tvAmountInfo = (TextView) v.findViewById(R.id.tvAmountInfo);
		hideHeaderview();
		// Title NVBH
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH);
			parent.setTitleName(StringUtil
					.getString(R.string.TITLE_VIEW_CUSTOMER_INFO));
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_THONGTINKH);
			parent.setTitleName(StringUtil
					.getString(R.string.TITLE_VIEW_GSNPP_CUSTOMER_INFO));
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.TBHV_THONGTINNV);
			parent.setTitleName(StringUtil
					.getString(R.string.TITLE_VIEW_GSNPP_CUSTOMER_INFO));
		}
		initViewControl(v);
		if(isFromSupervisor){
			setTitleHeaderView(StringUtil
					.getString(R.string.TITLE_VIEW_CUSTOMER_INFO_GSNPP_TBHV));
		}
		return v;
	}

	/**
	 * Khoi tao cac control layout
	 *
	 * @author : BangHN since : 9:35:46 AM
	 */
	private void initViewControl(View v) {
		tvCustomerName = (TextView) v.findViewById(R.id.tvCustomerName);
		tvCustomerAddress = (TextView) v.findViewById(R.id.tvCustomerAddressCusInfo);
		tvCustomerPhone = (TextView) v.findViewById(R.id.tvCustomerPhone);
		tvCustomerMobile = (TextView) v.findViewById(R.id.tvCustomerMobile);
		tvCustomerType = (TextView) v.findViewById(R.id.tvCustomerType);
		tvCustomerContact = (TextView) v.findViewById(R.id.tvCustomerContact);
		tvInforDynamic = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvInforDynamic,
				PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_THUOCTINH);
		tvInforDynamic.setOnClickListener(this);
		tvRegisterKeyShop = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvRegisterKeyShop,
				PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_DANGKYKEYSHOP);
		tvRegisterKeyShop.setOnClickListener(this);
		tvOrderInMonth = (TextView) v.findViewById(R.id.tvOrderInMonth);
		tvAverageSales = (TextView) v.findViewById(R.id.tvAverageSales);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			tvSKU = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSKU, PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_SKU);
			tvSalesInMonth = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesInMonth,
					PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_DOANHSOTHEONGANH);
			tvSalesLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesLast3Month,
					PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_DOANHSOTHEONGANH);
			tvQuantityLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvQuantityLast3Month,
					PriHashMap.PriControl.NVBH_THONGTINKHACHHANG_DOANHSOTHEONGANH);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			tvSKU = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSKU, PriHashMap.PriControl.GSNPP_THONGTINKH_SKU);
			tvSalesInMonth = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesInMonth,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
			tvSalesLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesLast3Month,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
			tvQuantityLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvQuantityLast3Month,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			tvSKU = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSKU, PriHashMap.PriControl.GSNPP_THONGTINKH_SKU);
			tvSalesInMonth = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesInMonth,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
			tvSalesLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvSalesLast3Month,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
			tvQuantityLast3Month = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(v, R.id.tvQuantityLast3Month,
					PriHashMap.PriControl.GSNPP_THONGTINKH_DOANHSO3THANG);
		}
		tvLastSalesGroup = (TextView) v.findViewById(R.id.tvLastSalesGroup);
		tvMonthFirst = (TextView) v.findViewById(R.id.tvMonthFirst);
		tvMonthSecond = (TextView) v.findViewById(R.id.tvMonthSecond);
		tvMonthThird = (TextView) v.findViewById(R.id.tvMonthThird);
		//san luong
		tvMonthFirstQuantity = (TextView) v.findViewById(R.id.tvMonthFirstQuantity);
		tvMonthSecondQuantity = (TextView) v.findViewById(R.id.tvMonthSecondQuantity);
		tvMonthThirdQuantity = (TextView) v.findViewById(R.id.tvMonthThirdQuantity);
		tvAverageQuantity = (TextView) v.findViewById(R.id.tvAverageQuantity);

		tvQuantityInMonth = (TextView) v.findViewById(R.id.tvQuantityInMonth);
		tvSKUQuantity = (TextView) v.findViewById(R.id.tvSKUQuantity);
		tvSKUQuantity.setOnClickListener(this);
		tvQuantityInMonth.setOnClickListener(this);
		tvOrderInMonthQuantity = (TextView) v.findViewById(R.id.tvOrderInMonthQuantity);
		tvImage = (TextView) v.findViewById(R.id.tvImage);
		tvImage.setOnClickListener(this);
		tvPosition = (TextView) v.findViewById(R.id.tvPosition);
		tvPosition.setOnClickListener(this);
		if (isFirstInitView || GlobalInfo.isChangeInfoCurrentCustomer()) {
			parent.showLoadingDialog();
			getCustomerInfo(customerId);
			// getLastSaleOrder();
		}
		checkRegisterKeyshop();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		initMenuActionBar();
		if (isFirstInitView || GlobalInfo.isChangeInfoCurrentCustomer()) {
		} else {
			renderData(customerInfoDTO);
			renderDataLastSaleOrder();
		}
	}

	/**
	 * Init menu action bar
	 *
	 * @author : BangHN since : 2:53:53 PM
	 */
	private void initMenuActionBar() {
		enableMenuBar(this);
		int countTab = 3;
		MenuTab[] tabs = new MenuTab[countTab];
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH_HINHANHKH);
//			tabs[2] = new MenuTab(R.string.TEXT_FEEDBACK, R.drawable.icon_reminders,
//					MENU_LIST_FEEDBACKS, PriHashMap.PriForm.NVBH_GOPY);
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.NVBH_BANHANG_THONGTINKH);
			addMenuItem(PriForm.NVBH_BANHANG_THONGTINKH, tabs);
		} else if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR
				|| GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER) {
			tabs[0] = new MenuTab(R.string.TEXT_POSITION,R.drawable.icon_map,
					MENU_MAP, PriHashMap.PriForm.GSNPP_VITRI);
			tabs[1] = new MenuTab(R.string.TEXT_PICTURE, R.drawable.menu_picture_icon,
					MENU_IMAGE, PriHashMap.PriForm.GSNPP_HINHANH);
//			if (isFromSupervisor == false) {
//				tabs[2] = new MenuTab(R.string.TEXT_LABLE_REVIEWS,
//						R.drawable.icon_note, MENU_REVIEW, PriForm.GSNPP_DANHGIA);
//			}else{
//				tabs[2] = new MenuTab(StringUtil.getString(R.string.TEXT_FEEDBACK),
//						R.drawable.icon_reminders, MENU_LIST_FEEDBACKS, PriForm.GSNPP_GOPY);
//			}
			tabs[2] = new MenuTab(R.string.TEXT_INFO, R.drawable.icon_detail,
					MENU_INFO_DETAIL, PriHashMap.PriForm.GSNPP_THONGTINKH);
			addMenuItem(PriForm.GSNPP_THONGTINKH, tabs);
		}
	}

	/**
	 * Chuong trinh khach hang dang tham gia
	 *
	 * @author : BangHN since : 1.0
	 */
	private void renderCustomerDisplayPrograme() {
		if (customerInfoDTO != null
				&& customerInfoDTO.getListCustomerPrograme() != null) {
			int size = customerInfoDTO.getListCustomerPrograme().size();
			if (size > 0) {
				llProgrameDisplay.setVisibility(View.VISIBLE);
			} else {
				llProgrameDisplay.setVisibility(View.GONE);
			}
		}
	}

	/**
	 *
	 *
	 * Di toi chi tiet don hang
	 *
	 * @author: HoanPD1
	 * @return: void
	 * @param dto
	 * @throws:
	 * @since : 12-12-2013
	 */
	@SuppressWarnings("unused")
	private void gotoOrderDetail(SaleOrderDTO dto) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "" + dto.saleOrderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(customerId));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE,
				customerInfoDTO.getCustomer().customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				customerInfoDTO.getCustomer().getHouseNumber() + " - "
						+ customerInfoDTO.getCustomer().getStreet());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerInfoDTO
				.getCustomer().getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE,
				customerInfoDTO.getCustomer().customerCode);
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_VIEW_ORDER);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * render data don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void renderDataLastSaleOrder() {
		tbLastSalesInfo.clearAllDataAndHeader();
		initHeaderTable(tbLastSalesInfo, new CustomerLastOrderRow(parent));
		if (customerInfoDTO == null
				|| customerInfoDTO.listOrderCustomer == null) {
			tvLastSalesGroup.setVisibility(View.GONE);
			return;
		}
		int size = customerInfoDTO.listOrderCustomer.size() > 5  ? 5 : customerInfoDTO.listOrderCustomer.size();

		if (size > 0) {
			tvLastSalesGroup.setText(StringUtil.getString(R.string.TEXT_LIST)
					+ size
					+ StringUtil.getString(R.string.TEXT_ORDERS_RECENTLY));
			tvLastSalesGroup.setVisibility(View.VISIBLE);
			for (int i = 0; i < size; i++) {
				//final int index = i;
				CustomerLastOrderRow row = new CustomerLastOrderRow(parent);
				row.setClickable(true);
				row.setDataRow(customerInfoDTO.listOrderCustomer.get(i), i + 1);
				tbLastSalesInfo.addRow(row);
			}
		} else {
			tbLastSalesInfo.setVisibility(View.GONE);
			tvLastSalesGroup.setVisibility(View.GONE);
		}
	}

	/**
	 * Lay thong tin cua khach hang
	 *
	 * @author : BangHN since : 1.0
	 */
	private void getCustomerInfo(String customerId) {
		MyLog.d("CustomerInfoView", "getCustomerInfo " + customerId);
		Bundle  b = new Bundle();
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		b.putString(IntentConstants.INTENT_PAGE, "" + currentPage);
		b.putString(IntentConstants.INTENT_NUMTOP,"" + numTop);
		handleViewEvent(b, ActionEventConstant.GET_CUSTOMER_BASE_INFO, SaleController.getInstance());
	}

	/**
	 * gotoFeedBackList
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoFeedBackList() {
		if (customerInfoDTO.getCustomer() == null) {
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
				customerInfoDTO.getCustomer());
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR,
				isFromSupervisor);
		handleSwitchFragment(bundle, ActionEventConstant.GET_LIST_CUS_FEED_BACK, SaleController.getInstance());
	}

	/**
	 * di toi man hinh cap nhat vi tri khach hang
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoCustomerLocation() {
		if (customerInfoDTO.getCustomer() == null) {
			return;
		}
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER,
				customerInfoDTO.getCustomer());
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromSupervisor);
		handleSwitchFragment(bundle, ActionEventConstant.GOTO_CUSTOMER_LOCATION, UserController.getInstance());
	}

	/**
	 * Render du lieu len view
	 *
	 * @author : BangHN since : 9:29:23 AM
	 */
	private void renderData(CustomerInfoDTO viewData) {
		if (viewData != null && viewData.getCustomer() != null) {
			tvCustomerName.setText(viewData.getCustomer().getCustomerCode()
					+ " - " + viewData.getCustomer().getCustomerName());
			if(!StringUtil.isNullOrEmpty(viewData.getCustomer().address)){
				tvCustomerAddress.setText(viewData.getCustomer().address);
			}else if(!StringUtil.isNullOrEmpty(viewData.getCustomer().getHouseNumber())
					&& !StringUtil.isNullOrEmpty(viewData.getCustomer().getStreet())){
				tvCustomerAddress.setText(viewData.getCustomer().getHouseNumber()
						+ " - " + viewData.getCustomer().getStreet());
			}else if(!StringUtil.isNullOrEmpty(viewData.getCustomer().getHouseNumber())){
				tvCustomerAddress.setText(viewData.getCustomer().getHouseNumber());
			}else if(!StringUtil.isNullOrEmpty(viewData.getCustomer().getStreet())){
				tvCustomerAddress.setText(viewData.getCustomer().getStreet());
			}else {
				tvCustomerAddress.setText(Constants.STR_BLANK);
			}

			tvCustomerPhone.setText(viewData.getCustomer().getPhone());
			tvCustomerMobile.setText(viewData.getCustomer().getMobilephone());
			if (viewData.getCustomerType() != null) {
				tvCustomerType.setText(viewData.getCustomerType().channelTypeCode
								+ " - "+ viewData.getCustomerType().channelTypeName);
			} else {
				tvCustomerType.setText("");
			}
			tvCustomerContact.setText(viewData.getCustomer().getContactPerson());
			tvOrderInMonth.setText("" + viewData.saleOrdersInMonth);

			tvSKU.setText("" + viewData.sku);
			tvSKU.setTextColor(ImageUtil.getColor(R.color.COLOR_USER_NAME));
//			tvSKU.setOnClickListener(this);
			PriUtils.getInstance().setOnClickListener(tvSKU, this);

			StringUtil.display(tvSalesInMonth, viewData.amountInMonth);
			tvSalesInMonth.setTextColor(ImageUtil
					.getColor(R.color.COLOR_USER_NAME));
//			tvSalesInMonth.setOnClickListener(this);
			PriUtils.getInstance().setOnClickListener(tvSalesInMonth, this);
			StringUtil.display(tvAverageSales, (viewData.amountInOneMonthAgo + viewData.amountInTwoMonthAgo) / 2);
//			Calendar cal = Calendar.getInstance();
//			int month = cal.get(Calendar.MONTH);
//			int oneMonthAgo = (month - 0 + 12) % 12;
//			if (oneMonthAgo == 0)
//				oneMonthAgo = 12;
//			int twoMonthAgo = (month - 1 + 12) % 12;
//			if (twoMonthAgo == 0)
//				twoMonthAgo = 12;
//			int threeMonthAgo = (month - 2 + 12) % 12;
//			if (threeMonthAgo == 0)
//				threeMonthAgo = 12;

			SpannableObject tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInOneMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.amountInOneMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthFirst.setText(tmp.getSpan());

			tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInTwoMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.amountInTwoMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthSecond.setText(tmp.getSpan());

			tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInThreeMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.amountInThreeMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthThird.setText(tmp.getSpan());
			tvSalesLast3Month.setTextColor(ImageUtil
					.getColor(R.color.COLOR_USER_NAME));
			tvQuantityLast3Month.setTextColor(ImageUtil
					.getColor(R.color.COLOR_USER_NAME));
//			tvSalesLast3Month.setOnClickListener(this);
			PriUtils.getInstance().setOnClickListener(tvSalesLast3Month, this);
			PriUtils.getInstance().setOnClickListener(tvQuantityLast3Month, this);

			//san luong
			StringUtil.display(tvAverageQuantity, (viewData.quantityInOneMonthAgo + viewData.quantityInTwoMonthAgo) / 2);
			StringUtil.display(tvQuantityInMonth,viewData.quantityInMonth);
			tvQuantityInMonth.setTextColor(ImageUtil
					.getColor(R.color.COLOR_USER_NAME));

			tvSKUQuantity.setText("" + viewData.sku);
			tvSKUQuantity.setTextColor(ImageUtil.getColor(R.color.COLOR_USER_NAME));
			tvOrderInMonthQuantity.setText("" + viewData.saleOrdersInMonth);
			//san luong 3 thang gan fullDate
			tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInOneMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.quantityInOneMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthFirstQuantity.setText(tmp.getSpan());

			tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInTwoMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.quantityInTwoMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthSecondQuantity.setText(tmp.getSpan());

			tmp = new SpannableObject();
			tmp.addSpan(StringUtil.getString(R.string.TEXT_MONTH) + " " + viewData.numInThreeMonthAgo + "  	",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tmp.addSpan(
					StringUtil.formatNumber(viewData.quantityInThreeMonthAgo),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.BOLD);
			tvMonthThirdQuantity.setText(tmp.getSpan());
			//render doanh so CTTB tam thoi chua lam
			renderCustomerDisplayPrograme();
			//render cac don hang gan fullDate
			renderDataLastSaleOrder();
			// hien thi noi dung
			llContent.setVisibility(View.VISIBLE);
			if(!GlobalInfo.getInstance().isSysShowPrice()) {
				tvAmountInfo.setVisibility(View.GONE);
				llSaleInfo.setVisibility(View.GONE);
			} else if(GlobalInfo.getInstance().isSysShowPrice()) {
				tvAmountInfo.setVisibility(View.VISIBLE);
				llSaleInfo.setVisibility(View.VISIBLE);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_CUSTOMER_BASE_INFO:
			customerInfoDTO = (CustomerInfoDTO) modelEvent.getModelData();
			renderData(customerInfoDTO);
			isFirstInitView = false;
			parent.closeProgressDialog();
			requestInsertLogKPI(HashMapKPI.NVBH_CHITIETKHACHHANG, modelEvent.getActionEvent());
			//set change data customer
			if (GlobalInfo.isChangeInfoCurrentCustomer()) {
				GlobalInfo.setChangeInfoCurrentCustomer(false);
				GlobalInfo.setInfoCustomerChange(customerInfoDTO.getCustomer());
			}
			break;
		case ActionEventConstant.GET_CUSTOMER_SALE_SKU: {
			ArrayList<CustomerSaleSKUDTO> listSaleSKU = (ArrayList<CustomerSaleSKUDTO>) modelEvent
					.getModelData();
			showCustomerSaleSKUDialog(listSaleSKU);
			parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.GET_CUSTOMER_CAT_AMOUNT_IN_NEARLY_3_MONTH: {
			CustomerCatAmountPopupViewDTO listSaleSKU = (CustomerCatAmountPopupViewDTO) modelEvent
					.getModelData();
			showCustomerCatAmountDialog(listSaleSKU, requestShowQuantityPopup);
			parent.closeProgressDialog();
			break;
		}
		case ActionEventConstant.ACTION_GET_ALL_LIST_KEYSHOP:
			registerKeyShopDTO = (RegisterKeyShopViewDTO)modelEvent.getModelData();
			showDialogRegisterKeyShop();
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP:
			RegisterKeyShopViewDTO dtoTemp = (RegisterKeyShopViewDTO)modelEvent.getModelData();
			registerKeyShopDTO.lstItem = dtoTemp.lstItem;
			registerKeyShopDTO.lstCycle = dtoTemp.lstCycle;
			showDialogRegisterKeyShop();
			parent.closeProgressDialog();
			if(isUpdateRegisterKeyShop){
				parent.showDialog(StringUtil.getString(R.string.TEXT_REGISTER_KEYSHOP_SUCCESS));
				isUpdateRegisterKeyShop = false;
				GlobalUtil.forceHideKeyboard(parent);
			}
			break;
		case ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP:
			isUpdateRegisterKeyShop = true;
			getLevelOfKeyShop(ksId, cycleId,fromCycleId,toCycleId);
			CustomerListView frag = (CustomerListView) this.findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
			if (frag != null) {
				frag.isBackFromPopStack = false;
				// reset tat ca gia tri de lay lai du lieu dang ki keyshop
				frag.resetAllValue();
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Request lay thong tin doanh so sku trong thang cua kh
	 *
	 * @author: BANGHN
	 */
	private void requestGetSaleSKUInMonth() {
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, ""
				+ customerInfoDTO.getCustomer().getShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		handleViewEvent(data, ActionEventConstant.GET_CUSTOMER_SALE_SKU, SaleController.getInstance());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvQuantityLast3Month:
		case R.id.tvQuantityInMonth:
			requestShowQuantityPopup = true;
			if(!isShowingDialog){
				requestGetCustomerCatAmountINNearly3Month();
			}
			break;
		case R.id.tvSalesLast3Month:
		case R.id.tvSalesInMonth:
			requestShowQuantityPopup = false;
			//if (alertDialog == null) {
			if(!isShowingDialog){
				requestGetCustomerCatAmountINNearly3Month();
			}
			//}
			break;
		case R.id.tvSKU:
			//if (alertDialog == null) {
			if(!isShowingDialog){
				requestShowQuantityPopup = false;
				requestGetSaleSKUInMonth();
			}
			//}
			break;
		case R.id.tvSKUQuantity:
			//if (alertDialog == null) {
			if(!isShowingDialog){
				requestShowQuantityPopup = true;
				requestGetSaleSKUInMonth();
			}
			//}
			break;
		case R.id.btClose:
			if(alertDialog!=null){
				alertDialog.dismiss();
			}
			break;
		case R.id.tvInforDynamic:{
			gotoCustomerInforDynamic();
			break;
		}
		case R.id.tvImage:
			if (customerInfoDTO.getCustomer() == null) {
				break;
			}
			gotoListAlbumUser(customerInfoDTO.getCustomer());
			break;
		case R.id.tvPosition:
			gotoCustomerLocation();
			break;
		case R.id.tvRegisterKeyShop:
			getAllListKeyShop();
			break;
		default:
			break;
		}
		;
	}


	/**
	 * Qua man hinh xem cac thuoc tinh cua khach hang
	 * @author: hoanpd1
	 * @since: 17:28:16 18-03-2015
	 * @return: void
	 * @throws:
	 */
	private void gotoCustomerInforDynamic() {
		Bundle data = new Bundle();
		// truyen them id customer de hien thi thong tin
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, String.valueOf(customerId));
		data.putString(IntentConstants.INTENT_IS_VIEW_ALL, CustomerInfoView.class.getName());
		data.putBoolean(IntentConstants.INTENT_IS_EDIT, false);
		data.putBoolean(IntentConstants.INTENT_OLD_CUSTOMER, true);
		handleSwitchFragment(data, ActionEventConstant.GO_TO_CREATE_CUSTOMER, SaleController.getInstance());
	}

	/**
	 * Request lay thong tin doanh so sku trong thang cua kh
	 *
	 * @author: BANGHN
	 */
	private void requestGetCustomerCatAmountINNearly3Month() {
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, ""
				+ customerInfoDTO.getCustomer().getShopId());
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		handleViewEvent(data, ActionEventConstant.GET_CUSTOMER_CAT_AMOUNT_IN_NEARLY_3_MONTH, SaleController.getInstance());
	}

	boolean isShowingDialog = false;

	/**
	 * Hien thi dialog doanh so sku cua khach hang
	 *
	 * @author: BANGHN
	 * @param dto
	 */
	private void showCustomerSaleSKUDialog(
			ArrayList<CustomerSaleSKUDTO> listSaleSKU) {
		if(isShowingDialog){
			return;
		}

		Builder build = new AlertDialog.Builder(parent,android.R.style.Theme_Black_NoTitleBar_Fullscreen);

		CustomerSaleSKUPopupView popupSKU = new CustomerSaleSKUPopupView(
				parent, listSaleSKU, requestShowQuantityPopup, customerInfoDTO.numInMonth);
		build.setView(popupSKU.viewLayout);
		alertDialog = build.create();
		alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				isShowingDialog = false;
			}
		});
		Window window = alertDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255,
				255)));
		window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		if (!isShowingDialog && !alertDialog.isShowing()) {
			isShowingDialog = true;
			alertDialog.show();
		}
	}

	/**
	 * HLay doanh so theo nganh trong 3 thang gan nhat
	 *
	 * @author: BANGHN
	 * @param requestShowQuantityPopup2
	 * @param dto
	 */
	private void showCustomerCatAmountDialog(
			CustomerCatAmountPopupViewDTO listSaleSKU, boolean requestShowQuantityPopup) {
		if(isShowingDialog){
			return;
		}

		Builder build = new AlertDialog.Builder(parent,	android.R.style.Theme_Black_NoTitleBar_Fullscreen);

//		CustomerCatAmountPopupView popupSKU = new CustomerCatAmountPopupView(
//				parent, listSaleSKU, requestShowQuantityPopup, customerInfoDTO.numInMonth,
//				customerInfoDTO.numInOneMonthAgo, customerInfoDTO.numInTwoMonthAgo, customerInfoDTO.numInThreeMonthAgo);
//		build.setView(popupSKU.viewLayout);
		alertDialog = build.create();
		alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

			@Override
			public void onDismiss(DialogInterface arg0) {
				// TODO Auto-generated method stub
				isShowingDialog = false;
			}
		});
		Window window = alertDialog.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255,
				255)));
		window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);
		if (!isShowingDialog && !alertDialog.isShowing()) {
			isShowingDialog = true;
			alertDialog.show();
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case MENU_REVIEW: //Di toi man hinh danh gia nhan vien Danh cho module gs npp
			GlobalUtil.popBackStack(this.getActivity());
			handleSwitchFragment(null, ActionEventConstant.GO_TO_REVIEWS_STAFF_VIEW, SupervisorController.getInstance());
			break;
		case MENU_LIST_FEEDBACKS: // gotoFeedBackList
			GlobalUtil.popBackStack(this.getActivity());
			gotoFeedBackList();
			break;
		case MENU_MAP:
			GlobalUtil.popBackStack(this.getActivity());
			gotoCustomerLocation();
			break;
		case MENU_IMAGE:
			if (customerInfoDTO.getCustomer() == null) {
				break;
			}
			GlobalUtil.popBackStack(this.getActivity());
			gotoListAlbumUser(customerInfoDTO.getCustomer());
			break;
		case ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP:{
			ksId = ((Bundle)data).getLong(IntentConstants.INTENT_KEYSHOP_ID);
			cycleId = ((Bundle)data).getString(IntentConstants.INTENT_CYCLE_ID);
			fromCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_FROM_CYCLE_ID);
			toCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_TO_CYCLE_ID);
			getLevelOfKeyShop(ksId,cycleId,fromCycleId,toCycleId);
			break;
		}
		case ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP:
			@SuppressWarnings("unchecked")
			ArrayList<RegisterKeyShopItemDTO> lstItemClone = (ArrayList<RegisterKeyShopItemDTO>) ((Bundle) data)
					.getSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP);
			cycleId = ((Bundle) data).getString(IntentConstants.INTENT_CYCLE_ID);
			ksId = ((Bundle)data).getLong(IntentConstants.INTENT_KEYSHOP_ID);
			fromCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_FROM_CYCLE_ID);
			toCycleId = ((Bundle)data).getLong(IntentConstants.INTENT_TO_CYCLE_ID);
			updateRegisterKeyShop(lstItemClone,cycleId);
			break;
		default:
			break;
		}

	}


	/**
	 * Toi man hinh ds album cua kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customer
	 * @return: void
	 * @throws:
	 */

	private void gotoListAlbumUser(CustomerDTO customer) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
		bundle.putBoolean(IntentConstants.INTENT_FROM_SUPERVISOR, isFromSupervisor);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_LIST_ALBUM_USER, SaleController.getInstance());
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbLastSalesInfo) {
			currentPage = tbLastSalesInfo.getPagingControl().getCurrentPage();
			// getLastSaleOrder();
		}

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub

	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible() && !StringUtil.isNullOrEmpty(customerId)) {
				parent.showLoadingDialog();
				getCustomerInfo(customerId);
			}
			checkRegisterKeyshop();
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}


	 /**
	 * Lay tat ca key shop
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getAllListKeyShop(){
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId()+"");
		data.putInt(IntentConstants.INTENT_REGISTER_APPROVED_KEYSHOP, GlobalInfo.getInstance().getRegisterApprovedKeyshop());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_ALL_LIST_KEYSHOP, SaleController.getInstance());
	}

	/**
	 * hien thi man hinh dang ky keyshop
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void showDialogRegisterKeyShop() {
		if (keyShopView == null) {
			keyShopView = new RegisterKeyShopView(parent, this,
					StringUtil.getString(R.string.TEXT_REGISTER_KEY_SHOP_FULL));
		}
		boolean isFirst = false;
		if (!keyShopView.isShowing()) {
			keyShopView.show();
			keyShopView.getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
							| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
//			keyShopView.getWindow().setSoftInputMode(
//					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			isFirst = true;
		}
		keyShopView.renderLayout(registerKeyShopDTO, isFirst);
	}

	/**
	 * Lay cac muc cua keyshop
	 *
	 * @author: Tuanlt11
	 * @param ksId
	 * @param cycleId
	 * @return: void
	 * @throws:
	 */
	private void getLevelOfKeyShop(long ksId, String cycleId, long fromCycleId, long toCycleIds) {
		if(!parent.isShowProgressDialog())
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID,
				customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritId()
				+ "");
		data.putLong(IntentConstants.INTENT_KEYSHOP_ID, ksId);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		data.putLong(IntentConstants.INTENT_FROM_CYCLE_ID, fromCycleId);
		data.putLong(IntentConstants.INTENT_TO_CYCLE_ID, toCycleId);
		data.putInt(IntentConstants.INTENT_REGISTER_APPROVED_KEYSHOP, GlobalInfo.getInstance().getRegisterApprovedKeyshop());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LEVEL_OF_KEYSHOP,
				SaleController.getInstance());
	}

	/**
	 * Dang ki keyshop
	 *
	 * @author: Tuanlt11
	 * @param cycleId
	 * @return: void
	 * @throws:
	 */
	private void updateRegisterKeyShop(
			ArrayList<RegisterKeyShopItemDTO> lstItemClone, String cycleId) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID,customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritId()
				+ "");
		data.putSerializable(IntentConstants.INTENT_LIST_LEVEL_OF_KEY_SHOP,
				lstItemClone);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		handleViewEvent(data,
				ActionEventConstant.ACTION_UPDATE_LEVEL_OF_KEYSHOP,
				SaleController.getInstance());
	}

	 /**
	 * Kiem tra co hien thi dang ky keyshop hay ko
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void checkRegisterKeyshop(){
		// an dang ki keyshop
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() != UserDTO.TYPE_STAFF
				&& GlobalInfo.getInstance().getRegisterCTHTTM() != Constants.TYPE_REGISTER_CTHTTM) {
			tvRegisterKeyShop.setVisibility(View.GONE);
		}else{
			tvRegisterKeyShop.setVisibility(View.VISIBLE);
		}
	}
}
