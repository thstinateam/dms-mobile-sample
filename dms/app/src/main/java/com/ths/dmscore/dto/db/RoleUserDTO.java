/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;

import com.ths.dmscore.dto.AppVersionDTO;
import com.ths.dmscore.dto.DBVersionDTO;
import com.ths.dmscore.dto.UserDTO;

/**
 * Luu thong tin vai tro (phan quyen) nguoi dung
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class RoleUserDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 5727537176498584017L;
	public int roleId;
	public String roleCode;
	public String roleName;
	public ArrayList<ShopDTO> listShop;
	// thong tin lien quan user ke thua
	public UserDTO inheritProfile = new UserDTO();
	public String inheritStaffTypeName; // ten role ke thua
	public int inheritStaffSpecificType; // loai nv ke thua
	// thong tin version app
	public AppVersionDTO appVersion; // apVersion cua inherit user
	// thong tin version db
	public DBVersionDTO dbVersion; // db Version cua inherit user
}
