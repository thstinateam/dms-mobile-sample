/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 *  thong tin mot mat hang trong tam
 *  @author: HaiTC3
 *  @version: 1.1
 *  @since: 1.0
 */
public class SupervisorReportStaffSaleCat implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// d/s ke hoach
	public double amountPlan;
	// d/s da thuc hien
	public double amount;
	// san luong ke hoach
	public long quantityPlan;
	// San luong da thuc hien
	public long quantity;
	// tien do doanh so
	public double progressAmount;
	// tien do san luong
	public double progressQuantity;
	// con lai
	public double remain;
	// forcus item name
	public String focusItemName;

	public SupervisorReportStaffSaleCat() {
		amountPlan = 0;
		amount = 0;
		quantityPlan= 0 ;
		quantity= 0 ;
		progressAmount = 0;
		progressQuantity = 0;
		remain = 0;
		focusItemName = "";
	}
	
	/**
	 * 
	*  parse data from cursor cat name
	*  @author: DungNT19
	*  @param c
	*  @param catName
	*  @return: void
	*  @throws:
	 */
	public void parseDataFromCursor(Cursor c, String catName){
		amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_CAT" + catName + "_PLAN");
		amount = CursorUtil.getDoubleUsingSysConfig(c, "MONTH_CAT"+ catName);
		progressAmount = StringUtil.calPercentUsingRound(amountPlan, amount);
	}
	
	/**
	 * set amount, progress
	 * @author: DungNX
	 * @param amountPlan
	 * @param amount
	 * @return: void
	 * @throws:
	*/
	public void setAmount(double amountPlan, double amount){
		this.amountPlan = StringUtil.getDoubleUsingSysConfig(amountPlan);
		this.amount = StringUtil.getDoubleUsingSysConfig(amount);
		this.progressAmount = StringUtil.calPercentUsingRound(this.amountPlan, this.amount);
	}
	
	/**
	 * set quantity , progress quantity
	 * @author: hoanpd1
	 * @since: 11:20:24 07-04-2015
	 * @return: void
	 * @throws:  
	 * @param quantityPlan
	 * @param quantity
	 */
	public void setQuantity(long quantityPlan, long quantity){
		this.quantityPlan = quantityPlan;
		this.quantity = quantity;
		this.progressQuantity = StringUtil.calPercentUsingRound(this.quantityPlan, this.quantity);

	}
}


