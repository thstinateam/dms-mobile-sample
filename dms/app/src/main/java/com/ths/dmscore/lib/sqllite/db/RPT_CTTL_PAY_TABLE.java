package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.RptCttlDetailPayDTO;
import com.ths.dmscore.dto.db.RptCttlPayDTO;
import com.ths.dmscore.util.MyLog;

/**
 * Chua thong tin tien trinh km tich luy detail
 * 
 * @author: DungNT19
 * @version: 1.0
 * @since: 1.0
 */
public class RPT_CTTL_PAY_TABLE extends ABSTRACT_TABLE {
	public static final String RPT_CTTL_PAY_ID = "RPT_CTTL_PAY_ID";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	public static final String PROMOTION_FROM_DATE = "PROMOTION_FROM_DATE";
	public static final String PROMOTION_TO_DATE = "PROMOTION_TO_DATE";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String TOTAL_QUANTITY_PAY_PROMOTION = "TOTAL_QUANTITY_PAY_PROMOTION";
	public static final String TOTAL_AMOUNT_PAY_PROMOTION = "TOTAL_AMOUNT_PAY_PROMOTION";
	public static final String PROMOTION = "PROMOTION";
	public static final String IS_MIGRATE = "IS_MIGRATE";
	public static final String APPROVED = "APPROVED";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String AMOUNT_PROMOTION = "AMOUNT_PROMOTION";
	public static final String ACTUALLY_PROMOTION = "ACTUALLY_PROMOTION";
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";

	public static final String TABLE_NAME = "RPT_CTTL_PAY";

	public RPT_CTTL_PAY_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { RPT_CTTL_PAY_ID, CREATE_DATE,
				PROMOTION_PROGRAM_ID, PROMOTION_FROM_DATE, PROMOTION_TO_DATE,
				SHOP_ID, CUSTOMER_ID, TOTAL_QUANTITY_PAY_PROMOTION,
				TOTAL_AMOUNT_PAY_PROMOTION, PROMOTION, IS_MIGRATE, APPROVED,
				STAFF_ID, AMOUNT_PROMOTION, ACTUALLY_PROMOTION, SALE_ORDER_ID, SYN_STATE };
		;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#insert(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value =  initDataRow((RptCttlPayDTO) dto);
		return insert(null, value);
	}
	
	private ContentValues initDataRow(RptCttlPayDTO dto) {
		ContentValues editedValues = new ContentValues();
		
		editedValues.put(RPT_CTTL_PAY_ID, dto.rptCttlPayId);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(PROMOTION_PROGRAM_ID, dto.promotionProgramId);
		editedValues.put(PROMOTION_FROM_DATE, dto.promotionFromDate);
		editedValues.put(PROMOTION_TO_DATE, dto.promotionToDate);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(TOTAL_QUANTITY_PAY_PROMOTION, dto.totalQuantityPayPromotion);
		editedValues.put(TOTAL_AMOUNT_PAY_PROMOTION, dto.totalAmountPayPromotion);
		editedValues.put(PROMOTION, dto.promotion);
		editedValues.put(APPROVED, dto.approved);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		
		return editedValues;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#update(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.viettel.vinamilk.lib.sqllite.db.ABSTRACT_TABLE#delete(com.viettel.vinamilk.dto.db.AbstractTableDTO)
	 */
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public ArrayList<RptCttlPayDTO> deleteAllRptCttlPayOfOrder(long saleOrderId) {
		ArrayList<RptCttlPayDTO> cttlPayList = new ArrayList<RptCttlPayDTO>();
		//Lay ds rpt cttl pay de xoa
		StringBuilder sqlBalance = new StringBuilder();
		ArrayList<String> paramsBalance = new ArrayList<String>();
		sqlBalance.append("SELECT * FROM ");
		sqlBalance.append("       RPT_CTTL_PAY RPT_AP_PAY ");
		sqlBalance.append("WHERE 1 = 1 ");
		sqlBalance.append("AND RPT_AP_PAY.SALE_ORDER_ID = ? ");
		paramsBalance.add(String.valueOf(saleOrderId));
		Cursor c = null;
		try {
			c = rawQuery(sqlBalance.toString(), paramsBalance.toArray(new String[paramsBalance.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						RptCttlPayDTO cttlPayDto = new RptCttlPayDTO();
						cttlPayDto.initFromCursor(c);
						cttlPayList.add(cttlPayDto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		RPT_CTTL_DETAIL_PAY_TABLE rptDetailPayTable = new RPT_CTTL_DETAIL_PAY_TABLE(mDB);
		//Xoa tung chi tiet con truoc roi moi xoa cha sau
		for(RptCttlPayDTO cttlPayDto : cttlPayList) {
			delete("RPT_CTTL_PAY_ID = ?", new String[] {String.valueOf(cttlPayDto.rptCttlPayId)});
			rptDetailPayTable.delete("RPT_CTTL_PAY_ID = ?", new String[] {String.valueOf(cttlPayDto.rptCttlPayId)});
		}
		return cttlPayList;
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ArrayList<RptCttlPayDTO>
	 * @throws:  
	 * @param saleOrderId
	 * @return
	 */
	public ArrayList<RptCttlPayDTO> getRptCttlPayBySaleOrderId(long saleOrderId) {
		ArrayList<RptCttlPayDTO> cttlPayList = new ArrayList<RptCttlPayDTO>();
		ArrayList<String> cttpPayIdList = new ArrayList<String>();
		//Lay ds rpt cttl pay de xoa
		StringBuilder sqlBalance = new StringBuilder();
		ArrayList<String> paramsBalance = new ArrayList<String>();
		sqlBalance.append("SELECT * FROM ");
		sqlBalance.append("       RPT_CTTL_PAY RPT_AP_PAY ");
		sqlBalance.append("WHERE 1 = 1 ");
		sqlBalance.append("AND RPT_AP_PAY.SALE_ORDER_ID = ? ");
		paramsBalance.add(String.valueOf(saleOrderId));
		Cursor c = null;
		try {
			c = rawQuery(sqlBalance.toString(), paramsBalance.toArray(new String[paramsBalance.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						RptCttlPayDTO cttlPayDto = new RptCttlPayDTO();
						cttlPayDto.initFromCursor(c);
						cttlPayList.add(cttlPayDto);
						cttpPayIdList.add(String.valueOf(cttlPayDto.rptCttlPayId));
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		//Lay thong tin cac sp tich luy
		StringBuilder sqlDetailPay = new StringBuilder();
		ArrayList<String> paramsDetailPay = new ArrayList<String>();
		sqlDetailPay.append("SELECT * FROM ");
		sqlDetailPay.append("       RPT_CTTL_DETAIL_PAY RPT_AP_DETAIL_PAY ");
		sqlDetailPay.append("WHERE 1 = 1 ");
		sqlDetailPay.append("AND RPT_AP_DETAIL_PAY.RPT_CTTL_PAY_ID IN ( ");
		sqlDetailPay.append(TextUtils.join(",", cttpPayIdList));
		sqlDetailPay.append(") ");
		Cursor cDetailPay = null;
		try {
			cDetailPay = rawQuery(sqlDetailPay.toString(), paramsDetailPay.toArray(new String[paramsDetailPay.size()]));
			if (cDetailPay != null) {
				if (cDetailPay.moveToFirst()) {
					do {
						RptCttlDetailPayDTO rptCttlDetail = new RptCttlDetailPayDTO();
						rptCttlDetail.initFromCursor(cDetailPay);
						
						for(RptCttlPayDTO rptCttlPay : cttlPayList) {
							if(rptCttlPay.rptCttlPayId == rptCttlDetail.rptCttlPayId) {
								rptCttlPay.rptCttlDetailList.add(rptCttlDetail);
								break;
							}
						}
						
//						int productId = cDetailPay.getInt(cDetailPay.getColumnIndex(RPT_CTTL_DETAIL_PAY_TABLE.PRODUCT_ID));
//						int quantityPayPromotion = cDetailPay.getInt(cDetailPay.getColumnIndex(RPT_CTTL_DETAIL_PAY_TABLE.QUANTITY_PROMOTION));
//						long amountPayPromotion = cDetailPay.getLong(cDetailPay.getColumnIndex(RPT_CTTL_DETAIL_PAY_TABLE.AMOUNT_PROMOTION));
//						
//						for(RptCttlDetailDTO rptCttlDetail : listRptDetail) {
//							if(rptCttlDetail.productId == productId) {
//								rptCttlDetail.quantity -= quantityPayPromotion;
//								rptCttlDetail.amount -= amountPayPromotion;
//								break;
//							}
//						}
						
					} while (cDetailPay.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (cDetailPay != null) {
					cDetailPay.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
		return cttlPayList;
	}
}