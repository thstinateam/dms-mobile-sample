package com.ths.dmscore.lib.sqllite.sync;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.syndata.SynDataTableDTO;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

public class SynDataTableDAO {
	//thoi gian gui log khi dong bo loi
	public static long timeSendLogErrorSynData = 0;

	List<SynDataTableDTO> listTables;
	SQLiteDatabase db;
	//so table, row du lieu update de kiem tra log len server.
	public int numTableUpdate = 0;
	public long numRowUpdate = 0;
	//trang thai db de gui log
	int stateDBConnection = 0;
	private final static int STATE_DB_CLOSE = 1; // db close
	private final static int STATE_DB_LOCK = 2; // database lock
	private final static int STATE_DB_READ_ONLY = 3; // database chi read only
	private final static int STATE_DB_OTHER= 4; // nhung trang thai khac cua DB
	public SynDataTableDAO(List<SynDataTableDTO> listTables) {
		this.listTables = listTables;
	}

	public SynDataTableDAO() {

	}

	private boolean openDBForWrite() throws Exception {
		boolean result = false;

		try {
			db = SQLUtils.getInstance().getmDB();
			if (!db.isOpen()) {
				stateDBConnection = STATE_DB_CLOSE;
				return result;
			}
			if (db.isDbLockedByCurrentThread() || db.isDbLockedByOtherThreads()) {
				stateDBConnection = STATE_DB_LOCK;
				return result;
			}
			if (db.isReadOnly()) {
				stateDBConnection = STATE_DB_READ_ONLY;
				return result;
			}

			result = true;
		} catch (Exception ex) {
			stateDBConnection = STATE_DB_OTHER;
			throw ex;
		}

		return result;
	}

	private boolean openDBForRead() throws Exception {
		boolean result = false;

		try {
			if (db == null) {
				db = SQLUtils.getInstance().getmDB();;//db = DBHelper.getInstance().getReadableDatabase();
			}

			if (!db.isOpen()) {
				return result;
			}

			result = true;
		} catch (Exception ex) {
			throw ex;
		}

		return result;
	}

	public int synData(long lastLogId, long newLogId) throws Exception {
		int rows = 0;
		boolean haveErrorSynData = false;
		if (this.listTables == null || this.listTables.size() <= 0) {
			throw new Exception("No data need to syn.");
		} else {
			if (openDBForWrite()) {
				try {
					// Begin transaction
					this.db.beginTransaction();
					numTableUpdate = this.listTables.size();
					for (SynDataTableDTO item : this.listTables) {
						String tableName = item.getTableName();
						String query = item.getPkField() +" = ? ";

						String[] arrColumnName =item.getTableColumns();
						List<List<String>> listRowData = item.getData();

						SynDataRowDAO rowDTO = new SynDataRowDAO(this.db,
								tableName, arrColumnName, query, null , item.getPkField());
						// xoa nhung record co trang thai la 1 (trang thai
						// chuyen len server roi)
						//rowDTO.deleteTranseferedRows(tableName);
						// insert du lieu tu server ve database
						numRowUpdate += listRowData.size();
						for (List<String> row : listRowData) {
							//String[] arrValues = getArgForQuery(row, ",");
							rowDTO.executeSyncRow(db, row);
							if(rowDTO.haveErrorSynData)
								haveErrorSynData = rowDTO.haveErrorSynData;
						}
					}
					// Commit transaction
					this.db.setTransactionSuccessful();
					//gui log khi qua trinh dong bo xay ra loi
					if(haveErrorSynData){
						ServerLogger.sendLog("SYNDATA",
								"LastLogId :  " + lastLogId +
								"  -  ResponeLogId: " + newLogId +
								"  -  NumTable, NumRow update :  " + numTableUpdate + " - "
								+  numRowUpdate, false, TabletActionLogDTO.LOG_CLIENT);
					}
				}catch (Exception ex) {
					//MyLog.logToFile("SYN", DateUtils.now() + "- Exception : " + ex.toString());
					if((System.currentTimeMillis() - timeSendLogErrorSynData) >= 30000){
						timeSendLogErrorSynData = System.currentTimeMillis();
						ServerLogger.sendLog("SYNDATA",
							ex.toString(), false, TabletActionLogDTO.LOG_EXCEPTION);
					}
					throw ex;
				} finally {
					// Rollback transaction
					this.db.endTransaction();
					// this.db.close();
				}
			} else {
				MyLog.d("SYN", DateUtils.now() + "- Exception Can't open connection to SQLite. Please check it again: ");
				throw new Exception(
						"Can't open connection to SQLite. Please check it again.");
			}
		}
		return rows;
	}

	// lay gia tri cac arg trong doi tuong row.
	// strArg : chuoi arg name can lay, cac column ngan cach nhau boi dau ";"
	// row : row can lay ra chuoi gia tri.
	public String[] getArgForQuery(String strArg,String split) throws Exception {
		String[] result = null;

		try {
			if (strArg == null || "".equals(strArg)) {
				return result;
			} else {
				result = strArg.split(split);
				if (result == null || result.length <= 0) {
					throw new Exception(
							"Can't get list name of argument query.");
				}

			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			// this.db.close();
		}

		return result;
	}


	public void readDataFromTable() throws Exception {
		if (this.openDBForRead()) {
			try {
				List<String> listTableName = this.getDBNames();
				// // S O S ADD THEM PRODUCT
				listTableName.add("PRODUCT");

				if (listTableName == null || listTableName.size() > 0) {
					MyLog.e("Total of table : ",
							Integer.toString(listTableName.size()));

					MyLog.e("List table name :", "");

					for (String tableName : listTableName) {

						MyLog.e("tableName : ", tableName);

						String selectQuery = "SELECT  * FROM " + tableName;

						Cursor cursor = this.db.rawQuery(selectQuery, null);

						if (cursor != null) {
							MyLog.e("Number row in table : ",
									Integer.toString(cursor.getCount()));
							int countTemp = 0;
							try {
								if (cursor.moveToFirst()) {
									do {
										int colCount = cursor.getColumnCount();
										StringBuilder log = new StringBuilder();
										
										for (int i = 0; i < colCount; i++) {
											log.append(cursor.getColumnName(i) + " : "
													+ cursor.getString(i) + " ||| ");
										}
										
										MyLog.e("Rows " + ++countTemp, log.toString());
										
									} while (cursor.moveToNext());
								}
							} catch (Exception e) {
								MyLog.e("getAllRow", e);
							} finally {
								if (cursor != null) {
									cursor.close();
								}
							}
							
						}

					}
				}

			} catch (Exception ex) {

			} finally {
				// this.db.close();
			}
		} else {
			throw new Exception(
					"Can't open connection to SQLite. Please check it again.");
		}
	}

	public List<String> getDBNames() throws Exception {
		List<String> result = new ArrayList<String>();

		if (this.openDBForRead()) {
			try {
				StringBuilder sb = new StringBuilder();
				sb.append("SELECT name FROM sqlite_master ");
				sb.append("WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%' AND name NOT LIKE 'android%'");
				sb.append("UNION ALL ");
				sb.append("SELECT name FROM sqlite_temp_master ");
				sb.append("WHERE type IN ('table','view') AND name NOT LIKE 'android%'");
				sb.append("ORDER BY 1");

				// sb.append("SELECT name FROM sqlite_master ");
				// sb.append("WHERE type IN ('table','view')");

				Cursor c = this.db.rawQuery(sb.toString(), null);
				c.moveToFirst();

				// result = new String[c.getCount()];
				while (c.moveToNext()) {
					result.add(CursorUtil.getString(c, "name"));
				}
				c.close();
			} catch (SQLiteException e) {
				MyLog.e("Exception : ", e.getMessage());
			}

			return result;
		} else {
			throw new Exception(
					"Can't open connection to SQLite. Please check it again.");
		}

	}

	public boolean insertTempProduct() throws Exception {
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put("PRODUCT_ID", 1);
			editedValues.put("PRODUCT_NAME", "thuattq");
			editedValues.put("STATUS", 1);
			editedValues.put("UOM1", "xxxx");
			editedValues.put("UOM2", "yy");

			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM PRODUCT where PRODUCT_ID = 1");
			Cursor c = this.db.rawQuery(sb.toString(), null);

			if (c.getCount() > 0) {
				if (db.update("PRODUCT", editedValues, "PRODUCT_ID = ?",
						new String[] { "1" }) > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				if (db.insert("PRODUCT", null, editedValues) > 0) {
					return true;
				} else {
					return false;
				}
			}

		} catch (Exception ex) {
			throw ex;
		}
	}

	 /**
	 * Thuc thi dong bo script
	 * @author:
	 * @param listScript
	 * @return: void
	 * @throws:
	*/
	public void executeDBScript(List<String> listScript) {
		if (listScript == null || listScript.size() < 1) {
			return;
		}
		SQLiteDatabase mDB = null;
		try {
			mDB = SQLUtils.getInstance().getmDB();
			mDB.beginTransaction();
			for (String script : listScript) {
				mDB.execSQL(script);
			}
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
		} finally {
			if (mDB != null) {
				try {
					mDB.endTransaction();
				} catch (Exception e1) {
				}
			}
		}
	}

	 /**
	 * Dong bo du lieu theo script
	 * @author: Tuanlt11
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	*/
	public int synData() throws Exception {
		int rows = 0;
		//int total = 0;
		//boolean haveErrorSynData = false;
		if (this.listTables == null || this.listTables.size() <= 0) {
			throw new Exception("No data need to syn.");
		} else {
			if (openDBForWrite()) {
				try {
					// Begin transaction
					this.db.beginTransaction();
					numTableUpdate = this.listTables.size();
					for (SynDataTableDTO item : this.listTables) {
						String tableName = item.getTableName();
						String query = item.getPkField() +" = ? ";

						String[] arrColumnName =item.getTableColumns();
						List<List<String>> listRowData = item.getData();

						//total+= listRowData.size();
						SynDataRowDAO rowDTO = new SynDataRowDAO(this.db,
								tableName, arrColumnName, query, null , item.getPkField());
						// xoa nhung record co trang thai la 1 (trang thai
						// chuyen len server roi)
						//rowDTO.deleteTranseferedRows(tableName);
						// insert du lieu tu server ve database
						numRowUpdate += listRowData.size();
						for (List<String> row : listRowData) {
							//String[] arrValues = getArgForQuery(row, ",");
							rowDTO.executeSyncRow(db, row);
							//if(rowDTO.haveErrorSynData)
								//haveErrorSynData = rowDTO.haveErrorSynData;
						}
					}
					// Commit transaction
					this.db.setTransactionSuccessful();
					//MyLog.d("SynData", DateUtils.now() + ": Row Data = " + total);
				}catch (Exception ex) {
					MyLog.d("SYN", DateUtils.now()  + "- Exception : " + ex.toString());
					//MyLog.logToFile("SYN", DateUtils.now() + "- Exception : " + ex.toString());
					if((System.currentTimeMillis() - timeSendLogErrorSynData) >= 10000){
						timeSendLogErrorSynData = System.currentTimeMillis();
						ServerLogger.sendLog("SYNDATA", VNMTraceUnexceptionLog
								.getReportFromThrowable(ex), false,
								TabletActionLogDTO.LOG_EXCEPTION);
						//LogFile.logToFile(DateUtils.now() + " || SynData || Exception =  " + ex.toString());
					}
					throw ex;
				} finally {
					// Rollback transaction
					this.db.endTransaction();
					// this.db.close();
				}
			} else {
				MyLog.d("SYN", DateUtils.now() + "- Exception Can't open connection to SQLite. Please check it again: ");
				throw new Exception(
						"Can't open connection to SQLite. Please check it again. StateConnection = " + stateDBConnection);
			}
		}
		return rows;
	}
}