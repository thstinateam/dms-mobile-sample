package com.ths.dmscore.dto.syndata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultObject {
	@JsonProperty("result")
	private ResponObject result;

	public ResponObject getResult() {
		return result;
	}

	public void setResult(ResponObject result) {
		this.result = result;
	}

}
