/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.math.BigDecimal;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.PO_PROMOTION_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_PROMOTION_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;


/**
 * SaleOrderPromotionDTO.java
 * @author: dungnt19
 * @version: 1.0
 * @since:  1.0
 */
public class SaleOrderPromotionDTO extends AbstractTableDTO {
	public static final int GROUP_TYPE_PRODUCT = 1;
	public static final int GROUP_TYPE_PROMOTION = 2;

	public static final int TYPE_PROMOTION_QUANTITY = 1;
	public static final int TYPE_PROMOTION_NUM = 2;
	public static final int TYPE_PROMOTION_AMOUNT = 3;

	public static final int TYPE_CHECK_NULL = -1;
	public static final int TYPE_CHECK_SHOP = 1;
	public static final int TYPE_CHECK_STAFF = 2;
	public static final int TYPE_CHECK_CUSTOMER = 3;

	private static final long serialVersionUID = 7474255160980812051L;
	public long saleOrderPromotionId;
	public long saleOrderId;
	public long shopId;
	public String orderDate;
	public long promotionProgramId;
	public String promotionProgramCode;
	public long productGroupId;
	public long groupLevelId;
	public int promotionLevel;
	public int quantityReceived;
	public int quantityReceivedMax;
	public long staffId;
	public String createUser;
	public String createDate;
	public String updateDate;
	public String updateUser;
	public long poPromotionId;
	public long poId;

	//KM duoc sp
	public int hasProduct;

	//Da render len MH
	public boolean isShowed;
	// Co duoc KM tien (muc con) trong 1 muc
	public boolean hasMoney;
	// them de xu li cho phan kiem tru di so tien hoac so luong vansale
	// so luong toi da dc nhan cho mot ctkm
	public double amountReceived;
	public int numReceived; // so luong da nhan duoc
	public double amountReceivedMax;
	public int numReceivedMax; // so luong da nhan duoc
	public String promotionProgramName;
	public String promotionDetail;

	//Loai KM: so suat, so luong, so tien
	public int exceedUnit;
	//Doi tuong phan bo: shop, staff, customer
	public int exceedObject;
	public double[][][] promotionDetailValue;
	//Muc cua nhom
	public int groupOrderNumber;

	/** Khoi tao du lieu nhom cua CTKM
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		saleOrderPromotionId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_PROMOTION_ID);
		saleOrderId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_ID);
		poPromotionId = CursorUtil.getLong(c, PO_PROMOTION_TABLE.PO_CUSTOMER_PROMOTION_ID);

		poId = CursorUtil.getLong(c, PO_PROMOTION_TABLE.PO_CUSTOMER_ID);
		shopId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.SHOP_ID);
		orderDate = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.ORDER_DATE);
		promotionProgramId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_ID);
		promotionProgramCode = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_CODE);
		productGroupId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.PRODUCT_GROUP_ID);
		groupLevelId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL_ID);
		promotionLevel = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL);
		quantityReceived = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED);
		quantityReceivedMax = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED_MAX);
		numReceived = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED);
		numReceivedMax = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED_MAX);
		amountReceived = CursorUtil.getDouble(c, SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED);
		amountReceivedMax = CursorUtil.getDouble(c, SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED_MAX);
		promotionDetail = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.PROMOTION_DETAIL);
		exceedUnit = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.EXCEED_UNIT);
		exceedObject = CursorUtil.getInt(c, SALE_ORDER_PROMOTION_TABLE.EXCEED_OBJECT);
		staffId = CursorUtil.getLong(c, SALE_ORDER_PROMOTION_TABLE.STAFF_ID);
		createUser = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.CREATE_USER);
		createDate = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.CREATE_DATE);
		updateDate = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.UPDATE_DATE);
		updateUser = CursorUtil.getString(c, SALE_ORDER_PROMOTION_TABLE.UPDATE_USER);
	}

	/**
	 * xoa tat ca so suat cua don hang
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:59:23 15 Sep 2014
	 * @return: Vector<?>
	 * @throws:
	 * @param saleOrderId2
	 * @return
	 */
	public JSONObject generateDeleteByOrderSql(long saleOrderId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_PROMOTION_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_ID, String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * xoa tat ca so suat cua PO
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:59:23 15 Sep 2014
	 * @return: Vector<?>
	 * @throws:
	 * @param saleOrderId2
	 * @return
	 */
	public JSONObject generateDeleteByPoSql(long poId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, PO_PROMOTION_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PO_CUSTOMER_ID, String.valueOf(poId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * gen json for insert sale order promotion
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 19:24:49 15 Sep 2014
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonInsertSaleOrderPromotion() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_PROMOTION_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_PROMOTION_ID, this.saleOrderPromotionId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_ID, this.saleOrderId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_ID, this.promotionProgramId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_CODE, this.promotionProgramCode, null));
			if(this.productGroupId > 0){
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PRODUCT_GROUP_ID, this.productGroupId, null));
			}

			if(this.groupLevelId > 0){
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL_ID, this.groupLevelId, null));
			}

			if(this.promotionLevel > 0){
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL, this.promotionLevel, null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED, this.quantityReceived, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED_MAX, this.quantityReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED, this.numReceived, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED_MAX, this.numReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED, this.amountReceived, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED_MAX, this.amountReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.EXCEED_UNIT, this.exceedUnit, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.EXCEED_OBJECT, this.exceedObject, null));
			if (!StringUtil.isNullOrEmpty(this.promotionDetail)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.PROMOTION_DETAIL, this.promotionDetail, null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.STAFF_ID, this.staffId, null));
			if (!StringUtil.isNullOrEmpty(this.createUser)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.CREATE_USER, this.createUser, null));
			}

			if (!StringUtil.isNullOrEmpty(this.createDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.CREATE_DATE, this.createDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.updateDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.UPDATE_DATE, this.updateDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.updateUser)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_PROMOTION_TABLE.UPDATE_USER, this.updateUser, null));
			}

			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}


	/**
	 * gen json for insert po promotion
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 19:24:10 15 Sep 2014
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonInsertPoPromotion() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, PO_PROMOTION_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PO_CUSTOMER_PROMOTION_ID, this.poPromotionId, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PO_CUSTOMER_ID, this.poId, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.ORDER_DATE, this.orderDate, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PROMOTION_PROGRAM_ID, this.promotionProgramId, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PROMOTION_PROGRAM_CODE, this.promotionProgramCode, null));
			if(this.productGroupId > 0){
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PRODUCT_GROUP_ID, this.productGroupId, null));
			}

			if(this.groupLevelId > 0){
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.GROUP_LEVEL_ID, this.groupLevelId, null));
			}

			if(this.promotionLevel > 0){
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PROMOTION_LEVEL, this.promotionLevel, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.QUANTITY_RECEIVED, this.quantityReceived, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.QUANTITY_RECEIVED_MAX, this.quantityReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.NUM_RECEIVED, this.numReceived, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.NUM_RECEIVED_MAX, this.numReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.AMOUNT_RECEIVED, this.amountReceived, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.AMOUNT_RECEIVED_MAX, this.amountReceivedMax, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.EXCEED_UNIT, this.exceedUnit, null));
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.EXCEED_OBJECT, this.exceedObject, null));
			if (!StringUtil.isNullOrEmpty(this.promotionDetail)) {
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.PROMOTION_DETAIL, this.promotionDetail, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.STAFF_ID, this.staffId, null));
			if (!StringUtil.isNullOrEmpty(this.createUser)) {
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.CREATE_USER, this.createUser, null));
			}

			if (!StringUtil.isNullOrEmpty(this.createDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.CREATE_DATE, this.createDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.updateDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.UPDATE_DATE, this.updateDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.updateUser)) {
				params.put(GlobalUtil.getJsonColumn(PO_PROMOTION_TABLE.UPDATE_USER, this.updateUser, null));
			}

			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * init insert order sale promotion
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 19:13:13 15 Sep 2014
	 * @return: ContentValues
	 * @throws:
	 * @param this
	 * @return
	 */
	public ContentValues initInsertOrderSalePromotion(){
		ContentValues values = new ContentValues();
		values.put(SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_PROMOTION_ID, this.saleOrderPromotionId);
		values.put(SALE_ORDER_PROMOTION_TABLE.SALE_ORDER_ID, this.saleOrderId);
		values.put(SALE_ORDER_PROMOTION_TABLE.SHOP_ID, this.shopId);
		values.put(SALE_ORDER_PROMOTION_TABLE.ORDER_DATE, this.orderDate);
		values.put(SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_ID, this.promotionProgramId);
		values.put(SALE_ORDER_PROMOTION_TABLE.PROMOTION_PROGRAM_CODE, this.promotionProgramCode);
		if(this.productGroupId > 0){
			values.put(SALE_ORDER_PROMOTION_TABLE.PRODUCT_GROUP_ID, this.productGroupId);
		}

		if(this.groupLevelId > 0){
			values.put(SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL_ID, this.groupLevelId);
		}

		if(this.promotionLevel > 0){
			values.put(SALE_ORDER_PROMOTION_TABLE.PROMOTION_LEVEL, this.promotionLevel);
		}
		values.put(SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED, this.quantityReceived);
		values.put(SALE_ORDER_PROMOTION_TABLE.QUANTITY_RECEIVED_MAX, this.quantityReceivedMax);
		values.put(SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED, this.numReceived);
		values.put(SALE_ORDER_PROMOTION_TABLE.NUM_RECEIVED_MAX, this.numReceivedMax);
		values.put(SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED, this.amountReceived);
		values.put(SALE_ORDER_PROMOTION_TABLE.AMOUNT_RECEIVED_MAX, this.amountReceivedMax);
		values.put(SALE_ORDER_PROMOTION_TABLE.EXCEED_UNIT, this.exceedUnit);
		values.put(SALE_ORDER_PROMOTION_TABLE.EXCEED_OBJECT, this.exceedObject);
		if (!StringUtil.isNullOrEmpty(this.promotionDetail)) {
			values.put(SALE_ORDER_PROMOTION_TABLE.PROMOTION_DETAIL, this.promotionDetail);
		}
		values.put(SALE_ORDER_PROMOTION_TABLE.STAFF_ID, this.staffId);
		if (!StringUtil.isNullOrEmpty(this.createUser)) {
			values.put(SALE_ORDER_PROMOTION_TABLE.CREATE_USER, this.createUser);
		}

		if (!StringUtil.isNullOrEmpty(this.createDate)) {
			values.put(SALE_ORDER_PROMOTION_TABLE.CREATE_DATE, this.createDate);
		}

		if (!StringUtil.isNullOrEmpty(this.updateDate)) {
			values.put(SALE_ORDER_PROMOTION_TABLE.UPDATE_DATE, this.updateDate);
		}

		if (!StringUtil.isNullOrEmpty(this.updateUser)) {
			values.put(SALE_ORDER_PROMOTION_TABLE.UPDATE_USER, this.updateUser);
		}

		return values;
	}

	/**
	 * init insert po sale promotion
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 19:13:13 15 Sep 2014
	 * @return: ContentValues
	 * @throws:
	 * @param this
	 * @return
	 */
	public ContentValues initInsertPoSalePromotion(){
		ContentValues values = new ContentValues();
		values.put(PO_PROMOTION_TABLE.PO_CUSTOMER_PROMOTION_ID, this.poPromotionId);
		values.put(PO_PROMOTION_TABLE.PO_CUSTOMER_ID, this.poId);
		values.put(PO_PROMOTION_TABLE.SHOP_ID, this.shopId);
		values.put(PO_PROMOTION_TABLE.ORDER_DATE, this.orderDate);
		values.put(PO_PROMOTION_TABLE.PROMOTION_PROGRAM_ID, this.promotionProgramId);
		values.put(PO_PROMOTION_TABLE.PROMOTION_PROGRAM_CODE, this.promotionProgramCode);
		if(this.productGroupId > 0){
			values.put(PO_PROMOTION_TABLE.PRODUCT_GROUP_ID, this.productGroupId);
		}

		if(this.groupLevelId > 0){
			values.put(PO_PROMOTION_TABLE.GROUP_LEVEL_ID, this.groupLevelId);
		}

		if(this.promotionLevel > 0){
			values.put(PO_PROMOTION_TABLE.PROMOTION_LEVEL, this.promotionLevel);
		}
		values.put(PO_PROMOTION_TABLE.QUANTITY_RECEIVED, this.quantityReceived);
		values.put(PO_PROMOTION_TABLE.QUANTITY_RECEIVED_MAX, this.quantityReceivedMax);
		values.put(PO_PROMOTION_TABLE.NUM_RECEIVED, this.numReceived);
		values.put(PO_PROMOTION_TABLE.NUM_RECEIVED_MAX, this.numReceivedMax);
		values.put(PO_PROMOTION_TABLE.AMOUNT_RECEIVED, this.amountReceived);
		values.put(PO_PROMOTION_TABLE.AMOUNT_RECEIVED_MAX, this.amountReceivedMax);
		values.put(PO_PROMOTION_TABLE.EXCEED_UNIT, this.exceedUnit);
		values.put(PO_PROMOTION_TABLE.EXCEED_OBJECT, this.exceedObject);
		if (!StringUtil.isNullOrEmpty(this.promotionDetail)) {
			values.put(PO_PROMOTION_TABLE.PROMOTION_DETAIL, this.promotionDetail);
		}
		values.put(PO_PROMOTION_TABLE.STAFF_ID, this.staffId);
		if (!StringUtil.isNullOrEmpty(this.createUser)) {
			values.put(PO_PROMOTION_TABLE.CREATE_USER, this.createUser);
		}

		if (!StringUtil.isNullOrEmpty(this.createDate)) {
			values.put(PO_PROMOTION_TABLE.CREATE_DATE, this.createDate);
		}

		if (!StringUtil.isNullOrEmpty(this.updateDate)) {
			values.put(PO_PROMOTION_TABLE.UPDATE_DATE, this.updateDate);
		}

		if (!StringUtil.isNullOrEmpty(this.updateUser)) {
			values.put(PO_PROMOTION_TABLE.UPDATE_USER, this.updateUser);
		}

		return values;
	}

	/**
	 * So suat cua CT HTTM
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void initHTTMQuantityReceived(OrderDetailViewDTO promotion) {
		promotionProgramId = promotion.orderDetailDTO.programeId;
		promotionProgramCode = promotion.orderDetailDTO.programeCode;
		if((int)promotion.orderDetailDTO.quantity == 0) {
			quantityReceived = 0;
			quantityReceivedMax = 0;
		} else {
			quantityReceived = 1;
			quantityReceivedMax = 1;	
		}
		
		numReceived = (int) promotion.orderDetailDTO.quantity;
		numReceivedMax = (int) promotion.orderDetailDTO.quantity;
	}

	/**
	 * Luu thong tin so suat: (111: 5); (112: 100); (121: 5); (122: 10); (131: 12); (132:   ); (231: 12);……..
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */

	public String generatePromotionDetail() {
		StringBuilder result = new StringBuilder();

		int numPromotionUnit = 3;
		int numPromotionObject = 3;
		int numPromotionValue = 2;

		for(int i = 0; i < numPromotionUnit; i++) {
			for (int j = 0; j < numPromotionObject; j++) {
				for(int k = 0; k < numPromotionValue; k++) {
					StringBuilder value = new StringBuilder();
					value.append("(");
					value.append(i + 1).append(j + 1).append(k + 1);
					value.append(": ");
//					if (promotionDetailValue[i][j][k] != -1
//							&& promotionDetailValue[i][j][k] != Integer.MAX_VALUE
//							&& promotionDetailValue[i][j][k] != Double.MAX_VALUE) {
					if (BigDecimal.valueOf(promotionDetailValue[i][j][k]).compareTo(BigDecimal.valueOf(-1)) != 0
							&& BigDecimal.valueOf(promotionDetailValue[i][j][k]).compareTo(BigDecimal.valueOf(Integer.MAX_VALUE)) != 0
							&& BigDecimal.valueOf(promotionDetailValue[i][j][k]).compareTo(BigDecimal.valueOf(Double.MAX_VALUE)) != 0) {
						if(k == 0) {
							value.append(String.format("%.2f", promotionDetailValue[i][j][0]));
						} else if(k == 1) {
							value.append(String.format("%.2f", promotionDetailValue[i][j][1]));
						}
					} else {
						value.append(" ");
					}

					value.append(")");

					result.append(value.toString());
					result.append(";");
				}
			}
		}

		return result.substring(0, result.length() - 1);
	}

}
