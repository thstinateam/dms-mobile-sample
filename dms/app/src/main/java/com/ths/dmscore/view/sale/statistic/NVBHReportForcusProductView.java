/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.ForcusProductOfNVBHDTO;
import com.ths.dmscore.dto.view.NVBHReportForcusProductInfoViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Bao cao mat hang trong tam trong module thong ke chung cua NVBH
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class NVBHReportForcusProductView extends BaseFragment implements DMSColSortManager.OnSortChange {

	// khach hang chua phat sinh doanh so
	private static final int ACTION_MENU_NEED_DONE = 3;
	// mat hang trong tam
	private static final int ACTION_MENU_MHTT = 2;
	// thong ke chung
	private static final int ACTION_MENU_GENERAL_STATISTICS = 1;
	// CTTB
	private final int MENU_REPORT_DISPLAY_PROGRESS = 4;
	// number fullDate sale plan
	private TextView tvNumDaySalePlan;
	// num fullDate sold
	private TextView tvNumDaySold;
	// percent sold
	private TextView tvSoldPercent;
	// table list product
	private DMSTableView tbProductList;
	// flag check load data the first
	public boolean isDoneLoadFirst = false;
	// data for screen
	public NVBHReportForcusProductInfoViewDTO screenData = new NVBHReportForcusProductInfoViewDTO();
	String staffId = "";
	String shopId = "";
	String saleTypeCode;

	/**
	 *
	 * get instance
	 *
	 * @param data
	 * @return
	 * @return: ReportForcusProductView
	 * @throws:
	 * @author: HaiTC3
	 * @date: Dec 28, 2012
	 */
	public static NVBHReportForcusProductView getInstance(Bundle data) {
		NVBHReportForcusProductView instance = new NVBHReportForcusProductView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			if (bundle.containsKey(IntentConstants.INTENT_STAFF_ID)) {
				staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);
			} else {
				staffId = String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId());
			}
			if (bundle.containsKey(IntentConstants.INTENT_SHOP_ID)) {
				shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
			} else {
				shopId = String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId());
			}
			if (bundle.containsKey(IntentConstants.INTENT_SALE_TYPE_CODE)) {
				saleTypeCode = bundle
						.getString(IntentConstants.INTENT_SALE_TYPE_CODE);
			} else {
				saleTypeCode = GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritSaleTypeCode();
			}
		}

	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_report_forcus_product_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_BAOCAOCTTT_CHITIET);
		}else{
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BAOCAOMHTT);
		}


		initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_PROGRESS_REPORT_SALES_FOCUS_NV));
		if (!this.isDoneLoadFirst) {
			this.getReportForcusProductInfo();
		}
		return v;
	}

	/**
	 *
	 * init control of view
	 *
	 * @param v
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	private void initView(View v) {
		tvNumDaySalePlan = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySalePlan, PriHashMap.PriControl.NVBH_MHTT_NGAYBANHANGKEHOACH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySalePlanTitle, PriHashMap.PriControl.NVBH_MHTT_NGAYBANHANGKEHOACH);
		tvNumDaySold = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySold, PriHashMap.PriControl.NVBH_MHTT_NGAYBANHANGDAQUA);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvNumDaySoldTitle, PriHashMap.PriControl.NVBH_MHTT_NGAYBANHANGDAQUA);
		tvSoldPercent = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldPercent, PriHashMap.PriControl.NVBH_MHTT_TIENDOCHUAN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvSoldPercentTitle, PriHashMap.PriControl.NVBH_MHTT_TIENDOCHUAN);

		tbProductList = (DMSTableView) v.findViewById(R.id.tbProductList);
		tbProductList.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onResume() {
		this.initHeadermenu();
		if (this.isDoneLoadFirst) {
			renderLayout();
		}
		super.onResume();
	}

	/**
	 *
	 * get report forcus product info
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void getReportForcusProductInfo() {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(
				IntentConstants.INTENT_STAFF_ID,staffId);
		data.putString(
				IntentConstants.INTENT_SHOP_ID,shopId);
		data.putString(
				IntentConstants.INTENT_SALE_TYPE_CODE,saleTypeCode);
		//add sort info
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbProductList.getSortInfo());
		handleViewEvent(data, ActionEventConstant.NVBH_GET_REPORT_FORCUS_PRODUCT_INFO_VIEW, SaleController.getInstance());
	}

	/**
	 *
	 * render layout for screen
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	public void renderLayout() {
		if (!isDoneLoadFirst) {
			tbProductList.clearAllDataAndHeader();
			initHeader();
		}else{
			tbProductList.clearAllData();
		}
		if (screenData != null) {
			tbProductList.setVisibility(View.VISIBLE);
			tvNumDaySalePlan.setText(String.valueOf(screenData.numberDayPlan));
			tvNumDaySold.setText(String.valueOf(screenData.numberDaySold));
			StringUtil.displayPercent(tvSoldPercent, screenData.progressSold);

			// render table
			if (this.screenData.listForcusProduct != null
					&& this.screenData.listForcusProduct.size() > 0) {
				// add list row nomal
				for (int i = 0, size = this.screenData.listForcusProduct.size(); i < size; i++) {
					ForcusProductOfNVBHDTO dto = this.screenData.listForcusProduct
							.get(i);
					NVBHReportForcusProductRow row = new NVBHReportForcusProductRow(
							parent, tbProductList, false);
					row.setClickable(true);
					// update number order for product on view
					row.renderLayoutForRowProductInfo(dto, i + 1,
							this.screenData.progressSold);
					tbProductList.addRow(row);
				}

				// add list row total
				for (int i = 0, size = this.screenData.listTotalForcusProduct
						.size(); i < size; i++) {
					ForcusProductOfNVBHDTO dto = this.screenData.listTotalForcusProduct
							.get(i);
					NVBHReportForcusProductRow row = new NVBHReportForcusProductRow(
							parent, tbProductList, true);
					row.setClickable(true);
					// update number order for product on view
					row.renderLayoutForRowTotal(dto,
							this.screenData.progressSold);
					tbProductList.addRow(row);
				}
			} else {
				// if null
				tbProductList.showNoContentRow();
			}
		} else {
			tbProductList.showNoContentRow();
		}
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				isDoneLoadFirst = false;
				this.getReportForcusProductInfo();
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/**
	 *
	 * init header for menu
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 2, 2013
	 */
	private void initHeadermenu() {
		// enable menu bar
		enableMenuBar(this);
		addMenuItem(
				PriHashMap.PriForm.NVBH_BAOCAOMHTT,
				new MenuTab(R.string.TEXT_HEADER_MENU_CTTB, R.drawable.icon_list_check,
						MENU_REPORT_DISPLAY_PROGRESS, PriHashMap.PriForm.NVBH_BAOCAOCTTB),
//				new MenuTab(R.string.TEXT_HEADER_MENU_NEED_DONE, R.drawable.icon_order,
//						ACTION_MENU_NEED_DONE, PriForm.NVBH_THONGKECHUNG_CANTHUCHIEN),
				new MenuTab(R.string.TEXT_HEADER_MENU_MHTT, R.drawable.icon_list_star,
						ACTION_MENU_MHTT, PriHashMap.PriForm.NVBH_BAOCAOMHTT),
				new MenuTab(R.string.TEXT_HEADER_MENU_GENERAL_STATISTICS, R.drawable.icon_reminders,
						ACTION_MENU_GENERAL_STATISTICS, PriHashMap.PriForm.NVBH_THONGKECHUNG_THONGKE));
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		case ACTION_MENU_GENERAL_STATISTICS:
			handleSwitchFragment(new Bundle(), ActionEventConstant.GO_TO_GENERAL_STATISTICS, SaleController.getInstance());
			break;
		case ACTION_MENU_NEED_DONE:
			handleSwitchFragment(new Bundle(), ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW, SaleController.getInstance());
			break;
		case MENU_REPORT_DISPLAY_PROGRESS:
			handleSwitchFragment(new Bundle(), ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY, SaleController.getInstance());
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
		case ActionEventConstant.NVBH_GET_REPORT_FORCUS_PRODUCT_INFO_VIEW:
			this.screenData = (NVBHReportForcusProductInfoViewDTO) modelEvent
					.getModelData();
			this.renderLayout();
			isDoneLoadFirst = true;
			parent.closeProgressDialog();
			break;

		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent event = modelEvent.getActionEvent();
		switch (event.action) {
		case ActionEventConstant.NVBH_GET_REPORT_FORCUS_PRODUCT_INFO_VIEW:
			parent.closeProgressDialog();
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Khoi tao header
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	private void initHeader() {
		ArrayList<String> lstTitle = new ArrayList<String>();
		lstTitle.add(StringUtil.getString(R.string.TEXT_STT));
		lstTitle.add(StringUtil.getString(R.string.TEXT_HEADER_PRODUCT_CODE));
		lstTitle.add(StringUtil.getString(R.string.TEXT_COLUM_ORDER_NAME));
		lstTitle.add(StringUtil.getString(R.string.TEXT_HEADER_PRODUCT_INDUSTRY));
		lstTitle.add(StringUtil.getString(R.string.TEXT_HEADER_TYPE_FORCUS_PRODUCT));
		if (GlobalInfo.getInstance().isSysShowPrice()) {
			lstTitle.add(StringUtil.getReportUnitTitle(StringUtil.getString(R.string.TEXT_SALES_)));
			lstTitle.add(StringUtil.getString(R.string.TEXT_KH));
			lstTitle.add(StringUtil.getString(R.string.TEXT_ACTION));
			lstTitle.add(StringUtil
					.getString(R.string.TEXT_HEADER_TABLE_REMAIN));
			lstTitle.add(StringUtil.getString(R.string.TEXT_PROGRESS));
		}
		lstTitle.add(StringUtil.getString(R.string.TEXT_QUANTITYS));
		lstTitle.add(StringUtil.getString(R.string.TEXT_KH));
		lstTitle.add(StringUtil.getString(R.string.TEXT_ACTION));
		lstTitle.add(StringUtil
				.getString(R.string.TEXT_HEADER_TABLE_REMAIN));
		lstTitle.add(StringUtil.getString(R.string.TEXT_PROGRESS));

		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
				.addInfo(4, SortActionConstants.CAT)
				.addInfo(5, SortActionConstants.TYPE)
				.build();

		initHeaderTable(tbProductList, new NVBHReportForcusProductRow(
				parent, tbProductList, false), lstTitle,lstSort,this);
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		getReportForcusProductInfo();
	}
}
