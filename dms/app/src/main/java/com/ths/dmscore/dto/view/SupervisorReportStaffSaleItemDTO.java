/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.RPT_STAFF_SALE_TABLE;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.lib.sqllite.db.SHOP_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 *  thong tin mot dong bao cao tien do luy ke nganh
 *  @author: Nguyen Thanh Dung
 *  @version: 1.1
 *  @since: 1.0
 */
public class SupervisorReportStaffSaleItemDTO implements Serializable{
	private static final long serialVersionUID = -3263668975083419011L;
	// children object id
	public long staffId;
	// staff code
	public String staffCode;
	// children object name
	public String staffName;
	// parent object id
	public long staffOwnerId;
	// staff owner code
	public String staffOwnerCode;
	// parent object name
	public String staffOwnerName;

	// children object id
	public long objectId;
	// staff code
	public String objectCode;
	// children object name
	public String objectName;

	//amount
	String concatAmountPlan;
	String concatAmount;
	String concatAmountApproved;
	String concatAmountPending;
	//Quantity
	String concatQuantityPlan;
	String concatQuantity;
	String concatQuantityApproved;
	String concatQuantityPending;

	String concatProductInfoCode;
	String concatProductInfoName;



	// list report focus item
	public ArrayList<SupervisorReportStaffSaleCat> listFocusProductItem = new ArrayList<SupervisorReportStaffSaleCat>();

	public SupervisorReportStaffSaleItemDTO() {
		objectId = 0;
		objectCode ="";
		objectName = "";
		staffId = 0;
		staffName = "";
		staffOwnerId = 0;
		staffOwnerName = "";
		staffCode = "";
		staffOwnerCode = "";
		listFocusProductItem = new ArrayList<SupervisorReportStaffSaleCat>();
		for (int i = 0, size = RPT_STAFF_SALE_TABLE.getInstance().getCAT_LIST().length; i < size; i++) {
			SupervisorReportStaffSaleCat item = new SupervisorReportStaffSaleCat();
			item.focusItemName = RPT_STAFF_SALE_TABLE.getInstance().getCAT_LIST()[i];

			item.amountPlan = 0;
			item.amount = 0;
			item.progressAmount = 0;
			item.quantityPlan = 0;
			item.quantity = 0;
			item.progressQuantity = 0;
			listFocusProductItem.add(item);
		}
	}

	/**
	 * parse data from cursor after query sql success
	*  @author: DungNX
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void parseDataFromCursor(Cursor c, int sysCalUnApproved){
		objectId = CursorUtil.getLong(c, "OBJECT_ID");
		objectCode = CursorUtil.getString(c, "OBJECT_CODE");
		objectName = CursorUtil.getString(c, "OBJECT_NAME");

		concatAmountPlan = CursorUtil.getString(c, "CONCAT_AMOUNT_PLAN");
		concatAmount = CursorUtil.getString(c, "CONCAT_AMOUNT");
		concatAmountApproved = CursorUtil.getString(c, "CONCAT_AMOUNT_APPROVED");
		concatAmountPending = CursorUtil.getString(c, "CONCAT_AMOUNT_PENDING");

		concatQuantityPlan = CursorUtil.getString(c, "CONCAT_QUANTITY_PLAN");
		concatQuantity = CursorUtil.getString(c, "CONCAT_QUANTITY");
		concatQuantityApproved = CursorUtil.getString(c, "CONCAT_QUANTITY_APPROVED");
		concatQuantityPending = CursorUtil.getString(c, "CONCAT_QUANTITY_PENDING");
		// set cau hinh tong hop nhu the nao
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			concatAmount = concatAmountApproved;
			concatQuantity = concatQuantityApproved;
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			concatAmount = concatAmountPending;
			concatQuantity = concatQuantityPending;
		}
		concatProductInfoCode = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_CODE");
		concatProductInfoName = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_NAME");

		String[] amountPlan = concatAmountPlan.split(",");
		String[] amount = concatAmount.split(",");
		String[] quantityPlan = concatQuantityPlan.split(",");
		String[] quantity = concatQuantity.split(",");

		String[] productInfoCode = concatProductInfoCode.split(",");
		String[] productInfoName = concatProductInfoName.split(",");

		if (amountPlan.length == amount.length
				&& amount.length == productInfoCode.length
				&& productInfoCode.length == productInfoName.length) {
			for (int i = 0, size = listFocusProductItem.size(); i < size; i++) {
				SupervisorReportStaffSaleCat item = listFocusProductItem.get(i);
				for(int j=0, n = amountPlan.length; j<n; j++){
					if(item.focusItemName.equals(productInfoCode[j])){
						item.setAmount(Double.valueOf(amountPlan[j]), Double.valueOf(amount[j]));
					}
				}
			}
		}

		if (quantityPlan.length == quantity.length
				&& quantity.length == productInfoCode.length
				&& productInfoCode.length == productInfoName.length) {
			for (int i = 0, size = listFocusProductItem.size(); i < size; i++) {
				SupervisorReportStaffSaleCat item = listFocusProductItem.get(i);
				for(int j=0, n = quantityPlan.length; j<n; j++){
					if(item.focusItemName.equals(productInfoCode[j])){
						item.setQuantity(Long.valueOf(quantityPlan[j]), Long.valueOf(quantity[j]));
					}
				}
			}
		}
	}

	/**
	 * Tong cac amount lai
	*  @author: Nguyen Thanh Dung
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void addAmountFrom(SupervisorReportStaffSaleItemDTO addItem) {
		for (int i = 0, size = listFocusProductItem.size(); i < size; i++) {
			SupervisorReportStaffSaleCat cat = listFocusProductItem.get(i);
			SupervisorReportStaffSaleCat addCat = addItem.listFocusProductItem.get(i);
			cat.amountPlan += addCat.amountPlan;
			cat.amount += addCat.amount;
			cat.quantityPlan += addCat.quantityPlan;
			cat.quantity += addCat.quantity;
			cat.progressAmount = StringUtil.calPercentUsingRound(cat.amountPlan, cat.amount);
			cat.progressQuantity = StringUtil.calPercentUsingRound(cat.quantityPlan, cat.quantity);
		}
	}

	/**
	 * parse data from cursor after query sql success
	*  @author: DungNX
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void parseTBHVDataFromCursor(Cursor c) {
		staffId = CursorUtil.getLong(c, STAFF_TABLE.STAFF_ID, 0);
//		if (c, STAFF_TABLE.STAFF_CODE) >= 0) {
//			staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
//		} else {
//			staffCode = "";
//		}

		staffName = CursorUtil.getString(c, "STAFF_NAME", "");
		staffOwnerId = CursorUtil.getLong(c, SHOP_TABLE.SHOP_ID, 0);
		staffCode = CursorUtil.getString(c, SHOP_TABLE.SHOP_CODE, "");

		String concatAmountPlan;
		String concatAmount;
		String concatProductInfoCode;
		String concatProductInfoName;

		concatAmountPlan = CursorUtil.getString(c, "CONCAT_AMOUNT_PLAN", "");
		concatAmount = CursorUtil.getString(c, "CONCAT_AMOUNT", "");
		concatProductInfoCode = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_CODE", "");
		concatProductInfoName = CursorUtil.getString(c, "CONCAT_PRODUCT_INFO_NAME", "");

		String[] amountPlan = concatAmountPlan.split(",");
		String[] amount = concatAmount.split(",");
		String[] productInfoCode = concatProductInfoCode.split(",");
		String[] productInfoName = concatProductInfoName.split(",");

		if (amountPlan.length == amount.length
				&& amount.length == productInfoCode.length
				&& productInfoCode.length == productInfoName.length) {
			for (int i = 0, size = listFocusProductItem.size(); i < size; i++) {
				SupervisorReportStaffSaleCat item = listFocusProductItem.get(i);
				for(int j=0, n = amountPlan.length; j<n; j++){
					if(item.focusItemName.equals(productInfoCode[j])){
						item.setAmount(Double.valueOf(amountPlan[j]), Double.valueOf(amount[j]));
					}
				}
				// item.parseDataFromCursor(c, item.focusItemName);
			}
		}
//		for (int i = 0, size = listFocusProductItem.size(); i < size; i++) {
//			SupervisorReportStaffSaleCat item = listFocusProductItem.get(i);
//			item.parseDataFromCursor(c, item.focusItemName);
//		}
	}
}
