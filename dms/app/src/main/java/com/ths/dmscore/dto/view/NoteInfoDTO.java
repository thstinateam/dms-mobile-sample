/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.util.CursorUtil;


/**
 *  note information
 *  @author: HaiTC3
 *  @version: 1.0
 *  @since: 1.0
 */
public class NoteInfoDTO implements Serializable{
	private static final long serialVersionUID = 4293452324129117958L;
	// feed back dto
	public FeedBackDTO feedBack;
	// customer DTO
	public CustomerDTO customerDTO;
	
	public NoteInfoDTO(){
		feedBack = new FeedBackDTO();
		customerDTO = new CustomerDTO();
	}
	
	/**
	 * 
	*  init data with cursor
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void initDateWithCursor(Cursor c){
		feedBack.initDataWithCursor(c);
		customerDTO.setCustomerId(Long.parseLong(feedBack.getCustomerId()));
		customerDTO.setCustomerCode(CursorUtil.getString(c, "CUSTOMER_CODE"));
		customerDTO.setCustomerName(CursorUtil.getString(c, "CUSTOMER_NAME"));
	}
	
	/**
	 * 
	*  general json object update feed back
	*  @author: HaiTC3
	*  @return
	*  @return: JSONObject
	*  @throws:
	 */
	public JSONObject generateJsonUpdateFeedBack(){
		JSONObject result = feedBack.generateUpdateFeedbackSql();
		return result;
	}
	
}
