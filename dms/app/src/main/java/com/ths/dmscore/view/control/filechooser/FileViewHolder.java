/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.dto.view.filechooser.FileEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.TextDrawable.ColorGenerator;
import com.ths.dmscore.util.TextDrawable.TextDrawable;
import com.ths.dms.R;

public class FileViewHolder implements CompoundButton.OnCheckedChangeListener,
		View.OnClickListener {
	static ColorGenerator colorGenerator = ColorGenerator.getMATERIAL();
	ImageView ivIconFolder;
	TextView tvFileName;
	CheckBox cbSelected;
	public View view;
	public FileInfo info;
	FileEvent fileChooseEvent;

	public FileViewHolder(Context context, ViewGroup parent,
			FileEvent fileChooseEvent) {
		this.fileChooseEvent = fileChooseEvent;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.item_file, parent, false);
		ivIconFolder = (ImageView) view.findViewById(R.id.ivIconFolder);
		tvFileName = (TextView) view.findViewById(R.id.tvFileName);
		cbSelected = (CheckBox) view.findViewById(R.id.cbSelected);
		cbSelected.setOnCheckedChangeListener(this);
		//tvFileName.setOnClickListener(this);
		view.setOnClickListener(this);
	}

	public void render(FileInfo info) {
		this.info = info;
		switch (info.type) {
		case FileInfo.TYPE_FOLDER:
			ivIconFolder.setImageResource(R.drawable.ic_file_folder);
			ivIconFolder.setVisibility(View.VISIBLE);
			cbSelected.setVisibility(View.GONE);
			break;
		case FileInfo.TYPE_FILE:
			ivIconFolder.setVisibility(View.VISIBLE);
			cbSelected.setVisibility(View.VISIBLE);
			cbSelected.setChecked(info.checked);

			String key = this.info.extension.toUpperCase();
			int color = colorGenerator.getColor(key);
			if (key.length() > 3) {
				key = key.substring(0, 3);
			}
			TextDrawable drawable = TextDrawable.builder()
					.buildRect(key, color);
			ivIconFolder.setImageDrawable(drawable);
			break;
		}
		tvFileName.setText(info.fileName);
	}

	@Override
	public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
		if (info.checked != isCheck) {
			MyLog.d("onCheckedChanged", this.info.fileName + " " + isCheck);
			info.checked = isCheck;
			fileChooseEvent.onFileChoose(this.info);
		}
	}

	@Override
	public void onClick(View view) {
		if (this.view == view) {
			fileChooseEvent.onFileNameSelected(this.info);
		}
	}
}