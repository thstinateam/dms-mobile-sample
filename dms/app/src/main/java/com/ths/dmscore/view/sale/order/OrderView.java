/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.BigDecimalRound;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.SaleOrderPromotionDTO;
import com.ths.dmscore.dto.db.ShopLockDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.ListFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.lib.sqllite.db.ABSTRACT_TABLE;
import com.ths.dmscore.lib.sqllite.db.CalPromotions;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.GlobalUtil.QuantityInfo;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.RoleActivity.PreProcess;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dmscore.view.main.SalesPersonActivity.PopupProblemsListener;
import com.ths.dms.R;

/**
 * Man hinh dat hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
@SuppressLint({ "UseSparseArrays", "InflateParams" })
public class OrderView extends BaseFragment implements OnClickListener,
		OnEventControlListener, OnItemSelectedListener, VinamilkTableListener,
		OnDismissListener, OnFocusChangeListener,OnDateSetListener, OnTimeSetListener, PreProcess {
	// gia tri mac dinh orderType khi tao moi
	public static final String NEW_ORDER_TYPE = "PO";
	public static final String DEFAULT_DELIVERY_CODE = "GN01";

	// bien phan biet tao don hang presale or vansale
	public static final int CREATE_ORDER_FOR_PRESALE = 0;
	public static final int CREATE_ORDER_FOR_VANSALE = 1;
	// gia tri mac dinh orderType khi tao moi
	public static final String TAG = OrderView.class.getName();
	// xem chi tiet san pham
	public static final int ACTION_VIEW_PRODUCT = 0;
	// xem chi tiet khuyen mai
	public static final int ACTION_VIEW_PROMOTION = 1;
	// xoa mat hang ra khoi ds
	public static final int ACTION_DELETE = 2;
	// thay doi thong tin dat hang
	public static final int ACTION_CHANGE_REAL_ORDER = 3;
	// thay doi khuyen mai
	public static final int ACTION_CHANGE_PROMOTION_PRODUCT = 4;
	// xoa sp khuyen mai
	public static final int ACTION_DELETE_PROMOTION = 5;
	// dong y luu dong hang
	public static final int ACTION_SAVE_ORDER_OK = 6;
	// tu choi luu don hang
	public static final int ACTION_CANCEL_SAVE_ORDER = 7;
	// cancel back default
	public static final int ACTION_CANCEL_BACK_DEFAULT = 8;
	// dong y back
	public static final int ACTION_AGRRE_BACK = 9;
	// tu choi back
	public static final int ACTION_CANCEL_BACK = 10;
	// dong y ket thuc vieng tham
	private static final int ACTION_END_VISIT_OK = 11;
	// tu choi ket thuc ghe tham
	private static final int ACTION_END_VISIT_CANCEL = 12;
	// thay doi so luong dat khuyen mai
	public static final int ACTION_CHANGE_REAL_QUANTITY_PROMOTION = 13;
	// Dong y doi mode luu don hang
	public static final int ACTION_CHANGE_MODE_SAVE_ORDER_OK = 14;
	// Cancel doi mode luu don hang
	public static final int ACTION_CANCEL_CHANGE_MODE_SAVE_ORDER = 15;
	// action refresh caculator promotion for promotion order
	public static final int ACTION_CHANGE_PROMOTION_FOR_PROMOTION_ORDER = 16;
	// thay doi tien KM cua don hang
	public static final int ACTION_CHANGE_REAL_MONEY_PROMOTION = 17;
	// thay doi so luong KM cua don hang
	public static final int ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ORDER = 18;
	// cancel thoat man hinh dat hang
	public static final int CONFIRM_EXIT_ORDER_VIEW_CANCEL = 19;
	// dong y thoat man hinh dat hang khi da ket thuc ghe tham
	private static final int CONFIRM_EXIT_ORDER_VIEW_VISIT_OK = 21;
	// dong y tao don tam
	public static final int CONFIRM_DRAFT_ORDER_OK = 22;
	// dong y luu thanh toan
	public static final int CONFIRM_PAYMENT_OK = 23;
	// tu choi luu thanh toan
	public static final int CONFIRM_PAYMENT_CANCEL = 24;
	// dong y
	public static final int CONFIRM_NOT_VALIDATE_PAYMENT_CANCEL = 25;
	//Xem ds KM don hang %, tien cho sp
	public static final int ACTION_VIEW_ORDER_PROMOTION = 26;
	//Doi so suat
	public static final int ACTION_CHANGE_REAL_QUANTITY_RECEIVED = 27;
	// xac nhan thanh toan khong thanh cong
	public static final int CONFIRM_PAYMENT_FAIL = 28;
	// thay doi so luong KM cua sp tich luy
	public static final int ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ACCUMULATION = 29;
	// Xoa KM 1 lan KM tich luy
	public static final int ACTION_DELETE_PROMOTION_ACCUMULATION = 30;
	private static final int CONFIRM_DELETE_ACCUMULATION_OK = 31;
	private static final int CONFIRM_DELETE_ACCUMULATION_CANCEL = 32;
	public static final int ACTION_CHANGE_REAL_QUANTITY_PROMOTION_NEW_OPEN = 33;
	// Dong y tiep tuc luu don co KM tay
	public static final int ACTION_SAVE_PROMOTION_MANUAL = 34;
	public static final int ACTION_CANCEL_PROMOTION_MANUAL = 35;
	//Thong bao don hang co doi so luong sp
	public static final int ACTION_SAVE_PROMOTION_CHANGE_QUANTITY = 36;
	public static final int ACTION_CANCEL_PROMOTION_CHANGE_QUANTITY = 37;
	private static final int ACTION_SYN_PROMOTION_OK = 38;
	private static final int ACTION_SYN_PROMOTION_CANCEL = 39;
	public static final int ACTION_CHANGE_REAL_QUANTITY_PROMOTION_IN_POPUP = 40;
	public static final int ACTION_CHANGE_PRICE = 41;
	public static final int ACTION_CHANGE_PACKAGE_PRICE = 42;
	//  thoat man hinh dat hang khong tao don tam
	public static final int CONFIRM_DRAFT_ORDER_CANCEL = 43;
	public static final int ACTION_PROMOTION_PRODUCT_HAS_NO_PRICE = 44;
	public static final int ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE = 45;

	public static final double NULL_PRICE = -1;

	// bien de kiem tra la don hang moi hay don hay chinh sua
	//private boolean isEditOrder = false;
	// thong tin khach hang
	private HeaderOrderInfo infoOrder;
	// button them hang
	private Button btAddBill;
	// button tinh khuyen mai
	private Button btnCalPromotion;
	// table mat hang ban
	private DMSTableView tbProducts;
	// linearlayout mat hang khuyen mai
	private LinearLayout llPromotionProducts;
	// header ds mat hang khuyen mai c
	private DMSTableView headerPromotionProducts;
	// table mat hang khuyen mai
	private DMSTableView tbPromotionProducts;
	// mat hang khuyen mai
	private TextView tvTextPromotionProduct;
	// text mat hang ban
	private TextView tvTextTitleListProduct;
	// text tong tien
	private TextView tvTotalAmount;
	// luu tren local
	// private Button btSave;
	// luu va chuyen xuong server
	private Button btSaveAndSend;
	// ds muu do uu tien
	private Spinner spinnerPriority;
	// ngay chuyen
	private TextView tvDeliveryDate;// etDate
	// thoi gian chuyen
	private TextView tvDeliveryTime;
	// parent
	SalesPersonActivity parent;
	// dto man hinh
	private OrderViewDTO orderDTO = new OrderViewDTO();
	// row tong cua ds mat hang ban
	private OrderProductRow productTotalRow;
	// row tong cua ds mat khuyen mai
	// private OrderPromotionRow promotionTotalRow;

	private AlertDialog alertRemindDialog = null;
	private TextView tvTextTime;
	// dung de hien thi import code khi sua don hang
	// private TextView tvImportCode;
	boolean firstTimeGetOrder;
	String cashierStaffId;
	String deliveryId;

	// toa do cua nvbh hien tai
	private double lat = -1;
	private double lng = -1;
	// bien dung de kiem tra neu bi duplicate request
	private boolean isRequesting = false;
	// bien dung de kiem tra neu bi duplicate request khi nhan vao nut nhieu lan
	private boolean isClickingPromotionOrder = false;
	// Kiem tra don hang import thanh cong thi hien thi mau binh thuong
	private boolean isCheckStockTotal = true;
	private boolean isDraftOrder = false;
	//Truoc do la don hang sai KM
	private boolean isWrongPromotionOrder = false;
	private boolean showBackPopup;

	// cac control dung cho vansale
	// vung thong tin dung cho vansale
//	private RelativeLayout rlOrderVansale;
	// check box dat hang
	private CheckBox cbOrder;
	// co tra thuong keyshop vao don hang ko
	private CheckBox cbKeyShop;
	// text tong tien valsale
	private TextView tvTotalAmountVansales;
	// button luu
	private Button btSave;
	// button thanh toan
//	private Button btPayment;

	// thong tin dat hang cho presale
	private LinearLayout llOrderPresale;

	// dialog product detail view
	private AlertDialog alertSelectPromotionProduct;
	// dialog product detail view for change num
	private AlertDialog alertChangeNumPromotionProduct;
	// table vinamilk list product change for product have promotion (just
	// display in popup)
	private DMSTableView tbProductPromotionList;
	private DMSTableView tbProductPromotionListChangeNum;
	// promotion programe code
	private TextView tvCTKMCode;
	// promotion programe name
	private TextView tvCTKMName;
	// list product to change
	private ArrayList<OrderDetailViewDTO> listProductChange = new ArrayList<OrderDetailViewDTO>();
	// product selected
	private OrderDetailViewDTO productSelected = new OrderDetailViewDTO();
	// button close
	private Button btClosePopupPrograme;
	// button close
	private Button btClosePopupProgrameChangeNum;
	// button OK
	private Button btOKPopupProgrameChangeNum;

	// bien dung de phan biet tao don hang presale hay vansale
	private int orderType;

	// linearlayout llNewPromotion
	private LinearLayout llPromotionOrder;
	// TextView tvSTTNewPromotion
	// TableLayout tbListromotionForOrder
	private DMSTableView tbListPromotionOrder;
	// linearlayout llNewPromotion
	private LinearLayout llPromotionAccumulation;
	// TextView tvSTTNewPromotion
	// TableLayout tbListromotionForOrder
	private DMSTableView tbListPromotionAccumulation;
	// linearlayout llNewPromotion
	private LinearLayout llPromotionNewOpen;
	// TextView tvSTTNewPromotion
	// TableLayout tbListromotionForOrder
	private DMSTableView tbListPromotionNewOpen;
	// linearlayout llNewPromotion
	private LinearLayout llPromotionKeyShop;
	// table cho tra thuong keyshop
	private DMSTableView tbListPromotionKeyShop;
	// dialog list promotion for promotion type order
	AlertDialog alertOrderPromotionList;
	// dialog list promotion %, discount for a product
	AlertDialog alertDiscountPromotionList;
	// refresh update list promotion for promotion type order
	boolean isRefresh = true;
	// table list promotion for order
	private DMSTableView tbPromotionListView;

	// view don goc
	//boolean isViewPO = false;
	private int indexMenu = -1;

	// kiem tra muc no khach hang vansale
	DebitDTO debitCustomerDTO;
	TextView tvKeyShopPromotion;

	private boolean showPopupChooseProduct = true;
	private boolean isShowPrice;
	private boolean isEditPrice;
	private long routingId = 0;
	private long PRIORITY_NOW_VALUE = 0;
	private boolean isHaveKeyShop = false;
	boolean isRefreshView = false;
	private long lastClickSaveOrderTime = 0;
	private long lastClickPromotionTime = 0;
	private TextView tvMessageCheckQuantityReceive;

	private OnDismissListener dismissDialogPromotionListener = new OnDismissListener() {

		@Override
		public void onDismiss(DialogInterface dialog) {
			isClickingPromotionOrder = false;
		}
	};

	public static OrderView newInstance(Bundle viewData) {
		OrderView f = new OrderView();
		f.setArguments(viewData);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		parent = (SalesPersonActivity) getActivity();
		parent.addPreProcessSwitchMenu(this);
		Bundle bundle = getArguments();
		if (bundle != null) {
			// trang thai xu ly don hang
			orderDTO.stateOrder = bundle.getInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_VIEW_ORDER);

			if(orderDTO.stateOrder == OrderViewDTO.STATE_NEW){
				orderDTO.orderInfo.isVisitPlan = Integer.parseInt(bundle.getString(IntentConstants.INTENT_IS_OR, "0")) == 0 ? 1 : 0;
			} else if (orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_PO){
				orderDTO.orderInfo.fromPOCustomerId = Long.parseLong(bundle.getString(IntentConstants.INTENT_ORDER_ID, "0"));
			} else{
				orderDTO.orderInfo.saleOrderId = Long.parseLong(bundle.getString(IntentConstants.INTENT_ORDER_ID, "0"));
			}

			orderDTO.beginTimeVisit = DateUtils.now();
			orderDTO.customer.setCustomerId(Long.parseLong(bundle
					.getString(IntentConstants.INTENT_CUSTOMER_ID)));
			orderDTO.customer.setCustomerName(bundle
					.getString(IntentConstants.INTENT_CUSTOMER_NAME));
			orderDTO.customer.setAddress(bundle
					.getString(IntentConstants.INTENT_CUSTOMER_ADDRESS));
			orderDTO.customer.setCustomerTypeId(bundle
					.getInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID));

			cashierStaffId = bundle
					.getString(IntentConstants.INTENT_CASHIER_STAFF_ID);
			if(!StringUtil.isNullOrEmpty(cashierStaffId)){
				orderDTO.customer.setCashierStaffID(cashierStaffId);
			}

			deliveryId = bundle
					.getString(IntentConstants.INTENT_DELIVERY_ID);
			routingId  = bundle
					.getLong(IntentConstants.INTENT_ROUTING_ID);
			if(!StringUtil.isNullOrEmpty(deliveryId)){
				orderDTO.customer.setDeliverID(deliveryId);
			}

			String customerCode = bundle
					.getString(IntentConstants.INTENT_CUSTOMER_CODE);
			if (customerCode != null) {
				orderDTO.customer.setCustomerCode(customerCode);
			}

			orderDTO.isBackToRemainOrder = bundle.getBoolean(
					IntentConstants.INTENT_HAS_REMAIN_PRODUCT, false);
			orderDTO.orderInfo.customerId = orderDTO.customer.customerId;
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.layout_order_view, null);
		isShowPrice = GlobalInfo.getInstance().isSysShowPrice();
		isEditPrice = GlobalInfo.getInstance().isSysModifyPrice();
		View view = super.onCreateView(inflater, v, savedInstanceState);
		lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLatitude();
		lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLongtitude();
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DSDONHANG_TAODONHANG);
		}else{
			// nhan vien ban hang
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_TAODONHANG);
		}
		initView(view);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_ORDER));

		if (orderDTO.isFirstInit) {
			//neu dang
			if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW) {
				initData();
				// hien thi ds mat hang ban
				showInfoOnView();

			} else {
				isRefreshView = false;
				if (orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_ORDER
						|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT) {
					this.getOrderForEdit("" + orderDTO.orderInfo.saleOrderId);
				} else if(orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_PO){
					getPO("" + orderDTO.orderInfo.fromPOCustomerId);
				}
			}
			// kiem tra keyshop cua kh
			checkKeyShopCustomer();

			// lay thong tin no cua KH de kiem tra luc tao don vansale
			getCustomerDebit();

			// lay ds muc do uu tien
			getCommonData("ORDER_PIRITY", orderDTO.orderInfo.customerId);

			// Show title
			final ActionLogDTO action = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			//hien thi title cho dung
			if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW) {
				if (action != null && action.objectType.equals("0")
						&& !StringUtil.isNullOrEmpty(action.endTime)) {
					// neu da ghe tham thi chi hien thi title Ghe tham
					parent.initCustomerNameOnMenuVisit(
							action.aCustomer.customerCode,
							action.aCustomer.customerName);
				}
			} else {
				if (action != null && action.objectType.equals("0")
						&& StringUtil.isNullOrEmpty(action.endTime)) {
					// initMenuVisitCustomer(action.aCustomer.customerCode,
					// action.aCustomer.customerName);
					parent.setStatusVisible(StringUtil.getString(R.string.TEXT_VISITING) +" : "
							+ action.aCustomer.customerName + " - "
							+ action.aCustomer.customerCode
							+ " ", View.VISIBLE);
				} else {
					parent.setStatusVisible("", View.GONE);
				}
			}
		}

		// kiem tra stock lock truoc khi cho dat hang
		// gop vansale - presale: mac dinh la ban hang presale
//		if (orderType == CREATE_ORDER_FOR_VANSALE && !isEditOrder && !isViewPO) {
//			if (!checkStockLock() ) {
//				// vansale chua lock kho thi thong bao
//				GlobalUtil.showDialogConfirm(parent,
//						StringUtil.getString(R.string.TEXT_VANLOCK), "OK",
//						null, true);
//				// chuyen sang don pre
//				cbOrder.setChecked(true);
//				onClick(cbOrder);
//			} else if(!checkLockDate()){
//				GlobalUtil.showDialogConfirm(parent,
//						StringUtil.getString(R.string.TEXT_LOCK_DATE), "OK",
//						null, true);
//				// chuyen sang don pre
//				cbOrder.setChecked(true);
//				onClick(cbOrder);
//			}
////			GlobalUtil.popBackStack(parent);
//		}

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		if (orderDTO.isFirstInit) {
		} else {
			layoutOrder();
			// layout ds sp khuyen mai
//			displayPromotion();
//
//			// layout ds sp ban
//			reLayoutBuyProducts();

			// cap nhat thoi gian chuyen
			tvDeliveryDate.setText(orderDTO.deliveryDate);
			tvDeliveryTime.setText(orderDTO.deliveryTime);
			orderDTO.isFirstInit = true;

			// disable button save neu nhu sua don hang
			checkEnableButton();

			// layout combobox
			displayListPriority();
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (parent != null) {
			parent.showTitleDraftOrder();
			parent.removePreProcessSwitchMenu(this);
		}
	}

//	/**
//	 * Khoi tao ds goi y
//	 *
//	 * @author: TruongHN
//	 * @param listSuggest
//	 * @return: void
//	 * @throws:
//	 */
//	private void initSuggestList(ArrayList<RemainProductViewDTO> listDTO) {
//		if (listDTO != null) {
//			for (int i = 0, size = listDTO.size(); i < size; i++) {
//				RemainProductViewDTO dto = listDTO.get(i);
//				SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
//				OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();
//
//				saleOrderDTO.productId = Integer.valueOf(dto.getPRODUCT_ID());
//				saleOrderDTO.price = Long.valueOf(dto.getPRICE());
//				saleOrderDTO.priceId = Long.valueOf(dto.getPRICE_ID());
//				saleOrderDTO.synState = ABSTRACT_TABLE.CREATED_STATUS;
//				saleOrderDTO.updateUser = GlobalInfo.getInstance().getProfile()
//						.getUserData().userCode;
//				saleOrderDTO.isFreeItem = 0;
//				if(dto.hasPromotionProgrameCode) {
//					saleOrderDTO.programeCode = dto.getPROMOTION_PGROGRAME_CODE();
//				}
//				if(!StringUtil.isNullOrEmpty(saleOrderDTO.programeCode)){
//					saleOrderDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
//				}
//				detailViewDTO.convfact = dto.CONVFACT;
//				detailViewDTO.quantity = dto.getHINT_NUMBER();
//				saleOrderDTO.quantity = GlobalUtil.calRealOrder(
//						detailViewDTO.quantity, detailViewDTO.convfact);
//				saleOrderDTO.priceNotVat = dto.PRICE_NOT_VAT;
//				BigDecimal grossWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", dto.GROSS_WEIGHT));
//				saleOrderDTO.totalWeight = grossWeight.multiply(new BigDecimal(saleOrderDTO.quantity)).setScale(3, RoundingMode.HALF_UP).doubleValue();
////				saleOrderDTO.totalWeight = dto.GROSS_WEIGHT * saleOrderDTO.quantity;
//				saleOrderDTO.vat = dto.VAT;
//				if (dto.sign == 4 || dto.sign == 3 || dto.sign == 6) {
//					detailViewDTO.isFocus = 1;
//				}
//				// detailViewDTO.isFocus = dto.getIS_FOCUS();
//				detailViewDTO.productCode = dto.getPRODUCT_CODE();
//				detailViewDTO.productName = dto.getPRODUCT_NAME();
//				detailViewDTO.stock = dto.getQUANTITY_REMAIN();
//				detailViewDTO.grossWeight = dto.GROSS_WEIGHT;
//				detailViewDTO.stock = dto.stock;
//				detailViewDTO.stockActual = dto.stockActual;
//				detailViewDTO.orderDetailDTO = saleOrderDTO;
//
//				orderDTO.listBuyOrders.add(detailViewDTO);
//			}
//		}
//	}

	/**
	 * Khoi tao thong tin man hinh
	 *
	 * @author: TruongHN
	 * @param v
	 * @return: void
	 * @throws:
	 */
	private void initView(View v) {
		// thong tin khach hang
		infoOrder = (HeaderOrderInfo) v.findViewById(R.id.headerOrderInfo);
		btAddBill = (Button) PriUtils.getInstance().findViewById(v, R.id.btAddBill, PriControl.NVBH_BANHANG_DONHANG_THEMHANG);
		PriUtils.getInstance().setOnClickListener(btAddBill, this);
		btnCalPromotion = (Button) PriUtils.getInstance().findViewById(v, R.id.btnCalPromotion, PriControl.NVBH_BANHANG_DONHANG_TINHKHUYENMAI);
		PriUtils.getInstance().setOnClickListener(btnCalPromotion, this);

		// mat hang ban
		tvTextTitleListProduct = (TextView) v
				.findViewById(R.id.tvTextTitleListProduct);
		tbProducts = (DMSTableView) v.findViewById(R.id.tbProducts);
		// Mat hang khuyen mai
		tvTextPromotionProduct = (TextView) v
				.findViewById(R.id.tvTextPromotionProduct);
		llPromotionProducts = (LinearLayout) v
				.findViewById(R.id.llPromotionProducts);
		headerPromotionProducts = (DMSTableView) v
				.findViewById(R.id.headerPromotionProducts);
		headerPromotionProducts.setShowNoContent(false);
		tbPromotionProducts = (DMSTableView) v
				.findViewById(R.id.tbPromotionProducts);
		tbPromotionProducts.setShowNoContent(false);
		// tong tien
		tvTotalAmount = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvTotalAmount, PriControl.NVBH_BANHANG_DONHANG_TONGTIEN);

		btSaveAndSend = (Button) PriUtils.getInstance().findViewById(v, R.id.btSaveAndSend, PriControl.NVBH_BANHANG_DONHANG_LUUVACHUYEN);
		PriUtils.getInstance().setOnClickListener(btSaveAndSend, this);
		PriUtils.getInstance().findViewById(v, R.id.tvTpriority, PriControl.NVBH_BANHANG_DONHANG_MUCDOKHAN);
		spinnerPriority = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spinnerPriority, PriControl.NVBH_BANHANG_DONHANG_MUCDOKHAN);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerPriority, this);
		tvTextTime = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvTextTime, PriControl.NVBH_BANHANG_DONHANG_THOIGIANGIAOHANG);
		tvDeliveryDate = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvDeliveryDate, PriControl.NVBH_BANHANG_DONHANG_THOIGIANGIAOHANG);
		PriUtils.getInstance().setOnClickListener(tvDeliveryDate, this);
		tvDeliveryTime = (TextView) PriUtils.getInstance().findViewById(v, R.id.tvDeliveryTime, PriControl.NVBH_BANHANG_DONHANG_THOIGIANGIAOHANG);
		PriUtils.getInstance().setOnClickListener(tvDeliveryTime, this);

		// thong tin vansale
//		rlOrderVansale = (RelativeLayout) v.findViewById(R.id.rlOrderVansale);
		tvMessageCheckQuantityReceive = (TextView) v.findViewById(R.id.tvMessageCheckQuantityReceive);
		tvTotalAmountVansales = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTotalAmountVansales, PriControl.NVBH_BANHANG_DONHANG_TONGTIEN_VALSALE);

		btSave = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btSave, PriControl.NVBH_BANHANG_DONHANG_LUU);
		PriUtils.getInstance().setOnClickListener(btSave, this);
		cbOrder = (CheckBox) PriUtils.getInstance().findViewById(v, R.id.cbOrder, PriControl.NVBH_BANHANG_DONHANG_DATHANG);
		PriUtils.getInstance().setOnClickListener(cbOrder, this);
		cbKeyShop = (CheckBox) PriUtils.getInstance().findViewById(v, R.id.cbKeyShop, PriControl.NVBH_BANHANG_DONHANG_KEYSHOP);
		PriUtils.getInstance().setOnClickListener(cbKeyShop, this);
		cbKeyShop.setChecked(orderDTO.isKeyShop);
		cbKeyShop.setVisibility(isHaveKeyShop?View.VISIBLE:View.GONE);
		llOrderPresale = (LinearLayout) v.findViewById(R.id.llOrderPresale);

		if (!isShowPrice) {
			tvTotalAmount.setVisibility(View.GONE);
			tvTotalAmountVansales.setVisibility(View.GONE);
		}
		if (orderDTO.isFirstInit) {
			this.orderType = CREATE_ORDER_FOR_PRESALE;
		}
		// Render lai layout tuy thuoc vao loai don hang dan chon
		if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
			PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.INVISIBLE);
			PriUtils.getInstance().setStatus(btSave, PriUtils.INVISIBLE);
			llOrderPresale.setVisibility(View.VISIBLE);
			cbOrder.setChecked(true);
			if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW) {
				if(orderDTO.orderInfo.approved != -1) {
					PriUtils.getInstance().setStatus(cbOrder, PriUtils.DISABLE);
				}
			}
		} else {
			if (isShowPrice) {
				PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.ENABLE);
			}
			PriUtils.getInstance().setStatus(btSave, PriUtils.ENABLE);
			llOrderPresale.setVisibility(View.GONE);
		}

		// thong tin control cho hien thi CTKM cho don hang
		llPromotionOrder = (LinearLayout) v.findViewById(R.id.llPromotionOrder);
		tbListPromotionOrder = (DMSTableView) v.findViewById(R.id.tbListPromotionOrder);
		tbListPromotionOrder.setShowNoContent(false);
		// thong tin control cho hien thi CTKM tich luy
		llPromotionAccumulation = (LinearLayout) v.findViewById(R.id.llPromotionAccumulation);
//		tvSTTPromotionAccumulation = (TextView) v.findViewById(R.id.tvSTTPromotionAccumulation);
		tbListPromotionAccumulation = (DMSTableView) v.findViewById(R.id.tbListPromotionAccumulation);
		tbListPromotionAccumulation.setShowNoContent(false);
		// thong tin control cho hien thi CTKM mo moi
		llPromotionNewOpen = (LinearLayout) v.findViewById(R.id.llPromotionNewOpen);
//		tvSTTPromotionNewOpen = (TextView) v.findViewById(R.id.tvSTTPromotionNewOpen);
		tbListPromotionNewOpen = (DMSTableView) v.findViewById(R.id.tbListPromotionNewOpen);
		tbListPromotionNewOpen.setShowNoContent(false);
		llPromotionKeyShop = (LinearLayout) v.findViewById(R.id.llPromotionKeyShop);
		tbListPromotionKeyShop = (DMSTableView) v.findViewById(R.id.tbListPromotionKeyShop);
		tbListPromotionKeyShop.setShowNoContent(false);
		tvKeyShopPromotion = (TextView) v.findViewById(R.id.tvKeyShopPromotion);
	}

	/**
	 * Khoi tao data khi tao moi don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void initData() {
		// mac dinh them moi
		// SaleOrderDTO orderInfo = new SaleOrderDTO();
		orderDTO.orderInfo.createUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		//Don pre or van deu approve = 0
		orderDTO.orderInfo.approved = 0;
		if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
			orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_PRESALE;
//			orderDTO.orderInfo.approved = 0;// don hang kieu presale thi chua
											// duoc approve
		} else {
			orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_VANSALE;
//			orderDTO.orderInfo.approved = 1;// don hang kieu vansale thi duoc
											// approve luon
		}
		orderDTO.orderInfo.orderSource = 2;
		// orderInfo.shopId = 1;
		orderDTO.orderInfo.shopId = Integer.parseInt(GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		orderDTO.orderInfo.shopCode = GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopCode();
		orderDTO.orderInfo.staffId = GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId();
		//them routing id
		//orderDTO.orderInfo.routingId = ???;
		// orderDTO.orderInfo.state = 1; // chua duyet
		orderDTO.staffId = orderDTO.orderInfo.staffId;

		orderDTO.orderInfo.orderDate = DateUtils.now();
		orderDTO.orderInfo.createDate = orderDTO.orderInfo.orderDate;
//		orderDTO.orderInfo.accountDate = orderDTO.orderInfo.orderDate;
		orderDTO.orderInfo.accountDate = DateUtils.convertFormatDate(orderDTO.orderInfo.orderDate,DateUtils.DATE_FORMAT_NOW, DateUtils.DATE_FORMAT_DATE);
		orderDTO.orderInfo.deliveryDate = orderDTO.orderInfo.orderDate;
		orderDTO.orderInfo.synState = ABSTRACT_TABLE.CREATED_STATUS;
		enableButtonSave(false);
	}

	/**
	 * Tinh khuyen mai
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void calPromotion() {
		//Khong dang thuc hien gi thi moi thuc hien tinh KM,
		//Phong truong hop may cham bi goi nhieu lan
		if(!isRequesting) {
			if (parent != null) {
				parent.showProgressDialog(StringUtil.getString(R.string.loading), true);
			}
			isRequesting = true;
			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_ORDER, orderDTO);
			handleViewEvent(b, ActionEventConstant.GET_PROMOTION_PRODUCT_FROM_SALE_PRODUCT, SaleController.getInstance());
		} else {
			ServerLogger.sendLog("calPromotion",
					"calPromotion lan 2, KH: " + orderDTO.customer.getCustomerCode() , TabletActionLogDTO.LOG_CLIENT);
		}
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: TruongHN
	 * @param listAutoPromotion
	 * @return: void
	 * @throws:
	 */
	private void calculatePromotionProduct(ArrayList<OrderDetailViewDTO> listAutoPromotion) {
		// giu lai cac mat hang khuyen mai chon tu man hinh them hang
		ArrayList<OrderDetailViewDTO> listManualPromotion = new ArrayList<OrderDetailViewDTO>();
		for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
			if (promotion.orderDetailDTO.programeType != PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
				listManualPromotion.add(promotion);

				//Tao ra so suat cho KM tay, huy, doi, tra
				if(promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
						promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
						) {
					SaleOrderPromotionDTO quantityReceived = new SaleOrderPromotionDTO();
					quantityReceived.initHTTMQuantityReceived(promotion);
					orderDTO.productQuantityReceivedList.add(quantityReceived);
				}
			}
		}
		orderDTO.listPromotionOrders.clear();
		orderDTO.listPromotionOrders.addAll(listManualPromotion);
		orderDTO.listPromotionOrders.addAll(listAutoPromotion);
	}

	/**
	 * Them ds khuyen mai vao view
	 *
	 * @author: TruongHN
	 * @param listPromotion
	 * @return: void
	 * @throws:
	 */
	private void displayPromotion() {
		orderDTO.numSKUPromotion = 0;
		orderDTO.promotionTotalWeight = 0;
//		orderDTO.promotionDiscount = 0;
//		orderDTO.promotionMaxDiscount = 0;
		//Reset discount & total
//		orderDTO.orderInfo.discount = 0;
//		orderDTO.orderInfo.discount += orderDTO.discountAmount;
//		orderDTO.orderInfo.total = orderDTO.orderInfo.amount - orderDTO.orderInfo.discount;
//		orderDTO.checkStockTotal();

		headerPromotionProducts.clearAllDataAndHeader();
		if (!headerPromotionProducts.isHeaderExists()) {
			OrderPromotionRow row = new OrderPromotionRow(parent, isShowPrice);
			row.renderHeader();
			headerPromotionProducts.addHeader(row);
		}

		// remove table KM
		tbPromotionProducts.clearAllData();
		tbListPromotionOrder.clearAllData();
		tbListPromotionAccumulation.clearAllData();
		tbListPromotionNewOpen.clearAllData();
		tbListPromotionKeyShop.clearAllData();

		//Reset cac gia tri hien thi
		for(SaleOrderPromotionDTO received: orderDTO.productQuantityReceivedList) {
			received.isShowed = false;
		}

		if (orderDTO.listPromotionOrders.size() > 0) {
			for (int i = 0, size = orderDTO.listPromotionOrders.size(); i < size; i++) {
				// cap nhat dinh dang so luong ton kho thung/hop
				layoutPromotionForProduct(tbPromotionProducts, i);
			}
		}

		// render KM don hang
		if (orderDTO.listPromotionOrder.size() > 0) {
			boolean hasStartPromotionOrder = false;
			for (int i = 0, size = orderDTO.listPromotionOrder.size(); i < size; i++) {
				if(!hasStartPromotionOrder) {
					hasStartPromotionOrder = true;
					//Neu co tinh KM thi moi co, xem lai don hang thi ko co
					if(orderDTO.listPromotionForOrderChange.size() > 0) {
						orderDTO.listPromotionOrder.get(i).showChangePromotionOrder = !orderDTO.isHaveOneOrderPromotion;
					}
				}
				layoutPromotionForOrder(tbListPromotionOrder, orderDTO.listPromotionOrder.get(i), i);
			}

			llPromotionOrder.setVisibility(View.VISIBLE);
		} else {
			llPromotionOrder.setVisibility(View.GONE);
		}

		//KM mo moi
		if (orderDTO.listPromotionNewOpen.size() > 0) {
			for (int i = 0, size = orderDTO.listPromotionNewOpen.size(); i < size; i++) {
//				OrderDetailViewDTO promotion = orderDTO.listPromotionNewOpen.get(i);
//				orderDTO.promotionDiscount += promotion.orderDetailDTO.discountAmount;
				layoutPromotionForOrder(tbListPromotionNewOpen, orderDTO.listPromotionNewOpen.get(i), i);
			}

			llPromotionNewOpen.setVisibility(View.VISIBLE);
		} else {
			llPromotionNewOpen.setVisibility(View.GONE);
		}

		//KM tich luy
		if (orderDTO.listPromotionAccumulation.size() > 0) {
			for (int i = 0, size = orderDTO.listPromotionAccumulation.size(); i < size; i++) {
				OrderDetailViewDTO promotion = orderDTO.listPromotionAccumulation.get(i);

				//KM tich luy neu thieu ton kho thi cho phep xoa KM
				if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT) {
					promotion.allowDeleteAccumulation = ((orderType == CREATE_ORDER_FOR_VANSALE //&& !orderDTO.listLackRemainAccumulationProduct.isEmpty()
							)
							|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT) ? true : false;
				}

				//Tinh so tien tich luy don hang co the nhan duoc
//				if(promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY) {
//					promotion.orderDetailDTO.discountAmount = 0;
//					promotion.rptCttlPay.actuallyPromotion = 0;
//					for(SaleOrderPromoDetailDTO accuPromoDetail: promotion.listPromoDetail) {
//						//Tien chiet khau van chua vuot qua so tien don hang
//						if(orderDTO.orderInfo.discount + orderDTO.promotionDiscount + accuPromoDetail.maxAmountFree < orderDTO.orderInfo.amount) {
//							accuPromoDetail.discountAmount = accuPromoDetail.maxAmountFree;
//						} else {
//							accuPromoDetail.discountAmount = orderDTO.orderInfo.amount - orderDTO.orderInfo.discount - orderDTO.promotionDiscount;
//						}
//						promotion.orderDetailDTO.discountAmount += Math.round(accuPromoDetail.discountAmount);
//						promotion.rptCttlPay.actuallyPromotion += promotion.orderDetailDTO.discountAmount;
//						orderDTO.promotionDiscount += promotion.orderDetailDTO.discountAmount;
//						orderDTO.promotionMaxDiscount += promotion.orderDetailDTO.maxAmountFree;
//					}
//				}

				layoutPromotionForOrder(tbListPromotionAccumulation, orderDTO.listPromotionAccumulation.get(i), i);
			}
			llPromotionAccumulation.setVisibility(View.VISIBLE);
		} else {
			llPromotionAccumulation.setVisibility(View.GONE);
		}
		//render keyshop
		if(!tbListPromotionKeyShop.isHeaderExists())
			tbListPromotionKeyShop.addHeader(new OrderPromotionKeyShopRow(parent, isShowPrice,this));
		if(orderDTO.listPromotionKeyShop.size()>  0){

			boolean isExistKeyShop = false; // kiem tra ton tai tra thuong sp hay tien ko, neu ko thi an grid keyshop
			for(int i = 0, size = orderDTO.listPromotionKeyShop.size(); i < size ; i ++){
				OrderDetailViewDTO detail = orderDTO.listPromotionKeyShop.get(i);
				// truong hop co discount moi render thong tin tra thuong keyshop
				if (detail.orderDetailDTO.getDiscountAmount() > 0
						|| detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
					layoutPromotionForKeyShop(tbListPromotionKeyShop, detail, i);
					isExistKeyShop = true;
				}
			}
			if(isExistKeyShop){
				tvKeyShopPromotion.setVisibility(View.VISIBLE);
				llPromotionKeyShop.setVisibility(View.VISIBLE);
			}else{
				tvKeyShopPromotion.setVisibility(View.GONE);
				llPromotionKeyShop.setVisibility(View.GONE);
			}
		}else{
			tvKeyShopPromotion.setVisibility(View.GONE);
			llPromotionKeyShop.setVisibility(View.GONE);
		}
		//Remove header neu ko co KM
		if (orderDTO.listPromotionOrders.size() == 0
				&& orderDTO.listPromotionOrder.size() == 0
				&& orderDTO.listPromotionAccumulation.size() == 0
				&& orderDTO.listPromotionNewOpen.size() == 0
				) {
			headerPromotionProducts.clearAllDataAndHeader();
//			llPromotionProducts.setVisibility(View.GONE);
			tvTextPromotionProduct.setVisibility(View.GONE);
		} else {
			llPromotionProducts.setVisibility(View.VISIBLE);
			tvTextPromotionProduct.setVisibility(View.VISIBLE);
		}

		//Tinh discount don hang dua tren discount tich luy
//		calculateDiscountFromAccumulationDiscount();

		// tinh so tien VAT sau khi tru khuyen mai
		orderDTO.orderInfo.setTotal(orderDTO.orderInfo.getAmount() - orderDTO.orderInfo.getDiscount() - orderDTO.getPromotionDiscount());

		// update text tong tien
//		initTotalPriveVAT();
	}

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param i
	 */
	public void layoutPromotionForProduct(DMSTableView tbPromotionProducts2,  int i) {
		orderDTO.listPromotionOrders.get(i).remaindStockFormat = GlobalUtil
				.formatNumberProductFlowConvfact(
						orderDTO.listPromotionOrders.get(i).stock,
						orderDTO.listPromotionOrders.get(i).convfact);
		orderDTO.listPromotionOrders.get(i).remaindStockFormatActual = GlobalUtil
				.formatNumberProductFlowConvfact(
						orderDTO.listPromotionOrders.get(i).stockActual,
						orderDTO.listPromotionOrders.get(i).convfact);

		OrderPromotionRow row = new OrderPromotionRow(parent, isShowPrice);
		row.setListner(this);
		OrderDetailViewDTO detailView = orderDTO.listPromotionOrders.get(i);
		detailView.indexParent = i;
		// Tinh so luong khuyen mai
		orderDTO.numSKUPromotion += detailView.orderDetailDTO.quantity;
		double discountRounded = detailView.orderDetailDTO.getDiscountAmount();
		detailView.orderDetailDTO.setDiscountAmount(Math.round(discountRounded));
		BigDecimalRound promotionTotalWeight = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.###", orderDTO.promotionTotalWeight));
		promotionTotalWeight = promotionTotalWeight.add(new BigDecimal(detailView.orderDetailDTO.totalWeight)).setScale();
		orderDTO.promotionTotalWeight = promotionTotalWeight.toDouble();
		detailView.orderType = orderDTO.orderInfo.orderType;
		detailView.getQuantityReceived(orderDTO.productQuantityReceivedList);
		// Sp ko co ton kho hoac tong ton kho > ton kho
		if (orderDTO.stateOrder == OrderViewDTO.STATE_EDIT || orderDTO.stateOrder == OrderViewDTO.STATE_NEW || orderDTO.orderInfo.approved == -1) {
			if(orderType == CREATE_ORDER_FOR_VANSALE) {
				if(detailView.stock <= 0 || detailView.totalOrderQuantity.orderDetailDTO.quantity > detailView.stock) {
					//CTKM thuoc loai co so suat
					if(detailView.isHasQuantityReceived) {
						detailView.isEdited = 2;
					} else {
						detailView.isEdited = 1;
					}
				} else {
					if(detailView.isHasQuantityReceived) {
						if (detailView.quantityReceived != null
								&& detailView.quantityReceived.quantityReceived == detailView.quantityReceived.quantityReceivedMax) {
							detailView.isEdited = detailView.oldIsEdited;
						}
					}
				}
				//Neu dat hang presale thi ko cho phep sua so luong
			}
		}
		row.updateData(detailView, isCheckStockTotal);
		if(orderDTO.stateOrder != OrderViewDTO.STATE_NEW
				&& orderDTO.stateOrder != OrderViewDTO.STATE_EDIT
				&& orderDTO.orderInfo.approved != -1) {
			row.etTotal.setEnabled(false);
		}
		tbPromotionProducts2.addRow(row);
	}

	/**
	 * Layout ds KM cho don hang
	 *
	 * @author: TruongHN
	 * @param listPromotion
	 * @param numberRowCurrent
	 * @return: void
	 * @throws:
	 */
	private void layoutPromotionForOrder(DMSTableView tbListPromotionOrder2, OrderDetailViewDTO detailView, int indexParent) {
		// render row ZV19, ZV20,ZV21
		if (detailView.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_MONEY
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_MONEY
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
			// cap nhat dinh dang so luong ton kho thung/hop
			if (StringUtil.isNullOrEmpty(detailView.remaindStockFormat)) {
				detailView.remaindStockFormat = GlobalUtil
						.formatNumberProductFlowConvfact(detailView.stock,
								detailView.convfact);
			}

			if (StringUtil.isNullOrEmpty(detailView.remaindStockFormatActual)) {
				detailView.remaindStockFormatActual = GlobalUtil
						.formatNumberProductFlowConvfact(detailView.stockActual,
								detailView.convfact);
			}

			OrderPromotionRow row = new OrderPromotionRow(parent, isShowPrice);
			row.setListner(this);
			detailView.indexParent = indexParent;
			// Tinh so luong khuyen mai
			orderDTO.numSKUPromotion += detailView.orderDetailDTO.quantity;
			BigDecimal promotionTotalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orderDTO.promotionTotalWeight));
			promotionTotalWeight = promotionTotalWeight.add(new BigDecimal(detailView.orderDetailDTO.totalWeight)).setScale(3, RoundingMode.HALF_UP);
			orderDTO.promotionTotalWeight = promotionTotalWeight.doubleValue();
			detailView.orderType = orderDTO.orderInfo.orderType;
			detailView.getQuantityReceived(orderDTO.productQuantityReceivedList);

			//KM don hang hay KM mo moi thi cho phep doi so suat neu het kho
			boolean hasSetQuantityReceived = false;
			for (int j = 0, sizepro = detailView.listPromotionForPromo21.size(); j < sizepro; j++) {
				OrderDetailViewDTO promotion = detailView.listPromotionForPromo21.get(j);
				if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
						|| orderDTO.orderInfo.approved == -1) {
					if(orderType == CREATE_ORDER_FOR_VANSALE) {
						if(promotion.stock <= 0 || promotion.totalOrderQuantity.orderDetailDTO.quantity > promotion.stock) {
							if (detailView.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
									|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {
								if(detailView.isHasQuantityReceived) {
									hasSetQuantityReceived = true;
									detailView.isEdited = 2;
								}
							}
						}
					}
				}
			}

			if(!hasSetQuantityReceived) {
				if(detailView.isHasQuantityReceived) {
					if (detailView.quantityReceived != null
							&& detailView.quantityReceived.quantityReceived == detailView.quantityReceived.quantityReceivedMax) {
						detailView.isEdited = detailView.oldIsEdited;
					}
				}
			}

			row.updateData(detailView, isCheckStockTotal);
			if(orderDTO.stateOrder != OrderViewDTO.STATE_NEW
					&& orderDTO.stateOrder != OrderViewDTO.STATE_EDIT
					&& orderDTO.orderInfo.approved != -1) {
				row.etTotal.setEnabled(false);
			}
			tbListPromotionOrder2.addRow(row);
		}

		// KM neu la KM 21 thi hien thi them ds san pham KM./
		if (detailView.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_HEADER_PRODUCT
				|| detailView.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_HEADER_PRODUCT) {

			for (int j = 0, sizepro = detailView.listPromotionForPromo21.size(); j < sizepro; j++) {
				// cap nhat dinh dang so luong ton kho
				// thung/hop
				OrderDetailViewDTO promotion = detailView.listPromotionForPromo21.get(j);
				if (StringUtil.isNullOrEmpty(promotion.remaindStockFormat)) {
					promotion.remaindStockFormat = GlobalUtil
							.formatNumberProductFlowConvfact(promotion.stock,
									promotion.convfact);
				}
				// </HaiTC>
				if (StringUtil.isNullOrEmpty(promotion.remaindStockFormatActual)) {
					promotion.remaindStockFormatActual = GlobalUtil
							.formatNumberProductFlowConvfact(promotion.stockActual,
									promotion.convfact);
				}
				OrderPromotionRow row = new OrderPromotionRow(parent, isShowPrice);
				row.setListner(this);
				promotion.indexParent = indexParent;
				promotion.indexChild = j;

				// Tinh so luong khuyen mai
				orderDTO.numSKUPromotion += promotion.orderDetailDTO.quantity;
				orderDTO.promotionTotalWeight += promotion.orderDetailDTO.totalWeight;
				promotion.orderType = orderDTO.orderInfo.orderType;
				// Sp ko co ton kho hoac tong ton kho > ton kho
				if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
						|| orderDTO.orderInfo.approved == -1) {
					if(orderType == CREATE_ORDER_FOR_VANSALE) {
						if(promotion.stock <= 0 || promotion.totalOrderQuantity.orderDetailDTO.quantity > promotion.stock) {
							//CTKM thuoc loai co so suat
							if(promotion.isHasQuantityReceived) {
								promotion.isEdited = 2;
							} else {
								promotion.isEdited = 1;
							}
						} else {
							if(promotion.isHasQuantityReceived) {
								if (promotion.quantityReceived != null
										&& promotion.quantityReceived.quantityReceived == promotion.quantityReceived.quantityReceivedMax) {
									promotion.isEdited = promotion.oldIsEdited;
								}
							}
						}
						//Neu dat hang presale thi ko cho phep sua so luong
					}
				}

				//KM tich luy thi ko cho phep duoc sua so luong bang tay
				if (promotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
					promotion.isEdited = 0;
				}

				promotion.getQuantityReceived(orderDTO.productQuantityReceivedList);
				row.updateData(promotion, isCheckStockTotal);
				if(orderDTO.stateOrder != OrderViewDTO.STATE_NEW
						&& orderDTO.stateOrder != OrderViewDTO.STATE_EDIT
						&& orderDTO.orderInfo.approved != -1) {
					row.etTotal.setEnabled(false);
				}
				tbListPromotionOrder2.addRow(row);
			}
		}
	}

	/**
	 * Lay thong tin don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param orderId
	 */
	private void getOrderForEdit(String orderId) {
		if (parent != null) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading), true);
		}
		boolean isGenPrice = orderDTO.stateOrder == OrderViewDTO.STATE_EDIT;

		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER, orderId);
		bundle.putLong(IntentConstants.INTENT_CUSTOMER_ID, orderDTO.customer.customerId);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		bundle.putString(IntentConstants.INTENT_STAFF_ID, ""+GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		bundle.putBoolean(IntentConstants.INTENT_IS_NEED_GEN_PRICE, isGenPrice);
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, orderDTO.customer.getCustomerTypeId());

		handleViewEvent(bundle, ActionEventConstant.GET_ORDER_FOR_EDIT, SaleController.getInstance());
	}

	/**
	 * lay don goc
	 * @author: DungNX
	 * @param fromPOCustomerID
	 * @return: void
	 * @throws:
	*/
	private void getPO(String fromPOCustomerID) {
		if (parent != null) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading), true);
		}
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER, fromPOCustomerID);
		bundle.putLong(IntentConstants.INTENT_CUSTOMER_ID, orderDTO.customer.customerId);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		bundle.putString(IntentConstants.INTENT_STAFF_ID, ""+GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		handleViewEvent(bundle, ActionEventConstant.GET_PO, SaleController.getInstance());
	}

	/**
	 * Lay cac thong tin chung tren man hinh
	 *
	 * @author: TruongHN
	 * @param code
	 * @return: void
	 * @throws:
	 */
	private void getCommonData(String code, long customerId) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(customerId));
		bundle.putString(IntentConstants.INTENT_PRIORITY_CODE, code);
		// bundle.putString(IntentConstants.INTENT_PRIORITY_CODE,code);
		// bundle.putString(IntentConstants.INTENT_PRIORITY_CODE,code);
		handleViewEvent(bundle, ActionEventConstant.GET_COMMON_DATA_ORDER, SaleController.getInstance());
	}

	/**
	 * Hien thi thong tin mat hang ban tren view
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void showInfoOnView() {
		// orderDTO.orderInfo.amount = 0;
		// orderDTO.numSKU = 0;
		// Tinh toan so luong ton kho tong cua tong mat hang
		orderDTO.checkStockTotal();

		// thong tin khach hang
		infoOrder.updateData(orderDTO);
		// thong tin ma mat hang
//		if (headerProducts.getHeaderCount() == 0) {
//			// thong tin mat hang ban
//			headerProducts.addColumns(
//					TableDefineContanst.ORDER_PRODUCT_HEADER_WIDTH,
//					TableDefineContanst.ORDER_PRODUCT_HEADER_TITLE,
//					ImageUtil.getColor(R.color.BLACK),
//					ImageUtil.getColor(R.color.TABLE_HEADER_BG));
//		}

		tbProducts.clearAllData();
		if (!tbProducts.isHeaderExists()) {
			tbProducts.addHeader(new OrderProductRow(parent, isShowPrice, isEditPrice));
		}

		// ds mat hang ban
		for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
			// <HaiTC> cap nhat dinh dang so luong ton kho thung/hop
			if (StringUtil
					.isNullOrEmpty(orderDTO.listBuyOrders.get(i).remaindStockFormat)) {
				orderDTO.listBuyOrders.get(i).remaindStockFormat = GlobalUtil
						.formatNumberProductFlowConvfact(
								orderDTO.listBuyOrders.get(i).stock,
								orderDTO.listBuyOrders.get(i).convfact);
			}
			// </HaiTC>

			if (StringUtil
					.isNullOrEmpty(orderDTO.listBuyOrders.get(i).remaindStockFormatActual)) {
				orderDTO.listBuyOrders.get(i).remaindStockFormatActual = GlobalUtil
						.formatNumberProductFlowConvfact(
								orderDTO.listBuyOrders.get(i).stockActual,
								orderDTO.listBuyOrders.get(i).convfact);
			}

			OrderProductRow row = new OrderProductRow(parent, isShowPrice, isEditPrice);
			OrderDetailViewDTO dto = orderDTO.listBuyOrders.get(i);
			dto.indexParent = i;
			row.setListner(this);

			orderDTO.numSKU += dto.orderDetailDTO.quantity;
			BigDecimal totalWeightOrder = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orderDTO.orderInfo.totalWeight));
			totalWeightOrder = totalWeightOrder.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", dto.orderDetailDTO.totalWeight)));
//			totalWeightOder = totalWeightOder.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", selectedDetail.orderDetailDTO.totalWeight)));
			orderDTO.orderInfo.totalWeight = totalWeightOrder.doubleValue();
			dto.orderDetailDTO.setAmount(dto.orderDetailDTO.calculateAmountProduct());
			//sum to total orderInfo
			orderDTO.orderInfo.addAmount(dto.orderDetailDTO.getAmount());
			dto.quantity = dto.quantityProductStr();
			row.updateData(dto, isCheckStockTotal);
			tbProducts.addRow(row);
		}
		// update row tong
		productTotalRow = new OrderProductRow(parent, isShowPrice, isEditPrice);
		productTotalRow.renderTotalRow(orderDTO.orderInfo.getAmount(), orderDTO.numSKU);
		tbProducts.addRow(productTotalRow);

//		orderDTO.orderInfo.total = orderDTO.orderInfo.amount - orderDTO.orderInfo.discount;
		// update text tong tien
		initTotalPriveVAT();

		llPromotionProducts.setVisibility(View.GONE);
		tvTextPromotionProduct.setVisibility(View.GONE);
		tvTextTitleListProduct.setVisibility(View.VISIBLE);
		setPriority(orderDTO.orderInfo.priority);
		tvDeliveryDate.setText(orderDTO.deliveryDate);
		tvDeliveryTime.setText(orderDTO.deliveryTime);
	}

	/**
	 * Layout ds mat hang ban
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void reLayoutBuyProducts() {
		tbProducts.clearAllDataAndHeader();
		orderDTO.numSKU = 0;
		orderDTO.orderInfo.totalWeight = 0;
//		orderDTO.orderInfo.discount = 0;
		// thong tin khach hang
		infoOrder.updateData(orderDTO);

		if(!tbProducts.isHeaderExists()){
			tbProducts.addHeader(new OrderProductRow(parent, isShowPrice, isEditPrice));
		}

//		tbProducts.removeAllViews();
		// ds mat hang ban
		int i = 0;
		while (i < orderDTO.listBuyOrders.size()) {
			OrderDetailViewDTO dto = orderDTO.listBuyOrders.get(i);
			// <HaiTC> cap nhat dinh dang so luong ton kho thung/hop
//			if (StringUtil
//					.isNullOrEmpty(orderDTO.listBuyOrders.get(i).remaindStockFormat)) {
			orderDTO.listBuyOrders.get(i).remaindStockFormat = GlobalUtil.formatNumberProductFlowConvfact(orderDTO.listBuyOrders.get(i).stock,orderDTO.listBuyOrders.get(i).convfact);
			orderDTO.listBuyOrders.get(i).remaindStockFormatActual = GlobalUtil.formatNumberProductFlowConvfact(orderDTO.listBuyOrders.get(i).stockActual,orderDTO.listBuyOrders.get(i).convfact);
			//}
			// </HaiTC>
			if (dto.orderDetailDTO.quantity == 0) {
				orderDTO.listBuyOrders.remove(i);
			} else {
				OrderProductRow row = new OrderProductRow(parent, isShowPrice, isEditPrice);
				dto.indexParent = i;
				orderDTO.numSKU += dto.orderDetailDTO.quantity;
				BigDecimal totalWeightOrder = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orderDTO.orderInfo.totalWeight));
				totalWeightOrder = totalWeightOrder.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", dto.orderDetailDTO.totalWeight)));

				orderDTO.orderInfo.totalWeight = totalWeightOrder.doubleValue();
//				orderDTO.orderInfo.discount += dto.orderDetailDTO.discountAmount;
				dto.quantity = dto.quantityProductStr();
				row.setListner(this);
				row.updateData(dto, isCheckStockTotal);
				if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW
						&& orderDTO.stateOrder != OrderViewDTO.STATE_EDIT
						&& orderDTO.orderInfo.approved != -1) {
					row.setIsEdit(false);
				}
				tbProducts.addRow(row);
				i++;
			}
		}

		// update row tong
		productTotalRow = new OrderProductRow(parent, isShowPrice, isEditPrice);
		productTotalRow.renderTotalRow(orderDTO.orderInfo.getAmount(), orderDTO.numSKU);
		tbProducts.addRow(productTotalRow);

		// update text tong tien
//		initTotalPriveVAT(orderDTO.orderInfo.total);
		// Dung cho kiem tra ton kho, move ham hien thi KM len truoc ham reLayoutProduct
		//Remove header neu ko co KM
//		if (orderDTO.listPromotionOrders.size() == 0
//				&& orderDTO.listPromotionOrder.size() == 0
//				&& orderDTO.listPromotionAccumulation.size() == 0) {
//			headerPromotionProducts.removeAllColumns();
//			llPromotionProducts.setVisibility(View.GONE);
//			tvTextPromotionProduct.setVisibility(View.GONE);
//		} else {
//			llPromotionProducts.setVisibility(View.VISIBLE);
//			tvTextPromotionProduct.setVisibility(View.VISIBLE);
//		}
		tvTextTitleListProduct.setVisibility(View.VISIBLE);

		setPriority(orderDTO.orderInfo.priority);
		tvDeliveryDate.setText(orderDTO.deliveryDate);
		tvDeliveryTime.setText(orderDTO.deliveryTime);
		// hien thi import code (neu co)
		// if (!StringUtil.isNullOrEmpty(orderDTO.orderInfo.importCode)) {
		// tvImportCode.setText(StringUtil.getString(R.string.TEXT_REASON) + " "
		// + orderDTO.orderInfo.importCode.trim());
		// tvImportCode.setVisibility(View.VISIBLE);
		// } else {
		// tvImportCode.setVisibility(View.GONE);
		// }

		// tinh so tien VAT sau khi tru khuyen mai
		orderDTO.orderInfo.setTotal(orderDTO.orderInfo.getAmount() - orderDTO.orderInfo.getDiscount() - orderDTO.getPromotionDiscount());

		// update text tong tien
//		initTotalPriveVAT();
	}

	private void initTotalPriveVAT() {
		//Tinh discount don hang dua tren discount tich luy
//		calculateDiscountFromAccumulationDiscount();

		// tinh so tien VAT sau khi tru khuyen mai

		orderDTO.orderInfo.setTotal(orderDTO.orderInfo.getAmount() - orderDTO.orderInfo.getDiscount() - orderDTO.getPromotionDiscount());
		// update text tong tien
		SpannableObject obj = new SpannableObject();
		obj.addSpan(StringUtil.getString(R.string.TEXT_TOTAL_MONEY_PLUS_VAT)
				+ Constants.STR_SPACE, ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);
		obj.addSpan(StringUtil.formatNumber(orderDTO.orderInfo.getTotal()),
				ImageUtil.getColor(R.color.RED), android.graphics.Typeface.BOLD);
		StringUtil.display(tvTotalAmount, obj.getSpan());
		StringUtil.display(tvTotalAmountVansales, obj.getSpan());
	}

	/**
	 * Hien thi layout don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	private void layoutOrder() {
		orderDTO.calculateDiscount();

		orderDTO.checkStockTotal();
		//Dung cho don Vansale: check KM tich luy du hay ko?
		orderDTO.checkStockTotalSuccess();
		//Check kho cho sp tich luy don vansale
		if (orderType == CREATE_ORDER_FOR_VANSALE && orderDTO.stateOrder == OrderViewDTO.STATE_NEW) {
			orderDTO.checkStockTotalAccumulationProduct3();
//		if (orderType == CREATE_ORDER_FOR_VANSALE) {
//			orderDTO.checkStockTotalAccumulationProduct();
		}
		reLayoutBuyProducts();
		displayPromotion();
		initTotalPriveVAT();
	}

	@Override
	public void onClick(View v) {
		if (v == btAddBill) {
			// dong ban phim
			GlobalUtil.forceHideKeyboard(parent);
			if (orderDTO != null && orderDTO.orderInfo != null) {
				if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
						|| orderDTO.orderInfo.approved == -1) {
					// chuyen sang man hinh them moi don hang
					gotoFindProductAddToListOrder();
				}
			}
//		} else if(v == btPayment){
//			// show popup thanh toan
//				showPopUpPayment();
		} else if (v == btSaveAndSend || v == btSave) {
			//prevent double click
			if (SystemClock.elapsedRealtime() - lastClickSaveOrderTime < Constants.TIME_CHECK_DOUBLE_CLICK){
				//he thong dang xu ly nhieu
				parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
	            return;
	        }

			lastClickSaveOrderTime = SystemClock.elapsedRealtime();

			try {
//				if(orderType == CREATE_ORDER_FOR_PRESALE) {
					orderDTO.checkQuantityReceivedFull();
					String message = "";
					if(orderDTO.mapQuantityReceiveMissing.size() > 0) {
						message = orderDTO.generateQuantityReceiveMissingMessage();

						if(!message.equals(tvMessageCheckQuantityReceive.getText())) {
							message = message + StringUtil.getString(R.string.TEXT_RECALCULATE_PROMOTION);

							GlobalUtil.showDialogConfirm(parent, message,
									StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
									false);

							return;
						}
					}
//				}
			} catch (Exception e) {
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}

			if (!isClickingPromotionOrder || !GlobalInfo.getInstance().isValidateMultiClickOrder()) {
				isClickingPromotionOrder = true;

				if((orderDTO.stateOrder == OrderViewDTO.STATE_NEW || orderDTO.orderInfo.approved == -1)
						&& !checkDistanceProcessAfterVisit()){
					ActionLogDTO al = GlobalInfo.getInstance().getProfile()
							.getActionLogVisitCustomer();
					String mess = StringUtil
							.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_1)
							+ " "
							+ al.aCustomer.customerCode
							+ " - "
							+ al.aCustomer.customerName
							+ " "
							+ StringUtil
									.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_2);

					GlobalUtil.showDialogConfirm(parent, mess,
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
							false);

					//cancel request promition order
					isClickingPromotionOrder = false;
					return;
				}

				String listProgrameManual = orderDTO.hasProgramePromotionManual();
				String listProductChangeQuantity = orderDTO.hasPromotionProductChangeQuantity();

				if (orderType == CREATE_ORDER_FOR_VANSALE
						&& !orderDTO.checkStockTotalSuccess()) {
					// Truong hop vansale khong du hang thi ko cho dat hang
					Collection<OrderDetailViewDTO> temp = (Collection<OrderDetailViewDTO>) orderDTO.listLackRemainProduct
							.values();
					ArrayList<String> listProductCode = new ArrayList<String>();
					for (OrderDetailViewDTO detail : temp) {
						listProductCode.add(detail.productCode);
					}

					GlobalUtil.showDialogConfirm(this, this.parent, StringUtil
							.getStringAndReplace(
									R.string.TEXT_CONFIRM_CHANGE_ORDER_MODE,
									TextUtils.join(", ", listProductCode)),
							StringUtil.getString(R.string.TEXT_AGREE),
							ACTION_CHANGE_MODE_SAVE_ORDER_OK, StringUtil
									.getString(R.string.TEXT_DENY),
							ACTION_CANCEL_CHANGE_MODE_SAVE_ORDER, null);

					//cancel request promition order
					isClickingPromotionOrder = false;
				} else if(orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.hasProgramePromotionCancelChangeReturn()) {
					// truong hop vansale co KM huy doi tra thi thong bao
					// khong cho tao don hang
					GlobalUtil.showDialogConfirm(parent, StringUtil.getString(R.string.TEXT_CONFIRM_ORDER_HAS_PROMOTION_CANCEL_CHANGE_RETURN),
							StringUtil.getString(R.string.TEXT_AGREE), null, true);

					//cancel request promition order
					isClickingPromotionOrder = false;
				} else if(!StringUtil.isNullOrEmpty(listProductChangeQuantity)) {
					// truong hop co KM huy, doi ,tra, trung bay, km tay thi canh bao
					Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil
							.getStringAndReplace(
									R.string.TEXT_CONFIRM_ORDER_HAS_PROMOTION_CHANGE_QUANTITY,
									listProductChangeQuantity),
									StringUtil.getString(R.string.TEXT_AGREE),
									ACTION_SAVE_PROMOTION_CHANGE_QUANTITY, StringUtil
									.getString(R.string.TEXT_DENY),
									ACTION_CANCEL_PROMOTION_CHANGE_QUANTITY, null);
					if (dialog != null) {
						dialog.setOnDismissListener(dismissDialogPromotionListener);
					}
				} else if(!StringUtil.isNullOrEmpty(listProgrameManual)) {
					// truong hop co KM huy, doi ,tra, trung bay, km tay thi canh bao
					Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil
							.getStringAndReplace(
									R.string.TEXT_CONFIRM_ORDER_HAS_PROMOTION_MANUAL,
									listProgrameManual),
									StringUtil.getString(R.string.TEXT_AGREE),
									ACTION_SAVE_PROMOTION_MANUAL, StringUtil
									.getString(R.string.TEXT_DENY),
									ACTION_CANCEL_PROMOTION_MANUAL, null);
					if (dialog != null) {
						dialog.setOnDismissListener(dismissDialogPromotionListener);
					}
				} else {
					// kiem tra va luu don hang
					//Thong bao neu co sp ko co gia, truong hop khong show gia thi khong kiem tra sp KM khong co gia
					String listMissingPrice = getListPromotionProductMissingPrice();
					if(!StringUtil.isNullOrEmpty(listMissingPrice)) {
						Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil.getStringAndReplace(
								R.string.TEXT_PROMOTION_PRODUCT_HAS_NO_PRICE,
								listMissingPrice),
										StringUtil.getString(R.string.TEXT_AGREE),
										ACTION_PROMOTION_PRODUCT_HAS_NO_PRICE, StringUtil
										.getString(R.string.TEXT_DENY),
										ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE, null);
						if (dialog != null) {
							dialog.setOnDismissListener(dismissDialogPromotionListener);
						}
					} else {
						processSaveOrder();
					}
				}
			} else {
				//he thong dang xu ly nhieu
				parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
			}
		} else if (v == btnCalPromotion) {
			//prevent double click
			if (SystemClock.elapsedRealtime() - lastClickPromotionTime < Constants.TIME_CHECK_DOUBLE_CLICK) {
				//he thong dang xu ly nhieu
				parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
	            return;
	        }

			lastClickPromotionTime  = SystemClock.elapsedRealtime();

			if (!isClickingPromotionOrder || !GlobalInfo.getInstance().isValidateMultiClickOrder()) {
				isClickingPromotionOrder = true;
				
				//Remove KM tay ko co so luong
				ArrayList<OrderDetailViewDTO> tempList = new ArrayList<OrderDetailViewDTO>();
				for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
					if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL
							|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION
							|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL
							|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE
							|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
							) {
						if((long)promotion.orderDetailDTO.quantity == 0) {
							tempList.add(promotion);
						}
					} 
				}
				
				orderDTO.listPromotionOrders.removeAll(tempList);
				
				// tin toan khuyen mai tu dong
				processCalPromotion();
				// khi tinh lai khuyen mai thi reset lai
				realOrderMap = null;
			} else {
				//he thong dang xu ly nhieu
				parent.showToastMessage(StringUtil.getString(R.string.TEXT_MULTI_TASK));
			}
		} else if (v == tvDeliveryDate) {
			parent.showDatePickerDialog(tvDeliveryDate.getText().toString(), true, this);
		} else if (v == tvDeliveryTime) {
			parent.showTimePickerDialog(this);
		} else if (v.getId() == R.id.btClose) {// TamPQ dong dialog nhac nho
			this.alertRemindDialog.dismiss();
			// chuyen sang man hinh thong tin khach hang neu don truoc do ko phai la don sai KM
			if(isWrongPromotionOrder) {
				GlobalUtil.popBackStack(this.parent);
			} else {
				handleAfterCreateOrder(orderDTO.customer.getCustomerId());
			}
		} else if (v == btClosePopupPrograme) {
			if (alertSelectPromotionProduct.isShowing()) {
				alertSelectPromotionProduct.dismiss();
			}
		} else if (v == btClosePopupProgrameChangeNum) {
			if (alertChangeNumPromotionProduct.isShowing()) {
				parent.hideKeyboardCustom();
				alertChangeNumPromotionProduct.dismiss();
			}
			//Cap nhat lai gia tri ban dau
			for(OrderDetailViewDTO promotion: listProductChange) {
				if(oldRealOrderMap.containsKey(promotion.orderDetailDTO.productId)) {
					int oldQuantity = oldRealOrderMap.get(promotion.orderDetailDTO.productId);
					promotion.orderDetailDTO.quantity = oldQuantity;
				}
			}
			orderDTO.checkStockTotal();
		} else if (v == btOKPopupProgrameChangeNum) {
			if (alertChangeNumPromotionProduct.isShowing()) {
				parent.hideKeyboardCustom();
				alertChangeNumPromotionProduct.dismiss();
			}
			processChooseNewPromotion();
		} else if (v == cbOrder) {
			// dat hang cho vansale
			boolean allowCalPromotion = true;
			if (cbOrder.isChecked()) {
				PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.INVISIBLE);
				PriUtils.getInstance().setStatus(btSave, PriUtils.INVISIBLE);
//				btPayment.setVisibility(View.GONE);
				llOrderPresale.setVisibility(View.VISIBLE);
				this.orderType = CREATE_ORDER_FOR_PRESALE;
			} else {
//				if (!GlobalInfo.getInstance().isFirstSynData()){
//					// chua dong bo dau ngay valsale
//					GlobalUtil.showDialogConfirm(parent,
//							StringUtil.getString(R.string.TEXT_FIST_SYN_DATA_VALSALE), "OK",
//							null, true);
//					// chuyen sang don pre
//					cbOrder.setChecked(true);
//					allowCalPromotion = false;
//				} else
				if (!checkLockDateValsale()) {
					// vansale da chot ngay thi thong bao
					GlobalUtil.showDialogConfirm(parent,
							StringUtil.getString(R.string.TEXT_LOCK_DATE_VALSALE), "OK",
							null, true);
					// chuyen sang don pre
					cbOrder.setChecked(true);
					allowCalPromotion = false;
				}
				//bo truong hop ngay chot khong trung voi ngay tao don vansale
				/*else if(!checkLockDate()){
					GlobalUtil.showDialogConfirm(parent,
							StringUtil.getString(R.string.TEXT_LOCK_DATE_CONFIRM), "OK",
							null, true);
					cbOrder.setChecked(true);
					allowCalPromotion = false;
				} */else {
					if (isShowPrice) {
						PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.ENABLE);
					}
					PriUtils.getInstance().setStatus(btSave, PriUtils.ENABLE);
//					btPayment.setVisibility(View.VISIBLE);
					llOrderPresale.setVisibility(View.GONE);
					this.orderType = CREATE_ORDER_FOR_VANSALE;
				}
			}

			// order type for calculate promotion
			if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
				orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_PRESALE;
				//orderDTO.orderInfo.approved = 0;// don hang kieu presale thi
												// chua duoc approve
			} else {
				orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_VANSALE;
				//orderDTO.orderInfo.approved = 1;// don hang kieu vansale thi
												// duoc approve luon
			}

			if (allowCalPromotion) {
				// Neu da tinh KM roi thi khi chon checkbox thi tinh KM lai
				// bug 0002253
				//			if (orderDTO.isEnableButton) {
				showPopupChooseProduct = false;
				processCalPromotion();
				realOrderMap = new HashMap<Integer, Integer>();
			}

//			}
		}
		else if (v == btClose) {
			if (alertProductDetail != null) {
				alertProductDetail.dismiss();
			}
			handleAfterCreateOrder(orderDTO.customer.getCustomerId());
		} else if (v == btPaidDebt) {
			//format text
			formatSetextPayment(etThanhToan);
			formatSetextPayment(etChietKhau);

			// thuc hien gach no
			calRemain();

			if(chietKhau <= 0 && thanhToan <= 0){
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_PAYMENT_NOT_VALIDATE_), StringUtil.getString(R.string.TEXT_AGREE),
						CONFIRM_NOT_VALIDATE_PAYMENT_CANCEL, null, true);
			} else if (orderDTO.debitDetailDto.remain < chietKhau + thanhToan){
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_PAYMENT_NOT_VALIDATE), StringUtil.getString(R.string.TEXT_AGREE),
						CONFIRM_NOT_VALIDATE_PAYMENT_CANCEL, null, true);
			} else {
				// confirm thuc hien luu xuong db
				GlobalUtil.showDialogConfirm(this, this.parent, StringUtil.getString(R.string.TEXT_PAYMENT_CONFIRM), StringUtil.getString(R.string.TEXT_AGREE),
						CONFIRM_PAYMENT_OK, StringUtil.getString(R.string.TEXT_DENY), CONFIRM_PAYMENT_CANCEL, null);
			}
		} else if (v == etChietKhau || v == etThanhToan) {
			this.onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM_UPPER_VIEW,
					etChietKhau, null);
		}else if(v == cbKeyShop){
			orderDTO.isKeyShop = cbKeyShop.isChecked();
			processCalPromotion();
			// khi tinh lai khuyen mai thi reset lai
			realOrderMap = null;
		}
		else {
			super.onClick(v);
		}
	}

	/**
	 * Tinh toan khuyen mai tu dong
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void processCalPromotion() {
		changeQuantityProductByConfig();

		// dong ban phim
		GlobalUtil.forceHideKeyboard(parent);
		// mac dinh button "Luu va Chuyen"
		btSaveAndSend.setText(StringUtil.getString(R.string.TEXT_SAVE_SEND));
		//Reset cac gia tri lien quan KM cu~
		resetValueBeforeCalculatePromotion();
		orderDTO.isCalculatePromotion = true;
		// kiem tra neu mat hang bi xoa het
		if (orderDTO.listBuyOrders.size() == 0) {
//			calculatePromotionProduct(new ArrayList<OrderDetailViewDTO>());
//			//Layout ds sp KM
//			layoutOrder();
//			displayPromotion();
			ArrayList<OrderDetailViewDTO> listPromotionAuto = new ArrayList<OrderDetailViewDTO>();
			for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
				// Ds KM co KM tu dong thi bat buoc phai co mat
				// hang ban
				if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
					listPromotionAuto.add(promotion);
				}
			}
			orderDTO.listPromotionOrders.removeAll(listPromotionAuto);

			// neu so luong sp khuyen mai > 0 & ton tai sp CTTB --> enable
			// button luu
			boolean errorInput = false;
			if (orderDTO.listPromotionOrders.size() == 0) {
				errorInput = true;
//			} else {
//				for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
//					// Ds KM co KM tu dong thi bat buoc phai co mat
//					// hang ban
//					if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
//						errorInput = true;
//						break;
//					}
//				}
			}

			if (errorInput) {
				if (showPopupChooseProduct) {
					parent.showDialog(StringUtil
							.getString(R.string.ERROR_CHOOSE_PRODUCT));
				}
				//cancel request promition order
				isClickingPromotionOrder = false;
			} else {
				recalculateAmountOrder();
				// tinh khuyen mai
				calPromotion();
				enableButtonSave(true);
			}
		} else {
			// validate don hang co mat hang ban thi moi tinh KM
			if (validateOrder() == -1) {
				// recalculate amount
				recalculateAmountOrder();

				// tinh khuyen mai
				calPromotion();
			} else{
				//cancel request promition order
				isClickingPromotionOrder = false;
			}

		}
		showPopupChooseProduct = true;
	}

	/**
	 * doi so luong san pham ban theo cau hinh
	 * @author: duongdt3
	 * @time: 5:23:29 PM Nov 19, 2015
	*/
	private void changeQuantityProductByConfig() {
		try {
			for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
				OrderProductRow row = (OrderProductRow) tbProducts.getRowAt(i);
				if (row != null) {
					if(row.etRealOrder.isFocused() && row.etRealOrder.isEnabled()){
						row.changeQuantityProductByConfig();
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("changeQuantityProductByConfig", e);
		}
	}

	/**
	 * Reset cac gia tri lien quan KM
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void resetValueBeforeCalculatePromotion() {
		orderDTO.listPromotionChange.clear();
		orderDTO.productQuantityReceivedList.clear();
		orderDTO.orderQuantityReceivedList.clear();
//		orderDTO.listPromoDetail.clear();
		orderDTO.listPromotionOrder.clear();
		orderDTO.listPromotionAccumulation.clear();
		orderDTO.listPromotionAccumulationTemp.clear();
		orderDTO.listPromotionNewOpen.clear();
		orderDTO.listLackRemainAccumulationProduct.clear();
		orderDTO.listPromotionKeyShop.clear();
		orderDTO.listPromotionOrdersMoney.clear();
		orderDTO.listPromotionNewOpenMoney.clear();
		//Reset index cua promotion
//		indexPromotionOrder = 0;
	}

	/**
	 * Kiem tra thong tin va luu don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void processSaveOrder() {
		boolean allowSave = false;
		// Validate co don hang thi moi cho luu
		if (orderDTO.listBuyOrders.size() == 0) {
			// neu so luong sp khuyen mai > 0 & ton tai sp CTTB --> enable
			// button luu
			boolean errorInput = false;
			if (orderDTO.listPromotionOrders.size() == 0) {
				errorInput = true;
			} else {
				for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
					// Ds KM co KM tu dong thi bat buoc phai co mat
					// hang ban
					
					if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
						errorInput = true;
						break;
					} 
				}
			}

			if (errorInput) {
				parent.showDialog(StringUtil.getString(R.string.ERROR_CHOOSE_PRODUCT));
				allowSave = false;
			} else {
				allowSave = true;
			}
		} else {
			allowSave = true;
		}

		
		//validate truoc khi cho phep luu
		if (allowSave) {
			StringBuilder listProgram = new StringBuilder();
			boolean hasPromotionZeroQuantity = false;
			boolean hasManualPromotion = false;
			
			for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
				if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
						) {
					hasManualPromotion = true;
					if((long)promotion.orderDetailDTO.quantity == 0) {
						hasPromotionZeroQuantity = true;
						
						if(!StringUtil.isNullOrEmpty(promotion.orderDetailDTO.programeCode)
							&& listProgram.indexOf(promotion.orderDetailDTO.programeCode) < 0) {
							listProgram.append(promotion.orderDetailDTO.programeCode).append(", ");
						}
					}
				} 
			}
			
			//validate tong tien am
			// chi validate khi hien thi gia
			if (hasManualPromotion && hasPromotionZeroQuantity) {
				allowSave = false;
				if(listProgram.length() > 0) {
					listProgram.delete(listProgram.length() - 2, listProgram.length() - 1);
				}
				parent.showDialog(StringUtil.getString(R.string.ERROR_CHOOSE_MANUAL_PROMOTION) + " " + listProgram.toString());
			} else if (orderDTO.orderInfo.getTotal() < 0) {
				allowSave = false;
				if (isShowPrice) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_VALIDATE_TOTAL_ORDER_SHOW_PRICE));
				} else{
					parent.showDialog(StringUtil.getString(R.string.TEXT_VALIDATE_TOTAL_ORDER_NOT_SHOW_PRICE));
				}
			} else{
				if(isShowPrice){
					boolean isVaildPrice = true;
					String productPriceInvalid = "";
					boolean isFirst = true;
					//validate gia
					for (OrderDetailViewDTO buyProduct : orderDTO.listBuyOrders) {
						//neu co so luong thung phai co gia thung, neu co so luong le phai co gia le
						if((buyProduct.orderDetailDTO.quantityRetail > 0 && buyProduct.orderDetailDTO.isPriceNull)
								||(buyProduct.orderDetailDTO.quantityPackage > 0 && buyProduct.orderDetailDTO.isPackagePriceNull)){
							isVaildPrice = false;
							productPriceInvalid += (!isFirst ? ", " : "") + buyProduct.productCode + " - " + buyProduct.productName;
							isFirst = false;
						}
					}

					if (!isVaildPrice) {
						allowSave = false;
						parent.showDialog(StringUtil.getString(R.string.TEXT_VALIDATE_PRICE_ORDER, productPriceInvalid));
					}
				}
			}
		}


		if (allowSave) {
			// Ngay giao hang khong hop le
			if (this.orderType == CREATE_ORDER_FOR_PRESALE
					&& !validateDeliveryDate()) {
				if (parent != null) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_VALIDATE_DELIVERY_DATE));
				}
			} else {
				String confirm = null;
				if (this.orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.customer.applyDebitLimited == 1
						&& debitCustomerDTO != null) {
					// kiem tra han muc no cua khach hang
					int maxDebitdate = 0;
					try {
						maxDebitdate = Integer
								.parseInt(orderDTO.customer.maxDebitDate);
					} catch (Exception e) {
					}
					if (orderDTO.customer.maxDebitAmount != -1
							&& debitCustomerDTO.totalDebit
									+ orderDTO.orderInfo.getTotal() > orderDTO.customer.maxDebitAmount
							&& !StringUtil
									.isNullOrEmpty(orderDTO.customer.maxDebitDate)
							&& debitCustomerDTO.maxDebitDays > maxDebitdate) {
						confirm = StringUtil
								.getString(R.string.TEXT_MAX_DEBIT_AMOUNT_DATE_CONFIRM);
						confirm = confirm
								.replace(
										Constants.REPLACED_STRING,
										StringUtil.formatNumber(orderDTO.customer.maxDebitAmount));
						confirm = confirm.replace("yyy",
								orderDTO.customer.maxDebitDate + "");
					} else if (orderDTO.customer.maxDebitAmount != -1
							&& debitCustomerDTO.totalDebit
									+ orderDTO.orderInfo.getTotal() > orderDTO.customer.maxDebitAmount) {
						confirm = StringUtil
								.getStringAndReplace(
										R.string.TEXT_MAX_DEBIT_AMOUNT_CONFIRM,
										StringUtil
												.formatNumber(orderDTO.customer.maxDebitAmount));
					} else if (!StringUtil
							.isNullOrEmpty(orderDTO.customer.maxDebitDate)
							&& debitCustomerDTO.maxDebitDays > maxDebitdate) {
						confirm = StringUtil.getStringAndReplace(
								R.string.TEXT_MAX_DEBIT_DATE_CONFIRM,
								maxDebitdate + "");
					}
				}

				if (!StringUtil.isNullOrEmpty(confirm)) {
					Dialog dialog = GlobalUtil.showDialogConfirm(this, this.parent, confirm,
							StringUtil.getString(R.string.TEXT_AGREE),
							ACTION_SAVE_ORDER_OK,
							StringUtil.getString(R.string.TEXT_DENY),
							ACTION_CANCEL_SAVE_ORDER, null);
					if (dialog != null) {
						dialog.setOnDismissListener(dismissDialogPromotionListener);
					}
				} else {
					String customerName = "";
					if (orderDTO.customer != null
							&& !StringUtil.isNullOrEmpty(orderDTO.customer
									.getCustomerName())) {
						customerName = orderDTO.customer.getCustomerName()
								.trim();
					}
					Dialog dialog = GlobalUtil.showDialogConfirm(this, this.parent, StringUtil
							.getStringAndReplace(
									R.string.TEXT_CONFIRM_SAVE_ORDER,
									customerName), StringUtil
							.getString(R.string.TEXT_AGREE),
							ACTION_SAVE_ORDER_OK, StringUtil
									.getString(R.string.TEXT_DENY),
							ACTION_CANCEL_SAVE_ORDER, null);

					if (dialog != null) {
						dialog.setOnDismissListener(dismissDialogPromotionListener);
					}
				}
			}
		} else {
			//cancel request promition order
			isClickingPromotionOrder = false;
		}
	}

	/**
	 * Xu ly Dong y back
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: int
	 * @throws:
	 */
	private void handleAgreeBack() {
		ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if (actionLog != null && actionLog.isOr == 1
				&& StringUtil.isNullOrEmpty(actionLog.endTime)) {
			parent.requestUpdateActionLog("0", "0", null, this);
		}

		if (orderDTO.isBackToRemainOrder) {
			RemainProductView remainFragment = (RemainProductView)
					findFragmentByTag(GlobalUtil.getTag(RemainProductView.class));
			if (remainFragment != null) {
				remainFragment.receiveBroadcast(
						ActionEventConstant.BROADCAST_ORDER_VIEW, null);
			}
			GlobalUtil.popBackStack(this.parent);
		} else {
			if (orderDTO.isChangeData) {
				GlobalUtil.popBackStack(this.parent);
			}
		}
	}

	/**
	 * Enable button
	 *
	 * @author: TruongHN
	 * @param isEnable
	 * @return: void
	 * @throws:
	 */
	private void enableButtonSave(boolean isEnable) {
		orderDTO.isEnableButton = isEnable;
		GlobalUtil.setEnableButton(btSave, isEnable);
		GlobalUtil.setEnableButton(btSaveAndSend, isEnable);
//		if (isEnable) {
//			PriUtils.getInstance().setStatus(btSave, PriUtils.ENABLE);
//			PriUtils.getInstance().setStatus(btSaveAndSend, PriUtils.ENABLE);
//		} else {
//			PriUtils.getInstance().setStatus(btSave, PriUtils.DISABLE);
//			PriUtils.getInstance().setStatus(btSaveAndSend, PriUtils.DISABLE);
//		}
	}

	/**
	 * Enable all button
	 *
	 * @author: TruongHN
	 * @param isEnable
	 * @return: void
	 * @throws:
	 */
	private void enableAllButton(boolean isEnable) {
		GlobalUtil.setEnableButton(btnCalPromotion, isEnable);
		GlobalUtil.setEnableButton(btAddBill, isEnable);
		// GlobalUtil.setEnableButton(btSave, isEnable);
		if (isEnable) {
			PriUtils.getInstance().setOnClickListener(tvDeliveryDate, this);
			PriUtils.getInstance().setOnClickListener(tvDeliveryTime, this);
			PriUtils.getInstance().setStatus(spinnerPriority, PriUtils.ENABLE);
		} else {
			PriUtils.getInstance().setStatus(spinnerPriority, PriUtils.DISABLE);
			tvDeliveryDate.setOnClickListener(null);
			tvDeliveryTime.setOnClickListener(null);
		}
		enableButtonSave(isEnable);
		if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW) {
			if (isEnable) {
				PriUtils.getInstance().setStatus(cbOrder, PriUtils.ENABLE);
				cbKeyShop.setEnabled(true);
			} else {
				PriUtils.getInstance().setStatus(cbOrder, PriUtils.DISABLE);
				cbKeyShop.setEnabled(false);
			}
				//			if (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES) {
				// Render lai layout tuy thuoc vao loai don hang dan chon
				if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
					cbOrder.setChecked(true);
					if(orderDTO.orderInfo.approved != -1){
						PriUtils.getInstance().setStatus(cbOrder, PriUtils.DISABLE);
					}
				}
//			}
		}
	}

	/**
	 * Kiem tra enable/disable button
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void checkEnableButton() {
		// chi cho phep sua don hang tra ve trong ngay hien tai
		// Xem lai don hang : isEditOrder
		if(orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_PO){
			enableAllButton(false);
			if (orderDTO.orderInfo.approved == 1) {
				isCheckStockTotal = false;
			}
		} else if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW
				&& orderDTO.stateOrder != OrderViewDTO.STATE_EDIT
				&& (orderDTO.orderInfo.approved != -1
				|| DateUtils.compareWithNow(orderDTO.orderInfo.createDate, "yyyy-MM-dd") != 0)) {
			enableAllButton(false);
			if (orderDTO.orderInfo.approved == 1) {
				isCheckStockTotal = false;
			}
		} else if (!orderDTO.isEnableButton) {
			enableButtonSave(false);
		} else {
			enableAllButton(true);
		}
	}

	/**
	 * Request tao moi don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void requestCreateOrder() {
		// Cap nhat ngay
		if (!isRequesting) {
			checkOrderBeforeSave();
			isRequesting = true;
			orderDTO.orderInfo.routingId = routingId;
			orderDTO.orderInfo.orderDate = DateUtils.now();
			orderDTO.orderInfo.createDate = orderDTO.orderInfo.orderDate;
			orderDTO.orderInfo.updateUser = "";
			orderDTO.orderInfo.updateDate = "";
			orderDTO.orderInfo.cashierId = cashierStaffId;
			if (orderDTO.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {
				orderDTO.orderInfo.deliveryId = deliveryId;
			} else if (orderDTO.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)){
				orderDTO.orderInfo.deliveryId = String.valueOf(orderDTO.orderInfo.staffId);
			}
			if (PRIORITY_NOW_VALUE == orderDTO.orderInfo.priority) {
				orderDTO.orderInfo.deliveryDate = orderDTO.orderInfo.orderDate;
				recalculateDeliveryDateAndTime();
			}
			if (StringUtil.isNullOrEmpty(orderDTO.orderInfo.deliveryDate)
					|| ":00".equals(orderDTO.orderInfo.deliveryDate)) {
				hardcode();
			}

			if (parent != null) {
				parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
			}
			ActionEvent e = new ActionEvent();
			e.viewData = orderDTO;
			e.sender = this;
			e.action = ActionEventConstant.CREATE_NEW_ORDER;
			e.isNeedCheckTimeServer = true;
			SaleController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * Cap nhat don hang truoc khi luu
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void checkOrderBeforeSave() {
		if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
			orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_PRESALE;
			String strDateTime = DateUtils.getDateTimeStringFromDateAndTime(
					orderDTO.deliveryDate, orderDTO.deliveryTime);
			orderDTO.orderInfo.deliveryDate = strDateTime;
		} else {
			orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_VANSALE;
			orderDTO.orderInfo.deliveryDate = "";
			orderDTO.orderInfo.priority = PRIORITY_NOW_VALUE;
		}
		orderDTO.orderInfo.quantity = orderDTO.numSKU;
	}

	/**
	 * Tinh lai amount & total cua don hang truoc khi luu
	 *
	 * @author: Nguyen Thanh Dung
	 * @param
	 * @return: void
	 * @throws:
	 */
	private void recalculateAmountOrder() {
		orderDTO.orderInfo.setAmount(0);
//		orderDTO.discountAmount = 0;
		orderDTO.orderInfo.setTotal(0);
		orderDTO.orderInfo.setDiscount(0);
		orderDTO.setPromotionDiscount(0);

		// ds mat hang ban
		for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
			OrderDetailViewDTO dto = orderDTO.listBuyOrders.get(i);
			dto.orderDetailDTO.setAmount(dto.orderDetailDTO.calculateAmountProduct());
			orderDTO.orderInfo.addAmount(dto.orderDetailDTO.getAmount());
			dto.quantity = dto.quantityProductStr();
		}
		orderDTO.orderInfo.setTotal(orderDTO.orderInfo.getAmount() - orderDTO.orderInfo.getDiscount() - orderDTO.getPromotionDiscount());
	}

	/**
	 *
	 * Fix ta.m bug deliver code khong co nen delivery dayInOrder cung ko co'
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	private void hardcode() {

		// lay code

		for (int i = 0, size = orderDTO.listPriority.size(); i < size; i++) {

			ApParamDTO dto = orderDTO.listPriority.get(i);
			if (ApParamDTO.PRIORITY_NOW.equals(dto.getValue())) {
				orderDTO.orderInfo.priority = dto.getApParamId();
				break;
			}
		}
		orderDTO.orderInfo.deliveryDate = orderDTO.orderInfo.createDate;
	}

	/**
	 *
	 * Kiem tra ngay giao hang co hop le hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @rewturn: boolean
	 * @throws:
	 */
	private boolean validateDeliveryDate() {
		boolean result = true;
		// lay code
		String appCode = "";
		for (int i = 0, size = orderDTO.listPriority.size(); i < size; i++) {
			ApParamDTO dto = orderDTO.listPriority.get(i);
			if (dto.getApParamId() == orderDTO.orderInfo.priority) {
				appCode = dto.getValue();
				break;
			}
		}

		if (ApParamDTO.PRIORITY_NOW.equals(appCode)) {
			orderDTO.orderInfo.deliveryDate = orderDTO.orderInfo.createDate;
			recalculateDeliveryDateAndTime();

		} else {
			String strDateTime = DateUtils.getDateTimeStringFromDateAndTime(
					orderDTO.deliveryDate, orderDTO.deliveryTime);
			orderDTO.orderInfo.deliveryDate = strDateTime;
		}

		if (ApParamDTO.PRIORITY_OUT_DAY.equals(appCode)) {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			try {
				Date createDate = formatter
						.parse(orderDTO.orderInfo.createDate);
				Date deliveryDate = formatter
						.parse(orderDTO.orderInfo.deliveryDate);

				if (deliveryDate.compareTo(createDate) == -1) {
					result = false;
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
			}
		}

		return result;
	}

	/**
	 * Request chinh sua don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void requestEditOrder() {
		if (!isRequesting) {
			checkOrderBeforeSave();
			isRequesting = true;
			if (StringUtil.isNullOrEmpty(orderDTO.orderInfo.deliveryDate)
					|| ":00".equals(orderDTO.orderInfo.deliveryDate)) {
				hardcode();
			}
			if (parent != null) {
				parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
			}
			ActionEvent e = new ActionEvent();
			e.viewData = orderDTO;
			e.sender = this;
			e.action = ActionEventConstant.EDIT_AND_SEND_ORDER;
			// if (orderDTO.orderInfo.isSend == 1) {
			// e.isNeedCheckTimeServer = true;
			// }
			SaleController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * Validate don hang truoc khi luu
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	private int validateOrder() {
		int res = -1;
		// validate ds mat hang ban
		for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
			OrderDetailViewDTO orderProduct = orderDTO.listBuyOrders.get(i);
			if (!GlobalUtil.isValidQuantity(orderProduct.quantity)
					&& !StringUtil.isNullOrEmpty(orderProduct.quantity.trim())) {
				// chua nhap so luong --> Gia tri thuc dat phai lon hon 0
				parent.showDialog(StringUtil
						.getString(R.string.ERROR_INVALID_REAL_ORDER));
				res = i;
				break;
			}
			if(orderProduct.orderDetailDTO.quantity==0){
				if (StringUtil.isNullOrEmpty(orderProduct.quantity.trim())) {
					parent.showDialog(StringUtil
							.getString(R.string.ERROR_INVALID_REAL_ORDER_EMPTY));
				} else {
					parent.showDialog(StringUtil
							.getString(R.string.ERROR_INVALID_REAL_ORDER_0));
				}
				res = i;
				break;
			}
		}
		//
		return res;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spinnerPriority) {
			ApParamDTO app = getPriority(spinnerPriority
					.getSelectedItemPosition());
			if (app != null) {
				orderDTO.orderInfo.priority = app.getApParamId();
				Calendar calendar = Calendar.getInstance(TimeZone
						.getTimeZone("GMT+7"));
				SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
				SimpleDateFormat dateFormatter = new SimpleDateFormat(
						"dd/MM/yyyy");

				if (ApParamDTO.PRIORITY_NOW.equals(app.getValue())) {
					tvDeliveryTime.setText(timeFormatter.format(calendar.getTime()));

					PriUtils.getInstance().setStatus(tvTextTime, PriUtils.INVISIBLE);
					PriUtils.getInstance().setStatus(tvDeliveryDate, PriUtils.INVISIBLE);
					PriUtils.getInstance().setStatus(tvDeliveryTime, PriUtils.INVISIBLE);

				} else if (ApParamDTO.PRIORITY_IN_DAY.equals(app.getValue())) {
					PriUtils.getInstance().setStatus(tvTextTime, PriUtils.ENABLE);
					PriUtils.getInstance().setStatus(tvDeliveryDate, PriUtils.DISABLE);
					PriUtils.getInstance().setStatus(tvDeliveryTime, PriUtils.ENABLE);

					tvDeliveryTime.setText(timeFormatter.format(calendar.getTime()));
					tvDeliveryDate.setTextColor(Color.GRAY);
				} else if (ApParamDTO.PRIORITY_OUT_DAY.equals(app.getValue())) {
					PriUtils.getInstance().setStatus(tvTextTime, PriUtils.ENABLE);
					PriUtils.getInstance().setStatus(tvDeliveryDate, PriUtils.ENABLE);
					PriUtils.getInstance().setStatus(tvDeliveryTime, PriUtils.ENABLE);

					calendar.add(Calendar.DAY_OF_YEAR, 1);
					tvDeliveryTime.setText("08:00");
					tvDeliveryDate.setTextColor(Color.BLACK);
				}

				tvDeliveryDate
						.setText(dateFormatter.format(calendar.getTime()));
				tvDeliveryTime.setTextColor(Color.BLACK);

				if (firstTimeGetOrder) {
					firstTimeGetOrder = false;
					tvDeliveryDate.setText(orderDTO.deliveryDate);
					tvDeliveryTime.setText(orderDTO.deliveryTime);
				} else {
					orderDTO.deliveryDate = tvDeliveryDate.getText().toString()
							.trim();
					orderDTO.deliveryTime = tvDeliveryTime.getText().toString()
							.trim();
				}
			}
		}
	}

	/**
	 *
	 * display find product add to order view
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void gotoFindProductAddToListOrder() {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_LIST_PRODUCT_NOT_IN,
				orderDTO.listBuyOrders);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				orderDTO.customer.getCustomerId());
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				orderDTO.customer.getCustomerTypeId());
		bundle.putString(IntentConstants.INTENT_ORDER_TYPE,
				orderDTO.orderInfo.orderType);
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = bundle;
		e.action = ActionEventConstant.GO_TO_LIST_PRODUCTS_ADD_ORDER_LIST;
		UserController.getInstance().handleSwitchFragment(e);

		orderDTO.isFirstInit = false;
	}

	/**
	 * Chuyen sang man hinh thong tin chi tiet khach hang
	 *
	 * @author: TruongHN
	 * @param customerId
	 * @return: void
	 * @throws:
	 */
	private void handleAfterCreateOrder(String customerId) {
		int isOr = orderDTO.orderInfo.isVisitPlan == 0 ? 1 : 0;
		// ghi log ghe tham khi dat hang
//		parent.requestInserActionLog(orderDTO.beginTimeVisit,
//				ActionLogDTO.TYPE_ORDER,
//				String.valueOf(orderDTO.orderInfo.saleOrderId),
//				orderDTO.customer.customerId, String.valueOf(isOr));
		ActionLogDTO action = GlobalInfo.getInstance().getProfile().getActionLogVisitCustomer();

		// thay doi gp: doi saleOrderId -> poId
		parent.requestInserActionLog(orderDTO.beginTimeVisit,
				ActionLogDTO.TYPE_ORDER,
				String.valueOf(orderDTO.orderInfo.poId),
				orderDTO.customer.customerId, String.valueOf(isOr), action.aCustomer.lat, action.aCustomer.lng, action.routingId);

		parent.getOrderInLogForNotify();

		// Sau khi thuc hien xong cac chuc nang tren. Cac anh nho them vao cau
		// lenh hide button Dong cua giup em.
		parent.removeMenuCloseCustomer();

		// Dat hang cho KH ngoai tuyen
		if (isOr == 1) {
			endVisitCustomer();
		} else {// trong tuyen
			// Kiem tra xem co da ket thuc hay chua U hoi co muon ket thuc hay
			// ko?
			if (action != null && action.objectType.equals("0")
					&& StringUtil.isNullOrEmpty(action.endTime)) {
				String endTime = DateUtils.getVisitEndTime(action.aCustomer);
				SpannableObject textConfirmed = new SpannableObject();
				textConfirmed.addSpan(StringUtil
						.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
					textConfirmed
							.addSpan(" " + action.aCustomer.customerCode,
									ImageUtil.getColor(R.color.WHITE),
									android.graphics.Typeface.BOLD);
				}
				textConfirmed.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				if (!StringUtil.isNullOrEmpty(action.aCustomer.customerName)) {
					textConfirmed.addSpan(action.aCustomer.customerName,
							ImageUtil.getColor(R.color.WHITE),
							android.graphics.Typeface.BOLD);
				}
				textConfirmed.addSpan(" "+StringUtil.getString(R.string.TEXT_IN)+" ",
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);
				textConfirmed.addSpan(
						DateUtils.getVisitTime(action.startTime, endTime),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.BOLD);
				textConfirmed.addSpan(StringUtil
						.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
						ImageUtil.getColor(R.color.WHITE),
						android.graphics.Typeface.NORMAL);

				GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this,
						parent, textConfirmed.getSpan(),
						StringUtil.getString(R.string.TEXT_AGREE),
						ACTION_END_VISIT_OK,
						StringUtil.getString(R.string.TEXT_CANCEL),
						ACTION_END_VISIT_CANCEL, null, false, false);
			} else {
				gotoCustomerInfo();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_PROMOTION_PRODUCT_FROM_SALE_PRODUCT:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			//cancel request promition order
			isClickingPromotionOrder = false;
			isRequesting = false;
			SortedMap<Long, ArrayList<OrderDetailViewDTO>> sortListOutput = (SortedMap<Long, ArrayList<OrderDetailViewDTO>>) modelEvent
					.getModelData();

			// Tinh so luong khuyen mai
			orderDTO.numSKUPromotion = 0;
			// tinh tong so tien khuyen mai
			//Reset discount & total
//			orderDTO.orderInfo.discount = 0;
//			orderDTO.orderInfo.discount += orderDTO.discountAmount;
//			orderDTO.orderInfo.total = orderDTO.orderInfo.amount - orderDTO.orderInfo.discount;

			// ArrayList<OrderDetailViewDTO> listPromotion =
			// (ArrayList<OrderDetailViewDTO>) modelEvent.getModelData();
			if (sortListOutput != null) {
				calculatePromotionProduct(sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT)));
				orderDTO.listPromotionOrder = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ORDER));
//				orderDTO.listPromotionAccumulation = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION));
				orderDTO.listPromotionAccumulationTemp = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_ACCUMULATION));
				orderDTO.listPromotionNewOpen = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));
				orderDTO.listPromotionKeyShop = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_KEY_SHOP));

				//Ds KM tien phuc vu kiem tra tinh toan so suat
				orderDTO.listPromotionOrdersMoney = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_PRODUCT_MONEY));
				orderDTO.listPromotionNewOpenMoney = sortListOutput.get(Long.valueOf(OrderViewDTO.INDEX_PROMOTION_NEW_OPEN));

				// Luu ds cac ds san pham khuyen mai co the duoc doi
				if (sortListOutput.size() > OrderViewDTO.NUM_PROMOTION_TYPE) {
					Iterator<Long> it = sortListOutput.keySet().iterator();
					for(int i = 0; i < OrderViewDTO.NUM_PROMOTION_TYPE; i++) {
						it.next();
					}

					while (it.hasNext()) {
						Long md = it.next();
						ArrayList<OrderDetailViewDTO> listProductChange = sortListOutput.get(md);
						orderDTO.listPromotionChange.put(md, listProductChange);
					}
				}

				//Tinh toan lai so suat toi do duoc huong
				validatePromotionQuantityReceived();
				calPriceKeyShop();// tinh toan lai tien tra thuong cho keyshop truoc khi render layout
				layoutOrder();
//				displayPromotion();
//				reLayoutBuyProducts();
			}

			// // cap nhat lai, khong can tinh khuyen mai khi luu don hang nua
			// (neu khong co j thay doi)
			// orderDTO.isNeedCallPromotion = false;


			//Thong bao neu co sp ko co gia
			String listMissingPrice = getListPromotionProductMissingPrice();
			if(!StringUtil.isNullOrEmpty(listMissingPrice)) {
				Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil.getStringAndReplace(
						R.string.TEXT_PROMOTION_PRODUCT_HAS_NO_PRICE,
						listMissingPrice),
								StringUtil.getString(R.string.TEXT_AGREE),
								ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE, StringUtil
								.getString(R.string.TEXT_DENY),
								ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE, null);
				if (dialog != null) {
					dialog.setOnDismissListener(dismissDialogPromotionListener);
				}
				enableButtonSave(true);
			} else {
				//Hien thi so tien KM tich luy con du
				showAcumulationDiscountRemain();

				if (orderDTO.listBuyOrders.size() > 0) {
					enableButtonSave(true);
				} else {
					boolean hasPromotionProduct = false;
					boolean hasDisplayProduct = false;
					for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
						// Ds KM co KM = tay or tu dong
						if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
							hasPromotionProduct = true;
							break;
						} else {
							hasDisplayProduct = true;
						}
					}

					if (hasPromotionProduct || !hasDisplayProduct) {
						// parent.showDialog(StringUtil
						// .getString(R.string.ERROR_CHOOSE_PRODUCT));
					} else {
						enableButtonSave(true);
					}
				}

				if(orderDTO.lstWrongProgrameId.size() > 0){
					if (GlobalInfo.getInstance().getStateConnectionMode() == Constants.CONNECTION_ONLINE) {
						isWrongPromotionOrder = true;
						enableButtonSave(false);
						GlobalUtil.showDialogConfirm(this, StringUtil.getString(R.string.TEXT_ERROR_KM),
								StringUtil.getString(R.string.PROMOTION_WRONG),
								StringUtil.getString(R.string.TEXT_AGREE),
								ACTION_SYN_PROMOTION_OK,
								StringUtil.getString(R.string.TEXT_CANCEL),
								ACTION_SYN_PROMOTION_CANCEL, null);
					} else {
						//hoat dong khong ket noi
						parent.showDialog(StringUtil.getString(R.string.PROMOTION_SYN_OFFLINE));
						btSaveAndSend.setText(StringUtil.getString(R.string.TEXT_SAVE));
					}
				}
			}

			parent.hideKeyboardCustom();
			break;
		case ActionEventConstant.GET_PO:
		case ActionEventConstant.GET_ORDER_FOR_EDIT:

			OrderViewDTO model = (OrderViewDTO) modelEvent.getModelData();
			if (model.orderInfo != null && (orderDTO.stateOrder != OrderViewDTO.STATE_VIEW_PO || !isRefreshView)) {
				orderDTO.orderInfo = model.orderInfo;
			}

			orderDTO.productQuantityReceivedList = model.productQuantityReceivedList;

			if (model.listBuyOrders != null) {
				orderDTO.listBuyOrders = model.listBuyOrders;
			}

			//update discount on product when doen't have listPromotionOrders
//			orderDTO.discountAmount = model.discountAmount;

			firstTimeGetOrder = true;
			orderDTO.isChangeData = false;

			// TruongHN: fix bug mac dinh khi sua don hang la phai disable
			// button
			orderDTO.isEnableButton = false;

			recalculateDeliveryDateAndTime();
			// kiem tra don hang la don hang presale hay vansale
//			if (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_PRESALES) {
//				rlOrderVansale.setVisibility(View.GONE);
//			} else if (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES) {
				if (SALE_ORDER_TABLE.ORDER_TYPE_PRESALE
						.equals(orderDTO.orderInfo.orderType)) {
					this.orderType = CREATE_ORDER_FOR_PRESALE;
					PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.INVISIBLE);
					PriUtils.getInstance().setStatus(btSave, PriUtils.INVISIBLE);
//					btPayment.setVisibility(View.GONE);
					llOrderPresale.setVisibility(View.VISIBLE);
					if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW) {
						cbOrder.setChecked(true);
						if(orderDTO.orderInfo.approved != -1){
							PriUtils.getInstance().setStatus(cbOrder, PriUtils.DISABLE);
						} else if(orderDTO.orderInfo.approved == -1){
							// la don tam thi ENABLE
							PriUtils.getInstance().setStatus(cbOrder, PriUtils.ENABLE);
						}
					}
				} else if (SALE_ORDER_TABLE.ORDER_TYPE_VANSALE.equals(orderDTO.orderInfo.orderType)
						|| SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE.equals(orderDTO.orderInfo.orderType)) {
					this.orderType = CREATE_ORDER_FOR_VANSALE;
					llOrderPresale.setVisibility(View.GONE);
					if (isShowPrice) {
						PriUtils.getInstance().setStatus(tvTotalAmountVansales, PriUtils.ENABLE);
					}
					PriUtils.getInstance().setStatus(btSave, PriUtils.ENABLE);
//					btPayment.setVisibility(View.VISIBLE);
					cbOrder.setChecked(false);
					if(orderDTO.orderInfo.approved == -1){
						// la don tam thi ENABLE
						PriUtils.getInstance().setStatus(cbOrder, PriUtils.ENABLE);
					}
				}
//			}

			// Kiem tra xem co enable cac nut de thuc hien chuc nang hay ko?
			checkEnableButton();
			// enable check/ ko check keyshop
			if(cbKeyShop.getVisibility() == View.VISIBLE){ // checkbox keyshop dang hien thi thi moi xu li check / hay ko
				cbKeyShop.setChecked(orderDTO.orderInfo.isRewardKS);
				orderDTO.isKeyShop = orderDTO.orderInfo.isRewardKS;
			}else{
				// truong hop tra thuong keyshop da off cho KH do thi mac dinh view don hang ko tinh tra thuong keyshop nua de cho truong hop edit
				orderDTO.isKeyShop = false;
			}
			// Update promotion
//			if (model.listPromotionOrders != null) {
			orderDTO.listPromotionOrders.clear();
			calculatePromotionProduct(model.listPromotionOrders);
			orderDTO.listPromotionOrder = model.listPromotionOrder;
			orderDTO.listPromotionAccumulation = model.listPromotionAccumulation;
			orderDTO.listPromotionNewOpen = model.listPromotionNewOpen;
			orderDTO.listPromotionKeyShop = model.listPromotionKeyShop;
//			}

			// showInfoOnView();
//			displayPromotion();
//			reLayoutBuyProducts();
			layoutOrder();

			if (parent != null) {
				parent.closeProgressDialog();
			}
			break;
		case ActionEventConstant.EDIT_AND_SEND_ORDER: {
			requestInsertLogKPI(HashMapKPI.NVBH_SUADONHANG,
					modelEvent.getActionEvent());
			// Khong edit don hang tam
			if (orderDTO.orderInfo.approved != -1) {
				// remove id trong listId neu co
				String orderId = String.valueOf(orderDTO.orderInfo.saleOrderId);
				if (!GlobalInfo.getInstance().getNotifyOrderReturnInfo().listOrderProcessedId
						.contains(orderId)) {
					GlobalInfo.getInstance().getNotifyOrderReturnInfo().listOrderProcessedId
							.add(orderId);
				}

				// closeProgressDialog();
				isRequesting = false;
				//cancel request promition order
				isClickingPromotionOrder = false;

				Bundle dataObject = new Bundle();
				dataObject.putSerializable(
						IntentConstants.INTENT_UPDATE_EDITED_ORDER,
						(Serializable) orderDTO);

				ListOrderView orderFragment = (ListOrderView)parent
						.getFragmentManager().findFragmentByTag(
								GlobalUtil.getTag(ListOrderView.class));
				if (orderFragment != null) {
					orderFragment.receiveBroadcast(
							ActionEventConstant.BROADCAST_UPDATE_EDIT_ORDER,
							dataObject);
				}

				if (parent != null) {
					parent.closeProgressDialog();
					// kiem tra network
					if (!GlobalUtil.checkNetworkAccess()) {
						parent.showToastMessage(StringUtil
								.getString(R.string.TEXT_NETWORK_DISABLE));
					}

					parent.getOrderInLogForNotify();
				}

				if (isDraftOrder) {
					parent.draftOrderId = "";
					parent.setStatusVisible(StringUtil.getString(R.string.TEXT_VISITING) +" : "
							+ orderDTO.customer.customerName + " - "
							+ orderDTO.customer.customerCode + " ", View.VISIBLE);
					// chuyen sang man hinh thong tin khach hang
					// handleAfterCreateOrder(orderDTO.customer.getCustomerId());
					if (orderType == CREATE_ORDER_FOR_PRESALE) {
						// chuyen sang man hinh thong tin khach hang
						handleAfterCreateOrder(orderDTO.customer.getCustomerId());
					} else {
						// vansale
						// show popup thanh toan
						if (orderDTO.orderInfo.getTotal() > 0 && orderDTO.orderInfo.approved != 2 && isShowPrice) {
							showPopUpPayment();
						} else {
							handleAfterCreateOrder(orderDTO.customer.getCustomerId());
						}
					}
				} else {
					if (orderType == CREATE_ORDER_FOR_VANSALE) {
						if (orderDTO.orderInfo.getTotal() > 0 && orderDTO.orderInfo.approved != 2 && isShowPrice) {
							showPopUpPayment();
						} else {
							GlobalUtil.popBackStack(this.parent);
						}
					} else {
						GlobalUtil.popBackStack(this.parent);
					}
				}
			} else {
				if (parent != null) {
					parent.closeProgressDialog();
				}

				if (showBackPopup) {
					showBackPopup = false;
					handleAgreeBack();
				} else {
					if (indexMenu >= 0) {
						parent.switchMenuLastSaveAction();
					}
				}
			}
		}
			break;
		case ActionEventConstant.CREATE_NEW_ORDER:
			// // chuyen sang man hinh thong tin khach hang
			// handleAfterCreateOrder(orderDTO.customer.getCustomerId());
			requestInsertLogKPI(HashMapKPI.NVBH_LUUDONHANG,
					modelEvent.getActionEvent());
				// Khong edit don hang tam
			if (orderDTO.orderInfo.approved != -1) {
				//cancel request promition order
				isClickingPromotionOrder = false;
				isRequesting = false;
				if (parent != null) {
					parent.closeProgressDialog();
					// kiem tra network
					if (!GlobalUtil.checkNetworkAccess()) {
						parent.showToastMessage(StringUtil
								.getString(R.string.TEXT_NETWORK_DISABLE));
					}
				}

//				handleAfterCreateOrder(orderDTO.customer.getCustomerId());
				if (orderType == CREATE_ORDER_FOR_PRESALE) {
					// chuyen sang man hinh thong tin khach hang
					handleAfterCreateOrder(orderDTO.customer.getCustomerId());
				} else {
					// vansale
					// show popup thanh toan
					if (parent != null) {
						parent.closeProgressDialog();
					}
					if (orderDTO.orderInfo.getTotal() > 0 && orderDTO.orderInfo.approved != 2 && isShowPrice) {
						showPopUpPayment();
					} else {
						handleAfterCreateOrder(orderDTO.customer.getCustomerId());
					}
				}
			} else {
				if (parent != null) {
					parent.getOrderDraft(orderDTO.customer.getCustomerId());
					parent.closeProgressDialog();
				}

				if (showBackPopup) {
					showBackPopup = false;
					handleAgreeBack();
				} else {
					if (indexMenu >= 0) {
						parent.switchMenuLastSaveAction();
					}
				}
			}
			break;
		case ActionEventConstant.GET_COMMON_DATA_ORDER:
			ArrayList<Object> res = (ArrayList<Object>) modelEvent
					.getModelData();
			if (res != null) {
				// cap nhat ds muc do
				orderDTO.listPriority = (ArrayList<ApParamDTO>) res.get(0);
				for (ApParamDTO prority : orderDTO.listPriority) {
					if(ApParamDTO.PRIORITY_NOW.equals(prority.getValue())){
						PRIORITY_NOW_VALUE  = prority.getApParamId();
						break;
					}
				}

				if (orderDTO.listPriority.size() > 1) {
					if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW) {
						orderDTO.orderInfo.priority = orderDTO.listPriority.get(1).getApParamId();
					}

				}
				displayListPriority();
				// BangHN: check dinh vi cua khach hang
				if ((lat <= 0 || lng <= 0) && orderDTO.isFirstInit && orderDTO.orderInfo.isVisitPlan == 1) {
					//o che do ket noi thi moi hien thi waitting
					if(GlobalInfo.getInstance().getStateConnectionMode() == Constants.CONNECTION_ONLINE){
						parent.reStartLocatingWithWaiting();
					} else{
						parent.reStartLocating();
					}
				}
			}
			break;
		case ActionEventConstant.PAYMENT_VANSALE:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			if(isWrongPromotionOrder) {
				GlobalUtil.popBackStack(this.parent);
			} else {
				handleAfterCreateOrder(orderDTO.customer.getCustomerId());
			}
			break;
		case ActionEventConstant.ACTION_GET_CUSTOMER_DEBIT:
			debitCustomerDTO = (DebitDTO) modelEvent.getModelData();
			break;
		case ActionEventConstant.ACTION_CHECK_KEYSHOP_CUSTOMER:
			isHaveKeyShop = (Boolean)modelEvent.getModelData();
			cbKeyShop.setVisibility(isHaveKeyShop?View.VISIBLE:View.GONE);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	private void validatePromotionQuantityReceived() {
		try {
//			if(orderType == CREATE_ORDER_FOR_PRESALE) {
				orderDTO.checkQuantityReceivedFull();
				String message = "";
				if(orderDTO.mapQuantityReceiveMissing.size() > 0) {
					message = orderDTO.generateQuantityReceiveMissingMessage();
				}

				if(!StringUtil.isNullOrEmpty(message)) {
					tvMessageCheckQuantityReceive.setText(message);
					tvMessageCheckQuantityReceive.setVisibility(View.VISIBLE);
				} else {
					tvMessageCheckQuantityReceive.setVisibility(View.GONE);
				}
//			}
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	/**
	 * Lay ds ma sp khong co gia
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: String
	 * @throws:
	 * @param listPromotionOrders
	 * @return
	 */
	private String getListPromotionProductMissingPrice() {
		StringBuilder result = new StringBuilder();
		if (isShowPrice) {
			//Loai bo cac dong sp trung
			HashMap<String, String> productCodeHash = new HashMap<String, String>();

			//Reset danh sp
			ArrayList<OrderDetailViewDTO> listProduct = new ArrayList<OrderDetailViewDTO>();

			//KM sp
			for(OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
				if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_DISPLAY_COMPENSATION
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE
						|| promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
						){
					//bo qua km huy, doi, tra, CTTB
				} else{
					listProduct.add(promotion);
				}
			}

			//KM don hang - loai sp
			for(OrderDetailViewDTO promotion : orderDTO.listPromotionOrder) {
				if(promotion.listPromotionForPromo21.size() > 0) {
					listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			//KM tich luy loai don hang
			for(OrderDetailViewDTO promotion : orderDTO.listPromotionAccumulation) {
				if(promotion.listPromotionForPromo21.size() > 0) {
					listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			//KM tich luy loai don hang
			for(OrderDetailViewDTO promotion : orderDTO.listPromotionNewOpen) {
				if(promotion.listPromotionForPromo21.size() > 0) {
					listProduct.addAll(promotion.listPromotionForPromo21);
				}
			}

			for (OrderDetailViewDTO promotionProduct : listProduct) {
				//Neu co san pham KM thi moi gan lai
				if (promotionProduct.orderDetailDTO.productId > 0) {
					if (promotionProduct.orderDetailDTO.price <= 0 && promotionProduct.orderDetailDTO.priceId <= 0) {
						if(!productCodeHash.containsValue(promotionProduct.productCode)) {
							productCodeHash.put(promotionProduct.productCode, promotionProduct.productCode);
						}
					}
				}
			}

			for (String productCode : productCodeHash.values()) {
				result.append(productCode + ", ");
			}

			if(result.length() >= 2) {
				result.replace(result.length() - 2,result.length() - 1, "");
			}
		}
		return result.toString();
	}

	/**
	 * Mo ta chuc nang cua ham
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */

	private void recalculateDeliveryDateAndTime() {
		if (orderDTO.orderInfo.deliveryDate != null) {
			String[] arrDate = orderDTO.orderInfo.deliveryDate.split(" ");

			// Ngay
			if (arrDate.length >= 1) {
				String[] dates = arrDate[0].trim().split("-");

				if (dates.length >= 3) {
					StringBuilder sbDate = new StringBuilder();
					sbDate.append(dates[2]).append("/").append(dates[1])
							.append("/").append(dates[0]);
					orderDTO.deliveryDate = sbDate.toString();
				}
			}

			// Gio
			if (arrDate.length >= 2) {
				String[] times = arrDate[1].trim().split(":");

				if (times.length >= 3) {
					StringBuilder sbTime = new StringBuilder();
					sbTime.append(times[0]).append(":").append(times[1]);
					orderDTO.deliveryTime = sbTime.toString();
				}
			} else {
				orderDTO.deliveryTime = "00:00";
			}
		}
	}

	/**
	 * Hien thi ds muc do
	 *
	 * @author: TruongHN
	 * @param listApp
	 * @return: void
	 * @throws:
	 */
	private void displayListPriority() {
		if (orderDTO.listPriority != null) {
			String[] arr = new String[orderDTO.listPriority.size()];
			for (int i = 0, size = orderDTO.listPriority.size(); i < size; i++) {
				arr[i] = orderDTO.listPriority.get(i).getApParamName();
			}
			// orderDTO.listPriority = listApp;
			// cap nhat vao spiner
//			SpinnerAdapter adapterLine = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, arr);
			ArrayAdapter adapterLine = new ArrayAdapter(parent, R.layout.simple_spinner_item, arr);
			adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			this.spinnerPriority.setAdapter(adapterLine);

			// if (orderDTO.listPriority.size() > 1) {
			// if (!orderDTO.isEditOrder) {
			// orderDTO.orderInfo.priority = orderDTO.listPriority.get(1).value;
			// }
			// // Set priority
			// setPriority(orderDTO.orderInfo.priority);
			// }

			// Set priority
			setPriority(orderDTO.orderInfo.priority);
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.EDIT_AND_SEND_ORDER:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			// Don draft
			if (isDraftOrder == true) {
				orderDTO.orderInfo.approved = -1;
			}
			//cancel request promition order
			isClickingPromotionOrder = false;
			isRequesting = false;
			enableButtonSave(true);
			parent.showDialog(modelEvent.getModelMessage());
			break;
		case ActionEventConstant.CREATE_NEW_ORDER:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			// Don draft
			if (isDraftOrder == true) {
				orderDTO.orderInfo.approved = -1;
			}
			//cancel request promition order
			isClickingPromotionOrder = false;
			isRequesting = false;
			enableButtonSave(true);
			parent.showDialog(modelEvent.getModelMessage());
			break;
		case ActionEventConstant.GET_PROMOTION_PRODUCT_FROM_SALE_PRODUCT:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			//cancel request promition order
			isClickingPromotionOrder = false;
			isRequesting = false;
			break;
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			enableButtonSave(true);
			super.handleErrorModelViewEvent(modelEvent);
			break;
		case ActionEventConstant.PAYMENT_VANSALE:
			if (parent != null) {
				parent.closeProgressDialog();
			}
			GlobalUtil.showDialogConfirm(this, parent, modelEvent.getModelMessage(), StringUtil.getString(R.string.TEXT_AGREE),
					CONFIRM_PAYMENT_FAIL, null, true);
			break;
		case ActionEventConstant.ACTION_GET_CUSTOMER_DEBIT:
			break;
		case ActionEventConstant.ACTION_CHECK_KEYSHOP_CUSTOMER:
			parent.closeProgressDialog();
			break;
		default:
			// closeProgressDialog();
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		// TODO Auto-generated method stub
		switch (action) {
		case ActionEventConstant.BROADCAST_CHOOSE_PRODUCT_ADD_ORDER_LIST:
			ListFindProductSaleOrderDetailViewDTO orgListProductDTO = (ListFindProductSaleOrderDetailViewDTO) bundle
					.getSerializable(IntentConstants.INTENT_PRODUCTS_ADD_ORDER_LIST);
			if (orgListProductDTO != null) {
				ArrayList<FindProductSaleOrderDetailViewDTO> listProduct = orgListProductDTO.listObject;
				if (listProduct != null && listProduct.size() > 0) {
					// ArrayList<OrderDetailViewDTO> listPromotion = new
					// ArrayList<OrderDetailViewDTO>();
					for (int i = listProduct.size() - 1; i >= 0; i--) {
						FindProductSaleOrderDetailViewDTO findProductDTO = listProduct
								.get(i);
						// add vao ds san pham ban
						OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();
						// ma chuong trinh khuyen mai???
						SaleOrderDetailDTO saleOrderDTO = findProductDTO.saleOrderDetail;
						// bien de kiem tra them moi hay update
						boolean isAddNew = true;
						// kiem tra xem da co san pham nay chua, neu co roi thi
						// cap
						// nhat lai so luong dat
						if (!findProductDTO.hasSelectPrograme) {
							int res = checkExistProducts(findProductDTO.saleOrderDetail.productId);
							if (res >= 0) {
								detailViewDTO = orderDTO.listBuyOrders.get(res);
								orderDTO.numSKU -= detailViewDTO.orderDetailDTO.quantity;
								orderDTO.orderInfo.subtractAmount(detailViewDTO.orderDetailDTO.getAmount());

								//Cap nhat thong tin sp duoc chon
								updateChoosenProductInfo(detailViewDTO, saleOrderDTO, findProductDTO);
//								// So luong
								orderDTO.numSKU += detailViewDTO.orderDetailDTO.quantity;

								// So tien
								detailViewDTO.orderDetailDTO.setAmount(detailViewDTO.orderDetailDTO.calculateAmountProduct());
								orderDTO.orderInfo.addAmount(detailViewDTO.orderDetailDTO.getAmount());

//								//Cap nhat lai cac thong tin KM tu dong
								detailViewDTO.orderDetailDTO.programeCode = findProductDTO.promotionProgrameCode;
								detailViewDTO.orderDetailDTO.programeTypeCode = findProductDTO.programTypeCode;
								detailViewDTO.orderDetailDTO.programeIdBuyView = findProductDTO.promotionProgrameId;
								if(!StringUtil.isNullOrEmpty(detailViewDTO.orderDetailDTO.programeCode)) {
									detailViewDTO.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
								}

								isAddNew = false;
							} else {
								// them moi
							}
						} else {
							int res = checkExistPromotions(
									findProductDTO.saleOrderDetail.productId,
									findProductDTO.saleOrderDetail.programeType,
									findProductDTO.saleOrderDetail.programeCode);
							if (res >= 0) {
								detailViewDTO = orderDTO.listPromotionOrders.get(res);
								//Cap nhat thong tin sp duoc chon
								updateChoosenProductInfo(detailViewDTO, saleOrderDTO, findProductDTO);
								detailViewDTO.orderDetailDTO.isFreeItem = 1;

								isAddNew = false;
							}
						}
						if (isAddNew) {
							//Cap nhat thong tin sp duoc chon
							updateChoosenProductInfo(detailViewDTO, saleOrderDTO, findProductDTO);

							// co chon CT khuyen mai --> sp khuyen mai
							if (findProductDTO.hasSelectPrograme) {
								//sp KM
								detailViewDTO.orderDetailDTO.isFreeItem = 1;
								detailViewDTO.type = OrderDetailViewDTO.FREE_PRODUCT;
//								if (indexPromotionOrder >= 0) {
									orderDTO.listPromotionOrders.add(0, detailViewDTO);
//									indexPromotionOrder++;
//								}

								//Tao ra so suat cho KM tay, huy, doi, tra
								if(detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
										detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
										detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
										detailViewDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
										) {
									SaleOrderPromotionDTO quantityReceived = new SaleOrderPromotionDTO();
									quantityReceived.initHTTMQuantityReceived(detailViewDTO);
									orderDTO.productQuantityReceivedList.add(quantityReceived);
								}
								// san pham ban
							} else {
								//Cap nhat cac KM tu dong
								detailViewDTO.orderDetailDTO.programeCode = findProductDTO.promotionProgrameCode;
								detailViewDTO.orderDetailDTO.programeTypeCode = findProductDTO.programTypeCode;
								detailViewDTO.orderDetailDTO.programeIdBuyView = findProductDTO.promotionProgrameId;
								//Sp ban
								detailViewDTO.orderDetailDTO.isFreeItem = 0;
								if(!StringUtil.isNullOrEmpty(detailViewDTO.orderDetailDTO.programeCode)) {
									detailViewDTO.orderDetailDTO.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
								}

								//So luong
//								orderDTO.numSKU += detailViewDTO.orderDetailDTO.quantity;
								//So tien
								detailViewDTO.orderDetailDTO.setAmount(detailViewDTO.orderDetailDTO.calculateAmountProduct());
								orderDTO.orderInfo.addAmount(detailViewDTO.orderDetailDTO.getAmount());
								orderDTO.listBuyOrders.add(0, detailViewDTO);

								//Update layout
//								OrderProductRow row = new OrderProductRow(parent, null);
//								row.setListner(this);
//								row.updateData(detailViewDTO, isCheckStockTotal);
//								tbProducts.addView(row, 0);
							}
						}
					}
					// cap nhat lai stt cua row
//					for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
//						OrderProductRow row = (OrderProductRow) tbProducts.getChildAt(i);
//						orderDTO.listBuyOrders.get(i).indexParent = i;
//						row.updateOrderNumber();
//					}
					// tinh lai tong tien
//					orderDTO.orderInfo.total = orderDTO.orderInfo.amount - orderDTO.orderInfo.discount;
					// update text tong tien
//					initTotalPriveVAT(orderDTO.orderInfo.total);

					// them vao ds khuyen mai (neu co)
//					displayPromotion();

					// // can tinh lai khuyen mai
					// orderDTO.isNeedCallPromotion = true;
					// disable button
					enableButtonSave(false);
					orderDTO.isChangeData = true;
				}

			}
			break;
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				isShowPrice = GlobalInfo.getInstance().isSysShowPrice();
				isEditPrice = GlobalInfo.getInstance().isSysModifyPrice();
				if (orderDTO.stateOrder != OrderViewDTO.STATE_NEW) {
					isRefreshView = true;
					if (orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
							|| orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_ORDER) {
						this.getOrderForEdit("" + orderDTO.orderInfo.saleOrderId);
					} else if (orderDTO.stateOrder == OrderViewDTO.STATE_VIEW_PO){
						getPO("" + orderDTO.orderInfo.fromPOCustomerId);
					}
				}
				// lay thong tin no cua KH de kiem tra luc tao don vansale
				getCustomerDebit();

				//Neu nhan cap nhat thi disable button save
				enableButtonSave(false);

				// check keyshop cua kh
				checkKeyShopCustomer();
			}
			break;
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}
	}

	/**
	 * Cap nhat thong tin sp duoc chon
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param detailViewDTO
	 * @param findProductDTO
	 */
	public void updateChoosenProductInfo(OrderDetailViewDTO detailViewDTO, SaleOrderDetailDTO saleOrderDTO, FindProductSaleOrderDetailViewDTO findProductDTO) {
		detailViewDTO.orderDetailDTO = saleOrderDTO;

		//check null price
		boolean isNullPrice = (detailViewDTO.orderDetailDTO.priceId <= 0);
		detailViewDTO.orderDetailDTO.isPriceNull = isNullPrice;
		detailViewDTO.orderDetailDTO.isPackagePriceNull = isNullPrice;

		detailViewDTO.orderDetailDTO.synState = ABSTRACT_TABLE.CREATED_STATUS;
		detailViewDTO.orderDetailDTO.updateUser = orderDTO.orderInfo.updateUser;
		detailViewDTO.orderDetailDTO.createUser = orderDTO.orderInfo.createUser;
		detailViewDTO.quantity = findProductDTO.numProduct;
		detailViewDTO.convfact = findProductDTO.convfact;
		//add convfact column
		detailViewDTO.orderDetailDTO.convfact = findProductDTO.convfact;

		QuantityInfo quantityInfo = GlobalUtil.calQuantityFromOrderStr(detailViewDTO.quantity, detailViewDTO.convfact);
		detailViewDTO.orderDetailDTO.quantity = quantityInfo.quantityTotal;
		detailViewDTO.orderDetailDTO.quantityRetail = quantityInfo.quantitySingle;
		detailViewDTO.orderDetailDTO.quantityPackage = quantityInfo.quantityPackage;

		detailViewDTO.productCode = findProductDTO.productCode;
		detailViewDTO.productName = findProductDTO.productName;
		detailViewDTO.isFocus = findProductDTO.mhTT;
		detailViewDTO.stock = findProductDTO.available_quantity;
		detailViewDTO.stockActual = findProductDTO.quantity;
		detailViewDTO.grossWeight = findProductDTO.grossWeight;
		detailViewDTO.remaindStockFormat = GlobalUtil
				.formatNumberProductFlowConvfact(
						detailViewDTO.stock,
						detailViewDTO.convfact);
		detailViewDTO.remaindStockFormatActual = GlobalUtil
				.formatNumberProductFlowConvfact(
						detailViewDTO.stockActual,
						detailViewDTO.convfact);
	}

	/**
	 * Kiem tra ton tai mat hang trong ds hay khong
	 *
	 * @author: TruongHN
	 * @param productId
	 * @return: int
	 * @throws:
	 */
	private int checkExistProducts(int productId) {
		int res = -1; // khong ton tai
		for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
			if (orderDTO.listBuyOrders.get(i).orderDetailDTO.productId == productId) {
				res = i;
				break;
			}
		}
		return res;
	}

	/**
	 * Kiem tra ton tai mat hang KM trong ds hay khong
	 *
	 * @author: TruongHN
	 * @param productId
	 * @return: int
	 * @throws:
	 */
	private int checkExistPromotions(int productId, int programeType,
			String programeCode) {
		int res = -1; // khong ton tai
		for (int i = 0, size = orderDTO.listPromotionOrders.size(); i < size; i++) {
			SaleOrderDetailDTO detail = orderDTO.listPromotionOrders.get(i).orderDetailDTO;
			if (detail.productId == productId
					&& detail.programeType == programeType
					&& detail.programeCode.equals(programeCode)) {
				res = i;
				break;
			}
		}
		return res;
	}

	/**
	 * Link den MH can thuc hien khi click xem chi tiet popup Nhung van de can thuc hien
	 * @author: yennth16
	 * @since: 15:02:30 26-05-2015
	 * @return: void
	 * @throws:
	 */
	public void gotoNoteListView(){
		handleSwitchFragment(new Bundle(),ActionEventConstant.NOTE_LIST_VIEW, SaleController.getInstance());
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		switch (eventType) {
		//canh bao sp km khong co gia va cho phep luu don hang
		case ACTION_PROMOTION_PRODUCT_HAS_NO_PRICE:
			processSaveOrder();
			break;
		case ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE:
			//cancel request promition order
			isClickingPromotionOrder = false;
			break;
		case ActionEventConstant.SHOW_KEYBOARD_CUSTOM:
			parent.showKeyboardCustom(control);
			break;
		case ActionEventConstant.SHOW_KEYBOARD_CUSTOM_UPPER_VIEW:
			//if (windowChangeNumPromotionProduct != null && alertChangeNumPromotionProduct.isShowing()) {
			//	parent.showKeyboardCustom(windowChangeNumPromotionProduct, control);
			//}
			if(keyboardWindowCurrent != null) {
				parent.showKeyboardCustom(keyboardWindowCurrent, control);
			}

			break;
		case ACTION_VIEW_PRODUCT: {
			OrderDetailViewDTO detailDto = (OrderDetailViewDTO) data;
			if (detailDto != null) {
				requestGetProductInfoDetail(String
						.valueOf(detailDto.orderDetailDTO.productId));
				// gotoIntroduceProduct(
				// String.valueOf(detailDto.orderDetailDTO.productId),
				// detailDto.productName, detailDto.productCode);
			}

			break;
		}
		case ACTION_CHANGE_REAL_ORDER: {
			Bundle bundle = (Bundle)data;
			int index = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			QuantityInfo newQuantity = (QuantityInfo) bundle.getSerializable(IntentConstants.INTENT_VALUE);
			// tinh tong
			if (productTotalRow != null && tbProducts != null) {
				reCalTotalPrice(index, newQuantity);
			}
			break;
		}
		case ACTION_CHANGE_PRICE: {
			Bundle bundle = (Bundle)data;
			int index = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			double newPrice = bundle.getDouble(IntentConstants.INTENT_VALUE);
			// tinh tong
			if (productTotalRow != null && tbProducts != null) {
				reCalPrice(index, newPrice, false);
			}
			break;
		}
		case ACTION_CHANGE_PACKAGE_PRICE: {
			Bundle bundle = (Bundle)data;
			int index = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			double newPrice = bundle.getDouble(IntentConstants.INTENT_VALUE);
			// tinh tong
			if (productTotalRow != null && tbProducts != null) {
				reCalPrice(index, newPrice, true);
			}
			break;
		}
		case ACTION_VIEW_PROMOTION: {
			// gotoListProductHasPromotion();
			// xem chi tiet khuyen mai
			OrderDetailViewDTO orderDto = (OrderDetailViewDTO) data;
			if (orderDto != null) {
				if(!StringUtil.isNullOrEmpty(orderDto.orderDetailDTO.programeIdBuyView))// truong hop CTKM thuoc table sp ban
					requestGetPromotionDetail(orderDto.orderDetailDTO.programeCode,orderDto.orderDetailDTO.programeIdBuyView);
				else // truong hop CTKM thuoc table sp km
					requestGetPromotionDetail(orderDto.orderDetailDTO.programeCode,"" +orderDto.orderDetailDTO.programeId);
				orderDTO.isFirstInit = false;
			}
			break;
		}
		case ACTION_DELETE: {
			// xoa mat hang ra khoi don hang: chi xoa khi don hang tao moi hoac
			// don hang tu choi duyet
			if (orderDTO.orderInfo != null
					&& (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
							|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
							|| orderDTO.orderInfo.approved == -1)) {
				OrderDetailViewDTO dto = (OrderDetailViewDTO) data;
				if (dto != null) {
					int indexRow = reCalTotalPrice(dto.indexParent, new QuantityInfo());
					if (indexRow >= 0) {
						// xoa ra khoi danh sach
						orderDTO.listBuyOrders.remove(dto.indexParent);
						// remove row
						tbProducts.removeRowAt(dto.indexParent);
						//.removeViewAt(dto.indexParent);

						// cap nhat lai STT trong ds
						for (int i = dto.indexParent, size = orderDTO.listBuyOrders.size(); i < size; i++) {
							OrderProductRow row = (OrderProductRow) tbProducts.getRowAt(i);
							if (row != null) {
								orderDTO.listBuyOrders.get(i).indexParent = i;
								row.updateOrderNumber();
							}
						}

						// kiem tra neu la row cuoi thi cap nhat row tong
						if (orderDTO.listBuyOrders.size() == 0) {
							orderDTO.numSKU = 0;
							orderDTO.orderInfo.setAmount(0);
							productTotalRow.updateTotalValue(orderDTO.orderInfo.getAmount(), orderDTO.numSKU);
						}
					}
				}
			} else {
				// thong bao loi
			}
			break;
		}
		case ACTION_CHANGE_PROMOTION_PRODUCT: {
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO) data;

			Iterator<Long> it = orderDTO.listPromotionChange.keySet().iterator();

			while (it.hasNext()) {
				Long md = it.next();
				ArrayList<OrderDetailViewDTO> listProductChange = orderDTO.listPromotionChange
						.get(md);

				if (listProductChange.size() > 0) {
					OrderDetailViewDTO promotionProduct = listProductChange.get(0);
					if (promotionProduct.keyList.longValue() == orderDetailDto.keyList.longValue()) {
						showPopupChangePromotionProduct(listProductChange, orderDetailDto);
						break;
					}
				}
			}

			break;
		}
		case ACTION_DELETE_PROMOTION: {
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO) data;
			if (orderDetailDto != null
					&& (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
					|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
					|| orderDTO.orderInfo.approved == -1)) {
				// xoa mat hang khuyen mai thuoc CTKM/CTTB tu chon
				orderDTO.numSKUPromotion -= orderDetailDto.orderDetailDTO.quantity;

				if (orderDetailDto.indexParent >= 0
						&& orderDetailDto.indexParent < orderDTO.listPromotionOrders.size()) {

					// xoa ra khoi danh sach
					orderDTO.listPromotionOrders.remove(orderDetailDto.indexParent);

					// remove row
					tbPromotionProducts.removeRowAt(orderDetailDto.indexParent);

					// cap nhat lai STT trong ds
					for (int i = orderDetailDto.indexParent, size = orderDTO.listPromotionOrders.size(); i < size; i++) {
						OrderPromotionRow row = (OrderPromotionRow) tbPromotionProducts.getRowAt(i);
						if (row != null) {
							orderDTO.listPromotionOrders.get(i).indexParent = i;
							row.updateNumberRow();
						}
					}

					//Cap nhat lai index cua KM don hang
//					if(indexPromotionOrder > 0) {
//						indexPromotionOrder--;
//					}

					//Xoa 1 so suat cua KM tay
					if(orderDetailDto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_MANUAL ||
							orderDetailDto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CANCEL ||
							orderDetailDto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_CHANGE ||
							orderDetailDto.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_RETURN
							) {
						for(SaleOrderPromotionDTO quantityReceived : orderDTO.productQuantityReceivedList) {
							if(orderDetailDto.orderDetailDTO.programeCode.equals(quantityReceived.promotionProgramCode)) {
								orderDTO.productQuantityReceivedList.remove(quantityReceived);
								break;
							}
						}
					}
				}

				// update text tong tien
				initTotalPriveVAT();
				// // can tinh lai khuyen mai
				enableButtonSave(false);
				orderDTO.isChangeData = true;
			}

			break;
		}
		case ACTION_DELETE_PROMOTION_ACCUMULATION: {
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO) data;
			if (orderDetailDto != null
					&& (orderDTO.stateOrder == OrderViewDTO.STATE_NEW
					|| orderDTO.stateOrder == OrderViewDTO.STATE_EDIT
					|| orderDTO.orderInfo.approved == -1 )) {
				GlobalUtil.showDialogConfirm(this, parent, StringUtil
						.getString(R.string.CONFIRM_DELETE_ACCUMULATION_PROMOTION),
						StringUtil.getString(R.string.TEXT_AGREE),
						CONFIRM_DELETE_ACCUMULATION_OK, StringUtil
								.getString(R.string.TEXT_CANCEL),
						CONFIRM_DELETE_ACCUMULATION_CANCEL, orderDetailDto);
			}

			break;
		}
		case CONFIRM_DELETE_ACCUMULATION_OK: {
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO) data;
			if (orderDetailDto.indexParent >= 0
					&& orderDetailDto.indexParent < orderDTO.listPromotionAccumulation.size()) {

				// xoa ra khoi danh sach
				orderDTO.listPromotionAccumulation.remove(orderDetailDto.indexParent);
				calPriceKeyShop();
				layoutOrder();

				parent.showDialog(StringUtil
						.getString(R.string.DELETE_ACCUMULATION_PROMOTION_SUCCESS));
			}
			break;
		}
		case CONFIRM_DELETE_ACCUMULATION_CANCEL: {
			break;
		}
		case ACTION_CANCEL_SAVE_ORDER: {
			//cancel request promition order
			isClickingPromotionOrder = false;
			break;
		}
		case ACTION_SAVE_ORDER_OK: {
			// Disable -> do not allow do action again
			enableButtonSave(false);

			// Don draft
			if (orderDTO.orderInfo.approved == -1) {
				isDraftOrder = true;
			}

			// don hang ban nhung chua tra
			orderDTO.orderInfo.type = 1;
			orderDTO.orderInfo.totalWeight += orderDTO.promotionTotalWeight;
			orderDTO.orderInfo.adddDiscount(orderDTO.getPromotionDiscount());
			// vansale cung cap nhat approve = 0
//			if (this.orderType == CREATE_ORDER_FOR_PRESALE) {
				//orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_PRESALE;
//			orderDTO.orderInfo.approved = 0;
//			} else {
				//orderDTO.orderInfo.orderType = SALE_ORDER_TABLE.ORDER_TYPE_VANSALE;
//				orderDTO.orderInfo.approved = 1;
//			}

			// luu hoac tao don hang
			if(orderDTO.lstWrongProgrameId.isEmpty()) {
				orderDTO.orderInfo.approved = 0;
			} else {
				//Don hang loi KM
				orderDTO.orderInfo.approved = 2;
				orderDTO.orderInfo.description = StringUtil.getString(R.string.PROMOTION_IMPORT_CODE);
			}
			//Clear cac ds khi phat sinh luu hay sua don hang
			orderDTO.listProduct.clear();
//			orderDTO.listPromoDetail.clear();
			orderDTO.listRptCttPay.clear();

			if (!isDraftOrder) {
				orderDTO.orderInfo.updateDate = DateUtils.now();
				orderDTO.orderInfo.updateUser = GlobalInfo.getInstance().getProfile()
						.getUserData().getUserCode();
			}
			if (orderDTO.stateOrder == OrderViewDTO.STATE_EDIT) {
				// sua -> chuyen
				requestEditOrder();
			} else if(orderDTO.stateOrder == OrderViewDTO.STATE_NEW){
				requestCreateOrder();
			}
			break;
		}
		case ACTION_CHANGE_MODE_SAVE_ORDER_OK: {
			cbOrder.setChecked(!cbOrder.isChecked());
			onClick(cbOrder);
			break;
		}
		case ACTION_CANCEL_CHANGE_MODE_SAVE_ORDER: {
			break;
		}
		case ACTION_AGRRE_BACK: {
			// dong y back tro lai

			// TamPQ: CR0075: neu dang la kh ngoai tuyen thi tu dong update
			// action_log ghe tham khi thoat man hinh dat hang
			// int isOr = orderDTO.orderInfo.isVisitPlan == 0 ? 1 : 0;
			ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			if (actionLog != null && actionLog.isOr == 1
					&& StringUtil.isNullOrEmpty(actionLog.endTime)) {
				parent.requestUpdateActionLog("0", "0", null, this);
			}

			if (orderDTO.isBackToRemainOrder) {
				RemainProductView remainFragment = (RemainProductView) parent
						.getFragmentManager().findFragmentByTag(
								GlobalUtil.getTag(RemainProductView.class));
				if (remainFragment != null) {
					remainFragment.receiveBroadcast(
							ActionEventConstant.BROADCAST_ORDER_VIEW, null);
				}
				GlobalUtil.popBackStack(this.parent);
			} else {
				if (orderDTO.isChangeData) {
					GlobalUtil.popBackStack(this.parent);
				}

				// GlobalUtil.popBackStack(this.parent);
			}

			break;
		}
		case CONFIRM_DRAFT_ORDER_OK: {
			if (data != null && data instanceof Integer) {
				int action = (Integer) data;
				saveOrderDraft(action);
			}
			break;
		}
		case CONFIRM_DRAFT_ORDER_CANCEL: {
			if (data != null && data instanceof Integer) {
				int action = (Integer) data;
				if (action == -1) {
					handleAgreeBack();
				} else {
					parent.switchMenuLastSaveAction();
				}
			}
			break;
		}
		case CONFIRM_EXIT_ORDER_VIEW_CANCEL: {
			break;
		}
		case CONFIRM_EXIT_ORDER_VIEW_VISIT_OK: {
			handleAgreeBack();
			break;
		}
		case ACTION_CANCEL_BACK: {
			// khong dong y back
			break;
		}
		case ACTION_END_VISIT_OK: {
			// ket thuc ghe tham
//			endVisitCustomer();
//			gotoNoteListView();
			parent.showPopupProblemsFeedBack(new PopupProblemsListener() {
				@Override
				public void closePopup() {
					endVisitCustomer();
				}

				@Override
				public void gotoCustomerInfo() {
					endVisitCustomer();
					gotoNoteListView();
				}
			});
			break;
		}
		case ACTION_END_VISIT_CANCEL: {
			gotoCustomerInfo();
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_PROMOTION: {
			Bundle bundle = (Bundle) data;
			int indexParent = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			QuantityInfo newQuantity = new QuantityInfo();
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO)bundle.getSerializable(IntentConstants.INTENT_VALUE);
			OrderPromotionRow row = (OrderPromotionRow) bundle.getSerializable(IntentConstants.INTENT_TABLE_ROW);
			ArrayList<OrderDetailViewDTO> listProductChange = new ArrayList<OrderDetailViewDTO>();
			//Sp KM co sp doi
			if(orderDetailDto.keyList != null) {
				listProductChange = orderDTO.listPromotionChange.get(orderDetailDto.keyList);

				if(listProductChange != null && listProductChange.size() > 0) {
					newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
				}
			//Sp KM ko co sp doi
			} else {
				listProductChange.add(orderDetailDto);
				newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
			}

			// tinh tong
			if (productTotalRow != null && tbPromotionProducts != null) {
				reCalQuantityPromotion(row, indexParent, newQuantity);
				//Neu co ton tai trong KM tich luy thi tinh toan lai
				if (orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						&& existInAccumulationOrder(orderDetailDto)) {
					layoutOrder();
					orderDetailDto.row.etTotal.requestFocus();
				}
			}
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ORDER: {
			Bundle bundle = (Bundle) data;
			int indexParent = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			int indexChild = bundle.getInt(IntentConstants.INTENT_INDEX_CHILD);
			QuantityInfo newQuantity = new QuantityInfo();
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO)bundle.getSerializable(IntentConstants.INTENT_VALUE);
			OrderPromotionRow row = (OrderPromotionRow) bundle.getSerializable(IntentConstants.INTENT_TABLE_ROW);

			ArrayList<OrderDetailViewDTO> listProductChange = new ArrayList<OrderDetailViewDTO>();
			//Sp KM co sp doi
			if(orderDetailDto.keyList != null) {
				listProductChange = orderDTO.listPromotionChange.get(orderDetailDto.keyList);

				if(listProductChange != null && listProductChange.size() > 0) {
					newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
				}
			//Sp KM ko co sp doi
			} else {
				listProductChange.add(orderDetailDto);
				newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
			}

			// tinh tong
			if (productTotalRow != null && tbListPromotionOrder != null) {
				reCalQuantityPromotionOrder(row, indexParent, indexChild, newQuantity);
				//Neu co ton tai trong KM tich luy thi tinh toan lai
				if (orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						&& existInAccumulationOrder(orderDetailDto)) {
					layoutOrder();
					orderDetailDto.row.etTotal.requestFocus();
				}
			}
			break;
		}
		case ACTION_CHANGE_PROMOTION_FOR_PROMOTION_ORDER: {
			// thay doi chuong trinh khuyen mai dang khuyen mai cho don hang
			showPopupChangePromotionForPromotionTypeOrder((OrderDetailViewDTO) data);
			break;
		}
		//Ko cho chuc nang doi tien
		case ACTION_CHANGE_REAL_MONEY_PROMOTION: {
//			OrderDetailViewDTO dto = (OrderDetailViewDTO) data;
//			OrderPromotionRow promotionRow = (OrderPromotionRow)control;
//			// Thay doi so tien cua KM 19, 20 & KM tien, % cho sp
////			reCalMoneyPromotion(dto);
//			reCalMoneyKeyShop(dto,promotionRow);
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_PROMOTION_ACCUMULATION: {
			Bundle bundle = (Bundle) data;
			int indexParent = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			int indexChild = bundle.getInt(IntentConstants.INTENT_INDEX_CHILD);
			QuantityInfo newQuantity = new QuantityInfo();
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO)bundle.getSerializable(IntentConstants.INTENT_VALUE);
			OrderPromotionRow row = (OrderPromotionRow) bundle.getSerializable(IntentConstants.INTENT_TABLE_ROW);

			ArrayList<OrderDetailViewDTO> listProductChange = new ArrayList<OrderDetailViewDTO>();
			//Sp KM co sp doi
			if(orderDetailDto.keyList != null) {
				listProductChange = orderDTO.listPromotionChange.get(orderDetailDto.keyList);

				if(listProductChange != null && listProductChange.size() > 0) {
					newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
				}
				//Sp KM ko co sp doi
			} else {
				listProductChange.add(orderDetailDto);
				newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
			}

			// tinh tong
			if (productTotalRow != null && tbListPromotionAccumulation != null) {
				reCalQuantityPromotionAccumulation(row, indexParent, indexChild, newQuantity);
				//Neu co ton tai trong KM tich luy thi tinh toan lai
				if (orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						&& existInAccumulationOrder(orderDetailDto)) {
					layoutOrder();
					orderDetailDto.row.etTotal.requestFocus();
				}
			}
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_PROMOTION_NEW_OPEN: {
			Bundle bundle = (Bundle) data;
			int indexParent = bundle.getInt(IntentConstants.INTENT_INDEX_PARENT);
			int indexChild = bundle.getInt(IntentConstants.INTENT_INDEX_CHILD);
			QuantityInfo newQuantity = new QuantityInfo();
			OrderDetailViewDTO orderDetailDto = (OrderDetailViewDTO)bundle.getSerializable(IntentConstants.INTENT_VALUE);
			OrderPromotionRow row = (OrderPromotionRow) bundle.getSerializable(IntentConstants.INTENT_TABLE_ROW);

			ArrayList<OrderDetailViewDTO> listProductChange = new ArrayList<OrderDetailViewDTO>();
			//Sp KM co sp doi
			if(orderDetailDto.keyList != null) {
				listProductChange = orderDTO.listPromotionChange.get(orderDetailDto.keyList);

				if(listProductChange != null && listProductChange.size() > 0) {
					newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
				}
				//Sp KM ko co sp doi
			} else {
				listProductChange.add(orderDetailDto);
				newQuantity = calculateQuantity(listProductChange, orderDetailDto, row);
			}

			// tinh tong
			if (productTotalRow != null && tbListPromotionNewOpen != null) {
				reCalQuantityPromotionNewOpen(row, indexParent, indexChild, newQuantity);
				if (orderType == CREATE_ORDER_FOR_VANSALE
						&& orderDTO.stateOrder == OrderViewDTO.STATE_NEW
						&& existInAccumulationOrder(orderDetailDto)) {
					layoutOrder();
					orderDetailDto.row.etTotal.requestFocus();
				}
			}
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_PROMOTION_IN_POPUP: {
			ChangeNumPromotionProductRow row = (ChangeNumPromotionProductRow) control;
 			OrderDetailViewDTO myData = (OrderDetailViewDTO) data;
			int newQuantity = 0;
			if(orderType == CREATE_ORDER_FOR_VANSALE && myData.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
 				newQuantity = checkAndGetQuantityAcumulation(row.edNumberPromotion, myData);
 			} else {
 				newQuantity = (int) checkAndGetQuantity(row.edNumberPromotion, myData).quantityTotal;
 			}
//			myData.orderDetailDTO.quantity = newQuantity;
			calculateTotalQuantityUsingWhenChangeValueInPopup(myData);
//			myData.totalOrderQuantity.orderDetailDTO.quantity -= myData.orderDetailDTO.quantity + newQuantity;
			myData.orderDetailDTO.quantity = newQuantity;
			row.checkStockTotal(myData);
			break;
		}
		case ACTION_VIEW_ORDER_PROMOTION: {
			showPopupPromotionHasDiscount((OrderDetailViewDTO) data);
			break;
		}
		case CONFIRM_PAYMENT_OK: {
			if (alertProductDetail != null) {
				alertProductDetail.dismiss();
			}
			parent.showProgressDialog(StringUtil.getString(R.string.TEXT_SAVING_OWE));
			// thuc hien luu thanh toan
			calPayment();
			break;
		}
		case CONFIRM_PAYMENT_CANCEL: {
			break;
		}
		case CONFIRM_PAYMENT_FAIL: {
			if(isWrongPromotionOrder) {
				GlobalUtil.popBackStack(this.parent);
			} else {
				handleAfterCreateOrder(orderDTO.customer.getCustomerId());
			}
			break;
		}
		case CONFIRM_NOT_VALIDATE_PAYMENT_CANCEL: {
			break;
		}
		case ACTION_CHANGE_REAL_QUANTITY_RECEIVED: {
			Bundle bundle = (Bundle) data;
			OrderDetailViewDTO dto = (OrderDetailViewDTO) bundle.getSerializable(IntentConstants.INTENT_DATA);
			dto.quantityReceived.isShowed = false;
			int indexPromotionProduct = isContainOldProduct(dto, orderDTO.listPromotionOrders);
			int indexPromotionOrder = isContainOldProduct(dto, orderDTO.listPromotionOrder);
			int indexPromotionNewOpen = isContainOldProduct(dto, orderDTO.listPromotionNewOpen);

			ArrayList<OrderDetailViewDTO> listPromotionTemp = new ArrayList<OrderDetailViewDTO>();
			//Xu ly thay the sp chon cho KM sp
			if(indexPromotionProduct >= 0) {
				listPromotionTemp = orderDTO.listPromotionOrders;
			}

			//Xu ly thay the sp chon cho KM don hang
			if(indexPromotionOrder >= 0) {
				listPromotionTemp = orderDTO.listPromotionOrder.get(indexPromotionOrder).listPromotionForPromo21;
			}

			//Xu ly thay sp chon KM mo moi
			if(indexPromotionNewOpen >= 0) {
				listPromotionTemp = orderDTO.listPromotionNewOpen.get(indexPromotionNewOpen).listPromotionForPromo21;
			}

			//Update so luong o ds sp KM sp
			int indexRemove = -1;
			ArrayList<OrderDetailViewDTO> listPromotionHasQuantity = new ArrayList<OrderDetailViewDTO>();
			ArrayList<OrderDetailViewDTO> listRemove = new ArrayList<OrderDetailViewDTO>();
			for(OrderDetailViewDTO rowDTO: listPromotionTemp) {
				if (rowDTO.orderDetailDTO.programeCode.equals(dto.quantityReceived.promotionProgramCode)
						&& rowDTO.orderDetailDTO.productGroupId == dto.quantityReceived.productGroupId
						&& rowDTO.orderDetailDTO.groupLevelId == dto.quantityReceived.groupLevelId) {

					QuantityInfo quantityInfo = new QuantityInfo();
					int newQuantity = rowDTO.quantityReceived.quantityReceived
							* rowDTO.orderDetailDTO.maxQuantityFree
							/ rowDTO.quantityReceived.quantityReceivedMax;

					quantityInfo.quantityPackage = 0;
					quantityInfo.quantitySingle = newQuantity;
					quantityInfo.quantityTotal = newQuantity;
					if(indexPromotionProduct >= 0) {
						reCalQuantityPromotion(rowDTO.row, rowDTO.indexParent, quantityInfo);
					}

					//Xu ly thay the sp chon cho KM don hang
					if(indexPromotionOrder >= 0) {
						reCalQuantityPromotionOrder(rowDTO.row, rowDTO.indexParent, rowDTO.indexChild, quantityInfo);
					}

					//Xu ly thay sp chon KM mo moi
					if(indexPromotionNewOpen >= 0) {
						reCalQuantityPromotionNewOpen(rowDTO.row, rowDTO.indexParent, rowDTO.indexChild, quantityInfo);
					}

					//Remove cac sp tuy chon khac
					if(rowDTO.keyList != null) {
						ArrayList<OrderDetailViewDTO> listPromotionChange = orderDTO.listPromotionChange.get(rowDTO.keyList);

						if(listPromotionChange != null && listPromotionChange.size() > 0) {
							for(OrderDetailViewDTO item: listPromotionChange){
								//Cap nhat so suat cho cac sp option
								item.getQuantityReceived(orderDTO.productQuantityReceivedList);
								int quantity = item.quantityReceived.quantityReceived
										* item.orderDetailDTO.maxQuantityFree
										/ item.quantityReceived.quantityReceivedMax;
								item.orderDetailDTO.quantity = quantity;

								if(indexRemove == -1) {
									indexRemove = listPromotionTemp.indexOf(rowDTO);
									listRemove.addAll(listPromotionChange);
									listPromotionHasQuantity.add(rowDTO);
								}

							}
						}
					}
				}
			}

			//Neu co sp de remove
			if(indexRemove >= 0) {
				listPromotionTemp.removeAll(listRemove);
				listPromotionTemp.addAll(indexRemove, listPromotionHasQuantity);
			}

			layoutOrder();
			dto.row.etQuantityReceived.requestFocus();

			break;
		}
		case ACTION_SAVE_PROMOTION_MANUAL: {
			// kiem tra va luu don hang
			//Thong bao neu co sp ko co gia
			String listMissingPrice = getListPromotionProductMissingPrice();
			if(!StringUtil.isNullOrEmpty(listMissingPrice)) {
				Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil.getStringAndReplace(
						R.string.TEXT_PROMOTION_PRODUCT_HAS_NO_PRICE,
						listMissingPrice),
								StringUtil.getString(R.string.TEXT_AGREE),
								ACTION_PROMOTION_PRODUCT_HAS_NO_PRICE, StringUtil
								.getString(R.string.TEXT_DENY),
								ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE, null);
				if (dialog != null) {
					dialog.setOnDismissListener(dismissDialogPromotionListener);
				}
			} else {
				processSaveOrder();
			}
			break;
		}
		case ACTION_CANCEL_PROMOTION_MANUAL: {
			//cancel request promition order
			isClickingPromotionOrder = false;
			break;
		}
		case ACTION_SAVE_PROMOTION_CHANGE_QUANTITY: {
			// kiem tra va luu don hang
			String listProgrameManual = orderDTO.hasProgramePromotionManual();
			if(!StringUtil.isNullOrEmpty(listProgrameManual)){
				// truong hop co KM huy, doi ,tra, trung bay, km tay thi canh bao
				Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil
						.getStringAndReplace(
								R.string.TEXT_CONFIRM_ORDER_HAS_PROMOTION_MANUAL,
								listProgrameManual),
								StringUtil.getString(R.string.TEXT_AGREE),
								ACTION_SAVE_PROMOTION_MANUAL, StringUtil
								.getString(R.string.TEXT_DENY),
								ACTION_CANCEL_PROMOTION_MANUAL, null);

				if (dialog != null) {
					dialog.setOnDismissListener(dismissDialogPromotionListener);
				}
			} else {
				//Thong bao neu co sp ko co gia
				String listMissingPrice = getListPromotionProductMissingPrice();
				if(!StringUtil.isNullOrEmpty(listMissingPrice)) {
					Dialog dialog = GlobalUtil.showDialogConfirmGetDialog(this, "", StringUtil.getStringAndReplace(
							R.string.TEXT_PROMOTION_PRODUCT_HAS_NO_PRICE,
							listMissingPrice),
									StringUtil.getString(R.string.TEXT_AGREE),
									ACTION_PROMOTION_PRODUCT_HAS_NO_PRICE, StringUtil
									.getString(R.string.TEXT_DENY),
									ACTION_CANCEL_PROMOTION_PRODUCT_HAS_NO_PRICE, null);
					if (dialog != null) {
						dialog.setOnDismissListener(dismissDialogPromotionListener);
					}
				} else {
					processSaveOrder();
				}
			}
			break;
		}
		case ACTION_CANCEL_PROMOTION_CHANGE_QUANTITY: {
			//cancel request promition order
			isClickingPromotionOrder = false;
			break;
		}
		case ACTION_SYN_PROMOTION_OK: {
			parent.showProgressDialog(StringUtil.getString(R.string.PROMOTION_SYN), false);
			parent.requestSynProgrameData(GlobalInfo.getInstance().getLstWrongProgrameId().get(0));
			break;
		}
		case ACTION_SYN_PROMOTION_CANCEL: {
			//Dang sai khuyen mai hien button co ten "Luu"
			enableButtonSave(true);
			btSaveAndSend.setText(StringUtil.getString(R.string.TEXT_SAVE));
			break;
		}
		default:
			super.onEvent(eventType, control, data);
			break;
		}

	}

	/**
	 * Kiem tra 1 sp co trong KM tich luy hay ko
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: boolean
	 * @throws:
	 * @param orderDetailDto
	 * @return
	 */
	private boolean existInAccumulationOrder(OrderDetailViewDTO orderDetailDto) {
		boolean result = false;

		for(OrderDetailViewDTO accuPromotion : orderDTO.listPromotionAccumulation) {
			for(OrderDetailViewDTO accuProduct: accuPromotion.listPromotionForPromo21) {
				if(orderDetailDto.orderDetailDTO.productId == accuProduct.orderDetailDTO.productId) {
					result = true;
					break;
				}
			}

			if(result == true) {
				break;
			}

		}

		return result;
	}

	/**
	 * @author: duongdt3
	 * @since: 10:11:26 10 Apr 2015
	 * @return: void
	 * @throws:
	 * @param index
	 * @param newPrice
	 */
	private int reCalPrice(int indexRow, double newPrice, boolean isPackagePrice) {
		int res = -1;
		// tinh tong
		if (indexRow >= 0 && indexRow < orderDTO.listBuyOrders.size()) {
			OrderDetailViewDTO selectedDetail = orderDTO.listBuyOrders.get(indexRow);
			res = indexRow;

			//set is price null
			if (isPackagePrice) {
//				selectedDetail.orderDetailDTO.isPackagePriceNull = (newPrice == NULL_PRICE);
				selectedDetail.orderDetailDTO.isPackagePriceNull = BigDecimal.valueOf(newPrice).compareTo(BigDecimal.valueOf(NULL_PRICE)) == 0;
			} else{
				selectedDetail.orderDetailDTO.isPriceNull = BigDecimal.valueOf(newPrice).compareTo(BigDecimal.valueOf(NULL_PRICE)) == 0;
//				selectedDetail.orderDetailDTO.isPriceNull = (newPrice == NULL_PRICE);
			}

			//gia dang null
//			if (newPrice == NULL_PRICE) {
			if (BigDecimal.valueOf(newPrice).compareTo(BigDecimal.valueOf(NULL_PRICE)) == 0) {
				//chuyen gia ve khong
				newPrice = 0;
			}

			orderDTO.orderInfo.setDiscount(0);
			if (selectedDetail != null) {
				double oldAmount = selectedDetail.orderDetailDTO.getAmount();
				if (isPackagePrice) {
					selectedDetail.isModifyPackagePrice = true;
					selectedDetail.orderDetailDTO.packagePrice = newPrice;
				} else{
					selectedDetail.isModifyPrice = true;
					selectedDetail.orderDetailDTO.price = newPrice;
				}

				double newAmount = selectedDetail.orderDetailDTO.calculateAmountProduct();
				selectedDetail.orderDetailDTO.setAmount(newAmount);

				// tinh lai tong
				orderDTO.orderInfo.subtractAmount(oldAmount);
				orderDTO.orderInfo.addAmount(newAmount);
				productTotalRow.updateTotalValue(orderDTO.orderInfo.getAmount(), orderDTO.numSKU);

				// update thanh tien cua row dc chon
				OrderProductRow row = (OrderProductRow) tbProducts.getRowAt(indexRow);
				if (row != null) {
					row.updateAmount(newAmount);
				}
				initTotalPriveVAT();

				// can tinh lai khuyen mai
				enableButtonSave(false);
				orderDTO.isChangeData = true;
			}
		}
		return res;
	}

	/**
	 * Luu don hang tam
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	public void saveOrderDraft(int index) {
		indexMenu = index;
		orderDTO.orderInfo.approved = -1; // 0: tao moi chua chuyen
		// this.getCurrentDateTimeServer();
		if (orderDTO.stateOrder == OrderViewDTO.STATE_EDIT) {
			// sua -> chuyen
			requestEditOrder();
		} else if(orderDTO.stateOrder == OrderViewDTO.STATE_NEW){
			requestCreateOrder();
		}
	}

	/**
	 *
	 * hien thi popup thay doi chuong trinh khuyen mai cho loai khuyen mai theo
	 * don hang
	 *
	 * @author: HaiTC3
	 * @param currentData
	 * @return: void
	 * @throws:
	 * @since: May 10, 2013
	 */
	public void showPopupChangePromotionForPromotionTypeOrder(OrderDetailViewDTO currentData) {
//		if (alertOrderPromotionList == null) {
			LayoutInflater inflater = this.parent.getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_select_promotion_for_promotion_type_order, null);

			TextView tvHeaderTitle = (TextView) view.findViewById(R.id.tvHeaderTitle);
			tvHeaderTitle.setText(StringUtil.getString(R.string.TEXT_HEADER_CHOOSE_PROMOTION_FOR_PROMOTION_TYPE_ORDER));
			tbPromotionListView = (DMSTableView) view.findViewById(R.id.tbPromotionListView);
			tbPromotionListView.setVisibility(View.VISIBLE);
			Button btClosePopup = (Button) view.findViewById(R.id.btClosePopupPromotion);
			btClosePopup.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					if (alertOrderPromotionList.isShowing()) {
						alertOrderPromotionList.dismiss();
					}
				}
			});

			initHeaderTable(tbPromotionListView, new SelectPromotionForPromotionTypeOrder(parent, this));

			tbPromotionListView.setNumItemsPage(10);

			Builder build = new AlertDialog.Builder(parent,R.style.CustomDialogTheme);
			build.setView(view);
			alertOrderPromotionList = build.create();
			// alertProductDetail.setCancelable(false);

			Window window = alertOrderPromotionList.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
//		}

		if (isRefresh) {
			int pos = 1;
			HashMap<String, String> promotionCodeMap = new HashMap<String, String>();
			for (OrderDetailViewDTO promotionOrder : orderDTO.listPromotionForOrderChange) {
				String promotionCode = promotionCodeMap.get(promotionOrder.orderDetailDTO.programeCode);
				if (StringUtil.isNullOrEmpty(promotionCode) && (promotionOrder.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER
						|| promotionOrder.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ZV21)) {
					promotionCodeMap.put(promotionOrder.orderDetailDTO.programeCode, promotionOrder.orderDetailDTO.programeCode);
					SelectPromotionForPromotionTypeOrder row = new SelectPromotionForPromotionTypeOrder(
							parent, this);
					row.setClickable(true);
					row.setTag(promotionOrder);
					row.renderLayout(pos, promotionOrder);
					if (currentData.orderDetailDTO.programeCode.equals(promotionOrder.orderDetailDTO.programeCode)) {
						row.updateLayoutSelected();
					}
					pos++;
					tbPromotionListView.addRow(row);
				}
			}
			String message = Constants.STR_BLANK;
			if (orderDTO.listPromotionForOrderChange.size() == 0) {
				message = StringUtil.getString(R.string.TEXT_NOTIFY_NOT_HAVE_PROGRAME);
				SelectProgrameForProduct row = new SelectProgrameForProduct(
						parent, tbPromotionListView);
				row.setClickable(true);
				pos++;
				tbPromotionListView.addRow(productTotalRow);
			}
		}
		if (!alertOrderPromotionList.isShowing()) {
			alertOrderPromotionList.show();
		}
	}

	/**
	 *
	 * Hien thi popup cac CTKM co discount Amount cho sp tuong ung
	 *
	 * @author: dungnt19
	 * @param currentData
	 * @return: void
	 * @throws:
	 * @since: May 10, 2013
	 */
	public void showPopupPromotionHasDiscount(OrderDetailViewDTO currentData) {
		LayoutInflater inflater = this.parent.getLayoutInflater();
		View view = inflater
				.inflate(R.layout.layout_select_promotion_for_promotion_type_order, null);

		TextView tvHeaderTitle = (TextView) view.findViewById(R.id.tvHeaderTitle);
		tvHeaderTitle.setText(StringUtil.getString(R.string.TEXT_HEADER_PROMOTION_LIST_GET));
		tbPromotionListView = (DMSTableView) view.findViewById(R.id.tbPromotionListView);
		tbPromotionListView.setVisibility(View.VISIBLE);
		Button btClosePopup = (Button) view.findViewById(R.id.btClosePopupPromotion);
		btClosePopup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (alertDiscountPromotionList.isShowing()) {
					alertDiscountPromotionList.dismiss();
				}
			}
		});

		initHeaderTable(tbPromotionListView, new SelectPromotionForPromotionTypeOrder(parent, this));
		tbPromotionListView.setNumItemsPage(10);

		Builder build = new AlertDialog.Builder(parent,
				R.style.CustomDialogTheme);
		build.setView(view);
		alertDiscountPromotionList = build.create();

		Window window = alertDiscountPromotionList.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255, 255,
				255)));
		window.setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		window.setGravity(Gravity.CENTER);

		if (isRefresh) {
			HashMap<String, SaleOrderPromoDetailDTO> discountPromotionList = new HashMap<String, SaleOrderPromoDetailDTO>();
			for(SaleOrderPromoDetailDTO promoDetail: currentData.listPromoDetail) {
				SaleOrderPromoDetailDTO discountPromotion = discountPromotionList.get(promoDetail.programCode);
				if(discountPromotion != null) {
					discountPromotion.discountAmount += promoDetail.discountAmount;
				} else {
					discountPromotion = new SaleOrderPromoDetailDTO();
					discountPromotion.programCode = promoDetail.programCode;
					discountPromotion.programName = promoDetail.programName;
					discountPromotion.discountAmount = promoDetail.discountAmount;
					discountPromotionList.put(promoDetail.programCode, discountPromotion);
				}
			}
			int pos = 1;
			for(SaleOrderPromoDetailDTO promoDetail: discountPromotionList.values()) {
				SelectPromotionForPromotionTypeOrder row = new SelectPromotionForPromotionTypeOrder(parent, this);
				row.renderLayout(pos, promoDetail);
				pos++;
				tbPromotionListView.addRow(row);
			}

		}
		if (!alertDiscountPromotionList.isShowing()) {
			alertDiscountPromotionList.show();
		}
	}

	/**
	 * Thuc hien ket thuc ghe tham KH
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */

	public void endVisitCustomer() {
		parent.requestUpdateActionLog("0", null, null, parent);

		// kiem tra neu truoc do con man hinh kiem hang ton thi pop ra
		if (orderDTO.isBackToRemainOrder) {
			GlobalUtil.popBackStack(this.parent);
		}

		// pop ko cho quay ve man hinh tao don hang nua
		GlobalUtil.popBackStack(this.parent);
	}

	/**
	 * Chuyen qua man hinh thong tin kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */

	private void gotoCustomerInfo() {
		// kiem tra neu truoc do con man hinh kiem hang ton thi pop ra
		if (orderDTO.isBackToRemainOrder) {
			GlobalUtil.popBackStack(this.parent);
		}

		// pop ko cho quay ve man hinh tao don hang nua
		// TamPQ: set 1 frag ko cho back ve DSKH load lai DS
		CustomerListView frag = (CustomerListView)parent
				.getFragmentManager().findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
		if (frag != null) {
			frag.isBackFromPopStack = true;
		}
		GlobalUtil.popBackStack(this.parent);

		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID,
				orderDTO.customer.getCustomerId());
		// bunde.putBoolean(IntentConstants.INTENT_IS_FROM_CREATE_ORDER, true);
		e.viewData = bunde;
		e.action = ActionEventConstant.GO_TO_CUSTOMER_INFO;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	/**
	 *
	 * show list product has promotion
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void showPopupChangePromotionProduct(
			ArrayList<OrderDetailViewDTO> listProductChange,
			OrderDetailViewDTO orderDetailDto) {

		this.listProductChange = listProductChange;
		productSelected = orderDetailDto;

		//Hien thi popup chon sp thay doi
		if (!isPromotionNAorNB(listProductChange)) {
			showPopupSelectPromotionProduct();
		//Hien thi popup nhap so luong sp thay doi
		} else {
			realOrderMap = new HashMap<Integer, Integer>();
			oldRealOrderMap = new HashMap<Integer, Integer>();
			// 2 cai OrderDetailViewDTO khac nhau @@
			for(OrderDetailViewDTO item: listProductChange){
				int indexPromotionProduct = isContainOldProduct(item, orderDTO.listPromotionOrders);
				int indexPromotionOrder = isContainOldProduct(item, orderDTO.listPromotionOrder);
				int indexPromotionAcumulation = isContainOldProduct(item, orderDTO.listPromotionAccumulation);
				int indexPromotionNewOpen = isContainOldProduct(item, orderDTO.listPromotionNewOpen);

				//Cap nhat so suat cho cac sp option
				item.getQuantityReceived(orderDTO.productQuantityReceivedList);
				//Sp KM ko duoc chon de luu
				if(indexPromotionProduct < 0 && indexPromotionOrder < 0 && indexPromotionAcumulation < 0 && indexPromotionNewOpen < 0) {
					item.orderDetailDTO.quantity = 0;
				} else {
					if(item.orderDetailDTO.productId == orderDetailDto.orderDetailDTO.productId) {
						if (orderDetailDto.row.etTotal.getText().toString().length() > 0) {
							try {
								int realOrder = GlobalUtil.calRealOrder(orderDetailDto.row.etTotal.getText().toString(), orderDetailDto.convfact);
								item.orderDetailDTO.quantity = realOrder;
							} catch (Exception e) {
							}
						}
					}
				}

				realOrderMap.put(item.orderDetailDTO.productId, (int)item.orderDetailDTO.quantity);
				oldRealOrderMap.put(item.orderDetailDTO.productId, (int)item.orderDetailDTO.quantity);
			}
			showPopupSetNumPromotionProduct();
		}
	}

	/**
	 * Tinh toan so luong co the dat duoc khi thay doi so luong KM dua vao cac so luong sp trong 1 nhom
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: int
	 * @throws:
	 * @param listProductChange
	 * @param orderDetailDto
	 * @param row
	 * @return
	 */
	public QuantityInfo calculateQuantity(ArrayList<OrderDetailViewDTO> listProductChange, OrderDetailViewDTO orderDetailDto, OrderPromotionRow row) {
		QuantityInfo quantityInfoResult = new QuantityInfo();
		this.listProductChange = listProductChange;
		productSelected = orderDetailDto;
		realOrderMap = new HashMap<Integer, Integer>();

			for(OrderDetailViewDTO item: listProductChange){
				int indexPromotionProduct = isContainOldProduct(item, orderDTO.listPromotionOrders);
				int indexPromotionOrder = isContainOldProduct(item, orderDTO.listPromotionOrder);
				int indexPromotionAcumulation = isContainOldProduct(item, orderDTO.listPromotionAccumulation);
				int indexPromotionNewOpen = isContainOldProduct(item, orderDTO.listPromotionNewOpen);
				//Sp KM ko duoc chon de luu
				if(indexPromotionProduct < 0 && indexPromotionOrder < 0 && indexPromotionAcumulation < 0 && indexPromotionNewOpen < 0) {
					item.orderDetailDTO.quantity = 0;
					item.orderDetailDTO.quantityPackage = 0;
					item.orderDetailDTO.quantityRetail = 0;
				} else {
					if(item.orderDetailDTO.productId == orderDetailDto.orderDetailDTO.productId) {
						if (orderDetailDto.row.etTotal.getText().toString().length() > 0) {
							try {
								//int realOrder = GlobalUtil.calRealOrder(orderDetailDto.row.etTotal.getText().toString(), orderDetailDto.convfact);
								//item.orderDetailDTO.quantity = realOrder;
								QuantityInfo quantityInfo = GlobalUtil.calQuantityFromOrderStr(orderDetailDto.row.etTotal.getText().toString(), orderDetailDto.convfact);
								item.orderDetailDTO.quantity = quantityInfo.quantityTotal;
								item.orderDetailDTO.quantityPackage = quantityInfo.quantityPackage;
								item.orderDetailDTO.quantityRetail = quantityInfo.quantitySingle;
							} catch (Exception e) {
							}
						}
					}
				}

				realOrderMap.put(item.orderDetailDTO.productId, (int)item.orderDetailDTO.quantity);
			}

		quantityInfoResult = checkAndGetQuantity(row.etTotal, orderDetailDto);

		return quantityInfoResult;
	}

	/**
	 * Tinh lai so tien tong, sau khi them or delete mat hang
	 *
	 * @author: TruongHN
	 * @param indexRow
	 * @param newQuantity
	 * @return: res
	 * @throws:
	 */
	private int reCalTotalPrice(int indexRow, QuantityInfo newQuantity) {
		int res = -1;
		// tinh tong
		if (indexRow >= 0 && indexRow < orderDTO.listBuyOrders.size()) {
			orderDTO.orderInfo.setDiscount(0);
			OrderDetailViewDTO selectedDetail = orderDTO.listBuyOrders.get(indexRow);
			if (selectedDetail != null) {
				res = indexRow;
				double oldAmount = selectedDetail.orderDetailDTO.getAmount();
				double oldQuantity = selectedDetail.orderDetailDTO.quantity;
				//long oldQuantitySingle = selectedDetail.orderDetailDTO.quantitySingle;
				//long oldQuantityPackage = selectedDetail.orderDetailDTO.quantityPackage;
				double oldWeight = selectedDetail.orderDetailDTO.totalWeight;
				// tinh lai quantity moi
				selectedDetail.orderDetailDTO.quantity = newQuantity.quantityTotal;
				selectedDetail.orderDetailDTO.quantityRetail = newQuantity.quantitySingle;
				selectedDetail.orderDetailDTO.quantityPackage = newQuantity.quantityPackage;

				BigDecimal totalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", selectedDetail.grossWeight));
				totalWeight = totalWeight.multiply(new BigDecimal(selectedDetail.orderDetailDTO.quantity)).setScale(3, RoundingMode.HALF_UP);

				selectedDetail.orderDetailDTO.totalWeight = totalWeight.doubleValue();
				selectedDetail.totalOrderQuantity.orderDetailDTO.quantity += -oldQuantity + selectedDetail.orderDetailDTO.quantity;
				selectedDetail.quantity =  selectedDetail.quantityProductStr();
				double newAmount = selectedDetail.orderDetailDTO.calculateAmountProduct();
				selectedDetail.orderDetailDTO.setAmount(newAmount);

				// tinh lai tong
				orderDTO.orderInfo.subtractAmount(oldAmount);
				orderDTO.orderInfo.addAmount(newAmount);
				orderDTO.numSKU = orderDTO.numSKU - (long)oldQuantity + (long)selectedDetail.orderDetailDTO.quantity;
				orderDTO.orderInfo.totalWeight = (orderDTO.orderInfo.totalWeight - oldWeight)
						+ selectedDetail.orderDetailDTO.totalWeight;
				productTotalRow.updateTotalValue(orderDTO.orderInfo.getAmount(), orderDTO.numSKU);

				// update thanh tien cua row dc chon
				OrderProductRow row = (OrderProductRow) tbProducts.getRowAt(indexRow);
				if (row != null) {
					row.updateAmount(newAmount);
				}
				// tinh so tien VAT sau khi tru khuyen mai
//				orderDTO.orderInfo.total = orderDTO.orderInfo.amount
//						- orderDTO.orderInfo.discount;

				// update text tong tien
				initTotalPriveVAT();

				// can tinh lai khuyen mai
				enableButtonSave(false);
				orderDTO.isChangeData = true;
			}
		}
		return res;
	}

	/**
	 * Tinh lai tong khuyen mai
	 *
	 * @author: TruongHN
	 * @param indexParent
	 * @param newQuantity
	 * @return: int
	 * @throws:
	 */
	private void reCalQuantityPromotion(OrderPromotionRow row, int indexParent, QuantityInfo newQuantity) {
		// tinh tong
		if (indexParent >= 0 && indexParent < orderDTO.listPromotionOrders.size()) {
			OrderDetailViewDTO selectedDetail = orderDTO.listPromotionOrders.get(indexParent);
			if (selectedDetail != null) {
				updateQuantityForPromotionProduct(selectedDetail, row, newQuantity);
			}
		}
	}

	/**
	 * Update quantity for promotion product
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param selectedDetail
	 * @param row
	 * @param newQuantity
	 */
	public void updateQuantityForPromotionProduct(OrderDetailViewDTO selectedDetail, OrderPromotionRow row, QuantityInfo newQuantity) {
		int oldQuantity = (int)selectedDetail.orderDetailDTO.quantity;
		double oldWeight = selectedDetail.orderDetailDTO.totalWeight;
		selectedDetail.orderDetailDTO.quantity = newQuantity.quantityTotal;
		selectedDetail.orderDetailDTO.quantityPackage = newQuantity.quantityPackage;
		selectedDetail.orderDetailDTO.quantityRetail = newQuantity.quantitySingle;

		//Check ton kho
		orderDTO.checkStockTotal();
		//Cap nhat tong trong luong
		BigDecimal totalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", selectedDetail.grossWeight));
		totalWeight = totalWeight.multiply(new BigDecimal(selectedDetail.orderDetailDTO.quantity)).setScale(3, RoundingMode.HALF_UP);
		selectedDetail.orderDetailDTO.totalWeight = totalWeight.doubleValue();

		// cap nhat lai mau
		if (isCheckStockTotal) {
			// update thanh tien cua row dc chon
			if (row != null) {
				row.checkStockTotal(selectedDetail);
			}
		}

		//Khong cho phep sua so luong
		if(selectedDetail.isEdited != 1) {
			if (row != null) {
				row.updateQuantityLayout();
			}
		}

		// tinh lai tong
		BigDecimal totalWeightOder = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orderDTO.orderInfo.totalWeight));
		totalWeightOder = totalWeightOder.subtract(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", oldWeight)));
		totalWeightOder = totalWeightOder.add(new BigDecimal(StringUtil.decimalFormatSymbols("#.###", selectedDetail.orderDetailDTO.totalWeight)));
		orderDTO.orderInfo.totalWeight = totalWeightOder.doubleValue();
		orderDTO.numSKUPromotion = orderDTO.numSKUPromotion
				- oldQuantity + newQuantity.quantityTotal;

		//Cap nhat lai total quantity cho cac sp
		selectedDetail.totalOrderQuantity.orderDetailDTO.quantity += newQuantity.quantityTotal - oldQuantity;
	}

	/**
	 * Tinh lai tong khuyen mai
	 *
	 * @author: TruongHN
	 * @param indexParent
	 * @param newQuantity
	 * @return: int
	 * @throws:
	 */
	private void reCalQuantityPromotionOrder(OrderPromotionRow row, int indexParent, int indexChild, QuantityInfo newQuantity) {
		if (indexParent >= 0 && indexParent < orderDTO.listPromotionOrder.size()) {
			OrderDetailViewDTO promotionDetail = orderDTO.listPromotionOrder.get(indexParent);
			// tinh tong
			if (indexChild >= 0
					&& indexChild < promotionDetail.listPromotionForPromo21.size()) {
				OrderDetailViewDTO selectedDetail = promotionDetail.listPromotionForPromo21.get(indexChild);
				if (selectedDetail != null) {
					updateQuantityForPromotionProduct(selectedDetail, row, newQuantity);
				}
			}
		}
	}

	/**
	 * Tinh lai tong khuyen mai
	 *
	 * @author: TruongHN
	 * @param indexParent
	 * @param newQuantity
	 * @return: int
	 * @throws:
	 */
	private void reCalQuantityPromotionAccumulation(OrderPromotionRow row, int indexParent, int indexChild, QuantityInfo newQuantity) {
		if (indexParent >= 0 && indexParent < orderDTO.listPromotionAccumulation.size()) {
			OrderDetailViewDTO promotionDetail = orderDTO.listPromotionAccumulation.get(indexParent);
			// tinh tong
			if (indexChild >= 0 && indexChild < promotionDetail.listPromotionForPromo21.size()) {
				OrderDetailViewDTO selectedDetail = promotionDetail.listPromotionForPromo21.get(indexChild);
				if (selectedDetail != null) {
					updateQuantityForPromotionProduct(selectedDetail, row, newQuantity);
				}
			}
		}
	}

	/**
	 * Tinh lai tong khuyen mai
	 *
	 * @author: TruongHN
	 * @param indexParent
	 * @param newQuantity
	 * @return: int
	 * @throws:
	 */
	private void reCalQuantityPromotionNewOpen(OrderPromotionRow row, int indexParent, int indexChild, QuantityInfo newQuantity) {
		if (indexParent >= 0 && indexParent < orderDTO.listPromotionNewOpen.size()) {
			OrderDetailViewDTO promotionDetail = orderDTO.listPromotionNewOpen.get(indexParent);
			// tinh tong
			if (indexChild >= 0 && indexChild < promotionDetail.listPromotionForPromo21.size()) {
				OrderDetailViewDTO selectedDetail = promotionDetail.listPromotionForPromo21.get(indexChild);
				if (selectedDetail != null) {
					updateQuantityForPromotionProduct(selectedDetail, row, newQuantity);
				}
			}
		}
	}

	/**
	 *
	 * Cap nhat khi thay doi tien KM cua KM cho don hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	private void reCalMoneyPromotion(OrderDetailViewDTO dto) {
		orderDTO.orderInfo.setDiscount(orderDTO.orderInfo.getDiscount()
				- dto.oldDiscountAmount + dto.orderDetailDTO.getDiscountAmount());
		orderDTO.orderInfo.setTotal(orderDTO.orderInfo.getAmount()
				- orderDTO.orderInfo.getDiscount());
		double totalAmountFix = orderDTO.orderInfo.getAmount();
		// update info for order if change money of promotion ZV19,20
		if (dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
//			if (totalAmountFix != 0) {
			if (BigDecimal.valueOf(totalAmountFix).compareTo(BigDecimal.ZERO) != 0) {
				BigDecimalRound discountAmount = new BigDecimalRound(dto.orderDetailDTO.getDiscountAmount());
				dto.orderDetailDTO.discountPercentage = discountAmount.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT)).divide(new BigDecimal(totalAmountFix)).toDouble();
			} else{
				dto.orderDetailDTO.discountPercentage = 0;
			}
		}
		// update text tong tien
		initTotalPriveVAT();
		// if (promotionTotalRow != null) {
		// promotionTotalRow.updateTotalPromotionRow("" +
		// orderDTO.numSKUPromotion, "" + orderDTO.orderInfo.discount);
		// }
		reLayoutBuyProducts();
	}

	/**
	 * updateDisplay
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public void updateDate(int day, int month, int year) {
		// this.mDay = fullDate;
		// this.mMonth = month;
		// this.mYear = year;
		//
		String sDay = String.valueOf(day);
		String sMonth = String.valueOf(month + 1);
		if (day < 10) {
			sDay = "0" + sDay;
		}
		if (month + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		tvDeliveryDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
		tvDeliveryDate.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(sDay).append("/").append(sMonth).append("/")
				.append(year).append(" "));

		orderDTO.deliveryDate = tvDeliveryDate.getText().toString().trim();
		//

	}

	/**
	 * Cap nhat thoi gian
	 *
	 * @author: TruongHN
	 * @param hourOfDay
	 * @param minute
	 * @return: void
	 * @throws:
	 */
	public void updateTime(int hourOfDay, int minute) {
		String hour = String.valueOf(hourOfDay);
		String sMinute = String.valueOf(minute);
		if (hourOfDay < 10) {
			hour = "0" + hour;
		}
		if (minute < 10) {
			sMinute = "0" + sMinute;
		}
		tvDeliveryTime.setTextColor(ImageUtil.getColor(R.color.BLACK));
		tvDeliveryTime.setText(hour + ":" + sMinute);

		orderDTO.deliveryTime = tvDeliveryTime.getText().toString().trim();
	}

	/**
	 * Lay app value
	 *
	 * @author: TruongHN
	 * @param index
	 * @return: int
	 * @throws:
	 */
	private ApParamDTO getPriority(int index) {
		ApParamDTO app = null;
		if (index >= 0 && index < orderDTO.listPriority.size()) {
			app = orderDTO.listPriority.get(index);
		}
		return app;
	}

	/**
	 * Set muc do khan khi sua don hang
	 *
	 * @author: TruongHN
	 * @param value
	 * @return: void
	 * @throws:
	 */
	private void setPriority(long id) {
		if (id > 0) {
			for (int i = 0, size = orderDTO.listPriority.size(); i < size; i++) {
				if (orderDTO.listPriority.get(i).getApParamId() == id) {
					spinnerPriority.setSelection(i);
				}
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * updateFeedbackRow
	 *
	 * @author: TamPQ
	 * @param value
	 * @return: void
	 * @throws:
	 */
	public void updateFeedbackRow(FeedBackDTO item) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.action = ActionEventConstant.UPDATE_FEEDBACK;
		e.viewData = item;
		// e.isNeedCheckTimeServer = true;
		SaleController.getInstance().handleViewEvent(e);
	}

	/**
	 * Xu ly xu kien back he thong
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	public int onBackPressed() {
		int handleBack = -1; // chua xu ly

		showBackPopup = handleBackPressed(-1);
		if (showBackPopup) {
			handleBack = ACTION_CANCEL_BACK_DEFAULT;
		}
		//
		// if (!orderDTO.isEditOrder) {
		// // neu tao moi don hang, cancel request
		// handleBack = ACTION_CANCEL_BACK_DEFAULT;
		// String mess = "";
		// if (orderDTO.isBackToRemainOrder) {
		// mess = StringUtil.getString(R.string.TEXT_CONFIRM_BACK_REMAIN_ORDER);
		// // hien thi dialog confirm
		// GlobalUtil.showDialogConfirm(this, this.parent, mess,
		// StringUtil.getString(R.string.TEXT_AGREE), ACTION_AGRRE_BACK,
		// StringUtil.getString(R.string.TEXT_DENY), ACTION_CANCEL_BACK, null);
		// } else {
		// if (orderDTO.isChangeData) {
		// // neu thay doi
		// handleBack = ACTION_CANCEL_BACK_DEFAULT;
		// mess = StringUtil.getString(R.string.TEXT_CONFIRM_BACK_ORDER);
		// GlobalUtil.showDialogConfirm(this, this.parent, mess,
		// StringUtil.getString(R.string.TEXT_AGREE), ACTION_AGRRE_BACK,
		// StringUtil.getString(R.string.TEXT_DENY), ACTION_CANCEL_BACK, null);
		// } else {
		// // back binh thuong
		// GlobalUtil.popBackStack(this.parent);
		// }
		//
		// }
		//
		// } else {
		// // nguoc lai, neu nhu sua don hang thi kiem tra da thay doi chua
		// // neu chua thay doi thi back binh thuong
		// String mess = "";
		// if (orderDTO.isChangeData) {
		// // neu thay doi
		// handleBack = ACTION_CANCEL_BACK_DEFAULT;
		// mess = StringUtil.getString(R.string.TEXT_CONFIRM_BACK_ORDER);
		// GlobalUtil.showDialogConfirm(this, this.parent, mess,
		// StringUtil.getString(R.string.TEXT_AGREE), ACTION_AGRRE_BACK,
		// StringUtil.getString(R.string.TEXT_DENY), ACTION_CANCEL_BACK, null);
		// }
		//
		// }

		return handleBack;
	}

	/**
	 * Kiem tra co thay doi du lieu hay khong
	 *
	 * @author: TruongHN
	 * @return: String - message hien thi thong bao
	 * @throws:
	 */
	public int checkChangeData() {
		int result = 0;// 0: khong co thay doi, 1: co thay doi chua ket thuc ghe
						// tham, 2: co thay doi: da ket thuc ghe tham
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();

		if (orderDTO.stateOrder == OrderViewDTO.STATE_NEW) {
			// neu tao moi don hang, cancel request
			if (orderDTO.isBackToRemainOrder) {
				if (action != null
						&& action.isOr == 0
						&& action.objectType.equals("0")
						&& StringUtil.isNullOrEmpty(action.endTime)
						&& action.aCustomer.customerId == orderDTO.customer.customerId
						&& orderDTO.orderInfo.synState == 0) {
					result = 1;
				} else {
					result = 2;
				}
				// strMessage =
				// StringUtil.getString(R.string.TEXT_CONFIRM_EXIT_ORDER);
				// strMessage =
				// StringUtil.getString(R.string.TEXT_CONFIRM_BACK_REMAIN_ORDER);
			} else {
				if (orderDTO.isChangeData) {
					// neu thay doi
					if (action != null
							&& action.isOr == 0
							&& action.objectType.equals("0")
							&& StringUtil.isNullOrEmpty(action.endTime)
							&& action.aCustomer.customerId == orderDTO.customer.customerId
							&& orderDTO.orderInfo.synState == 0) {
						result = 1;
					} else {
						result = 2;
					}
					// strMessage =
					// StringUtil.getString(R.string.TEXT_CONFIRM_EXIT_ORDER);
					// strMessage =
					// StringUtil.getString(R.string.TEXT_CONFIRM_BACK_ORDER);
				}
			}
		} else {
			// nguoc lai, neu nhu sua don hang thi kiem tra da thay doi chua
			// neu chua thay doi thi back binh thuong
			if (orderDTO.isChangeData) {
				// neu thay doi
				if (action != null
						&& action.isOr == 0
						&& action.objectType.equals("0")
						&& StringUtil.isNullOrEmpty(action.endTime)
						&& action.aCustomer.customerId == orderDTO.customer.customerId
						&& orderDTO.orderInfo.synState == 0) {
					result = 1;
				} else {
					result = 2;
				}
				// strMessage =
				// StringUtil.getString(R.string.TEXT_CONFIRM_EXIT_ORDER);
				// strMessage =
				// StringUtil.getString(R.string.TEXT_CONFIRM_BACK_ORDER);
			}
		}

		// Khong co sp thi ko cho phep luu
		boolean allowSave = false;
		// Validate co don hang thi moi cho luu
		if (orderDTO.listBuyOrders.size() == 0) {
			// neu so luong sp khuyen mai > 0 & ton tai sp CTTB --> enable
			// button luu
			boolean errorInput = false;
			if (orderDTO.listPromotionOrders.size() == 0) {
				errorInput = true;
			} else {
				for (OrderDetailViewDTO promotion : orderDTO.listPromotionOrders) {
					// Ds KM co KM tu dong thi bat buoc phai co mat
					// hang ban
					if (promotion.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO) {
						errorInput = true;
						break;
					}
				}
			}

			if (errorInput) {
				allowSave = false;
			} else {
				allowSave = true;
			}
		} else {
			allowSave = true;
		}

		if (!allowSave) {
			result = 0;
		}
		return result;
	}

	/**
	 *
	 * show popup select promotion product
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 7, 2013
	 */
	private void showPopupSelectPromotionProduct() {
		if (alertSelectPromotionProduct == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			LayoutInflater inflater = this.parent.getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_select_product_promotion_view, null);

			tbProductPromotionList = (DMSTableView) view
					.findViewById(R.id.tbProductPromotionList);
			tbProductPromotionList.setListener(this);

			tvCTKMCode = (TextView) view.findViewById(R.id.tvCTKMCode);
			tvCTKMName = (TextView) view.findViewById(R.id.tvCTKMName);
			btClosePopupPrograme = (Button) view
					.findViewById(R.id.btClosePopupPrograme);
			btClosePopupPrograme.setOnClickListener(this);
			initHeaderTable(tbProductPromotionList, new SelectPromotionProductRow(
						parent, tbProductPromotionList));
			tbProductPromotionList.getPagingControl().setVisibility(View.GONE);

			build.setView(view);
			alertSelectPromotionProduct = build.create();
			// alertProductDetail.setCancelable(false);

			Window window = alertSelectPromotionProduct.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					GlobalUtil.dip2Pixel(600));
			window.setGravity(Gravity.CENTER);
		}

		tvCTKMCode.setText(productSelected.orderDetailDTO.programeCode);
		tvCTKMName.setText(productSelected.orderDetailDTO.programeName);
		tbProductPromotionList.clearAllData();
		int pos = 1;
		if (this.listProductChange.size() > 0) {
			for (OrderDetailViewDTO dto : this.listProductChange) {
				//Cap nhat so suat cho cac sp option
				calculateTotalQuantityUsingPopupSelectPromotion(dto);
				dto.getQuantityReceived(orderDTO.productQuantityReceivedList);
				SelectPromotionProductRow row = new SelectPromotionProductRow(
						parent, tbProductPromotionList);
				row.setClickable(true);
				row.setTag(Integer.valueOf(pos));
				row.renderLayout(pos, dto);
				if (this.productSelected.productCode.equals(dto.productCode)) {
					row.updateLayoutSelected();
				}
				row.setListener(this);
				tbProductPromotionList.addRow(row);
				pos++;
			}
		} else {
			tbProductPromotionList.showNoContentRow();
		}

		if (!alertSelectPromotionProduct.isShowing()) {
			alertSelectPromotionProduct.show();
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		//Doi sp tuy chon
		// send borad cast ProductHasPromotionDTO to order view
		if (action == ActionEventConstant.BROADCAST_CHANGE_PROMOTION_PRODUCT) {
			updatePromotionProductAfterSelectInPopup((OrderDetailViewDTO) data, productSelected);
			if (alertSelectPromotionProduct.isShowing()) {
				alertSelectPromotionProduct.dismiss();
			}
		}
	//Doi giua cac CTKM don hang
		else if (action == ActionEventConstant.ACTION_SELECT_PROMOTION_TYPE_PROMOTION_ORDER) {
			if(orderDTO.listPromotionOrder.size() > 0) {
				OrderDetailViewDTO newPromotion = (OrderDetailViewDTO) data;
				OrderDetailViewDTO oldPromotion = orderDTO.listPromotionOrder.get(0);
				if (!oldPromotion.orderDetailDTO.programeCode.equals(newPromotion.orderDetailDTO.programeCode)) {
					//Cap nhat lai tien discount amount cua tung sp & don hang khi tu KM don hang tien -> KM khac
					if(oldPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER) {
						for(OrderDetailViewDTO buyProduct: orderDTO.listBuyOrders) {
							ArrayList<SaleOrderPromoDetailDTO> listPromoDetailRemove = new ArrayList<SaleOrderPromoDetailDTO>();
							for(SaleOrderPromoDetailDTO buyPromotion: buyProduct.listPromoDetail) {
								if(buyPromotion.programCode.equals(oldPromotion.orderDetailDTO.programeCode)) {
									buyProduct.orderDetailDTO.subtractDiscountAmount(buyPromotion.discountAmount);
//										orderDTO.orderInfo.discount -= buyPromotion.discountAmount;
//										orderDTO.orderInfo.total += buyPromotion.discountAmount;

									listPromoDetailRemove.add(buyPromotion);
								}
							}

							//Loai bo cac chi tiet KM cu~
							buyProduct.listPromoDetail.removeAll(listPromoDetailRemove);
						}
					}

					//Cap nhat lai tien discount amount cua tung sp & don hang khi tu KM khac -> KM tien
					if(newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER) {
						//Dung de tinh toan discount cho tung sp, duoc huong toi da la so tien qui, giam sai so
						for(OrderDetailViewDTO orderPromotion: orderDTO.listPromotionForOrderChange) {
							if(orderPromotion.orderDetailDTO.programeCode.equals(newPromotion.orderDetailDTO.programeCode)) {
								double totalDiscount = 0;
								double fixDiscountAmount = orderPromotion.orderDetailDTO.getDiscountAmount();
//									for(OrderDetailViewDTO buyProduct: orderDTO.listBuyOrders) {
								int numProduct = 0;
								for (int i = orderDTO.listBuyOrders.size() - 1; i >= 0; i--) {
									OrderDetailViewDTO buyProduct = orderDTO.listBuyOrders.get(i);
									numProduct++;
//										buyProduct.orderDetailDTO.discountPercentage += newPromotion.orderDetailDTO.discountPercentage;
//										BigDecimal discountPercent = new BigDecimal(StringUtil.decimalFormatSymbols("#.##", orderPromotion.orderDetailDTO.discountPercentage));
//										double discountAmount = discountPercent.multiply(new BigDecimal(buyProduct.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT), 0, RoundingMode.DOWN).doubleValue();
									BigDecimalRound discountPercent = new BigDecimalRound(StringUtil.decimalFormatSymbols("#.##", orderPromotion.orderDetailDTO.discountPercentage));
									double discountAmount = discountPercent.multiply(new BigDecimal(buyProduct.orderDetailDTO.getAmount())).divide(new BigDecimal(CalPromotions.ROUND_PERCENT)).toDouble();
									//Tinh toan de lam tron, tong so tien <= tong tien KM
									if(discountAmount > fixDiscountAmount) {
										discountAmount = fixDiscountAmount;
									}

									if(totalDiscount + discountAmount > fixDiscountAmount) {
										discountAmount = fixDiscountAmount - totalDiscount;
									} else {
										if(numProduct == orderDTO.listBuyOrders.size()) {
											discountAmount = fixDiscountAmount - totalDiscount;
										}
									}
									totalDiscount += discountAmount;
									buyProduct.orderDetailDTO.addDiscountAmount(discountAmount);
//										orderDTO.orderInfo.discount += discountAmount;
//										orderDTO.orderInfo.total -= discountAmount;

									//promo detail sau khi tinh khuyen mai lai
									SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
									promoDetail.updateData(orderPromotion, discountAmount);
									//neu so luong san pham quy dinh == 1 -> la LINE, isOwner = 1, con lai la isOwner = 2
									promoDetail.isOwner = 2;
									buyProduct.listPromoDetail.add(promoDetail);
								}
							}
						}
					}

					//Tinh lai so suat
					//Xoa cac so suat cua CTKM don hang cu~
					ArrayList<SaleOrderPromotionDTO> removedQuantityReceived = new ArrayList<SaleOrderPromotionDTO>();
					for(SaleOrderPromotionDTO quantityReceived: orderDTO.productQuantityReceivedList) {
						if(oldPromotion.orderDetailDTO.programeCode.equals(quantityReceived.promotionProgramCode)) {
							removedQuantityReceived.add(quantityReceived);
						}
					}

					orderDTO.productQuantityReceivedList.removeAll(removedQuantityReceived);
					//Them so suat cua CTKM don hang duoc chon vao ds so suat
					for(SaleOrderPromotionDTO quantityReceived: orderDTO.orderQuantityReceivedList) {
						if(newPromotion.orderDetailDTO.programeCode.equals(quantityReceived.promotionProgramCode)) {
							orderDTO.productQuantityReceivedList.add(quantityReceived);
						}
					}

					//Lay ra tat ca cac muc cua 1 KM duoc chon
					ArrayList<OrderDetailViewDTO> listOrderPromotion = new ArrayList<OrderDetailViewDTO>();
					for(OrderDetailViewDTO orderPromotion: orderDTO.listPromotionForOrderChange) {
						if(orderPromotion.orderDetailDTO.programeCode.equals(newPromotion.orderDetailDTO.programeCode)) {
							listOrderPromotion.add(orderPromotion);
						}
					}

					orderDTO.listPromotionOrder.clear();
					orderDTO.listPromotionOrder.addAll(listOrderPromotion);
//					orderDTO.listPromotionOrders.set(indexPromotionOrder, dto);

					orderDTO.isCalculatePromotion = true;

					//Tinh toan lai so suat toi do duoc huong
					validatePromotionQuantityReceived();

					calPriceKeyShop();
					layoutOrder();
//						// layout ds sp khuyen mai
//						displayPromotion();
//						//Doi sp KM don hang -> cap nhat thong tin tien chiet khau len cac sp ban
//						reLayoutBuyProducts();
				}
			}
			if (alertOrderPromotionList.isShowing()) {
				alertOrderPromotionList.dismiss();
			}

			//Hien thi so tien KM tich luy con du
			showAcumulationDiscountRemain();
		}else if(action == ActionEventConstant.GET_PROMOTION_DETAIL_KEYSHOP){
			long keyShopId = (Long)data;
			requestGetPromotionDetailKeyShop(""+keyShopId);
		}if(action == ACTION_VIEW_PRODUCT){
			OrderDetailViewDTO detailDto = (OrderDetailViewDTO) data;
			if (detailDto != null) {
				requestGetProductInfoDetail(String
						.valueOf(detailDto.orderDetailDTO.productId));
			}

		}
	}

	/**
	 * Hien thi thong bao so tien tich luy con du
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 */
	public void showAcumulationDiscountRemain() {
		if(orderDTO.getPromotionMaxDiscount() - orderDTO.getPromotionDiscount() > 0) {
			parent.showDialog(StringUtil.getStringAndReplace(
					R.string.MESSAGE_ACUMULATION_REMAIN,
					StringUtil.formatNumber(orderDTO.getPromotionMaxDiscount() - orderDTO.getPromotionDiscount())));
		}

		if (orderType == CREATE_ORDER_FOR_VANSALE) {
			Collection<OrderDetailViewDTO> temp = (Collection<OrderDetailViewDTO>) orderDTO.listLackRemainAccumulationProduct.values();
			if(!temp.isEmpty()) {
				ArrayList<String> listProductCode = new ArrayList<String>();
				for (OrderDetailViewDTO detail : temp) {
					listProductCode.add(detail.productCode);
				}
				parent.showDialog(StringUtil.getStringAndReplace(
						R.string.CONFIRM_GET_ACCUMULATION_PROMOTION_REMAIN, TextUtils.join(",", listProductCode)));
			}
		}
	}

	/**
	 *
	 * update promotion for product after change product
	 *
	 * @param newPromotion
	 * @param oldPromotion
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 7, 2013
	 */
	private void updatePromotionProductAfterSelectInPopup(OrderDetailViewDTO newPromotion, OrderDetailViewDTO oldPromotion) {
		if (newPromotion.orderDetailDTO.productId != oldPromotion.orderDetailDTO.productId) {
			if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
				//Tim vi tri trong ds de thay the
				int index = orderDTO.listPromotionOrders.indexOf(oldPromotion);
				if(index >= 0) {
					//Reset gia tri quantity ban dau voi so suat tuong ung
					if(oldPromotion.quantityReceived != null) {
						newPromotion.orderDetailDTO.quantity = oldPromotion.quantityReceived.quantityReceived
								* newPromotion.orderDetailDTO.maxQuantityFree
								/ oldPromotion.quantityReceived.quantityReceivedMax;
					} else {
						newPromotion.orderDetailDTO.quantity = 1 * newPromotion.orderDetailDTO.maxQuantityFree;
					}
					orderDTO.listPromotionOrders.set(index, newPromotion);
				}
			} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT ||
					newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT ||
					newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT
					) {
				ArrayList<OrderDetailViewDTO> listPromotionProduct = new ArrayList<OrderDetailViewDTO>();
				if(newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionOrder;
				} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionAccumulation;
				} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionNewOpen;
				}

				for (OrderDetailViewDTO promotionOrder : listPromotionProduct) {
					boolean isFound = false;
					//Tim vi tri trong ds de thay the
					int index = promotionOrder.listPromotionForPromo21.indexOf(oldPromotion);
					if(index >= 0) {
						//Reset gia tri quantity ban dau voi so suat tuong ung
						if(oldPromotion.quantityReceived != null) {
							newPromotion.orderDetailDTO.quantity = oldPromotion.quantityReceived.quantityReceived
									* newPromotion.orderDetailDTO.maxQuantityFree
									/ oldPromotion.quantityReceived.quantityReceivedMax;
						} else {
							newPromotion.orderDetailDTO.quantity = 1 * newPromotion.orderDetailDTO.maxQuantityFree;
						}

						promotionOrder.listPromotionForPromo21.set(index, newPromotion);
						isFound = true;

					}

					//Tim thay duoc dung KM don hang de doi
					if(isFound) {
						break;
					}
				}
			}

			// Kiem tra xem co enable cac nut de thuc hien chuc nang hay ko?
			checkEnableButton();

			// cap nhat thong tin san pham khuyen mai
//			displayPromotion();
//			reLayoutBuyProducts();

			//Reset so luong ve max
			for(SaleOrderPromotionDTO salePromotionDto: orderDTO.productQuantityReceivedList) {
				if (newPromotion.orderDetailDTO.programeCode != null
						&& newPromotion.orderDetailDTO.programeCode.equals(salePromotionDto.promotionProgramCode)
						&& newPromotion.orderDetailDTO.productGroupId == salePromotionDto.productGroupId
						&& newPromotion.orderDetailDTO.groupLevelId == salePromotionDto.groupLevelId) {
					salePromotionDto.numReceivedMax -= oldPromotion.orderDetailDTO.maxQuantityFree;
					salePromotionDto.numReceivedMax += newPromotion.orderDetailDTO.maxQuantityFree;

					break;
				}
			}

			//Cap nhat so suat, so luong cho cac sp cung 1 level
			if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_PRODUCT) {
				for(OrderDetailViewDTO orderDetail : orderDTO.listPromotionOrders) {
					if (newPromotion.orderDetailDTO.programeCode != null
							&& newPromotion.orderDetailDTO.programeCode.equals(orderDetail.orderDetailDTO.programeCode)
							&& newPromotion.orderDetailDTO.productGroupId == orderDetail.orderDetailDTO.productGroupId
							&& newPromotion.orderDetailDTO.groupLevelId == orderDetail.orderDetailDTO.groupLevelId) {

						if(orderDetail.quantityReceived != null) {
							orderDetail.quantityReceived.quantityReceived = oldPromotion.quantityReceived.quantityReceivedMax;

							orderDetail.orderDetailDTO.quantity = orderDetail.quantityReceived.quantityReceived
									* newPromotion.orderDetailDTO.maxQuantityFree
									/ oldPromotion.quantityReceived.quantityReceivedMax;
						} else {
							orderDetail.orderDetailDTO.quantity = 1 * orderDetail.orderDetailDTO.maxQuantityFree;
						}
					}
				}
			} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT ||
					newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT ||
					newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT
					) {
				ArrayList<OrderDetailViewDTO> listPromotionProduct = new ArrayList<OrderDetailViewDTO>();
				if(newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_FOR_ORDER_TYPE_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionOrder;
				} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionAccumulation;
				} else if (newPromotion.promotionType == OrderDetailViewDTO.PROMOTION_NEW_OPEN_PRODUCT) {
					listPromotionProduct = orderDTO.listPromotionNewOpen;
				}

				for (OrderDetailViewDTO promotionOrder : listPromotionProduct) {
					boolean isFound = false;
					//Tim vi tri trong ds de thay the
					int index = promotionOrder.listPromotionForPromo21.indexOf(newPromotion);
					if(index >= 0) {
//						if (newPromotion.orderDetailDTO.programeCode != null
//								&& newPromotion.orderDetailDTO.programeCode.equals(promotionOrder.orderDetailDTO.programeCode)
//								&& newPromotion.orderDetailDTO.productGroupId == promotionOrder.orderDetailDTO.productGroupId
//								&& newPromotion.orderDetailDTO.groupLevelId == promotionOrder.orderDetailDTO.groupLevelId) {
							for(OrderDetailViewDTO orderDetail : promotionOrder.listPromotionForPromo21) {
								//Reset gia tri quantity ban dau voi so suat tuong ung
								if(orderDetail.quantityReceived != null) {
									orderDetail.quantityReceived.quantityReceived = oldPromotion.quantityReceived.quantityReceivedMax;

									orderDetail.orderDetailDTO.quantity = orderDetail.quantityReceived.quantityReceived
											* newPromotion.orderDetailDTO.maxQuantityFree
											/ oldPromotion.quantityReceived.quantityReceivedMax;
								} else {
									orderDetail.orderDetailDTO.quantity = 1 * orderDetail.orderDetailDTO.maxQuantityFree;
								}
							}
//					}
						isFound = true;

					}

					//Tim thay duoc dung KM don hang de doi
					if(isFound) {
						break;
					}
				}
			}

			//Tinh toan lai so suat toi do duoc huong
			validatePromotionQuantityReceived();

			calPriceKeyShop();
			layoutOrder();

			//Hien thi so tien KM tich luy con du
			showAcumulationDiscountRemain();
		}
	}

	/**
	 * Kiem tra xem khi nhan nut Back thi co hien confirm hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: int
	 * @throws:
	 */
	private boolean handleBackPressed(int action) {
		boolean isConfirm = false;
		int changeData = checkChangeData();
		if (changeData != 0) {

			// hien thi thong bao don hang tam
			if (changeData == 1) {
				//change format quantity
				changeQuantityProductByConfig();
				SpannableObject span = new SpannableObject();
				span.addSpan(
						StringUtil.getString(R.string.TEXT_CONFIRM_EXIT_ORDER),
						ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
				span.addSpan("\n");
				span.addSpan(StringUtil
						.getString(R.string.TEXT_CONFIRM_EXIT_ORDER_NOTIFY),
						ImageUtil.getColor(R.color.RED), Typeface.BOLD, 20);
				GlobalUtil.showDialogConfirm(this, parent, span.getSpan(),
						StringUtil.getString(R.string.TEXT_AGREE),
						CONFIRM_DRAFT_ORDER_OK,
						StringUtil.getString(R.string.TEXT_DENY),
						CONFIRM_DRAFT_ORDER_CANCEL, action);
				isConfirm = true;
			} else if (changeData == 2) {
				//neu la action back va dang o Order View thi moi hoi muon thoat khoi dat hang
				if (action == -1
						&& GlobalUtil.getTag(OrderView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
					GlobalUtil.showDialogConfirm(this, parent, StringUtil
							.getString(R.string.TEXT_CONFIRM_EXIT_ORDER_VISITED),
							StringUtil.getString(R.string.TEXT_AGREE),
							CONFIRM_EXIT_ORDER_VIEW_VISIT_OK, StringUtil
							.getString(R.string.TEXT_CANCEL),
							CONFIRM_EXIT_ORDER_VIEW_CANCEL, null);
					isConfirm = true;
				}
			}
		} else {
			// back binh thuong

			// TamPQ: CR0075: neu dang la kh ngoai tuyen thi tu dong
			// update
			// action_log ghe tham khi thoat man hinh dat hang
			// int isOr = orderDTO.orderInfo.isVisitPlan == 0 ? 1 : 0;
			ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			if (actionLog != null && actionLog.isOr == 1
					&& StringUtil.isNullOrEmpty(actionLog.endTime)) {
				parent.requestUpdateActionLog("0", "0", null, this);
			}
		}

		return isConfirm;
	}


	/**
	 *
	 * show popup promotion product truong hop mua A: n B, nC, n D doi thanh x
	 * B, y C, z D voi (x+y+z) = n
	 *
	 * @return: void
	 * @throws:
	 * @author: DUNGNX
	 * @date: Jan 7, 2013
	 */
	//Window windowChangeNumPromotionProduct = null;
	//Window windowPopUpPayment = null;
	Window keyboardWindowCurrent = null;
	TextView tvCTKMCodeChangeNum = null;
	TextView tvCTKMNameChangeNum = null;
	private void showPopupSetNumPromotionProduct() {
		//close before show
		if (alertChangeNumPromotionProduct != null
				&& alertChangeNumPromotionProduct.isShowing()) {
			alertChangeNumPromotionProduct.dismiss();
		}
		Builder build = new AlertDialog.Builder(parent,
				R.style.CustomDialogTheme);
		LayoutInflater inflater = this.parent.getLayoutInflater();
		View view = inflater.inflate(
				R.layout.layout_change_num_product_promotion_view, null);

		tbProductPromotionListChangeNum = (DMSTableView) view
				.findViewById(R.id.tbProductPromotionList);
		tbProductPromotionListChangeNum.setListener(this);

		tvCTKMCodeChangeNum = (TextView) view.findViewById(R.id.tvCTKMCode);
		tvCTKMNameChangeNum = (TextView) view.findViewById(R.id.tvCTKMName);
		btClosePopupProgrameChangeNum = (Button) view
				.findViewById(R.id.btClosePopupPrograme);
		btClosePopupProgrameChangeNum.setOnClickListener(this);

		btOKPopupProgrameChangeNum = (Button) view
				.findViewById(R.id.btOKPopupProgrameChangeNum);
		btOKPopupProgrameChangeNum.setOnClickListener(this);
		tbProductPromotionListChangeNum.addHeader(new ChangeNumPromotionProductRow(
						parent, tbProductPromotionListChangeNum));
		tbProductPromotionListChangeNum.getPagingControl().setVisibility(
				View.GONE);

		build.setView(view);
		alertChangeNumPromotionProduct = build.create();

		Window window = alertChangeNumPromotionProduct.getWindow();
		window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
				255, 255)));
		window.setLayout(LayoutParams.WRAP_CONTENT,
				GlobalUtil.dip2Pixel(600));
		window.setGravity(Gravity.CENTER);


		if (tvCTKMCodeChangeNum != null) {
			tvCTKMCodeChangeNum.setText(productSelected.orderDetailDTO.programeCode);
		}
		if (tvCTKMNameChangeNum != null) {
			tvCTKMNameChangeNum.setText(productSelected.orderDetailDTO.programeName);
		}
		tbProductPromotionListChangeNum.clearAllData();
		int pos = 1;
		if (this.listProductChange.size() > 0) {
			for (OrderDetailViewDTO dto : this.listProductChange) {
				ChangeNumPromotionProductRow row = new ChangeNumPromotionProductRow(
						parent, tbProductPromotionListChangeNum);
				row.setClickable(true);
				row.setTag(Integer.valueOf(pos));
				row.setOnEventControlListener(this);
				row.renderLayout(pos, dto);
				tbProductPromotionListChangeNum.addRow(row);
				pos++;
			}
		} else {
			tbProductPromotionListChangeNum.showNoContentRow();
		}

		if (!alertChangeNumPromotionProduct.isShowing()) {
			//set window for show keyboard
			keyboardWindowCurrent = window;
			alertChangeNumPromotionProduct.show();
		}
	}

	/**
	 * kiem tra se show popup doi sp hay so luong
	 *
	 * @author: DungNX
	 * @param listProductChange
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	boolean isPromotionNAorNB(ArrayList<OrderDetailViewDTO> listProductChange) {
		boolean isChangeNum = true;
		for (int i = 1; i < listProductChange.size(); i++) {
			if (listProductChange.get(i).orderDetailDTO.maxQuantityFree != listProductChange
					.get(i - 1).orderDetailDTO.maxQuantityFree) {
				isChangeNum = false;
				break;
			}
		}
		return isChangeNum;
	}

	// dung de tinh toan khi thay doi so luong
	HashMap<Integer, Integer> realOrderMap;
	HashMap<Integer, Integer> oldRealOrderMap;
//	HashMap<Integer, Integer> realOrderMapTemp;

	// show dialog thanh toan
	private AlertDialog alertProductDetail;

	/**
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param edNumberPromotion
	 * @param myData
	 */
	public QuantityInfo checkAndGetQuantity(EditText edNumberPromotion, OrderDetailViewDTO myData) {
		//int realOrder = 0;
		QuantityInfo quantityInfo = new QuantityInfo();
		int total = 0;
		for (Entry<Integer, Integer> entry : realOrderMap.entrySet()) {
			int key = entry.getKey();
			int value = entry.getValue();
			if (key != myData.orderDetailDTO.productId) {
				// do stuff
				total += value;
			}
		}

		if (edNumberPromotion.getText().toString().length() > 0) {
			try {
				//realOrder = GlobalUtil.calRealOrder(edNumberPromotion.getText().toString(), myData.convfact);
				quantityInfo = GlobalUtil.calQuantityFromOrderStr(edNumberPromotion.getText().toString(), myData.convfact);
			} catch (Exception e) {
			}
		}
		//if (realOrder < 0) {
		if (quantityInfo.quantityTotal < 0) {
			// tranh lap vo tan
			//realOrder = 0;
			quantityInfo.quantityTotal = 0;
			quantityInfo.quantityPackage = 0;
			quantityInfo.quantitySingle = 0;
			edNumberPromotion.setText("" + quantityInfo.quantityTotal);
		} else if (myData != null && myData.orderDetailDTO != null) {
			// doi quantity thanh kieu long
			long newQuantity = myData.orderDetailDTO.maxQuantityFree;
			if(myData.quantityReceived != null) {
				newQuantity = myData.quantityReceived.quantityReceived * (myData.orderDetailDTO.maxQuantityFree/ myData.quantityReceived.quantityReceivedMax);
			}
			//if (realOrder + total > newQuantity) {
			if (quantityInfo.quantityTotal + total > newQuantity) {
				long remain = newQuantity - total;
				edNumberPromotion.setText(String.valueOf(remain));
				//realOrder = remain;
				quantityInfo.quantityTotal = remain;
				quantityInfo.quantityPackage = 0;
				quantityInfo.quantitySingle = remain;
//			} else if (realOrder + total < newQuantity){
//				if(orderType == CREATE_ORDER_FOR_VANSALE) {
//					if(realOrder < myData.stock) {
//						int remain = (newQuantity - total) <  myData.stock ? newQuantity - total : (int)myData.stock;
//						edNumberPromotion.setText(String.valueOf(remain));
//						realOrder = remain;
//					}
//				}
			}
		}
		if (realOrderMap != null && myData != null) {
			realOrderMap.put(myData.orderDetailDTO.productId, (int)quantityInfo.quantityTotal);
		}

		return quantityInfo;
	}

	public int checkAndGetQuantityAcumulation(EditText edNumberPromotion,
			OrderDetailViewDTO myData) {
		int realOrder = 0;
		int total = 0;
		for (Entry<Integer, Integer> entry : realOrderMap.entrySet()) {
			int key = entry.getKey();
			int value = entry.getValue();
			if (key != myData.orderDetailDTO.productId) {
				// do stuff
				total += value;
			}
		}

		if (edNumberPromotion.getText().toString().length() > 0) {
			try {
				realOrder = GlobalUtil.calRealOrder(edNumberPromotion.getText()
						.toString(), myData.convfact);
			} catch (Exception e) {
			}
		}
		if (realOrder < 0) {
			// tranh lap vo tan
			realOrder = 0;
			edNumberPromotion.setText("" + realOrder);
			// edNumberPromotion.setText("" + (-realOrder));
		} else if (myData != null && myData.orderDetailDTO != null) {
			int newQuantity = myData.orderDetailDTO.maxQuantityFree;
			if (myData.quantityReceived != null) {
				newQuantity = myData.quantityReceived.quantityReceived
						* myData.orderDetailDTO.maxQuantityFree
						/ myData.quantityReceived.quantityReceivedMax;
			}

			boolean isNeedUpdateLayout = false;
			if (realOrder + total > newQuantity) {
				int remain = newQuantity - total;

				realOrder = remain;
				isNeedUpdateLayout = true;
			}

			if (isNeedUpdateLayout) {
				edNumberPromotion.setText(String.valueOf(realOrder));
			}
		}
		if (realOrderMap != null && myData != null) {
			realOrderMap.put(myData.orderDetailDTO.productId, realOrder);
		}

		return realOrder;
	}

	/**
	 * xu ly sau khi nhap so luong trong popup doi sp KM dang nA hoac nB hoac nC..
	 *
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	void processChooseNewPromotion() {
		// sau khi bam OK
		// kiem tra neu ds sp chon = 0 het thi de nhu cu hoac lay gia tri mac
		// dinh
		// cac gia tri trong realOrderMapTemp = 0
		boolean changeProduct = false;
		for (Entry<Integer, Integer> entry : realOrderMap.entrySet()) {
			int value = entry.getValue();
			// do stuff
			if(value>0){
				changeProduct = true;
				break;
			}
		}

		if (changeProduct) {
			if (realOrderMap != null) {
				OrderDetailViewDTO promotionProduct = listProductChange.get(0);
				int quantityRemain = promotionProduct.orderDetailDTO.maxQuantityFree;
				if(promotionProduct.quantityReceived != null) {
					quantityRemain = promotionProduct.quantityReceived.quantityReceived
							* promotionProduct.orderDetailDTO.maxQuantityFree
							/ promotionProduct.quantityReceived.quantityReceivedMax;
				}

//				realOrderMap.putAll(realOrderMapTemp);
				int totalIsEdited = 0;
				for (OrderDetailViewDTO item : listProductChange) {
					//Sp hien thi thi da set order type, sp doi thi co the chua set
					//Neu orderType = vansale &
					if (item.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
						item.isEdited = 0;
					}

					if (realOrderMap.get(item.orderDetailDTO.productId) != null) {
						int realOrder = realOrderMap.get(item.orderDetailDTO.productId);
						// item.totalOrderQuantity = realOrder;
						item.orderDetailDTO.quantity = realOrder;
						BigDecimal totalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", item.grossWeight));
						totalWeight = totalWeight.multiply(new BigDecimal(item.orderDetailDTO.quantity)).setScale(3, RoundingMode.HALF_UP);
						item.orderDetailDTO.totalWeight = totalWeight.doubleValue();
//						item.orderDetailDTO.maxQuantityFree = realOrder;
						quantityRemain -= realOrder;

						if(realOrder > 0) {
							promotionProduct = item;
						}
					} else {
						item.orderDetailDTO.quantity = 0;
					}

					if(orderType == CREATE_ORDER_FOR_VANSALE) {
						//Tinh toan so luong su dung & ton kho con lai
						calculateTotalQuantityUsingWhenPressOK(item);
							// orderDTO.price = detail.orderDetailDTO.price;
							// orderDTO.programeCode = detail.orderDetailDTO.programeCode;
//							orderDTO.listDistinctProduct.put(Long.valueOf(item.orderDetailDTO.productId), orderDetail);
						if(item.stock <= 0 || item.totalOrderQuantity.orderDetailDTO.quantity > item.stock) {
							//CTKM thuoc loai co so suat
							if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
								if(item.isHasQuantityReceived) {
									item.isEdited = 2;
								} else {
									item.isEdited = 1;
								}
							}
							totalIsEdited = item.isEdited;
						} else {
							if(item.isHasQuantityReceived) {
								if (item.quantityReceived != null
										&& item.quantityReceived.quantityReceived == item.quantityReceived.quantityReceivedMax) {
									item.isEdited = item.oldIsEdited;
								}
							}
						}
						//Neu dat hang presale thi ko cho phep sua so luong
					}
				}

				//Cap nhat isEdit total
				if(orderType == CREATE_ORDER_FOR_VANSALE && totalIsEdited != 0) {
					for (OrderDetailViewDTO item : listProductChange) {
						item.isEdited = totalIsEdited;
					}
				}
//				if(quantityRemain > 0) {
//					promotionProduct.orderDetailDTO.maxQuantityFree += quantityRemain;
					if(promotionProduct.isEdited == 0) {
						if (orderType == CREATE_ORDER_FOR_VANSALE && promotionProduct.promotionType == OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//							if(!orderDTO.checkStockTotalAccumulationProduct2()) {
//							long stockRemain = orderDTO.listDistinctProduct.get(Long.valueOf(promotionProduct.orderDetailDTO.productId)).stock;
//							long quantityUsing = 0;
//							if(promotionProduct.orderDetailDTO.quantity + quantityRemain > stockRemain) {
//								quantityUsing = (long) (stockRemain - promotionProduct.orderDetailDTO.quantity);
//							} else {
//								quantityUsing = quantityRemain;
//								promotionProduct.orderDetailDTO.quantity += quantityRemain;
//								quantityRemain = 0;
//							}
//							promotionProduct.orderDetailDTO.quantity = promotionProduct.orderDetailDTO.quantity + quantityUsing;
//							quantityRemain -= quantityUsing;
//							orderDTO.listDistinctProduct.get(Long.valueOf(promotionProduct.orderDetailDTO.productId)).orderDetailDTO.quantity += quantityUsing;
//
//							boolean hasRemainProductHasQuantity = false;
//							for(OrderDetailViewDTO promotion : listProductChange) {
//								OrderDetailViewDTO stock = orderDTO.listDistinctProduct.get(Long.valueOf(promotion.orderDetailDTO.productId));
//								if(stock.orderDetailDTO.quantity < stock.stock) {
//									hasRemainProductHasQuantity = true;
//
//									break;
//								}
//							}
//
//							if(hasRemainProductHasQuantity) {
//								promotionProduct.orderDetailDTO.quantity += quantityRemain;
//							}

//							}
						} else {
							promotionProduct.orderDetailDTO.quantity += quantityRemain;
						}
					} else if(promotionProduct.isEdited == 2) {
						promotionProduct.orderDetailDTO.quantity += quantityRemain;
					} else {

					}
//				}
			}
			// show sp moi sau khi bam OK
			showPromotionProductAfterChange(listProductChange);
		}
	}

	/**
	 * Tinh toan bien total quantity: tong so luong sp da su dung, duoc su dung sau khi nhan nut Dong y
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param item
	 */
	public void calculateTotalQuantityUsingPopupSelectPromotion(OrderDetailViewDTO item) {
		//Check co bien kiem tra ton kho chua, neu chua thi cap nhat
		OrderDetailViewDTO orderDetail = orderDTO.listDistinctProduct.get(Long.valueOf(item.orderDetailDTO.productId));

		if(orderDetail == null) {
			orderDetail = new OrderDetailViewDTO();
			orderDTO.listDistinctProduct.put(Long.valueOf(item.orderDetailDTO.productId), orderDetail);
			SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
			orderDetail.orderDetailDTO = orderDetailDTO;

			orderDetailDTO.quantity = item.orderDetailDTO.quantity;
			orderDetailDTO.productId = item.orderDetailDTO.productId;
			orderDetail.stock = item.stock;
//			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				orderDetail.stock -= item.orderDetailDTO.quantity;
//			}
//		} else {
//			//Su dung oldRealOrderMap vi ham nay duoc su dung sau khi nhan nut dong y
//			int oldQuantity = oldRealOrderMap.get(item.orderDetailDTO.productId);
//			orderDetail.orderDetailDTO.quantity += item.orderDetailDTO.quantity - oldQuantity;
//			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				orderDetail.stock += -item.orderDetailDTO.quantity + oldQuantity;
//			}
		}
		item.totalOrderQuantity = orderDetail;
	}

	/**
	 * Tinh toan bien total quantity: tong so luong sp da su dung, duoc su dung sau khi nhan nut Dong y
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param item
	 */
	public void calculateTotalQuantityUsingWhenChangeValueInPopup(OrderDetailViewDTO item) {
		//Check co bien kiem tra ton kho chua, neu chua thi cap nhat
		OrderDetailViewDTO totalQuantity = orderDTO.listDistinctProduct.get(Long.valueOf(item.orderDetailDTO.productId));
		int oldQuantity = realOrderMap.get(item.orderDetailDTO.productId);
		if(totalQuantity == null) {
			totalQuantity = new OrderDetailViewDTO();
			orderDTO.listDistinctProduct.put(Long.valueOf(item.orderDetailDTO.productId), totalQuantity);
			SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
			totalQuantity.orderDetailDTO = orderDetailDTO;

			orderDetailDTO.quantity = oldQuantity;
			orderDetailDTO.productId = item.orderDetailDTO.productId;
			totalQuantity.stock = item.stock;
			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
				totalQuantity.stock -= oldQuantity;
			}
		} else {
			//Su dung oldRealOrderMap vi ham nay duoc su dung sau khi nhan nut dong y
//			int oldQuantity = realOrderMap.get(item.orderDetailDTO.productId);
			totalQuantity.orderDetailDTO.quantity += -item.orderDetailDTO.quantity + oldQuantity;
			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
				totalQuantity.stock += item.orderDetailDTO.quantity - oldQuantity;
			}
		}
		item.totalOrderQuantity = totalQuantity;
	}

	/**
	 * chinh sua listPromotionOrders sau khi them xoa sp
	 *
	 * @author: DungNX
	 * @param listPromotion
	 * @return: void
	 * @throws:
	 */
	private void showPromotionProductAfterChange(ArrayList<OrderDetailViewDTO> listPromotion) {
		int indexPromotionProduct = isContainOldProduct(productSelected, orderDTO.listPromotionOrders);
		int indexPromotionOrder = isContainOldProduct(productSelected, orderDTO.listPromotionOrder);
		int indexPromotionAcumulation = isContainOldProduct(productSelected, orderDTO.listPromotionAccumulation);
		int indexPromotionNewOpen = isContainOldProduct(productSelected, orderDTO.listPromotionNewOpen);

		ArrayList<OrderDetailViewDTO> listPromotionHasQuantity = new ArrayList<OrderDetailViewDTO>();
		for(OrderDetailViewDTO promotion: listPromotion) {
			if(promotion.orderDetailDTO.quantity > 0) {
				listPromotionHasQuantity.add(promotion);
			}
		}

		ArrayList<OrderDetailViewDTO> listPromotionTemp = new ArrayList<OrderDetailViewDTO>();
		//Xu ly thay the sp chon cho KM sp
		if(indexPromotionProduct >= 0) {
			listPromotionTemp = orderDTO.listPromotionOrders;
		}

		//Xu ly thay the sp chon cho KM don hang
		if(indexPromotionOrder >= 0) {
			listPromotionTemp = orderDTO.listPromotionOrder.get(indexPromotionOrder).listPromotionForPromo21;
		}

		//Xu ly thay the sp chon cho KM tich luy
		if(indexPromotionAcumulation >= 0) {
			listPromotionTemp = orderDTO.listPromotionAccumulation.get(indexPromotionAcumulation).listPromotionForPromo21;
		}
		//Xu ly thay sp chon KM mo moi
		if(indexPromotionNewOpen >= 0) {
			listPromotionTemp = orderDTO.listPromotionNewOpen.get(indexPromotionNewOpen).listPromotionForPromo21;
		}

		if(listPromotionTemp.size() > 0) {
			int index = -1;

			//Tim index nho nhat cua sp KM trong nhom ma nho nhat
			for(OrderDetailViewDTO promotion: listPromotion) {
				int realIndex = listPromotionTemp.indexOf(promotion);

				if((realIndex >= 0 && realIndex < index) || index == -1) {
					index = realIndex;
				}
			}
			listPromotionTemp.removeAll(listPromotion);
			listPromotionTemp.addAll(index, listPromotionHasQuantity);
		}

		calPriceKeyShop();
		// xoa het cac table layout lai
		layoutOrder();
	}

	/**
	 * kiem tra ds da chua sp hien tai
	 *
	 * @author: DungNX
	 * @param temp
	 * @param list
	 * @return
	 * @return: int
	 * @throws:
	 */
	int isContainOldProduct(OrderDetailViewDTO temp, ArrayList<OrderDetailViewDTO> list) {
		int index = -1;

		//Lay index trong ds o ngoai
		index = list.indexOf(temp);

		//Neu ko co trong ds thi tim trong ds ben trong
		if(index < 0) {
			for (OrderDetailViewDTO promotion : list) {
				int indexTemp = promotion.listPromotionForPromo21.indexOf(temp);

				if(indexTemp >= 0) {
					index = list.indexOf(promotion);
					break;
				}
			}
		}
		return index;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	boolean checkLockDateValsale() {
		// kiem tra vansale co vanlock = true moi cho vao order view
		boolean allowGoToOrderView = true;
		LockDateDTO stockLock = GlobalInfo.getInstance().getLockDateValsale();
		// chua chot ngay valsale moi cho vao order view
		if (stockLock == null || stockLock.vanLock != 1)
			allowGoToOrderView = true;
		else
			allowGoToOrderView = false;
		return allowGoToOrderView;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	boolean checkLockDate(){
		boolean isLockDate = true;
		ShopLockDTO shopLock = GlobalInfo.getInstance().getShopLockDto();
		if (DateUtils.compare(DateUtils.formatNow(DateUtils.DATE_FORMAT_DATE),
				shopLock.lockDate, DateUtils.DATE_FORMAT_DATE) == 0
				&& shopLock.shopLock == 1) // da chot ngay moi cho dat hang
			isLockDate = true;
		else
			isLockDate = false;
		return isLockDate;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param customerId
	 * @return: void
	 * @throws:
	*/
	void getCustomerDebit(){
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		ActionEvent e = new ActionEvent();
		e.viewData = b;
		e.userData = orderDTO.customer;
		e.sender = this;
		e.action = ActionEventConstant.ACTION_GET_CUSTOMER_DEBIT;
		SaleController.getInstance().handleViewEvent(e);
	}

	boolean isShowing = false;
	TextView tvTotalMoney;
	TextView tvRemain;
	VNMEditTextClearable etChietKhau;
	String ck = "";
	VNMEditTextClearable etThanhToan;
	String tt = "";
	Button btClose;
	Button btPaidDebt;

	double chietKhau = 0;
	double thanhToan = 0;

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	private void showPopUpPayment() {
		if (alertProductDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			build.setCancelable(true);
			LayoutInflater inflater = parent.getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_payment_popup, null);

			// initview
			tvTotalMoney = (TextView) view.findViewById(R.id.tvTotalMoney);
			tvRemain = (TextView) view.findViewById(R.id.tvRemain);
			etChietKhau = (VNMEditTextClearable) view.findViewById(R.id.etChietKhau);
			GlobalUtil.setFilterInputPrice(etChietKhau, 20);
			etChietKhau.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					calRemain();
				}
			});
			etThanhToan = (VNMEditTextClearable) view.findViewById(R.id.etThanhToan);
			GlobalUtil.setFilterInputPrice(etThanhToan, 20);
			etThanhToan.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
					calRemain();
				}
			});
			if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
				etChietKhau.setInputType(InputType.TYPE_NULL);
				etChietKhau.setOnClickListener(this);
				etChietKhau.setOnFocusChangeListener(this);
				etThanhToan.setInputType(InputType.TYPE_NULL);
				etThanhToan.setOnClickListener(this);
				etThanhToan.setOnFocusChangeListener(this);
			}
			btClose = (Button) PriUtils.getInstance().findViewById(view, R.id.btPaidClose, PriControl.NVBH_BANHANG_DONHANG_THANHTOAN_DONG);
			PriUtils.getInstance().setOnClickListener(btClose, this);
			btPaidDebt = (Button) PriUtils.getInstance().findViewById(view, R.id.btPaidDebt, PriControl.NVBH_BANHANG_DONHANG_THANHTOAN_GACHNO);
			PriUtils.getInstance().setOnClickListener(btPaidDebt, this);

			build.setView(view);
			alertProductDetail = build.create();
			alertProductDetail.setCancelable(false);
			alertProductDetail.setOnDismissListener(this);
			Window window = alertProductDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		if (!isShowing) {
			if (orderDTO != null) {
				tvTotalMoney.setText("" + StringUtil.formatNumber(orderDTO.debitDetailDto.remain));
				tvRemain.setText("" + StringUtil.formatNumber(orderDTO.debitDetailDto.remain));
			}
			isShowing = true;
			Window window = alertProductDetail.getWindow();
			keyboardWindowCurrent = window;
			alertProductDetail.show();
		}
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// TODO Auto-generated method stub
		isShowing = false;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	void calRemain(){
		chietKhau = StringUtil.roundPromotion(StringUtil.parseDoubleStr(etChietKhau.getText().toString()));
		thanhToan = StringUtil.roundPromotion(StringUtil.parseDoubleStr(etThanhToan.getText().toString()));
		double conlai = orderDTO.debitDetailDto.remain - chietKhau - thanhToan;
		StringUtil.display(tvRemain, conlai);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return: void
	 * @throws:
	*/
	void calPayment(){
		orderDTO.paymentMoney = thanhToan;
		orderDTO.discount = chietKhau;
		ActionEvent e = new ActionEvent();
		e.viewData = orderDTO;
		e.sender = this;
		e.action = ActionEventConstant.PAYMENT_VANSALE;
		SaleController.getInstance().handleViewEvent(e);
	}


	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v == etThanhToan || v == etChietKhau) {
			if (hasFocus) {
				onEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM_UPPER_VIEW, etChietKhau, null);
			} else {
				if (v == etThanhToan) {
					formatSetextPayment(etThanhToan);
				} else {
					formatSetextPayment(etChietKhau);
				}
			}
		}
	}

	void formatSetextPayment(EditText ed){
		String strNumber = ed.getText().toString();
		double number = StringUtil.roundPromotion(StringUtil.parseDoubleStr(strNumber));
		ed.setText(StringUtil.formatNumber(number));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		updateTime(hourOfDay, minute);
	}

	//process before switch menu
	@Override
	public boolean process(int action) {
		boolean isNeedProcess = handleBackPressed(action);
		return !isNeedProcess;
	}

	/**
	 * Tinh toan bien total quantity: tong so luong sp da su dung, duoc su dung sau khi nhan nut Dong y
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param item
	 */
	public void calculateTotalQuantityUsingWhenPressOK(OrderDetailViewDTO item) {
		//Check co bien kiem tra ton kho chua, neu chua thi cap nhat
//		OrderDetailViewDTO orderDetail = orderDTO.listDistinctProduct.get(Long.valueOf(item.orderDetailDTO.productId));
//
//		if(orderDetail == null) {
//			orderDetail = new OrderDetailViewDTO();
//			orderDTO.listDistinctProduct.put(Long.valueOf(item.orderDetailDTO.productId), orderDetail);
//			SaleOrderDetailDTO orderDetailDTO = new SaleOrderDetailDTO();
//			orderDetail.orderDetailDTO = orderDetailDTO;
//
//			orderDetailDTO.quantity = item.orderDetailDTO.quantity;
//			orderDetailDTO.productId = item.orderDetailDTO.productId;
//			orderDetail.stock = item.stock;
//			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				orderDetail.stock -= item.orderDetailDTO.quantity;
//			}
//		} else {
//			//Su dung oldRealOrderMap vi ham nay duoc su dung sau khi nhan nut dong y
//			int oldQuantity = oldRealOrderMap.get(item.orderDetailDTO.productId);
//			orderDetail.orderDetailDTO.quantity += item.orderDetailDTO.quantity - oldQuantity;
//			if (item.promotionType != OrderDetailViewDTO.PROMOTION_ACCUMULCATION_PRODUCT) {
//				orderDetail.stock += -item.orderDetailDTO.quantity + oldQuantity;
//			}
//		}
//		item.totalOrderQuantity = orderDetail;
	}

	 /**
	 * Tra thuong keyshop
	 * @author: Tuanlt11
	 * @param tbListPromotionOrder2
	 * @param detailView
	 * @param indexParent
	 * @return: void
	 * @throws:
	*/
	private void layoutPromotionForKeyShop(DMSTableView tbListPromotionOrder2, OrderDetailViewDTO detailView, int indexParent) {
		// render row ZV19, ZV20,ZV21
		if (detailView.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY || detailView.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
			// cap nhat dinh dang so luong ton kho thung/hop
			if (StringUtil.isNullOrEmpty(detailView.remaindStockFormat)) {
				detailView.remaindStockFormat = GlobalUtil
						.formatNumberProductFlowConvfact(detailView.stock,
								detailView.convfact);
			}

			if (StringUtil.isNullOrEmpty(detailView.remaindStockFormatActual)) {
				detailView.remaindStockFormatActual = GlobalUtil
						.formatNumberProductFlowConvfact(detailView.stockActual,
								detailView.convfact);
			}

			OrderPromotionKeyShopRow row = new OrderPromotionKeyShopRow(parent, isShowPrice,this);
			detailView.indexParent = indexParent;
			// Tinh so luong khuyen mai
			orderDTO.numSKUPromotion += detailView.orderDetailDTO.quantity;
			BigDecimal promotionTotalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orderDTO.promotionTotalWeight));
			promotionTotalWeight = promotionTotalWeight.add(new BigDecimal(detailView.orderDetailDTO.totalWeight)).setScale(3, RoundingMode.HALF_UP);
			orderDTO.promotionTotalWeight = promotionTotalWeight.doubleValue();
			detailView.orderType = orderDTO.orderInfo.orderType;
			detailView.getQuantityReceived(orderDTO.productQuantityReceivedList);
			row.updatePromotionKeyShop(detailView, isCheckStockTotal);
			tbListPromotionOrder2.addRow(row);
		}
	}

	 /**
	 * Tinh tong tien keyshop duoc tra thuong vao don hang
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void calPriceKeyShop() {
		// xoa cac ck cua keyshop ban dau de tinh toan lai tra thuong keyshop
		for (OrderDetailViewDTO detail : orderDTO.listPromotionKeyShop) {
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
				for (OrderDetailViewDTO buyProduct : orderDTO.listBuyOrders) {
					ArrayList<SaleOrderPromoDetailDTO> listPromoDetailRemove = new ArrayList<SaleOrderPromoDetailDTO>();
					for (SaleOrderPromoDetailDTO buyPromotion : buyProduct.listPromoDetail) {
						if (buyPromotion.programCode
								.equals(detail.orderDetailDTO.programeCode)) {
							buyProduct.orderDetailDTO
									.subtractDiscountAmount(buyPromotion.discountAmount);
							listPromoDetailRemove.add(buyPromotion);
						}
					}

					// Loai bo cac chi tiet KM cu~
					buyProduct.listPromoDetail.removeAll(listPromoDetailRemove);
				}
			}
		}
		double totalAmountFix = 0;
		// tinh tong discount hien tai chua co tinh discount keyshop
		double totalDiscountCurrent = 0;
		for (int i = 0, size = orderDTO.listBuyOrders.size(); i < size; i++) {
			OrderDetailViewDTO detail = orderDTO.listBuyOrders.get(i);
			totalAmountFix += detail.orderDetailDTO.getAmount();
			totalDiscountCurrent += detail.orderDetailDTO.getDiscountAmount();
		}
		// tong tien hien tai chua co keyshop
		double total = orderDTO.orderInfo.getAmount() - totalDiscountCurrent;

		// tinh tien keyshop, neu tien keyshop > tong tien thi chia lai tien cho
		// keyshop
		double totalKeyShop = 0;
		boolean isResetDiscount = false;
		if (orderDTO.listPromotionKeyShop.size() == 0) {
			llPromotionKeyShop.setVisibility(View.GONE);
		} else {
			llPromotionKeyShop.setVisibility(View.VISIBLE);
		}
		for (int i = 0, size = orderDTO.listPromotionKeyShop.size(); i < size; i++) {
			OrderDetailViewDTO detail = orderDTO.listPromotionKeyShop.get(i);
			// dua ck ve lai tu dau truoc khi tinh toan
			detail.orderDetailDTO.setDiscountAmount(detail.oldDiscountAmount);
			// km tien
			if ((detail.type == OrderDetailViewDTO.FREE_PRICE)
					&& !isResetDiscount) {
				if (totalKeyShop + detail.orderDetailDTO.getDiscountAmount() > total) {
					isResetDiscount = true;
					if(total >= totalKeyShop){
						detail.orderDetailDTO.setDiscountAmount(total
								- totalKeyShop);
					}else{
						// neu tong tien < totalkeyshop thi ko tinh tien keyshop vao
						detail.orderDetailDTO.setDiscountAmount(0);
					}
				} else {
					// set lai amount ban dau
					detail.orderDetailDTO
							.setDiscountAmount(detail.oldDiscountAmount);
					totalKeyShop += detail.orderDetailDTO.getDiscountAmount();
				}
			} else {
				detail.orderDetailDTO.setDiscountAmount(0);
			}
			BigDecimalRound discountAmountTemp = new BigDecimalRound(detail.orderDetailDTO.getDiscountAmount());
			if(totalAmountFix > 0)
				detail.orderDetailDTO.discountPercentage = discountAmountTemp.multiply(new BigDecimal(CalPromotions.ROUND_PERCENT)).divide(new BigDecimal(totalAmountFix)).toDouble();
			else
				detail.orderDetailDTO.discountPercentage = 0;

			// Cap nhat lai tien discount amount cua tung sp & don hang khi
			// tu KM don hang tien -> KM khac
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
				for (OrderDetailViewDTO buyProduct : orderDTO.listBuyOrders) {
					ArrayList<SaleOrderPromoDetailDTO> listPromoDetailRemove = new ArrayList<SaleOrderPromoDetailDTO>();
					for (SaleOrderPromoDetailDTO buyPromotion : buyProduct.listPromoDetail) {
						if (buyPromotion.programCode
								.equals(detail.orderDetailDTO.programeCode)) {
							buyProduct.orderDetailDTO
									.subtractDiscountAmount(buyPromotion.discountAmount);
							listPromoDetailRemove.add(buyPromotion);
						}
					}

					// Loai bo cac chi tiet KM cu~
					buyProduct.listPromoDetail.removeAll(listPromoDetailRemove);
				}
			}

			// Cap nhat lai tien discount amount cua tung sp & don hang khi
			// tu KM khac -> KM tien
			if (detail.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_MONEY) {
				// Dung de tinh toan discount cho tung sp, duoc huong toi da
				// la so tien qui, giam sai so
				double totalDiscount = 0;
				double fixDiscountAmount = detail.orderDetailDTO
						.getDiscountAmount();
				int numProduct = 0;
//				if(fixDiscountAmount == 0)
				if(BigDecimal.valueOf(fixDiscountAmount).compareTo(BigDecimal.ZERO) == 0)
					continue;
				for (int j = orderDTO.listBuyOrders.size() - 1; j >= 0; j--) {
					OrderDetailViewDTO buyProduct = orderDTO.listBuyOrders
							.get(j);
					numProduct++;
					BigDecimalRound discountPercent = new BigDecimalRound(
							StringUtil.decimalFormatSymbols("#.##",
									detail.orderDetailDTO.discountPercentage));
					double discountAmount = discountPercent
							.multiply(
									new BigDecimal(buyProduct.orderDetailDTO
											.getAmount()))
							.divide(new BigDecimal(CalPromotions.ROUND_PERCENT))
							.toDouble();
					// Tinh toan de lam tron, tong so tien <= tong
					// tien KM
					if (discountAmount > fixDiscountAmount) {
						discountAmount = fixDiscountAmount;
					}

					if (totalDiscount + discountAmount > fixDiscountAmount) {
						discountAmount = fixDiscountAmount - totalDiscount;
					} else {
						if (numProduct == orderDTO.listBuyOrders.size()) {
							discountAmount = fixDiscountAmount - totalDiscount;
						}
					}
					totalDiscount += discountAmount;
					buyProduct.orderDetailDTO.addDiscountAmount(discountAmount);
					// orderDTO.orderInfo.discount +=
					// discountAmount;
					// orderDTO.orderInfo.total -= discountAmount;

					// promo detail sau khi tinh khuyen mai lai
					SaleOrderPromoDetailDTO promoDetail = new SaleOrderPromoDetailDTO();
					promoDetail.updateData(detail, discountAmount);
					// neu so luong san pham quy dinh == 1 -> la
					// LINE, isOwner = 1, con lai la isOwner = 2
					promoDetail.isOwner = 2;
					buyProduct.listPromoDetail.add(promoDetail);
				}
			}
		}

	}

	 /**
	 * Kiem tra trang thai keyshop cua KH
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void checkKeyShopCustomer(){
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		Bundle b = new Bundle();
		b.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		b.putString(IntentConstants.INTENT_CUSTOMER_ID, ""+orderDTO.orderInfo.customerId);
		handleViewEvent(b, ActionEventConstant.ACTION_CHECK_KEYSHOP_CUSTOMER, SaleController.getInstance());
	}

}