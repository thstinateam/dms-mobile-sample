/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.table;

import android.util.SparseArray;

/**
 * Lst Sort Info Builder
 * @author: duongdt3
 * @version: 1.0 
 * @since:  09:54:52 27 Mar 2015
 */
public class DMSListSortInfoBuilder {
	SparseArray<DMSColSortInfo> lstColSort = new SparseArray<DMSColSortInfo>();
	
	/**
	 * add col sort info
	 * @author: duongdt3
	 * @since: 12:29:07 27 Mar 2015
	 * @return: DMSListSortInfoBuilder
	 * @throws:  
	 * @param colSeq so thu tu cot can sort
	 * @param sortAction sort action nam trong SortActionConstants
	 * @return
	 */
	public DMSListSortInfoBuilder addInfo(int colSeq, int sortAction){
		lstColSort.put(colSeq, new DMSColSortInfo(colSeq, sortAction));
		return this;
	}
	
	/**
	 * build list sort info
	 * @author: duongdt3
	 * @since: 12:30:36 27 Mar 2015
	 * @return: SparseArray<DMSColSortInfo>
	 * @throws:  
	 * @return
	 */
	public SparseArray<DMSColSortInfo> build(){
		return lstColSort;
	}

	public DMSListSortInfoBuilder addInfoCaseUnSensitive(int colSeq, int sortAction) {
		lstColSort.put(colSeq, new DMSColSortInfo(colSeq, sortAction, DMSSortInfo.DATA_TYPE_CASE_UN_SENSITIVE));
		return this;
	}
}
