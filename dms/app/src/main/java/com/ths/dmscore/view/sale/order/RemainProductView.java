package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.ListRemainProductDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.dto.view.RemainProductViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

// man hinh kiem hang ton
// xay dung layout chhinh roi
// xu ly nhung nut nhan button
// xay dung layout con chua
public class RemainProductView extends BaseFragment implements OnClickListener,
		OnEventControlListener, VinamilkTableListener, DMSColSortManager.OnSortChange {
	public static final int MAP_ACTION = 0;
	public static final int NOTE_ACTION = 1;
	public static final int DETAIL_ACTION = 2;
	public static final int LIST_ACTION = 3;
	public static final int REFRESH_ACTION = 4;
	private static final int ACTION_SAVE_REMAIN_SUCCESS = 1;
	private static final int ACTION_SAVE_REMAIN_FAIL = 2;

	private static final int ACTION_FROM_ORDER_VIEW = 1;

	ListRemainProductDTO listMngDTO = new ListRemainProductDTO();
	DMSTableView tbOrder;
	RemainProductView instance;
	String customerId;
	String customerCode;
	String customerName;
	String customerAddress;
	String cashierStaffID;
	String deliveryID;
	int cusTypeId;
	String is_or;
	Button btSave;

	public CustomerListItem customerListObject;

	// danh sach nhung mat hang duoc danh dau check se dua qua man hinh dat hang
	ArrayList<RemainProductViewDTO> listSelectedProduct = new ArrayList<RemainProductViewDTO>();
	// danh sach nhung mat hang can cap nhat len server vao bang
	// customer_stock_history
	ArrayList<RemainProductViewDTO> listUpdatedProduct = new ArrayList<RemainProductViewDTO>();
	private int actionFromView = 0;
	private String startTime;
	private double lat;
	private double lng;
	private Button btEnd;
	private int lastOrdinalRemain;

	// HashMap<Integer, Integer> pageIndexList = new HashMap<Integer, Integer>
	// ();
	public static RemainProductView newInstance(Bundle bundle) {
		RemainProductView instance = new RemainProductView();
		// Supply index input as an argument.

		instance.setArguments(bundle);

		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getArguments();
		if(b != null) {
			customerId = b.getString(
					IntentConstants.INTENT_CUSTOMER_ID);
			customerCode = b.getString(
					IntentConstants.INTENT_CUSTOMER_CODE);
			customerName = b.getString(
					IntentConstants.INTENT_CUSTOMER_NAME);
			customerAddress = b.getString(
					IntentConstants.INTENT_CUSTOMER_ADDRESS);
			cusTypeId = b.getInt(
					IntentConstants.INTENT_CUSTOMER_TYPE_ID);
			is_or = b.getString(IntentConstants.INTENT_IS_OR)
					.toString();
			cashierStaffID = b.getString(
					IntentConstants.INTENT_CASHIER_STAFF_ID);
			deliveryID = b.getString(
					IntentConstants.INTENT_DELIVERY_ID);
			customerListObject = (CustomerListItem) b
					.getSerializable(IntentConstants.INTENT_CUSTOMER_LIST_ITEM);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.layout_remain_product, null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_KIEMTON);

		// kiem tra status cua form va ds cac control cua form
		int formStatus = PriUtils.getInstance().getFormStatus();
		if(formStatus != 1){
			// neu status cua form != 1 thi chuyen sang man hinh khac / hoac back ra man hinh truoc
			// truong hop di vao order view
			gotoCreateOrder(null, false);
		}

		initView(v);

		parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_REMAIN_PRODUCT_VIEW));
		this.startTime = DateUtils.now();

		if (actionFromView == 0) {
			getListOrder(customerId, false);
			getLastOrdinalRemainSaveInDay();
		}

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (actionFromView == 0) {
		} else {
			renderLayout();
		}
		
//		//chuyen qua dat hang
//		if (StringUtil.isNullOrEmpty(parent.draftOrderId)) {
//			gotoCreateOrder(listSelectedProduct, false);
//		} else {
//			gotoViewOrder(parent.draftOrderId, false);
//		}
	}

	private void initView(View v) {
		btSave = (Button) PriUtils.getInstance().findViewById(v, R.id.btSave, PriHashMap.PriControl.NVBH_BANHANG_KIEMTON_LUU);
		PriUtils.getInstance().setOnClickListener(btSave, this);
		btEnd = (Button) v.findViewById(R.id.btEnd);
		btEnd.setOnClickListener(this);
		// GlobalUtil.setEnableButton(btSave, false);

		tbOrder = (DMSTableView) v.findViewById(R.id.tbOrder);
		tbOrder.setListener(this);
		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
						.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
						.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
						.build();
		RemainProductRow header = new RemainProductRow(parent, null);
		//header.renderHeader();
		initHeaderTable(tbOrder,header, lstSort, this);

	}

	/**
	 * lay danh sach hang ma khach hang do' da dat cua nhan vien do' cua shop
	 * do' trong vong 2 thang truoc (hang do co trong CTKM hay kg, co trong
	 * CTTTam hay kg, co so luong ton trong kho cua nha phan phoi <= 0 hay kg,
	 * sort theo mat hang trong tam, mat hang khuyen mai, ma mat hang
	 *
	 * @param orderType
	 * @param staffIdGS
	 */

	private void getListOrder(String customerId, boolean isGotCount) {
		if (!parent.isShowProgressDialog()) {
			parent.showLoadingDialog();
		}

		Bundle data = new Bundle();
		// data.putString(IntentConstants.INTENT_ORDER_TYPE, orderType);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, /* "131105" */
				customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				Integer.toString(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		data.putBoolean(IntentConstants.INTENT_IS_GOT_COUNT, isGotCount);
		data.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, cusTypeId);
		data.putString(IntentConstants.INTENT_PAGE, "");
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbOrder.getSortInfo());

		handleViewEvent(data, ActionEventConstant.GET_REMAIN_PRODUCT, SaleController.getInstance());
	}
	
	private void getLastOrdinalRemainSaveInDay() {
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance() .getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, Integer.toString(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		
		handleViewEvent(data, ActionEventConstant.GET_LAST_ORDINAL_REMAIN_PRODUCT, SaleController.getInstance());
	}

	 /**
	 * Render layout
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderLayout() {
		tbOrder.clearAllData();
		GlobalUtil.setEnableButton(btSave, true);
		int indexPage = tbOrder.getPagingControl().getCurrentPage() - 1;
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbOrder.getPagingControl().getCurrentPage() - 1);
		for (int i = indexPage * Constants.NUM_ITEM_PER_PAGE; i < (indexPage + 1)
				* Constants.NUM_ITEM_PER_PAGE
				&& i < listMngDTO.listDTO.size(); i++) {
			// for (int i = 0;i< 1; i++) {
			RemainProductViewDTO dto = listMngDTO.listDTO.get(i);
			RemainProductRow row = new RemainProductRow(parent, this);

			row.setClickable(true);
			row.setTag(Integer.valueOf(pos));
			if (listSelectedProduct.contains(dto)) {
				row.renderLayout(pos, listSelectedProduct
						.get(listSelectedProduct.indexOf(dto)));
			} else {
				row.renderLayout(pos, dto);
			}
			// khi quay tro lai thi khong duoc edit so luong ton
			if (actionFromView == ACTION_FROM_ORDER_VIEW)
				row.edRemainQuanlity.setEnabled(false);

			pos++;

			tbOrder.addRow(row);
		}

		if (tbOrder.getPagingControl().totalPage < 0)
			tbOrder.setTotalSize(listMngDTO.total,1);

	}

	// private int getBeginIndexInList() {
	// if (pageIndexList.containsKey(new
	// Integer(tbOrder.getPagingControl().getCurrentPage())))
	// return pageIndexList.get((new
	// Integer(tbOrder.getPagingControl().getCurrentPage()))).intValue();
	// return -1;
	// }

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_REMAIN_PRODUCT:
			listMngDTO = new ListRemainProductDTO();

			ListRemainProductDTO listResult = (ListRemainProductDTO) modelEvent
					.getModelData();
			int pos = 1 + Constants.NUM_ITEM_PER_PAGE
					* (tbOrder.getPagingControl().getCurrentPage() - 1);

			for (int i = 0; i < listResult.listDTO.size(); i++) {
				listResult.listDTO.get(i).stt = pos;
				pos++;
				listMngDTO.listDTO.add(listResult.listDTO.get(i));
			}
			listMngDTO.total = listResult.total;
			// sortListByProperty(listMngDTO.listDTO);
			if (listMngDTO.listDTO.size() > 0) {
				renderLayout();
				parent.closeProgressDialog();
				// TamPQ: them luong dinh vi
				processWaitingPosition();
			} else {
				// gotoCreateOrder(listSelectedProduct, false);
				// parent.closeProgressDialog();
				if (StringUtil.isNullOrEmpty(parent.draftOrderId)) {
					gotoCreateOrder(listSelectedProduct, false);
				} else {
					gotoViewOrder(parent.draftOrderId, false);
				}

				parent.closeProgressDialog();
			}
			break;
		case ActionEventConstant.GET_LAST_ORDINAL_REMAIN_PRODUCT: {
			Bundle b = (Bundle) modelEvent.getModelData();
			lastOrdinalRemain = b.getInt(IntentConstants.INTENT_LAST_ORDINAL_REMAIN);
			break;
		}

		case ActionEventConstant.SAVE_NUMBER_REMAIN_PRODUCT:
			customerListObject.isTodayCheckedRemain = true;
			insertActionLog();
			parent.closeProgressDialog();
			parent.removeMenuCloseCustomer();
			gotoCreateOrder(listSelectedProduct, true);
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		parent.closeProgressDialog();
		switch (actionEvent.action) {
		// case ActionEventConstant.G:
		// parent.showDialog("co loi xay ra");
		// break;
		case ActionEventConstant.SAVE_NUMBER_REMAIN_PRODUCT:
			// insertActionLog();
			GlobalUtil.setEnableButton(btSave, true);
			parent.removeMenuCloseCustomer();
			super.handleErrorModelViewEvent(modelEvent);
			break;
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER:

			GlobalUtil.setEnableButton(btSave, true);
			super.handleErrorModelViewEvent(modelEvent);
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		if (control == tbOrder) {
			savePageDTO(tbOrder.getPagingControl().getOldPage());
			// if (getBeginIndexInList() == -1){
			// getListOrder(customerId, true);
			// }
			// else
			renderLayout();
			// load more data for table product list
		}
	}

	/**
	 * Den man hinh tao don hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoViewOrder(String orderId, boolean hasData) {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				customerAddress);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, cusTypeId);
		bundle.putString(IntentConstants.INTENT_IS_OR, is_or);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID, deliveryID);
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				cashierStaffID);
		bundle.putBoolean(IntentConstants.INTENT_HAS_REMAIN_PRODUCT, hasData);
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_EDIT);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * Luong dinh vi
	 *
	 * @author : TamPQ since : 1.0
	 */
	public void processWaitingPosition() {
		lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLatitude();
		lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
				.getLongtitude();

		// check dinh vi cua khach hang
		if (lat <= 0 || lng <= 0 && Integer.parseInt(is_or) == 0) {
			parent.reStartLocatingWithWaiting();
		}
	}

	/**
	 * Ket thuc ghe tham khi ghe tham khach hang khac
	 *
	 * @author : BangHN since : 1.0
	 */
	private void insertActionLog() {
		// ket thuc ghe tham
		// save to action log
		try {
			ActionLogDTO action = new ActionLogDTO();
			action.staffId = GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId();
			action.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			action.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
			action.aCustomer.customerId = Long.parseLong(customerId);
			action.objectId = customerId;
			action.objectType = "3";
			action.startTime = this.startTime;
			action.endTime = DateUtils.now();
			action.isOr = Integer.parseInt(is_or);
			action.aCustomer.lat = this.customerListObject.aCustomer.lat;
			action.aCustomer.lng = this.customerListObject.aCustomer.lng;
			action.shopId = Long.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			action.routingId = this.customerListObject.routingId;
			parent.requestInsertActionLog(action);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// String dateTimePattern =
		// "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
		// Pattern pattern = Pattern.compile(dateTimePattern);
		if (v == btSave) {
			// goToScreen();
			parent.hideKeyboardCustom();
			parent.runOnUiThread(new Runnable() {

				@Override
				public void run() {

					// TODO Auto-generated method stub
					if (tbOrder.getChildCount() > 0) {
						GlobalUtil.forceHideKeyboard(parent);
					}
				}
			});
			if (!checkDistanceProcessAfterVisit()) {
				ActionLogDTO al = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				String mess = StringUtil
						.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_1)
						+ " "
						+ al.aCustomer.customerCode
						+ " - "
						+ al.aCustomer.customerName
						+ " "
						+ StringUtil
								.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_2);

				GlobalUtil.showDialogConfirm(parent, mess,
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
						false);
			} else {
				if (actionFromView == 0) {		
					GlobalUtil.showDialogConfirm(this, parent,
									StringUtil.getString(R.string.TEXT_CONFIRM_REMAIN_PRODUCT_VIEW),
									StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
									ACTION_SAVE_REMAIN_SUCCESS,
									StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
									ACTION_SAVE_REMAIN_FAIL, null);
				} else {
					savePageDTO(tbOrder.getPagingControl().getCurrentPage());
					// // chuyen qua man hinh dat hang
					// gotoCreateOrder(listSelectedProduct, true);
					// chuyen qua man hinh dat hang
					if (StringUtil.isNullOrEmpty(parent.draftOrderId)) {
						gotoCreateOrder(listSelectedProduct, true);
					} else {
						gotoViewOrder(parent.draftOrderId, true);
					}
				}
			}
		} else if(v == btEnd){
			passSaveRemainProduct();
		} else {
			super.onClick(v);
		}

	}

	/**
	 * Chuyen den man hinh dat hang
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	private void gotoCreateOrder(ArrayList<RemainProductViewDTO> listCheck,
			boolean hasData) {
		if (!hasData) {
			CustomerListView frag = (CustomerListView) findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
			if (frag != null) {
				frag.isBackFromPopStack = true;
			}
			GlobalUtil.popBackStack(parent);
		}
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, customerName);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				customerAddress);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID, deliveryID);
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				cashierStaffID);
		bundle.putString(IntentConstants.INTENT_ORDER_ID, "0");
		bundle.putString(IntentConstants.INTENT_IS_OR, is_or);
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, cusTypeId);
		bundle.putSerializable(IntentConstants.INTENT_SUGGEST_ORDER_LIST,
				listSelectedProduct);
		bundle.putBoolean(IntentConstants.INTENT_HAS_REMAIN_PRODUCT, hasData);
		bundle.putInt(IntentConstants.INTENT_STATE, OrderViewDTO.STATE_NEW);
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, customerListObject.routingId);
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * luu hang ton xuong CSDL
	 */
	private void saveRemainProduct() {

		savePageDTO(tbOrder.getPagingControl().getCurrentPage());

		// cap nhat du lieu xuong db va server
		if (listUpdatedProduct.size() > 0) {
			parent.showLoadingDialog();
			actionFromView = ACTION_FROM_ORDER_VIEW;
			Bundle b = new Bundle();
			b.putSerializable(IntentConstants.INTENT_DATA_LIST_PRODUCT_REMAIN, listUpdatedProduct);
			handleViewEvent(b, ActionEventConstant.SAVE_NUMBER_REMAIN_PRODUCT, SaleController.getInstance(), true);
		} else {
			passSaveRemainProduct();
		}
		
//		} else if(GlobalInfo.getInstance().isNeedSaveRemainProduct() && customerListObject.isOr == 0 && !customerListObject.isTodayCheckedRemain) {
//			parent.showDialog(StringUtil.getString(R.string.TEXT_NEED_SAVE_REMAIN));
//		} else {
//			actionFromView = ACTION_FROM_ORDER_VIEW;
//			gotoCreateOrder(listSelectedProduct, true);
//		}
	}
	
	/**
	 * Bo qua luu kiem ton
	 */
	private void passSaveRemainProduct() {
		if(GlobalInfo.getInstance().isNeedSaveRemainProduct() && customerListObject.isOr == 0 && !customerListObject.isTodayCheckedRemain) {
			parent.showDialog(StringUtil.getString(R.string.TEXT_NEED_SAVE_REMAIN));
		} else {
			actionFromView = ACTION_FROM_ORDER_VIEW;
			gotoCreateOrder(listSelectedProduct, true);
		}
	}

//	public ArrayList<RemainProductViewDTO> sortListByProperty(
//			ArrayList<RemainProductViewDTO> orgList) {
//		ArrayList<RemainProductViewDTO> result = orgList;
//		Collections.sort(result, new Comparator<RemainProductViewDTO>() {
//			public int compare(RemainProductViewDTO s1, RemainProductViewDTO s2) {
//				// sap xep theo status
////				if (s1.sign > s2.sign)
////					return -1;
////				if (s1.sign > s2.sign) {
////					return 1;
////				}
//				// if (s1.IS_FOCUS == s2.IS_FOCUS) {
//				// return 0;
//				// }
//				// sap xep theo demand
//				// if (s1.GS > s2.GS)
//				// return -1;
//				// if (s1.GS < s2.GS)
//				// return 1;
//				// if (s1.GS == s2.GS)
//				// return 0;
//				// sap xep theo ten
//				return s1.getPRODUCT_CODE().compareToIgnoreCase(
//						s2.getPRODUCT_CODE());
//
//			}
//		});
//		return result;
//	}

	 /**
	 * Luu thong tin kiem ton
	 * @author: Tuanlt11
	 * @param page
	 * @return: void
	 * @throws:
	*/
	private void savePageDTO(int page) {
		// TODO Auto-generated method stub
		// listSelectedProduct.clear();
		// listUpdatedProduct.clear();
		int position = Constants.NUM_ITEM_PER_PAGE * (page - 1);
		for (int i = 0,size = tbOrder.getListChildRow().size(); i < size; i++) {
			RemainProductRow row = (RemainProductRow) tbOrder.getListChildRow().get(i);
			RemainProductViewDTO sale = listMngDTO.listDTO.get(position + i);

			sale.setCUSTOMER_ID(Long.parseLong(customerId));
			sale.setSTAFF_ID(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			if (!StringUtil.isNullOrEmpty(row.edRemainQuanlity.getText()
					.toString())) {
				try {
						int quantity = GlobalUtil.calRealOrder(
								row.edRemainQuanlity.getText().toString(),
								sale.CONVFACT);
						sale.setQUANTITY(quantity);
						sale.REMAIN_NUMBER = quantity + "";
						sale.ordinal = lastOrdinalRemain + 1;
				} catch (Exception e) {
					return;
				}
				if (!listUpdatedProduct.contains(sale)){
					listUpdatedProduct.add(sale);
				}
			}
//			if (row.cbCheck.isChecked()) {
//				sale.setCheck(true);
//				int hint = 0;
//				try {
//					hint = Integer.parseInt(sale.getHINT_NUMBER());
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
//				if (!listSelectedProduct.contains(sale) && hint > 0)
//					listSelectedProduct.add(sale);
//			}
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub

		switch (eventType) {
		case ACTION_SAVE_REMAIN_SUCCESS:
			saveRemainProduct();
			break;
		case ACTION_SAVE_REMAIN_FAIL:
			GlobalUtil.setEnableButton(btSave, true);
			break;
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		if(action == ActionEventConstant.SHOW_KEYBOARD_CUSTOM){
			parent.showKeyboardCustom(control);
		}
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		// TODO Auto-generated method stub

		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// cau request du lieu man hinh
				// listMngDTO = new ListRemainProductDTO();
				getListOrder(customerId, true);
			}
			break;
		case ActionEventConstant.BROADCAST_ORDER_VIEW:
			actionFromView = ACTION_FROM_ORDER_VIEW;
			break;
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}

	}

	/* (non-Javadoc)
	 * @see DMSColSortManager.OnSortChange#onSortChange(DMSTableView, DMSSortInfo)
	 */
	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		getListOrder(customerId, false);
	}
}
