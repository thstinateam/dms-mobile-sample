/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.GlobalUtil.AppInfo;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 * App Change Receiver, nhan thay doi khi cai dat ung dung moi
 * 
 * @author: duongdt3
 * @version: 1.0
 * @since: 17:15:59 26 Jan 2015
 */
public class AppChangeReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		MyLog.d("AppChangeReceiver", "onReceive isVaildateMockLocation: " 
				+ GlobalInfo.getInstance().isVaildateMockLocation());
		
		//chi xu ly khi can validate mock location
		if (intent != null && intent.getData() != null) {
			String action = intent.getAction();
			
			PackageManager pm = context.getPackageManager();
			Integer uid = intent.getIntExtra(Intent.EXTRA_UID, Integer.MIN_VALUE);
			String packageName = pm.getNameForUid(uid);
			
			MyLog.d("AppChangeReceiver", action + " " + packageName
					+ " " + intent.getData().getSchemeSpecificPart());
			//khi cai dat ung dung moi, nang cap ung dung cu thi kiem tra lai quyen 
			if (Intent.ACTION_PACKAGE_ADDED.equals(action)){
				onPackageChanged(context, pm, packageName);
			} else if(Intent.ACTION_PACKAGE_REMOVED.equals(action)){
				onPackageRemoved(context, pm, packageName);
			}
			/*else if(Intent.ACTION_PACKAGE_DATA_CLEARED.equals(action)){
				onPackageCleared(context, pm, packageName);
			}*/
		}
	}

	/**
	 * detect other apps clear data
	 * @author: duongdt3
	 * @since: 16:08:59 11 Feb 2015
	 * @return: void
	 * @throws:  
	 * @param context
	 * @param pm
	 * @param packageName
	 */
	@SuppressWarnings("unused")
	private void onPackageCleared(Context context, PackageManager pm,
			String packageName) {
		MyLog.d("AppChangeReceiver onPackageCleared", packageName + "");
	}

	/**
	 * on package removed
	 * @author: duongdt3
	 * @since: 16:07:18 11 Feb 2015
	 * @return: void
	 * @throws:  
	 * @param pm
	 * @param packageName
	 */
	private void onPackageRemoved(Context ctx, PackageManager pm, String packageName) {
		MyLog.d("AppChangeReceiver onPackageRemoved", packageName + "");
		if(!StringUtil.isNullOrEmpty(packageName) 
				&& GlobalInfo.getInstance().isVaildateMockLocation()){
			List<AppInfo> lstAppMockEnable = GlobalUtil.getListMockApps(ctx, true); 
			//khong con ung dung nao co quyen mock thi cho phep nhan request vi tri
			if ((lstAppMockEnable == null || lstAppMockEnable.isEmpty()) && !GlobalUtil.isMockLocation()) {
				MyLog.e("AppChangeReceiver onPackageRemoved", "empty list app mock");
				//neu truoc fullDate co gia lap vi tri
				if (GlobalBaseActivity.getInstance().isFakeGPS()) {
					GlobalBaseActivity.getInstance().setFakeGPS(false);
					//lay lai ds vi tri duoc cache
					GlobalBaseActivity.reloadListKnownLocation();
					MyLog.e("AppChangeReceiver onPackageRemoved", "set isFakeGPS = false, reload list cache Location");
				}
			} else{
				MyLog.e("AppChangeReceiver onPackageRemoved", "list app mock not empty");
				GlobalBaseActivity.getInstance().setFakeGPS(true);
				MyLog.e("AppChangeReceiver onPackageRemoved", "set isFakeGPS = true");
			}
		}
	}

	/**
	 * on Package add
	 * @author: duongdt3
	 * @since: 16:06:36 11 Feb 2015
	 * @return: void
	 * @throws:  
	 * @param pm
	 * @param packageName
	 */
	private void onPackageChanged(Context ctx, PackageManager pm, String packageName) {
		MyLog.d("AppChangeReceiver onPackageChanged", packageName + "");
		if(!StringUtil.isNullOrEmpty(packageName) 
				&& GlobalInfo.getInstance().isVaildateMockLocation()){
			try {
				boolean isFakeGPSApp = GlobalUtil.isAppFakeLocation(pm, packageName);
				if (isFakeGPSApp) {
					GlobalBaseActivity.getInstance().setFakeGPS(true);
					String message = StringUtil.getString(R.string.TEXT_DETECT_GPS_FAKE_APPS_INSTALL);
					message += "\n " + GlobalUtil.getAppName(pm, packageName);
					GlobalUtil.showToastLong(message);
					MyLog.d("AppChangeReceiver onPackageChanged", packageName + " isFakeGPS");
				}
			} catch (Exception e) {
				String errorStr = MyLog.printStackTrace(e); 
				MyLog.e("AppChangeReceiver onPackageChanged", errorStr);
				ServerLogger.sendLog(errorStr, TabletActionLogDTO.LOG_CLIENT);
			}
		}
	}
}
