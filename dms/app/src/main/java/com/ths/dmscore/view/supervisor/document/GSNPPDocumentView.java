/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.document;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.OfficeDocumentDTO;
import com.ths.dmscore.dto.view.OfficeDocumentListDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.SupervisorActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 *
 * MH danh sach cong van
 *
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
public class GSNPPDocumentView extends BaseFragment implements
		VinamilkTableListener, OnItemSelectedListener, OnEventControlListener,
		OnClickListener, OnTouchListener, OnDateSetListener {
	// menu cong van
	public static final int ACTION_DOCUMENT = 0;
	// menu cttb
	public static final int ACTION_PROMOTION = 1;
	// menu ctkm
	public static final int ACTION_DISPLAY = 2;
	// san pham
	public static final int ACTION_PRODUCT_LIST = 3;
	private final int MENU_SEARCH_IMAGE = 18;

	private DMSTableView tbDocumentList;
	// loai chuong trinh
	private Spinner spinnerDocumentType;
	// tu ngay
	private VNMEditTextClearable edFromDate;
	// den ngay
	private VNMEditTextClearable edToDate;
	// tim kiem
	private Button btDocumentSearch;
	// kiem tra lan dau tien vao man hinh
	private boolean isFirstTime;
	// loai cong van
	private List<ApParamDTO> listType = null;
	private SupervisorActivity parent;
	private int comboboxtypeselected = 0;
	// bien kiem tra viec load them (phan trang)
	private boolean checkLoadMore;
	// dto view
	// thong tin cong van
	public OfficeDocumentListDTO officeDocument = new OfficeDocumentListDTO();
	private int currentCalender;
	private static final int DATE_FROM_CONTROL = 1;
	private static final int DATE_TO_CONTROL = 2;
	int currentTypeIndex = 0;
	// current coducment
	private OfficeDocumentDTO currentDocument;
	// bien kiem tra refresh
	private boolean isRefresh = false;
	private boolean isGetType = true;

	private TextView tvDocumentType;

	private TableLayout llSearchDocument;

	private TextView tvFromDateToDate;

	private TextView tvCrossLine;

	public static GSNPPDocumentView getInstance(Bundle bundle) {
		GSNPPDocumentView instance = new GSNPPDocumentView();
		instance.setArguments(bundle);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		parent = (SupervisorActivity) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		isFirstTime = true;
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.layout_document_view, null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_GS_VIEW_LIST_DOCUMENT));
		tbDocumentList = (DMSTableView) view
				.findViewById(R.id.tbDocumentList);
		tbDocumentList.setListener(this);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DANHMUC_CONGVAN);
		spinnerDocumentType = (Spinner) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.spinnerDocumentType,
						PriHashMap.PriControl.GSNPP_CONGVAN_LOAICONGVAN);
		tvDocumentType = (TextView) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.tvDocumentType,
						PriHashMap.PriControl.GSNPP_CONGVAN_LOAICONGVAN);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerDocumentType, this);

		edFromDate = (VNMEditTextClearable) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.edFromDate,
						PriHashMap.PriControl.GSNPP_CONGVAN_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		tvFromDateToDate = (TextView) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.tvFromDateToDate,
						PriHashMap.PriControl.GSNPP_CONGVAN_TUNGAY);
		tvCrossLine = (TextView) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.tvCrossLine,
						PriHashMap.PriControl.GSNPP_CONGVAN_DENNGAY);
		this.edFromDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		PriUtils.getInstance().setOnTouchListener(edFromDate, this);
		this.edFromDate.setIsHandleDefault(false);
		this.edFromDate.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		edToDate = (VNMEditTextClearable) PriUtils.getInstance()
				.findViewByIdGone(view, R.id.edToDate,
						PriHashMap.PriControl.GSNPP_CONGVAN_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		this.edToDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		PriUtils.getInstance().setOnTouchListener(edToDate, this);
		this.edToDate.setIsHandleDefault(false);
		this.edToDate.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		btDocumentSearch = (Button) view.findViewById(R.id.btDocumentSearch);
		btDocumentSearch.setOnClickListener(this);

		llSearchDocument = (TableLayout) PriUtils.getInstance().findViewByIdGone(
				view, R.id.llSearchDocument, PriHashMap.PriControl.GSNPP_CONGVAN_TIMKIEM);

		updateData();
		refresh();
		isGetType = true;
		getDocumentList(StringUtil.EMPTY_STRING,
				officeDocument.fromDateForRequest,
				officeDocument.toDateForRequest);
		return view;
	}

	/**
	 *
	 * render layout
	 *
	 * @author: YenNTH
	 * @param model
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
//		if (isFirstTime) {
//			DocumentRow row = new DocumentRow(parent, null, this.getTAG());
//			tbDocumentList.addHeader(row);
//			isFirstTime = false;
//		}
//		tbDocumentList.clearAllData();
//		int pos = (tbDocumentList.getPagingControl().getCurrentPage() - 1)
//				* Constants.NUM_ITEM_PER_PAGE + 1;
//		if (officeDocument.list.size() > 0) {
//			for (int i = 0, n = officeDocument.list.size(); i < n; i++) {
//				DocumentRow row = new DocumentRow(parent, null, this.getTAG());
//
//				row.setClickable(true);
//				row.setOnClickListener(this);
//				row.renderLayout(pos, officeDocument.list.get(i));
//				row.setListener(this);
//				pos++;
//
//				tbDocumentList.addRow(row);
//			}
//		} else {
//			tbDocumentList.showNoContentRow();
//		}
//		tbDocumentList.setNumItemsPage(Constants.NUM_ITEM_PER_PAGE);
//		if (tbDocumentList.getPagingControl().totalPage < 0) {
//			tbDocumentList.setTotalSize(officeDocument.getTotal(),1);
//		}
	}

	/**
	 *
	 * cap nhat du lieu cho loai cong van
	 *
	 * @author: YenNTH
	 * @param modelData
	 * @return: void
	 * @throws:
	 */
	private void updateData() {
		// TODO Auto-generated method stub
		if (listType == null) {
//			listType = new ArrayList<DisplayProgrameItemDTO>();
//			DisplayProgrameItemDTO itemDTO = new DisplayProgrameItemDTO();
//			itemDTO.name = StringUtil.getString(R.string.TEXT_ALL);
//			itemDTO.value = StringUtil.EMPTY_STRING;
//			listType.add(itemDTO);
//			itemDTO = new DisplayProgrameItemDTO();
//			itemDTO.name = StringUtil.getString(R.string.TEXT_CTKM);
//			itemDTO.value = "1";
//			listType.add(itemDTO);
//			itemDTO = new DisplayProgrameItemDTO();
//			itemDTO.name = StringUtil.getString(R.string.TEXT_HEADER_MENU_CTTB);
//			itemDTO.value = "2";
//			listType.add(itemDTO);
//			itemDTO = new DisplayProgrameItemDTO();
//			itemDTO.name = StringUtil.getString(R.string.TEXT_HEADER_MENU_CTTT);
//			itemDTO.value = "3";
//			listType.add(itemDTO);
			return;
		}
		ApParamDTO itemDTO = new ApParamDTO();
		itemDTO.setDescription(StringUtil.getString(R.string.TEXT_ALL));
		itemDTO.setValue(StringUtil.EMPTY_STRING);
		listType.add(0, itemDTO);
		int lengthType = listType.size();
		String typeName[] = new String[lengthType];
		// khoi tao gia tri cho loai CT
		for (int i = 0; i < lengthType; i++) {
			ApParamDTO dto = listType.get(i);
			typeName[i] = dto.getDescription();
		}
		SpinnerAdapter adapterType = new SpinnerAdapter(parent,
				R.layout.simple_spinner_item, typeName);
		this.spinnerDocumentType.setAdapter(adapterType);
		spinnerDocumentType.setSelection(comboboxtypeselected);

	}

	/**
	 *
	 * refresh
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	public void refresh() {
		String dateTimePattern = StringUtil
				.getString(R.string.TEXT_DATE_TIME_PATTERN);
		Pattern pattern = Pattern.compile(dateTimePattern);
		if (!StringUtil.isNullOrEmpty(this.edFromDate.getText().toString())) {
			String strTN = this.edFromDate.getText().toString().trim();
			officeDocument.fromDate = edFromDate.getText().toString();
			Matcher matcher = pattern.matcher(strTN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date tn = StringUtil.stringToDate(strTN,
							StringUtil.EMPTY_STRING);
					String strFindTN = StringUtil.dateToString(tn,
							DateUtils.DATE_STRING_YYYY_MM_DD);

					officeDocument.fromDateForRequest = strFindTN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			officeDocument.fromDate = StringUtil.EMPTY_STRING;
			officeDocument.fromDateForRequest = StringUtil.EMPTY_STRING;
		}
		if (!StringUtil.isNullOrEmpty(this.edToDate.getText().toString())) {
			String strDN = this.edToDate.getText().toString().trim();
			officeDocument.toDate = edToDate.getText().toString();
			Matcher matcher = pattern.matcher(strDN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date dn = StringUtil.stringToDate(strDN,
							StringUtil.EMPTY_STRING);
					String strFindDN = StringUtil.dateToString(dn,
							DateUtils.DATE_STRING_YYYY_MM_DD);

					officeDocument.toDateForRequest = strFindDN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			officeDocument.toDate = StringUtil.EMPTY_STRING;
			officeDocument.toDateForRequest = StringUtil.EMPTY_STRING;
		}
		// luu lai combobox chuong trinh
		if (officeDocument == null) {
			officeDocument.programeCode = StringUtil.EMPTY_STRING;
		} else {
			int selected = spinnerDocumentType.getSelectedItemPosition();
			if (selected < 0) {
				selected = 0;
			}
			officeDocument.programeCode = String.valueOf(selected);
		}
	}

	/**
	 *
	 * lay danh sach cong van
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	private void getDocumentList(String type, String fromDate, String toDate) {
		int indexPage = 0;
		if (checkLoadMore) {
			indexPage = (tbDocumentList.getPagingControl().getCurrentPage() - 1);
		}
		String page = " limit " + (indexPage * Constants.NUM_ITEM_PER_PAGE)
				+ "," + Constants.NUM_ITEM_PER_PAGE;

		Bundle data = new Bundle();
		if (!StringUtil.isNullOrEmpty(type)) {
			data.putInt(IntentConstants.INTENT_DOCUMENT_TYPE,
					Integer.valueOf(type));
		}
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE, fromDate);
		data.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, toDate);
		data.putString(IntentConstants.INTENT_PAGE, page);
		data.putBoolean(IntentConstants.INTENT_CHECK_PAGGING, checkLoadMore);
		data.putBoolean(IntentConstants.INTENT_IS_GET_LIST_TYPE, isGetType);
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		ActionEvent e = new ActionEvent();
		e.viewData = data;
		if (checkLoadMore == false) {
			e.tag = 11;
		}
		e.sender = this;
		e.action = ActionEventConstant.GET_LIST_DOCUMENT;
		SupervisorController.getInstance().handleViewEvent(e);

	}

	/**
	 *
	 * Chuyen toi ds san pham
	 *
	 * @author: ThanhNN8
	 * @return: void
	 * @throws:
	 */
	private void gotoProductListView() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.GO_TO_PRODUCT_LIST;
		SupervisorController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * chuyen den man hinh danh sach trung bay
	 *
	 * @author: ThanhNN8
	 * @return: void
	 * @throws:
	 */
	private void gotoDisplayProgramView() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.GO_TO_DISPLAY_PROGRAM;
		SupervisorController.getInstance().handleSwitchFragment(e);
	}

	/**
	 * chuyen den man hinh danh sach trung bay
	 *
	 * @author: ThanhNN8
	 * @return: void
	 * @throws:
	 */
	private void gotoPromotionProgramView() {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		e.viewData = new Bundle();
		e.action = ActionEventConstant.GO_TO_PROMOTION_PROGRAM;
		SupervisorController.getInstance().handleSwitchFragment(e);
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_LIST_DOCUMENT:
			if (actionEvent.tag == 11) {
				tbDocumentList.getPagingControl().totalPage = -1;
				tbDocumentList.getPagingControl().setCurrentPage(1);
			}
			officeDocument = (OfficeDocumentListDTO) modelEvent.getModelData();
			if (isGetType) {
				listType = officeDocument.listType;
				updateData();
			}
			renderLayout();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {

		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		checkLoadMore = false;
		if (v == btDocumentSearch) {
			GlobalUtil.forceHideKeyboard(parent);
			refresh();
			// luu lai gia tri de thuc hien tim kiem
			String dateTimePattern = StringUtil
					.getString(R.string.TEXT_DATE_TIME_PATTERN);
			Pattern pattern = Pattern.compile(dateTimePattern);
			if (!StringUtil.isNullOrEmpty(this.edFromDate.getText().toString())) {
				String strTN = this.edFromDate.getText().toString().trim();
				officeDocument.fromDate = edFromDate.getText().toString();
				Matcher matcher = pattern.matcher(strTN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strTN,
								StringUtil.EMPTY_STRING);
						String strFindTN = StringUtil.dateToString(tn,
								DateUtils.DATE_STRING_YYYY_MM_DD);

						officeDocument.fromDateForRequest = strFindTN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil
								.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				officeDocument.fromDate = StringUtil.EMPTY_STRING;
				officeDocument.fromDateForRequest = StringUtil.EMPTY_STRING;
			}
			if (!StringUtil.isNullOrEmpty(this.edToDate.getText().toString())) {
				String strDN = this.edToDate.getText().toString().trim();
				officeDocument.toDate = edToDate.getText().toString();
				Matcher matcher = pattern.matcher(strDN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date dn = StringUtil.stringToDate(strDN,
								StringUtil.EMPTY_STRING);
						String strFindDN = StringUtil.dateToString(dn,
								DateUtils.DATE_STRING_YYYY_MM_DD);

						officeDocument.toDateForRequest = strFindDN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil
								.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				officeDocument.toDate = StringUtil.EMPTY_STRING;
				officeDocument.toDateForRequest = StringUtil.EMPTY_STRING;
			}
			// luu lai combobox chuong trinh
			if (officeDocument.list == null) {
				officeDocument.programeCode = StringUtil.EMPTY_STRING;
			} else {
				int selected = spinnerDocumentType.getSelectedItemPosition();
				if (selected < 0) {
					selected = 0;
				}
				if (listType != null && listType.size() > 0) {
					ApParamDTO itemDTO = listType.get(selected);
					officeDocument.programeCode = itemDTO.getValue();
				}
			}

			if (!StringUtil.isNullOrEmpty(officeDocument.fromDateForRequest)
					&& !StringUtil
							.isNullOrEmpty(officeDocument.toDateForRequest)
					&& DateUtils.compareDate(officeDocument.fromDateForRequest,
							officeDocument.toDateForRequest) == 1) {
				GlobalUtil
						.showDialogConfirm(this, parent, StringUtil
								.getString(R.string.TEXT_DATE_TIME_INVALID_2),
								StringUtil
										.getString(R.string.TEXT_BUTTON_CLOSE),
								0, null, false);
			} else {
				isGetType = false;
				getDocumentList(officeDocument.programeCode,
						officeDocument.fromDateForRequest,
						officeDocument.toDateForRequest);
			}
		} else {
			super.onClick(v);
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		if (eventType == ACTION_DISPLAY) {
			gotoDisplayProgramView();
		} else if (eventType == ACTION_PRODUCT_LIST) {
			gotoProductListView();
		} else if (eventType == ACTION_PROMOTION) {
			gotoPromotionProgramView();
		} else if (eventType == MENU_SEARCH_IMAGE) {
			handleSwitchFragment(null, ActionEventConstant.GO_TO_SEARCH_IMAGE, SupervisorController.getInstance());
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		checkLoadMore = true;
		refresh();
		isGetType = false;
		getDocumentList(officeDocument.programeCode,
				officeDocument.fromDateForRequest,
				officeDocument.toDateForRequest);

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		// TODO Auto-generated method stub
		if (action == ActionEventConstant.GO_TO_DOCUMENT_DETAIL) {
			currentDocument = (OfficeDocumentDTO) data;
			viewDocument(currentDocument);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> paramAdapterView, View paramView,
			int paramInt, long paramLong) {
		checkLoadMore = false;
		if (currentTypeIndex != paramInt && !isFirstTime && !isRefresh) {
			currentTypeIndex = paramInt;
			refresh();
			this.onClick(btDocumentSearch);
		} else {
			isRefresh = false;
			currentTypeIndex = 0;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> paramAdapterView) {
		// TODO Auto-generated method stub

	}

	/**
	 *
	 * reset cac gia tri ve mac dinh khi nhan refresh
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	public void resetAllValue() {
		isRefresh = true;
		this.edFromDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		this.edFromDate.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		this.edToDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		this.edToDate.setPadding(GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
				GlobalUtil.dip2Pixel(5));
		String dateTimePattern = StringUtil
				.getString(R.string.TEXT_DATE_TIME_PATTERN);
		Pattern pattern = Pattern.compile(dateTimePattern);
		if (!StringUtil.isNullOrEmpty(this.edFromDate.getText().toString())) {
			String strTN = this.edFromDate.getText().toString().trim();
			officeDocument.fromDate = edFromDate.getText().toString();
			Matcher matcher = pattern.matcher(strTN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date tn = StringUtil.stringToDate(strTN,
							StringUtil.EMPTY_STRING);
					String strFindTN = StringUtil.dateToString(tn,
							DateUtils.DATE_STRING_YYYY_MM_DD);

					officeDocument.fromDateForRequest = strFindTN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			officeDocument.fromDate = StringUtil.EMPTY_STRING;
			officeDocument.fromDateForRequest = StringUtil.EMPTY_STRING;
		}
		if (!StringUtil.isNullOrEmpty(this.edToDate.getText().toString())) {
			String strDN = this.edToDate.getText().toString().trim();
			officeDocument.toDate = edToDate.getText().toString();
			Matcher matcher = pattern.matcher(strDN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date dn = StringUtil.stringToDate(strDN,
							StringUtil.EMPTY_STRING);
					String strFindDN = StringUtil.dateToString(dn,
							DateUtils.DATE_STRING_YYYY_MM_DD);

					officeDocument.toDateForRequest = strFindDN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			officeDocument.toDate = StringUtil.EMPTY_STRING;
			officeDocument.toDateForRequest = StringUtil.EMPTY_STRING;
		}
		// combobox chuong trinh
		spinnerDocumentType.setSelection(0);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				checkLoadMore = false;
				resetAllValue();
				isGetType = false;
				getDocumentList(StringUtil.EMPTY_STRING,
						officeDocument.fromDateForRequest,
						officeDocument.toDateForRequest);
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if (v == edFromDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_FROM_CONTROL;
				parent.showDatePickerDialog(edFromDate.getText().toString(), true, this);
			}
		}

		if (v == edToDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_TO_CONTROL;
				parent.showDatePickerDialog(edToDate.getText().toString(), true, this);
			}
		}
		return false;
	}

	public void updateDate(int dayOfMonth, int monthOfYear, int year) {
		// TODO Auto-generated method stub
		String sDay = String.valueOf(dayOfMonth);
		String sMonth = String.valueOf(monthOfYear + 1);
		if (dayOfMonth < 10) {
			sDay = "0" + sDay;
		}
		if (monthOfYear + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		if (currentCalender == DATE_FROM_CONTROL) {

			edFromDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
			edFromDate.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(sDay).append("/").append(sMonth).append("/")
					.append(year).append(" "));

		}
		if (currentCalender == DATE_TO_CONTROL) {

			edToDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
			edToDate.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(sDay).append("/").append(sMonth).append("/")
					.append(year).append(" "));
		}
	}

	/**
	 *
	 * hien thi hinh anh cong van
	 *
	 * @author: YenNTH
	 * @param imageDocument
	 * @return: void
	 * @throws:
	 */
	private void viewDocument(OfficeDocumentDTO imageDocument) {
		String urlImage;
		urlImage = StringUtil.md5(imageDocument.url)
				+ StringUtil.getString(R.string.TEXT_FORMAT_IMAGE);
		// neu ton tai cache truoc do roi
		if (GlobalUtil.isFileExistsInDirectory(
				ExternalStorage.getDocumentFolder(), urlImage)) {
			// xem offline, file ton tai duoi may tinh bang
			openMeida(ExternalStorage.getDocumentFolder() + "/" + urlImage);
		} else if (!GlobalUtil.checkNetworkAccess()) {
			// truong hop khong co mang
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_NETWORK_DISABLE_PRODUCT_INFO));
		} else {
			// download moi ve
			new DownloadTask().execute(ServerPath.CV_IMAGE_PATH
					+ imageDocument.url);
		}
	}

	/**
	 * Open gallery de xem anh full
	 *
	 * @author: BangHN
	 * @param path
	 * @return: void
	 * @throws:
	 */
	private void openMeida(String path) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
		if (this != null && !this.isFinished && this.getActivity() != null) {
			startActivity(intent, StringUtil.getString(R.string.TEXT_VIEW_DOCUMENT));
		}
	}

	private String getDataSource(String path) throws IOException {
		URL url = new URL(path);
		URLConnection cn = url.openConnection();
		cn.connect();
		InputStream stream = cn.getInputStream();
		if (stream == null) {
			throw new RuntimeException("stream is null");
		}
		File temp = new File(ExternalStorage.getDocumentFolder(),
				StringUtil.md5(currentDocument.url)
						+ StringUtil.getString(R.string.TEXT_FORMAT_IMAGE));
		temp.deleteOnExit();
		String tempPath = temp.getAbsolutePath();
		FileOutputStream out = new FileOutputStream(temp);
		byte buf[] = new byte[128];
		try {
			do {
				int numread = stream.read(buf);
				if (numread <= 0)
					break;
				out.write(buf, 0, numread);
			} while (true);
		} catch (IOException ex) {
			MyLog.e(this.getTAG(), "error: " + ex.getMessage(), ex);
			throw ex;
		} finally {
			out.close();
			stream.close();
		}
		return tempPath;
	}

	/**
	 * Background task to download and unpack .zip file in background.
	 */
	private class DownloadTask extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}

		@Override
		protected String doInBackground(String... params) {
			String url = (String) params[0];
			try {
				String path = getDataSource(url);
				publishProgress(path);
				return path;

			} catch (Exception e) {
				return null;
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			if (values != null) {
				String sdCardPath = values[0];
				parent.closeProgressDialog();
				openMeida(sdCardPath);
			} else {
				parent.showDialog(StringUtil
						.getString(R.string.ERROR_DOWNLOAD_DOCUMENT));
			}
		}

		@Override
		protected void onPostExecute(String result) {
			parent.closeProgressDialog();
			if (result == null) {
				parent.showDialog(StringUtil
						.getString(R.string.ERROR_DOWNLOAD_DOCUMENT));
			}
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		addMenuItem(PriHashMap.PriForm.GSNPP_DANHMUC_CONGVAN,
				new MenuTab(
				R.string.TEXT_OFFICE_DOCUMENT, R.drawable.icon_document,
				ACTION_DOCUMENT, PriHashMap.PriForm.GSNPP_DANHMUC_CONGVAN),
				new MenuTab(R.string.TEXT_SEARCH_IMAGE, R.drawable.icon_image_search,
						MENU_SEARCH_IMAGE, PriHashMap.PriForm.GSNPP_TIMKIEMHINHANH),
				new MenuTab(
				R.string.TEXT_HEADER_MENU_CTTB, R.drawable.menu_manage_icon,
				ACTION_DISPLAY, PriHashMap.PriForm.GSNPP_CTTB),
				new MenuTab(
				R.string.TEXT_CTKM, R.drawable.menu_promotion_icon,
				ACTION_PROMOTION, PriHashMap.PriForm.GSNPP_CTKM),
				new MenuTab(
				R.string.TEXT_PRODUCT, R.drawable.icon_product_list,
				ACTION_PRODUCT_LIST, PriHashMap.PriForm.GSNPP_DSSANPHAM));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}
}
