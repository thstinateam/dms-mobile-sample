/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.model.TBHVModel;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;

/**
 * TBHVController -- Controller cho TBHV
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVController extends AbstractController {

	static volatile TBHVController instance;

	protected TBHVController() {
	}

	public static TBHVController getInstance() {
		if (instance == null) {
			instance = new TBHVController();
		}
		return instance;
	}

	/**
	 * Xu ly cac request tu view
	 *
	 * @author: TruongHN
	 * @param e
	 * @return: void
	 * @throws:
	 */
	@Override
	public void handleViewEvent(final ActionEvent e) {
		if (e.isUsingAsyntask) {
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {
					//cap nhat thoi gian chay cua event
					e.startTimeFromBootActive = SystemClock.elapsedRealtime();
					TBHVModel.getInstance().handleControllerEvent(
							TBHVController.this, e);
					GlobalBaseActivity base = null;
					if (e.sender instanceof Activity) {
						base = (GlobalBaseActivity) e.sender;
					} else if (e.sender instanceof Fragment) {
						base = (GlobalBaseActivity) ((Fragment) e.sender).getActivity();
					}
					if (e.request != null && base != null) {
						base.addProcessingRequest(e.request, e.isBlockRequest);
					}
					return null;
				}
			};
			task.execute();
		} else {
			TBHVModel.getInstance().handleControllerEvent(this, e);
		}
	}

	/**
	 * Xu ly du lieu tra ve tu model
	 *
	 * @author: TruongHN
	 * @param modelEvent
	 * @return: void
	 * @throws:
	 */
	public void handleModelEvent(final ModelEvent modelEvent) {
		if (modelEvent.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
			final ActionEvent e = modelEvent.getActionEvent();
			HTTPRequest request = e.request;
			if (e.sender != null && (request == null || (request != null && request.isAlive()))) {
				if (e.sender instanceof GlobalBaseActivity) {
					final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
					if (sender == null || sender.isFinished)
						return;
					sender.runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.isFinished)
								return;
							sender.handleModelViewEvent(modelEvent);
						}
					});
				} else if (e.sender instanceof BaseFragment) {
					final BaseFragment sender = (BaseFragment) e.sender;
					if (sender == null || sender.getActivity() == null || sender.isFinished) {
						return;
					}
					sender.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.getActivity() == null || sender.isFinished) {
								return;
							}
							sender.handleModelViewEvent(modelEvent);
						}
					});
				}
			} else {
				handleErrorModelEvent(modelEvent);
			}
		} else {
			handleErrorModelEvent(modelEvent);
		}
	}

	@Override
	public void handleSwitchActivity(ActionEvent e) {
	}

	@Override
	public boolean handleSwitchFragment(ActionEvent e) {
		Activity base = null;
		if (e.sender instanceof Activity) {
			base = (Activity) e.sender;
		} else if (e.sender instanceof Fragment) {
			base = ((Fragment) e.sender).getActivity();
		}

		boolean resultSwitch = false;
		if (base != null && !base.isFinishing()) {
			//an ban phim truoc khi chuyen man hinh
			GlobalUtil.forceHideKeyboard(base);

			//chuyen man hinh
			boolean isRemoveAllBackStack = false;
			BaseFragment frag = null;
			Bundle data = (e.viewData instanceof Bundle) ? (Bundle)e.viewData : null;
			//level trong truong hop view quan ly
			int managerLevelCurrent = 1;
			if (data != null) {
				//put addition info switch action
				data.putInt(IntentConstants.INTENT_SWITCH_ACTION, e.action);

				managerLevelCurrent = data.getInt(IntentConstants.INTENT_MANAGER_LEVEL, 1);
			}
			boolean isRemoveAllForManagerView = (managerLevelCurrent == 1);

			switch (e.action) {
//			case ActionEventConstant.GET_LIST_IMAGE_OF_TBHV: {
//				isRemoveAllBackStack = true;
////				frag = TBHVListImageView.getInstance(data);
//				frag = SupervisorImageListView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_SEARCH_IMAGE_TBHV: {
//				isRemoveAllBackStack = true;
////				frag = ImageSearchTBHVView.getInstance(data);
//				frag = ImageSearchGSNPPView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_TBHV_REPORT_DISPLAY_DETAIL_CUSTOMER: {
//				isRemoveAllBackStack = false;
//				frag = TBHVReportDisplayProgressDetailCustomerView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_GSNPP_IMAGE_LIST: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorImageListView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_TBHV_REPORT_PROGRESS_DATE: {
//				isRemoveAllBackStack = isRemoveAllForManagerView;
////				frag = TBHVReportProgressDateView.getInstance(data);
//				frag = ReportProgressDateView.getInstance(data);
//				frag.setManagerFragmentMode();
//				break;
//			}
////			case ActionEventConstant.GO_TO_TBHV_REPORT_PROGRESS_DATE_DETAIL: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportProgressDateDetailView.getInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_TBHV_REPORT_CUSTOMER_NOT_PSDS: {
//				isRemoveAllBackStack = true;
////				frag = TBHVReportCustomerNotPSDSView.getInstance(data);
//				frag = CustomerNotPSDSInMonthView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_GET_TBHV_TOTAL_CAT_AMOUNT_REPORT: {
//				isRemoveAllBackStack = true;
////				frag = TBHVTotalReportCatAmountView.getInstance(data);
//				frag = SupervisorReportStaffView.getInstance(data);
//				break;
//			}
////			case ActionEventConstant.GO_TO_TBHV_REPORT_CUSTOMER_NOT_PSDS_DETAIL: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportCustomerNotPSDSDetailView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_DISPLAY_PROGRAM: {
////				isRemoveAllBackStack = true;
////				frag = TBHVDisplayProgramView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_PROMOTION_PROGRAM: {
////				isRemoveAllBackStack = true;
////				frag = TBHVPromotionProgramView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_ROUTE_SUPERVISION: {
////				isRemoveAllBackStack = true;
//////				frag = TBHVRouteSupervisionView.newInstance(data);
////				frag = GsnppRouteSupervisionView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_ROUTE_SUPERVISION_MAP: {
////				isRemoveAllBackStack = false;
////				frag = TBHVRouteSupervisionMapView.newInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_TBHV_PRODUCT_LIST: {
//				isRemoveAllBackStack = true;
////				frag = TBHVProductListView.getInstance(data);
//				frag = SupervisorProductListView.newInstance(data);
//				break;
//			}
////			case ActionEventConstant.GOTO_PRODUCT_INTRODUCTION: {
////				isRemoveAllBackStack = false;
////				frag = TBHVIntroduceProductView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_REPORT_CAT_AMOUNT: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportCatAmountView.getInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_TBHV_FOLLOW_LIST_PROBLEM: {
////				isRemoveAllBackStack = true;
////				frag = TBHVFollowProblemView.newInstance(data);
//				isRemoveAllBackStack = true;
//				frag = FollowProblemView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GET_TBHV_DIS_PRO_COM_PROG_REPORT: {
//				isRemoveAllBackStack = true;
////				frag = TBHVDisProComProgReportView.getInstance(data);
//				frag = DisProComProgReportView.getInstance(data);
//				break;
//			}
////			case ActionEventConstant.GET_TBHV_NPP_DIS_PRO_COM_PROG_REPORT: {
////				isRemoveAllBackStack = false;
////				frag = TBHVDisProComProgReportNPPView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_TRAINING_PLAN: {
////				isRemoveAllBackStack = true;
////				frag = TBHVTrainingPlanView.newInstance(data);
////				break;
////			}
//			case ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_MONTH: {
//				isRemoveAllBackStack = isRemoveAllForManagerView;
////				frag = TBHVReportProgressMonthView.getInstance(data);
//				frag = AccSaleProgReportView.getInstance(data);
//				frag.setManagerFragmentMode();
//				break;
//			}
//			case ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_SALES_FORCUS: {
//				isRemoveAllBackStack = true;
////				frag = TBHVReportProgressSalesFocusView.getInstance(data);
//				frag = ProgressReportSalesFocusView.newInstance(data);
//				break;
//			}
////			case ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_MONTH_DETAIL_NPP: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportProgressMonthNPPDetailView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GOTO_TBHV_REPORT_PROGRESS_SALES_FORCUS_DETAIL: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportProgressSalesFocusDetailView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_PLAN_TRAINING_HISTORY_ACC: {
////				isRemoveAllBackStack = true;
////				frag = TBHVTrainingPlanHistoryAccView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_TRAINING_PLAN_DAY_RESULT_REPORT: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTrainingPlanDayResultReportView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.GOTO_TBHV_MANAGER_EQUIPMENT: {
////				isRemoveAllBackStack = true;
////				frag = TBHVManagerEquipmentView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_ATTENDANCE: {
////				isRemoveAllBackStack = isRemoveAllForManagerView;
////				frag = TBHVAttendanceView.getInstance(data);
////				frag.setManagerFragmentMode();
////				break;
////			}
//			case ActionEventConstant.GSNPP_GET_LIST_SALE_FOR_ATTENDANCE: {
//				isRemoveAllBackStack = isRemoveAllForManagerView;
//				frag = SuperVisorTakeAttendanceView.getInstance(data);
//				frag.setManagerFragmentMode();
//				break;
//			}
////			case ActionEventConstant.TBHV_SHOW_REPORT_VISIT_CUSTOMER_DETAIL_NPP: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportVisitCustomerDayOfDetailNPP.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_MANAGER_EQUIPMENT_DETAIL: {
////				isRemoveAllBackStack = false;
////				frag = TBHVManagerEquipmentDetailView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_TRACKING_CABINET_STAFF: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTrackingCabinetStaffView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GET_TBHV_PROG_REPOST_PRO_DISP_DETAIL_SALE: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReportDisplayProgressDetailDayView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TBHV_REVIEWS_INFO_VIEW: {
////				isRemoveAllBackStack = false;
////				frag = TBHVReviewsGSNPPView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.ACTION_TBHV_GO_TO_ADD_REQUIREMENT: {
////				isRemoveAllBackStack = false;
////				frag = TBHVAddRequirementView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_SUPERVISE_GSNPP_POSITION: {
////				isRemoveAllBackStack = true;
////				frag = TBHVGsnppPositionView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_VISIT_CUS_NOTIFICATION: {
////				isRemoveAllBackStack = isRemoveAllForManagerView;
////				frag = TBHVVisitCustomerNotificationView.getInstance(data);
////				frag.setManagerFragmentMode();
////				break;
////			}
//			case ActionEventConstant.GO_TO_REPORT_VISIT_CUSTOMER_ON_PLAN: {
//				isRemoveAllBackStack = isRemoveAllForManagerView;
//				frag = ReportVisitCustomerOnPlanOfDayView.getInstance(data);
//				frag.setManagerFragmentMode();
//				break;
//			}
////			case ActionEventConstant.GO_TO_TBHV_ATTENDANCE_DETAIL: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTakeAttendanceView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_DOCUMENT: {
////				isRemoveAllBackStack = true;
////				frag = TBHVDocumentView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_GSNPP_TRAINING_PLAN: {
////				isRemoveAllBackStack = true;
////				frag = TBHVTrainingPlanWithStaffView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.TBHV_TRAINING_RESULT_ACC_REPORT:{
////				isRemoveAllBackStack = true;
////				frag = TBHVTrainingHistoryView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_TRAINING_CUSTOMER_LIST_VIEW: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTrainingCustomerListView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTrainingNeedToDoView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_LIST_CRITERIA: {
////				isRemoveAllBackStack = false;
////				frag = TBHVTrainingListStandardView.getInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_GSNPP_TRAINING_HISTORY:{
////				isRemoveAllBackStack = true;
////				frag = TBHVTrainingPlanView.newInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_KPI:
//				isRemoveAllBackStack = true;
//				frag = ListDynamicKPIView.newInstance(data);
//				break;
//			case ActionEventConstant.ACTION_GET_TBHV_TOTAL_CAT_QUANTITY_REPORT:{
//				isRemoveAllBackStack = true;
//				frag = SupervisorReportCatQuantityView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.TBHV_GO_TO_CUSTOMER_SALE_LIST: {
//				isRemoveAllBackStack = true;
//				frag = CustomerSaleList.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.NOTE_LIST_VIEW: {
//				isRemoveAllBackStack = true;
//				frag = NoteListView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.SUPERVISE_STAFF_POSITION: {
//				isRemoveAllBackStack = true;
//				frag = StaffPositionRoutedView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_KEY_SHOP_LIST_VIEW: {
//				isRemoveAllBackStack = true;
//				frag = KeyShopListView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_KEY_SHOP: {
//				isRemoveAllBackStack = true;
//				frag = ReportKeyShopView.getInstance(data);
//				break;
//			}case ActionEventConstant.GO_TO_REPORT_KPI_SUPVERVISOR: {
//				isRemoveAllBackStack = true;
//				frag = ReportKPISupervisorView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_ASO_SUPVERVISOR: {
//				isRemoveAllBackStack = true;
//				frag = SupReportASOView.getInstance(data);
//				break;
//			}
			}


			if (frag != null) {
				resultSwitch  = switchFragment(base, frag, isRemoveAllBackStack);
			}
		}

		return resultSwitch;
	}

}
