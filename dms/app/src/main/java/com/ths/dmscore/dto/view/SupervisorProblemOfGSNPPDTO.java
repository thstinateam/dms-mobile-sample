/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.FEED_BACK_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * problem info of gsnpp of module gsnpp
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class SupervisorProblemOfGSNPPDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int OBJECT_TYPE_NOMAL = 0;
	public static final int OBJECT_TYPE_OUT_REMIND_NOT_DONE = 1;
	public static final int OBJECT_TYPE_DONE_TO_DAY = 2;
	public static final int OBJECT_TYPE_DONE_OUT_TO_DAY = 3;
	
	
	// problem status
	public ApParamDTO problemStatus;
	// problem id
	public long feedbackId;
	// problem type
	public String problemType;
	// problem content
	public String problemContent;
	// customer info
	public CustomerDTO customerInfo;
	// remain dayInOrder for gsnpp that tbhv set
	public String remindDate;
	// create dayInOrder
	public String createDate;
	// done dayInOrder
	public String doneDate;
	// status
	public int status;
	// row status : check object status ( 1: red, 2: blue, 3: gray)
	public int rowStatus;

	public SupervisorProblemOfGSNPPDTO() {
		problemStatus = new ApParamDTO();
		feedbackId = 0;
		problemType = Constants.STR_BLANK;
		problemContent = Constants.STR_BLANK;
		customerInfo = new CustomerDTO();
		remindDate = Constants.STR_BLANK;
		rowStatus = 0;
	}

	/**
	 * 
	 * init datea with cursor
	 * 
	 * @param c
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public void initDataWithCursor(Cursor c) {
		// feedback id
		feedbackId = CursorUtil.getLong(c, FEED_BACK_TABLE.FEEDBACK_ID);
		// create dayInOrder
		createDate = CursorUtil.getString(c, FEED_BACK_TABLE.CREATE_DATE);
		// done dayInOrder
//		doneDate = CursorUtil.getString(c, FEED_BACK_TABLE.DONE_DATE);
		// remind dayInOrder
		remindDate = CursorUtil.getString(c, FEED_BACK_TABLE.REMIND_DATE);
		// problem content
		problemContent = CursorUtil.getString(c, FEED_BACK_TABLE.CONTENT);
		// problem status
		status = CursorUtil.getInt(c, FEED_BACK_TABLE.STATUS);
		// customer id
		this.customerInfo.customerId = CursorUtil.getLong(c, CUSTOMER_TABLE.CUSTOMER_ID);
		// customer code
		customerInfo.customerCode = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE);
		// customer name
		customerInfo.customerName = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
		// customer street
		customerInfo.setStreet(CursorUtil.getString(c, CUSTOMER_TABLE.STREET));
		// customer housenumber
		customerInfo.setHouseNumber(CursorUtil.getString(c, CUSTOMER_TABLE.HOUSENUMBER));
		// problem type
		problemType = CursorUtil.getString(c, "TYPE");
		
		if (!StringUtil.isNullOrEmpty(doneDate)) {			
			if (doneDate.equals(DateUtils.getCurrentDate())) {
				rowStatus = SupervisorProblemOfGSNPPDTO.OBJECT_TYPE_DONE_TO_DAY;				
			} else {
				rowStatus = SupervisorProblemOfGSNPPDTO.OBJECT_TYPE_DONE_OUT_TO_DAY;
			}
		} else if (!StringUtil.isNullOrEmpty(remindDate)
				&& DateUtils.compareWithNow(remindDate, "dd/MM/yyyy") == -1) {
			rowStatus = SupervisorProblemOfGSNPPDTO.OBJECT_TYPE_OUT_REMIND_NOT_DONE;
		}
		else {
			rowStatus = 0;
		}
	}

}
