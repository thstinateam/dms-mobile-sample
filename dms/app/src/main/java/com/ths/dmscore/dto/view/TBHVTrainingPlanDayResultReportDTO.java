package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * TBHVDayTrainingSupervisionDTO
 * 
 * @author : TamPQ
 * @version: 1.1
 * @since : 1.0
 */
public class TBHVTrainingPlanDayResultReportDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public double amountMonth;
	public double amount;
	public int isNew;
	public int isOn;
	public int isOr;
	public double score;
	public double distance;
	public ArrayList<TBHVTrainingPlanDayResultReportItem> listResult = new ArrayList<TBHVTrainingPlanDayResultReportDTO.TBHVTrainingPlanDayResultReportItem>();

	public class TBHVTrainingPlanDayResultReportItem implements Serializable {
		private static final long serialVersionUID = 1L;
		public int stt;
		public String custCode;
		public int custId;
		public String custName;
		public String custAddr;
		public double amountMonth;
		public double amount;
		public double score;
		public int isNew;
		public int isOn;
		public int isOr;
		public double lat;
		public double lng;
		
		public TBHVTrainingPlanDayResultReportItem(){
			
		}

		public void initFromCursor(Cursor c, TBHVTrainingPlanDayResultReportDTO dto, int stt2) {
			stt = stt2;
			custCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			custId = CursorUtil.getInt(c, "CUSTOMER_ID");
			custName = CursorUtil.getString(c, "CUSTOMER_NAME");
			custAddr = CursorUtil.getString(c, "ADDRESS");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			isNew = CursorUtil.getInt(c, "IS_NEW");
			isOn = CursorUtil.getInt(c, "IS_ON");
			isOr = CursorUtil.getInt(c, "IS_OR");
			score = CursorUtil.getDouble(c, "SCORE");
			lat = CursorUtil.getDouble(c, "LAT");
			lng = CursorUtil.getDouble(c, "LNG");
			dto.listResult.add(this);
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.isNew += isNew;
			dto.isOr += isOr;
			dto.isOn += isOn;
//			dto.score += score;
			
		}
		

	}
	
	public TBHVTrainingPlanDayResultReportDTO(){
		listResult = new ArrayList<TBHVTrainingPlanDayResultReportItem>();
	}
	
	public TBHVTrainingPlanDayResultReportItem newStaffTrainResultReportItem() {
		return new TBHVTrainingPlanDayResultReportItem();
	}

}
