/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * FEEDBACK_STAFF_PRODUCT.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  09:56:29 21-05-2015
 */
public class FEEDBACK_STAFF_PRODUCT_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String FEEDBACK_STAFF_PRODUCT_ID = "FEEDBACK_STAFF_PRODUCT_ID";
	// FEEDBACK_STAFF_ID
	public static final String FEEDBACK_STAFF_ID = "FEEDBACK_STAFF_ID";
	// STAFF_ID
	public static final String STAFF_ID = "STAFF_ID";
	// RESULT
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// parent staff id
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String FEEDBACK_STAFF_PRODUCT_TABLE = "FEEDBACK_STAFF_PRODUCT";

	public FEEDBACK_STAFF_PRODUCT_TABLE(SQLiteDatabase mDB) {
		this.tableName = FEEDBACK_STAFF_PRODUCT_TABLE;
		this.columns = new String[] { FEEDBACK_STAFF_PRODUCT_ID, FEEDBACK_STAFF_ID,
				STAFF_ID, PRODUCT_ID, CREATE_USER_ID, 
				CREATE_DATE, UPDATE_USER, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}
	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

}
