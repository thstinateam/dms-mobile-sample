package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.DebitDetailDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;


/**
 * DTO dinh nghia cong no khach hang
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class CustomerDebtDTO implements Serializable{
	private static final long serialVersionUID = -4500578998944065405L;
	//cong no khach hang
	public DebitDetailDTO debitDetail;
	//chi tiet khach hang
	public CustomerDTO customer;
	//so ngay cong no khach hang
	public int remainDay = 0;
	public CustomerDebtDTO(){
		customer = new CustomerDTO();
		debitDetail = new DebitDetailDTO();
	}

	/**
	* Parse du lieu sau khi query tu DB
	* @author: BangHN
	* @param cursor
	* @return
	* @return: CustomerDebtDTO
	* @throws: Ngoai le do ham dua ra (neu co)
	*/
	
	public CustomerDebtDTO initCustomerDebtFromCursor(Cursor c) {
		if(c != null){
			this.customer.setShortCode(CursorUtil.getString(c, "SHORT_CODE"));
			this.customer.customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
			this.customer.customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			this.customer.customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
			this.customer.address = CursorUtil.getString(c, "ADDRESS");
			//street = housenumber + street
			if (StringUtil.isNullOrEmpty(this.customer.address)) {
				this.customer.address = CursorUtil.getString(c, "STREET_HOUSE");
			}
			this.debitDetail.total = CursorUtil.getLong(c, "TOTAL");
			this.debitDetail.amount = CursorUtil.getLong(c, "AMOUNT");
			this.debitDetail.totalPay = CursorUtil.getLong(c, "TOTAL_PAY");
			this.debitDetail.remain = CursorUtil.getLong(c, "REMAIN");
			this.debitDetail.repay = CursorUtil.getLong(c, "REPAY");
			this.debitDetail.debitID = CursorUtil.getLong(c, "DEBIT_ID");
			this.debitDetail.debitDetailID = CursorUtil.getLong(c, "DEBIT_DETAIL_TEMP_ID");
			this.remainDay = CursorUtil.getInt(c, "REMAIN_DAY");
			this.debitDetail.refuse = CursorUtil.getInt(c, "NUMREFUSE");
		}
		return null;
	}
	
}
