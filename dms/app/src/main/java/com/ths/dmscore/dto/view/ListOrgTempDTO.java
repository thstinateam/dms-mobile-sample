/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.OrgTempDTO;

/**
 * danh sach cac data tam cho bang org
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  4:24:35 PM Mar 18, 2015
 */
public class ListOrgTempDTO implements Serializable {

	private static final long serialVersionUID = 3836238879780199393L;
	public ArrayList<OrgTempDTO> listOrgTemp;
	
	public ListOrgTempDTO() {
		// TODO Auto-generated constructor stub
		listOrgTemp = new ArrayList<OrgTempDTO>();
	}
	
	public void addItem(OrgTempDTO dto){
		listOrgTemp.add(dto);
	}
	
	public int getSize(){
		return listOrgTemp.size();
	}

}
