/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dms.R;

/**
 * hien thi thong tin mot chuong trinh di kem voi san pham
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
@SuppressLint("ViewConstructor")
public class SelectCustomerForProductVideo extends DMSTableRow implements
		OnClickListener {

	TextView tvCustomerName;
	TextView tvCustomerAddress;
	ImageView ivCustomerVideo;
	// data to render layout for row
	CustomerListItem myData;

	/**
	 * constructor for class
	 *
	 * @param context
	 * @param aRow
	 */
	public SelectCustomerForProductVideo(Context context) {
		super(context, R.layout.layout_select_customer_for_product_video_row);
		setOnClickListener(this);
		tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
		tvCustomerAddress = (TextView) findViewById(R.id.tvCustomerAddress);
		ivCustomerVideo = (ImageView) findViewById(R.id.ivCustomerVideo);
		ivCustomerVideo.setOnClickListener(this);
		myData = new CustomerListItem();
	}

	/**
	 *
	 * init layout for row
	 *
	 * @author: HaiTC3
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(int position, CustomerListItem item) {
		this.myData = item;
		tvCustomerName.setText(item.aCustomer.getCustomerCode() + " - " + item.aCustomer.getCustomerName());
		tvCustomerAddress.setText(item.aCustomer.getStreet());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == ivCustomerVideo && listener != null) {
			listener.handleVinamilkTableRowEvent(
					ActionEventConstant.ACTION_WATCH_PRODUCT_VIDEO, ivCustomerVideo,
					this.myData);
		}
	}
}
