/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.statistic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.view.ReportStaffSaleDetailItemDTO;
import com.ths.dmscore.dto.view.ReportStaffSaleDetailViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Bao cao chi tiet tien do ngay cua NVBH
 *
 * @author: Nguyen Thanh Dung
 * @version: 1.0
 * @since: Sep, 07, 2013
 */

public class ReportStaffSaleDetailView extends BaseFragment implements
		OnClickListener, OnEventControlListener, VinamilkTableListener {

	// can thuc hien
	private final int MENU_FOLLOW_PROBLEM = 2;
	// bao cao cttb
	private final int MENU_REPORT_DISPLAY_PROGRESS = 3;
	// mat hang trong tam
	private static final int ACTION_MENU_MHTT = 4;

	// table report
	private DMSTableView tbReportList;
	// kiem tra lan dau tien vao man hinh
	private boolean isFirstTime;
	private boolean checkSendRequest = true;
	private ReportStaffSaleDetailViewDTO dto = new ReportStaffSaleDetailViewDTO();

	/**
	 *
	 * method get instance
	 *
	 * @author: DungNT19
	 * @param data
	 * @since:
	 * @param index
	 * @return
	 * @return: ReportStaffSaleDetailView
	 * @throws:
	 */
	public static ReportStaffSaleDetailView newInstance(Bundle data) {
		ReportStaffSaleDetailView instance = new ReportStaffSaleDetailView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		isFirstTime = true;
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.layout_report_staff_detail_view, null);
		View view = super.onCreateView(inflater, v, savedInstanceState);
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_THONGKECHUNG_CHITIETTIENDONGAY);
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_REPORT_STAFF_SALE_DETAIL));

		tbReportList = (DMSTableView) view.findViewById(R.id.tbReportList);
		tbReportList.setListener(this);

		// request get danh sach loai CT va danh sach nganh hang
		if (isFirstTime) {
			getReportStaffSaleDetail();
		} else {
			renderLayout();
		}
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (checkSendRequest) {
		} else {
			// layout ds sp khuyen mai
			renderLayout();
			// cap nhat thoi gian chuyen
			checkSendRequest = true;
		}
	}

	/**
	 * lay thong tin bao cao theo ngay va theo thang
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void getReportStaffSaleDetail() {
		this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		data.putString(
				IntentConstants.INTENT_SHOP_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId()));
		handleViewEvent(data, ActionEventConstant.ACTION_GET_REPORT_STAFF_SALE_DETAIL, SaleController.getInstance());
	}

	/**
	 *
	 * render layout cho table
	 *
	 * @author: ThanhNN8
	 * @param model
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		tbReportList.clearAllDataAndHeader();

		if (!tbReportList.isHeaderExists()) {
			ReportStaffSaleDetailRow header = new ReportStaffSaleDetailRow(parent, this);
			tbReportList.addHeader(header);
		}

		int pos = 1;
		ReportStaffSaleDetailItemDTO sumDTO = new ReportStaffSaleDetailItemDTO();

		for (ReportStaffSaleDetailItemDTO item : dto.listReport) {
			ReportStaffSaleDetailRow row = new ReportStaffSaleDetailRow(parent,
					this);
			row.setClickable(true);
			row.setOnClickListener(this);
			row.setTag(Integer.valueOf(pos));
			row.render(pos, item);

			pos++;
			tbReportList.addRow(row);

			sumDTO.saleDetailDTO.sumItem(item.saleDetailDTO);
			//sumDTO.saleDetailDTO.score += item.saleDetailDTO.score;
		}

		if (dto.listReport.size() > 0) {
			sumDTO.saleDetailDTO.sumProcess();
			//hien thi dong tong
			ReportStaffSaleDetailRow rowSum = new ReportStaffSaleDetailRow(parent, this);
			rowSum.renderRowSum(sumDTO);
			rowSum.setClickable(true);
			rowSum.setOnClickListener(this);
			tbReportList.addRow(rowSum);
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.ACTION_GET_REPORT_STAFF_SALE_DETAIL: {
			dto = (ReportStaffSaleDetailViewDTO) modelEvent.getModelData();
			renderLayout();
			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.ACTION_GET_REPORT_STAFF_SALE_DETAIL: {
			renderLayout();
			break;
		}

		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		ActionEvent e = new ActionEvent();
		switch (eventType) {
		case MENU_FOLLOW_PROBLEM: {
			gotoFollowProblem();
			break;
		}
		case MENU_REPORT_DISPLAY_PROGRESS: {
			e.action = ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY;
			e.sender = this;
			e.viewData = new Bundle();
			UserController.getInstance().handleSwitchFragment(e);
			break;
		}
		case ACTION_MENU_MHTT:
			e.action = ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW;
			e.viewData = new Bundle();
			e.sender = this;
			UserController.getInstance().handleSwitchFragment(e);
			break;

		default:
			break;
		}
	}

	/**
	 * Di toi man hinh theo doi van de can khac phuc
	 *
	 * @author banghn
	 * @return void
	 */
	private void gotoFollowProblem() {
		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW;
		e.viewData = new Bundle();
		e.sender = this;
		UserController.getInstance().handleSwitchFragment(e);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		}
	}

	/**
	 * di toi man hinh chi tiet Khach hang
	 *
	 * @author : TamPQ since : 10:59:39 AM
	 */
	public void gotoCustomerInfo(String customerId) {
		ActionEvent e = new ActionEvent();
		e.sender = this;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		e.viewData = bunde;
		e.action = ActionEventConstant.GO_TO_CUSTOMER_INFO;
		SaleController.getInstance().handleSwitchFragment(e);
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				getReportStaffSaleDetail();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

}
