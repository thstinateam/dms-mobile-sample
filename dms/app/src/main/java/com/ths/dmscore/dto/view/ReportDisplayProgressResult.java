package com.ths.dmscore.dto.view;

import java.io.Serializable;

public class ReportDisplayProgressResult implements Serializable {
	private static final long serialVersionUID = 891664173577484502L;
	public int resultNumber;
	public int joinNumber;
	public String levelCode;

	public ReportDisplayProgressResult() {
	}
}