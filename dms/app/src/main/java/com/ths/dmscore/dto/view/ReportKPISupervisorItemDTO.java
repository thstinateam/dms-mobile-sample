package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cho man hinh bao cao KPI cua role GS
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPISupervisorItemDTO {
	public ArrayList<ReportKPIItemDTO> lstReportKPI = new ArrayList<ReportKPIItemDTO>();
	public long staffId; // id staff
	public String staffName; // staffName
	public String staffCode; // staffCode


	 /**
	 * parse du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void parseDataFromCursor(Cursor c) {
		staffId = CursorUtil.getLong(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
	}

}
