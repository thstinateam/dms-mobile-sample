package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.lib.sqllite.db.MEDIA_ITEM_TABLE;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;

public class ImageListItemDTO implements Serializable {
	private static final long serialVersionUID = -7337910367022561324L;
	// id khach hang
	public long customerId;
	// ma khach hang
	public String customerCode;
	// ten khach hang
	public String customerName;
	// dia chi khach hang
	public String street;
	// so luong anh
	public int imageNumber;
	public CustomerDTO aCustomer = new CustomerDTO();
	public double lat;
	public double lng;
	public String houseNumber;
	public String visitPlan;
	public int nvbhStaffId;
	public String nvbhStaffName;
	public long routingId;
	public long cycleId;
	public String address;
	public PhotoDTO mediaItem= new PhotoDTO();

	public void imageListItemDTO(Cursor c) throws Exception {
		customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		customerCode = CursorUtil.getString(c,"CUSTOMER_CODE");
		customerName = CursorUtil.getString(c,"CUSTOMER_NAME");
		address = CursorUtil.getString(c,"ADDRESS");
		routingId = CursorUtil.getLong(c, "ROUTING_ID");
		street = CursorUtil.getString(c,"STREET");
		imageNumber = CursorUtil.getInt(c,"NUM_ITEM");
		lat = CursorUtil.getDouble(c,CUSTOMER_TABLE.LAT);
		lng = CursorUtil.getDouble(c,CUSTOMER_TABLE.LNG);
		houseNumber = CursorUtil.getString(c,CUSTOMER_TABLE.HOUSENUMBER);
		visitPlan = CursorUtil.getString(c,"VISIT_PLAN");
		if (!StringUtil.isNullOrEmpty(visitPlan) && visitPlan.substring(0, 1).equals(",")) {
			visitPlan = visitPlan.substring(1, visitPlan.length());
		}
		nvbhStaffId = CursorUtil.getInt(c,"STAFF_ID");
		nvbhStaffName = CursorUtil.getString(c,"NVBH_STAFF_NAME");

	}

	/**
	 *
	*  init data search image
	*  @author: YenNTH
	*  @param c
	*  @throws Exception
	*  @return: void
	*  @throws:
	 */
	public void imageSearchListItemDTO(Cursor c) throws Exception {
		customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		customerCode = CursorUtil.getString(c,"CUSTOMER_CODE");
		customerName = CursorUtil.getString(c,"CUSTOMER_NAME");
		street = CursorUtil.getString(c,"STREET");
		mediaItem.customerCode = customerCode;
		mediaItem.customerName = customerName;
		imageNumber = CursorUtil.getInt(c,"NUM_ITEM");
		lat = CursorUtil.getDouble(c,CUSTOMER_TABLE.LAT);
		lng = CursorUtil.getDouble(c,CUSTOMER_TABLE.LNG);
		houseNumber = CursorUtil.getString(c,CUSTOMER_TABLE.HOUSENUMBER);
		visitPlan = CursorUtil.getString(c,"VISIT_PLAN");
		if (!StringUtil.isNullOrEmpty(visitPlan) && visitPlan.substring(0, 1).equals(",")) {
			visitPlan = visitPlan.substring(1, visitPlan.length());
		}
		nvbhStaffId = CursorUtil.getInt(c,"STAFF_ID");
		nvbhStaffName = CursorUtil.getString(c,"NVBH_STAFF_NAME");
		mediaItem.thumbUrl = CursorUtil.getString(c, MEDIA_ITEM_TABLE.THUMB_URL);
		mediaItem.fullUrl = CursorUtil.getString(c,MEDIA_ITEM_TABLE.URL);
		mediaItem.createdTime = CursorUtil.getString(c,MEDIA_ITEM_TABLE.CREATE_DATE);
		mediaItem.lat = CursorUtil.getDouble(c,"LAT_MEDIA");
		mediaItem.lng = CursorUtil.getDouble(c,"LNG_MEDIA");
		mediaItem.staffCode = CursorUtil.getString(c,"STAFF_CODE");
		mediaItem.staffName = CursorUtil.getString(c,"STAFF_NAME");
		mediaItem.createUser = CursorUtil.getString(c,MEDIA_ITEM_TABLE.CREATE_USER);
	}

	public ImageListItemDTO() {
		customerCode = "";
		customerName = "";
		street = "";
		imageNumber = 0;
	}
	
}
