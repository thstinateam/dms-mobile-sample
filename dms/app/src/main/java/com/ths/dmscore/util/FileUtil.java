/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.io.File;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dms.R;

public class FileUtil {
	public static final int KB_IN_BYTE = 1024;
	/**
	 * get File Extension
	 * @author: duongdt3
	 * @since: 12:06:19 23 Aug 2015
	 * @return: String
	 * @throws:  
	 * @param file
	 * @return
	 */
    public static String getFileExtension(File file) {
    	String res = "";
    	if (file != null) {
    		String fileName = file.getName();
    		int lastIndexOf = fileName.lastIndexOf(".");
    		if(lastIndexOf >= 0) {
    			res = fileName.substring(lastIndexOf + 1);
    		}
		}
    	return res;
    }

    /**
     * check Validate Upload White List
     * @author: duongdt3
     * @since: 12:06:02 23 Aug 2015
     * @return: boolean
     * @throws:  
     * @param file
     * @return
     */
	public static boolean isValidateUploadAttach(File file) {
		return isValidateUploadAttach(getFileExtension(file));
	}
	
	/**
	 * check Validate Upload White List with file extension
	 * @author: duongdt3
	 * @since: 15:47:36 24 Aug 2015
	 * @return: boolean
	 * @throws:  
	 * @param fileExtension
	 * @return
	 */
	public static boolean isValidateUploadAttach(String fileExtension) {
		boolean res = false;
		String[] whiteList = GlobalInfo.getInstance().getWhiteListUploadAttach();
		if (whiteList == null || whiteList.length == 0) {
			//accept all file type
			res = true;
		} else{
			if (!StringUtil.isNullOrEmpty(fileExtension)) {
				for (String fileTypeAllow : whiteList) {
					String curType = fileExtension.toLowerCase();
					boolean isPass = curType.equals(fileTypeAllow.toLowerCase());
					if (isPass) {
						res = true;
						break;
					}
				}
			}
		}
		return res;
	}
	
	/**
	 * show view file
	 * @author: duongdt3
	 * @since: 14:06:45 24 Aug 2015
	 * @return: void
	 * @throws:  
	 * @param con
	 * @param file
	 */
	public static void viewFile(GlobalBaseActivity con, File file) {
		String actionName = StringUtil.getString(R.string.TEXT_VIEW_FILE) + " " + FileUtil.getFileExtension(file).toUpperCase();
		if (file != null && file.exists()) {
			MimeTypeMap myMime = MimeTypeMap.getSingleton();
			Intent newIntent = new Intent(Intent.ACTION_VIEW);
			String mimeType = myMime.getMimeTypeFromExtension(getFileExtension(file));
			if (mimeType == null) {
				newIntent.setData(Uri.fromFile(file));
			} else{
				newIntent.setDataAndType(Uri.fromFile(file), mimeType);
			}
			newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try {
				GlobalUtil.startActivityOtherApp(con, newIntent);
			} catch (ActivityNotFoundException e) {
				String note = StringUtil.getString(R.string.TEXT_NOT_ACTIVITY_FOUND, actionName);
				con.showDialog(note);
				ServerLogger.sendLog(actionName + " not found activity "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			} catch (Exception e) {
				String note = StringUtil.getString(R.string.TEXT_START_ACTIVITY_FAIL);
				con.showDialog(String.format(note, actionName));
				ServerLogger.sendLog(actionName + " fail "  + e.getMessage(), TabletActionLogDTO.LOG_EXCEPTION);
			}
		} else{
			String note = StringUtil.getString(R.string.TEXT_START_ACTIVITY_FAIL, actionName);
			con.showDialog(note);
			ServerLogger.sendLog(actionName + " not exists ", 
					VNMTraceUnexceptionLog.getReportFromThrowable(new Throwable()), TabletActionLogDTO.LOG_EXCEPTION);
		}
	}
}
