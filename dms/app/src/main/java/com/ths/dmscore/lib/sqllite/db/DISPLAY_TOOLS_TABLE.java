/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.DisplayToolDTO;

/**
 *  Ds cong cu trung bay
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class DISPLAY_TOOLS_TABLE extends ABSTRACT_TABLE{
	// id cua bang
	public static final String DISPLAY_TOOLS_ID = "DISPLAY_TOOLS_ID";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// id san pham
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// so luong
	public static final String QUANTITY = "QUANTITY";
	// truong nay chua dung
	public static final String RESULT = "RESULT";
	// ngay chuyen
	public static final String TRANSFER_DATE = "TRANSFER_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// id NVBH
	public static final String STAFF_ID = "STAFF_ID";

	private static final String TABLE_DISPLAY_TOOLS = "DISPLAY_TOOLS";
	
	public DISPLAY_TOOLS_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_DISPLAY_TOOLS;
		this.columns = new String[] { DISPLAY_TOOLS_ID, SHOP_ID,
				CUSTOMER_ID, PRODUCT_ID, QUANTITY, RESULT, TRANSFER_DATE,
				CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE, STAFF_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((DisplayToolDTO) dto);
		return insert(null, value);
	}

	/**
	 * 
	 * them 1 dong xuong CSDL
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(DisplayToolDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}
	
	/**
	 * Update 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		DisplayToolDTO disDTO = (DisplayToolDTO)dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.displayToolsId };
		return update(value, DISPLAY_TOOLS_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: TruongHN
	 * @param ap_param_id
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(DISPLAY_TOOLS_ID + " = ?", params);
	}
	
	public long delete(AbstractTableDTO dto) {
		DisplayToolDTO disDTO = (DisplayToolDTO)dto;
		String[] params = { "" + disDTO.displayToolsId };
		return delete(DISPLAY_TOOLS_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * 
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayToolDTO
	 * @throws:
	 */
	public DisplayToolDTO getRowById(String id) {
		DisplayToolDTO DisplayToolDTO = null;
		Cursor c = null;
		try {
			String[]params = {id};
			c = query(DISPLAY_TOOLS_ID + " = ?", params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				DisplayToolDTO = initDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return DisplayToolDTO;
	}

	private DisplayToolDTO initDTOFromCursor(Cursor c) {
		DisplayToolDTO dpDetailDTO = new DisplayToolDTO();

		dpDetailDTO.displayToolsId = (CursorUtil.getInt(c, DISPLAY_TOOLS_ID));
		dpDetailDTO.shopId = (CursorUtil.getInt(c, SHOP_ID));
		dpDetailDTO.customerId = (CursorUtil.getLong(c, CUSTOMER_ID));
		dpDetailDTO.productId = (CursorUtil.getString(c, PRODUCT_ID));
		
		dpDetailDTO.quantity = (CursorUtil.getInt(c, QUANTITY));
		dpDetailDTO.result = (CursorUtil.getInt(c, RESULT));
		dpDetailDTO.transferDate = (CursorUtil.getString(c, TRANSFER_DATE));
		dpDetailDTO.createUser = (CursorUtil.getString(c, CREATE_USER));
		
		dpDetailDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dpDetailDTO.createUser = (CursorUtil.getString(c, CREATE_DATE));
		dpDetailDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dpDetailDTO.staffId = (CursorUtil.getInt(c, STAFF_ID));
		
		return dpDetailDTO;
	}

	/**
	 * 
	 * lay tat ca cac dong cua CSDL
	 * 
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayToolDTO>
	 * @throws:
	 */
	public Vector<DisplayToolDTO> getAllRow() {
		Vector<DisplayToolDTO> v = new Vector<DisplayToolDTO>();
		Cursor c = null;
		try {
			c = query(null,null, null, null, null);
			if (c != null) {
				DisplayToolDTO DisplayToolDTO;
				if (c.moveToFirst()) {
					do {
						DisplayToolDTO = initDTOFromCursor(c);
						v.addElement(DisplayToolDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;
	}

	private ContentValues initDataRow(DisplayToolDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(DISPLAY_TOOLS_ID, dto.displayToolsId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(CUSTOMER_ID, dto.customerId);
		editedValues.put(PRODUCT_ID, dto.productId);
		
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(RESULT, dto.result);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(TRANSFER_DATE, dto.transferDate);
		
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createUser);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(STAFF_ID, dto.staffId);

		return editedValues;
	}
}
