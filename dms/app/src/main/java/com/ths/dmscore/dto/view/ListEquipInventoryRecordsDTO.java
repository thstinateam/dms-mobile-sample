/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Thong tin kiem ke cua mot khach hang
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:02:44 16-12-2014
 */
public class ListEquipInventoryRecordsDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	// Danh sach cac ky kiem ke
	public ArrayList<EquipStatisticRecordDTO> listInventoryRecord;
	// total product
	public int totalItem = 0;
	// k.c cho kiem ke
	public double shopDistance;

	public ListEquipInventoryRecordsDTO() {
		listInventoryRecord = new ArrayList<EquipStatisticRecordDTO>();
	}
}
