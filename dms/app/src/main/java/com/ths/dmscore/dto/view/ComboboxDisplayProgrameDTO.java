/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.ShopDTO;

/**
 * 
 * Mo ta cho class
 * 
 * @author: ThanhNN8
 * @version: 1.0
 * @since: 1.0
 */
public class ComboboxDisplayProgrameDTO  implements Serializable {
	private static final long serialVersionUID = 7596321589292684289L;
	//danh sach loai chuong trinh
	public ArrayList<DisplayProgrameItemDTO> listTypePrograme = null;
	//danh sach loai nganh hang
	public ArrayList<DisplayProgrameItemDTO> listDepartPrograme = null;
	//danh sach loai nganh hang con
	public ArrayList<DisplayProgrameItemDTO> listSubcat = null;
	//Danh sach don vi
	public ArrayList<ShopDTO> listShop = null;
}
