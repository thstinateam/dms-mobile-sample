/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.view.AutoCompleteFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ComboboxDisplayProgrameDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.FindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.ListFindProductOrderListDTO;
import com.ths.dmscore.dto.view.ListFindProductSaleOrderDetailViewDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.dto.view.ProgrameForProductDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 *
 * find product to add order list
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class FindProductAddOrderListView extends BaseFragment implements
		OnEventControlListener, OnClickListener, VinamilkTableListener, OnTouchListener, DMSColSortManager.OnSortChange, OnItemSelectedListener {
	public static final String VNM_ITEM_PER_PAGE = "com.viettel.vinamilk.itemPerPage";
	// back action
	public static final int BACK_ACTION = 0;
	// table vinamilk list product
	DMSTableView tbProductOrderList;
	// text view display number product result
	TextView tvNumberProduct;
	// list product result
	ListFindProductOrderListDTO listProduct;
	// button close
	Button btClose;
	// button search
	Button btSearch;
	// button choose
	Button btChoose;
	// input order code
	VNMEditTextClearable etInputOrderCode;
	// // input CTKM code
	// VNMEditTextClearable etInputCTKMCode;
	// input order Name
	VNMEditTextClearable etInputOrderName;
	// button clear all data input
	Button btClearAllInput;
	// flag when searching product
	boolean isSearchingProduct = false;
	// flag when load list product the first
	boolean isFirstLoadProduct = false;
	// button close popup programe
	Button btClosePopupPrograme;
	// button close popup and reset choose programe
	Button btResetChoosePrograme;
	// limit row in page
	public static final int LIMIT_ROW_PER_PAGE = 10;
	public static final int LIMIT_ROW_PER_PAGE2 = 20;
	public static final int LIMIT_ROW_PER_PAGE3 = 30;
	// list product
	ListFindProductSaleOrderDetailViewDTO listDTO = new ListFindProductSaleOrderDetailViewDTO();
	// list product
	ListFindProductSaleOrderDetailViewDTO orgListDTO = new ListFindProductSaleOrderDetailViewDTO();
	// list number order for each product selected
	// ArrayList<Integer> arrayNumSelected = new ArrayList<Integer>();
	// list product not in
	ArrayList<OrderDetailViewDTO> listBuyOrders = new ArrayList<OrderDetailViewDTO>();

	// current customer id
	public String currentCustomerId;
	// customer type id
	public int customerTypeId;
	// check load data
	boolean isLoadData = false;

	// dialog product detail view
	AlertDialog alertProductDetail;
	boolean isFirstShowSelectPrograme = false;
	// dialog product detail view
	AlertDialog alertPromotionDetail;
	// table list programe
	DMSTableView tbProgrameList;
	// list ProgrameForProductDTO
	List<ProgrameForProductDTO> listPrograme = new ArrayList<ProgrameForProductDTO>();
	FindProductSaleOrderDetailViewDTO currentObjectClick = new FindProductSaleOrderDetailViewDTO();

	// product code keConstants.STR_BLANKword to search
	private String productCodeKW = Constants.STR_BLANK;
	// product name key word to search
	private String productNameKW = Constants.STR_BLANK;
	// // progame code key word to search
	// private String programeCodeKW = Constants.STR_BLANK;
	// flag check click update on header menu
	private boolean isUpdate = false;
	// type staff
	private int staffType = UserDTO.TYPE_STAFF;
	private String orderType = SALE_ORDER_TABLE.ORDER_TYPE_PRESALE;
	private int currentSelectedFilterItem = -1;
	customAutoCompleteAdapter customAutoCompleteFilterProductName;
	private boolean isFilteredSelected = false;
	/**
	 * control for popup promotion detail
	 */
	//PromotionProgrameDetailView promotionDetailView;
	boolean isShowPrice = GlobalInfo.getInstance().isSysShowPrice();
	// nganh hang
	private Spinner spCat;
	// nganh hang con
	private Spinner spSubCat;
	// so item/ page
	private Spinner spPageItem;
	private int indexCat = -1;
	private int indexSubCat = -1;
	private int indexPage = -1;
	// nhung thong tin ho tro tim kiem
	AutoCompleteFindProductSaleOrderDetailViewDTO lstInfoSearch = new AutoCompleteFindProductSaleOrderDetailViewDTO();
	// so item tren page, hard code
	String[] strPageItem= new String[]{LIMIT_ROW_PER_PAGE+"",LIMIT_ROW_PER_PAGE2+"",LIMIT_ROW_PER_PAGE3+""};
	// ma nganh hang
	private String productInfoCat = "";
	// ma nganh hang con
	private String productInfoSubCat = "";
	private int pageItem = LIMIT_ROW_PER_PAGE;


	/**
	 * method get instance
	 *
	 * @author: HaiTC3
	 * @param index
	 * @return
	 * @return: FindProductAddOrderListView
	 * @throws:
	 */
	public static FindProductAddOrderListView getInstance(Bundle data) {
		FindProductAddOrderListView instance = new FindProductAddOrderListView();
		instance.setArguments(data);
		return instance;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Bundle b = getArguments();
		if (b != null) {
			if (b.getSerializable(
					IntentConstants.INTENT_LIST_PRODUCT_NOT_IN) != null) {
				listBuyOrders = (ArrayList<OrderDetailViewDTO>) b
						.getSerializable(
								IntentConstants.INTENT_LIST_PRODUCT_NOT_IN);
			}
			if (b.getString(IntentConstants.INTENT_CUSTOMER_ID) != null) {
				this.currentCustomerId = b.getString(
						IntentConstants.INTENT_CUSTOMER_ID);
			}
			if (b.getInt(IntentConstants.INTENT_STAFF_TYPE) >= 0) {
				this.staffType = b.getInt(
						IntentConstants.INTENT_STAFF_TYPE);
			}
			if (b.getString(IntentConstants.INTENT_ORDER_TYPE) != null) {
				this.orderType = b.getString(
						IntentConstants.INTENT_ORDER_TYPE);
			}
			if (b.getInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID) >= 0) {
				this.customerTypeId = b.getInt(
						IntentConstants.INTENT_CUSTOMER_TYPE_ID);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_find_product_add_orderslist_view, container,
				false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BANHANG_THEMHANG);
		initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TEXT_HEADER_TITLE_FIND_PRODUCT_ADD_ORDERLIST));
		if (!this.isLoadData) {
			// request get list product add to order list
			isFirstLoadProduct = true;
			getInitListProductAddOrder();
		} else {
			this.renderLayout();
		}

		return v;
	}

	/**
	 *
	 * get list product add order
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void getListProductAddOrder(int numPage, String productCodeKeyWord,
			String productNameKeyWord, boolean isGetCount) {
		if (!this.parent.isShowProgressDialog()) {
			this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}
		Bundle data = new Bundle();
		String page = " limit " + (numPage * pageItem) + ","
				+ pageItem;
		this.productCodeKW = productCodeKeyWord.trim();
		this.productNameKW = StringUtil
				.getEngStringFromUnicodeString(productNameKeyWord.trim());

		data.putString(IntentConstants.INTENT_PAGE, page);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, currentCustomerId);

		if (!StringUtil.isNullOrEmpty(this.productCodeKW)) {
			data.putString(IntentConstants.INTENT_PRODUCT_CODE,
					this.productCodeKW);
		}
		if (!StringUtil.isNullOrEmpty(this.productNameKW)) {
			data.putString(IntentConstants.INTENT_PRODUCT_NAME,
					this.productNameKW);
		}
		data.putString(IntentConstants.INTENT_SALE_TYPE_CODE, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritSaleTypeCode());
		data.putString(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				String.valueOf(this.customerTypeId));
		data.putInt(IntentConstants.INTENT_STAFF_TYPE, this.staffType);
		data.putString(IntentConstants.INTENT_ORDER_TYPE, this.orderType);

		data.putString(
				IntentConstants.INTENT_SHOP_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId()));
		data.putString(IntentConstants.INTENT_PRODUCT_CAT,productInfoCat );
		data.putString(IntentConstants.INTENT_PRODUCT_SUB_CAT,productInfoSubCat);

		// list product don't get
		if (orgListDTO != null && orgListDTO.listObject != null
				&& orgListDTO.listObject.size() > 0) {
			String strProductID = Constants.STR_BLANK;
			for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
				if (orgListDTO.listObject.get(i).typeObject == FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_FILTER) {
					FindProductSaleOrderDetailViewDTO product = orgListDTO.listObject
							.get(i);
					strProductID += "'" + product.productCode + "'";
					strProductID += ",";
				}
			}
			if (strProductID.length() > 1) {
				strProductID = strProductID.substring(0,
						strProductID.length() - 1);
			}
			data.putString(IntentConstants.INTENT_LIST_PRODUCT_NOT_IN,
					strProductID);
		}

		data.putBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM,
				isGetCount);
		//add sort info
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbProductOrderList.getSortInfo());
		handleViewEvent(data, ActionEventConstant.GET_LIST_PRODUCT_ADD_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 *
	 * khoi tao 3 textview autocomplete
	 *
	 * @author: HieuNH6
	 * @param numPage
	 * @param productCodeKeyWord
	 * @param productNameKeyWord
	 * @param programeCodeKeyWord
	 * @param isGetCount
	 * @return: void
	 * @throws:
	 */
	private void getInitListProductAddOrder() {
		this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		String page = Constants.STR_BLANK;
		this.productCodeKW = Constants.STR_BLANK;
		this.productNameKW = Constants.STR_BLANK;
		// this.programeCodeKW = Constants.STR_BLANK;

		data.putString(IntentConstants.INTENT_PAGE, page);
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, currentCustomerId);

		if (!StringUtil.isNullOrEmpty(this.productCodeKW)) {
			data.putString(IntentConstants.INTENT_PRODUCT_CODE,
					this.productCodeKW);
		}
		if (!StringUtil.isNullOrEmpty(this.productNameKW)) {
			data.putString(IntentConstants.INTENT_PRODUCT_NAME,
					this.productNameKW);
		}

		data.putString(
				IntentConstants.INTENT_SHOP_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritShopId()));

		// list product don't get
		if (orgListDTO != null && orgListDTO.listObject != null
				&& orgListDTO.listObject.size() > 0) {
			String strProductID = Constants.STR_BLANK;
			for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
				if (orgListDTO.listObject.get(i).typeObject == FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_FILTER) {
					FindProductSaleOrderDetailViewDTO product = orgListDTO.listObject
							.get(i);
					strProductID += "'" + product.productCode + "'";
					strProductID += ",";
				}
			}
			if (strProductID.length() > 1) {
				strProductID = strProductID.substring(0,
						strProductID.length() - 1);
			}
			data.putString(IntentConstants.INTENT_LIST_PRODUCT_NOT_IN,
					strProductID);
		}
		data.putString(IntentConstants.INTENT_CUSTOMER_ID, currentCustomerId);
		data.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID, customerTypeId);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_ORDER_TYPE, this.orderType);
		data.putBoolean(IntentConstants.INTENT_IS_GET_NUM_TOTAL_ITEM, false);
		handleViewEvent(data, ActionEventConstant.GET_INIT_LIST_PRODUCT_ADD_TO_ORDER_VIEW, SaleController.getInstance());
	}

	/**
	 * Init view for screen
	 *
	 * @author: HaiTC3
	 * @param v
	 * @return: void
	 * @throws:
	 */
	public void initView(View v) {
		tbProductOrderList = (DMSTableView) v
				.findViewById(R.id.tbProductOrderList);
		tbProductOrderList.setListener(this);
		tvNumberProduct = (TextView) v.findViewById(R.id.tvNumberProduct);
		btClose = (Button) PriUtils.getInstance().findViewById(v, R.id.btClose, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHONMATHANG_HUY);
		PriUtils.getInstance().setOnClickListener(btClose, this);
		btSearch = (Button) PriUtils.getInstance().findViewById(v, R.id.btSearch, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHONMATHANG_TIM);
		PriUtils.getInstance().setOnClickListener(btSearch, this);
		btClearAllInput = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btClearAllInput, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHONMATHANG_NHAPLAI);
		PriUtils.getInstance().setOnClickListener(btClearAllInput, this);
		btChoose = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btChoose, PriHashMap.PriControl.NVBH_BANHANG_DONHANG_CHONMATHANG_CHAPNHAN);
		PriUtils.getInstance().setOnClickListener(btChoose, this);
		etInputOrderCode = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.etInputOrderCode, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_MAHANG);
		PriUtils.getInstance().setOnItemClickListener(etInputOrderCode, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				searchProductFlowFillter();
			}
		});
//		etInputOrderCode.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//				searchProductFlowFillter();
//			}
//		});
		etInputOrderName = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.etInputOrderName, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_TENHANGHANG);
		PriUtils.getInstance().setOnItemClickListener(etInputOrderName, new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				currentSelectedFilterItem = arg2;
				searchProductFlowFillter();
				FindProductAddOrderListView.this.isFilteredSelected = true;
				etInputOrderName.clearFocus();
			}
		});
//		etInputOrderName.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//				currentSelectedFilterItem = arg2;
//				searchProductFlowFillter();
//				FindProductAddOrderListView.this.isFilteredSelected = true;
//				etInputOrderName.clearFocus();
//			}
//		});

		//an ban phim custom khi muon tim kiem theo ten, ma san pham
		if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
			OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
						parent.hideKeyboardCustom();
					}
				}
			};
			etInputOrderCode.setOnFocusChangeListener(onFocusChangeListener);
			etInputOrderName.setOnFocusChangeListener(onFocusChangeListener);
		}

		spCat = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spCat, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_NGANHHANG);
		PriUtils.getInstance().setOnItemSelectedListener(spCat, this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvCat, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_NGANHHANG);
		spSubCat = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spSubCat, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_NGANHHANGCON);
		PriUtils.getInstance().setOnItemSelectedListener(spSubCat, this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvSubCat, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_NGANHHANGCON);
		spPageItem = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spPageItem, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_SOLUONGITEM);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvPageItem, PriHashMap.PriControl.NVBH_BANHANG_THEMHANG_SOLUONGITEM);
		PriUtils.getInstance().setOnItemSelectedListener(spPageItem, this);
	}

	/**
	 *
	 * handle search product after user click product code / product name from
	 * list combox textview
	 *
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Jan 8, 2013
	 */
	private void searchProductFlowFillter() {
		GlobalUtil.getInstance().forceHideKeyboardUseToggle(parent);

		// update object type for all item in list save
		for (int i = 0, size = this.orgListDTO.listObject.size(); i < size; i++) {
			this.orgListDTO.listObject.get(i).typeObject = FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_FILTER;
		}
		if (this.saveProductSelected(FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_FILTER)) {
			listDTO.listObject.clear();
			isSearchingProduct = true;
			String productCodeKeyWord = etInputOrderCode.getText().toString()
					.trim();
			String productNameKeyWord = etInputOrderName.getText().toString()
					.trim();
			getListProductAddOrder(0, productCodeKeyWord, productNameKeyWord,
					true);
		}
	}

	/**
	 * render layout for screen after get data from db
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		int pos = 1;
		int numberItem = 0;
		// chi add header cho table view lan dau tien
		if (isFirstLoadProduct) {
			tbProductOrderList.clearAllDataAndHeader();
			isShowPrice = GlobalInfo.getInstance().isSysShowPrice();
			int indexShortTotal = 10;
			int indexShortPromotion = 6;
			if (!isShowPrice){
				indexShortTotal = 8;
				indexShortPromotion = 4;
			}
			SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
					.addInfoCaseUnSensitive(2, SortActionConstants.PRODUCT_CODE)
					.addInfoCaseUnSensitive(3, SortActionConstants.PRODUCT_NAME)
					.addInfo(indexShortPromotion, SortActionConstants.PROMOTION)
					.addInfo(indexShortTotal,
							SortActionConstants.PRODUCT_STOCK_TOTAL).build();
			// khoi tao header cho table
			initHeaderTable(tbProductOrderList, new FindProductAddOrderListRow(
					parent, tbProductOrderList, isShowPrice), lstSort, this);
		}
		tbProductOrderList.clearAllData();
		if (listDTO.listObject.size() > 0) {
			numberItem = listDTO.totalObject;
			for (int i = 0, size = listDTO.listObject.size(); i < size; i++) {
				FindProductSaleOrderDetailViewDTO dto = listDTO.listObject
						.get(i);
				FindProductAddOrderListRow row = new FindProductAddOrderListRow(
						parent, tbProductOrderList,isShowPrice);
				row.setClickable(true);
				row.setTag(Integer.valueOf(pos));
				// update number order for product on view
				FindProductSaleOrderDetailViewDTO productSelected = this
						.getNumberOrderForProduct(dto);
				if (productSelected != null) {
					dto = productSelected;
					listDTO.listObject.set(i, productSelected);

				}
				if (size == 1) {
					row.renderLayout(
							pos
									+ (tbProductOrderList.getPagingControl()
											.getCurrentPage() - 1)
									* pageItem, dto, true);
					//hien ban phim custom neu co su dung + co 1 san pham
					if (GlobalInfo.getInstance().isUsingCustomKeyboard()) {
						parent.showKeyboardCustom(null);
					} else{
						GlobalUtil.getInstance().showKeyboardUseToggle(parent);
					}
				} else {
					row.renderLayout(
							pos
									+ (tbProductOrderList.getPagingControl()
											.getCurrentPage() - 1)
									* pageItem, dto, false);

				}

				row.setListener(this);
				tbProductOrderList.addRow(row);
				pos++;
			}
		} else {
			tbProductOrderList.showNoContentRow(StringUtil.getString(R.string.TEXT_NOTIFY_PRODUCT_NULL));
		}
		if (isFirstLoadProduct) {
			// update textView number item
			SpannableObject obj = new SpannableObject();
			obj.addSpan(StringUtil.getString(R.string.TEXT_NOTIFY_TOTAL) + " ",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			obj.addSpan(String.valueOf(numberItem),
					ImageUtil.getColor(R.color.RED),
					android.graphics.Typeface.BOLD);
			obj.addSpan(" " + StringUtil.getString(R.string.TEXT_NOTIFY_PRODUCT),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tvNumberProduct.setText(obj.getSpan());
		} else if (isSearchingProduct) {
			SpannableObject obj = new SpannableObject();
			obj.addSpan(StringUtil.getString(R.string.TEXT_NOTIFY_TOTAL) + " ",
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			obj.addSpan(String.valueOf(numberItem),
					ImageUtil.getColor(R.color.RED),
					android.graphics.Typeface.BOLD);
			obj.addSpan(" " + StringUtil.getString(R.string.TEXT_NOTIFY_PRODUCT),
					ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			tvNumberProduct.setText(obj.getSpan());
		}
		tvNumberProduct.setVisibility(View.VISIBLE);

	}

	@Override
	protected void setTitleHeaderView(String title) {
		super.setTitleHeaderView(title);
	}

	/**
	 *
	 * init cho textview de autoComplete
	 *
	 * @author: HieuNH6
	 * @param list
	 * @return: void
	 * @throws:
	 */
	private void initAutoCompleteTextView(
			AutoCompleteFindProductSaleOrderDetailViewDTO list) {
		lstInfoSearch = list;
		customAutoCompleteAdapter customAutoCompleteFilterProductCode = new customAutoCompleteAdapter(
				parent, R.layout.custom_dropdown_item_1line,
				list.strInputOrderCode, null,
				customAutoCompleteAdapter.FILTER_CODE);
		etInputOrderCode.setAdapter(customAutoCompleteFilterProductCode);
		etInputOrderCode.setThreshold(1);

		customAutoCompleteFilterProductName = new customAutoCompleteAdapter(
				parent, R.layout.custom_dropdown_item_1line,
				list.strInputOrderName, list.strInputOrderNameTextUnSigned,
				customAutoCompleteAdapter.FILTER_NAME);
		etInputOrderName.setAdapter(customAutoCompleteFilterProductName);
		etInputOrderName.setThreshold(1);
		PriUtils.getInstance().setOnTouchListener(etInputOrderName, this);
		renderSpCat();
		renderSpPageItem();
		renderSpSubcat();
	}

	public class customAutoCompleteAdapter extends ArrayAdapter<String> {

		public static final int FILTER_CODE = 0;
		public static final int FILTER_NAME = 1;
		public ArrayList<String> allItems;
		// su dung cho trong hop filter ten khong dau
		public ArrayList<String> allItemsNoSinged = new ArrayList<String>();
		public ArrayList<String> items;
		public ArrayList<String> arrayListFilter = new ArrayList<String>();
		public ArrayList<Integer> listFilteredIndex = new ArrayList<Integer>();

		// 0: input product code,
		// 1: input product name
		public int typeInput = -1;

		/**
		 * @param context
		 * @param textViewResourceId
		 * @param strInputData
		 */
		@SuppressWarnings("unchecked")
		public customAutoCompleteAdapter(Context context,
				int textViewResourceId, ArrayList<String> strInputData,
				ArrayList<String> strInputDataNoSigend, int type) {
			super(context, textViewResourceId, strInputData);
			this.items = strInputData;
			allItems = (ArrayList<String>) this.items.clone();
			typeInput = type;
			if (type == FILTER_NAME) {
				allItemsNoSinged = strInputDataNoSigend;
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (typeInput == FILTER_CODE) {
				TextView view = (TextView) super.getView(position, convertView,
						parent);
				String textOfRow = view.getText().toString().trim();
				String textInput = Constants.STR_BLANK;
				if (!StringUtil.isNullOrEmpty(etInputOrderCode.getText()
						.toString())) {
					textInput = etInputOrderCode.getText().toString().trim()
							.toUpperCase();
				}
				if (!StringUtil.isNullOrEmpty(textInput)
						&& !StringUtil.isNullOrEmpty(textOfRow)) {
					int index = textOfRow.toUpperCase().indexOf(textInput);
					if (index >= 0) {
						SpannableObject obj = new SpannableObject();
						int idBegin = textOfRow.toUpperCase()
								.indexOf(textInput);
						int idEnd = idBegin + textInput.length();
						if (idBegin == 0) {
							obj.addSpan(textOfRow.substring(idBegin, idEnd),
									ImageUtil.getColor(R.color.BLACK),
									android.graphics.Typeface.BOLD);
							if (idEnd < textOfRow.length()) {
								obj.addSpan(textOfRow.substring(idEnd),
										ImageUtil.getColor(R.color.BLACK),
										android.graphics.Typeface.NORMAL);
							}
						} else {
							obj.addSpan(textOfRow.substring(0, idBegin),
									ImageUtil.getColor(R.color.BLACK),
									android.graphics.Typeface.NORMAL);
							if (idBegin < textOfRow.length()) {
								obj.addSpan(
										textOfRow.substring(idBegin, idEnd),
										ImageUtil.getColor(R.color.BLACK),
										android.graphics.Typeface.BOLD);
								if (idEnd < textOfRow.length()) {
									obj.addSpan(textOfRow.substring(idEnd),
											ImageUtil.getColor(R.color.BLACK),
											android.graphics.Typeface.NORMAL);
								}
							}
						}
						view.setText(obj.getSpan());
					} else {
						return super.getView(position, convertView, parent);
					}
				} else {
					return super.getView(position, convertView, parent);
				}
				return view;
			} else {
				return super.getView(position, convertView, parent);
			}
		}

		@Override
		public Filter getFilter() {
			return myFilter;
		}

		Filter myFilter = new Filter() {

			@Override
			public CharSequence convertResultToString(Object resultValue) {
				return super.convertResultToString(resultValue);
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if (results != null && results.count > 0) {
					items.clear();
					// allItemsNoSingedFilter.clear();
					for (int i = 0, size = ((ArrayList<String>) results.values)
							.size(); i < size; i++) {
						items.add(((ArrayList<String>) results.values).get(i));
					}
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				if (constraint != null) {
					arrayListFilter.clear();
					listFilteredIndex.clear();
					// allItemsNoSingedFilterTMP.clear();
					if (typeInput == FILTER_CODE) {
						for (int i = 0, size = allItems.size(); i < size; i++) {
							if (allItems
									.get(i)
									.toUpperCase()
									.indexOf(
											String.valueOf(constraint)
													.toUpperCase()) != -1) {
								arrayListFilter.add(allItems.get(i));
							}
						}
					} else if (typeInput == FILTER_NAME) {
						for (int i = 0, size = allItemsNoSinged.size(); i < size; i++) {
							if (!StringUtil.isNullOrEmpty(allItemsNoSinged
									.get(i))) {
								if ((allItemsNoSinged.get(i).toUpperCase().indexOf(String.valueOf(constraint).toUpperCase()) != -1)
										|| (allItems.get(i).toUpperCase().indexOf(String.valueOf(constraint).toUpperCase()) != -1)) {
									arrayListFilter.add(allItems.get(i));
									// allItemsNoSingedFilterTMP.add(allItemsNoSinged.get(i));
									listFilteredIndex.add(i);
								}
							}
						}
					}
					filterResults.values = arrayListFilter;
					filterResults.count = arrayListFilter.size();
				}
				return filterResults;
			}
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_INIT_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			AutoCompleteFindProductSaleOrderDetailViewDTO dto = (AutoCompleteFindProductSaleOrderDetailViewDTO) modelEvent
					.getModelData();
			if (dto != null) {
				initAutoCompleteTextView(dto);
				getListProductAddOrder(0, this.productCodeKW,
						this.productNameKW, true);
			}
			break;
		case ActionEventConstant.GET_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			ListFindProductSaleOrderDetailViewDTO list = (ListFindProductSaleOrderDetailViewDTO) modelEvent
					.getModelData();
			if (list != null) {
				listDTO = list;
				this.isLoadData = true;
			}
			// paging
			if (listDTO.listObject.size() > 0) {
				if (tbProductOrderList.getPagingControl().totalPage < 0
						|| isSearchingProduct) {
					tbProductOrderList.getPagingControl().setVisibility(
							View.VISIBLE);
					tbProductOrderList.setNumItemsPage(pageItem);
					tbProductOrderList.setTotalSize(listDTO.totalObject,1);
				}
			} else {
				tbProductOrderList.getPagingControl().setVisibility(View.GONE);
			}

			renderLayout();
			if (isSearchingProduct) {
				isSearchingProduct = false;
			}
			if (isFirstLoadProduct) {
				isFirstLoadProduct = false;
			}
			this.parent.closeProgressDialog();
			requestInsertLogKPI(HashMapKPI.NVBH_THEMSANPHAM, actionEvent);
			break;
		case ActionEventConstant.GET_LIST_PROGRAME_FOR_PRODUCT: {
			List<ProgrameForProductDTO> listObject = (List<ProgrameForProductDTO>) modelEvent
					.getModelData();
			if (listObject != null) {
				this.listPrograme = listObject;
				this.showSelectProgrameFlowProduct();
				if (isFirstShowSelectPrograme) {
					isFirstShowSelectPrograme = false;
				}
			}
			this.parent.closeProgressDialog();
			break;
		}case ActionEventConstant.GET_LIST_SUB_CAT:
			ComboboxDisplayProgrameDTO result = (ComboboxDisplayProgrameDTO) modelEvent.getModelData();
			lstInfoSearch.lstSubcat = result.listSubcat;
			renderSpSubcat();
			searchProductFlowFillter();
			this.parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 *
	 * validate data num product input
	 *
	 * @author: HaiTC3
	 * @param productData
	 * @param numberProduct
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean validateDataProductInput(
			FindProductSaleOrderDetailViewDTO productData, String numberProduct) {
		boolean kq = false;
		if (productData.hasSelectPrograme) {
			if (StringUtil.isNullOrEmpty(numberProduct)) {
				kq = false;
			} else {
				if (numberProduct.equals("0")) {
					kq = false;
				} else {
					kq = StringUtil.isValidateNumProductInput(numberProduct);
				}
			}
		} else {
			if (!StringUtil.isNullOrEmpty(numberProduct)) {
				kq = StringUtil.isValidateNumProductInput(numberProduct);
			} else {
				kq = true;
			}
		}
		return kq;
	}

	/**
	 *
	 * save list product we selected and number order for each product
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public boolean saveProductSelected(int typeObjectSave) {
		if (listDTO != null && listDTO.listObject != null
				&& listDTO.listObject.size() > 0) {
			//change quantity format of active row
			for (int i = 0, size = tbProductOrderList.getListChildRow().size(); i < size; i++) {
				FindProductAddOrderListRow row = (FindProductAddOrderListRow) tbProductOrderList.getListChildRow().get(i);
				if (row.etNumber.isFocused()) {
					row.changeQuantityFormat();
				}
			}
		}
				
		boolean check = true;
		if (listDTO != null && listDTO.listObject != null
				&& listDTO.listObject.size() > 0) {
			for (int i = 0, size = tbProductOrderList.getListChildRow().size(); i < size; i++) {
				FindProductAddOrderListRow row = (FindProductAddOrderListRow) tbProductOrderList
						.getListChildRow().get(i);
				String numberProduct = row.etNumber.getText().toString();
				if (!this.validateDataProductInput(listDTO.listObject.get(i),
						numberProduct)) {
					GlobalUtil
							.showDialogConfirm(
									this.parent,
									StringUtil.getString(R.string.TEXT_NOTIFY_INPUT_NUM_PRODUCT_INCORRECT),
									StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
									Constants.STR_BLANK, 0, null);
					check = false;
					break;
				}
				if (!StringUtil.isNullOrEmpty(numberProduct) && !validatePrice(listDTO.listObject.get(i))) {
					GlobalUtil
							.showDialogConfirm(
									this.parent,
									StringUtil.getString(R.string.TEXT_NOTIFY_CHOOSE_PRODUCT_HAS_PRICE),
									StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
									Constants.STR_BLANK, 0, null);
					check = false;
					row.etNumber.requestFocus();
					break;
				}
			}
			if (check) {
				for (int i = 0, size = tbProductOrderList.getListChildRow()
						.size(); i < size; i++) {
					FindProductAddOrderListRow row = (FindProductAddOrderListRow) tbProductOrderList
							.getListChildRow().get(i);
					String numberProduct = row.etNumber.getText().toString();
					if (this.validateDataProductInput(
							listDTO.listObject.get(i), numberProduct)) {
						if ((!StringUtil.isNullOrEmpty(numberProduct) && !numberProduct
								.equals("0"))
								|| listDTO.listObject.get(i).hasSelectPrograme) {
							if (!this
									.isExitsProductInListSelected(listDTO.listObject
											.get(i))) {
								if (StringUtil.isNullOrEmpty(numberProduct)) {
									listDTO.listObject.get(i).numProduct = "0";
								} else {
									listDTO.listObject.get(i).numProduct = numberProduct;
								}
								listDTO.listObject.get(i).typeObject = typeObjectSave;
								orgListDTO.listObject.add(0,listDTO.listObject
										.get(i));
							} else {
								if (this.getIndexProductInProductListSelected(listDTO.listObject
										.get(i)) >= 0) {

									// update value number product for org list
									// dto
									int index = getIndexProductInProductListSelected(listDTO.listObject
											.get(i));
									if (StringUtil.isNullOrEmpty(numberProduct)) {
										listDTO.listObject.get(i).numProduct = "0";
									} else {
										listDTO.listObject.get(i).numProduct = numberProduct;
									}
									listDTO.listObject.get(i).typeObject = typeObjectSave;
									orgListDTO.listObject.set(index,
											listDTO.listObject.get(i));
								}
							}
						} else {
							if (this.isExitsProductInListSelected(listDTO.listObject
									.get(i))) {
								if (this.getIndexProductInProductListSelected(listDTO.listObject
										.get(i)) >= 0) {
									int index = this
											.getIndexProductInProductListSelected(listDTO.listObject
													.get(i));
									orgListDTO.listObject.remove(index);
								}
							}
						}
					} else {

					}
				}
			}
		}
		return check;
	}

	/**
	 * check saleOrderDetailDTO exits in list product selected
	 *
	 * @author: HaiTC3
	 * @param currentProduct
	 * @return
	 * @return: boolean
	 * @throws:
	 */
	public boolean isExitsProductInListSelected(
			FindProductSaleOrderDetailViewDTO currentProduct) {
		boolean kq = false;
		if (orgListDTO != null) {
			for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
				FindProductSaleOrderDetailViewDTO selectedProduct = orgListDTO.listObject
						.get(i);
				if (currentProduct.saleOrderDetail.productId == selectedProduct.saleOrderDetail.productId) {
					kq = true;
					break;
				}
			}
		}
		return kq;
	}

	/**
	 * get number order for product
	 *
	 * @author: HaiTC3
	 * @param product
	 * @return
	 * @return: int (0: none selected or order = 0)
	 * @throws:
	 */
	public FindProductSaleOrderDetailViewDTO getNumberOrderForProduct(
			FindProductSaleOrderDetailViewDTO product) {
		for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
			FindProductSaleOrderDetailViewDTO productSelected = orgListDTO.listObject
					.get(i);
			if (product.saleOrderDetail.productId == productSelected.saleOrderDetail.productId) {
				return productSelected;
			}
		}
		return null;
	}

	/**
	 * get index of current product in list product selected
	 *
	 * @author: HaiTC3
	 * @param product
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int getIndexProductInProductListSelected(
			FindProductSaleOrderDetailViewDTO product) {
		int kq = -1;
		if (this.isExitsProductInListSelected(product)) {
			for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
				FindProductSaleOrderDetailViewDTO selectedProduct = orgListDTO.listObject
						.get(i);
				if (product.saleOrderDetail.productId == selectedProduct.saleOrderDetail.productId) {
					kq = i;
					break;
				}
			}
		}
		return kq;
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			listDTO = new ListFindProductSaleOrderDetailViewDTO();
			orgListDTO = new ListFindProductSaleOrderDetailViewDTO();
			this.renderLayout();
			this.parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_LIST_PROGRAME_FOR_PRODUCT:
			this.parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_INIT_LIST_PRODUCT_ADD_TO_ORDER_VIEW:
			this.parent.closeProgressDialog();
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		super.onEvent(eventType, control, data);
	}

	@Override
	public void onClick(View v) {
		if (v == btClose) {
			GlobalUtil.getInstance().forceHideKeyboardUseToggle(parent);
			parent.onBackPressed();
			MyLog.v("close view", "message close view");
			// this.gotoPaymentOrderBilling();
		} else if (v == btSearch) {
			this.searchProductFlowFillter();
		} else if (v == btChoose) {
			GlobalUtil.getInstance().forceHideKeyboardUseToggle(parent);
			sendProductToOrderList();
		} else if (v == btClosePopupPrograme) {
			if (alertProductDetail.isShowing()) {
				alertProductDetail.dismiss();
			}
		} else if (v == btResetChoosePrograme) {
			this.resetSelectProgrameForProduct();
			if (alertProductDetail.isShowing()) {
				alertProductDetail.dismiss();
			}
		} else if (v == btClearAllInput) {
			// etInputCTKMCode.setText(Constants.STR_BLANK);
			etInputOrderCode.setText(Constants.STR_BLANK);
			etInputOrderName.setText(Constants.STR_BLANK);
			// mac dinh focus vao ma mat hang
			// etInputCTKMCode.clearFocus();
			etInputOrderName.clearFocus();
			etInputOrderCode.requestFocus();
			GlobalUtil.getInstance().showKeyboardUseToggle(parent);
		} else {
			GlobalUtil.getInstance().forceHideKeyboardUseToggle(parent);
			super.onClick(v);
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbProductOrderList) {
			// load more data for table product list
			if (this.saveProductSelected(FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_NOT_FILTER)) {
				getListProductAddOrder((tbProductOrderList.getPagingControl()
						.getCurrentPage() - 1), this.productCodeKW,
						this.productNameKW, false);
			} else {
				this.tbProductOrderList.getPagingControl()
						.setCurrentPage(
								this.tbProductOrderList.getPagingControl()
										.getOldPage());
			}
		}
	}

	/**
	 *
	 * send all product slected to order list screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void sendProductToOrderList() {		
		// send all product to order list
		if (this.saveProductSelected(FindProductSaleOrderDetailViewDTO.TYPE_OBJECT_SAVE_FILTER)) {
			// tinh toan lai trong luong cua san pham theo so luong san pham da
			// chon
			for (int i = 0, size = orgListDTO.listObject.size(); i < size; i++) {
				BigDecimal totalWeight = new BigDecimal(StringUtil.decimalFormatSymbols("#.###", orgListDTO.listObject.get(i).grossWeight));
				totalWeight = totalWeight.multiply(new BigDecimal(GlobalUtil.calRealOrder(
						orgListDTO.listObject.get(i).numProduct,
						orgListDTO.listObject.get(i).convfact))).setScale(3, RoundingMode.HALF_UP);

				orgListDTO.listObject.get(i).saleOrderDetail.totalWeight = totalWeight.doubleValue();
			}
			Bundle data = new Bundle();
			data.putSerializable(
					IntentConstants.INTENT_PRODUCTS_ADD_ORDER_LIST,
					(Serializable) orgListDTO);
			// sendBroadcast(ActionEventConstant.BROADCAST_CHOOSE_PRODUCT_ADD_ORDER_LIST,
			// data);

			OrderView orderFragment = (OrderView) parent.getFragmentManager().findFragmentByTag(OrderView.TAG);
			if (orderFragment != null) {
				orderFragment
						.receiveBroadcast(
								ActionEventConstant.BROADCAST_CHOOSE_PRODUCT_ADD_ORDER_LIST,
								data);
			}
			GlobalUtil.popBackStack(this.parent);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		case ActionEventConstant.SHOW_KEYBOARD_CUSTOM:
			// show keyboard custom
			parent.showKeyboardCustom(control);
			break;
		case ActionEventConstant.ROW_CLICK:
			// click row of table
			break;
		case ActionEventConstant.GO_TO_PROMOTION_PROGRAME_DETAIL: {
			FindProductSaleOrderDetailViewDTO dto = (FindProductSaleOrderDetailViewDTO) data;
			this.requestGetPromotionDetail(dto.promotionProgrameCode,dto.promotionProgrameId);
			break;
		}
		case ActionEventConstant.GO_TO_CT_DETAIL: {
			if (this.listPrograme != null && this.listPrograme.size() > 0
					&& !isUpdate) {
				currentObjectClick = (FindProductSaleOrderDetailViewDTO) data;
				this.showSelectProgrameFlowProduct();
			} else {
				if (isUpdate) {
					isUpdate = false;
				}
				currentObjectClick = (FindProductSaleOrderDetailViewDTO) data;
				getListProgrameFlowProduct((FindProductSaleOrderDetailViewDTO) data);
			}
			break;
		}
		case ActionEventConstant.ACTION_SELECT_PROGRAME:
			alertProductDetail.dismiss();
			if (data != null) {
				updateSelectProgrameForProduct((ProgrameForProductDTO) data);
			} else {
				this.resetSelectProgrameForProduct();
			}
			break;
		}
	}

	/**
	 * cap nhat trang thai san pham co chon chuong trinh khuyen mai hoac chuong
	 * trinh trung bay
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return: void
	 * @throws:
	 */
	public void updateSelectProgrameForProduct(ProgrameForProductDTO dto) {
		FindProductSaleOrderDetailViewDTO objectData = new FindProductSaleOrderDetailViewDTO();
		for (int i = 0, size = listDTO.listObject.size(); i < size; i++) {
			objectData = listDTO.listObject.get(i);
			if (objectData.saleOrderDetail.productId == currentObjectClick.saleOrderDetail.productId) {
				objectData.saleOrderDetail.programeType = dto.programeType;
				objectData.saleOrderDetail.programeCode = dto.programeCode;
				objectData.saleOrderDetail.programeTypeCode = dto.type;
				objectData.saleOrderDetail.programeId = dto.programId;
				objectData.hasSelectPrograme = true;
				break;
			}
		}

		for (int i = 0, size = tbProductOrderList.getListChildRow().size(); i < size; i++) {
			FindProductAddOrderListRow row = (FindProductAddOrderListRow) tbProductOrderList
					.getListChildRow().get(i);
			if (row.myData.saleOrderDetail.productId == currentObjectClick.saleOrderDetail.productId) {
				row.updateLayout(objectData);
				break;
			}
		}
	}

	/**
	 *
	 * reset select programe for product
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void resetSelectProgrameForProduct() {
		FindProductSaleOrderDetailViewDTO objectData = new FindProductSaleOrderDetailViewDTO();
		for (int i = 0, size = listDTO.listObject.size(); i < size; i++) {
			objectData = listDTO.listObject.get(i);
			if (objectData.saleOrderDetail.productId == currentObjectClick.saleOrderDetail.productId) {
				objectData.saleOrderDetail.programeType = PromotionProgrameDTO.PROGRAME_PROMOTION_AUTO;
				objectData.saleOrderDetail.programeCode = Constants.STR_BLANK;
				objectData.saleOrderDetail.programeTypeCode = Constants.STR_BLANK;
				objectData.saleOrderDetail.programeId = 0;
				objectData.hasSelectPrograme = false;
				// SP thuoc nganh hang Z hoac khac Z & gia = 0 - chi duoc tra thuong
				if (objectData.productInfoCode.equals("Z")
						|| (!objectData.productInfoCode.equals("Z") && objectData.saleOrderDetail.price == 0)) {
					objectData.numProduct = "0";
				}
				break;
			}
		}

		for (int i = 0, size = tbProductOrderList.getListChildRow().size(); i < size; i++) {
			FindProductAddOrderListRow row = (FindProductAddOrderListRow) tbProductOrderList
					.getListChildRow().get(i);
			if (row.myData.saleOrderDetail.productId == currentObjectClick.saleOrderDetail.productId) {
				row.updateLayout(objectData);
				break;
			}
		}
	}

	/**
	 * get list programe for product to choose
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return: void
	 * @throws:
	 */
	private void getListProgrameFlowProduct(
			FindProductSaleOrderDetailViewDTO data) {
		this.parent.showProgressDialog(StringUtil.getString(R.string.loading));
		isFirstShowSelectPrograme = true;
		Bundle dataSend = new Bundle();
		dataSend.putString(IntentConstants.INTENT_CUSTOMER_ID,
				this.currentCustomerId);
		dataSend.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritShopId());
		dataSend.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		dataSend.putString(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				String.valueOf(this.customerTypeId));
		dataSend.putString(IntentConstants.INTENT_ORDER_TYPE,
				String.valueOf(this.orderType));

		handleViewEvent(dataSend, ActionEventConstant.GET_LIST_PROGRAME_FOR_PRODUCT, SaleController.getInstance());
	}

	/**
	 * show dialog select programe flow product
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return: void
	 * @throws:
	 */
	private void showSelectProgrameFlowProduct() {
		if (alertProductDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			LayoutInflater inflater = this.parent.getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_select_programe_for_product, null);

			tbProgrameList = (DMSTableView) view
					.findViewById(R.id.tbProgrameListView);
			btClosePopupPrograme = (Button) view
					.findViewById(R.id.btClosePopupPrograme);
			btClosePopupPrograme.setOnClickListener(this);
			btResetChoosePrograme = (Button) view
					.findViewById(R.id.btResetChoosePrograme);
			btResetChoosePrograme.setOnClickListener(this);
			tbProgrameList.addHeader(new SelectProgrameForProduct(
					parent, null));
			tbProgrameList.setNumItemsPage(listPrograme.size() + 1);

			build.setView(view);
			alertProductDetail = build.create();
			// alertProductDetail.setCancelable(false);

			Window window = alertProductDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		if (isFirstShowSelectPrograme) {
			tbProgrameList.clearAllData();
			int pos = 1;
			for (int i = 0, size = listPrograme.size(); i < size; i++) {
				ProgrameForProductDTO dto = listPrograme.get(i);
				SelectProgrameForProduct row = new SelectProgrameForProduct(
						parent, tbProgrameList);
				row.setClickable(true);
				row.setTag(dto);
				row.renderLayout(pos, dto);
				row.setListener(this);
				pos++;
				tbProgrameList.addRow(row);
			}
		} else if (listPrograme != null) {
			for(int i=0; i<listPrograme.size(); i++){
				if((listPrograme.get(i).programeCode + listPrograme.get(i).programeLevel).equals(((FindProductSaleOrderDetailViewDTO)currentObjectClick).saleOrderDetail.programeCode)){
					((SelectProgrameForProduct)(tbProgrameList.getListChildRow().get(i))).renderLayoutSelectedRow();
				} else {
					((SelectProgrameForProduct)(tbProgrameList.getListChildRow().get(i))).resetLayout();
				}
			}
		}
		alertProductDetail.show();
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				isSearchingProduct = true;
				isUpdate = true;
				isFirstLoadProduct = true;
				this.clearDataOfScreen();
				//this.getListProductAddOrder(0, this.productCodeKW, this.productNameKW, true);
				getInitListProductAddOrder();
			}
			break;

		default:
			super.receiveBroadcast(action, extras);
			break;
		}

	}

	/**
	 *
	 * clear data of screen when click action "update du lieu" on top screen
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void clearDataOfScreen() {
		etInputOrderCode.setText(Constants.STR_BLANK);
		etInputOrderName.setText(Constants.STR_BLANK);
		this.productCodeKW = Constants.STR_BLANK;
		this.productNameKW = Constants.STR_BLANK;
		listDTO = new ListFindProductSaleOrderDetailViewDTO();
		orgListDTO = new ListFindProductSaleOrderDetailViewDTO();
		listPrograme.clear();
		if (alertProductDetail != null && alertProductDetail.isShowing()) {
			alertProductDetail.dismiss();
		}
		productInfoCat = "";
		productInfoSubCat = "";
		tbProductOrderList.resetSortInfo();
	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
//		if (v == etInputOrderName) {
//			if (this.isFilteredSelected && customAutoCompleteFilterProductName != null && customAutoCompleteFilterProductName.allItemsNoSinged != null
//					&& customAutoCompleteFilterProductName.items != null && currentSelectedFilterItem > -1
//					&& currentSelectedFilterItem < customAutoCompleteFilterProductName.items.size()) {
//				etInputOrderName.setText(customAutoCompleteFilterProductName.allItemsNoSinged.get(customAutoCompleteFilterProductName.listFilteredIndex
//						.get(currentSelectedFilterItem)));
//				this.isFilteredSelected = false;
//			}
//		}
		return false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getView().setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					// handle back button
					parent.hideKeyboardCustom();
					return true;
				}
				return false;
			}
		});
	}

	 /**
	 * validate gia
	 * @author: Tuanlt11
	 * @param productData
	 * @return: void
	 * @throws:
	*/
	private boolean validatePrice(FindProductSaleOrderDetailViewDTO productData ){
		// kiem tra gia phu thuoc vao cau hinh, neu cau hinh = 1 thi phai dat
		// mat hang co gia( tuc la priceId > 0) va ko phai la mat hang chon KM tay
//		if (isShowPrice)
//			if (productData.saleOrderDetail.priceId == 0 && !productData.hasSelectPrograme)
//				return false;
		return true;
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		getListProductAddOrder((tbProductOrderList.getPagingControl()
				.getCurrentPage() - 1), this.productCodeKW,
				this.productNameKW, false);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		// TODO Auto-generated method stub
		if(arg0 == spCat){
			if(indexCat != position){
				indexCat = position;
				// truong hop co tat ca
				if (lstInfoSearch.lstCat.size() > 1) {
					if(indexCat == 0){
						productInfoCat = "";
					}else
						productInfoCat = lstInfoSearch.lstCat.get(indexCat - 1).name;
				}else// ko co tat ca
					productInfoCat = lstInfoSearch.lstCat.get(indexCat).name;
				getSubcat();
			}
		}else if(arg0 == spSubCat){
			if(indexSubCat != position){
				indexSubCat = position;
				if (lstInfoSearch.lstSubcat.size() > 1) {
					if(indexSubCat == 0){
						productInfoSubCat = "";
					}else
						productInfoSubCat = lstInfoSearch.lstSubcat.get(indexSubCat - 1).name;
				}else// ko co tat ca
					productInfoSubCat = lstInfoSearch.lstSubcat.get(indexSubCat).name;
				searchProductFlowFillter();
			}
		}else if(arg0 == spPageItem){
			if(indexPage != position){
				try {
					indexPage = position;
					pageItem = Integer.valueOf(strPageItem[position]);
					// lay role, shop dang dang nhap
					SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
					Editor prefsPrivateEditor = sharedPreferences.edit();
					String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
					prefsPrivateEditor.putString(roleShop + VNM_ITEM_PER_PAGE,pageItem +"");
					prefsPrivateEditor.commit();
				} catch (Exception e) {
					// TODO: handle exception
					VNMTraceUnexceptionLog.getReportFromThrowable(e);
				}

			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}


	 /**
	 * Lay nganh hang con
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void getSubcat() {
		if (!parent.isShowProgressDialog()) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading));
		}
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_PRODUCT_CAT, productInfoCat);
		handleViewEvent(bundle, ActionEventConstant.GET_LIST_SUB_CAT, SaleController.getInstance());
	}


	 /**
	 * render spinner cat
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderSpCat() {
		// TODO Auto-generated method stub
		int size = 0;
		int i = 1;
		if(lstInfoSearch.lstCat.size() > 1){
			size = lstInfoSearch.lstCat.size() + 1;
		}else{
			size = lstInfoSearch.lstCat.size();
			i = 0;
		}
		String[] strAdap = new String[size];
		//add text all neu truong hop co hon 2 nganh hang
		if(lstInfoSearch.lstCat.size() > 1)
			strAdap[0] = StringUtil.getString(R.string.TEXT_ALL);
		for (DisplayProgrameItemDTO dto : lstInfoSearch.lstCat) {
			strAdap[i] = dto.name;
			i++;
		}
//		SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, strAdap);
		ArrayAdapter adapterNPP = new ArrayAdapter(parent, R.layout.simple_spinner_item, strAdap);
        adapterNPP.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		spCat.setAdapter(adapterNPP);
		spCat.setSelection(0);
		indexCat = 0;

	}


	 /**
	 * render spinner subcat
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderSpSubcat() {
		// TODO Auto-generated method stub
		int size = 0;
		int i = 1;
		if(lstInfoSearch.lstSubcat.size() > 1){
			size = lstInfoSearch.lstSubcat.size() + 1;
		}else{
			size = lstInfoSearch.lstSubcat.size();
			i = 0;
		}
		String[] strAdap = new String[size];
		//add text all neu truong hop co hon 2 nganh hang
		if(lstInfoSearch.lstSubcat.size() > 1)
			strAdap[0] = StringUtil.getString(R.string.TEXT_ALL);
		for (DisplayProgrameItemDTO dto : lstInfoSearch.lstSubcat) {
			strAdap[i] = dto.name;
			i++;
		}
//		SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, strAdap);
        ArrayAdapter adapterNPP = new ArrayAdapter(parent, R.layout.simple_spinner_item, strAdap);
        adapterNPP.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		spSubCat.setAdapter(adapterNPP);
		spSubCat.setSelection(0);
		indexSubCat = 0;
		
		//Chon sub cat cho dung
		if(lstInfoSearch.lstSubcat.size() == 1){
			productInfoSubCat = lstInfoSearch.lstSubcat.get(0).name;
		} else {
			productInfoSubCat = "";
		}
	}

	 /**
	 * Render spinner so trang/ page
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void renderSpPageItem(){
		SharedPreferences sharedPreferences = GlobalInfo.getInstance()
				.getDmsPrivateSharePreference();
		String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
		String itemPerPage = sharedPreferences.getString(roleShop+ VNM_ITEM_PER_PAGE, "");
		int index = 0;
		for(int i = 0, size = strPageItem.length; i < size; i ++){
			String item = strPageItem[i];
			if(item.equals(itemPerPage)){
				index = i;
				try {
					pageItem = Integer.valueOf(item);
				} catch (Exception e) {
					// TODO: handle exception
					VNMTraceUnexceptionLog.getReportFromThrowable(e);
				}
				break;
			}
		}
//		SpinnerAdapter adapt = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, strPageItem);
        ArrayAdapter adapt = new ArrayAdapter(parent, R.layout.simple_spinner_item, strPageItem);
        adapt.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		spPageItem.setAdapter(adapt);
		spPageItem.setSelection(index);
		indexPage = index;
	}
}
