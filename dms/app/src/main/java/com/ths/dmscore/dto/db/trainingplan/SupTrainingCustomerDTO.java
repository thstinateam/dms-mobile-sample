package com.ths.dmscore.dto.db.trainingplan;

/**
 * 
 * SupTraningCustomerDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:42:44 17-11-2014
 */
public class SupTrainingCustomerDTO {
	private int supTrainingCustomerId;
	private int supTrainingPlanId;
	private int trainingRateId;
	private int customerId;
	private String note;
	private String createUser;
	private String updateUser;
	private String createDate;
	private String updateDate;

	public int getSupTraningCustomerId() {
		return supTrainingCustomerId;
	}

	public void setSupTraningCustomerId(int supTrainingCustomerId) {
		this.supTrainingCustomerId = supTrainingCustomerId;
	}

	public int getSupTraningPlanId() {
		return supTrainingPlanId;
	}

	public void setSupTraningPlanId(int supTrainingPlanId) {
		this.supTrainingPlanId = supTrainingPlanId;
	}

	public int getTrainingRateId() {
		return trainingRateId;
	}

	public void setTrainingRateId(int trainingRateId) {
		this.trainingRateId = trainingRateId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
}
