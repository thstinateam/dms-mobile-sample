/**
 * Copyright 2011 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import android.util.Log;

import com.google.code.microlog4android.LoggerFactory;
import com.ths.dmscore.global.GlobalInfo;

/**
 *  Show log trace(interface)
 *  @author: TamPQ
 *  @version: 1.1
 *  @since: 1.0
 */
public class VinamilkLog{

	public static final com.google.code.microlog4android.Logger logger = LoggerFactory.getLogger();
	
	public static void d(String tag, String msg){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.d(tag, msg);
		}
	}
	
	public static void d(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.d(tag, msg, tr);
		}
	}
	
	public static void e(String tag, String msg){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.e(tag, msg);
		}
	}
	
	public static void e(String tag, String msg, Throwable tr) {
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.e(tag, msg, tr);
		}
	}
	
	public static void i(String tag, String msg){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.i(tag, msg);
		}
	}
	
	public static void i(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.i(tag, msg, tr);
		}
	}	

	public static synchronized void logToFile(String title, String content) {
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			logger.debug(title + " : " + content);
			logger.debug("-------------------------------------");
		}
	}
	
	public static void v(String tag, String msg){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.v(tag, msg);
		}
	}
	
	public static void v(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.v(tag, msg, tr);
		}
	}
	
	public static void w(String tag, String msg){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.w(tag, msg);
		}
	}
	
	public static void w(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().getProfile().isDebugMode()){
			Log.w(tag, msg, tr);
		}
	}
}
