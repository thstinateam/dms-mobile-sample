/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.ChannelTypeDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * dto thong tin cho chi tiet khach hang
 * @author : BangHN
 * since : 4:57:53 PM
 * version :
 */
public class CustomerInfoDTO {

	private CustomerDTO customer;//chi tiet khach hang
	private ChannelTypeDTO customerType;//
	public int sku = 0;//so mat hang khac nhau ban duoc trong ngay
	public int saleOrdersInMonth = 0;//so don hang trong thang
	public double amountInMonth = 0; //tong so tien cac don hang ban trong thang hien tai
	public double amountInOneMonthAgo = 0; //tong so tien cac don hang ban trong thang  - 1
	public double amountInTwoMonthAgo = 0; //tong so tien cac don hang ban trong thang - 2
	public double amountInThreeMonthAgo = 0; //tong so tien cac don hang ban trong thang  - 3
	//san luong
	public long quantityInMonth = 0; //tong san luong cac don hang ban trong thang hien tai
	public long quantityInOneMonthAgo = 0; //tong san luon cac don hang ban trong thang  - 1
	public long quantityInTwoMonthAgo = 0; //tong san luong cac don hang ban trong thang - 2
	public long quantityInThreeMonthAgo = 0; //tong san luong cac don hang ban trong thang  - 3
	
	public int numInMonth = 0; //chu ku hien tai
	public int numInOneMonthAgo = 0; //chu ku -1
	public int numInTwoMonthAgo = 0; //chu ky -2
	public int numInThreeMonthAgo = 0; //chu ky -3


	//chuong trinh khach hang tham gia
	private ArrayList<CustomerProgrameDTO> listCustomerPrograme;

	public int numLastOrdersCustomer = 0;
	//danh sach don hang gan fullDate
	public ArrayList<SaleOrderCustomerDTO> listOrderCustomer;


	public CustomerInfoDTO() {
		// TODO Auto-generated constructor stub
	}


	public CustomerDTO getCustomer() {
		return customer;
	}


	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}


	public ChannelTypeDTO getCustomerType() {
		return customerType;
	}


	public void setCustomerType(ChannelTypeDTO customerType) {
		this.customerType = customerType;
	}


	public ArrayList<CustomerProgrameDTO> getListCustomerPrograme() {
		return listCustomerPrograme;
	}

	public void setListCustomerPrograme(
			ArrayList<CustomerProgrameDTO> listCustomerPrograme) {
		this.listCustomerPrograme = listCustomerPrograme;
	}


	/**
	 * parse thong tin co ban cua khach hang
	 * @author : BangHN
	 * since : 1.0
	 */
	public void parseCustomerInfo(Cursor c, int sysCurrencyDivide){
		// chia nho don vi tinh
		if(customer == null)
			customer = new CustomerDTO();
		if(customerType == null)
			customerType = new ChannelTypeDTO();

		customer.initLogDTOFromCursor(c);
		customerType.initCustomerTypeDTOFromCursor(c);

		sku = CursorUtil.getInt(c, "NUM_SKU");
		saleOrdersInMonth = CursorUtil.getInt(c, "NUM_ORDER_IN_MONTH");
		amountInMonth = CursorUtil.getDouble(c, "AVG_IN_MONTH");
		quantityInMonth = CursorUtil.getLong(c, "AVG_QUANTITY_IN_MONTH");
	}
}
