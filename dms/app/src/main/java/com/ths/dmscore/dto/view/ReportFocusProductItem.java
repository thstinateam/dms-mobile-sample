/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  thong tin mot mat hang trong tam
 *  @author: HaiTC3
 *  @version: 1.1
 *  @since: 1.0
 */
public class ReportFocusProductItem implements Serializable{
	private static final long serialVersionUID = -7116178542648036774L;
	// d/s ke hoach
	public double amountPlan;
	// d/s da thuc hien
	public double amount;
	// tien do
	public int progress;
	// con lai
	public double remain;
	// forcus item name
	public String focusItemName;

	public ReportFocusProductItem() {
		amountPlan = 0;
		amount = 0;
		progress = 0;
		remain = 0;
		focusItemName = "";
	}
	
	/**
	 * 
	*  parse data from cursor with number display program forcus
	*  @author: HaiTC3
	*  @param c
	*  @param numForcus
	*  @return: void
	*  @throws:
	 */
	public void parseDataFromCursor(Cursor c, String numForcus){
		amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "FOCUS" + numForcus + "_AMOUNT_PLAN");
		amount = CursorUtil.getDoubleUsingSysConfig(c, "FOCUS" + numForcus + "_AMOUNT");
	}
}