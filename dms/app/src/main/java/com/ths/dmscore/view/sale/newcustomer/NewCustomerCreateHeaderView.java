/*
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.newcustomer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.AreaItem;
import com.ths.dmscore.dto.view.CreateCustomerInfoDTO.CustomerType;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dms.R;

/**
 * Create customer header for SaleMan CreateCustomerHeaderView.java
 * 
 * @author: duongdt3
 * @version: 1.0
 * @since: 16:20:38 4 Jan 2014
 */
public class NewCustomerCreateHeaderView  extends LinearLayout implements OnItemSelectedListener, View.OnTouchListener {
	// ten khach hang
	private TextView tvCustomerName;
	// input ten khach hang
	private VNMEditTextClearable edCustomerName;
	// textView Loai khach hang
	private TextView tvType;
	// spinner loai khach hang
	private Spinner spinnerType;
	// textView so dien thoai co dinh
	private TextView tvPhone;
	// input so dien thoai co dinh
	private VNMEditTextClearable edPhone;
	// input email
	private VNMEditTextClearable edEmail;
	// textView so dien thoai di dong
	private TextView tvMobilePhone;
	// input so dien thoai di dong
	private VNMEditTextClearable edMobilePhone;
	// textview nguoi lien he
	private TextView tvContactName;
	// input nguoi lien he
	private VNMEditTextClearable edContactName;
	// textview Tinh/ thanh pho
	private TextView tvProvine;
	// spinner chon tinh/thanh pho
	private Spinner spinnerProvine;
	//TextView quan/huyen
	private TextView tvDistrict;
	//spinner chon quan/huyen
	private Spinner spinnerDistrict;
	//TextView phuong xa
	private TextView tvPrecinct;
	// spinner chon phuong/xa
	private Spinner spinnerPrecinct;
	//input so nha
	private VNMEditTextClearable edHouseNumber;
	//input duong
	private VNMEditTextClearable edStreet;
	// textview thong bao nhap sai
	private TextView tvError;
	
	//for map fragment use
	public Button btSave;
	private boolean isEditMode = false;
	private TextView tvEmail;
	private TextView tvHouseNumber;
	private TextView tvStreet;
	public NewCustomerCreateHeaderView(Context context, boolean isEditMode) {
		super(context);
		this.isEditMode  = isEditMode; 
		initView(context);
	}

	public NewCustomerCreateHeaderView(Context context, AttributeSet attrs) {
		super(context);
		initView(context);
	}

	private void initView(Context mContext) {
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_new_customer_header, this, true);
		TableLayout table = (TableLayout) view.findViewById(R.id.table);
		tvCustomerName = (TextView) view.findViewById(R.id.tvCustomerName);
		edCustomerName = (VNMEditTextClearable) view.findViewById(R.id.edCustomerName);
		tvType = (TextView) view.findViewById(R.id.tvType);
		spinnerType = (Spinner) view.findViewById(R.id.spinnerType);
		tvPhone = (TextView) view.findViewById(R.id.tvPhone);
		edPhone = (VNMEditTextClearable) view.findViewById(R.id.edPhone);
//		edEmail = (VNMEditTextClearable) view.findViewById(R.id.edEmail);
		edEmail = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.edEmail, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_EMAIL, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		tvEmail = (TextView) PriUtils.getInstance().findViewById(view, R.id.tvEmail, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_EMAIL);
		tvMobilePhone = (TextView) view.findViewById(R.id.tvMobilePhone);
		edMobilePhone = (VNMEditTextClearable) view.findViewById(R.id.edMobilePhone);
		tvContactName = (TextView) view.findViewById(R.id.tvContactName);
		edContactName = (VNMEditTextClearable) view.findViewById(R.id.edContactName);
		tvProvine = (TextView) view.findViewById(R.id.tvProvine);
		spinnerProvine = (Spinner) view.findViewById(R.id.spinnerProvine);
		tvDistrict = (TextView) view.findViewById(R.id.tvDistrict);
		spinnerDistrict = (Spinner) view.findViewById(R.id.spinnerDistrict);
		tvPrecinct = (TextView) view.findViewById(R.id.tvPrecinct);
		spinnerPrecinct = (Spinner) view.findViewById(R.id.spinnerPrecinct);
		//TextView tvHouseNumber = (TextView) view.findViewById(R.id.tvHouseNumber);
		tvHouseNumber = (TextView) PriUtils.getInstance().findViewById(view, R.id.tvHouseNumber, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_SONHA);
		edHouseNumber = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.edHouseNumber, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_SONHA, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		tvStreet = (TextView) PriUtils.getInstance().findViewById(view, R.id.tvStreet, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_TENDUONG);
		edStreet = (VNMEditTextClearable) PriUtils.getInstance().findViewById(view, R.id.edStreet, PriHashMap.PriControl.NVBH_THEMMOIKHACHHANG_TENDUONG, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		btSave = (Button) view.findViewById(R.id.btSave);
		tvError = (TextView) view.findViewById(R.id.tvError);

		//set event for spinners
		spinnerDistrict.setOnItemSelectedListener(this);
		spinnerPrecinct.setOnItemSelectedListener(this);
		spinnerProvine.setOnItemSelectedListener(this);
		spinnerType.setOnItemSelectedListener(this);
		
		// array of view
		TextView[] arrTextView = new TextView[] { tvCustomerName, tvType,
				tvPhone, tvMobilePhone, tvContactName, tvProvine, tvDistrict,
				tvPrecinct };

		setRequireSymbolTextViews(arrTextView);
		
		//hide keyboard
		table.setOnTouchListener(this);
		spinnerDistrict.setOnTouchListener(this);
		spinnerPrecinct.setOnTouchListener(this);
		spinnerProvine.setOnTouchListener(this);
		spinnerType.setOnTouchListener(this);
		btSave.setOnTouchListener(this);
		
		//khong ko cho chinh sua neu la view mode
		if (!isEditMode) {
			//an nut luu
			btSave.setVisibility(View.INVISIBLE);
			//disiable toan bo view
			//spinner
			spinnerType.setEnabled(false);
			spinnerProvine.setEnabled(false);
			spinnerDistrict.setEnabled(false);
			spinnerPrecinct.setEnabled(false);
			
			//edit text
			edCustomerName.setEnabled(false);
			edPhone.setEnabled(false);
			edEmail.setEnabled(false);
			edMobilePhone.setEnabled(false);
			edContactName.setEnabled(false);
			edHouseNumber.setEnabled(false);
			edStreet.setEnabled(false);
		}
	}

	/**
	 * set dau * cho cac TextView thong tin bat buot
	 * 
	 * @author: duongdt3
	 * @since: 14:33:32 6 Jan 2014
	 * @return: void
	 * @throws:
	 * @param arrTextView
	 */
	private void setRequireSymbolTextViews(TextView[] arrTextView) {
		for (int i = 0; i < arrTextView.length; i++) {
			String text = arrTextView[i].getText().toString();
			int color = ImageUtil.getColor(R.color.RED);
			// them *: vao
			text += " " + StringUtil.getColorText("*", color);

			arrTextView[i].setText(StringUtil.getHTMLText(text));
		}
	}
	
	NewCustomerCreateView parrent = null;
	public void setParrent(NewCustomerCreateView view){
		parrent = view;
	}
	
	CreateCustomerInfoDTO dto;
	
	/**
	 * Set thong tin KH
	 * 
	 * @author: duongdt3
	 * @since: 16:37:29 4 Jan 2014
	 * @return: void
	 * @throws:
	 * @param dto
	 */
	public void setCustomerInfo(CreateCustomerInfoDTO dto) {
		this.dto = dto;
		
		if(dto != null){
    		// neu co thong tin KH
    		if (dto.cusInfo != null) {
    			// ten
    			edCustomerName.setText(dto.cusInfo.getCustomerName());
    			// dien thoai ban
    			edPhone.setText(dto.cusInfo.getPhone());
    			// email
    			edEmail.setText(dto.cusInfo.email);
    			// fax
    			// dien thoai di dong
    			edMobilePhone.setText(dto.cusInfo.getMobilephone());
    			// nguoi lien he
    			edContactName.setText(dto.cusInfo.getContactPerson());
    			// so nha
    			edHouseNumber.setText(dto.cusInfo.getHouseNumber());
    			// duong
    			edStreet.setText(dto.cusInfo.getStreet());
    		}
    		
    		boolean isHaveC2Type = true;

    		// Danh sach loai khach hang
    		int indexC2Remove = renderListCusType(isHaveC2Type);
    		if (indexC2Remove >= 0) {
    			//neu vi tri hien tai nam duoi C2, ma c2 da bi remove => tru index di 1
				if (dto.currentIndexType > indexC2Remove) {
					dto.currentIndexType--;
				}
			}
    		spinnerType.setSelection(dto.currentIndexType);

    		// Danh sach tinh
    		renderListProvince();    		
    		spinnerProvine.setSelection(dto.currentIndexProvince);

    		// Danh sach huyen
    		renderListDicstrict();
    		spinnerDistrict.setSelection(dto.currentIndexDistrict);
    
    		// Danh sach xa
    		renderListPrecinct();
    		spinnerPrecinct.setSelection(dto.currentIndexPrecinct);
		}
	}

	List<CustomerType> listCusTypeCurrent = new ArrayList<CustomerType>();
	boolean isHaveC2Current = true;
	
	/**
	 * hien thi danh sach loai khach hang
	 * @author: duongdt38
	 * @since: 09:00:58 8 Jan 2014
	 * @return: int
	 * @throws:  
	 * @param isHaveC2
	 * @return
	 */
	int renderListCusType(boolean isHaveC2){
		int indexC2 = -1;
		// Danh sach loai khach hang
		if(dto.listCusType != null){
			int sizeCusType = dto.listCusType.size();
			listCusTypeCurrent.clear();
			for (int i = 0; i < sizeCusType; i++) {
				CustomerType item = dto.listCusType.get(i);
				//neu ko render c2 => object_type = 4
				if (isHaveC2 || (!isHaveC2 && item.objectType != 4)) {
					listCusTypeCurrent.add(item);
				}else{
					indexC2 = i;
				}
			}
			
			int sizeCurrent = listCusTypeCurrent.size();
			if (sizeCurrent > 0) {
				String[] arrCusType = new String[sizeCurrent];
				for (int i = 0; i < sizeCurrent; i++) {
					arrCusType[i] = listCusTypeCurrent.get(i).typeName;
				}
				SpinnerAdapter adapterCusType = new SpinnerAdapter(getContext(), R.layout.simple_spinner_item, arrCusType);
				spinnerType.setAdapter(adapterCusType);
			}
		}
		
		isHaveC2Current = isHaveC2;
		return indexC2;
	}
	
	/**
	 * Hien thi danh sach huyen
	 * @author: duongdt3
	 * @since: 09:01:10 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListDicstrict(){
		// Danh sach huyen
		if(dto.listDistrict != null){
			int sizeDistrict = dto.listDistrict.size();
			String[] arrDistrict = new String[sizeDistrict];

			for (int i = 0; i < sizeDistrict; i++) {
				AreaItem item = dto.listDistrict.get(i);
				arrDistrict[i] = item.areaName;
			}
			SpinnerAdapter adapterDistrict = new SpinnerAdapter(getContext(), R.layout.simple_spinner_item, arrDistrict);
			spinnerDistrict.setAdapter(adapterDistrict);
		}
	}
	
	/**
	 * Hien thi danh sach tinh
	 * @author: duongdt3
	 * @since: 09:01:25 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListProvince(){
		// Danh sach tinh
		if(dto.listProvine != null){
			int sizeProvine = dto.listProvine.size();
			String[] arrProvine = new String[sizeProvine];

			for (int i = 0; i < sizeProvine; i++) {
				AreaItem item = dto.listProvine.get(i);
				arrProvine[i] = item.areaName;
			}
			SpinnerAdapter adapterProvine = new SpinnerAdapter(getContext(), R.layout.simple_spinner_item, arrProvine);
			spinnerProvine.setAdapter(adapterProvine);

		}
	}
	
	/**
	 * Hien thi danh sach xa
	 * @author: duongdt3
	 * @since: 09:01:35 8 Jan 2014
	 * @return: void
	 * @throws:
	 */
	void renderListPrecinct(){
		// Danh sach xa
		if(dto.listPrecinct != null){
			int sizePrecinct = dto.listPrecinct.size();
			String[] arrPrecinct = new String[sizePrecinct];

			for (int i = 0; i < sizePrecinct; i++) {
				AreaItem item = dto.listPrecinct.get(i);
				arrPrecinct[i] = item.areaName;
			}
			SpinnerAdapter adapterPrecinct = new SpinnerAdapter(getContext(), R.layout.simple_spinner_item, arrPrecinct);
			spinnerPrecinct.setAdapter(adapterPrecinct);
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> spinner, View v, int pos, long id) {
		if (spinner == spinnerDistrict) {
			if (this.dto.currentIndexDistrict != pos) {
				this.dto.setCurrentDistrict(pos);
				//request lai danh sach xa
				parrent.requestDataArea(CUSTOMER_TABLE.AREA_TYPE_PRECINCT, this.dto.curentIdDistrict);
			}
		} else if (spinner == spinnerPrecinct) {
			if (this.dto.currentIndexPrecinct != pos) {
				this.dto.setCurrentPrecinct(pos);
			}
		} else if (spinner == spinnerProvine) {
			if (this.dto.currentIndexProvince != pos) {
				this.dto.setCurrentProvince(pos);
				//request lai danh sach huyen
				parrent.requestDataArea(CUSTOMER_TABLE.AREA_TYPE_DISTRICT, this.dto.curentIdProvine);
			}
		} else if (spinner == spinnerType) {
			if (this.dto.currentIndexType != pos) {
				//vi co the bi thay doi C2 hay khong
				if (pos >= 0 && pos < listCusTypeCurrent.size()) {
					int typeId = listCusTypeCurrent.get(pos).typeId;
					this.dto.setCurrentType(pos, typeId);
					dto.cusInfo.setCustomerTypeId(typeId);
				} 
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> spinner) {
		
	}

	/**
	 * set danh sach dia ban theo loai render vao spinner
	 * @author: duongdt3
	 * @since: 17:14:16 6 Jan 2014
	 * @return: void
	 * @throws:  
	 * @param type
	 * @param arrayArea
	 */
	public void setArrayAreaInfo(int type, ArrayList<AreaItem> arrayArea) {
		switch (type) {
    		case CUSTOMER_TABLE.AREA_TYPE_PRECINCT:
    			//list xa
    			dto.listPrecinct = arrayArea;
    			dto.setCurrentPrecinct(-1);
    			renderListPrecinct();
    			break;
    		case CUSTOMER_TABLE.AREA_TYPE_DISTRICT:
    			//list huyen
    			dto.listDistrict = arrayArea;
    			dto.setCurrentDistrict(-1);
    			renderListDicstrict();
    			break;
    		default:
    			break;
		}
	}

	/**
	 * check cac input thong tin cho Khach hang
	 * @author: duongdt3
	 * @since: 08:34:36 7 Jan 2014
	 * @return: boolean
	 * @throws:  
	 * @return
	 */
	public boolean checkInputInfo() {
		boolean isVaild = true;
		String strError = "";
		//remove khoang trang
		edCustomerName.setText(edCustomerName.getText().toString().trim());
		edContactName.setText(edContactName.getText().toString().trim());
		edHouseNumber.setText(edHouseNumber.getText().toString().trim());
		edStreet.setText(edStreet.getText().toString().trim());
		
		//check
		if (edCustomerName.getText().toString().isEmpty()) {
			// kiem tra ten rong
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_CUSTOMER_NAME_NOT_NULL);
		} else if (!StringUtil.isCustomerNameContainValidChars(edCustomerName.getText().toString())) {
			// kiem tra ten co chua ky tu dac biet
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_CUSTOMER_NAME_NOT_VALID);
		} else if (edContactName.getText().toString().isEmpty()) {
			// kiem tra ten lien he rong
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_CONTACT_NAME_NOT_NULL);
		} else if (edMobilePhone.getText().toString().isEmpty()) {
			// kiem tra dien thoai di dong rong
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_MOBILE_NOT_NULL);
		} else if (edPhone.getText().toString().isEmpty()) {
			// kiem tra dien thoai ban rong
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_PHONE_NOT_NULL);
		}  else if (!StringUtil.isCustomerNameContainValidChars(edContactName.getText().toString())) {
			// kiem tra ten lien he co chua ky tu dac biet
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_CONTACT_NAME_NOT_VALID);
		} else if (!StringUtil.isNullOrEmpty(edHouseNumber.getText().toString()) && !StringUtil.isHouseNumberContainValidChars(edHouseNumber.getText().toString())) {
			// kiem tra so nha co gia tri + chua ky tu dac biet
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_HOUSE_NUMBER_NOT_VALID);
		} else if (!StringUtil.isNullOrEmpty(edStreet.getText().toString()) && !StringUtil.isStreetContainValidChars(edStreet.getText().toString())) {
			// kiem tra duong co gia tri + chua ky tu dac biet 
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_HOUSE_STREET_NOT_VALID);
		} else if (dto.currentIndexType < 0) {
			// kiem tra khong co loai khach hang
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_CUSTOMER_TYPE_NOT_NULL);
		} else if (dto.currentIndexPrecinct < 0) {
			// kiem tra khong co dia chi
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_PRECINCT_NOT_NULL);
		} else if (!StringUtil.isNullOrEmpty(edEmail.getText().toString()) && !StringUtil.isValidateEmail(edEmail.getText().toString())) {
			// kiem tra khong co dia chi
			isVaild = false;
			strError = StringUtil.getString(R.string.TEXT_NOTIFY_EMAIL_NOT_VALID);
		}
		
		tvError.setText(strError);
		return isVaild;
	}

	/**
	 * lay thong tin tu view => cusInfo
	 * @author: duongdt3
	 * @since: 09:12:47 7 Jan 2014
	 * @return: void
	 * @throws:  
	 */
	public void getInfoFromView() {
		//id
		dto.cusInfo.setCustomerTypeId((int)dto.curentIdType);
		dto.cusInfo.areaId = (int)dto.curentIdPrecinct;
		//text
		dto.cusInfo.setCustomerName(edCustomerName.getText().toString());
		dto.cusInfo.setMobilephone(edMobilePhone.getText().toString());
		dto.cusInfo.setPhone(edPhone.getText().toString());
		dto.cusInfo.email = edEmail.getText().toString();
		dto.cusInfo.setHouseNumber(edHouseNumber.getText().toString());
		dto.cusInfo.setStreet(edStreet.getText().toString());
		dto.cusInfo.setContactPerson(edContactName.getText().toString());
		dto.cusInfo.setAddress(spinnerPrecinct.getSelectedItem().toString() + ", " + spinnerDistrict.getSelectedItem().toString() + ", " + spinnerProvine.getSelectedItem().toString());
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		GlobalUtil.forceHideKeyboardInput(getContext(), this);
		return super.onTouchEvent(event);
	}
}
