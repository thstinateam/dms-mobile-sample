package com.ths.dmscore.view.main;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;

public class MenuItemRow extends LinearLayout {
	OnEventControlListener listener;
	Context mContext;
	LinearLayout llRow;
	public TextView tvText;
	
	public MenuItemRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initMenu();
	}

	public MenuItemRow(Context context, View row) {
		super(context);
		mContext = context;
		initMenu(row);
	}

	private void initMenu() {
		LayoutInflater vi = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = vi.inflate(R.layout.layout_fragment_menu_item, null);
		
		tvText = (TextView) view.findViewById(R.id.tvText);
		llRow = (LinearLayout) view.findViewById(R.id.llRow);
	}
	
	private void initMenu(View row) {
		llRow = (LinearLayout) row.findViewById(R.id.llRow);
		tvText = (TextView) row.findViewById(R.id.tvText);
	}

	public void populateFrom(MenuItemDTO item) {
		if(item != null){
			tvText.setText(item.getTextMenu());
			tvText.setCompoundDrawablesWithIntrinsicBounds(item.getIconMenu(), 0, 0, 0);
			if(item.isSelected()){
				llRow.setBackgroundResource(R.drawable.bg_vnm_menu_focus);
			}else{
				llRow.setBackgroundResource(R.drawable.fragment_menu_item_selector);
			}
		}
	}
	
	
	public void setBackground(int res){
		if(llRow != null)
			llRow.setBackgroundResource(res);
	}
}
