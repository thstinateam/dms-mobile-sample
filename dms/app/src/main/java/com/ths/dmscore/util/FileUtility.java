/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

/**
 * The Class FileUtility.
 */
public class FileUtility {

	/**
	 * tao zip danh sach file zip co mat khau
	 * @author: trungnt56
	 * @since: 3:05:53 PM Aug 21, 2014
	 * @return: String
	 * @throws:
	 * @param contentFiles
	 * @param zipFile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static String createZipFile(ArrayList<File> contentFiles,
			String zipFile, String password) throws Exception {
		net.lingala.zip4j.io.ZipOutputStream os = null;
		try {
			os = new net.lingala.zip4j.io.ZipOutputStream(new FileOutputStream(
					new File(zipFile)));
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

			// set level compress file
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

			if (!StringUtil.isNullOrEmpty(password)) {
				parameters.setEncryptFiles(true);
				parameters
						.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword(password);
			}

			for (File contentFile : contentFiles) {
				FileInputStream fileInputStream = new FileInputStream(
						contentFile);
				os.putNextEntry(contentFile, parameters);
				os.write(toByteArray(fileInputStream));
				os.closeEntry();
				fileInputStream.close();
			}

			os.finish();
		} catch (Exception e) {
			throw e;
		} finally {
			if (os != null) {
				os.close();
			}
		}
		for (File contentFile : contentFiles) {
			if (contentFile != null) {
				contentFile.delete();
			}
		}

		return zipFile;
	}

	/**
	 * Tao file zip co mat khau
	 * @author: banghn
	 * @param contentFile
	 * @param zipFile
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static String createZipFile(File contentFile, String zipFile,
			String password) throws Exception {
		ZipOutputStream os = null;
		try {
			os = new ZipOutputStream(new FileOutputStream(new File(zipFile)));
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

			// set level compress file
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);

			if (password != null && !password.trim().equals("")) {
				parameters.setEncryptFiles(true);
				parameters
						.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword(password);
			}

			FileInputStream fileInputStream = new FileInputStream(contentFile);
			os.putNextEntry(contentFile, parameters);
			writeFileMultiPart(fileInputStream, os);
			os.closeEntry();
			fileInputStream.close();
			os.finish();
		} catch (Exception e) {
			throw e;
		} finally {
			if (os != null) {
				os.close();
			}
		}

		if (contentFile != null) {
			contentFile.delete();
		}

		return zipFile;
	}

	/**
	 * convert input stream sang mang byte
	 * 
	 * @author: trungnt56
	 * @since: 3:05:24 PM Aug 21, 2014
	 * @return: byte[]
	 * @throws:
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private static byte[] toByteArray(InputStream is) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int l;
		byte[] data = new byte[1024];
		while ((l = is.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, l);
		}
		buffer.flush();
		return buffer.toByteArray();
	}

	/**
	 * Ghi file nhieu phan
	 * @author ThangNV31
	 * @param fis
	 * @param zos
	 * @throws Exception
	 */
	private static void writeFileMultiPart(FileInputStream fis,
			ZipOutputStream zos) throws Exception {
		int l = -1;
		byte[] data = new byte[8192];
		while ((l = fis.read(data, 0, data.length)) != -1) {
			zos.write(data, 0, l);
		}
	}
}
