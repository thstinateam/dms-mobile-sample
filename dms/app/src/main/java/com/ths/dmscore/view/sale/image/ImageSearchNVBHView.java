/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.image;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.ImageSearchViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

/**
 *
 * tim kiem hinh anh(nvbh)
 *
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
public class ImageSearchNVBHView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener, OnItemSelectedListener,
		OnTouchListener, OnItemClickListener, OnDateSetListener {

	public static final int ACTION_SEARCH_IMAGE = 0;// action tim kiem hinh anh
	public static final int ACTION_IMAGE = 1;// action hinh anh
	public static final int ACTION_PROMOTION = 2;
	public static final int ACTION_DISPLAY = 3;
	public static final int ACTION_PRODUCT = 4;
	private static final int NUM_LOAD_MORE = 20;
	protected static final int[] IMAGE_IDS = { R.id.imgAlbumImage };
	private static final int DATE_FROM_CONTROL = 1;// control den ngay
	private static final int DATE_TO_CONTROL = 2;// control tu ngay
//	private VNMEditTextClearable edCusCode;// ma khach hang
	private VNMEditTextClearable edCusName;// ten khach hang
	private Button btSearch;// tim kiem
	boolean isReload = false;
	private int currentPage = -1;
	private int currentCalender;
	private int selectedLineIndex;// index tuyen
	private int selectedProgrameIndex;// index ct
	private int selectedImageIndex;// index hinh anh
	private Spinner spLine;// sp tuyen
	private Spinner spPrograme;// sp ct
	private Spinner spImage;// sp hinh anh
	private VNMEditTextClearable edFromDate;// Tu ngay
	private VNMEditTextClearable edToDate;// Den ngay
	private List<DisplayProgrameItemDTO> listDisplayPrograme = null;// danh sach
																	// chuong
																	// trinh
	private List<DisplayProgrameItemDTO> listImage = null;// hinh anh

//	SpinnerAdapter adLine;// adapter tuyen
//	SpinnerAdapter adPrograme;// adapter ct
//	SpinnerAdapter adImage;// adapter hinh anh
	ArrayAdapter adLine;// adapter tuyen
	ArrayAdapter adPrograme;// adapter ct
	ArrayAdapter adImage;// adapter hinh anh

	private Button btReInput;// nhap lai
	Boolean isSearch = false;// Tim kiem hay load ds mac dinh
	ImageSearchViewDTO imageSearchDTO = new ImageSearchViewDTO();// dto tim
																	// kiem//
																	// hinh anh
	private GridView gvUserImageView;// hien thi danh sach hinh anh
	private ImageAdapter thumbs = null;
	private int numTopLoaded = 0;
	private boolean isAbleGetMore = true;
	TextView tvNoDataResult;// thong bao ko du lieu

	public static ImageSearchNVBHView getInstance(Bundle data) {
		ImageSearchNVBHView f = new ImageSearchNVBHView();
		f.setArguments(data);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_image_search_nvbh, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_TIMKIEMHINHANH);

//		setTitleHeaderView(getString(R.string.TITLE_VIEW_IMAGE_LIST_SEARCH));
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_IMAGE_LIST_SEARCH));
		PriUtils.getInstance().findViewByIdGone(v, R.id.llSearch, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM);
		PriUtils.getInstance().findViewById(v, R.id.tvImage, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_LOAIHINHANH);
		PriUtils.getInstance().findViewById(v, R.id.tvPrograme, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_CHUONGTRINH);
		PriUtils.getInstance().findViewById(v, R.id.tvLine, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TUYEN);
		PriUtils.getInstance().findViewById(v, R.id.tvFromDate, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TUNGAY);
		PriUtils.getInstance().findViewById(v, R.id.tvToDate, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_DENNGAY);
		PriUtils.getInstance().findViewById(v, R.id.tvCus, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TENMA);

//		edCusCode = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edCusCode, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TENMA);
		edCusName = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edCusName, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TENMA);
		edFromDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edFromDate, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		edToDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edToDate, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);

		PriUtils.getInstance().setOnTouchListener(edFromDate, this);
		PriUtils.getInstance().setOnTouchListener(edToDate, this);
		edFromDate.setIsHandleDefault(false);
		edToDate.setIsHandleDefault(false);

		edFromDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2), DateUtils.defaultDateFormat.toPattern()));
		edToDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));

		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btSearch, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM);
		PriUtils.getInstance().setOnClickListener(btSearch, this);

		spLine = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spLine, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_TUYEN);
		PriUtils.getInstance().setOnItemSelectedListener(spLine, this);

		spPrograme = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spPrograme, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_CHUONGTRINH);
		PriUtils.getInstance().setOnItemSelectedListener(spPrograme, this);

		btReInput = (Button) PriUtils.getInstance().findViewById(v, R.id.btReInput, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_NHAPLAI);
		PriUtils.getInstance().setOnClickListener(btReInput, this);

		spImage = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spImage, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_TIMKIEM_LOAIHINHANH);
		PriUtils.getInstance().setOnItemSelectedListener(spImage, this);

//		gvUserImageView = (GridView) view
//				.findViewById(R.id.gvImageUserAlbumView);
//		gvUserImageView.setOnItemClickListener(this);
		gvUserImageView = (GridView) PriUtils.getInstance().findViewById(view, R.id.gvImageUserAlbumView, PriHashMap.PriControl.NVBH_TIMKIEMHINHANH_HINHANH, PriUtils.CONTROL_GRIDVIEW);
		PriUtils.getInstance().setOnItemClickListener(gvUserImageView, this);

		tvNoDataResult = (TextView) view.findViewById(R.id.tvNoDataResult);
		tvNoDataResult.setVisibility(View.GONE);

		if (currentPage > 0) {
			spLine.setAdapter(adLine);
			spPrograme.setAdapter(adPrograme);
		} else {
//			adLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item,
//					Constants.getArrayLineChoose());
			adLine = new ArrayAdapter(parent, R.layout.simple_spinner_item, Constants.getArrayLineChoose());
			spLine.setAdapter(adLine);
			selectedLineIndex = Constants.getArrayLineChoose().length - 1;
			spLine.setSelection(selectedLineIndex);
			updateDataSpImage();
		}
		if (imageSearchDTO.isFirstInit) {
			gvUserImageView.setAdapter(thumbs);
			thumbs.notifyDataSetChanged();
			listDisplayPrograme = null;
			updateProgrameSpinner();
		} else {
			refreshData();
			imageSearchDTO.isFirstInit = true;
			imageSearchDTO.isUpdateProgame = true;
			getListAlbum(imageSearchDTO.isUpdateProgame);
		}

		return v;

	}

	/**
	 *
	 * Reset gia tri tren layout ve gia tri mac dinh
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	private void resetLayout() {
		edFromDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
				DateUtils.defaultDateFormat.toPattern()));
		edToDate.setText(DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		spLine.setSelection(Constants.getArrayLineChoose().length - 1);
		spPrograme.setSelection(0);
//		edCusCode.setText("");
		edCusName.setText("");
		spImage.setSelection(0);
	}

	/**
	 *
	 * cap nhat du lieu hinh anh
	 *
	 * @author: YenNTH
	 * @param modelData
	 * @return: void
	 * @throws:
	 */
	private void updateDataSpImage() {
		// TODO Auto-generated method stub
		if (listImage == null) {
			listImage = new ArrayList<DisplayProgrameItemDTO>();
			DisplayProgrameItemDTO itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_ALL);
			itemDTO.value = "";
			listImage.add(itemDTO);
			itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_SALE);
			itemDTO.value = String.valueOf(MediaItemDTO.TYPE_LOCATION_IMAGE);
			listImage.add(itemDTO);
			itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_CLOSE);
			itemDTO.value = String.valueOf(MediaItemDTO.TYPE_LOCATION_CLOSED);
			listImage.add(itemDTO);
			itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_TB_GENERATE);
			itemDTO.value = String.valueOf(MediaItemDTO.TYPE_DISPLAY_PROGAME_IMAGE);
			listImage.add(itemDTO);
			itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_LABLE_KEY_SHOP);
			itemDTO.value = String.valueOf(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
			listImage.add(itemDTO);
		}
		int lengthType = listImage.size();
		String typeName[] = new String[lengthType];
		// khoi tao gia tri cho loai hinh anh
		for (int i = 0; i < lengthType; i++) {
			DisplayProgrameItemDTO dto = listImage.get(i);
			typeName[i] = dto.name;
		}
//		SpinnerAdapter adImage = new SpinnerAdapter(parent,
//				R.layout.simple_spinner_item, typeName);
		adImage = new ArrayAdapter(parent, R.layout.simple_spinner_item, typeName);
		this.spImage.setAdapter(adImage);
		spImage.setSelection(selectedImageIndex);

	}

	/**
	 *
	 * cap nhat chuong trinh
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	private void updateProgrameSpinner() {
		if (listDisplayPrograme == null) {
			listDisplayPrograme = new ArrayList<DisplayProgrameItemDTO>();
			DisplayProgrameItemDTO itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_ALL);
			itemDTO.value = "";

			listDisplayPrograme.add(itemDTO);

			if (imageSearchDTO.listPrograme != null) {
				for (int i = 0, size = imageSearchDTO.listPrograme.size(); i < size; i++) {
					DisplayProgrameDTO programe = imageSearchDTO.listPrograme
							.get(i);

					DisplayProgrameItemDTO item = new DisplayProgrameItemDTO();
//					item.name = programe.displayProgrameCode;
					if (!StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& !StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameCode + " - " + programe.displayProgrameName;
					} else if (StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& !StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameName;
					} else if (!StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameCode;
					} else {
						item.name = "";
					}
					item.value = String.valueOf(programe.displayProgrameId);
					listDisplayPrograme.add(item);
				}
			}

			int numProgame = listDisplayPrograme.size();
			String[] programeName = new String[numProgame];
			for (int i = 0; i < numProgame; i++) {
				DisplayProgrameItemDTO item = listDisplayPrograme.get(i);

				programeName[i] = item.name;
			}

//			adPrograme = new SpinnerAdapter(parent,
//					R.layout.simple_spinner_item, programeName);
			adPrograme = new ArrayAdapter(parent, R.layout.simple_spinner_item, programeName);
			spPrograme.setAdapter(adPrograme);
			spPrograme.setSelection(0);
		}
	}

	/**
	 *
	 * cap nhat dayInOrder
	 *
	 * @author: YenNTH
	 * @param dayOfMonth
	 * @param monthOfYear
	 * @param year
	 * @return: void
	 * @throws:
	 */
	public void updateDate(int dayOfMonth, int monthOfYear, int year) {
		// TODO Auto-generated method stub
		String sDay = String.valueOf(dayOfMonth);
		String sMonth = String.valueOf(monthOfYear + 1);
		if (dayOfMonth < 10) {
			sDay = "0" + sDay;
		}
		if (monthOfYear + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		if (currentCalender == DATE_FROM_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				edFromDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				edFromDate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/")
						.append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent,
						StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
						null, false);
			}
		}
		if (currentCalender == DATE_TO_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				edToDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				edToDate.setText(new StringBuilder()
						// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/")
						.append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent,
						StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), 0,
						null, false);
			}
		}
	}

	/**
	 *
	 * refresh data
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	public void refreshData() {
		isAbleGetMore = true;
		numTopLoaded = 0;
		// loai hinh anh
		selectedImageIndex = spImage.getSelectedItemPosition();
		if (selectedImageIndex >= 0) {
			DisplayProgrameItemDTO dtoImage = listImage.get(selectedImageIndex);
			if (!StringUtil.isNullOrEmpty(dtoImage.value)) {
				imageSearchDTO.objectTypeImage = dtoImage.value;
			} else {
				imageSearchDTO.objectTypeImage = "";
			}
		}
		// loai chuong trinh
		selectedProgrameIndex = spPrograme.getSelectedItemPosition();
		if (selectedProgrameIndex >= 0) {
			DisplayProgrameItemDTO dtoProgame = listDisplayPrograme
					.get(selectedProgrameIndex);
			if (dtoProgame.value != "") {
				imageSearchDTO.programeId = dtoProgame.value;
			} else {
				imageSearchDTO.programeId = "";
			}
		}
		// tuyen
		selectedLineIndex = spLine.getSelectedItemPosition();

		// tu ngay
		GlobalUtil.forceHideKeyboard(parent);
		// luu lai gia tri de thuc hien tim kiem
		String dateTimePattern = StringUtil
				.getString(R.string.TEXT_DATE_TIME_PATTERN);
		Pattern pattern = Pattern.compile(dateTimePattern);
		if (!StringUtil.isNullOrEmpty(edFromDate.getText().toString())) {
			String strTN = edFromDate.getText().toString().trim();
			imageSearchDTO.fromDate = strTN;
			Matcher matcher = pattern.matcher(strTN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date tn = StringUtil.stringToDate(strTN, "");
					String strFindTN = StringUtil
							.dateToString(tn, "yyyy-MM-dd");

					imageSearchDTO.fromDateForRequest = strFindTN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			imageSearchDTO.fromDate = "";
			imageSearchDTO.fromDateForRequest = "";
		}
		// den ngay
		if (!StringUtil.isNullOrEmpty(edToDate.getText().toString())) {
			String strDN = edToDate.getText().toString().trim();
			imageSearchDTO.toDate = strDN;
			Matcher matcher = pattern.matcher(strDN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date dn = StringUtil.stringToDate(strDN, "");
					String strFindDN = StringUtil
							.dateToString(dn, "yyyy-MM-dd");

					imageSearchDTO.toDateForRequest = strFindDN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			imageSearchDTO.toDate = "";
			imageSearchDTO.toDateForRequest = "";
		}
		// ma kh
//		imageSearchDTO.customerCode = edCusCode.getText().toString().trim();
		// ten hoac dia chi
		imageSearchDTO.customerName = edCusName.getText().toString().trim();
	}

	/**
	 *
	 * lay danh sach hinh anh
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	private void getListAlbum(boolean isAll) {
		parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putBoolean(IntentConstants.INTENT_IS_ALL, isAll);
		data.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		if (!StringUtil.isNullOrEmpty(imageSearchDTO.customerCode)) {
			data.putString(IntentConstants.INTENT_CUSTOMER_CODE,
					imageSearchDTO.customerCode);
		}
		if (!StringUtil.isNullOrEmpty(imageSearchDTO.customerName)) {
			data.putString(IntentConstants.INTENT_CUSTOMER_NAME, StringUtil
					.getEngStringFromUnicodeString(imageSearchDTO.customerName));
		}
		data.putString(IntentConstants.INTENT_OBJECT_TYPE_IMAGE,
				imageSearchDTO.objectTypeImage);
		data.putString(IntentConstants.INTENT_VISIT_PLAN, DateUtils
				.getVisitPlan(Constants.getArrayLineChoose()[selectedLineIndex]));

		if (StringUtil.isNullOrEmpty(imageSearchDTO.objectTypeImage)
				|| imageSearchDTO.objectTypeImage.equals(String
						.valueOf(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE))) {
			data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID,
					imageSearchDTO.programeId);
		}
		data.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
				imageSearchDTO.fromDateForRequest);
		data.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE,
				imageSearchDTO.toDateForRequest);
		data.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE, NUM_LOAD_MORE);
		data.putInt(IntentConstants.INTENT_PAGE, numTopLoaded / NUM_LOAD_MORE);
		handleViewEventWithTag(data, ActionEventConstant.GET_IMAGE_LIST_SEARCH, SaleController.getInstance(), 0);
	}

	/**
	 *
	 * get more photo
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	public void getMorePhoto() {
		if (isAbleGetMore) {
			Bundle data = new Bundle();
			data.putString(
					IntentConstants.INTENT_STAFF_ID,
					String.valueOf(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritId()));
			data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo
					.getInstance().getProfile().getUserData().getInheritShopId());
			if (!StringUtil.isNullOrEmpty(imageSearchDTO.customerCode)) {
				data.putString(IntentConstants.INTENT_CUSTOMER_CODE,
						imageSearchDTO.customerCode);
			}
			if (!StringUtil.isNullOrEmpty(imageSearchDTO.customerName)) {
				data.putString(
						IntentConstants.INTENT_CUSTOMER_NAME,
						StringUtil
								.getEngStringFromUnicodeString(imageSearchDTO.customerName));
			}
			data.putString(IntentConstants.INTENT_OBJECT_TYPE_IMAGE,
					imageSearchDTO.objectTypeImage);
			data.putString(
					IntentConstants.INTENT_VISIT_PLAN,
					DateUtils
							.getVisitPlan(Constants.getArrayLineChoose()[selectedLineIndex]));
			data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID,
					imageSearchDTO.programeId);
			data.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
					imageSearchDTO.fromDateForRequest);
			data.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE,
					imageSearchDTO.toDateForRequest);
			data.putInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE,
					NUM_LOAD_MORE);
			data.putInt(IntentConstants.INTENT_PAGE, numTopLoaded
					/ NUM_LOAD_MORE);
			imageSearchDTO.isUpdateProgame = false;
			handleViewEventWithTag(data, ActionEventConstant.GET_IMAGE_LIST_SEARCH, SaleController.getInstance(), 1);
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_IMAGE_LIST_SEARCH:
			ImageSearchViewDTO tempModel = (ImageSearchViewDTO) modelEvent
					.getModelData();
			if (imageSearchDTO.isUpdateProgame) {
				imageSearchDTO.listPrograme.clear();
				imageSearchDTO.listPrograme.addAll(tempModel.listPrograme);
				updateProgrameSpinner();
			}
			if (modelEvent.getActionEvent().tag == 0) {
				imageSearchDTO.albumInfo.getListPhoto().clear();
				imageSearchDTO.albumInfo.getListPhoto().addAll(
						tempModel.albumInfo.getListPhoto());
				if (imageSearchDTO.albumInfo.getListPhoto().size() == 0) {
					tvNoDataResult.setVisibility(View.VISIBLE);
//					gvUserImageView.setVisibility(View.GONE);
					PriUtils.getInstance().setStatus(gvUserImageView, PriUtils.INVISIBLE);
				} else {
					tvNoDataResult.setVisibility(View.GONE);
//					gvUserImageView.setVisibility(View.VISIBLE);
					PriUtils.getInstance().setStatus(gvUserImageView, PriUtils.ENABLE, PriUtils.CONTROL_GRIDVIEW);
				}
				if (thumbs == null) {
					thumbs = new ImageAdapter(parent,
							imageSearchDTO.albumInfo.getListPhoto());
				}
				gvUserImageView.setAdapter(thumbs);
				thumbs.notifyDataSetChanged();

				int size = imageSearchDTO.albumInfo.getListPhoto().size();
				numTopLoaded = size;
				if (size < NUM_LOAD_MORE) {
					isAbleGetMore = false;
				}
			} else {
				if (tempModel.albumInfo.getListPhoto().size() < NUM_LOAD_MORE) {
					isAbleGetMore = false;
				}
				int size = tempModel.albumInfo.getListPhoto().size();
				numTopLoaded += size;
				imageSearchDTO.albumInfo.getListPhoto().addAll(
						tempModel.albumInfo.getListPhoto());

				Bundle bundle = new Bundle();
				bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO,
						imageSearchDTO.albumInfo);

				FullImageView orderFragment = (FullImageView)
						findFragmentByTag(GlobalUtil.getTag(FullImageView.class));
				if (orderFragment != null) {
					orderFragment.receiveBroadcast(
							ActionEventConstant.GET_RESULT_MORE_PHOTOS, bundle);
				}

				thumbs.notifyDataSetChanged();
			}
			parent.closeProgressDialog();
			break;

		default:
			super.handleModelViewEvent(modelEvent);
			parent.closeProgressDialog();
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	@Override
	public void onClick(View v) {
		if (v == btSearch) {
			if (compareDate()) {
				refreshData();
				imageSearchDTO.isUpdateProgame = false;
				getListAlbum(imageSearchDTO.isUpdateProgame);
			}
		} else if (v == btReInput) {
			resetLayout();
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {

	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {

	}

	/**
	 *
	 * reset gia tri khi nhan nhap lai
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	private void resetAllValue() {
		isAbleGetMore = true;
		numTopLoaded = 0;
		isSearch = false;
		selectedLineIndex = Constants.getArrayLineChoose().length - 1;
		selectedProgrameIndex = 0;
		imageSearchDTO.customerName = "";
		imageSearchDTO.customerCode = "";
		imageSearchDTO.programeId = "";
		currentPage = -1;
		listDisplayPrograme = null;
		resetLayout();
		imageSearchDTO.fromDate = DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern());
		imageSearchDTO.toDate = DateUtils.convertDateTimeWithFormat(
				DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern());
		String dateTimePattern = StringUtil
				.getString(R.string.TEXT_DATE_TIME_PATTERN);
		Pattern pattern = Pattern.compile(dateTimePattern);
		if (!StringUtil.isNullOrEmpty(edFromDate.getText().toString())) {
			String strTN = edFromDate.getText().toString().trim();
			imageSearchDTO.fromDate = strTN;
			Matcher matcher = pattern.matcher(strTN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date tn = StringUtil.stringToDate(strTN, "");
					String strFindTN = StringUtil
							.dateToString(tn, "yyyy-MM-dd");

					imageSearchDTO.fromDateForRequest = strFindTN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			imageSearchDTO.fromDate = "";
			imageSearchDTO.fromDateForRequest = "";
		}
		// den ngay
		if (!StringUtil.isNullOrEmpty(edToDate.getText().toString())) {
			String strDN = edToDate.getText().toString().trim();
			imageSearchDTO.toDate = strDN;
			Matcher matcher = pattern.matcher(strDN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date dn = StringUtil.stringToDate(strDN, "");
					String strFindDN = StringUtil
							.dateToString(dn, "yyyy-MM-dd");

					imageSearchDTO.toDateForRequest = strFindDN;
				} catch (Exception ex) {
					parent.showDialog(StringUtil
							.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		} else {
			imageSearchDTO.toDate = "";
			imageSearchDTO.toDateForRequest = "";
		}
		imageSearchDTO.objectTypeImage = "";
		imageSearchDTO.programeId = "";
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// cau request du lieu man hinh
				imageSearchDTO.isUpdateProgame = true;
				resetAllValue();
				getListAlbum(imageSearchDTO.isUpdateProgame);
			}
			break;
		case ActionEventConstant.GET_MORE_PHOTOS: {
			getMorePhoto();
			break;
		}
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spImage) {
			DisplayProgrameItemDTO itemDTO = listImage.get(arg2);
			if (itemDTO.value.equals(String
					.valueOf(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE))
					|| StringUtil.isNullOrEmpty(itemDTO.value)) {
				spPrograme.setEnabled(true);
				spPrograme.setSelection(0);
			} else {
				spPrograme.setEnabled(false);
				spPrograme.setSelection(0);
			}
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v == edFromDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_FROM_CONTROL;
				parent.showDatePickerDialog(edFromDate.getText().toString(), true, this);
			}
		}

		if (v == edToDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_TO_CONTROL;
				parent.showDatePickerDialog(edToDate.getText().toString(), true, this);
			}
		}
		return false;
	}

	/**
	 *
	 * chuyen toi MH hinh anh
	 *
	 * @author: YenNTH
	 * @return: void
	 * @throws:
	 */
	private void gotoImageListView() {
		handleSwitchFragment(new Bundle(), ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		if (eventType == ACTION_IMAGE) {
			gotoImageListView();
		}
		if (eventType == ACTION_DISPLAY) {//hien thi man hinh danh sach trung bay
			handleSwitchFragment(null, ActionEventConstant.GO_TO_DISPLAY_PROGRAM, UserController.getInstance());
		}
		if (eventType == ACTION_PROMOTION) {// den man hinh CTKM
			handleSwitchFragment(null,ActionEventConstant.GO_TO_PROMOTION_PROGRAM, UserController.getInstance());
		}
		if (eventType == ACTION_PRODUCT) {// den man hinh ds san pham
			handleSwitchFragment(null,ActionEventConstant.GO_TO_PRODUCT_LIST, SaleController.getInstance());
		}
	}

	public class ImageAdapter extends BaseAdapter {

		private ArrayList<PhotoDTO> list = new ArrayList<PhotoDTO>();

		public ImageAdapter(Context c, ArrayList<PhotoDTO> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			if (list != null) {
				return list.size();
			} else {
				return 0;
			}
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup pt) {
			View row = convertView;
			ViewHolder holder = null;

			if (convertView == null) {
				LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) parent)
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = layout.inflate(R.layout.layout_album_detail_user, pt, false);
				holder = new ViewHolder();
				holder.imageView = (ImageView) row
						.findViewById(R.id.imgAlbumImage);
				holder.staffName = (TextView) row
						.findViewById(R.id.tvStaffName);
				holder.staffName.setVisibility(View.VISIBLE);
				holder.titleAlbum = (TextView) row
						.findViewById(R.id.tvAlbumName);
				holder.titleAlbum.setTextColor(getResources().getColor(
						R.color.BLACK));
				holder.titleAlbum.setTypeface(null,
						android.graphics.Typeface.NORMAL);
				holder.titleAlbum.setTextSize(GlobalUtil.dip2Pixel(16));
				row.setTag(holder);
			} else {
				holder = (ViewHolder) row.getTag();
			}

			holder.imageView.setImageResource(R.drawable.album_default);
			if (!StringUtil.isNullOrEmpty(list.get(position).thumbUrl)) {
				if (list.get(position).thumbUrl
						.contains(ExternalStorage.SDCARD_PATH)){
					ImageUtil.loadImage(list.get(position).thumbUrl,
							holder.imageView);
				}
				else{
					ImageUtil.loadImage(ServerPath.IMAGE_PATH + list.get(position).thumbUrl,
							holder.imageView);
				}

			}

			if(list.get(position).customerName.length() > 15){
				holder.staffName.setText(list.get(position).customerCode + " - "
					+ list.get(position).customerName.substring(0, 13) + "...");
			}else{
				holder.staffName.setText(list.get(position).customerCode + " - "
						+ list.get(position).customerName);
			}
			holder.titleAlbum.setText(list.get(position).createdTime);
			if (position == numTopLoaded - 1) {
				getMorePhoto(); // lay them hinh anh
			}

			return row;
		}
	}

	public static class ViewHolder {
		public ImageView imageView;
		public ImageView imageViewBg;
		public TextView titleAlbum;
		public TextView staffName;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO,
				imageSearchDTO.albumInfo);
		bundle.putInt(IntentConstants.INTENT_ALBUM_INDEX_IMAGE, arg2);
		bundle.putString(IntentConstants.INTENT_FROM, GlobalUtil.getTag(ImageSearchNVBHView.class));
		handleSwitchFragment(bundle, ActionEventConstant.ACTION_LOAD_IMAGE_FULL, UserController.getInstance());

	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();

	}

	/**
	 * So sanh ngay
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public boolean compareDate() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date fromDate = new Date();
		Date toDate = new Date();
		try {
			fromDate = formatter.parse(edFromDate.getText().toString());
			toDate = formatter.parse(edToDate.getText().toString());

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}
		if (toDate.compareTo(fromDate) == -1) {
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_REMIND_SEARCH_IMAGE));
			return false;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		addMenuItem(PriForm.NVBH_TIMKIEMHINHANH,
				new MenuTab(R.string.TEXT_SEARCH_IMAGE,
						R.drawable.icon_image_search, ACTION_SEARCH_IMAGE, PriForm.NVBH_TIMKIEMHINHANH),
				new MenuTab(R.string.TEXT_HEADER_MENU_CTTB,R.drawable.menu_manage_icon,
						ACTION_DISPLAY, PriForm.NVBH_CTTB),
				new MenuTab(R.string.TEXT_CTKM,R.drawable.menu_promotion_icon,
						ACTION_PROMOTION, PriForm.NVBH_CTKM),
				new MenuTab(R.string.TEXT_PRODUCT,R.drawable.icon_product_list,
						ACTION_PRODUCT, PriForm.NVBH_DSSANPHAM));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}
}
