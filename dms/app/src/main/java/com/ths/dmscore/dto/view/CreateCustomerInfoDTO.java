/*
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Thong tin man hinh them moi Khach hang
 * NewCustomerListDTO.java
 * @author: duongdt3
 * @version: 1.0 
 * @since:  11:13:41 3 Jan 2014
 */
public class CreateCustomerInfoDTO implements Serializable {
	private static final long serialVersionUID = 916104348406749256L;
	public ArrayList<AreaItem> listProvine;
	public ArrayList<AreaItem> listDistrict;
	public ArrayList<AreaItem> listPrecinct;
	public ArrayList<CustomerType> listCusType;
	public CustomerDTO cusInfo;
	public long curentIdProvine;
	public long curentIdDistrict;
	public long curentIdPrecinct;
	public long curentIdTypePosition;
	public long curentIdType;
	// nguon lay hang
	public String orderSource;
	

	public CreateCustomerInfoDTO() {
		 curentIdProvine = 0;
		 curentIdDistrict = 0;
		 curentIdPrecinct = 0;
		 curentIdType = 0;
		 
		 currentIndexDistrict = -1;
		 currentIndexPrecinct = -1;
		 currentIndexProvince = -1;
		 currentIndexType = -1;
		 currentIndexTypePosition = -1;
		 orderSource = Constants.STR_BLANK;
	}
	
	public int currentIndexProvince;
	public void setCurrentProvince(int index){
		int size = listProvine.size();
		if (index >= 0 && index < size) {
			currentIndexProvince = index;
			curentIdProvine = listProvine.get(index).areaId;
		}else{
			currentIndexProvince = index;
			curentIdProvine = 0;
		}
	}
	
	public int currentIndexDistrict;
	public void setCurrentDistrict(int index){
		int size = listDistrict.size();
		if (index >= 0 && index < size) {
			currentIndexDistrict = index;
			curentIdDistrict = listDistrict.get(index).areaId;
		}else{
			currentIndexDistrict = -1;
			curentIdDistrict = 0;
		}
    }
	
	public int currentIndexPrecinct;
	public void setCurrentPrecinct(int index){
		int size = listPrecinct.size();
		if (index >= 0 && index < size) {
			currentIndexPrecinct = index;
			curentIdPrecinct = listPrecinct.get(index).areaId;
		} else {
			currentIndexPrecinct = -1;
			curentIdPrecinct = 0;
		}
    }
    
	public int currentIndexType;
	public int currentIndexTypePosition;
	public ArrayList<ApParamDTO> listTypePosition;
	public void setCurrentType(int index, long typeId){
		if (index >= 0 ) {
			currentIndexType = index;
			curentIdType = typeId;
		} else {
			currentIndexType = -1;
			curentIdType = 0;
		}
    }
	
	/**
	 * Thong tin tinh, quan/huyen, phuong/xa
	 * CreateCustomerInfoDTO.java
	 * @author: duongdt3
	 * @version: 1.0 
	 * @since:  09:32:02 6 Jan 2014
	 */
	public static class AreaItem implements Serializable {
		private static final long serialVersionUID = 1L;
		public String areaName;
		public long areaId;
		public long parrentId;
		
		public AreaItem() {
			areaName = "";
			areaId = 0;
			parrentId = 0;
		}

		/**
		 * @author: duongdt3
		 * @since: 11:55:13 6 Jan 2014
		 * @return: void
		 * @throws:  
		 * @param c
		 */
		public void initFromCursor(Cursor c) {
			areaId = CursorUtil.getLong(c, CUSTOMER_TABLE.AREA_ID);
			areaName = CursorUtil.getString(c, "AREA_NAME");
			parrentId = CursorUtil.getLong(c, "PARENT_AREA_ID");
		}
	}
	
	public static class CustomerType  implements Serializable {
		private static final long serialVersionUID = 1L;
		public String typeName;
		public int typeId;
		public int objectType;

		public CustomerType() {
			typeName = "";
			typeId = 0;
			objectType = 0;
		}

		/**
		 * init type customer from cursor
		 * @author: duongdt3
		 * @since: 10:19:10 6 Jan 2014
		 * @return: void
		 * @throws:  
		 * @param c
		 */
		public void initFromCursor(Cursor c) {
			this.typeId = CursorUtil.getInt(c, CUSTOMER_TABLE.CHANNEL_TYPE_ID);
			this.typeName = CursorUtil.getString(c, "CHANNEL_TYPE_NAME");
			objectType = CursorUtil.getInt(c, "OBJECT_TYPE");
		}
	}

	public void setCurrentTypePosition(int index) {
		int size = listTypePosition.size();
		if (index >= 0 && index < size) {
			currentIndexTypePosition = index;
			curentIdTypePosition = listTypePosition.get(index).getApParamId();
		} else {
			currentIndexTypePosition = -1;
			curentIdTypePosition = 0;
		}
	}
}
