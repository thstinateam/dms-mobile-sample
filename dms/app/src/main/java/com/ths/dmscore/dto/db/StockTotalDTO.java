/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.lib.sqllite.db.STOCK_TOTAL_TABLE;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Thong tin ton kho
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class StockTotalDTO extends AbstractTableDTO {
	// loai doi tuong: 1 doi tuong trong SHOP, 2 doi tuong trong STAFF, 3 CUSTOMER
	public static final int TYPE_SHOP = 1;
	public static final int TYPE_VANSALE = 2;
	public static final int TYPE_CUSTOMER = 3;
	
	private static final long serialVersionUID = 1L;
	// id ton kho
	public int stockTotalId;
	// id doi tuong ton kho
	public int objectId;
	// loai doi tuong: 1 doi tuong trong SHOP, 2 doi tuong trong STAFF, 3 doi
	// tuong CUSTOMER
	public int objectType;
	// id san pham
	public int productId;
	// mo ta
	public String descr;
	// so luong ton kho
	public int quantity;
	// so luong co the dat hang
	public int availableQuantity;
	// nguoi tao
	public String createUser;
	// nguoi cap nhat
	public String updateUser;
	// ngay tao
	public String createDate;
	// ngay cap nhat
	public String updateDate;
	//ton kho goc nhin ke toan
	public int approvedQuantity;
	//trang thai
	public int status;
	//ma kho
	public int warehouseId;
	//ma npp (truong hop NV valsale co nhieu NPP)
	public int shopId;

	
	
	
	public StockTotalDTO() {
		super(TableType.STOCK_TOTAL_TABLE);
	}
	
	/**
	 * Tao doi tuong ton kho
	 * 
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @param orderDetailDTO
	 * @return
	 * @return: StockTotalDTO
	 * @throws:
	 */

	public static StockTotalDTO createStockTotalInfo(OrderViewDTO dto, SaleOrderDetailDTO orderDetailDTO) {
		int shopId = dto.orderInfo.shopId;
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

		int objectType = 1;
		int objectId = 0;

		StockTotalDTO stockTotal = new StockTotalDTO();
		if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (dto.orderInfo.orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = staffId;
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}
		stockTotal.shopId = shopId;
		stockTotal.objectId = objectId;
		stockTotal.objectType = objectType;

		// stockTotal.objectType = 1;
		stockTotal.availableQuantity = (int)orderDetailDTO.quantity;
		stockTotal.quantity = (int)orderDetailDTO.quantity;
		stockTotal.productId = orderDetailDTO.productId;
		return stockTotal;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @param orderDetailDTO
	 * @return
	 * @return: StockTotalDTO
	 * @throws:
	*/
	public static StockTotalDTO createStockTotalInfoReturnOrder(SaleOrderDetailDTO orderDetailDTO) {
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

		StockTotalDTO stockTotal = new StockTotalDTO();

		stockTotal.objectId = staffId;
		stockTotal.objectType = StockTotalDTO.TYPE_VANSALE;

		// stockTotal.objectType = 1;
		stockTotal.availableQuantity = (int)orderDetailDTO.quantity;
		stockTotal.quantity = (int)orderDetailDTO.quantity;
		stockTotal.productId = orderDetailDTO.productId;
		stockTotal.shopId = orderDetailDTO.shopId;
		return stockTotal;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @param orderDetailDTO
	 * @return
	 * @return: StockTotalDTO
	 * @throws JSONException
	 * @throws:
	*/
	public static StockTotalDTO createStockTotalInfoFromJson(JSONArray orderDetail) throws JSONException {
		int staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();

		StockTotalDTO stockTotal = new StockTotalDTO();

		stockTotal.objectId = staffId;
		stockTotal.objectType = StockTotalDTO.TYPE_VANSALE;

		for (int i = 0; i < orderDetail.length(); i++) {
			JSONObject object = orderDetail.getJSONObject(i);
			if (object.getString("column").equals("QUANTITY")) {
				stockTotal.availableQuantity = object.getInt("value");
				stockTotal.quantity = object.getInt("value");
			}
			if (object.getString("column").equals("PRODUCT_ID")) {
				stockTotal.productId = object.getInt("value");
			}
		}
		return stockTotal;
	}

	/**
	 * Generate cau lenh update
	 * 
	 * @author: TruongHN
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateUpdateFromOrderSql() {
		JSONObject json = new JSONObject();
		try {
			// UPDATE STOCK_TOTAL SET STOCK_TOTAL.QUANTITY =
			// STOCK_TOTAL.QUANTITY - :11,STOCK_TOTAL.AVAILABLE_QUANTITY =
			// STOCK_TOTAL.AVAILABLE_QUANTITY -
			// :12 WHERE STOCK_TOTAL.OWNER_ID =:2 and STOCK_TOTAL.OWNER_TYPE =:
			// 3 and STOCK_TOTAL.PRODUCT_ID =: 4
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, STOCK_TOTAL_TABLE.TABLE_STOCK_TOTAL);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.QUANTITY, "*-" + this.quantity,
					DATA_TYPE.OPERATION.toString()));
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY, "*-" + this.quantity,
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_ID, this.objectId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_TYPE, this.objectType, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.PRODUCT_ID, this.productId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * 
	 * Giam available_quantity cua stock total Presale
	 * 
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	/*public JSONObject generateDescreaseSqlPresale() {
		JSONObject json = new JSONObject();
		try {
			// UPDATE STOCK_TOTAL SET STOCK_TOTAL.QUANTITY =
			// STOCK_TOTAL.QUANTITY - :11,STOCK_TOTAL.AVAILABLE_QUANTITY =
			// STOCK_TOTAL.AVAILABLE_QUANTITY -
			// :12 WHERE STOCK_TOTAL.OWNER_ID =:2 and STOCK_TOTAL.OWNER_TYPE =:
			// 3 and STOCK_TOTAL.PRODUCT_ID =: 4
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, STOCK_TOTAL_TABLE.TABLE_STOCK_TOTAL);

			// ds params
			JSONArray params = new JSONArray();
//			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.QUANTITY, "*-" + this.quantity,
//					DATA_TYPE.OPERATION.toString()));
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY, "*-" + this.availableQuantity,
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_ID, this.objectId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_TYPE, this.objectType, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.PRODUCT_ID, this.productId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
						 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}*/
	
	/**
	 * 
	 * Giam quantity & available_quantity cua stock total Vansale
	 * 
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateDescreaseSqlVansale() {
		JSONObject json = new JSONObject();
		try {
			// UPDATE STOCK_TOTAL SET STOCK_TOTAL.QUANTITY =
			// STOCK_TOTAL.QUANTITY - :11,STOCK_TOTAL.AVAILABLE_QUANTITY =
			// STOCK_TOTAL.AVAILABLE_QUANTITY -
			// :12 WHERE STOCK_TOTAL.OWNER_ID =:2 and STOCK_TOTAL.OWNER_TYPE =:
			// 3 and STOCK_TOTAL.PRODUCT_ID =: 4
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, STOCK_TOTAL_TABLE.TABLE_STOCK_TOTAL);
			
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.QUANTITY, "*-" + this.quantity,
					DATA_TYPE.OPERATION.toString()));
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY, "*-" + this.availableQuantity,
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
			
			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.SHOP_ID, this.shopId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_ID, this.objectId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_TYPE, this.objectType, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.PRODUCT_ID, this.productId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
			
		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * 
	 * Tang quantity cua stock total
	 * 
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateIncreaseSql() {
		JSONObject json = new JSONObject();
		try {
			// UPDATE STOCK_TOTAL SET STOCK_TOTAL.QUANTITY =
			// STOCK_TOTAL.QUANTITY - :11,STOCK_TOTAL.AVAILABLE_QUANTITY =
			// STOCK_TOTAL.AVAILABLE_QUANTITY -
			// :12 WHERE STOCK_TOTAL.OWNER_ID =:2 and STOCK_TOTAL.OWNER_TYPE =:
			// 3 and STOCK_TOTAL.PRODUCT_ID =: 4
			json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, STOCK_TOTAL_TABLE.TABLE_STOCK_TOTAL);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.QUANTITY, "*+" + this.quantity,
					DATA_TYPE.OPERATION.toString()));
			params.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY, "*+" + this.availableQuantity,
					DATA_TYPE.OPERATION.toString()));
			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_ID, this.objectId, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.OBJECT_TYPE, this.objectType, null));
			wheres.put(GlobalUtil.getJsonColumn(STOCK_TOTAL_TABLE.PRODUCT_ID, this.productId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}
}
