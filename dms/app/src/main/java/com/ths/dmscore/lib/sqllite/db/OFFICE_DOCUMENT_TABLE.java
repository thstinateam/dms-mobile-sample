/**
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.OfficeDocumentDTO;
import com.ths.dmscore.dto.view.OfficeDocumentListDTO;
import com.ths.dmscore.util.StringUtil;

/**
 * 
 * luu thong tin cong van
 * 
 * @author: YenNTH
 * @version: 1.0
 * @since: 1.0
 */
public class OFFICE_DOCUMENT_TABLE extends ABSTRACT_TABLE {
	// id cong van
	public static final String OFFICE_DOCUMENT_ID = "OFFICE_DOCUMENT_ID";
	// ma cong van
	public static final String DOC_CODE = "DOC_CODE";
	// ten cong van
	public static final String DOC_NAME = "DOC_NAME";
	// loai cong van
	public static final String TYPE = "TYPE";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// from dayInOrder
	public static final String FROM_DATE = "FROM_DATE";
	// to dayInOrder
	public static final String TO_DATE = "TO_DATE";
	//status
	public static final String STATUS = "STATUS";

	public static final String OFFICE_DOCUMENT_TABLE = "OFFICE_DOCUMENT";

	public OFFICE_DOCUMENT_TABLE(SQLiteDatabase mDB) {
		this.tableName = OFFICE_DOCUMENT_TABLE;
		this.columns = new String[] { OFFICE_DOCUMENT_ID, DOC_CODE, DOC_NAME,
				TYPE, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER,
				FROM_DATE, TO_DATE, STATUS, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	public ContentValues initDocumentData(OfficeDocumentDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(OFFICE_DOCUMENT_ID, dto.officeDocumentId);
		editedValues.put(DOC_CODE, dto.docCode);
		editedValues.put(DOC_NAME, dto.docName);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(FROM_DATE, dto.fromDate);
		editedValues.put(TO_DATE, dto.toDate);
		return editedValues;
	}

	/**
	 * 
	 * lay danh sach cong van NVBH, GSNPP
	 * 
	 * @author: YenNTH
	 * @param type
	 * @param fromDate
	 * @param toDate
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: OfficeDocumentListDTO
	 * @throws:
	 */
	public OfficeDocumentListDTO getListOfficeDocument(int type,
													   String fromDate, String toDate, String ext, boolean checkLoadMore, String shopId) {
		OfficeDocumentListDTO officeDocumentDTO = new OfficeDocumentListDTO();
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT distinct OF.office_document_id              AS OFFICE_DOCUMENT_ID, ");
		sql.append("       doc_code                        AS DOC_CODE, ");
		sql.append("       doc_name                        AS DOC_NAME, ");
		sql.append("       OF.type                         AS TYPE, ");
		sql.append("       Strftime('%d/%m/%Y', from_date) AS FROM_DATE, ");
		sql.append("       Strftime('%d/%m/%Y', to_date)   AS TO_DATE, ");
		sql.append("       MI.[url] ");
		sql.append("FROM   OFFICE_DOC_SHOP_MAP s, ");
		sql.append("  	   office_document OF ");
		sql.append("       LEFT JOIN media_item MI ");
		sql.append("              ON MI.object_type = 6 ");
		sql.append("                 AND MI.object_id = OF.OFFICE_DOCUMENT_ID AND MI.STATUS = 1 ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND OF.status = 1  ");
		sql.append("       AND s.STATUS = 1   ");
		sql.append("       AND s.OFFICE_DOCUMENT_ID= OF.OFFICE_DOCUMENT_ID ");
		sql.append("       AND s.SHOP_ID = ?   ");
		params.add("" + shopId);
		if (type > 0) {
			sql.append("    AND  OF.type = ? ");
			params.add("" + type);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sql.append("    AND ( to_date IS NULL OR  DATE(to_date) >= DATE(?) ) ");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sql.append("    AND  DATE(from_date) <= DATE(?) ");
			params.add(toDate);
		}
		sql.append(" ORDER BY type asc, datetime(from_date) desc, doc_code asc, doc_name asc  ");

		String query = sql.toString();
		String count = " SELECT COUNT(*) AS TOTAL_ROW FROM (" + query + ") ";
		Cursor c = null;
		String[] param = new String[params.size()];
		for (int i = 0, length = params.size(); i < length; i++) {
			param[i] = params.get(i);
		}
		Cursor cTmp = null;
		try {
			// get total row first
			if (checkLoadMore == false) {
				cTmp = rawQuery(count, param);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				officeDocumentDTO.setTotal(total);
			}
			// end
			c = rawQuery(query + ext, param);

			if (c != null) {

				if (c.moveToFirst()) {
					ArrayList<OfficeDocumentDTO> listOfficeDocument = new ArrayList<OfficeDocumentDTO>();
					do {
						OfficeDocumentDTO note = new OfficeDocumentDTO();
						note.initDocumentData(c);
						listOfficeDocument.add(note);
					} while (c.moveToNext());
					officeDocumentDTO.list = listOfficeDocument;
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
			}
		}

		return officeDocumentDTO;
	}
	

	/**
	 * 
	 * lay danh sach cong van TBHV
	 * 
	 * @author: YenNTH
	 * @param type
	 * @param fromDate
	 * @param toDate
	 * @param ext
	 * @param checkLoadMore
	 * @return
	 * @return: OfficeDocumentListDTO
	 * @throws:
	 */
	public OfficeDocumentListDTO getListOfficeDocumentTBHV(int type,
			String fromDate, String toDate, String ext, boolean checkLoadMore, String shopId) {
		OfficeDocumentListDTO officeDocumentDTO = new OfficeDocumentListDTO();
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT distinct OF.office_document_id              AS OFFICE_DOCUMENT_ID, ");
		sql.append("       doc_code                        AS DOC_CODE, ");
		sql.append("       doc_name                        AS DOC_NAME, ");
		sql.append("       OF.type                         AS TYPE, ");
		sql.append("       Strftime('%d/%m/%Y', from_date) AS FROM_DATE, ");
		sql.append("       Strftime('%d/%m/%Y', to_date)   AS TO_DATE, ");
		sql.append("       MI.[url] ");
		sql.append("FROM   OFFICE_DOC_SHOP_MAP s, ");
		sql.append("  	   office_document OF ");
		sql.append("       LEFT JOIN media_item MI ");
		sql.append("              ON MI.object_type = 6 ");
		sql.append("                 AND MI.object_id = OF.OFFICE_DOCUMENT_ID AND MI.STATUS = 1 ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND OF.status = 1  ");
		sql.append("       AND s.STATUS = 1   ");
		sql.append("       AND s.OFFICE_DOCUMENT_ID= OF.OFFICE_DOCUMENT_ID ");
		sql.append("       AND s.shop_id in (select shop_id from shop where parent_shop_id = ?)   ");
		params.add("" + shopId);
		if (type > 0) {
			sql.append("    AND  OF.type = ? ");
			params.add("" + type);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sql.append("    AND ( to_date IS NULL OR  DATE(to_date) >= DATE(?) ) ");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sql.append("    AND  DATE(from_date) <= DATE(?) ");
			params.add(toDate);
		}
		sql.append(" ORDER BY datetime(from_date) desc, datetime(to_date) desc, doc_code asc  ");

		String query = sql.toString();
		String count = " SELECT COUNT(*) AS TOTAL_ROW FROM (" + query + ") ";
		Cursor c = null;
		String[] param = new String[params.size()];
		for (int i = 0, length = params.size(); i < length; i++) {
			param[i] = params.get(i);
		}
		Cursor cTmp = null;
		try {
			// get total row first
			if (checkLoadMore == false) {
				cTmp = rawQuery(count, param);
				int total = 0;
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
				}
				officeDocumentDTO.setTotal(total);
			}
			// end
			c = rawQuery(query + ext, param);

			if (c != null) {

				if (c.moveToFirst()) {
					ArrayList<OfficeDocumentDTO> listOfficeDocument = new ArrayList<OfficeDocumentDTO>();
					do {
						OfficeDocumentDTO note = new OfficeDocumentDTO();
						note.initDocumentData(c);
						listOfficeDocument.add(note);
					} while (c.moveToNext());
					officeDocumentDTO.list = listOfficeDocument;
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
			}
		}

		return officeDocumentDTO;
	}

}
