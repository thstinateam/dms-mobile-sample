package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.StaffPositionLogDTO;
import com.ths.dmscore.dto.map.GeomDTO;
import com.ths.dmscore.dto.view.StaffPositionDTO;
import com.ths.dmscore.dto.view.TBHVAttendanceDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;

/**
 * Table ghi vi tri NVBH
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class STAFF_POSITION_LOG_TABLE extends ABSTRACT_TABLE {
	// id
	public static final String STAFF_POSITION_ID = "STAFF_POSITION_LOG_ID";
	// staff_id
	public static final String STAFF_ID = "STAFF_ID";
	//shop id
	public static final String SHOP_ID = "SHOP_ID";
	// toa do lat
	public static final String LAT = "LAT";
	// toa do long
	public static final String LNG = "LNG";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// do chinh xac
	public static final String ACCURACY = "ACCURACY";
	public static final String DISTANCE = "DISTANCE";

	public static final String PIN = "PIN";
	public static final String NETWORK_TYPE = "NETWORK_TYPE";
	public static final String SPEED = "SPEED";
	public static final String STAFF_POSITION_LOG_TABLE = "STAFF_POSITION_LOG";

	public STAFF_POSITION_LOG_TABLE(SQLiteDatabase mDB) {
		this.tableName = STAFF_POSITION_LOG_TABLE;
		this.columns = new String[] { STAFF_POSITION_ID, STAFF_ID, LAT, LNG, CREATE_DATE,DISTANCE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	public StaffPositionDTO getListLog(String startTime) {
		String[] params = {};
		StringBuilder sqlRequest = new StringBuilder();
		sqlRequest.append("SELECT ID AS STAFF_POSITION_LOG_ID, ");
		sqlRequest.append("       STAFF_ID AS STAFF_ID, ");
		sqlRequest.append("       CREATE_DATE AS CREATE_DATE, ");
		sqlRequest.append("       LAT AS LAT, ");
		sqlRequest.append("       LNG AS LNG, ");
		sqlRequest.append("       ACCURACY AS ACCURACY ");
		sqlRequest.append("FROM   STAFF_POSITION_LOG ");
		sqlRequest.append("WHERE  STRFTIME('%s', CREATE_DATE) >= STRFTIME('%s', '" + startTime+ "' ) ");
		sqlRequest.append("ORDER BY DateTime(CREATE_DATE) DESC");

		StaffPositionDTO temp = new StaffPositionDTO();
		Cursor c = null;
		try {
			c = rawQuery(sqlRequest.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						StaffPositionLogDTO dto = (new StaffPositionLogDTO()).initFromCursor(c);
						temp.itemList.add(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getListLog", "fail", e);
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}

		return temp;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}


	public long deleteOldPositionLog(){
		long result = -1;
		try {
			mDB.beginTransaction();
			StringBuffer sqlDel = new StringBuffer();
			sqlDel.append("  substr(create_date,0,11) < dayInOrder('now', '-1 fullDate') ");

			delete(sqlDel.toString(), null);
			result = 1;
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("deleteOldPositionLog", "fail", e);
			result = -1;
		} finally {
			mDB.endTransaction();
		}
		return result;
	}


	@SuppressWarnings("unused")
	private ContentValues initDataRow(StaffPositionLogDTO log){
		ContentValues editedValues = new ContentValues();
		editedValues.put(STAFF_POSITION_ID, log.id);
		editedValues.put(STAFF_ID, log.staffId);
		editedValues.put(LAT, log.lat);
		editedValues.put(LNG, log.lng);
		editedValues.put(ACCURACY, log.accuracy);
		editedValues.put(CREATE_DATE, log.createDate);
		return editedValues;
	}

	/**
	 *
	 * lay thong tin bao cao cham cong ngay cua TBHV
	 *
	 * @param data
	 * @return
	 * @return: ArrayList<ReportTakeAttendOfDateDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @throws Exception
	 * @date: Jan 22, 2013
	 */
	public TBHVAttendanceDTO getTBHVAttendance(Bundle bundle) throws Exception {
		TBHVAttendanceDTO dto = new TBHVAttendanceDTO();
		String shopId = bundle.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = bundle.getString(IntentConstants.INTENT_STAFF_ID);

//		// lay ds nhung nhan vien o duoi staffId truyen vao mot cap
//		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
////		String listGs = staffTable.getListStaffOfSupervisor(staffId, shopId);
//		String listGs = staffTable.getListStaffManagerOfChildShop(staffId, shopId);
//		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
//		// lay shop con
//		ArrayList<String> shopListChild = staffTable.getListShopChild(shopId);
////		ArrayList<String> shopListChild = shopTable.getShopRecursiveReverse(shopId);
//		String shopChildStr = Constants.STR_BLANK;
//		if(shopListChild != null && shopListChild.size() > 0) {
//			shopChildStr = TextUtils.join(",", shopListChild);
//		} else if(shopListChild == null || shopListChild.size() == 0) {
//			shopChildStr = shopId;
//		}
//		// lay shop chau
//		ArrayList<String> shopListGrandChild = shopTable.getGrandShopReverse(shopId);
//		String shopGrandChildStr = TextUtils.join(",", shopListGrandChild);
//
//		StringBuffer shopParamSql = new StringBuffer();
//		shopParamSql.append("SELECT * FROM SHOP_PARAM WHERE SHOP_ID = ? AND TYPE LIKE '%CC%' AND STATUS = 1");
//		Cursor cScore = null;
//		try {
//			cScore = rawQuery(shopParamSql.toString(), new String[] { String.valueOf(shopId) });
//			if (cScore != null) {
//				if (cScore.moveToFirst()) {
//					do {
//						if (dto != null) {
//							ShopParamDTO item = new ShopParamDTO();
//							item.initObjectWithCursor(cScore);
//							if(item.code.equals("CC_DISTANCE")){
//								try {
//									double kc = Double.parseDouble(item.value);
//									if (kc>=0) {
//										dto.listParam.put(item.value, item);
//									}
//								} catch (Exception e) {
//									// TODO: handle exception
//								}
//							} else if (!StringUtil.isNullOrEmpty(item.value)
//									&& !StringUtil.isNullOrEmpty(item.name)) {
//								dto.listParam.put(item.value, item);
//							}
//						}
//					} while (cScore.moveToNext());
//				}
//			}
//		} catch (Exception e) {
//		} finally {
//			if (cScore != null) {
//				cScore.close();
//			}
//		}
//		StringBuffer  sql = new StringBuffer();
//		sql.append(" SELECT * ");
//		sql.append(" FROM (SELECT GS.STAFF_ID          AS GS_STAFF_ID, ");
//		sql.append("              GS.STAFF_CODE        AS GS_STAFF_CODE, ");
//		sql.append("              GS.STAFF_NAME        AS GS_STAFF_NAME, ");
//		sql.append("              SH.SHOP_ID           AS NVBH_SHOP_ID, ");
//		sql.append("              SH.SHOP_CODE         AS NVBH_SHOP_CODE, ");
//		sql.append("              SH.SHOP_NAME         AS NVBH_SHOP_NAME, ");
//		sql.append("              SH.LAT               AS NVBH_SHOP_LAT, ");
//		sql.append("              SH.LNG               AS NVBH_SHOP_LNG, ");
//		sql.append("              GS_CH.OBJECT_TYPE    AS GS_OBJECT_TYPE ");
////		sql.append("              ,COUNT(NVBH.STAFF_ID) AS NUM_NVBH ");
//		sql.append("       FROM  ");
//		sql.append("            SHOP SH, ");
//		sql.append("            STAFF GS, ");
//		sql.append("            CHANNEL_TYPE GS_CH ");
//		sql.append("            , ORG_TEMP OT ");
//		sql.append("       WHERE GS.STAFF_TYPE_ID = GS_CH.CHANNEL_TYPE_ID ");
//		sql.append("             AND OT.SHOP_ID = SH.SHOP_ID  ");
//		sql.append("             AND OT.STAFF_ID = GS.STAFF_ID  ");
//		sql.append("             AND GS_CH.TYPE = 2 ");
////		sql.append("             AND NVBH.STATUS = 1 ");
//		sql.append("             AND SH.STATUS = 1 ");
////		sql.append("             AND SH.SHOP_ID IN (" + shopChildStr + ") ");
//		sql.append("             AND GS.STAFF_ID IN ( "+ listGs + ") ");
//		sql.append("             AND GS.STAFF_TYPE_ID = GS_CH.CHANNEL_TYPE_ID ");
//		sql.append("             AND GS_CH.TYPE = 2 ");
//		sql.append("             AND GS.STATUS = 1 ");
//		sql.append("       GROUP BY SH.SHOP_ID, ");
//		sql.append("                GS.STAFF_ID ");
//		sql.append("       ORDER  BY SH.SHOP_CODE, ");
//		sql.append("                 GS.STAFF_NAME) NPP_LIST");


		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listGs = staff.getListStaffManagerOfChildShop(staffId, shopId);
		ArrayList<String> listShopID = staff.getListShopChild(shopId);
		String res = TextUtils.join(",", listShopID);
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * ");
		sql.append("FROM   (SELECT GS.STAFF_ID          AS GS_STAFF_ID, ");
		sql.append("               GS.STAFF_CODE        AS GS_STAFF_CODE, ");
		sql.append("               GS.STAFF_NAME        AS GS_STAFF_NAME, ");
		sql.append("               SH.SHOP_ID           AS NVBH_SHOP_ID, ");
		sql.append("               SH.SHOP_CODE         AS NVBH_SHOP_CODE, ");
		sql.append("               SH.SHOP_NAME         AS NVBH_SHOP_NAME, ");
		sql.append("               SH.LAT               AS NVBH_SHOP_LAT, ");
		sql.append("               SH.LNG               AS NVBH_SHOP_LNG ");
		sql.append("        FROM   ORG_TEMP OT, ");
		sql.append("               SHOP SH, ");
		sql.append("               STAFF_TYPE ST, ");
		sql.append("               STAFF GS ");
		sql.append("        WHERE  1 = 1 ");
		sql.append("               AND SH.STATUS = 1 ");
		sql.append("           	AND OT.SHOP_ID = SH.SHOP_ID ");
		sql.append("           	AND OT.STAFF_ID = GS.STAFF_ID ");
		sql.append("       		AND ST.STATUS = 1 ");
		sql.append("       		AND ST.STAFF_TYPE_ID = GS.STAFF_TYPE_ID ");
		sql.append("               AND GS.STAFF_ID IN ( ");
		sql.append(listGs);
		sql.append("               ) ");
		sql.append("               AND SH.SHOP_ID IN ( ");
		sql.append(res);
		sql.append("               ) ");
		//-- ket thuc lay nhan vien con
		sql.append("               AND GS.STATUS = 1 ");
		sql.append("        GROUP  BY SH.SHOP_ID, ");
		sql.append("                  GS.STAFF_ID ");
		sql.append("        ORDER  BY SH.SHOP_CODE, ");
		sql.append("                  GS.STAFF_NAME) NPP_LIST ");
		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), null);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						TBHVAttendanceDTO.TVBHAttendanceItem item = dto.newTVBHAttendanceItem();
						item.initObjectWithCursor(c);
						dto.arrGsnppList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
			}
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}

		return dto;
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: TamPQ
	 * @param staffId
	 * @return
	 * @return: StaffDTOvoid
	 * @throws Exception 
	 * @throws:
	 */
	public ArrayList<GeomDTO> getPosition(int staffId) throws Exception {
		ArrayList<GeomDTO> geom = new ArrayList<GeomDTO>();
		StringBuffer sql = new StringBuffer();
		ArrayList<String> paArrayList = new ArrayList<String>();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		sql.append("SELECT STAFF_ID, LAT,LNG, STRFTIME('%H:%M', CREATE_TIME) CREATE_TIME, TYPE, ACCURACY ");
		sql.append("FROM ");
		sql.append("(SELECT SPL.STAFF_ID, SPL.LAT, SPL.LNG, SPL.CREATE_DATE AS CREATE_TIME, 1 TYPE, ACCURACY ");
		sql.append(" 	FROM STAFF_POSITION_LOG SPL ");
		sql.append("	WHERE SPL.STAFF_ID = ?  ");
		paArrayList.add("" + staffId);
		sql.append("	AND substr(SPL.CREATE_DATE, 1, 10) = ? ");
		paArrayList.add(date_now);
		sql.append("	UNION ALL ");
		sql.append("SELECT AL.STAFF_ID, AL.LAT, AL.LNG, AL.start_time AS CREATE_TIME, 2 TYPE, 1 ACCURACY ");
		sql.append(" 	FROM  ACTION_LOG AL ");
		sql.append("	WHERE AL.STAFF_ID = ?  ");
		paArrayList.add("" + staffId);
		sql.append("	and al.object_type in (0, 1) AND substr(al.start_time, 1, 10) = ?)");
		paArrayList.add(date_now);
		sql.append("	WHERE 1=1 and LAT > 0 and LNG > 0 ");
		sql.append("	ORDER BY STAFF_ID, CREATE_TIME  ");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), paArrayList);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						GeomDTO temp = new GeomDTO();
						temp.createDate = CursorUtil.getString(c, "CREATE_TIME");
						temp.lat = CursorUtil.getDouble(c, "LAT");
						temp.lng = CursorUtil.getDouble(c, "LNG");
						temp.type = CursorUtil.getInt(c, "TYPE");
						temp.accuracy = CursorUtil.getDouble(c, "ACCURACY");
						geom.add(temp);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getPosition", "fail", e);
			throw e;
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
				}
			}
		}
		return geom;
	}

		/**
	 * Lay log position gan fullDate nhat dong bo len server
	 * @author: 
	 * @return
	 */
	public StaffPositionLogDTO getLastPosition(){
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		StaffPositionLogDTO lastLogPosition = null;
		String[] params = { dateNow, GlobalInfo.getInstance().getProfile().getUserData().getInheritId() + ""};
		StringBuffer  sql = new StringBuffer();
		sql.append("SELECT STAFF_POSITION_LOG_ID 			AS STAFF_POSITION_LOG_ID, "); 
		sql.append("       STAFF_ID 						AS STAFF_ID, "); 
		sql.append("       STRFTIME('%H:%M', CREATE_DATE) 	AS CREATE_DATE, ");  
		sql.append("       LAT 								AS LAT, "); 
		sql.append("       LNG 								AS LNG, "); 
		sql.append("       ACCURACY 						AS ACCURACY "); 
		sql.append("FROM   STAFF_POSITION_LOG ");
		sql.append("WHERE substr(CREATE_DATE, 1, 10) >= ? ");
		sql.append("AND STAFF_ID = ? ");
		sql.append("ORDER BY CREATE_DATE DESC ");
		sql.append("LIMIT 1");
		
		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
						lastLogPosition = (new StaffPositionLogDTO()).initFromCursor(c);
				}
			}
		} catch (Exception e) {
			MyLog.e("getLastPosition", "fail", e);
		} finally {
			if (c != null) {
				try {
					c.close();
				} catch (Exception e2) {
					// TODO: handle exception
				}
			}
		}
		return lastLogPosition;
	}
}
