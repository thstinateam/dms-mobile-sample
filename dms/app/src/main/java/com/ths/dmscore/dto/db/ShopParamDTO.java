/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.SHOP_PARAM_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin cua shop param
 * 
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class ShopParamDTO extends AbstractTableDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// id cua bang
	public int shopPramId;
	// id cua shop tuong ung
	public int shopId;
	// type cua param
	public String type;
	// code cua param
	public String code;
	// name cua param
	public String name;
	// description cua param
	public String description;
	// status cua param (1: active, 0: off)
	public int status;
	// gia tri
	public String value;
	// create dayInOrder
	public String createDate;
	// create use
	public String createUser;
	// update dayInOrder
	public String updateDate;
	// update user
	public String updateUser;

	public ShopParamDTO() {
		super(TableType.SHOP_PARAM_TABLE);
	}

	/**
	 * 
	 * init object with cursor
	 * 
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 * @since: Mar 8, 2013
	 */
	public void initObjectWithCursor(Cursor c) {
		this.shopPramId = CursorUtil.getInt(c, SHOP_PARAM_TABLE.SHOP_PARAM_ID);
		this.shopId = CursorUtil.getInt(c, SHOP_PARAM_TABLE.SHOP_ID);
		this.type = CursorUtil.getString(c, SHOP_PARAM_TABLE.TYPE);
		this.code = CursorUtil.getString(c, SHOP_PARAM_TABLE.CODE);
		this.name = CursorUtil.getString(c, SHOP_PARAM_TABLE.NAME);
		this.description = CursorUtil.getString(c, SHOP_PARAM_TABLE.DESCRIPTION);
		this.status = CursorUtil.getInt(c, SHOP_PARAM_TABLE.STATUS);
		this.value = CursorUtil.getString(c, SHOP_PARAM_TABLE.VALUE);
		this.createDate = CursorUtil.getString(c, SHOP_PARAM_TABLE.CREATE_DATE);
		this.createUser = CursorUtil.getString(c, SHOP_PARAM_TABLE.CREATE_USER);
		this.updateDate = CursorUtil.getString(c, SHOP_PARAM_TABLE.UPDATE_DATE);
		this.updateUser = CursorUtil.getString(c, SHOP_PARAM_TABLE.UPDATE_USER);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.type + " " + value + " " + shopId + " " + status;
	}
}
