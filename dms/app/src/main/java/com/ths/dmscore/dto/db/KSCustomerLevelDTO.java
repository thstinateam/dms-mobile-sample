package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.KS_CUSTOMER_LEVEL_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * DTO cho table ks_customer_level
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KSCustomerLevelDTO extends AbstractTableDTO {

	// noi dung field
	private static final long serialVersionUID = 1L;
	public long ksCustomerLevelId;
	public long ksCustomerId;
	public long ksLevelId;
	public long multiplier;
	public long status;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public long createUserId;
	public long ksId; // ksId
	public long shopId; // shopIds

	@Override
	public Object clone(){
		// TODO Auto-generated method stub
		KSCustomerLevelDTO object = new KSCustomerLevelDTO();
		object.ksCustomerLevelId = ksCustomerLevelId;
		object.ksCustomerId = ksCustomerId;
		object.ksLevelId = ksLevelId;
		object.multiplier = multiplier;
		object.status = status;
		object.createDate = createDate;
		object.createUser = createUser;
		object.updateDate = updateDate;
		object.updateUser = updateUser;
		object.createUserId = createUserId;
		object.ksId = ksId;
		object.shopId = shopId;
		return object;
	}

	 /**
	 * gen sql insert or update
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERTORUPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, KS_CUSTOMER_LEVEL_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.KS_CUSTOMER_LEVEL_ID, ksCustomerLevelId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.KS_CUSTOMER_ID, ksCustomerId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.KS_LEVEL_ID, ksLevelId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.MULTIPLIER, multiplier, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.KS_ID, ksId, null));
			detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.SHOP_ID, shopId, null));
			if(createUserId > 0)
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.CREATE_USER_ID, createUserId, null));
			if(!StringUtil.isNullOrEmpty(createDate)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.CREATE_DATE, createDate, null));
			}
			if(!StringUtil.isNullOrEmpty(createUser)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.CREATE_USER, createUser, null));
			}
			if(!StringUtil.isNullOrEmpty(updateDate)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.UPDATE_DATE, updateDate, null));
			}
			if(!StringUtil.isNullOrEmpty(updateUser)){
				detailPara.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.UPDATE_USER, updateUser, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(KS_CUSTOMER_LEVEL_TABLE.KS_CUSTOMER_LEVEL_ID,
					ksCustomerLevelId, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
		}

		return json;
	}

}
