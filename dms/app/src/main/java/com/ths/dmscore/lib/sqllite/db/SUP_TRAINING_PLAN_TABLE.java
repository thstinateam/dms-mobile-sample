/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingPlanDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSubTrainingPlanDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * 
 * SUP_TRANING_PLAN_TABLE.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:39:53 17-11-2014
 */
public class SUP_TRAINING_PLAN_TABLE extends ABSTRACT_TABLE {
	public static final String SUP_TRAINING_PLAN_ID = "SUP_TRAINING_PLAN_ID";
	public static final String SUP_STAFF_ID = "SUP_STAFF_ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String TRAINING_MONTH = "TRAINING_MONTH";
	public static final String TRAINING_DATE = "TRAINING_DATE";
	public static final String IS_SEND = "IS_SEND";
	public static final String SEND_DATE = "SEND_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String TABLE_NAME = "SUP_TRAINING_PLAN";

	public SUP_TRAINING_PLAN_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { SUP_TRAINING_PLAN_ID, SUP_STAFF_ID,
				STAFF_ID, SHOP_ID, TRAINING_MONTH, TRAINING_DATE, IS_SEND,
				SEND_DATE, CREATE_USER, UPDATE_USER, CREATE_DATE, UPDATE_DATE,
				SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Get lich huan luyen nhan vien cua TBHV
	 * 
	 * @author: hoanpd1
	 * @since: 14:28:01 17-11-2014
	 * @return: SupTraningPlanDTO
	 * @throws:
	 * @param parentStaffId
	 * @param inheritShopId
	 * @return
	 */
	public ListSubTrainingPlanDTO getStaffTrainingPlan(Bundle data) {
		ListSubTrainingPlanDTO dto = new ListSubTrainingPlanDTO();
		
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		String monthTraining = data.getString(IntentConstants.INTENT_MONTH);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer sqlString = new StringBuffer();
		sqlString.append("select stp.sup_training_plan_id AS TRAINING_PLAN_ID, ");
		sqlString.append("       stp.training_month AS MONTH, ");
		sqlString.append("       stp.IS_SEND AS IS_SEND, ");
		sqlString.append("       stp.training_date AS DATE, ");
		sqlString.append("       stp.[SUP_STAFF_ID] AS SUP_STAFF_ID, ");
		sqlString.append("       s.staff_id AS STAFF_ID, ");
		sqlString.append("       s.staff_code AS STAFF_CODE, ");
		sqlString.append("       s.staff_name AS STAFF_NAME, ");
		sqlString.append("       gs.staff_id AS STAFF_ID_GS, ");
		sqlString.append("       gs.staff_code AS STAFF_CODE_GS, ");
		sqlString.append("       gs.staff_name AS STAFF_NAME_GS, ");
		sqlString.append("       sh.shop_id AS SHOP_ID, ");
		sqlString.append("       sh.shop_code AS SHOP_CODE, ");
		sqlString.append("       sh.shop_name AS SHOP_NAME ");
		sqlString.append("from sup_training_plan stp, ");
		sqlString.append("     staff s, ");
		sqlString.append("     staff gs, ");
		sqlString.append("     (select shop_id, ");
		sqlString.append("             shop_code, ");
		sqlString.append("             shop_name ");
		sqlString.append("      from shop ");
		sqlString.append("      where [PARENT_SHOP_ID] in (select shop_id ");
		sqlString.append("                                     from shop ");
		sqlString.append("                                     where [PARENT_SHOP_ID] = ? ) ");
		param.add("" + shopId);
		sqlString.append("             OR SHOP_ID  in (select shop_id ");
		sqlString.append("                             from shop ");
		sqlString.append("                             where [PARENT_SHOP_ID] = ? )) sh ");
		param.add("" + shopId);
		sqlString.append("where 1=1 ");
		sqlString.append("      AND s.status =1 ");
		sqlString.append("      AND exists (select 1 from CHANNEL_TYPE CT where 1 = 1 ");
		sqlString.append("      AND CT.CHANNEL_TYPE_ID = S.STAFF_TYPE_ID ");
		sqlString.append("      AND CT.TYPE = 2 "); // Loai Nhan vien
		sqlString.append("      AND CT.OBJECT_TYPE  IN (1,2)  "); // NV
		sqlString.append("      AND CT.STATUS = 1) ");
		sqlString.append("      AND gs.status =1 ");
		sqlString.append("      AND exists (select 1 from CHANNEL_TYPE CT where 1 = 1 ");
		sqlString.append("      AND CT.CHANNEL_TYPE_ID = gs.STAFF_TYPE_ID ");
		sqlString.append("      AND CT.TYPE = 2 "); // Loai Nhan vien
		sqlString.append("      AND CT.OBJECT_TYPE = 5 "); // GSNPP 
		sqlString.append("      AND CT.STATUS = 1) ");
		sqlString.append("		AND s.staff_id in (SELECT PS.staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlString.append("				AND PS.parent_staff_id = gs.staff_id AND PS.STATUS = 1 ");
		sqlString.append("				AND not exists (select 1 from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		// sqlString.append("      AND s.[STAFF_OWNER_ID] = gs.staff_id  ");
		sqlString.append("      and stp.sup_staff_id =? ");
		param.add("" + staffId);
		sqlString.append("      and s.staff_id = stp.staff_id ");
		sqlString.append("      and substr(stp.training_month,1,7) = substr(?,1,7) ");
		param.add(monthTraining);
		sqlString.append("      and stp.shop_id = sh.shop_id");

		Cursor cursor = null;
		try {
			cursor = this.rawQueries(sqlString.toString(), param);
			if (cursor != null && cursor.moveToFirst()) {
				do {
					SupTrainingPlanDTO item = new SupTrainingPlanDTO();
					item.initDataTrainingPlanTBHV(cursor);
					dto.tbhvTrainingPlan.put(item.getTrainingDate(), item);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.e("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}
}
