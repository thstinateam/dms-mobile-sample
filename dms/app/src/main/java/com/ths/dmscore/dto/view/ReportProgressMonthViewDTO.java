/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *  report progress month info view dto
 *  @author: HaiTC3
 *  @version: 1.1
 *  @since: 1.0
 */
public class ReportProgressMonthViewDTO implements Serializable{
	private static final long serialVersionUID = 1155821454925417218L;
	// number fullDate sale plan
	public int numDaySalePlan;
	// number fullDate sold plan
	public int numDaySoldPlan;
	// progress sale
	public double progessSold;
	// list report progess month dto
	public ArrayList<ReportProgressMonthCellDTO> listReportProgessMonthDTO = new ArrayList<ReportProgressMonthCellDTO>();
	// total report progess month object
	public ReportProgressMonthCellDTO totalReportObject = new ReportProgressMonthCellDTO();

	public ReportProgressMonthViewDTO(){
		numDaySalePlan = 0;
		numDaySoldPlan = 0;
		progessSold = 0;
		listReportProgessMonthDTO = new ArrayList<ReportProgressMonthCellDTO>();
		totalReportObject = new ReportProgressMonthCellDTO();
	}
	
}
