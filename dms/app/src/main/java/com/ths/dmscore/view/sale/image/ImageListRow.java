package com.ths.dmscore.view.sale.image;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.ImageListItemDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dms.R;

public class ImageListRow extends DMSTableRow implements OnClickListener {
	public static final int ACTION_VIEW_ALBUM = 1;
	public static final int ACTION_VIEW_FULL_ALBUM = 2;
	// so thu tu
	TextView tvSTT;
	// ma khach hang
	TextView tvCusCode;
	// ten khach hang
	TextView tvCusName;
	// Dia chi
	TextView tvCusAdd;
	// so hinh hinh anh
	TextView tvImageNumber;
	// vinamilk table view
	//private View tableParent;
	// row dto
	ImageListItemDTO dto;
	// listener when row have action
	private TextView tvVisitPlan;

	// khoi tao control
	public ImageListRow(Context context, View aRow) {
		super(context, R.layout.layout_image_list_row);
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvCusCode = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvCustomerCode, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_CHITIETKH);
		PriUtils.getInstance().setOnClickListener(tvCusCode, this);
		tvCusName = (TextView) findViewById(R.id.tvCustomerName);
		tvCusAdd = (TextView) findViewById(R.id.tvAddress);
		tvImageNumber = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvImageNumber, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TIMKIEMKH);
		PriUtils.getInstance().setOnClickListener(tvImageNumber, this);
		tvVisitPlan = (TextView) findViewById(R.id.tvVisitPlan);
	}

	// render layout
	 /**
	 * render layout cho view
	 * @author: Tuanlt11
	 * @param position
	 * @param item
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(int position, ImageListItemDTO item) {
		this.dto = item;
		tvSTT.setText("" + position);
		tvCusCode.setText(item.customerCode);
		tvCusName.setText(item.customerName);
		tvCusAdd.setText(item.address);
		tvImageNumber.setText("" + item.imageNumber);
		tvVisitPlan.setText(item.visitPlan);
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == tvCusCode && listener != null) {
			listener.handleVinamilkTableRowEvent(ACTION_VIEW_ALBUM, arg0, dto);
		} else if (arg0 == this && context != null) {
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity) context);
		}else if (arg0 == tvImageNumber && listener != null) {
			listener.handleVinamilkTableRowEvent(ACTION_VIEW_FULL_ALBUM, arg0, dto);
		}
	}
}
