package com.ths.dmscore.lib.oAuth.builder.api;

import com.ths.dmscore.lib.network.http.OAuthRequestManager;
import com.ths.dmscore.lib.oAuth.model.OAuthConfig;

public class VT_Api extends DefaultApi20 {

	/**
	 * @author ThangNV31
	 */
	@Override
	public String getAccessTokenEndpoint() {
		return OAuthRequestManager.getServerPathOauth() + "oauth/token";
	}

	/**
	 * @author ThangNV31
	 */
	@Override
	public String getAuthorizationUrl(OAuthConfig config) {
		return null;
	}
}