/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Luu tru thong tin version db
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class DBVersionDTO implements Serializable{	
	private static final long serialVersionUID = 6667100171725909485L;
	public static final String UPDATE = "UPDATEDB";
	public static final String RESET  = "RESETDB";
	
	//action update DB (update or reset)
	private String action;
	//script
	private String script = Constants.STR_BLANK;;
	//version db
	private String version = Constants.STR_BLANK;;
	
	// last log id luc tao file
	private String lastLogId = "";
	// log db hien tai tren server
	private String maxDBLogId = "";
	// url tai file db
	private String urlDB = "";
		
	/**
	 * parse thong tin db tu login
	 * @author: BANGHN
	 * @param jsonObject
	 */
	public void parseFromJson(JSONObject jsonObject){
		try {					
			setAction(jsonObject.getString("action"));
			setScript(jsonObject.getString("script"));
			setVersion(jsonObject.getString("version"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}		
	}
	
	/**
	 * Parse luu du lieu khi login ve: luu thong tin version db, last_log_udate
	 * @author : BangHN
	 * since : 2:08:43 PM
	 */
	public void parseGetLinkDB(JSONObject jsonObject){		
		try {					
			setLastLogId(jsonObject.getString("lastLogId_update"));
			setMaxDBLogId(jsonObject.getString("maxDBLogId"));
			setVersion(jsonObject.getString("currentDBVersion"));
			setUrlDB(jsonObject.getString("URL"));
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}		
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getLastLogId() {
		return lastLogId;
	}

	public void setLastLogId(String lastLogId) {
		this.lastLogId = lastLogId;
	}

	public String getMaxDBLogId() {
		return maxDBLogId;
	}

	public void setMaxDBLogId(String maxDBLogId) {
		this.maxDBLogId = maxDBLogId;
	}

	public String getUrlDB() {
		return urlDB;
	}

	public void setUrlDB(String urlDB) {
		this.urlDB = urlDB;
	}
}
