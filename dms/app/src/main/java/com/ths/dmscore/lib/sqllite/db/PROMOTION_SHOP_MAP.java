/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.dto.db.PromotionShopMapDTO;

/**
 *  Thong tin khuyen mai cua SHOP
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 2.1
 */
public class PROMOTION_SHOP_MAP extends ABSTRACT_TABLE {
	// id promotion shop
	public static final String PROMOTION_SHOP_MAP_ID = "PROMOTION_SHOP_MAP_ID";
	// id CTKM
	public static final String PROMOTION_PROGRAM_ID = "PROMOTION_PROGRAM_ID";
	// id shop
	public static final String SHOP_ID = "SHOP_ID";
	// so luong toi da
	public static final String QUANTITY_MAX = "QUANTITY_MAX";
	// so luong thuc nhan sau khi duyet
	public static final String QUANTITY_RECEIVED = "QUANTITY_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet)
	public static final String QUANTITY_RECEIVED_TOTAL = "QUANTITY_RECEIVED_TOTAL";
	// ?
	public static final String STATUS = "STATUS";
	// so xuat ap dung cho KH voi TH CTKM chi den NPP
	public static final String QUANTITY_CUSTOMER = "QUANTITY_CUSTOMER";
	// doi tuong ap dung 1: chi ap dung den NPP, 2: ap dung den NPP, loai KH; 3: ap dung den tan KH
	public static final String OBJECT_APPLY = "OBJECT_APPLY";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String FROM_DATE = "FROM_DATE";
	public static final String TO_DATE = "TO_DATE";
	// so luong toi da
	public static final String AMOUNT_MAX = "AMOUNT_MAX";
	// so luong thuc nhan sau khi duyet
	public static final String AMOUNT_RECEIVED = "AMOUNT_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet)
	public static final String AMOUNT_RECEIVED_TOTAL = "AMOUNT_RECEIVED_TOTAL";
	// so luong toi da
	public static final String NUM_MAX = "NUM_MAX";
	// so luong thuc nhan sau khi duyet
	public static final String NUM_RECEIVED = "NUM_RECEIVED";
	// so luong thuc nhan sau khi tao don (chua duyet)
	public static final String NUM_RECEIVED_TOTAL = "NUM_RECEIVED_TOTAL";

	public static final String TABLE_PROMOTION_SHOP_MAP = "PROMOTION_SHOP_MAP";

	public PROMOTION_SHOP_MAP(SQLiteDatabase mDB) {
		this.tableName = TABLE_PROMOTION_SHOP_MAP;
		this.columns = new String[] { PROMOTION_SHOP_MAP_ID,
				PROMOTION_PROGRAM_ID, SHOP_ID, QUANTITY_MAX, QUANTITY_RECEIVED,
				QUANTITY_RECEIVED_TOTAL, NUM_RECEIVED_TOTAL, AMOUNT_RECEIVED_TOTAL,
				STATUS, QUANTITY_CUSTOMER, OBJECT_APPLY, CREATE_USER,
				UPDATE_USER, CREATE_DATE, CREATE_USER, UPDATE_USER, FROM_DATE,
				TO_DATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PromotionShopMapDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PromotionShopMapDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * @author: BangHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PromotionShopMapDTO disDTO = (PromotionShopMapDTO)dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.promotionShopMapId };
		return update(value, PROMOTION_SHOP_MAP_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: BangHN
	 * @param id
	 * @return: int
	 * @throws:
	 */
	public int delete(String id) {
		String[] params = { id };
		return delete(PROMOTION_SHOP_MAP_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PromotionShopMapDTO proDetail = (PromotionShopMapDTO)dto;
		String[] params = { "" + proDetail.promotionShopMapId };
		return delete(PROMOTION_SHOP_MAP_ID + " = ?", params);
	}



	/**
	* intit du lieu update vao bang
	* @author: BangHN
	* @return
	* @return: ContentValues
	*/
	private ContentValues initDataRow(PromotionShopMapDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PROMOTION_SHOP_MAP_ID, dto.promotionShopMapId);
		editedValues.put(PROMOTION_PROGRAM_ID, dto.promotionProgrameId);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(QUANTITY_MAX, dto.quantityMax);
		editedValues.put(QUANTITY_RECEIVED, dto.quantityReceived);
		editedValues.put(STATUS, dto.status);

		editedValues.put(QUANTITY_CUSTOMER, dto.quantityCustomer);
		editedValues.put(OBJECT_APPLY, dto.objectApply);
		editedValues.put(CREATE_USER, dto.createUser);

		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);

		return editedValues;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param amountReceived
	 * @param numReceived
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	*/
	public boolean increaseQuantityRecevie(long shopMapID, int quantityReceived, int numReceived, double amountReceived) throws Exception {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_SHOP_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN QUANTITY_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN QUANTITY_RECEIVED + ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", NUM_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN NUM_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN NUM_RECEIVED + ");
		sqlObject.append(numReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(numReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", AMOUNT_RECEIVED =  ");
		sqlObject.append(" ( CASE WHEN AMOUNT_RECEIVED IS NOT NULL ");
		sqlObject.append(" THEN AMOUNT_RECEIVED + ");
		sqlObject.append(amountReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(amountReceived);
		sqlObject.append(" END) ");
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_SHOP_MAP_ID = ");
		sqlObject.append(shopMapID);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.e("increaseQuantityRecevie", "fail", e);
			throw e;
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param amountReceived
	 * @param numReceived
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	 */
	public boolean increaseQuantityRecevieTotal(long shopMapID, int quantityReceived, int numReceived, double amountReceived) throws Exception {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_SHOP_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN QUANTITY_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN QUANTITY_RECEIVED_TOTAL + ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", NUM_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN NUM_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN NUM_RECEIVED_TOTAL + ");
		sqlObject.append(numReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(numReceived);
		sqlObject.append(" END) ");
		sqlObject.append(", AMOUNT_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN AMOUNT_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN AMOUNT_RECEIVED_TOTAL + ");
		sqlObject.append(amountReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(amountReceived);
		sqlObject.append(" END) ");
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_SHOP_MAP_ID = ");
		sqlObject.append(shopMapID);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.e("increaseQuantityRecevieTotal", "fail", e);
			throw e;
		} finally {
		}

		return true;
	}

	/**
	 * Giam so suat da nhan duoc
	 * @author: tuanlt11
	 * @since: 3:07:58 PM Mar 31, 2016
	 * @return: boolean
	 * @throws:
	 * @param shopMapID
	 * @param quantityReceived
	 * @param numReceived
	 * @param amountReceived
	 * @return
	 * @throws Exception
	 */
	public boolean decreaseQuantityRecevieTotal(long shopMapID, int quantityReceived, int numReceived, double amountReceived) throws Exception {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_SHOP_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN QUANTITY_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN QUANTITY_RECEIVED_TOTAL - ");
		sqlObject.append(quantityReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(0);
		sqlObject.append(" END) ");
		sqlObject.append(", NUM_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN NUM_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN NUM_RECEIVED_TOTAL - ");
		sqlObject.append(numReceived);
		sqlObject.append(" ELSE  ");
		sqlObject.append(0);
		sqlObject.append(" END) ");
		sqlObject.append(", AMOUNT_RECEIVED_TOTAL =  ");
		sqlObject.append(" ( CASE WHEN AMOUNT_RECEIVED_TOTAL IS NOT NULL ");
		sqlObject.append(" THEN AMOUNT_RECEIVED_TOTAL - ");
		sqlObject.append(amountReceived);
		sqlObject.append(" ELSE ");
		sqlObject.append(0);
		sqlObject.append(" END) ");
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_SHOP_MAP_ID = ");
		sqlObject.append(shopMapID);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.e("increaseQuantityRecevieTotal", "fail", e);
			throw e;
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	*/
	public boolean decreaseQuantityRecevie(long shopMapID, int num) throws Exception {
		StringBuilder sqlObject = new StringBuilder();
		sqlObject.append("update PROMOTION_SHOP_MAP set ");
		sqlObject.append("QUANTITY_RECEIVED = QUANTITY_RECEIVED - ");
		sqlObject.append(num);
		sqlObject.append(" WHERE ");
		sqlObject.append(" PROMOTION_SHOP_MAP_ID = ");
		sqlObject.append(shopMapID);

		try {
			execSQL(sqlObject.toString());
		} catch (Exception e) {
			MyLog.e("decreaseQuantityRecevie", "fail", e);
			throw e;
		} finally {
		}

		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param promotionProgramCode
	 * @return
	 * @return: PromotionShopMapDTO
	 * @throws:
	*/
	public PromotionShopMapDTO getPromotionShopMapByPromotionProgramCode(String promotionProgramCode, String shopId) throws Exception{
		PromotionShopMapDTO dto = null;
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTable.getShopRecursive(shopId);
		String idListString = TextUtils.join(",", shopIdArray);

		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("	SELECT	");
		sql.append("	    *	");
		sql.append("	FROM	");
		sql.append("	    PROMOTION_PROGRAM pp,	");
		sql.append("	    PROMOTION_SHOP_MAP psm	");
		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND pp.PROMOTION_PROGRAM_CODE = ? ");
		params.add(promotionProgramCode);
		sql.append("	    AND psm.SHOP_ID in (" + shopId);
		sql.append("       ) ");
		sql.append("	    AND pp.STATUS = 1 ");
		sql.append("	    AND psm.STATUS = 1 ");
		sql.append("	    AND pp.PROMOTION_PROGRAM_ID = psm.PROMOTION_PROGRAM_ID	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = new PromotionShopMapDTO();
					dto.initDataFromCursor(c);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		//Neu khong co du lieu cua shop thi lay cua nhung shop tren no
		if(dto == null) {
			StringBuffer sql2 = new StringBuffer();
			ArrayList<String> params2 = new ArrayList<String>();
			sql2.append("	SELECT	");
			sql2.append("	    *	");
			sql2.append("	FROM	");
			sql2.append("	    PROMOTION_PROGRAM pp,	");
			sql2.append("	    PROMOTION_SHOP_MAP psm	");
			sql2.append("	WHERE	");
			sql2.append("	    1=1	");
			sql2.append("	    AND pp.PROMOTION_PROGRAM_CODE = ? ");
			params2.add(promotionProgramCode);
			sql2.append("	    AND psm.SHOP_ID in (" + idListString);
			sql2.append("       ) ");
			sql2.append("	    AND pp.STATUS = 1 ");
			sql2.append("	    AND psm.STATUS = 1 ");
			sql2.append("	    AND pp.PROMOTION_PROGRAM_ID = psm.PROMOTION_PROGRAM_ID	");

			Cursor c2 = null;
			try {
				c2 = rawQueries(sql2.toString(), params2);
				if (c2 != null) {
					if (c2.moveToFirst()) {
						dto = new PromotionShopMapDTO();
						dto.initDataFromCursor(c2);
					}
				}
			} catch (Exception e) {
			} finally {
				try {
					if (c2 != null) {
						c2.close();
					}
				} catch (Exception e) {
				}
			}
		}

		return dto;
	}

	/**
	 * Lay thong tin so suat cua npp
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 17:25:14 23 Sep 2014
	 * @return: PromotionShopMapDTO
	 * @throws:
	 * @param promotionProgramCode
	 * @param shopId
	 * @return
	 */
	public PromotionShopMapDTO getPromotionShopMapByPromotionProgramId(long proId, String shopId) throws Exception{
		PromotionShopMapDTO dto = null;
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopIdArray = shopTable.getShopRecursive(shopId);
		String idListString = TextUtils.join(",", shopIdArray);

		StringBuffer sql = new StringBuffer();
		ArrayList<String> params = new ArrayList<String>();
		sql.append("	SELECT	");
		sql.append("	    *	");
		sql.append("	FROM	");
		sql.append("	    PROMOTION_PROGRAM pp,	");
		sql.append("	    PROMOTION_SHOP_MAP psm	");
		sql.append("	WHERE	");
		sql.append("	    1=1	");
		sql.append("	    AND pp.PROMOTION_PROGRAM_ID = ? ");
		params.add(proId + "");
		sql.append("	    AND psm.SHOP_ID in (" + shopId);
		sql.append("       ) ");
		sql.append("	    AND pp.STATUS = 1 ");
		sql.append("	    AND psm.STATUS = 1 ");
		sql.append("	    AND pp.PROMOTION_PROGRAM_ID = psm.PROMOTION_PROGRAM_ID	");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					dto =  new PromotionShopMapDTO();
					dto.initDataFromCursor(c);
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		//Neu khong co du lieu cua shop thi lay cua nhung shop tren no
		if(dto == null) {
			dto = new PromotionShopMapDTO();
			StringBuffer sql2 = new StringBuffer();
			ArrayList<String> params2 = new ArrayList<String>();
			sql2.append("	SELECT	");
			sql2.append("	    *	");
			sql2.append("	FROM	");
			sql2.append("	    PROMOTION_PROGRAM pp,	");
			sql2.append("	    PROMOTION_SHOP_MAP psm	");
			sql2.append("	WHERE	");
			sql2.append("	    1=1	");
			sql2.append("	    AND pp.PROMOTION_PROGRAM_ID = ? ");
			params2.add(proId + "");
			sql2.append("	    AND psm.SHOP_ID in (" + idListString);
			sql2.append("       ) ");
			sql2.append("	    AND pp.STATUS = 1 ");
			sql2.append("	    AND psm.STATUS = 1 ");
			sql2.append("	    AND pp.PROMOTION_PROGRAM_ID = psm.PROMOTION_PROGRAM_ID	");

			Cursor c2 = null;
			try {
				c2 = rawQueries(sql2.toString(), params2);
				if (c2 != null) {
					if (c2.moveToFirst()) {
						dto.initDataFromCursor(c2);
					}
				}
			} catch (Exception e) {
			} finally {
				try {
					if (c2 != null) {
						c2.close();
					}
				} catch (Exception e) {
				}
			}
		}

		return dto;
	}
}
