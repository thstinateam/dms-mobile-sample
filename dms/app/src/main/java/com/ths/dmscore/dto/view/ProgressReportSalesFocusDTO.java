package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

public class ProgressReportSalesFocusDTO {
	public int planDay = 0;// so ngay ban hang ke hoach
	public int pastDay = 0;// so ngay ban hang da qua
	public double progress = 0;// tien do
	public ArrayList<String> arrMMTTText;

	// bang bao cao tien do ban hang trong tam
	public ArrayList<InfoProgressEmployeeDTO> listProgressSalesStaff;
	public ArrayList<ReportProductFocusItem> arrRptFocusItemTotal;
	public static final int TYPE_STAFF = 1;
	public static final int TYPE_ROUTING = 2;
	public static final int TYPE_SHOP = 3;


	public ProgressReportSalesFocusDTO() {
		listProgressSalesStaff = new ArrayList<InfoProgressEmployeeDTO>();
		arrMMTTText = new ArrayList<String>();
		arrRptFocusItemTotal = new ArrayList<ReportProductFocusItem>();
	}

	public void addItem(InfoProgressEmployeeDTO c) {
		listProgressSalesStaff.add(c);
	}

	public class InfoProgressEmployeeDTO {

		// id nhan vien
		public int staffId ;
		public String staffCode;// ma nhan vien
		public String staffName;// ten nhan vien
		public String staffPhone;// sdt nhan vien
		public String staffMobile;// di dong nhan vien
		public String saleTypeCode = "";// loai mh dc ban
		// MHTT1
		public double moneyPlan1;// so tien theo ke hoach
		public double moneySold1;// so tien ban duoc
		public double moneyRemain1;// so tien con lai
		public double progress1;// % tien do
		// MHTT2
		public double moneyPlan2;// so tien theo ke hoach
		public double moneySold2;// so tien ban duoc
		public double moneyRemain2;// so tien con lai
		public double progress2;// % tien do

		public ArrayList<ReportProductFocusItem> arrRptFocusItem;

		public InfoProgressEmployeeDTO() {
			staffCode = "";
			staffName = "";
			moneyPlan1 = 0;
			moneySold1 = 0;
			progress1 = 0;
			moneyRemain1 = 0;

			moneyPlan2 = 0;
			moneySold2 = 0;
			progress2 = 0;
			moneyRemain2 = 0;

			arrRptFocusItem = new ArrayList<ReportProductFocusItem>();

		}

		public InfoProgressEmployeeDTO(Cursor c) {
			staffCode = CursorUtil.getString(c, "staff_code");
			staffName = CursorUtil.getString(c, "name");
			moneyPlan1 = CursorUtil.getLong(c, "plan_amount1");
			moneySold1 = CursorUtil.getLong(c, "sold_amount1");
			if (moneyPlan1 > 0) {
				progress1 = (double) moneySold1 / (double) moneyPlan1;
				progress1 *= 100;
			}
			moneyRemain1 = moneyPlan1 - moneySold1;

			moneyPlan2 = CursorUtil.getLong(c, "plan_amount2");
			moneySold2 = CursorUtil.getLong(c, "sold_amount2");
			if (moneyPlan2 > 0) {
				progress2 = (double) moneySold2 / (double) moneyPlan2;
				progress2 *= 100;
			}
			moneyRemain2 = moneyPlan2 - moneySold2;

		}

	}

	public class ReportProductFocusItem {
		public double amountPlan;// so tien theo ke hoach
		public double amountDone;// so tien ban duoc
		public double amountApproved;
		public double amountPending;
		public double amountPercent;// % tien do
		public long quantityPlan;
		public long quantityDone;
		public long quantityApproved;
		public long quantityPending;
		public double quantityPercent;
		public String typeFocusProduct;
	}

	public InfoProgressEmployeeDTO newInfoProgressEmployeeDTO() {
		return new InfoProgressEmployeeDTO();
	}

	public ReportProductFocusItem newReportProductFocusItem() {
		return new ReportProductFocusItem();
	}

}
