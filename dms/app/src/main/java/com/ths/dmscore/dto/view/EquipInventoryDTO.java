/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.me.photo.PhotoDTO;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.EQUIP_STATISTIC_REC_DTL_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * THiet bi can kiem ke
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 09:52:54 16-12-2014
 */
public class EquipInventoryDTO extends AbstractTableDTO implements Serializable {
	public static final int EQUIP_USAGE_STATUS_LOST = 0;
	public static final int EQUIP_USAGE_STATUS_RESOLVE = 1;
	public static final int EQUIP_USAGE_STATUS_IN_STOCK = 2;
	public static final int EQUIP_USAGE_STATUS_USING = 3;
	public static final int EQUIP_USAGE_STATUS_REPARING = 4;
	
	
//	public static final int EQUIP_LOST_REC_MOBILE_NOT_RESOLVE = 0;
//	public static final int EQUIP_LOST_REC_MOBILE_ALREAD_RESOLVE = 1;
	
	public static final int EQUIP_TRADE_STATUS_NOT_TRANSACTION = 0;
	public static final int EQUIP_TRADE_STATUS_IN_TRANSACTION = 1;
	
	public static final int EQUIP_TRADE_TYPE_REPORT_LOST = 3;
	public static final int EQUIP_TRADE_TYPE_REPORT_LOST_MOBILE = 4;
	
	private static final long serialVersionUID = 1L;
	// ID bien ban kiem ke
	private long idRecordInven;
	// ID chi tiet kiem ke
	public long idInvenDetail;
	// id thiet bi
	public long idEquip;
	// trang thai thiet bi 
	public int usageStatusEquip;
	// ma thiet bi
	public String codeEquip;
	// so serial theit bi
	public String seriEquip;
	// id Nhom thiet bi
	public long idEquipGroup;
	// Ma Nhom thiet bi
	public String codeEquipGroup;
	//ten nhom thiet bi
	public String nameEquipGroup;
	// ma Loai thiet bij
	public String codeCategory;
	// ten Loai thiet bi
	public String nameCategory;
	//trang thai thiet bi -> ap_param
	public String healthStatus;
	// id khach hang
	public long stockId;
	// Ma kho khach hang
	public String stockCode;
	// ngay cap nhat
	public String updateDate;
	// user cap nhat
	public String updateUser;
	// ngay cap nhat
	public String createDate;
	// user cap nhat
	public String createUser;
	// trang thai
	public int status;
	// duong dan hinh anh
	public String imagePath;
	//da check
	public boolean checkStatus = false;
	
	// thong tin de luu vao EQUIP_STATISTIC_REC_DTL
	private String shopId;
	private int objectType;
	public long objectId;
	public long objectStockId;
	public String statisticDate;
	// so lan kiem ke lan truoc
	public int preTimes;
	public int statisticTime;
	public int quantity;
	public String dateInWeek;
	// so luong kiem ke u ke
	public int quantityActually;
	// 1 kho, 2 KH
	public int objectStockType;
	private String staffId;
	
	// ma u ke
	public long idUke;
	public String codeUke;
	public String nameUke;
	
	public int tradeStatus = -1;
	public int tradeType = -1;
	private List<PhotoDTO> listPhotoTaken = new ArrayList<PhotoDTO>();
	/**
	 * parse du lieu thiet bi kiem ke/ danh sach thiet bi cua khach hang
	 * @author: hoanpd1
	 * @since: 14:31:43 29-12-2014
	 * @return: void
	 * @throws:  
	 * @param c
	 * @throws Exception
	 */
	public void parseDataEquipInventoryRecord(Cursor c) throws Exception {
		try {
			setIdRecordInven(CursorUtil.getLong(c, "ID_RECORD_INVEN"));
			idInvenDetail = CursorUtil.getLong(c, "ID_INVENTORY_DETAIL");
			idEquip = CursorUtil.getLong(c, "ID_EQUIP");
			codeEquip = CursorUtil.getString(c, "CODE_EQUIP");
			seriEquip = CursorUtil.getString(c, "SERIAL_EQUIP");
			idEquipGroup = CursorUtil.getLong(c, "ID_EQUIP_GROUP");
			nameEquipGroup = CursorUtil.getString(c, "NAME_EQUIP_GROUP");
			codeEquipGroup= CursorUtil.getString(c, "CODE_EQUIP_GROUP");
			nameCategory = CursorUtil.getString(c, "NAME_EQUIP_CATEGORY");
			codeCategory = CursorUtil.getString(c, "CODE_EQUIP_CATEGORY");
			healthStatus = CursorUtil.getString(c, "HEALTH_STATUS");
			stockId = CursorUtil.getLong(c, "STOCK_ID");
			stockCode = CursorUtil.getString(c, "STOCK_CODE");
			
			setObjectType(CursorUtil.getInt(c, "OBJECT_TYPE"));
			idUke = CursorUtil.getLong(c, "ID_UKE");
			codeUke = CursorUtil.getString(c, "CODE_UKE");
			nameUke = CursorUtil.getString(c, "NAME_UKE");
			usageStatusEquip = CursorUtil.getInt(c, "USAGE_STATUS");
			tradeStatus = CursorUtil.getInt(c, "TRADE_STATUS");
			tradeType = CursorUtil.getInt(c, "TRADE_TYPE");
			preTimes = CursorUtil.getInt(c, "PRETIMES");

		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * 
	 * @author: hoanpd1
	 * @since: 11:43:51 17-12-2014
	 * @return: JSONObject
	 * @throws:  
	 * @return
	 */
	public JSONObject generateUpdateFromOrderSql(EquipInventoryDTO item) {
		JSONObject jsonUpdate = new JSONObject(); 
		try {
			// Update
			jsonUpdate.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE); 
			jsonUpdate.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_STATISTIC_REC_DTL_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			// ...them thuoc tinh		
			detailPara.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.STATUS, item.status, null));
			detailPara.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.UPDATE_DATE, item.updateDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.UPDATE_USER, item.updateUser, null));
			jsonUpdate.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.EQUIP_STATISTIC_REC_DTL_ID, item.idInvenDetail, null));
			jsonUpdate.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (JSONException e) {
			MyLog.e("UnexceptionLog",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}

		return jsonUpdate;
	}

	/**
	 * @author: DungNX
	 * @param c
	 * @throws Exception
	 * @return: void
	 * @throws:
	*/
	public void parseDataEquipLostReportError(Cursor c) throws Exception {
		try {
			idEquip = CursorUtil.getLong(c, "EQUIP_ID");
			codeEquip = CursorUtil.getString(c, "CODE");
			seriEquip = CursorUtil.getString(c, "SERIAL");
		} catch (Exception ex) {
			throw ex;
		}
	}

	/**
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInsertEquipLostMobileRec() {
		JSONObject jsonInsert = new JSONObject();
		try {
			// Insert
			jsonInsert.put(IntentConstants.INTENT_TYPE, TableAction.INSERT); 
			jsonInsert.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_STATISTIC_REC_DTL_TABLE.TABLE_NAME);
			// ds params
			JSONArray jsonDetail = new JSONArray();
			// Khi insert thi insert cac truong sau
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.EQUIP_STATISTIC_REC_DTL_ID, idInvenDetail, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.EQUIP_STATISTIC_RECORD_ID, getIdRecordInven(), null));  
			if (!StringUtil.isNullOrEmpty(getShopId())) {
				jsonDetail.put(GlobalUtil.getJsonColumn(
						EQUIP_STATISTIC_REC_DTL_TABLE.SHOP_ID, getShopId(), null));
			}
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_STOCK_ID, objectStockId, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_ID, idEquip, null));
//			if (getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_ALL
//					|| getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_GROUP_EQUIPMENT
//					|| getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_EQUIPMENT) {
//				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_ID, idEquip, null));
//			} else if (getObjectType() == EquipStatisticRecordDTO.OBJECT_TYPE_U_KE) {
//				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_ID, idUke, null));
//			}
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.STATUS, status, null));  
			if (!StringUtil.isNullOrEmpty(createDate)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.CREATE_DATE, createDate, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.CREATE_USER, createUser, null));
			}
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_TYPE, getObjectType(), null));
			if (!StringUtil.isNullOrEmpty(statisticDate)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.STATISTIC_DATE, statisticDate, null));
			}
			if (statisticTime > 0) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.STATISTIC_TIME, statisticTime, null));
			}
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.QUANTITY, quantity, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.QUANTITY_ACTUALLY, quantityActually, null));
			jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.OBJECT_STOCK_TYPE, objectStockType, null));
			if (!StringUtil.isNullOrEmpty(getStaffId())) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.STAFF_ID, getStaffId(), null));
			}
			if (!StringUtil.isNullOrEmpty(dateInWeek)) {
				jsonDetail.put(GlobalUtil.getJsonColumn(EQUIP_STATISTIC_REC_DTL_TABLE.DATE_IN_WEEK, dateInWeek, null));
			}
			jsonInsert.put(IntentConstants.INTENT_LIST_PARAM, jsonDetail);
		} catch (JSONException e) {
			MyLog.e("UnexceptionLog", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return jsonInsert;
	}

	public long getIdRecordInven() {
		return idRecordInven;
	}

	public void setIdRecordInven(long idRecordInven) {
		this.idRecordInven = idRecordInven;
	}

	public int getObjectType() {
		return objectType;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public List<PhotoDTO> getListPhotoTaken() {
		return listPhotoTaken;
	}

	public void setListPhotoTaken(List<PhotoDTO> listPhotoTaken) {
		this.listPhotoTaken = listPhotoTaken;
	}
}
