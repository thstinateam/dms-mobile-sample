package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_LOT_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_LOT_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class SaleOrderLotDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 8798252986495645693L;
	// id
	public long saleOrderLotId;
	public long saleOrderId;
	public long saleOrderDetailId;
	public long wareHouseId;
	// Id Stock total
	public long stockTotalId;
	// So lo (neu co)
	public String lot;
	public long productId;
	public double price ;
	public long shopId;
	public long staffId;
	// So luong
	public long quantity;
	// Ngay het han cua lo hang
	public String expirationDate;
	// Ngay dat hang
	public String orderDate;
	public String createDate;
	public String updateDate;
	public String createUser;
	public String udpateUser;
	// luu discount percent cho Kho (lo) nay
	public double discountPercent;
	// discount cua kho(lo) nay
	public double discountAmount;
	public long poLotId;
	public long poId;
	public long poDetailId;
	public double packagePrice ;
	public long priceId;
	public long quantityRetail;
	public long quantityPackage;

	public SaleOrderLotDTO(){
		super(TableType.SALE_ORDER_LOT_TABLE);
	}

	public SaleOrderLotDTO(OrderDetailViewDTO detailViewDTO) {
		super(TableType.SALE_ORDER_LOT_TABLE);
		this.saleOrderId = detailViewDTO.orderDetailDTO.salesOrderId;
		this.saleOrderDetailId = detailViewDTO.orderDetailDTO.salesOrderDetailId;
		this.poId = detailViewDTO.orderDetailDTO.poId;
		this.poDetailId = detailViewDTO.orderDetailDTO.poDetailId;
		this.createDate = detailViewDTO.orderDetailDTO.createDate;
		this.createUser = detailViewDTO.orderDetailDTO.createUser;
		this.discountAmount = detailViewDTO.orderDetailDTO.getDiscountAmount();
		this.discountPercent = detailViewDTO.orderDetailDTO.discountPercentage;
		//this.expirationDate
		//this.lot
		this.orderDate = detailViewDTO.orderDetailDTO.orderDate;
		this.price = detailViewDTO.orderDetailDTO.price;
		this.productId = detailViewDTO.orderDetailDTO.productId;
		this.quantity = (int)detailViewDTO.orderDetailDTO.quantity;
		this.shopId = detailViewDTO.orderDetailDTO.shopId;
		this.staffId = detailViewDTO.orderDetailDTO.staffId;
		this.stockTotalId = detailViewDTO.stockId;
		//this.wareHouseId
		this.udpateUser = detailViewDTO.orderDetailDTO.updateUser;
		this.updateDate = detailViewDTO.orderDetailDTO.updateDate;
		this.packagePrice =  detailViewDTO.orderDetailDTO.packagePrice;
		this.priceId =  detailViewDTO.orderDetailDTO.priceId;
		this.quantityRetail =  detailViewDTO.orderDetailDTO.quantityRetail;
		this.quantityPackage =  detailViewDTO.orderDetailDTO.quantityPackage;
	}

	/**
	 * Tao JSON luu sale_order_lot
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 14:26:26 26 Sep 2014
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonInsertSaleOrderLot() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, SALE_ORDER_LOT_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.SALE_ORDER_LOT_ID, this.saleOrderLotId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.SALE_ORDER_ID, this.saleOrderId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.SALE_ORDER_DETAIL_ID, this.saleOrderDetailId, null));
			if (this.wareHouseId > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.WAREHOUSE_ID, this.wareHouseId, null));
			}
			if (this.stockTotalId > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.STOCK_TOTAL_ID, this.stockTotalId, null));
			}
			if (!StringUtil.isNullOrEmpty(this.lot)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.LOT, this.lot, null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.PRODUCT_ID, this.productId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.PRICE, this.price, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.QUANTITY, this.quantity, null));

			if (!StringUtil.isNullOrEmpty(this.expirationDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.EXPIRATION_DATE, this.expirationDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.orderDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.ORDER_DATE, this.orderDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.updateDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.UPDATE_DATE, this.updateDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.createDate)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.CREATE_DATE, this.createDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.createUser)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.CREATE_USER, this.createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(this.udpateUser)) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.UPDATE_USER, this.udpateUser, null));
			}
//			if (this.discountPercent > 0) {
//				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
//			}
			if (this.discountAmount > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			}
			if (this.packagePrice > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.PACKAGE_PRICE, this.packagePrice, null));
			}
			if (this.priceId > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.PRICE_ID, this.priceId, null));
			}
			if (this.quantityRetail > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.QUANTITY_RETAIL, this.quantityRetail, null));
			}
			if (this.quantityPackage > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_LOT_TABLE.QUANTITY_PACKAGE, this.quantityPackage, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * Tao JSON luu po_lot
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 14:26:26 26 Sep 2014
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateJsonInsertPoLot() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, PO_CUSTOMER_LOT_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PO_CUSTOMER_LOT_ID, this.poLotId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PO_CUSTOMER_ID, this.poId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PO_CUSTOMER_DETAIL_ID, this.poDetailId, null));
			if (this.wareHouseId > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.WAREHOUSE_ID, this.wareHouseId, null));
			}
			if (this.stockTotalId > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.STOCK_TOTAL_ID, this.stockTotalId, null));
			}
			if (!StringUtil.isNullOrEmpty(this.lot)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.LOT, this.lot, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PRODUCT_ID, this.productId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PRICE, this.price, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.SHOP_ID, this.shopId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.STAFF_ID, this.staffId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.QUANTITY, this.quantity, null));

			if (!StringUtil.isNullOrEmpty(this.expirationDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.EXPIRATION_DATE, this.expirationDate, null));
			}

			if (!StringUtil.isNullOrEmpty(this.orderDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.ORDER_DATE, this.orderDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.updateDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.UPDATE_DATE, this.updateDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.createDate)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.CREATE_DATE, this.createDate, null));
			}
			if (!StringUtil.isNullOrEmpty(this.createUser)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.CREATE_USER, this.createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(this.udpateUser)) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.UPDATE_USER, this.udpateUser, null));
			}
//			if (this.discountPercent > 0) {
//				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.DISCOUNT_PERCENT, this.discountPercent, null));
//			}
			if (this.discountAmount > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.DISCOUNT_AMOUNT, this.discountAmount, null));
			}

			if (this.packagePrice > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PACKAGE_PRICE, this.packagePrice, null));
			}
			if (this.priceId > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.PRICE_ID, this.priceId, null));
			}
			if (this.quantityRetail > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.QUANTITY_RETAIL, this.quantityRetail, null));
			}
			if (this.quantityPackage > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_LOT_TABLE.QUANTITY_PACKAGE, this.quantityPackage, null));
			}
			json.put(IntentConstants.INTENT_LIST_PARAM, params);
		} catch (JSONException e) {
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return json;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		saleOrderLotId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.SALE_ORDER_LOT_ID, -1);
		saleOrderId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.SALE_ORDER_ID);
		saleOrderDetailId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.SALE_ORDER_DETAIL_ID);
		createDate = CursorUtil.getString(c, SALE_ORDER_LOT_TABLE.CREATE_DATE);
		createUser = CursorUtil.getString(c, SALE_ORDER_LOT_TABLE.CREATE_USER);
		discountAmount = CursorUtil.getDouble(c, SALE_ORDER_LOT_TABLE.DISCOUNT_AMOUNT);
		discountPercent = CursorUtil.getDouble(c, SALE_ORDER_LOT_TABLE.DISCOUNT_PERCENT);
		orderDate = CursorUtil.getString(c, SALE_ORDER_LOT_TABLE.ORDER_DATE);
		price = CursorUtil.getFloat(c, SALE_ORDER_LOT_TABLE.PRICE);
		productId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.PRODUCT_ID, -1);
		quantity = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.QUANTITY);
		shopId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.SHOP_ID, -1);
		staffId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.STAFF_ID, -1);
		stockTotalId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.STOCK_TOTAL_ID);
		udpateUser = CursorUtil.getString(c, SALE_ORDER_LOT_TABLE.UPDATE_USER);
		updateDate = CursorUtil.getString(c, SALE_ORDER_LOT_TABLE.UPDATE_DATE);
		packagePrice = CursorUtil.getDouble(c, SALE_ORDER_LOT_TABLE.PACKAGE_PRICE);
		priceId = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.PRICE_ID);
		quantityRetail = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.QUANTITY_RETAIL);
		quantityPackage = CursorUtil.getLong(c, SALE_ORDER_LOT_TABLE.QUANTITY_PACKAGE);
	}
}
