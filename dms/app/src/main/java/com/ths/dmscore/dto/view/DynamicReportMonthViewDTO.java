package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * dto theo thang cho man hinh bao cao
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class DynamicReportMonthViewDTO extends AbstractDynamicReportViewDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	// mang ds row cua bao cao thang
	public ArrayList<DynamicReportMonthRowDTO> lstReportMonth;
	
	public DynamicReportMonthViewDTO() {
		lstReportMonth = new ArrayList<DynamicReportMonthRowDTO>();
		lstReport.addAll(lstReportMonth);
	}

	@Override
	public void setData() {
		// TODO Auto-generated method stub
		lstReport.addAll(lstReportMonth);
	}
	
}
