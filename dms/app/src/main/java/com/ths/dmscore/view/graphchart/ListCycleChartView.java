package com.ths.dmscore.view.graphchart;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ths.dmscore.dto.view.ChartDTO;
import com.ths.dms.R;

/**
 * Ds cac man hinh chart
 * 
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ListCycleChartView extends LinearLayout implements
		OnTouchListener, OnPageChangeListener {
	ViewPager mPager; // pager picture
	Context context;
	// co show cot kh hay ko
	public boolean isShowPlan = true;
	// co show cot thuc hien hay ko
	public boolean isShowReality = true;

	/**
	 * Co show cac cot quantity plan, plan hay ko
	 * 
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void setShowColumn(boolean isShowPlan, boolean isShowReality) {
		this.isShowPlan = isShowPlan;
		this.isShowReality = isShowReality;
	}

	public ListCycleChartView(Context context) {
		super(context);
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_list_cycle_chart_view, this, true);
		mPager = (ViewPager) view.findViewById(R.id.mpager);
		mPager.setOnTouchListener(this);
		mPager.setOnPageChangeListener(this);
	}

	/**
	 * set adapter cho pager ve hinh tron
	 * 
	 * @author: Tuanlt11
	 * @param annotation
	 * @param lstColorChart
	 * @param lstCycleChart
	 * @return: void
	 * @throws:
	 */
	public void setAdapter(String[] annotation, int[] lstColorChart,
			List<ChartDTO> lstCycleChart) {
		// kiem tra co phan tu nao lon hon 0 trong item ko, neu ko co thi ko add vao view pager
		List<ChartDTO> lstCycleChartChoose = new ArrayList<ChartDTO>();
		for(int i = 0, size = lstCycleChart.size(); i < size; i++){
			ChartDTO item = lstCycleChart.get(i);
			boolean isHaveValues = false;
			int length = item.value.length;
			for (int j = 0; j < length; j++) {
				if (item.value[j] > 0){
					isHaveValues = true;
				}
			}
			if(isHaveValues){
				lstCycleChartChoose.add(item);
			}
		}
			
		CycleChartPagerAdapter adapter = new CycleChartPagerAdapter(context,
				annotation, lstColorChart, lstCycleChartChoose);
		adapter.setShowColumn(isShowPlan, isShowReality);
		mPager.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

	double y; // toa do y touch vao hinh anh
	double x; // to do x touch vao hinh anh
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			v.getParent().requestDisallowInterceptTouchEvent(false);
			y = event.getY();
			x = event.getX();
			break;

		case MotionEvent.ACTION_UP:
			v.getParent().requestDisallowInterceptTouchEvent(false);
			break;
		case MotionEvent.ACTION_MOVE:
			if (y + 40 < event.getY() || y - 40 > event.getY()) {
				v.getParent().requestDisallowInterceptTouchEvent(false);

			}
			break;
		default:
			break;
		}
		return false;
	}

	@Override
	public void onPageSelected(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		mPager.getParent().requestDisallowInterceptTouchEvent(true);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

}
