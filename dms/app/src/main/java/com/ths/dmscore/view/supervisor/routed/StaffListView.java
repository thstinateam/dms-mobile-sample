/**
 * Copyright 2013 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.routed;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.view.control.StaffChoosenAdapter;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Popup ds nhan vien chup hinh
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class StaffListView extends RelativeLayout implements OnClickListener{

	Context context;
	BaseFragment listener;
	public View viewLayout;
	ListView lvListStaff; // ds nhan vien
	Button btClose; // button dong
	int indexChoose = 0; // luu lai vi tri chon nv

	public StaffListView(Context context, BaseFragment listener) {
		super(context);
		this.context = context;
		this.listener = listener;
		LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_list_staff_view, null);
		lvListStaff = (ListView) viewLayout
				.findViewById(R.id.lvListStaff);
		btClose = (Button) viewLayout
				.findViewById(R.id.btClose);
		btClose.setOnClickListener(this);

	}

	/**
	*  Nhan su kien tu cac row chuyen qua
	*  @author: Tuanlt11
	*  @return: void
	*  @throws:
	*/
	public void onEvent(StaffChoosenDTO item){
		//adapter.notifyDataSetChanged();
		listener.onEvent(ActionEventConstant.ACTION_CHOOSE_STAFF_TAKE_PHOTO, null, item);
	}


	/**
	 * Tim cac control co trong view
	 *
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(ArrayList<StaffChoosenDTO> listStaff, int indexChoose) {
		this.indexChoose = indexChoose;
		StaffChoosenAdapter ad = new StaffChoosenAdapter(context,
				R.layout.layout_choose_staff_item, listStaff, indexChoose);
		ad.setListener(this);
		lvListStaff.setAdapter(ad);
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		listener.onEvent(ActionEventConstant.ACTION_CLOSE_POPUP_CHOOSE_STAFF, null, null);
	}

}
