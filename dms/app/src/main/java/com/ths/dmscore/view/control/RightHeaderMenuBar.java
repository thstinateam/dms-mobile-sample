package com.ths.dmscore.view.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

public class RightHeaderMenuBar extends LinearLayout {
	public OnEventControlListener listener;
	public Context context;
	public LinearLayout view;

	public RightHeaderMenuBar(Context context) {
		super(context);
		this.context = context;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = (LinearLayout) inflater.inflate(
				R.layout.layout_right_header_menu_bar, this);

	}

	public void addMenuItem(int iconRscId, int action, int separateVisible) {
		MenuItem newItem = new MenuItem(context, iconRscId, action);
		newItem.setOnEventControlListener(listener);
		newItem.setSeparateVisible(separateVisible);
		view.addView(newItem);
	}
	
	public int addMenuItem(String text, int iconRscId, int action, int separateVisible, String menuCode) {
		MenuItem newItem = new MenuItem(context, text, iconRscId, action);
		newItem.setOnEventControlListener(listener);
		newItem.setSeparateVisible(separateVisible);
		int status = PriUtils.getInstance().checkMenu(newItem, menuCode);
		if (status == PriUtils.ENABLE) {
			view.addView(newItem);
			return 0;
		} else {
			return 1;
		}
	}
	
	public void addMenuItem(String text, int iconRscId, int action, int separateVisible) {
		MenuItem newItem = new MenuItem(context, text, iconRscId, action);
		newItem.setOnEventControlListener(listener);
		newItem.setSeparateVisible(separateVisible);
		view.addView(newItem);
	}

	public void setOnEventControlListener(OnEventControlListener lis) {
		listener = lis;
	}

	public void setMenuItemFocus(int index) {
		if (view.getChildCount() > index) {
			View viewItem = view.getChildAt(index);
			if (viewItem instanceof MenuItem) {
				MenuItem item = (MenuItem) view.getChildAt(index);
				item.setMenuSelected(true);
			}
		}
	}
	
	public void removeAllMenuItem(){
		view.removeAllViews();
	}
	
	 /**
	 * Add menu dong
	 * @author: dungdq
	 * @param text
	 * @param action
	 * @param menuID
	 * @return: void
	 * @throws:
	*/
	public void addMenuItem(String text, int action, int menuID) {
		MenuItem newItem = new MenuItem(action, menuID, context, text);
		newItem.setOnEventControlListener(listener);
		view.addView(newItem);
		
	}

}
