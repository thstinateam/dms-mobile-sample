package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

public class WrongPlanCustomerDTO {
	public ArrayList<WrongPlanCustomerItem> arrWrong = new ArrayList<WrongPlanCustomerDTO.WrongPlanCustomerItem>();

	public WrongPlanCustomerItem newWrongPlanCustomerItem() {
		return new WrongPlanCustomerItem();
	}

	public class WrongPlanCustomerItem {
		public CustomerDTO aCustomer = new CustomerDTO();
		public String plan;
		public String visitedTime;
		public long revenue;
		public String cusCodeName;

		public WrongPlanCustomerItem() {
		}

		public void initWrongPlanCusItem(Cursor c) {
			aCustomer.customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
			cusCodeName = CursorUtil.getString(c, "CUSTOMER_CODE_NAME");
			aCustomer.address = CursorUtil.getString(c, "ADDRESS");
			if (!StringUtil.isNullOrEmpty(aCustomer.address)) {
				aCustomer.street = aCustomer.address;
			} else {
				aCustomer.street = CursorUtil.getString(c, "STREET");
			}
			plan = CursorUtil.getString(c, "WEEK_PLAN");
			if (!StringUtil.isNullOrEmpty(plan) && plan.substring(0, 1).equals(",")) {
				plan = plan.substring(1, plan.length());
			}
			revenue = CursorUtil.getLong(c, "REVENUE");
			visitedTime = CursorUtil.getString(c, "START_TIME");
		}
	}
}
