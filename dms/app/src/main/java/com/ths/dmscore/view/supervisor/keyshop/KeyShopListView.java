/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.KeyShopItemDTO;
import com.ths.dmscore.dto.db.KeyShopListDTOView;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.ComboboxDisplayProgrameDTO;
import com.ths.dmscore.dto.view.CustomerListFeedbackDTO;
import com.ths.dmscore.dto.view.ShopProblemDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.control.table.DMSColSortManager.OnSortChange;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Danh sach cau lac bo role GSBH
 * 
 * @author: yennth16
 * @version: 1.0
 * @since: 10:17:53 10-07-2015
 */
public class KeyShopListView extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener, OnItemSelectedListener,
		OnSortChange {
	private DMSTableView tbKeyShop;
	KeyShopListDTOView dto;
	// MH chi tiet van de
	KeyShopDetailView followProblemDetail;
	KeyShopItemDTO dtoGoToDetail;
	// dialog product detail view
	AlertDialog alertFollowProblemDetail;
	// dialog product detail view
	AlertDialog customerListPopup;
	int page;
	// get total page or not
	boolean isGetTotalPage;
	// Ds Kh hien thi
	CustomerListFeedbackDTO customerListDto = new CustomerListFeedbackDTO();
	ArrayList<ShopProblemDTO> lstShopProlem = new ArrayList<ShopProblemDTO>();
	String shopId = "";
	boolean isFirst = false;
	int channelObjectType = 0;
	
	// don vi
	private Spinner spShop;
	private int indexSelected = -1;
	// DTO spinner NPP
	private ComboboxDisplayProgrameDTO comboboxData;
	LinearLayout llHeader;// llHeader

	public static KeyShopListView newInstance(Bundle data) {
		KeyShopListView instance = new KeyShopListView();
		instance.setArguments(data);
		return instance;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_keyshop_list_view, container,
				false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		parent.setTitleName(StringUtil
				.getString(R.string.TITLE_VIEW_SUPERVISOR_LIST_KEY_SHOP));
		channelObjectType = GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritSpecificType();
		if (channelObjectType == UserDTO.TYPE_STAFF)
			PriUtils.getInstance().genPriHashMapForForm(
					PriHashMap.PriForm.NVBH_DANHSACHCLB);
		else if (channelObjectType == UserDTO.TYPE_SUPERVISOR)
			PriUtils.getInstance().genPriHashMapForForm(
					PriHashMap.PriForm.GSNPP_DANHSACHCLB);
		else {
			PriUtils.getInstance().genPriHashMapForForm(
					PriHashMap.PriForm.TBHV_DANHSACHCLB);
		}

		tbKeyShop = (DMSTableView) v.findViewById(R.id.tbKeyShop);
		tbKeyShop.setListener(this);
		// init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
				.addInfoCaseUnSensitive(4, SortActionConstants.FROM_DATE)
				.addInfoCaseUnSensitive(5, SortActionConstants.TO_DATE).build();
		initHeaderTable(tbKeyShop, new KeyShopListRow(parent, this), lstSort,
				this);
		llHeader = (LinearLayout) v.findViewById(R.id.llHeader);
		spShop = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spUnit, PriHashMap.PriControl.GIAOVANDE_DONVI);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvUnit,
				PriHashMap.PriControl.GIAOVANDE_DONVI);
		if (channelObjectType == UserDTO.TYPE_STAFF){
			llHeader.setVisibility(View.GONE);
		}else {
			llHeader.setVisibility(View.VISIBLE);
			PriUtils.getInstance().setOnItemSelectedListener(spShop, this);
		}
		if (!isFirst) {
			shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
			if (channelObjectType == UserDTO.TYPE_STAFF){
				getListKeyshop(true, true);
			}else {
				requestGetListCombobox();
			}
			isFirst = true;
		} else {
			if (channelObjectType != UserDTO.TYPE_STAFF){
				initSpNPP();
			}
			renderData();
		}
		return v;
	}

	/**
	 * Lay danh sach don vi 
	 * @author: yennth16
	 * @since: 11:39:46 10-07-2015
	 * @return: void
	 * @throws:
	 */
	private void requestGetListCombobox() {
		parent.showLoadingDialog();
		isFirst = false;
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleViewEvent(bundle, ActionEventConstant.GET_LIST_COMBOBOX, SupervisorController.getInstance());
	}


	/**
	 * Lay danh sach cau lac bo
	 * @author: yennth16
	 * @since: 11:43:30 10-07-2015
	 * @return: void
	 * @throws:  
	 * @param repareReset
	 * @param getCombobox
	 */
	private void getListKeyshop(boolean repareReset, boolean getCombobox) {
		parent.showProgressDialog(StringUtil.getString(R.string.loading));
		Bundle data = new Bundle();
		int page = 0;
		if (!repareReset) {
			page = (tbKeyShop.getPagingControl().getCurrentPage() - 1);
		}
		String strPage = " limit " + (page * Constants.NUM_ITEM_PER_PAGE) + ","
				+ Constants.NUM_ITEM_PER_PAGE;
		data.putString(IntentConstants.INTENT_PAGE, strPage);
		data.putBoolean(IntentConstants.INTENT_CHECK_PAGGING, !repareReset);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		int tag = 0;
		if (repareReset) {
			tag = 11;
		}
		data.putSerializable(IntentConstants.INTENT_SORT_DATA,
				tbKeyShop.getSortInfo());
		handleViewEventWithTag(data,
				ActionEventConstant.GO_TO_KEY_SHOP_LIST,
				SupervisorController.getInstance(), tag);
	}
	
	/**
	 * init spinner npp
	 * @author: yennth16
	 * @since: 11:30:21 10-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNPP() {
		if(comboboxData.listShop != null){
			String [] arrMaNPP = new String[comboboxData.listShop.size()];
			int i = 0;
			for (ShopDTO shop : comboboxData.listShop) {
				if (!StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopCode + " - " + shop.shopName;
				} else if (StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopName;
				} else if (!StringUtil.isNullOrEmpty(shop.shopCode) && StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopCode;
				}else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spShop.setAdapter(adapterNPP);
			if (indexSelected < 0) {
				spShop.setSelection(0);
			} else
				spShop.setSelection(indexSelected);
		}

	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		switch (modelEvent.getActionEvent().action) {
		case ActionEventConstant.GET_LIST_COMBOBOX: {
			ComboboxDisplayProgrameDTO  comboDTO= (ComboboxDisplayProgrameDTO) modelEvent.getModelData();
			if (comboDTO != null) {
				comboboxData = comboDTO;
				initSpNPP();
			}
			break;
		}
		case ActionEventConstant.GO_TO_KEY_SHOP_LIST:
			dto = (KeyShopListDTOView) modelEvent.getModelData();
			if (modelEvent.getActionEvent().tag == 11) {
				tbKeyShop.getPagingControl().totalPage = -1;
				tbKeyShop.getPagingControl().setCurrentPage(1);
			}
			renderData();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		super.handleErrorModelViewEvent(modelEvent);
		parent.closeProgressDialog();
	}

	/**
	 * Render data
	 * @author: yennth16
	 * @since: 11:41:40 10-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderData() {
		tbKeyShop.clearAllData();
		int length = dto.arrItem.size();
		int position = (tbKeyShop.getPagingControl().getCurrentPage() - 1)
				* Constants.NUM_ITEM_PER_PAGE + 1;
		for (int i = 0; i < length; i++) {
			KeyShopListRow row = new KeyShopListRow(parent, this);
			row.render(dto.arrItem.get(i), position);
			position++;
			row.setListener(this);
			tbKeyShop.addRow(row);
		}
		if (tbKeyShop.getPagingControl().totalPage < 0)
			tbKeyShop.setTotalSize(dto.total, 1);
	}

	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		default:
			break;
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		getListKeyshop(false, false);
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		case ActionEventConstant.GO_TO_KEY_SHOP_DETAIL_VIEW: {
			// show detail problem
			dtoGoToDetail = (KeyShopItemDTO) data;
			showFollowProblemDetail(dtoGoToDetail);
			break;
		}
		case ActionEventConstant.ACTION_CLOSE_POPUP:
			if(customerListPopup != null){
				customerListPopup.dismiss();
			}
			break;
		default:
			break;
		}

	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spShop && indexSelected != spShop.getSelectedItemPosition()) {
			indexSelected = spShop.getSelectedItemPosition();
			dto = null;
			shopId = String.valueOf(comboboxData.listShop.get(spShop.getSelectedItemPosition()).shopId);
			tbKeyShop.resetSortInfo();
			getListKeyshop(true, true);
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {

	}

	/**
	 * Hien thi pop up chi tiet van de
	 * @author: yennth16
	 * @since: 11:43:05 10-07-2015
	 * @return: void
	 * @throws:  
	 * @param dto
	 */
	private void showFollowProblemDetail(KeyShopItemDTO dto) {
		if (alertFollowProblemDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			followProblemDetail = new KeyShopDetailView(parent, this);
			build.setView(followProblemDetail.viewLayout);
			alertFollowProblemDetail = build.create();
			Window window = alertFollowProblemDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		followProblemDetail.updateLayout(dto);
		alertFollowProblemDetail.show();
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				resetVar();
				tbKeyShop.resetSortInfo();
				if (channelObjectType == UserDTO.TYPE_STAFF){
					llHeader.setVisibility(View.GONE);
					getListKeyshop(true, true);
				}else {
					llHeader.setVisibility(View.VISIBLE);
					requestGetListCombobox();
				}
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/**
	 * reset cac gia tri
	 * @author: yennth16
	 * @since: 11:29:46 10-07-2015
	 * @return: void
	 * @throws:
	 */
	private void resetVar() {
		indexSelected = -1;
		shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getListKeyshop(false, false);
	}
	
	/**
	 * onClick
	 */
	public void onClick(View v) {
		if (followProblemDetail != null && followProblemDetail.btCloseKeyshopDetail == v) {
			if (alertFollowProblemDetail != null) {
				alertFollowProblemDetail.dismiss();
			}
		}
	}

}