/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.REPORT_TEMPLATE_CRITERION_TABLE;
import com.ths.dmscore.lib.sqllite.db.REPORT_TEMPLATE_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * DynamicKPIDTO.java
 *
 * @author: dungdq3
 * @version: 1.0
 * @since: 5:05:18 PM Oct 24, 2014
 */
public class DynamicKPIDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int reportTemplateID;
	private String kpiName;
	private String chuKy;
	private String typeKPI;
	public int kpiId; // id KPI trong bang report_template
	// id cua loai report link qua bang report_template_criterion
	public int typeKPIID;
	public int columnId;
	public String fromDate;
	public String toDate;
	public int totalItem;
	// list danh sach thoi gian popup
	public ArrayList<PopupChoseObjectItem> listReportColumnObjectTimeDTO = null;
	public int periodCycleType = 0; // loai chu ki
	public int parentPeriodTypeId;// loai chu ki cha
	public int numRecentParentPeriod = 0;// trong thang hien tai hoac trong 2 thang hien tai ....

	public DynamicKPIDTO() {
		// TODO Auto-generated constructor stub
		reportTemplateID = 0;
		kpiName = Constants.STR_BLANK;
		chuKy = Constants.STR_BLANK;
		typeKPI = Constants.STR_BLANK;
		columnId = 0;
		fromDate = Constants.STR_BLANK;
		toDate = Constants.STR_BLANK;
		totalItem = 0;
		listReportColumnObjectTimeDTO = new ArrayList<PopupChoseObjectItem>();
	}

	public int getReportTemplateID() {
		return reportTemplateID;
	}

	public void setReportTemplateID(int kpiID) {
		this.reportTemplateID = kpiID;
	}

	public String getKpiName() {
		return kpiName;
	}

	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}

	public String getChuKy() {
		return chuKy;
	}

	public void setChuKy(String chuKy) {
		this.chuKy = chuKy;
	}

	public String getTypeKPI() {
		return typeKPI;
	}

	public void setTypeKPI(String typeKPI) {
		this.typeKPI = typeKPI;
	}

	/**
	 * init from cursor
	 *
	 * @author: dungdq3
	 * @since: 5:10:43 PM Oct 24, 2014
	 * @return: void
	 * @throws:
	 * @param c
	 *
	 */
	public void initFromCursor(Cursor c) {
		kpiName = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.NAME);
		reportTemplateID = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.REPORT_TEMPLATE_ID);
		typeKPI = CursorUtil.getString(c, "CRITIAL_NAME");
		kpiId = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.KPI_ID);
		chuKy = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.PERIOD_TYPE_ID);
		typeKPIID = CursorUtil.getInt(c, REPORT_TEMPLATE_CRITERION_TABLE.REPORT_TEMPLATE_CRITERION_ID);
		columnId = CursorUtil.getInt(c, "CRI_COLUMN_ID");
		parentPeriodTypeId = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.PARENT_PERIOD_TYPE_ID);
		numRecentParentPeriod = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.NUM_RECENT_PARENT_PERIOD);

		periodCycleType = CursorUtil.getInt(c, REPORT_TEMPLATE_TABLE.PERIOD_CYCLE_TYPE);
		fromDate = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.FROM_DATE);
		toDate = CursorUtil.getString(c, REPORT_TEMPLATE_TABLE.TO_DATE);

	}

}
