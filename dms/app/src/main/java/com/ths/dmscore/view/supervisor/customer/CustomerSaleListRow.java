/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.customer;

import java.util.Calendar;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;
/**
 * Row hien thi thong tin danh sach diem ban trong nha phan phoi
 * CustomerSaleListRow.java
 * @author: hoanpd1
 * @version: 1.0
 * @since:  10:25:28 05-03-2015
 */
public class CustomerSaleListRow extends DMSTableRow implements OnClickListener {

	//stt
	public TextView tvSTT;
	//nhan vien ban hang
	public TextView tvNVBH;
	//ten khach hang
	public TextView tvCustomer;
	//dia chi khach hang
	public TextView tvAddress;
	//ngay udpate toa do
	public TextView tvDateUpdate;
	//so lan update toa do
	public TextView tvNumUpdate;
	//icon ban do, cho phep dat hang tu xa
	public ImageView ivMap;
	public ImageView ivAllowOrder;
	public LinearLayout llMap;
	//dong du lieu null
	public TextView tvNull;
	//ngay hien tai
	Calendar calendar = Calendar.getInstance();
	String sToday = DateUtils.defaultDateFormat.format(calendar.getTime());
	private CustomerListItem data;

	public CustomerSaleListRow(Context context, VinamilkTableListener lis) {
		super(context, R.layout.layout_customer_sale_list_row, GlobalInfo
				.getInstance().getAllowDistanceOrder() == Constants.TYPE_ALLOW_DISTANCE_ORDER ? null
				: new int[] { R.id.llAllowOrder });
		listener = lis;
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvNVBH = (TextView) findViewById(R.id.tvNVBH);
		tvCustomer = (TextView) findViewById(R.id.tvCustomer);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
		tvDateUpdate = (TextView) findViewById(R.id.tvDateUpdate);
		tvNumUpdate = (TextView) PriUtils.getInstance()
				.findViewByIdNotAllowInvisible(this, R.id.tvNumUpdate,
						PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_SLCN);
		tvNull = (TextView) findViewById(R.id.tvNull);
		ivMap = (ImageView) PriUtils.getInstance()
				.findViewByIdNotAllowInvisible(this, R.id.ivMap,
						PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_BANDO);
		ivAllowOrder =  (ImageView)findViewById(R.id.ivAllowOrder);
		llMap = (LinearLayout) findViewById(R.id.llMap);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v ==  tvCustomer) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_CUSTOMER_INFO, null, data);
		}
	}

	/**
	 * reder layout
	 * @author: hoanpd1
	 * @since: 10:26:15 05-03-2015
	 * @return: void
	 * @throws:
	 * @param pos
	 * @param item
	 */
	public void render(int pos, CustomerListItem item) {
		this.data = item;
		tvSTT.setText("" + pos);
		if(!StringUtil.isNullOrEmpty(item.staffSale.name) && !StringUtil.isNullOrEmpty(item.staffSale.staffCode)){
			SpannableObject obj = new SpannableObject();
			obj.addSpan(item.staffSale.name, ImageUtil.getColor(R.color.BLACK),
					android.graphics.Typeface.NORMAL);
			obj.addSpan("\n" + item.staffSale.staffCode, ImageUtil.getColor(R.color.COLOR_USER_NAME),
					android.graphics.Typeface.NORMAL);
			tvNVBH.setText(obj.getSpan());
		}
		tvCustomer.setText(item.aCustomer.getCustomerCode() + " - " + item.aCustomer.getCustomerName());
		tvCustomer.setOnClickListener(this);
		tvCustomer.setTextColor(ImageUtil.getColor(R.color.COLOR_USER_NAME));
		tvAddress.setText(item.aCustomer.address);
		if(item.numUpdatePosition > 0 ){
			SpannableString content = new SpannableString("" + item.numUpdatePosition);
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			content.setSpan(new ForegroundColorSpan(ImageUtil.getColor(R.color.COLOR_LINK)), 0, content.length(), 0);
			tvNumUpdate.setText(content);
		}else{
			tvNumUpdate.setTextColor(ImageUtil.getColor(R.color.BLACK));
			tvNumUpdate.setText("" + item.numUpdatePosition);
		}
		tvDateUpdate.setText(item.lastUpdatePosition);
		if(item.aCustomer.getLat() > 0 && item.aCustomer.getLng() > 0){
			ivMap.setVisibility(View.VISIBLE);
		}else{
			ivMap.setVisibility(View.GONE);
		}
		if(!StringUtil.isNullOrEmpty(item.exceptionOrderDate) && sToday.equals(item.exceptionOrderDate)){
			setImageResource(R.drawable.icon_check, ivAllowOrder);
		}else{
			setImageResource(R.drawable.icon_check_gray, ivAllowOrder);
		}
	}

}
