/*
 * Copyright 2013 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.model.UserModel;
import com.ths.dmscore.view.main.GCMCheckRoleActivity;
import com.ths.dms.R;

/**
 * GCM intent Service
 * @author: DungDQ3
 * @version: 1.1
 * @since: 1.0
 */
public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(Constants.SENDER_ID);
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	SharedPreferences sharedPreferences;

	@Override
	protected void onRegistered(Context context, String registrationId) {
		ServerUtilities.register(context, registrationId);

		HTTPRequest request= UserModel.getInstance().requestRegisterId(registrationId);
		if (request.getErrorCode()==0) {
//			sharedPreferences = getSharedPreferences(getString(R.string.app_name),
//					MODE_PRIVATE);
			sharedPreferences =  GlobalInfo.getInstance().getDmsPrivateSharePreference();
			sharedPreferences.edit()
					.putString(IntentConstants.INTENT_REGID, registrationId)
					.commit();
		}
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {

		if (GCMRegistrar.isRegisteredOnServer(context)) {
			ServerUtilities.unregister(context, registrationId);
		}
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Object data = intent.getExtras().get("data");
		if (data != null) {
			String message = data.toString();
			String title = "";
			String content = "";
			JSONObject temp;
			try {
				temp = new JSONObject(message);
				title = (String) temp.get("title".toString());
				content = (String) temp.get("content".toString());
			} catch (JSONException e) {
				MyLog.e("onMessage", "fail", e);
			}
			// notifies user
			generateNotification(context, content, title);
		}
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		String message = getString(R.string.TEXT_MESSAGE_ERROR_PASSWORD_WRONG, total);
		CommonUtilities.displayMessage(context, message);
		String title1 = "";
		String content = "";
		JSONObject temp;
		try {
			temp = new JSONObject(message);
			title1 = (String) temp.get("title".toString());
			content = (String) temp.get("content".toString());
		} catch (JSONException e) {
			MyLog.e("onDeletedMessages", "fail", e);
		}
		generateNotification(context, content, title1);
	}

	@Override
	public void onError(Context context, String errorId) {
		//CommonUtilities.displayMessage(context,
		//		getString(R.string.ERROR_ACCOUNT_LOCKED, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		//CommonUtilities.displayMessage(context,
		//		getString(R.string.ERROR_INVALID_IMEI_OR_SERIAL, errorId));
		return super.onRecoverableError(context, errorId);
	}

	static int stt = 0;

	/**
	* Tao notification de hien thi.
	* @author dungdq
	* @param: context
	* @param: message
	*/
	public static void generateNotification(Context context, String message,
			String title) {
		Intent checkRole=new Intent(context, GCMCheckRoleActivity.class);
		
		checkRole.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		checkRole.putExtra(CommonUtilities.EXTRA_MESSAGE, message);
		
		PendingIntent notifyIntent = PendingIntent.getActivity(context, stt,
				checkRole, PendingIntent.FLAG_UPDATE_CURRENT);
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				context).setSmallIcon(R.drawable.icon_app)
				.setContentTitle(title).setContentText(message)
				.setWhen(System.currentTimeMillis()).setAutoCancel(true);

		mBuilder.setContentIntent(notifyIntent);
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(stt, mBuilder.getNotification());
		stt++;
	}

}
