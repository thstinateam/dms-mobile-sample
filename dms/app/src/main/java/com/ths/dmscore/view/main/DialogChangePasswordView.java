/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.Vector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dms.R;

public class DialogChangePasswordView extends ScrollView implements OnClickListener{
	private GlobalBaseActivity parent;
	public View viewLayout;
	private int actionChangePass = -1;
	private VNMEditTextClearable edMKcu;
	private VNMEditTextClearable edMKmoi;
	private VNMEditTextClearable edMKxacnhan;
	private Button btChangePass;
	private TextView tvError;
	private String userName;

	public DialogChangePasswordView(Context context, int actionChangePass, String userName) {
		super(context);
		this.actionChangePass = actionChangePass;
		this.userName = userName;
		parent = (GlobalBaseActivity) context;
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_change_password_dialog_view, null);
		edMKcu = (VNMEditTextClearable) viewLayout.findViewById(R.id.edMKcu);
		edMKmoi = (VNMEditTextClearable) viewLayout.findViewById(R.id.edMKmoi);
		edMKxacnhan = (VNMEditTextClearable) viewLayout.findViewById(R.id.edMKxacnhan);
		tvError = (TextView) viewLayout.findViewById(R.id.tvError);
		btChangePass = (Button) viewLayout.findViewById(R.id.btChangePass);
		btChangePass.setOnClickListener(this);
//		edMKcu.setNextFocusDownId(edMKmoi.getId());
//		edMKmoi.setNextFocusDownId(edMKxacnhan.getId());
	}
	
	@Override
	public void onClick(View v) {
		if (v == btChangePass) {
			renderError("");
			if (checkPolicyPassword()) {
				try {
					Vector<String> data = new Vector<String>();
					String oldPass = edMKcu.getText().toString();
					data.add(oldPass);
					String newPass = edMKmoi.getText().toString().trim();
					data.add(newPass);
					parent.onEvent(actionChangePass, this, data);
				} catch (Exception e) {
					MyLog.e("Change Pass", e);
				}
			} else{
				
			}
		}
	}
	
	public void renderError(String errorString){
		tvError.setText(errorString);
	}

	private boolean checkPolicyPassword() {
		boolean res = false;
		String passOld = edMKcu.getText().toString();
		String pass = edMKmoi.getText().toString();
		String pass2 = edMKxacnhan.getText().toString();
		if (StringUtil.isNullOrEmpty(passOld)) {
			renderError(StringUtil.getString(R.string.TEXT_PASSWORD_OLD_NOT_INPUT));
			edMKcu.requestFocus();
		}else if (!pass.equals(pass2)) {
			renderError(StringUtil.getString(R.string.TEXT_WRONG_CONFIRMED_PASS));
			edMKmoi.requestFocus();
		} else if (!StringUtil.checkPolicyPassword(pass)) {
			renderError(StringUtil.getString(R.string.TEXT_PASSWORD_POLICY_FAIL));
			edMKmoi.requestFocus();
		} else if (passOld.equals(pass)) {
			renderError(StringUtil.getString(R.string.TEXT_PASSWORD_OLD_NOT_SAME_NEW));
			edMKmoi.requestFocus();
		} else{
			res = true;
		}
		return res;
	}
}
