package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.SHOP_LOCK_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * Mo ta muc dich cua class
 * 
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

@SuppressWarnings("serial")
public class ShopLockDTO extends AbstractTableDTO {

	public int shopLockID;
	public String lockedDate;
	public String lockDate;
	public String createDate;
	public int createUser;
	// id NPP
	public int shopId;
	public int staffLock;
	public int shopLock;

	public ShopLockDTO() {
		super(TableType.SHOP_LOCK_TABLE);
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initFromCursor(Cursor c) {
		shopLockID = CursorUtil.getInt(c, SHOP_LOCK_TABLE.SHOP_LOCK_ID);
		lockedDate = CursorUtil.getString(c, SHOP_LOCK_TABLE.LOCKED_DATE);
		lockDate = CursorUtil.getString(c, SHOP_LOCK_TABLE.LOCK_DATE);
		createDate = CursorUtil.getString(c, SHOP_LOCK_TABLE.CREATE_DATE);
		createUser = CursorUtil.getInt(c, SHOP_LOCK_TABLE.CREATE_USER);
		shopId = CursorUtil.getInt(c, SHOP_LOCK_TABLE.SHOP_ID);
		staffLock = CursorUtil.getInt(c, SHOP_LOCK_TABLE.STAFF_LOCK);
		shopLock = CursorUtil.getInt(c, SHOP_LOCK_TABLE.SHOP_LOCK);
	}

}
