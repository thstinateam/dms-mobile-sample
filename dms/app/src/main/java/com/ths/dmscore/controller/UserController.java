/**
R * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;

/**
 *  Controller giao tiep cua User
 *  @author: DoanDM
 *  @version: 1.1
 *  @since: 1.0
 */
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.model.UserModel;
import com.ths.dmscore.view.main.ChangePasswordView;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.main.SalesPersonActivity;
import com.ths.dmscore.view.main.SupervisorActivity;
import com.ths.dmscore.view.main.TBHVActivity;
import com.ths.dmscore.view.sale.customer.CustomerLocationUpdateView;
import com.ths.dmscore.view.sale.customer.CustomerRouteView;
import com.ths.dmscore.view.sale.order.FindProductAddOrderListView;
import com.ths.dmscore.view.sale.order.ListOrderView;
import com.ths.dmscore.view.sale.order.PaymentOrderBillingView;
import com.ths.dmscore.view.sale.salestatistics.SaleStatisticsInDayVanSalesView;
import com.ths.dmscore.view.sale.statistic.NVBHReportForcusProductView;
import com.ths.dmscore.view.sale.statistic.NoteListView;
import com.ths.dmscore.view.sale.statistic.ReportAmountCategoryView;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.statistic.GeneralStatisticsView;

public class UserController extends AbstractController {

	static volatile UserController instance;

	protected UserController() {
	}

	public static UserController getInstance() {
		if (instance == null) {
			instance = new UserController();
		}
		return instance;
	}

	@Override
	public void handleViewEvent(final ActionEvent e) {
		if (e.isUsingAsyntask) {
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {
					//cap nhat thoi gian chay cua event
					e.startTimeFromBootActive = SystemClock.elapsedRealtime();
					UserModel.getInstance().handleControllerEvent(
							UserController.this, e);
					GlobalBaseActivity base = null;
					if (e.sender instanceof Activity) {
						base = (GlobalBaseActivity) e.sender;
					} else if (e.sender instanceof Fragment) {
						base = (GlobalBaseActivity) ((Fragment) e.sender).getActivity();
					}
					if (e.request != null && base != null) {
						base.addProcessingRequest(e.request, e.isBlockRequest);
					}
					return null;
				}
			};
			task.execute();
		} else {
			UserModel.getInstance().handleControllerEvent(UserController.this,
					e);
		}
	}

	/**
	 *
	 * Xu ly du lieu tra ve tu model
	 *
	 * @author: DoanDM
	 * @param modelEvent
	 * @return: void
	 * @throws:
	 */
	public void handleModelEvent(final ModelEvent modelEvent) {
		if (modelEvent.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
			final ActionEvent e = modelEvent.getActionEvent();
			HTTPRequest request = e.request;
			if (e.sender != null && (request == null || (request != null && request.isAlive()))) {
				if (e.sender instanceof GlobalBaseActivity) {
					final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
					if (sender == null || sender.isFinished)
						return;
					sender.runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.isFinished)
								return;
							sender.handleModelViewEvent(modelEvent);
						}
					});
				} else if (e.sender instanceof BaseFragment) {
					final BaseFragment sender = (BaseFragment) e.sender;
					if (sender == null || sender.getActivity() == null || sender.isFinished) {
						return;
					}
					sender.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.getActivity() == null || sender.isFinished) {
								return;
							}
							sender.handleModelViewEvent(modelEvent);
						}
					});
				}
			} else {
				modelEvent.setIsSendLog(false);
				handleErrorModelEvent(modelEvent);
			}
		} else {
			handleErrorModelEvent(modelEvent);
		}
	}

	@Override
	public void handleSwitchActivity(ActionEvent e) {
		Intent intent;
		Bundle extras;
		GlobalBaseActivity sender;

		switch (e.action) {
		case ActionEventConstant.GO_TO_SALE_PERSON_VIEW:
			sender = (GlobalBaseActivity) e.sender;
			extras = (Bundle) e.viewData;

			intent = new Intent(sender, SalesPersonActivity.class);
			if (extras != null) {
				intent.putExtras(extras);
			}
			sender.startActivity(intent);
			break;
		case ActionEventConstant.GO_TO_SUPERVISOR_VIEW:
			sender = (GlobalBaseActivity) e.sender;
			extras = (Bundle) e.viewData;
			intent = new Intent(sender, SupervisorActivity.class);
			if (extras != null) {
				intent.putExtras(extras);
			}
			sender.startActivity(intent);
			break;
		case ActionEventConstant.GO_TO_TBHV_VIEW:
			sender = (GlobalBaseActivity) e.sender;
			extras = (Bundle) e.viewData;
			intent = new Intent(sender, TBHVActivity.class);
			if (extras != null) {
				intent.putExtras(extras);
			}
			sender.startActivity(intent);
			break;
		}
	}

	@Override
	public boolean handleSwitchFragment(ActionEvent e) {
		Activity base = null;
		if (e.sender instanceof Activity) {
			base = (Activity) e.sender;
		} else if (e.sender instanceof Fragment) {
			base = ((Fragment) e.sender).getActivity();
		}
		
		boolean resultSwitch = false;
		if (base != null && !base.isFinishing()) {
			//an ban phim truoc khi chuyen man hinh
			GlobalUtil.forceHideKeyboard(base);
			
			//chuyen man hinh
			boolean isRemoveAllBackStack = false;
			BaseFragment frag = null;
			Bundle data = (e.viewData instanceof Bundle) ? (Bundle)e.viewData : null;
			if (data != null) {
				//put addition info switch action
				data.putInt(IntentConstants.INTENT_SWITCH_ACTION, e.action);
			}
			
			switch (e.action) {
//			case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY: {
//				isRemoveAllBackStack = true;
//				frag = ReportDisplayProgressDetailDayView.newInstance(new Bundle());
//				break;
//			}
			case ActionEventConstant.GO_TO_LIST_ORDER: {
				isRemoveAllBackStack = true;
				frag = ListOrderView.newInstance(data);
				break;
			}
			case ActionEventConstant.GOTO_CUSTOMER_LOCATION: {
				isRemoveAllBackStack = false;
				frag = CustomerLocationUpdateView.newInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_GENERAL_STATISTICS: {
				isRemoveAllBackStack = true;
				frag = GeneralStatisticsView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_LIST_PRODUCTS_ADD_ORDER_LIST: {
				isRemoveAllBackStack = false;
				frag = FindProductAddOrderListView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_PAYMENT_ORDER_BILLING: {
				isRemoveAllBackStack = false;
				frag = PaymentOrderBillingView.getInstance(data);
				break;

			}
//			case ActionEventConstant.GO_TO_DISPLAY_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = DisplayProgramView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_LIST_CUSTOMER_ATTEND_PROGRAM: {
//				isRemoveAllBackStack = false;
//				frag = ListCustomerAttentProgram.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_PRODUCT_LIST: {
//				isRemoveAllBackStack = true;
//				frag = ProductListView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_LOAD_IMAGE_FULL: {
//				isRemoveAllBackStack = false;
//				frag = FullImageView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_STATISTICS_TOTAL_PRODUCT: {
//				isRemoveAllBackStack = false;
//				frag = StatisticsTotalProductsView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_INDEBTEDNESS_LIST_VIEW: {
//				isRemoveAllBackStack = false;
//				frag = IndebtednessListView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_PROMOTION_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = PromotionProgramView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_CUSTOMER_ROUTE: {
				isRemoveAllBackStack = true;
				frag = CustomerRouteView.getInstance(data);
				break;
			}
//			case ActionEventConstant.GET_GSNPP_ROUTE_SUPERVISION:
//				isRemoveAllBackStack = true;
//				frag = GsnppRouteSupervisionView.newInstance(data);
//				break;
			case ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW:{
				isRemoveAllBackStack = true;
				frag = NVBHReportForcusProductView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW:{
				isRemoveAllBackStack = true;
				frag = NoteListView.newInstance(data);
				break;
			}
			case ActionEventConstant.CHANGE_PASS: {
				isRemoveAllBackStack = false;
				frag = ChangePasswordView.newInstance();
				Fragment existsFrag = base.getFragmentManager().findFragmentByTag(frag.getTAG());
				if (existsFrag != null) {
					frag = null;
				}
				break;
			}
//			case ActionEventConstant.GO_TO_DOCUMENT: {
//				isRemoveAllBackStack = true;
//				frag = DocumentView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_SALE_STATISTICS_PRODUCT_VIEW_IN_DAY_VAL: {
				isRemoveAllBackStack = true;
				frag = SaleStatisticsInDayVanSalesView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_AMOUNT_CATEGORY: {
				isRemoveAllBackStack = true;
				frag = ReportAmountCategoryView.getInstance(data);
				break;
			}
			case ActionEventConstant.GO_TO_REPORT_QUANTITY_CATEGORY: {
				isRemoveAllBackStack = true;
				frag = ReportAmountCategoryView.getInstance(data);
				break;
			}
			}
			
			if (frag != null) {
				resultSwitch  = switchFragment(base, frag, isRemoveAllBackStack);
			}
		}
		
		return resultSwitch;
	}

}
