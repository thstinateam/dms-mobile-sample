/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dms.R;

/**
 * View hien thi danh sach ung dung khong cho phep cai dat
 * @author: yennth16
 * @version: 1.0
 * @since: 18:56:07 25-12-2014
 */
public class BlackListAppView extends ScrollView implements OnClickListener {
	private GlobalBaseActivity parent;
	public View viewLayout;
	private DMSTableView tbApp;
	private int actionSelectedApp = -1;
	private TextView tvTitleApp;
	private ImageView ivIconMenu;

	public BlackListAppView(Context context, int actionSelectedApp) {
		super(context);
		this.actionSelectedApp = actionSelectedApp;
		parent = (GlobalBaseActivity) context;
		LayoutInflater inflater = parent.getLayoutInflater();
		viewLayout = inflater
				.inflate(R.layout.layout_black_list_app_view, null);
		tbApp = (DMSTableView) viewLayout.findViewById(R.id.tbApp);
		tvTitleApp = (TextView) viewLayout.findViewById(R.id.tvTitleApp);
		ivIconMenu = (ImageView) viewLayout.findViewById(R.id.ivIconMenu);
		ivIconMenu.setOnClickListener(this);
		tbApp.addHeader(new BlackListAppRow(parent,0));
	}

	/**
	 * render nhung row ung dung can xoa
	 * @author: Tuanlt11
	 * @param listApp
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(final List<ApplicationInfo> listApp) {
		tbApp.clearAllData();
		tvTitleApp.setText(StringUtil.getString(R.string.TEXT_BLACK_LIST));
		int pos = 1;
		if (listApp != null && listApp.size() > 0) {
			for (int i = 0, s = listApp.size(); i < s; i++) {
				BlackListAppRow row = new BlackListAppRow(parent,
						actionSelectedApp);
				row.renderLayout(pos, listApp.get(i));
				pos++;
				tbApp.addRow(row);
			}
		}
	}


	@Override
	public void onClick(View v) {
		if (v == ivIconMenu) {
			parent.onEvent(ActionEventConstant.ACTION_REFRESH_APP_UNINSTALL,
					ivIconMenu, null);
		}
	}
}