package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.view.HistoryItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

public class HistoryRow extends DMSTableRow implements OnClickListener {
	public TextView tvStt;
	public TextView tvSKU;
	public TextView tvAmount;
	public TextView tvQuantity;

	public HistoryRow(Context context) {
		super(context, R.layout.layout_history_row,760, GlobalInfo.getInstance()
				.isSysShowPrice() ? null : new int[] { R.id.tvAmount });
		setOnClickListener(this);
		tvStt = (TextView) findViewById(R.id.tvStt);
		tvSKU = (TextView) findViewById(R.id.tvSKU);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
	}

	 /**
	 * render du lieu
	 * @author: Tuanlt11
	 * @param historyItem
	 * @return: void
	 * @throws:
	*/
	public void render(HistoryItemDTO historyItem) {
		tvStt.setText("" + historyItem.STT);
		tvSKU.setText(historyItem.sku);
		Double amountDone = 0.0;
		long quantityDone = 0;
		if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED){
			amountDone = historyItem.amountApproved;
			quantityDone = historyItem.quantityApproved;
		}else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING){
			amountDone = historyItem.amountPending;
			quantityDone = historyItem.quantityPending;
		}else{
			amountDone = historyItem.amountDone;
			quantityDone = historyItem.quantityDone;
		}

		display(tvAmount, amountDone);
		display(tvQuantity, quantityDone);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}
}
