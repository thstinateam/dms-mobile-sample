/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;


/**
 * DTO thiet bi
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  08:14:37 27-12-2014
 */
public class EquipmentDTO  extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;

	//id thiet bi
	public long equipId;
	// id nhom thiet bi
	public long equipGroupId;
	// id nha cung cap
	public long equipProviderId;
	// ma thiet bi
	public String codeEquip;
	// so serial thiet bi
	public String seriEquip;
	// trang thai thiet bi
	public int status;
	// trang thai giao dich
	public int usageStatus;
	// tinh trang thiet bi ( moi , cu ...)
	public String healthStatus;
	// trang thai gaio dich
	public int tradeStatus;
	// loai gioa nhan
	public int tradeType;
	// ma kho
	public String stockCode;
	// loai kho
	public int stockType;
	// id kho
	public long stockId;
	// ngay het han bao hanh
	public String warrantyExpiredDate;
	// nam san xuat
	public int manufacturingYear;
	// gia
	public int price;
	// ngay dau dua vao su dung
	public String firstDateInUse;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	// id bien ban
	public long equipImportRecordId;
	
	public EquipmentDTO() {
		super(TableType.EQUIPMENT_TABLE);
	}
	
	public void parseDataEquipment(Cursor c) throws Exception {
		try {
			equipId = CursorUtil.getInt(c, "ID_EQUIP");
			equipGroupId =CursorUtil.getLong (c, "ID_EQUIP_GROUP");
			equipProviderId = CursorUtil.getLong(c, "ID_EQUIP_PROVIDER");
			codeEquip = CursorUtil.getString(c, "CODE_EQUIP");
			stockCode = CursorUtil.getString(c, "STOCK_CODE");
			seriEquip = CursorUtil.getString(c, "SERIAL_EQUIP");
			usageStatus = CursorUtil.getInt(c, "USAGE_STATUS", -1);
			healthStatus = CursorUtil.getString(c, "HEALTH_STATUS");
			tradeStatus = CursorUtil.getInt(c, "TRADE_STATUS", -1);
			tradeType = CursorUtil.getInt(c, "TRADE_TYPE");
			stockCode = CursorUtil.getString(c, "STOCK_CODE");
			stockType = CursorUtil.getInt(c, "STOCK_TYPE");
			stockId = CursorUtil.getLong(c, "STOCK_ID");
			warrantyExpiredDate = CursorUtil.getString(c, "WARRANTY_EXPRIRED_DATE");
		} catch (Exception ex) {
			throw ex;
		}
	}
}
