/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto.NoSuccessSaleOrderItem;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 *  Row trong popup dong hang loi
 *  @author: HieuNH
 *  @version: 1.0
 *  @since: 1.0
 */
public class NoSuccessSaleOrderRow extends DMSTableRow implements OnClickListener {
	private TextView tvSTT;
	private TextView tvOrderNum;
	private TextView tvCusCode;
	private TextView tvTenKH;
	private TextView tvTotal;
	private TextView tvDescription;

	public NoSuccessSaleOrderRow(Context context, boolean isShowPrice) {
		super(context, R.layout.layout_no_success_order_list_row,
				(isShowPrice ? null : new int[]{ R.id.tvTotal }));
		tvSTT = (TextView) this.findViewById(R.id.tvSTT);
		tvOrderNum = (TextView) this.findViewById(R.id.tvOrderNum);
		tvCusCode = (TextView) this.findViewById(R.id.tvCusCode);
		tvTenKH = (TextView) this.findViewById(R.id.tvTenKH);
		tvTotal = (TextView) this.findViewById(R.id.tvTotal);
		tvDescription = (TextView) this.findViewById(R.id.tvDescription);

		if (!isShowPrice) {
			int widthTotal = GlobalUtil.dip2Pixel(160);
			tvTenKH.getLayoutParams().width += widthTotal;
		}
	}

	public void renderLayout(int pos, NoSuccessSaleOrderItem item) {
		String synState = String.valueOf(item.saleOrder.synState);
		display(tvSTT, pos);
		display(tvOrderNum, item.saleOrder.orderNumber);
		display(tvCusCode, item.customer.customerCode);
		display(tvTenKH, item.customer.customerName);
		display(tvTotal, item.saleOrder.getTotal());
		//fix bug hien thi trang thai don hang tren ds don hang va canh bao khac nhau
		if(item.saleOrder.approved == 0 || (item.saleOrder.approved == 1 && !synState.equals(LogDTO.STATE_SUCCESS) )){//Don hang da chuyen len server
			if(synState.equals(LogDTO.STATE_NEW)) {// Chua gui
				display(tvDescription, StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND));
			} else if(synState.equals(LogDTO.STATE_FAIL)) {// Dang gui
				display(tvDescription, StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND));
			} else if(synState.equals(LogDTO.STATE_SUCCESS)) {
				if (DateUtils.compareWithNow(item.saleOrder.orderDate, "dd/MM/yyyy") == -1){
					display(tvDescription, StringUtil.getString(R.string.TEXT_OVERDATE_NOT_APROVE));
				}else{
					display(tvDescription, StringUtil.getString(R.string.TEXT_WAITING_PROCESS));
				}
			} else if(synState.equals(LogDTO.STATE_INVALID_TIME)) {//Sai thoi gian
				display(tvDescription, StringUtil.getString(R.string.TEXT_ORDER_WRONG));
			} else if(synState.equals(LogDTO.STATE_UNIQUE_CONTRAINTS)) {//Duplicate don hang
				display(tvDescription, StringUtil.getString(R.string.TEXT_ORDER_WRONG));
			}
		} else if (item.saleOrder.approved == 1){
			display(tvDescription, StringUtil.getString(R.string.TEXT_SUCCESS));
		} else if (item.saleOrder.approved == 2){
			display(tvDescription,item.saleOrder.description);
		} else if (item.saleOrder.approved == 3){
			display(tvDescription, StringUtil.getString(R.string.TEXT_BUTTON_CANCEL));
		} else if (item.saleOrder.approved == 4){
			// don huy sau chot
			display(tvDescription, StringUtil.getString(R.string.TEXT_CANCEL_AFTER_LOCK));
		} else if (item.saleOrder.approved == -1) {// don hang draft
			display(tvDescription, StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_UNFINISH));
		}else {
			display(tvDescription,item.saleOrder.description);
		}
	}

	@Override
	public void onClick(View v) {

	}

}
