/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_POSITION_LOG_TABLE;
import com.ths.dmscore.util.GlobalUtil;


/**
 *  Luu thong tin lich su cap nhat vi tri khach hang
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class CustomerPositionLogDTO extends AbstractTableDTO{
	private static final long serialVersionUID = 1950866232926778182L;
	// id row
	public int id;
	// staffId
	public int staffId;
	// ten khach hang
	public long customerId;
	// lat
	public double lat;
	//lng
	public double lng;
	//status
	public int status;
	//createdate
	public String createDate;
	//syndate
	public int synState;

	public CustomerPositionLogDTO() {
		super(TableType.CUSTOMER_POSITION_LOG);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public int getSynState() {
		return synState;
	}

	public void setSynState(int synState) {
		this.synState = synState;
	}

	public String getCustomerId() {
		return Long.toString(customerId);
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
	
	/**
	 * generate cau lenh update location trong customerinfo
	 * @author : BangHN
	 * since : 1.0
	 */
	public JSONObject generateInsertCustomerPositionLogSql() {
		JSONObject updateLocationJson = new JSONObject();
		try {
			updateLocationJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			updateLocationJson.put(IntentConstants.INTENT_TABLE_NAME,
					CUSTOMER_POSITION_LOG_TABLE.CUSTOMER_POSITION_LOG_TABLE);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.CUSTOMER_POSITION_LOG_ID,"CUSTOMER_POSITION_LOG_SEQ", 
					DATA_TYPE.SEQUENCE.toString()));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.STAFF_ID, staffId,
					null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.CUSTOMER_ID, customerId,
					null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.LAT, lat,
					null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.LNG, lng,
					null));
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.LAT, lat, lat == Constants.LAT_LNG_NULL ? DATA_TYPE.NULL.toString(): null));
//			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.LNG, lng, lng == Constants.LAT_LNG_NULL ? DATA_TYPE.NULL.toString(): null));
			detailPara.put(GlobalUtil.getJsonColumn(CUSTOMER_POSITION_LOG_TABLE.CREATE_DATE, createDate,
					null));
			updateLocationJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}
		return updateLocationJson;
	}
}
