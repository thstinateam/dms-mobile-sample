/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.lib.sqllite.db.RPT_DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.lib.sqllite.db.SHOP_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Mo ta muc dich cua lop (interface)
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class TBHVDisProComProgReportItem implements Serializable {
	private static final long serialVersionUID = -8228190424003264546L;
	// id nha phan phoi
	public String idNPP;
	// id giam sat nha phan phoi
	public String idGSNPP;
	// id chuong trinh Trung bay
	public String programId;
	// code nha phan phoi
	public String codeNPP;
	// ma giam sat nha phan phoi
	public String codeGSNPP;
	// ma CTTB
	public String programCode;
	// ma short CTTB
	public String programShortCode;
	// ma Level
	public String programLevel;
	// ten nha phan phoi
	public String nameNPP;
	// ten giam sat nha phan phoi
	public String nameGSNPP;
	// ten CTTB
	public String programName;
	// ten short CTTB
	public String programShortName;
	// thoi gian
	public String dateFromTo;
	// so chua phat sinh doanh so
	public String resultNumber;
	// so tham gia
	public String joinNumber;
	public ArrayList<TBHVDisProComProgReportItemResult> arrLevelCode;
	public TBHVDisProComProgReportItemResult itemResultTotal;

	public TBHVDisProComProgReportItem() {
		arrLevelCode = new ArrayList<TBHVDisProComProgReportItemResult>();
		itemResultTotal = new TBHVDisProComProgReportItemResult();
		nameNPP = "";
		nameGSNPP = "";
		programCode = "";
		programName = "";
		dateFromTo = "";
	}

	public TBHVDisProComProgReportItem(Cursor c) {
		idNPP = CursorUtil.getString(c, SHOP_TABLE.SHOP_ID);
		codeNPP = CursorUtil.getString(c, SHOP_TABLE.SHOP_CODE);
		nameNPP = CursorUtil.getString(c, SHOP_TABLE.SHOP_NAME);
		idGSNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_ID);
		codeGSNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		nameGSNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		programCode = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_CODE);
		programName = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_NAME);
		programLevel = CursorUtil.getString(c , RPT_DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAME_LEVEL);
	}

	/**
	 * new DisProComProgReportItemResult
	 *
	 * @return
	 */
	public TBHVDisProComProgReportItemResult newDisProComProgReportItemResult() {
		return new TBHVDisProComProgReportItemResult();
	}

	/**
	 *
	 * init data with cursor
	 *
	 * @param c
	 * @param dto
	 * @return: void
	 * @throws:
	 * @author: HaiTC3
	 * @date: Oct 30, 2012
	 */
	public void initWithCursor(Cursor c, TBHVDisProComProgReportDTO dto) {
		idNPP = CursorUtil.getString(c, "NPP_ID");
		codeNPP = CursorUtil.getString(c, "NPP_CODE");
		nameNPP = CursorUtil.getString(c, "NPP_NAME");
		idGSNPP = CursorUtil.getString(c, "GSNPP_ID");
		codeGSNPP = CursorUtil.getString(c, "GSNPP_CODE");
		nameGSNPP = CursorUtil.getString(c, "GSNPP_NAME");

		programShortCode = CursorUtil.getString(c, "DP_CODE");
		programShortName = CursorUtil.getString(c, "DP_NAME");
		programLevel = CursorUtil.getString(c, "DP_LEVEL");
		int joinNumber = CursorUtil.getInt(c, "NUM_JOIN");
		int resultNumber = CursorUtil.getInt(c, "NUM_NON_COMPLETE");
//		try {
//
//			for (int i = 0; i < dto.arrLevelCode.size(); i++) {
//				arrLevelCode.add(newDisProComProgReportItemResult());
//				if (programLevel.equals(dto.arrLevelCode.get(i))) {
//					arrLevelCode.get(i).joinNumber = joinNumber;
//					arrLevelCode.get(i).resultNumber = resultNumber;
//					TBHVDisProComProgReportItemResult rs = dto.arrResultTotal
//							.get(i);
//					TBHVDisProComProgReportItemResult rsi = arrLevelCode.get(i);
//					rs.joinNumber += rsi.joinNumber;
//					rs.resultNumber += rsi.resultNumber;
//					itemResultTotal.joinNumber += rsi.joinNumber;
//					itemResultTotal.resultNumber += rsi.resultNumber;
//				}
//			}
//			//
//			dto.dtoResultTotal.joinNumber += itemResultTotal.joinNumber;
//			dto.dtoResultTotal.resultNumber += itemResultTotal.resultNumber;
//
//		} catch (Exception e) {
//			dateFromTo = "";
//			e.printStackTrace();
//		}
		try {

			//convert 1,2,3; d,e,f -> a,b,c
			for(int i = 0; i < dto.arrLevelCodeInDB.size();i++){
				if(programLevel.equals(dto.arrLevelCodeInDB.get(i))){
					programLevel = dto.arrLevelCodeInDB.get(i);
				}
			}

			for (int i = 0; i < dto.arrLevelCodeInView.size(); i++) {
				arrLevelCode.add(newDisProComProgReportItemResult());
				if (programLevel.equals(dto.arrLevelCodeInView.get(i))) {
					arrLevelCode.get(i).joinNumber = joinNumber;
					arrLevelCode.get(i).resultNumber = resultNumber;
					TBHVDisProComProgReportItemResult rs = dto.arrResultTotal
							.get(i);
					TBHVDisProComProgReportItemResult rsi = arrLevelCode.get(i);
					rs.joinNumber += rsi.joinNumber;
					rs.resultNumber += rsi.resultNumber;
					itemResultTotal.joinNumber += rsi.joinNumber;
					itemResultTotal.resultNumber += rsi.resultNumber;
				}
			}
			//
			dto.dtoResultTotal.joinNumber += itemResultTotal.joinNumber;
			dto.dtoResultTotal.resultNumber += itemResultTotal.resultNumber;

		} catch (Exception e) {
			dateFromTo = "";
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	/**
	 *
	*  init data with cursor for SQL of NPP
	*  @param c
	*  @param dto
	*  @return: void
	*  @throws:
	*  @author: HaiTC3
	*  @date: Oct 31, 2012
	 */
	public void initWithCursorForNPP(Cursor c, TBHVDisProComProgReportNPPDTO dto) {

		idNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_ID);
		codeNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		nameNPP = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);

		idGSNPP = CursorUtil.getString(c, "STAFF_OWNER_ID");
		codeGSNPP = CursorUtil.getString(c, "STAFF_OWNER_CODE");
		nameGSNPP = CursorUtil.getString(c, "STAFF_OWNER_NAME");

		programShortCode = CursorUtil.getString(c, "DP_CODE");
		programShortName = CursorUtil.getString(c, "DP_NAME");
		programLevel = CursorUtil.getString(c, "DP_LEVEL");
		int joinNumber = CursorUtil.getInt(c, "NUM_JOIN");
		int resultNumber = CursorUtil.getInt(c, "NUM_NON_COMPLETE");
		try {

			//convert level 1,2,3;d,e,f -> a,b,c
			for(int i = 0; i < dto.arrLevelInDB.size(); i++){
				if(programLevel.equals(dto.arrLevelInDB.get(i))){
					programLevel = dto.arrLevelInDB.get(i);
				}
			}
			for (int i = 0; i < dto.arrLevelCodeInView.size(); i++) {
				arrLevelCode.add(newDisProComProgReportItemResult());
				if (programLevel.equals(dto.arrLevelCodeInView.get(i))) {
					arrLevelCode.get(i).joinNumber = joinNumber;
					arrLevelCode.get(i).resultNumber = resultNumber;
					TBHVDisProComProgReportItemResult rs = dto.arrResultTotal
							.get(i);
					TBHVDisProComProgReportItemResult rsi = arrLevelCode.get(i);
					rs.joinNumber += rsi.joinNumber;
					rs.resultNumber += rsi.resultNumber;
					itemResultTotal.joinNumber += rsi.joinNumber;
					itemResultTotal.resultNumber += rsi.resultNumber;
				}
			}
			//
			dto.dtoResultTotal.joinNumber += itemResultTotal.joinNumber;
			dto.dtoResultTotal.resultNumber += itemResultTotal.resultNumber;

		} catch (Exception e) {
			dateFromTo = "";
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}
	
}
