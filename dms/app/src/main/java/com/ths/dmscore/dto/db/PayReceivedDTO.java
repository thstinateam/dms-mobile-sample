/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PAY_RECEIVED_TEMP_TABLE;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Thong tin chi tiet cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class PayReceivedDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 3728578369404106869L;
	public static final String PAYMENT_TYPE_CASH = "0";
	// id
	public long payReceivedID;
	public String payReceivedNumber;
	public long amount;
	public String paymentType;
	public String shopId;
//	public long customerId;
	public long receiptType;
	public int type;
	public String createUser;
	public String updateUser;
	public String updateDate;
	public String createDate;
	public long staffId;
	public int paymentStatus;
	public String payerName;
	public String payerAddress;
	public String paymentReason;

	public PayReceivedDTO() {
		super(TableType.PAY_DECEIVED_TABLE);
	}

	/**
	 * tao row insert gach no
	 * 
	 * @author: TamPQ
	 * @param dto
	 * @return
	 * @return: JSONObjectvoid
	 * @throws:
	 */
	public JSONObject generateInsertForPayDebt() {
		JSONObject payreceived = new JSONObject();
		try {
			payreceived.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			payreceived.put(IntentConstants.INTENT_TABLE_NAME, PAY_RECEIVED_TEMP_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.PAY_RECEIVED_ID, payReceivedID, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.PAY_RECEIVED_NUMBER, payReceivedNumber, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.AMOUNT, amount, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.PAYMENT_TYPE, paymentType, null));
//			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.CUSTOMER_ID, customerId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.RECEIPT_TYPE, receiptType, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.SHOP_ID, shopId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.CREATE_USER, createUser, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.CREATE_DATE, createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.TYPE, type, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAY_RECEIVED_TEMP_TABLE.PAYMENT_STATUS, paymentStatus, null));
			if (!StringUtil.isNullOrEmpty(payerName)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						PAY_RECEIVED_TEMP_TABLE.PAYER_NAME, payerName, null));
			}
			if (!StringUtil.isNullOrEmpty(payerAddress)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						PAY_RECEIVED_TEMP_TABLE.PAYER_ADDRESS, payerAddress,
						null));
			}
			if (!StringUtil.isNullOrEmpty(paymentReason)) {
				detailPara.put(GlobalUtil.getJsonColumn(
						PAY_RECEIVED_TEMP_TABLE.PAYMENT_REASON, paymentReason,
						null));
			}
			payreceived.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

		} catch (JSONException e) {
		}
		return payreceived;
	}
}
