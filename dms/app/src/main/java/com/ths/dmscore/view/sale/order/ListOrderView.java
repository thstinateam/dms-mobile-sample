/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.HashMapKPI;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.view.ListOrderMngDTO;
import com.ths.dmscore.dto.view.SaleOrderViewDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.MenuItem;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Man hinh danh sach don hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class ListOrderView extends BaseFragment implements OnClickListener,
		OnItemSelectedListener, OnEventControlListener, VinamilkTableListener,
		OnTouchListener, OnDateSetListener, DMSColSortManager.OnSortChange {
	public static final int MAP_ACTION = 0;
	public static final int NOTE_ACTION = 1;
	public static final int DETAIL_ACTION = 2;
	public static final int LIST_ACTION = 3;
	public static final int REFRESH_ACTION = 4;

	private static final int ACTION_CREATE_CUSTOMER = 100;
	private static final int ACTION_ORDER_LIST = 101;
	private static final int ACTION_PATH = 102;
	private static final int ACTION_CUS_LIST = 103;
	public static final int ACTION_IMAGE = 104;
	private static final int ACTION_LOCK_DATE = 105;

	public static final int STATUS_ALL = -1;
	public static final int STATUS_SUCCESS = 0;
	public static final int STATUS_WAITING_PROCESS = 1;
	public static final int STATUS_OVERDATE = 2;
	public static final int STATUS_DENY = 3;
	public static final int STATUS_NOT_SEND = 4;
	public static final int STATUS_CANCEL = 5;
	public static final int BILL_CATEGORY_ALL = -1;
	public static final int BILL_CATEGORY_PRESALE = 0;
	public static final int BILL_CATEGORY_VANSALE = 1;
	public static final int BILL_CATEGORY_RETURN_VANSALE = 2;
	private static final int DATE_FROM_CONTROL = 1;
	private static final int DATE_TO_CONTROL = 2;
	private static final int ACTION_DELETE_ORDER_SUCCESS = 1;
	private static final int ACTION_DELETE_ORDER_FAIL = 2;
	private static final int ACTION_RETURN_ORDER_SUCCESS = 3;
	private static final int ACTION_RETURN_ORDER_FAIL = 4;

	ListOrderMngDTO listMngDTO = new ListOrderMngDTO();
	DMSTableView tbOrder;
	Button btSearch;
	Button btReset;

	VNMEditTextClearable edTKH;
	VNMEditTextClearable edTN;
	VNMEditTextClearable edDN;
	Spinner spinnerLine;
	Spinner spinnerState;
	Spinner spinnerBillCategory;
	LinearLayout llShopStaff;// ll chon don vi, nhan vien danh cho role GS
	// nhung gia tri can search
	String mkh;
	String tkh;
	String from_date;
	String to_date;
	private SparseArray<DMSColSortInfo> lstSortInfo;

	TextView tvNotChecked;
	ArrayList<SaleOrderViewDTO> listSelectedOrder = new ArrayList<SaleOrderViewDTO>();
	private String[] arrLineChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_IN_ROUTE_CAP),
			StringUtil.getString(R.string.TEXT_OUT_ROUTE_CAP) };

	private String[] arrStateChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_WAITING_PROCESS),
			StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND),
			StringUtil.getString(R.string.TEXT_SUCCESS),
			StringUtil.getString(R.string.TEXT_BUTTON_DENY),
			StringUtil.getString(R.string.TEXT_OVERDATE),
			StringUtil.getString(R.string.TEXT_ORDER_CANCEL) };

	private String[] arrStateChooseSup = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_WAITING_PROCESS),
			StringUtil.getString(R.string.TEXT_SUCCESS),
			StringUtil.getString(R.string.TEXT_BUTTON_DENY),
			StringUtil.getString(R.string.TEXT_OVERDATE),
			StringUtil.getString(R.string.TEXT_ORDER_CANCEL) };

	private String[] arrBillCategoryChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_VANSALE),
			StringUtil.getString(R.string.TEXT_PRESALE),
			StringUtil.getString(R.string.TEXT_RETURN_BILL) };

	private int selectedState = -1;
	// -1 : tat ca , 1 : trong tuyen , 0 : ngoai tuyen.
	private int selectedLine = -1;
	private int selectedBillCategory = -1;

	private int mDay;
	private int mMonth;
	private int mYear;
	private int currentCalender;
	private boolean isFirstLoad = true;
	private boolean getListLog = false;
	private boolean isVisible = false;
	private String textShopId = "";
	ListComboboxShopStaffDTO combobox;
	// isRefresh
//	private boolean isRefresh = false;
	//don vi
	private Spinner spShop;
	// spiner list nhan vien ban hang
	Spinner spNVBH;
	private int indexSPShop = 0;
	// list ds nhan vien o spiner
	private String[] arrMaNPP;
	// KH dang chon
	int currentNVBH = -1;
	private long staffId;
	// co cap nhat du lieu khong
	private boolean isUpdateData = false;
	ArrayAdapter adapterState;
	int currentPage = -1;
	boolean isBack = false;

	public static ListOrderView newInstance(Bundle data) {
		ListOrderView f = new ListOrderView();
		f.setArguments(data);

		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		lstSortInfo = new DMSListSortInfoBuilder()
		.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
		.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
		.addInfo(4, SortActionConstants.DATE)
		.addInfo(5, SortActionConstants.AMOUNT)
		.addInfo(6, SortActionConstants.QUANITY)
		.build();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.layout_list_oder,container, false);
		View view = super.onCreateView(inflater, v, savedInstanceState);

		hideHeaderview();
		initView(v);
		parent.setTitleName(StringUtil.getString(R.string.TITLE_MODULE_LIST_ORDER));
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DSDONHANG);
			if (isFirstLoad && combobox == null && currentPage < 0) {
				textShopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
				getListCombobox();
			}else {
				initSpNPP();
				initSpNVBH();
//				renderLayout();
			}
		}else{
			// nhan vien ban hang
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DANHSACHDONHANG);
			if (isFirstLoad && currentPage < 0) {
				getListLog = true;
				from_date = edTN.getText().toString().trim();
				to_date = edDN.getText().toString().trim();
			}
			// luon goi lay lai ds don hang
			getListOrder(true);
//			else {
//				renderLayout();
//			}
		}
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// menu bar
		enableMenuBar(this);
		addMenuItem(PriHashMap.PriForm.NVBH_DANHSACHDONHANG,
				new MenuTab(R.string.TEXT_PHOTO,
						R.drawable.menu_picture_icon, ACTION_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH),
				new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
						R.drawable.icon_lock, ACTION_LOCK_DATE, PriHashMap.PriForm.NVBH_CHOTKHO),
				new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
						ACTION_CREATE_CUSTOMER, PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH),
				new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
						ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
				new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
						ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
				new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
						ACTION_CUS_LIST, PriHashMap.PriForm.NVBH_BANHANG_DSKH));
	}

	private void initView(View v) {
		// TODO Auto-generated method stub
//		LinearLayout llSearch = (LinearLayout) PriUtils.getInstance().findViewByIdGone(v, R.id.llSearch, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM);
//		TextView tvMKH = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvMKH, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TENMA);
//		TextView tvTKH = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTKH, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TENMA);
//		TextView tvTG = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTG, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TUNGAY);
//		TextView tvTuyen = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTuyen, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TUYEN);
//		TextView tvTrangThai = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvTrangThai, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TRANGTHAI);
//		TextView tvLoaiDon = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvLoaiDon, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_LOAIDON);
//		TextView tvSplash = (TextView) PriUtils.getInstance().findViewByIdGone(v, R.id.tvSplash, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_DENNGAY);

		// tim kiem ten/ma
//		edMKH = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edMKH, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TENMA);
		edTKH = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edTKH, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TENMA);
//		GlobalUtil.setEditTextMaxLength(edMKH,
//		Constants.MAX_LENGTH_CUSTOMER_CODE);
		GlobalUtil.setEditTextMaxLength(edTKH,
		Constants.MAX_LENGTH_CUSTOMER_NAME);
		// tim kiem thoi gian
		edTN = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edTN, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		edDN = (VNMEditTextClearable) PriUtils.getInstance().findViewByIdGone(v, R.id.edDN, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		PriUtils.getInstance().setOnTouchListener(edTN, this);
		edTN.setIsHandleDefault(false);
		edTN.setText(DateUtils.getCurrentDate());
		PriUtils.getInstance().setOnTouchListener(edDN, this);
		edDN.setIsHandleDefault(false);
		edDN.setText(DateUtils.getCurrentDate());
		// tuyen
		spinnerLine = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spinnerLine, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TUYEN);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerLine, this);
		// trang thai
		spinnerState = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spinnerState, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TRANGTHAI);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerState, this);
		// loai don
		spinnerBillCategory = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spinnerBillCategory, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_LOAIDON);
		PriUtils.getInstance().setOnItemSelectedListener(spinnerBillCategory, this);

//		SpinnerAdapter adapterLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrLineChoose);
        ArrayAdapter adapterLine = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrLineChoose);
        adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		this.spinnerLine.setAdapter(adapterLine);
//		if (selectedLine > 0) {
//			this.spinnerLine.setSelection(selectedLine);
//		} else {
//			this.spinnerLine.setSelection(0);
//			selectedLine = 0;
//		}
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
//			adapterState = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrStateChooseSup);
            adapterState = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrStateChooseSup);
            adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			this.spinnerState.setAdapter(adapterState);
		}else{
//			adapterState = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrStateChoose);
            adapterState = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrStateChoose);
            adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			this.spinnerState.setAdapter(adapterState);
		}
//		if (selectedState > 0) {
//			this.spinnerState.setSelection(selectedState);
//		} else {
//			this.spinnerState.setSelection(0);
//			selectedState = 0;
//		}

//		SpinnerAdapter adapterBillCategory = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrBillCategoryChoose);
        ArrayAdapter adapterBillCategory = new ArrayAdapter(parent, R.layout.simple_spinner_item, arrBillCategoryChoose);
        adapterLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
		this.spinnerBillCategory.setAdapter(adapterBillCategory);
//		if (selectedBillCategory > 0) {
//			this.spinnerBillCategory.setSelection(selectedBillCategory);
//		} else {
//			this.spinnerBillCategory.setSelection(0);
//			selectedBillCategory = 0;
//		}
		tvNotChecked = (TextView) v.findViewById(R.id.tvNotChecked);
		tbOrder = (DMSTableView) v.findViewById(R.id.tbOrder);
		tbOrder.setListener(this);
		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btSearch, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_TIM);
		PriUtils.getInstance().setOnClickListener(btSearch, this);

		btReset = (Button) PriUtils.getInstance().findViewByIdGone(v, R.id.btReset, PriHashMap.PriControl.NVBH_DSDONHANG_TIMKIEM_NHAPLAI);
		PriUtils.getInstance().setOnClickListener(btReset, this);
		llShopStaff = (LinearLayout) v.findViewById(R.id.llShopStaff);
		spNVBH = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spNVBH, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_NHANVIEN);
		PriUtils.getInstance().findViewByIdGone(v,
				R.id.tvNVBH, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_NHANVIEN);
		spShop = (Spinner) PriUtils.getInstance().findViewByIdGone(v, R.id.spShop, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM_DONVI);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvShop, PriHashMap.PriControl.GSNPP_DANHSACHDIEMBAN_TIMKIEM_DONVI);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			PriUtils.getInstance().setOnItemSelectedListener(spShop, this);
			PriUtils.getInstance().setOnItemSelectedListener(spNVBH, this);
			llShopStaff.setVisibility(View.VISIBLE);
			isVisible = false;
			tvNotChecked.setVisibility(View.GONE);
		}else{
			// nhan vien ban hang
			llShopStaff.setVisibility(View.GONE);
			isVisible = true;
			tvNotChecked.setVisibility(View.VISIBLE);
		}

	}

	private void renderLayout() {
		tbOrder.clearAllDataAndHeader();
		// add sort info
		initHeaderTable(tbOrder, new OrderMngRow(parent, null, isVisible), lstSortInfo,
				this);
		if (currentPage != -1) {
			if(isBack){
				tbOrder.setTotalSize(listMngDTO.total,currentPage);
				isBack = false;
			}
			tbOrder.getPagingControl().setCurrentPage(currentPage);
		}else{
			tbOrder.setTotalSize(listMngDTO.total, 1);
			currentPage = tbOrder.getPagingControl().getCurrentPage();
		}
		if (listMngDTO != null) {
			int pos = 1 + Constants.NUM_ITEM_PER_PAGE
					* (tbOrder.getPagingControl().getCurrentPage() - 1);
			for (final SaleOrderViewDTO dto : listMngDTO.getListDTO()) {
				OrderMngRow row = new OrderMngRow(parent, this, isVisible);
				row.setClickable(true);
				if (listSelectedOrder.contains(dto)) {
					row.renderLayout(pos, listSelectedOrder
							.get(listSelectedOrder.indexOf(dto)));
				} else {
					row.renderLayout(pos, dto);
				}
				pos++;
				tbOrder.addRow(row);
			}

			// totalPage < 0 when delete at last page
//			if (tbOrder.getPagingControl().totalPage < 0)
//				tbOrder.setTotalSize(listMngDTO.total,1);
		}
	}

	/**
	 * request lay danh sach nhan vien ban hang trong shop
	 * @author: hoanpd1
	 * @since: 10:21:38 05-03-2015
	 * @return: void
	 * @throws:
	 */
	private void getListCombobox() {
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_DATA, combobox);
		b.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		b.putString(IntentConstants.INTENT_STAFF_OWNER_ID, String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		b.putBoolean(IntentConstants.INTENT_ORDER, true);
		b.putBoolean(IntentConstants.INTENT_IS_ALL, true);
		b.putBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST, isFirstLoad);
		b.putBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID, false);
		handleViewEvent(b, ActionEventConstant.GET_LIST_COMBOBOX_SHOP_STAFF, SupervisorController.getInstance());
	}
	/**
	 * Cap nhat du lieu sp man hinh
	 *
	 * @author: ThanhNN8
	 * @param modelData
	 * @return: void
	 * @throws:
	 */
	private void updateData() {
		if(isFirstLoad){
			initSpNPP();
		}
		initSpNVBH();
	}

	/**
	 * khoi tao spinner Don vi
	 * @author: hoanpd1
	 * @since: 19:38:12 23-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNPP() {
		if(combobox.listShop != null && combobox.listShop.size() > 0){
			String [] arrMaNPP = new String[combobox.listShop.size()];
			int i = 0;
			for (ShopDTO shop : combobox.listShop) {
				if (!StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)) {
					arrMaNPP[i] = shop.shopCode + " - " + shop.shopName;
				} else if (StringUtil.isNullOrEmpty(shop.shopCode) && !StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopName;
				} else if (!StringUtil.isNullOrEmpty(shop.shopCode) && StringUtil.isNullOrEmpty(shop.shopName)){
					arrMaNPP[i] = shop.shopCode;
				}else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spShop.setAdapter(adapterNPP);
			spShop.setSelection(indexSPShop);
			if (spShop.getSelectedItemPosition() != -1) {
				textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
			}
		} else {
			// truong hop ko co ds nv ban hang
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent, R.layout.simple_spinner_item, new String[0]);
			spShop.setAdapter(adapterNPP);;
			// truong hop ko co ds nv ban hang
			listMngDTO = new ListOrderMngDTO();
//			tbOrder.getPagingControl().totalPage = -1;
			renderLayout();
		}
	}

	/**
	 * khoi tao sprinner danh sach NVB
	 * @author: hoanpd1
	 * @since: 11:41:36 26-03-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpNVBH() {
		if (combobox.listStaff != null && combobox.listStaff.size() > 0) {
			int size = combobox.listStaff.size();
			// cong size them 1 do add them tat ca vap mang
			int i = 0;
			ListComboboxShopStaffDTO.StaffItemDTO item1 = new ListComboboxShopStaffDTO().newStaffItem();
			if(!combobox.listStaff.get(0).staffName.equalsIgnoreCase(StringUtil.getString(R.string.TEXT_ALL))){
				arrMaNPP = new String[size + 1];
				item1.addAll(0, StringUtil.getString(R.string.TEXT_ALL), "");
				combobox.listStaff.add(0, item1);
			}
			for (ListComboboxShopStaffDTO.StaffItemDTO item : combobox.listStaff) {
				if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode + " - " + item.staffName;
				} else if (StringUtil.isNullOrEmpty(item.staffCode)
						&& !StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffName;
				} else if (!StringUtil.isNullOrEmpty(item.staffCode)
						&& StringUtil.isNullOrEmpty(item.staffName)) {
					arrMaNPP[i] = item.staffCode;
				}else {
					arrMaNPP[i] = "";
				}
				i++;

			}
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent, R.layout.simple_spinner_item, arrMaNPP);
			spNVBH.setAdapter(adapterNPP);
			spNVBH.setSelection(currentNVBH);
			currentNVBH = spNVBH.getSelectedItemPosition();
			if (spNVBH.getSelectedItemPosition() != -1) {
				staffId = combobox.listStaff.get(spNVBH.getSelectedItemPosition()).staffId;
			}
		}else {
			SpinnerAdapter adapterNPP = new SpinnerAdapter(parent, R.layout.simple_spinner_item, new String[0]);
			spNVBH.setAdapter(adapterNPP);
			// truong hop ko co ds nv ban hang
			listMngDTO = new ListOrderMngDTO();
//			tbOrder.getPagingControl().totalPage = -1;
			renderLayout();
		}
	}
	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {

		ActionEvent actionEvent = modelEvent.getActionEvent();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_LIST_COMBOBOX_SHOP_STAFF:
			combobox = (ListComboboxShopStaffDTO) modelEvent.getModelData();
			if (combobox != null) {
				updateData();
			}else {
				// truong hop ko co ds nv ban hang
				listMngDTO = new ListOrderMngDTO();
//				tbOrder.getPagingControl().totalPage = -1;
				renderLayout();
			}
			parent.closeProgressDialog();
			break;
		case ActionEventConstant.GET_SALE_ORDER:
			getListLog = false;
			isFirstLoad = false;
			ListOrderMngDTO list = (ListOrderMngDTO) modelEvent.getModelData();
			if (list != null) {
				if (this.listMngDTO == null) {
					this.listMngDTO = new ListOrderMngDTO();
				} else {
					this.listMngDTO.getListDTO().clear();
				}

				for (SaleOrderViewDTO item : list.getListDTO()) {
					this.listMngDTO.getListDTO().add(item);
				}
				if(currentPage == - 1) // load lan dau tien
					this.listMngDTO.total = list.total;
				this.listMngDTO.totalIsSend = list.totalIsSend;
				renderLayout();

				// Update icon order status
				// ArrayList<LogDTO> listLog = list.getListLog();
				if (parent != null) {
					parent.sendBroadcaseNotifyOrder(list.notifyDTO);

					parent.closeProgressDialog();
				}
				if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
					tvNotChecked.setVisibility(View.GONE);
				}else{
					tvNotChecked.setText(StringUtil.getStringAndReplace(
							R.string.TEXT_X_ORDER_NOT_SEND, ""
									+ listMngDTO.totalIsSend));
				}
				requestInsertLogKPI(HashMapKPI.NVBH_DANHSACHDONHANG, actionEvent);
			}
			break;
		case ActionEventConstant.DELETE_SALE_ORDER:
			// Remove don hang khoi ds don hang
			SaleOrderViewDTO dto = (SaleOrderViewDTO) actionEvent.viewData;
			for (int i = 0; i < listMngDTO.getListDTO().size(); i++) {
				if (dto.saleOrder.saleOrderId == listMngDTO.getListDTO().get(i).saleOrder.saleOrderId) {
					listMngDTO.getListDTO().remove(i);
					break;
				}
			}

			// Sent broad cast de cap nhat icon notify don hang loi
			Bundle b = new Bundle();
			// b.putInt(IntentConstants.INTENT_DATA, count);
			b.putLong(IntentConstants.INTENT_SALE_ORDER_ID,
					dto.saleOrder.saleOrderId);
			((GlobalBaseActivity) GlobalInfo.getInstance().getActivityContext())
					.sendBroadcast(
							ActionEventConstant.NOTIFY_DELETE_SALE_ORDER, b);

			// xoa don hang cuoi cung cua trang cuoi cung
			// reset lai trang truoc do
			if ((tbOrder.getPagingControl().getCurrentPage() > 1 && tbOrder
					.getPagingControl().getCurrentPage() == tbOrder
					.getPagingControl().totalPage)
					&& (listMngDTO.listDTO.size() == 0)) {

				tbOrder.getPagingControl().setCurrentPage(
						tbOrder.getPagingControl().getCurrentPage() - 1);

				tbOrder.getPagingControl().totalPage = -1;
			} else {
				// Old current page
				int currentPage = tbOrder.getPagingControl().getCurrentPage();

				// Calculate total page & set current page = 1: old code
				tbOrder.setTotalSize(listMngDTO.total - 1,1);
				if (currentPage <= tbOrder.getPagingControl().totalPage) {
					tbOrder.getPagingControl().setCurrentPage(currentPage);
				} else {
					tbOrder.getPagingControl().setCurrentPage(
							tbOrder.getPagingControl().totalPage);
				}
			}

			// renderLayout();
			getListOrder(true);

			if (parent != null && dto.saleOrder.approved == -1) {
				parent.draftOrderId=StringUtil.EMPTY_STRING;
				parent.setStatusVisible(StringUtil.getString(R.string.TEXT_VISITING) +" : " + dto.customer.customerName + " - " + dto.customer.customerCode + " ", View.VISIBLE);
			}

			parent.closeProgressDialog();
			tvNotChecked.setText(StringUtil.getStringAndReplace(
					R.string.TEXT_X_ORDER_NOT_SEND, ""
							+ (listMngDTO.totalIsSend - 1)));
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_DELETE_ORDER_SUCCESS));
			break;
		case ActionEventConstant.ACTION_CREATE_RETURN_ORDER:{
			Bundle bundle =  (Bundle)actionEvent.viewData;
			SaleOrderViewDTO item = (SaleOrderViewDTO) bundle.getSerializable(IntentConstants.INTENT_ORDER);
			if(parent!=null){
				parent.closeProgressDialog();
				parent.showDialog(StringUtil
						.getString(R.string.TEXT_RETURN_ORDER_SUCCESS));
				parent.requestInserActionLog( DateUtils.now(),
						ActionLogDTO.TYPE_RETURN_ORDER,
						String.valueOf(item.saleOrder.saleOrderId),
						item.customer.customerId, String.valueOf(item.saleOrder.isVisitPlan), item.customer.lat, item.customer.lng, item.saleOrder.routingId);
			}
			receiveBroadcast(ActionEventConstant.NOTIFY_REFRESH_VIEW, null);
			break;
		}
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}

	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent actionEvent = modelEvent.getActionEvent();
		parent.closeProgressDialog();
		switch (actionEvent.action) {
		case ActionEventConstant.GET_SALE_ORDER:{
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_MESSAGE_ERROR_HAPPEN));
			break;
		}
		case ActionEventConstant.DELETE_SALE_ORDER:{
			parent.showDialog(StringUtil
					.getString(R.string.TEXT_DELETE_ORDER_FAIL));
			break;
		}
		case ActionEventConstant.ACTION_GET_CURRENT_DATE_TIME_SERVER:{
			if (parent != null) {
				parent.closeProgressDialog();
			}
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
		case ActionEventConstant.ACTION_CREATE_RETURN_ORDER: {
			if (parent != null) {
				parent.closeProgressDialog();
				super.handleErrorModelViewEvent(modelEvent);
				receiveBroadcast(ActionEventConstant.NOTIFY_REFRESH_VIEW, null);
			}
			break;
		}
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}

	}
	int i = 0;
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == btSearch) {
			currentPage = -1;
			GlobalUtil.forceHideKeyboard(parent);
			//tbOrder.getPagingControl().totalPage = -1;
			updateSearchValue();
			String dateTimePattern = StringUtil.getString(R.string.TEXT_DATE_TIME_PATTERN);
			String strFindTN = Constants.STR_BLANK;
			String strFindFN = Constants.STR_BLANK;
			Pattern pattern = Pattern.compile(dateTimePattern);
			if (!StringUtil.isNullOrEmpty(edDN.getText().toString().trim())) {
				String strFN = this.edDN.getText().toString().trim();
				Matcher matcher = pattern.matcher(strFN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strFN,StringUtil.EMPTY_STRING);
						strFindFN = StringUtil.dateToString(tn,DateUtils.DATE_STRING_YYYY_MM_DD);
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(edTN.getText().toString().trim())) {
				String strTN = this.edTN.getText().toString().trim();
				Matcher matcher = pattern.matcher(strTN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strTN,StringUtil.EMPTY_STRING);
						strFindTN = StringUtil.dateToString(tn,DateUtils.DATE_STRING_YYYY_MM_DD);
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(strFindFN)&& !StringUtil.isNullOrEmpty(strFindTN)
					&& DateUtils.compareDate(strFindTN, strFindFN) == 1) {
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID_2),
								StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),0, null, false);

			}else {

//				if(isBack){
//					i++;
//					if(i>=3){
//						isBack = false;
//					}
//				}else{
//				}
				getListOrder(true);
			}

		}
		if (v == btReset) {
			resetSearchValue();
			getListCombobox();
		}
	}

	/**
	 * update gia tri search
	 *
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	private void updateSearchValue() {
		// TODO Auto-generated method stub
//		mkh = edMKH.getText().toString().trim();
		tkh = edTKH.getText().toString().trim();
		tbOrder.getPagingControl().setCurrentPage(1);
		from_date = edTN.getText().toString().trim();
		to_date = edDN.getText().toString().trim();
	}

	/**
	 * reset gia tri search
	 *
	 * @author: PhucNT
	 * @return: void
	 * @throws:
	 */
	private void resetSearchValue() {
//		edMKH.getText().clear();
		edTKH.getText().clear();
		edTN.getText().clear();
		edTN.setText(DateUtils.getCurrentDate());
		edDN.getText().clear();
		edDN.setText(DateUtils.getCurrentDate());
		spinnerLine.setSelection(0);
		spinnerState.setSelection(0);
		spinnerBillCategory.setSelection(0);
		this.selectedLine = -1;
		this.selectedState = STATUS_ALL;
		this.selectedBillCategory = BILL_CATEGORY_ALL;
		spShop.setSelection(0);
		spNVBH.setSelection(0);
		currentNVBH = 0;
		indexSPShop = 0;
		isFirstLoad = true;
		textShopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		combobox = new ListComboboxShopStaffDTO();
		updateSearchValue();
	}

	/**
	 * updateDisplay
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	public void updateDate(int day, int month, int year) {
		this.mDay = day;
		this.mMonth = month;
		this.mYear = year;

		String sDay = String.valueOf(mDay);
		String sMonth = String.valueOf(mMonth + 1);
		if (mDay < 10) {
			sDay = "0" + sDay;
		}
		if (mMonth + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		if (currentCalender == DATE_FROM_CONTROL) {
			edTN.setTextColor(ImageUtil.getColor(R.color.BLACK));
			edTN.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(sDay).append("/").append(sMonth).append("/")
					.append(mYear).append(" "));
		}
		if (currentCalender == DATE_TO_CONTROL) {
			edDN.setTextColor(ImageUtil.getColor(R.color.BLACK));
			edDN.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(sDay).append("/").append(sMonth).append("/")
					.append(mYear).append(" "));
		}

	}

	/**
	 * lay danh sach don hang
	 *
	 * @author: dungdq3
	 * @since: 4:13:19 PM Apr 6, 2015
	 * @return: void
	 * @throws:
	 * @param isGetTotalPage
	 */
	private void getListOrder(boolean isGetTotalPage) {
		// parent.showProgressDialog(getString(R.string.loading));
		// TODO Auto-generated method stub
		String dateTimePattern = StringUtil
				.getString(R.string.TEXT_DATE_TIME_PATTERN);
		Pattern pattern = Pattern.compile(dateTimePattern);
		Bundle viewData = new Bundle();
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			viewData.putString(IntentConstants.INTENT_FIND_ORDER_STAFFID, Constants.STR_BLANK + staffId);
			if(combobox != null &&!StringUtil.isNullOrEmpty(combobox.listStaffId)){
				viewData.putString(IntentConstants.INTENT_LIST_STAFF, Constants.STR_BLANK + combobox.listStaffId);
			}else{
				viewData.putString(IntentConstants.INTENT_LIST_STAFF, Constants.STR_BLANK);
			}
			viewData.putString(IntentConstants.INTENT_FIND_ORDER_SHOP_ID, textShopId);
		}else{
			// nhan vien ban hang
			viewData.putString(
					IntentConstants.INTENT_FIND_ORDER_STAFFID,
					Integer.toString(GlobalInfo.getInstance().getProfile()
							.getUserData().getInheritId()));

			if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId())) {
				viewData.putString(IntentConstants.INTENT_FIND_ORDER_SHOP_ID,
						GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			} else {
				viewData.putString(IntentConstants.INTENT_FIND_ORDER_SHOP_ID, "1");
			}
		}

		if (!StringUtil.isNullOrEmpty(from_date)) {
			String strTN = from_date;

			Matcher matcher = pattern.matcher(strTN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil.getString(R.string.TEXT_DATE_TIME_PATTERN));
				return;
			} else {
				try {
					Date tn = StringUtil.stringToDate(strTN, "");
					String strFindTN = StringUtil
							.dateToString(tn, "yyyy-MM-dd");

					viewData.putString(
							IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
							strFindTN);
				} catch (Exception ex) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		}

		if (!StringUtil.isNullOrEmpty(to_date)) {

			String strDN = to_date;

			Matcher matcher = pattern.matcher(strDN);
			if (!matcher.matches()) {
				parent.showDialog(StringUtil.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
				return;
			} else {
				try {
					Date dn = StringUtil.stringToDate(strDN, "");
					String strFindDN = StringUtil
							.dateToString(dn, "yyyy-MM-dd");

					viewData.putString(
							IntentConstants.INTENT_FIND_ORDER_TO_DATE,
							strFindDN);
				} catch (Exception ex) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				}
			}
		}

		if (!StringUtil.isNullOrEmpty(mkh)) {
			viewData.putString(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_CODE,this.mkh);
		}

		if (!StringUtil.isNullOrEmpty(tkh)) {
			viewData.putString(IntentConstants.INTENT_FIND_ORDER_CUSTOMER_NAME,tkh);
		}

		viewData.putString(IntentConstants.INTENT_FIND_ORDER_STATUS,Integer.toString(this.selectedState));
		viewData.putString(IntentConstants.INTENT_FIND_ORDER_TYPEROUTE, Integer.toString(this.selectedLine));
		viewData.putString(IntentConstants.INTENT_FIND_ORDER_BILL_CATEGORY, Integer.toString(this.selectedBillCategory));
		String page = " limit "
				+ ((tbOrder.getPagingControl().getCurrentPage() - 1) * Constants.NUM_ITEM_PER_PAGE)
				+ "," + Constants.NUM_ITEM_PER_PAGE;

		viewData.putString(IntentConstants.INTENT_PAGE, page);
		// can hien thi icon notify don hang loi
		viewData.putBoolean(IntentConstants.INTENT_GET_LIST_LOG, getListLog);
		viewData.putBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE,
				isGetTotalPage);
		viewData.putSerializable(IntentConstants.INTENT_SORT_DATA, tbOrder.getSortInfo());

		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();

		if (action != null && action.objectType.equals("0")
				&& StringUtil.isNullOrEmpty(action.endTime)) {
			viewData.putString(IntentConstants.INTENT_CUSTOMER_ID,
					String.valueOf(action.aCustomer.customerId));
		}

		parent.showLoadingDialog();

		handleViewEvent(viewData, ActionEventConstant.GET_SALE_ORDER, SaleController.getInstance());
	}

	@Override
	public void onItemSelected(AdapterView<?> adap, View v, int pos, long arg3) {
			if (adap == this.spinnerState) {
				int oldIndex = this.selectedState;
				if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
					switch (pos) {
					case 0:
						this.selectedState = STATUS_ALL;
						break;
					case 1:
						this.selectedState = STATUS_WAITING_PROCESS;
						break;
					case 2:
						this.selectedState = STATUS_SUCCESS;
						break;
					case 3:
						this.selectedState = STATUS_DENY;
						break;
					case 4:
						this.selectedState = STATUS_OVERDATE;
						break;
					case 5:
						this.selectedState = STATUS_CANCEL;
						break;
					default:
						this.selectedState = STATUS_ALL;
						break;
					}
				}else{
					switch (pos) {
					case 0:
						this.selectedState = STATUS_ALL;
						break;
					case 1:
						this.selectedState = STATUS_WAITING_PROCESS;
						break;
					case 2:
						this.selectedState = STATUS_NOT_SEND;
						break;
					case 3:
						this.selectedState = STATUS_SUCCESS;
						break;
					case 4:
						this.selectedState = STATUS_DENY;
						break;
					case 5:
						this.selectedState = STATUS_OVERDATE;
						break;
					case 6:
						this.selectedState = STATUS_CANCEL;
						break;
					default:
						this.selectedState = STATUS_ALL;
						break;
					}
				}
				// neu truong hop trang thai thay doi moi tim kiem lai
				if(oldIndex != selectedState)
					onClick(btSearch);
			} else if (adap == this.spinnerLine) {
				int oldIndex = this.selectedLine;
				switch (pos) {
				case 0:
					this.selectedLine = -1;
					break;
				case 1:
					this.selectedLine = 1;
					break;
				case 2:
					this.selectedLine = 0;
					break;
				default:
					this.selectedLine = -1;
					break;
				}
				// neu truong hop trang thai thay doi moi tim kiem lai
				if(oldIndex != selectedLine)
					onClick(btSearch);
			} else if (adap == this.spinnerBillCategory) {
				int oldIndex = this.selectedBillCategory;
				switch (pos) {
				case 0:
					this.selectedBillCategory = BILL_CATEGORY_ALL;
					break;
				case 1:
					this.selectedBillCategory = BILL_CATEGORY_VANSALE;
					break;
				case 2:
					this.selectedBillCategory = BILL_CATEGORY_PRESALE;
					break;
				default:
					this.selectedBillCategory = BILL_CATEGORY_RETURN_VANSALE;
					break;
				}
				// neu truong hop trang thai thay doi moi tim kiem lai
				if(oldIndex != selectedBillCategory)
					onClick(btSearch);
			}
			else if (adap == spShop) {
				if (indexSPShop != spShop.getSelectedItemPosition()) {
					indexSPShop = spShop.getSelectedItemPosition();
					currentNVBH = 0;
					textShopId = String.valueOf(combobox.listShop.get(spShop.getSelectedItemPosition()).shopId);
					if(isUpdateData){
						isFirstLoad =false;
					}
					if (!isFirstLoad) {
						combobox.listStaff = null;
						getListCombobox();
					}
					isFirstLoad = false;
				}
			} else if (adap == spNVBH) {
				if (currentNVBH != spNVBH.getSelectedItemPosition()) {
					currentNVBH = spNVBH.getSelectedItemPosition();
					staffId = combobox.listStaff.get(spNVBH.getSelectedItemPosition()).staffId;
				}
				onClick(btSearch);
			}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		// TODO Auto-generated method stub
		if (control == tbOrder) {
			currentPage = tbOrder.getPagingControl().getCurrentPage();
			getListOrder(true);
			// load more data for table product list
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		// TODO Auto-generated method stub
		if (control instanceof MenuItem) {
			if (eventType == ACTION_PATH){
				gotoCustomerRouteView();
			}
			if (eventType == ACTION_CUS_LIST) {
				gotoCustomerList();
			}
			if (eventType == ACTION_CREATE_CUSTOMER){
				gotoListCustomerCreated();
			}
			if (eventType == ACTION_LOCK_DATE){
				handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
			}
			if (eventType == ACTION_IMAGE){
				handleSwitchFragment(null, ActionEventConstant.GO_TO_IMAGE_LIST, SaleController.getInstance());
			}
		} else {
			switch (eventType) {
			case ACTION_DELETE_ORDER_SUCCESS:{
				deleteOrder((SaleOrderViewDTO) data);
				break;
			}
			case ACTION_RETURN_ORDER_SUCCESS:{
				SaleOrderViewDTO item = (SaleOrderViewDTO) data;
				if (GlobalInfo.getInstance().getSysReturnDate() == Constants.RETURN_DATE_SYS_DATE) {
					item.saleOrder.accountDate = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
				} else if (GlobalInfo.getInstance().getSysReturnDate() == Constants.RETURN_DATE_ORDER_DATE) {
					item.saleOrder.accountDate = DateUtils.convertFormatDate(item.saleOrder.orderDate,DateUtils.DATE_FORMAT_NOW, DateUtils.DATE_FORMAT_DATE);
				} else if (GlobalInfo.getInstance().getSysReturnDate() == Constants.RETURN_DATE_APPROVED_DATE) {
					item.saleOrder.accountDate = DateUtils.convertFormatDate(item.saleOrder.approvedDate,DateUtils.DATE_FORMAT_NOW, DateUtils.DATE_FORMAT_DATE);
				}
				createReturnOrder(item);
				break;
			}
			case ACTION_RETURN_ORDER_FAIL:
			case ACTION_DELETE_ORDER_FAIL:{
				break;
			}
			default:
				break;
			}
			super.onEvent(eventType, control, data);
		}
	}

	/**
	 *
	 * go to vote display present product
	 *
	 * @author: TamPQ
	 * @return: void
	 * @throws:
	 */
	private void gotoCustomerRouteView() {
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_MAP_SHOW_MENU, "true");
		bundle.putString(IntentConstants.INTENT_MAP_TITLE,
				StringUtil.getString(R.string.TEXT_POSITION));
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_CUSTOMER_ROUTE, SaleController.getInstance());

	}

	private void gotoCustomerList() {
		handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
	}

	private void deleteOrder(SaleOrderViewDTO dto) {
		// TODO Auto-generated method stub
		if (dto != null) {
			parent.showProgressDialog(StringUtil.getString(R.string.loading), false);
			ActionEvent e = new ActionEvent();
			e.viewData = dto;
			e.sender = this;
			e.action = ActionEventConstant.DELETE_SALE_ORDER;
			SaleController.getInstance().handleViewEvent(e);
		}
	}

	/**
	 * di toi man hinh chi tiet Khach hang
	 *
	 * @author : PhucNT since : 10:59:39 AM
	 */
	public void gotoCustomerInfo(long customerId) {
		isFirstLoad = false;
		Bundle bunde = new Bundle();
		bunde.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(customerId));
		handleSwitchFragment(bunde, ActionEventConstant.GO_TO_CUSTOMER_INFO, SaleController.getInstance());
	}

	/**
	 * Den man hinh tao don hang
	 *
	 * @author: TruongHN
	 * @param staffId
	 * @return: void
	 * @throws:
	 */
	private void gotoViewOrder(String orderId, CustomerDTO cus, int stateProcess, int staffId) {
		isFirstLoad = false;
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_ORDER_ID, orderId);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID,
				String.valueOf(cus.customerId));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, cus.customerCode);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ADDRESS,
				cus.address);
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME,
				cus.getCustomerName());
		bundle.putInt(IntentConstants.INTENT_CUSTOMER_TYPE_ID,
				cus.getCustomerTypeId());
		bundle.putString(IntentConstants.INTENT_CASHIER_STAFF_ID,
				cus.cashierStaffID);
		bundle.putString(IntentConstants.INTENT_DELIVERY_ID,
				cus.deliverID);
		bundle.putInt(IntentConstants.INTENT_STAFF_ID,staffId);
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			bundle.putInt(IntentConstants.INTENT_STATE, 1);
			// shop chon danh cho role GS
			bundle.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
			handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_SUPERVISOR_VIEW, SupervisorController.getInstance());
		}else{
			bundle.putInt(IntentConstants.INTENT_STATE, stateProcess);
			handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ORDER_VIEW, SaleController.getInstance());
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		case OrderMngRow.ACTION_CLICK_MKH: {
			isBack = true;
			currentPage = -1;
			// click row of table
			SaleOrderViewDTO item = (SaleOrderViewDTO) data;
			gotoCustomerInfo(item.getCustomerId());
			break;
		}
		case OrderMngRow.ACTION_CLICK_SDH: {
			isBack = true;
			currentPage = -1;
			SaleOrderViewDTO item = (SaleOrderViewDTO) data;
			if (item != null && item.saleOrder != null) {
				if ((item.saleOrder.orderType
						.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE) || item.saleOrder.orderType
						.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE_RE))
						&& item.saleOrder.approved == 2) {
					// don vansale bi tu choi khong cho sua
					// hoac don tra bi tu choi khong cho sua
					gotoViewOrder(String.valueOf(item.getFromPOCustomerId()), item.customer, OrderViewDTO.STATE_VIEW_PO,item.saleOrder.staffId);
				} else {
					int state = OrderViewDTO.STATE_VIEW_ORDER;
					if((item.saleOrder.approved == -1 && DateUtils.compare(item.saleOrder.orderDate, DateUtils.now(), DateUtils.DATE_STRING_YYYY_MM_DD) == 0) || (item.saleOrder.approved == 2
							&& DateUtils.compare(item.saleOrder.maxApproved, DateUtils.now(), DateUtils.DATE_STRING_YYYY_MM_DD) >= 0)) {
						state = OrderViewDTO.STATE_EDIT;
					}
					gotoViewOrder(String.valueOf(item.getSaleOrderId()), item.customer, state,item.saleOrder.staffId);
				}
			}
			break;
		}
		case OrderMngRow.ACTION_CLICK_DELETE: {
			GlobalUtil.showDialogConfirm(this, parent, StringUtil
					.getString(R.string.TEXT_CONFIRM_DELETE_ORDER),
					StringUtil.getString(R.string.TEXT_HAVE),
					ACTION_DELETE_ORDER_SUCCESS, StringUtil
							.getString(R.string.TEXT_NO),
					ACTION_DELETE_ORDER_FAIL, data);
			break;
		}
		case OrderMngRow.ACTION_VIEW_ORDER: {
			isBack = true;
			currentPage = -1;
			SaleOrderViewDTO item = (SaleOrderViewDTO) data;
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
				gotoViewOrder(String.valueOf(item.getSaleOrderId()),
						item.customer, OrderViewDTO.STATE_VIEW_PO,item.saleOrder.staffId);
			}else{
				gotoViewOrder(String.valueOf(item.getFromPOCustomerId()),
						item.customer, OrderViewDTO.STATE_VIEW_PO,item.saleOrder.staffId);
			}
			break;
		}
		case OrderMngRow.ACTION_RETURN_ORDER: {
			GlobalUtil.showDialogConfirm(this, parent, StringUtil
					.getString(R.string.TEXT_CONFIRM_RETURN_ORDER),
					StringUtil.getString(R.string.TEXT_HAVE),
					ACTION_RETURN_ORDER_SUCCESS, StringUtil
							.getString(R.string.TEXT_NO),
					ACTION_RETURN_ORDER_FAIL, data);
			break;
		}
		default:
			break;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		// TODO Auto-generated method stub

		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				currentPage = -1;
				isBack = false;
				// cau request du lieu man hinh
//				tbOrder.getPagingControl().totalPage = -1;
				textShopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
				isUpdateData = true;
				tbOrder.resetSortInfo();
				resetSearchValue();
				// TruongHN - khi dong bo du lieu ve thi can lay lai ds log de
				// hien thi notify icon
				getListLog = true;
				if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
					getListCombobox();
				}else{
					getListOrder(true);
				}
			}
			break;
		case ActionEventConstant.BROADCAST_UPDATE_EDIT_ORDER:
			OrderViewDTO orderDTO = (OrderViewDTO) bundle
					.getSerializable(IntentConstants.INTENT_UPDATE_EDITED_ORDER);
			if (orderDTO != null) {
				for (int i = 0; i < listMngDTO.getListDTO().size(); i++) {
					SaleOrderViewDTO dto = listMngDTO.getListDTO().get(i);
					if (orderDTO.orderInfo.saleOrderId == dto.saleOrder.saleOrderId) {
						// dto.saleOrder.isSend = orderDTO.orderInfo.isSend;
						dto.saleOrder.setTotal(orderDTO.orderInfo.getTotal());
						dto.setDescription("");
						break;
					}
				}

				renderLayout();
			}
			break;
		case ActionEventConstant.NOTIFY_LIST_ORDER_STATE: {
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() != UserDTO.TYPE_STAFF) {
				ArrayList<LogDTO> list = (ArrayList<LogDTO>) bundle
						.getSerializable(IntentConstants.INTENT_DATA);
				if (list != null && listMngDTO != null) {
					for (SaleOrderViewDTO dto : listMngDTO.getListDTO()) {
						for (LogDTO logDto : list) {
							if (dto.saleOrder.saleOrderId == Long
									.valueOf(logDto.tableId)) {
								dto.saleOrder.synState = Integer
										.parseInt(logDto.state);
							}
						}
					}
				}
				renderLayout();
			} else {
				if (this.isVisible()) {
					// vansale refresh lai view
					getListLog = true;
					getListOrder(false);
				}
			}
			break;
		}
		default:
			super.receiveBroadcast(action, bundle);
			break;
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent arg1) {
		// TODO Auto-generated method stub
		if (v == edTN) {
			if (!edTN.onTouchEvent(arg1)) {
				currentCalender = DATE_FROM_CONTROL;
				parent.showDatePickerDialog(edTN.getText().toString(), true, this);
			}
		}
		if (v == edDN) {
			if (!edDN.onTouchEvent(arg1)) {
				currentCalender = DATE_TO_CONTROL;
				parent.showDatePickerDialog(edDN.getText().toString(), true, this);
			}
		}
		return true;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param item
	 * @return: void
	 * @throws:
	*/
	void createReturnOrder(SaleOrderViewDTO item){
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_ORDER, item);

		handleViewEvent(b, ActionEventConstant.ACTION_CREATE_RETURN_ORDER, SaleController.getInstance(), true);
	}

	/**
	 * Toi man hinh them moi KH
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void gotoListCustomerCreated() {
		handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		getListOrder(false);
	}
	@Override
	public void onDestroyView() {
		super.onDestroyView();
//		isFirstLoad = true;
	}

	@Override
	public void onStop() {
		super.onStop();
//		isFirstLoad = true;
	}

}
