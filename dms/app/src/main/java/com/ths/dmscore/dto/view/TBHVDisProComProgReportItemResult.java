/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

/**
 * display programe programe report item result
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.1
 */
public class TBHVDisProComProgReportItemResult implements Serializable{
	private static final long serialVersionUID = -6283124637090597341L;
	public int resultNumber;
	public int joinNumber;
}
