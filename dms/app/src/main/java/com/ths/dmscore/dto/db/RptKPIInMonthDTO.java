package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DIM_DATE_TABLE;
import com.ths.dmscore.lib.sqllite.db.RPT_KPI_IN_MONTH_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * dto cho bao cao thang
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RptKPIInMonthDTO extends RptKPIDTO {

	private static final long serialVersionUID = 1L;
	// id bao cao
	public long rptKpiInMonthId;
	// staff Id
	public int staffId;
	// ten cua cot
	public String columnName;

	// danh cho bang tuan
	public int rptInWeek;// tuan trong nam
	public int month ; // thang
	public int year; // nam

	// danh cho bang quy
	public int quarter;

	// danh cho bang ngay
	public String rptInDate;


	@Override
	public RptKPIInMonthDTO clone(){
		RptKPIInMonthDTO dto = new RptKPIInMonthDTO();
		dto.rptKpiInMonthId = this.rptKpiInMonthId;
		dto.objectId = this.objectId;
		dto.objectTypeId = this.objectTypeId;
		dto.shopId = this.shopId;
		dto.kpiId = this.kpiId;
		dto.columnCriId = this.columnCriId;
		dto.columnCriTypeId = this.columnCriTypeId;
		dto.valuePlan = this.valuePlan;
		dto.value = this.value;
		dto.percentComplete = this.percentComplete;
		dto.staffId  = this.staffId;
		dto.columnName = this.columnName;
		dto.year = this.year;
		dto.quarter = this.quarter;
		dto.rptInWeek = this.rptInWeek;// tuan trong nam
		dto.rptInDate = this.rptInDate;
		dto.month = this.month; // thang
		dto.year = this.year; // nam

		return dto;
	}

	/**
	 * Khoi tao du lieu tu cursor
	 *
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	 */
	@Override
	public void initDataFromCursor(Cursor c, int typeKpiID) {
		rptKpiInMonthId = CursorUtil.getLong(c,
				RPT_KPI_IN_MONTH_TABLE.RPT_KPI_IN_MONTH_ID);
		objectId = CursorUtil.getInt(c,
				RPT_KPI_IN_MONTH_TABLE.OBJECT_ID);
		objectTypeId = CursorUtil.getInt(c,
				RPT_KPI_IN_MONTH_TABLE.OBJECT_TYPE_ID);
		shopId = CursorUtil.getLong(c, RPT_KPI_IN_MONTH_TABLE.SHOP_ID);
		kpiId = CursorUtil.getInt(c, RPT_KPI_IN_MONTH_TABLE.KPI_ID);
//		rptInMonth = StringUtil.getStringFromCursor(c,
//				RPT_KPI_IN_MONTH_TABLE.RPT_IN_MONTH);
		columnCriId = CursorUtil.getInt(c,
				RPT_KPI_IN_MONTH_TABLE.COLUMN_CRI_ID);
		columnCriTypeId = CursorUtil.getInt(c,
				RPT_KPI_IN_MONTH_TABLE.COLUMN_CRI_TYPE_ID);
		valuePlan = CursorUtil.getDouble(c,
				RPT_KPI_IN_MONTH_TABLE.VALUE_PLAN);
		value = CursorUtil.getDouble(c,
				RPT_KPI_IN_MONTH_TABLE.VALUE);
		valuePlan = CursorUtil.getDouble(c,
				"VALUE_PLAN_SUM");
		value = CursorUtil.getDouble(c, "VALUE_SUM");
		// neu la san luong thi chia 1000 ra so tien
		if (typeKpiID == ReportTemplateCriterionDTO.AMOUNT) {
			valuePlan = valuePlan / 1000;
			value = value / 1000;
		}
		percentComplete = CursorUtil.getDouble(c, RPT_KPI_IN_MONTH_TABLE.PERCENT_COMPLETE);
		// tinh lai percent cho chac
		percentComplete = valuePlan > 0 ? value / valuePlan * 100.0
				: 100.0;
		staffId  = CursorUtil.getInt(c, RPT_KPI_IN_MONTH_TABLE.STAFF_ID);
		columnName = CursorUtil.getString(c, "COLUMN_NAME");

		// lay thong tin trong bang tuan

		rptInWeek = CursorUtil.getInt(c, DIM_DATE_TABLE.WEEK_IN_MONTH_NUM);
		month = CursorUtil.getInt(c, DIM_DATE_TABLE.MONTH_IN_YEAR_NUM);
		year = CursorUtil.getInt(c, DIM_DATE_TABLE.YEAR_NUM);
		quarter = CursorUtil.getInt(c, DIM_DATE_TABLE.QUARTER_IN_YEAR_NUM);
		rptInDate = CursorUtil.getString(c, DIM_DATE_TABLE.FROM_DATE);
	}
}
