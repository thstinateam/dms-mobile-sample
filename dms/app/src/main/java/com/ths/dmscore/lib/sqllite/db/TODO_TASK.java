package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.ToDoTaskDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * Thong tin ghi chu
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class TODO_TASK extends ABSTRACT_TABLE {
	// id
	public static final String ID = "ID";
	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// noi dung
	public static final String CONTENT = "CONTENT";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay nhac nho
	public static final String REMIND_DATE = "REMIND_DATE";
	// ngay thuc hien
	public static final String DONE_DATE = "DONE_DATE";

	public static final String TODO_TASK = "TODO_TASK";

	public TODO_TASK() {
		this.tableName = TODO_TASK;
		this.columns = new String[] { ID, STAFF_ID, CONTENT, CREATE_DATE,
				REMIND_DATE, DONE_DATE, TODO_TASK };
		
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}
	
	public TODO_TASK(SQLiteDatabase mDB) {
		this.tableName = TODO_TASK;
		this.columns = new String[] { ID, STAFF_ID, CONTENT, CREATE_DATE,
				REMIND_DATE, DONE_DATE, TODO_TASK };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	private ContentValues initDataRow(AbstractTableDTO d) {
		ToDoTaskDTO dto = (ToDoTaskDTO) d;
		ContentValues editedValues = new ContentValues();
		editedValues.put(ID, dto.id);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(CONTENT, dto.content);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(REMIND_DATE, dto.remindDate);
		editedValues.put(DONE_DATE, dto.doneDate);

		return editedValues;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	public long postNote(ToDoTaskDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.TODO_TASK_TABLE.equals(dto.getType())) {
			try {
				int maxFeedbackId = getMaxIdInTable("ID");
				dto.id = maxFeedbackId + 1;
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		}	
		return returnCode;
	}

}
