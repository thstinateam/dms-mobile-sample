/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.util.ArrayList;

/**
 * menu and list submenu
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  10:35:46 AM Apr 15, 2015
 */
public class MenuAndSubMenu {

	private ArrayList<MenuItemDTO> listSubMenu;
	
	public MenuAndSubMenu() {
		// TODO Auto-generated constructor stub
		listSubMenu = new ArrayList<MenuItemDTO>();
	}
	
	public void addSubMenu(String text, int icon){
		MenuItemDTO menuItemDTO = new MenuItemDTO(text, icon);
		menuItemDTO.createSubMenu();
		listSubMenu.add(menuItemDTO);
	}

	public ArrayList<MenuItemDTO> getListSubMenu() {
		return listSubMenu;
	}

	public void setSubMenu(ArrayList<MenuItemDTO> listSubMenuTemp) {
		listSubMenu.clear();
		listSubMenu.addAll(listSubMenuTemp);
	}

}
