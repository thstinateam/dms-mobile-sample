/**
 * Copyright 2013 THSe. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ths.dmscore.dto.StaffChoosenDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.StaffChoosenItemDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.supervisor.routed.StaffListView;
import com.ths.dms.R;

/**
 *  Row cho popup chon nhan vien chup hinh
 *  @author: Tuanlt11
 *  @version: 1.0
 *  @since: 1.0
 */
public class StaffChoosenItemRow extends RelativeLayout {

	Context mContext;
	RadioButton rdChooseStaff;
	TextView tvStaff;
	long staffId ; // id nhan vien
	String staffCode ; // ma nhan vien
	StaffListView listener;
	StaffChoosenItemDTO item; // luu lai thong tin nhan vien da chon

	public StaffChoosenItemRow(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		//initMenu();
	}

	public StaffChoosenItemRow(Context context, View row) {
		super(context);
		mContext = context;
		initMenu(row);
	}

	private void initMenu(View row) {
		//LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//View view = vi.inflate(R.layout.layout_choose_staff_item, null);
		rdChooseStaff = (RadioButton) row.findViewById(R.id.rdChooseStaff);
		tvStaff = (TextView) row.findViewById(R.id.tvStaff);

	}

	/**
	*  Ve noi dung cho row
	*  @author: Tuanlt11
	*  @param item
	*  @return: void
	*  @throws:
	*/
	public void populateFrom(StaffChoosenItemDTO item) {
		if (item != null) {
			//tvStaff.setText(item.getTextMenu());
			tvStaff.setText(item.getTextMenu());
			staffId = item.staffId;
			this.item = item;
			//item.setSelected(false);
//			if(staffId != StaffChoosenItemDTO.staffIdChoose)
//				rdChooseStaff.setChecked(false);
			if(!item.isSelected())
				rdChooseStaff.setChecked(false);
		}
	}



	public void setBackground(int res) {

	}

	/**
	*  set Listener
	*  @author: Tuanlt11
	*  @param staffListView
	*  @return: void
	*  @throws:
	*/
	public void setListener(StaffListView staffListView){
		listener = staffListView;
	}

	/**
	*  Render data
	*  @author: Tuanlt11
	*  @param item
	*  @return: void
	*  @throws:
	*/
	public void populateFrom(StaffChoosenDTO item, View row) {
		if (item != null) {
			//tvStaff.setText(item.getTextMenu());
			if(item.roleType == UserDTO.TYPE_MANAGER)
			{
				row.setPadding(0, 0, 0, 0);
				if(item.statffId > 0) {
					tvStaff.setText(item.staffCode + " - " + item.staffName);
				} else {
					tvStaff.setText(item.staffName);
				}
				staffId = item.statffId;
				staffCode = item.staffCode;
			}
			else if(item.roleType == UserDTO.TYPE_SUPERVISOR)
			{
				row.setPadding(20, 0, 0, 0);
				if(item.statffId > 0) {
					tvStaff.setText(item.staffCode + " - " + item.staffName);
				} else {
					if(!StringUtil.isNullOrEmpty(item.staffCode))
						tvStaff.setText(item.staffCode + " - " + item.staffName);
					else
					{
						tvStaff.setText(item.staffName);
					}
				}
				staffId = item.statffId;
				staffCode = item.staffCode;
			} else if (item.roleType == UserDTO.TYPE_STAFF) {
				row.setPadding(40, 0, 0, 0);
				if(item.statffId > 0) {
					tvStaff.setText(item.staffCode + " - " + item.staffName);
				} else {
					tvStaff.setText(item.staffName);
				}
				staffId = item.statffId;
				staffCode = item.staffCode;
			}
			if(!item.isSelected)
			{
				rdChooseStaff.setChecked(false);
			}
			else
				rdChooseStaff.setChecked(true);

		}
	}


}
