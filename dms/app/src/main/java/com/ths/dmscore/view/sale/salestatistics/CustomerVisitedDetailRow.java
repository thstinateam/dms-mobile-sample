/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.salestatistics;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerVisitedViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

public class CustomerVisitedDetailRow extends DMSTableRow {
	// STT
	public TextView tvSTT;
	// ma KH + ten KH
	public TextView tvKhachHang;
	// dia chi khach hang
	public TextView tvDiaChi;
	// thoi gian bat dau ghe tham
	public TextView tvBatDau;
	// thoi gian ket thuc ghe tham
	public TextView tvKetThuc;
	// thoi gian ghe tham
	public TextView tvThoiGian;
	// doanh so
	public TextView tvDoanhSo;
	// san luong
	public TextView tvSanLuong;

	public CustomerVisitedDetailRow(Context context) {
		super(context, R.layout.layout_customer_visited_sale_row, GlobalInfo
				.getInstance().isSysShowPrice() ? null
				: new int[] { R.id.tvDoanhSo });
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvKhachHang = (TextView) findViewById(R.id.tvKhachHang);
		tvDiaChi = (TextView) findViewById(R.id.tvDiaChi);
		tvBatDau = (TextView) findViewById(R.id.tvBatDau);
		tvKetThuc = (TextView) findViewById(R.id.tvKetThuc);
		tvThoiGian = (TextView) findViewById(R.id.tvThoiGian);
		tvDoanhSo = (TextView) findViewById(R.id.tvDoanhSo);
		tvSanLuong = (TextView) findViewById(R.id.tvSanLuong);
	}

	/**
	 * render layout cho row
	 * @author: DungNX
	 * @param item
	 * @param pos
	 * @return: void
	 * @throws:
	*/
	public void render(CustomerVisitedViewDTO.CustomerVisitedDTO item, int pos) {
		tvSTT.setText(String.valueOf(pos));
		tvKhachHang.setText(item.aCustomer.customerCode + " - "
				+ item.aCustomer.customerName);
		if (!StringUtil.isNullOrEmpty(item.aCustomer.getStreet())) {
			tvDiaChi.setText(item.aCustomer.getStreet());
		} else {
			tvDiaChi.setText("");
		}
		if (!StringUtil.isNullOrEmpty(item.startTimeShort)) {
			tvBatDau.setText(item.startTimeShort);
		} else {
			tvBatDau.setText("");
		}
		if (!StringUtil.isNullOrEmpty(item.endTimeShort)) {
			tvKetThuc.setText(item.endTimeShort);
		} else {
			tvKetThuc.setText("");
		}
		if (!StringUtil.isNullOrEmpty(item.visitedTime)) {
			tvThoiGian.setText(item.visitedTime);
		} else {
			tvThoiGian.setText("");
		}
//		Double amountDone = 0.0;
//		long quantityDone = 0;
//		if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_APPROVED){
//			amountDone = item.amountApproved;
//			quantityDone = item.quantityApproved;
//		}else if(GlobalInfo.getInstance().getSysCalUnapproved() == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING){
//			amountDone = item.amountPending;
//			quantityDone = item.quantityPending;
//		}else{
//			amountDone = item.amount;
//			quantityDone = item.quantity;
//		}
		display(tvDoanhSo, item.amount);
		display(tvSanLuong, item.quantity);
		if (item.isOr == 1) {
			setBackgroundRowByColor(ImageUtil
					.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
//			setColorBackgroundForRow(ImageUtil
//					.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
		}
		long min = DateUtils.getDistanceMinutesFrom2DateCheckAttendance(item.startTimeFull,
				item.endTimeFull);
		if (min < 2 || min > 30) {
			tvThoiGian.setTextColor(ImageUtil.getColor(R.color.RED));
		} else if (min == 30) {
			// neu so phut dung 30 thi kiem tra giay > 0
			long sec = DateUtils.getDistanceSecondFrom2Date(item.startTimeFull,
					item.endTimeFull);
			if (sec > 0) {
				tvThoiGian.setTextColor(ImageUtil.getColor(R.color.RED));
			}
		}

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);

	}

	 /**
	 * Tao header cho doanh so
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	public void initHeaderAmount(){
		display(tvDoanhSo, StringUtil.getReportUnitTitle(StringUtil.getString(R.string.TEXT_SALES_)));
	}
}
