package com.ths.dmscore.view.sale.image;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.ImageListDTO;
import com.ths.dmscore.dto.view.ImageListItemDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.view.AlbumDTO;
import com.ths.dmscore.dto.view.DisplayProgrameModel;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 *
 * 04-00. Danh sach hinh anh
 *
 * @author: HoanPD1
 * @version: 1.0
 * @since: 1.1
 */
public class ImageListView extends BaseFragment implements OnEventControlListener, VinamilkTableListener,
		OnItemSelectedListener, OnTouchListener, OnDateSetListener, DMSColSortManager.OnSortChange {

	public static final int ACTION_IMAGE = 1;
	private static final int ACTION_CUS_LIST = 2;
	private static final int ACTION_PATH = 3;
	private static final int ACTION_ORDER_LIST = 4;
	private static final int ACTION_CREATE_CUSTOMER = 5;
	private static final int ACTION_LOCK_DATE = 6;

	public static final int ACTION_CANCEL = 0;

//	private VNMEditTextClearable edCusCode;// ma khach hang
	private VNMEditTextClearable edCusName;// ten khach hang
	private Button btSearch;// tim kiem
	private DMSTableView tbCusList;
	public ImageListDTO ilDTO;
	boolean isReload = false;
	private int currentPage = -1;
	private String customerCode = "";
	private String customerName = "";
	private boolean isUpdateData = false;
	private int currentCalender;

	private int selectedLineIndex;
	private String fromDate;
	private String toDate;
	private String preFromDate;
	private String preToDate;
	private String fromDateForRequest = "";
	private String toDateForRequest = "";
	private String programeId = "";
	private int selectedProgrameIndex;
	private Spinner spLine;
	private Spinner spPrograme;
	private VNMEditTextClearable edFromDate;// Tu ngay
	private VNMEditTextClearable edToDate;// Den ngay
	private List<DisplayProgrameItemDTO> listDisplayPrograme = null;
	private int getTotalPage;
	ArrayAdapter adLine;
    ArrayAdapter adPrograme;
	private Button btReInput;
	Boolean isSearch = false;// Tim kiem hay load ds mac dinh

	private static final int DATE_FROM_CONTROL = 1;
	private static final int DATE_TO_CONTROL = 2;

	public static ImageListView getInstance(Bundle data) {
		ImageListView f = new ImageListView();
		f.setArguments(data);
		return f;
	}

	/**
	 *
	 * khoi tao control
	 *
	 * @author: HoanPD1
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return: v
	 * @throws:
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_image_list, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
//		setTitleHeaderView(getString(R.string.TITLE_VIEW_IMAGE_LIST));
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_IMAGE_LIST));
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_DSHINHANH);

		PriUtils.getInstance().findViewByIdGone(v, R.id.llSearch, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM);
		PriUtils.getInstance().findViewById(v, R.id.tvPrograme, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_CHUONGTRINH);
		PriUtils.getInstance().findViewById(v, R.id.tvLine, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TUYEN);
		PriUtils.getInstance().findViewById(v, R.id.tvFromDate, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TUNGAY);
		PriUtils.getInstance().findViewById(v, R.id.tvToDate, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_DENNGAY);
		PriUtils.getInstance().findViewById(v, R.id.tvCus, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TENMA);
//		edCusCode = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edCusCode, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TENMA);
		edCusName = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edCusName, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TENMA);
		edFromDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edFromDate, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TUNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);
		edToDate = (VNMEditTextClearable) PriUtils.getInstance().findViewById(v, R.id.edToDate, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_DENNGAY, PriUtils.CONTROL_VNMEDITTEXTCLEARABLE);

		PriUtils.getInstance().setOnTouchListener(edFromDate, this);
		PriUtils.getInstance().setOnTouchListener(edToDate, this);
		edFromDate.setIsHandleDefault(false);
		edToDate.setIsHandleDefault(false);

		edFromDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
				DateUtils.defaultDateFormat.toPattern()));
		edToDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));

		btSearch = (Button) v.findViewById(R.id.btSearch);
		btSearch.setOnClickListener(this);

		tbCusList = (DMSTableView) v.findViewById(R.id.tbCusList);
		tbCusList.setListener(this);
		//init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
			.addInfoCaseUnSensitive(2, SortActionConstants.CODE)
			.addInfoCaseUnSensitive(3, SortActionConstants.NAME)
			.addInfo(6, SortActionConstants.NUM_IMAGE)
			.build();

		initHeaderTable(tbCusList, new ImageListRow(parent, tbCusList), lstSort, this);

		spLine = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spLine, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_TUYEN);
		PriUtils.getInstance().setOnItemSelectedListener(spLine, this);

		spPrograme = (Spinner) PriUtils.getInstance().findViewById(v, R.id.spPrograme, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_CHUONGTRINH);
		PriUtils.getInstance().setOnItemSelectedListener(spPrograme, this);

		btReInput = (Button) PriUtils.getInstance().findViewById(v, R.id.btReInput, PriHashMap.PriControl.NVBH_DANHSACHHINHANH_TIMKIEM_NHAPLAI);
		PriUtils.getInstance().setOnClickListener(btReInput, this);

		if (ilDTO != null && currentPage > 0) {
			spLine.setAdapter(adLine);
			spPrograme.setAdapter(adPrograme);
			renderLayout();
			tbCusList.getPagingControl().setCurrentPage(currentPage);
		} else {
//			adLine = new SpinnerAdapter(parent, R.layout.simple_spinner_item, Constants.getArrayLineChoose());
			adLine = new ArrayAdapter(parent, R.layout.simple_spinner_item, Constants.getArrayLineChoose());
			adLine.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spLine.setAdapter(adLine);
			selectedLineIndex = Constants.getArrayLineChoose().length - 1;
			spLine.setSelection(selectedLineIndex);
			fromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
					DateUtils.defaultDateFormat.toPattern());
			toDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
					DateUtils.defaultDateFormat.toPattern());
			getCustomerList(1, 1);
		}

		return v;

	}

	 /**
	 * reset noi dung
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void resetAllValue() {
		isSearch = false;
		selectedLineIndex = Constants.getArrayLineChoose().length - 1;
		selectedProgrameIndex = 0;
		customerName = "";
		customerCode = "";
		programeId = "";
		currentPage = -1;
		listDisplayPrograme = null;
		resetLayout();
		fromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
				DateUtils.defaultDateFormat.toPattern());
		toDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern());
		fromDateForRequest = "";
		toDateForRequest = "";
	}

	/**
	 *
	 * Reset gia tri tren layout ve gia tri mac dinh
	 *
	 * @author: Nguyen Thanh Dung
	 * @return: void
	 * @throws:
	 */
	private void resetLayout() {
		edFromDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
				DateUtils.defaultDateFormat.toPattern()));
		edToDate.setText(DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
				DateUtils.defaultDateFormat.toPattern()));
		spLine.setSelection(Constants.getArrayLineChoose().length - 1);
		spPrograme.setSelection(0);
//		edCusCode.setText("");
		edCusName.setText("");
	}

	 /**
	 * Cap nhat lai ngay cho cac textbox chon ngay
	 * @author: Tuanlt11
	 * @param dayOfMonth
	 * @param monthOfYear
	 * @param year
	 * @return: void
	 * @throws:
	*/
	public void updateDate(int dayOfMonth, int monthOfYear, int year) {
		// TODO Auto-generated method stub
		String sDay = String.valueOf(dayOfMonth);
		String sMonth = String.valueOf(monthOfYear + 1);
		if (dayOfMonth < 10) {
			sDay = "0" + sDay;
		}
		if (monthOfYear + 1 < 10) {
			sMonth = "0" + sMonth;
		}

		if (currentCalender == DATE_FROM_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				edFromDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				edFromDate.setText(new StringBuilder()
				// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/").append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, null, false);
			}
		}
		if (currentCalender == DATE_TO_CONTROL) {
			if (DateUtils.checkDateInOffsetMonth(sDay, sMonth, year, -2)) {
				edToDate.setTextColor(ImageUtil.getColor(R.color.BLACK));
				edToDate.setText(new StringBuilder()
				// Month is 0 based so add 1
						.append(sDay).append("/").append(sMonth).append("/").append(year).append(" "));
			} else {
				GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID),
						StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, null, false);
			}
		}
	}

	/**
	 *
	 * get danh sach khach hang
	 *
	 * @author: HoanPD1
	 * @param : page
	 * @param : isGetTotolPage
	 * @return: void
	 * @throws:
	 */
	private void getCustomerList(int page, int isGetTotolPage) {// (int page) {
		parent.showLoadingDialog();
		Bundle data = new Bundle();

		getTotalPage = isGetTotolPage;
		data.putInt(IntentConstants.INTENT_PAGE, page);
		data.putInt(IntentConstants.INTENT_GET_TOTAL_PAGE, isGetTotolPage);
		data.putInt(IntentConstants.INTENT_STAFF_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritId());

		if (!StringUtil.isNullOrEmpty(customerCode)) {
			data.putString(IntentConstants.INTENT_CUSTOMER_CODE, customerCode);
		}
		if (!StringUtil.isNullOrEmpty(customerName)) {
			data.putString(IntentConstants.INTENT_CUSTOMER_NAME,
					StringUtil.getEngStringFromUnicodeString(customerName.trim()));
		}
		data.putString(IntentConstants.INTENT_VISIT_PLAN,
				DateUtils.getVisitPlan(Constants.getArrayLineChoose()[selectedLineIndex]));
		data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, programeId);
		data.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE, fromDateForRequest);
		data.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE, toDateForRequest);
		data.putBoolean(IntentConstants.INTENT_IS_SEARCH, isSearch);
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		data.putSerializable(IntentConstants.INTENT_SORT_DATA, tbCusList.getSortInfo());

		handleViewEvent(data, ActionEventConstant.GET_CUSTOMER_IMAGE_LIST, SaleController.getInstance());
	}

	 /**
	 * Cap nhat cac chuong trinh CB
	 * @author: Tuanlt11
	 * @param listPrograme
	 * @return: void
	 * @throws:
	*/
	private void updateProgrameSpinner(List<DisplayProgrameDTO> listPrograme) {
		if (listDisplayPrograme == null) {
			listDisplayPrograme = new ArrayList<DisplayProgrameItemDTO>();
			DisplayProgrameItemDTO itemDTO = new DisplayProgrameItemDTO();
			itemDTO.name = StringUtil.getString(R.string.TEXT_ALL);
			itemDTO.value = "";

			listDisplayPrograme.add(itemDTO);

			if (listPrograme != null) {
				for (int i = 0, size = listPrograme.size(); i < size; i++) {
					DisplayProgrameDTO programe = listPrograme.get(i);

					DisplayProgrameItemDTO item = new DisplayProgrameItemDTO();

//					item.name =  TextUtils.isEmpty(programe.displayProgrameCode) ? "" : programe.displayProgrameCode;
					
					if (!StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& !StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameCode + " - " + programe.displayProgrameName;
					} else if (StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& !StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameName;
					} else if (!StringUtil.isNullOrEmpty(programe.displayProgrameCode)
							&& StringUtil.isNullOrEmpty(programe.displayProgrameName)) {
						item.name = programe.displayProgrameCode;
					} else {
						item.name = "";
					}
					item.value = String.valueOf(programe.displayProgrameId);
					// item.value = String.valueOf(programe.displayProgrameId);
					listDisplayPrograme.add(item);
				}
			}

			int numProgame = listDisplayPrograme.size();
			String[] programeName = new String[numProgame];
			for (int i = 0; i < numProgame; i++) {
				DisplayProgrameItemDTO item = listDisplayPrograme.get(i);

				programeName[i] = item.name;
			}

//			adPrograme = new SpinnerAdapter(parent, R.layout.simple_spinner_item, programeName);
			adPrograme = new ArrayAdapter(parent, R.layout.simple_spinner_item, programeName);
			adPrograme.setDropDownViewResource(R.layout.simple_spinner_dropdown);
			spPrograme.setAdapter(adPrograme);
			spPrograme.setSelection(0);
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		// parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_CUSTOMER_IMAGE_LIST:
			ImageListDTO tempDto = (ImageListDTO) modelEvent.getModelData();
			if (ilDTO != null) {
				ilDTO.listItem.clear();
				ilDTO.listItem.addAll(tempDto.listItem);
			} else {
				// ilDTO = (ImageListDTO) modelEvent.getModelData();
				ilDTO = tempDto;
			}

			if (getTotalPage > 0) {
				ilDTO.totalCustomer = tempDto.totalCustomer;
			}

			if (isUpdateData) {
				isUpdateData = false;
				currentPage = -1;
				ilDTO.listPrograme = tempDto.listPrograme;
			}

			updateProgrameSpinner(ilDTO.listPrograme);
			renderLayout();
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 *
	 * render layout for list customer - image view
	 *
	 * @author: HoanPD1
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		// TODO Auto-generated method stub
		// Phan trang
		tbCusList.clearAllData();
		if (currentPage <= 0) {
			tbCusList.setTotalSize(ilDTO.totalCustomer,1);
			currentPage = tbCusList.getPagingControl().getCurrentPage();
		} else {
			if (isReload) {
				isReload = false;
				tbCusList.setTotalSize(ilDTO.totalCustomer,1);
			}
			tbCusList.getPagingControl().setCurrentPage(currentPage);
		}
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE * (tbCusList.getPagingControl().getCurrentPage() - 1);

		if (ilDTO.listItem.size() > 0) {
			for (int i = 0, s = ilDTO.listItem.size(); i < s; i++) {
				ImageListRow row = new ImageListRow(parent, tbCusList);
				row.renderLayout(pos, ilDTO.listItem.get(i));
				pos++;
				row.setListener(this);
				tbCusList.addRow(row);
			}
		} else {
			tbCusList.showNoContentRow();
		}
	}

	/**
	 *
	 * xu ly khi click button tim kiem
	 *
	 * @author: HoanPD1
	 * @param : v
	 * @return: void
	 * @throws:
	 */
	@Override
	public void onClick(View v) {
		if (v == btSearch) {
			// ilDTO = null;
//			customerCode = edCusCode.getText().toString().trim();
			customerName = edCusName.getText().toString().trim();
			if (spPrograme.getSelectedItemPosition() >= 0) {
				selectedProgrameIndex = spPrograme.getSelectedItemPosition();
			}
			selectedLineIndex = spLine.getSelectedItemPosition();
			if (listDisplayPrograme != null && listDisplayPrograme.size() > 0) {
				programeId = listDisplayPrograme.get(selectedProgrameIndex).value;
			}

			preFromDate = fromDate;
			preToDate = toDate;
			GlobalUtil.forceHideKeyboard(parent);
			// luu lai gia tri de thuc hien tim kiem
			String dateTimePattern = StringUtil.getString(R.string.TEXT_DATE_TIME_PATTERN);
			Pattern pattern = Pattern.compile(dateTimePattern);
			if (!StringUtil.isNullOrEmpty(edFromDate.getText().toString())) {
				String strTN = edFromDate.getText().toString().trim();
				fromDate = strTN;
				Matcher matcher = pattern.matcher(strTN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date tn = StringUtil.stringToDate(strTN, "");
						String strFindTN = StringUtil.dateToString(tn, "yyyy-MM-dd");

						fromDateForRequest = strFindTN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_START_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				fromDate = "";
				fromDateForRequest = "";
			}
			if (!StringUtil.isNullOrEmpty(edToDate.getText().toString())) {
				String strDN = edToDate.getText().toString().trim();
				toDate = strDN;
				Matcher matcher = pattern.matcher(strDN);
				if (!matcher.matches()) {
					parent.showDialog(StringUtil.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
					return;
				} else {
					try {
						Date dn = StringUtil.stringToDate(strDN, "");
						String strFindDN = StringUtil.dateToString(dn, "yyyy-MM-dd");

						toDateForRequest = strFindDN;
					} catch (Exception ex) {
						parent.showDialog(StringUtil.getString(R.string.TEXT_END_DATE_SEARCH_SYNTAX_ERROR));
						return;
					}
				}
			} else {
				toDate = "";
				toDateForRequest = "";
			}

			String currentFromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
					DateUtils.defaultDateFormat.toPattern());
			String currentToDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
					DateUtils.defaultDateFormat.toPattern());

			// Tim kiem: co thay doi it nhat 1 tham so
			if (!StringUtil.isNullOrEmpty(programeId) || !StringUtil.isNullOrEmpty(customerCode)
					|| !StringUtil.isNullOrEmpty(customerName) || !currentFromDate.equals(fromDate)
					|| !currentToDate.equals(toDate) || selectedLineIndex != Constants.getArrayLineChoose().length - 1) {
				if (!StringUtil.isNullOrEmpty(fromDateForRequest) && !StringUtil.isNullOrEmpty(toDateForRequest)
						&& DateUtils.compareDate(fromDateForRequest, toDateForRequest) == 1) {
					GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID_2),
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, null, false);
//					tbCusList.clearAllData();
//					tbCusList.getPagingControl().totalPage = -1;
//					currentPage = -1;
				} else {
					tbCusList.getPagingControl().totalPage = -1;
					currentPage = -1;
					isSearch = true;
					getCustomerList(1, 1);
					preFromDate = fromDate;
					preToDate = toDate;
				}
			} else {// Mode load ds mac dinh ban dau
				if (!StringUtil.isNullOrEmpty(fromDateForRequest) && !StringUtil.isNullOrEmpty(toDateForRequest)
						&& DateUtils.compareDate(fromDateForRequest, toDateForRequest) == 1) {
					GlobalUtil.showDialogConfirm(this, parent, StringUtil.getString(R.string.TEXT_DATE_TIME_INVALID_2),
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, null, false);
//					tbCusList.clearAllData();
//					tbCusList.getPagingControl().totalPage = -1;
//					currentPage = -1;
				} else {
					if (isSearch == true) {
						tbCusList.getPagingControl().totalPage = -1;
						currentPage = -1;
						isSearch = false;
						fromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
								DateUtils.defaultSqlDateFormat.toPattern());
						toDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
								DateUtils.defaultSqlDateFormat.toPattern());
						fromDateForRequest = "";
						toDateForRequest = "";
						getCustomerList(1, 1);
						preFromDate = fromDate;
						preToDate = toDate;
					}
					isSearch = false;
				}
			}

		} else if (v == btReInput) {
			resetLayout();
		}
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		if (control == tbCusList) {
			currentPage = tbCusList.getPagingControl().getCurrentPage();
			getCustomerList(currentPage, 0);
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control, Object data) {

		switch (action) {
		case ImageListRow.ACTION_VIEW_ALBUM: {
			isReload = true;
			ImageListItemDTO dto = (ImageListItemDTO) data;
			CustomerDTO customer  = initDataCustomer(dto);
//			AlbumDTO album = initDataAlbum(dto);

			// O che do khoi tao MH -> MH Hinh anh o Muc thong tin KH
//			if (!isSearch) {
//				gotoListAlbumUser(customer);
//			} else {// O che do tim kiem
//				goProgrameShowImage(customer, album);
//			}
			gotoListAlbumUser(customer, dto.routingId, dto.cycleId);
			break;
		}
		case ImageListRow.ACTION_VIEW_FULL_ALBUM:
			isReload = true;
			ImageListItemDTO dto = (ImageListItemDTO) data;
			CustomerDTO customer  = initDataCustomer(dto);
			AlbumDTO album = initDataAlbum(dto);
			goProgrameShowImage(customer, album);
			break;
		default:
			break;
		}
	}

	/**
	 * Vao man hinh chi tiet album cua chuong trinh
	 *
	 * @author ThanhNN
	 * @param arg2
	 */
	private void goProgrameShowImage(CustomerDTO customer, AlbumDTO album) {
		// TODO Auto-generated method stub
		Bundle bundle = new Bundle();
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customer.getCustomerId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_NAME, customer.getCustomerName());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_CODE, customer.getCustomerCode());
		if (isSearch) {
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
					preFromDate);
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE,
					preToDate);
		}else{
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE,
					DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
							DateUtils.defaultDateFormat.toPattern()));
			bundle.putString(IntentConstants.INTENT_FIND_ORDER_TO_DATE,
					DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
							DateUtils.defaultDateFormat.toPattern()));
		}
		bundle.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_CODE,
				listDisplayPrograme.get(selectedProgrameIndex).name);
		bundle.putInt(IntentConstants.INTENT_TYPE, 4);
		bundle.putSerializable(IntentConstants.INTENT_ALBUM_INFO, album);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME, SaleController.getInstance());
	}

	/**
	 * Toi man hinh ds album cua kh
	 *
	 * @author: Nguyen Thanh Dung
	 * @param customer
	 * @return: void
	 * @throws:
	 */

	private void gotoListAlbumUser(CustomerDTO customer, long routingId, long cycleId) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, customer);
		bundle.putLong(IntentConstants.INTENT_ROUTING_ID, routingId);
		bundle.putLong(IntentConstants.INTENT_CYCLE_ID, cycleId);
		bundle.putInt(IntentConstants.INTENT_TYPE, 4);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());

		DisplayProgrameModel displayModel = new DisplayProgrameModel();
		displayModel.setModelData(ilDTO.listPrograme);
		bundle.putSerializable(IntentConstants.INTENT_DISPLAY_PROGRAM_MODEL, displayModel);

		handleSwitchFragment(bundle, ActionEventConstant.GO_TO_LIST_ALBUM_USER, SaleController.getInstance());
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// hide ban phim
				GlobalUtil.forceHideKeyboard(parent);
				// cau request du lieu man hinh
				isUpdateData = true;
				resetAllValue();
				tbCusList.resetSortInfo();
				getCustomerList(1, 1);
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android
	 * .widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if (arg0 == spLine) {
			if (selectedLineIndex != spLine.getSelectedItemPosition()) {
				// if(isSearch) {
				// selectedLineIndex = spLine.getSelectedItemPosition();
				// }

				// edCusCode.setText("");
				// edCusName.setText("");
				// edCusAdd.setText("");
				// textCusCode = edCusCode.getText().toString().trim();
				// textCusName = edCusName.getText().toString().trim();
				// textCusAdd = edCusAdd.getText().toString().trim();

				// currentPage = -1;
				// getCustomerList(1,1);
			}
		}

		if (arg0 == spPrograme) {
			if (selectedProgrameIndex != spPrograme.getSelectedItemPosition()) {
				// if(isSearch) {
				// selectedProgrameIndex = spPrograme.getSelectedItemPosition();
				// }

				// edCusCode.setText("");
				// edCusName.setText("");

				// textCusCode = edCusCode.getText().toString().trim();
				// textCusName = edCusName.getText().toString().trim();
				// textCusAdd = edCusAdd.getText().toString().trim();

				// currentPage = -1;
				// getCustomerList(1,1);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * android.widget.AdapterView.OnItemSelectedListener#onNothingSelected(android
	 * .widget.AdapterView)
	 */
	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View,
	 * android.view.MotionEvent)
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v == edFromDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_FROM_CONTROL;
				parent.showDatePickerDialog(edFromDate.getText().toString(), true, this);
			}
		}

		if (v == edToDate) {
			if (!v.onTouchEvent(event)) {
				currentCalender = DATE_TO_CONTROL;
				parent.showDatePickerDialog(edToDate.getText().toString(), true, this);
			}
		}
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
//		if(eventType == ACTION_CANCEL) {
//			fromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
//					DateUtils.defaultDateFormat.toPattern());
//			toDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
//					DateUtils.defaultDateFormat.toPattern());
//		}
		switch (eventType) {
		case ACTION_CANCEL:{
			fromDate = DateUtils.convertDateTimeWithFormat(DateUtils.getFirstDateOfOffsetMonth(-2),
					DateUtils.defaultDateFormat.toPattern());
			toDate = DateUtils.convertDateTimeWithFormat(DateUtils.getStartTimeOfDay(new Date()),
					DateUtils.defaultDateFormat.toPattern());
			break;
		}
		case ACTION_PATH:
			handleSwitchFragment(null,ActionEventConstant.GO_TO_CUSTOMER_ROUTE, UserController.getInstance());
			break;
		case ACTION_ORDER_LIST:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_ORDER, UserController.getInstance());
			break;
		case ACTION_CREATE_CUSTOMER:{
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, SaleController.getInstance());
			break;
		}
		case ACTION_CUS_LIST:
			handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
			break;
		case ACTION_LOCK_DATE:
			handleSwitchFragment(null, ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, SaleController.getInstance());
			break;
		default:
			break;
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		enableMenuBar(this);
		addMenuItem(PriHashMap.PriForm.NVBH_DSHINHANH,
		new MenuTab(R.string.TEXT_PHOTO,
				R.drawable.menu_picture_icon, ACTION_IMAGE, PriHashMap.PriForm.NVBH_DSHINHANH),
		new MenuTab(R.string.TEXT_LOCK_DATE_STOCK,
				R.drawable.icon_lock, ACTION_LOCK_DATE, PriHashMap.PriForm.NVBH_CHOTKHO),
		new MenuTab(R.string.TEXT_MENU_ADD_CUSTOMER_NEW, R.drawable.icon_add_new,
				ACTION_CREATE_CUSTOMER, PriHashMap.PriForm.NVBH_THEMMOIKH_DANHSACH),
		new MenuTab(R.string.TEXT_MENU_LIST_ORDER, R.drawable.icon_order,
				ACTION_ORDER_LIST, PriHashMap.PriForm.NVBH_DANHSACHDONHANG),
		new MenuTab(R.string.TEXT_MENU_ROUTE, R.drawable.icon_map,
				ACTION_PATH, PriHashMap.PriForm.NVBH_LOTRINH),
		new MenuTab(R.string.TEXT_MENU_CUS_LIST,R.drawable.menu_customer_icon,
				ACTION_CUS_LIST, PriHashMap.PriForm.NVBH_BANHANG_DSKH));
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		updateDate(dayOfMonth, monthOfYear, year);
	}

	 /**
	 * Khoi tao thong tin KH
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private CustomerDTO initDataCustomer(ImageListItemDTO dto){
		CustomerDTO customer = new CustomerDTO();
		customer.customerId = dto.customerId;
		customer.customerCode = dto.customerCode;
		customer.customerName = dto.customerName;
		customer.setLat(dto.lat);
		customer.setLng(dto.lng);
		customer.setStreet(dto.street);
		customer.setHouseNumber(dto.houseNumber);
		return customer;
	}
	 /**
	 * Khoi tao thong tin album
	 * @author: Tuanlt11
	 * @param dto
	 * @return
	 * @return: AlbumDTO
	 * @throws:
	*/
	private AlbumDTO initDataAlbum(ImageListItemDTO dto){
		AlbumDTO album = new AlbumDTO();
		album.setAlbumTitle(listDisplayPrograme.get(selectedProgrameIndex).name);
		album.setNumImage(dto.imageNumber);
		if(!StringUtil.isNullOrEmpty(listDisplayPrograme.get(selectedProgrameIndex).value)) {
			album.setDisplayProgrameId(Long.parseLong(listDisplayPrograme.get(selectedProgrameIndex).value));
			album.setAlbumType(MediaItemDTO.TYPE_DISPLAY_ALBUM_IMAGE);
		}
		return album;
	}

	/* (non-Javadoc)
	 * @see DMSColSortManager.OnSortChange#onSortChange(DMSTableView, DMSSortInfo)
	 */
	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		// TODO Auto-generated method stub
		getCustomerList(currentPage, 1);
	}
}
