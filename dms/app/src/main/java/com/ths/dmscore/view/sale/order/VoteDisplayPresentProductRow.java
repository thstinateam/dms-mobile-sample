/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.VoteDisplayProductDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 *  display row vote display present product promotion
 *  @author: HaiTC3
 *  @version: 1.0
 *  @since: 1.0
 */
public class VoteDisplayPresentProductRow extends DMSTableRow implements OnClickListener, OnFocusChangeListener,TextWatcher{
	// STT
	TextView tvSTT;
	// code
	TextView tvProductCode;
	// Name
	TextView tvProductName;
	// price
	TextView tvNumberProduct;
	// number
	EditText etVote;

	LinearLayout llEditText;

	/**
	 * constructor for class
	 * @param context
	 * @param aRow
	 */
	public VoteDisplayPresentProductRow(Context context) {
		super(context, R.layout.layout_vote_display_present_product_row);
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvNumberProduct = (TextView) findViewById(R.id.tvNumberProduct);

		etVote = (EditText) findViewById(R.id.etVote);
		etVote.addTextChangedListener(this);
		GlobalUtil.setFilterInputConvfact(etVote, Constants.MAX_LENGHT_QUANTITY);

		llEditText = (LinearLayout) findViewById(R.id.llEditText);
		if (GlobalInfo.getInstance().isUsingCustomKeyboard() && etVote != null ) {
			etVote.setInputType(InputType.TYPE_NULL);
			etVote.setOnClickListener(this);
			etVote.setOnFocusChangeListener(this);
		}
	}

	/**
	 *
	*  init layout for row
	*  @author: HaiTC3
	*  @param position
	*  @param item
	*  @return: void
	*  @throws:
	 */
	public void renderLayout(int position, VoteDisplayProductDTO item) {
		// demo data
		tvSTT.setText(String.valueOf(position));
		tvProductCode.setText(item.productCode);
		tvProductName.setText(item.productName);
//		GlobalUtil.setFilterInputForSearchLike(etVote, 5);
		tvNumberProduct.setText(StringUtil.parseAmountMoney(String.valueOf(item.numberProduct)));
	}

	/**
	 *
	*  init layout for row
	*  @author: HaiTC3
	*  @param position
	*  @param item
	*  @return: void
	*  @throws:
	 */
	public void renderTitle(int position, VoteDisplayProductDTO item) {
		// demo data
		tvSTT.setText("");
		showRowSum(item.groupCode + " - " + item.groupName, tvProductCode,tvProductName);
		etVote.setVisibility(View.GONE);
		tvNumberProduct.setText("");
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (v ==  etVote && context != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v,
					null);
		} else if (v == this && context != null){
			GlobalUtil.forceHideKeyboard((GlobalBaseActivity)context);
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		if (v == etVote && hasFocus) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM, v,
					null);
		}

	}

	/* (non-Javadoc)
	 * @see android.text.TextWatcher#beforeTextChanged(java.lang.CharSequence, int, int, int)
	 */
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see android.text.TextWatcher#onTextChanged(java.lang.CharSequence, int, int, int)
	 */
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see android.text.TextWatcher#afterTextChanged(android.text.Editable)
	 */
	@Override
	public void afterTextChanged(Editable s) {
		// TODO Auto-generated method stub

	}
}
