/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.model.SupervisorModel;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.order.PhotoInventoryEquipmentView;
import com.ths.dmscore.view.sale.statistic.NVBHReportASOView;

/**
 * Controller xu ly nghiep vu cua NVGS
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class SupervisorController extends AbstractController {

	static volatile SupervisorController instance;

	protected SupervisorController() {
	}

	public static SupervisorController getInstance() {
		if (instance == null) {
			instance = new SupervisorController();
		}
		return instance;
	}


	/**
	 * Xu ly du lieu tra ve tu model
	 *
	 * @author: TruongHN
	 * @param modelEvent
	 * @return: void
	 * @throws:
	 */
	public void handleModelEvent(final ModelEvent modelEvent) {
		if (modelEvent.getModelCode() == ErrorConstants.ERROR_CODE_SUCCESS) {
			final ActionEvent e = modelEvent.getActionEvent();
			HTTPRequest request = e.request;
			if (e.sender != null
					&& (request == null || (request != null && request
							.isAlive()))) {
				if (e.sender instanceof GlobalBaseActivity) {
					final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
					if (sender == null || sender.isFinished)
						return;
					sender.runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.isFinished)
								return;
							sender.handleModelViewEvent(modelEvent);
						}
					});
				} else if (e.sender instanceof BaseFragment) {
					final BaseFragment sender = (BaseFragment) e.sender;
					if (sender == null || sender.getActivity() == null || sender.isFinished) {
						return;
					}
					sender.getActivity().runOnUiThread(new Runnable() {
						public void run() {
							if (sender == null || sender.getActivity() == null || sender.isFinished) {
								return;
							}
							sender.handleModelViewEvent(modelEvent);
						}
					});
				}
			} else {
				modelEvent.setIsSendLog(false);
				handleErrorModelEvent(modelEvent);
			}
		} else {
			handleErrorModelEvent(modelEvent);
		}
	}

	@Override
	public void handleSwitchActivity(ActionEvent e) {
		switch (e.action) {

		default:
			break;

		}
	}

	@Override
	public boolean handleSwitchFragment(ActionEvent e) {
		Activity base = null;
		if (e.sender instanceof Activity) {
			base = (Activity) e.sender;
		} else if (e.sender instanceof Fragment) {
			base = ((Fragment) e.sender).getActivity();
		}

		boolean resultSwitch = false;
		if (base != null && !base.isFinishing()) {
			//an ban phim truoc khi chuyen man hinh
			GlobalUtil.forceHideKeyboard(base);

			//chuyen man hinh
			boolean isRemoveAllBackStack = false;
			BaseFragment frag = null;
			Bundle data = (e.viewData instanceof Bundle) ? (Bundle)e.viewData : null;

			//level trong truong hop view quan ly
			int managerLevelCurrent = 1;
			if (data != null) {
				//put addition info switch action
				data.putInt(IntentConstants.INTENT_SWITCH_ACTION, e.action);

				managerLevelCurrent = data.getInt(IntentConstants.INTENT_MANAGER_LEVEL, 1);
			}
			@SuppressWarnings("unused")
			boolean isRemoveAllForManagerView = (managerLevelCurrent == 1);

			switch (e.action) {
//			case ActionEventConstant.ACTION_REPORT_CUSTOMER_NOT_PSDS_IN_MONTH: {
//				isRemoveAllBackStack = true;
//				frag = CustomerNotPSDSInMonthView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorReportDisplayProgressDetailDayView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_CUSTOMER: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorReportDisplayProgressDetailCustomerView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_AMOUNT_REPORT: {
//				isRemoveAllBackStack = true;
//				frag = SupervisorReportStaffView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_MANAGER_EQUIPMENT: {
//				isRemoveAllBackStack = true;
//				frag = ManagerEquipmentView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_GO_TO_TRACKING_CABINET_STAFF: {
//				isRemoveAllBackStack = false;
//				frag = TrackingCabinetStaffView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.PROG_REPOST_SALE_FOCUS: {
//				isRemoveAllBackStack = true;
//				frag = ProgressReportSalesFocusView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_REPORT_PROGRESS_DATE: {
//				isRemoveAllBackStack = true;
//				frag = ReportProgressDateView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_PROGRESS_DATE_DETAIL_VIEW: {
//				isRemoveAllBackStack = false;
//				frag = ReportProgressDateDetailView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GET_GSNPP_ROUTE_SUPERVISION: {
//				isRemoveAllBackStack = true;
//				frag = GsnppRouteSupervisionView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACC_SALE_PROG_REPORT: {
//				isRemoveAllBackStack = true;
//				frag = AccSaleProgReportView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.DIS_PRO_COM_PROG_REPORT: {
//				isRemoveAllBackStack = true;
//				frag = DisProComProgReportView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.STAFF_DIS_PRO_COM_PROG_REPORT: {
//				isRemoveAllBackStack = false;
//				frag = StaffDisProComProgReportView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GSNPP_ROUTE_SUPERVISION_MAP_VIEW:
//				isRemoveAllBackStack = false;
//				frag = GsnppRouteSupervisionMapView.getInstance(data);
//				break;
//
//			case ActionEventConstant.PROG_REPOST_PRO_DISP_DETAIL_SALE: {
//				isRemoveAllBackStack = false;
//				frag = ProgReportProDispDetailSaleView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_DISPLAY_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = SuperVisorDisplayProgramView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_PROMOTION_PROGRAM: {
//				isRemoveAllBackStack = true;
//				frag = SuperVisorPromotionProgramView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_LIST_CUSTOMER_ATTEND_PROGRAM: {
//				break;
//			}
//			case ActionEventConstant.ACTION_LOAD_LIST_CUSTOMER: {
//				isRemoveAllBackStack = true;
//				frag = SupervisorCustomerList.newInstance(data);
//				break;
//			}
////			case ActionEventConstant.GSNPP_TRAINING_RESULT_ACC_REPORT: {
////				isRemoveAllBackStack = true;
////				frag = GsnppTrainingResultAccReportView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.GSNPP_TRAINING_PLAN_DAY_REPORT: {
////				isRemoveAllBackStack = true;
////				frag = GsnppTrainingResultDayReportView.newInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_FOLLOW_LIST_PROBLEM: {
//				isRemoveAllBackStack = true;
//				frag = FollowProblemView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.POST_FEEDBACK_GSNPP: {
//				isRemoveAllBackStack = false;
//				frag = GSNPPPostFeedbackView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_LIST_PROBLEM_OF_GSNPP_NEED_DO_IT: {
//				isRemoveAllBackStack = true;
////				frag = TrackAndFixProblemsOfGSNPPView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GET_LIST_PROMOTION_PROGRAME: {
//				isRemoveAllBackStack = true;
//				frag = SuperVisorPromotionProgramView.getInstance(data);
//				break;
//			}
////			case ActionEventConstant.GSNPP_TRAINING_PLAN: {
////				isRemoveAllBackStack = true;
////				frag = GSNPPTrainingPlanView.newInstance(data);
////				break;
////			}
////			case ActionEventConstant.GO_TO_REVIEWS_STAFF_VIEW: {
////				isRemoveAllBackStack = false;
////				frag = ReviewsStaffView.getInstance(data);
////				break;
////			}
//			case ActionEventConstant.STAFF_INFORMATION: {
//				isRemoveAllBackStack = false;
//				frag = StaffInformationView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.SUPERVISE_STAFF_POSITION: {
//				isRemoveAllBackStack = true;
//				frag = StaffPositionRoutedView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.SUPERVISE_STAFF_POSITION2: {
//				isRemoveAllBackStack = false;
//				frag = StaffPositionRoutedView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_CUSTOMER_SALE_LIST: {
//				isRemoveAllBackStack = true;
//				frag = CustomerSaleList.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_VISIT_CUSTOMER_ON_PLAN: {
//				isRemoveAllBackStack = true;
//				frag = ReportVisitCustomerOnPlanOfDayView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GOTO_CUSTOMER_SALE_LOCATION_RESET: {
//				isRemoveAllBackStack = false;
//				frag = CustomerSaleLocationResetView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_GSNPP_IMAGE_LIST: {
//				isRemoveAllBackStack = true;
//				frag = SupervisorImageListView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_LIST_ALBUM_USER: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorListAlbumUserView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_ALBUM_DETAIL_USER: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorPhotoThumbnailListUserView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_LOAD_IMAGE_FULL: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorFullImageView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_PRODUCT_LIST: {
//				isRemoveAllBackStack = true;
//				frag = SupervisorProductListView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GOTO_PRODUCT_INTRODUCTION: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorIntroduceProductView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GSNPP_GET_LIST_SALE_FOR_ATTENDANCE: {
//				isRemoveAllBackStack = true;
//				frag = SuperVisorTakeAttendanceView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_ALBUM_DETAIL_PROGRAME: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorPhotoThumbnailListProgrameView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_SEARCH_IMAGE: {
//				isRemoveAllBackStack = true;
//				frag = ImageSearchGSNPPView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_KEY_SHOP_LIST_VIEW: {
//				isRemoveAllBackStack = true;
//				frag = KeyShopListView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_DOCUMENT: {
//				isRemoveAllBackStack = true;
//				frag = GSNPPDocumentView.getInstance(data);
//				break;
//			}
////			case ActionEventConstant.GO_TO_GSNPP_TRAINING_HISTORY:{
////				isRemoveAllBackStack = true;
////				frag = GSNPPTrainingHistoryView.getInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_KPI:{
//				isRemoveAllBackStack = true;
//				frag = ListDynamicKPIView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_AMOUNT_CATEGORY: {
//				isRemoveAllBackStack = true;
//				frag = ReportAmountCategoryView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.ACTION_GET_SUPERVISOR_CAT_QUANTITY_REPORT: {
//				isRemoveAllBackStack = true;
//				frag = SupervisorReportCatQuantityView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.NOTE_LIST_VIEW:{
//				isRemoveAllBackStack = true;
//				frag = NoteListView.newInstance(data);
//				break;
//			}
////			case ActionEventConstant.GO_TO_TBHV_FOLLOW_LIST_PROBLEM: {
////				isRemoveAllBackStack = true;
////				frag = TBHVFollowProblemView.newInstance(data);
////				break;
////			}
//			case ActionEventConstant.GO_TO_LIST_ORDER: {
//				isRemoveAllBackStack = true;
//				frag = ListOrderView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_NVBH_REPORT_FORCUS_PRODUCT_VIEW:{
//				isRemoveAllBackStack = false;
//				frag = NVBHReportForcusProductView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_KEY_SHOP: {
//				isRemoveAllBackStack = true;
//				frag = ReportKeyShopView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH_VIEW: {
//				isRemoveAllBackStack = false;
//				frag = ReportKeyShopNVBH.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_KPI_SUPVERVISOR: {
//				isRemoveAllBackStack = true;
//				frag = ReportKPISupervisorView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_ORDER_SUPERVISOR_VIEW: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorOrderView.newInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_ASO_SUPVERVISOR: {
//				isRemoveAllBackStack = true;
//				frag = SupReportASOView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.GO_TO_REPORT_NVBH_ASO: {
				isRemoveAllBackStack = false;
				frag = NVBHReportASOView.getInstance(data);
				break;
			}
//			case ActionEventConstant.GO_TO_CUSTOMER_LIST_REPORT_LOST: {
//				isRemoveAllBackStack = true;
//				frag = CustomerListReportLostEquipmentView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_LIST_DEVICE_REPORT_LOST: {
//				isRemoveAllBackStack = false;
//				frag = ReportLostView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_REPORT_LOST_DEVICE_INFOR: {
//				isRemoveAllBackStack = false;
//				frag = ReportLostInforView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_CUSTOMER_LIST_INVENTORY_DEVICE: {
//				isRemoveAllBackStack = true;
//				frag = CustomerListInventoryEquipmentView.getInstance(data);
//				break;
//			}
//			case ActionEventConstant.GO_TO_INVENTORY_DEVICE: {
//				isRemoveAllBackStack = false;
//				frag = SupervisorInventoryDeviceView.getInstance(data);
//				break;
//			}
			case ActionEventConstant.ACTION_GET_LIST_IMAGE_INVENTORY_EQUIPMENT: {
				isRemoveAllBackStack = false;
				frag = PhotoInventoryEquipmentView.getInstance(data);
				break;
			}
			default:
				break;
			}

			if (frag != null) {
				resultSwitch  = switchFragment(base, frag, isRemoveAllBackStack);
			}
		}

		return resultSwitch;
	}

	@Override
	public void handleViewEvent(final ActionEvent e) {
		if (e.isUsingAsyntask) {
			AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
				protected Void doInBackground(Void... params) {
					SupervisorModel.getInstance().handleControllerEvent(SupervisorController.this,e);
					GlobalBaseActivity base = null;
					if (e.sender instanceof Activity) {
						base = (GlobalBaseActivity) e.sender;
					} else if (e.sender instanceof Fragment) {
						base = (GlobalBaseActivity) ((Fragment) e.sender).getActivity();
					}
					if (e.request != null && base != null) {
						base.addProcessingRequest(e.request, e.isBlockRequest);
					}
					return null;
				}
			};
			task.execute();
		} else {
			SupervisorModel.getInstance().handleControllerEvent(SupervisorController.this, e);
		}
	}
}
