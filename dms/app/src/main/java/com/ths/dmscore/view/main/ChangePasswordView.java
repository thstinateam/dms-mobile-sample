package com.ths.dmscore.view.main;

import java.io.File;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.sqllite.download.DownloadFile;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.util.FileUtility;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.SqlCipherUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dms.R;

/**
 * Change Password
 * 
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class ChangePasswordView extends BaseFragment implements OnFocusChangeListener {
	private static final int ACTION_OK = 0;
	private static final int ACTION_CANCEL = -1;
	private GlobalBaseActivity parent; // parent
	private VNMEditTextClearable edMKcu;// edMKcu
	private VNMEditTextClearable edMKmoi;// edMKmoi
	private VNMEditTextClearable edMKxacnhan;// edMKxacnhan
	private TextView tvMaNV;// tvMaNV
	private TextView tvTenNV;// tvTenNV
	private TextView tvLoaiNV;// tvLoaiNV
	private Button btLuu;// btLuu
	private String newPass;
	private Button btSenDB;
	//file zip db gui ve he thong
	String zipFileDB;

	public static ChangePasswordView newInstance() {
		ChangePasswordView f = new ChangePasswordView();
		// Supply index input as an argument.
		Bundle args = new Bundle();
		f.setArguments(args);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		parent = (GlobalBaseActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.layout_change_password_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		hideHeaderview();
		tvMaNV = (TextView) v.findViewById(R.id.tvMaNV);
		tvTenNV = (TextView) v.findViewById(R.id.tvTenNV);
		tvLoaiNV = (TextView) v.findViewById(R.id.tvLoaiNV);
		edMKcu = (VNMEditTextClearable) v.findViewById(R.id.edMKcu);
		edMKmoi = (VNMEditTextClearable) v.findViewById(R.id.edMKmoi);
		edMKxacnhan = (VNMEditTextClearable) v.findViewById(R.id.edMKxacnhan);
		edMKmoi.setOnFocusChangeListener(this);
		edMKcu.setOnFocusChangeListener(this);
		btLuu = (Button) v.findViewById(R.id.btLuu);
		btLuu.setOnClickListener(this);
		btSenDB = (Button) v.findViewById(R.id.btSenDB);
		btSenDB.setOnClickListener(this);

		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_USER_INFO));

		tvMaNV.setText(GlobalInfo.getInstance().getProfile().getUserData().getUserCode());
		tvTenNV.setText(GlobalInfo.getInstance().getProfile().getUserData().getDisplayName());
		 tvLoaiNV.setText(GlobalInfo.getInstance().getProfile().getUserData().getStaffTypeName());

//		getStaffType();
		return v;
	}

//	/**
//	 * Mo ta muc dich cua ham
//	 * 
//	 * @author: TamPQ
//	 * @return: voidvoid
//	 * @throws:
//	 */
//	private void getStaffType() {
//		ActionEvent e = new ActionEvent();
//		e.action = ActionEventConstant.GET_STAFF_TYPE;
//		e.sender = this;
//		e.viewData = GlobalInfo.getInstance().getProfile().getUserData().staffTypeId;
//		UserController.getInstance().handleViewEvent(e);
//	}

	@Override
	public void onClick(View v) {
		if (v == btLuu) {
			if (isPassValid()) {
				if (GlobalUtil.checkNetworkAccess()) {
					String textConfirm = StringUtil.getString(R.string.TEXT_CHANGE_PASS_CONFIRM);
					GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this, parent, textConfirm,
							StringUtil.getString(R.string.TEXT_BUTTON_AGREE), ACTION_OK,
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), ACTION_CANCEL, -1, true, true);
				} else {
					String mess = StringUtil.getString(R.string.TEXT_CHANGE_PASS_NO_CONNECTION);
					GlobalUtil.showDialogConfirm(this, parent, mess, StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
							-1, -1, false);
				}
			}
		}else if (v == btSenDB) {
			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
			String roleId = sharedPreferences.getString(LoginView.VNM_ROLE_ID, "");
			String shopId = sharedPreferences.getString(LoginView.VNM_SHOP_ID, "");
			
			String dbDirPath = ExternalStorage.getFileDBPath(GlobalInfo.getInstance().getAppContext()).getAbsolutePath();
			String dbPath = dbDirPath + "/" + roleId + "_" + shopId + "_" + Constants.DATABASE_NAME;
			String decryptDbPath = dbDirPath + "/" + roleId + "_" + shopId + "_" 
					+ Constants.DATABASE_NAME + "_bk_" 
					+ DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_FILE_EXPORT);
			String zipFileDB = decryptDbPath + ".zip";
			new CipherTask().execute(dbPath, decryptDbPath, zipFileDB);
		}
		else {
			super.onClick(v);
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		case ACTION_OK:
			parent.showLoadingDialog();
			try {
				String oldPass = edMKcu.getText().toString();
				String newPass = edMKmoi.getText().toString();
				Vector<String> vt = new Vector<String>();
				vt.add(IntentConstants.INTENT_USER_NAME);
				vt.add(GlobalInfo.getInstance().getProfile().getUserData().getUserName());
				vt.add(IntentConstants.NEW_PASS);
				vt.add(newPass);
				vt.add(IntentConstants.OLD_PASS);
				vt.add(oldPass);
				ActionEvent e = new ActionEvent();
				e.viewData = vt;
				e.action = ActionEventConstant.CHANGE_PASS;
				e.sender = this;
				UserController.getInstance().handleViewEvent(e);
			} catch (Exception e) {
				MyLog.e("change pass", e);
			}
			
			break;

		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.GET_STAFF_TYPE:
			if (modelEvent.getModelData() != null) {
				tvLoaiNV.setText((String) modelEvent.getModelData());
			} else {
				tvLoaiNV.setText("");
			}
			break;
		case ActionEventConstant.CHANGE_PASS:
			parent.showDialog(StringUtil.getString(R.string.TEXT_CHANGE_PASS_SUCCEEDED));
			try {
				String newPassEcrypt = StringUtil.generateHash(newPass, GlobalInfo.getInstance().getProfile().getUserData().getUserName().toLowerCase());
				GlobalInfo.getInstance().getProfile().getUserData().setPass(newPassEcrypt);
				SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
				Editor prefsPrivateEditor = sharedPreferences.edit();
				prefsPrivateEditor.putString(LoginView.VNM_PASSWORD, GlobalInfo.getInstance().getProfile()
						.getUserData().getPass());
				prefsPrivateEditor.commit();
				resetValues();
			} catch (Exception e1) {
				MyLog.e("CHANGE_PASS SUCCESS", e1);
			}
			break;

		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.CHANGE_PASS:
			parent.closeProgressDialog();
			switch (modelEvent.getModelCode()) {
			case ErrorConstants.ERROR_POLICY_PASSWORD:
				parent.showDialog(StringUtil.getString(R.string.TEXT_PASS_POLICY_VALIDATE_SERVER));
				break;
			case ErrorConstants.ERROR_SESSION_RESET:
				parent.showDialog(StringUtil.getString(R.string.TEXT_PLEASE_LOGIN_BEFORE_DO));
				break;
			default:
				if(modelEvent != null && !StringUtil.isNullOrEmpty(modelEvent.getModelMessage())){
					parent.showDialog(modelEvent.getModelMessage());
				}else{
					parent.showDialog(StringUtil.getString(R.string.TEXT_MESSAGE_ERROR_COMMON));
				}
				break;
			}
			break;
		default:
			super.handleErrorModelViewEvent(modelEvent);
			break;
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @return: voidvoid
	 * @throws:
	 */
	private void resetValues() {
		edMKcu.setText("");
		edMKmoi.setText("");
		edMKxacnhan.setText("");
		edMKcu.requestFocus();
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @return
	 * @return: booleanvoid
	 * @throws:
	 */
	private boolean isPassValid() {
		boolean isValid = true;
		String pass = GlobalInfo.getInstance().getProfile().getUserData().getPass();

		String oldPass = null;
		try {
			oldPass = StringUtil.generateHash(edMKcu.getText().toString().trim(), GlobalInfo.getInstance().getProfile()
					.getUserData().getUserName().toLowerCase());
			String oldPassInput = edMKcu.getText().toString();
			newPass = edMKmoi.getText().toString().trim();
			String confirmPass = edMKxacnhan.getText().toString().trim();
			if (StringUtil.isNullOrEmpty(edMKcu.getText().toString().trim())) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PLEASE_INPUT) + " "
						+ StringUtil.getString(R.string.TEXT_OLD_PASS));
				edMKcu.requestFocus();
			} else if (StringUtil.isNullOrEmpty(newPass)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PLEASE_INPUT) + " "
						+ StringUtil.getString(R.string.TEXT_NEW_PASS));
				edMKmoi.requestFocus();
			} else if (StringUtil.isNullOrEmpty(confirmPass)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PLEASE_INPUT) + " "
						+ StringUtil.getString(R.string.TEXT_CONFIRMED_PASS));
				edMKxacnhan.requestFocus();
			} else if (!oldPass.equals(pass)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_WRONG_OLD_PASS));
				resetValues();
			} else if (!newPass.equals(confirmPass)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_WRONG_CONFIRMED_PASS));
				resetValues();
				// resetValues();
			} else if (newPass.length() < 6 || newPass.length() > 16) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PASS_6_16));
				resetValues();
			} else if (!StringUtil.checkPolicyPassword(newPass)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PASSWORD_POLICY_FAIL));
				resetValues();
			} else if (newPass.equals(oldPassInput)) {
				isValid = false;
				parent.showDialog(StringUtil.getString(R.string.TEXT_PASSWORD_OLD_NOT_SAME_NEW));
				resetValues();
			}
		} catch (Exception e) {
			isValid = false;
		}
		
		return isValid;
	}
	/**
	 * kiem tra mat khau manh
	 * @author: yennth16
	 * @since: 19:51:57 12-09-2014
	 * @return: void
	 * @throws:  
	 * @param edMK
	 */
//	private void checkStrongPassword(VNMEditTextClearable edMK) {
//		String dateTimePattern = StringUtil.getString(R.string.TEXT_STRONG_PASSWORD_PATTERN);
//		Pattern pattern = Pattern.compile(dateTimePattern);
//		if (!StringUtil.isNullOrEmpty(edMK.getText().toString())) {
//			String strTN = edMK.getText().toString().trim();
//			Matcher matcher = pattern.matcher(strTN);
//			if (!matcher.matches()) {
//				parent.showDialog(StringUtil.getString(R.string.TEXT_STRONG_PASSWORD_SYNTAX_ERROR));
//				return;
//			}
//		}
//	}

	@Override
	public void onFocusChange(View arg0, boolean arg1) {
//		if (arg0 == edMKmoi && !arg1) {
//			checkStrongPassword(edMKmoi);
//		} else if (arg0 == edMKxacnhan && !arg1) {
//			checkStrongPassword(edMKxacnhan);
//		}
	}
	
	private class CipherTask extends AsyncTask<String, Void, Exception> {
		//file zip db gui ve he thong
		String zipFileDB;
		
		@Override
		protected void onPreExecute() {
			parent.showProgressDialog(StringUtil.getString(R.string.TEXT_REPARE_SEND_DATA_WAIT_A_BIT));
		}

		@Override
		protected Exception doInBackground(String... params) {
			File bkFile = null;
			String dbPath = params[0];
			String dbPathBK = dbPath + "_" + System.currentTimeMillis();
			String decryptDbPath = params[1];
			zipFileDB = params[2];
			try {
				bkFile = new File(dbPathBK);
				//copy file db hien tai ra, tranh tranh chap
				DownloadFile.copy(new File(dbPath), bkFile);
				//giai ma db backup
				SqlCipherUtil.decryptCiphertextDB(dbPathBK, decryptDbPath);
				//nen file db moi giai ma
				FileUtility.createZipFile(new File(decryptDbPath), zipFileDB, Constants.CIPHER_KEY);
			} catch (Exception e) {
				return e;
			} finally{
				if (bkFile != null) {
					try {
						bkFile.delete();
					} catch (Exception e2) {}
				}
				try {
					if (!StringUtil.isNullOrEmpty(decryptDbPath)) {
						File filedecrypt = new File(decryptDbPath);
						if (filedecrypt.exists()) {
							filedecrypt.delete();
						}
					}
				} catch (Exception e2) {}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Exception result) {
			parent.closeProgressDialog();
			if (result != null) {
				parent.showDialog(StringUtil.getString(R.string.TEXT_DATA_SEND_FAIL_TRY_AGAIN));
				return;
			} else{
				if(zipFileDB != null){
					parent.showToastMessage(StringUtil.getString(R.string.TEXT_DATA_SEND_MAIL));
					shareToGMail(zipFileDB);
				}
			}
			
		}
	}
	
	public void shareToGMail(String filePaths) {
		String userCode = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		String subject = StringUtil.getString(R.string.app_name) + "_" + userCode + "_DB_" + DateUtils.getCurrentDateTimeWithFormat("dd_MM_yyyy");
		
		StringBuffer content = new StringBuffer();
		content.append(StringUtil.getString(R.string.TEXT_HEADER_SEND_DATA)+" \n");
		content.append(StringUtil.getString(R.string.TEXT_FOLLOW_PROBLEM_STAFF) +": " + userCode);
		content.append(" " + StringUtil.getString(R.string.TEXT_SEND_DATA_ANALYSIC));
		content.append("\n" + StringUtil.getString(R.string.TEXT_GOODBYE));
		
	    Intent emailIntent = new Intent(Intent.ACTION_SEND);
	    emailIntent.putExtra(Intent.EXTRA_EMAIL,  new String[] {"dmsviettel@gmail.com"});
	    emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
	    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, content.toString());
	    emailIntent.setType("text/plain");
		// convert from paths to Android friendly Parcelable Uri's
		File fileIn = new File(filePaths);
		Uri u = Uri.fromFile(fileIn);
		emailIntent.putExtra(Intent.EXTRA_STREAM, u);
	 		
		final PackageManager pm = GlobalInfo.getInstance().getActivityContext()
				.getPackageManager();
		final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
		ResolveInfo best = null;
		for (final ResolveInfo info : matches)
			if (info.activityInfo.packageName.endsWith(".gm")
					|| info.activityInfo.name.toLowerCase().contains("gmail"))
				best = info;
		if (best != null){
			emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
		}
		try {
			startActivity(emailIntent, StringUtil.getString(R.string.TEXT_SEND_MAIL_DATA));
		} catch (ActivityNotFoundException e) {
			parent.showDialog(StringUtil.getString(R.string.TEXT_SEND_MAIL_FAIL_NO_APP));
		} catch (Exception e) {
			parent.showDialog(StringUtil.getString(R.string.TEXT_SEND_MAIL_FAIL_TRY_AGAIN));
		}
		
	}

}
