package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * DTO cua man hinh bao cao chuong trinh trung bay
 * @author hieunq1
 *
 */
/**
 * DisProComProgReportItem
 * 
 * @author hieunq1
 * 
 */
public class SupervisorReportDisplayProgressItem implements Serializable {
	private static final long serialVersionUID = -7184198439225859929L;
	public String programId;
	public String programCode;
	public String programName;
	public String dateFromTo;
	public ArrayList<ReportDisplayProgressResult> arrLevelCode;
	public ReportDisplayProgressResult itemResultTotal;

	public SupervisorReportDisplayProgressItem() {
		arrLevelCode = new ArrayList<ReportDisplayProgressResult>();
		itemResultTotal = new ReportDisplayProgressResult();
		programCode = "";
		programName = "";
		dateFromTo = "";
	}

	public SupervisorReportDisplayProgressItem(Cursor c) {
		programCode = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_CODE);
		programName = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_NAME);
	}
	
}
