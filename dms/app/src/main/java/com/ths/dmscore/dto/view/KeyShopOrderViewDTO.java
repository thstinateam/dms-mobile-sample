package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * DTO cho phan tra thuong keyshop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KeyShopOrderViewDTO implements Serializable {

	// noi dung field
	private static final long serialVersionUID = 1L;
	public ArrayList<KeyShopOrderDTO> lstKeyShopOrder = new ArrayList<KeyShopOrderDTO>();

}
