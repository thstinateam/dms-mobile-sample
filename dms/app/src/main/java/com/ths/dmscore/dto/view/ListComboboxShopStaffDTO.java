/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * Danh sach combobox shop va staff
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 10:09:09 26-03-2015
 */
public class ListComboboxShopStaffDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	// danh sach shop
	public ArrayList<ShopDTO> listShop; 
	// danh sach nhan vien
	public ArrayList<StaffItemDTO> listStaff;
	// danh sacch chuong trinh trung bay
	public ArrayList<DisplayProgrameDTO> listDisplayProgram;
	public ArrayList<ValueItemDTO> listCycle;
	public String curentCycle;
	public String listStaffId;
	public ArrayList<DisplayProgrameDTO> listDisplayProgramThreeCycle;
	
	public ListComboboxShopStaffDTO() {
		listShop = new ArrayList<ShopDTO>();
		listStaff = new ArrayList<StaffItemDTO>();
		listDisplayProgram = new ArrayList<DisplayProgrameDTO>();
		listStaffId = "";
		listDisplayProgramThreeCycle = new ArrayList<DisplayProgrameDTO>();
	}
	
	public StaffItemDTO newStaffItem(){
		return  new StaffItemDTO();
	}

	public class StaffItemDTO implements Serializable {
		private static final long serialVersionUID = 1L;

		// id nhan vien
		public long staffId;
		// inherit id
		public long inheritId;
		// ma nhan vien
		public String staffCode;
		// ten nhan vien
		public String staffName;
		// toa nhan vien
		public double staffLat;
		public double staffLng;
		// loai Nhan vien
		public int objectType;// loai gs
		
		public StaffItemDTO (){
			
		}

		public StaffItemDTO addAll(long id, String name, String code) {		
			this.staffId = id;
			this.staffCode = code;
			this.staffName = name;
			return new StaffItemDTO();
		}

		/**
		 * init data with cursor
		 * 
		 * @author: hoanpd1
		 * @since: 18:45:45 12-03-2015
		 * @return: void
		 * @throws:
		 * @param c
		 */
		public void initObjectWithCursor(Cursor c) throws Exception {
			staffId = CursorUtil.getInt(c, "STAFF_ID");
			inheritId = CursorUtil.getInt(c, "INHERIT_ID");
			staffCode = CursorUtil.getString(c, "STAFF_CODE");
			staffName = CursorUtil.getString(c, "STAFF_NAME");
			objectType = CursorUtil.getInt(c, "OBJECT_TYPE");
		}
	}
}
