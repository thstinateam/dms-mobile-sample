/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

/**
 * Thong tin mo ta doanh so sku cua khach hang mua
 * @author BANGHN
 * @version 1.0
 */
public class CustomerCatAmountPopupDTO implements Serializable{
	private static final long serialVersionUID = -2396927783186409326L;
	//id san pham
	public long productId;
	//ma san pham
	public String categoryCode = "";
	//ten san pham
	public String categoryName = "";
	//doanh so cua san pham
	public double[] amountArray = {0, 0, 0, 0};
	//san luong san pham
	public long[] quantitytArray = {0, 0, 0, 0};
	
	
	/**
	 * Parse du lieu sau khi select tu db
	 * @author: BANGHN
	 * @param c
	 */
	public void parseFromCursor(Cursor c){
//		if (c.getColumnIndex("PRODUCT_ID") >= 0) {
//			productId = c.getLong(c.getColumnIndex("PRODUCT_ID"));
//		}
//		if (c.getColumnIndex("PRODUCT_CODE") >= 0) {
//			productCode = c.getString(c.getColumnIndex("PRODUCT_CODE"));
//		}
//		if (c.getColumnIndex("PRODUCT_NAME") >= 0) {
//			productName = c.getString(c.getColumnIndex("PRODUCT_NAME"));
//		}
	}
}
