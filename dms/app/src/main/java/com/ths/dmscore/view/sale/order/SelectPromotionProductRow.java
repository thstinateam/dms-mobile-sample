/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 *  row display for select promotion product view
 *  @author: HaiTC3
 *  @version: 1.0
 *  @since: 1.0
 */
public class SelectPromotionProductRow extends DMSTableRow implements OnClickListener{
	// STT
	TextView tvSTT;
	// code
	TextView tvProductCode;
	// Name
	TextView tvProductName;
	// number stock
	TextView tvNumStock;
//	// number tvPrice
//	TextView tvPrice;
	// number tvNumberPromotion
	TextView tvNumberPromotion;
	// number stock Thuc te
	TextView tvNumStockActual;

	// listener
	protected VinamilkTableListener listener;
	// table parent
	private View tableParent;
	// data to render layout for row
	OrderDetailViewDTO myData;

	/**
	 * constructor for class
	 * @param context
	 * @param aRow
	 */
	public SelectPromotionProductRow(Context context, View aRow) {
		super(context,R.layout.layout_select_product_promotion_row);
		tableParent = aRow;
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
//		tvProductCode.setOnClickListener(this);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvNumStock = (TextView) findViewById(R.id.tvNumStock);
		tvNumStockActual = (TextView) findViewById(R.id.tvNumStockActual);
//		tvPrice = (TextView) findViewById(R.id.tvPrice);
		tvNumberPromotion = (TextView) findViewById(R.id.tvNumberPromotion);
		myData = new OrderDetailViewDTO();
	}

	public void setListener(VinamilkTableListener listener) {
		this.listener = listener;
	}

	/**
	 *
	*  init layout for row
	*  @author: HaiTC3
	*  @param position
	*  @param item
	*  @return: void
	*  @throws:
	 */
	public void renderLayout(int position, OrderDetailViewDTO item) {
		this.myData = item;
		tvSTT.setText(String.valueOf(position));
		tvProductCode.setText(item.productCode);
		tvProductName.setText(item.productName);
//		long stock1 = item.stock/item.convfact;
//		long stock2 = item.stock % item.convfact;
//		String stock = String.valueOf(stock1) + "/" + String.valueOf(stock2);
		if (StringUtil.isNullOrEmpty(item.remaindStockFormat)) {
			item.remaindStockFormat = GlobalUtil
					.formatNumberProductFlowConvfact(item.stock,
							item.convfact);
		}
		// </HaiTC>
		if (StringUtil.isNullOrEmpty(item.remaindStockFormatActual)) {
			item.remaindStockFormatActual = GlobalUtil
					.formatNumberProductFlowConvfact(item.stockActual,
							item.convfact);
		}
		tvNumStock.setText(item.remaindStockFormat);
		tvNumStockActual.setText(item.remaindStockFormatActual);
//		tvPrice.setText(StringUtil.parseAmountMoney(String.valueOf(item.orderDetailDTO.price)));
		tvNumberPromotion.setText(String.valueOf(item.orderDetailDTO.maxQuantityFree));

		//Render check stock total
		checkStockTotal(item);
	}

	/**
	 * Kiem tra ton kho
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */

	public void checkStockTotal(OrderDetailViewDTO item) {
		if(item.stock <= 0){
			this.updateColorForRow(ImageUtil.getColor(R.color.RED));
		}
		else if(item.stock < item.totalOrderQuantity.orderDetailDTO.quantity){
			this.updateColorForRow(ImageUtil.getColor(R.color.OGRANGE));
		}
	}

	/**
	 *
	*  update color for row
	*  @param color
	*  @return: void
	*  @throws:
	*  @author: HaiTC3
	*  @date: Oct 24, 2012
	 */
	public void updateColorForRow(int color){
		tvSTT.setTextColor(color);
		tvProductCode.setTextColor(color);
		tvProductName.setTextColor(color);
		tvNumStock.setTextColor(color);
		tvNumStockActual.setTextColor(color);
//		tvPrice.setTextColor(color);
		tvNumberPromotion.setTextColor(color);
	}

	/**
	 *
	*  cap nhat layout khi dong nay da duoc selected
	*  @author: HaiTC3
	*  @return: void
	*  @throws:
	 */
	public void updateLayoutSelected(){
		tvSTT.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvProductCode.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvProductName.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvNumStock.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvNumStockActual.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
//		tvPrice.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
		tvNumberPromotion.setBackgroundColor(getResources().getColor(R.color.BG_ITEM_SELECT));
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == this && listener != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.BROADCAST_CHANGE_PROMOTION_PRODUCT, tableParent, this.myData);
		}
	}
}
