package com.ths.dmscore.view.sale.customer;

import java.util.ArrayList;

import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;

public class CustomerFeedBackDto {
	public int totalFeedBack = -1;
	public int currentPage = 0;
	public CustomerDTO customer;
	public ArrayList<ApParamDTO> typeList;
//	public ArrayList<ApParamDTO> gsnppTypeList;

	public CustomerDTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public ArrayList<FeedBackDTO> arrItem = new ArrayList<FeedBackDTO>();

	public CustomerFeedBackDto() {
	}

//	public void getGsnppTypeList() {
//		gsnppTypeList = new ArrayList<ApParamDTO>();
//		for (ApParamDTO element : typeList) {
//			if (element.type.equals("FEEDBACK_TYPE_GSNPP")) {
//				gsnppTypeList.add(element);
//			}
//		}
//	}
}
