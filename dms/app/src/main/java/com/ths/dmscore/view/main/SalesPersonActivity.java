package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.view.ProblemsFeedBackDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.TransactionProcessManager;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.sale.customer.CustomerListView;
import com.ths.dmscore.view.sale.order.OrderView;
import com.ths.dmscore.view.sale.order.VoteDisplayPresentProductView;
import com.ths.dmscore.view.sale.salestatistics.SaleStatisticsInDayPreSalesView;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.controller.SaleController;
import com.ths.dmscore.controller.UserController;
import com.ths.dmscore.dto.MenuAndSubMenu;
import com.ths.dmscore.dto.db.LockDateDTO;
import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.dto.db.ShopLockDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.ImageValidatorTakingPhoto;
import com.ths.dmscore.util.LatLng;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriHashMap.PriForm;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.view.sale.image.ImageListView;
import com.ths.dmscore.view.sale.order.ListProblemsFeedbackRow;
import com.ths.dmscore.view.sale.order.RemainProductView;
import com.ths.dmscore.view.sale.statistic.GeneralStatisticsView;
import com.ths.dms.R;

/**
 * Activity chinh: Chua cac man hinh cua nhan vien ban hang
 *
 * @author : BangHN since : 4:41:25 PM version :
 */
public class SalesPersonActivity extends RoleActivity implements
		OnClickListener, OnItemClickListener {
	// confirm show camera
	public static final int CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR = 13;
	// dong y thoat man hinh dat hang cancle thoat man hinh dat hang
	public static final int CONFIRM_EXIT_ORDER_VIEW_OK = 14;

	public static final int CONFIRM_EXIT_ORDER_VIEW_CANCEL = 15;
	// dong y ket thuc ghe tham
	private static final int ACTION_FINISH_VISIT_CUS_OK = 16;
	// cancel ket thuc ghe tham
	private static final int ACTION_FINISH_VISIT_CUS_CANCEL = 17;
	// dong y thoat man hinh dat hang khi da ket thuc ghe tham
	private static final int CONFIRM_EXIT_ORDER_VIEW_VISIT_OK = 18;
	public boolean isShowMenu = false;
	boolean isTakePhotoFromMenuCloseCustomer = false;
	boolean isSaveActionLogAndCloseCustomerAfterTakePhoto = false;
	private int currPageFeedBack = -1;
	boolean isShowTextInMenu = false;
	private PopupProblemsListener listener = null;
	// thoi gian tu server tra ve
	private DMSTableView tbListProblems;
	private AlertDialog alertProductDetail;
	private Button btnDetail;
	private Button btnPass;
	private TextView tvTitle;
	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	private GoogleApiClient client;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, PriHashMap.PriMenu.NVBH_MENU, PriForm.NVBH_MENU);
		// check time
		validateTimeClientServer();

		// TamPQ : xu ly trang thai ghe tham neu thoat dot ngot ( force close)
		checkVisitFromActionLog();
		// check va gui toan bo log dang con ton dong
		TransactionProcessManager.getInstance().startChecking(
				TransactionProcessManager.SYNC_NORMAL);

		MyLog.i("TamPQ", "activity onCreate");

		// xu li khi vao lan dau khong sendbroadcast duoc
		readLockDateState();
		readShopLock();
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
		client.connect();
		MyLog.i("TamPQ", "activity onStart");
		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.start(client, getIndexApiAction());
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MyLog.i("TamPQ", "activity onResume");
	}

	/**
	 * ATTENTION: This was auto-generated to implement the App Indexing API.
	 * See https://g.co/AppIndexing/AndroidStudio for more information.
	 */
	public Action getIndexApiAction() {
		Thing object = new Thing.Builder()
				.setName("SalesPerson Page") // TODO: Define a title for the content shown.
				// TODO: Make sure this auto-generated URL is correct.
				.setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
				.build();
		return new Action.Builder(Action.TYPE_VIEW)
				.setObject(object)
				.setActionStatus(Action.STATUS_TYPE_COMPLETED)
				.build();
	}

	@Override
	public void onStop() {
		super.onStop();

		// ATTENTION: This was auto-generated to implement the App Indexing API.
		// See https://g.co/AppIndexing/AndroidStudio for more information.
		AppIndex.AppIndexApi.end(client, getIndexApiAction());
		client.disconnect();
	}

	public interface PopupProblemsListener {
		public void closePopup();

		public void gotoCustomerInfo();

	}

	/**
	 * Link den MH can thuc hien khi click xem chi tiet popup Nhung van de can thuc hien
	 *
	 * @author: yennth16
	 * @since: 15:02:30 26-05-2015
	 * @return: void
	 * @throws:
	 */
	public void gotoNoteListView() {
		handleSwitchFragment(new Bundle(), ActionEventConstant.NOTE_LIST_VIEW, SaleController.getInstance());
	}

	/**
	 * Thong bao cac van de can thuc hien
	 *
	 * @author quangvt
	 */
	public void showPopupProblemsFeedBack(PopupProblemsListener l) {
		listener = l;
		// Kiem tra trong tuye, da ket thuc hay chua
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		if (
			// action.objectType.equals("0")
			// &&
				StringUtil.isNullOrEmpty(action.endTime) && action.isOr == 0) {
			int staffId = action.staffId;
			long cusId = action.aCustomer.customerId;
			currPageFeedBack = 1;
			showPopUp(action.aCustomer);
			getListProblemsFeedBack(staffId, cusId, currPageFeedBack);
		} else if (StringUtil.isNullOrEmpty(action.endTime) && action.isOr == 1) {
			if (l != null) {
				l.closePopup();
			}
		}
	}

	/**
	 * show popup nhung van de can thuc hien khi ket thuc ghe tham
	 *
	 * @param aCustomer
	 * @author: yennth16
	 * @since: 17:13:38 26-05-2015
	 * @return: void
	 * @throws:
	 */
	private void showPopUp(CustomerDTO aCustomer) {
		if (alertProductDetail == null) {
			Builder build = new Builder(this,
					R.style.CustomDialogTheme);
			build.setCancelable(false);
			LayoutInflater inflater = getLayoutInflater();
			View view = inflater.inflate(
					R.layout.layout_list_problems_feedback, null);

			tbListProblems = (DMSTableView) view
					.findViewById(R.id.tbListProblems);
			tbListProblems.setListener(new ListenerTableProblemsFeedback());
			btnDetail = (Button) view.findViewById(R.id.btDetail);
			btnDetail.setOnClickListener(this);
			btnPass = (Button) view.findViewById(R.id.btnPass);
			btnPass.setOnClickListener(this);
			tvTitle = (TextView) view.findViewById(R.id.tvTitle);
			initHeaderTable(tbListProblems, new ListProblemsFeedbackRow(this));

			build.setView(view);
			alertProductDetail = build.create();
			// alertProductDetail.setCancelable(false);

			Window window = alertProductDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}

		SpannableObject spanTitle = new SpannableObject();
		spanTitle.addSpan(StringUtil.getString(R.string.TEXT_PROBLEMS_NEED_DO),
				ImageUtil.getColor(R.color.BLACK),
				Typeface.NORMAL);
		spanTitle.addSpan(" " + aCustomer.customerCode + " - "
						+ aCustomer.customerName, ImageUtil.getColor(R.color.BLACK),
				Typeface.BOLD);
		tvTitle.setText(spanTitle.getSpan());

		// alertProductDetail.show();
	}

	/**
	 * Khoi tao header table
	 *
	 * @param tb
	 * @param header
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	protected void initHeaderTable(DMSTableView tb, DMSTableRow header) {
		tb.addHeader(header);
	}

	@Override
	protected void onPostResume() {
		// TODO Auto-generated method stub
		super.onPostResume();
		MyLog.i("TamPQ", "activity onPostResume");
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		MyLog.i("TamPQ", "activity onPostCreate");
	}

	/**
	 * Khoi tao menu chuong trinh
	 *
	 * @author : BangHN since : 4:41:56 PM
	 */
	protected void initMenuGroups() {
		lstMenuItems = new ArrayList<MenuItemDTO>();
		MenuItemDTO item1 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_SUMMARIZE),
				R.drawable.menu_report_icon);

		// list menu tong quan
		MenuAndSubMenu listTongQuan = new MenuAndSubMenu();

		// thong ke chung
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_REPORT),
				R.drawable.icon_reminders);
        // chart
        listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CHART),
                R.drawable.icon_reminders);
		// chart
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CHART),
				R.drawable.icon_reminders);
		// mat hang trong tam
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_FOCUS_PRODUCT),
				R.drawable.icon_list_star);
		// can thuc hien
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_MENU_NEED_DONE),
//				R.drawable.icon_order);
		// Chuong trinh trung bay
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_DISPLAY_PROGRAM),
//				R.drawable.icon_list_check);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KEY_SHOP),
				R.drawable.icon_report_keyshop);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_KPI),
				R.drawable.icon_kpi);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_CUSTOMER_SALE),
				R.drawable.icon_cust_report);
		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_REPORT_NVBH_ASO),
				R.drawable.icon_product_report);
		// doanh so nganh
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_MENU_AMOUNT_CATEGORY),
//				R.drawable.icon_list_check);
//		// san luong nganh
//		listTongQuan.addSubMenu(StringUtil.getString(R.string.TEXT_HEADER_MENU_QUANTITY_CATEGORY),
//				R.drawable.icon_list_check);

		item1.setItems(listTongQuan.getListSubMenu());

		// menu and submenu Don tong
		MenuItemDTO item2 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_STATISTICS),
				R.drawable.icon_cost);

		MenuAndSubMenu listDonTong = new MenuAndSubMenu();
		// submenu Don tong ngay pre
		listDonTong.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_SALE_STATISTICS_IN_DAY_PRE),
				R.drawable.icon_calendar);
		// submenu Don tong ngay val
		listDonTong.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_SALE_STATISTICS_IN_DAY_VAL),
				R.drawable.icon_calendar_2);
		// submenu Don tong luy ke
		listDonTong.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_REPORT_MONTH),
				R.drawable.icon_accumulated);
		item2.setItems(listDonTong.getListSubMenu());

		MenuItemDTO item3 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_MENU_SALES),
				R.drawable.icon_sales);
		item3.menuIndex = MENU_INDEX_SELECTED;
		// menu and submenu Ban hang
		MenuAndSubMenu listBanHang = new MenuAndSubMenu();
		// submenu GS Huan luyen NVBH
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_CUSTOMER_LIST_VIEW),
				R.drawable.ico_customer_list);
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_ROUTE),
				R.drawable.icon_map);
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_LIST_ORDER),
				R.drawable.icon_order);
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_ADD_CUSTOMER_NEW),
				R.drawable.icon_add_new);
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_LOCK_DATE),
				R.drawable.icon_lock);
		listBanHang.addSubMenu(StringUtil.getString(R.string.TEXT_IMAGE_CUSTOMER_LIST_VIEW),
				R.drawable.menu_picture_icon);
		item3.setItems(listBanHang.getListSubMenu());

		// menu and submenu HTTM
		MenuItemDTO item4 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_CATEGORY),
				R.drawable.icon_categories);
		MenuAndSubMenu listCTHTTM = new MenuAndSubMenu();
		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_PRODUCT),
				R.drawable.icon_product_list);
		// submenu Ke Hoach Huan luyen
		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CTKM),
				R.drawable.menu_promotion_icon);
//		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_CTTB),
//				R.drawable.menu_manage_icon);
		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_SEARCH_IMAGE),
				R.drawable.icon_image_search);
		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_LIST_KEY_SHOP),
				R.drawable.icon_document);
//		listCTHTTM.addSubMenu(StringUtil.getString(R.string.TEXT_OFFICE_DOCUMENT),
//				R.drawable.icon_document);

		item4.setItems(listCTHTTM.getListSubMenu());

		//menu and submenu Hinh anh
//		MenuItemDTO item5 = new MenuItemDTO(
//				StringUtil.getString(R.string.TEXT_PICTURE),
//				R.drawable.menu_picture_icon);
//
//		// submenu San pham
//		MenuAndSubMenu listHinhAnh = new MenuAndSubMenu();
//		listHinhAnh.addSubMenu(StringUtil.getString(R.string.TEXT_PHOTO),
//				R.drawable.menu_manage_icon);
//		listHinhAnh.addSubMenu(StringUtil.getString(R.string.TEXT_SEARCH_IMAGE),
//				R.drawable.icon_image_search);
//		// submenu CTKM
//		item5.setItems(listHinhAnh.getListSubMenu());

		// menu and submenu DSSP
//		MenuItemDTO item6 = new MenuItemDTO(
//				StringUtil.getString(R.string.TEXT_LIST_PRODUCT),
//				R.drawable.icon_product_list);

//cong no
		MenuItemDTO item6 = new MenuItemDTO(
				StringUtil.getString(R.string.TEXT_DEBT),
				R.drawable.icon_table_money);

//		MenuItemDTO item8 = new MenuItemDTO(
//				StringUtil.getString(R.string.TEXT_LOCK_DATE),
//				R.drawable.icon_lock);
		// menu va submenu Theo doi khac phuc
		MenuAndSubMenu listTheoDoiKhacPhuc = new MenuAndSubMenu();
		MenuItemDTO item7 = new MenuItemDTO(StringUtil.getString(R.string.TEXT_PROBLEMS_MANAGE),
				R.drawable.menu_problem_icon);
		// submenu Theo doi khac phuc
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_TAKE_PROBLEM),
				R.drawable.icon_feedback);
		// submenu Theo doi khac phuc
		listTheoDoiKhacPhuc.addSubMenu(StringUtil.getString(R.string.TEXT_MENU_NEED_DO_IT),
				R.drawable.icon_feedback2);

		item7.setItems(listTheoDoiKhacPhuc.getListSubMenu());

//		MenuItemDTO item10 = new MenuItemDTO(
//				StringUtil.getString(R.string.TEXT_DYNAMIC_REPORT),
//				R.drawable.icon_product_list);
		item1.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_TONGQUAN.getMenuName();
		item2.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_DONTONG.getMenuName();
		item3.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_BANHANG.getMenuName();
		item4.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_DANHMUC.getMenuName();
//		item5.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_HINHANH.getMenuName();
//		item6.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_DANHSACHSP.getMenuName();

		//cong no
		item6.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_CONGNO.getMenuName();
		item7.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_THEODOIKHACPHUC.getMenuName();
//		item8.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_KHO.getMenuName();
//		item10.controlOrdinal = PriHashMap.PriMenu.NVBH_MENU_KPI.getMenuName();
		lstMenuItems.add(item1);
		lstMenuItems.add(item2);
		lstMenuItems.add(item3);
		lstMenuItems.add(item4);
//		lstMenuItems.add(item5);
//		lstMenuItems.add(item6);
		//cong no
		lstMenuItems.add(item6);
		lstMenuItems.add(item7);
//		lstMenuItems.add(item8);
//		lstMenuItems.add(item10);

		initHashActionbar();

//		PriUtils.getInstance().checkMenu(lstMenuItems);

		setSelectMenu(item3.menuIndex);
	}

	@Override
	protected void initHashActionbar() {
		hashActionbar = new SparseArray<List<MenuTab>>();

		int i = 0;

		// Menu Thong ke chung
		List<MenuTab> listThongke = new ArrayList<MenuTab>();
		listThongke.add(new MenuTab(-1, PriForm.NVBH_THONGKECHUNG_THONGKE));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_THONGKECHUNG_THONGKE));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_THONGKECHUNG_THONGKE));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_BAOCAOMHTT));
//		listThongke.add(new MenuTab(ActionEventConstant.GO_TO_NVBH_NEED_DONE_VIEW, PriHashMap.PriForm.NVBH_THONGKECHUNG_CANTHUCHIEN));
//		listThongke.add(new MenuTab(ActionEventConstant.REPORT_DISPLAY_PROGRESS_DETAIL_DAY, PriHashMap.PriForm.NVBH_BAOCAOCTTB));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_BAOCAOKEYSHOP));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_BAOCAOKPI));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_BAOCAO_SLDS_KH));
		listThongke.add(new MenuTab(-1, PriForm.NVBH_BAOCAO_ASO_KH));
		hashActionbar.put(i, listThongke);
		i++;

		// Don tong
		List<MenuTab> listDontong = new ArrayList<MenuTab>();
		listDontong.add(new MenuTab(-1, PriForm.NVBH_DONTONGNGAYPRE));
		listDontong.add(new MenuTab(-1, PriForm.NVBH_DONTONGNGAYVAN));
		listDontong.add(new MenuTab(-1, PriForm.NVBH_DONTONGLUYKE));
		hashActionbar.put(i, listDontong);
		i++;

		// Ban hang
		List<MenuTab> listBanhang = new ArrayList<MenuTab>();
		listBanhang.add(new MenuTab(ActionEventConstant.GET_CUSTOMER_LIST, PriForm.NVBH_BANHANG_DSKH));
		listBanhang.add(new MenuTab(ActionEventConstant.GO_TO_CUSTOMER_ROUTE, PriForm.NVBH_LOTRINH));
		listBanhang.add(new MenuTab(ActionEventConstant.GO_TO_LIST_ORDER, PriForm.NVBH_DANHSACHDONHANG));
		listBanhang.add(new MenuTab(ActionEventConstant.GO_TO_LIST_CUSTOMER_CREATED, PriForm.NVBH_THEMMOIKH_DANHSACH));
		listBanhang.add(new MenuTab(ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, PriForm.NVBH_CHOTKHO));
		listBanhang.add(new MenuTab(ActionEventConstant.GO_TO_IMAGE_LIST, PriForm.NVBH_DSHINHANH));

		hashActionbar.put(i, listBanhang);
		i++;

		// Ho tro thuong mai
		List<MenuTab> listHotrothuongmai = new ArrayList<MenuTab>();
		listHotrothuongmai.add(new MenuTab(-1, PriForm.NVBH_DSSANPHAM));
		listHotrothuongmai.add(new MenuTab(-1, PriForm.NVBH_CTKM));
//		listHotrothuongmai.add(new MenuTab(ActionEventConstant.GO_TO_DISPLAY_PROGRAM, PriForm.NVBH_CTTB));
		listHotrothuongmai.add(new MenuTab(-1, PriForm.NVBH_TIMKIEMHINHANH));
		listHotrothuongmai.add(new MenuTab(-1, PriForm.NVBH_DANHSACHCLB));
//		listHotrothuongmai.add(new MenuTab(ActionEventConstant.GO_TO_DOCUMENT, PriForm.NVBH_HOTROTHUONGMAI_CONGVAN));
		hashActionbar.put(i, listHotrothuongmai);
		i++;

//		// Hinh anh
//		List<MenuTab> listHinhanh = new ArrayList<MenuTab>();
//		listHinhanh.add(new MenuTab(ActionEventConstant.GO_TO_IMAGE_LIST, PriHashMap.PriForm.NVBH_DANHSACHHINHANH));
//		listHinhanh.add(new MenuTab(ActionEventConstant.GO_TO_IMAGE_LIST_SEARCH, PriHashMap.PriForm.NVBH_TIMKIEMHINHANH));
//		hashActionbar.put(i, listHinhanh);
//		i++;

		// Ds San pham
//		List<MenuTab> listDsSanpham = new ArrayList<MenuTab>();
//		listDsSanpham.add(new MenuTab(ActionEventConstant.GO_TO_PRODUCT_LIST, PriHashMap.PriForm.NVBH_DANHSACHSANPHAM));
//		hashActionbar.put(i, listDsSanpham);
//		i++;


//		// Cong no
		List<MenuTab> listCongno = new ArrayList<MenuTab>();
		listCongno.add(new MenuTab(-1, PriForm.NVBH_CONGNO));
		hashActionbar.put(i, listCongno);
		i++;

		// kho
//		List<MenuTab> listKho = new ArrayList<MenuTab>();
//		listKho.add(new MenuTab(ActionEventConstant.GO_TO_LOCK_DATE_VAL_VIEW, PriForm.NVBH_TRANG_THAI_KHO));
//		hashActionbar.put(i, listKho);
//		i++;

		// Theo doi khac phuc
		List<MenuTab> listTheodoikhacphuc = new ArrayList<MenuTab>();
		listTheodoikhacphuc.add(new MenuTab(-1, PriForm.NVBH_GIAOVANDE));
		listTheodoikhacphuc.add(new MenuTab(-1, PriForm.NVBH_CANTHUCHIEN));
		hashActionbar.put(i, listTheodoikhacphuc);
		i++;

		// bao cao dong
//		List<MenuTab> listDynamicReport = new ArrayList<MenuTab>();
//		listDynamicReport.add(new MenuTab(ActionEventConstant.GO_TO_KPI,PriForm.NVBH_DANHSACHKPI));
//		hashActionbar.put(i, listDynamicReport);
//		i++;
	}

	/**
	 * kiem tra duoi DB lan login truoc co dang ghe tham khach hang nao ko
	 *
	 * @author TamPQ
	 */
	private void checkVisitFromActionLog() {
		ActionEvent e = new ActionEvent();
		Vector<String> v = new Vector<String>();
		v.add(IntentConstants.INTENT_STAFF_ID);
		v.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId()));
		v.add(IntentConstants.INTENT_SHOP_ID);
		v.add(String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId()));

		e.viewData = v;
		e.action = ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG;
		e.sender = this;
		SaleController.getInstance().handleViewEvent(e);
	}

	@Override
	public void onClick(View v) {
		if (v == btnPass) {
			if (alertProductDetail != null) {
				alertProductDetail.dismiss();
			}
			if (listener != null) {
				listener.closePopup();
			}
		} else if (v == btnDetail) {
			if (alertProductDetail != null) {
				alertProductDetail.dismiss();
			}
			if (listener != null) {
				listener.gotoCustomerInfo();
//				gotoNoteListView();
			}
		} else {
			super.onClick(v);
		}
	}

	/**
	 * validate khi nhan menu chinh
	 *
	 * @author : BangHN since : 1.0
	 */
	private boolean isValidateMenu(int index) {
		boolean isOk = false;
		FragmentManager fm = getFragmentManager();
		BaseFragment fg;
		if (lstMenuItems.get(index).isSelected()) {
			switch (index) {
				case 0:// thong ke
					fg = (BaseFragment) fm
							.findFragmentByTag(GlobalUtil.getTag(GeneralStatisticsView.class));
					if (fg != null && fg.isVisible()) {
						isOk = true;
					}
					break;
				case 1:// ds khach hang
					fg = (BaseFragment) fm.findFragmentByTag(GlobalUtil.getTag(CustomerListView.class));
					if (fg != null && fg.isVisible()) {
						isOk = true;
					}
					break;
//				case 2:// ho tro thuong mai
//					fg = (BaseFragment) fm
//							.findFragmentByTag(GlobalUtil.getTag(PromotionProgramView.class));
//					if (fg != null && fg.isVisible()) {
//						isOk = true;
//					}
//					break;
				case 3:// ds hinh anh
					fg = (BaseFragment) fm.findFragmentByTag(GlobalUtil.getTag(ImageListView.class));
					if (fg != null && fg.isVisible()) {
						isOk = true;
					}
					break;
//				case 4:// ds san pham
//					fg = (BaseFragment) fm.findFragmentByTag(GlobalUtil.getTag(ProductListView.class));
//					if (fg != null && fg.isVisible()) {
//						isOk = true;
//					}
//					break;
				case 5:// ds don tong ngay
					fg = (BaseFragment) fm
							.findFragmentByTag(GlobalUtil.getTag(SaleStatisticsInDayPreSalesView.class));
					if (fg != null && fg.isVisible()) {
						isOk = true;
					}
					break;
//				case 8:// ds don tong ngay
//					fg = (BaseFragment) fm
//							.findFragmentByTag(GlobalUtil.getTag(CustomerListReportLostEquipmentView.class));
//					if (fg != null && fg.isVisible()) {
//						isOk = true;
//					}
//					break;
				default:
					break;
			}
		}

		return isOk;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (!isCheckConfirmOrderView(arg2)) {
			processClickOnMenu(arg2);
		}
	}

	// /**
	// * Kiem tra co phai dang o man hinh dat hang hay khong?
	// *
	// * @author: TruongHN
	// * @return: boolean
	// * @throws:
	// */
	// private boolean isCheckConfirmOrderView(int index) {
	// boolean isConfirm = false;
	// if (OrderView.TAG.equals(GlobalInfo.getInstance().getCurrentTag())) {
	// // kiem tra co thay doi du lieu ko
	// OrderView orderFragment = (OrderView)
	// getFragmentManager().findFragmentByTag(OrderView.TAG);
	// if (orderFragment != null) {
	// int changeData = orderFragment.checkChangeData();
	// if (!StringUtil.isNullOrEmpty(changeData)) {
	// // hien thi thong bao
	// GlobalUtil.showDialogConfirm(this, changeData,
	// StringUtil.getString(R.string.TEXT_AGREE),
	// CONFIRM_EXIT_ORDER_VIEW_OK, StringUtil.getString(R.string.TEXT_DENY),
	// CONFIRM_EXIT_ORDER_VIEW_CANCEL, index);
	// isConfirm = true;
	// }
	// }
	// }
	// return isConfirm;
	// }

	/**
	 * Kiem tra co phai dang o man hinh dat hang hay khong?
	 *
	 * @author: TruongHN
	 * @return: boolean
	 * @throws:
	 */
	private boolean isCheckConfirmOrderView(int index) {
		boolean isConfirm = false;
		if (GlobalUtil.getTag(OrderView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
			// kiem tra co thay doi du lieu ko
			OrderView orderFragment = (OrderView)
					findFragmentByTag(GlobalUtil.getTag(OrderView.class));
			if (orderFragment != null) {
				int changeData = orderFragment.checkChangeData();
				if (changeData != 0) {
					// hien thi thong bao
					if (changeData == 1) {
						SpannableObject span = new SpannableObject();
						span.addSpan(StringUtil
										.getString(R.string.TEXT_CONFIRM_EXIT_ORDER),
								ImageUtil.getColor(R.color.WHITE),
								Typeface.NORMAL);
						span.addSpan("\n");
						span.addSpan(
								StringUtil
										.getString(R.string.TEXT_CONFIRM_EXIT_ORDER_NOTIFY),
								ImageUtil.getColor(R.color.RED), Typeface.BOLD,
								20);
						GlobalUtil.showDialogConfirm(this, span.getSpan(),
								StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
								CONFIRM_EXIT_ORDER_VIEW_OK,
								StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
								CONFIRM_EXIT_ORDER_VIEW_CANCEL, index);
					} else if (changeData == 2) {
						GlobalUtil
								.showDialogConfirm(
										this,
										StringUtil
												.getString(R.string.TEXT_CONFIRM_EXIT_ORDER_VISITED),
										StringUtil
												.getString(R.string.TEXT_BUTTON_AGREE),
										CONFIRM_EXIT_ORDER_VIEW_VISIT_OK,
										StringUtil
												.getString(R.string.TEXT_BUTTON_CANCEL),
										CONFIRM_EXIT_ORDER_VIEW_CANCEL, index);
					}
					isConfirm = true;
				}
			}
		}
		return isConfirm;
	}

	/**
	 * Thuc thi su kien onClick tren menu
	 *
	 * @param index
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void processClickOnMenu(int index) {
		if (!isValidateMenu(index)) {
			// neu khong phai la module hien tai
			int menuIndex = 1;
			for (int i = 0; i < lstMenuItems.size(); i++) {
				if (i == index) {
					lstMenuItems.get(i).setSelected(true);
					menuIndex = lstMenuItems.get(i).menuIndex;
				} else
					lstMenuItems.get(i).setSelected(false);
			}
			lstMenuAdapter.notifyDataSetChanged();

			// TamPQ: CR0075: neu dang la kh ngoai tuyen thi tu dong update
			// action_log ghe tham khi thoat man hinh dat hang
			ActionLogDTO actionLog = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			if (actionLog != null && actionLog.isOr == 1) {
				requestUpdateActionLog("0", "0", null, this);
			}
			if (actionLog != null && actionLog.isVisited()) {
				endVisitCustomerBar();
			}
			showDetails(menuIndex);

		} else {
			// neu la module hien tai thi khong lam j ca
		}
	}

	@Override
	protected void getActionWhenClickMenu(int action) {
		// TODO Auto-generated method stub
		handleSwitchFragment(null, action, SaleController.getInstance());
	}

	/**
	 * Toi danh sach khach hang
	 *
	 * @author tampq
	 */
	private void gotoCustomerList() {
		handleSwitchFragment(null, ActionEventConstant.GET_CUSTOMER_LIST, SaleController.getInstance());
	}

	/**
	 * TamPQ
	 */
	public void endVisitCustomer() {
		// ket thuc ghe tham
		requestUpdateActionLog("0", null, null, this);
		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_INDEX, 1);
		receiveBroadcast(ActionEventConstant.NOTIFY_MENU, bundle);
//		groupdPosSelected = 2;
//		childPosSelected = 0;
//		initMenu();
		preGroupPosition = 2;
		setSelectMenu(MENU_INDEX_SELECTED);
		// huy bo toa do dang ghe tham khach hang, dinh vi ko can toi uu cho kh
		// nay
		GlobalInfo.getInstance().setPositionCustomerVisiting(null);
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getFragmentManager();
		if (fm.getBackStackEntryCount() <= 1) {
			// finish();
			GlobalUtil.showDialogConfirm(this,
					StringUtil.getString(R.string.TEXT_CONFIRM_EXIT),
					StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
					CONFIRM_EXIT_APP_OK,
					StringUtil.getString(R.string.TEXT_BUTTON_CANCEL),
					CONFIRM_EXIT_APP_CANCEL, null);
		} else if (fm.getBackStackEntryCount() > 1) {
			int handleBack = -1;
			if (GlobalUtil.getTag(OrderView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
				OrderView orderView = (OrderView) getFragmentManager()
						.findFragmentByTag(GlobalUtil.getTag(OrderView.class));
				if (orderView != null) {
					handleBack = orderView.onBackPressed();
					if (handleBack == OrderView.ACTION_CANCEL_BACK_DEFAULT) {
						// cancel back
					}
				}

			} else if (GlobalUtil.getTag(RemainProductView.class).equals(GlobalInfo.getInstance()
					.getCurrentTag())) {
				// cancel back
				handleBack = 0;
				// }else if
				// (CustomerInfoView.TAG.equals(GlobalInfo.getInstance().getCurrentTag())){
				// CustomerInfoView cusView =
				// (CustomerInfoView)getFragmentManager().findFragmentByTag(CustomerInfoView.TAG);
				// if (cusView != null){
				// handleBack = cusView.onBackPressed();
				// if (handleBack == CustomerInfoView.GO_TO_CUSTOMER_LIST){
				// // tro ve man hinh ds KH
				// gotoCustomerList();
				// }
				// }
			}
//			else if (GlobalUtil.getTag(GSNPPPostFeedbackView.class).equals(GlobalInfo.getInstance().getCurrentTag())) {
//				GSNPPPostFeedbackView view = (GSNPPPostFeedbackView) getFragmentManager()
//						.findFragmentByTag(GlobalUtil.getTag(GSNPPPostFeedbackView.class));
//				if (view != null) {
//					handleBack = view.onBackPressed();
//				}
//			}
			// else if
			// (NoteListView.TAG.equals(GlobalInfo.getInstance().getCurrentTag()))
			// {
			// NoteListView noteView = (NoteListView)
			// getFragmentManager().findFragmentByTag(NoteListView.TAG);
			// if (noteView != null) {
			// handleBack = noteView.onBackPressed();
			// }
			// }
			if (handleBack == -1) {
				GlobalInfo.getInstance().popCurrentTag();
				super.onBackPressed();

			}
		} else {
			GlobalInfo.getInstance().popCurrentTag();
			super.onBackPressed();
		}
	}

	/**
	 * kiem tra duoi DB lan login truoc KH co da dat don hang nao ko?
	 *
	 * @author TamPQ
	 */
	private void checkCreateOrderFromActionLog(String customerId) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());

		e.viewData = bundle;
		e.action = ActionEventConstant.CHECK_CREATE_ORDER_FROM_ACTION_LOG;
		e.sender = this;
		SaleController.getInstance().handleViewEvent(e);
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
			case ActionEventConstant.CHECK_VISIT_FROM_ACTION_LOG: {
				ActionLogDTO action = (ActionLogDTO) modelEvent.getModelData();
				if (action != null) {
					// GlobalInfo.getInstance().getProfile().setVisitingCustomer(true);
					GlobalInfo.getInstance().getProfile()
							.setActionLogVisitCustomer(action);
					if (action != null && action.objectType.equals("0")
							&& StringUtil.isNullOrEmpty(action.endTime)) {
						if (action.isOr == 0) {
							initMenuVisitCustomer(action.aCustomer.customerCode,
									action.aCustomer.customerName);
							checkCreateOrderFromActionLog(String
									.valueOf(action.aCustomer.customerId));
						} else {
							setStatusVisible(StringUtil.getString(R.string.TEXT_VISITTING_1) + " : "
									+ action.aCustomer.customerName + " - "
									+ action.aCustomer.customerCode + " ", View.VISIBLE);
						}
					}
					getOrderDraft(String.valueOf(action.aCustomer.customerId));
					// Lay thong tin KH
					getCustomerInfoVisit(String
							.valueOf(action.aCustomer.customerId));
				}
				break;
			}
		/*
		 * case ActionEventConstant.UPDATE_ACTION_LOG:{
		 * GlobalInfo.getInstance().getProfile()
		 * .setActionLogVisitCustomer((ActionLogDTO) e.viewData); break; }
		 */
			case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER: {
				if (this.isSaveActionLogAndCloseCustomerAfterTakePhoto) {
					this.isSaveActionLogAndCloseCustomerAfterTakePhoto = false;
					this.closeProgressDialog();
				} else {
					super.handleModelViewEvent(modelEvent);
				}
				break;
			}
			case ActionEventConstant.ACTION_LOAD_LIST_PPROBLEMS_FEEDBACK: {
				ProblemsFeedBackDTO dto = (ProblemsFeedBackDTO) modelEvent
						.getModelData();
				this.closeProgressDialog();

				if (dto.listProblems.size() <= 0 && listener != null) {
					listener.closePopup();
				} else {
					renderListProblemsFeedback(dto);
					if (!alertProductDetail.isShowing()) {
						alertProductDetail.show();
					}
				}
				break;
			}

			case ActionEventConstant.CHECK_CREATE_ORDER_FROM_ACTION_LOG: {
				ActionLogDTO action = (ActionLogDTO) modelEvent.getModelData();
				if (action != null) {
					removeMenuCloseCustomer();
				}
				break;
			}
			case ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT: {
				CustomerListItem item = (CustomerListItem) modelEvent
						.getModelData();
				if (item != null) {
					GlobalInfo.getInstance().getProfile()
							.setLastVisitCustomer(item);
				}

				break;
			}
			case ActionEventConstant.ACTION_GET_LOCK_DATE_STATE:
				LockDateDTO dto = (LockDateDTO) modelEvent.getModelData();
				GlobalInfo.getInstance().setLockDateValsale(dto);
				break;
			case ActionEventConstant.ACTION_READ_SHOP_LOCK:
				ShopLockDTO shopLock = (ShopLockDTO) modelEvent.getModelData();
				GlobalInfo.getInstance().setShopLockDto(shopLock);
				break;
			default:
				super.handleModelViewEvent(modelEvent);
				break;
		}
	}

	/**
	 * Lay thong tin KH dang ghe tham
	 *
	 * @author: TruongHN
	 * @return: void
	 * @throws:
	 */
	public void getCustomerInfoVisit(String customerId) {
		ActionEvent e = new ActionEvent();
		Bundle bundle = new Bundle();
		bundle.putString(
				IntentConstants.INTENT_STAFF_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		bundle.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo
				.getInstance().getProfile().getUserData().getInheritShopId());
		bundle.putString(IntentConstants.INTENT_CUSTOMER_ID, customerId);
		e.viewData = bundle;
		e.sender = this;
		e.action = ActionEventConstant.ACTION_CUSTOMER_INFO_VISIT;

		SaleController.getInstance().handleViewEvent(e);
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		// TODO Auto-generated method stub
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
			case ActionEventConstant.UPLOAD_PHOTO_TO_SERVER: {
				if (this.isSaveActionLogAndCloseCustomerAfterTakePhoto) {
					this.isSaveActionLogAndCloseCustomerAfterTakePhoto = false;
					this.closeProgressDialog();
				} else {
					super.handleErrorModelViewEvent(modelEvent);
				}
				break;
			}
			default:
				super.handleErrorModelViewEvent(modelEvent);
				break;
		}
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
			case MENU_FINISH_VISIT:
				ActionLogDTO action = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				CustomerListItem item = GlobalInfo.getInstance().getProfile()
						.getLastVisitCustomer();
				if (item != null && item.isHaveDisplayProgramNotYetVote) {
					this.showDialog(StringUtil
							.getString(R.string.TEXT_REQUEST_TAKE_VOTE_DISPLAY_BEFORE_EXIT_CUSTOMER));
				} else if (action != null
						&& StringUtil.isNullOrEmpty(action.endTime)) {
					new CaculateTimeVisit().execute(action.aCustomer);
				}
				break;
			case MENU_FINISH_VISIT_CLOSE:
				this.showPopupConfirmCustomerClose();
				break;
			case CONFIRM_EXIT_APP_OK:
				// ket thuc ghe tham
				// requestUpdateActionLog("0", null, null, this);
//			sendBroadcast(ActionEventConstant.ACTION_FINISH_APP, new Bundle());
				TransactionProcessManager.getInstance().cancelTimer();
				//xu ly thoat ung dung
				processExitApp();
				//finish();
				break;
			case CONFIRM_EXIT_APP_CANCEL:
				break;
			case CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR:
				// show camera
				ActionLogDTO al = GlobalInfo.getInstance().getProfile()
						.getActionLogVisitCustomer();
				LatLng cusLatLng = new LatLng(al.aCustomer.lat, al.aCustomer.lng);
				LatLng myLatLng = new LatLng(GlobalInfo.getInstance().getProfile()
						.getMyGPSInfo().getLatitude(), GlobalInfo.getInstance()
						.getProfile().getMyGPSInfo().getLongtitude());
				double distance = GlobalUtil
						.getDistanceBetween(cusLatLng, myLatLng);
				double distanceAllow = al.aCustomer.shopDTO.distanceOrder;
				if (myLatLng.lat > 0 && myLatLng.lng > 0 && cusLatLng.lat > 0
						&& cusLatLng.lng > 0 && distance <= distanceAllow) {
					this.isTakePhotoFromMenuCloseCustomer = true;
					this.takenPhoto = GlobalUtil.takePhoto(this,
							RQ_TAKE_PHOTO);
				} else {
					String mess = StringUtil
							.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_1)
							+ " "
							+ al.aCustomer.customerCode
							+ " - "
							+ al.aCustomer.customerName
							+ " "
							+ StringUtil
							.getString(R.string.TEXT_TAKE_PHOTO_CLOSE_DOOR_TOO_FAR_FROM_SHOP_2);

					GlobalUtil.showDialogConfirm(this, mess,
							StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), null,
							false);
				}
				break;
			case CONFIRM_EXIT_ORDER_VIEW_VISIT_OK: {
				int index = Integer.valueOf(data.toString());
				processClickOnMenu(index);
				break;
			}
			case CONFIRM_EXIT_ORDER_VIEW_OK:
				// int index = Integer.valueOf(data.toString());
				// processClickOnMenu(index);
				// break;
				int index = Integer.valueOf(data.toString());
				OrderView orderFragment = (OrderView)
						findFragmentByTag(GlobalUtil.getTag(OrderView.class));
				if (orderFragment != null) {
					orderFragment.saveOrderDraft(index);
				}
				// int index = Integer.valueOf(data.toString());
				// processClickOnMenu(index);
				break;
			case CONFIRM_EXIT_ORDER_VIEW_CANCEL:
				break;
			case ACTION_FINISH_VISIT_CUS_OK: {
				// endVisitCustomer();
				showPopupProblemsFeedBack(new PopupProblemsListener() {
					@Override
					public void closePopup() {
						endVisitCustomer();
					}

					@Override
					public void gotoCustomerInfo() {
						ActionLogDTO al = GlobalInfo.getInstance().getProfile()
								.getActionLogVisitCustomer();
						endVisitCustomer();
						gotoNoteListView();
//					gotoFeedBackListOfCusOrder(al.aCustomer);
					}
				});
				break;
			}
			case ACTION_FINISH_VISIT_CUS_CANCEL: {
				break;
			}
			default:
				super.onEvent(eventType, control, data);
				break;
		}
	}

	/**
	 * Thuc hien ket thuc ghe tham khach hang
	 *
	 * @author: banghn
	 */
	private void finishVisitCustomer(String endTime) {
		ActionLogDTO action = GlobalInfo.getInstance().getProfile()
				.getActionLogVisitCustomer();
		SpannableObject textConfirmed = new SpannableObject();
		textConfirmed.addSpan(
				StringUtil.getString(R.string.TEXT_ALREADY_VISIT_CUSTOMER),
				ImageUtil.getColor(R.color.WHITE),
				Typeface.NORMAL);
		if (!StringUtil.isNullOrEmpty(action.aCustomer.customerCode)) {
			textConfirmed.addSpan(
					" " + action.aCustomer.customerCode.substring(0, 3),
					ImageUtil.getColor(R.color.WHITE),
					Typeface.BOLD);
		}
		textConfirmed.addSpan(" - ", ImageUtil.getColor(R.color.WHITE),
				Typeface.BOLD);
		if (!StringUtil.isNullOrEmpty(action.aCustomer.customerName)) {
			textConfirmed.addSpan(action.aCustomer.customerName,
					ImageUtil.getColor(R.color.WHITE),
					Typeface.BOLD);
		}
		textConfirmed.addSpan(" " + StringUtil.getString(R.string.TEXT_IN) + " ", ImageUtil.getColor(R.color.WHITE),
				Typeface.NORMAL);
		textConfirmed.addSpan(
				DateUtils.getVisitTime(action.startTime, endTime),
				ImageUtil.getColor(R.color.WHITE),
				Typeface.BOLD);
		textConfirmed.addSpan(
				StringUtil.getString(R.string.TEXT_ASK_END_VISIT_CUSTOMER),
				ImageUtil.getColor(R.color.WHITE),
				Typeface.NORMAL);

		GlobalUtil.showDialogConfirmCanBackAndTouchOutSide(this,
				textConfirmed.getSpan(),
				StringUtil.getString(R.string.TEXT_BUTTON_AGREE),
				ACTION_FINISH_VISIT_CUS_OK,
				StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
				ACTION_FINISH_VISIT_CUS_CANCEL, null, true, true);
	}

	@Override
	public void receiveBroadcast(int action, Bundle bundle) {
		switch (action) {
			case ActionEventConstant.NOTIFY_MENU:
				int index = bundle.getInt(IntentConstants.INTENT_INDEX);
				if (index >= 0) {
					for (MenuItemDTO dto : lstMenuItems) {
						dto.setSelected(false);
					}
					lstMenuItems.get(index).setSelected(true);
					lvMenu.setSelection(index);
					lstMenuAdapter.notifyDataSetChanged();
				}
				break;
			case ActionEventConstant.NOTIFY_REFRESH_VIEW:
				readLockDateState();
				readShopLock();
				break;
			default:
				super.receiveBroadcast(action, bundle);
				break;
		}

	}

	/**
	 * thuc hien request luu action log - hien thi man hinh danh sach khach hang
	 * va thiet lap ket thuc ghe tham khach hang hien tai
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void saveCustomerCloseDoorAndGotoCustomerList() {
		requestUpdateActionLog("1", null, null, this);
		// ket thuc ghe tham
		// GlobalInfo.getInstance().getProfile().setVisitingCustomer(false);
		gotoCustomerList();
		// endVisitCustomerBar();
	}

	/**
	 * show popup before show camera when click button "dong cua"
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	public void showPopupConfirmCustomerClose() {
		GlobalUtil
				.showDialogConfirm(
						this,
						getString(R.string.TEXT_NOTIFY_DISPLAY_CAMERA_CLICK_BUTTON_CUSTOMER_CLOSE_THE_DOOR),
						"OK", CONFIRM_SHOW_CAMERA_WHEN_CLICK_CLOSE_DOOR, "", 0,
						null);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			String filePath = "";
			switch (requestCode) {
				case RQ_TAKE_PHOTO: {
					if (this.isTakePhotoFromMenuCloseCustomer) {
						this.isTakePhotoFromMenuCloseCustomer = false;
						this.isSaveActionLogAndCloseCustomerAfterTakePhoto = true;
						if (resultCode == RESULT_OK && takenPhoto != null) {
							filePath = takenPhoto.getAbsolutePath();
							ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
									this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
							validator.setDataIntent(data);
							if (validator.execute()) {
								// imageFile = takingFile;
								// GlobalInfo.getInstance().setBitmapData(validator.getBitmap());
								this.saveCustomerCloseDoorAndGotoCustomerList();
								updateTakenPhoto();
							}
						}
					} else {
						super.onActivityResult(requestCode, resultCode, data);
					}
					break;
				}
				case RQ_TAKE_PHOTO_VOTE_DP: {
					VoteDisplayPresentProductView statisticsProduct = (VoteDisplayPresentProductView)
							findFragmentByTag(GlobalUtil.getTag(VoteDisplayPresentProductView.class));

					if (resultCode == RESULT_OK && takenPhoto != null) {
						filePath = takenPhoto.getAbsolutePath();
						ImageValidatorTakingPhoto validator = new ImageValidatorTakingPhoto(
								this, filePath, Constants.MAX_FULL_IMAGE_HEIGHT);
						validator.setDataIntent(data);
						if (validator.execute()) {
							if (statisticsProduct != null) {
								statisticsProduct.updateTakenPhoto();
							}
						}
					} else {
						// request code cancel
						this.closeProgressDialog();
						// bat buoc phai chup hinh trung bay, ko cho huy
						// if(statisticsProduct != null) {
						// statisticsProduct.takeNextImage();
						// }
					}
					break;
				}

				default:
					super.onActivityResult(requestCode, resultCode, data);
					break;

			}
		} catch (Exception ex) {
			ServerLogger.sendLog("ActivityState", "GlobalBaseActivity : "
							+ VNMTraceUnexceptionLog.getReportFromThrowable(ex),
					TabletActionLogDTO.LOG_EXCEPTION);
		}
	}

	/**
	 * update image to db and to server
	 *
	 * @author: HaiTC3
	 * @return: void
	 * @throws:
	 */
	private void updateTakenPhoto() {
		this.showProgressDialog(StringUtil.getString(R.string.loading));
		Vector<String> viewData = new Vector<String>();

		viewData.add(IntentConstants.INTENT_STAFF_ID);
		viewData.add(Integer.toString(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId()));

		viewData.add(IntentConstants.INTENT_SHOP_ID);
		if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritShopId())) {
			viewData.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		} else {
			viewData.add("1");
		}

		if (this.takenPhoto != null) {
			viewData.add(IntentConstants.INTENT_FILE_NAME);
			viewData.add(this.takenPhoto.getName());

			viewData.add(IntentConstants.INTENT_TAKEN_PHOTO);
			viewData.add(this.takenPhoto.getAbsolutePath());
		}

		MediaItemDTO dto = new MediaItemDTO();

		try {
			dto.objectId = Long.parseLong(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerId());
			dto.objectType = 0;
			dto.mediaType = 0;// loai hinh anh , 1 loai video
			dto.url = this.takenPhoto.getAbsolutePath();
			dto.thumbUrl = this.takenPhoto.getAbsolutePath();
			dto.createDate = DateUtils.now();
			dto.createUser = GlobalInfo.getInstance().getProfile()
					.getUserData().getUserCode();
			dto.lat = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLatitude();
			dto.lng = GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.getLongtitude();
//			dto.type = 1;
			dto.status = 1;
			dto.fileSize = this.takenPhoto.length();
			dto.staffId = GlobalInfo.getInstance().getProfile().getUserData().getInheritId();
			if (!StringUtil.isNullOrEmpty(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId())) {
				dto.shopId = Integer.valueOf(GlobalInfo.getInstance()
						.getProfile().getUserData().getInheritShopId());
			} else {
				dto.shopId = 1;
			}

			viewData.add(IntentConstants.INTENT_OBJECT_ID);
			viewData.add(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerId());
			viewData.add(IntentConstants.INTENT_OBJECT_TYPE_IMAGE);
			viewData.add(String.valueOf(dto.objectType));
			viewData.add(IntentConstants.INTENT_MEDIA_TYPE);
			viewData.add(String.valueOf(dto.mediaType));
			viewData.add(IntentConstants.INTENT_URL);
			viewData.add(String.valueOf(dto.url));
			viewData.add(IntentConstants.INTENT_THUMB_URL);
			viewData.add(String.valueOf(dto.thumbUrl));
			viewData.add(IntentConstants.INTENT_CREATE_DATE);
			viewData.add(String.valueOf(dto.createDate));
			viewData.add(IntentConstants.INTENT_CREATE_USER);
			viewData.add(String.valueOf(dto.createUser));
			viewData.add(IntentConstants.INTENT_LAT);
			viewData.add(String.valueOf(dto.lat));
			viewData.add(IntentConstants.INTENT_LNG);
			viewData.add(String.valueOf(dto.lng));
			viewData.add(IntentConstants.INTENT_FILE_SIZE);
			viewData.add(String.valueOf(dto.fileSize));
			viewData.add(IntentConstants.INTENT_CUSTOMER_CODE);
			viewData.add(GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer().aCustomer.getCustomerCode());
			viewData.add(IntentConstants.INTENT_STATUS);
			viewData.add("1");
//			viewData.add(IntentConstants.INTENT_TYPE);
//			viewData.add("1");
		} catch (NumberFormatException e1) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e1));
		}

		CustomerListItem lastVisitCustomer = GlobalInfo.getInstance()
				.getProfile().getLastVisitCustomer();
		if (lastVisitCustomer != null) {
			lastVisitCustomer.visitStatus = VISIT_STATUS.VISITED_CLOSED;
		}
		ActionEvent e = new ActionEvent();
		e.action = ActionEventConstant.UPLOAD_PHOTO_TO_SERVER;
		e.viewData = viewData;
		e.userData = dto;
		e.sender = this;
		UserController.getInstance().handleViewEvent(e);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return super.onKeyDown(keyCode, event);
	}

	private void renderListProblemsFeedback(ProblemsFeedBackDTO dto) {
		if (currPageFeedBack != -1) {
			tbListProblems.setTotalSize(dto.totalRows, currPageFeedBack);
			tbListProblems.getPagingControl().setCurrentPage(currPageFeedBack);
		} else {
			tbListProblems.setTotalSize(dto.totalRows, 1);
			currPageFeedBack = tbListProblems.getPagingControl().getCurrentPage();
		}

		tbListProblems.clearAllData();
		if (dto != null) {
			int pos = 1 + Constants.NUM_ITEM_PER_PAGE
					* (tbListProblems.getPagingControl().getCurrentPage() - 1);
			if (dto.listProblems.size() > 0) {
				for (int i = 0, s = dto.listProblems.size(); i < s; i++) {
					ListProblemsFeedbackRow row = new ListProblemsFeedbackRow(
							this);
					// row.setListener(this);
					ProblemsFeedBackDTO.ProblemsFeedBackItem item = dto.listProblems.get(i);
					row.renderLayout(pos, item);

					pos++;
					tbListProblems.addRow(row);
				}
			} else {
				tbListProblems.showNoContentRow();
			}
//			tbListProblems.addContent(listRows);
//			tbListProblems.setTotalSize(dto.totalRows);
//			tbListProblems.getPagingControl().setCurrentPage(currPageFeedBack);
		}
	}

	public void gotoFeedBackListOfCusOrder(CustomerDTO dto) {
		Bundle bundle = new Bundle();
		bundle.putSerializable(IntentConstants.INTENT_CUSTOMER, dto);
		handleSwitchFragment(bundle, ActionEventConstant.GET_LIST_CUS_FEED_BACK, SaleController.getInstance());
	}

	/**
	 * Lang nghe su kien table tren popup problems feedback
	 *
	 * @author quangvt
	 */
	class ListenerTableProblemsFeedback implements VinamilkTableListener {

		@Override
		public void handleVinamilkTableloadMore(View control, Object data) {
			ActionLogDTO action = GlobalInfo.getInstance().getProfile()
					.getActionLogVisitCustomer();
			int staffId = action.staffId;
			long cusId = action.aCustomer.customerId;
			currPageFeedBack = tbListProblems.getPagingControl()
					.getCurrentPage();

			getListProblemsFeedBack(staffId, cusId, currPageFeedBack);
		}

		@Override
		public void handleVinamilkTableRowEvent(int action, View control,
												Object data) {

		}
	}

	/**
	 * Lay danh sach Problems FeedBack
	 *
	 * @param staffId
	 * @param cusId
	 * @author quangvt
	 */
	private void getListProblemsFeedBack(int staffId, long cusId, int page) {
		showLoadingDialog();

		Bundle bundle = new Bundle();
		bundle.putInt(IntentConstants.INTENT_STAFF_ID, staffId);
		bundle.putLong(IntentConstants.INTENT_CUSTOMER_ID, cusId);
		bundle.putInt(IntentConstants.INTENT_PAGE, page);

		handleViewEvent(bundle, ActionEventConstant.ACTION_LOAD_LIST_PPROBLEMS_FEEDBACK, SaleController.getInstance());
	}

	/**
	 * Dang tinh toan thoi gian ket thuc
	 *
	 * @author banghn
	 * @version 1.0
	 */
	private class CaculateTimeVisit extends
			AsyncTask<CustomerDTO, String, String> {
		@Override
		protected void onPreExecute() {
			showProgressDialog(StringUtil.getString(R.string.TEXT_ENDING_TIME));
		}

		@Override
		protected String doInBackground(CustomerDTO... params) {
			CustomerDTO cus = params[0];
			return DateUtils.getVisitEndTime(cus);
		}

		@Override
		protected void onPostExecute(String result) {
			closeProgressDialog();
			finishVisitCustomer(result);
			super.onPostExecute(result);
		}
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	private void readLockDateState() {
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		handleViewEvent(data, ActionEventConstant.ACTION_GET_LOCK_DATE_STATE, SaleController.getInstance());
	}

	/**
	 * Mo ta muc dich cua ham
	 *
	 * @author: DungNX
	 * @return: void
	 * @throws:
	 */
	private void readShopLock() {
		Bundle data = new Bundle();
		data.putString(IntentConstants.INTENT_SHOP_ID, GlobalInfo.getInstance()
				.getProfile().getUserData().getInheritShopId());
		data.putString(IntentConstants.INTENT_STAFF_ID, ""
				+ GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		handleViewEventWithOutAsyntask(data, ActionEventConstant.ACTION_READ_SHOP_LOCK, SaleController.getInstance());
	}
}
