/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.CustomerStockHistoryDTO;
import com.ths.dmscore.lib.json.me.JSONArray;

/**
 * list customer display programe score
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class ListCustomerDisplayProgrameScoreDTO implements Serializable {
	private static final long serialVersionUID = 1480141343561183336L;
	private ArrayList<CustomerStockHistoryDTO> listCusDTO = new ArrayList<CustomerStockHistoryDTO>();

	public ListCustomerDisplayProgrameScoreDTO() {
		setListCusDTO(new ArrayList<CustomerStockHistoryDTO>());
	}

	/**
	 * @return the listCusDTO
	 */
	public ArrayList<CustomerStockHistoryDTO> getListCusDTO() {
		return listCusDTO;
	}

	/**
	 * @param listCusDTO
	 *            the listCusDTO to set
	 */
	public void setListCusDTO(ArrayList<CustomerStockHistoryDTO> listCusDTO) {
		this.listCusDTO = listCusDTO;
	}

	/**
	 * 
	 * general SQL vote display product
	 * 
	 * @author: HaiTC3
	 * @return
	 * @return: JSONArray
	 * @throws:
	 */
	public JSONArray generateNewVoteDisplayList() {
		JSONArray result = new JSONArray();
		for (int i = 0, size = listCusDTO.size(); i < size; i++) {
			CustomerStockHistoryDTO item = listCusDTO.get(i);
			result.put(item.generateCreateSqlInsertVoteDisplayProgram());
		}
		return result;
	}
	
}
