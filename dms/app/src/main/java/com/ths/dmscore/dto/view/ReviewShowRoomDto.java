package com.ths.dmscore.dto.view;

public class ReviewShowRoomDto {
	public String programCode;
	public String programName;
	public String cusCode;
	public String cusName;
	public String fromDate;
	public String toDate;
	public int level;
	public ShowRoomToolDto toolDto;
	public ShowRoomGoodsDto goodsDto;
	
	public ReviewShowRoomDto() {
	}
}
