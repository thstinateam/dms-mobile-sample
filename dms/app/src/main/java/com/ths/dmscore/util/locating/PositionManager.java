/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util.locating;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.LocatingListener;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Xu ly luong dinh vi
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class PositionManager extends Activity implements LocatingListener
		{
	//5 phut
	public final static long TIME_SEND_LOG_LOCATING = 300000;
	// 1 phut
	public final static int TIME_OUT_SHOW_LOCATING = 60000;
	final String LBS = "NETWORK";
	// thoi gian time out lay gps
	final int GPS_TIME_OUT = 90000;
	// 10s thoi gian time out lay lbs
	final int LBS_TIME_OUT = 10000;
	//thoi gian dinh vi luc cham cong
	//public static final long TIME_LOC_PERIOD_ATTENDACE = 120000;
	//thoi gian time period dang dung
	private static long currentTimePeriod = 0;
	public static final int ACCURACY = 50;

	final String LOG_TAG = "PositionManager";

	public static final int LOCATING_NONE = 0;// trang thai none
	static final int LOCATING_GPS_START = 1;// trang thai start gps
	static final int LOCATING_GPS_ON_PROGRESS = 2;// trang thai dang lay gps
	static final int LOCATING_GPS_FAILED = 3;// trang thai lay gps that bai
	static final int LOCATING_LBS_START = 4;// trang thai start lbs
	static final int LOCATING_LBS_ON_PROGRESS = 5;// trang thai dang lay lbs
	static final int LOCATING_LBS_FAILED = 6;// trang thai lay lbs that bai
	static final int LOCATING_VIETTEL = 7;// trang thai dinh vi viettel
	public int actionSuccess;// id action dinh vi 1 lan thanh cong
	public int actionFail;// id action dinh vi 1 lan that bai
	private static volatile PositionManager instance;
	private boolean isStart = false;
	public boolean isUsingGoogleLBS = true;// co su dung lbs cua Google khong
	Locating gpsLocating;// dinh vi gps
	Locating lbsLocating;// dinh vi lbs
	int locatingState = LOCATING_NONE;// trang thai dinh vi
	private Timer locTimer = new Timer();// timer dinh thoi lay toa do
	private PositionTimerTask locTask;// task lay toa do dung kem voi timer
	private boolean isFirstLBS;// bat luong LBS truoc
	private BaseFragment listener;


	private static final Object lockObject = new Object();
	public static PositionManager getInstance() {
		if (instance == null) {
			synchronized (lockObject) {
				if (instance == null) {
					instance = new PositionManager();
				}
			}
		}
		return instance;
	}

	class PositionTimerTask extends TimerTask {
		public boolean isCancled = false;

		@Override
		public void run() {
			// TODO Auto-generated method stub
		}
	}

	private PositionManager() {
		instance = this;
	}

	/**
	 * Thuc hien restart lai luong dinh vi
	 * stop() -> start()
	 * @author: BANGHN
	 */
	public void reStart(){
		stop();
		start();
	}


	/**
	 * khoi dong lay toa do
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	public void start() {
		if(DateUtils.isInAttendaceTime()){
			start(GlobalInfo.getInstance().getTimeTrigPositionAttendance());
		}else{
			start(GlobalInfo.getInstance().getTimeTrigPosition());
		}
	}

	public void start(long timePeriod) {
		// TODO Auto-generated method stub
		setCurrentTimePeriod(timePeriod);
		MyLog.i("PositionManager", "Start Load Location");
		GlobalInfo.getInstance().getAppContext()
				.getSystemService(Context.LOCATION_SERVICE);
		isUsingGoogleLBS = isEnableGoogleLBS();
		isStart = true;
		isFirstLBS = true;
		// run 1 luong lbs song song voi luong dinh vi
		if (isUsingGoogleLBS) {
			// lay lbs google
			getLBSGoogle();
		}else {
			// lay luong lbs truoc, viettel truoc,// google sau
			onLocationChanged(null);

		}
		locTimer = new Timer();
		locTask = new PositionTimerTask() {
			@Override
			public void run() {
				if (!locTask.isCancled) {
					(GlobalInfo.getInstance().getAppHandler())
							.post(new Runnable() {
								@Override
								public void run() {
									//MyLog.logToFile("Position", DateUtils.now() + " - new locating request");
									if (GlobalInfo.getInstance().isAppActive()) {
										locatingState = LOCATING_NONE;
										requestLocationUpdates();
									} else {
										stop();
									}
								}
							});
				}
			}
		};
		locTimer.schedule(locTask, 0, timePeriod);
	}

	/**
	 * Dung lay toa do
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	public void stop() {
		isStart = false;
		if (GlobalInfo.getInstance().getLoc() != null
				&& GlobalInfo.getInstance().getLoc().getTime() < System
						.currentTimeMillis() - Locating.MAX_TIME_RESET) {
			GlobalInfo.getInstance().setLoc(null);
			GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.setLongtitude(-1);
			GlobalInfo.getInstance().getProfile().getMyGPSInfo()
					.setLattitude(-1);
			GlobalInfo.getInstance().getProfile().save();
		}
		if (locTask != null) {
			locTask.cancel();
			locTask.isCancled = true;
		}
		if (locTimer != null) {
			locTimer.cancel();
		}
		if (gpsLocating != null) {
			gpsLocating.resetTimer();
		}
		if (lbsLocating != null) {
			lbsLocating.resetTimer();
		}
	}

	/**
	 *
	 * quan ly trang thai va goi lay toa do
	 *
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	private void requestLocationUpdates() {
		synchronized (this) {
			switch (locatingState) {
			case LOCATING_NONE:
				MyLog.i(LOG_TAG, "requestLocationUpdates() - LOCATING_NONE");
				locatingState = LOCATING_GPS_START;
				requestLocationUpdates();
				break;

			case LOCATING_GPS_START:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_GPS_START");
				if (gpsLocating == null) {
					gpsLocating = new Locating(LocationManager.GPS_PROVIDER,
							this);
				}
				if (!gpsLocating.requestLocating(GPS_TIME_OUT)) {
					locatingState = LOCATING_GPS_FAILED;
					requestLocationUpdates();
				} else {
					locatingState = LOCATING_GPS_ON_PROGRESS;
				}
				break;

			case LOCATING_GPS_ON_PROGRESS:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_GPS_ON_PROGRESS");
				break;

			case LOCATING_GPS_FAILED:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_GPS_FAILED");
				if (isUsingGoogleLBS) {
					locatingState = LOCATING_LBS_START;
				} else {
					locatingState = LOCATING_VIETTEL;
				}
				if (GlobalInfo.getInstance().isAppActive()
						&& (System.currentTimeMillis() - GlobalInfo
								.getInstance().getTimeSendLogLocating()) >= TIME_SEND_LOG_LOCATING
						&& GlobalUtil.checkNetworkAccess()) {
					GlobalInfo.getInstance().setTimeSendLogLocating(System
							.currentTimeMillis());
					ServerLogger.sendLog("Locating",
							"Time : " + DateUtils.now()
									+ " .... LOCATING_GPS_FAILED", false,
							TabletActionLogDTO.LOG_CLIENT);
				}
				requestLocationUpdates();
				break;

			case LOCATING_LBS_START:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_LBS_START");
				if (lbsLocating == null) {
					lbsLocating = new Locating(
							LocationManager.NETWORK_PROVIDER, this);
				}
				if (!lbsLocating.requestLocating(LBS_TIME_OUT)) {// dinh vi lbs
					locatingState = LOCATING_LBS_FAILED;
					requestLocationUpdates();
				} else {
					locatingState = LOCATING_LBS_ON_PROGRESS;
				}
				break;

			case LOCATING_LBS_ON_PROGRESS:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_LBS_ON_PROGRESS");
				break;

			case LOCATING_LBS_FAILED:
				MyLog.i(LOG_TAG,
						"requestLocationUpdates() - LOCATING_LBS_FAILED");
				// MyLog.logToFile(LOG_TAG,
				// "requestLocationUpdates() - LOCATING_LBS_FAILED");
				if (GlobalInfo.getInstance().isAppActive()
						&& (System.currentTimeMillis() - GlobalInfo
								.getInstance().getTimeSendLogLocating()) >= TIME_SEND_LOG_LOCATING
						&& GlobalUtil.checkNetworkAccess()) {
					GlobalInfo.getInstance().setTimeSendLogLocating(System
							.currentTimeMillis());

					ServerLogger.sendLog("Locating",
							"Time : " + DateUtils.now()
									+ " .... LOCATING_LBS_FAILED", false,
							TabletActionLogDTO.LOG_CLIENT);
				}
				locatingState = LOCATING_NONE;
				break;

			case LOCATING_VIETTEL:
				MyLog.i(LOG_TAG, "requestLocationUpdates() - LOCATING_VIETTEL");
				onLocationChanged(null);
				break;
			}
		}
	}

	private void getLBSGoogle() {
		// isFirstLBS = false;
		(GlobalInfo.getInstance().getAppHandler()).post(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Locating lbs = new Locating(LocationManager.NETWORK_PROVIDER,
						PositionManager.this);
				lbs.requestLocating(LBS_TIME_OUT);
			}
		});
	}

	/**
	 *
	 * chuyen trang thai khi dinh vi that bai
	 *
	 * @author: AnhND
	 * @return: void
	 * @throws:
	 */
	private void handleLocatingFailed() {
		synchronized (this) {
			switch (locatingState) {
			case LOCATING_GPS_ON_PROGRESS:
				MyLog.i(LOG_TAG, "handleLocatingFailed() - GPS TIME OUT");
				locatingState = LOCATING_GPS_FAILED;
				requestLocationUpdates();
				break;
			case LOCATING_LBS_ON_PROGRESS:
				MyLog.i(LOG_TAG, "handleLocatingFailed() - LBS TIME OUT");
				locatingState = LOCATING_LBS_FAILED;
				requestLocationUpdates();
				break;
			case LOCATING_VIETTEL:// lay lbs Viettel error thi lay lbs Google
				MyLog.i(LOG_TAG, "handleLocatingFailed() - LBS TIME OUT");
				locatingState = LOCATING_LBS_START;
				(GlobalInfo.getInstance().getAppHandler()).post(new Runnable() {
					@Override
					public void run() {
						requestLocationUpdates();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location != null) {
			if (isFirstLBS && LBS.equals(location.getProvider().toString())) {
				isFirstLBS = false;
			}
			MyLog.d("PositionManager",
					location.getProvider().toString() + " (X,Y)= ("
							+ location.getLatitude() + " , "
							+ location.getLongitude() + " )");

			Bundle bd = new Bundle();
			bd.putParcelable(IntentConstants.INTENT_DATA, location);
			if (GlobalInfo.getInstance().getActivityContext() instanceof GlobalBaseActivity) {
				((GlobalBaseActivity) GlobalInfo.getInstance().getActivityContext())
						.sendBroadcast(ActionEventConstant.ACTION_UPDATE_POSITION_SERVICE,
								bd);
			}
			// broadcast thay doi vi tri
			//updatePosition(location.getLongitude(), location.getLatitude(),
			//		location);
			// neu co 1 truong hop gps success thi chay xong gps se dung tien
			// trinh kg lam gi
			if (locatingState == LOCATING_GPS_ON_PROGRESS) {
				locatingState = LOCATING_NONE;
			}
		} else {
			// requestActionUpdatePosition();
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		handleLocatingFailed();
		if (provider.toUpperCase().contains("GPS")) {
			try {
				((GlobalBaseActivity) GlobalInfo.getInstance()
						.getActivityContext())
						.showToastMessage(StringUtil.getString(R.string.TEXT_GPS_NEED_START_GPS));
			} catch (Exception e) {
				MyLog.e("PositionManager", e.getMessage());
			}
		} else if (provider.toUpperCase().contains("NETWORK")) {
			try {
				((GlobalBaseActivity) GlobalInfo.getInstance()
						.getActivityContext())
						.showToastMessage(StringUtil.getString(R.string.TEXT_GPS_NEED_START_GPS_NETWORK));
			} catch (Exception e) {
				MyLog.e("PositionManager", e.getMessage());
			}
		}
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onTimeout(Locating locating) {
		// TODO Auto-generated method stub
		if (!isFirstLBS) {
			handleLocatingFailed();
		}
		isFirstLBS = false;//LBS google luon timeout truoc GPS

		if (GlobalInfo.getInstance().isAppActive()
				&& GlobalUtil.checkNetworkAccess()
				&& System.currentTimeMillis()
						- GlobalInfo.getInstance().getTimeSendLogLocating() >= TIME_SEND_LOG_LOCATING) {
			GlobalInfo.getInstance().setTimeSendLogLocating(System
					.currentTimeMillis());
			ServerLogger.sendLog("Locating", "Time : " + DateUtils.now()
					+ "  -  Provider time-out: " + locating.getProviderName(),
					false, TabletActionLogDTO.LOG_CLIENT);
		}
	}


	public boolean getIsStart() {
		return isStart;
	}

	public int getLocatingState() {
		return locatingState;
	}

	public boolean getIsFirstLBS() {
		return isFirstLBS;
	}

	/**
	 * @return the enableGPS
	 */
	public boolean isEnableGPS() {
		return new Locating(LocationManager.GPS_PROVIDER, this).isEnableGPS();
	}

	/**
	 * @return the enableGoogleLBS
	 */
	public boolean isEnableGoogleLBS() {
		return new Locating(LocationManager.NETWORK_PROVIDER, this).isEnableGPS();
	}

	/**
	 * Kiem tra enable GPS va show setting thiet lap GPS
	 * @author banghn
	 */
	public void checkAndShowSettingGPS() {
		if (!isEnableGPS()) {
			showDialogSettingGPS();
		}
	}

	/**
	 * show dialog confirm setting gps
	 *
	 * @author banghn
	 */
	private void showDialogSettingGPS() {
		AlertDialog alertDialog = null;
		try {
			alertDialog = new AlertDialog.Builder(GlobalInfo.getInstance()
					.getActivityContext()).create();
			alertDialog.setMessage(StringUtil
					.getString(R.string.NOTIFY_SETTING_GPS));
			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							// hien thi man hinh setting
							GlobalUtil.startActivityOtherApp(GlobalInfo.getInstance().getActivityContext(),
									new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
							return;

						}
					});
			alertDialog.show();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
	}

	public static long getCurrentTimePeriod() {
		return currentTimePeriod;
	}

	public static void setCurrentTimePeriod(long currentTimePeriod) {
		PositionManager.currentTimePeriod = currentTimePeriod;
	}
}
