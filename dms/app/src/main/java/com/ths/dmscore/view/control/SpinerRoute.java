/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * thong tin chung
 * @author : BangHN
 * since   : 1.0
 * version : 1.0
 * com.viettel.vinamilk.view.control.SpinerRoute
 */
public class SpinerRoute extends LinearLayout {
	MySpinner spinerRoute;
	// xac dinh spinner nay dung cho man hinh nao
	private int useFor;
	private int customerRouteView = 0;
	private int staffPositionView = 1;
	private int tbhvGSnppNVbhPositionView = 2;
	private static final int StaffPositionRoutedView = 4;
	
	public SpinerRoute(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context);
	}
	
	public SpinerRoute(Context context, int useFor) {
		super(context);
		this.useFor = useFor;
		initView(context);
	}
	
	private void initView(Context mContext) {
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.layout_spiner_route, this);
		if (useFor == customerRouteView) {
			spinerRoute = (MySpinner) PriUtils.getInstance().findViewById(view, R.id.spinerRoute, PriHashMap.PriControl.NVBH_LOTRINH_TUYEN);
		} else if (useFor == staffPositionView) {
			spinerRoute = (MySpinner) PriUtils.getInstance().findViewById(view, R.id.spinerRoute, PriHashMap.PriControl.GSNPP_GIAMSAT_VITRINHANVIEN_DANHSACHNHANVIEN);
		} else if (useFor == tbhvGSnppNVbhPositionView) {
			spinerRoute = (MySpinner) PriUtils.getInstance().findViewById(view, R.id.spinerRoute, PriHashMap.PriControl.NVBH_LOTRINH_TUYEN);
		} else if (useFor == StaffPositionRoutedView) {
			 spinerRoute = (MySpinner)view.findViewById(R.id.spinerRoute);
			 spinerRoute.setSendSpiner2Event(true);
//			spinerRoute = (MySpinner) PriUtils.getInstance().findViewById(view, R.id.spinerRoute, PriHashMap.PriControl.NVBH_LOTRINH_TUYEN);
		} 
	}
	
	public void setAdapter(ArrayAdapter apdater){
		spinerRoute.setAdapter(apdater);
		
	}
	public void setAdapter(VNMSpinnerTextAdapter apdater){
		spinerRoute.setAdapter(apdater);
		
	}
	
	public void setPrompt(String prompt){
		spinerRoute.setPrompt(prompt);
	}
	
	public void setMinimumWidth(int w){
		spinerRoute.setMinimumWidth(GlobalUtil.dip2Pixel(w));
	}
	
	public void setSelection(int selection){
		spinerRoute.setSelection(selection);
	}
	
	public int getSelectedItemPosition(){
		return spinerRoute.getSelectedItemPosition();
	}
	
	
	public void setOnItemSelectedListener(OnItemSelectedListener listener){
//		spinerRoute.setOnItemSelectedListener(listener);
		PriUtils.getInstance().setOnItemSelectedListener(spinerRoute, listener);
		spinerRoute.setOnItemSelectedEvenIfUnchangedListener(listener);
	}
	
	public Spinner getSpiner(){
		return spinerRoute;
	}
}
