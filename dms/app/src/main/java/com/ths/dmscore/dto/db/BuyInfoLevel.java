/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;




/**
 * BuyInfo.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class BuyInfoLevel extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	public double totalQuantity;
	public double totalQuantityFix;
	public long totalAmount;
	public long totalAmountFix;
	public double usedQuantity;
	public long usedAmount;
	
	//Thong tin tong mua cua muc con
//	public ArrayList<BuyInfoLevel> buyInfoSubLevelArray = new ArrayList<BuyInfoLevel>();
}
