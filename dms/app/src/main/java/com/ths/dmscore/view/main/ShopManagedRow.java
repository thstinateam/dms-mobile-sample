package com.ths.dmscore.view.main;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.db.RoleUserDTO;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dms.R;
/**
 *  Row mo ta mot dong du lieu Shop khi dang nhap giam sat.
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ShopManagedRow extends DMSTableRow implements OnClickListener {
	private int actionSelectedShop = -1;
	//private Context context;
	private TextView tvNPP;
	private TextView tvAddress;
	ShopDTO itemShop;
	//su kien click row
	private OnEventControlListener listener;
	private RoleUserDTO itemRole;

	public ShopManagedRow(Context context, int actionSelectedShop) {
		super(context, R.layout.layout_shop_manage_row);
		this.actionSelectedShop = actionSelectedShop;
		listener = (OnEventControlListener) context;
		setOnClickListener(this);
		tvNPP = (TextView) findViewById(R.id.tvNPP);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
	}

	public void renderLayoutRole(int pos, RoleUserDTO item) {
		itemRole = item;
		if (!StringUtil.isNullOrEmpty(item.roleCode)) {
			tvNPP.setText(item.roleCode);
		}
		if (!StringUtil.isNullOrEmpty(item.roleName)) {
			tvAddress.setText(item.roleName);
		}
	}

	public void renderLayoutShop(int pos, ShopDTO item) {
		itemShop = item;
		tvNPP.setText(item.shopCode + " - " + item.shopName);
		if (!StringUtil.isNullOrEmpty(item.street) && !item.street.contains("null")) {
			tvAddress.setText(item.street);
		}
	}

	@Override
	public void onClick(View v) {
		if(v == this && listener != null && (itemShop != null || itemRole != null)){
			listener.onEvent(actionSelectedShop, null, itemShop != null ? itemShop : itemRole);
		}
	}

}
