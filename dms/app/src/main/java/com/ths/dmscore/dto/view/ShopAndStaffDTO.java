/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * Luu danh sach shop va staff tuong ung
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  18:43:18 12-03-2015
 */
public class ShopAndStaffDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public ArrayList<ShopAndStaffItem> listShopStaff = new ArrayList<ShopAndStaffItem>();
	
	public ShopAndStaffDTO () {
		listShopStaff = new ArrayList<ShopAndStaffItem>();
	}
	
	public ShopAndStaffItem newShopAndStaffItem() {
		return new ShopAndStaffItem();
	}
	
	/**
	 * shop va staff tuong ung
	 * 
	 * @author: hoanpd1
	 * @version: 1.0 
	 * @since:  18:47:40 12-03-2015
	 */
	public class ShopAndStaffItem implements Serializable {
		private static final long serialVersionUID = 1L;
		
		// id shop
		public int shopId;
		// ma shop
		public String shopCode;
		// ten shop
		public String shopName;
		// toa shop
		public double shopLat;
		public double shopLng;
		
		// id nhan vien
		public int staffId;
		// ma nhan vien
		public String staffCode;
		// ten nhan vien
		public String staffName;
		// toa nhan vien
		public double staffLat;
		public double staffLng;
		//loai Nhan vien
		public int gsnppObjectType;// loai gs

		/**
		 * init data with cursor
		 * @author: hoanpd1
		 * @since: 18:45:45 12-03-2015
		 * @return: void
		 * @throws:  
		 * @param c
		 */
		public void initObjectWithCursor(Cursor c) {
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
				shopId = CursorUtil.getInt(c, "SHOP_ID");
				shopCode = CursorUtil.getString(c, "SHOP_CODE");
				shopName = CursorUtil.getString(c, "SHOP_NAME");
				shopLat = CursorUtil.getDouble(c, "SHOP_LAT");
				shopLng = CursorUtil.getDouble(c, "SHOP_LNG");
				
				staffId = CursorUtil.getInt(c, "STAFF_ID");
				staffCode = CursorUtil.getString(c, "STAFF_CODE");
				staffName = CursorUtil.getString(c, "STAFF_NAME");
				gsnppObjectType = CursorUtil.getInt(c, "GS_OBJECT_TYPE");

			} catch (Exception e) {
			}
		}
	}
}
