package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;

/**
 * chua thong tin so luong cua u ke
 * 
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class EQUIP_SHELF_TOTAL_TABLE extends ABSTRACT_TABLE {

	public static final String EQUIP_SHELF_TOTAL_ID = "EQUIP_SHELF_TOTAL_ID";
	// id u ke
	public static final String SHELF_ID = "SHELF_ID";
	// so luong u ke
	public static final String QUANTITY = "QUANTITY";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_USER = "UPDATE_USER";
	// id KH
	public static final String STOCK_ID = "STOCK_ID";
	public static final String SHOP_ID = "SHOP_ID";

	public static final String EQUIP_SHELF_TOTAL_TABLE = "EQUIP_SHELF_TOTAL";

	public EQUIP_SHELF_TOTAL_TABLE(SQLiteDatabase mDB) {
		this.tableName = EQUIP_SHELF_TOTAL_TABLE;
		this.columns = new String[] { EQUIP_SHELF_TOTAL_ID, SHELF_ID, QUANTITY,
				STOCK_ID, SHOP_ID, CREATE_DATE, CREATE_USER, UPDATE_USER,
				UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * lay so luong cua u ke trong bang tong hop
	 * 
	 * @author: DungNX
	 * @param shopID
	 * @param customerId
	 * @param idUke
	 * @return
	 * @throws Exception
	 * @return: int
	 * @throws:
	 */
	public int getShelfQuantity(String shopID, long customerId, long idUke)
			throws Exception {
		int shelfQuantity = 0;

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sql = new StringBuffer();

		sql.append("	SELECT QUANTITY	");
		sql.append("	from EQUIP_SHELF_TOTAL	");
		sql.append("	where shop_id = ?	");
		params.add(shopID);
		sql.append("	and STOCK_ID = ?	");
		params.add(customerId + "");
		sql.append("	and SHELF_ID = ?	");
		params.add(idUke + "");

		Cursor c = null;
		try {
			c = rawQueries(sql.toString(), params);
			if (c != null) {

				if (c.moveToFirst()) {
					if (c.getColumnIndex("QUANTITY") >= 0) {
						shelfQuantity = c.getInt(c.getColumnIndex("QUANTITY"));
					} else {
						shelfQuantity = 0;
					}
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return shelfQuantity;
	}
}
