/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.util.ArrayList;


/**
 * KeyShopListDTOView.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  11:48:14 10-07-2015
 */
public class KeyShopListDTOView extends AbstractTableDTO {
	public int total;
	public ArrayList<KeyShopItemDTO> arrItem = new ArrayList<KeyShopItemDTO>();
	
}
