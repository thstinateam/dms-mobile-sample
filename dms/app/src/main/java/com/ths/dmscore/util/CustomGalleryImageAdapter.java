/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.util;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ths.dmscore.dto.db.MediaItemDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ServerPath;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Thumbnail Image Adapter cho gallery cham trung bay
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class CustomGalleryImageAdapter extends BaseAdapter {
	private ArrayList<MediaItemDTO> list;
	private int mGalleryItemBackground;
	private Context context;
	private BaseFragment fragParent;
	private boolean showBg = true;

	public CustomGalleryImageAdapter(Context c, BaseFragment fragParent,
			ArrayList<MediaItemDTO> list) {
		this.list = list;
		this.context = c;
		this.fragParent = fragParent;
		TypedArray a = c.obtainStyledAttributes(R.styleable.Gallery1);
		mGalleryItemBackground = a.getResourceId(
				R.styleable.Gallery1_android_galleryItemBackground, 0);
		a.recycle();
	}

	public void isShowBackground(boolean isShow) {
		showBg = isShow;
	}

	@Override
	public int getCount() {
		if (list != null) {
			return list.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup pt) {
		View row = convertView;
		ImageView imageItem = null;

		if (convertView == null) {
			LayoutInflater layout = (LayoutInflater) ((GlobalBaseActivity) context)
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = layout.inflate(R.layout.layout_thumbnail_item_view, pt, false);
			imageItem = (ImageView) row.findViewById(R.id.thumbnail_item);
			imageItem.setScaleType(ImageView.ScaleType.FIT_XY);
			if (showBg) {
				imageItem.setBackgroundResource(mGalleryItemBackground);
			}
			row.setTag(imageItem);
		} else {
			imageItem = (ImageView) row.getTag();
		}
		imageItem.setImageResource(R.drawable.album_default);
		if (!StringUtil.isNullOrEmpty(list.get(position).thumbUrl)) {
			if (list.get(position).thumbUrl
					.contains(ExternalStorage.SDCARD_PATH)) {
//				imageItem.setTag(list.get(position).thumbUrl);
				ImageUtil.loadImage(list.get(position).thumbUrl,
						imageItem);
			} else {
//				imageItem.setTag(ServerPath.IMAGE_PATH
//						+ list.get(position).thumbUrl);
				ImageUtil.loadImage(ServerPath.IMAGE_PATH
						+ list.get(position).thumbUrl,
						imageItem);
			}
		}

		// get more
		if (position == list.size() - 3) {
			fragParent.onEvent(ActionEventConstant.ACTION_GET_MORE_PHOTO, null,
					null);
		}

		return row;
	}
}
