/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.controller.SupervisorController;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;
import com.ths.dmscore.dto.view.ListComboboxShopStaffDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffDTO;
import com.ths.dmscore.dto.view.ReportKeyshopStaffItemDTO;
import com.ths.dmscore.dto.view.ValueItemDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpinnerAdapter;
import com.ths.dmscore.view.control.table.DMSColSortManager;
import com.ths.dmscore.view.control.table.DMSListSortInfoBuilder;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.listener.OnEventControlListener;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dmscore.view.control.table.DMSColSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 * Bao cao keyshop NVBH
 *
 * @author: yennth16
 * @version: 1.0
 * @since: 08:30:44 20-07-2015
 */
public class ReportKeyShopNVBH extends BaseFragment implements
		OnEventControlListener, VinamilkTableListener, OnClickListener,
		OnItemSelectedListener, DMSColSortManager.OnSortChange {
	private Button btSearch;// tim kiem
	private DMSTableView tbReportKeyshop;
	public ReportKeyshopStaffDTO ilDTO = new ReportKeyshopStaffDTO();
	boolean isReload = false;
	private Spinner spResult;
	private Spinner spPrograme;
	private Spinner spLine;
	SpinnerAdapter adapterLine;
	SpinnerAdapter adapterAward;
	SpinnerAdapter adapterResult;
	SpinnerAdapter adapterDisPlay;
	private boolean isSearch;
	private String programeId = Constants.STR_BLANK;
	private String cycleId = Constants.STR_BLANK;
	private String customer =Constants.STR_BLANK;
	private int result = 0;
	private int award = -1;

	private String staffId;
	private String shopId;
	// vo man hinh lan dau tien hay kg?
	private boolean isFirst = true;

	// don vi
	private Spinner spAward;
	ListComboboxShopStaffDTO combobox = new ListComboboxShopStaffDTO();
	private int indexAward = 0;
	private int indexResult= 0;
	private int indexSPLine = -1;
	private int indexSPProgram = -1;
	private String textShopId = "";
	private String[] arrDisplayPro;
	// MH chi tiet van de
	KeyShopDetailView followProblemDetail;
	// dialog product detail view
	AlertDialog alertFollowProblemDetail;
	ReportKeyshopStaffItemDTO dtoGoToDetail;
	KeyShopRewarDetailView paidDetail;
	AlertDialog alertPaidDetail;
	VNMEditTextClearable edCustomer; // kh
	String staffname = Constants.STR_BLANK;
	int channelObjectType = 0;
	long ksId = 0;

	private String[] arrAwardChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.TEXT_TRANSFER),
			StringUtil.getString(R.string.TEXT_LOCK),
			StringUtil.getString(R.string.TEXT_NOT_PAID),
			StringUtil.getString(R.string.TEXT_PAID_PART),
			StringUtil.getString(R.string.TEXT_PAID),
			StringUtil.getString(R.string.TEXT_KEY_SHOP_NOT_AWARD) };

	private String[] arrResultChoose = new String[] {
			StringUtil.getString(R.string.TEXT_ALL),
			StringUtil.getString(R.string.ATTAIN),
			StringUtil.getString(R.string.TEXT_NOT_ATTAIN)};

	public static ReportKeyShopNVBH getInstance(Bundle b) {
		ReportKeyShopNVBH f = new ReportKeyShopNVBH();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		channelObjectType = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType();
		if( channelObjectType == UserDTO.TYPE_STAFF){
			staffId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritId());
			shopId = String.valueOf(GlobalInfo.getInstance().getProfile()
					.getUserData().getInheritShopId());
			staffname = GlobalInfo.getInstance().getProfile().getUserData().getDisplayName();
		}else{
			staffId = getArguments().getString(IntentConstants.INTENT_STAFF_ID);
			shopId = getArguments().getString(IntentConstants.INTENT_SHOP_ID);
			staffname = getArguments().getString(IntentConstants.INTENT_STAFF_NAME);
			ksId = getArguments().getLong(IntentConstants.INTENT_KS_ID);
			if (StringUtil.isNullOrEmpty(staffId)) {
				staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
			}
			if(StringUtil.isNullOrEmpty(shopId)){
				shopId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.layout_report_key_shop_staff_view, container, false);
		View v = super.onCreateView(inflater, view, savedInstanceState);
		if( channelObjectType == UserDTO.TYPE_STAFF){
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.NVBH_BAOCAOKEYSHOP);
		}else{
			PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_BAOCAOKEYSHOP);
		}
		hideHeaderview();
		parent.setTitleName(StringUtil.getString(R.string.TITLE_VIEW_REPORT_KEYSHOP) +" - "+ staffname);
		initView(v);
		initHeaderTable(v);
		if (isFirst) {
			textShopId = shopId;
			getListCombobox();
		} else {
			spAward.setAdapter(adapterAward);
			spAward.setSelection(indexAward);
			spPrograme.setAdapter(adapterDisPlay);
			spPrograme.setSelection(indexSPProgram);
			spResult.setAdapter(adapterResult);
			spResult.setSelection(indexResult);
			spLine.setAdapter(adapterLine);
			spLine.setSelection(indexSPLine);
			renderLayout();
		}
		return v;

	}

	/**
	 * reqquest list SHOP va list NVBH cua shop
	 * @author: yennth16
	 * @since: 09:34:39 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void getListCombobox() {
		parent.showLoadingDialog();
		Bundle b = new Bundle();
		b.putSerializable(IntentConstants.INTENT_DATA, combobox);
		b.putString(IntentConstants.INTENT_SHOP_ID, textShopId);
		b.putString(IntentConstants.INTENT_STAFF, staffId);
		b.putString(
				IntentConstants.INTENT_STAFF_OWNER_ID,
				String.valueOf(GlobalInfo.getInstance().getProfile()
						.getUserData().getInheritId()));
		b.putBoolean(IntentConstants.INTENT_ORDER, true);
		b.putBoolean(IntentConstants.INTENT_IS_ALL, true);
		b.putBoolean(IntentConstants.INTENT_IS_REQUEST_FIRST, isFirst);
		b.putBoolean(IntentConstants.INTENT_IS_STAFF_OWNER_ID, false);
		handleViewEvent(b,
				ActionEventConstant.ACTION_GET_COMBOBOX_CTHTTM,
				SupervisorController.getInstance());
	}

	/**
	 * initSpinner
	 * @author: yennth16
	 * @since: 09:34:16 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpinner() {
		adapterResult = new SpinnerAdapter(parent, R.layout.simple_spinner_item,
				arrResultChoose);
		spResult.setAdapter(adapterResult);
		spResult.setSelection(indexResult);
		adapterAward = new SpinnerAdapter(parent, R.layout.simple_spinner_item,
				arrAwardChoose);
		spAward.setAdapter(adapterAward);
		spAward.setSelection(indexAward);
		initCycleInfo();
		initSpDisplayProgram();
		indexResult = 0;
		indexAward = 0;
		indexSPLine = 0;
		indexSPProgram = 0;
		getReportList(1);
	}

	/**
	 * initCycleInfo
	 * @author: yennth16
	 * @since: 09:34:29 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initCycleInfo() {
		if (combobox.listCycle != null && combobox.listCycle.size() > 0) {
			String[] arrMaNPP = new String[combobox.listCycle.size()];
			int i = 0;
			for (ValueItemDTO shop : combobox.listCycle) {
				if (!StringUtil.isNullOrEmpty(shop.name)) {
					arrMaNPP[i] = shop.name;
					if (shop.id.equalsIgnoreCase(combobox.curentCycle)) {
						indexSPLine = i;
					}
				} else {
					arrMaNPP[i] = "";
				}
				i++;
			}
			adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrMaNPP);
			spLine.setAdapter(adapterLine);
			if (indexSPLine >= 0) {
				spLine.setSelection(indexSPLine);
			} else{
				spLine.setSelection(0);
			}
			if (spLine.getSelectedItemPosition() >= 0) {
				textShopId = String.valueOf(combobox.listCycle.get(spLine
						.getSelectedItemPosition()).id);
			}
		} else {
			adapterLine = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spLine.setAdapter(adapterLine);
			// truong hop ko co ds nv ban hang
			renderLayout();
		}

	}
	/**
	 * Hien thi pop up chi tiet van de
	 * @author: yennth16
	 * @since: 11:43:05 10-07-2015
	 * @return: void
	 * @throws:
	 * @param dto
	 */
	private void showFollowProblemDetail(ReportKeyshopStaffItemDTO dto) {
		if (alertFollowProblemDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			followProblemDetail = new KeyShopDetailView(parent, this);
			build.setView(followProblemDetail.viewLayout);
			alertFollowProblemDetail = build.create();
			Window window = alertFollowProblemDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		followProblemDetail.updateLayoutReport(dto);
		alertFollowProblemDetail.show();
	}

	/**
	 * Hien thi pop up chi tiet van de
	 * @author: yennth16
	 * @since: 11:43:05 10-07-2015
	 * @return: void
	 * @throws:
	 * @param dto
	 */
	private void showPaidDetail(ReportKeyshopStaffItemDTO dto) {
		if (alertPaidDetail == null) {
			Builder build = new AlertDialog.Builder(parent,
					R.style.CustomDialogTheme);
			paidDetail = new KeyShopRewarDetailView(parent, this);
			build.setView(paidDetail.viewLayout);
			alertPaidDetail = build.create();
			Window window = alertPaidDetail.getWindow();
			window.setBackgroundDrawable(new ColorDrawable(Color.argb(0, 255,
					255, 255)));
			window.setLayout(LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT);
			window.setGravity(Gravity.CENTER);
		}
		paidDetail.updateLayout(dto);
		alertPaidDetail.show();
	}

	/**
	 * initSpDisplayProgram
	 * @author: yennth16
	 * @since: 07:50:09 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initSpDisplayProgram() {
		if (combobox.listDisplayProgramThreeCycle != null
				&& combobox.listDisplayProgramThreeCycle.size() > 0) {
			int size = combobox.listDisplayProgramThreeCycle.size();
			// cong size them 1 do add them tat ca vap mang
			arrDisplayPro = new String[size + 1];
			int i = 0;
			DisplayProgrameDTO item1 = new DisplayProgrameDTO();
			item1.addAll(-1, StringUtil.getString(R.string.TEXT_ALL), "");
			combobox.listDisplayProgramThreeCycle.add(0, item1);
			for (DisplayProgrameDTO item : combobox.listDisplayProgramThreeCycle) {
				if(ksId > 0 && ksId == item.displayProgrameId){
					indexSPProgram = i;
				}
				if (!StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& !StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameCode + " - "
							+ item.displayProgrameName;
				} else if (StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& !StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameName;
				} else if (!StringUtil.isNullOrEmpty(item.displayProgrameCode)
						&& StringUtil.isNullOrEmpty(item.displayProgrameName)) {
					arrDisplayPro[i] = item.displayProgrameCode;
				} else {
					arrDisplayPro[i] = "";
				}
				i++;

			}
			adapterDisPlay = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, arrDisplayPro);
			spPrograme.setAdapter(adapterDisPlay);
			if (indexSPProgram >= 0) {
				spPrograme.setSelection(indexSPProgram);
			} else {
				spPrograme.setSelection(0);
			}
			if (spPrograme.getSelectedItemPosition() >= 0) {
				programeId = String
						.valueOf(combobox.listDisplayProgramThreeCycle.get(spPrograme
								.getSelectedItemPosition()).displayProgrameId);
			}
		} else {
			// truong hop ko co ds nv ban hang
			adapterDisPlay = new SpinnerAdapter(parent,
					R.layout.simple_spinner_item, new String[0]);
			spPrograme.setAdapter(adapterDisPlay);
			ilDTO.total = 0;
			ilDTO.listItem.clear();
			renderLayout();
		}
	}

	/**
	 * initView
	 * @author: yennth16
	 * @since: 09:33:55 21-07-2015
	 * @return: void
	 * @throws:
	 * @param v
	 */
	private void initView(View v) {
		PriUtils.getInstance().genPriHashMapForForm(PriHashMap.PriForm.GSNPP_DSHINHANH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.lnSearch,
				PriHashMap.PriControl.GSNPP_DSHINHANH_TIMKIEM);
		spResult = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spResult, PriHashMap.PriControl.GSNPP_DSHINHANH_NHANVIEN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvStaff,
				PriHashMap.PriControl.GSNPP_DSHINHANH_NHANVIEN);

		spPrograme = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spPrograme, PriHashMap.PriControl.GSNPP_DSHINHANH_CHUONGTRINH);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvPrograme,
				PriHashMap.PriControl.GSNPP_DSHINHANH_CHUONGTRINH);

		spLine = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spLine, PriHashMap.PriControl.GSNPP_DSHINHANH_TUYEN);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvLine,
				PriHashMap.PriControl.GSNPP_DSHINHANH_TUYEN);
		btSearch = (Button) PriUtils.getInstance().findViewByIdGone(v,
				R.id.btSearch, PriHashMap.PriControl.GSNPP_DSHINHANH_TIMKIEM);
		PriUtils.getInstance().setOnItemSelectedListener(spResult, this);
		PriUtils.getInstance().setOnItemSelectedListener(spPrograme, this);
		PriUtils.getInstance().setOnItemSelectedListener(spLine, this);
		PriUtils.getInstance().setOnClickListener(btSearch, this);

		tbReportKeyshop = (DMSTableView) v.findViewById(R.id.tbReportKeyshop);
		tbReportKeyshop.setListener(this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.lnSearch,
				PriHashMap.PriControl.GSNPP_DSHINHANH_DSDONVI);
		spAward = (Spinner) PriUtils.getInstance().findViewByIdGone(v,
				R.id.spAward, PriHashMap.PriControl.GSNPP_DSHINHANH_DSDONVI);
		PriUtils.getInstance().setOnItemSelectedListener(spAward, this);
		PriUtils.getInstance().findViewByIdGone(v, R.id.tvShop,
				PriHashMap.PriControl.GSNPP_DSHINHANH_DSDONVI);
		edCustomer = (VNMEditTextClearable) v.findViewById(R.id.edCustomer);

	}

	/**
	 * init gia tri tim kiem
	 * @author: yennth16
	 * @since: 09:33:42 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void initValueSearch() {
		tbReportKeyshop.getPagingControl().totalPage = -1;
		indexSPProgram = spPrograme.getSelectedItemPosition();
		indexSPLine = spLine.getSelectedItemPosition();
		indexResult = spResult.getSelectedItemPosition();
		indexAward = spAward.getSelectedItemPosition();
		if (spPrograme.getSelectedItemPosition() >0) {
			programeId = String.valueOf(combobox.listDisplayProgramThreeCycle
					.get(indexSPProgram).displayProgrameId);
		}else{
			programeId = Constants.STR_BLANK;
		}
		if (spLine.getSelectedItemPosition() >= 0) {
			cycleId = String.valueOf(combobox.listCycle.get(indexSPLine).id);
		}
		if (spResult.getSelectedItemPosition() >= 0) {
			result = result(spResult.getSelectedItemPosition());
		}

		if (spAward.getSelectedItemPosition() >= 0) {
			award = award(spAward.getSelectedItemPosition());
		}
		customer = edCustomer.getText().toString();
		getReportList(1);

	}

	/**
	 * result
	 * @author: yennth16
	 * @since: 08:26:47 21-07-2015
	 * @return: int
	 * @throws:
	 * @param position
	 * @return
	 */
	public int result(int position){
		int result = 0;
		if(position == 1){
			result = ReportKeyshopStaffItemDTO.TYPE_RESULT_ATTAIN;
		}else if(position == 2){
			result = ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_ATTAIN;
		}
		return result;
	}

	/**
	 * award
	 * @author: yennth16
	 * @since: 08:28:24 21-07-2015
	 * @return: int
	 * @throws:
	 * @param position
	 * @return
	 */
	public int award(int position){
		int award = -1;
		if(position == 1){
			award = ReportKeyshopStaffItemDTO.TYPE_REWARD_TRANSFER;
		}else if(position == 2){
			award = ReportKeyshopStaffItemDTO.TYPE_RESULT_LOCK;
		}else if(position == 3){
			award = ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_PAID;
		}else if(position == 4){
			award = ReportKeyshopStaffItemDTO.TYPE_RESULT_PAID_PART;
		}else if(position == 5){
			award = ReportKeyshopStaffItemDTO.TYPE_RESULT_PAID;
		}else if(position == 6){
			award = ReportKeyshopStaffItemDTO.TYPE_RESULT_NOT_ASSIGN;
		}else{
			award = -1;
		}
		return award;
	}

	/**
	 * initHeaderTable
	 *
	 * @author: yennth16
	 * @since: 15:21:49 15-07-2015
	 * @return: void
	 * @throws:
	 * @param v
	 */
	private void initHeaderTable(View v) {
		// init header with sort
		SparseArray<DMSColSortInfo> lstSort = new DMSListSortInfoBuilder()
				.addInfoCaseUnSensitive(2, SortActionConstants.CODE_PROGRAM)
				.addInfoCaseUnSensitive(3, SortActionConstants.CODE)
				.addInfoCaseUnSensitive(4, SortActionConstants.NAME)
				.addInfoCaseUnSensitive(5, SortActionConstants.ADDRESS)
				.addInfoCaseUnSensitive(6, SortActionConstants.LEVEL)
				.addInfoCaseUnSensitive(7, SortActionConstants.RESULT)
				.addInfoCaseUnSensitive(8, SortActionConstants.REWARD)
				.addInfoCaseUnSensitive(9, SortActionConstants.AMOUNT_PLAN)
				.addInfoCaseUnSensitive(10, SortActionConstants.AMOUNT_DONE)
				.addInfoCaseUnSensitive(11, SortActionConstants.QUANITY_PLAN)
				.addInfoCaseUnSensitive(12, SortActionConstants.QUANITY_DONE)
				.build();
		// khoi tao header cho table
		initHeaderTable(tbReportKeyshop, new ReportKeyShopNVBHRow(parent), lstSort,
				this);
	}

	/**
	 * Lay danh bao cao
	 * @author: yennth16
	 * @since: 08:32:08 21-07-2015
	 * @return: void
	 * @throws:
	 * @param page
	 */
	private void getReportList(int page) {// (int page) {
		if (!parent.isShowProgressDialog())
			parent.showLoadingDialog();
		Bundle data = new Bundle();
		data.putInt(IntentConstants.INTENT_PAGE, page);
		data.putString(IntentConstants.INTENT_DISPLAY_PROGRAM_ID, programeId);
		data.putString(IntentConstants.INTENT_CYCLE_ID, cycleId);
		data.putInt(IntentConstants.INTENT_REPORT_RESULT, result);
		data.putInt(IntentConstants.INTENT_REPORT_AWARD, award);
		data.putString(IntentConstants.INTENT_CUSTOMER, customer);
		data.putBoolean(IntentConstants.INTENT_IS_SEARCH, isSearch);
		data.putString(IntentConstants.INTENT_SHOP_ID, shopId);
		data.putString(IntentConstants.INTENT_STAFF_ID, staffId);

		data.putSerializable(IntentConstants.INTENT_SORT_DATA,
				tbReportKeyshop.getSortInfo());

		handleViewEvent(data, ActionEventConstant.GO_TO_REPORT_STAFF,
				SupervisorController.getInstance());
	}

	@Override
	public void handleModelViewEvent(ModelEvent modelEvent) {
		ActionEvent e = modelEvent.getActionEvent();
		switch (e.action) {
		case ActionEventConstant.ACTION_GET_COMBOBOX_CTHTTM:
			combobox = (ListComboboxShopStaffDTO) modelEvent.getModelData();
			if (combobox != null) {
				initSpinner();
			} else {
				// truong hop ko co ds nv ban hang
				ilDTO.total = 0;
				ilDTO.listItem.clear();
				renderLayout();
			}
			break;
		case ActionEventConstant.GO_TO_REPORT_STAFF:
			ReportKeyshopStaffDTO tempDto = (ReportKeyshopStaffDTO) modelEvent.getModelData();
			ilDTO = tempDto;
			renderLayout();
			parent.closeProgressDialog();
			break;
		default:
			super.handleModelViewEvent(modelEvent);
			break;
		}
		parent.closeProgressDialog();
	}

	@Override
	public void handleErrorModelViewEvent(ModelEvent modelEvent) {
		parent.closeProgressDialog();
		super.handleErrorModelViewEvent(modelEvent);
	}

	/**
	 * renderLayout
	 * @author: yennth16
	 * @since: 09:32:46 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void renderLayout() {
		tbReportKeyshop.clearAllData();
		int length = ilDTO.listItem.size();
		int pos = 1 + Constants.NUM_ITEM_PER_PAGE
				* (tbReportKeyshop.getPagingControl().getCurrentPage() - 1);
		for (int i = 0; i < length; i++) {
			ReportKeyShopNVBHRow row = new ReportKeyShopNVBHRow(parent);
			row.renderLayout(pos, ilDTO.listItem.get(i));
			pos++;
			row.setListener(this);
			tbReportKeyshop.addRow(row);
		}
		if (tbReportKeyshop.getPagingControl().totalPage < 0){
			tbReportKeyshop.setTotalSize(ilDTO.total, 1);
		}

	}

	@Override
	public void onClick(View v) {
		if (v == btSearch) {
			GlobalUtil.forceHideKeyboard(parent);
			// hide ban phim
			search();
		}
		if (followProblemDetail != null && followProblemDetail.btCloseKeyshopDetail == v) {
			if (alertFollowProblemDetail != null) {
				alertFollowProblemDetail.dismiss();
			}
		}
		if (paidDetail != null && paidDetail.btCloseKeyshopDetail == v) {
			if (alertPaidDetail != null) {
				alertPaidDetail.dismiss();
			}
		}
	}

	private void search() {
		if (ilDTO != null) {
			initValueSearch();
		}
	}

	/**
	 * resetAllValue
	 * @author: yennth16
	 * @since: 09:31:37 21-07-2015
	 * @return: void
	 * @throws:
	 */
	private void resetAllValue() {
		isSearch = false;
		indexSPLine = -1;
		indexAward = 0;
		indexResult = 0;
		indexSPProgram = -1;
		programeId = "";
		isFirst = true;
		if (tbReportKeyshop != null) {
			tbReportKeyshop.resetSortInfo();
		}
//		spLine.setSelection(0);
//		spPrograme.setSelection(0);
//		spAward.setSelection(0);
//		spResult.setSelection(0);
	}

	@Override
	public void onEvent(int eventType, View control, Object data) {
		switch (eventType) {
		default:
			super.onEvent(eventType, control, data);
			break;
		}
	}

	@Override
	public void handleVinamilkTableRowEvent(int action, View control,
			Object data) {
		switch (action) {
		case ActionEventConstant.GO_TO_REPORT_KEY_SHOP_NVBH: {
			// show detail problem
			dtoGoToDetail = (ReportKeyshopStaffItemDTO) data;
			showFollowProblemDetail(dtoGoToDetail);
			break;
		}
		case ActionEventConstant.GO_TO_REPORT_KEY_SHOP_PAID: {
			// show detail problem
			dtoGoToDetail = (ReportKeyshopStaffItemDTO) data;
			showPaidDetail(dtoGoToDetail);
			break;
		}
		default:
			break;
		}
	}


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		if (arg0 == spPrograme) {
			if (indexSPProgram != spPrograme.getSelectedItemPosition()) {
				indexSPProgram = spPrograme.getSelectedItemPosition();
				search();
			}
		} else if (arg0 == spLine
				&& arg0.getSelectedItemPosition() != indexSPLine) {
			indexSPLine = spLine.getSelectedItemPosition();
			search();
		}else if(arg0 == spResult
				&& arg0.getSelectedItemPosition() != indexResult){
			indexResult = spResult.getSelectedItemPosition();
			search();
		}else if(arg0 == spAward
				&& arg0.getSelectedItemPosition() != indexAward){
			indexAward = spAward.getSelectedItemPosition();
			search();
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	protected void receiveBroadcast(int action, Bundle extras) {
		switch (action) {
		case ActionEventConstant.NOTIFY_REFRESH_VIEW:
			if (this.isVisible()) {
				// hide ban phim
				GlobalUtil.forceHideKeyboard(parent);
				resetAllValue();
				textShopId = shopId;
				getListCombobox();
			}
			break;
		default:
			super.receiveBroadcast(action, extras);
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onSortChange(DMSTableView tb, DMSSortInfo sortInfo) {
		getReportList(tbReportKeyshop.getPagingControl().getCurrentPage());
	}

	@Override
	public void handleVinamilkTableloadMore(View control, Object data) {
		getReportList(tbReportKeyshop.getPagingControl().getCurrentPage());

	}
}
