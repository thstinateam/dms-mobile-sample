/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dms.R;

public class NvbhReportAsoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public String code;
	public String name;
	public String typeName;

	public long plan;
	public long done;
	public long remain;
	public double percent;

	public NvbhReportAsoDTO() {
		code = Constants.STR_BLANK;
		name = Constants.STR_BLANK;
		typeName = Constants.STR_BLANK;
		plan = 0;
		done = 0;
		remain = 0;
		percent = 0;
	}

	public void initDataWithCursor(Cursor c) {
		code = CursorUtil.getString(c, "CODE");
		name = CursorUtil.getString(c, "NAME");
		int type = CursorUtil.getInt(c, "OBJECT_TYPE");
		typeName = type == 1 ? StringUtil.getString(R.string.TEXT_ASO_TYPE_SKU)
				: (type == 2 ? StringUtil.getString(R.string.TEXT_ASO_TYPE_SUB_CAT) : "");
		plan = CursorUtil.getLong(c, "PLAN");
		done = CursorUtil.getLong(c, "DONE");
		remain = StringUtil.calRemain(plan, done);
		percent = StringUtil.calPercentUsingRound(plan, done);
	}
}
