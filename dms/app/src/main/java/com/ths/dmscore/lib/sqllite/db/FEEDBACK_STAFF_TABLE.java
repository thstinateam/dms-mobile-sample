/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedbackStaffDTO;
import com.ths.dmscore.dto.view.FollowProblemItemDTO;

/**
 * FEEDBACK_STAFF_TABLE.java
 * @author: yennth16
 * @version: 1.0
 * @since:  09:45:49 21-05-2015
 */
public class FEEDBACK_STAFF_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String FEEDBACK_STAFF_ID = "FEEDBACK_STAFF_ID";
	// id FEEDBACK
	public static final String FEEDBACK_ID = "FEEDBACK_ID";
	// STAFF_ID
	public static final String STAFF_ID = "STAFF_ID";
	// RESULT
	public static final String RESULT = "RESULT";
	// nguoi thuc hien
	public static final String DONE_DATE = "DONE_DATE";
	// so lan yeu cau lam lai
	public static final String NUM_RETURN = "NUM_RETURN";
	// ID HUAN LUYEN
	public static final String TRAINING_PLAN_DETAIL_ID = "TRAINING_PLAN_DETAIL_ID";
	// parent staff id
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";

	public static final String FEEDBACK_STAFF_TABLE = "FEEDBACK_STAFF";

	public FEEDBACK_STAFF_TABLE(SQLiteDatabase mDB) {
		this.tableName = FEEDBACK_STAFF_TABLE;
		this.columns = new String[] { FEEDBACK_STAFF_ID, FEEDBACK_ID,
				STAFF_ID, RESULT, DONE_DATE, NUM_RETURN,
				TRAINING_PLAN_DETAIL_ID, CREATE_USER_ID,
				CREATE_DATE, UPDATE_USER, UPDATE_DATE, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}
	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((FeedbackStaffDTO) dto);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	public long updateDoneDateFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_STAFF_TABLE.equals(dto.getType())) {
			try {
				ContentValues editedValues = new ContentValues();
				editedValues.put(RESULT, dto.getFeedbackStaffDTO().result);
				editedValues.put(DONE_DATE, dto.getFeedbackStaffDTO().doneDate);
				editedValues.put(UPDATE_DATE, dto.getFeedbackStaffDTO().updateDate);
				editedValues.put(UPDATE_USER, dto.getFeedbackStaffDTO().updateUser);
				String[] params = { "" + dto.getFeedbackStaffDTO().feedBackStaffId };
				returnCode = update(editedValues, FEEDBACK_STAFF_ID + " = ?", params);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Thuc hien cap nhat van de cua nvbh (thuc hien boi GSNPP)
	 *
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateGSNPPFollowProblemDone(FollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(RESULT, dto.status);
			editedValues.put(UPDATE_DATE, dto.updateDate);
			editedValues.put(UPDATE_USER, dto.updateUser);
			String[] params = { "" + dto.feedbackStaffId };
			returnCode = update(editedValues, FEEDBACK_STAFF_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	 /**
	 * insert feedback staff
	 * @author: Tuanlt11
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	*/
	public long insertFeedbackStaff(FeedbackStaffDTO dto) {
		long returnCode = -1;
			try {
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		return returnCode;
	}

	private ContentValues initDataRow(FeedbackStaffDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(FEEDBACK_ID, dto.feedBackId);
		editedValues.put(FEEDBACK_STAFF_ID, dto.feedBackStaffId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.trainingPlanDetailId);
		editedValues.put(CREATE_USER_ID, dto.createUserId);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(RESULT, dto.result);
		editedValues.put(NUM_RETURN, dto.numReturn);
		return editedValues;
	}

}
