package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.lib.sqllite.db.RPT_CTTL_DETAIL_TABLE;

public class RptCttlDetailDTO extends AbstractTableDTO {
	public static final long serialVersionUID = 1L;
	public long rptCttlDetailId;
	public long rptCttlId;
	public int productId;
	public double quantity;
	public long amount;
	public double quantityPayPromotion;
	public long amountPayPromotion;
	public double quantityBegin;
	public long amountBegin;
	public long priceBegin;
	
	/**
	 * Khoi tao du lieu tu cursor
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param cOrderDetail
	 */
	public void initFromCursor(Cursor c) {
		rptCttlDetailId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_TABLE.RPT_CTTL_DETAIL_ID);
		rptCttlId = CursorUtil.getLong(c, RPT_CTTL_DETAIL_TABLE.RPT_CTTL_ID);
		productId = CursorUtil.getInt(c, RPT_CTTL_DETAIL_TABLE.PRODUCT_ID);
		quantity = CursorUtil.getDouble(c, RPT_CTTL_DETAIL_TABLE.QUANTITY);
		amount = CursorUtil.getLong(c, RPT_CTTL_DETAIL_TABLE.AMOUNT);
		quantityPayPromotion = CursorUtil.getDouble(c, RPT_CTTL_DETAIL_TABLE.QUANTITY_PAY_PROMOTION);
		amountPayPromotion = CursorUtil.getLong(c, RPT_CTTL_DETAIL_TABLE.AMOUNT_PAY_PROMOTION);
		quantityBegin = quantity;
		amountBegin = amount;
		priceBegin = (long)(amountBegin / quantityBegin);
	}
}