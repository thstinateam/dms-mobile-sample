/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.controller;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.global.ActionEvent;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.ErrorConstants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.global.ModelEvent;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPRequest;
import com.ths.dmscore.lib.network.http.OAuthRequestManager;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;

/**
 *  Lop base controller
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public abstract class AbstractController {
	//private int isController = -1;
	// trace log xuong server
	abstract public void handleViewEvent(ActionEvent e);
    abstract public void handleModelEvent(ModelEvent modelEvent);
    abstract public void handleSwitchActivity(ActionEvent e);
    abstract public boolean handleSwitchFragment(ActionEvent e);

    public void handleErrorModelEvent(final ModelEvent modelEvent) {
    	final ActionEvent e = modelEvent.getActionEvent();
    	HTTPRequest request = e.request;
    	String msg = modelEvent.getDataText();
    	if(msg != null && msg.contains(Constants.APACHE_TOMCAT)){
    		modelEvent.setIsSendLog(false);
    	}
    	if (modelEvent.getModelCode() == ErrorConstants.ERROR_EXPIRED_TIMESTAMP){
    		//truong hop timestamp co oauth la time-out thi van set lai thoi gian
    	}else if (modelEvent.getModelCode() == ErrorConstants.ERROR_SESSION_RESET||
    			modelEvent.getCode() == OAuthRequestManager.TIMED_OUT) {
    		modelEvent.setModelCode(ErrorConstants.ERROR_SESSION_RESET);
			GlobalInfo.getInstance().getProfile().getUserData().setLoginState(UserDTO.NOT_LOGIN);
		}

		// check: chi ghi log khi loi server
		if (modelEvent.getIsSendLog() && modelEvent.getModelCode() !=  ErrorConstants.ERROR_NO_CONNECTION
				&& modelEvent.getModelCode() !=  ErrorConstants.ERROR_SESSION_RESET
				&&  (modelEvent.getModelCode() == ErrorConstants.ERROR_COMMON ||
				   (modelEvent.getModelCode() != ErrorConstants.ERROR_COMMON  && e.action != ActionEventConstant.ACTION_LOGIN ))){
			boolean isInsertTableLog = true;
			if (e.logData != null && e.logData instanceof LogDTO){
				LogDTO logDTO = (LogDTO) e.logData;
				if (logDTO != null &&
						(logDTO.tableType == LogDTO.TYPE_LOG ||
								(LogDTO.STATE_FAIL.equals(logDTO.state)))){
					// TH log loi thi chi can goi len server, ko insert lai table_log
					isInsertTableLog = false;
				}
			}
			
			String param = modelEvent.getParams();
			try {
				JSONObject json = new JSONObject(param);
				json.remove(IntentConstants.INTENT_USER_NAME);
				json.remove(IntentConstants.INTENT_LOGIN_PASSWORD);
				
				param = json.toString();
			} catch (JSONException e1) {
				
			}
			
			ServerLogger.sendLog(param, modelEvent.getDataText(),
					isInsertTableLog, TabletActionLogDTO.LOG_SERVER);
		}
		
		// end
		handleCommonError(modelEvent);
		if (e.sender != null && (request == null || (request != null && request.isAlive()))) {
			if(e.sender instanceof GlobalBaseActivity){
				final GlobalBaseActivity sender = (GlobalBaseActivity) e.sender;
				if(sender == null || sender.isFinished){
					return;
				}
				sender.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(sender == null || sender.isFinished){
							return;
						}
						sender.handleErrorModelViewEvent(modelEvent);
					}
				});
			}else if(e.sender instanceof BaseFragment){
				final BaseFragment sender = (BaseFragment) e.sender;
				if(sender == null || sender.getActivity() == null || sender.isFinished){
					return;
				}
				sender.getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if(sender == null || sender.getActivity() == null || sender.isFinished){
							return;
						}
						sender.handleErrorModelViewEvent(modelEvent);
					}
				});
			}
		}
	}

    /**
     *
    *  Xu ly cac loi chung
    *  @author: AnhND
    *  @param modelEvent
    *  @return: void
    *  @throws:
     */
    public void handleCommonError(ModelEvent modelEvent){
    	ActionEvent actionEvent = modelEvent.getActionEvent();
    	switch(modelEvent.getModelCode()){
    	case ErrorConstants.ERROR_SESSION_RESET:
    		actionEvent.controller = this;
    		break;
    	}
    }
    /**
    * remove all fragment in back stack Muc dich: cac trang o menu chinh khong
	* can luu trong stack khi chuyen cac trang trong menu.
    *  @author: BangHn
    *  @param fm
    *  @return: void
    *  @throws:
     */
	public void removeAllInBackStack(FragmentManager fm) {
		for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
			fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
			fm.executePendingTransactions();
		}
		GlobalInfo.getInstance().popAllTag();
	}

	/**
	 * switch Fragment
	 * @author: duongdt3
	 * @since: 15:33:50 16 Jan 2015
	 * @return: boolean
	 * @throws:  
	 * @param parent
	 * @param frag
	 * @param isRemoveInBackStack
	 * @return
	 */
	boolean switchFragment(Activity parent, BaseFragment frag, boolean isRemoveInBackStack){
		boolean result = false;
		String TAG = frag != null ? frag.getTAG() : null;
		MyLog.d("Fragment", "switchFragment: " + TAG);
		if (parent != null && !parent.isFinishing() && frag != null) {
			FragmentManager fm = parent.getFragmentManager();
			if (fm != null) {
				if (isRemoveInBackStack) {
					removeAllInBackStack(fm);
				}
				
				Fragment existsFrag = fm.findFragmentByTag(TAG);
				if (existsFrag != null) {
					FragmentTransaction ftRemove = fm.beginTransaction();
					ftRemove.remove(existsFrag);
					ftRemove.commit();
					MyLog.d("Fragment", "remove Fragment: " + TAG);
				}
				
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.fragDetail, frag, TAG);
				ft.addToBackStack(TAG);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
				
				GlobalInfo.getInstance().setCurrentTag(TAG);
				MyLog.d("Fragment",
						"Num fragment in stack : " + fm.getBackStackEntryCount());
				
				//set result
				result = true;
			} else{
				MyLog.d("Fragment", "switchFragment fail FragmentManager null: " + TAG);
			}
		} else{
			MyLog.d("Fragment", "switchFragment fail: " + TAG);
		}
		MyLog.d("Fragment", "switchFragment result: " + result);
		return result;
	}
}
