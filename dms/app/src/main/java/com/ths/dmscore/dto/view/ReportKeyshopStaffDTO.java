/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * ReportKeyshopStaffDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  16:29:33 20-07-2015
 */
public class ReportKeyshopStaffDTO {
	public int total;
	public ArrayList<ReportKeyshopStaffItemDTO> listItem;
	
	public ReportKeyshopStaffDTO(){
		listItem = new ArrayList<ReportKeyshopStaffItemDTO>();
		total=0;
	}
	
	public void addItem(ReportKeyshopStaffItemDTO c) {
		listItem.add(c);
	}
	public ReportKeyshopStaffItemDTO newImageListRowDTO() {
		return new ReportKeyshopStaffItemDTO();
	}
}
