/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * ReportKeyshopDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  17:36:03 21-07-2015
 */
public class ReportKeyshopDTO {
public ArrayList<ReportKeyshopItemDTO> listItem;
	
	public ReportKeyshopDTO(){
		listItem = new ArrayList<ReportKeyshopItemDTO>();
	}
	
	public void addItem(ReportKeyshopItemDTO c) {
		listItem.add(c);
	}
	public ReportKeyshopItemDTO total = new ReportKeyshopItemDTO();
}
