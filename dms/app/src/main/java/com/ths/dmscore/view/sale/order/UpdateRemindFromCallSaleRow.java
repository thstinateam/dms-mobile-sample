package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.widget.CheckBox;
import android.widget.TextView;

import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

public class UpdateRemindFromCallSaleRow extends DMSTableRow {
	public TextView tvNumer;
	public TextView tvDoneDate;
	public CheckBox cbDone;
	public TextView tvContent;
	public TextView tvRemindDate;
	public TextView tvType;

	public UpdateRemindFromCallSaleRow(Context context) {
		super(context, R.layout.layout_update_remind_from_callsale_row);
		tvNumer = (TextView) findViewById(R.id.tvSTT);
		tvContent = (TextView) findViewById(R.id.tvContent);
		tvRemindDate = (TextView) findViewById(R.id.tvRemindDate);
		tvDoneDate = (TextView) findViewById(R.id.tvOpDate);
		cbDone = (CheckBox) findViewById(R.id.cbNote);
		tvType = (TextView)findViewById(R.id.tvType);
	}

	public void renderLayout(int pos, FeedBackDTO item) {
		tvNumer.setText("" + pos);
		if(item.getRemindDate() != null){
			tvRemindDate.setText(item.getRemindDate());
		}else{
			tvRemindDate.setText("");
		}
		if(item.getDoneDate() != null){
			tvDoneDate.setText(item.getDoneDate());
		}else{
			tvDoneDate.setText("");
		}
		if(!StringUtil.isNullOrEmpty(item.getDoneDate())){
			cbDone.setChecked(true);
			cbDone.setEnabled(false);
		}else{
			cbDone.setChecked(false);
		}
		tvType.setText(item.getTypeTitle(item.getFeedbackType()));
		tvContent.setText(item.getContent());
	}

}
