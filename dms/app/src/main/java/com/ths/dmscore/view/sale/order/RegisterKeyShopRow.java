package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.RegisterKeyShopItemDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.view.control.VNMEditTextClearable;
import com.ths.dms.R;

/**
 * Row ds CTKM them hang
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RegisterKeyShopRow extends DMSTableRow implements OnClickListener, TextWatcher, OnFocusChangeListener{

	TextView tvLevel; // Muc
	TextView tvMoney; // Money
	TextView tvQuantity; // SL
	VNMEditTextClearable edMultiple; // boi so
	TextView tvTotalMoney; // tong tien
	TextView tvTotalQuantity; // so luong
	public RegisterKeyShopItemDTO dto; // dto du lieu cua mot row

	public RegisterKeyShopRow(Context context) {
		super(context, R.layout.layout_register_keyshop_row, GlobalUtil
				.getInstance().getDPIScreen()
				- GlobalUtil
						.dip2Pixel(Constants.LEFT_MARGIN_TABLE_DIP_SMALL * 8),
				GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {R.id.tvTotalMoney});
		setOnClickListener(this);
		initView();
	}

	 /**
	 * Khoi tao view
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void initView(){
		tvLevel = (TextView) findViewById(R.id.tvLevel);
		tvMoney = (TextView) findViewById(R.id.tvMoney);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
		tvTotalMoney = (TextView) findViewById(R.id.tvTotalMoney);
		tvTotalQuantity = (TextView) findViewById(R.id.tvTotalQuantity);
		edMultiple = (VNMEditTextClearable) findViewById(R.id.edMultiple);
		edMultiple.addTextChangedListener(this);
		tvLevel.requestFocus();
	}

	 /**
	 * render layout
	 * @author: Tuanlt11
	 * @param isEdit
	 * @param listener
	 * @return: void
	 * @throws:
	*/
	public void renderLayout(RegisterKeyShopItemDTO dto, boolean isEdit){
		this.dto = dto;
		display(tvLevel, dto.ksLevelItem.name);
		display(tvMoney, dto.ksLevelItem.amount);
		display(tvQuantity, dto.ksLevelItem.quantity);
		if(!isEdit){
			edMultiple.setEnabled(false);
			edMultiple.setImageClearVisibile(false);
			edMultiple.clearFocus();
			edMultiple.setFocusable(false);
		}
		else{
			edMultiple.setEnabled(true);
			edMultiple.setImageClearVisibile(true);
			edMultiple.setFocusable(true);
			edMultiple.clearFocus();
		}
		if(dto.ksCustomerLevelItem.ksCustomerLevelId > 0)
			edMultiple.setText(""+dto.ksCustomerLevelItem.multiplier);
		else{
			edMultiple.setText("");
		}
		display(tvTotalMoney, getTotalMoney());
		display(tvTotalQuantity, getTotalQuantity());
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		display(tvTotalMoney, getTotalMoney());
		display(tvTotalQuantity, getTotalQuantity());
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	 /**
	 * Lay tong tien
	 * @author: Tuanlt11
	 * @return
	 * @return: double
	 * @throws:
	*/
	private double getTotalMoney(){
		double amount = 0;
		if(!StringUtil.isNullOrEmpty(edMultiple.getText())){
			try {
				long num = Long.parseLong(edMultiple.getText().toString());
				amount = StringUtil.getDouble(num * dto.ksLevelItem.amount, 1,
						GlobalInfo.getInstance().getSysNumRounding());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return amount;
	}

	 /**
	 * Lay tong san luong
	 * @author: Tuanlt11
	 * @return
	 * @return: double
	 * @throws:
	*/
	private long getTotalQuantity(){
		long quantity = 0;
		if(!StringUtil.isNullOrEmpty(edMultiple.getText())){
			try {
				long num = Long.parseLong(edMultiple.getText().toString());
				quantity = num * dto.ksLevelItem.quantity;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return quantity;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
//		if (listener != null && v == edMultiple && hasFocus) {
//			listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
//					edMultiple, null);
//		}
//		GlobalUtil.getInstance().showKeyboardUseToggle((GlobalBaseActivity)context);
	}
	/* (non-Javadoc)
	 * @see DMSTableRow#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		if (listener != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.SHOW_KEYBOARD_CUSTOM,
					v, null);
		}
	}
}
