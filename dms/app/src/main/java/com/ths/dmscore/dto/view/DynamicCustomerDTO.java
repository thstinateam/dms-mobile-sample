/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 * DynamicCustomerDTO.java
 * 
 * @author: hoanpd1
 * @version: 1.0
 * @since: 15:38:39 11-02-2015
 */
public class DynamicCustomerDTO {
	// id khach hang
	public long customerId;
	// ma khach hang
	public String customerCode;
	// ten khach hang
	public String customerName;
	// dia chi
	public String address;
	// trang thai khach hang
	public int statusCustomer;
	public int statusCustomerOfLine;
	// bien de biet row co duoc chon hay ko
	public boolean isCheck = false;
	
	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initFromCursor(Cursor c) {
		customerId = CursorUtil.getLong(c, "CUSTOMER_ID");
		customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
		customerName = CursorUtil.getString(c, "CUSTOMER_NAME");
		address = CursorUtil.getString(c, "ADDRESS");
		statusCustomer = CursorUtil.getInt(c, "STATUS_CUSTOMER");
		statusCustomerOfLine = CursorUtil.getInt(c, "STATUS_LINE_CUSTOMER");
	}
}
