/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control.filechooser;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.ths.dmscore.dto.view.filechooser.FileEvent;
import com.ths.dmscore.dto.view.filechooser.FileInfo;

public class FileAdapter extends ArrayAdapter<FileInfo> {
        List<FileInfo> objects;
        FileEvent fileChooseEvent;

        public FileAdapter(Context context, List<FileInfo> objects, FileEvent fileChooseEvent) {
            super(context, -1, objects);
            this.objects = objects;
            this.fileChooseEvent = fileChooseEvent;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FileViewHolder viewHolder;
            if (convertView == null){
                viewHolder = new FileViewHolder(getContext(), parent, this.fileChooseEvent);
                convertView = viewHolder.view;
                convertView.setTag(viewHolder);
            } else{
                viewHolder = (FileViewHolder) convertView.getTag();
            }
            viewHolder.render(objects.get(position));
            return convertView;
        }
    }