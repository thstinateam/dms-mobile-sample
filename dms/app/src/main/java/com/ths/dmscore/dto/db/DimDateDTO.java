package com.ths.dmscore.dto.db;

/**
 * Bang du lieu thoi gian
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class DimDateDTO extends AbstractTableDTO {

	// noi dung field
	private static final long serialVersionUID = 1L;
	public static final int DATE_TYPE_DAY = 1;
	public static final int DATE_TYPE_WEEK = 2;
	public static final int DATE_TYPE_MONTH = 3;
	public static final int DATE_TYPE_QUARTER = 4;

}
