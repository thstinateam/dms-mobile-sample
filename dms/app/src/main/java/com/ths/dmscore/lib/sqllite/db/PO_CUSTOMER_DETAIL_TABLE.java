package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PromotionProgrameDTO;
import com.ths.dmscore.dto.db.SaleOrderDetailDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.dto.db.SaleOrderPromoDetailDTO;
import com.ths.dmscore.dto.db.StockTotalDTO;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dms.R;

/**
 * Mo ta muc dich cua class
 *
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public class PO_CUSTOMER_DETAIL_TABLE extends ABSTRACT_TABLE {
	// id chi tiet don hang
	public static final String PO_CUSTOMER_DETAIL_ID = "PO_CUSTOMER_DETAIL_ID";
	// ma san pham
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// so luong LE
	public static final String QUANTITY = "QUANTITY";
	// gia lich su
	public static final String PRICE_ID = "PRICE_ID";
	// % khuyen mai
	public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// so tien khuyen mai
	public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	// 0: hang ban, 1: hang KM
	public static final String IS_FREE_ITEM = "IS_FREE_ITEM";
	// ma chuong trinh khuyen mai
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// Ma CTKM tham gia
	public static final String JOIN_PROGRAM_CODE = "JOIN_PROGRAM_CODE";
	// loai khuyen mai hay chuong trinh trung bay: 0 - CTKM tinh tu dong, 1 -
	// CTKM tinh manual, 2 - CTTB, 3: doi, 4: huy, 5: tra
	public static final String PROGRAM_TYPE = "PROGRAM_TYPE";
	// so tien
	public static final String AMOUNT = "AMOUNT";
	// id don dat hang
	public static final String PO_CUSTOMER_ID = "PO_CUSTOMER_ID";
	// gia ban
	public static final String PRICE = "PRICE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// ngay dat
	public static final String ORDER_DATE = "ORDER_DATE";
	// so luong KM max cua mat hang
	public static final String MAX_QUANTITY_FREE = "MAX_QUANTITY_FREE";
	// id NPP
	public static final String SHOP_ID = "SHOP_ID";
	// id nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// Thue VAT
	public static final String VAT = "VAT";
	// Trong luong
	public static final String TOTAL_WEIGHT = "TOTAL_WEIGHT";
	// ID hoa don VAT
	public static final String INVOICE_ID = "INVOICE_ID";
	// gia chua co VAT
	public static final String PRICE_NOT_VAT = "PRICE_NOT_VAT";
	//luu loai KM (vi du ZV01, 02...)
	public static final String PROGRAME_TYPE_CODE = "PROGRAME_TYPE_CODE";
	public static final String PRODUCT_GROUP_ID = "PRODUCT_GROUP_ID";
	public static final String GROUP_LEVEL_ID = "GROUP_LEVEL_ID";
	public static final String PAYING_ORDER = "PAYING_ORDER";

	public static final String PROMOTION_ORDER_NUMBER = "PROMOTION_ORDER_NUMBER";
	public static final String PACKAGE_PRICE = "PACKAGE_PRICE";
	public static final String PACKAGE_PRICE_NOT_VAT = "PACKAGE_PRICE_NOT_VAT";
	public static final String QUANTITY_RETAIL = "QUANTITY_RETAIL";
	public static final String QUANTITY_PACKAGE = "QUANTITY_PACKAGE";
	public static final String CAT_ID = "CAT_ID";
	public static final String CONVFACT = "CONVFACT";
	public static final String TABLE_NAME = "PO_CUSTOMER_DETAIL";

	/**
	 *
	 * tao va mo mot CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: SQLiteUtil
	 * @throws:
	 */
	public PO_CUSTOMER_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.columns = new String[] { PO_CUSTOMER_DETAIL_ID, PRODUCT_ID,
				QUANTITY, PRICE_ID, DISCOUNT_PERCENT, DISCOUNT_AMOUNT,
				IS_FREE_ITEM, PROGRAM_CODE, PROGRAM_TYPE, AMOUNT,
				PO_CUSTOMER_ID, PRICE, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, ORDER_DATE, MAX_QUANTITY_FREE, SHOP_ID, STAFF_ID,
				VAT, TOTAL_WEIGHT, INVOICE_ID, PRICE_NOT_VAT, PROGRAME_TYPE_CODE, PROMOTION_ORDER_NUMBER,
				PRODUCT_GROUP_ID, GROUP_LEVEL_ID, PAYING_ORDER, PACKAGE_PRICE, PACKAGE_PRICE_NOT_VAT,
				QUANTITY_RETAIL, QUANTITY_PACKAGE, CAT_ID, CONVFACT, SYN_STATE };
		this.tableName = TABLE_NAME;
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SaleOrderDetailDTO) dto);
		return insert(null, value);
	}

	/**
	 * Thay doi 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	public int update(SaleOrderDetailDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.salesOrderDetailId, "" + dto.synState };
		return update(value, PO_CUSTOMER_DETAIL_ID + " = ? AND " + SYN_STATE
				+ " = ? ", params);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long delete(AbstractTableDTO dto) {
		SaleOrderDetailDTO saleDTO = (SaleOrderDetailDTO) dto;
		String[] params = { "" + saleDTO.salesOrderDetailId,
				"" + saleDTO.synState };
		return delete(PO_CUSTOMER_DETAIL_ID + " = ? AND " + SYN_STATE + " = ? ",
				params);
	}

	/**
	 *
	 * Xoa cac chi tiet cu~
	 *
	 * @author: DungNX3
	 * @param orderID
	 * @return
	 * @return: int
	 * @throws:
	 */
	public int deleteAllDetailOfOrder(long orderID) {
		// String[] params = { "" + orderID };
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(orderID));

		return delete(PO_CUSTOMER_ID + " = ? ",
				params.toArray(new String[params.size()]));
	}

	private ContentValues initDataRow(SaleOrderDetailDTO dto) {
		ContentValues editedValues = new ContentValues();

		editedValues.put(PO_CUSTOMER_DETAIL_ID, dto.poDetailId);
		if (dto.productId > 0) {
			editedValues.put(PRODUCT_ID, dto.productId);
		} else {
			String temp = null;
			editedValues.put(PRODUCT_ID, temp);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(QUANTITY_RETAIL, dto.quantityRetail);
		editedValues.put(QUANTITY_PACKAGE, dto.quantityPackage);
		editedValues.put(CAT_ID, dto.catId);
		editedValues.put(PRICE_ID, dto.priceId);
		editedValues.put(DISCOUNT_PERCENT, dto.discountPercentage);
		editedValues.put(DISCOUNT_AMOUNT, dto.getDiscountAmount());

		editedValues.put(IS_FREE_ITEM, dto.isFreeItem);
//		if (!StringUtil.isNullOrEmpty(dto.programeCode) &&  (dto.hasPromotion || dto.isFreeItem == 1)){
//			editedValues.put(PROGRAM_CODE, dto.programeCode);
//		}
//		if (!StringUtil.isNullOrEmpty(dto.programeCode)){
//			editedValues.put(JOIN_PROGRAM_CODE, dto.programeCode);
//		}
//
//		if (dto.hasPromotion || dto.isFreeItem == 1) {
//			editedValues.put(PROGRAM_TYPE, dto.programeType);
//		}
		if(dto.isFreeItem == 1){
			if (!StringUtil.isNullOrEmpty(dto.programeCode)){
				editedValues.put(PROGRAM_CODE, dto.programeCode);
			}
			editedValues.put(PROGRAM_TYPE, dto.programeType);
			if (dto.promotionOrderNumber > 0) {
				editedValues.put(PROMOTION_ORDER_NUMBER, dto.promotionOrderNumber);
			}
			if (!StringUtil.isNullOrEmpty(dto.programeTypeCode)){
				editedValues.put(PROGRAME_TYPE_CODE, dto.programeTypeCode);
			}
			if(dto.maxQuantityFree > 0) {
				editedValues.put(MAX_QUANTITY_FREE, dto.maxQuantityFree);
			}

			if(dto.productGroupId > 0) {
				editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
			}

			if(dto.groupLevelId > 0) {
				editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);
			}

			if(dto.payingOrder > 0) {
				editedValues.put(PAYING_ORDER, dto.payingOrder);
			}
		}
		editedValues.put(AMOUNT, dto.getAmount());
		editedValues.put(PO_CUSTOMER_ID, dto.poId);
		editedValues.put(PRICE, dto.price);
		editedValues.put(PACKAGE_PRICE, dto.packagePrice);

		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(ORDER_DATE, dto.orderDate);
		if(dto.maxQuantityFree > 0) {
			editedValues.put(MAX_QUANTITY_FREE, dto.maxQuantityFree);
		}
		editedValues.put(SYN_STATE, dto.synState);
		editedValues.put(SHOP_ID, dto.shopId);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(VAT, dto.vat);
		editedValues.put(PRICE_NOT_VAT, dto.priceNotVat);
		editedValues.put(PACKAGE_PRICE_NOT_VAT, dto.packagePriceNotVat);
		editedValues.put(TOTAL_WEIGHT, dto.totalWeight);
		editedValues.put(CONVFACT, dto.convfact);
//		if (!StringUtil.isNullOrEmpty(dto.programeTypeCode) && (dto.hasPromotion || dto.isFreeItem == 1)){
//			editedValues.put(PROGRAME_TYPE_CODE, dto.programeTypeCode);
//		}
//		if (dto.promotionOrderNumber > 0) {
//			editedValues.put(PROMOTION_ORDER_NUMBER, dto.promotionOrderNumber);
//		}

//		if(dto.productGroupId > 0) {
//			editedValues.put(PRODUCT_GROUP_ID, dto.productGroupId);
//		}
//
//		if(dto.groupLevelId > 0) {
//			editedValues.put(GROUP_LEVEL_ID, dto.groupLevelId);
//		}
//
//		if(dto.payingOrder > 0) {
//			editedValues.put(PAYING_ORDER, dto.payingOrder);
//		}

		return editedValues;
	}

	/**
	 *
	 * Lay ds sp ban de sua don hang
	 *
	 * @author: DungNX
	 * @param poCustomerId
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws Exception
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 */
	public List<OrderDetailViewDTO> getListProductForEdit(String poCustomerId,
			String orderType, long customerId, String shopId, String staffId) throws Exception {
		Cursor c = null;
		String objectId = "0";
		int objectType = 1;
		if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_PRESALE)) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals(SALE_ORDER_TABLE.ORDER_TYPE_VANSALE)) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = staffId;
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    pr.PRODUCT_ID ,	");
		sqlObject.append("	    pr.PRODUCT_CODE,	");
		sqlObject.append("	    pr.PRODUCT_NAME,	");
		sqlObject.append("	    pod.PRICE,	");
		sqlObject.append("	    pod.PACKAGE_PRICE,	");
		sqlObject.append("	    pod.quantity as ACTUAL,	");
		sqlObject.append("	    pod.QUANTITY_RETAIL,	");
		sqlObject.append("	    pod.QUANTITY_PACKAGE,	");
		sqlObject.append("	    pod.MAX_QUANTITY_FREE,	");
		sqlObject.append("	    pod.AMOUNT,	");
		sqlObject.append("	    pod.DISCOUNT_PERCENT,	");
		sqlObject.append("	    pod.DISCOUNT_AMOUNT,	");
		sqlObject.append("	    pod.IS_FREE_ITEM,	");
		sqlObject.append("	    Group_concat(PCPM.PROGRAM_CODE) PROGRAM_CODE,	"); // CODE CTKM ma sp dat duoc
		sqlObject.append("	    Group_concat(PCPM.PROGRAM_ID) PROGRAM_ID,	"); // ID CTKM ma sp dat duoc
//		sqlObject.append("	    pod.PROGRAM_CODE,	");
//		sqlObject.append("	    pod.JOIN_PROGRAM_CODE,	");
		sqlObject.append("	    pod.PROGRAM_TYPE,	");
		sqlObject.append("	    pod.PRICE_ID,	");
		sqlObject.append("	    pod.CONVFACT,	");
		sqlObject.append("	    st.QUANTITY,	");
		sqlObject.append("	    st.AVAILABLE_QUANTITY ,	");
		sqlObject.append("	    pod.PRICE_NOT_VAT ,	");
		sqlObject.append("	    pod.PACKAGE_PRICE_NOT_VAT ,	");
		sqlObject.append("	    pod.TOTAL_WEIGHT,	");
		sqlObject.append("	    pod.VAT,	");
		sqlObject.append("	    pr.GROSS_WEIGHT,	");
		sqlObject.append("	    pod.PROGRAME_TYPE_CODE,	");
		sqlObject.append("	    pod.PO_CUSTOMER_DETAIL_ID	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    po_customer_detail pod	");
		sqlObject.append("	LEFT JOIN (SELECT	");
		sqlObject.append("	    		PP.PROMOTION_PROGRAM_ID PROGRAM_ID,	");
		sqlObject.append("	    		PCPM.PROGRAM_CODE,	");
		sqlObject.append("	    		PCPM.PO_CUSTOMER_DETAIL_ID	");
		sqlObject.append("			   FROM	");
		sqlObject.append("	   			  PROMOTION_PROGRAM PP,	");
		sqlObject.append("	  			  PO_CUSTOMER_PROMO_MAP PCPM	");
		sqlObject.append("			   WHERE	");
		sqlObject.append("	    		  PCPM.PROGRAM_CODE = PP.PROMOTION_PROGRAM_CODE	");
		sqlObject.append("	    		) PCPM ON PCPM.PO_CUSTOMER_DETAIL_ID = pod.PO_CUSTOMER_DETAIL_ID,	");
		sqlObject.append("	    po_customer po,	");
		sqlObject.append("	    product pr	");
		sqlObject.append("	LEFT OUTER JOIN	");
		sqlObject.append("	    (	");
		//doi cau lay ton kho (nhiu kho)
		sqlObject.append("       SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) AVAILABLE_QUANTITY, st.PRODUCT_ID ");
		sqlObject.append("       FROM stock_total st ");
		sqlObject.append("       LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sqlObject.append("       WHERE 1=1 ");
		sqlObject.append("        	AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		paramsObject.add(shopId);
		sqlObject.append("        	AND st.object_id = ? AND st.status = 1 ");
		paramsObject.add(objectId);
		sqlObject.append("       		AND st.object_type = ? ");
		paramsObject.add(String.valueOf(objectType));
		sqlObject.append("       group by st.product_id ");
		sqlObject.append("	    ) st	");
		sqlObject.append("	        ON st.product_id = pr.product_id	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND po.[PO_CUSTOMER_ID] = pod.[PO_CUSTOMER_ID]	");
		sqlObject.append("	    AND pod.product_id = pr.product_id	");
		sqlObject.append("	    AND po.[PO_CUSTOMER_ID] = ?	");
		paramsObject.add(poCustomerId);
		sqlObject.append("	    AND pod.is_free_item = 0	");
		sqlObject.append("	GROUP BY  pod.PO_CUSTOMER_DETAIL_ID ");
		ArrayList<OrderDetailViewDTO> v = new ArrayList<OrderDetailViewDTO>();

		try {
			c = rawQueries(sqlObject.toString(),
					paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					OrderDetailViewDTO orderJoinTableDTO;

					do {
						orderJoinTableDTO = initSaleOrderDetailObjectFromGetProductStatement(c);
						getPromoDetailOfOrderDetail(orderJoinTableDTO);
						// lay chi tiet tra thuong cua keyshop
						getPromoDetailKeyShopOfOrderDetail(orderJoinTableDTO,customerId);
						//tam thi b check focus
						//checkFocusProduct(orderJoinTableDTO);
						v.add(orderJoinTableDTO);

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return v;
	}

	/**
	 *
	 * Mo ta chuc nang cua ham
	 *
	 * @author: DungNX
	 * @param c
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	private OrderDetailViewDTO initSaleOrderDetailObjectFromGetProductStatement(
			Cursor c) {
		SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
		OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();

		saleOrderDTO.salesOrderDetailId = CursorUtil.getLong(c, PO_CUSTOMER_DETAIL_ID);
		saleOrderDTO.productId = CursorUtil.getInt(c, PRODUCT_ID);
		saleOrderDTO.price = CursorUtil.getDouble(c, PRICE);
		saleOrderDTO.packagePrice = CursorUtil.getDouble(c, PACKAGE_PRICE);
		saleOrderDTO.priceId = CursorUtil.getLong(c, PRICE_ID);
		saleOrderDTO.quantity = CursorUtil.getInt(c, "ACTUAL");
		saleOrderDTO.quantityRetail = CursorUtil.getLong(c, QUANTITY_RETAIL);
		saleOrderDTO.quantityPackage = CursorUtil.getLong(c, QUANTITY_PACKAGE);
		saleOrderDTO.catId = CursorUtil.getInt(c, CAT_ID);
		saleOrderDTO.maxQuantityFree = CursorUtil.getInt(c, MAX_QUANTITY_FREE);
		saleOrderDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		saleOrderDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.discountPercentage = CursorUtil.getDouble(c, DISCOUNT_PERCENT);
		saleOrderDTO.isFreeItem = CursorUtil.getInt(c, IS_FREE_ITEM);

		saleOrderDTO.programeCode = CursorUtil.getString(c, PROGRAM_CODE);
		saleOrderDTO.programeIdBuyView = CursorUtil.getString(c, "PROGRAM_ID");

//		saleOrderDTO.programeCode = CursorUtil.getString(c, JOIN_PROGRAM_CODE);
		saleOrderDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		saleOrderDTO.synState = ABSTRACT_TABLE.CREATED_STATUS;
		saleOrderDTO.priceNotVat = CursorUtil.getDouble(c, PRICE_NOT_VAT);
		saleOrderDTO.packagePriceNotVat = CursorUtil.getDouble(c, PACKAGE_PRICE_NOT_VAT);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		saleOrderDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		detailViewDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		saleOrderDTO.vat = CursorUtil.getDouble(c, "VAT");
		detailViewDTO.grossWeight = CursorUtil.getDouble(c, "GROSS_WEIGHT");

		detailViewDTO.productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		detailViewDTO.productName = CursorUtil.getString(c, "PRODUCT_NAME");
		detailViewDTO.stock = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		detailViewDTO.stockActual = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.QUANTITY);
		saleOrderDTO.programeTypeCode = CursorUtil.getString(c, PROGRAME_TYPE_CODE);

		detailViewDTO.orderDetailDTO = saleOrderDTO;
		detailViewDTO.quantity = detailViewDTO.quantityStr();
		return detailViewDTO;
	}

	/**
	 * Kiem tra sp co thuoc chuong trinh trong tam hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderJoinTableDTO
	 * @return: void
	 * @throws:
	 */
	private void checkFocusProduct(OrderDetailViewDTO orderJoinTableDTO)
			throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(GlobalInfo.getInstance().getProfile()
				.getUserData().getInheritId()));
		params.add(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId());
		params.add(String.valueOf(orderJoinTableDTO.orderDetailDTO.productId));

		sql.append(" SELECT distinct fpd.product_id AS product_id ");
		sql.append(" FROM focus_programe fp_, focus_programe_detail fpd, focus_shop_map map , STAFF AS STF ");
		sql.append(" WHERE fp_.status = 1 ");
		sql.append(" AND IFNULL(DATE(fp_.from_date) <= DATE('now','localtime'),0) ");
		sql.append(" AND (IFNULL(DATE(fp_.TO_DATE) >= DATE('now','localtime'),0) ");
		sql.append(" OR DATE(fp_.TO_DATE) IS NULL OR fp_.TO_DATE = '') ");
		sql.append(" AND fp_.focus_program_id = MAP.focus_program_id ");
		sql.append(" AND (map.status = 1 OR map.status = 'true') ");
		sql.append(" AND MAP.FOCUS_SHOP_MAP_ID = fpd.FOCUS_SHOP_MAP_ID ");
		sql.append(" AND MAP.STAFF_TYPE = STF.STAFF_TYPE ");
		sql.append(" AND STF.STAFF_ID = ?");
		// sql.append(GlobalInfo.getInstance().getProfile().getUserData().id);
		sql.append(" AND map.shop_id = ?");
		// sql.append(GlobalInfo.getInstance().getProfile().getUserData().shopId);
		sql.append(" AND fpd.product_id = ?");
		// sql.append(orderJoinTableDTO.orderDetailDTO.productId);

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(),
					params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					int product_id = CursorUtil.getInt(c, "product_id");
					if (product_id > 0) {
						orderJoinTableDTO.isFocus = 1;
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}
	}

	/**
	 * Kiem tra sp co thuoc chuong trinh trong tam hay ko?
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderDetailDTO
	 * @return: void
	 * @throws:
	 */

	private void getPromoDetailOfOrderDetail(OrderDetailViewDTO orderDetailDTO) throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();

		sql.append(" SELECT *,PP.PROMOTION_PROGRAM_ID PROGRAM_ID ");
		sql.append(" FROM po_customer_promo_detail pcpd, PROMOTION_PROGRAM PP  ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND pcpd.PO_CUSTOMER_DETAIL_ID = ? ");
		params.add(String.valueOf(orderDetailDTO.orderDetailDTO.salesOrderDetailId));
		sql.append(" AND pcpd.PROGRAM_CODE = PP.PROMOTION_PROGRAM_CODE ");
		sql.append(" AND pcpd.PROGRAM_TYPE <> ? ");
		params.add(""+ PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleOrderPromoDetailDTO promoDetailDTO = new SaleOrderPromoDetailDTO();
						promoDetailDTO.parseDataFromCursor(c);
						orderDetailDTO.listPromoDetail.add(promoDetailDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
			}
		}
	}

	/**
	 * Lay ds san pham khuyen mai cua 1 don hang PO
	 *
	 * @author: DungNX
	 * @param poCustomerId
	 *            : id cua don hang
	 * @param customerId
	 * @param staffId
	 * @param shopId
	 * @param string
	 * @return
	 * @return: List<OrderDetailViewDTO>
	 * @throws:
	 */

	public List<OrderDetailViewDTO> getPromotionProductsPO(String poCustomerId,
			String orderType, long customerId, String shopId, String staffId) throws Exception {
		Cursor c = null;
//		ArrayList<String> params = new ArrayList<String>();
		String objectId = "0";
		int objectType = 1;
		if (orderType.equals("IN")) {// presale
			objectType = StockTotalDTO.TYPE_SHOP;
			objectId = shopId;
		} else if (orderType.equals("SO")) {// vansale
			objectType = StockTotalDTO.TYPE_VANSALE;
			objectId = staffId;
		} else {
			objectType = StockTotalDTO.TYPE_CUSTOMER;
		}
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    pr.PRODUCT_CODE,	");
		sqlObject.append("	    pr.PRODUCT_NAME,	");
		sqlObject.append("	    pod.PRICE,	");
		sqlObject.append("	    pod.PACKAGE_PRICE,	");
		sqlObject.append("	    pod.PRICE_ID,	");
		sqlObject.append("	    pod.PRICE_NOT_VAT,	");
		sqlObject.append("	    pod.PACKAGE_PRICE_NOT_VAT,	");
		sqlObject.append("	    pod.TOTAL_WEIGHT,	");
		sqlObject.append("	    pod.MAX_QUANTITY_FREE,	");
		sqlObject.append("	    pod.quantity AS ACTUAL ,	");
		sqlObject.append("	    pod.QUANTITY_RETAIL,	");
		sqlObject.append("	    pod.QUANTITY_PACKAGE,	");
		sqlObject.append("	    pod.DISCOUNT_AMOUNT,	");
		sqlObject.append("	    pod.IS_FREE_ITEM,	");
		sqlObject.append("	    Case pod.MAX_QUANTITY_FREE	");
		sqlObject.append("	        WHEN 0 Then 'KM tien'	");
		sqlObject.append("	        ELSE 'KM hang'	");
		sqlObject.append("	    END as typeName ,	");
		sqlObject.append("	    pod.PROGRAM_CODE,	");
//		sqlObject.append("	    pod.JOIN_PROGRAM_CODE,	");
		sqlObject.append("	    pp.PROMOTION_PROGRAM_ID PROGRAM_ID,	");
		sqlObject.append("	    ks.ks_id KS_PROGRAM_ID,	");
		sqlObject.append("	    pod.PROGRAM_TYPE,	");
		sqlObject.append("	    pr.PRODUCT_ID,	");
		sqlObject.append("	    st.QUANTITY,	");
		sqlObject.append("	    st.AVAILABLE_QUANTITY   ,	");
		sqlObject.append("	    pod.VAT,	");
		sqlObject.append("	    pr.GROSS_WEIGHT,	");
		sqlObject.append("	    pod.PRODUCT_GROUP_ID,	");
		sqlObject.append("	    pod.GROUP_LEVEL_ID,	");
		sqlObject.append("	    pod.PAYING_ORDER,	");
		sqlObject.append("	    pod.DISCOUNT_PERCENT,	");
		sqlObject.append("	    pod.PROGRAME_TYPE_CODE,	");
		sqlObject.append("	    pod.CONVFACT CONVFACT	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    po_customer_detail pod	");
		sqlObject.append("	LEFT JOIN PROMOTION_PROGRAM PP	");
		sqlObject.append("	    	ON PP.PROMOTION_PROGRAM_CODE = pod.PROGRAM_CODE	");
		sqlObject.append("	LEFT JOIN KS KS	");
		sqlObject.append("	    	ON KS.KS_CODE = pod.PROGRAM_CODE AND pod.PROGRAM_TYPE = ?	");
		paramsObject.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);
//		sqlObject.append("	    po_customer po	");
		sqlObject.append("	LEFT OUTER JOIN	");
		sqlObject.append("	    product pr	");
		sqlObject.append("	        ON pod.product_id = pr.product_id	");

		//begin doi cau lay ton kho (nhiu kho)
		sqlObject.append("       LEFT OUTER JOIN (SELECT SUM(st.available_quantity) AVAILABLE_QUANTITY, SUM(st.quantity) QUANTITY, st.PRODUCT_ID ");
		sqlObject.append("                  FROM stock_total st ");
		sqlObject.append("                  LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sqlObject.append("                  WHERE 1=1 ");
		sqlObject.append("        				AND ((st.shop_id = ? AND st.object_type = 2) OR (st.object_type = 1 AND ifnull(wh.warehouse_id, '') <> '')) ");
		paramsObject.add(shopId);
		sqlObject.append("                  	AND st.object_id = ? AND st.status = 1 ");
		paramsObject.add(objectId);
		sqlObject.append("                  	AND st.object_type = ? ");
		paramsObject.add(String.valueOf(objectType));
		sqlObject.append("               	group by st.product_id) st ");
		//end doi cau lay ton kho (nhiu kho)

		sqlObject.append("	        ON st.product_id = pr.product_id	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1 = 1	");
		sqlObject.append("	    AND pod.[PO_CUSTOMER_ID] = ?	");
//		sqlObject.append("	    AND po.FROM_SALE_ORDER_ID = ?	");
		paramsObject.add(poCustomerId);
		sqlObject.append("	    AND pod.is_free_item = 1	");
		sqlObject.append("	ORDER BY ks.KS_CODE,pr.order_index	");

		ArrayList<OrderDetailViewDTO> v = new ArrayList<OrderDetailViewDTO>();
		KEYSHOP_TABLE ksTable = new KEYSHOP_TABLE(mDB);
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					OrderDetailViewDTO orderJoinTableDTO;

					do {
						orderJoinTableDTO = initPromotionProductFromCursor(c);
						// neu la CT key shop thi cap nhat lai programeId
						if(orderJoinTableDTO.orderDetailDTO.programeType == PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP ){
							orderJoinTableDTO.orderDetailDTO.programeId = CursorUtil.getLong(c, "KS_PROGRAM_ID");
						}
						ksTable.getNumProductRewardByCustomer(
								orderJoinTableDTO,
								orderJoinTableDTO.orderDetailDTO.programeId,
								orderJoinTableDTO.orderDetailDTO.productId,customerId);
						v.add(orderJoinTableDTO);

					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return v;
	}

	/**
	 *
	 * Khoi tao doi tuong chi tiet Don hang da.ng KM
	 *
	 * @author: Nguyen Thanh Dung
	 * @param c
	 * @return
	 * @return: OrderDetailViewDTO
	 * @throws:
	 */
	private OrderDetailViewDTO initPromotionProductFromCursor(Cursor c) {
		SaleOrderDetailDTO saleOrderDTO = new SaleOrderDetailDTO();
		OrderDetailViewDTO detailViewDTO = new OrderDetailViewDTO();

		saleOrderDTO.productId = CursorUtil.getInt(c, PRODUCT_ID);
		saleOrderDTO.price = CursorUtil.getDouble(c, PRICE);
		saleOrderDTO.packagePrice = CursorUtil.getDouble(c, PACKAGE_PRICE);
		saleOrderDTO.priceId = CursorUtil.getLong(c, PRICE_ID);
		saleOrderDTO.quantity = CursorUtil.getInt(c, "ACTUAL");
		saleOrderDTO.quantityRetail = CursorUtil.getLong(c, QUANTITY_RETAIL);
		saleOrderDTO.quantityPackage = CursorUtil.getLong(c, QUANTITY_PACKAGE);
		saleOrderDTO.catId = CursorUtil.getInt(c, CAT_ID);
		saleOrderDTO.maxQuantityFree = CursorUtil.getInt(c, MAX_QUANTITY_FREE);
		saleOrderDTO.setAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.setDiscountAmount(CursorUtil.getDouble(c, DISCOUNT_AMOUNT));
		saleOrderDTO.discountPercentage = CursorUtil.getDouble(c, DISCOUNT_PERCENT);
//		saleOrderDTO.programeCode = CursorUtil.getString(c, JOIN_PROGRAM_CODE);
		saleOrderDTO.programeCode = CursorUtil.getString(c, PROGRAM_CODE);
		saleOrderDTO.programeId = CursorUtil.getLong(c, "PROGRAM_ID");
		saleOrderDTO.programeType = CursorUtil.getInt(c, PROGRAM_TYPE);
		saleOrderDTO.isFreeItem = CursorUtil.getInt(c, "IS_FREE_ITEM");
		saleOrderDTO.priceNotVat = CursorUtil.getDouble(c, PRICE_NOT_VAT);
		saleOrderDTO.packagePriceNotVat = CursorUtil.getDouble(c, PACKAGE_PRICE_NOT_VAT);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		saleOrderDTO.vat = CursorUtil.getDouble(c, "VAT");
		detailViewDTO.grossWeight = CursorUtil.getDouble(c, "GROSS_WEIGHT");
		detailViewDTO.productCode = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_CODE);
		detailViewDTO.productName = CursorUtil.getString(c, PRODUCT_TABLE.PRODUCT_NAME);
		saleOrderDTO.convfact = CursorUtil.getInt(c, CONVFACT);
		detailViewDTO.convfact = CursorUtil.getInt(c, CONVFACT);

		if (detailViewDTO.productName == null
				|| detailViewDTO.productName.length() == 0) {
			detailViewDTO.productName = StringUtil
					.getString(R.string.TEXT_KM_CK_GET);
		}
		detailViewDTO.stock = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.AVAILABLE_QUANTITY);
		detailViewDTO.stockActual = CursorUtil.getLong(c, STOCK_TOTAL_TABLE.QUANTITY);
		saleOrderDTO.programeTypeCode = CursorUtil.getString(c, SALES_ORDER_DETAIL_TABLE.PROGRAME_TYPE_CODE);
		saleOrderDTO.productGroupId = CursorUtil.getLong(c, SALES_ORDER_DETAIL_TABLE.PRODUCT_GROUP_ID);
		saleOrderDTO.groupLevelId = CursorUtil.getLong(c, SALES_ORDER_DETAIL_TABLE.GROUP_LEVEL_ID);
		saleOrderDTO.payingOrder = CursorUtil.getInt(c, SALES_ORDER_DETAIL_TABLE.PAYING_ORDER);

		//Get type of promotion for render
		if(saleOrderDTO.maxQuantityFree > 0) {
			detailViewDTO.type = OrderDetailViewDTO.FREE_PRODUCT;
		} else if (saleOrderDTO.discountPercentage > 0) {
			detailViewDTO.type = OrderDetailViewDTO.FREE_PERCENT;
		} else if (saleOrderDTO.maxAmountFree > 0) {
			detailViewDTO.type = OrderDetailViewDTO.FREE_PRICE;
		}

		detailViewDTO.orderDetailDTO = saleOrderDTO;
		detailViewDTO.quantity = detailViewDTO.quantityStr();
		return detailViewDTO;
	}

	/**
	 * lay POID tu saleorderid
	 *
	 * @author: Nguyen Thanh Dung
	 * @param orderJoinTableDTO
	 * @return: void
	 * @throws:
	 */
	public long getPOIDFromSaleOrderID(long saleOrderID) throws Exception {
		long poID = -1;
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    po_customer_id	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    po_customer	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    FROM_SALE_ORDER_ID = ?	");
		paramsObject.add(String.valueOf(saleOrderID));

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);

			if (c != null) {
				if (c.moveToFirst()) {
					poID = CursorUtil.getInt(c, "po_customer_id");
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.w("",	 e.toString());
			}
		}

		return poID;
	}

	 /**
	 * Lay thong tin tra thuong keyshop cho don goc
	 * @author: Tuanlt11
	 * @param orderDetailDTO
	 * @param customerId
	 * @throws Exception
	 * @return: void
	 * @throws:
	*/
	private void getPromoDetailKeyShopOfOrderDetail(OrderDetailViewDTO orderDetailDTO, long customerId) throws Exception {
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();

		sql.append(" SELECT *, KS.KS_ID PROGRAM_ID  ");
		sql.append(" FROM po_customer_promo_detail pcpd,  ks  ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND pcpd.PO_CUSTOMER_DETAIL_ID = ? ");
		params.add(String.valueOf(orderDetailDTO.orderDetailDTO.salesOrderDetailId));
		sql.append(" AND pcpd.PROGRAM_CODE = ks.KS_CODE ");
		sql.append(" AND pcpd.PROGRAM_TYPE = ? ");
		params.add(""+PromotionProgrameDTO.PROGRAME_PROMOTION_KEYSHOP);

		Cursor c = null;
		try {
			c = rawQuery(sql.toString(), params.toArray(new String[params.size()]));

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						SaleOrderPromoDetailDTO promoDetailDTO = new SaleOrderPromoDetailDTO();
						promoDetailDTO.parseDataFromCursor(c);
						orderDetailDTO.listPromoDetail.add(promoDetailDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				MyLog.e("getPromoDetailOfOrderDetail: ", e.toString());
			}
		}
	}
}
