package com.ths.dmscore.dto.syndata;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponObject {
	@JsonProperty("response")
	private SynDataDTO response;
	
	@JsonProperty("errorCode")
	private Integer errorCode;
	
	@JsonProperty("errorMessage")
	private String errorMessage;

	public SynDataDTO getResponse() {
		return response;
	}

	public void setSynDataDTO(SynDataDTO response) {
		this.response = response;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
