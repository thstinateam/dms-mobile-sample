/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

/**
 * ChooseParametersBeforeExportReportDTO.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  16:17:58 20-05-2015
 */
public class ChooseParametersBeforeExportReportDTO {
	// number fullDate sale plan
	public int numDaySalePlan = 0;
	// number fullDate sold plan
	public int numDaySoldPlan = 0;
	// progress sale
	public double progessSold = 0;
}
