/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.lib.sqllite.db.EQUIP_STATISTIC_RECORD_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;

public class EquipStatisticRecordDTO extends AbstractTableDTO {
	public static final long OBJECT_TYPE_ALL = 0;
	public static final long OBJECT_TYPE_GROUP_EQUIPMENT = 1;
	public static final long OBJECT_TYPE_U_KE = 2;
	public static final long OBJECT_TYPE_EQUIPMENT = 3;
	private static final long serialVersionUID = 1L;
	private long equipStatisticRecordId;
	private String code;
	private String name;
	private String fromDate;
	private String toDate;
	public boolean isInventory;
	private int totalItem;
	// loai kiem ke: 1 nhom thiet bi, 2 u ke, 3: thiet bi cu the
	private int objectType;
	// loai kiem ke: 1 tuyen, 2 so lan
	private int type;

	// danh sach thiet bi can duoc kiem ke trong dot
	public ArrayList<EquipInventoryDTO> listEquipInventory = new ArrayList<EquipInventoryDTO>();
	public double shopDistance;

	public EquipStatisticRecordDTO() {
		setEquipStatisticRecordId(0);
		setCode("");
		setName("");
		setFromDate("");
		setToDate("");
		isInventory = false;
		objectType = 0;
		type = 0;
	}

	/**
	 * init du lieu dot kiem ke
	 * 
	 * @author: hoanpd1
	 * @since: 09:59:15 16-12-2014
	 * @return: void
	 * @throws:
	 * @param c
	 * @throws Exception
	 */
	public void initPeriodInventoryInfo(Cursor c) throws Exception {
		setEquipStatisticRecordId(CursorUtil.getInt(c, EQUIP_STATISTIC_RECORD_TABLE.EQUIP_STATISTIC_RECORD_ID));
		setCode(CursorUtil.getString(c, EQUIP_STATISTIC_RECORD_TABLE.CODE));
		setName(CursorUtil.getString(c, EQUIP_STATISTIC_RECORD_TABLE.NAME));
		setFromDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, EQUIP_STATISTIC_RECORD_TABLE.FROM_DATE)));
		setToDate(DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, EQUIP_STATISTIC_RECORD_TABLE.TO_DATE)));
		objectType = CursorUtil.getInt(c, "OBJECT_TYPE");
		type = CursorUtil.getInt(c, "TYPE");
	}

	public long getEquipStatisticRecordId() {
		return equipStatisticRecordId;
	}

	public void setEquipStatisticRecordId(long equipStatisticRecordId) {
		this.equipStatisticRecordId = equipStatisticRecordId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public int getTotalItem() {
		return totalItem;
	}

	public void setTotalItem(int totalItem) {
		this.totalItem = totalItem;
	}

	public int getObjectType() {
		return objectType;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}

	public int getEquipStatisticRecordType() {
		return type;
	}

	public void setEquipStatisticRecordType(int type) {
		this.type = type;
	}

}
