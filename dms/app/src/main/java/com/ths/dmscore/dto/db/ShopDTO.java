/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;

/**
 *  Luu thong tin NPP
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ShopDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -3072845404845766903L;
	// id npp
	public int shopId ;
	// ma NPP
	public String shopCode ;
	// ten NPP
	public String shopName ; 
	// id NPP cha
	public int parentShopId ; 
	// duong
	public String street ; 
	// phuong
	public String ward ;
	// quan/huyen
	public String district ;
	// tinh
	public String province ;
	// ma vung
	public String areaCode ;
	// nuoc
	public String country ;
	// sdt ban
	public String phone ;
	// di dong
	public String telephone ;
	// loai NPP, 1: NPP(default) 2: Vinamil
	public int shopType ; 
	// nguoi tao
	public String createUser ;
	// nguoi cap nhat
	public String updateUser ;
	// ngay tao
	public String createDate ;
	// ngay cap nhat
	public String updateDate ;
	// loai kenh
	public int channelTypeId ;
	// email giam sat NPP
	public String email ;
	// duong dan den NPP
	public String shopPath ;
	// khoang cach cho phep dat hang
	public double distanceOrder;
	
	public double shopLat = -1;
	public double shopLng = -1;
	
	public ShopDTO(){
		super(TableType.SHOP_TABLE);
	}
	
	/**
	* Parse du lieu shop 
	* @author: BangHN
	* @param c
	* @return: void
	*/
	public void parseDataFromCusor(Cursor c){
		shopId = CursorUtil.getInt(c, "SHOP_ID");
		shopCode = CursorUtil.getString(c, "SHOP_CODE");
		shopName = CursorUtil.getString(c, "SHOP_NAME");

	}
}
