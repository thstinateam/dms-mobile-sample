package com.ths.dmscore.view.sale.order;

/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
import java.io.Serializable;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap.PriControl;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Row mat hang ban
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class OrderPromotionKeyShopRow extends DMSTableRow implements  Serializable, OnClickListener {
	private static final long serialVersionUID = 1L;
	// keyshop
	TextView tvKeyShop;
	// ma mat hang
	TextView tvProductCode;
	// ten mat hang
	TextView tvProductName;
	// so luong ton kho dang format thung/ hop
	TextView tvRemaindStockFormat;
	// so luong ton kho dang format thung/ hop Thuc te
	TextView tvRemaindStockFormatActual;
	// quantity hoac amount
	TextView tvQuantityOrAmount;
	// tong tien phai tra
	TextView tvTotalReward;
	// tong tien da tra
	TextView tvTotalRewardDone;
	int typeActionRow = -1;
	// check when input money > allow value -> settext -> call afterTextChange again
	boolean hasResetAmount = false;
	// dto row
	private OrderDetailViewDTO rowDTO;

	public OrderPromotionKeyShopRow(Context context, boolean isShowPrice, VinamilkTableListener listener) {
		super(context, R.layout.layout_order_promotion_keyshop_row);
		setListener(listener);
		tvProductCode = (TextView)PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvProductCode, PriControl.NVBH_BANHANG_DONHANG_CHITIETSP);
		PriUtils.getInstance().setOnClickListener(tvProductCode, this);
		tvRemaindStockFormat = (TextView) findViewById(R.id.tvRemaindStockFormat);
		tvRemaindStockFormatActual = (TextView) findViewById(R.id.tvRemaindStockFormatActual);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvTotalReward = (TextView) findViewById(R.id.tvTotalRewardKeyShop);
		tvTotalRewardDone = (TextView) findViewById(R.id.tvTotalRewardKeyShopDone);
		tvQuantityOrAmount = (TextView) findViewById(R.id.tvQuantityOrAmount);
		tvKeyShop = (TextView)PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvKeyShop, PriControl.NVBH_BANHANG_DONHANG_CHITIET_KEYSHOP);
		PriUtils.getInstance().setOnClickListener(tvKeyShop, this);
	}

	 /**
	 * update tra thuong keyshop
	 * @author: Tuanlt11
	 * @param dto
	 * @param isCheckStockTotal
	 * @return: void
	 * @throws:
	*/
	public void updatePromotionKeyShop(OrderDetailViewDTO dto,
			boolean isCheckStockTotal) {
		rowDTO = dto;
		tvKeyShop.setText(dto.orderDetailDTO.programeCode);
		if (StringUtil.isNullOrEmpty(dto.productName)) {
			tvProductName.setText(dto.typeName);
		}
		if(dto.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT){
			tvProductCode.setText(dto.productCode);
			tvProductName.setText(dto.productName);
			tvRemaindStockFormat.setText(dto.remaindStockFormat);
			tvRemaindStockFormatActual.setText(dto.remaindStockFormatActual);
			tvQuantityOrAmount.setText(GlobalUtil.formatNumberProductFlowConvfact((long)dto.orderDetailDTO.quantity, dto.convfact));
			tvTotalRewardDone.setText(GlobalUtil.formatNumberProductFlowConvfact(dto.keyshopProductNumDone, dto.convfact));
			tvTotalReward.setText(GlobalUtil.formatNumberProductFlowConvfact(dto.keyshopProductNum, dto.convfact));
		}else{
			// HaiTC: hien thi so luong ton kho dang format thung / hop
			tvProductCode.setText("");
			tvProductName.setText("");
			tvRemaindStockFormat.setText("");
			tvRemaindStockFormatActual.setText("");
			display(tvQuantityOrAmount, dto.orderDetailDTO.getDiscountAmount());
			display(tvTotalReward, dto.keyshopRewardMoney);
			display(tvTotalRewardDone, dto.keyshopRewardMoneyDone);
		}


		// Cap nhat mau khi kiem tra ton kho
		if (isCheckStockTotal) {
			checkStockTotal(dto);
		}
	}

	/**
	 * Kiem tra ton kho
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return: void
	 * @throws:
	 */

	public void checkStockTotal(OrderDetailViewDTO dto) {
		if (dto.orderDetailDTO.quantity > 0) {// San pham KM hang, SP KM tien
												// thi quantity = 0
			if (dto.stock <= 0) {
				updateRowWithColor(ImageUtil.getColor(R.color.RED));
			} else if (dto.totalOrderQuantity != null && dto.totalOrderQuantity.orderDetailDTO.quantity > dto.stock) {
				updateRowWithColor(ImageUtil.getColor(R.color.OGRANGE));
			}
		}
	}

	/**
	 *
	 * Cap nhat full mau cho 1 row
	 *
	 * @author: Nguyen Thanh Dung
	 * @param color
	 * @return: void
	 * @throws:
	 */
	private void updateRowWithColor(int color) {
		GlobalUtil.setTextColor(color,
				tvKeyShop, tvProductCode, tvProductName,
				tvRemaindStockFormat, tvRemaindStockFormatActual,tvTotalRewardDone,tvTotalReward,tvQuantityOrAmount);
	}

	@Override
	public void onClick(View v) {
		if (v == tvProductCode && listener != null && rowDTO.promotionType == OrderDetailViewDTO.PROMOTION_KEYSHOP_PRODUCT) {
			listener.handleVinamilkTableRowEvent(OrderView.ACTION_VIEW_PRODUCT, this, rowDTO);
		}else if (v == tvKeyShop && listener != null) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GET_PROMOTION_DETAIL_KEYSHOP, this, rowDTO.orderDetailDTO.programeId);
		}else{
			super.onClick(v);
		}
	}
}
