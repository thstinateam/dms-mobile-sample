package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO cho man hinh bao cao KPI cua gs
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPISupervisorViewDTO {
	// ds chua thong tin KPI cho tung NV
	public ArrayList<ReportKPISupervisorItemDTO> lstReportKPISuperVisorItem = new ArrayList<ReportKPISupervisorItemDTO>();
	// ngay lam viec trong thang
	public int monthSalePlan;
	// ngay da lam viec trong thang
	public int soldSalePlan;
	// percent ngay lam viec trong thang
	public double perSalePlan;
	// header cho row
	public ArrayList<ReportKPIItemDTO> lstHeader = new ArrayList<ReportKPIItemDTO>();

}
