/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.supervisor.keyshop;

import android.content.Context;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dms.R;

/**
 * KeyShopLevelRewardRow.java
 * @author: yennth16
 * @version: 1.0 
 * @since:  10:31:24 21-07-2015
 */
public class KeyShopLevelRewardRow extends DMSTableRow {
	public TextView tvStt;
	public TextView tvKeyshopLevelCode;
	public TextView tvKeyshopLevelName;
	public TextView tvAmount;
	public TextView tvQuantity;

	public KeyShopLevelRewardRow(Context context) {
		super(context, R.layout.layout_key_shop_level_reward_row);
		setOnClickListener(this);
		tvStt = (TextView) findViewById(R.id.tvStt);
		tvKeyshopLevelCode = (TextView) findViewById(R.id.tvKeyshopLevelCode);
		tvKeyshopLevelName = (TextView) findViewById(R.id.tvKeyshopLevelName);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
		tvQuantity = (TextView) findViewById(R.id.tvQuantity);
	}

	/**
	 * render
	 * @author: yennth16
	 * @since: 15:42:49 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param historyItem
	 */
	public void render(KSLevelDTO item, int position) {
		tvStt.setText("" + position);
		tvKeyshopLevelCode.setText(item.ksLevelCode);
		tvKeyshopLevelName.setText(item.name);
		tvQuantity.setText(item.productNum);
		tvAmount.setText(item.productNumDone);
	}
}