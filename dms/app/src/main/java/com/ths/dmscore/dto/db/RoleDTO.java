/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

/**
 *  Luu cac quyen trong he thong
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class RoleDTO extends AbstractTableDTO {
	// id quyen
	public int roleId ;
	// ma quyen
	public String roleCode ; 
	// trang thai 1: hieu luc 0: khong hieu luc
	public int status ;
	
	public RoleDTO(){
		super(TableType.ROLE_TABLE);
	}
}
