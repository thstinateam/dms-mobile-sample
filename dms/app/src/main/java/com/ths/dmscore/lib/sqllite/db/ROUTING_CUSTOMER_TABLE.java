package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Date;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.RoutingCustomerDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;

/**
 * Mo ta muc dich cua class
 *
 * @author: TamPQ
 * @version: 1.0
 * @since: 1.0
 */
public class ROUTING_CUSTOMER_TABLE extends ABSTRACT_TABLE {
	// ID bang
	public static final String ROUTING_CUSTOMER_ID = "ROUTING_CUSTOMER_ID";
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// id khach hang
	public static final String SHOP_ID = "SHOP_ID";
	// 0: ko di , 1: di
	public static final String MONDAY = "MONDAY";
	//week interval
	public static final String WEEK_INTERVAL = "WEEK_INTERVAL";
	// 0: ko di , 1: di
	public static final String TUESDAY = "TUESDAY";
	// 0: ko di , 1: di
	public static final String WEDNESDAY = "WEDNESDAY";
	// 0: ko di , 1: di
	public static final String THURSDAY = "THURSDAY";
	// 0: ko di , 1: di
	public static final String FRIDAY = "FRIDAY";
	// 0: ko di , 1: di
	public static final String SATURDAY = "SATURDAY";
	// 0: ko di , 1: di
	public static final String SUNDAY = "SUNDAY";
	// 0: khong hoat dong, 1: hoat dong
	public static final String STATUS = "STATUS";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// ngay update
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// tan suat
	public static final String SEQ = "SEQ";
	// id tuyen
	public static final String ROUTING_ID = "ROUTING_ID";
	// thu tu ghe tham thu 2
	public static final String SEQ2 = "SEQ2";
	// thu tu ghe tham thu 3
	public static final String SEQ3 = "SEQ3";
	// thu tu ghe tham thu 4
	public static final String SEQ4 = "SEQ4";
	// thu tu ghe tham thu 5
	public static final String SEQ5 = "SEQ5";
	// thu tu ghe tham thu 6
	public static final String SEQ6 = "SEQ6";
	// thu tu ghe tham thu 7
	public static final String SEQ7 = "SEQ7";
	// thu tu ghe tham thu cn
	public static final String SEQ8 = "SEQ8";
	// START_WEEK
	public static final String START_WEEK = "START_WEEK";
	public static final String START_DATE= "START_DATE";
	public static final String END_DATE= "END_DATE";
	public static final String WEEK1= "WEEK1";
	public static final String WEEK2= "WEEK2";
	public static final String WEEK3= "WEEK3";
	public static final String WEEK4= "WEEK4";
	public static final String LAST_ORDER= "LAST_ORDER";
	public static final String LAST_APPROVE_ORDER= "LAST_APPROVE_ORDER";

	public static final String TABLE_NAME = "ROUTING_CUSTOMER";

	public ROUTING_CUSTOMER_TABLE() {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { ROUTING_CUSTOMER_ID, CUSTOMER_ID, SHOP_ID, MONDAY, TUESDAY, WEDNESDAY,
				THURSDAY, FRIDAY, SATURDAY, SUNDAY, STATUS, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, SEQ,
				SEQ2, SEQ3, SEQ4, SEQ5, SEQ6, SEQ7, SEQ8, START_WEEK, SYN_STATE };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public ROUTING_CUSTOMER_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { ROUTING_CUSTOMER_ID, CUSTOMER_ID, SHOP_ID, MONDAY, TUESDAY, WEDNESDAY,
				THURSDAY, FRIDAY, SATURDAY, SUNDAY, STATUS, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER, SEQ,
				SEQ2, SEQ3, SEQ4, SEQ5, SEQ6, SEQ7, SEQ8, START_WEEK, SYN_STATE };

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		return 0;
	}

	/**
	 * Cap nhat khi tao moi order
	 *
	 * @author: Dung NT19
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long updateLastOrder(AbstractTableDTO dto) {
		RoutingCustomerDTO cusDTO = (RoutingCustomerDTO) dto;
		ContentValues value = initDataUpdateFromOrder(cusDTO);
		String[] params = { "" + cusDTO.routingCustomerId };
		return update(value, ROUTING_CUSTOMER_ID + " = ?", params);
	}

	/**
	 * Khoi tao du lieu cap nhat routing customer
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: ContentValues
	 * @throws:
	 * @param dto
	 * @return
	 */
	public ContentValues initDataUpdateFromOrder(RoutingCustomerDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(LAST_ORDER, dto.lastOrder);
		if (!StringUtil.isNullOrEmpty(dto.lastApproveOrder)) {
			editedValues.put(LAST_APPROVE_ORDER, dto.lastApproveOrder);
		}
		return editedValues;
	}

	/**
	 * Lay routing customer id cua kh
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: long
	 * @throws:
	 * @param staffId
	 * @param shopId
	 * @param customerId
	 * @return
	 * @throws Exception
	 */
	public long getRoutingCustomerIdOfCustomer(int staffId, int shopId, long customerId) throws Exception{
		long routingCustomerId = 0;
		Cursor c = null;
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String firstDayOfYear = DateUtils.getFirstDateOfYear(DateUtils.DATE_FORMAT_NOW,new Date(), 0);
		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();

		var1.append("SELECT RTC.ROUTING_CUSTOMER_ID AS ROUTING_CUSTOMER_ID ");
		var1.append("        FROM   VISIT_PLAN VP, ");
		var1.append("               ROUTING RT, ");
		var1.append("               (SELECT ROUTING_CUSTOMER_ID, ");
		var1.append("                       ROUTING_ID, ");
		var1.append("                       CUSTOMER_ID, ");
		var1.append("                       STATUS, ");
		var1.append("                       WEEK1, ");
		var1.append("                       WEEK2, ");
		var1.append("                       WEEK3, ");
		var1.append("                       WEEK4, ");
		var1.append("                       MONDAY, ");
		var1.append("                       TUESDAY, ");
		var1.append("                       WEDNESDAY, ");
		var1.append("                       THURSDAY, ");
		var1.append("                       FRIDAY, ");
		var1.append("                       SATURDAY, ");
		var1.append("                       SUNDAY, ");
		var1.append("                       SEQ2, ");
		var1.append("                       SEQ3, ");
		var1.append("                       SEQ4, ");
		var1.append("                       SEQ5, ");
		var1.append("                       SEQ6, ");
		var1.append("                       SEQ7, ");
		var1.append("                       SEQ8 ");
		var1.append("                FROM   ROUTING_CUSTOMER ");
		var1.append("                WHERE (DATE(END_DATE) >=Date(?) OR DATE(END_DATE) IS NULL) and ");
		param.add(dateNow);
		var1.append("                DATE(START_DATE) <= ? and ");
		param.add(dateNow);
		var1.append("                ( ");
		var1.append("                (WEEK1 IS NULL AND WEEK2 IS NULL AND WEEK3 IS NULL AND WEEK4 IS NULL) OR");
		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(dateNow);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(dateNow);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(dateNow);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		param.add(firstDayOfYear);
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		param.add(firstDayOfYear);
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=1 and week1=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(dateNow);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(dateNow);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(dateNow);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		param.add(firstDayOfYear);
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		param.add(firstDayOfYear);
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=2 and week2=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(dateNow);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(dateNow);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(dateNow);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		param.add(firstDayOfYear);
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		param.add(firstDayOfYear);
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=3 and week3=1) or");

		var1.append("	(((cast((julianday(?) - julianday(?)) / 7  as integer) +");
		param.add(dateNow);
		param.add(firstDayOfYear);
		var1.append("		   (case when 1");
		var1.append("			and (");
		var1.append("		cast((case when strftime('%w', ?) = '0' ");
		param.add(dateNow);
		var1.append("									  then 7 ");
		var1.append("									  else strftime('%w', ?)                          ");
		param.add(dateNow);
		var1.append("									  end ) as integer) < ");
		var1.append("		 cast((case when strftime('%w', ?) = '0' ");
		param.add(firstDayOfYear);
		var1.append("										  then 7 ");
		var1.append("										  else strftime('%w', ?)                          ");
		param.add(firstDayOfYear);
		var1.append("										  end ) as integer) ) ");
		var1.append("			then 1 else 0 end)) % 4 + 1)=4 and week4=1) )");
		var1.append("                ) RTC ");
		var1.append("        WHERE  VP.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		var1.append("               AND RTC.CUSTOMER_ID = ? ");
		param.add(String.valueOf(customerId));
		var1.append("               AND DATE(VP.FROM_DATE) <= DATE(?) ");
		param.add(dateNow);
		var1.append("               AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		param.add(dateNow);
		var1.append("               AND VP.STAFF_ID = ? ");
		param.add(String.valueOf(staffId));
		var1.append("               AND VP.STATUS = 1 ");
		var1.append("               AND RT.STATUS = 1 ");
		var1.append("               AND RTC.STATUS = 1 ");

		try {
			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						routingCustomerId = CursorUtil.getLong(c, ROUTING_CUSTOMER_TABLE.ROUTING_CUSTOMER_ID);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
			}
		}

		return routingCustomerId;
	}

}
