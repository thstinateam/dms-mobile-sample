package com.ths.dmscore.dto.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.database.Cursor;
import android.net.ParseException;

import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.lib.sqllite.db.RPT_DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * DTO cua man hinh bao cao chuong trinh trung bay
 * 
 * @author hieunq1
 * 
 */
public class DisProComProgReportDTO {
	// danh sach item
	public ArrayList<DisProComProgReportItem> arrList;
	// danh sach level code
	public ArrayList<DisplayProgramLevelForProgramDTO> arrayLevel = new ArrayList<DisplayProgramLevelForProgramDTO>();
	// tong ket qua
	public ArrayList<DisProComProgReportItemResult> arrResultTotal;
	public DisProComProgReportItemResult dtoResultTotal;
	// max level display
	public int maxLevelDisPlay = 0;

	public DisProComProgReportDTO() {
		arrList = new ArrayList<DisProComProgReportDTO.DisProComProgReportItem>();
		arrayLevel = new ArrayList<DisplayProgramLevelForProgramDTO>();
		// arrLevelCode = new ArrayList<String>();
		arrResultTotal = new ArrayList<DisProComProgReportItemResult>();
		dtoResultTotal = new DisProComProgReportItemResult();
	}

	/**
	 * new DisProComProgReportItemResult
	 * 
	 * @return
	 */
	public DisProComProgReportItemResult newDisProComProgReportItemResult() {
		return new DisProComProgReportItemResult();
	}

	/**
	 * add item
	 * 
	 * @param c
	 */
	public void addItem(Cursor c) {
		arrList.add(new DisProComProgReportItem(c));
	}

	/**
	 * new DisProComProgReportItem
	 * 
	 * @return
	 */
	public DisProComProgReportItem newDisProComProgReportItem() {
		return new DisProComProgReportItem();
	}

	/**
	 * DisProComProgReportItemResult
	 * 
	 * @author hieunq1
	 * 
	 */
	public class DisProComProgReportItemResult {
		public int resultNumber;
		public int joinNumber;
		public String leveLCode;
	}

	/**
	 * DisProComProgReportItem
	 * 
	 * @author hieunq1
	 * 
	 */
	public class DisProComProgReportItem {
		public String programId;
		public String staffId;
		public String staffCode;
		public String staffName;
		public String programCodeShort;
		public String programNameShort;
		public String programCode;
		public String programName;
		// ma Level
		public String programLevel;
		public String dateFromTo;
		public String from_date;
		public String to_date;
		public ArrayList<DisProComProgReportItemResult> arrLevelCode;
		public DisProComProgReportItemResult itemResultTotal;

		public DisProComProgReportItem() {
			arrLevelCode = new ArrayList<DisProComProgReportItemResult>();
			itemResultTotal = new DisProComProgReportItemResult();
			programCodeShort = "";
			programNameShort = "";
			programCode = "";
			programName = "";
			programLevel = "";
			dateFromTo = "";
		}

		public DisProComProgReportItem(Cursor c) {

			programCodeShort = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DP_SHORT_CODE);
			programNameShort = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DP_SHORT_NAME);
			programCode = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_CODE);
			programName = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_NAME);
			programLevel = CursorUtil.getString(c, RPT_DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAME_LEVEL);
			from_date = CursorUtil.getString(c, RPT_DISPLAY_PROGRAME_TABLE.FROM_DATE);
			to_date = CursorUtil.getString(c, RPT_DISPLAY_PROGRAME_TABLE.TO_DATE);
		}

		/**
		 * new DisProComProgReportItemResult
		 * 
		 * @return
		 */
		public DisProComProgReportItemResult NewDisProComProgReportItemResult() {
			return new DisProComProgReportItemResult();
		}

		/**
		 * 
		 * khoi tao du lieu cho tung CTTB, tinh toan lai so KH tham gia va so
		 * khach hang dat
		 * 
		 * @author: HaiTC3
		 * @param c
		 * @param dto
		 * @return: void
		 * @throws:
		 * @since: Feb 19, 2013
		 */
		public void initWithCursor(Cursor c, DisProComProgReportDTO dto) {
			SimpleDateFormat sfs = DateUtils.defaultSqlDateFormat;
			SimpleDateFormat sfd = DateUtils.defaultDateFormat;
			staffId = CursorUtil.getString(c, "STAFF_ID");
			staffCode = CursorUtil.getString(c, "STAFF_CODE");
			staffName = CursorUtil.getString(c, "STAFF_NAME");
			programId = CursorUtil.getString(c, "DP_ID");
			programCodeShort = CursorUtil.getString(c, "SHORT_CODE");
			if (StringUtil.isNullOrEmpty(programCodeShort)) {
				programCodeShort = CursorUtil.getString(c, "STAFF_CODE");
			}
			programNameShort = CursorUtil.getString(c, "SHORT_NAME");
			if (StringUtil.isNullOrEmpty(programNameShort)) {
				programNameShort = CursorUtil.getString(c, "STAFF_NAME");
			}
			programLevel = CursorUtil.getString(c, "LEVEL");
			from_date = CursorUtil.getString(c, "FROM_DATE");
			to_date = CursorUtil.getString(c, "TO_DATE");
			try {
				try {
					if (!StringUtil.isNullOrEmpty(from_date)) {
						dateFromTo = sfd.format(sfs.parse(from_date));
					}
					dateFromTo += "-";
					if (!StringUtil.isNullOrEmpty(to_date)) {
						dateFromTo += sfd.format(sfs.parse(to_date));
					}
				} catch (java.text.ParseException e) {
					 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
				}
			} catch (ParseException e) {
				dateFromTo = "";
				 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
			String joinNumber = CursorUtil.getString(c, "NUM_JOIN");
			String resultNumber = CursorUtil.getString(c, "NUM_NON_COMPLETE");

			String[] arrProgramLevel = programLevel.split(",");
			String[] arrJoinNumber = joinNumber.split(",");
			String[] arrResultNumber = resultNumber.split(",");

			try {
				// tao d/s level tmp va lay dung display program tuong ung voi
				// programId
				DisplayProgramLevelForProgramDTO currentDPLevel = new DisplayProgramLevelForProgramDTO();
				for (int i = 0, size = dto.arrayLevel.size(); i < size; i++) {
					if (String.valueOf(dto.arrayLevel.get(i).displayProgramId)
							.equals(programId)) {
						currentDPLevel = dto.arrayLevel.get(i);
					}
				}
				for (int i = 0; i < dto.maxLevelDisPlay; i++) {
					arrLevelCode.add(newDisProComProgReportItemResult());
				}

				// cap nhat du lieu cho tung level tmp
				for (int i = 0, size = currentDPLevel.listDisProLevel.size(); i < size; i++) {

					for (int j = 0, size2 = arrProgramLevel.length; j < size2; j++) {
						if (arrProgramLevel[j]
								.equals(currentDPLevel.listDisProLevel.get(i).levelCode)) {
							arrLevelCode.get(i).joinNumber = Integer
									.parseInt(arrJoinNumber[j]);
							arrLevelCode.get(i).resultNumber = Integer
									.parseInt(arrResultNumber[j]);
							DisProComProgReportItemResult rs = dto.arrResultTotal
									.get(i);
							DisProComProgReportItemResult rsi = arrLevelCode
									.get(i);
							rs.joinNumber += rsi.joinNumber;
							rs.resultNumber += rsi.resultNumber;
							// dung tinh cho cot tong
							itemResultTotal.joinNumber += rsi.joinNumber;
							itemResultTotal.resultNumber += rsi.resultNumber;
						}
					}
				}
				dto.dtoResultTotal.joinNumber += itemResultTotal.joinNumber;
				dto.dtoResultTotal.resultNumber += itemResultTotal.resultNumber;

			} catch (Exception e) {
				dateFromTo = "";
				 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
			}
		}
	}

}
