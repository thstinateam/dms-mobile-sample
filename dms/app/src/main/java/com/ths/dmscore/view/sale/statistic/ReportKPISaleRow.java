/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.ReportKPIItemDTO;
import com.ths.dms.R;

/**
 * row cua bao cao kpi
 *
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportKPISaleRow extends DMSTableRow implements OnClickListener {

	TextView tvCriteria; // chi tieu
	TextView tvValueDayPlan; // gia tri ke hoach ngay
	TextView tvValueDayDone; // gia tri thuc hien ngay
	TextView tvProgressDay; // tien do ngay
	TextView tvValueCyclePlan; // gia tri ke hoach chu ky
	TextView tvValueCycleDone; // gia tri thuc hien chu ky
	TextView tvProgressCycle; // tien do chu ky
	public ReportKPISaleRow(Context context) {
		super(context, R.layout.layout_report_kpi_sale_row);
		setOnClickListener(this);
		tvCriteria = (TextView) findViewById(R.id.tvCriteria);
		tvValueDayPlan = (TextView) findViewById(R.id.tvValueDayPlan);
		tvValueDayDone = (TextView) findViewById(R.id.tvValueDayDone);
		tvProgressDay = (TextView) findViewById(R.id.tvProgressDay);
		tvValueCyclePlan = (TextView) findViewById(R.id.tvValueCyclePlan);
		tvValueCycleDone = (TextView) findViewById(R.id.tvValueCycleDone);
		tvProgressCycle = (TextView) findViewById(R.id.tvProgressCycle);
	}

	 /**
	 * render du lieu cho row
	 * @author: Tuanlt11
	 * @param ReportKPIItemDTO
	 * @return: void
	 * @throws:
	*/
	public void render(ReportKPIItemDTO item) {
		tvCriteria.setText(item.criteria);
		if (item.dataType == ReportKPIItemDTO.TYPE_INT) {
			display(tvValueDayPlan, (long) item.valueDayPlan);
			display(tvValueDayDone, (long) item.valueDayDone);
			displayPercent(tvProgressDay, item.progressDay);
			display(tvValueCyclePlan, (long) item.valueCyclePlan);
			display(tvValueCycleDone, (long) item.valueCycleDone);
			displayPercent(tvProgressCycle, item.progressCycle);
		} else {
			display(tvValueDayPlan, item.valueDayPlan);
			display(tvValueDayDone, item.valueDayDone);
			displayPercent(tvProgressDay, item.progressDay);
			display(tvValueCyclePlan, item.valueCyclePlan);
			display(tvValueCycleDone, item.valueCycleDone);
			displayPercent(tvProgressCycle, item.progressCycle);
		}

	}

}
