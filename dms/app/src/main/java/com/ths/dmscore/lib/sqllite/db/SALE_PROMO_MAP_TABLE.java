package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SalePromoMapDTO;

/**
 * Table cua bang luu thong tin KM dat hay ko dat
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class SALE_PROMO_MAP_TABLE extends ABSTRACT_TABLE {
	public static final String SALE_PROMO_MAP_ID = "SALE_PROMO_MAP_ID";
	public static final String SALE_ORDER_DETAIL_ID = "SALE_ORDER_DETAIL_ID";
	public static final String PROGRAM_CODE = "PROGRAM_CODE";
	public static final String STATUS = "STATUS";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	public static final String TABLE_NAME = "SALE_PROMO_MAP";

	public SALE_PROMO_MAP_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] {SALE_PROMO_MAP_ID,SALE_ORDER_DETAIL_ID,PROGRAM_CODE,STATUS,STAFF_ID,SALE_ORDER_ID,SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}


	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		SalePromoMapDTO promoDto = (SalePromoMapDTO) dto;
		return insert(null, initDataRow(promoDto));
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	private ContentValues initDataRow(SalePromoMapDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(SALE_PROMO_MAP_ID, dto.salePromoMapId);
		editedValues.put(SALE_ORDER_DETAIL_ID, dto.saleOrderDetailId);
		editedValues.put(PROGRAM_CODE, dto.programCode);
		editedValues.put(STATUS, dto.status);
		editedValues.put(STAFF_ID, dto.staffId);
		editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		return editedValues;
	}

	 /**
	 * Xoa tat ca cac km lien quan toi don hang
	 * @author: Tuanlt11
	 * @param saleOrderId
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int deleteAllPromoMapOfOrder(long saleOrderId) {
		ArrayList<String> params = new ArrayList<String>();
		params.add(String.valueOf(saleOrderId));

		return delete(SALE_ORDER_ID + " = ? ",
						params.toArray(new String[params.size()]));
	}

	/**
	 * @author: DungNX
	 * @param parentStaffId
	 * @param inheritShopId
	 * @param saleOrderID
	 * @return
	 * @throws Exception
	 * @return: List<SaleOrderPromoDetailDTO>
	 * @throws:
	*/
	public ArrayList<SalePromoMapDTO> getSalePromoMap(long saleOrderID) throws Exception{
		ArrayList<SalePromoMapDTO> result = new ArrayList<SalePromoMapDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    SALE_PROMO_MAP	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND SALE_ORDER_ID = ?	");
		paramsObject.add(saleOrderID + "");

		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do{
						SalePromoMapDTO item = new SalePromoMapDTO();
						item.initDataFromCursor(c);
						result.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}

		return result;
	}
}
