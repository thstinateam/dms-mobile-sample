/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

/**
 *  Luu kho thuc cua Presale
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class WarehouseDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 4828310485531500268L;
	public int warehouseId;
	public String warehouseCode;
	public String warehouseName;
	public int shopId;
	public String description;
	public int status;
	public int warehouseType;
	public int seq;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	public String warehouseNameText;
	public int synState;
	
	public WarehouseDTO(){
		super(TableType.WAREHOUSE_TABLE);
	}
}
