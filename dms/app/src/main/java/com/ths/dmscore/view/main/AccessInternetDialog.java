/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.Calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.guard.AccessInternetService;
import com.ths.dms.R;

/**
 * Dialog hien thi nhap mat khau de su dung ung dung truy cap internet
 * @author: tuanlt
 * @version: 1.0
 * @since: 18:54:30 25-12-2014
 */
public class AccessInternetDialog extends LinearLayout implements OnClickListener, Callback {

	EditText edUserName;// control nhap ten
	EditText edPassword;// control nhap pass
	Button btLogin; // button login
	TextView tvWrongTime; // text sai username, password
	public View viewLayout;
	Context context;

	public AccessInternetDialog(Context context) {
		super(context);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		viewLayout = inflater.inflate(R.layout.layout_dialog_access_network, null);
		this.context = context;
		btLogin = (Button) viewLayout.findViewById(R.id.btLogin);
		btLogin.setOnClickListener(this);
		edUserName = (EditText) viewLayout.findViewById(R.id.edUserName);
		edPassword = (EditText) viewLayout.findViewById(R.id.edPassword);
		tvWrongTime = (TextView) viewLayout.findViewById(R.id.tvWrongTime);
		GlobalUtil.setEditTextMaxLength(edUserName, 50);
		GlobalUtil.setEditTextMaxLength(edPassword, 50);

		//bo action select text, copy...
		edPassword.setCustomSelectionActionModeCallback(this);
		edUserName.setCustomSelectionActionModeCallback(this);
	}

	@Override
	public void onClick(View arg0) {
		requestUnlock();
	}

	/**
	 * request unlock network
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void requestUnlock() {
		try {
			String fullName = edUserName.getText().toString().trim().toLowerCase();
			//edUserName.setText(fullName);
			int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) / 2;
			String dateNow = DateUtils
					.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
			String edPass = StringUtil.generateHash(dateNow, fullName).substring(0, 3);
			String edPassAll = StringUtil.generateHash(dateNow, "vtvnm").substring(0, 3);
			String edPassWithHour = StringUtil.generateHash(dateNow + hour, fullName).substring(0, 3);
			String edPassWithHourAll = StringUtil.generateHash(dateNow + hour, "vtvnm").substring(0, 3);
			String passInput = edPassword.getText().toString().trim().toLowerCase();
			if (edPass.equals(passInput) || edPassAll.equals(passInput) 
					|| edPassWithHour.equals(passInput) || edPassWithHourAll.equals(passInput)) {
				AccessInternetService.unlockAppPrevent(true);
				tvWrongTime.setVisibility(View.INVISIBLE);
			} else {
				AccessInternetService.unlockAppPrevent(false);
				tvWrongTime.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Hien thi ten user dang nhap neu da dang nhap roi
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	 */
	public void showUserName() {
		try {
			tvWrongTime.setVisibility(View.INVISIBLE);
			SharedPreferences sharedPreferences = GlobalInfo.getInstance()
					.getDmsPrivateSharePreference();
			String userName = sharedPreferences.getString(
					LoginView.VNM_USER_NAME, "").trim();;
			// reset password
			edPassword.setText("");
			if (!StringUtil.isNullOrEmpty(userName)) {
				edUserName.setEnabled(false);
				edUserName.setPadding(GlobalUtil.dip2Pixel(10),
						GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
						GlobalUtil.dip2Pixel(5));
				edUserName.setText(userName);
				edPassword.requestFocus();
			} else {
				edUserName.setEnabled(true);
				edUserName.setText("");
				edUserName.requestFocus();
			}
		} catch (Exception e) {
			MyLog.e("showUserName", e.getMessage(), e);
		}
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
		return false;
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu) {
		return false;
	}

	@Override
	public void onDestroyActionMode(ActionMode mode) {
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
		return false;
	}

}