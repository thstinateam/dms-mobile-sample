package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

public class ListReportColumnCatDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public int totalItem;
	public ArrayList<ReportColumnCatDTO> listReportColumnCatDTO;
	
	public ListReportColumnCatDTO(){
		totalItem = 0;
		listReportColumnCatDTO = new ArrayList<ReportColumnCatDTO>();
	}

}
