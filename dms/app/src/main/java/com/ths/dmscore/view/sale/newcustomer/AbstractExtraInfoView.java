/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.util.StringUtil;

/**
 * class abstract dung cho thuoc tinh dong khach hang
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  2:25:15 PM Jan 27, 2015
 */
public abstract class AbstractExtraInfoView extends LinearLayout {

	protected Context con;
	protected final ArrayList<View> arrView;
	public long tag;
	
	private AbstractExtraInfoView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		con = context;
		arrView = new ArrayList<View>();
	}

	private AbstractExtraInfoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		con = context;
		arrView = new ArrayList<View>();
	}

	private AbstractExtraInfoView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		con = context;
		arrView = new ArrayList<View>();
	}
	
	protected AbstractExtraInfoView(Context context, int layout){
		this(context);
		LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout view = (LinearLayout)inflater.inflate(layout, this, false);
		initView(view);
	}
	
	/**
	 * khoi tao view
	 * 
	 * @author: dungdq3
	 * @since: 2:37:06 PM Jan 27, 2015
	 * @return: void
	 * @throws:  
	 * @param row
	 */
	protected void initView(LinearLayout row){
		setLayoutParams(row.getLayoutParams());
		setOrientation(row.getOrientation());
		setPadding(0, 5, 0, 5);
		if (row != null) {
			int childcount = row.getChildCount();
			for (int i = 0; i < childcount; i++) {
				arrView.add(row.getChildAt(i));
			}
			row.removeAllViews();
		}
		removeAllViews();// add to this
		for (View v : arrView) {
			addView(v);
		}
	}
	
	public abstract Object getDataFromView();
	public abstract void renderLayout(CustomerAttributeViewDTO dto, boolean isEdit, String fromView);
	public abstract boolean isCheckData();
	public abstract String getName();
	
	/**
	 * request focus
	 * 
	 * @author: dungdq3
	 * @since: 9:20:28 AM Feb 24, 2015
	 * @return: void
	 * @throws:  
	 */
	public boolean requestFocusView(){
		return false;
	}
	
	protected void setSymbol(String symbol, String text, int color, TextView tv){
		// them *: vao
		StringBuilder sb = new StringBuilder(text);
		sb.append(Constants.STR_SPACE);
		sb.append(StringUtil.getColorText(symbol, color));

		tv.setText(StringUtil.getHTMLText(sb.toString()));
	}

}
