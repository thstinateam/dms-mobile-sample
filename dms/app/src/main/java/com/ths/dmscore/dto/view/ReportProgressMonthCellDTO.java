/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.db.RPT_STAFF_SALE_TABLE;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * report month info cell
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class ReportProgressMonthCellDTO implements Serializable {
	private static final long serialVersionUID = -3069648381163460031L;
	// NPP id
	public long staffID; // idGSNPP , idNVBH
	public String listStaffID; // idGSNPP , idNVBH
	// staff code
	public String staffCode; // code gsnpp, nvbh
	// name NPP
	public String staffName; // NAME_GSNPP, NAME_NVBH
	// GSNPP id
	public long staffOwnerID; // idNPP, idGSNPP
	// staff owner code
	public String staffOwnerCode; // code npp, gsnpp
	// name GSNPP
	public String staffOwnerName; // NAME_NPP, NAME_GSNPP
	// amount plan
	public double amountPlan;
	// amount sale
	public double amountDone;
	// amount duyet
	public double duyet;
	// progress done
	public double progressAmountDone;
	// amount remain
	public double amountRemain;

	// amount plan
	public long quantityPlan;
	// amount sale
	public long quantityDone;
	// amount duyet
	public long quantityDuyet;
	// progress done
	public double progressQuantityDone;
	// amount remain
	public long quantityRemain;

	// sku plan
	public float numSKUPlan;
	// sku done
	public float numSKUDone;

	public ReportProgressMonthCellDTO() {
		staffID = -1;
		staffName = "";
		staffOwnerName = "";
		listStaffID = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		amountDone = 0;
		quantityDone = 0;
		duyet = 0;
		quantityDuyet = 0;
		amountPlan = 0;
		quantityPlan = 0;
		amountRemain = 0;
		quantityRemain = 0;
		progressAmountDone = 0;
		progressQuantityDone = 0;
		numSKUDone = 0;
		numSKUPlan = 0;
		staffCode = "";
		staffOwnerCode = "";
	}

	/**
	 *
	 * init object report progress month
	 *
	 * @author: HaiTC3
	 * @param c
	 * @param sysCurrencyDivide
	 * @return: void
	 * @throws:
	 */
	public void initReportProgressMonthObject(Cursor c, int sysCurrencyDivide) {
		staffID = CursorUtil.getLong(c, STAFF_TABLE.STAFF_ID);

		staffCode = CursorUtil.getString(c, STAFF_TABLE.STAFF_CODE);
		staffName = CursorUtil.getString(c, STAFF_TABLE.STAFF_NAME);
		staffOwnerID = CursorUtil.getLong(c, "STAFF_OWNER_ID");
		staffOwnerCode = CursorUtil.getString(c, "STAFF_OWNER_CODE");
		staffOwnerName = CursorUtil.getString(c, "STAFF_OWNER_NAME");

		amountPlan = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_TABLE.MONTH_AMOUNT_PLAN);
		amountDone = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_TABLE.MONTH_AMOUNT);
		duyet = CursorUtil.getDoubleUsingSysConfig(c, RPT_STAFF_SALE_TABLE.MONTH_AMOUNT_APPROVED);

		// remain (may be < 0)
		amountRemain = (amountPlan - amountDone) >= 0 ? (amountPlan - amountDone)
				: 0;
		// progress
		progressAmountDone = StringUtil.calPercentUsingRound(amountPlan, progressAmountDone);

		quantityPlan = CursorUtil.getLong(c, "MONTH_QUANTITY_PLAN");
		quantityDone = CursorUtil.getLong(c, "MONTH_QUANTITY");
		quantityDuyet = CursorUtil.getLong(c, "MONTH_QUANTITY_APPROVED");
		quantityRemain = quantityPlan >= quantityDone ? quantityPlan - quantityDone : 0;
		progressQuantityDone = StringUtil.calPercentUsingRound(quantityPlan, quantityDone);

//		numSKUDone = StringUtil.getFloatFromCursor(c, RPT_STAFF_SALE_TABLE.MONTH_SKU);
//		numSKUPlan = StringUtil.getFloatFromCursor(c, RPT_STAFF_SALE_TABLE.MONTH_SKU_PLAN);

	}

}
