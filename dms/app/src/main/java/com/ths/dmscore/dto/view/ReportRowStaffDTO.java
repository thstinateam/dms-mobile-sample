package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;


/**
 * DTO chon row nhan vien cho man hinh bao cao dong
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportRowStaffDTO  {
	// id nv
	public int staffId;
	// ma nv 
	public String staffCode;
	// ten nv
	public String staffName;
	// id nguoi quan li
	public int parentStaffId;
	// nguoi quan li
	public String parentStaffName;
	// code nguoi quan li
	public String parentStaffCode;
	// shopId
	public String shopId;
	// ten shop
	public String shopName;
	// bien de biet row co duoc chon hay ko
	public boolean isCheck = false;
	// trang thai
	public int status;
	
	 /**
	 * Khoi tao du lieu
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initFromCursor(Cursor c) {
		staffId = CursorUtil.getInt(c, "STAFF_ID");
		staffCode = CursorUtil.getString(c, "STAFF_CODE");
		staffName = CursorUtil.getString(c, "STAFF_NAME");
		parentStaffId = CursorUtil.getInt(c, "PARENT_STAFF_ID");
		parentStaffName = CursorUtil.getString(c, "PARENT_STAFF_NAME");
		parentStaffCode = CursorUtil.getString(c, "PARENT_STAFF_CODE");
		shopId = CursorUtil.getString(c, "SHOP_ID");
		shopName = CursorUtil.getString(c, "SHOP_NAME");
		status = CursorUtil.getInt(c, "STATUS");
	}
}
