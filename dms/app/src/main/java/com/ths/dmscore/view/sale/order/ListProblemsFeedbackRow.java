package com.ths.dmscore.view.sale.order;

import android.content.Context;
import android.widget.TextView;

import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.view.ProblemsFeedBackDTO;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

public class ListProblemsFeedbackRow extends DMSTableRow {
	TextView tvSTT; 
	TextView tvContent; 
	TextView tvType; 
	TextView tvRemindDate; 
	ProblemsFeedBackDTO.ProblemsFeedBackItem item;
	/**
	 * khoi tao control
	 * 
	 * @author quangvt
	 * @param context
	 */
	public ListProblemsFeedbackRow(Context context) {
		super(context, R.layout.layout_list_problems_feedback_row);
		tvSTT = (TextView) this.findViewById(R.id.tvSTT);
		tvContent = (TextView) this.findViewById(R.id.tvContent);
		tvType = (TextView) this.findViewById(R.id.tvType);
		tvRemindDate = (TextView) this.findViewById(R.id.tvReindDate); 
	}
	
	/**
	 * 
	*  Render row
	*  @author: quangvt
	*  @param position
	*  @param item
	*  @return: void
	*  @throws:
	 */
	public void renderLayout(int position, ProblemsFeedBackDTO.ProblemsFeedBackItem item){
		this.item = item; 
		
		String strPos = String.valueOf(position);
		String strContent = item.content;
		String strType = item.typeName;
		String strRemind = item.remindDate;
		
		tvSTT.setText(strPos);
		tvContent.setText(strContent);
		tvType.setText(strType);
		tvRemindDate.setText(strRemind);
		
		if((item.status == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE
				|| item.status == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE) 
				&& DateUtils.compareWithNow(item.doneDate, "dd/MM/yyyy") != 0){
			setColorBackgroundForRow(ImageUtil.getColor(R.color.COLOR_GRAY_DONE));
		}else if((item.status == FeedBackDTO.FEEDBACK_STATUS_STAFF_DONE
				|| item.status == FeedBackDTO.FEEDBACK_STATUS_STAFF_OWNER_DONE) 
				&& DateUtils.compareWithNow(item.doneDate, "dd/MM/yyyy") == 0){
			setColorBackgroundForRow(ImageUtil.getColor(R.color.COLOR_BLUE_DONE_TODAY));
		}else if(item.status == FeedBackDTO.FEEDBACK_STATUS_CREATE 
				&& DateUtils.compareWithNow(item.remindDate, "dd/MM/yyyy") == -1){
			setColorForTextRow(ImageUtil.getColor(R.color.RED));
		}
	}
	
	/**
	 * set mau chu cho textview
	 * @author banghn
	 * @param color: mau vi du: ImageUtil.getColor(R.color.RED)
	 */
	public void setColorForTextRow(int color) {
		tvSTT.setTextColor(color);
		tvContent.setTextColor(color); 
		tvRemindDate.setTextColor(color);
		tvType.setTextColor(color);
	}
	
	/**
	 * set background color for row
	 * @param color
	 * @return: void
	 * @throws:
	 * @author: BangHN
	 * @date: Nov 6, 2012
	 */
	public void setColorBackgroundForRow(int color) {
		tvSTT.setBackgroundColor(color);
		tvContent.setBackgroundColor(color); 
		tvRemindDate.setBackgroundColor(color);
		tvType.setBackgroundColor(color); 
	}
}
