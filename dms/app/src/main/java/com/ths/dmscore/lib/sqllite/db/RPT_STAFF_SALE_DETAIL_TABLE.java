package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;

import com.ths.dmscore.dto.view.SupReportAsoDTO;
import com.ths.dmscore.dto.view.SupReportAsoViewDTO;
import com.ths.dmscore.dto.view.trainingplan.ListSubTrainingPlanDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.CycleInfo;
import com.ths.dmscore.dto.db.trainingplan.SupTrainingPlanDTO;
import com.ths.dmscore.dto.view.CustomerSaleOfNVBHDTO;
import com.ths.dmscore.dto.view.NVBHReportCustomerSaleViewDTO;
import com.ths.dmscore.dto.view.NvbhReportAsoDTO;
import com.ths.dmscore.dto.view.NvbhReportAsoViewDTO;
import com.ths.dmscore.dto.view.ReportStaffSaleDetailItemDTO;
import com.ths.dmscore.dto.view.ReportStaffSaleDetailViewDTO;
import com.ths.dmscore.dto.view.trainingplan.TBHVDayTrainingSupervisionDTO;
import com.ths.dmscore.dto.view.trainingplan.TBHVDayTrainingSupervisionDTO.TBHVDayTrainingSupervisionItem;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DMSCursor;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.table.DMSSortInfo;

/**
 *
 * table summary all report detail for customer
 *
 * @author: HaiTC3
 * @version: 1.1
 * @since: 1.0
 */
public class RPT_STAFF_SALE_DETAIL_TABLE extends ABSTRACT_TABLE {
	//public static final String ID = "";    
	//public static final String STAFF_ID = "";   
	//public static final String CUSTOMER_ID = "";   
	//public static final String SHOP_ID = "";   
	//public static final String SALE_DATE = "";         
	//public static final String IS_OR = "";    
	public static final String DAY_AMOUNT = "DAY_AMOUNT"; 
	public static final String DAY_AMOUNT_PLAN = "DAY_AMOUNT_PLAN"; 
	public static final String DAY_AMOUNT_APPROVED = "DAY_AMOUNT_APPROVED"; 
	public static final String DAY_AMOUNT_PENDING = "DAY_AMOUNT_PENDING"; 
	public static final String DAY_QUANTITY = "DAY_QUANTITY";   
	public static final String DAY_QUANTITY_PLAN = "DAY_QUANTITY_PLAN";   
	public static final String DAY_QUANTITY_APPROVED = "DAY_QUANTITY_APPROVED";   
	public static final String DAY_QUANTITY_PENDING = "DAY_QUANTITY_PENDING";   
	public static final String MONTH_AMOUNT = "MONTH_AMOUNT"; 
	public static final String MONTH_AMOUNT_PLAN = "MONTH_AMOUNT_PLAN"; 
	public static final String MONTH_AMOUNT_APPROVED = "MONTH_AMOUNT_APPROVED"; 
	public static final String MONTH_AMOUNT_PENDING = "MONTH_AMOUNT_PENDING"; 
	public static final String MONTH_QUANTITY = "MONTH_QUANTITY";   
	public static final String MONTH_QUANTITY_PLAN = "MONTH_QUANTITY_PLAN";   
	public static final String MONTH_QUANTITY_APPROVED = "MONTH_QUANTITY_APPROVED";   
	public static final String MONTH_QUANTITY_PENDING = "MONTH_QUANTITY_PENDING";   
	public static final String OBJECT_TYPE = "OBJECT_TYPE";    
	public static final String OBJECT_ID = "OBJECT_ID"; 
	// id khach hang
	public static final String ID = "ID";
	public static final String STAFF_ID = "STAFF_ID";
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	public static final String SHOP_ID = "SHOP_ID";
	public static final String SALE_DATE = "SALE_DATE";
	public static final String AMOUNT_PLAN = "AMOUNT_PLAN";
	public static final String AMOUNT = "AMOUNT";
	public static final String AMOUNT_APPROVED = "AMOUNT_APPROVED";
	public static final String SCORE = "SCORE";
	public static final String IS_NEW = "IS_NEW";
	public static final String IS_ON = "IS_ON";
	public static final String IS_OR = "IS_OR";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String PARENT_STAFF_ID = "PARENT_STAFF_ID";
	public static final String WW_AMOUNT_PLAN = "WW_AMOUNT_PLAN";
	public static final String IS_KA = "IS_KA";

	public static final String RPT_STAFF_SALE_DETAIL_TABLE = "RPT_STAFF_SALE_DETAIL";

	public RPT_STAFF_SALE_DETAIL_TABLE() {
		this.tableName = RPT_STAFF_SALE_DETAIL_TABLE;
		this.columns = new String[] { ID, STAFF_ID, CUSTOMER_ID, SHOP_ID,
				SALE_DATE, AMOUNT_PLAN, AMOUNT, AMOUNT_APPROVED, SCORE, IS_NEW, IS_ON, IS_OR,
				CREATE_DATE, UPDATE_DATE, PARENT_STAFF_ID, WW_AMOUNT_PLAN,
				IS_KA };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = SQLUtils.getInstance().getmDB();
	}

	public RPT_STAFF_SALE_DETAIL_TABLE(SQLiteDatabase mDB) {
		this.tableName = RPT_STAFF_SALE_DETAIL_TABLE;
		this.columns = new String[] { ID, STAFF_ID, CUSTOMER_ID, SHOP_ID,
				SALE_DATE, AMOUNT_PLAN, AMOUNT, AMOUNT_APPROVED, SCORE, IS_NEW, IS_ON, IS_OR,
				CREATE_DATE, UPDATE_DATE, PARENT_STAFF_ID, WW_AMOUNT_PLAN,
				IS_KA };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 *
	 * lay thong tin bao cao chi tiet doanh so ngay
	 *
	 * @author: DungNT19
	 * @param
	 * @return
	 * @return: ReportStaffSaleDetailViewDTO
	 * @throws:
	 */
	public ReportStaffSaleDetailViewDTO getCatAmountDetailReport(Bundle data) throws Exception {
		ReportStaffSaleDetailViewDTO dto = new ReportStaffSaleDetailViewDTO();
		Cursor c = null;
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		int sysCalUnApproved = data.getInt(IntentConstants.INTENT_SYS_CAL_UNAPPROVED);
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sqlQuery = new StringBuffer();
//		StringBuffer  var1 = new StringBuffer();
//		var1.append("SELECT cus.customer_id, ");
//		var1.append("       cus.shop_id, ");
//		var1.append("       cus.customer_code, ");
//		var1.append("       cus.customer_name, ");
//		var1.append("       cus.contact_name, ");
//		var1.append("       cus.street, ");
//		var1.append("       ssd.[amount_plan], ");
//		var1.append("       ssd.amount, ");
//		var1.append("       ssd.score ");
//		var1.append("FROM   (SELECT * ");
//		var1.append("        FROM   (SELECT customer_id, ");
//		var1.append("                       shop_id, ");
//		var1.append("                       customer_code, ");
//		var1.append("                       customer_name, ");
//		var1.append("                       contact_name, ");
//		var1.append("                       street ");
//		var1.append("                FROM   (SELECT VP.visit_plan_id               AS VISIT_PLAN_ID, ");
//		var1.append("                               VP.shop_id                     AS SHOP_ID, ");
//		var1.append("                               VP.staff_id                    AS STAFF_ID, ");
//		var1.append("                               VP.from_date                   AS FROM_DATE, ");
//		var1.append("                               VP.to_date                     AS TO_DATE, ");
//		var1.append("                               RT.routing_id                  AS ROUTING_ID, ");
//		var1.append("                               RT.routing_code                AS ROUTING_CODE, ");
//		var1.append("                               RT.routing_name                AS ROUTING_NAME, ");
//		var1.append("                               CT.customer_id                 AS CUSTOMER_ID, ");
//		var1.append("                               Substr(CT.customer_code, 1, 3) AS CUSTOMER_CODE, ");
//		var1.append("                               CT.customer_name               AS CUSTOMER_NAME, ");
//		var1.append("                               CT.name_text                   AS NAME_TEXT, ");
//		var1.append("                               CT.mobiphone                   AS MOBIPHONE, ");
//		var1.append("                               CT.phone                       AS PHONE, ");
//		var1.append("                               CT.lat                         AS LAT, ");
//		var1.append("                               CT.lng                         AS LNG, ");
//		var1.append("                               CT.channel_type_id             AS CHANNEL_TYPE_ID ");
//		var1.append("                               , ");
//		var1.append("                               CT.address ");
//		var1.append("                               AS ADDRESS, ");
//		var1.append("                               ( CT.housenumber ");
//		var1.append("                                 || ' ' ");
//		var1.append("                                 || CT.street )               AS STREET, ");
//		var1.append("                               ( CASE ");
//		var1.append("                                   WHEN RTC.monday = 1 THEN 'T2' ");
//		var1.append("                                   ELSE '' ");
//		var1.append("                                 END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.tuesday = 1 THEN ',T3' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.wednesday = 1 THEN ',T4' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.thursday = 1 THEN ',T5' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.friday = 1 THEN ',T6' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.saturday = 1 THEN ',T7' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END ");
//		var1.append("                                 || CASE ");
//		var1.append("                                      WHEN RTC.sunday = 1 THEN ',CN' ");
//		var1.append("                                      ELSE '' ");
//		var1.append("                                    END )                     AS CUS_PLAN, ");
//		var1.append("                               ( CASE ");
//		var1.append("                                   WHEN RTC.seq2 = 0 THEN '' ");
//		var1.append("                                   ELSE RTC.seq2 ");
//		var1.append("                                 END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq3 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq3 ");
//		var1.append("                                    END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq4 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq4 ");
//		var1.append("                                    END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq5 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq5 ");
//		var1.append("                                    END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq6 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq6 ");
//		var1.append("                                    END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq7 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq7 ");
//		var1.append("                                    END ) ");
//		var1.append("                               || ( CASE ");
//		var1.append("                                      WHEN RTC.seq8 = 0 THEN '' ");
//		var1.append("                                      ELSE ',' ");
//		var1.append("                                           || RTC.seq8 ");
//		var1.append("                                    END )                     AS PLAN_SEQ, ");
//		var1.append("                               RTC.start_week                 AS START_WEEK, ");
//		var1.append("                               RTC.week_interval              AS WEEK_INTERVAL, ");
//		var1.append("                               RTC.routing_customer_id        AS ");
//		var1.append("                               ROUTING_CUSTOMER_ID, ");
//		var1.append("                               RTC.seq2                       AS DAY_SEQ, ");
//		var1.append("                               ct.contact_name ");
//		var1.append("                        FROM   visit_plan VP, ");
//		var1.append("                               routing RT, ");
//		var1.append("                               (SELECT routing_customer_id, ");
//		var1.append("                                       routing_id, ");
//		var1.append("                                       customer_id, ");
//		var1.append("                                       start_week, ");
//		var1.append("                                       week_interval, ");
//		var1.append("                                       status, ");
//		var1.append("                                       ( CASE ");
//		var1.append("                                           WHEN ( Round(Strftime('%W', Date( ");
//		var1.append("                                                        'now', ");
//		var1.append("                                                        'localtime') ");
//		var1.append("                                                        ) ");
//		var1.append("                                                        + 1 ");
//		var1.append("                                                        - start_week) >= 0 ");
//		var1.append("                                                  AND ( Round(Strftime('%W', ");
//		var1.append("                                                              Date('now' ");
//		var1.append("                                                              , ");
//		var1.append("                                                              'localtime')) ");
//		var1.append("                                                              + 1 ");
//		var1.append("                                                              - start_week ");
//		var1.append("                                                        ) ");
//		var1.append("                                                        % ");
//		var1.append("                                                            week_interval ) = 0 ");
//		var1.append("                                                ) THEN ");
//		var1.append("                                           1 ");
//		var1.append("                                           ELSE 0 ");
//		var1.append("                                         END ) AS INWEEK, ");
//		var1.append("                                       monday, ");
//		var1.append("                                       tuesday, ");
//		var1.append("                                       wednesday, ");
//		var1.append("                                       thursday, ");
//		var1.append("                                       friday, ");
//		var1.append("                                       saturday, ");
//		var1.append("                                       sunday, ");
//		var1.append("                                       seq2, ");
//		var1.append("                                       seq3, ");
//		var1.append("                                       seq4, ");
//		var1.append("                                       seq5, ");
//		var1.append("                                       seq6, ");
//		var1.append("                                       seq7, ");
//		var1.append("                                       seq8 ");
//		var1.append("                                FROM   routing_customer) RTC, ");
//		var1.append("                               customer CT ");
//		var1.append("                        WHERE  VP.routing_id = RT.routing_id ");
//		var1.append("                               AND RTC.routing_id = RT.routing_id ");
//		var1.append("                               AND RTC.customer_id = CT.customer_id ");
//		var1.append("                               AND Date(VP.from_date) <= Date('now', 'localtime' ");
//		var1.append("                                                         ) ");
//		var1.append("                               AND ( Date(VP.to_date) >= Date('now', 'localtime' ");
//		var1.append("                                                         ) ");
//		var1.append("                                      OR Date(VP.to_date) IS NULL ) ");
//		var1.append("                               -- ");
//		var1.append("                               AND VP.staff_id = ? ");
//		params.add(staffId);
//		var1.append("                               AND VP.status = 1 ");
//		var1.append("                               AND RT.status = 1 ");
//		var1.append("                               AND CT.status = 1 ");
//		var1.append("                               AND RTC.status = 1 ");
//		var1.append("                               AND RTC.inweek = 1) CUS ");
//		var1.append("                WHERE  1 = 1 ");
//		var1.append("                       AND Upper(cus_plan) LIKE Upper('%T2%') ");
//		var1.append("                GROUP  BY customer_id ");
//		var1.append("                UNION ALL ");
//		var1.append("                SELECT c.customer_id, ");
//		var1.append("                       c.shop_id, ");
//		var1.append("                       Substr(C.customer_code, 1, 3) AS CUSTOMER_CODE, ");
//		var1.append("                       c.customer_name, ");
//		var1.append("                       c.contact_name, ");
//		var1.append("                       ( C.housenumber ");
//		var1.append("                         || ' ' ");
//		var1.append("                         || C.street )               AS STREET ");
//		var1.append("                FROM   action_log ac ");
//		var1.append("                       LEFT JOIN customer c ");
//		var1.append("                              ON ac.customer_id = c.customer_id ");
//		var1.append("                WHERE  Date(ac.start_time) = Date('now', 'localtime') ");
//		var1.append("                       AND object_type IN ( 0, 1 )) ");
//		var1.append("        GROUP  BY customer_id) cus ");
//		var1.append("       LEFT JOIN rpt_staff_sale_detail ssd ");
//		var1.append("              ON cus.[customer_id] = ssd.customer_id ");
//		var1.append("WHERE  ssd.create_date = Date('now', 'localtime') ");

		try {
			//Tam thoi khong lay score rssd.SCORE 
			sqlQuery.append(" SELECT c.short_code CUSTOMER_CODE, c.CUSTOMER_NAME, rssd.CUSTOMER_ID, rssd.IS_OR, c.STREET, c.HOUSENUMBER, ");
			sqlQuery.append(" rssd.DAY_AMOUNT_PLAN, rssd.DAY_AMOUNT_APPROVED, ");
			if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
				sqlQuery.append(" 	      rssd.DAY_AMOUNT_APPROVED ");
			}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
				sqlQuery.append(" 	      rssd.DAY_AMOUNT_PENDING ");
			}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
				sqlQuery.append(" 	      rssd.DAY_AMOUNT");
			}else {
				sqlQuery.append(" 	      rssd.DAY_AMOUNT	");
			}
			sqlQuery.append(" as DAY_AMOUNT, rssd.DAY_QUANTITY_PLAN, rssd.DAY_QUANTITY_APPROVED, ");
			if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
				sqlQuery.append(" 	      rssd.DAY_QUANTITY_APPROVED ");
			}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
				sqlQuery.append(" 	      rssd.DAY_QUANTITY_PENDING	");
			}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
				sqlQuery.append(" 	      rssd.DAY_QUANTITY	");
			}else {
				sqlQuery.append(" 	      rssd.DAY_QUANTITY ");
			}
			sqlQuery.append("             as DAY_QUANTITY  ,( SELECT 1 ");
			sqlQuery.append("                          FROM   ACTION_LOG ");
			sqlQuery.append("                          WHERE  substr(ACTION_LOG.START_TIME, 1, 10) = ? AND ACTION_LOG.END_TIME IS NOT NULL ");
			params.add(date_now);
			sqlQuery.append("                                 AND ACTION_LOG.STAFF_ID = ? ");
			params.add(staffId);
			sqlQuery.append("                                 AND ACTION_LOG.SHOP_ID = ? ");
			params.add(shopId);
			sqlQuery.append("                                 AND rssd.CUSTOMER_ID = ACTION_LOG.CUSTOMER_ID ");
			sqlQuery.append("                                 AND ACTION_LOG.OBJECT_TYPE IN ( 0, 1 ) ) AS VISITED ");
			sqlQuery.append(" FROM rpt_staff_sale_detail rssd, customer c");
			sqlQuery.append(" WHERE rssd.object_type = 1 and rssd.object_id = ? and rssd.shop_id = ? and rssd.customer_id = c.customer_id ");
			params.add(staffId);
			params.add(shopId);
			sqlQuery.append(" and c.status > 0 ");
			sqlQuery.append(" and substr(rssd.SALE_DATE, 1, 10) = ?");
			params.add(date_now);
			//la nvbh thi group theo customer
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF) {
				sqlQuery.append(" group by rssd.customer_id ");
			}
			sqlQuery.append(" ORDER BY rssd.is_or, c.short_code, c.customer_name");

			c = this.rawQuery(sqlQuery.toString(), params.toArray(new String[params.size()]));

			if (c.moveToFirst()) {
				do {
					ReportStaffSaleDetailItemDTO item = new ReportStaffSaleDetailItemDTO();
					item.initDataFromCursor(c);

					dto.listReport.add(item);
				} while (c.moveToNext());
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * Danh sach khach hang cua NVBH trong ngay training cua TBHV
	 *
	 * @author: hoanpd1
	 * @since: 07:59:18 19-11-2014
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 * @param data
	 * @return
	 */
	public TBHVDayTrainingSupervisionDTO getListCustomerTraingStaff(Bundle data) {
		TBHVDayTrainingSupervisionDTO dto = new TBHVDayTrainingSupervisionDTO();
		Cursor c = null;
//		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		int tbhvID = data.getInt(IntentConstants.INTENT_STAFF_OWNER_ID);
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		String line = data.getString(IntentConstants.INTENT_VISIT_PLAN);
		String day = data.getString(IntentConstants.INTENT_DAY);

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT CT.CUSTOMER_ID AS CUSTOMER_ID, ");
		sqlQuery.append("          substr(CT.CUSTOMER_CODE, 1, 3) AS CUSTOMER_CODE, ");
		sqlQuery.append("          CT.CUSTOMER_NAME AS CUSTOMER_NAME, ");
		sqlQuery.append("          (CT.HOUSENUMBER || ', ' || CT.STREET) AS STREET, ");
		sqlQuery.append("          RSSD.AMOUNT_PLAN AS AMOUNT_PLAN, ");
		sqlQuery.append("          RSSD.[AMOUNT] AS AMOUNT, ");
		sqlQuery.append("          RSSD.AMOUNT_APPROVED AS AMOUNT_APPROVED, ");
		sqlQuery.append("          CT.LAT AS LAT, ");
		sqlQuery.append("          CT.LNG AS LNG, ");
		sqlQuery.append("          AL.CUSTOMER_ID AS AL_CUSTOMER_ID, ");
		sqlQuery.append("          str.CUSTOMER_ID AS STR_CUSTOMER_ID, ");
		sqlQuery.append("          AL.START_TIME AS START_TIME, ");
		sqlQuery.append("          AL.END_TIME AS END_TIME, ");
		sqlQuery.append("          AL.OBJECT_TYPE AS OBJECT_TYPE, ");
		sqlQuery.append("          sh.distance_order AS distance_order, ");
		sqlQuery.append("               ( CASE ");
		sqlQuery.append("                   WHEN VP.MONDAY = 1 THEN 'T2' ");
		sqlQuery.append("                   ELSE '' ");
		sqlQuery.append("                 END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.TUESDAY = 1 THEN ',T3' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.WEDNESDAY = 1 THEN ',T4' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.THURSDAY = 1 THEN ',T5' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.FRIDAY = 1 THEN ',T6' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.SATURDAY = 1 THEN ',T7' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END ");
		sqlQuery.append("                 || CASE ");
		sqlQuery.append("                      WHEN VP.SUNDAY = 1 THEN ',CN' ");
		sqlQuery.append("                      ELSE '' ");
		sqlQuery.append("                    END )              AS TUYEN ");
		sqlQuery.append("FROM      (SELECT 	RTC.*, VP.STAFF_ID, VP.SHOP_ID  ");
		sqlQuery.append("          	FROM 	VISIT_PLAN VP, ");
		sqlQuery.append("          			ROUTING RT, ");
		sqlQuery.append("          			ROUTING_CUSTOMER RTC ");
		sqlQuery.append("          	WHERE 	1 = 1 ");
		sqlQuery.append("        			AND  VP.ROUTING_ID = RT.ROUTING_ID ");
		sqlQuery.append("               	AND RTC.ROUTING_ID = RT.ROUTING_ID ");
		sqlQuery.append("               	AND DATE(VP.FROM_DATE) <= DATE(?) ");
		params.add(day);
		sqlQuery.append("               	AND (DATE(VP.TO_DATE) >= DATE(?) OR DATE(VP.TO_DATE) IS NULL) ");
		params.add(day);
		sqlQuery.append("               	AND DATE(RTC.START_DATE) <= DATE(?) ");
		params.add(day);
		sqlQuery.append("               	AND (DATE(RTC.END_DATE) >= DATE(?) OR DATE(RTC.END_DATE) IS NULL) ");
		params.add(day);
		sqlQuery.append("              		AND VP.STAFF_ID = ? ");
		params.add(String.valueOf(staffId));
		sqlQuery.append("               	AND VP.STATUS = 1 ");
		sqlQuery.append("               	AND VP.SHOP_ID = ? ");
		params.add(String.valueOf(shopId));
		sqlQuery.append("               	AND RT.STATUS = 1 ");
		sqlQuery.append("               	AND RTC.STATUS = 1) VP ");
		sqlQuery.append("   LEFT JOIN CUSTOMER CT ON VP.CUSTOMER_ID = CT.CUSTOMER_ID ");
		sqlQuery.append("   LEFT JOIN RPT_STAFF_SALE_DETAIL RSSD ON (VP.CUSTOMER_ID = RSSD.CUSTOMER_ID ");
		sqlQuery.append("                                           AND VP.STAFF_ID = RSSD.STAFF_ID ");
		sqlQuery.append("                                           AND RSSD.IS_OR = 0 ");
		sqlQuery.append("                                           AND SUBSTR(RSSD.SALE_DATE,1,10) = ? ");
		params.add(day);
		sqlQuery.append("                                           AND RSSD.SHOP_ID = ? ) ");
		params.add(""+shopId);
		sqlQuery.append("	LEFT JOIN (SELECT CUSTOMER_ID, START_TIME, END_TIME, OBJECT_TYPE ");
		sqlQuery.append("              FROM   ACTION_LOG  ");
		sqlQuery.append("              WHERE  SUBSTR(ACTION_LOG.START_TIME, 1, 10) = ?  ");
		params.add(day);
		if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_MANAGER){
			sqlQuery.append("                  	  AND ACTION_LOG.STAFF_ID = ? ");
			params.add(""+tbhvID);
		} else if(GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR){
			// sqlQuery.append("                  	  AND ACTION_LOG.STAFF_ID = (select staff_owner_id from staff where staff_id = ?) ");
			sqlQuery.append("                  	  AND ACTION_LOG.STAFF_ID IN ");
			sqlQuery.append("						(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
			sqlQuery.append("								AND PS.staff_id = ? AND PS.STATUS = 1 ");
			params.add("" + tbhvID);
			sqlQuery.append("								AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		}
		sqlQuery.append("                    	 AND ACTION_LOG.OBJECT_TYPE IN ( 0,1 )) AL ON VP.CUSTOMER_ID = AL.CUSTOMER_ID   ");
		sqlQuery.append("	LEFT JOIN (select distance_order, shop_id from shop where shop_id = ?) sh on sh.shop_id = VP.SHOP_ID ");
		params.add(""+shopId);
		sqlQuery.append("	LEFT JOIN (	SELECT 	stc.[CUSTOMER_ID], ");
		sqlQuery.append("       				str.[SUP_TRAINING_RESULT_ID] ");
		sqlQuery.append("				FROM   	sup_training_customer stc, ");
		sqlQuery.append("       				sup_training_result str ");
		sqlQuery.append("				WHERE  	stc.[SUP_TRAINING_CUSTOMER_ID] = str.[SUP_TRAINING_CUSTOMER_ID] ");
		sqlQuery.append("						AND    str.result > 0 ");
		sqlQuery.append("						AND    stc.[CREATE_STAFF_ID] = ? ");
		params.add(""+tbhvID);
		sqlQuery.append("						AND    str.[CREATE_STAFF_ID] = stc.[CREATE_STAFF_ID] ");
		sqlQuery.append("						AND    Substr(str.[CREATE_DATE], 1, 10) = ? ");
		params.add(day);
		sqlQuery.append(" 						AND    Substr(stc.[CREATE_DATE], 1, 10) = ?  ");
		params.add(day);
		sqlQuery.append(" 				GROUP BY stc.[CUSTOMER_ID] ");
		sqlQuery.append("			) str on VP.CUSTOMER_ID = str.CUSTOMER_ID ");
		sqlQuery.append(" WHERE 1 = 1 ");
		sqlQuery.append("      AND CT.STATUS = 1 ");
		if (!StringUtil.isNullOrEmpty(line)) {
			line = StringUtil.getEngStringFromUnicodeString(line);
			line = StringUtil.escapeSqlString(line);
			line = DatabaseUtils.sqlEscapeString("%" + line + "%");
			line = line.substring(1, line.length() - 1);
			sqlQuery.append("      AND upper(TUYEN) LIKE upper(?) escape '^' ");
			params.add(line);
		}
		sqlQuery.append("GROUP BY CT.customer_id ");
		sqlQuery.append("ORDER BY AL.OBJECT_TYPE asc, AL.START_TIME desc, ");
		sqlQuery.append("         CT.CUSTOMER_CODE ASC, ");
		sqlQuery.append("         CT.CUSTOMER_NAME ASC ");

		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c!=null && c.moveToFirst()) {
				do {
					TBHVDayTrainingSupervisionItem item = dto
							.newStaffTrainResultReportItem();
					item.initDataCustomerTraing(c, dto);
					dto.listResult.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			MyLog.e("", e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return dto;
	}

	/**
	 * Lay du lieu lich su huan luyen trong thang cua TBHV
	 * @author: hoanpd1
	 * @since: 11:07:15 20-11-2014
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ListSubTrainingPlanDTO getListTrainingHistory(Bundle data) throws Exception {
		ListSubTrainingPlanDTO dto = new ListSubTrainingPlanDTO();
		Cursor c = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String date_now = Constants.STR_BLANK;
		String trainingDate = data.getString(IntentConstants.INTENT_MONTH, Constants.STR_BLANK);
		int position = data.getInt(IntentConstants.INTENT_DATA, 2); //2: thang hien tai
		if(position < 2 ){
			date_now = DateUtils.getLastDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD, position - 2);
		} else if(position == 2){
			date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		}
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		// ds nhan vien cua GS
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffOfSupervisor = staff
				.getListStaffOfSupervisor(String.valueOf(staffId),
						String.valueOf(shopId));

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT  stp.sup_training_plan_id AS TRAINING_PLAN_ID, ");
		sqlQuery.append("        stp.[SUP_STAFF_ID] AS SUP_STAFF_ID, ");
		sqlQuery.append("        sh.shop_id AS SHOP_ID, ");
		sqlQuery.append("        sh.shop_code AS SHOP_CODE, ");
		sqlQuery.append("        sh.shop_name AS SHOP_NAME, ");
		sqlQuery.append("        s.staff_id AS STAFF_ID, ");
		sqlQuery.append("        s.staff_code AS STAFF_CODE, ");
		sqlQuery.append("        s.STAFF_NAME AS STAFF_NAME, ");
		sqlQuery.append("        gs.staff_id AS STAFF_ID_GS, ");
		sqlQuery.append("        gs.staff_code AS STAFF_CODE_GS, ");
		sqlQuery.append("        gs.STAFF_NAME AS STAFF_NAME_GS, ");
		sqlQuery.append("        stp.training_month AS MONTH, ");
		sqlQuery.append("        stp.IS_SEND AS IS_SEND, ");
		sqlQuery.append("        stp.training_date AS DATE, ");
		sqlQuery.append("        sl.[amount_plan] AS AMOUNT_PLAN, ");
		sqlQuery.append("        sl.amount AS AMOUNT ");
		sqlQuery.append("FROM  sup_training_plan stp, ");
		sqlQuery.append("      staff gs, ");
		sqlQuery.append("      (SELECT shop_id, ");
		sqlQuery.append("              shop_code, ");
		sqlQuery.append("              shop_name ");
		sqlQuery.append("      FROM shop ");
		sqlQuery.append("      WHERE [PARENT_SHOP_ID] in (SELECT shop_id ");
		sqlQuery.append("                                FROM shop ");
		sqlQuery.append("                                WHERE [PARENT_SHOP_ID] = ? ) ");
		params.add(""+shopId);
		sqlQuery.append("            OR SHOP_ID  in (SELECT shop_id ");
		sqlQuery.append("                            FROM shop ");
		sqlQuery.append("                            WHERE [PARENT_SHOP_ID] = ?  )) sh, ");
		params.add(""+shopId);
		sqlQuery.append("          staff s ");
		sqlQuery.append(" LEFT JOIN ");
//		sqlQuery.append("   left join (select s.staff_id, ");
//		sqlQuery.append("                     rssd.sale_date, ");
//		sqlQuery.append("                     VP.START_DATE, ");
//		sqlQuery.append("                     sum(rssd.amount_plan) AS amount_plan, ");
//		sqlQuery.append("                     sum(rssd.amount) AS amount ");
//		sqlQuery.append("              from visit_plan vp, ");
//		sqlQuery.append("                   staff s, ");
//		sqlQuery.append("                   customer ct ");
//		sqlQuery.append("                left join rpt_staff_sale_detail rssd on ( rssd.customer_id = ct.customer_id and rssd.shop_id = s.shop_id and rssd.is_or =0 ) ");
//		sqlQuery.append("              where 1=1 ");
//		sqlQuery.append("                    and vp.active =1 ");
//		sqlQuery.append("                    and ct.status =1 ");
//		sqlQuery.append("                    and s.status =1 ");
//		sqlQuery.append("                    and s.role_type =1 ");
//		sqlQuery.append("                    and rssd.sale_date not null ");
//		sqlQuery.append("                    and vp.customer_id = ct.customer_id ");
//		sqlQuery.append("                    and s.staff_id = vp.staff_id ");
//		sqlQuery.append("                    and IFNULL(Substr(VP.START_DATE, 1, 10) <= ?, 0) ");
//		params.add(""+date_now);
//		sqlQuery.append("                    and (VP.END_DATE IS NULL OR Substr(VP.END_DATE, 1, 10) >= ? ) ");
//		params.add(""+date_now);
//		sqlQuery.append("              group by rssd.sale_date, vp.staff_id ) SL ");

		sqlQuery.append("    (SELECT s.staff_id, ");
		sqlQuery.append("            rssd.sale_date, ");
		sqlQuery.append("            VP.FROM_DATE, ");
		sqlQuery.append("            sum(rssd.amount_plan) AS amount_plan, ");
		sqlQuery.append("            sum(rssd.amount) AS amount ");
		sqlQuery.append("        FROM    ");
		sqlQuery.append("               routing R, ");
		sqlQuery.append("               routing_customer RC, ");
		sqlQuery.append("               visit_plan VP, ");
		sqlQuery.append("               staff s, ");
		sqlQuery.append("               customer CT ");
		sqlQuery.append("     		    LEFT JOIN ");
		sqlQuery.append("           			 rpt_staff_sale_detail rssd ");
		sqlQuery.append("                 ON (rssd.customer_id = ct.customer_id and rssd.shop_id = s.shop_id ");
		sqlQuery.append("                         AND rssd.is_or =0 ) ");
		sqlQuery.append("        WHERE  CT.status = 1 ");
		sqlQuery.append("               AND VP.shop_id = ? ");
		params.add(""+shopId);
		sqlQuery.append("               AND R.status = 1 ");
		sqlQuery.append("               AND RC.status = 1 ");
		sqlQuery.append("               AND R.routing_id = VP.routing_id ");
		sqlQuery.append("               AND VP.status = 1 ");
		sqlQuery.append("               AND R.routing_id = RC.routing_id ");
		sqlQuery.append("               AND CT.customer_id = RC.customer_id ");
		sqlQuery.append("               AND s.status =1 ");
		sqlQuery.append("               AND rssd.sale_date not null ");
		sqlQuery.append("               AND s.staff_id = vp.staff_id ");
		sqlQuery.append("        		AND s.staff_id in (" + listStaffOfSupervisor + ")");
		sqlQuery.append("               AND IFNULL(Substr(VP.FROM_DATE, 1, 10) <= ?, 0) ");
		params.add(""+date_now);
		sqlQuery.append("               AND ( VP.TO_DATE IS NULL ");
		sqlQuery.append("                   OR Substr(VP.TO_DATE, 1, 10) >= ?  ) ");
		params.add(""+date_now);
		sqlQuery.append("               AND CT.status = 1  ");
		sqlQuery.append("        GROUP BY rssd.sale_date, ");
		sqlQuery.append("                 vp.staff_id  ) SL  ");

		sqlQuery.append("   on ( sl.sale_date = stp.training_date  ");
		sqlQuery.append("        and s.staff_id = sl.staff_id  ");
		sqlQuery.append("        and IFNULL(Substr(SL.FROM_DATE, 1, 10) <= Substr(stp.training_date,1,10), 0) ) ");
		sqlQuery.append("WHERE  1=1 ");
		sqlQuery.append("       AND s.status =1 ");
		sqlQuery.append("       AND gs.status =1 ");
		sqlQuery.append("       AND stp.sup_staff_id = ? ");
		params.add(""+staffId);
//		sqlQuery.append("       AND s.staff_id = stp.staff_id ");
//		sqlQuery.append("       AND s.[SHOP_ID] = sh.shop_id ");
//		sqlQuery.append("       AND gs.[SHOP_ID] = sh.shop_id ");
//		sqlQuery.append("       AND s.staff_owner_id = gs.staff_id ");
//		sqlQuery.append("       AND substr(stp.training_month,1,7) = substr(? ,1,7) ");
//		params.add(""+date_now);
//		sqlQuery.append("       AND substr(stp.training_date,1,10) <= substr(? ,1,10) ");
//		params.add(""+date_now);
//		sqlQuery.append("       AND stp.shop_id = sh.shop_id ");
//		sqlQuery.append("ORDER BY stp.training_date DESC ");

		sqlQuery.append("       AND s.staff_id = stp.staff_id ");
		sqlQuery.append("       AND s.[SHOP_ID] = sh.shop_id ");
		// nhan vien thuoc quyen quan li cua gs
		sqlQuery.append("		AND s.staff_id IN (SELECT PS.staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.staff_id = gs.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
		sqlQuery.append("       AND substr(stp.training_month,1,7) = substr(? ,1,7) ");
		params.add("" + trainingDate);
		sqlQuery.append("       AND substr(stp.training_date,1,10) <= substr(? ,1,10) ");
		params.add("" + date_now);
		sqlQuery.append("       AND stp.shop_id = sh.shop_id ");
		sqlQuery.append(" ORDER BY stp.training_date DESC ");


		String getTotalCount = "";
		if (page > 0) {
			if (isGetTotalPage == 1) {
				// get count
				getTotalCount = " select count(*) as TOTAL_ROW from ("
						+ sqlQuery + ")";
			}

			sqlQuery.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlQuery
					.append(" offset "
							+ Integer.toString((page - 1)
									* Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c!=null && c.moveToFirst()) {
				do {
					SupTrainingPlanDTO item = new SupTrainingPlanDTO();
					item.initDataTrainingPlanTBHV(c);
					dto.listResult.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		Cursor cTotalRow = null;
		try{
			if (isGetTotalPage ==1) {
				cTotalRow = rawQueries(getTotalCount, params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalSize(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("UnexceptionLog", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		} finally {
			// Close cursor lay so luong dong
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				MyLog.e("Quang", e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * Lay du lieu lich su huan luyen trong thang cua GSNPP
	 * @author: hoanpd1
	 * @update: tuanlt11
	 * @since: 11:07:15 20-11-2014
	 * @return: TBHVDayTrainingSupervisionDTO
	 * @throws:
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public ListSubTrainingPlanDTO getListTrainingHistoryGSNPP(Bundle data) throws Exception {
		ListSubTrainingPlanDTO dto = new ListSubTrainingPlanDTO();
		Cursor c = null;
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		int isGetTotalPage = data.getInt(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String date_now = Constants.STR_BLANK;
		String trainingDate = data.getString(IntentConstants.INTENT_MONTH, Constants.STR_BLANK);
		int position = data.getInt(IntentConstants.INTENT_DATA, 2); //2: thang hien tai
		if(position < 2 ){
			date_now = DateUtils.getLastDateOfNumberPreviousMonthWithFormat(DateUtils.DATE_STRING_YYYY_MM_DD, position - 2);
		} else if(position == 2){
			date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		}
		int staffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		int shopId = data.getInt(IntentConstants.INTENT_SHOP_ID);
		// ds nhan vien cua GS
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		String listStaffOfSupervisor = staff.getListStaffOfSupervisor(String.valueOf(staffId), String.valueOf(shopId));

		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  sqlQuery = new StringBuffer();
		sqlQuery.append(" SELECT  stp.sup_training_plan_id AS TRAINING_PLAN_ID, ");
		sqlQuery.append("         stp.[SUP_STAFF_ID] AS SUP_STAFF_ID, ");
		sqlQuery.append("         sh.shop_id AS SHOP_ID, ");
		sqlQuery.append("         sh.shop_code AS SHOP_CODE, ");
		sqlQuery.append("         sh.shop_name AS SHOP_NAME, ");
		sqlQuery.append("         s.staff_id AS STAFF_ID, ");
		sqlQuery.append("         s.staff_code AS STAFF_CODE, ");
		sqlQuery.append("         s.staff_name AS STAFF_NAME, ");
		sqlQuery.append("         gs.staff_id AS STAFF_ID_GS, ");
		sqlQuery.append("         gs.staff_code AS STAFF_CODE_GS, ");
		sqlQuery.append("         gs.staff_name AS STAFF_NAME_GS, ");
		sqlQuery.append("         stp.training_month AS MONTH, ");
		sqlQuery.append("         stp.IS_SEND AS IS_SEND, ");
		sqlQuery.append("         stp.training_date AS DATE, ");
		sqlQuery.append("         sl.[amount_plan] AS AMOUNT_PLAN, ");
		sqlQuery.append("         sl.amount AS AMOUNT, ");
		sqlQuery.append("         sl.AMOUNT_APPROVED AS AMOUNT_APPROVED ");
		sqlQuery.append(" FROM sup_training_plan stp, ");
		sqlQuery.append("     staff gs, ");
		sqlQuery.append("    (SELECT shop_id, ");
		sqlQuery.append("            shop_code, ");
		sqlQuery.append("            shop_name ");
		sqlQuery.append("    FROM ");
		sqlQuery.append("        shop ");
		sqlQuery.append("    WHERE shop_id = ? ");
		params.add(""+shopId);
		sqlQuery.append("         OR SHOP_ID  in (SELECT shop_id ");
		sqlQuery.append("                        FROM shop ");
		sqlQuery.append("                        WHERE [PARENT_SHOP_ID] = ?  ) ) sh, ");
		params.add(""+shopId);
		sqlQuery.append("    staff s ");
		sqlQuery.append(" LEFT JOIN ");
		sqlQuery.append("    (SELECT s.staff_id, ");
		sqlQuery.append("            rssd.sale_date, ");
		sqlQuery.append("            VP.FROM_DATE, ");
		sqlQuery.append("            sum(rssd.amount_plan) AS amount_plan, ");
		sqlQuery.append("            sum(rssd.amount) AS amount, ");
		sqlQuery.append("            sum(rssd.AMOUNT_APPROVED) AS AMOUNT_APPROVED ");
		sqlQuery.append("        FROM    ");
		sqlQuery.append("               routing R, ");
		sqlQuery.append("               routing_customer RC, ");
		sqlQuery.append("               visit_plan VP, ");
		sqlQuery.append("               staff s, ");
		sqlQuery.append("               customer CT ");
		sqlQuery.append("     		    LEFT JOIN ");
		sqlQuery.append("           			 rpt_staff_sale_detail rssd ");
		sqlQuery.append("                 ON (rssd.customer_id = ct.customer_id and rssd.shop_id = s.shop_id ");
		sqlQuery.append("                         AND rssd.is_or =0 ) ");
		sqlQuery.append("        WHERE  CT.status = 1 ");
		sqlQuery.append("               AND VP.shop_id = ? ");
		params.add(""+shopId);
		sqlQuery.append("               AND R.status = 1 ");
		sqlQuery.append("               AND RC.status = 1 ");
		sqlQuery.append("               AND R.routing_id = VP.routing_id ");
		sqlQuery.append("               AND VP.status = 1 ");
		sqlQuery.append("               AND R.routing_id = RC.routing_id ");
		sqlQuery.append("               AND CT.customer_id = RC.customer_id ");
		sqlQuery.append("               AND s.status =1 ");
		sqlQuery.append("               AND rssd.sale_date not null ");
		sqlQuery.append("               AND s.staff_id = vp.staff_id ");
		sqlQuery.append("        		AND s.staff_id in (" + listStaffOfSupervisor + ")");
		sqlQuery.append("               AND IFNULL(Substr(VP.FROM_DATE, 1, 10) <= ?, 0) ");
		params.add(""+date_now);
		sqlQuery.append("               AND ( VP.TO_DATE IS NULL ");
		sqlQuery.append("                   OR Substr(VP.TO_DATE, 1, 10) >= ?  ) ");
		params.add(""+date_now);
		sqlQuery.append("               AND CT.status = 1  ");
		sqlQuery.append("        GROUP BY rssd.sale_date, ");
		sqlQuery.append("                 vp.staff_id  ) SL  ");
		sqlQuery.append("        ON ( Substr(sl.sale_date,1,10) = Substr(stp.training_date,1,10)  ");
		sqlQuery.append("             and s.staff_id = sl.staff_id  ");
		sqlQuery.append("             and IFNULL(Substr(SL.FROM_DATE, 1, 10) <= Substr(stp.training_date,1,10), 0) ) ");
		sqlQuery.append(" WHERE 1=1 ");
		sqlQuery.append("       AND s.status =1 ");
		sqlQuery.append("       AND gs.status =1 ");
		sqlQuery.append("       AND gs.staff_id = ? ");
		params.add(""+staffId);
		// phan quyen quan li cua GS
		sqlQuery.append("       AND stp.sup_staff_id IN ");
		sqlQuery.append("			(SELECT PS.parent_staff_id FROM PARENT_STAFF_MAP PS WHERE 1=1	");
		sqlQuery.append("				AND PS.staff_id = gs.staff_id AND PS.STATUS = 1 ");
		sqlQuery.append("				AND not exists (select * from EXCEPTION_USER_ACCESS where STAFF_ID = PS.STAFF_ID and PARENT_STAFF_ID = PS.PARENT_STAFF_ID and status = 1))   ");
//		sqlQuery.append("       AND stp.is_send = 1 ");
		sqlQuery.append("       AND s.staff_id = stp.staff_id ");
		sqlQuery.append("       AND s.[SHOP_ID] = sh.shop_id ");
		// nhan vien thuoc quyen quan li cua gs
		sqlQuery.append("       AND s.staff_id in (" + listStaffOfSupervisor + ")");
		sqlQuery.append("       AND substr(stp.training_month,1,7) = substr(? ,1,7) ");
		params.add(""+trainingDate);
		sqlQuery.append("       AND substr(stp.training_date,1,10) <= substr(? ,1,10) ");
		params.add(""+date_now);
		sqlQuery.append("       AND stp.shop_id = sh.shop_id ");
		sqlQuery.append(" ORDER BY stp.training_date DESC ");


		String getTotalCount = "";
		if (page > 0) {
			if (isGetTotalPage == 1) {
				// get count
				getTotalCount = " select count(*) as TOTAL_ROW from ("
						+ sqlQuery + ")";
			}

			sqlQuery.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			sqlQuery
					.append(" offset "
							+ Integer.toString((page - 1)
									* Constants.NUM_ITEM_PER_PAGE));
		}

		try {
			c = this.rawQueries(sqlQuery.toString(), params);
			if (c!=null && c.moveToFirst()) {
				do {
					SupTrainingPlanDTO item = new SupTrainingPlanDTO();
					item.initDataTrainingPlanTBHV(c);
					dto.listResult.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			// dto = null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}

		Cursor cTotalRow = null;
		try{
			if (isGetTotalPage ==1) {
				cTotalRow = rawQueries(getTotalCount, params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.setTotalSize(cTotalRow.getInt(0));
					}
				}
			}
		} catch (Exception e) {
			MyLog.e("UnexceptionLog", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		} finally {
			// Close cursor lay so luong dong
			try {
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				MyLog.e("Quang", e.getMessage());
			}
		}
		return dto;
	}

	public NVBHReportCustomerSaleViewDTO getReportCustomerSale(Bundle data) throws Exception {
		final NVBHReportCustomerSaleViewDTO dto = new NVBHReportCustomerSaleViewDTO();
		int numItemOnPage = Constants.NUM_ITEM_PER_PAGE;

		int currentPage = data.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = data.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String searchText = data.getString(IntentConstants.INTENT_SEARCH);
		
		String dateStr = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		EXCEPTION_DAY_TABLE exceptionTable = new EXCEPTION_DAY_TABLE(mDB);
		CycleInfo cycleInfo = exceptionTable.getCycleInfoOfDate(dateStr);
		
		Date beginDateCycle = DateUtils.parseDateFromString(cycleInfo.beginDate, DateUtils.DATE_FORMAT_DATE);
		Calendar beginCalenderCycle = Calendar.getInstance();
		beginCalenderCycle.setTime(beginDateCycle);
		beginCalenderCycle.add(Calendar.DATE, -1);
		String datePreCycleStr = DateUtils.formatDate(beginCalenderCycle.getTime(), DateUtils.DATE_FORMAT_DATE);
		CycleInfo cyclePreInfo = exceptionTable.getCycleInfoOfDate(datePreCycleStr);
		
		ArrayList<String> paramsObject = new ArrayList<String>();
		StringBuffer sqlObject = new StringBuffer();
		
		sqlObject.append("	SELECT	* FROM (");
		sqlObject.append("	SELECT	");
		sqlObject.append("	    CT.SHORT_CODE AS CUSTOMER_CODE,	");
		sqlObject.append("	    CT.CUSTOMER_ID as CUSTOMER_ID,	");
		sqlObject.append("	    CT.CUSTOMER_NAME AS CUSTOMER_NAME	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    VISIT_PLAN VP,	");
		sqlObject.append("	    ROUTING RT,	");
		sqlObject.append("	    ROUTING_CUSTOMER RTC,	");
		sqlObject.append("	    CUSTOMER CT	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    VP.ROUTING_ID = RT.ROUTING_ID	");
		sqlObject.append("	    AND rt.routing_Id = rtc.routing_id	");
		sqlObject.append("	    AND rtc.customer_id = ct.customer_id	");
		sqlObject.append("	    AND vp.status = 1	");
		sqlObject.append("	    AND vp.shop_id = ?	");
		paramsObject.add(String.valueOf(shopId));
		sqlObject.append("	    AND vp.STAFF_ID = ?	");
		paramsObject.add(String.valueOf(staffId));
		sqlObject.append("	    AND rt.status = 1	");
		sqlObject.append("	    AND rtc.status = 1	");
		sqlObject.append("	    AND ct.status = 1	");
		sqlObject.append("	    AND ct.status = 1	");
		
		if (!StringUtil.isNullOrEmpty(searchText)) {
			searchText = StringUtil.getEngStringFromUnicodeString(searchText);
			searchText = StringUtil.escapeSqlString(searchText);
			sqlObject.append("	AND ( upper(ct.SHORT_CODE) like upper(?) ");
			paramsObject.add("%" + searchText + "%");
			sqlObject.append("	OR upper(ct.NAME_TEXT) like upper(?)) ");
			paramsObject.add("%" + searchText + "%");
		}
		
		sqlObject.append("	    AND substr(VP.FROM_DATE, 1, 10) <= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	    AND (	");
		sqlObject.append("	        substr(VP.TO_DATE, 1, 10) >= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	        OR VP.TO_DATE IS NULL	");
		sqlObject.append("	    )	");
		sqlObject.append("	    AND substr(RTC.START_DATE, 1, 10) <= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	    AND (	");
		sqlObject.append("	        substr(RTC.END_DATE, 1, 10) >= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	        OR RTC.END_DATE IS NULL	");
		sqlObject.append("	    ))	CUS");
		sqlObject.append("	    LEFT JOIN");
		sqlObject.append("	(SELECT	");
		sqlObject.append("	    RPTSALE.CUSTOMER_ID RPT_CUSTOMER_ID,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN AMOUNT_APPROVED	");
		paramsObject.add(String.valueOf(cyclePreInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_AMOUNT_APPROVED_PRE,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN AMOUNT_APPROVED	");
		paramsObject.add(String.valueOf(cycleInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_AMOUNT_APPROVED,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN AMOUNT	");
		paramsObject.add(String.valueOf(cycleInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_AMOUNT,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN QUANTITY_APPROVED	");
		paramsObject.add(String.valueOf(cyclePreInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_QUANTITY_APPROVED_PRE,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN QUANTITY_APPROVED	");
		paramsObject.add(String.valueOf(cycleInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_QUANTITY_APPROVED,	");
		sqlObject.append("	    SUM(CASE	");
		sqlObject.append("	        WHEN rptSale.CYCLE_ID = ? THEN QUANTITY	");
		paramsObject.add(String.valueOf(cycleInfo.cycleId));
		sqlObject.append("	        ELSE 0	");
		sqlObject.append("	    END) MONTH_QUANTITY	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    RPT_SALE_PRIMARY_MONTH rptSale	");
		sqlObject.append("	WHERE 1=1	");
		sqlObject.append("	    AND rptSale.SHOP_ID     = ?	");
		paramsObject.add(String.valueOf(shopId));
		sqlObject.append("	    AND rptSale.STAFF_ID   = ?	");
		paramsObject.add(String.valueOf(staffId));
		sqlObject.append("	    AND rptSale.CYCLE_ID   IN (	");
		sqlObject.append("	        ?, ?	");
		paramsObject.add(String.valueOf(cycleInfo.cycleId));
		paramsObject.add(String.valueOf(cyclePreInfo.cycleId));
		sqlObject.append("	    )	");
		sqlObject.append("	GROUP BY RPTSALE.CUSTOMER_ID) RPT	");
		sqlObject.append("	ON 	CUS.CUSTOMER_ID = RPT.RPT_CUSTOMER_ID ");

		String sqlStr = sqlObject.toString();

		//lay tong so dong san pham
		if (isGetTotalPage) {
			dto.totalData = cursorRunner.queryCount(sqlStr, paramsObject);
		}
		
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String defaultOrderByStr = "order by CUSTOMER_CODE asc";
		String orderByStr = new DMSSortQueryBuilder()
    		.addMapper(SortActionConstants.CODE, "CUSTOMER_CODE")
    		.addMapper(SortActionConstants.NAME, "CUSTOMER_NAME")
    		.addMapper(SortActionConstants.AMOUNT_APPROVED_PRE, "MONTH_AMOUNT_APPROVED_PRE")
    		.addMapper(SortActionConstants.AMOUNT_APPROVED, "MONTH_AMOUNT_APPROVED")
    		.addMapper(SortActionConstants.AMOUNT, "MONTH_AMOUNT")
    		.addMapper(SortActionConstants.QUANITY_APPROVED_PRE, "MONTH_QUANTITY_APPROVED_PRE")
    		.addMapper(SortActionConstants.QUANITY_APPROVED, "MONTH_QUANTITY_APPROVED")
    		.addMapper(SortActionConstants.QUANITY, "MONTH_QUANTITY")
    		.defaultOrderString(defaultOrderByStr )
    		.build(sortInfo);
		
		//add order info
		sqlObject.append(orderByStr);
		sqlStr = sqlObject.toString();
		
		//lay ds san pham o trang currentPage, moi trang co numItemOnPage san pham
		cursorRunner.query(sqlStr, paramsObject, currentPage, numItemOnPage, new ICursorActionQuery() {

			@Override
			public void onMoveCursor(DMSCursor c) throws Exception {
				CustomerSaleOfNVBHDTO item = new CustomerSaleOfNVBHDTO();
				item.initDataWithCursor(c.cursor);
				dto.listDto.add(item );
			}
		});
		return dto;
	}

	/**
	 * get report ASO for NVBH
	 * @author: duongdt3
	 * @time: 2:31:23 PM Oct 20, 2015
	 * @param viewInfo
	 * @return
	 * @throws Exception 
	*/
	public NvbhReportAsoViewDTO getReportNvbhAso(Bundle data) throws Exception {
		final NvbhReportAsoViewDTO dto = new NvbhReportAsoViewDTO();
		int numItemOnPage = Constants.NUM_ITEM_PER_PAGE;

		int currentPage = data.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = data.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String routeId = data.getString(IntentConstants.INTENT_ROUTING_ID);
		boolean isGetUnplan = data.getBoolean(IntentConstants.INTENT_GET_UNPLAN);
		
		String dateStr = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		EXCEPTION_DAY_TABLE exceptionTable = new EXCEPTION_DAY_TABLE(mDB);
		CycleInfo cycleInfo = exceptionTable.getCycleInfoOfDate(dateStr);
		String cycleId = String.valueOf(cycleInfo.cycleId);
		
		String colDoneASO = "";
		int kpiRouteType = GlobalInfo.getInstance().getKpiRouteType();
		switch (kpiRouteType) {
		case GlobalInfo.KPI_ROUTE_TYPE_IN_ROUTE:
			colDoneASO = "DONE_IR";
			break;
		case GlobalInfo.KPI_ROUTE_TYPE_OUT_ROUTE:
			colDoneASO = "DONE_OR";
			break;
		case GlobalInfo.KPI_ROUTE_TYPE_ALL:
			colDoneASO = "DONE";
			break;
		default:
			colDoneASO = "DONE_IR";
			break;
		}
		
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    OBJECT_ID as OBJECT_ID,	");
		sqlObject.append("	    OBJECT_TYPE as OBJECT_TYPE,	");
		sqlObject.append("	    PLAN as PLAN,	");
		sqlObject.append("	    ").append(colDoneASO).append(" as DONE,	");
		sqlObject.append("	    (case	");
		sqlObject.append("	        WHEN OBJECT_TYPE = 1 then PRODUCT_CODE	");
		sqlObject.append("	        ELSE PRODUCT_INFO_CODE	");
		sqlObject.append("	    END) CODE,	");
		sqlObject.append("	    (case	");
		sqlObject.append("	        WHEN OBJECT_TYPE = 1 then PRODUCT_NAME	");
		sqlObject.append("	        ELSE PRODUCT_INFO_NAME	");
		sqlObject.append("	    END) NAME	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (     SELECT	");
		sqlObject.append("	        aso.OBJECT_ID,	");
		sqlObject.append("	        aso.OBJECT_TYPE ,	");
		sqlObject.append("	        aso.PLAN ,	");
		sqlObject.append("	        aso.").append(colDoneASO).append(",	");
		sqlObject.append("	        pd.PRODUCT_ID,	");
		sqlObject.append("	        pd.PRODUCT_CODE,	");
		sqlObject.append("	        pd.PRODUCT_NAME,	");
		sqlObject.append("	        pdinfo.PRODUCT_INFO_ID,	");
		sqlObject.append("	        pdinfo.PRODUCT_INFO_CODE,	");
		sqlObject.append("	        pdinfo.PRODUCT_INFO_NAME	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        (    SELECT	");
		sqlObject.append("	            RT.ROUTING_ID	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            VISIT_PLAN VP,	");
		sqlObject.append("	            ROUTING RT	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            VP.ROUTING_ID = RT.ROUTING_ID	");
		if (!StringUtil.isNullOrEmpty(routeId)) {
			sqlObject.append("	        AND ? = RT.ROUTING_ID	");
			paramsObject.add(routeId);
		}
		sqlObject.append("	            AND substr(VP.FROM_DATE,1, 10) <= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	            AND (	");
		sqlObject.append("	                substr(VP.TO_DATE, 1, 10) >= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	                OR VP.TO_DATE IS NULL	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND VP.STAFF_ID = ?	");
		paramsObject.add(staffId);
		sqlObject.append("	            AND VP.STATUS in (	");
		sqlObject.append("	                0, 1	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND VP.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	            AND RT.STATUS = 1) RT	");
		sqlObject.append("	    JOIN	");
		sqlObject.append("	        ASO_IMPLEMENT aso	");
		sqlObject.append("	            ON aso.ROUTING_ID = RT.ROUTING_ID	");
		sqlObject.append("	    LEFT JOIN	");
		sqlObject.append("	        product pd	");
		sqlObject.append("	            ON aso.object_type = 1	");
		sqlObject.append("	            AND aso.object_id = pd.product_id	");
		sqlObject.append("	    LEFT JOIN	");
		sqlObject.append("	        product_info pdinfo	");
		sqlObject.append("	            ON aso.object_type = 2	");
		sqlObject.append("	            AND aso.object_id  = pdinfo.PRODUCT_INFO_ID	");
		sqlObject.append("	            AND pdinfo.type = 2	");
		sqlObject.append("	    WHERE	");
		sqlObject.append("	        1=1	");
		if (isGetUnplan) {
			sqlObject.append("	        AND ((aso.PLAN is not null AND aso.PLAN > 0) or aso.").append(colDoneASO).append(" > 0)	");
		} else{
			sqlObject.append("	        AND (aso.PLAN is not null AND aso.PLAN > 0)	");
		}
		sqlObject.append("	        AND aso.staff_id = ?	");
		paramsObject.add(staffId);
		sqlObject.append("	        AND aso.shop_id = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	        AND aso.OBJECT_TYPE IN (1, 2)	");
		sqlObject.append("	        AND aso.CYCLE_ID = ?	");
		paramsObject.add(cycleId);
		sqlObject.append("	        AND (	");
		sqlObject.append("	            pd.product_id is not null	");
		sqlObject.append("	            OR pdinfo.PRODUCT_INFO_ID is not null	");
		sqlObject.append("	        )	");
		sqlObject.append("	    )	");

		String sqlStr = sqlObject.toString();

		//lay tong so dong san pham
		if (isGetTotalPage) {
			dto.totalData = cursorRunner.queryCount(sqlStr, paramsObject);
			
			StringBuffer sqlTotalObject = new StringBuffer();
			ArrayList<String> paramsTotalObject = new ArrayList<String>();
			sqlTotalObject.append("	SELECT	");
			sqlTotalObject.append("	    PLAN as PLAN,	");
			sqlTotalObject.append("	    ").append(colDoneASO).append(" as DONE	");
			sqlTotalObject.append("	FROM	");
			sqlTotalObject.append("	    (	");
			sqlTotalObject.append("		SELECT	");
			sqlTotalObject.append("	        aso.PLAN ,	");
			sqlTotalObject.append("	        aso.").append(colDoneASO).append("	");
			sqlTotalObject.append("	    FROM	");
			sqlTotalObject.append("	        (    SELECT	");
			sqlTotalObject.append("	            RT.ROUTING_ID	");
			sqlTotalObject.append("	        FROM	");
			sqlTotalObject.append("	            VISIT_PLAN VP,	");
			sqlTotalObject.append("	            ROUTING RT	");
			sqlTotalObject.append("	        WHERE	");
			sqlTotalObject.append("	            VP.ROUTING_ID = RT.ROUTING_ID	");
			sqlTotalObject.append("	            AND substr(VP.FROM_DATE,1, 10) <= ?	");
			paramsTotalObject.add(dateStr);
			sqlTotalObject.append("	            AND (	");
			sqlTotalObject.append("	                substr(VP.TO_DATE, 1, 10) >= ?	");
			paramsTotalObject.add(dateStr);
			sqlTotalObject.append("	                OR VP.TO_DATE IS NULL	");
			sqlTotalObject.append("	            )	");
			sqlTotalObject.append("	            AND VP.STAFF_ID = ?	");
			paramsTotalObject.add(staffId);
			sqlTotalObject.append("	            AND VP.STATUS in (	");
			sqlTotalObject.append("	                0, 1	");
			sqlTotalObject.append("	            )	");
			sqlTotalObject.append("	            AND VP.SHOP_ID = ?	");
			paramsTotalObject.add(shopId);
			sqlTotalObject.append("	            AND RT.STATUS = 1) RT	");
			sqlTotalObject.append("	    JOIN	");
			sqlTotalObject.append("	        ASO_IMPLEMENT aso	");
			sqlTotalObject.append("	            ON aso.ROUTING_ID = RT.ROUTING_ID	");
			sqlTotalObject.append("	    WHERE	");
			sqlTotalObject.append("	        1=1	");
			sqlTotalObject.append("	        AND aso.staff_id = ?	");
			paramsTotalObject.add(staffId);
			sqlTotalObject.append("	        AND aso.shop_id = ?	");
			paramsTotalObject.add(shopId);
			sqlTotalObject.append("	        AND aso.OBJECT_TYPE IN (3)	");
			sqlTotalObject.append("	        AND aso.CYCLE_ID = ?	");
			paramsTotalObject.add(cycleId);
			sqlTotalObject.append("	    )	");
			
			cursorRunner.query(sqlTotalObject.toString(), paramsTotalObject, new ICursorActionQuery() {
				
				@Override
				public void onMoveCursor(DMSCursor c) throws Exception {
					dto.totalPlan = CursorUtil.getLong(c.cursor, "PLAN");
					dto.totalDone = CursorUtil.getLong(c.cursor, "DONE");
					dto.totalPercent = StringUtil.calPercentUsingRound(dto.totalPlan, dto.totalDone);
				}
			});
		}
		
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String defaultOrderByStr = "order by OBJECT_TYPE asc, CODE asc";
		String orderByStr = new DMSSortQueryBuilder()
    		.addMapper(SortActionConstants.CODE, "CODE")
    		.addMapper(SortActionConstants.NAME, "NAME")
    		.addMapper(SortActionConstants.AMOUNT_DONE, "DONE")
    		.addMapper(SortActionConstants.AMOUNT_PLAN, "PLAN")
    		.addMapper(SortActionConstants.TYPE, "OBJECT_TYPE")
    		.defaultOrderString(defaultOrderByStr)
    		.build(sortInfo);
		
		//add order info
		sqlObject.append(orderByStr);
		sqlStr = sqlObject.toString();
		
		//lay ds san pham o trang currentPage, moi trang co numItemOnPage san pham
		cursorRunner.query(sqlStr, paramsObject, currentPage, numItemOnPage, new ICursorActionQuery() {

			@Override
			public void onMoveCursor(DMSCursor c) throws Exception {
				NvbhReportAsoDTO item = new NvbhReportAsoDTO();
				item.initDataWithCursor(c.cursor);
				dto.listDto.add(item);
			}
		});
		return dto;
	}

	/**
	 * get report aso for sup
	 * @author: duongdt3
	 * @time: 6:57:30 AM Oct 23, 2015
	 * @param viewInfo
	 * @return
	 * @throws Exception 
	*/
	public SupReportAsoViewDTO getReportSupAso(Bundle data) throws Exception {
		final SupReportAsoViewDTO dto = new SupReportAsoViewDTO();
		int numItemOnPage = Constants.NUM_ITEM_PER_PAGE;

		int currentPage = data.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = data.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String shopIdData = data.getString(IntentConstants.INTENT_SHOP_ID);
		boolean isGetShopData = data.getBoolean(IntentConstants.INTENT_GET_SHOP_DATA);
		String shopId = "";
		SHOP_TABLE shop = new SHOP_TABLE(mDB);
		if (isGetShopData) {
			dto.lstShop = shop.getListShopNPPRecursive(shopIdData);
			if (dto.lstShop.isEmpty()) {
				shopId = "-1";
			} else{
				shopId = String.valueOf(dto.lstShop.get(0).shopId);
			}
		} else{
			shopId = shopIdData;
		}
		
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		String lstStaffId = staffTable.getListStaffOfSupervisor(String.valueOf(staffId), shopId);
		String dateStr = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		EXCEPTION_DAY_TABLE exceptionTable = new EXCEPTION_DAY_TABLE(mDB);
		CycleInfo cycleInfo = exceptionTable.getCycleInfoOfDate(dateStr);
		String cycleId = String.valueOf(cycleInfo.cycleId);
		
		String colDoneASO = "";
		int kpiRouteType = GlobalInfo.getInstance().getKpiRouteType();
		switch (kpiRouteType) {
		case GlobalInfo.KPI_ROUTE_TYPE_IN_ROUTE:
			colDoneASO = "DONE_IR";
			break;
		case GlobalInfo.KPI_ROUTE_TYPE_OUT_ROUTE:
			colDoneASO = "DONE_OR";
			break;
		case GlobalInfo.KPI_ROUTE_TYPE_ALL:
			colDoneASO = "DONE";
			break;
		default:
			colDoneASO = "DONE_IR";
			break;
		}
		
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    *	");
		sqlObject.append("	FROM	");
		sqlObject.append("	    (SELECT	");
		sqlObject.append("	        SHOP_ID,	");
		sqlObject.append("	        ROUTING_ID,	");
		sqlObject.append("	        ROUTING_CODE,	");
		sqlObject.append("	        ROUTING_NAME,	");
		sqlObject.append("	        STAFF_ID,	");
		sqlObject.append("	        STAFF_CODE,	");
		sqlObject.append("	        STAFF_NAME,	");
		sqlObject.append("	        OBJECT_TYPE,	");
		sqlObject.append("	        SUM(CASE	");
		sqlObject.append("	            WHEN PLAN > 0 THEN 1	");
		sqlObject.append("	            ELSE 0	");
		sqlObject.append("	        END) as PLAN,	");
		sqlObject.append("	        SUM(CASE	");
		sqlObject.append("	            WHEN PLAN > 0 AND DONE > 0	");
		sqlObject.append("	            	AND DONE >= PLAN THEN 1	");
		sqlObject.append("	            ELSE 0	");
		sqlObject.append("	        END) as DONE	");
		sqlObject.append("	    FROM	");
		sqlObject.append("	        ( SELECT	");
		sqlObject.append("	            OBJECT_ID as OBJECT_ID,	");
		sqlObject.append("	            OBJECT_TYPE as OBJECT_TYPE,	");
		sqlObject.append("	            PLAN as PLAN,	");
		sqlObject.append("	            DONE as DONE,	");
		sqlObject.append("	            (case	");
		sqlObject.append("	                WHEN OBJECT_TYPE = 1 then PRODUCT_CODE	");
		sqlObject.append("	                ELSE PRODUCT_INFO_CODE	");
		sqlObject.append("	            END) CODE,	");
		sqlObject.append("	            (case	");
		sqlObject.append("	                WHEN OBJECT_TYPE = 1 then PRODUCT_NAME	");
		sqlObject.append("	                ELSE PRODUCT_INFO_NAME	");
		sqlObject.append("	            END) NAME,	");
		sqlObject.append("	            ROUTING_ID as ROUTING_ID,	");
		sqlObject.append("	            ROUTING_CODE as ROUTING_CODE,	");
		sqlObject.append("	            ROUTING_NAME as ROUTING_NAME,	");
		sqlObject.append("	            SHOP_ID as SHOP_ID,	");
		sqlObject.append("	            STAFF_ID as STAFF_ID,	");
		sqlObject.append("	            STAFF_CODE as STAFF_CODE,	");
		sqlObject.append("	            STAFF_NAME as STAFF_NAME	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            (     SELECT	");
		sqlObject.append("	                aso.OBJECT_ID,	");
		sqlObject.append("	                aso.OBJECT_TYPE ,	");
		sqlObject.append("	                aso.PLAN ,	");
		sqlObject.append("	                aso.").append(colDoneASO).append(" DONE,	");
		sqlObject.append("	                pd.PRODUCT_ID,	");
		sqlObject.append("	                pd.PRODUCT_CODE,	");
		sqlObject.append("	                pd.PRODUCT_NAME,	");
		sqlObject.append("	                pdinfo.PRODUCT_INFO_ID,	");
		sqlObject.append("	                pdinfo.PRODUCT_INFO_CODE,	");
		sqlObject.append("	                pdinfo.PRODUCT_INFO_NAME,	");
		sqlObject.append("	                RT.ROUTING_ID,	");
		sqlObject.append("	                RT.ROUTING_CODE,	");
		sqlObject.append("	                RT.ROUTING_NAME,	");
		sqlObject.append("	                RT.SHOP_ID,	");
		sqlObject.append("	                RT.STAFF_ID,	");
		sqlObject.append("	                RT.STAFF_CODE,	");
		sqlObject.append("	                RT.STAFF_NAME	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	        (    SELECT	");
		sqlObject.append("	                RT.ROUTING_ID,	");
		sqlObject.append("	                RT.ROUTING_CODE,	");
		sqlObject.append("	                RT.ROUTING_NAME,	");
		sqlObject.append("	                st.STAFF_ID,	");
		sqlObject.append("	                st.STAFF_CODE,	");
		sqlObject.append("	                st.STAFF_NAME,	");
		sqlObject.append("	                VP.SHOP_ID	");
		sqlObject.append("	        FROM	");
		sqlObject.append("	            VISIT_PLAN VP,	");
		sqlObject.append("	            ROUTING RT, STAFF st	");
		sqlObject.append("	        WHERE	");
		sqlObject.append("	            VP.ROUTING_ID = RT.ROUTING_ID	");
		sqlObject.append("	            AND VP.STAFF_ID = st.STAFF_ID	");
		sqlObject.append("	            AND st.STATUS = 1	");
		sqlObject.append("	            AND substr(VP.FROM_DATE,1, 10) <= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	            AND (	");
		sqlObject.append("	                substr(VP.TO_DATE, 1, 10) >= ?	");
		paramsObject.add(dateStr);
		sqlObject.append("	                OR VP.TO_DATE IS NULL	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND VP.STAFF_ID IN (").append(lstStaffId).append(")	");
		sqlObject.append("	            AND VP.STATUS in (	");
		sqlObject.append("	                0, 1	");
		sqlObject.append("	            )	");
		sqlObject.append("	            AND VP.SHOP_ID = ?	");
		paramsObject.add(shopId);
		sqlObject.append("	            AND RT.STATUS = 1) RT	");
		sqlObject.append("	            LEFT JOIN	");
		sqlObject.append("	                ASO_IMPLEMENT aso	");
		sqlObject.append("	                    ON aso.ROUTING_ID = RT.ROUTING_ID AND aso.STAFF_ID = RT.STAFF_ID AND RT.SHOP_ID = aso.SHOP_ID ");
		sqlObject.append("	            			AND aso.PLAN > 0	");
		sqlObject.append("	                		AND aso.OBJECT_TYPE IN (1, 2)	");
		sqlObject.append("	                		AND aso.CYCLE_ID = ?	");
		paramsObject.add(cycleId);
		sqlObject.append("	            LEFT JOIN	");
		sqlObject.append("	                product pd	");
		sqlObject.append("	                    ON aso.object_type = 1	");
		sqlObject.append("	                    AND aso.object_id = pd.product_id	");
		sqlObject.append("	            LEFT JOIN	");
		sqlObject.append("	                product_info pdinfo	");
		sqlObject.append("	                    ON aso.object_type = 2	");
		sqlObject.append("	                    AND aso.object_id  = pdinfo.PRODUCT_INFO_ID	");
		sqlObject.append("	                    AND pdinfo.type = 2	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1=1	");
		sqlObject.append("	                AND (aso.ASO_IMPLEMENT_ID IS NULL or (aso.ASO_IMPLEMENT_ID is not null AND (pd.product_id is not null OR pdinfo.PRODUCT_INFO_ID is not null)))	");
		sqlObject.append("	                   )   )	");
		sqlObject.append("	        GROUP BY	");
		sqlObject.append("	            SHOP_ID,	");
		sqlObject.append("	            ROUTING_ID,	");
		sqlObject.append("	            ROUTING_CODE,	");
		sqlObject.append("	            ROUTING_NAME,	");
		sqlObject.append("	            STAFF_ID,	");
		sqlObject.append("	            STAFF_CODE,	");
		sqlObject.append("	            STAFF_NAME,	");
		sqlObject.append("	            OBJECT_TYPE   ) ASO	");
		sqlObject.append("	    LEFT JOIN	");
		sqlObject.append("	        (	");
		sqlObject.append("	            SELECT	");
		sqlObject.append("	                aso.ROUTING_ID ROUTING_ID_TOTAL,	");
		sqlObject.append("	                aso.SHOP_ID SHOP_ID_TOTAL,	");
		sqlObject.append("	                aso.STAFF_ID STAFF_ID_TOTAL,	");
		sqlObject.append("	                SUM(aso.PLAN) TOTAL_PLAN,	");
		sqlObject.append("	                SUM(aso.").append(colDoneASO).append(") TOTAL_DONE	");
		sqlObject.append("	            FROM	");
		sqlObject.append("	                ASO_IMPLEMENT aso	");
		sqlObject.append("	            WHERE	");
		sqlObject.append("	                1=1	");
		sqlObject.append("	                AND aso.CYCLE_ID = ?	");
		paramsObject.add(cycleId);
		sqlObject.append("	                AND aso.OBJECT_TYPE IN (3)	");
		sqlObject.append("	                AND aso.staff_id IN (").append(lstStaffId).append(")	");
		sqlObject.append("	                AND aso.shop_id IN (?)	");
		paramsObject.add(shopId);
		sqlObject.append("	            GROUP BY	");
		sqlObject.append("	                SHOP_ID,	");
		sqlObject.append("	                ROUTING_ID,	");
		sqlObject.append("	                STAFF_ID	");
		sqlObject.append("	        ) ASO_TOTAL	");
		sqlObject.append("	            ON ASO.STAFF_ID = ASO_TOTAL.STAFF_ID_TOTAL	");
		sqlObject.append("	            AND ASO.ROUTING_ID = ASO_TOTAL.ROUTING_ID_TOTAL	");
		sqlObject.append("	            AND ASO.SHOP_ID = ASO_TOTAL.SHOP_ID_TOTAL	");
		
		String sqlStr = sqlObject.toString();

		//lay tong so dong
		if (isGetTotalPage) {
			dto.totalData = cursorRunner.queryCount(sqlStr, paramsObject);
		}
		
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		String defaultOrderByStr = "ORDER BY ROUTING_CODE asc, ROUTING_NAME asc, STAFF_CODE asc, STAFF_NAME asc";
		String orderByStr = new DMSSortQueryBuilder()
    		.addMapper(SortActionConstants.STAFF_CODE, "STAFF_CODE")
    		.addMapper(SortActionConstants.STAFF_NAME, "STAFF_NAME")
    		.addMapper(SortActionConstants.ROUTE_CODE, "ROUTING_CODE")
    		.addMapper(SortActionConstants.ROUTE_NAME, "ROUTING_NAME")
    		.addMapper(SortActionConstants.AMOUNT_PLAN, "PLAN")
    		.addMapper(SortActionConstants.AMOUNT_DONE, "DONE")
    		.addMapper(SortActionConstants.TYPE, "OBJECT_TYPE")
    		.defaultOrderString(defaultOrderByStr)
    		.build(sortInfo);
		
		//add order info
		sqlObject.append(orderByStr);
		sqlStr = sqlObject.toString();
		
		//lay ds san pham o trang currentPage, moi trang co numItemOnPage san pham
		cursorRunner.query(sqlStr, paramsObject, currentPage, numItemOnPage, new ICursorActionQuery() {

			@Override
			public void onMoveCursor(DMSCursor c) throws Exception {
				SupReportAsoDTO item = new SupReportAsoDTO();
				item.initDataWithCursor(c.cursor);
				dto.listDto.add(item);
			}
		});
		return dto;
	}
}
