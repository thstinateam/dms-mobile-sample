/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ths.dmscore.dto.db.RoleUserDTO;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dms.R;

/**
 *  Popup danh sach nha phan phoi quan ly
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class ListShopManagedView extends ScrollView{
	private GlobalBaseActivity parent;
	public View viewLayout;
	private DMSTableView tbShop;
	private int actionSelectedShop = -1;
	private TextView tvTitle;

	public ListShopManagedView(Context context, int actionSelectedShop) {
		super(context);
		this.actionSelectedShop = actionSelectedShop;
		parent = (GlobalBaseActivity) context;
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater.inflate(R.layout.layout_shop_managed_view, null);
		tbShop = (DMSTableView) viewLayout.findViewById(R.id.tbShop);
		tvTitle = (TextView)viewLayout.findViewById(R.id.tvTitle);

	}

	/**
	* Render du lieu bang npp giam sat quan ly
	* @author: BangHN
	* @param listShop: danh sach nha phan phoi (id, name, code, street)
	* @return: void
	* @throws: Ngoai le do ham dua ra (neu co)
	*/
	public void renderLayoutShop(final List<ShopDTO> listShop) {
		tvTitle.setText(StringUtil.getString(R.string.TEXT_CHOOSE_SHOP_MANAGE));
		int pos = 1;
		tbShop.clearAllDataAndHeader();
		String[] CUSTOMER_LIST_TABLE_TITLES =  { StringUtil.getString(R.string.TEXT_DISTRIBUTOR),
				StringUtil.getString(R.string.TEXT_ADDRESS) };
		ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(CUSTOMER_LIST_TABLE_TITLES));
		tbShop.addHeader(new ShopManagedRow(parent, 0),stringList );
		if (listShop != null && listShop.size() > 0) {
			for (int i = 0, s = listShop.size(); i < s; i++) {
				ShopManagedRow row = new ShopManagedRow(parent, actionSelectedShop);
				row.renderLayoutShop(pos, listShop.get(i));
				pos++;
				tbShop.addRow(row);
			}
		}
	}

	public void renderLayoutRole(final ArrayList<RoleUserDTO> listRole) {
		tvTitle.setText(StringUtil.getString(R.string.TEXT_CHOOSE_ROLE_MANAGE));
		int pos = 1;
		tbShop.clearAllDataAndHeader();
		String[] CUSTOMER_LIST_TABLE_TITLES =  { StringUtil.getString(R.string.TEXT_HEADER_TABLE_ROLE_USER_CODE),
				StringUtil.getString(R.string.TEXT_HEADER_TABLE_ROLE_USER_NAME) };
		ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(CUSTOMER_LIST_TABLE_TITLES));
		tbShop.addHeader(new ShopManagedRow(parent, 0),stringList );
		if (listRole != null && listRole.size() > 0) {
			for (int i = 0, s = listRole.size(); i < s; i++) {
				ShopManagedRow row = new ShopManagedRow(parent, actionSelectedShop);
				row.renderLayoutRole(pos, listRole.get(i));
				pos++;
				tbShop.addRow(row);
			}
		}
	}


}
