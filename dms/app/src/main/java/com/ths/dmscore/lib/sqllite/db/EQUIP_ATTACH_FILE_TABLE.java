/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;

import net.sqlcipher.database.SQLiteDatabase;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.EquipAttachFileDTO;
import com.ths.dmscore.dto.view.ImageListItemDTO;
import com.ths.dmscore.dto.view.TakePhotoEquipmentImageViewDTO;
import com.ths.dmscore.util.CursorUtil;

/**
 * luu file hinh anh
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  19:53:59 19-12-2014
 */
public class EQUIP_ATTACH_FILE_TABLE extends ABSTRACT_TABLE {
	//ID file
	public static final String EQUIP_ATTACH_FILE_ID = "EQUIP_ATTACH_FILE_ID";
	//
	public static final String OBJECT_TYPE = "OBJECT_TYPE";
	// ma khach hang / ma chuong trinh
	public static final String OBJECT_ID = "OBJECT_ID";
	// duong dan
	public static final String URL = "URL";
	public static final String THUMB_URL = "THUMB_URL";
	// ten file
	public static final String FILE_NAME = "FILE_NAME";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// lat
	public static final String LAT = "LAT";
	// lng
	public static final String LNG = "LNG";
	public static final String STAFF_ID = "STAFF_ID";
	// table name
	public static final String TABLE_NAME = "EQUIP_ATTACH_FILE";
	
	public EQUIP_ATTACH_FILE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { EQUIP_ATTACH_FILE_ID, OBJECT_TYPE,
				OBJECT_ID, URL, FILE_NAME, CREATE_DATE, UPDATE_DATE,
				CREATE_USER, UPDATE_USER, LAT, LNG, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		EquipAttachFileDTO attach = (EquipAttachFileDTO) dto;
		ContentValues value = initDataRow(attach);
		return insert(null, value);
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	/**
	 * @author: hoanpd1
	 * @since: 20:00:02 19-12-2014
	 * @return: ContentValues
	 * @throws:  
	 * @param attach
	 * @return
	 */
	private ContentValues initDataRow(EquipAttachFileDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(EQUIP_ATTACH_FILE_ID, dto.idEquipAttachFile);
		editedValues.put(OBJECT_TYPE, dto.objectType);
		editedValues.put(OBJECT_ID, dto.objectId);
		editedValues.put(URL, dto.url);
		editedValues.put(THUMB_URL, dto.thumbUrl);
		editedValues.put(FILE_NAME, dto.fileName);
		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(LAT, dto.lat);
		editedValues.put(LNG, dto.lng);
		editedValues.put(STAFF_ID, dto.staffId);
		return editedValues;
	}

	/**
	 * Lay ds hinh anh cua thiet bi kiem ke
	 * @author: dungnt19
	 * @param b
	 * @return
	 * @return: ArrayList<MediaItemDTO>
	 * @throws:
	 */
	public TakePhotoEquipmentImageViewDTO getListImageOfEquipment(Bundle b) throws Exception {
		TakePhotoEquipmentImageViewDTO image = new TakePhotoEquipmentImageViewDTO();
		String customerId = b.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = b.getString(IntentConstants.INTENT_SHOP_ID);
		String staffId = b.getString(IntentConstants.INTENT_STAFF_ID);
		long equipId = b.getLong(IntentConstants.INTENT_EQUIP_ID);
		int numTop = b.getInt(IntentConstants.INTENT_MAX_IMAGE_PER_PAGE);
		int page = b.getInt(IntentConstants.INTENT_PAGE);
		boolean isGetTotalPage = b.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		ArrayList<String> params = new ArrayList<String>();
		ArrayList<String> totalParams = new ArrayList<String>();
		StringBuffer sqlTotalObject = new StringBuffer();
		StringBuffer sqlObject = new StringBuffer();
		sqlObject.append("SELECT DISTINCT mi.url                 URL, ");
		sqlObject.append("                mi.EQUIP_ATTACH_FILE_ID       MEDIA_ITEM_ID, ");
		sqlObject.append("                mi.thumb_url           THUMB_URL, ");
		sqlObject.append("                mi.create_date         CREATE_DATE, ");
		sqlObject.append("                Mi.thumb_url                                 AS THUMB_URL, ");
		sqlObject.append("                Mi.url                                       AS URL, ");
		sqlObject.append("                S.staff_id                                  AS STAFF_ID, ");
		sqlObject.append("                Mi.lat                                       AS LAT_MEDIA, ");
		sqlObject.append("                Mi.lng                                       AS LNG_MEDIA, ");
		sqlObject.append("                Mi.create_user                               AS CREATE_USER, ");
		sqlObject.append("                Strftime('%d/%m/%Y - %H:%M', Mi.create_date) AS CREATE_DATE ");
		sqlObject.append("FROM   equip_attach_file mi, ");
		sqlObject.append("	     equip_statistic_rec_dtl s 	");
		sqlObject.append("WHERE  mi.object_id = s.EQUIP_STATISTIC_REC_DTL_ID ");
		sqlObject.append("AND  s.OBJECT_STOCK_TYPE = 2 ");
		sqlObject.append("AND  s.OBJECT_STOCK_ID = ? ");
		params.add(String.valueOf(customerId));
		totalParams.add(String.valueOf(customerId));
		sqlObject.append("AND  s.OBJECT_ID = ? ");
		params.add(String.valueOf(equipId));
		totalParams.add(String.valueOf(equipId));
//		sqlObject.append("       AND mi.object_type = ? ");
//		params.add(String.valueOf(MediaItemDTO.TYPE_DEVICE_IMAGE));
//		totalParams.add(String.valueOf(MediaItemDTO.TYPE_DEVICE_IMAGE));
//		sqlObject.append("       AND mi.media_type = 0 ");
//		sqlObject.append("       AND mi.status = 1 ");
		sqlObject.append("       AND s.shop_id = ? ");
		params.add(String.valueOf(shopId));
		totalParams.add(String.valueOf(shopId));
		sqlObject.append("       AND mi.STAFF_ID = ? ");
		params.add(String.valueOf(staffId));
		totalParams.add(String.valueOf(staffId));
		sqlObject.append("       AND mi.STAFF_ID = s.staff_id ");
		if (isGetTotalPage) {
			sqlTotalObject.append("select count(*) as TOTAL_ROW from (" + sqlObject + ")");
		}
		sqlObject.append(" order by datetime(mi.create_date) desc ");
		sqlObject.append(" limit ? offset ?");
		params.add("" + numTop);
		int offset = page * numTop;
		params.add(String.valueOf(offset));
		
		Cursor c = null;
		Cursor cTotal = null;
		
		try {
			if (isGetTotalPage) {
				cTotal = rawQueries(sqlTotalObject.toString(), totalParams);
				if (cTotal != null) {
					if (cTotal.moveToFirst()) {
						image.totalImage = CursorUtil.getInt(cTotal, "TOTAL_ROW");
					}
				}
			}
			c = rawQueries(sqlObject.toString(), params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ImageListItemDTO item = new ImageListItemDTO();
						item.imageSearchListItemDTO(c);
						image.lstImage.add(item.mediaItem);
					} while (c.moveToNext());
				}
			}
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotal != null) {
					cTotal.close();
				}
			} catch (Exception e) {
			}
		}
		return image;
	}
	
}
