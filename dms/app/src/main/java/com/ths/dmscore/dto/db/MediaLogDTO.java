/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.math.BigDecimal;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.MEDIA_LOG_TABLE;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Thong tin cau hinh
 * 
 * @author: ToanTT
 * @version: 1.0
 * @since: 1.0
 */
@SuppressWarnings("serial")
public class MediaLogDTO extends AbstractTableDTO {
	// id
	public long id;
	// staff id
	public int staffId;
	// customer id, -1: bo qua
	public long customerId;
	// media item id
	public long mediaId;
	// product id
	public long productId;
	// dayInOrder start watch video
	public String startTime;
	public String endTime;
	// 1: ngoai tuyen, 0: trong tuyen, -1: tbhv hoac bo qua
	public int isVisitPlan;
	// 0 chua ghe tham, 1 dang ghe tham, 2 da ke thuc ghe tham,  -1: tbhv hoac bo qua
	public int visitStatus;
	// lat
	public double lat;
	// lng
	public double lng;
	// k/c tu nhan vien den kh, -1: bo qua
	public double distance;
	// create user
	public String createUser;
	// create dayInOrder
	public String createDate;

	public MediaLogDTO() {
		super(TableType.MEDIA_LOG_TABLE);
	}

	/**
	 * tao ra mot cau truc sql de gui len server insert media log
	 * 
	 * @author : ToanTT
	 */
	public JSONObject generateMediaLogSql() {
		JSONObject actionJson = new JSONObject();
		try {
			actionJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			actionJson.put(IntentConstants.INTENT_TABLE_NAME, MEDIA_LOG_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.MEDIA_LOG_ID, id, null));
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.STAFF_ID, staffId, null));
			if (customerId != -1) {
				detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.CUSTOMER_ID, customerId, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.MEDIA_ID, mediaId, null));
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.PRODUCT_ID, productId, null));
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.START_TIME, startTime, null));
			if (isVisitPlan != -1) {
				detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.IS_OR, isVisitPlan, null));
			}
			if (visitStatus != -1) {
				detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.VISIT_STATUS, visitStatus, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.LAT, lat, null));
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.LNG, lng, null));
//			if (distance != -1) {
			if (BigDecimal.valueOf(distance).compareTo(BigDecimal.valueOf(-1)) != 0) {
				detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.DISTANCE, distance, null));
			}
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.CREATE_USER, createUser, null));
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.CREATE_DATE, createDate, null));
			actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return actionJson;
	}

	public JSONObject generateUpdateEndtimeMediaLogSql() {
		JSONObject actionJson = new JSONObject();
		try {
			actionJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			actionJson.put(IntentConstants.INTENT_TABLE_NAME, MEDIA_LOG_TABLE.TABLE_NAME);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.END_TIME, endTime, null));
			actionJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(MEDIA_LOG_TABLE.MEDIA_LOG_ID, this.id, null));
			actionJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (Exception e) {
		}
		return actionJson;
	}

	public static int isVisited(VISIT_STATUS visitStatus) {
		int result = 0;
		if (visitStatus == VISIT_STATUS.VISITING) {
			result = 1;
		}
		if (visitStatus == VISIT_STATUS.VISITED_FINISHED) {
			result = 2;
		}
		return result;
	}

}
