/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ths.dmscore.dto.view.LanguagesDTO;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

/**
 * SpinnerLanguagesAdapter.java
 *
 * @author: hoanpd1
 * @version: 1.0
 * @since: 08:25:29 21-01-2015
 */
public class SpinnerLanguagesAdapter extends ArrayAdapter<LanguagesDTO> {

	private final Context con;
	private final ArrayList<LanguagesDTO> listLanguages;

	public SpinnerLanguagesAdapter(Context context, int textViewResourceId,
			ArrayList<LanguagesDTO> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		con = context;
		listLanguages = objects;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listLanguages.size();
	}

	@Override
	public LanguagesDTO getItem(int position) {
		// TODO Auto-generated method stubs
		return listLanguages.get(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LanguagesDTO lang = getItem(position);
		if (convertView == null) {
			LayoutInflater layout = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layout.inflate(R.layout.layout_spinner_languages_adapter, parent, false);
		}
		TextView tvLanguages = (TextView) convertView.findViewById(R.id.tvLanguages);
		tvLanguages.setCompoundDrawablesWithIntrinsicBounds(0, 0, lang.getIcon(), 0);
		tvLanguages.setText("");
		return convertView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LanguagesDTO lang = getItem(position);
		if (convertView == null) {
			LayoutInflater layout = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layout.inflate(R.layout.layout_spinner_languages_adapter_dropdown, parent, false);
		}
		convertView.setBackgroundColor(ImageUtil.getColor(R.color.WHITE));
		TextView tvLanguages = (TextView) convertView.findViewById(R.id.tvLanguages);
		tvLanguages.setCompoundDrawablesWithIntrinsicBounds(lang.getIcon(), 0, 0, 0);
		tvLanguages.setText(lang.getName());
		return convertView;
	}
}
