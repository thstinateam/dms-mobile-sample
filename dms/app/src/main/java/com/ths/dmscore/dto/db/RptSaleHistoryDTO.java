package com.ths.dmscore.dto.db;

import android.database.Cursor;


@SuppressWarnings("serial")
public class RptSaleHistoryDTO extends AbstractTableDTO{
	public long id;
	public long staffId;
	public int month;
	public int numCustomer;
	public int numCustNotOrder;
	public int totalValueOfOrder;
	public int numSKU;
	public String createDate;

	public RptSaleHistoryDTO() {
		super(TableType.RPT_SALE_HISTORY);
	}

	public void initDataFromCursor(Cursor c) {

	}
}
