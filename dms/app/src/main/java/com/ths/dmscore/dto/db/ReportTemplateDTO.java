package com.ths.dmscore.dto.db;

import java.io.Serializable;

public class ReportTemplateDTO extends AbstractTableDTO implements Serializable {
	public static final long serialVersionUID = 1L;

	public int reportTemplateId;
	public String name;
	public int kpiId;
	public int isShowPlan;
	public int isShowReality;
	public int isShowColumnTotal;
	public int isShowRowTotal;
	public int criRowId;
	public int criSubRowId;
	public int criColumnId;
	public int periodType;
	public int periodCycleType;
	public String createDate;
	public String updateDate;
	public String createUser;
	public String updateUser;

	// ds cac loai chu ki theo periodCycleType
	public static final int DATE = 0;// tu ngay den ngay
	public static final int CURRENT_PERIOD = 1; // tuan hien tai, thang hien tai ...
	public static final int WITHIN_PARENT_PERIOD = 2;// tuan trong thang hien tai, thang trong nam hien tai ...


}
