/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_TABLE;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Thong tin don hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class SaleOrderDTO extends AbstractTableDTO implements Cloneable {
	private static final long serialVersionUID = -7138079946665484597L;
	// id don dat hang
	public long saleOrderId = 0;
	public long poId = 0;
	// mo ta
	public String description = "";
	// ma NPP
	public int shopId = 0;
	// loai don dat hang (IN, OFF)
	public String orderType = "";
	// ma don hang noi bo
	public String orderNumber = "";
	// ma don hang ban hang
	public String refOrderNumber = "";
	// so hoa don
	public String invoiceNumber = "";
	// ngay lap don hang
	public String orderDate = "";
	// id khach hang
	public long customerId = 0;
	// ma nhan vien
	public int staffId = 0;
	// phuong tien van chuyen
	public String deliveryId = "";
	// so tien don hang chua tinh khuyen mai
	private double amount = 0;
	// so tien khuyen mai
	private double discount = 0;
	// so tien don hang sau khi tinh khuyen mai
	private double total = 0;
	// nguoi tao
	public String createUser = "";
	// nguoi cap nhat
	public String updateUser = "";
	// ngay tao
	public String createDate = "";
	// ngay cap nhat
	public String updateDate = "";
	// 1: trong tuyen, 0: ngoai tuyen
	public int isVisitPlan = 0;
	// muc do khan
	public long priority;
	// ngay dat hang mong muon
	public String deliveryDate;
	// 1: don hang ban nhung chua tra, 0: don hang ban da thuc hien tra lai, 2
	// don tra hang
	public int type;
	// 2: don hang tao tren tablet chua yeu cau xac nhan, 3: don hang tao tren
	// tablet yeu cau xac nhan,
	// 0: don hang chua duyet,1: don hang da duyet, 4 huy do qua ngay khong phe
	// duyet
	// -1 : don hang tam : TRUNGHQM
	public int approved;
	// xe giao hang
	public String carId;
	// thue suat
	public double vat;
	// id nhan vien thu tien
	public String cashierId = "";
	// 1: don hang tren web; 2: don hang tao ra tren table;
	public int orderSource;
	// id don hang bi tra
	public long fromSaleOrderId;
	// id don hang bi tra
	public long fromPOCustomerId;
	// tong trong luong don hang
	public double totalWeight;
	// ma CTKM dang Docmt
	public String programeCode;
	// % chiet khau
	public double discountPercent;
	// so tien KM
//	public long discountAmount;
	// so tien chiet khau toi da
	public long maxAmountFree;
	// tong so luong sku cua don hang
	public int totalDetail;
	// MA NPP
	public String shopCode;

	public int approvedStep = 0;
	public String maxApproved;
	public String maxReturn;
	//tuyen
	public long routingId;
	//ngay ket toan
	public String accountDate;
	public String approvedDate;
	//so luong sp ban le trong don hang
	public long quantity;
	public int approvedVan;
	public long cycleId;
	public boolean isRewardKS;// co tham gia tra thuong keyshop cho don hang hay ko

	public int synState;

	public SaleOrderDTO() {
		super(TableType.SALE_ORDER);
	}

	/**
	 * Generate cau lenh insert
	 *
	 * @author: TruongHN
	 * @param seqOrderName
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateCreateSql() {
		JSONObject orderJson = new JSONObject();
		try {
			// INSERT INTO
			// SALES_ORDER(SALE_ORDER_ID,SHOP_ID,ORDER_TYPE,ORDER_NUMBER,INVOICE_NUMBER,ORDER_DATE,CUSTOMER_ID,STAFF_ID,
			// DELIVERY_CODE,AMOUNT,DISCOUNT,PROMO_CODE,TOTAL,STATE,CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,IS_VISIT_PLAN,
			// IS_SEND,SYN_STATUS) VALUES (...)

			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME,
					SALE_ORDER_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.SALE_ORDER_ID,
					saleOrderId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.FROM_PO_CUSTOMER_ID,
					fromPOCustomerId, null));
			if (fromSaleOrderId > 0) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.FROM_SALE_ORDER_ID, fromSaleOrderId,
						null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.SHOP_ID,
					shopId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_TYPE,
					orderType, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_NUMBER,orderNumber, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.REF_ORDER_NUMBER,refOrderNumber, null));
			// if (!StringUtil.isNullOrEmpty(invoiceNumber)){
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.INVOICE_NUMBER,invoiceNumber,
			// null));
			// }
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_DATE,
					orderDate, null));

			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.CUSTOMER_ID,
					customerId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.STAFF_ID,
					staffId, null));
			if (!StringUtil.isNullOrEmpty(deliveryId)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.DELIVERY_ID, deliveryId, null));
			}
			if (!StringUtil.isNullOrEmpty(cashierId)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.CASHIER_ID, cashierId, null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.AMOUNT,
					amount, null));
			// discount - so tien khuyen mai
			if(discount < 0) {
				discount = 0;
			}
//			if (discount >= 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.DISCOUNT, discount, null));
//			}
			// promo code - ma chuong trinh khuyen mai
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL, total,
					null));
			// order source
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_SOURCE,
					orderSource, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.CREATE_USER,
					createUser, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.CREATE_DATE,
					createDate, null));
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.DELIVERY_DATE, deliveryDate, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.DELIVERY_DATE, "",
						DATA_TYPE.NULL.toString()));
			}

			if (priority > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.PRIORITY,
						priority, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.PRIORITY,
						0, DATA_TYPE.NULL.toString()));
			}

			if (!StringUtil.isNullOrEmpty(updateDate)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.UPDATE_DATE, updateDate, null));
			}

			if (!StringUtil.isNullOrEmpty(updateUser)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.UPDATE_USER, updateUser, null));
			}

			// is_visit_plan
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_VISIT_PLAN,
					isVisitPlan, null));
			// is send
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_SEND,isSend,
			// null));
			// syn statuso
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TYPE, type,
					null));
			// total weght
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL_WEIGHT,
					totalWeight, null));
			// approve
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED,
					approved, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.CYCLE_ID,
					cycleId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_REWARD_KS,
					isRewardKS?1:0, null));

			// don tra thi co truong approvedvan
//			if(type == 2){
//				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED_VAN,
//						approvedVan, null));
//			}

			// total detail
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL_DETAIL,
					totalDetail, null));
			// shop code
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.SHOP_CODE,
					shopCode, null));

			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED_STEP,
					approvedStep, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED_VAN,
					approvedVan, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ROUTING_ID,
					routingId, null));
			if (!StringUtil.isNullOrEmpty(accountDate)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.ACCOUNT_DATE, accountDate, null));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.QUANTITY,
					quantity, null));
			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM,
			// value);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 *
	 * Generate ra cau sql sua don hang
	 *
	 * @author: Nguyen Thanh Dung
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateEditOrderSql() {
		JSONObject orderJson = new JSONObject();
		try {
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME,
					SALE_ORDER_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.SALE_ORDER_ID,saleOrderId,
			// null));
			// state -- don hang do minh tao nen state = 1
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.AMOUNT,
					String.valueOf(amount), null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.FROM_PO_CUSTOMER_ID,
					fromPOCustomerId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.CUSTOMER_ID,
					String.valueOf(customerId), null));// tam
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.DELIVERY_ID,
			// "GN01", null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.DISCOUNT,
					String.valueOf(discount), null));
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_SEND,isSend,
			// null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_VISIT_PLAN,
					String.valueOf(isVisitPlan), null));
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.INVOICE_NUMBER,
			// "Default", null));
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.STATE, "1",
			// null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL,
					String.valueOf(total), null));
			// params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_DATE,
			// DATA_VALUE.sysdate.toString(), DATA_TYPE.SYSDATE.toString()));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.UPDATE_DATE,
					updateDate, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.UPDATE_USER,
					updateUser, null));// Tam
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.DELIVERY_DATE, deliveryDate, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(
						SALE_ORDER_TABLE.DELIVERY_DATE, "",
						DATA_TYPE.NULL.toString()));
			}
			if (priority > 0) {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.PRIORITY,
						priority, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.PRIORITY,
						0, DATA_TYPE.NULL.toString()));
			}
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.DESCRIPTION,
					"", DATA_TYPE.NULL.toString()));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TYPE, type,
					null));
			// total weght
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL_WEIGHT,
					totalWeight, null));
			// order source
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.ORDER_SOURCE,
					orderSource, null));
			// approve
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED,
					approved, null));

			// total detail
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TOTAL_DETAIL,
					totalDetail, null));

			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.APPROVED_STEP,
					approvedStep, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.QUANTITY,
					quantity, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ROUTING_ID,
					routingId, null));
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.IS_REWARD_KS,
					isRewardKS?1:0, null));
			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(
					SALE_ORDER_TABLE.SALE_ORDER_ID,
					String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	// public JSONObject generateDeleteSaleOrderSql(String id) {
	// JSONObject saleJson = new JSONObject();
	// try {
	// saleJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
	// saleJson.put(IntentConstants.INTENT_TABLE_NAME,
	// SALE_ORDER_TABLE.TABLE_NAME);
	// // ds params
	// JSONArray detailPara = new JSONArray();
	// detailPara.put(GlobalUtil.getJsonColumn(
	// SALE_ORDER_TABLE.SALE_ORDER_ID, id, null));
	// saleJson.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
	// } catch (Exception e) {
	// }
	// return saleJson;
	// }

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateUpdateSentOrderSql() {
		// TODO Auto-generated method stub
		JSONObject orderJson = new JSONObject();
		try {
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME,
					SALE_ORDER_TABLE.TABLE_NAME);
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(SALE_ORDER_TABLE.TYPE, type,
					null));
			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(
					SALE_ORDER_TABLE.SALE_ORDER_ID,
					String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * Generate cau lenh insert - clone cho PO_CUSTOMER, PO_CUSTOMER_DETAIL
	 *
	 * @author: DungNX3
	 * @param seqOrderName
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateCreatePOCustomerSql() {
		JSONObject orderJson = new JSONObject();
		try {
			// INSERT INTO
			// SALES_ORDER(SALE_ORDER_ID,SHOP_ID,ORDER_TYPE,ORDER_NUMBER,INVOICE_NUMBER,ORDER_DATE,CUSTOMER_ID,STAFF_ID,
			// DELIVERY_CODE,AMOUNT,DISCOUNT,PROMO_CODE,TOTAL,STATE,CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,IS_VISIT_PLAN,
			// IS_SEND,SYN_STATUS) VALUES (...)

			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.PO_CUSTOMER_ID,
					poId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.SHOP_ID,
					shopId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_TYPE,
					orderType, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_NUMBER,orderNumber, null));
//			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.REF_ORDER_NUMBER,refOrderNumber, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.FROM_SALE_ORDER_ID,
					saleOrderId, null));
			// if (!StringUtil.isNullOrEmpty(invoiceNumber)){
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.INVOICE_NUMBER,invoiceNumber,
			// null));
			// }
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_DATE,
					orderDate, null));

			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.CUSTOMER_ID,
					customerId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.STAFF_ID,
					staffId, null));
			if (!StringUtil.isNullOrEmpty(cashierId) && !StringUtil.isNullOrEmpty(deliveryId)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.DELIVERY_ID, deliveryId, null));
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.CASHIER_ID, cashierId, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.AMOUNT,
					amount, null));
			// discount - so tien khuyen mai
			if(discount < 0) {
				discount = 0;
			}
//			if (discount > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.DISCOUNT,
						discount, null));
//			}
			// promo code - ma chuong trinh khuyen mai
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL, total,
					null));
			// order source
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_SOURCE,
					orderSource, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.CREATE_USER,
					createUser, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.CREATE_DATE,
					createDate, null));
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.DELIVERY_DATE, deliveryDate, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.DELIVERY_DATE, "",
						DATA_TYPE.NULL.toString()));
			}

			if (priority > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.PRIORITY,
						priority, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.PRIORITY,
						0, DATA_TYPE.NULL.toString()));
			}

			if (!StringUtil.isNullOrEmpty(updateDate)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.UPDATE_DATE, updateDate, null));
			}

			if (!StringUtil.isNullOrEmpty(updateUser)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.UPDATE_USER, updateUser, null));
			}

			// is_visit_plan
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_VISIT_PLAN,
					isVisitPlan, null));
			// is send
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_SEND,isSend,
			// null));
			// syn statuso
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TYPE, type,
					null));
			// total weght
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL_WEIGHT,
					totalWeight, null));
			// approve
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.APPROVED,
					approved, null));

			// total detail
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL_DETAIL,
					totalDetail, null));
			// shop code
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.SHOP_CODE,
					shopCode, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ROUTING_ID,
					routingId, null));
			if (!StringUtil.isNullOrEmpty(accountDate)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.ACCOUNT_DATE, accountDate, null));
			}
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.QUANTITY,
					quantity, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.CYCLE_ID,
					cycleId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_REWARD_KS,
					isRewardKS?1:0, null));
			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			// sqlInsertOrder.put(IntentConstants.INTENT_LIST_WHERE_PARAM,
			// value);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 *
	 * Generate ra cau sql sua don hang - clone PO
	 *
	 * @author: DungNX3
	 * @return
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateEditPOSql() {
		JSONObject orderJson = new JSONObject();
		try {
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.SALE_ORDER_ID,saleOrderId,
			// null));
			// state -- don hang do minh tao nen state = 1
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.AMOUNT,
					String.valueOf(amount), null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.CUSTOMER_ID,
					String.valueOf(customerId), null));// tam
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.DELIVERY_ID,
			// "GN01", null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.DISCOUNT,
					String.valueOf(discount), null));
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_SEND,isSend,
			// null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_VISIT_PLAN,
					String.valueOf(isVisitPlan), null));
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.INVOICE_NUMBER,
			// "Default", null));
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.STATE, "1",
			// null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL,
					String.valueOf(total), null));
			// params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_DATE,
			// DATA_VALUE.sysdate.toString(), DATA_TYPE.SYSDATE.toString()));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.UPDATE_DATE,
					updateDate, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.UPDATE_USER,
					updateUser, null));// Tam
			if (!StringUtil.isNullOrEmpty(deliveryDate)) {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.DELIVERY_DATE, deliveryDate, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(
						PO_CUSTOMER_TABLE.DELIVERY_DATE, "",
						DATA_TYPE.NULL.toString()));
			}
			if (priority > 0) {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.PRIORITY,
						priority, null));
			} else {
				params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.PRIORITY,
						0, DATA_TYPE.NULL.toString()));
			}
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.DESCRIPTION,
					"", DATA_TYPE.NULL.toString()));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TYPE, type,
					null));
			// total weght
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL_WEIGHT,
					totalWeight, null));
			// order source
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ORDER_SOURCE,
					orderSource, null));
			// approve
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.APPROVED,
					approved, null));

			// total detail
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.TOTAL_DETAIL,
					totalDetail, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.QUANTITY,
					quantity, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.ROUTING_ID,
					routingId, null));
			params.put(GlobalUtil.getJsonColumn(PO_CUSTOMER_TABLE.IS_REWARD_KS,
					isRewardKS?1:0, null));
			orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_TABLE.PO_CUSTOMER_ID,
					String.valueOf(poId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);

		} catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: SaleOrderDTO
	 * @throws CloneNotSupportedException
	 * @throws:
	*/
	public SaleOrderDTO cloneThis() throws CloneNotSupportedException {
		SaleOrderDTO itemCloned = null;
		itemCloned = (SaleOrderDTO) this.clone();
		return itemCloned;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return StringUtil.round(amount, GlobalInfo.getInstance().getSysNumRoundingPromotion());
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public void addAmount(double amountAdd) {
		this.amount += amountAdd;
	}

	public void subtractAmount(double amountSubtract) {
		this.amount -= amountSubtract;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the total
	 */
	public double getTotal() {
		return StringUtil.round(total, GlobalInfo.getInstance().getSysNumRoundingPromotion());
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(double discount) {
		this.discount = discount;
	}

	/**
	 * @return the discount
	 */
	public double getDiscount() {
		return StringUtil.round(discount, GlobalInfo.getInstance().getSysNumRoundingPromotion());
	}

	public void adddDiscount(double discountAdd) {
		this.discount += discountAdd;
	}

	public void subtractDiscount(double discountSubtract) {
		this.discount -= discountSubtract;
	}
}