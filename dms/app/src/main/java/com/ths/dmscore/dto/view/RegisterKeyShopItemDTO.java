package com.ths.dmscore.dto.view;

import android.database.Cursor;

import com.ths.dmscore.dto.db.KSCustomerDTO;
import com.ths.dmscore.dto.db.KSCustomerLevelDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.dto.db.KSLevelDTO;
import com.ths.dmscore.lib.sqllite.db.KEYSHOP_TABLE;

/**
 * Du lieu cho dang ki keyshop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class RegisterKeyShopItemDTO {
	public KSLevelDTO ksLevelItem = new KSLevelDTO();
	public KSCustomerLevelDTO ksCustomerLevelItem = new KSCustomerLevelDTO();
	public KSCustomerDTO ksCustomerItem = new KSCustomerDTO();
	// cho phep duoc edit hay ko
	public boolean isEdit = true;

	@Override
	public Object clone() {
		RegisterKeyShopItemDTO object = new RegisterKeyShopItemDTO();
		object.ksLevelItem = (KSLevelDTO)ksLevelItem.clone();
		object.ksCustomerLevelItem = (KSCustomerLevelDTO)ksCustomerLevelItem.clone();
		object.ksCustomerItem = (KSCustomerDTO)ksCustomerItem.clone();
		return object;
	}

	/**
	 * Khoi tao du lieu
	 *
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		ksLevelItem.ksId = CursorUtil.getLong(c, KEYSHOP_TABLE.KS_ID);
		ksLevelItem.ksLevelCode = CursorUtil.getString(c, "KS_LEVEL_CODE");
		ksLevelItem.name = CursorUtil.getString(c, "NAME");
		ksLevelItem.amount = CursorUtil.getLong(c, "AMOUNT");
		ksLevelItem.quantity = CursorUtil.getLong(c, "QUANTITY");
		ksLevelItem.ksLevelId = CursorUtil.getLong(c, "KS_LEVEL_ID");
//		ksCustomerItem.shopId = CursorUtil.getLong(c, "SHOP_ID");
		ksCustomerItem.ksCustomerId = CursorUtil.getLong(c, "KS_CUSTOMER_ID");
		ksCustomerItem.cycleId= CursorUtil.getLong(c, "CYCLE_ID");
		ksCustomerItem.customerId= CursorUtil.getLong(c, "CUSTOMER_ID");
		ksCustomerItem.ksId = ksLevelItem.ksId;
		ksCustomerLevelItem.multiplier = CursorUtil.getInt(c, "MULTIPLIER");
		ksCustomerLevelItem.ksCustomerLevelId= CursorUtil.getLong(c, "KS_CUSTOMER_LEVEL_ID");
		ksCustomerLevelItem.ksLevelId= ksLevelItem.ksLevelId;
		ksCustomerLevelItem.ksCustomerId= ksCustomerItem.ksCustomerId;
		// ko cho edit lai keyshop da dang ky
		int edit = CursorUtil.getInt(c, "IS_EDIT");
		if(edit == 0){
			isEdit = false;
		}
		ksCustomerItem.status = CursorUtil.getInt(c, "STATUS");
	}

}
