/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 * DTO man hinh :01-07. Tien do CTTB theo NPP (TBHV)
 * 
 * @author: HoanPD1
 * @version: 1.0
 * @since: 1.0
 */
public class TBHVDisProComProgReportNPPDTO {

	// danh sach item
	public ArrayList<TBHVDisProComProgReportItem> arrList;
	// danh sach level code
	public ArrayList<String> arrLevelCodeInView;
	// danh sach level code cua mot cttb trong db
	public ArrayList<String> arrLevelInDB;
	// tong ket qua
	public ArrayList<TBHVDisProComProgReportItemResult> arrResultTotal;
	// tong cua tung level
	public TBHVDisProComProgReportItemResult dtoResultTotal;
	// list display programe for use
	public ArrayList<DisplayPresentProductInfo> listDisplayProgrameInfo = new ArrayList<DisplayPresentProductInfo>();
	// list gsnpp
	public ArrayList<GSNPPInfoDTO> listGSNPPInfo = new ArrayList<GSNPPInfoDTO>();
	// tien do chuan thuc hien
	public double progressStandarPercent = 0;

	public TBHVDisProComProgReportNPPDTO() {
		arrList = new ArrayList<TBHVDisProComProgReportItem>();
//		arrLevelCode = new ArrayList<String>();
		arrLevelCodeInView = new ArrayList<String>();
		arrResultTotal = new ArrayList<TBHVDisProComProgReportItemResult>();
		dtoResultTotal = new TBHVDisProComProgReportItemResult();
		listDisplayProgrameInfo = new ArrayList<DisplayPresentProductInfo>();
		listGSNPPInfo = new ArrayList<GSNPPInfoDTO>();
	}
}
