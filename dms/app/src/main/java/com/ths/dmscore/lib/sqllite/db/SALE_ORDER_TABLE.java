/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteStatement;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.dto.view.CustomerInfoDTO;
import com.ths.dmscore.dto.view.OrderViewDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.db.ApParamDTO;
import com.ths.dmscore.dto.db.TabletActionLogDTO;
import com.ths.dmscore.dto.view.ListOrderMngDTO;
import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto;
import com.ths.dmscore.dto.view.NoSuccessSaleOrderDto.NoSuccessSaleOrderItem;
import com.ths.dmscore.dto.view.SaleInMonthDTO;
import com.ths.dmscore.dto.view.SaleOrderCustomerDTO;
import com.ths.dmscore.dto.view.SaleOrderViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ServerLogger;
import com.ths.dmscore.view.sale.order.ListOrderView;

/**
 * Thong tin don hang
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class SALE_ORDER_TABLE extends ABSTRACT_TABLE {
	// don hang presale
	public static final String ORDER_TYPE_PRESALE = "IN";
	public static final String ORDER_TYPE_VANSALE = "SO";
	// don tra vansale
	public static final String ORDER_TYPE_VANSALE_RE = "CO";
	// id don dat hang
	public static final String SALE_ORDER_ID = "SALE_ORDER_ID";
	// id don dat hang PO
	public static final String FROM_PO_CUSTOMER_ID = "FROM_PO_CUSTOMER_ID";
	// ma NPP
	public static final String SHOP_ID = "SHOP_ID";
	// Loai don hang: IN: pre->kh; CM: KH->Pre;SO Van->KH; CO KH->Van, TT-> don
	// hang tra thuong
	public static final String ORDER_TYPE = "ORDER_TYPE";
	// ma don hang noi bo
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	// ma don hang ban hang
	public static final String REF_ORDER_NUMBER = "REF_ORDER_NUMBER";
	// 1: don hang ban nhung chua tra, 0: don hang ban da thuc hien tra lai, 2
	// don tra hang
	public static final String TYPE = "TYPE"; //
	// 2: don hang tao tren tablet chua yeu cau xac nhan, 3: don hang tao tren
	// tablet yeu cau xac nhan,
	// 0: don hang chua duyet,1: don hang da duyet, 4 huy do qua ngay khong phe
	// duyet
	public static final String APPROVED = "APPROVED"; //
//	public static final String APPROVED_VAN = "APPROVED_VAN"; //
	// so hoa don
	// public static final String INVOICE_NUMBER = "INVOICE_NUMBER";
	// ngay lap don hang
	public static final String ORDER_DATE = "ORDER_DATE";
	// id khach hang
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// ma nhan vien
	public static final String STAFF_ID = "STAFF_ID";
	// id NVGH
	public static final String DELIVERY_ID = "DELIVERY_ID";
	// Xe giao hang
	public static final String CAR_ID = "CAR_ID";
	// thue suat
	//public static final String VAT = "VAT";
	// Id NVTT: nhan vien thu tien
	public static final String CASHIER_ID = "CASHIER_ID";
	// so tien don hang chua tinh khuyen mai
	public static final String AMOUNT = "AMOUNT";
	// so tien khuyen mai
	public static final String DISCOUNT = "DISCOUNT";
	// so tien don hang sau khi tinh khuyen mai
	public static final String TOTAL = "TOTAL";
	// 1: don hang tren web; 2: don hang tao ra tren table;
	public static final String ORDER_SOURCE = "ORDER_SOURCE";
	// mo ta
	public static final String DESCRIPTION = "DESCRIPTION";
	// id don hang bi tra
	public static final String FROM_SALE_ORDER_ID = "FROM_SALE_ORDER_ID";
	// tong trong luong don hang
	public static final String TOTAL_WEIGHT = "TOTAL_WEIGHT";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// 1: trong tuyen, 0: ngoai tuyen
	public static final String IS_VISIT_PLAN = "IS_VISIT_PLAN";
	// muc do khan
	public static final String PRIORITY = "PRIORITY";
	// ngay dat don hang mong muon
	public static final String DELIVERY_DATE = "DELIVERY_DATE";
	// ngay in
	public static final String TIME_PRINT = "TIME_PRINT";
	// ma user huy dung cho xoa don hang tablet: neu xoa tu dong code = AUTO_DEL
	public static final String DESTROY_CODE = "DESTROY_CODE";
	// tong so luong detail cua don hang
	public static final String TOTAL_DETAIL = "TOTAL_DETAIL";
	// ma NPP
	public static final String SHOP_CODE = "SHOP_CODE";

	public static final String APPROVED_STEP = "APPROVED_STEP";
	public static final String APPROVED_VAN = "APPROVED_VAN";

	//bo sung 151104
	public static final String ROUTING_ID = "ROUTING_ID";
	public static final String ACCOUNT_DATE = "ACCOUNT_DATE";
	public static final String QUANTITY = "QUANTITY";
	public static final String APPROVED_DATE = "APPROVED_DATE";
	public static final String CYCLE_ID = "CYCLE_ID";
	public static final String IS_REWARD_KS = "IS_REWARD_KS";

	//bo sung
//	ROUTING_ID                       NUMBER(20)
//	APPROVED_VAN                     NUMBER(2)
//	APPROVED_DATE                    DATE
//	ACCOUNT_DATE                     DATE
//	NUM_DETAIL                       NUMBER
//	QUANTITY                         NUMBER(20)
	// // ma CTKM dang Docmt
	// public static final String PROGRAM_CODE = "PROGRAM_CODE";
	// // so tien chiet khau %
	// public static final String DISCOUNT_PERCENT = "DISCOUNT_PERCENT";
	// // so tien KM
	// public static final String DISCOUNT_AMOUNT = "DISCOUNT_AMOUNT";
	// // so tien chiet khau toi da
	// public static final String MAX_AMOUNT_FREE = "MAX_AMOUNT_FREE";

	// public static final String SYN_OPERATOR = "SYN_OPERATOR";
	public static final String TABLE_NAME = "SALE_ORDER";

	public SALE_ORDER_TABLE(SQLiteDatabase mDB) {
		tableName = TABLE_NAME;
		this.columns = new String[] { SALE_ORDER_ID, FROM_PO_CUSTOMER_ID, SHOP_ID, ORDER_TYPE,
				ORDER_NUMBER, REF_ORDER_NUMBER , TYPE, APPROVED, ORDER_DATE, CUSTOMER_ID,
				STAFF_ID, DELIVERY_ID, CAR_ID, CASHIER_ID, AMOUNT,
				DISCOUNT, TOTAL, ORDER_SOURCE, DESCRIPTION, FROM_SALE_ORDER_ID,
				TOTAL_WEIGHT, CREATE_USER, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, IS_VISIT_PLAN, PRIORITY, DELIVERY_DATE,
				TIME_PRINT, DESTROY_CODE, SYN_STATE, TOTAL_DETAIL, APPROVED_STEP, ROUTING_ID, ACCOUNT_DATE, QUANTITY,CYCLE_ID,IS_REWARD_KS
				};

		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	/**
	 * xoa data
	 *
	 * @author: DoanDM
	 * @return: void
	 * @throws:
	 */
	public void clearTable() {
		execSQL(sqlDelete);
	}

	/**
	 * drop database trong data
	 *
	 * @author: BangHN
	 * @return: void
	 * @throws:
	 */
	public void dropDatabase() {
		try {
			GlobalInfo.getInstance().getAppContext()
					.deleteDatabase(Constants.DATABASE_NAME);
			MyLog.i("DBAdapter", "Drop database success.......");
		} catch (Exception e) {
			MyLog.i("DBAdapter", "Error drop database : " + e.toString());
		}
	}

	public long getCount() {
		SQLiteStatement statement = compileStatement(sqlGetCountQuerry);
		long count = statement.simpleQueryForLong();
		return count;
	}

	/**
	 * thay doi 1 dong cua CSDL
	 *
	 * @author: HieuNH
	 * @param inheritId
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public long update(SaleOrderDTO dto) {
		ContentValues value = initDataRow(dto);
		String[] params = { "" + dto.saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * cap nhat don hang da chuyen CSDL
	 *
	 * @author: PhucNT
	 * @param inheritId
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public long updateSentOrder(SaleOrderDTO dto) {
		ContentValues value = new ContentValues();

		dto.updateDate = DateUtils.now();
		// dto.state = 1;
		dto.updateUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		// dto.importCode = "";

		// value.put(IS_SEND, dto.isSend);
		value.put(UPDATE_DATE, dto.updateDate);
		// value.put(STATE, dto.state );
		// value.put(IMPORT_CODE, dto.importCode);

		String[] params = { "" + dto.saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	public long update(AbstractTableDTO dto) {
		SaleOrderDTO saleDTO = (SaleOrderDTO) dto;
		ContentValues value = initDataRowForEdit(saleDTO, false);
		String[] params = { "" + saleDTO.saleOrderId, "" + saleDTO.synState };
		return update(value, SALE_ORDER_ID + " = ? AND " + SYN_STATE + " = ?",
				params);
	}

	public int updateAfterCommit(AbstractTableDTO dto, boolean isCommited) {
		SaleOrderDTO saleDTO = (SaleOrderDTO) dto;
		ContentValues value = initDataRowForEdit(saleDTO, isCommited);
		String[] params = { "" + saleDTO.saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? AND " + SYN_STATE + " = 0 ",
				params);
	}

	public long delete(AbstractTableDTO dto) {
		SaleOrderDTO saleDTO = (SaleOrderDTO) dto;
		String[] params = { "" + saleDTO.saleOrderId, "" + saleDTO.synState };
		return delete(SALE_ORDER_ID + " = ? AND " + SYN_STATE + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: SaleOrderDTO
	 * @throws:
	 */
	public SaleOrderDTO getSaleById(long id) {
		SaleOrderDTO SaleOrderDTO = null;
		Cursor c = null;
		try {
			String[] params = { "" + id };
			c = query(SALE_ORDER_ID + " = ?", params, null, null, null);

			if (c != null) {
				if (c.moveToFirst()) {
					SaleOrderDTO = initSaleOrderDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
			MyLog.e("getSaleById", "fail", ex);
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception ex) {
				MyLog.e("getSaleById", "fail", ex);
			}
		}
		return SaleOrderDTO;
	}

	private SaleOrderDTO initSaleOrderDTOFromCursor(Cursor c) {
		SaleOrderDTO saleOrderDTO = new SaleOrderDTO();

		saleOrderDTO.setAmount(CursorUtil.getDouble(c, AMOUNT));
		saleOrderDTO.fromPOCustomerId = CursorUtil.getLong(c, FROM_PO_CUSTOMER_ID);
		saleOrderDTO.createDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, CREATE_DATE));
		saleOrderDTO.createUser = CursorUtil.getString(c, CREATE_USER);
		saleOrderDTO.customerId = CursorUtil.getLong(c, CUSTOMER_ID);
		saleOrderDTO.cashierId = CursorUtil.getString(c, CASHIER_ID);
		saleOrderDTO.deliveryId = CursorUtil.getString(c, DELIVERY_ID);
		saleOrderDTO.setDiscount(CursorUtil.getDouble(c, DISCOUNT));
		// saleOrderDTO.invoiceNumber = CursorUtil.getString(c
		// .getColumnIndex(INVOICE_NUMBER);
		saleOrderDTO.isVisitPlan = CursorUtil.getInt(c, IS_VISIT_PLAN);
		saleOrderDTO.orderDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, ORDER_DATE));
		// SaleOrderDTO.userName = CursorUtil.getString(c
		// .getColumnIndex(USER_NAME);
		saleOrderDTO.orderNumber = CursorUtil.getString(c, ORDER_NUMBER);
		saleOrderDTO.refOrderNumber = CursorUtil.getString(c, REF_ORDER_NUMBER);
		saleOrderDTO.orderType = CursorUtil.getString(c, ORDER_TYPE);
		saleOrderDTO.saleOrderId = CursorUtil.getLong(c, SALE_ORDER_ID);
		saleOrderDTO.shopId = CursorUtil.getInt(c, SHOP_ID);
		saleOrderDTO.staffId = CursorUtil.getInt(c, STAFF_ID);
		saleOrderDTO.approved = CursorUtil.getInt(c, APPROVED);
//		saleOrderDTO.approvedVan = CursorUtil.getInt(c, APPROVED_VAN);
		saleOrderDTO.totalWeight = CursorUtil.getDouble(c, TOTAL_WEIGHT);
		// saleOrderDTO.importCode = CursorUtil.getString(c, IMPORT_CODE);
		saleOrderDTO.priority = CursorUtil.getLong(c, PRIORITY);
		saleOrderDTO.deliveryDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, DELIVERY_DATE));
		// ---
		// saleOrderDTO.SYN_OPERATOR =
		// CursorUtil.getString(c, SYN_OPERATOR);
		// saleOrderDTO.synStatus = CursorUtil.getInt(c, SYN_STATUS);
		saleOrderDTO.setTotal(CursorUtil.getDouble(c, TOTAL));
		saleOrderDTO.updateDate = DateUtils.parseDateFromSqlLite(CursorUtil.getString(c, UPDATE_DATE));
		saleOrderDTO.updateUser = CursorUtil.getString(c, UPDATE_USER);
		saleOrderDTO.synState = CursorUtil.getInt(c, SYN_STATE);
		saleOrderDTO.orderSource = CursorUtil.getInt(c, ORDER_SOURCE);
		saleOrderDTO.totalDetail = CursorUtil.getInt(c, TOTAL_DETAIL);
		saleOrderDTO.quantity = CursorUtil.getLong(c, QUANTITY);
		saleOrderDTO.routingId = CursorUtil.getInt(c, ROUTING_ID);
		saleOrderDTO.accountDate = CursorUtil.getString(c, ACCOUNT_DATE);
		saleOrderDTO.isRewardKS = CursorUtil.getInt(c, IS_REWARD_KS) == 1?true:false;

		return saleOrderDTO;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<SaleOrderDTO>
	 * @throws:
	 */
	public Vector<SaleOrderDTO> getAllRow() {
		Vector<SaleOrderDTO> v = new Vector<SaleOrderDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				SaleOrderDTO SaleOrderDTO;
				if (c.moveToFirst()) {
					do {
						SaleOrderDTO = initSaleOrderDTOFromCursor(c);
						v.addElement(SaleOrderDTO);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	/**
	 * Init value sale_order_table
	 *
	 * @author : BangHN since : 3:35:36 PM
	 */
	private ContentValues initDataRow(SaleOrderDTO dto) {
		ContentValues editedValues = new ContentValues();

		// not null field
		if (dto.saleOrderId > 0) {
			editedValues.put(SALE_ORDER_ID, dto.saleOrderId);
		}
		if (dto.poId > 0) {
			editedValues.put(FROM_PO_CUSTOMER_ID, dto.poId);
		} else if (dto.type == 2 && dto.fromPOCustomerId > 0) {
			// truong hop don tra khong co poId
			editedValues.put(FROM_PO_CUSTOMER_ID, dto.fromPOCustomerId);
		}
		if (dto.fromSaleOrderId > 0) {
			editedValues.put(FROM_SALE_ORDER_ID, dto.fromSaleOrderId);
		}
		if (dto.shopId > 0) {
			editedValues.put(SHOP_ID, dto.shopId);
		}
		if (!StringUtil.isNullOrEmpty(dto.orderNumber)) {
			editedValues.put(ORDER_NUMBER, dto.orderNumber);
		}

		if (!StringUtil.isNullOrEmpty(dto.refOrderNumber)) {
			editedValues.put(REF_ORDER_NUMBER, dto.refOrderNumber);
		}

		if (!StringUtil.isNullOrEmpty(dto.orderDate)) {
			editedValues.put(ORDER_DATE, dto.orderDate);
		}
		if (dto.customerId > 0) {
			editedValues.put(CUSTOMER_ID, dto.customerId);
		}

		if (dto.staffId > 0) {
			editedValues.put(STAFF_ID, dto.staffId);
		}
		if (!StringUtil.isNullOrEmpty(dto.deliveryId) && !StringUtil.isNullOrEmpty(String.valueOf(dto.cashierId))) {
			editedValues.put(DELIVERY_ID, dto.deliveryId);
			editedValues.put(CASHIER_ID, dto.cashierId);
		}

		if (dto.getAmount() >= 0) {
			editedValues.put(AMOUNT, dto.getAmount());
		}
		if (dto.getTotal() >= 0) {
			editedValues.put(TOTAL, dto.getTotal());
		}
		if (!StringUtil.isNullOrEmpty(dto.createUser)) {
			editedValues.put(CREATE_USER, dto.createUser);
		}

		if (!StringUtil.isNullOrEmpty(dto.createDate))
			editedValues.put(CREATE_DATE, dto.createDate);

		// can null field
		if (dto.orderType != null) {
			editedValues.put(ORDER_TYPE, dto.orderType);
		} else {
			editedValues.put(ORDER_TYPE, "");
		}

		// if (dto.invoiceNumber != null) {
		// editedValues.put(INVOICE_NUMBER, dto.invoiceNumber);
		// } else {
		// editedValues.put(INVOICE_NUMBER, "");
		// }

		if (dto.updateUser != null) {
			editedValues.put(UPDATE_USER, dto.updateUser);
		} else {
			editedValues.put(UPDATE_USER, "");
		}

		if (dto.updateDate != null) {
			editedValues.put(UPDATE_DATE, dto.updateDate);
		} else {
			editedValues.put(UPDATE_DATE, "");
		}

		if (dto.deliveryDate != null) {
			editedValues.put(DELIVERY_DATE, dto.deliveryDate);
		} else {
			editedValues.put(DELIVERY_DATE, "");
		}

		if (dto.priority >= 0) {
			editedValues.put(PRIORITY, dto.priority);
		}

		editedValues.put(DISCOUNT, dto.getDiscount());
		editedValues.put(TYPE, dto.type);
		editedValues.put(IS_VISIT_PLAN, dto.isVisitPlan);
		editedValues.put(ORDER_SOURCE, dto.orderSource);
		editedValues.put(APPROVED, dto.approved);
//		if (dto.approvedVan > 0) {
//			editedValues.put(APPROVED_VAN, dto.approvedVan);
//		}
		editedValues.put(SYN_STATE, dto.synState);
		editedValues.put(TOTAL_WEIGHT, dto.totalWeight);
		editedValues.put(TOTAL_DETAIL, dto.totalDetail);
		editedValues.put(SHOP_CODE, dto.shopCode);
		editedValues.put(APPROVED_STEP, dto.approvedStep);
		if (!StringUtil.isNullOrEmpty(dto.accountDate)) {
			editedValues.put(ACCOUNT_DATE, dto.accountDate);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(ROUTING_ID, dto.routingId);
		editedValues.put(APPROVED_VAN, dto.approvedVan);
		editedValues.put(APPROVED_DATE, dto.approvedDate);
		editedValues.put(CYCLE_ID, dto.cycleId);
		editedValues.put(IS_REWARD_KS, dto.isRewardKS?1:0);

		return editedValues;
	}

	private ContentValues initDataRowForEdit(SaleOrderDTO dto,
			boolean isCommited) {
		ContentValues editedValues = new ContentValues();

		if (!StringUtil.isNullOrEmpty(dto.deliveryId)) {
			editedValues.put(DELIVERY_ID, dto.deliveryId);
		}
		if (dto.getAmount() > 0)
			editedValues.put(AMOUNT, dto.getAmount());
		if (dto.getTotal() > 0)
			editedValues.put(TOTAL, dto.getTotal());

		if (dto.invoiceNumber != null) {
			// editedValues.put(INVOICE_NUMBER, dto.invoiceNumber);
		}

		if (dto.updateUser != null) {
			editedValues.put(UPDATE_USER, dto.updateUser);
		}

		if (dto.updateDate != null) {
			editedValues.put(UPDATE_DATE, dto.updateDate);
		} else {
			editedValues.put(UPDATE_DATE, "");
		}
		if (dto.getDiscount() > 0)
			editedValues.put(DISCOUNT, dto.getDiscount());
		// if (dto.state > 0)
		// editedValues.put(STATE, dto.state);
		if (dto.isVisitPlan > 0)
			editedValues.put(IS_VISIT_PLAN, dto.isVisitPlan);
		// if (dto.isSend > 0)
		// editedValues.put(IS_SEND, dto.isSend);
		// if (dto.synStatus > 0)
		// editedValues.put(SYN_STATUS, dto.synStatus);
		if (isCommited) {
			editedValues.put(SYN_STATE, TRANSFERED_STATUS);
		} else{
			editedValues.put(SYN_STATE, dto.synState);
		}
		editedValues.put(QUANTITY, dto.quantity);
		editedValues.put(ROUTING_ID, dto.routingId);
		return editedValues;
	}

	/**
	 * Lay so don hang tra ve tu NPP trong ngay
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	public String getOrderDaftInday(String staffId, String customerId) {
		String orderId = "";
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT SALE_ORDER_ID ");
		var1.append("FROM   sale_order ");
		var1.append("WHERE  staff_id = ? ");
		var1.append("       AND customer_id = ? ");
		var1.append("       AND syn_state = 0 ");
		var1.append("       AND approved = -1 ");
		var1.append("       AND Date(create_date) = Date('now', 'localtime') ");

		String[] params = { staffId, customerId };
		Cursor cursor = null;
		try {
			cursor = rawQuery(var1.toString(), params);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					orderId = CursorUtil.getString(cursor, "SALE_ORDER_ID");
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getOrderDaftInday", e.getMessage());
			}
		}
		return orderId;
	}

	/**
	 * tinh binh quan doanh so cua khach hang trong thang
	 *
	 * @author : BangHN
	 * @param month
	 *            : Truoc do month thang, "0" thang hien tai since : 1.0
	 * @throws Exception
	 */
	public long getAverageSalesInMonth(String customerId, String shopId)
			throws Exception {
		long averageSale = 0;
		Cursor cursor = null;
		StringBuffer sql = new StringBuffer();
		// sql.append("SELECT ( Sum (CASE ");
		// sql.append("                WHEN so.order_type = 'CM' THEN -total ");
		// sql.append("                WHEN so.order_type = 'CO' THEN -total ");
		// sql.append("                ELSE total ");
		// sql.append("              end) ) AS accumulated ");
		// sql.append("FROM   sale_order so ");
		// sql.append("WHERE  so.customer_id = ? ");
		// sql.append("       AND so.shop_id = ? ");
		// sql.append("       AND ( ( so.[order_type] != 'CM' ");
		// sql.append("               AND so.[order_type] != 'CO' ");
		// sql.append("               AND so.[approved] = 1 ");
		// sql.append("               AND Date(so.order_date) < Date('NOW', 'localtime') ");
		// sql.append("               AND Date(so.order_date) >= Date('NOW', 'localtime', ");
		// sql.append("                                          'start of month') ) ");
		// sql.append("              OR ( so.[order_type] != 'CM' ");
		// sql.append("                   AND so.[order_type] != 'CO' ");
		// sql.append("                   AND so.[approved] != 2 ");
		// sql.append("                   AND Date(so.order_date) = Date('NOW', 'localtime') ) ) ");

		sql.append("select sum(sim.amount) as accumulated, strftime('%m', sim.month)  as month");
		sql.append(" from rpt_sale_in_month sim");
		sql.append(" where sim.CUSTOMER_ID = ? and sim.amount > 0 ");
		// sql.append(" and dayInOrder(sim.MONTH) <= dayInOrder('now','localtime','start of month', '-1 fullDate')");
		sql.append(" and dayInOrder(sim.MONTH) >= dayInOrder('now','localtime','start of month')");
		sql.append(" group by month");

		List<String> params = new ArrayList<String>();
		params.add(customerId);
		params.add(shopId);
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					averageSale = CursorUtil.getLong(cursor, "accumulated");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return averageSale;
	}

	/**
	 * lay doanh so/ san luong trong thang
	 * @author: hoanpd1
	 * @since: 15:07:29 21-03-2015
	 * @return: ArrayList<Long>
	 * @throws:
	 * @param customerId
	 * @param shopId
	 * @param sysCalUnApproved
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Double> getAverageSalesInMonth2(String customerId, String shopId, int sysCalUnApproved, int sysCurrencyDivide, CustomerInfoDTO customerInfoDTO)
			throws Exception {
		ArrayList<Double> liverageSale = new ArrayList<Double>();
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
		boolean isStaffSale = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF ;
		ArrayList<String> params = new ArrayList<String>();
		Cursor cursor = null;
		StringBuffer sql = new StringBuffer();
		sql.append("select sum(sim1.amount) as amount, ");
		sql.append("       sum(sim1.quantity) as quantity ");
		sql.append(" from (select SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.amount ");
		}else {
			sql.append(" 	 sim.amount ");
		}
		sql.append(") 	AS AMOUNT, SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	 sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	 sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	 sim.quantity ");
		}else {
			sql.append(" 	 sim.quantity ");
		}
		sql.append(") 	AS QUANTITY ");
		sql.append(" from rpt_sale_primary_month sim ");
		sql.append(" where sim.CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" and sim.shop_id = ? ");
		params.add(shopId);
		if (isStaffSale) {
			sql.append(" and sim.staff_id  = ? ");
			params.add(staffId);
		}
//		sql.append(" and dayInOrder(sim.affect_date, 'start of month') = ? ");
		sql.append("	AND substr(sim.affect_date, 1, 10) = substr(?, 1, 10)");
		params.add(startOfMonth);
		sql.append(" group by sim.product_id) sim1 ");
		try {
			cursor = rawQueries(sql.toString(), params);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					customerInfoDTO.amountInMonth = CursorUtil.getDoubleUsingSysConfig(cursor, "amount");
					customerInfoDTO.quantityInMonth = CursorUtil.getLong(cursor, "quantity");
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return liverageSale;
	}

	/**
	 * Lay thong tin doanh so 3 thang gan fullDate
	 *
	 * @author : BangHN since : 1.0
	 * @param shopId
	 */
	public ArrayList<SaleInMonthDTO> getAverageSalesIn3MonthAgo(
			String customerId, String shopId, int sysCalUnApproved, int sysCurrencyDivide) throws Exception {
		String startOfPreviousThreeMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -3);
		String endOfPreviousMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), -1);
		StringBuilder sql = new StringBuilder();
		ArrayList<String> params = new ArrayList<String>();
		Cursor cursor = null;
		ArrayList<SaleInMonthDTO> saleIn3Month = new ArrayList<SaleInMonthDTO>();
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
//		boolean isStaffSale = (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_STAFF ||
//									GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES ) ;
		boolean isStaffSale = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF;
		sql.append(" select sum(sim1.amount) as amount, ");
		sql.append("        sum(sim1.quantity) as quantity , ");
		sql.append("        sim1.NUM              as month, ");
		sql.append("        sim1.CYCLE_ID         as CYCLE_ID ");
//		sql.append("        strftime('%m', sim1.month)  as month");
		sql.append(" from (select SUM(");
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	      sim.quantity_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	      sim.quantity_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	      sim.quantity ");
		}else {
			sql.append(" 	      sim.quantity ");
		}
		sql.append(") 	        AS QUANTITY, SUM(");

		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append(" 	      sim.amount_approved ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" 	      sim.amount_pending ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" 	      sim.amount ");
		}else {
			sql.append(" 	     sim.amount ");
		}
		sql.append(") 	        AS AMOUNT, ");
		sql.append("            sim.affect_date  as month, ");
		sql.append("        cy.NUM              as NUM , ");
		sql.append("        cy.CYCLE_ID         as CYCLE_ID   ");
		sql.append("	   from RPT_SALE_PRIMARY_MONTH sim, cycle cy ");
		sql.append(" where sim.CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append("       and sim.amount > 0");
//		sql.append(" and sim.status = 1");
		sql.append(" and substr(sim.affect_date, 1, 10) <= substr(?, 1, 10) ");
		params.add(endOfPreviousMonth);
		sql.append(" and substr(sim.affect_date, 1, 10) >= substr(?, 1, 10) ");
		params.add(startOfPreviousThreeMonth);
		sql.append(" and sim.shop_id = ?");
		params.add(shopId);
		sql.append(" AND sim.CYCLE_ID = cy.CYCLE_ID  ");
		if (isStaffSale) {
//			sql.append("        and sim.object_id = ? ");
			sql.append("        and sim.staff_id = ? ");
			params.add(staffId);
//			sql.append("        and sim.object_type = ? ");
//			params.add(String.valueOf(UserDTO.TYPE_STAFF));
		}
		sql.append(" group by sim.product_id, sim.affect_date) sim1 ");
		sql.append(" group by CYCLE_ID");

		try {

			cursor = rawQueries(sql.toString(), params);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						SaleInMonthDTO dto = new SaleInMonthDTO();
						dto.parseSaleInMonth(cursor, sysCurrencyDivide);
						saleIn3Month.add(dto);
					} while (cursor.moveToNext());
				}
			}
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return saleIn3Month;
	}

	/**
	 * Lay so mat hang khac nhau ban trong thang cho khach hang (sku)
	 *
	 * @author : BangHN since : 1.0
	 * @throws Exception
	 */
	public int getSKUOfCustomerInMonth(String customerId, String shopId, int sysCalUnApproved)
			throws Exception {
		int sku = 0;
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		Cursor cursor = null;
		ArrayList<String> params = new ArrayList<String>();
		String staffId = String.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
//		boolean isStaffSale = (GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_STAFF ||
//				GlobalInfo.getInstance().getProfile().getUserData().chanelObjectType == UserDTO.TYPE_VALSALES ) ;
		boolean isStaffSale = GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_STAFF ;
		StringBuffer sql = new StringBuffer();
		// sql.append("SELECT Count (DISTINCT product_id) sku ");
		// sql.append("FROM   (SELECT DISTINCT product_id, ");
		// sql.append("                        Sum(( CASE ");
		// sql.append("                                WHEN s.order_type = 'CM' THEN -d.amount ");
		// sql.append("                                WHEN s.order_type = 'CO' THEN -d.amount ");
		// sql.append("                                ELSE d.amount ");
		// sql.append("                              end )) sotien ");
		// sql.append("        FROM   sale_order s, ");
		// sql.append("               sale_order_detail d ");
		// sql.append("        WHERE  1 = 1 ");
		// sql.append("               AND s.sale_order_id = d.sale_order_id ");
		// sql.append("               AND customer_id = ? ");
		// sql.append("               AND s.shop_id = ? ");
		// sql.append("               AND d.is_free_item = 0 ");
		// sql.append("               AND ( ( s.approved = 1 ");
		// sql.append("                       AND Date(s.order_date) >= Date('now', 'localtime','start of month') ");
		// sql.append("                       AND Date(s.order_date) < Date ('now', 'localtime') ) ");
		// sql.append("                      OR ( s.approved != 2 ");
		// sql.append("                           AND Date(s.order_date) = Date ('now', 'localtime') ) ");
		// sql.append("                   ) ");
		// sql.append("        GROUP  BY product_id) ");
		// sql.append("WHERE  sotien > 0 ");



		sql.append("SELECT Count (DISTINCT sim.product_id) sku ");
		sql.append("FROM   rpt_sale_primary_month sim ");
		sql.append("WHERE  sim.customer_id = ? ");
		params.add(customerId);
		sql.append("	  and sim.shop_id = ?  ");
		params.add(shopId);
		if(sysCalUnApproved == ApParamDTO.SYS_CAL_APPROVED){
			sql.append("  and sim.amount_approved > 0 ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED_PENDING) {
			sql.append(" and sim.amount_pending > 0 ");
		}else if (sysCalUnApproved == ApParamDTO.SYS_CAL_UNAPPROVED) {
			sql.append(" and sim.amount > 0  ");
		}else {
			sql.append(" and sim.amount > 0  ");
		}
		if (isStaffSale) {
			sql.append("	AND sim.staff_id = ?");
			params.add(staffId);
		}
		sql.append("	AND substr(sim.affect_date, 1, 10) = substr(?, 1, 10)");
		params.add(startOfMonth);
		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					sku = CursorUtil.getInt(cursor, "sku");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return sku;
	}

	/**
	 * Lay so tien ban duoc trong ngay (so tien cua cac don hang)
	 *
	 * @author : BangHN since : 1.0
	 * @throws Exception
	 */
	public String getAmountInDay(String customerId) throws Exception {
		String amount = "0";
		Cursor cursor = null;
		StringBuilder sql = new StringBuilder();
		List<String> params = new ArrayList<String>();
		sql.append(" select sum(amount) as  accumulated from sales_order");
		sql.append(" where customer_id = ?");
		sql.append(" and  DATE(ORDER_DATE) >= DATE('NOW','-0 fullDate')");// ngay
																		// hien
																		// tai
		params.add(customerId);
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					amount = CursorUtil.getString(cursor, "accumulated");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return amount;
	}

	/**
	 * lay so don hang cua khach hang dat trong thang
	 *
	 * @author : BangHN since : 1.0
	 * @throws Exception
	 */
	public int getNumberOrdersInMonth(String customerId, String shopId)
			throws Exception {
		int numOrders = 0;
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		String startOfMonth = new EXCEPTION_DAY_TABLE(mDB).getFirstDayOfOffsetCycle(new Date(), 0);
		Cursor cursor = null;
		List<String> params = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		// sql.append("SELECT Count(1) AS COUNT ");
		// sql.append("FROM   (SELECT Date(s.order_date), ");
		// sql.append("               Sum(CASE ");
		// sql.append("                     WHEN s.order_type = 'CM' THEN -s.total ");
		// sql.append("                     WHEN s.order_type = 'CO' THEN -s.total ");
		// sql.append("                     ELSE s.total ");
		// sql.append("                   end) AS tong ");
		// sql.append("        FROM   sale_order s ");
		// sql.append("        WHERE  s.customer_id = ? ");
		// sql.append("               AND s.shop_id = ? ");
		// sql.append("               AND ( ( s.[order_type] != 'CM' ");
		// sql.append("                       AND s.[order_type] != 'CO' ");
		// sql.append("                       AND s.[approved] = 1 ");
		// sql.append("                       AND Date(s.order_date) < ? ");
		// params.add(dateNow);
		// sql.append("                       AND Date(s.order_date) >= ? ");
		// sql.append("                                                  ) ");
		// params.add(startOfMonth);
		// sql.append("                      OR ( s.[order_type] != 'CM' ");
		// sql.append("                           AND s.[order_type] != 'CO' ");
		// sql.append("                           AND s.[approved] != 2 ");
		// sql.append("                           AND Date(s.order_date) = ? ) ) ");
		// params.add(dateNow);
		// sql.append("        GROUP  BY Date(s.order_date)) ");
		// sql.append("WHERE  tong > 0 ");
		sql.append("       select count( t2.ngay) AS COUNT ");
		sql.append("       		from( ");
		sql.append("       				select dayInOrder(s1.order_date) ngay,  count(s1.sale_order_id) ");
		sql.append("       				from sale_order s1  ");
		sql.append("       				where s1.customer_id = ? ");
		params.add(customerId);
		sql.append("       					and shop_id= ? ");
		params.add(shopId);
		sql.append("       					and s1.type = 1 ");
		sql.append("       					and ((dayInOrder(s1.order_date) < ? ");
		params.add(dateNow);
		sql.append("       					and dayInOrder(s1.order_date) >= ? ");
		params.add(startOfMonth);
		sql.append("       					and s1.approved = 1 ) ");
		sql.append("       					or (dayInOrder(s1.order_date) = ? ");
		params.add(dateNow);
		sql.append("      					 and s1.approved in (0, 1) )) ");
		sql.append("       					group by dayInOrder(s1.order_date) ");
		sql.append("       		having count(s1.sale_order_id)>0) t2 ");

		// String shopId =
		// GlobalInfo.getInstance().getProfile().getUserData().shopId;
		// params.add(customerId);
		// params.add(shopId);
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					numOrders = CursorUtil.getInt(cursor, "COUNT");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {
			}
		}
		return numOrders;
	}

	/**
	 *
	 * lay danh sach don dat hang trong ngay.
	 *
	 * @author: thuattq
	 * @param sortInfo
	 * @param listStaff
	 * @return : List<SaleOrderViewDTO>
	 * @throws :
	 */
	public ListOrderMngDTO getSalesOrderInDate(String fromDate, String toDate,
											   String customerCode, String customerName, String typeRoute,
											   String status, String billCategory, String staffId, String shopId, boolean bApproved,
											   String page, String customerId, boolean isGetTotalPage, DMSSortInfo sortInfo, String listStaff)
			throws Exception {

		ListOrderMngDTO listSaleOrderViewDTO = new ListOrderMngDTO();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		Cursor cursor = null;
		StringBuilder sql1 = new StringBuilder();
		List<String> params1 = new ArrayList<String>();
		Cursor cursor1 = null;
		boolean isDefault = true;

		if (!StringUtil.isNullOrEmpty(customerId)) {
			sql1.append(" SELECT DISTINCT SO.ORDER_DATE as ORDER_DATE, ");
			sql1.append(" 		CT.CUSTOMER_CODE as CUSTOMER_CODE, ");
			sql1.append(" 		CT.CUSTOMER_ID as CUSTOMER_ID, ");
			sql1.append(" 		CT.CUSTOMER_NAME as CUSTOMER_NAME, ");
			sql1.append(" 		CT.CASHIER_STAFF_ID as CASHIER_STAFF_ID, ");
			sql1.append(" 		CT.DELIVER_ID as DELIVER_ID, ");
			sql1.append(" 		CT.LAT as CUSTOMER_LAT, ");
			sql1.append(" 		CT.LNG as CUSTOMER_LNG, ");
//			sql1.append("		CT.CUSTOMER_TYPE_ID AS CUSTOMER_TYPE_ID, ");
			sql1.append("		CT.SHOP_ID as SHOP_ID, ");
			sql1.append("		SO.TOTAL as TOTAL, ");
			sql1.append("                SO.AMOUNT                                  AS AMOUNT, ");
			sql1.append("		SO.approved as APPROVED, ");
			sql1.append("		SO.PRIORITY as PRIORITY,  ");
			sql1.append("		SO.APPROVED_DATE as APPROVED_DATE,  ");
			sql1.append("                SO.type                             	  AS TYPE, ");
			sql1.append("                SO.DELIVERY_ID                                  AS DELIVERY_ID, ");
			sql1.append("                SO.CASHIER_ID                                  AS CASHIER_ID, ");
			sql1.append("                SO.DISCOUNT                                  AS DISCOUNT, ");
			sql1.append("                SO.DELIVERY_DATE                                  AS DELIVERY_DATE, ");
			sql1.append("                SO.SHOP_CODE                                  AS SHOP_CODE, ");
			sql1.append("                SO.TOTAL_WEIGHT                             	  AS TOTAL_WEIGHT, ");
			sql1.append("                SO.TOTAL_DETAIL                             	  AS TOTAL_DETAIL, ");
			sql1.append("                SO.ROUTING_ID                             	  AS ROUTING_ID, ");
			sql1.append("		CT.ADDRESS  AS ADDRESS,  ");
			sql1.append("                SO.IS_REWARD_KS                            		AS IS_REWARD_KS, ");
			sql1.append(" 	SO.IS_VISIT_PLAN as IS_VISIT_PLAN, SO.ORDER_NUMBER as ORDER_NUMBER, SO.REF_ORDER_NUMBER as REF_ORDER_NUMBER, (CT.HOUSENUMBER || ' ' || CT.STREET) AS STREET ,SO.ORDER_TYPE as ORDER_TYPE, SO.SALE_ORDER_ID as SALE_ORDER_ID, SO.SYN_STATE AS SYN_STATE, ");
			sql1.append(" SO.STAFF_ID as STAFF_ID, SO.ORDER_DATE as ORDER_DATE, SO.CREATE_USER as CREATE_USER ");
			sql1.append(" FROM SALE_ORDER SO ");
			sql1.append(" INNER JOIN CUSTOMER CT ");
			sql1.append(" ON SO.CUSTOMER_ID = CT.CUSTOMER_ID ");
			sql1.append(" WHERE 1 = 1 ");
			if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
				if(!staffId.equalsIgnoreCase("0")){
					sql1.append(" 	AND SO.STAFF_ID = ? ");
					params1.add(staffId);
				}else{
					// chon tat ca
					sql1.append(" 	AND SO.STAFF_ID IN ( " + listStaff +" ) ");
				}
			}else{
				sql1.append(" 	AND SO.STAFF_ID = ? ");
				params1.add(staffId);
			}
			sql1.append(" 	AND SO.SHOP_ID = ? ");
			params1.add(shopId);
			sql1.append(" 	AND SO.CUSTOMER_ID = ? ");
			params1.add(customerId);
			sql1.append("       AND (SO.total > 0 ");
			// don hang doanh so = 0 & tao tu tablet
			// hoac tao tu web va co tra thuong key shop
			sql1.append("       		or (SO.total = 0 and (SO.order_source = 2 or (SO.order_source = 1 and SO.is_reward_ks = 1))) ");
			sql1.append("       		) ");
			sql1.append(" AND IFNULL( DATE(SO.ORDER_DATE) = DATE(?), 0)");
			params1.add(dateNow);
			sql1.append("	AND SO.approved = -1 ");

		}

		StringBuilder sql = new StringBuilder();
		List<String> params = new ArrayList<String>();

		sql.append("SELECT DISTINCT SO.order_date AS ORDER_DATE, ");
		sql.append("                SO.create_date                             		AS CREATE_DATE, ");
		sql.append("                CT.short_code                          			AS CUSTOMER_CODE, ");
		sql.append("                CT.customer_id                            		AS CUSTOMER_ID, ");
		sql.append("                CT.customer_name                          		AS CUSTOMER_NAME, ");
		sql.append(" 				CT.CASHIER_STAFF_ID as CASHIER_STAFF_ID, ");
		sql.append(" 				CT.DELIVER_ID as DELIVER_ID, ");
		sql.append("                CT.channel_type_id                       		AS CUSTOMER_TYPE_ID, ");
		sql.append("                CT.shop_id                                		AS SHOP_ID, ");
		sql.append(" 				CT.LAT as CUSTOMER_LAT, ");
		sql.append(" 				CT.LNG as CUSTOMER_LNG, ");
		sql.append("                SO.DELIVERY_ID                                  AS DELIVERY_ID, ");
		sql.append("                SO.CASHIER_ID                                  	AS CASHIER_ID, ");
		sql.append("                SO.total                                  		AS TOTAL, ");
		sql.append("                SO.quantity                                  	AS QUANTITY, ");
		sql.append("                SO.AMOUNT                                  		AS AMOUNT, ");
		sql.append("                SO.DISCOUNT                                  	AS DISCOUNT, ");
		sql.append("                SO.DELIVERY_DATE                                AS DELIVERY_DATE, ");
		sql.append("                SO.SHOP_CODE                                  	AS SHOP_CODE, ");
		sql.append("                SO.priority                               		AS PRIORITY, ");
		sql.append("                SO.approved                              	 	AS APPROVED, ");
		sql.append("                SO.approved_date                               	AS APPROVED_DATE, ");
		sql.append("                SO.is_visit_plan                          		AS IS_VISIT_PLAN, ");
		sql.append("                SO.order_number                           		AS ORDER_NUMBER, ");
		sql.append("                SO.ref_order_number                       		AS REF_ORDER_NUMBER, ");
		sql.append("                SO.FROM_PO_CUSTOMER_ID                       	AS FROM_PO_CUSTOMER_ID, ");
		sql.append("                CT.address              						AS ADDRESS, ");
		sql.append("                ( CT.housenumber ");
		sql.append("                  || ' ' ");
		sql.append("                  || CT.street )                          		AS STREET, ");
		sql.append("                SO.order_type                             		AS ORDER_TYPE, ");
		sql.append("                SO.type                             	  		AS TYPE, ");
		sql.append("                SO.TOTAL_WEIGHT                             	AS TOTAL_WEIGHT, ");
		sql.append("                SO.TOTAL_DETAIL                             	AS TOTAL_DETAIL, ");
		sql.append("                SO.sale_order_id                          		AS SALE_ORDER_ID, ");
		sql.append("                SO.description                            		AS DESCRIPTION, ");
		sql.append("                SO.syn_state                              		AS SYN_STATE, ");
		sql.append("                SO.staff_id                               		AS STAFF_ID, ");
		sql.append("                SO.create_user                            		AS CREATE_USER, ");
		sql.append("                SO.ROUTING_ID                            		AS ROUTING_ID, ");
		sql.append("                SO.IS_REWARD_KS                            		AS IS_REWARD_KS, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN SO.approved = -1 THEN 0 ");
		sql.append("                    WHEN SO.approved = 2  THEN 1 ");
		sql.append("                    WHEN SO.approved = 0  THEN 2 ");
		sql.append("                    WHEN SO.approved = 1  THEN 3 ");
		sql.append("                    WHEN SO.approved = 3  THEN 4 ");
		sql.append("                  END )                                   		AS TMP, ");
		sql.append("                  AP.AP_PARAM_CODE ");
		sql.append("FROM   sale_order SO ");
		sql.append("       INNER JOIN customer CT ON SO.customer_id = CT.customer_id ");
		sql.append("       LEFT JOIN AP_PARAM AP ON SO.PRIORITY = AP.AP_PARAM_ID and ap.status = 1 ");
		sql.append("WHERE  1 = 1 ");
		if (GlobalInfo.getInstance().getProfile().getUserData().getInheritSpecificType() == UserDTO.TYPE_SUPERVISOR) {
			if(!staffId.equalsIgnoreCase("0")){
				sql.append("       AND SO.staff_id = ? ");
				params.add(staffId);
			}else{
				// chon tat ca
				sql1.append(" 	AND SO.STAFF_ID IN ( " + listStaff +" ) ");
			}
		}else{
			sql.append("       AND SO.staff_id = ? ");
			params.add(staffId);
		}
		sql.append("       AND SO.shop_id = ? ");
		params.add(shopId);
		sql.append("       AND (SO.total > 0 ");
		// don hang doanh so = 0 & tao tu tablet
		// hoac tao tu web va co tra thuong key shop
		sql.append("       		or (SO.total = 0 and (SO.order_source = 2 or (SO.order_source = 1 and SO.is_reward_ks = 1))) ");
		sql.append("       		) ");
		sql.append("       AND ((SO.APPROVED = -1 AND substr(SO.CREATE_DATE, 1, 10) = ?) OR SO.APPROVED != -1) ");
		params.add(dateNow);

		// tim kiem theo ma KH
//		if (!StringUtil.isNullOrEmpty(customerCode)) {
//			isDefault = false;
//			customerCode = StringUtil.escapeSqlString(customerCode);
//			customerCode = DatabaseUtils.sqlEscapeString("%" + customerCode
//					+ "%");
//			sql.append("	and upper(substr(CT.CUSTOMER_CODE,1,3)) like upper(");
//			sql.append(customerCode);
//			sql.append(") escape '^' ");
//
//			// sql.append(" AND LOWER(substr(CT.CUSTOMER_CODE,1,3)) LIKE ? ");
//			// customer_code = StringUtil.toOracleSearchText(customer_code,
//			// false).trim();
//			// if("%%".equals(customer_code)) {
//			// params.add(customer_code);
//			// } else {
//			// params.add("%" + customer_code.toLowerCase() + "%");
//			// }
//		}
		// tim kiem theo ten/dia chi
		if (!StringUtil.isNullOrEmpty(customerName)) {
			isDefault = false;
			customerName = StringUtil
					.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			customerName = DatabaseUtils.sqlEscapeString("%" + customerName
					+ "%");
			sql.append("	and (upper(substr(CT.CUSTOMER_CODE,1,3)) like upper(");
			sql.append(customerName);
			sql.append(") escape '^' ");
			sql.append("	or (upper(CT.SHORT_CODE) like upper(");
			sql.append(customerName);
			sql.append(") escape '^') ");
			sql.append("	or (upper(CT.NAME_TEXT) like upper(");
			sql.append(customerName);
			sql.append(") escape '^') or (upper(STREET) like upper( ");
			sql.append(customerName);
			sql.append(") escape '^') or (upper(ADDRESS) like upper( ");
			sql.append(customerName);
			sql.append(") escape '^')) ");

		}

		if (!StringUtil.isNullOrEmpty(fromDate)
				|| !StringUtil.isNullOrEmpty(toDate)) {
			isDefault = false;
			if (!StringUtil.isNullOrEmpty(fromDate)) {
				sql.append(" AND substr(SO.ORDER_DATE, 1, 10) >= substr(?, 1, 10) ");
				params.add(fromDate);
			}

			if (!StringUtil.isNullOrEmpty(toDate)) {
				sql.append(" AND substr(SO.ORDER_DATE, 1, 10) <= substr(?, 1, 10) ");
				params.add(toDate);
			}

		}

		// tim kiem theo tuyen
		if (!"-1".equals(typeRoute) && !StringUtil.isNullOrEmpty(typeRoute)) {
			isDefault = false;
			sql.append("	AND SO.IS_VISIT_PLAN = ? ");
			params.add(typeRoute);
		}

		// tim kiem theo trang thai
		if (String.valueOf(ListOrderView.STATUS_WAITING_PROCESS).equals(status)) {
			isDefault = false;
//			sql.append(" AND SO.APPROVED = 0 AND SO.SYN_STATE = 2 AND substr(SO.ORDER_DATE, 1, 10) >= substr(?, 1, 10) ");
			sql.append(" AND SO.APPROVED = 0 AND SO.SYN_STATE = 2 ");
//			params.add(dateNow);
		} else if (String.valueOf(ListOrderView.STATUS_NOT_SEND).equals(status)) {
			isDefault = false;
			sql.append(" AND ((SO.APPROVED = 0 OR SO.APPROVED = 1) AND ( SO.SYN_STATE = 0 or SO.SYN_STATE = 1 ))  ");
		} else if (String.valueOf(ListOrderView.STATUS_OVERDATE).equals(status)) {
			int sysMaxdayApprove = GlobalInfo.getInstance().getSysMaxdayApprove();
			isDefault = false;
			sql.append(" AND SO.APPROVED = 0 AND SO.SYN_STATE = 2 AND DATE(SO.ORDER_DATE) < DATE('NOW','localtime',?) ");
			params.add(""+-sysMaxdayApprove + " fullDate");
		} else if (String.valueOf(ListOrderView.STATUS_SUCCESS).equals(status)) {
			isDefault = false;
			sql.append(" AND SO.APPROVED = 1 AND SO.SYN_STATE = 2 ");
		} else if (String.valueOf(ListOrderView.STATUS_DENY).equals(status)) {
			isDefault = false;
			sql.append(" AND SO.APPROVED = 2 AND SO.SYN_STATE = 2 ");
		} else if (String.valueOf(ListOrderView.STATUS_CANCEL).equals(status)) {
			isDefault = false;
			sql.append(" AND SO.APPROVED = 3 AND SO.SYN_STATE = 2 ");
		}

		// tim kiem theo Loai don
		if (String.valueOf(ListOrderView.BILL_CATEGORY_VANSALE).equals(billCategory)) {
			isDefault = false;
			sql.append(" AND SO.ORDER_TYPE = 'SO' AND (SO.TYPE = 0 OR SO.TYPE = 1) ");
		} else if (String.valueOf(ListOrderView.BILL_CATEGORY_RETURN_VANSALE).equals(billCategory)) {
			isDefault = false;
			sql.append(" AND SO.ORDER_TYPE = 'CO' AND SO.TYPE = 2 ");
		} else if (String.valueOf(ListOrderView.BILL_CATEGORY_PRESALE).equals(billCategory)) {
			isDefault = false;
			sql.append(" AND SO.ORDER_TYPE = 'IN' ");
		}

		sql.append(" and SO.ORDER_TYPE != 'CM' ");
		String orderStr = new DMSSortQueryBuilder()
				.addMapper(SortActionConstants.CODE, "ORDER_NUMBER")
				.addMapper(SortActionConstants.NAME, "CUSTOMER_CODE")
				.addMapper(SortActionConstants.DATE, "ORDER_DATE")
				.addMapper(SortActionConstants.AMOUNT, "TOTAL")
				.addMapper(SortActionConstants.QUANITY, "SO.quantity")
				.defaultOrderString(
						" ORDER BY datetime(SO.ORDER_DATE) DESC, TMP ASC ")
				.build(sortInfo);
		// add order string
		sql.append(orderStr);
//		sql.append(" ORDER BY datetime(SO.ORDER_DATE) DESC, TMP ASC  ");
		try {
			if (isGetTotalPage) {
				String sqlTotal = " select count(*) as total_row from (" + sql.toString() + ")";
				// lay tong so
				cursor = rawQuery(sqlTotal, params.toArray(new String[params.size()]));
				if (cursor != null) {
					cursor.moveToFirst();
					listSaleOrderViewDTO.total = CursorUtil.getInt(cursor, "total_row");
				}
				cursor.close();
			}
			cursor = rawQuery(sql.toString() + page,
					params.toArray(new String[params.size()]));
			if (cursor != null) {
				listSaleOrderViewDTO.listDTO = SaleOrderViewDTO
						.initListDataFromCursor(cursor);
			}

			EXCEPTION_DAY_TABLE ex = new EXCEPTION_DAY_TABLE(mDB);
			int sysMaxdayApprove = GlobalInfo.getInstance().getSysMaxdayApprove();
			int sysMaxdayReturn = GlobalInfo.getInstance().getSysMaxdayReturn();
			//tra don la min sysMaxdayReturn va sysMaxdayApprove
			sysMaxdayReturn = Math.min(sysMaxdayApprove, sysMaxdayReturn);

			// duyet qua tung don hang
			for (SaleOrderViewDTO saleDTO : listSaleOrderViewDTO.listDTO) {
				ArrayList<String> listExceptionDay = new ArrayList<String>();
				// lay nhung ngay nghi tu ngay dat hang ve sau nay
				listExceptionDay = ex.getMaxDayOrderApproved(shopId, saleDTO.getOrderDate());
				Date d = DateUtils.parseDateFromString(saleDTO.getOrderDate(), DateUtils.DATE_FORMAT_NOW);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(d);
				int startPosApprove = 0;
				while (startPosApprove < sysMaxdayApprove) {
					if (listExceptionDay.size() > 0) {
						for (String dayExcep : listExceptionDay) {
							Date dExcep = DateUtils.parseDateFromString(
									dayExcep, DateUtils.DATE_FORMAT_DATE);
							// so sanh neu ngay nghi = ngay dat hang thi tinh
							// tiep
							Date time = DateUtils.parseDateFromString(DateUtils
									.convertDateTimeWithFormat(
											calendar.getTime(),
											DateUtils.DATE_FORMAT_DATE),
									DateUtils.DATE_FORMAT_DATE);
							int resultCompare = DateUtils.compare(dExcep,time);
							if (resultCompare == 0) {
								startPosApprove++;
							} else if (resultCompare == 1) {
								// neu qua so ngay dat hang cho phep
								startPosApprove++;
								if (startPosApprove < sysMaxdayApprove) {
									calendar.add(Calendar.DAY_OF_YEAR, 1);
								}
								break;
							}
						}
					} else if (listExceptionDay.size() <= 0) {
						startPosApprove = sysMaxdayApprove;
						calendar.add(Calendar.DAY_OF_YEAR, startPosApprove);
					}
				}
				// set max approved = ngay toi da
				saleDTO.saleOrder.maxApproved = DateUtils.formatDate(
						calendar.getTime(), DateUtils.DATE_FORMAT_NOW);

				// tinh ngay toi da duoc tra don van

				Date dReturn = DateUtils.parseDateFromString(saleDTO.getOrderDate(), DateUtils.DATE_FORMAT_NOW);
				Calendar calenReturn = Calendar.getInstance();
				calenReturn.setTime(dReturn);
				int startPosReturn = 0;
				while (startPosReturn < sysMaxdayReturn) {
					if (listExceptionDay.size() > 0) {
						for (String dayExcep : listExceptionDay) {
							Date dExcep = DateUtils.parseDateFromString(
									dayExcep, DateUtils.DATE_FORMAT_DATE);
							Date time = DateUtils.parseDateFromString(DateUtils
									.convertDateTimeWithFormat(
											calendar.getTime(),
											DateUtils.DATE_FORMAT_DATE),
									DateUtils.DATE_FORMAT_DATE);
							int resultCompare = DateUtils.compare(dExcep,time);
							if (resultCompare == 0) {
								startPosReturn++;
							} else if (resultCompare == 1) {
								// neu qua so ngay dat hang cho phep
								startPosReturn++;
								if (startPosReturn < sysMaxdayReturn) {
									calenReturn.add(Calendar.DAY_OF_YEAR, 1);
								}
								break;
							}
						}
					} else if (listExceptionDay.size() <=0) {
						startPosReturn = sysMaxdayReturn;
						calenReturn.add(Calendar.DAY_OF_YEAR, startPosReturn);
					}
				}
				// set max approved = ngay toi da
				saleDTO.saleOrder.maxReturn = DateUtils.formatDate(
						calenReturn.getTime(), DateUtils.DATE_STRING_YYYY_MM_DD);
			}

			if (isDefault && !StringUtil.isNullOrEmpty(customerId)) {
				cursor1 = rawQuery(sql1.toString(),
						params1.toArray(new String[params1.size()]));
				if (cursor1 != null) {
					List<SaleOrderViewDTO> tempList = SaleOrderViewDTO
							.initListDataFromCursor(cursor1);

					if (tempList.size() > 0) {
						listSaleOrderViewDTO.listDTO.add(0, tempList.get(0));

						if (isGetTotalPage) {
							listSaleOrderViewDTO.total++;
						}
					}
				}
			}

		} catch (Exception ex) {
			ServerLogger.sendLog("getSalesOrderInDate", ex.getMessage(),
					TabletActionLogDTO.LOG_EXCEPTION);
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			}
		}
		return listSaleOrderViewDTO;
	}

	/**
	 *
	 * Lay ngay dat hang cuoi cung
	 *
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: String
	 * @throws:
	 */
	public String getPreviousLastOrderDate(SaleOrderViewDTO dto) {
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		StringBuilder sqlTotal = new StringBuilder();
		sqlTotal.append("select order_date as ORDER_DATE from sale_order so ");
		sqlTotal.append(" where customer_id = ?");
		params.add(String.valueOf(dto.getCustomerId()));
		sqlTotal.append(" and create_user = ?");
		params.add(dto.saleOrder.createUser);
		sqlTotal.append(" AND IFNULL( DATE(SO.ORDER_DATE) = DATE(?), 0) ");
		params.add(dateNow);
		sqlTotal.append(" and syn_state != 3 and syn_state != 4 and amount > 0 ");
		sqlTotal.append(" order by order_date desc ");

		Cursor cursor = null;
		String result = "";
		ArrayList<String> listOrderDate = new ArrayList<String>();
		try {
			// lay tong so
			cursor = rawQuery(sqlTotal.toString(),
					params.toArray(new String[params.size()]));
			if (cursor != null) {
				while (cursor.moveToNext()) {
					listOrderDate.add(CursorUtil.getString(cursor, SALE_ORDER_TABLE.ORDER_DATE));
				}
			}
		} catch (Exception ex) {
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			}
		}

		if (listOrderDate.size() == 1) {// Co 1 don hang cuoi cung
			result = "";
		} else if (listOrderDate.size() >= 2) {
			if (dto.saleOrder.orderDate.equals(listOrderDate.get(0))) {
				result = listOrderDate.get(1);
			} else {
				result = dto.saleOrder.orderDate;
			}
		}

		return result;
	}


	/**
	 *
	*  Lay ngay dat hang cuoi cung
	*  @author: Nguyen Thanh Dung
	*  @param dto
	*  @return
	*  @return: String
	*  @throws:
	 */
	public String getPreviousLastOrderDate(OrderViewDTO dto) {
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();
		StringBuilder sqlTotal = new StringBuilder();
		sqlTotal.append("select order_date as ORDER_DATE from sale_order so ");
		sqlTotal.append(" where customer_id = ?");
		params.add(String.valueOf(dto.customer.customerId));
		sqlTotal.append(" and create_user = ?");
		params.add(dto.orderInfo.createUser);
		sqlTotal.append(" AND IFNULL( DATE(SO.ORDER_DATE) = DATE(?), 0) ");
		params.add(dateNow);
		sqlTotal.append(" and syn_state != 3 and syn_state != 4 and (amount > 0 or so.order_date = ? )");
		params.add(dto.orderInfo.orderDate);
		sqlTotal.append(" order by order_date desc ");

		Cursor cursor = null;
		String result = "";
		ArrayList<String> listOrderDate = new ArrayList<String>();
		try {
			// lay tong so
			cursor = rawQuery(sqlTotal.toString(),params.toArray(new String[params.size()]));
			if (cursor != null) {
				while(cursor.moveToNext()) {
					listOrderDate.add(CursorUtil.getString(cursor, SALE_ORDER_TABLE.ORDER_DATE));
				}
			}
		} catch (Exception ex) {
			MyLog.e("getPreviousLastOrderDate", "fail", ex);
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			}
		}

		if(listOrderDate.size() == 1) {//Co 1 don hang cuoi cung
			result = "";
		} else if (listOrderDate.size() >= 2) {
			if(dto.orderInfo.orderDate.equals(listOrderDate.get(0))) {
				result = listOrderDate.get(1);
			} else {
				result = dto.orderInfo.orderDate;
			}
		}

		return result;
	}

	/**
	 *
	 * Lay so don hang chua chuyen
	 *
	 * @author: thuattq
	 * @return : List<SaleOrderViewDTO>
	 * @throws Exception
	 * @throws :
	 */
	public int getTotalSalesOrderNotSend(String staffId, String shop_id) throws Exception {
		int count = 0;
		StringBuilder sql = new StringBuilder();
		Cursor cursor = null;
		List<String> params = new ArrayList<String>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);

		sql.append(" SELECT count(*) as Total ");
		sql.append(" FROM SALE_ORDER SO ");
		sql.append(" WHERE SO.STAFF_ID = ? ");
		params.add(staffId);
		sql.append(" 	AND SO.SHOP_ID = ? ");
		params.add(shop_id);
		sql.append(" 	AND SO.APPROVED = 2 ");
		sql.append("  AND IFNULL( DATE(SO.ORDER_DATE) >= DATE(?), 0)");
		params.add(dateNow);

		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);
			if (cursor != null) {
				cursor.moveToFirst();
				count = CursorUtil.getInt(cursor, "Total");
			}

		} catch (Exception ex) {
			ServerLogger.sendLog("getTotalSalesOrderNotSend", ex.getMessage(),
					TabletActionLogDTO.LOG_EXCEPTION);
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
					throw ex;
				}
			} else {

			}
		}
		return count;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((SaleOrderDTO) dto);
		return insert(null, value);
	}

	/**
	 * Lay tong so don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 3:41:04 PM
	 * @throws Exception
	 */
	public int getCountLastSaleOrders(String customerId, String shopId,
			String staffId) throws Exception {
		Cursor cursor = null;
		int count = 0;
		StringBuilder sql = new StringBuilder();
		List<String> params = new ArrayList<String>();
		sql.append(" select count((select count(product_id)");
		sql.append(" from sales_order_detail sod where sod.sale_order_id = s.sale_order_id)) as count");
		sql.append(" from sales_order s");
		sql.append(" WHERE   s.customer_id = ?");
		sql.append(" and   s.shop_id = ?");
		sql.append(" and   s.staff_id = ?");
		params.add(customerId);
		params.add(shopId);
		params.add(staffId);
		try {
			String[] arrParam = new String[params.size()];
			for (int i = 0; i < params.size(); i++) {
				arrParam[i] = params.get(i);
			}
			cursor = rawQuery(sql.toString(), arrParam);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					count = CursorUtil.getInt(cursor, "count");
				}
			}
			return count;
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
	}

	/**
	 * Lay nhung don hang gan fullDate cua khach hang
	 *
	 * @author : BangHN since : 1:50:02 PM
	 * @throws Exception
	 */
	public ArrayList<SaleOrderCustomerDTO> getLastSaleOrders(String customerId,
			String shopId, int page, int numTop, int sysCurrencyDivide) throws Exception {
		Cursor cursor = null;
		ArrayList<SaleOrderCustomerDTO> result = new ArrayList<SaleOrderCustomerDTO>();
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		ArrayList<String> params = new ArrayList<String>();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT s.sale_order_id, ");
		sql.append("       Strftime('%d-%m-%Y %H:%M:%S', s.order_date) AS ORDER_DATE, ");
		sql.append("       s.order_number, ");
		sql.append("       s.total, ");
		sql.append("       (SELECT Count(distinct product_id) ");
		sql.append("        FROM   sale_order_detail sod ");
		sql.append("        WHERE  sod.sale_order_id = s.sale_order_id ");
		sql.append("               AND sod.is_free_item = 0)           AS SKU ");
		sql.append("FROM   sale_order s ");
		sql.append("WHERE  s.customer_id = ? ");
		params.add(customerId);
		sql.append("       AND s.shop_id = ? ");
		params.add(shopId);
		sql.append("       AND s.total > 0 ");
		sql.append("       AND s.type = 1 ");
		sql.append("       AND ( (  ");
		sql.append("               s.[approved] = 1 ");
		sql.append("               AND Date(s.order_date) < ? ) ");
		params.add(dateNow);
		sql.append("              OR ( ");
		sql.append("                   s.[approved] in (0,1) ");
		sql.append("                   AND Date(s.order_date) = ? ) ) ");
		params.add(dateNow);
		sql.append("ORDER  BY s.order_date DESC, ");
		sql.append("          s.order_number DESC ");
		sql.append("LIMIT  ?, ? ");
		params.add("" + (page - 1) * numTop);
		params.add("" + numTop);
		try {
			cursor = rawQueries(sql.toString(), params);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						SaleOrderCustomerDTO saleOrder = SaleOrderCustomerDTO
								.initOrderFromCursor(cursor, sysCurrencyDivide);
						if (saleOrder != null)
							result.add(saleOrder);
					} while (cursor.moveToNext());
				}
			}
			return result;
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (cursor != null) {
				try {
					cursor.close();
				} catch (Exception ex) {
				}
			} else {

			}
		}
	}

	/**
	 * Cap nhat trang thai dong bo don hang
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @param dto
	 * @return: int
	 * @throws:
	 */
	public long updateState(String saleOrderId, int synState) {
		ContentValues value = new ContentValues();
		value.put(SYN_STATE, synState);
		String[] params = { "" + saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * Lay ds don hang chua send thanh cong
	 *
	 * @author: TamPQ
	 * @param e
	 * @return: void
	 * @throws:
	 */
	public NoSuccessSaleOrderDto getNoSuccessOrderList(ArrayList<String> idList) {
		NoSuccessSaleOrderDto dto = null;
		Cursor c = null;

		StringBuffer sql = new StringBuffer();
		sql.append("select so.*, ct.customer_name as CUSTOMER_NAME, substr(ct.customer_code, 0, 4) as CUSTOMER_CODE from sales_order so, customer ct");
		sql.append("	where so.sale_order_id in (");

		if (idList.size() > 1) {
			for (int i = 0; i < idList.size() - 1; i++) {
				sql.append("?,");
			}
		}
		sql.append("?)");
		sql.append("		and so.customer_id = ct.customer_id");
		sql.append("		and ct.status = 1");

		try {
			c = rawQueries(sql.toString(), idList);
			if (c != null) {
				dto = new NoSuccessSaleOrderDto();
				if (c.moveToFirst()) {
					do {
						NoSuccessSaleOrderItem item = dto
								.newNoSuccessSaleOrderItem();
						item.initNoSuccessSaleOrderItem(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return dto;
	}

	/**
	 * Lay ds don hang loi: bao gom don hang chua goi, don hang loi va don hang
	 * tra ve
	 *
	 * @author: TruongHN
	 * @param staffId
	 * @param shopId
	 * @return: NoSuccessSaleOrderDto
	 * @throws:
	 */
	public NoSuccessSaleOrderDto getAllOrderFail(int staffId, String shopId) {
		NoSuccessSaleOrderDto dto = null;
		ArrayList<String> listParams = new ArrayList<String>();

		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT DISTINCT so.sale_order_id               AS SALE_ORDER_ID, ");
		var1.append("                so.order_number                AS ORDER_NUMBER, ");
		var1.append("                so.order_date                  AS ORDER_DATE, ");
		var1.append("                so.total                       AS TOTAL, ");
		var1.append("                so.syn_state                   AS SYN_STATE, ");
		var1.append("                so.description                 AS DESCRIPTION, ");
		var1.append("                ct.customer_name               AS CUSTOMER_NAME, ");
		var1.append("                so.approved                    AS APPROVED, ");
		var1.append("                ct.short_code				AS CUSTOMER_CODE ");
		var1.append("FROM   sale_order so, ");
		var1.append("       customer ct ");
		var1.append("WHERE  so.staff_id = ? ");
		listParams.add(staffId + "");
		var1.append("		AND so.shop_id = ? ");
		listParams.add(shopId);
		var1.append("       AND ( so.sale_order_id IN (SELECT table_id ");
		var1.append("                                  FROM   log_table ");
		var1.append("                                  WHERE  table_type = 3 ");
		var1.append("                                         AND state IN ( 0, 1, 3, 4 ) ");
		var1.append("                                         AND Date(create_date) >= ");
		var1.append("                                             Date('now', 'localtime')) ) ");
		var1.append("       AND so.customer_id = ct.customer_id ");
		var1.append("UNION ");
		var1.append("SELECT DISTINCT so.sale_order_id               AS SALE_ORDER_ID, ");
		var1.append("                so.order_number                AS ORDER_NUMBER, ");
		var1.append("                so.order_date                  AS ORDER_DATE, ");
		var1.append("                so.total                       AS TOTAL, ");
//		var1.append("                so.syn_state                   AS SYN_STATE, ");
		var1.append(" 	ifnull((select state from log_table lt where so.sale_order_id = lt.table_id and lt.table_name = 'SALE_ORDER' order by lt.create_date desc limit 1 ), 2) SYN_STATE,  ");
		var1.append("                so.description                 AS DESCRIPTION, ");
		var1.append("                ct.customer_name               AS CUSTOMER_NAME, ");
		var1.append("                so.approved                    AS APPROVED, ");
		var1.append("                ct.short_code				    AS CUSTOMER_CODE ");
		var1.append("FROM   sale_order so, ");
		var1.append("       customer ct ");
		var1.append("WHERE  so.staff_id = ? ");
		listParams.add(staffId + "");
		var1.append("		AND so.shop_id = ? ");
		listParams.add(shopId);
		//Don hang da dong bo len server hoac don hang chua dong bo (sai KM)
		var1.append("       AND (( (so.syn_state = 2 or so.syn_state = 0) ");
		var1.append("       	AND so.approved = 2 ");
		var1.append("           AND Date(so.create_date) = Date('now', 'localtime') )) ");
		var1.append("       AND so.customer_id = ct.customer_id ");
		Cursor c = null;
		try {
			c = rawQueries(var1.toString(), listParams);
			if (c != null) {
				dto = new NoSuccessSaleOrderDto();
				if (c.moveToFirst()) {
					do {
						NoSuccessSaleOrderItem item = dto
								.newNoSuccessSaleOrderItem();
						item.initNoSuccessSaleOrderItem(c);
						dto.itemList.add(item);
					} while (c.moveToNext());
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getAllOrderFail", e.getMessage());
			}
		}
		return dto;
	}

	/**
	 * Lay so don hang tra ve tu NPP trong ngay
	 *
	 * @author: TruongHN
	 * @return: int
	 * @throws:
	 */
	public int getOrderReturnNPPInDay(int staffId) {
		int count = 0;
		String dateNow = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DATE);
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT Count(*) AS count ");
		var1.append("FROM   sale_order ");
		var1.append("WHERE  staff_id = ? ");
		var1.append("       AND syn_state = 2 ");
		var1.append("       AND approved = 2 ");
//		var1.append("       AND Date(create_date) = ? ");
		var1.append("       AND substr(create_date, 1, 10) = ? ");

		String[] params = { String.valueOf(staffId), dateNow };
		Cursor cursor = null;
		try {
			cursor = rawQuery(var1.toString(), params);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					count = CursorUtil.getInt(cursor, "count");
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getOrderReturnNPPInDay", e.getMessage());
			}
		}
		return count;
	}

	/**
	 * delete cac don hang truoc 1 thang
	 *
	 * @author banghn
	 * @return
	 */
	public long deleteOldSaleOrder() {
		long success = -1;
		try {
			String startOfMonth = DateUtils
					.getFirstDateOfNumberPreviousMonthWithFormat(
							DateUtils.DATE_FORMAT_DATE, -2);
			String[] params = {startOfMonth};

			mDB.beginTransaction();
			//xoa don hang giu lai trong 3 thang hien tai
			StringBuffer sqlDel = new StringBuffer();
			sqlDel.append("  ?  > dayInOrder(ORDER_DATE) ");
			delete(sqlDel.toString(), params);
			success = 1;
			mDB.setTransactionSuccessful();
		} catch (Exception e) {
			MyLog.e("deleteOldSaleOrder", "fail", e);
			success = -1;
		} finally {
			mDB.endTransaction();
		}
		return success;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	*/
	public long updateReturnedOrder(SaleOrderDTO dto) {
		ContentValues value = new ContentValues();

		dto.updateDate = DateUtils.now();
		dto.updateUser = GlobalInfo.getInstance().getProfile().getUserData().getUserCode();
		value.put(UPDATE_USER, dto.updateUser );
		value.put(UPDATE_DATE, dto.updateDate);
		value.put(TYPE, dto.type);

		String[] params = { "" + dto.saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * Cap nhat trang thai approve cua 1 don hang
	 * @author: Nguyen Thanh Dung
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	*/
	public int updateStateApprovedOfOrder(SaleOrderDTO dto) {
		ContentValues value = new ContentValues();

		value.put(APPROVED, dto.approved);

		String[] params = { "" + dto.saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * lay loai don hang
	 * @author: DungNX
	 * @param parentStaffId
	 * @param customerId
	 * @return
	 * @return: String
	 * @throws Exception
	 * @throws:
	*/
	public String getOrderDaftType(String saleOrderId) throws Exception {
		String orderType = "";
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT ORDER_TYPE ");
		var1.append("FROM   sale_order ");
		var1.append("WHERE  ");
		var1.append("       SALE_ORDER_ID = ? ");
		var1.append("       AND syn_state = 0 ");
		var1.append("       AND approved = -1 ");
		var1.append("       AND Date(create_date) = Date('now', 'localtime') ");

		String[] params = { saleOrderId };
		Cursor cursor = null;
		try {
			cursor = rawQuery(var1.toString(), params);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					orderType = CursorUtil.getString(cursor, "ORDER_TYPE");
				}
			}
		} catch (Exception ex) {
			throw ex;
		} finally {
			try {
				if (cursor != null) {
					cursor.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				MyLog.i("getOrderDaftInday", e.getMessage());
			}
		}
		return orderType;
	}

	public long updateOrderCancel(long saleOrderId) {
		ContentValues value = initDataRowCancelOrder();
		String[] params = { "" + saleOrderId };
		return update(value, SALE_ORDER_ID + " = ? ", params);
	}

	/**
	 * init data row cancel order
	 * @author: duongdt3
	 * @time: 2:29:08 PM Nov 3, 2015
	 * @param saleOrderId
	 * @return
	*/
	private ContentValues initDataRowCancelOrder() {
		ContentValues editedValues = new ContentValues();
		editedValues.put(UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode());
		editedValues.put(UPDATE_DATE, DateUtils.now());
		editedValues.put(APPROVED, 3);
		return editedValues;
	}
}