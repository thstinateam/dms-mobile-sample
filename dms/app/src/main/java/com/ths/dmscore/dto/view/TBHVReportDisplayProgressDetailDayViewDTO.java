/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.util.ArrayList;

/**
 *  ViewDTO cua man hinh bao cao tien do CTTB cua TBHV theo NVBH
 *  @author: BangHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class TBHVReportDisplayProgressDetailDayViewDTO {
	public int remainSaleTotal;
	public int totalSize;
	
	public ArrayList<ReportDisplayProgressDetailDayRowDTO> listItem;

	public  TBHVReportDisplayProgressDetailDayViewDTO(){
		listItem = new ArrayList<ReportDisplayProgressDetailDayRowDTO>();
		remainSaleTotal=0;
		totalSize = 0;
	}
	public void addItem(ReportDisplayProgressDetailDayRowDTO c) {
		listItem.add(c);
	}
}
