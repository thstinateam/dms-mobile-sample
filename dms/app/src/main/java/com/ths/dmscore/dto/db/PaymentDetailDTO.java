/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.CustomerDebitDetailItem;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PAYMENT_DETAIL_TABLE;
import com.ths.dmscore.lib.sqllite.db.PAYMENT_DETAIL_TEMP_TABLE;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;

/**
 * Thong tin chi tiet cong no
 * 
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class PaymentDetailDTO extends AbstractTableDTO {
	private static final long serialVersionUID = -5933074341042310705L;
	// id
	public long paymentDetailID;
	public long debitDetailID;
	public long payReceivedID;
	public double amount;
	public double discount;
	public String paymentType;
	public int status;
	public int staffId;
	public String payDate;
	public String createUser;
	public String createDate;
	public String updateUser;
	public String updateDate;
	public long saleOrderId;
	public int paymentStatus;
	public long shopId;
	public long customerId;

	public PaymentDetailDTO() {
		super(TableType.PAYMENT_DETAIL_TABLE);
	}

	/**
	 * Mo ta muc dich cua ham
	 * 
	 * @author: TamPQ
	 * @param paymentDetailDto
	 * @return
	 * @return: JSONObjectvoid
	 * @throws:
	 */
	public JSONObject generateInsertForPayDebt() {
		JSONObject payreceived = new JSONObject();
		try {
			payreceived.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
//			payreceived.put(IntentConstants.INTENT_TABLE_NAME, PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL);
			payreceived.put(IntentConstants.INTENT_TABLE_NAME, PAYMENT_DETAIL_TEMP_TABLE.TABLE_PAYMENT_DETAIL_TEMP);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.PAYMENT_DETAIL_ID, paymentDetailID, null));
//			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TABLE.DEBIT_DETAIL_ID, debitDetailID, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.PAY_RECEIVED_ID, payReceivedID, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.AMOUNT, amount, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.PAYMENT_TYPE, paymentType, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.PAY_DATE, payDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.CREATE_USER, createUser, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.CREATE_DATE, createDate, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.DISCOUNT, discount, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.FOR_OBJECT_ID, saleOrderId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.PAYMENT_STATUS, paymentStatus, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.SHOP_ID, shopId, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TEMP_TABLE.CUSTOMER_ID, customerId, null));
			payreceived.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

		} catch (JSONException e) {
		}
		return payreceived;
	}

	/**update status cua payment_detail_temp ve -1
	 * @param item
	 * @return
	 */
	public JSONObject upDateStatusDelete(CustomerDebitDetailItem item) {
		JSONObject payreceived = new JSONObject();
		try {
			payreceived.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			payreceived.put(IntentConstants.INTENT_TABLE_NAME, PAYMENT_DETAIL_TABLE.TABLE_PAYMENT_DETAIL_TEMP);

			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TABLE.STATUS, -1, null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TABLE.UPDATE_USER, GlobalInfo.getInstance().getProfile().getUserData().getUserCode(), null));
			detailPara.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TABLE.UPDATE_DATE, DateUtils.now(), null));
			payreceived.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
			//where
			JSONArray whereParam = new JSONArray();
			whereParam.put(GlobalUtil.getJsonColumn(PAYMENT_DETAIL_TABLE.PAYMENT_DETAIL_ID, paymentDetailID, null));
			payreceived.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParam);
		} catch (JSONException e) {
		}
		return payreceived;
	}

}
