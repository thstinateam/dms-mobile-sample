package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.LogDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.CUSTOMER_TABLE;
import com.ths.dms.R;

public class NoSuccessSaleOrderDto implements Serializable {
	private static final long serialVersionUID = 5872838535942226200L;
	public ArrayList<NoSuccessSaleOrderItem> itemList;

	public NoSuccessSaleOrderDto() {
		itemList = new ArrayList<NoSuccessSaleOrderItem>();
	}

	public NoSuccessSaleOrderItem newNoSuccessSaleOrderItem() {
		return new NoSuccessSaleOrderItem();
	}

	public class NoSuccessSaleOrderItem implements Serializable{
		private static final long serialVersionUID = -7627125550050355150L;
		public SaleOrderDTO saleOrder;
		public CustomerDTO customer;

		public NoSuccessSaleOrderItem() {
			saleOrder = new SaleOrderDTO();
			customer = new CustomerDTO();
		}

		public void initNoSuccessSaleOrderItem(Cursor c) {
			try {
				if (c == null) {
					throw new Exception("Cursor is empty");
				}
			} catch (Exception ex) {
			}

			this.customer.customerCode = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_CODE);
			this.customer.customerName = CursorUtil.getString(c, CUSTOMER_TABLE.CUSTOMER_NAME);
			this.saleOrder.orderNumber = CursorUtil.getString(c, SALE_ORDER_TABLE.ORDER_NUMBER);
			this.saleOrder.orderDate = CursorUtil.getString(c, SALE_ORDER_TABLE.ORDER_DATE);
			this.saleOrder.setTotal(CursorUtil.getDouble(c, SALE_ORDER_TABLE.TOTAL));
			this.saleOrder.saleOrderId = CursorUtil.getLong(c, SALE_ORDER_TABLE.SALE_ORDER_ID);
			this.saleOrder.synState = CursorUtil.getInt(c, SALE_ORDER_TABLE.SYN_STATE);
			this.saleOrder.description = CursorUtil.getString(c, SALE_ORDER_TABLE.DESCRIPTION);
			this.saleOrder.approved = CursorUtil.getInt(c, SALE_ORDER_TABLE.APPROVED);

			String synState = String.valueOf(this.saleOrder.synState);
			if (this.saleOrder.approved == 2){
				// lay luon thong tin description hien thi len view
			}else if (this.saleOrder.approved == 0 ||
					this.saleOrder.approved == 1){//Vansale
				if(synState.equals(LogDTO.STATE_NEW)) {// Chua gui
					this.saleOrder.description = StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND);
				} else if(synState.equals(LogDTO.STATE_FAIL)) {// Dang gui
					this.saleOrder.description = StringUtil.getString(R.string.TEXT_ORDER_NOT_SEND);
				} else if(synState.equals(LogDTO.STATE_SUCCESS)) {// Da gui
					this.saleOrder.description = StringUtil.getString(R.string.TEXT_WAITING_PROCESS);
				} else if(synState.equals(LogDTO.STATE_INVALID_TIME)) {//Sai thoi gian
					this.saleOrder.description = StringUtil.getString(R.string.TEXT_ORDER_WRONG);
				} else if(synState.equals(LogDTO.STATE_UNIQUE_CONTRAINTS)) {//Duplicate don hang
					this.saleOrder.description = StringUtil.getString(R.string.TEXT_ORDER_WRONG);
				}
			}

		}
		
	}
}
