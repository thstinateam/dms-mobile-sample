package com.ths.dmscore.view.sale.statistic;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.dto.view.ReportStaffSaleDetailItemDTO;
import com.ths.dms.R;

public class ReportStaffSaleDetailRow extends DMSTableRow {
	TextView tvNum;
	public TextView tvCusCode;
	TextView tvCusName;
	TextView tvAddress;
	TextView tvTarget;
	TextView tvDone;
	TextView tvDuyet;
	//TextView tvScore;
	ReportStaffSaleDetailItemDTO item;
	private TextView tvGroupAmount;
	private TextView tvQuantityTarget;
	private TextView tvQuantityDone;
	private TextView tvQuantityDuyet;

	public ReportStaffSaleDetailRow(Context context, VinamilkTableListener lis) {
		super(context, R.layout.layout_report_staff_sale_detail_row,
				(GlobalInfo.getInstance().isSysShowPrice() ? 1.3 : 1),
				(GlobalInfo.getInstance().isSysShowPrice() ? null : new int[] {R.id.groupAmount}));

		tvNum = (TextView) findViewById(R.id.tvNum);
		tvCusCode = (TextView) findViewById(R.id.tvCusCode);
		tvCusName = (TextView) findViewById(R.id.tvCusName);
		tvAddress = (TextView) findViewById(R.id.tvAdd);
		tvTarget = (TextView) findViewById(R.id.tvTarget);
		tvDone = (TextView) findViewById(R.id.tvDone);
		tvDuyet = (TextView) findViewById(R.id.tvDuyet);
		//tvScore = (TextView) findViewById(R.id.tvScore);
		tvGroupAmount = (TextView) findViewById(R.id.tvGroupAmount);
		display(tvGroupAmount, StringUtil.getReportUnitTitle(StringUtil.getString(R.string.TEXT_SALES_)));
		tvQuantityTarget = (TextView) findViewById(R.id.tvQuantityTarget);
		tvQuantityDone = (TextView) findViewById(R.id.tvQuantityDone);
		tvQuantityDuyet = (TextView) findViewById(R.id.tvQuantityDuyet);
		listener = lis;
	}

	/**
	 * render layout 1 dong chi tiet
	 * @author: DungNT19
	 * @return: void
	 * @throws:
	 */
	public void render(int pos, ReportStaffSaleDetailItemDTO item) {
		this.item = item;
		display(tvNum, pos);
		display(tvCusCode, item.customerCode);
		display(tvCusName, item.customerName);
		display(tvAddress, item.customerAddress);
		//amount
		display(tvTarget, item.saleDetailDTO.amountPlan);
		display(tvDone, item.saleDetailDTO.amount);
		display(tvDuyet, item.saleDetailDTO.amountApproved);
		//quantity
		display(tvQuantityTarget, item.saleDetailDTO.quantityPlan);
		display(tvQuantityDone, item.saleDetailDTO.quantity);
		display(tvQuantityDuyet, item.saleDetailDTO.quantityApproved);
		//display(tvScore, item.saleDetailDTO.score);

		if (item.saleDetailDTO.isOr == 1) {
			setBackgroundColorResource(R.color.WHITE,
					tvNum, tvCusCode, tvCusName,
					tvTarget, tvAddress, tvDone,
					tvDuyet/*, tvScore*/, tvQuantityTarget, tvQuantityDone, tvQuantityDuyet);
		}

		if(item.isVisited && item.saleDetailDTO.amount == 0) {
			setTextColorResource(R.color.RED,
					tvNum, tvCusCode, tvCusName,
					tvTarget, tvAddress, tvDone,
					tvDuyet/*, tvScore*/, tvQuantityTarget, tvQuantityDone, tvQuantityDuyet);
		}
	}

	/**
	 * render layout dong tong
	 * @author: DungNT19
	 * @return: void
	 * @throws:
	 */
	public void renderRowSum(ReportStaffSaleDetailItemDTO item) {
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvNum, tvCusCode, tvCusName, tvAddress);
		this.item = item;
		//amount
		display(tvTarget, item.saleDetailDTO.amountPlan);
		display(tvDone, item.saleDetailDTO.amount);
		display(tvDuyet, item.saleDetailDTO.amountApproved);
		//quantity
		display(tvQuantityTarget, item.saleDetailDTO.quantityPlan);
		display(tvQuantityDone, item.saleDetailDTO.quantity);
		display(tvQuantityDuyet, item.saleDetailDTO.quantityApproved);
		//display(tvScore, "TB:" + StringUtil.formatNumber(item.saleDetailDTO.score));
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
	}
}
