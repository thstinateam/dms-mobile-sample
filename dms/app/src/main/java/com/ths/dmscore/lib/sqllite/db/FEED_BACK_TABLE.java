/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.TextUtils;

import com.ths.dmscore.dto.view.SupervisorProblemOfGSNPPDTO;
import com.ths.dmscore.view.control.table.DMSSortQueryBuilder;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.constants.SortActionConstants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedBackDetailDTO;
import com.ths.dmscore.dto.db.FeedBackTBHVDTO;
import com.ths.dmscore.dto.view.AttachDTO;
import com.ths.dmscore.dto.view.CustomerListFeedbackDTO;
import com.ths.dmscore.dto.view.CustomerListFeedbackItem;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.FollowProblemDTO;
import com.ths.dmscore.dto.view.FollowProblemItemDTO;
import com.ths.dmscore.dto.view.ListNoteInfoViewDTO;
import com.ths.dmscore.dto.view.NoteInfoDTO;
import com.ths.dmscore.dto.view.ReviewsObjectDTO;
import com.ths.dmscore.dto.view.ReviewsStaffViewDTO;
import com.ths.dmscore.dto.view.SuperviorTrackAndFixProblemOfGSNPPViewDTO;
import com.ths.dmscore.dto.view.TBHVFollowProblemItemDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.view.control.table.DMSSortInfo;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;

/**
 * Luu thong tin phan anh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class FEED_BACK_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String FEEDBACK_ID = "FEEDBACK_ID";
	// 0: OFF; 1: ON
	public static final String STATUS = "STATUS";
	// noi dung
	public static final String CONTENT = "CONTENT";
	// loai phan anh
	public static final String TYPE = "TYPE";
	// nguoi nhac nho
	public static final String REMIND_DATE = "REMIND_DATE";
	// parent staff id
	public static final String CREATE_USER_ID = "CREATE_USER_ID";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// nguoi update
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// 0: chua xoa; 1: da xoa
	public static final String IS_DELETED = "IS_DELETED";

	public static final String FEED_BACK_TABLE = "FEEDBACK";

	public FEED_BACK_TABLE(SQLiteDatabase mDB) {
		this.tableName = FEED_BACK_TABLE;
		this.columns = new String[] { FEEDBACK_ID,
				STATUS, REMIND_DATE, UPDATE_USER, CREATE_DATE,
				UPDATE_DATE, TYPE, CREATE_USER_ID, IS_DELETED, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((FeedBackDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(FeedBackDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		FeedBackDTO dtoFeedBack = (FeedBackDTO) dto;
		ContentValues value = initDataRow(dtoFeedBack);
		String[] params = { "" + dtoFeedBack.getFeedBackId() };
		return update(value, FEEDBACK_ID + " = ?", params);
	}

	/**
	 *
	 * cap nhat noi dung cho feedback
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateContentFeedBack(FeedBackDTO dto) {
		ContentValues value = initDateRowUpdateFeedBackContent(dto);
		String[] params = { String.valueOf(dto.getFeedBackId()) };
		return update(value, FEEDBACK_ID + " = ?", params);
	}

	/**
	 *
	 * update status feedback dto in DB at table FEEDBACK
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateFeedBackStatus(FeedBackDTO dto) {
		try {
			ContentValues value = initDateRowUpdateFeedBackStatus(dto);
			String[] params = { String.valueOf(dto.getFeedBackId()) };
			return update(value, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 *
	 * update feedback status from feedbackId, status, doneDate
	 *
	 * @param status
	 * @param doneDate
	 * @param feedbackId
	 * @return
	 * @return: long
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public long updateFeedBackStatus(String status, String doneDate,
			String feedbackId) {
		try {
			ContentValues value = initdateUpdateFeedBackStatus(status, doneDate);
			String[] params = { feedbackId };
			return update(value, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param apParamId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(FEEDBACK_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		FeedBackDTO dtoFeedBack = (FeedBackDTO) dto;
		String[] params = { "" + dtoFeedBack.getFeedBackId() };
		return delete(FEEDBACK_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: FeedBackDTO
	 * @throws:
	 */
	public FeedBackDTO getRowById(String id) {
		FeedBackDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(FEEDBACK_ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	private FeedBackDTO initDTOFromCursor(Cursor c) {
		FeedBackDTO dto = new FeedBackDTO();
		dto.setFeedBackId((CursorUtil.getLong(c, FEEDBACK_ID)));
//		dto.staffId = (CursorUtil.getInt(c, STAFF_ID));
//		dto.customerId = (CursorUtil.getString(c, CUSTOMER_ID));
		dto.setStatus((CursorUtil.getInt(c, STATUS)));
		dto.setContent((CursorUtil.getString(c, "DESCR")));
		dto.setUserUpdate((CursorUtil.getString(c, UPDATE_USER)));
		dto.setRemindDate((CursorUtil.getString(c, REMIND_DATE)));
//		dto.doneDate = (CursorUtil.getString(c, DONE_DATE));
		dto.setCreateDate((CursorUtil.getString(c, CREATE_DATE)));
		dto.setUpdateDate((CursorUtil.getString(c, UPDATE_DATE)));
		dto.setFeedbackType((CursorUtil.getInt(c, TYPE)));
		dto.setCreateUserId((CursorUtil.getLong(c, CREATE_USER_ID)));
		return dto;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: Vector<FeedBackDTO>
	 * @throws:
	 */
	public Vector<FeedBackDTO> getAllRow() {
		Vector<FeedBackDTO> v = new Vector<FeedBackDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				FeedBackDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initDTOFromCursor(c);
						v.addElement(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(FEEDBACK_ID, dto.getFeedBackId());
//		editedValues.put(STAFF_ID, dto.staffId);
//		editedValues.put(CUSTOMER_ID, dto.customerId);
//		editedValues.put(SHOP_ID, dto.shopId);
//		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.trainingPlanDetailId);
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(CONTENT, dto.getContent());
		editedValues.put(TYPE, dto.getFeedbackType());
		editedValues.put(REMIND_DATE, dto.getRemindDate());
//		editedValues.put(DONE_DATE, dto.doneDate);
//		editedValues.put(NUM_RETURN, dto.numReturn);
		editedValues.put(CREATE_USER_ID, dto.getCreateUserId());
		editedValues.put(CREATE_DATE, dto.getCreateDate());
		editedValues.put(UPDATE_USER, dto.getUserUpdate());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());

		// editedValues.put(DESCR, dto.descr);
		// editedValues.put(IS_SEND, dto.isSend);
		// editedValues.put(IS_DELETED, dto.isDeleted);
		return editedValues;
	}

	/**
	 *
	 * update feedback dayInOrder row at column "STATUS"
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDateRowUpdateFeedBackStatus(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, dto.getStatus());
//		editedValues.put(DONE_DATE, dto.doneDate);
		return editedValues;
	}

	/**
	 *
	 * update feedback status from status and doneDate
	 *
	 * @param status
	 * @param doneDate
	 * @return
	 * @return: ContentValues
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	private ContentValues initdateUpdateFeedBackStatus(String status,
			String doneDate) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, status);
//		editedValues.put(DONE_DATE, doneDate);
		return editedValues;
	}

	/**
	 *
	 * tao du lieu update content cho feedaback
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDateRowUpdateFeedBackContent(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(CONTENT, dto.getContent());
		editedValues.put(UPDATE_USER, dto.getUserUpdate());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());
		editedValues.put(REMIND_DATE, dto.getRemindDate());
		return editedValues;
	}

	/**
	 * Lay danh sach feedback can thuc hien
	 * Role: QL
	 * @author: yennth16
	 * @since: 10:47:59 21-05-2015
	 * @return: void
	 * @throws:
	 * @param page
	 * @param isGetTotalPage
	 * @param sortInfo
	 */
	public CustomerFeedBackDto getFeedBackList(String staffId,
			String customerId, String type, String status, String doneDate,
			int page, int isGetTotalPage, DMSSortInfo sortInfo, BaseFragment sender) {
		CustomerFeedBackDto dto = null;
		Cursor c = null;
		Cursor c_totalRow = null;
		dto = new CustomerFeedBackDto();
		String date_now = DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
		String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();
		StringBuffer  varname1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalPageParam = new ArrayList<String>();
		varname1.append("SELECT ");
		varname1.append("    FB.FEEDBACK_ID                       as FEEDBACK_ID, ");
		varname1.append("    FBS.FEEDBACK_STAFF_ID                as FEEDBACK_STAFF_ID, ");
		varname1.append("    FBS.STAFF_ID                         as STAFF_ID, ");
//		varname1.append("    FBSC.CUSTOMER_ID                     as CUSTOMER_ID, ");
//		varname1.append("    CT.CUSTOMER_CODE                     as CUSTOMER_CODE, ");
//		varname1.append("    CT.CUSTOMER_NAME                     as CUSTOMER_NAME, ");
//		varname1.append("    CT.STREET                            as STREET, ");
//		varname1.append("    CT.HOUSENUMBER                       as HOUSE_NUMBER, ");
//		varname1.append("    CT.ADDRESS                           as ADDRESS, ");
		varname1.append("    FB.STATUS                            as STATUS, ");
		varname1.append("    FB.CONTENT                           as CONTENT, ");
		varname1.append("    FB.TYPE                              as TYPE, ");
		varname1.append("    FB.create_user_id                    as CREATE_USER_ID, ");
		varname1.append("    AP.AP_PARAM_NAME                     as AP_PARAM_NAME, ");
		varname1.append("    Strftime('%d/%m/%Y',FBS.CREATE_DATE) as CREATE_DATE, ");
		varname1.append("    Strftime('%d/%m/%Y %H:%M',FB.REMIND_DATE)  as REMIND_DATE, ");
		varname1.append("    Strftime('%d/%m/%Y',FBS.DONE_DATE)   as DONE_DATE, ");
		varname1.append("    Strftime('%d/%m/%Y',FBS.UPDATE_DATE) as UPDATE_DATE, ");
		varname1.append("    FBS.UPDATE_USER 					  as USER_UPDATE, ");
		varname1.append("    FBS.RESULT                           as RESULT ");
		varname1.append("FROM ");
		varname1.append("    AP_PARAM AP, ");
		varname1.append("    FEEDBACK FB, ");
		varname1.append("    FEEDBACK_STAFF FBS ");
//		varname1.append("    FEEDBACK_STAFF_CUSTOMER FBSC ");
//		varname1.append("LEFT JOIN ");
//		varname1.append("    CUSTOMER CT ");
//		varname1.append("        ON FBSC.CUSTOMER_ID = CT.CUSTOMER_ID ");
		varname1.append("WHERE ");
		varname1.append("    1 = 1 ");
		varname1.append("    AND FB.[FEEDBACK_ID] = FBS.[FEEDBACK_ID] ");
//		varname1.append("    AND FBS.[FEEDBACK_STAFF_ID] = FBSC.FEEDBACK_STAFF_ID ");
		varname1.append("    AND FBS.STAFF_ID = ? ");
		param.add(staffId);
		totalPageParam.add(staffId);
		varname1.append("    AND ( ");
		varname1.append("        AP.TYPE like 'FEEDBACK_TYPE' ");
		varname1.append("    ) ");
		varname1.append("    AND FB.TYPE = AP.AP_PARAM_CODE ");
		varname1.append("    AND FB.STATUS = 1  ");
//		varname1.append("    AND FB.IS_DELETE = 1  ");
		varname1.append("    AND ap.status = 1 ");
		varname1.append("    AND Date(FB.CREATE_DATE) <= Date(?) ");
		param.add(date_now);
		totalPageParam.add(date_now);
		varname1.append("    AND Date(FB.CREATE_DATE) >= DATE('NOW','localtime','START OF MONTH','-2 MONTH') ");
		if (!StringUtil.isNullOrEmpty(type)) {
			varname1.append(" AND AP.AP_PARAM_CODE = ?");
			param.add(type);
			totalPageParam.add(type);
		}
		if (!StringUtil.isNullOrEmpty(status)) {
			varname1.append(" AND FBS.RESULT = ? ");
			param.add(status);
			totalPageParam.add(status);
		}

		//default order by
		String defaultOrderByStr = "";
		defaultOrderByStr = "   order by FBS.RESULT asc,FB.REMIND_DATE ,FBS.DONE_DATE desc, FB.CREATE_DATE desc ";

		String orderByStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.CODE, "FB.CONTENT")
			.addMapper(SortActionConstants.TYPE, "AP.AP_PARAM_NAME")
			.addMapper(SortActionConstants.FROM_DATE, "FB.REMIND_DATE")
			.addMapper(SortActionConstants.TO_DATE, "FBS.DONE_DATE")
			.defaultOrderString(defaultOrderByStr)
			.build(sortInfo);

		//add order string
		varname1.append(orderByStr);

		// get count
		StringBuffer totalCount = new StringBuffer();
		if (isGetTotalPage == 1) {
			totalCount.append("select count(*) as TOTAL_ROW from (" + varname1
					+ ")");
		}

		if (page > 0) {
			varname1.append("       limit ? offset ?");
			param.add("" + Constants.NUM_ITEM_PER_PAGE);
			param.add("" + (page - 1) * Constants.NUM_ITEM_PER_PAGE);
		}

		try {
			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FeedBackDTO item = new FeedBackDTO();
						item.initDataFromCursor(c);
						item.setCustomerCode(getFeedBackCustomer(item.getFeedbackStaffDTO().feedBackStaffId));
						dto.arrItem.add(item);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == 1) {
				c_totalRow = rawQueries(totalCount.toString(), totalPageParam);
				if (c_totalRow != null) {
					if (c_totalRow.moveToFirst()) {
						dto.totalFeedBack = c_totalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
			if (c_totalRow != null) {
				c_totalRow.close();
			}
		}

		return dto;
	}
	/**
	 * Lay danh sach customer
	 * @author: yennth16
	 * @since: 19:16:11 21-05-2015
	 * @return: String
	 * @throws:
	 * @param feedBackStaffCustomer
	 * @return
	 */
	public String getFeedBackCustomer(long feedBackStaffCustomer) {
		Cursor c = null;
		Cursor c_totalRow = null;
		String result = "";
		String customerCode = "";
		StringBuffer  varname1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		varname1.append("SELECT CT.CUSTOMER_CODE ");
		varname1.append("FROM ");
		varname1.append("FEEDBACK_STAFF_CUSTOMER FBSC, ");
		varname1.append("CUSTOMER CT ");
		varname1.append("WHERE 1=1 ");
		varname1.append("AND FBSC.[FEEDBACK_STAFF_ID] = ? ");
		param.add(""+feedBackStaffCustomer);
		varname1.append("AND CT.CUSTOMER_ID = FBSC.[CUSTOMER_ID]");
		try {
			c = rawQueries(varname1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						customerCode = Constants.STR_BLANK;
						customerCode = CursorUtil.getString(c, "CUSTOMER_CODE");
						if(!StringUtil.isNullOrEmpty(result)){
							result = result + Constants.STR_TOKEN;
						}
						result = result + customerCode;

					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
			if (c_totalRow != null) {
				c_totalRow.close();
			}
		}
		return result;
	}

	public long postFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				TABLE_ID tableId = new TABLE_ID(mDB);
				// long offValue = GlobalInfo.getInstance().getProfile()
				// .getUserData().id
				// * GlobalInfo.getInstance().getFactorDefault();
				// long maxFeedbackId = tableId
				// .getMaxIdFromTableName(FEED_BACK_TABLE) + 1;
				// dto.feedBackId = maxFeedbackId + offValue;
				// if (returnCode != -1) {
				// tableId.updateMaxId(FEED_BACK_TABLE, maxFeedbackId);
				// }
				dto.setFeedBackId(tableId.getMaxIdTime(FEED_BACK_TABLE));
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	/**
	 *
	 * update note info
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param shopId
	 * @param noteUpdate
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO updateNoteInfo(String staffId, String shopId,
			NoteInfoDTO noteUpdate) {
		ListNoteInfoViewDTO listNote = new ListNoteInfoViewDTO();
		this.updateFeedBackStatus(noteUpdate.feedBack);
		listNote = this.getListNote(staffId, shopId, " limit 0,2");
		return listNote;
	}

	/**
	 *
	 * lay danh sach ghi chu cua NVBH doi voi cac user
	 *
	 * @author: HaiTC3
	 * @param staff_id
	 * @param shop_id
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO getListNote(String staff_id, String shop_id,
			String ext) {
		ListNoteInfoViewDTO DTO = new ListNoteInfoViewDTO();
		StringBuffer requestGetNoteList = new StringBuffer();
		requestGetNoteList.append("SELECT FB.feedback_id, ");
		requestGetNoteList.append("       FB.descr, ");
		requestGetNoteList.append("       FB.remind_date, ");
		requestGetNoteList.append("       FB.done_date, ");
		requestGetNoteList.append("       FB.status, ");
		requestGetNoteList
				.append("       Strftime('%d/%m/%Y', FB.create_date) AS CREATE_DATE, ");
		requestGetNoteList
				.append("       FB.type                              AS TYPE, ");
		requestGetNoteList
				.append("       FB.create_user_id                    AS CREATE_USER_ID, ");
		requestGetNoteList.append("       CT.customer_id, ");
		requestGetNoteList.append("       CT.customer_code, ");
		requestGetNoteList.append("       CT.customer_name ");
		requestGetNoteList.append("FROM   feedback AS FB, ");
		requestGetNoteList.append("       customer AS CT ");
		requestGetNoteList.append("WHERE  FB.customer_id = CT.customer_id ");
		requestGetNoteList.append("       AND FB.staff_id = ? ");
		requestGetNoteList.append("       AND CT.shop_id = ? ");
		requestGetNoteList.append("       AND FB.is_deleted = 0 ");
		requestGetNoteList.append("       AND CT.STATUS = 1 ");
		requestGetNoteList.append("       AND FB.status = 1 ");
		requestGetNoteList.append("ORDER  BY FB.create_date DESC ");

		Cursor c = null;
		Cursor cTmp = null;
		String getCountNoteList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ requestGetNoteList.toString() + ") ";
		String[] params = new String[] { staff_id, shop_id };

		try {
			cTmp = rawQuery(getCountNoteList, params);
			int total = 0;
			if (cTmp != null) {
				cTmp.moveToFirst();
				total = cTmp.getInt(0);
				DTO.setNoteNumber(total);
			}

			c = rawQuery(requestGetNoteList.toString() + ext, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						NoteInfoDTO note = new NoteInfoDTO();
						note.initDateWithCursor(c);
						DTO.getListNote().add(note);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (cTmp != null) {
				cTmp.close();
			}
			if (c != null) {
				c.close();
			}
		}
		return DTO;
	}

	public long updateDoneDateFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_STAFF_TABLE.equals(dto.getType())) {
			try {
				ContentValues editedValues = new ContentValues();
				editedValues.put(FEEDBACK_STAFF_TABLE.RESULT, dto.getFeedbackStaffDTO().result);
				editedValues.put(FEEDBACK_STAFF_TABLE.UPDATE_DATE, dto.getFeedbackStaffDTO().updateDate);
				editedValues.put(FEEDBACK_STAFF_TABLE.UPDATE_USER, dto.getFeedbackStaffDTO().updateUser);
				String[] params = { "" + dto.getFeedbackStaffDTO().feedBackStaffId };
				returnCode = update(editedValues, FEEDBACK_STAFF_TABLE.FEEDBACK_STAFF_ID + " = ?", params);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	public long updateDeleteFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				ContentValues editedValues = new ContentValues();
				editedValues.put(STATUS, dto.getStatus());
				if (dto.getUpdateDate() != null) {
					editedValues.put(UPDATE_DATE, dto.getUpdateDate());
				}
				if (dto.getUserUpdate() != null) {
					editedValues.put(UPDATE_USER, dto.getUserUpdate());
				}
				String[] params = { "" + dto.getFeedBackId() };
				returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Thuc hien cap nhat van de cua nvbh (thuc hien boi GSNPP)
	 *
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateGSNPPFollowProblemDone(FollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(STATUS, dto.status);
			// neu la yeu cau lam lai thi update lai so lan yeu cau
//			if (dto.status == FeedBackDTO.FEEDBACK_STATUS_CREATE) {
//				editedValues.put(NUM_RETURN, dto.numReturn);
//				editedValues.put(DONE_DATE, dto.doneDate);
//			}
			editedValues.put(UPDATE_USER, dto.updateUser);
			editedValues.put(UPDATE_DATE, dto.updateDate);
			String[] params = { "" + dto.feedBackId };
			returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 * HieuNH update van de cua TBHV
	 *
	 * @param dto
	 * @return
	 */
	public long updateTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(STATUS, dto.status);
			editedValues.put(UPDATE_USER, dto.updateUser);
			editedValues.put(UPDATE_DATE, dto.updateDate);
//			editedValues.put(DONE_DATE, dto.doneDate);
//			editedValues.put(NUM_RETURN, dto.numReturn);
			String[] params = { "" + dto.id };
			returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 * HieuNH delete van de cua TBHV
	 *
	 * @param dto
	 * @return
	 */
	public long deleteTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			String[] params = { "" + dto.id };
			returnCode = delete(FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 *
	 * danh sach theo doi khac phuc cua GSNPP
	 *
	 * @author: ThanhNN8
	 * @param data
	 * @return
	 * @return: FollowProblemDTO
	 * @throws Exception
	 * @throws:
	 */
	public FollowProblemDTO getListProblemOfSuperVisor(Bundle data) throws Exception {
		String extPage = data.getString(IntentConstants.INTENT_PAGE);
		// chua su dung id gsnpp
		String superStaffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID_PARA);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTB = new SHOP_TABLE(mDB);
		ArrayList<String> lstShopReverse = shopTB.getShopRecursiveReverse(shopId);
		String idShopListReverse = TextUtils.join(",", lstShopReverse);
		String customerCode = data
				.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String status = data.getString(IntentConstants.INTENT_STATE);
		String typeProblem = data
				.getString(IntentConstants.INTENT_TYPE_PROBLEM);
		String fromDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data
				.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		boolean checkPagging = data.getBoolean(
				IntentConstants.INTENT_CHECK_PAGGING, false);
		DMSSortInfo sortInfo = (DMSSortInfo) data.getSerializable(IntentConstants.INTENT_SORT_DATA);
		StringBuffer lstStaff = new StringBuffer();
		STAFF_TABLE staffTable = new STAFF_TABLE(mDB);
		for (String shopItem : lstShopReverse) {
			String idStaff = staffTable.getListStaffOfSupervisor(superStaffId, shopItem);
			if (!StringUtil.isNullOrEmpty(lstStaff.toString().trim()) && !StringUtil.isNullOrEmpty(idStaff)) {
				lstStaff.append(",");
			}
			lstStaff.append(idStaff);
		}
		if (!StringUtil.isNullOrEmpty(lstStaff.toString().trim())) {
			lstStaff.append(",");
		}
		lstStaff.append(staffId);
		FollowProblemDTO result = new FollowProblemDTO();
		List<String> params = new ArrayList<String>();
//		stringParams.add("" + superStaffId);

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT distinct s.staff_id                           STAFF_ID, ");
		sql.append("       s.staff_code                         STAFF_CODE, ");
		sql.append("       s.staff_name                         STAFF_NAME, ");
		sql.append("       group_concat(c.customer_id)     CUSTOMER_ID, ");
		sql.append("       group_concat(c.customer_code,', ')     CUSTOMER_CODE,  ");
		sql.append("       group_concat( c.customer_code || ' - ' ||	c.customer_name, ', ')     CUSTOMER_INFO, ");
		sql.append("       ap.ap_param_name                     AP_PARAM_NAME, ");
		sql.append("       fb.type                     			TYPE, ");
		sql.append("       fb.feedback_id                       FEEDBACK_ID, ");
		sql.append("       fbs.feedback_staff_id                       FEEDBACK_STAFF_ID, ");
		sql.append("       fb.content                           CONTENT, ");
		sql.append("       fbs.num_return                        NUM_RETURN, ");
		sql.append("       fbs.result                            RESULT, ");
		sql.append("       Strftime('%d/%m/%Y', fbs.create_date) CREATE_DATE, ");
		sql.append("       Strftime('%d/%m/%Y', fbs.done_date)   DONE_DATE, ");
		sql.append("       Strftime('%d/%m/%Y', fb.remind_date) REMIND_DATE ");
		sql.append("FROM   feedback fb,  ");
		sql.append("	   feedback_staff fbs  ");
		sql.append("       LEFT JOIN staff s ON (fbs.staff_id = s.staff_id AND s.shop_id IN (  ");
		sql.append(idShopListReverse);
		sql.append("       ))  ");
		sql.append("	   LEFT JOIN feedback_staff_customer fbsc  ON FBS.FEEDBACK_STAFF_ID = FBSC.FEEDBACK_STAFF_ID   ");
		sql.append("       LEFT JOIN customer c ON (fbsc.customer_id = c.customer_id), ");
		sql.append("       ap_param ap ");
		sql.append("WHERE  fb.type = ap.ap_param_code ");
		sql.append("   	   AND FB.FEEDBACK_ID = FBS.FEEDBACK_ID ");
		sql.append("       AND (FBS.FEEDBACK_STAFF_ID = FBSC.FEEDBACK_STAFF_ID or FBSC.FEEDBACK_STAFF_ID is null)  ");
		sql.append("       AND ap.type LIKE 'FEEDBACK_TYPE' ");
		sql.append("       AND s.staff_id in ( " + lstStaff + " ) ");
		sql.append("       AND s.status = 1 ");
		sql.append("       AND ap.status = 1 ");
		sql.append("       AND fb.status = 1  ");
		sql.append("       AND fb.create_user_id = ?  ");
		params.add(superStaffId);
//		sql.append("       AND fb.shop_id = ? ");
//		params.add(shopId);

		if (!StringUtil.isNullOrEmpty(status)) {
			sql.append("       AND fbs.result = ? ");
			params.add(status);
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			sql.append("       AND Date(fbs.create_date) >= Date(?) ");
			params.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			sql.append("       AND Date(fbs.create_date) <= Date(?) ");
			params.add(toDate);
		}
		if (!StringUtil.isNullOrEmpty(staffId)) {
			sql.append("       AND fbs.staff_id = ? ");
			params.add(staffId);
		}
		if (!StringUtil.isNullOrEmpty(customerCode)) {
			sql.append("       AND Lower(c.short_code) LIKE ? ");
			params.add("%" + customerCode.toLowerCase() + "%");
		}

		if (!StringUtil.isNullOrEmpty(typeProblem)) {
			sql.append("       AND fb.type = ? ");
			params.add(typeProblem);
		}
		sql.append("GROUP  BY fb.feedback_id, fbs.staff_id ");

		//default order by
		StringBuilder defaultOrderByStr = new StringBuilder();
		defaultOrderByStr.append("ORDER  BY RESULT, ");
		defaultOrderByStr.append("          DATE(fbs.done_date), ");
		defaultOrderByStr.append("          DATE(fb.remind_date), ");
		defaultOrderByStr.append("          DATE(fbs.create_date) DESC, ");
		defaultOrderByStr.append("          staff_name, ");
		defaultOrderByStr.append("          customer_code, ");
		defaultOrderByStr.append("          customer_name ");
		String orderByStr = new DMSSortQueryBuilder()
			.addMapper(SortActionConstants.STAFF, "s.staff_name")
			.addMapper(SortActionConstants.NAME, "customer_code")
			.addMapper(SortActionConstants.TYPE, "ap.ap_param_name")
			.addMapper(SortActionConstants.CODE, "fb.content")
			.addMapper(SortActionConstants.STATE, "fbs.result")
			.addMapper(SortActionConstants.DATE, "DATE(fbs.create_date)")
			.defaultOrderString(defaultOrderByStr.toString())
			.build(sortInfo);

		//add order string
		sql.append(orderByStr);

		String requestGetFollowProblemList = sql.toString();
		Cursor c = null;
//		String[] params = new String[stringParams.size()];
//		for (int i = 0, length = stringParams.size(); i < length; i++) {
//			params[i] = stringParams.get(i);
//		}

		Cursor cTmp = null;
		String getCountFollowProblemList = " SELECT COUNT(*) AS TOTAL_ROW FROM ("
				+ requestGetFollowProblemList + ") ";

		try {
			if (!checkPagging) {
				cTmp = rawQuery(getCountFollowProblemList, params.toArray(new String[params.size()]));
				if (cTmp != null) {
					cTmp.moveToFirst();
					result.total = cTmp.getInt(0);
				}
			}
			c = rawQuery(requestGetFollowProblemList + extPage, params.toArray(new String[params.size()]));
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FollowProblemItemDTO note = new FollowProblemItemDTO();
						note.initDateWithCursor(c);
						result.list.add(note);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}		
		return result;
	}

	// /**
	// *
	// * lay combobox nvbh cho man hinh theo doi khac phuc
	// *
	// * @author: ThanhNN8
	// * @param
	// * @return
	// * @return: List<ComboBoxDisplayProgrameItemDTO>
	// * @throws:
	// */
	// public List<ComboBoxDisplayProgrameItemDTO>
	// getComboboxProblemNVBHOfSuperVisor() {
	// List<ComboBoxDisplayProgrameItemDTO> result = new
	// ArrayList<ComboBoxDisplayProgrameItemDTO>();
	// StringBuilder stringbuilder = new StringBuilder();
	// stringbuilder.append("SELECT DISTINCT ST.STAFF_CODE FROM FEEDBACK AS FB, CUSTOMER AS CT, STAFF AS ST, AP_PARAM AS AP");
	// stringbuilder.append(" WHERE FB.CUSTOMER_ID = CT.CUSTOMER_ID");
	// stringbuilder.append(" AND FB.TYPE IN (5, 6, 7)");
	// stringbuilder.append(" AND FB.IS_DELETED = 0");
	// stringbuilder.append(" AND FB.STAFF_ID = ST.STAFF_ID");
	// stringbuilder.append(" AND DATE(FB.CREATE_DATE) <= DATE('now','localtime')");
	// stringbuilder.append(" AND AP.VALUE = FB.TYPE AND AP.CODE = ?");
	// stringbuilder.append(" AND FB.CREATE_USER_ID = ?");
	// stringbuilder.append(" ORDER BY ST.STAFF_CODE DESC");
	// String requestGetFollowProblemList = stringbuilder.toString();
	// Cursor c = null;
	// String[] params = new String[] { "FEEDBACK_TYPE_NVBH",
	// GlobalInfo.getInstance().getProfile().getUserData().id + "" };
	//
	// try {
	// c = rawQuery(requestGetFollowProblemList, params);
	// if (c != null) {
	// if (c.moveToFirst()) {
	// do {
	// ComboBoxDisplayProgrameItemDTO comboboxNVBHDTO = new
	// ComboBoxDisplayProgrameItemDTO();
	// if (c.getColumnIndex("STAFF_CODE") >= 0) {
	// comboboxNVBHDTO.value = c.getString(c
	// .getColumnIndex("STAFF_CODE"));
	// comboboxNVBHDTO.name = c.getString(c
	// .getColumnIndex("STAFF_CODE"));
	// } else {
	// comboboxNVBHDTO.value = "";
	// comboboxNVBHDTO.name = "";
	// }
	// result.add(comboboxNVBHDTO);
	// } while (c.moveToNext());
	// }
	// }
	// } catch (Exception e) {
	// return null;
	// } finally {
	// if (c != null) {
	// c.close();
	// }
	// }
	// return result;
	// }

	/**
	 *
	 * lay combobox trang thai cho man hinh theo doi khac phuc
	 *
	 * @author: ThanhNN8
	 * @param
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getComboboxProblemStatusOfSuperVisor() {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		return result;
	}

	/**
	 *
	 * get list track and fix problem of gsnpp
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SupervisorProblemOfGSNPPDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public SuperviorTrackAndFixProblemOfGSNPPViewDTO getListTrackAndFixProblemOfGSNPP(
			Bundle ext) {
		SuperviorTrackAndFixProblemOfGSNPPViewDTO result = new SuperviorTrackAndFixProblemOfGSNPPViewDTO();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			String typeProblem = ext
					.getString(IntentConstants.INTENT_TYPE_PROBLEM_GSNPP);
			String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);

			String staffShopId = null;

			StringBuffer sqlQueryStaffShop = new StringBuffer();
			sqlQueryStaffShop.append("SELECT SHOP_ID ");
			sqlQueryStaffShop.append("	from staff ");
			sqlQueryStaffShop.append("	where staff_id = ");
			sqlQueryStaffShop.append(staffId);

			Cursor cStaffShop = null;
			cStaffShop = rawQuery(sqlQueryStaffShop.toString(), null);
			if(cStaffShop != null){
				cStaffShop.moveToFirst();
				staffShopId = CursorUtil.getString(cStaffShop, "SHOP_ID");
			}

			//String createUserStaffId = ext
			//		.getString(IntentConstants.INTENT_CREATE_USER_STAFF_ID);
			boolean isGetTotalItem = ext
					.getBoolean(IntentConstants.INTENT_IS_ALL);
			String page = ext.getString(IntentConstants.INTENT_PAGE);

			String fromDate = ext
					.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
			String toDate = ext
					.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
			String shopId = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId();

			ArrayList<String> listparam = new ArrayList<String>();

			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT FB.feedback_id                       AS FEEDBACK_ID, ");
			sqlQuery.append("       FB.staff_id                          AS STAFF_ID, ");
			sqlQuery.append("       FB.customer_id                       AS CUSTOMER_ID, ");
			sqlQuery.append("       CT.customer_code                     AS CUSTOMER_CODE, ");
			sqlQuery.append("       CT.customer_name                     AS CUSTOMER_NAME, ");
			sqlQuery.append("       CT.street                            AS STREET, ");
			sqlQuery.append("       CT.housenumber                       AS HOUSE_NUMBER, ");
			sqlQuery.append("       FB.status                            AS STATUS, ");
			sqlQuery.append("       FB.content                           AS CONTENT, ");
			sqlQuery.append("       AP.ap_param_name                     AS TYPE, ");
			sqlQuery.append("       FB.status                            AS IS_DELETED, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.create_date) AS CREATE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.remind_date) AS REMIND_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.done_date)   AS DONE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.update_date) AS UPDATE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.update_user) AS USER_UPDATE ");
			sqlQuery.append("FROM   ap_param AP, ");
			sqlQuery.append("       feedback FB ");
			sqlQuery.append("       LEFT JOIN customer CT ");
			sqlQuery.append("              ON FB.customer_id = CT.customer_id ");
			sqlQuery.append("WHERE  FB.status <> 0 ");
			sqlQuery.append("       AND  FB.staff_id = ? ");
			listparam.add(staffId);
			sqlQuery.append("       AND  FB.shop_id in (? ");
			listparam.add(shopId);
			if(!StringUtil.isNullOrEmpty(staffShopId)){
				sqlQuery.append("       , ?) ");
				listparam.add(staffShopId);
			} else {
				sqlQuery.append("       ) ");
			}
			sqlQuery.append("       AND  AP.type LIKE 'FEEDBACK_TYPE_TBHV' ");
			sqlQuery.append("       and ap.status = 1 ");
			sqlQuery.append("       AND Date(FB.create_date) <= Date('NOW', 'localtime') ");
			sqlQuery.append("       AND FB.type = AP.ap_param_code ");

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				sqlQuery.append("       AND ifnull(dayInOrder(fb.remind_date) >= dayInOrder(?),1) ");
				listparam.add(fromDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				sqlQuery.append("       AND ifnull(dayInOrder(fb.remind_date) <= dayInOrder(?),1) ");
				listparam.add(toDate);
			}

			if (!StringUtil.isNullOrEmpty(typeProblem)) {
				sqlQuery.append("       AND fb.status = ? ");
				listparam.add(typeProblem);
			}

			sqlQuery.append("ORDER  BY FB.status ASC, ");
			sqlQuery.append("          FB.remind_date, ");
			sqlQuery.append("          FB.done_date DESC, ");
			sqlQuery.append("          FB.create_date DESC, ");
			sqlQuery.append("          CT.customer_code, ");
			sqlQuery.append("          CT.customer_name ");

			String getCountProblemList = " select count(*) as total_row from ("
					+ sqlQuery.toString() + ") ";

			String params[] = listparam.toArray(new String[listparam.size()]);

			//int total = 0;

			if (isGetTotalItem) {
				cTmp = rawQuery(getCountProblemList, params);
				if (cTmp != null) {
					cTmp.moveToFirst();
					result.totalItem = cTmp.getInt(0);
				}
			}

			c = this.rawQuery(sqlQuery.toString() + page, params);
			if (c.moveToFirst()) {
				do {
					SupervisorProblemOfGSNPPDTO item = new SupervisorProblemOfGSNPPDTO();
					item.initDataWithCursor(c);
					result.listProblemsOfGSNPP.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			result = null;
			// TODO: handle exception
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get list feedback reviews of tbhv
	 *
	 * @param data
	 * @return
	 * @return: List<FeedBackTBHVDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 8, 2012
	 */
	public List<FeedBackTBHVDTO> getListReviewsOfTBHV(Bundle data) {
		List<FeedBackTBHVDTO> listReviews = new ArrayList<FeedBackTBHVDTO>();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String createUserStaffId = data.getString(IntentConstants.INTENT_CREATE_USER_STAFF_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT FEEDBACK_ID, STAFF_ID, CUSTOMER_ID, STATUS, CONTENT, UPDATE_USER, CREATE_DATE, UPDATE_DATE, TYPE, Strftime('%d/%m/%Y',REMIND_DATE) REMIND_DATE, DONE_DATE, CREATE_USER_ID ");
		sqlQuery.append("FROM   feedback ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND staff_id = ? ");
		sqlQuery.append("       AND create_user_id = ? ");
		sqlQuery.append("       AND shop_id = ? ");
		sqlQuery.append("       AND Date(create_date) = Date('now', 'localtime') ");

		Cursor c = null;
		String[] paramsList = new String[] { staffId, createUserStaffId, shopId };

		try {
			c = this.rawQuery(sqlQuery.toString(), paramsList);
			if (c != null && c.moveToFirst()) {
				do {
					FeedBackTBHVDTO item = new FeedBackTBHVDTO();
					item.feedBackBasic.initDataWithCursor(c);
					item.currentState = FeedBackTBHVDTO.STATE_NO_UPDATE;
					listReviews.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listReviews;
	}

	/**
	 *
	 * get reviews of staff
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ReviewsStaffViewDTO
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ReviewsStaffViewDTO getReviewStaffView(Bundle data) {
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String createUserId = data
				.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String tpdId = data
				.getString(IntentConstants.INTENT_TRAINING_DETAIL_ID);
		ReviewsStaffViewDTO reviewsInfo = new ReviewsStaffViewDTO();
		reviewsInfo.feedBackSKU.feedBack = this.getFeedBackSKU(staffId,
				customerId, shopId, tpdId, createUserId);
		if (reviewsInfo.feedBackSKU.feedBack != null
				&& reviewsInfo.feedBackSKU.feedBack.getFeedBackId() > 0) {
			String feedBackSKUId = String
					.valueOf(reviewsInfo.feedBackSKU.feedBack.getFeedBackId());
			reviewsInfo.listSKU = this.getListSKUDone(feedBackSKUId);
		}
		reviewsInfo.listReviewsObject = this.getListReviewsOfStaff(shopId,
				createUserId, staffId, customerId, tpdId);

		return reviewsInfo;
	}

	/**
	 *
	 * get list revies of staff
	 *
	 * @author: HaiTC3
	 * @param shop_id
	 * @param supperStaff_id
	 * @param staff_id
	 * @param customer_id
	 * @return
	 * @return: ArrayList<ReviewsObjectDTO>
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ArrayList<ReviewsObjectDTO> getListReviewsOfStaff(String shopId,
			String createUserId, String staffId, String customerId, String tpdId) {
		ArrayList<ReviewsObjectDTO> listReviews = new ArrayList<ReviewsObjectDTO>();
		StringBuffer requestGetListReviews = new StringBuffer();
		requestGetListReviews.append("SELECT * ");
		requestGetListReviews.append("FROM   feedback ");
		requestGetListReviews.append("WHERE  staff_id = ? ");
		requestGetListReviews.append("       AND customer_id = ? ");
		requestGetListReviews.append("       AND shop_id = ? ");
		requestGetListReviews.append("       AND training_plan_detail_id = ? ");
		requestGetListReviews.append("       AND status = 1 ");
		requestGetListReviews.append("       AND type in (6, 7, 8) ");
		requestGetListReviews.append("       AND create_user_id = ? ");
		requestGetListReviews
				.append("       AND Date(create_date) = Date('now', 'localtime') ");
		String[] params = { staffId, customerId, shopId, tpdId, createUserId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(requestGetListReviews.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReviewsObjectDTO trainingResult = new ReviewsObjectDTO();
						trainingResult.parserDataFromCursor(c);
						listReviews.add(trainingResult);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listReviews;
	}

	/**
	 *
	 * get list SKU done
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: ArrayList<FeedBackDetailDTO>
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ArrayList<FeedBackDetailDTO> getListSKUDone(String feedbackId) {
		ArrayList<FeedBackDetailDTO> result = new ArrayList<FeedBackDetailDTO>();
		String var1 = "select fbd.*, p.product_code PRODUCT_CODE from feedback_detail fbd, product p where fbd.product_id = p.product_id and p.status = 1 and feedback_id = ?";
		String[] params = { feedbackId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(var1.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FeedBackDetailDTO feedBackSKU = new FeedBackDetailDTO();
						feedBackSKU.initWithCursor(c);
						result.add(feedBackSKU);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get feedback for list SKU
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param customerId
	 * @param shopId
	 * @param tpdId
	 * @param createUserId
	 * @return
	 * @return: FeedBackDTO
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public FeedBackDTO getFeedBackSKU(String staffId, String customerId,
			String shopId, String tpdId, String createUserId) {
		FeedBackDTO feedBackSKU = new FeedBackDTO();
		StringBuffer var1 = new StringBuffer();
		var1.append("SELECT * ");
		var1.append("FROM   feedback ");
		var1.append("WHERE  staff_id = ? ");
		var1.append("       AND customer_id = ? ");
		var1.append("       AND shop_id = ? ");
		var1.append("       AND training_plan_detail_id = ? ");
		var1.append("       AND status = 1 ");
		var1.append("       AND type = 9 ");
		var1.append("       AND create_user_id = ? ");
		var1
				.append("       AND Date(create_date) = Date('now', 'localtime') ");

		String[] params = { staffId, customerId, shopId, tpdId, createUserId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(var1.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					feedBackSKU.initDataWithCursor(c);
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return feedBackSKU;
	}


	 /**
	 * Lay ds khach hang cua ds van de
	 * @author: Tuanlt11
	 * @param data
	 * @return
	 * @return:
	 * @throws:
	*/
	public CustomerListFeedbackDTO getCustomerListFeedback(Bundle data) throws Exception {
		int page = data.getInt(IntentConstants.INTENT_PAGE);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		SHOP_TABLE shopTable = new SHOP_TABLE(mDB);
		ArrayList<String> shopList = shopTable.getShopRecursiveReverse(shopId);
		String shopStr = TextUtils.join(",", shopList);
		String customerCode = data
				.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String customerName = data
				.getString(IntentConstants.INTENT_CUSTOMER_NAME);
		boolean isGetNumRow = data
				.getBoolean(IntentConstants.INTENT_GET_TOTAL_PAGE);
		boolean isCheckLine = data.getBoolean(IntentConstants.INTENT_CHECK_COMBOBOX,false);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		ArrayList<String> param = new ArrayList<String>();
		StringBuffer var1 = new StringBuffer();
		StringBuffer totalPageSql = new StringBuffer();

		var1.append("SELECT * ");
		var1.append("FROM   shop SH, customer CT ");
		var1.append("WHERE  sh.shop_id IN ( ");
		var1.append(shopStr);
		var1.append(") ");
		if(!isCheckLine)
			var1.append("	and CT.shop_id = sh.shop_id ");
		else{
			String date_now = DateUtils
					.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_NOW);
			var1.append("	and EXISTS (SELECT	");
			var1.append("	    1");
			var1.append("	FROM	");
			var1.append("	    visit_plan vp,	");
			var1.append("	    routing RT,	");
			var1.append("	    routing_customer RTC	");
			var1.append("	WHERE	");
			var1.append("	    1 = 1	");
			var1.append("	    AND VP.ROUTING_ID = RT.ROUTING_ID	");
			var1.append("	    AND RTC.ROUTING_ID = RT.ROUTING_ID	");
			var1.append("	    AND RTC.CUSTOMER_ID = CT.CUSTOMER_ID	");
			var1.append("	    AND substr(vp.from_date,1,10) <= ?	");
			param.add(date_now);
			var1.append("	    AND (	");
			var1.append("	        substr(vp.to_date,1,10) >= ?	");
			param.add(date_now);
			var1.append("	        OR vp.to_date IS NULL	");
			var1.append("	    )	");
			var1.append("	    AND Vp.status = 1	");
			var1.append("	    AND VP.STAFF_ID = ?	");
			param.add(staffId);
			var1.append("	    AND RT.STATUS = 1	");
			var1.append("	    AND RTC.STATUS in (	");
			var1.append("	        1	");
			var1.append("	    )	");
			var1.append("	    AND VP.SHOP_ID IN (	");
			var1.append(shopStr);
			var1.append("	       )	");
			var1.append("	    AND VP.SHOP_ID = sh.shop_id	");
			var1.append("	    )	");
		}

		if (!StringUtil.isNullOrEmpty(customerCode)) {
			customerCode = StringUtil.escapeSqlString(customerCode);
			var1.append("	and upper(CT.SHORT_CODE) like upper(?) escape '^' ");
			param.add("%" + customerCode + "%");
		}
		if (!StringUtil.isNullOrEmpty(customerName)) {
			customerName = StringUtil
					.getEngStringFromUnicodeString(customerName);
			customerName = StringUtil.escapeSqlString(customerName);
			customerName = DatabaseUtils.sqlEscapeString("%" + customerName
					+ "%");
			var1.append("	and upper(CT.NAME_TEXT) like upper(");
			var1.append("?");
			param.add(customerName);
			var1.append(") escape '^' ");
		}
		var1.append("	ORDER  BY CT.short_code");
		if (page > 0) {
			// get count
			if (isGetNumRow) {
				totalPageSql.append("select count(*) as TOTAL_ROW from ("
						+ var1 + ")");
			}
			var1.append(" limit "
					+ Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			var1.append(" offset "
					+ Integer
							.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}
		Cursor c = null;
		Cursor cTotalRow = null;
		CustomerListFeedbackDTO dto = new CustomerListFeedbackDTO();
		try {
			// get total row first
			c = rawQueries(var1.toString(), param);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						CustomerListFeedbackItem item = new CustomerListFeedbackItem();
						item.customer.parseCustomerInfo(c);
						// gan staffId cho khach hang
						if(!StringUtil.isNullOrEmpty(staffId))
							item.customer.staffId = Long.parseLong(staffId);
						item.customer.shopDTO.parseDataFromCusor(c);
						dto.listCustomer.add(item);
					} while (c.moveToNext());
				}

			}
			if (page > 0 && isGetNumRow) {
				cTotalRow = rawQueries(totalPageSql.toString(), param);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalCustomer = cTotalRow.getInt(0);
					}
				}
			}

		}  finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	/**
	 * get list attach file of feedback
	 * @author: duongdt3
	 * @since: 09:13:46 27 Aug 2015
	 * @return: List<AttachDTO>
	 * @throws:  
	 * @param feedBackId
	 * @return
	 */
	public ArrayList<AttachDTO> getAttachInfoFeedback(long feedBackId) {
		ArrayList<AttachDTO> lstAttach = new ArrayList<AttachDTO>();
		StringBuffer sqlObject = new StringBuffer();
		ArrayList<String> paramsObject = new ArrayList<String>();
		sqlObject.append("	SELECT	");
		sqlObject.append("	    mi.MEDIA_ITEM_ID ATTACH_ID,	");
		sqlObject.append("	    mi.url	URL, mi.TITLE TITLE ");
		sqlObject.append("	FROM	");
		sqlObject.append("	    MEDIA_ITEM mi	");
		sqlObject.append("	WHERE	");
		sqlObject.append("	    1=1	");
		sqlObject.append("	    AND object_id = ?	");
		sqlObject.append("	    AND object_type = 5	");
		paramsObject.add(String.valueOf(feedBackId));
		Cursor c = null;
		try {
			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						AttachDTO item = new AttachDTO();
						item.initDataWithCursor(c);
						lstAttach.add(item);
					} while (c.moveToNext());
				}
			}
		}  finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
			}
		}
		return lstAttach;
	}
}
