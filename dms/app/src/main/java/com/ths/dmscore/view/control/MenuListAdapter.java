/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.control;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dms.R;

/**
 * MenuListAdapter.java
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  9:22:18 AM Mar 16, 2015
 */
public class MenuListAdapter extends BaseExpandableListAdapter {

	private Context context;
	private ArrayList<MenuItemDTO> groups;

	public MenuListAdapter(Context context, ArrayList<MenuItemDTO> groups) {
		this.context = context;
		this.groups = groups;
	}

	@Override
	public MenuItemDTO getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		ArrayList<MenuItemDTO> chList = groups.get(groupPosition).getItems();
		if(chList == null)
			return null;
		else
			return chList.get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {
		View row = view;
		MenuItemRow cell = null;
		if (row == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = vi.inflate(R.layout.layout_fragment_menu_item, null);
			cell = new MenuItemRow(context, row);
			row.setTag(cell);
		
		} else {
			cell = (MenuItemRow) row.getTag();
		}
		
		cell.populateFrom(getChild(groupPosition, childPosition));

		return row;
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		ArrayList<MenuItemDTO> chList = groups.get(groupPosition).getItems();
		if (groups.get(groupPosition).getItems() == null) {
			return 0;
		} else {
			return chList.size();
		}
	}

	@Override
	public MenuItemDTO getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return groups.get(groupPosition);
	}

	public int getGroupCount() {
		// TODO Auto-generated method stub
		return groups.size();
	}

	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isLastChild, View view,
			ViewGroup parent) {
		View row = view;
		MenuItemRow cell = null;
		if (row == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = vi.inflate(R.layout.layout_fragment_menu_parent_item, null);
			cell = new MenuItemRow(context, row);
			row.setTag(cell);
		} else {
			cell = (MenuItemRow) row.getTag();
		}
		cell.populateFrom(getGroup(groupPosition));
		return row;
	}

	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

}
