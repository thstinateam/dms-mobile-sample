package com.ths.dmscore.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.Spinner;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.MenuItemDTO;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.db.SQLUtils;
import com.ths.dmscore.view.control.MenuItem;
import com.ths.dmscore.view.control.VNMEditTextClearable;

/**
 * Mo ta muc dich cua class
 *
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class PriUtils implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	// trang thai control
	public static final int INVISIBLE = 1;
	public static final int ENABLE = 2;
	public static final int DISABLE = 3;

	// cac control dac biet
	public static final int CONTROL_VNMEDITTEXTCLEARABLE = 0;
	public static final int CONTROL_GRIDVIEW = 1;

	private boolean isFullPrivilege = false;

	// danh sach quyen cho cac control
	// duoc khoi tao khi khoi tao ung dung (hoac co the dung toi dau lay len toi
	// do)
	// int: controlcode, controlordinal
	private HashMap<String, PriDto> priHashMap;

	public static PriUtils getInstance(){
		if(GlobalInfo.getInstance().getPriUtilsInstance() == null){
			GlobalInfo.getInstance().setPriUtilsInstance(new PriUtils());
		}
		return GlobalInfo.getInstance().getPriUtilsInstance();
	}

	// NV, GS, TBHV
	private String role;
	private HashMap<String, PriDto> priHashMapMenu;

	private String form;
	private int formStatus;
	public int getFormStatus(){
		return formStatus;
	}

	/**
	 * moi khi vao 1 man hinh thi khoi tao lai hashMap
	 * @author: DungNX
	 * @param formName
	 * @return: void
	 * @throws:
	*/
	public void genPriHashMapForForm(PriHashMap.PriForm formPri) {
		try {
			form = formPri.getFormName();
			if (!isFullPrivilege) {
				// tao ds phan quyen
				// lay tu db len cho nay
				formStatus = SQLUtils.getInstance().getFormStatus(form);
				if (formStatus == 1) {
					priHashMap = SQLUtils.getInstance().getAppPrivilege(form);
				}
			} else {
//				 full quyen thi enable cho form
				formStatus = 1;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void initIsFullPrivilege(){
		try {
			isFullPrivilege = SQLUtils.getInstance().getIsFullPrivilege();
			// hard code full quyen chuc nang
			isFullPrivilege = true;
//			isFullPrivilege = false;
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	/**
	 * findViewById va setDefaultStatus cho view
	 * @author: DungNX
	 * @param v
	 * @param id
	 * @param priControl
	 * @return
	 * @return: View
	 * @throws:
	*/
	public View findViewById(View v, int id, PriHashMap.PriControl priControl) {
		View view = v.findViewById(id);
		if (view != null) {
			PriControlStatus cs = new PriControlStatus();
			cs.controlCode = priControl.getControlName();
			view.setTag(id, cs);
			setDefaultStatus(view);
		}
		return view;
	}

	/**
	 * cho cac control dac biet
	 * @author: DungNX
	 * @param v
	 * @param id
	 * @param priControl
	 * @param isVNMEditTextClearable
	 * @return
	 * @return: View
	 * @throws:
	*/
	public View findViewById(View v, int id, PriHashMap.PriControl priControl, int control) {
		View view = v.findViewById(id);
		if (view != null) {
			PriControlStatus cs = new PriControlStatus();
			cs.controlCode = priControl.getControlName();
			view.setTag(id, cs);
			if (control == CONTROL_VNMEDITTEXTCLEARABLE && setDefaultStatus(view) == DISABLE) {
				// la VNMEDITTEXTCLEARABLE, bi DISABLE thi khong cho hien dau X
				((VNMEditTextClearable)view).setImageClearVisibile(false);
			} else if(control == CONTROL_GRIDVIEW && setDefaultStatus(view) == DISABLE) {
				// neu la GRIDVIEW thi van enable, nhung khong cho chon item
				view.setEnabled(true);
			}
		}
		return view;
	}

	/**
	 * set them bien allowInvisible de nhan biet view co duoc invisible hay khong
	 * @author: DungNX
	 * @param v
	 * @param id
	 * @param controlCode
	 * @param allowInvisible
	 * @return
	 * @return: View
	 * @throws:
	*/
	public View findViewByIdNotAllowInvisible(View v, int id, PriHashMap.PriControl priControl) {
		View view = v.findViewById(id);
		if (view != null) {
			PriControlStatus cs = new PriControlStatus();
			cs.controlCode = priControl.getControlName();
			cs.allowInvisible = false;
			view.setTag(id, cs);
			setDefaultStatus(view);
		}
		return view;
	}

	/**
	 * view co bat buoc Gone
	 * @author: DungNX
	 * @param v
	 * @param id
	 * @param controlCode
	 * @return
	 * @return: View
	 * @throws:
	*/
	public View findViewByIdGone(View v, int id, PriHashMap.PriControl priControl) {
		View view = v.findViewById(id);
		if (view != null) {
			PriControlStatus cs = new PriControlStatus();
			cs.controlCode = priControl.getControlName();
			cs.mustGone = true;
			view.setTag(id, cs);
			setDefaultStatus(view);
		}
		return view;
	}

	/**
	 * @author: DungNX
	 * @param v
	 * @param id
	 * @param priControl
	 * @param control
	 * @return
	 * @return: View
	 * @throws:
	*/
	public View findViewByIdGone(View v, int id, PriHashMap.PriControl priControl, int control) {
		View view = v.findViewById(id);
		if (view != null) {
			PriControlStatus cs = new PriControlStatus();
			cs.controlCode = priControl.getControlName();
			cs.mustGone = true;
			view.setTag(id, cs);
			if (control == CONTROL_VNMEDITTEXTCLEARABLE && setDefaultStatus(view) == DISABLE) {
				// la VNMEDITTEXTCLEARABLE, bi DISABLE thi khong cho hien dau X
				((VNMEditTextClearable)view).setImageClearVisibile(false);
			} else if(control == CONTROL_GRIDVIEW && setDefaultStatus(view) == DISABLE) {
				// neu la GRIDVIEW thi van enable, nhung khong cho chon item
				view.setEnabled(true);
			}
		}
		return view;
	}

	/**
	 * setStatus cho view
	 * @author: DungNX
	 * @param v
	 * @param status
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int setStatus(View v, int status) {
		int statusPri = checkPri(v);

		if(status == INVISIBLE){
			// uu tien INVISIBLE cua nghiep vu
			setStatusByPri(v, status);
		} else if (statusPri == ENABLE) {
			// duoc phan quyen ENABLE thi tuy y nghiep vu
			setStatusByPri(v, status);
		} else {
			setStatusByPri(v, statusPri);
		}

		return statusPri;
	}

	/**
	 * setStatus cho cac control dac biet
	 * @author: DungNX
	 * @param v
	 * @param status
	 * @param control
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int setStatus(View v, int status, int control) {
		int statusPri = checkPri(v);

		if(control == CONTROL_GRIDVIEW){
			if(status == INVISIBLE){
				// uu tien INVISIBLE cua nghiep vu
				setStatusByPri(v, status);
			} else if (statusPri == ENABLE || statusPri == DISABLE) {
				// duoc phan quyen ENABLE thi tuy y nghiep vu
				setStatusByPri(v, status);
			} else {
				setStatusByPri(v, statusPri);
			}
		}

		return statusPri;
	}

	/**
	 * default status duoc dung khi findViewById
	 * @author: DungNX
	 * @param v
	 * @return
	 * @return: int
	 * @throws:
	*/
	private int setDefaultStatus(View v) {
		int statusPri = checkPri(v);
		setStatusByPri(v, statusPri);
		return statusPri;
	}

	/**
	 * setStatus cua view sau khi da tim duoc quyen trong priHashMap
	 * @author: DungNX
	 * @param v
	 * @param status
	 * @return: void
	 * @throws:
	*/
	private void setStatusByPri(View v, int status) {
		if (status == INVISIBLE) {
			// kiem tra view luc khoi tao (findView) co cho an hay khong
			PriControlStatus cs = (PriControlStatus) v.getTag(v.getId());
			if (cs == null || (cs!=null && cs.allowInvisible && !cs.mustGone)) {
				// cs == null: truong hop menu
				v.setVisibility(View.INVISIBLE);
				v.setEnabled(false);
			} else if(cs == null || (cs!=null && cs.mustGone)){
				v.setVisibility(View.GONE);
				v.setEnabled(false);
			} else {
				// khong cho INVISIBLE thi mac dinh se la DISABLE
				v.setVisibility(View.VISIBLE);
				v.setEnabled(false);
			}
		} else if (status == ENABLE) {
			v.setVisibility(View.VISIBLE);
			v.setEnabled(true);
		} else if (status == DISABLE) {
			v.setVisibility(View.VISIBLE);
			v.setEnabled(false);
		}
	}

	/**
	 * setOnClickListener sau khi tim duoc quyen trong priHashMap
	 * @author: DungNX
	 * @param v
	 * @param listener
	 * @return: void
	 * @throws:
	*/
	public void setOnClickListener(View v, OnClickListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnClickListener(listener);
		}
	}

	/**
	 * setOnTouchListener sau khi tim duoc quyen trong priHashMap
	 * @author: DungNX
	 * @param v
	 * @param listener
	 * @return: void
	 * @throws:
	*/
	public void setOnTouchListener(View v, OnTouchListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnTouchListener(listener);
		}
	}

	/**
	 * setOnCheckedChangeListener sau khi tim duoc quyen trong priHashMap
	 * @author: DungNX
	 * @param v
	 * @param listener
	 * @return: void
	 * @throws:
	*/
	public void setOnCheckedChangeListener(CheckBox v, OnCheckedChangeListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnCheckedChangeListener(listener);
		}
	}

	/**
	 * setOnItemSelectedListener sau khi tim duoc quyen trong priHashMap
	 * @author: DungNX
	 * @param v
	 * @param listener
	 * @return: void
	 * @throws:
	*/
	public void setOnItemSelectedListener(Spinner v, OnItemSelectedListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnItemSelectedListener(listener);
		}
	}

	public void setOnItemClickListener(GridView v, OnItemClickListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnItemClickListener(listener);
		}
	}

	public void setOnItemClickListener(VNMEditTextClearable v, OnItemClickListener listener) {
		int statusPri = checkPri(v);
		if (statusPri == ENABLE) {
			v.setOnItemClickListener(listener);
		}
	}

	/**
	 * tim quyen trong hashMap
	 * @author: DungNX
	 * @param v
	 * @return
	 * @return: int
	 * @throws:
	*/
	private int checkPri(View v) {
		// mac dinh se la invisible
		int status = INVISIBLE;
		if (priHashMap != null && !isFullPrivilege) {
			PriControlStatus cs = (PriControlStatus) v.getTag(v.getId());
			if(cs == null)
				return status;
			PriDto priDto = priHashMap.get(cs.controlCode);
			if (priDto != null) {
				// neu PriControl duoc khai bao cho 1 tap control (1 chuc nang nhieu
				// control)
				// thi add vap listControl de quan li
				priDto.control = v;
				status = priDto.controlStatus.status;
			}
		} else {
			status = ENABLE;
		}
		// tam thoi cho tat ca cac control deu duoc hien thi va duoc xu li
		status = ENABLE;
		return status;
	}

	// ----------- xu li menu --------------


	/**
	 * khoi tao priHashMapMenu, duoc goi khi vao activity
	 * @author: DungNX
	 * @param nvbhMenu.getMenuName()
	 * @return: void
	 * @throws:
	*/
	public void genPriHashMapForMenu(PriHashMap.PriMenu menu, PriHashMap.PriForm form) {
		try {
			role = menu.getMenuName();
			// tao ds phan quyen
			// lay tu db len cho nay
			genPriHashMapForForm(form);
			priHashMapMenu = SQLUtils.getInstance().getAppPrivilege(role);
		} catch (Exception e) {
			// TODO: handle exception
		}

	}


	/**
	 * kiem tra va remove cac menu khong co quyen, sau khi khoi tao menu
	 * @author: DungNX
	 * @param listMenu
	 * @return: void
	 * @throws:
	*/
	public void checkMenu(ArrayList<MenuItemDTO> listMenu) {
		ArrayList<MenuItemDTO> listMenuRemove = new ArrayList<MenuItemDTO>();
		int index = 0;
		for(MenuItemDTO item : listMenu) {
			if(formStatus != 1){
				// formStatus != 1 remove het
				listMenuRemove.add(item);
			} else if (!isFullPrivilege) {
				if (!StringUtil.isNullOrEmpty(item.controlOrdinal)) {
					int status = INVISIBLE;
					PriDto priDto = priHashMapMenu.get(item.controlOrdinal);
					if (priDto != null) {
						status = priDto.controlStatus.status;
					}
					if (status == INVISIBLE) {
						listMenuRemove.add(item);
					}
				}
			}
			item.menuIndex = index;
			index++;
		}
		listMenu.removeAll(listMenuRemove);
	}

	/**
	 * kiem tra quyen cua cac tab, addMenuItem cua RightHeaderMenuBar
	 * @author: DungNX
	 * @param newItem
	 * @param menuOrdinal
	 * @return
	 * @return: int
	 * @throws:
	*/
	public int checkMenu(MenuItem newItem, String menuOrdinal){
		int status = INVISIBLE;
		if (!isFullPrivilege) {
			PriDto priDto = priHashMapMenu.get(menuOrdinal);
			if (priDto != null) {
				status = priDto.controlStatus.status;
			}
		} else {
			status = ENABLE;
		}
		return status;
	}

	/**
	 * kiem tra quyen cua cac tab, addMenuItem cua RightHeaderMenuBar
	 * @author: DungNX
	 * @param menu
	 * @return
	 * @return: int :  INVISIBLE = 1; ENABLE = 2; DISABLE = 3;
	 * @throws:
	 */
	public int checkMenu(String menu){
		int status = INVISIBLE;
		if (!isFullPrivilege) {
			PriDto priDto = priHashMapMenu.get(menu);
			if (priDto != null) {
				status = priDto.controlStatus.status;
			}
		} else {
			status = ENABLE;
		}
		return status;
	}

	/**
	 * Lay danh sach id nhan vien ban hang thuoc quan ly cua giam sat
	 *
	 * @author: quangvt1
	 * @since: 11:22:48 07-08-2014
	 * @return: String
	 * @throws:
	 * @param staffOwnerId
	 * @return Tra ve chuoi dang: id1, id2, ....
	 * @throws Exception
	 */
	public String getListStaffOfSupervisor(String staffOwnerId, String shopId) throws Exception{
		String listStaffId = "";
		listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
		if(listStaffId == null){
			listStaffId = "";
		}
		return listStaffId;
	}

	/**
	 * Lay danh sach id nhan vien ban hang thuoc quan ly cua giam sat bao gom nhan vien da nghi/chuyen shop trong thang
	 *
	 * @author: cuonglt3
	 * @since: 11:22:48 07-08-2014
	 * @return: String
	 * @throws:
	 * @param staffOwnerId
	 * @return Tra ve chuoi dang: id1, id2, ....
	 */
	public String getListStaffOfSupervisorIncludeHistory(String staffOwnerId, String shopId){
		String listStaffId = "";
		listStaffId = SQLUtils.getInstance().getListStaffOfSupervisorIncludeHistory(staffOwnerId, shopId);
		if(listStaffId == null){
			listStaffId = "";
		}
		return listStaffId;
	}

	/**
	 * Lay danh sach id GS thuoc quan ly cua ASM
	 *
	 * @author: quangvt1
	 * @since: 11:22:48 07-08-2014
	 * @return: String
	 * @throws:
	 * @param staffIdASM
	 * @return Tra ve chuoi dang: id1, id2, ....
	 */
	public String getListSupervisorOfASM(String staffIdASM){
		String listStaffId = "";
		listStaffId = SQLUtils.getInstance().getListSupervisorOfASM(staffIdASM);
		if(listStaffId == null){
			listStaffId = "";
		}
		return listStaffId;
	}

	/**
	 * Lay danh sach id GS thuoc quan ly cua ASM bao gom ca gs da nghi/chuyen shop
	 *
	 * @author: cuonglt3
	 * @return Tra ve chuoi dang: id1, id2, ....
	 */
	public String getListSupervisorOfASMIncludeHistory(String staffIdASM){
		String listStaffId = "";
		listStaffId = SQLUtils.getInstance().getListSupervisorOfASMIncludeHistory(staffIdASM);
		if(listStaffId == null){
			listStaffId = "";
		}
		return listStaffId;
	}

	/**
	 * lay ds shop duoc phan quyen trong userDto
	 *
	 * @author: DungNX
	 * @param userDTO
	 * @return
	 * @return: String
	 * @throws:
	*/
	private ArrayList<Integer> getListShopTBHV(UserDTO userDTO) {
		ArrayList<Integer> listShopId = new ArrayList<Integer>();
		boolean isExistShop = false;
//		for (RoleUserDTO role : userDTO.listRole) {
//			for (ShopDTO shop : role.listShop) {
//				isExistShop = true;
//				if (!listShopId.contains(shop.shopId)) {
//					listShopId.add(shop.shopId);
//				}
//			}
//		}
		listShopId.add(Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().getInheritShopId()));
//		if(!isExistShop)
//			listShopId.add(Integer.valueOf(GlobalInfo.getInstance().getProfile().getUserData().shopIdProfile));// add shop default
		return listShopId;
	}

	/**
	 * lay ds tat ca shop con cua listshop trong userDto
	 * @author: DungNX
	 * @param listShop
	 * @return
	 * @return: String: shop1,shop2 ...
	 * @throws:
	*/
	public String getListShopChild() {
		ArrayList<Integer> listShop = getListShopTBHV(GlobalInfo.getInstance()
				.getProfile().getUserData());
		ArrayList<String> listShopId = new ArrayList<String>();
		try {
			if (listShop != null && listShop.size() > 0) {
				for (Integer shopId : listShop) {
					ArrayList<String> listShopIdTemp = SQLUtils.getInstance()
							.getListShopChild(String.valueOf(shopId));
					for (String stShopID : listShopIdTemp) {
						if (!listShopId.contains(stShopID)) {
							listShopId.add(stShopID);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		String strListShopChild = TextUtils.join(",", listShopId);

		return strListShopChild;
	}

	 /**
	 * Lay ds staff con
	 * @author: Tuanlt11
	 * @param staffId: staff nhan vien chon
	 * @param shopId: shop cua nhan vien chon
	 * @return
	 * @return: String
	 * @throws:
	*/
	public String getListStaffChild(String staffId, String shopId){
		String listStaffId = "";
		try {
			listStaffId = SQLUtils.getInstance().getListStaffChild(staffId,shopId);
			if(listStaffId == null){
				listStaffId = "";
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listStaffId;
	}

	/**
	 * Lay loai nhan vien
	 *
	 * @author: Tuanlt11
	 * @param staffId
	 * @return: void
	 * @throws:
	 */
	public int getStaffType(String staffId) {
		// mac dinh la nv
		int staffType = UserDTO.TYPE_STAFF;
		try {
			SQLUtils.getInstance().getStaffType(staffId);
		} catch (Exception e) {
			// TODO: handle exception
			MyLog.w("getStaffType",
					VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return staffType;
	}

	 /**
	 * Kiem tra phai la role quan li hay ko
	 * @author: Tuanlt11
	 * @param parentStaffId
	 * @return
	 * @return: boolean
	 * @throws:
	*/
	public boolean isRoleManager(int staffType) {
		if (staffType != UserDTO.TYPE_SUPERVISOR
				&& staffType != UserDTO.TYPE_STAFF
				) {
			return true;
		}
		return false;
	}


	 /**
	 * Ham kiem tra tat cac nhan vien duoi quyen deu la  loai NVBH de vao man hinh cuoi cung
	 * @author: Tuanlt11
	 * @param staffOwnerId: staff parent
	 * @param shopId: shop cua staff Parent
	 * @return: void
	 * @throws Exception
	 * @throws:
	*/
	public boolean checkAllChildStaffIsNV(String staffOwnerId, String shopId) throws Exception{
		String listStaffId = "";
		listStaffId = SQLUtils.getInstance().getListStaffOfSupervisor(staffOwnerId, shopId);
		if(!StringUtil.isNullOrEmpty(listStaffId)){
			return true;
		}
		return false;
	}

	/**
	 * Kiem tra loai don vi, neu tra ve '' ko la npp
	 *
	 * @author: dungdq3
	 * @since: 10:10:51 AM Mar 26, 2015
	 * @return: String
	 * @param shopID
	 * @throws Exception
	 */
	public String checkShopType(String shopID) throws Exception {
		String shopType = Constants.STR_BLANK;
		shopType = SQLUtils.getInstance().getShopType(shopID);
		return shopType;
	}

}
