package com.ths.dmscore.lib.oAuth.extractors;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ths.dmscore.lib.oAuth.exceptions.OAuthException;
import com.ths.dmscore.lib.oAuth.model.Token;
import com.ths.dmscore.lib.oAuth.utils.Preconditions;

public class JsonTokenExtractor implements AccessTokenExtractor {
	private static Pattern accessTokenPattern = Pattern.compile("\"access_token\":\"(\\S*?)\"");
	private static Pattern tokenTypePattern = Pattern.compile("\"token_type\":\"(\\S*?)\"");

	@Override
	public Token extract(String response) {
		String accessToken = "";
		String tokenType = "";
		Preconditions.checkEmptyString(response, "Cannot extract a token from a null or empty String");
		Matcher matcher = accessTokenPattern.matcher(response);
		if (matcher.find()) {
			accessToken = matcher.group(1);
		} else {
			throw new OAuthException("Cannot extract an access token. Response was: " + response);
		}

		matcher = tokenTypePattern.matcher(response);
		if (matcher.find()) {
			tokenType = matcher.group(1);
		} else {
			throw new OAuthException("Cannot extract an token type. Response was: " + response);
		}

		return new Token(tokenType, accessToken, "", response);
	}
}
