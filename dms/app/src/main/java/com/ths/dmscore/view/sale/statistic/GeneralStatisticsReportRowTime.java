/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.statistic;

import java.text.DecimalFormat;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.GeneralStatisticsInfoViewDTO;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.util.PriUtils;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dms.R;

/**
 * Thong tin cua 1 bao cao cham cong, thoi gian theo ngay
 *
 * @author: DungNX3
 * @version: 1.0
 * @since: Dec 10, 2012
 */
public class GeneralStatisticsReportRowTime extends DMSTableRow {
	// hien thi thoi gian cham cong hop le tu 7h45-8h15
	TextView tvTime1;
	// hien thi thoi gian cham cong sau 8h15
	TextView tvTime2;
	// tong thoi gian ghe tham
	TextView tvTotalTimeVisited;
	// action khi click vao thoi gian ghe tham
	TextView tvChamCong;
	int action;

	public GeneralStatisticsReportRowTime(Context context, VinamilkTableListener listen) {
		super(context, R.layout.layout_general_statistics_row_time);
		tvTime1 = (TextView) findViewById(R.id.tvTime1);
		tvTime2 = (TextView) findViewById(R.id.tvTime2);
		listener = listen;
		tvChamCong = (TextView) findViewById(R.id.tvChamCong);
		tvTotalTimeVisited = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvTotalTimeVisited,
				PriHashMap.PriControl.NVBH_THONGKE_DOANHSONGAY
						);
	}

	public void setAction(int _action) {
		this.action = _action;
//		tvTotalTimeVisited.setOnClickListener(this);
		PriUtils.getInstance().setOnClickListener(tvTotalTimeVisited, this);
	}

	/**
	 *
	 * render layout for row with a GeneralStatisticsViewDTO
	 *
	 * @author: HaiTC3
	 * @since: 4:51:47 PM | Jun 11, 2012
	 * @param position
	 * @param GeneralStatisticsViewDTO
	 *            item
	 * @return: void
	 * @throws:
	 */
	public void renderLayout(GeneralStatisticsInfoViewDTO item) {
		tvChamCong.setVisibility(GONE);
		if (!StringUtil.isNullOrEmpty(item.attendance.time1)) {
			StringBuilder str = new StringBuilder();
			str.append(DateUtils.convertDateOneFromFormatToAnotherFormat(
					item.attendance.time1, DateUtils.DATE_FORMAT_ATTENDANCE,
					DateUtils.DATE_FORMAT_HOUR_MINUTE));

			if (item.attendance.distance1 >= 1000) {
				double tempDistance = (double) item.attendance.distance1 / 1000;
				DecimalFormat df = new DecimalFormat("0.00");
				String formater = df.format(tempDistance);
				str.append(" - (" + formater + " km)");
			} else if (item.attendance.distance1 >= 0) {
				str.append(" - (" + item.attendance.distance1 + " m)");
			}

			tvTime1.setText(str.toString());
		}
		if (!item.attendance.onTime && item.attendance.time2 != null
				&& !StringUtil.isNullOrEmpty(item.attendance.time2)) {
			StringBuilder str = new StringBuilder();
			str.append(DateUtils.convertDateOneFromFormatToAnotherFormat(
					item.attendance.time2, DateUtils.DATE_FORMAT_ATTENDANCE,
					DateUtils.DATE_FORMAT_HOUR_MINUTE));
			if (item.attendance.distance2 >= 1000) {
				double tempDistance = (double) item.attendance.distance2 / 1000;
				DecimalFormat df = new DecimalFormat("0.00");
				String formater = df.format(tempDistance);
				str.append(" - (" + formater + " km)");
			} else if (item.attendance.distance2 >= 0) {
				str.append(" - (" + item.attendance.distance2 + " m)");
			}
			tvTime2.setText(str.toString());
		}

		SpannableObject obj = new SpannableObject();
		obj.addSpan(String.valueOf(item.time),
				ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL, null);
		SpannableString sText = obj.getSpan();
		tvTotalTimeVisited.setText(sText);

		if (!item.attendance.onTime || item.attendance.distance1 < 0) {
			renderWithColor(ImageUtil.getColor(R.color.RED));
		}
	}

	public void renderWithColor(int color) {
		tvTime1.setTextColor(color);
		tvTime2.setTextColor(color);
		tvTotalTimeVisited.setTextColor(color);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tvTotalTimeVisited:
			listener.handleVinamilkTableRowEvent(action, v, null);
			break;

		default:
			super.onClick(v);
			break;
		}

	}
}