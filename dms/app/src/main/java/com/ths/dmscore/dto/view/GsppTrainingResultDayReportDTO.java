package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

public class GsppTrainingResultDayReportDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double amountMonth;
	public double amount;
	public int isNew;
	public int isOn;
	public int isOr;
	public double score;
	public double distance;
	public ArrayList<StaffTrainResultReportItem> listResult = new ArrayList<StaffTrainResultReportItem>();

	public enum VISIT_STATUS {
		NONE_VISIT, VISITED, VISITING
	}

	public class StaffTrainResultReportItem implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public int stt;
		public String custCode;
		public int custId;
		public String custName;
		public String custAddr;
		public double amountMonth;
		public double amount;
		public double score;
		public int isNew;
		public int isOn;
		public int isOr;
		public double lat;
		public double lng;
		public int customerTypeId;
		public String startTime;
		public String endTime;
		public double tpdScore;
		public VISIT_STATUS visit = VISIT_STATUS.NONE_VISIT;
		public long visitActLogId;

		public void initFromCursor(Cursor c, GsppTrainingResultDayReportDTO dto, int stt2) {
			stt = stt2;
			custCode = CursorUtil.getString(c, "CUSTOMER_CODE");
			custId = CursorUtil.getInt(c, "CUSTOMER_ID");
			custName = CursorUtil.getString(c, "CUSTOMER_NAME");
			custAddr = CursorUtil.getString(c, "HOUSENUMBER");
			custAddr += " " + CursorUtil.getString(c, "STREET");
			amountMonth = CursorUtil.getDouble(c, "AMOUNT_PLAN");
			amount = CursorUtil.getDouble(c, "AMOUNT");
			isNew = CursorUtil.getInt(c, "IS_NEW");
			isOn = CursorUtil.getInt(c, "IS_ON");
			isOr = CursorUtil.getInt(c, "IS_OR");
			score = CursorUtil.getDouble(c, "SCORE");
			lat = CursorUtil.getDouble(c, "LAT");
			lng = CursorUtil.getDouble(c, "LNG");
			startTime = CursorUtil.getString(c, "AL_START_TIME");
			endTime = CursorUtil.getString(c, "AL_END_TIME");
			visitActLogId = CursorUtil.getLong(c, "AL_ID");
			if (!StringUtil.isNullOrEmpty(startTime)) {
				if (!StringUtil.isNullOrEmpty(endTime)) {
					visit = VISIT_STATUS.VISITED;
				} else {
					visit = VISIT_STATUS.VISITING;
				}
			}
			customerTypeId = CursorUtil.getInt(c, "CUSTOMER_TYPE_ID");
			tpdScore = CursorUtil.getDouble(c, "TPD_SCORE");
			dto.listResult.add(this);
			dto.amountMonth += amountMonth;
			dto.amount += amount;
			dto.isNew += isNew;
			dto.isOr += isOr;
			dto.isOn += isOn;
			// dto.score += score;
			dto.score = tpdScore;
		}

	}

	public StaffTrainResultReportItem newStaffTrainResultReportItem() {
		// TODO Auto-generated method stub
		return new StaffTrainResultReportItem();
	}
}
