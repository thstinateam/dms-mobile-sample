package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.SALE_PROMO_MAP_TABLE;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.view.OrderDetailViewDTO;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.PO_CUSTOMER_PROMO_MAP_TABLE;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Luu thong tin san pham mua dat KM hay ko dat
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class SalePromoMapDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 3789368646418890119L;
	// noi dung field
	public long salePromoMapId; // id bang
	public long saleOrderDetailId; // id cua saleoder detail
	public String programCode; // ma CTKM
	public int status; // trang thai dat hay ko dat CTKM
	public static final int TYPE_ACHIVE = 6;// dat
	public static final int TYPE_NOT_ACHIVE = 5;// ko dat KM
	public long staffId; // id nhan vien
	public long saleOrderId; // id don hang

	// dung cho Po customer map
	public long poCustomerPromoMapId;
	public long poCustomerId;
	public long poCustomerDetailId;


	 /**
	 * Cap nhat du lieu
	 * @author: Tuanlt11
	 * @param saleOrderDetailId
	 * @param promotion
	 * @return: void
	 * @throws:
	*/
	public void updateData(OrderDetailViewDTO promotion){
		programCode = promotion.orderDetailDTO.programeCode;
	}
	 /**
	 * gen cau insert
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInsertSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, SALE_PROMO_MAP_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.SALE_PROMO_MAP_ID, salePromoMapId, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.SALE_ORDER_DETAIL_ID, saleOrderDetailId, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.PROGRAM_CODE, programCode, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.SALE_ORDER_ID, saleOrderId, null));
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return json;
	}

	/**
	 * gen cau insert po customer promotion map
	 * @author: Tuanlt11
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInsertPOCustomerPromoMapSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME,
					PO_CUSTOMER_PROMO_MAP_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.PO_CUSTOMER_PROMO_MAP_ID, poCustomerPromoMapId,
					null));
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.PO_CUSTOMER_DETAIL_ID,
					poCustomerDetailId, null));
			detailPara.put(GlobalUtil
					.getJsonColumn(PO_CUSTOMER_PROMO_MAP_TABLE.PROGRAM_CODE,
							programCode, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.STATUS, status, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.STAFF_ID, staffId, null));
			detailPara.put(GlobalUtil.getJsonColumn(
					PO_CUSTOMER_PROMO_MAP_TABLE.PO_CUSTOMER_ID, poCustomerId,
					null));
			json.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
		} catch (JSONException e) {
		}

		return json;
	}

	 /**
	 * Xoa cac KM dat hoac ko dat lien quan toi don hang
	 * @author: Tuanlt11
	 * @param saleOrderId
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateDeletePromoMapByOrderSql(long saleOrderId) {
		JSONObject orderJson = new JSONObject();
		try{
			orderJson.put(IntentConstants.INTENT_TYPE, TableAction.DELETE);
			orderJson.put(IntentConstants.INTENT_TABLE_NAME, SALE_PROMO_MAP_TABLE.TABLE_NAME);

			// ds where params --> insert khong co menh de where
			JSONArray whereParams = new JSONArray();
			whereParams.put(GlobalUtil.getJsonColumn(SALE_PROMO_MAP_TABLE.SALE_ORDER_ID, String.valueOf(saleOrderId), null));
			orderJson.put(IntentConstants.INTENT_LIST_WHERE_PARAM, whereParams);
		}catch (JSONException e) {
			// TODO: handle exception
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return orderJson;
	}

	 /**
	 * Khoi tao du lieu tu cursor
	 * @author: Tuanlt11
	 * @param c
	 * @return: void
	 * @throws:
	*/
	public void initDataFromCursor(Cursor c) {
		saleOrderDetailId = CursorUtil.getLong(c, SALE_PROMO_MAP_TABLE.SALE_ORDER_DETAIL_ID);
		salePromoMapId = CursorUtil.getInt(c, SALE_PROMO_MAP_TABLE.SALE_PROMO_MAP_ID);
		programCode = CursorUtil.getString(c, SALE_PROMO_MAP_TABLE.PROGRAM_CODE);
		staffId = CursorUtil.getInt(c, SALE_PROMO_MAP_TABLE.STAFF_ID);
		saleOrderId = CursorUtil.getInt(c, SALE_PROMO_MAP_TABLE.SALE_ORDER_ID);
		status = CursorUtil.getInt(c, SALE_PROMO_MAP_TABLE.STATUS);
	}
}
