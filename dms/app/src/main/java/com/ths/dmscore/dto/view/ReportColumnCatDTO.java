package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.PRODUCT_INFO_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 * DTO chon column nganh hang cho man hinh bao cao dong
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ReportColumnCatDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	public int catID;
	public int subCatID;
	// ma nganh hang
	public String catCode;
	public String subCatCode;
	// ten nganh hang
	public String catName;
	public String subCatName;
	public boolean isChecked;
	public int status;

	public ReportColumnCatDTO(){
		catID = 0;
		subCatID = 0;
		catCode = Constants.STR_BLANK;
		subCatCode = Constants.STR_BLANK;
		catName = Constants.STR_BLANK;
		subCatName = Constants.STR_BLANK;
		isChecked = true;
	}

	public void initDataWithCursor(Cursor c) {
		// TODO Auto-generated method stub
		catID = CursorUtil.getInt(c, PRODUCT_INFO_TABLE.PRODUCT_INFO_ID);
		catCode = CursorUtil.getString(c, PRODUCT_INFO_TABLE.PRODUCT_INFO_CODE);
		subCatID = CursorUtil.getInt(c, "SUB_CAT_ID");
		subCatCode = CursorUtil.getString(c, "SUB_CAT_CODE");
		catName = CursorUtil.getString(c, PRODUCT_INFO_TABLE.PRODUCT_INFO_NAME);
		subCatName = CursorUtil.getString(c, "SUB_CAT_NAME");
		status = CursorUtil.getInt(c, PRODUCT_INFO_TABLE.STATUS);
	}

	public ReportColumnCatDTO clone() {
		ReportColumnCatDTO temp = new ReportColumnCatDTO();
		temp.catID = catID;
		temp.catCode = catCode;
		temp.subCatID = subCatID;
		temp.subCatCode = subCatCode;
		temp.catName = catName;
		temp.subCatName = subCatName;
		temp.status = status;
		temp.isChecked = isChecked;
		return temp;
	}

}
