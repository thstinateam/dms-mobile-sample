/**
 * Copyright THS.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.DisplayProgrameDTO;

/**
 * DTO chua du lieu cua view ListAlbumUser
 *
 * @author: SoaN
 * @version: 1.0
 * @since: Aug 2, 2012
 */

public class ListAlbumUserDTO implements Serializable {
	private static final long serialVersionUID = 34646893146881966L;
	public static final int TYPE_IS_OR = -1;
	public static final int TYPE_IS_VISITED = 1;
	public static final int TYPE_NO_VISITED = 0;


	private ArrayList<AlbumDTO> listAlbum;
	private CustomerDTO customer;
	private ArrayList<AlbumDTO> listProgrameAlbum;// thanhnn add
	private ArrayList<DisplayProgrameItemDTO> listDisplayPrograme;// thanhnn add
	private ArrayList<DisplayProgrameDTO> listPhotoDPrograme;// thanhnn add
	public boolean isFirstInit;
	public double shopDistance;

	public int visitType = -1;
	public int monthSeq;

	/**
	 *
	 */
	public ListAlbumUserDTO() {
		setListAlbum(new ArrayList<AlbumDTO>());
		setListProgrameAlbum(new ArrayList<AlbumDTO>());
		listDisplayPrograme = new ArrayList<DisplayProgrameItemDTO>();
		listPhotoDPrograme = new ArrayList<DisplayProgrameDTO>();
	}

	/**
	 * @return the listAlbum
	 */
	public ArrayList<AlbumDTO> getListAlbum() {
		return listAlbum;
	}

	/**
	 * @param listAlbum
	 *            the listAlbum to set
	 */
	public void setListAlbum(ArrayList<AlbumDTO> listAlbum) {
		this.listAlbum = listAlbum;
	}

	/**
	 * @return the customer
	 */
	public CustomerDTO getCustomer() {
		return customer;
	}

	/**
	 * @param customer
	 *            the customer to set
	 */
	public void setCustomer(CustomerDTO customer) {
		this.customer = customer;
	}

	public ArrayList<AlbumDTO> getListProgrameAlbum() {
		return listProgrameAlbum;
	}

	public void setListProgrameAlbum(ArrayList<AlbumDTO> listProgrameAlbum) {
		this.listProgrameAlbum = listProgrameAlbum;
	}

	public ArrayList<DisplayProgrameItemDTO> getListDisplayPrograme() {
		return listDisplayPrograme;
	}

	public void setListDisplayPrograme(
			ArrayList<DisplayProgrameItemDTO> listDisplayPrograme) {
		this.listDisplayPrograme = listDisplayPrograme;
	}

	public ArrayList<DisplayProgrameDTO> getListPhotoDPrograme() {
		return listPhotoDPrograme;
	}

	public void setListPhotoDPrograme(
			ArrayList<DisplayProgrameDTO> listPhotoDPrograme) {
		this.listPhotoDPrograme = listPhotoDPrograme;
	}
}
