/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.DEBIT_DETAIL_TEMP_TABLE;
import com.ths.dmscore.util.CollumnMapping;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.DMSCursor;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * Thong tin chi tiet cong no
 *
 * @author: BangHN
 * @version: 1.0
 * @since: 1.0
 */
public class DebitDetailDTO extends AbstractTableDTO implements Serializable {
	private static final long serialVersionUID = -5885382317573753993L;
	// id
	public long debitID;
	public long debitDetailID;
	public long fromObjectID;
	public double amount;
	public double discount;
	public double total;
	public long totalPay;
	public long discountAmount;
	public double remain;
	public long repay;
	public long numDayOver;
	public long numDenied;
	public long invoiceNumber;
	public String fromObjectNumber;
	public String createDate;
	public String returnDate;
	public String createUser;
	public String debitDate;
	public int type;
	public long shopId;
	public long customerId;
	public long staffID;
	//tu choi
	public int refuse;
	//can update
	public boolean isChanged;

	public String saleDate;
	public String orderType;

	public DebitDetailDTO() {
		super(TableType.DEBIT_DETAIL_TEMP_TABLE);
	}

	/**
	 * Khoi tao du lieu
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: void
	 * @throws:
	 * @param c
	 * @param mapping
	 */
	public void initData(Cursor c, CollumnMapping mapping) {
		DMSCursor cu = null;
		cu = new DMSCursor(c, mapping);
		this.debitID = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.DEBIT_ID, -1);
		this.debitDetailID = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DETAIL_TEMP_ID, -1);
		this.fromObjectID = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_ID, -1);
		this.amount = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.AMOUNT, 0);
		this.discount = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT, 0);
		this.total = cu.getDouble(DEBIT_DETAIL_TEMP_TABLE.TOTAL, 0);
		this.totalPay = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.TOTAL_PAY, 0);
		this.discountAmount = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT_AMOUNT, 0);
		this.remain = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.REMAIN, 0);
		this.invoiceNumber = cu.getLong(DEBIT_DETAIL_TEMP_TABLE.INVOICE_NUMBER, 0);
		this.fromObjectNumber = cu.getString(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_NUMBER, "");
		this.createDate = cu.getString(DEBIT_DETAIL_TEMP_TABLE.CREATE_DATE, "");
		this.returnDate = cu.getString("RETURN_DATE", "");
		this.createUser = cu.getString(DEBIT_DETAIL_TEMP_TABLE.CREATE_USER, "");
		this.debitDate = cu.getString(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DATE, "");
		this.type = cu.getInt(DEBIT_DETAIL_TEMP_TABLE.TYPE, 0);
		this.staffID = cu.getInt(DEBIT_DETAIL_TEMP_TABLE.STAFF_ID, 0);
	}

	/**
	 * Generate cau lenh insertOrupdate
	 *
	 * @author: TruongHN
	 * @return: JSONObject
	 * @throws:
	 */
	public JSONObject generateInsertFromOrderSql() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			json.put(IntentConstants.INTENT_TABLE_NAME, DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			// staff_customer_id: id tu tang tren server, va khong cap nhat khi
			// update
			params.put(GlobalUtil.getJsonColumnWithKey(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DETAIL_TEMP_ID, debitDetailID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_ID, debitID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_ID, fromObjectID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_NUMBER, fromObjectNumber, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.AMOUNT, amount, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT, discount, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TOTAL, total, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TOTAL_PAY, totalPay, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.REMAIN, remain, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TYPE, type, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT_AMOUNT, discountAmount, null));
			if (customerId > 0) {
				params.put(GlobalUtil.getJsonColumn(
						DEBIT_DETAIL_TEMP_TABLE.CUSTOMER_ID, customerId, null));
			}
			if (shopId > 0) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.SHOP_ID,
						shopId, null));
			}
			if (staffID > 0) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.STAFF_ID,
						staffID, null));
			}
			if (!StringUtil.isNullOrEmpty(createUser)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.CREATE_USER, createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(createDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.CREATE_DATE, createDate, null));
			}
			if (!StringUtil.isNullOrEmpty(debitDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DATE, debitDate, null));
			}
			if (!StringUtil.isNullOrEmpty(orderType)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.ORDER_TYPE, orderType, null));
			}
			if (!StringUtil.isNullOrEmpty(saleDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.ORDER_DATE, saleDate, null));
			}
			if (!StringUtil.isNullOrEmpty(returnDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.RETURN_DATE, returnDate, null));
			}


			json.put(IntentConstants.INTENT_LIST_PARAM, params);

		} catch (JSONException e) {
		}
		return json;
	}

//	/**
//	 * Mo ta muc dich cua ham
//	 *
//	 * @author: TamPQ
//	 * @param cusDebitDetailItem
//	 * @return: voidvoid
//	 * @throws:
//	 */
//	public JSONObject generateUpdateForPayDebt(CusDebitDetailItem cusDebitDetailItem) {
//		JSONObject debit = new JSONObject();
//		try {
//			debit.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
//			debit.put(IntentConstants.INTENT_TABLE_NAME, DEBIT_DETAIL_TABLE.TABLE_NAME);
//			// ds params
//			JSONArray detailPara = new JSONArray();
//			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TABLE.TOTAL_PAY, cusDebitDetailItem.paidAmount, null));
//			detailPara.put(GlobalUtil
//					.getJsonColumn(DEBIT_DETAIL_TABLE.REMAIN, cusDebitDetailItem.remainingAmount, null));
//			detailPara.put(GlobalUtil
//					.getJsonColumn(DEBIT_DETAIL_TABLE.DISCOUNT_AMOUNT, cusDebitDetailItem.discount, null));
//			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TABLE.UPDATE_DATE, DateUtils.now(), null));
//			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TABLE.UPDATE_USER, ""
//					+ GlobalInfo.getInstance().getProfile().getUserData().userName, null));
//			debit.put(IntentConstants.INTENT_LIST_PARAM, detailPara);
//
//			// ds where params --> insert khong co menh de where
//			JSONArray wheres = new JSONArray();
//			wheres.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TABLE.DEBIT_DETAIL_ID, cusDebitDetailItem.debitDetailId,
//					null));
//			debit.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
//		} catch (Exception e) {
//		}
//		return debit;
//	}

	/**
	 * tao cau Json update DebitDetailDTO
	 *
	 * @author: ThangNV31
	 * @since: 1.0
	 * @return: JSONObject
	 * @throws:
	 * @return
	 */
	public JSONObject generateUpdateForPayDebit() {
		JSONObject debit = new JSONObject();
		try {
			debit.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
			debit.put(IntentConstants.INTENT_TABLE_NAME, DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);
			// ds params
			JSONArray detailPara = new JSONArray();
			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TOTAL_PAY, totalPay, null));
			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.REMAIN, remain, null));
			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT_AMOUNT, discountAmount, null));
			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.UPDATE_DATE, DateUtils.now(), null));
			detailPara.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.UPDATE_USER, "" + GlobalInfo.getInstance().getProfile().getUserData().getUserName(), null));
			debit.put(IntentConstants.INTENT_LIST_PARAM, detailPara);

			// ds where params --> insert khong co menh de where
			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DETAIL_TEMP_ID, debitDetailID, null));
			debit.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
		} catch (Exception e) {
		}
		return debit;
	}

	/**
	 * Mo ta muc dich cua ham
	 * @author: DungNX
	 * @return
	 * @return: JSONObject
	 * @throws:
	*/
	public JSONObject generateInsertOrUpdateDebitDetail() {
		JSONObject json = new JSONObject();
		try {
			json.put(IntentConstants.INTENT_TYPE, TableAction.INSERTORUPDATE);
			json.put(IntentConstants.INTENT_TABLE_NAME, DEBIT_DETAIL_TEMP_TABLE.TABLE_NAME);

			// ds params
			JSONArray params = new JSONArray();
			// staff_customer_id: id tu tang tren server, va khong cap nhat khi
			// update
			params.put(GlobalUtil.getJsonColumnWithKey(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DETAIL_TEMP_ID, debitDetailID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_ID, debitID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_ID, fromObjectID, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.FROM_OBJECT_NUMBER, fromObjectNumber, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.AMOUNT, amount, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT, discount, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TOTAL, total, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TOTAL_PAY, totalPay, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.REMAIN, remain, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.TYPE, type, null));
			params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DISCOUNT_AMOUNT, discountAmount, null));

			if (!StringUtil.isNullOrEmpty(createUser)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.CREATE_USER, createUser, null));
			}
			if (!StringUtil.isNullOrEmpty(createDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.CREATE_DATE, createDate, null));
			}
			if (!StringUtil.isNullOrEmpty(debitDate)) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DATE, debitDate, null));
			}
			if (staffID > 0) {
				params.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.STAFF_ID,
						staffID, null));
			}

			json.put(IntentConstants.INTENT_LIST_PARAM, params);

			JSONArray wheres = new JSONArray();
			wheres.put(GlobalUtil.getJsonColumn(DEBIT_DETAIL_TEMP_TABLE.DEBIT_DETAIL_TEMP_ID, debitDetailID, null));
			json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);

		} catch (JSONException e) {
		}
		return json;
	}
}
