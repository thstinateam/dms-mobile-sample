/**

 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.io.Serializable;
import java.util.ArrayList;

import android.content.SharedPreferences;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.RoleUserDTO;
import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.network.http.HTTPClient;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.LoginView;
import com.ths.dmscore.dto.db.ShopDTO;
import com.ths.dmscore.dto.map.GeomDTO;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * Chua thong tin cua 1 user
 *
 * @author: DoanDM
 * @version: 1.1
 * @since: 1.0
 */
public class UserDTO implements Serializable {
	private static final long serialVersionUID = 7660587792488654592L;
	public static final int NOT_LOGIN = 0;
	public static final int LOGIN_SUCCESS = 1;
	public static final int PROCESSING_LOGIN = 2;
	// DINH NGHIA LOAI NHAN VIEN
	public static final int TYPE_STAFF = 1;// presales
	public static final int TYPE_SUPERVISOR = 2;// giam sat
	public static final int TYPE_MANAGER = 3;// tbv vung

	public static final int NPP_SPECIFIC_TYPE = 1;

	// la so cua viettel
//	public boolean isVT = false;
	private int staffId = -1;// id original cua NV dang nhap
	private int staffTypeId = -1;// id loai nhan vien
	private String userName;// ten dang nhap
	private String userCode;// ma code
	// ten hien thi
	private String birthday;// ngay sinh
	private String displayName;// ten
	private String street;// dia chi duong
	private String address;// dia chi duong
	private String plan;// plan
	// thong tin version app
	private AppVersionDTO appVersion;
	// thong tin version db
	private DBVersionDTO dbVersion;
	// co can reset clear DB
	private boolean isDeleteData = false;

	private GeomDTO geom = new GeomDTO();
	private int loginState = 0;
	private String pass;
	// id nguoi quan ly
	// public long staffOwnerId;
	// ma loai nhan vien ban hang
	private String staffTypeName;

	// loai nhan vien
	// public int chanelType;

	// loai NV dang nhap: 1: NV, 2: GS, 3 : QL
	//public int specificType = -1;
	// ten loai nhan vien
	private String channelTypeName;

	private String serverDate;// thoi gian server tra ve
	private boolean isUserDB = false;
	// shop chon de quan ly ( cua GSNPP)
	private ShopDTO shopManaged;
//	public String shopId; // shop mac dinh cua NV duoc ke thua
//	public String shopCode;
//	// shopId de dong bo du lieu, ....
//	public String shopIdProfile;
//	// toa do lat,lng NPP
//	public double shopLat = 0;
//	public double shopLng = 0;
//	public String saleTypeCode;

	// list shop quan ly cua GSNPP
	// public ArrayList<ShopDTO> listShop;
	private ArrayList<RoleUserDTO> listRole;
	private RoleUserDTO roleManaged;
	private String roleId;

	//--> thong tin lien quan den user ke thua, neu ko ke thua thi mac dinh inherit la nhan vien dang nhap

	// id cua NV ke thua, neu ko ke thua ai thi Id = id user dang nhap
	private int inheritId = -1;
	private String inheritStaffShopId; // shop mac dinh cua NV duoc ke thua
	// Id shop chon de quan ly
	private String inheritShopId;
	private String inheritShopCode;
	// shopId de dong bo du lieu, ....
	private String inheritShopIdProfile;
	// toa do lat,lng NPP
	private double inheritShopLat = 0;
	private double inheritShopLng = 0;
	// loai NV Ke thua: 1: NV, 2: GS, 3 : QL
	private int inheritSpecificType = -1;
	// loai ban hang cua nv ke thua
	private String inheritSaleTypeCode;

	//<-- Ket thuc thong tin lien quan den thong tin ke thua

	// cho phep ghi log kpi
	private int enableClientLog = 0;
	private int deviceId = 0;
	private static boolean needResetPassword = false;
	//trang thai check install, truy cap network
	public static final int CHECK_INSTALL = 1;
	public static final int UNCHECK_INSTALL = 2;
	// phan check network
	public static final int CHECK_NETWORK_3G = 1;
	public static final int CHECK_NETWORK_WIFI = 2;
	public static final int CHECK_NETWORK_ALL = 3;
	public static final int UNCHECK_ALL_ACCESS = 4;
	public static final int CHECK_NETWORK_OFFLINE = 5;

	public void parseFromJSONLogin(JSONObject entry, int action)
			throws Exception {
		if (entry == null)
			return;
		try {
			// apversion
			if (entry.has("need_reset_password")) {
				setNeedResetPassword(entry.getBoolean("need_reset_password"));
			}
			if (entry.has("appversion")
					&& entry.getJSONObject("appversion").length() != 0) {
				setAppVersion(new AppVersionDTO());
				getAppVersion().parseFromJson(entry
						.getJSONObject("appversion"));
			}
			// dbversion
			if (entry.has("dbversion")) {
				setDbVersion(new DBVersionDTO());
				getDbVersion().parseFromJson(entry
						.getJSONObject("dbversion"));
			}

			JSONObject channelType = entry.getJSONObject("staffType");
			if (channelType != null) {
				setStaffTypeName(channelType
						.getString("staffTypeName"));
//				specificType = channelType.getInt("specificType");
				// ban dau inherit type = specifictyp
				setInheritSpecificType(channelType.getInt("specificType"));
			}
			setServerDate(entry.getString("serverDate"));

			// isVT = entry.getBoolean("isVT");
			GlobalInfo.getInstance().getProfile()
					.setAuthData(entry.getString("authData"));

			//id thiet bi
			if (entry.has("deviceId")) {
				setDeviceId(entry.getInt("deviceId"));
			}

			parseProfile(this, entry);
			HTTPClient.setSessionID("JSESSIONID=" + entry.getString("sessionId"));

			JSONArray jArrRole = entry.getJSONArray("listRole");

			if (jArrRole != null) {
				// lay danh sach vai tro
				setListRole(new ArrayList<RoleUserDTO>());

				// moi vai tro se co 1 listShop
				for (int i = 0, size = jArrRole.length(); i < size; i++) {
					JSONObject item = jArrRole.getJSONObject(i);
					if (item != null) {
						RoleUserDTO role = new RoleUserDTO();
						role.roleCode = item.getString("roleCode");
						role.roleName = item.getString("roleName");
						role.roleId = item.getInt("roleId");
						role.inheritStaffSpecificType = item.getInt("inheritStaffSpecificType");
						role.inheritStaffTypeName = item.getString("inheritStaffTypeName");
						parseProfile(role.inheritProfile, item);
						role.inheritProfile.setServerDate(this.getServerDate());
						// apversion
						if (item.has("appversion")
								&& entry.getString("appversion").length() != 0) {
							role.appVersion = new AppVersionDTO();
							role.appVersion.parseFromJson(new JSONObject(entry.getString("appversion")));
						}
						role.dbVersion = getDbVersion();// set lai db version

						JSONArray jArrRoleShop = item.getJSONArray("listShop");
						if (jArrRoleShop != null) {
							role.listShop = new ArrayList<ShopDTO>();
							for (int j = 0, sizeShop = jArrRoleShop.length(); j < sizeShop; j++) {
								JSONObject jShop = jArrRoleShop
										.getJSONObject(j);
								if (jShop != null) {
									ShopDTO shop = new ShopDTO();
									shop.shopId = jShop.getInt("shopId");
									shop.shopName = jShop.getString("shopName");
									shop.shopCode = jShop.getString("shopCode");
									shop.street = jShop.getString("address");
									shop.shopLat = jShop.getDouble("lat");
									shop.shopLng = jShop.getDouble("lng");
									role.listShop.add(shop);
								}
							}
						}
						getListRole().add(role);
					}
				}
			}

			// Bo phan lay list shop, su dung lay list vai tro
			/*
			 * JSONArray listSubShop = entry.getJSONArray("listSubShop"); if
			 * (listSubShop != null) { // Fixed truong hop gsnpp login offline,
			 * // sau do relogin trong ct, can lay lai shopnpp chon truoc do
			 *
			 * SharedPreferences sharedPreferences = GlobalInfo .getInstance()
			 * .getAppContext() .getSharedPreferences(LoginView.PREFS_PRIVATE,
			 * Context.MODE_PRIVATE); String shopIdNPP =
			 * sharedPreferences.getString( LoginView.VNM_SHOP_ID, "");
			 *
			 * listShop = new ArrayList<ShopDTO>(); int size =
			 * listSubShop.length(); for (int i = 0; i < size; i++) { JSONObject
			 * item = listSubShop.getJSONObject(i); if (item != null) { ShopDTO
			 * shop = new ShopDTO(); shop.shopId = item.getInt("shopId");
			 * shop.shopName = item.getString("shopName"); shop.shopCode =
			 * item.getString("shopCode"); shop.street =
			 * item.getString("address"); listShop.add(shop);
			 *
			 * // lay lai id npp chon truoc do if (shopIdNPP != null &&
			 * shopIdNPP.equals("" + shop.shopId)) { this.shopId = "" +
			 * shop.shopId; } } } }
			 */

			if (action == ActionEventConstant.RE_LOGIN) {
				// fix relogin trong ct, can lay lai shopId, roleId chon truoc
				// do
				SharedPreferences sharedPreferences = GlobalInfo.getInstance()
						.getDmsPrivateSharePreference();
				if (sharedPreferences.contains(LoginView.VNM_SHOP_ID)) {
					String shopIdLast = sharedPreferences.getString(
							LoginView.VNM_SHOP_ID, "");
					this.setInheritShopId(shopIdLast);
				}
				if (sharedPreferences.contains(LoginView.VNM_ROLE_ID)) {
					String roleIdLast = sharedPreferences.getString(
							LoginView.VNM_ROLE_ID, "");
					this.setRoleId(roleIdLast);
				}
				// lay lai thong tin shop chon
				setShopManaged(GlobalInfo.getInstance().getShopManaged());
			}
			setLoginState(LOGIN_SUCCESS);

		} catch (JSONException ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			throw ex;
		}
	}

	public void setGeom(GeomDTO geom) {
		this.geom = geom;
	}

	public GeomDTO getGeom() {
		return geom;
	}

	public void setLoginState(int state) {
		this.loginState = state;
	}

	public int getLoginState() {
		return this.loginState;
	}

	/**
	 * Lay RoleID + ShopID cua user dang nhap
	 *
	 * @author: Tuanlt11
	 * @return
	 * @return: String
	 * @throws:
	 */
	public String getAddRoleShopUser() {
		return getRoleId() + getInheritShopId();
	}

	 /**
	 * parse thong tin tu profile
	 * @author: Tuanlt11
	 * @param userDTO
	 * @param entry
	 * @return: void
	 * @throws:
	*/
	public void parseProfile(UserDTO userDTO, JSONObject entry) throws Exception {
		try {
			// parse profile user login
			if(!entry.has("profile") || JSONObject.NULL.equals(entry.get("profile"))){
				// truong hop id dang nhap giong inherit thi profile ko tra ve du lieu
				// luc nay gan profile bang thong tin cua user dang nhap de xu li cho truong hop offline, lay lai thong tin
				userDTO = this;
				return;
			}
			JSONObject profile = entry.getJSONObject("profile");
			if (profile != null) {
				userDTO.setInheritId(profile.getInt("staffId"));
				// default staffId = inheritId
				userDTO.setStaffId(profile.getInt("staffId"));
				userDTO.setUserName(profile.getString("staffCode"));
				userDTO.setPass(profile.getString("password"));
				userDTO.setUserCode(profile.getString("staffCode"));
				userDTO.setBirthday(profile.getString("birthday"));
				userDTO.setStreet(profile.getString("street"));
				userDTO.setAddress(profile.isNull("address") ? "": profile.getString("address"));
				userDTO.setPlan(profile.getString("plan"));
				userDTO.setDisplayName(profile.getString("staffName"));
				// staffOwnerId = profile.getLong("staffOwnerId");
				userDTO.setStaffTypeId(profile.getInt("staffTypeId"));

				userDTO.setInheritSaleTypeCode(profile.getString("saleTypeCode"));
//				userDTO.saleTypeCode = profile.getString("saleTypeCode");
				if (StringUtil.isNullOrEmpty(getInheritSaleTypeCode())) {
					userDTO.setInheritSaleTypeCode(Constants.STR_BLANK);
				}
				int isDelete = profile.getInt("isDeleteData");
				if (isDelete == 1) {
					userDTO.setDeleteData(true);
				} else {
					userDTO.setDeleteData(false);
				}
				if (StringUtil.isNullOrEmpty(userDTO.getDisplayName())) {
					userDTO.setDisplayName(userDTO.getUserName());
				}
				// thong tin ban dau thi inherit = user dang nhap
				// sau khi login neu khac user dang nhap thi cac thong tin inherit bi thay doi
				userDTO.setInheritShopId(profile.getString("shopId"));
//				userDTO.shopId = profile.getString("shopId");
				userDTO.setInheritShopCode(profile.getString("shopCode"));
//				userDTO.shopCode = profile.getString("shopCode");;
				userDTO.setInheritShopIdProfile(userDTO.getInheritShopId());
//				userDTO.shopIdProfile = userDTO.inheritShopId;
				try {
					userDTO.setInheritShopLat(profile.getDouble("shopLat"));
					userDTO.setInheritShopLng(profile.getDouble("shopLng"));
//					userDTO.shopLat = profile.getDouble("shopLat");
//					userDTO.shopLng = profile.getDouble("shopLng");
				} catch (Exception e) {
					// TODO: handle exception
					userDTO.setInheritShopLat(-1);
//					userDTO.shopLat = -1;
					userDTO.setInheritShopLng(-1);
//					userDTO.shopLng = -1;
				}
				if (profile.has("enableClientLog")) {
					try {
						userDTO.setEnableClientLog(profile.getInt("enableClientLog"));
					} catch (Exception e) {
						userDTO.setEnableClientLog(0);
					}
				}
			}
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
			throw ex;
		}
	}

	public int getStaffId() {
		return staffId;
	}

	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}

	public int getStaffTypeId() {
		return staffTypeId;
	}

	public void setStaffTypeId(int staffTypeId) {
		this.staffTypeId = staffTypeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public AppVersionDTO getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(AppVersionDTO appVersion) {
		this.appVersion = appVersion;
	}

	public DBVersionDTO getDbVersion() {
		return dbVersion;
	}

	public void setDbVersion(DBVersionDTO dbVersion) {
		this.dbVersion = dbVersion;
	}

	public boolean isDeleteData() {
		return isDeleteData;
	}

	public void setDeleteData(boolean isDeleteData) {
		this.isDeleteData = isDeleteData;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getStaffTypeName() {
		return staffTypeName;
	}

	public void setStaffTypeName(String staffTypeName) {
		this.staffTypeName = staffTypeName;
	}

	public String getChannelTypeName() {
		return channelTypeName;
	}

	public void setChannelTypeName(String channelTypeName) {
		this.channelTypeName = channelTypeName;
	}

	public String getServerDate() {
		return serverDate;
	}

	public void setServerDate(String serverDate) {
		this.serverDate = serverDate;
	}

	public boolean isUserDB() {
		return isUserDB;
	}

	public void setUserDB(boolean isUserDB) {
		this.isUserDB = isUserDB;
	}

	public ShopDTO getShopManaged() {
		return shopManaged;
	}

	public void setShopManaged(ShopDTO shopManaged) {
		this.shopManaged = shopManaged;
	}

	public ArrayList<RoleUserDTO> getListRole() {
		return listRole;
	}

	public void setListRole(ArrayList<RoleUserDTO> listRole) {
		this.listRole = listRole;
	}

	public RoleUserDTO getRoleManaged() {
		return roleManaged;
	}

	public void setRoleManaged(RoleUserDTO roleManaged) {
		this.roleManaged = roleManaged;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public int getInheritId() {
		return inheritId;
	}

	public void setInheritId(int inheritId) {
		this.inheritId = inheritId;
	}

	public String getInheritStaffShopId() {
		return inheritStaffShopId;
	}

	public void setInheritStaffShopId(String inheritStaffShopId) {
		this.inheritStaffShopId = inheritStaffShopId;
	}

	public String getInheritShopId() {
		return inheritShopId;
	}

	public void setInheritShopId(String inheritShopId) {
		this.inheritShopId = inheritShopId;
	}

	public String getInheritShopCode() {
		return inheritShopCode;
	}

	public void setInheritShopCode(String inheritShopCode) {
		this.inheritShopCode = inheritShopCode;
	}

	public String getInheritShopIdProfile() {
		return inheritShopIdProfile;
	}

	public void setInheritShopIdProfile(String inheritShopIdProfile) {
		this.inheritShopIdProfile = inheritShopIdProfile;
	}

	public double getInheritShopLat() {
		return inheritShopLat;
	}

	public void setInheritShopLat(double inheritShopLat) {
		this.inheritShopLat = inheritShopLat;
	}

	public double getInheritShopLng() {
		return inheritShopLng;
	}

	public void setInheritShopLng(double inheritShopLng) {
		this.inheritShopLng = inheritShopLng;
	}

	public int getInheritSpecificType() {
		return inheritSpecificType;
	}

	public void setInheritSpecificType(int inheritSpecificType) {
		this.inheritSpecificType = inheritSpecificType;
	}

	public String getInheritSaleTypeCode() {
		return inheritSaleTypeCode;
	}

	public void setInheritSaleTypeCode(String inheritSaleTypeCode) {
		this.inheritSaleTypeCode = inheritSaleTypeCode;
	}

	public int getEnableClientLog() {
		return enableClientLog;
	}

	public void setEnableClientLog(int enableClientLog) {
		this.enableClientLog = enableClientLog;
	}

	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public static boolean isNeedResetPassword() {
		return needResetPassword;
	}

	public static void setNeedResetPassword(boolean needResetPassword) {
		UserDTO.needResetPassword = needResetPassword;
	}
}
