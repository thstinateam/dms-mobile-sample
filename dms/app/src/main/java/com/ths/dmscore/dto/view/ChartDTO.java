package com.ths.dmscore.dto.view;


/**
 * dto cho man hinh chart
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class ChartDTO {

	public static final int TYPE_PLAN_QUANTITY = 0;
	public static final int TYPE_PLAN= 1;
	// ten cot
	public String name = "";
	// value cot
	public double[] value;
	// 0: ke hoach, 1 : thuc hien, 2 : percent
	public int type = 2;
}
