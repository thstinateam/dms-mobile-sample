/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.content.ContentValues;

import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_CUSTOMER_MAP;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_MAP_DELTA_TABLE;
import com.ths.dmscore.lib.sqllite.db.PROMOTION_SHOP_MAP;
import com.ths.dmscore.lib.sqllite.db.SALE_ORDER_PROMOTION_TABLE;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;
import com.ths.dmscore.util.GlobalUtil;

/**
 *  Thong tin so suat
 *  @author: duongdt
 *  @version: 1.0
 *  @since: 1.0
 */
@SuppressWarnings("serial")
public class QuantityRevicevedDTO extends AbstractTableDTO {
	public static final int PROMOTION_MAP_DELTA_ACTION_INSERT = 1;
	public static final int PROMOTION_MAP_DELTA_ACTION_UPDATE = 2;
	public static final int PROMOTION_MAP_DELTA_ACTION_REJECT = 3;
	public static final int PROMOTION_MAP_DELTA_ACTION_CANCEL = 4;
	
	public static final int PROMOTION_MAP_DELTA_SOURCE_WEB = 1;
	public static final int PROMOTION_MAP_DELTA_SOURCE_MOBILE = 2;
	private long proId;
	private String proCode = "";
	private String proName = "";
	private long staffMapId = -1;
	private long customerMapId = -1;
	private long shopMapID = -1;
	private int quantityReviceved = 0;
	private int quantityRevicevedMax = 0;
	private int numReceived = 0;
	private double amountReceived = 0;
	private int quantityRevicevedTotal = 0;
	private int numReceivedTotal = 0;
	private double amountReceivedTotal = 0;
	//Loai KM: so suat, so luong, so tien
	private int promotionType;
	//Doi tuong phan bo: shop, staff, customer
	private int promotionCheck;
	
	private long promotionMapDeltaId;
	//Nguon tac dong: 1: Web, 2: Mobile
	private int source;
	//Tac dong: 1: Tao, 2: chinh, 3: tu choi, 4: Huy
	private int action;
	private long fromObjectId;
	private String createDate;
	private String createUser;
	
	private long shopId;
	private long staffId;
	private long customerId;
	
	private int quantityRevicevedMin;
	private int numRevicevedMin;
	private double amountRevicevedMin;

	/**
	 * @param type
	 * @param proId
	 * @param promotionProgramCode
	 * @param staffQuantity
	 * @param customerQuantity
	 * @param shopQuantity
	 */
	public QuantityRevicevedDTO(long proId, String promotionProgramCode) {
		this.setProId(proId);
		this.setProCode(promotionProgramCode);
	}

	/**
	 * Tang so suat cua nv, kh, npp khi tao don vansale
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:16:56 24 Sep 2014
	 * @return: void
	 * @throws:
	 * @param listSql
	 */
	public void generateInscreasePromotionSqlVansale(JSONArray listSql) {
		try {
			if(this.getShopMapID() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						PROMOTION_SHOP_MAP.TABLE_PROMOTION_SHOP_MAP);

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						PROMOTION_SHOP_MAP.QUANTITY_RECEIVED, "*+" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						PROMOTION_SHOP_MAP.PROMOTION_SHOP_MAP_ID,
						this.getShopMapID(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}

			if(this.getCustomerMapId() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*+" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
						this.getCustomerMapId(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}

			if(this.getStaffMapId() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						"PROMOTION_STAFF_MAP");

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						"QUANTITY_RECEIVED", "*+" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						"PROMOTION_STAFF_MAP_ID",
						this.getStaffMapId(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}
		} catch (JSONException e) {
		}
	}

	/**
	 * Giam so suat cua nv, kh, npp khi tao don tra vansale
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:16:56 24 Sep 2014
	 * @return: void
	 * @throws:
	 * @param listSql
	 */
	public void generateDescreasePromotionSqlVansale(JSONArray listSql) {
		try {
			if(this.getShopMapID() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						PROMOTION_SHOP_MAP.TABLE_PROMOTION_SHOP_MAP);

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						PROMOTION_SHOP_MAP.QUANTITY_RECEIVED, "*-" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						PROMOTION_SHOP_MAP.PROMOTION_SHOP_MAP_ID,
						this.getShopMapID(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}

			if(this.getCustomerMapId() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						PROMOTION_CUSTOMER_MAP.TABLE_PROMOTION_CUSTOMER_MAP);

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						PROMOTION_CUSTOMER_MAP.QUANTITY_RECEIVED, "*-" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						PROMOTION_CUSTOMER_MAP.PROMOTION_CUSTOMER_MAP_ID,
						this.getCustomerMapId(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}

			if(this.getStaffMapId() > 0){
				JSONObject json = new JSONObject();
				json.put(IntentConstants.INTENT_TYPE, TableAction.UPDATE);
				json.put(IntentConstants.INTENT_TABLE_NAME,
						"PROMOTION_STAFF_MAP");

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(
						"QUANTITY_RECEIVED", "*-" + getQuantityReviceved(),
						DATA_TYPE.OPERATION.toString()));
				json.put(IntentConstants.INTENT_LIST_PARAM, params);

				JSONArray wheres = new JSONArray();
				wheres.put(GlobalUtil.getJsonColumn(
						"PROMOTION_STAFF_MAP_ID",
						this.getStaffMapId(), null));
				json.put(IntentConstants.INTENT_LIST_WHERE_PARAM, wheres);
				listSql.put(json);
			}
		} catch (JSONException e) {
		}
	}
	
	/**
	 * init insert order sale promotion
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 19:13:13 15 Sep 2014
	 * @return: ContentValues
	 * @throws:
	 * @param this
	 * @return
	 */
	public ContentValues initInsertPromotionMapDelta(){
		ContentValues values = new ContentValues();
		values.put(PROMOTION_MAP_DELTA_TABLE.PROMOTION_MAP_DELTA_ID, getPromotionMapDeltaId());
		if(getShopMapID() > 0) {
			values.put(PROMOTION_MAP_DELTA_TABLE.PROMOTION_SHOP_MAP_ID, getShopMapID());
		}
		
		if(getStaffMapId() > 0) {
			values.put(PROMOTION_MAP_DELTA_TABLE.PROMOTION_STAFF_MAP_ID, getStaffMapId());
		}
		
		if(getCustomerMapId() > 0) {
			values.put(PROMOTION_MAP_DELTA_TABLE.PROMOTION_CUSTOMER_MAP_ID, getCustomerMapId());
		}
		
		values.put(PROMOTION_MAP_DELTA_TABLE.QUANTITY_DELTA, getQuantityReviceved());
		values.put(PROMOTION_MAP_DELTA_TABLE.AMOUNT_DELTA, getAmountReceived());
		values.put(PROMOTION_MAP_DELTA_TABLE.NUM_DELTA, getNumReceived());
		
		values.put(PROMOTION_MAP_DELTA_TABLE.SOURCE, getSource());
		values.put(PROMOTION_MAP_DELTA_TABLE.ACTION, getOrderAction());
		values.put(PROMOTION_MAP_DELTA_TABLE.FROM_OBJECT_ID, getFromObjectId());
		
		if (!StringUtil.isNullOrEmpty(this.getCreateUser())) {
			values.put(SALE_ORDER_PROMOTION_TABLE.CREATE_USER, this.getCreateUser());
		}

		if (!StringUtil.isNullOrEmpty(this.getCreateDate())) {
			values.put(SALE_ORDER_PROMOTION_TABLE.CREATE_DATE, this.getCreateDate());
		}
		
		values.put(PROMOTION_MAP_DELTA_TABLE.PROMOTION_PROGRAM_ID, getProId());
		values.put(PROMOTION_MAP_DELTA_TABLE.SHOP_ID, getShopId());
		values.put(PROMOTION_MAP_DELTA_TABLE.STAFF_ID, getStaffId());
		values.put(PROMOTION_MAP_DELTA_TABLE.CUSTOMER_ID, getCustomerId());
		
		return values;
	}
	
	/**
	 * Tang so suat cua nv, kh, npp khi tao don vansale
	 * @author: duongdt3
	 * @since: 1.0
	 * @time: 15:16:56 24 Sep 2014
	 * @return: void
	 * @throws:
	 * @param listSql
	 */
	public void generateInsertPromotionMapDelta(JSONArray listSql) {
		try {
			JSONObject orderJson = new JSONObject();

				orderJson.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
				orderJson.put(IntentConstants.INTENT_TABLE_NAME, PROMOTION_MAP_DELTA_TABLE.TABLE_NAME);

				// ds params
				JSONArray params = new JSONArray();
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.PROMOTION_MAP_DELTA_ID, getPromotionMapDeltaId(), null));
				if(getShopMapID() > 0) {
					params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.PROMOTION_SHOP_MAP_ID, getShopMapID(), null));
				}
				
				if(getStaffMapId() > 0) {
					params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.PROMOTION_STAFF_MAP_ID, getStaffMapId(), null));
				}
				
				if(getCustomerMapId() > 0) {
					params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.PROMOTION_CUSTOMER_MAP_ID, getCustomerMapId(), null));
				}
				
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.QUANTITY_DELTA, getQuantityReviceved(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.NUM_DELTA, getNumReceived(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.AMOUNT_DELTA, getAmountReceived(), null));
				
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.SOURCE, getSource(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.ACTION, getOrderAction(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.FROM_OBJECT_ID, getFromObjectId(), null));
				
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.CREATE_USER, getCreateUser(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.CREATE_DATE, getCreateDate(), null));
				
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.PROMOTION_PROGRAM_ID, getProId(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.SHOP_ID, getShopId(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.STAFF_ID, getStaffId(), null));
				params.put(GlobalUtil.getJsonColumn(PROMOTION_MAP_DELTA_TABLE.CUSTOMER_ID, getCustomerId(), null));
				
				orderJson.put(IntentConstants.INTENT_LIST_PARAM, params);
				
				listSql.put(orderJson);
		} catch (JSONException e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	public long getProId() {
		return proId;
	}

	public void setProId(long proId) {
		this.proId = proId;
	}

	public String getProCode() {
		return proCode;
	}

	public void setProCode(String proCode) {
		this.proCode = proCode;
	}

	public String getProName() {
		return proName;
	}

	public void setProName(String proName) {
		this.proName = proName;
	}

	public long getStaffMapId() {
		return staffMapId;
	}

	public void setStaffMapId(long staffMapId) {
		this.staffMapId = staffMapId;
	}

	public long getCustomerMapId() {
		return customerMapId;
	}

	public void setCustomerMapId(long customerMapId) {
		this.customerMapId = customerMapId;
	}

	public long getShopMapID() {
		return shopMapID;
	}

	public void setShopMapID(long shopMapID) {
		this.shopMapID = shopMapID;
	}

	public int getQuantityReviceved() {
		return quantityReviceved;
	}

	public void setQuantityReviceved(int quantityReviceved) {
		this.quantityReviceved = quantityReviceved;
	}

	public int getQuantityRevicevedMax() {
		return quantityRevicevedMax;
	}

	public void setQuantityRevicevedMax(int quantityRevicevedMax) {
		this.quantityRevicevedMax = quantityRevicevedMax;
	}

	public int getNumReceived() {
		return numReceived;
	}

	public void setNumReceived(int numReceived) {
		this.numReceived = numReceived;
	}

	public double getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(double amountReceived) {
		this.amountReceived = amountReceived;
	}

	public int getQuantityRevicevedTotal() {
		return quantityRevicevedTotal;
	}

	public void setQuantityRevicevedTotal(int quantityRevicevedTotal) {
		this.quantityRevicevedTotal = quantityRevicevedTotal;
	}

	public int getNumReceivedTotal() {
		return numReceivedTotal;
	}

	public void setNumReceivedTotal(int numReceivedTotal) {
		this.numReceivedTotal = numReceivedTotal;
	}

	public double getAmountReceivedTotal() {
		return amountReceivedTotal;
	}

	public void setAmountReceivedTotal(double amountReceivedTotal) {
		this.amountReceivedTotal = amountReceivedTotal;
	}

	public int getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(int promotionType) {
		this.promotionType = promotionType;
	}

	public int getPromotionCheck() {
		return promotionCheck;
	}

	public void setPromotionCheck(int promotionCheck) {
		this.promotionCheck = promotionCheck;
	}

	public long getPromotionMapDeltaId() {
		return promotionMapDeltaId;
	}

	public void setPromotionMapDeltaId(long promotionMapDeltaId) {
		this.promotionMapDeltaId = promotionMapDeltaId;
	}

	public int getSource() {
		return source;
	}

	public void setSource(int source) {
		this.source = source;
	}

	public int getOrderAction() {
		return action;
	}

	public void setOrderAction(int action) {
		this.action = action;
	}

	public long getFromObjectId() {
		return fromObjectId;
	}

	public void setFromObjectId(long fromObjectId) {
		this.fromObjectId = fromObjectId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public long getShopId() {
		return shopId;
	}

	public void setShopId(long shopId) {
		this.shopId = shopId;
	}

	public long getStaffId() {
		return staffId;
	}

	public void setStaffId(long staffId) {
		this.staffId = staffId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public int getQuantityRevicevedMin() {
		return quantityRevicevedMin;
	}

	public void setQuantityRevicevedMin(int quantityRevicevedMin) {
		this.quantityRevicevedMin = quantityRevicevedMin;
	}

	public int getNumRevicevedMin() {
		return numRevicevedMin;
	}

	public void setNumRevicevedMin(int numRevicevedMin) {
		this.numRevicevedMin = numRevicevedMin;
	}

	public double getAmountRevicevedMin() {
		return amountRevicevedMin;
	}

	public void setAmountRevicevedMin(double amountRevicevedMin) {
		this.amountRevicevedMin = amountRevicevedMin;
	}
}
