/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import com.ths.dmscore.lib.json.me.JSONArray;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.lib.sqllite.db.EQUIP_ATTACH_FILE_TABLE;
import com.ths.dmscore.lib.sqllite.db.MEDIA_ITEM_TABLE;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 * DTO luu file hinh anh kiem ke thiet bi
 * EquipAttachFileDTO.java
 * @author: hoanpd1
 * @version: 1.0 
 * @since:  19:47:03 19-12-2014
 */
public class EquipAttachFileDTO extends AbstractTableDTO {
	private static final long serialVersionUID = 1L;
	
	// hinh anh kiem ke thiet bi
	public static final int TYPE_INVENTORY_DEVICE_IMAGE = 7;
	// ma media
	public long idEquipAttachFile;
	// loai bien ban
	public int objectType;
	// ID cuar phieu tuong ung voi type
	public long objectId;
	// url cua media
	public String url;
	public String thumbUrl;
	// ma media
	public long media_id;
	// ten cua file
	public String fileName;
	// ngay tao
	public String createDate;
	// ngay cap nhat
	public String updateDate;
	// nguoi tao
	public String createUser;
	// nguoi cap nhat
	public String updateUser;
	// vi tri lat
	public double lat;
	// vi tri lng
	public double lng;
	// staff id
	public long staffId;
	// duong dan luu duoi sd card
	public String sdCardPath = "";

	public EquipAttachFileDTO() {
		super(TableType.EQUIP_ATTACH_FILE_TABLE);
	}

	/**
	 *  Tao cau sql de insert media item
	 * @author: hoanpd1
	 * @since: 20:11:12 19-12-2014
	 * @return: JSONArray
	 * @throws:  
	 * @return
	 */
	public JSONArray generateInsertMedia() {
		JSONArray result = new JSONArray();
		JSONObject jsonInsert = new JSONObject();
		try {
			// UPDATE media item
			jsonInsert.put(IntentConstants.INTENT_TYPE, TableAction.INSERT);
			jsonInsert.put(IntentConstants.INTENT_TABLE_NAME, EQUIP_ATTACH_FILE_TABLE.TABLE_NAME);
			// ds params
			JSONArray params = new JSONArray();
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.EQUIP_ATTACH_FILE_ID, idEquipAttachFile, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.OBJECT_TYPE, objectType, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.OBJECT_ID, objectId, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.URL, url, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.FILE_NAME, fileName, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.LAT, lat, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.LNG, lng, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.CREATE_USER, createUser, null));
			params.put(GlobalUtil.getJsonColumn(EQUIP_ATTACH_FILE_TABLE.CREATE_DATE, createDate, null));

			if (!StringUtil.isNullOrEmpty(updateDate)) {
				params.put(GlobalUtil.getJsonColumn(MEDIA_ITEM_TABLE.UPDATE_DATE, updateDate, null));
			}
			if (!StringUtil.isNullOrEmpty(updateUser)) {
				params.put(GlobalUtil.getJsonColumn(MEDIA_ITEM_TABLE.UPDATE_USER, updateUser, null));
			}
			// syn status
			jsonInsert.put(IntentConstants.INTENT_LIST_PARAM, params);
			result.put(jsonInsert);
		} catch (JSONException e) {
			// TODO: handle exception
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return result;
	}

}
