package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import com.ths.dmscore.dto.me.photo.PhotoDTO;

/**
 * Hinh anh cua mot key shopkey shop
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class VoteKeyShopImageViewDTO {
	//ds hinh anh
	public ArrayList<PhotoDTO> lstImage = new ArrayList<PhotoDTO>();
	public int totalImage = 0; // so luong hinh anh cho moi CT
}
