package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.HashMap;

import net.sqlcipher.database.SQLiteDatabase;

import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.PriDto;
import com.ths.dmscore.util.StringUtil;

/**
 * bang phan quyen
 *
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */
public class PERMISSION_TABLE extends ABSTRACT_TABLE {

	public static final String PERMISSION_ID = "PERMISSION_ID";
	public static final String PERMISSION_NAME = "PERMISSION_NAME";
	public static final String PERMISSION_NAME_TEXT = "PERMISSION_NAME_TEXT";
	public static final String PERMISSION_CODE = "PERMISSION_CODE";
	public static final String APP_ID = "APP_ID";
	public static final String PERMISSION_TYPE = "PERMISSION_TYPE";
	public static final String STATUS = "STATUS";
	public static final String DESCRIPTION = "DESCRIPTION";
	public static final String IS_FULL_PRIVILEGE = "IS_FULL_PRIVILEGE";
	public static final String CREATE_DATE = "CREATE_DATE";
	public static final String CREATE_USER = "CREATE_USER";
	public static final String UPDATE_DATE = "UPDATE_DATE";
	public static final String UPDATE_USER = "UPDATE_USER";
	public static final String SYN_STATE = "SYN_STATE";

	public static final String TABLE_NAME = "PERMISSION";

	public PERMISSION_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_NAME;
		this.columns = new String[] { PERMISSION_ID, PERMISSION_NAME,
				PERMISSION_NAME_TEXT, PERMISSION_CODE, APP_ID, PERMISSION_TYPE,
				STATUS, DESCRIPTION, IS_FULL_PRIVILEGE, CREATE_DATE,
				CREATE_USER, UPDATE_DATE, UPDATE_USER };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;
	}

	@Override
	protected long insert(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long update(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected long delete(AbstractTableDTO dto) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * kiem tra role cua user dang nhap co full quyen khong
	 *
	 * @author: DungNX
	 * @return
	 * @return: boolean
	 * @throws Exception
	 * @throws:
	 */
	public boolean getIsFullPrivilege() throws Exception {
		boolean result = false;
		Cursor c = null;
		try {
			String staffId = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritId());
			String roleId = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();

			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    p.IS_FULL_PRIVILEGE	IS_FULL_PRIVILEGE");
			sqlObject.append("	FROM	");
			sqlObject.append("	    (SELECT	");
			sqlObject.append("	        RU.role_user_id,	");
			sqlObject.append("	        RU.role_id	");
			sqlObject.append("	    FROM	");
			sqlObject.append("	        role_user RU	");
			sqlObject.append("	    WHERE	");
			sqlObject.append("	        RU.user_id = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	        AND RU.status = 1) RU	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role R	");
			sqlObject.append("	        ON R.role_id = RU.role_id	");
			sqlObject.append("	        AND r.status = 1	");
			if (!StringUtil.isNullOrEmpty(roleId)) {
				sqlObject.append("	        AND r.role_id = ?	");
				paramsObject.add(roleId);
			}
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role_permission_map RPM	");
			sqlObject.append("	        ON RPM.role_id = R.role_id	");
			sqlObject.append("	        AND RPM.status = 1	");
			sqlObject.append("	        AND r.app_id = RPM.app_id	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    permission P	");
			sqlObject
					.append("	        ON P.permission_id = RPM.permission_id	");
			sqlObject.append("	        AND r.app_id =  p.app_id	");
			sqlObject.append("	        AND p.status = 1	");
			sqlObject.append("	        AND p.PERMISSION_TYPE = 1	");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					result = c.getInt(c.getColumnIndex("IS_FULL_PRIVILEGE")) == 1;
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getIsFullPrivilege", e.toString());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return result;
	}

	/**
	 * lay thong tin quyen
	 *
	 * @author: DungNX
	 * @param formCode
	 * @return
	 * @return: HashMap<Integer,PriDto>
	 * @throws Exception
	 * @throws:
	 */
	public HashMap<String, PriDto> getAppPrivilege(String formCode) throws Exception {
		Cursor c = null;
		HashMap<String, PriDto> priHashMap = new HashMap<String, PriDto>();
		try {
			String staffId = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritId());
			String roleId = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();

			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    RU.role_user_id,	");
			sqlObject.append("	    RPM.permission_id,	");
			sqlObject.append("	    P.permission_id,	");
			sqlObject.append("	    F.FORM_CODE,	");
			sqlObject.append("	    C.CONTROL_CODE AS CODE,	");
			sqlObject.append("	    F.form_id,	");
			sqlObject.append("	    FA.SHOW_STATUS AS STATUS	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    (SELECT	");
			sqlObject.append("	        RU.role_user_id,	");
			sqlObject.append("	        RU.role_id	");
			sqlObject.append("	    FROM	");
			sqlObject.append("	        role_user RU	");
			sqlObject.append("	    WHERE	");
			sqlObject.append("	        RU.user_id = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	        AND RU.status = 1) RU	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role R	");
			sqlObject.append("	        ON R.role_id = RU.role_id	");
			sqlObject.append("	        AND r.status = 1	");
			if (!StringUtil.isNullOrEmpty(roleId)) {
				sqlObject.append("	        AND r.role_id = ?	");
				paramsObject.add(roleId);
			}
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role_permission_map RPM	");
			sqlObject.append("	        ON RPM.role_id = R.role_id	");
			sqlObject.append("	        AND RPM.status = 1	");
			sqlObject.append("	        AND r.app_id = RPM.app_id	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    permission P	");
			sqlObject.append("	        ON P.permission_id = RPM.permission_id	");
			sqlObject.append("	        AND r.app_id =  p.app_id	");
			sqlObject.append("	        AND p.status = 1	");
			sqlObject.append("	        AND p.PERMISSION_TYPE = 1	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    function_access FA	");
			sqlObject.append("	        ON FA.permission_id = P.permission_id	");
			sqlObject.append("	        AND r.app_id =  fa.app_id	");
			sqlObject.append("	        AND FA.status = 1	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    form F	");
			sqlObject.append("	        ON F.app_id = r.app_id	");
			sqlObject.append("	        AND F.form_id = fa.form_id	");
			sqlObject.append("	        AND F.status = 1	");
			sqlObject.append("	        AND F.subsystem = 2	");
			sqlObject.append("	        AND F.FORM_CODE = ?	");
			paramsObject.add(formCode);
			sqlObject.append("	JOIN	");
			sqlObject.append("	    control C	");
			sqlObject.append("	        ON C.form_id = F.form_id	");
			sqlObject.append("	        AND fa.control_id = c.control_id	");
			sqlObject.append("	        AND C.status = 1	");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						String controlCode = c.getString(c
								.getColumnIndex("CODE"));
						int status = Integer.valueOf(c.getInt(c
								.getColumnIndex("STATUS")));
						PriDto priDto = new PriDto(null, status);
						priHashMap.put(controlCode, priDto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getAppPrivilege", e.getMessage());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return priHashMap;
	}

	/**
	 * lay trang thai cua form
	 *
	 * @author: DungNX
	 * @param formCode
	 * @return
	 * @return: int
	 * @throws Exception
	 * @throws:
	*/
	public int getFormStatus(String formCode) throws Exception {
		Cursor c = null;
		int status = 0;
		try {
			String staffId = String.valueOf(GlobalInfo.getInstance()
					.getProfile().getUserData().getInheritId());
			String roleId = GlobalInfo.getInstance().getProfile().getUserData().getRoleId();

			StringBuffer sqlObject = new StringBuffer();
			ArrayList<String> paramsObject = new ArrayList<String>();
			sqlObject.append("	SELECT	");
			sqlObject.append("	    RU.role_user_id,	");
			sqlObject.append("	    RPM.permission_id,	");
			sqlObject.append("	    P.permission_id,	");
			sqlObject.append("	    F.FORM_CODE,	");
			sqlObject.append("	    F.form_id,	");
			sqlObject.append("	    F.status STATUS,	");
			sqlObject.append("	    FA.status FA_STATUS	");
			sqlObject.append("	FROM	");
			sqlObject.append("	    (SELECT	");
			sqlObject.append("	        RU.role_user_id,	");
			sqlObject.append("	        RU.role_id	");
			sqlObject.append("	    FROM	");
			sqlObject.append("	        role_user RU	");
			sqlObject.append("	    WHERE	");
			sqlObject.append("	        RU.user_id = ?	");
			paramsObject.add(staffId);
			sqlObject.append("	        AND RU.status = 1) RU	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role R	");
			sqlObject.append("	        ON R.role_id = RU.role_id	");
			sqlObject.append("	        AND r.status = 1	");
			if (!StringUtil.isNullOrEmpty(roleId)) {
				sqlObject.append("	        AND r.role_id = ?	");
				paramsObject.add(roleId);
			}
			sqlObject.append("	JOIN	");
			sqlObject.append("	    role_permission_map RPM	");
			sqlObject.append("	        ON RPM.role_id = R.role_id	");
			sqlObject.append("	        AND RPM.status = 1	");
			sqlObject.append("	        AND r.app_id = RPM.app_id	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    permission P	");
			sqlObject.append("	        ON P.permission_id = RPM.permission_id	");
			sqlObject.append("	        AND r.app_id =  p.app_id	");
			sqlObject.append("	        AND p.status = 1	");
			sqlObject.append("	        AND p.PERMISSION_TYPE = 1	");
			sqlObject.append("	JOIN	");
			sqlObject.append("	    form F	");
			sqlObject.append("	        ON F.app_id = r.app_id	");
			sqlObject.append("	        AND F.status = 1	");
			sqlObject.append("	        AND F.subsystem = 2	");
			sqlObject.append("	        AND F.FORM_CODE = ?	");
			paramsObject.add(formCode);
			sqlObject.append("	JOIN	");
			sqlObject.append("	    FUNCTION_ACCESS FA 	");
			sqlObject.append("	        ON F.FORM_ID = FA.FORM_ID ");
			sqlObject.append("	        and FA.PERMISSION_ID = P.permission_id	");
			sqlObject.append("	        and FA.control_id is null ");

			c = rawQueries(sqlObject.toString(), paramsObject);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						int fStatus = Integer.valueOf(c.getInt(c
								.getColumnIndex("STATUS")));
						int faStatus = Integer.valueOf(c.getInt(c
								.getColumnIndex("FA_STATUS")));
						if (fStatus == 1 && faStatus == 1)
							status = 1;
						else status = 0;
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.i("Pri-getFormStatus", e.getMessage());
			throw e;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return status;
	}
}
