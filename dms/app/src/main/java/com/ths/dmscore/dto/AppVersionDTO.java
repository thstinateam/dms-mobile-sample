/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto;

import java.io.Serializable;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.json.me.JSONObject;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.lib.json.me.JSONException;
import com.ths.dmscore.util.VNMTraceUnexceptionLog;

/**
 *  Luu tru thong tin version app
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class AppVersionDTO implements Serializable{	
	private static final long serialVersionUID = 4104327890311337639L;
	public String id = Constants.STR_BLANK;
	public String downloadLink = Constants.STR_BLANK;
	//bat buoc force download ?
	public boolean forceDownload = true;
	//can thiet resetDB hay khong
	public boolean needResetDB = false;
	public String version = Constants.STR_BLANK;
	public String content = Constants.STR_BLANK;
	public String getDate = Constants.STR_BLANK;
	
	public void parseFromJson(JSONObject jsonObject){
		try {					
			id = jsonObject.getString("id");
			downloadLink = jsonObject.getString("downloadLink");
			int force = jsonObject.getInt("forceDownload");
			if(force == 1){
				forceDownload = true;
			}else{
				forceDownload = false;
			}
			int resetDB = jsonObject.getInt("needResetDB");
			if(resetDB == 1){
				needResetDB = true;
			}else{
				needResetDB = false;
			}
			getDate = jsonObject.getString("getDate");	
			version = jsonObject.getString("version");	
			content = jsonObject.getString("content"); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}		
	}
}
