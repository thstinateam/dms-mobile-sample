package com.ths.dmscore.view.sale.customer;

import java.text.DecimalFormat;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ths.dmscore.global.ActionEventConstant;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.util.DateUtils;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.PriHashMap;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.dto.view.CustomerListItem.VISIT_STATUS;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.PriUtils;
import com.ths.dms.R;

public class CustomerListRow extends DMSTableRow implements OnClickListener {
	TextView tvNum;
	public TextView tvCusCode;
	TextView tvCusName;
	// TextView tvMobilePhone;
	// TextView tvHomePhone;
	TextView tvAddress;
	TextView tvPath;
	TextView tvDistance;
	TextView tvVisit;
	LinearLayout llvisitCus;
	public ImageView ivVisitCus;
	CustomerListItem item;
	public CustomerListRow(Context context, CustomerListView lis) {
		super(context, R.layout.layout_customer_list_row);
		setListener(lis);
		tvNum = (TextView)findViewById(R.id.tvNum);
//		tvCusCode = (TextView) view.findViewById(R.id.tvCusCode);
		tvCusCode = (TextView) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.tvCusCode,
				PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_CTKH
						);
		tvCusName = (TextView) findViewById(R.id.tvCusName);
		// tvMobilePhone = (TextView) view.findViewById(R.id.tvMobilePhone);
		// tvHomePhone = (TextView) view.findViewById(R.id.tvHomePhone);
		tvAddress = (TextView)findViewById(R.id.tvAdd);
		tvPath = (TextView) findViewById(R.id.tvPath);
		tvDistance = (TextView) findViewById(R.id.tvDistance);
		tvVisit = (TextView) findViewById(R.id.tvVisit);
//		ivVisitCus = (ImageView) view.findViewById(R.id.visitCus);
		ivVisitCus = (ImageView) PriUtils.getInstance().findViewById(this, R.id.visitCus,
				PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_GHETHAM);
//		llvisitCus = (LinearLayout) view.findViewById(R.id.llvisitCus);
		llvisitCus = (LinearLayout) PriUtils.getInstance().findViewByIdNotAllowInvisible(this, R.id.llvisitCus,
				PriHashMap.PriControl.NVBH_DANHSACHKHACHHANG_GHETHAM
						);

		tvCusCode.setOnClickListener(this);
		llvisitCus.setOnClickListener(this);
//		PriUtils.getInstance().setOnClickListener(tvCusCode, this);
//		PriUtils.getInstance().setOnClickListener(llvisitCus, this);
	}

	public void render(int pos, CustomerListItem item, int curDay, GlobalInfo.VistConfig visitConfig) {
		this.item = item;
		tvNum.setText("" + pos);
		tvCusCode.setText(item.aCustomer.getCustomerCode());
		tvCusName.setText(item.aCustomer.getCustomerName());
		// tvMobilePhone.setText(item.aCustomer.getMobilephone());
		// tvHomePhone.setText(item.aCustomer.getPhone());
		tvAddress.setText(item.aCustomer.getStreet());
		tvPath.setText(item.cusPlan);

		renderVisitStatus(visitConfig);

		if (item.isOr == 0 && (DateUtils.getCurrentDay() == curDay)) {
			tvVisit.setText(item.seqInDayPlan);
		} else {
			tvVisit.setText("");
		}
	}

	private void renderVisitStatus(GlobalInfo.VistConfig visitConfig) {
		PriUtils.getInstance().setStatus(ivVisitCus, PriUtils.ENABLE);
		PriUtils.getInstance().setStatus(llvisitCus, PriUtils.ENABLE);
		
		if (item.cusDistance >= 1000) {
			double tempDistance = (double) item.cusDistance / 1000;
			DecimalFormat df = new DecimalFormat("0.00");
			String formater = df.format(tempDistance);
			tvDistance.setText("" + formater + " km");
		} else if (item.cusDistance >= 0) {
			tvDistance.setText("" + item.cusDistance + " m");
		} else {
			tvDistance.setText("");
		}

		if (item.isVisit()) {
			ivVisitCus.setImageResource(R.drawable.icon_shop_checked);
		} else {
			ivVisitCus.setImageResource(R.drawable.icon_shop);
		}

		if (item.visitStatus == VISIT_STATUS.VISITED_CLOSED) {
			tvNum.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvCusCode.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvCusName.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvVisit.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvAddress.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvPath.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			tvDistance.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			ivVisitCus.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
			llvisitCus.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_CLOSED));
		} else if (item.isTodayOrdered) {
			tvNum.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvCusCode.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvCusName.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvVisit.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvAddress.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvPath.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			tvDistance.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			ivVisitCus.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
			llvisitCus.setBackgroundColor(ImageUtil.getColor(R.color.COLOR_VISIT_STORE_ORDER_SUCC));
		} else {
			tvNum.setBackgroundResource(R.drawable.style_row_default);
			tvCusCode.setBackgroundResource(R.drawable.style_row_default);
			tvCusName.setBackgroundResource(R.drawable.style_row_default);
			if(GlobalInfo.isTablet){
				tvCusName.setPadding(GlobalUtil.dip2Pixel(10), 0, GlobalUtil.dip2Pixel(10), 0);
			}else{
				tvCusName.setPadding(GlobalUtil.dip2Pixel(0), 0, GlobalUtil.dip2Pixel(0), 0);
			}
			tvVisit.setBackgroundResource(R.drawable.style_row_default);
			if(GlobalInfo.isTablet){
				tvVisit.setPadding(GlobalUtil.dip2Pixel(10), 0, GlobalUtil.dip2Pixel(10), 0);
			}else{
				tvVisit.setPadding(GlobalUtil.dip2Pixel(0), 0, GlobalUtil.dip2Pixel(0), 0);
			}
			tvAddress.setBackgroundResource(R.drawable.style_row_default);
			if(GlobalInfo.isTablet){
				tvAddress.setPadding(GlobalUtil.dip2Pixel(10), 0, GlobalUtil.dip2Pixel(10), 0);
			}else{
				tvAddress.setPadding(GlobalUtil.dip2Pixel(0), 0, GlobalUtil.dip2Pixel(0), 0);
			}
			tvPath.setBackgroundResource(R.drawable.style_row_default);
			if(GlobalInfo.isTablet){
				tvPath.setPadding(GlobalUtil.dip2Pixel(10), 0, GlobalUtil.dip2Pixel(10), 0);
			}else{
				tvPath.setPadding(GlobalUtil.dip2Pixel(0), 0, GlobalUtil.dip2Pixel(0), 0);
			}
			tvDistance.setBackgroundResource(R.drawable.style_row_default);
			ivVisitCus.setBackgroundResource(R.drawable.style_row_default);
			llvisitCus.setBackgroundResource(R.drawable.style_row_default);
		}

		if (item.canOrderException()) {
			PriUtils.getInstance().setStatus(ivVisitCus, PriUtils.ENABLE);
			PriUtils.getInstance().setStatus(llvisitCus, PriUtils.ENABLE);
		} else {
			boolean isShowVisitIcon = (visitConfig != null 
					&& visitConfig.isAllowVisit(item.isTooFarShop, item.isOr, item.isVisit(), item.aCustomer.isHaveLocation()));			
			if (isShowVisitIcon) {
				PriUtils.getInstance().setStatus(ivVisitCus, PriUtils.ENABLE);
				PriUtils.getInstance().setStatus(llvisitCus, PriUtils.ENABLE);
			} else{
				PriUtils.getInstance().setStatus(ivVisitCus, PriUtils.INVISIBLE);
				PriUtils.getInstance().setStatus(llvisitCus, PriUtils.DISABLE);
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v == tvCusCode) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.GO_TO_CUSTOMER_INFO, tvCusCode, item);
		} else if (v == llvisitCus) {
			listener.handleVinamilkTableRowEvent(ActionEventConstant.VISIT_CUSTOMER, llvisitCus, item);
		}else
			super.onClick(v);
	}

	public void reRenderVisitStatus(GlobalInfo.VistConfig visitConfig) {
		if (item != null) {
			item.updateCustomerDistance(item.distanceOrder);
			renderVisitStatus(visitConfig);
		}
	}

}
