/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.view.sale.newcustomer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.widget.Spinner;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerAttributeDetailViewDTO;
import com.ths.dmscore.dto.view.CustomerAttributeViewDTO;
import com.ths.dmscore.dto.view.ObjectAdapterForSpinner;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.ObjectItemForSpinner;
import com.ths.dmscore.dto.view.CustomerAttributeEnumViewDTO;
import com.ths.dmscore.util.SpinnerWithMultipleChoiceAdapter;
import com.ths.dmscore.util.SpinnerWithMultipleChoiceAdapter.OnSpinnerEventListener;
import com.ths.dms.R;

/**
 * class gom 1 text view va 1 spinner Thuoc tinh dong kh
 * 
 * @author: dungdq3
 * @version: 1.0
 * @since: 2:47:43 PM Jan 27, 2015
 */
public class SpinnerMultiChoiceExtraInfoView extends AbstractExtraInfoView implements OnSpinnerEventListener {

	private TextView tv;
	private Spinner sp;
	private CustomerAttributeViewDTO attrDTO;
	private List<Integer> listIndex;

	public SpinnerMultiChoiceExtraInfoView(Context context) {
		super(context, R.layout.layout_spinner_extra_info_view);
		// TODO Auto-generated constructor stub
		tv = (TextView) arrView.get(0);
		sp = (Spinner) arrView.get(1);
	}

	@Override
	public Object getDataFromView() {
		// TODO Auto-generated method stub
		ArrayList<CustomerAttributeDetailViewDTO> listDetail = new ArrayList<CustomerAttributeDetailViewDTO>();
		if (listIndex != null) {
			for (int i = 0, size = listIndex.size(); i < size; i++) {
				CustomerAttributeDetailViewDTO detail = new CustomerAttributeDetailViewDTO();
				int id = attrDTO.getListEnum()
						.get(listIndex.get(i) - 1).enumID;
				detail.attrID = attrDTO.customerAttributeID;
				detail.enumID = id;
				detail.detailID = attrDTO.attributeDetailID;
				listDetail.add(detail);
			}
		}
		return listDetail;
	}

	@Override
	public void renderLayout(CustomerAttributeViewDTO dto, boolean isEdit, String fromView) {
		// TODO Auto-generated method stub
		attrDTO = dto;
		if (dto.mandatory == 1) {
			Class<?> clazz;
			Object fromViewObject = null;
			try {
				clazz = Class.forName(fromView);
				fromViewObject = clazz.newInstance();
			} catch (ClassNotFoundException e) {
				MyLog.e("SpinnerMultiChoiceExtraInfoView", "ClassNotFoundException", e);
			} catch (java.lang.InstantiationException e) {
				MyLog.e("SpinnerMultiChoiceExtraInfoView", "InstantiationException", e);
			} catch (IllegalAccessException e) {
				MyLog.e("SpinnerMultiChoiceExtraInfoView", "IllegalAccessException", e);
			}
			if (fromViewObject != null && fromViewObject instanceof ListCustomerCreatedView) {
				setSymbol("*", dto.name, ImageUtil.getColor(R.color.RED), tv);
			} else {
				tv.setText(dto.name);
			}
		} else if (dto.mandatory == 0) {
			tv.setText(dto.name);
		}
		ObjectAdapterForSpinner adapterForGSNPP = new ObjectAdapterForSpinner() {

			@Override
			public ObjectItemForSpinner castObjectToObjectItem(Object object) {
				// TODO Auto-generated method stub
				CustomerAttributeEnumViewDTO item = (CustomerAttributeEnumViewDTO) object;
				ObjectItemForSpinner objectItem = new ObjectItemForSpinner(
						item, item.value);
				objectItem.isSelected = (item.enumSelected > 0);
				return objectItem;
			}
		};
		List<ObjectItemForSpinner> arrInfo = SpinnerWithMultipleChoiceAdapter
				.getListObjectItemEx(dto.toArray(), adapterForGSNPP);
		final SpinnerWithMultipleChoiceAdapter adapterStaff = new SpinnerWithMultipleChoiceAdapter(con, arrInfo,
				this);
		adapterStaff.l = new SpinnerWithMultipleChoiceAdapter.MyListener() {

			@Override
			public void onSelect() {
				sp.setSelection(0);
			}
		};

		sp.setAdapter(adapterStaff);
		sp.setSelection(0);
		sp.setEnabled(isEdit);
		tag = attrDTO.customerAttributeID;
	}

	@Override
	public void onSpninerBtnOK(List<Integer> listIndex) {
		// TODO Auto-generated method stub
		this.listIndex = listIndex;
	}

	@Override
	public boolean isCheckData() {
		boolean res = (listIndex != null && !listIndex.isEmpty());
		return res;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return attrDTO.name;
	}

}
