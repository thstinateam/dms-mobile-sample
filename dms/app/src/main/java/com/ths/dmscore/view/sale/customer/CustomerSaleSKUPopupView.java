package com.ths.dmscore.view.sale.customer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.table.DMSTableView;
import com.ths.dmscore.view.main.GlobalBaseActivity;
import com.ths.dmscore.dto.view.CustomerSaleSKUDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dms.R;

/**
 * Popup hien thi thong tin doanh so theo nganh hang cua kh
 * @author BANGHN
 * @version 1.0
 */
public class CustomerSaleSKUPopupView extends ScrollView implements
		OnClickListener {
	Context mContext;
	private GlobalBaseActivity parent;
	public View viewLayout;
	private TextView tvTitle;
	private Button btClose;
	List<TableRow> listRows = new ArrayList<TableRow>();
	private DMSTableView tbList;
	CustomerInfoView fragParent;
	public int numInMonth;
	public CustomerSaleSKUPopupView(Context context, ArrayList<CustomerSaleSKUDTO> data, boolean requestShowQuantity
			, int numInMonth) {
		super(context);
		mContext = context;
		parent = (GlobalBaseActivity) context;
		this.numInMonth = numInMonth;
		fragParent = (CustomerInfoView)
				parent.findFragmentByTag(GlobalUtil.getTag(CustomerInfoView.class));
		LayoutInflater inflater = this.parent.getLayoutInflater();
		viewLayout = inflater
				.inflate(R.layout.layout_cus_sale_info_popup_view, null);

		tbList = (DMSTableView) viewLayout
				.findViewById(R.id.tbList);
		btClose = (Button) viewLayout.findViewById(R.id.btClose);
		tvTitle = (TextView) viewLayout.findViewById(R.id.tvTitle);
//		int month =  DateUtils.getMonth(DateUtils.now(DateUtils.DATE_FORMAT_NOW)) + 1;
//		int year =  DateUtils.getYear(DateUtils.now(DateUtils.DATE_FORMAT_NOW));
		tvTitle.setText(StringUtil.getString(R.string.TEXT_SKU_IN_MONTH) + Constants.STR_SPACE + numInMonth);
		btClose.setOnClickListener(this);
		initHeaderTable(requestShowQuantity);
		renderLayout(data, requestShowQuantity);
	}

	private void renderLayout(ArrayList<CustomerSaleSKUDTO> data, boolean requestShowQuantity) {
		if(data != null && data.size() > 0){
			double totalAmount = 0;
			long totalQuantity = 0;
			for(int i = 0; i< data.size(); i++){
				CustomerSaleSKURow row = new CustomerSaleSKURow(mContext);
				row.render(data.get(i), (i+1), requestShowQuantity);
				if (requestShowQuantity) {
					totalQuantity += data.get(i).quantity;
				} else {
					totalAmount += data.get(i).amount;
				}
				tbList.addRow(row);
			}
			CustomerSaleSKURow row = new CustomerSaleSKURow(mContext);
			if(requestShowQuantity)
				row.renderTotal(totalQuantity);
			else{
				row.renderTotal(totalAmount);
			}
			tbList.addRow(row);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == btClose) {
			fragParent.onClick(v);
		}
	}

	 /**
	 * Khoi tao header table
	 * @author: Tuanlt11
	 * @return: void
	 * @throws:
	*/
	private void initHeaderTable(boolean requestShowQuantity){
		CustomerSaleSKURow header = new CustomerSaleSKURow(mContext, true);
		if (requestShowQuantity) {
			header.tvAmount.setText(StringUtil.getString(R.string.TEXT_QUANTITY));
		}
		tbList.addHeader(header);
	}

}
