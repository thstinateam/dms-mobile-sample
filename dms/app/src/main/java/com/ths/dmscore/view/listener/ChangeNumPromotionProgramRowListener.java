package com.ths.dmscore.view.listener;

import android.text.Editable;
import android.widget.EditText;

import com.ths.dmscore.dto.view.OrderDetailViewDTO;

/**
 * Mo ta muc dich cua class
 * @author: DungNX
 * @version: 1.0
 * @since: 1.0
 */

public interface ChangeNumPromotionProgramRowListener {
	void onTextItemChange(EditText edNumberPromotion, Editable arg0, OrderDetailViewDTO myData);

}
