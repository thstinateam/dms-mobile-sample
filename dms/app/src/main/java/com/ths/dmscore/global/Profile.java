/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.global;

import java.io.Serializable;

import android.content.SharedPreferences;

import com.ths.dmscore.dto.GPSInfo;
import com.ths.dmscore.dto.db.ActionLogDTO;
import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.UserDTO;
import com.ths.dmscore.dto.view.CustomerListItem;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.main.LoginView;

/**
 *  Luu tru thong tin user dang nhap
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class Profile implements Serializable {
	private static final long serialVersionUID = -5226771436632902463L;
	public static final String NONE = "NONE";
	public static final String UPDATEDB = "UPDATEDB";
	public static final String RESETDB = "RESETDB";
	private String actionLogin = NONE;

	public static final String VNM_PROFILE = "vinamilkProfile";
	private String softVersion = Constants.STR_BLANK;
	private String dbVersion = Constants.STR_BLANK;
	// revision cua thu muc cache hinh anh
	private String server_revision = "";
	// Luu chung thuc dang nhap
	private String authData = "";
	private UserDTO userData = new UserDTO();

	private GPSInfo myGPSInfo = new GPSInfo();
	//is mode debug or not
	private boolean isDebugMode = true;

	//luu cac gia tri ghe tham (chi dung cho chuc nang ket thuc || dong cua)
	private ActionLogDTO actionLogVisitCustomer;
	//Thong tin KH vao ghe tham lan gan nhat
	private CustomerListItem lastVisitCustomer;
	public void setUserData(UserDTO userData) {
		this.userData = userData;
		save();
	}

	public UserDTO getUserData() {
		return userData;
	}

	/**
	 * set action sau khi login
	 * @author : BangHN
	 * since : 10:17:33 AM
	 */
	public void setActionLogin(String action){
		this.actionLogin = action;
		save();
	}

	/**
	 * get action sau khi login: action voi db
	 * @author : BangHN
	 * since : 10:17:20 AM
	 */
	public String getActionLogin(){
		return actionLogin;
	}

	/**
	 * @return the isDebugMode
	 */
	public boolean isDebugMode() {
		return isDebugMode;
	}

	public void setActionLogVisitCustomer(ActionLogDTO action){
		this.actionLogVisitCustomer = action;
		save();
	}

	public ActionLogDTO getActionLogVisitCustomer(){
		return actionLogVisitCustomer;
	}


	public void setLastVisitCustomer(CustomerListItem customer){
		this.lastVisitCustomer = customer;
		save();
	}

	public CustomerListItem getLastVisitCustomer(){
		return lastVisitCustomer;
	}

	/**
	 * @param isDebugMode the isDebugMode to set
	 */
	public void setDebugMode(boolean isDebugMode) {
		this.isDebugMode = isDebugMode;
		save();
	}

	/**
	 * set chuoi chung thuc de dang nhap lan sau
	 *
	 * @author: BangHN
	 */
	public void setAuthData(String auth) {
		this.authData = auth;
		save();
	}

	/**
	 * Tra ve chuoi chung thuc dang nhap
	 *
	 * @author: BangHN
	 */
	public String getAuthData() {
		return authData;
	}

	/**
	 * set chuoi server revision
	 *
	 * @author: BangHN
	 */
	public void setServerRevision(String revision) {
		this.server_revision = revision;
		save();
	}

	/**
	 * Tra ve chuoi server revision
	 *
	 * @author: BangHN
	 */
	public String getServerRevision() {
		return server_revision;
	}

	/**
	 * @return the versionName
	 */
	public String getVersionApp() {
		String softVersionNew = GlobalUtil.getAppVersion();
		softVersion = softVersionNew;
		return softVersion;
	}


	/**
	 * tra ve thong tin version db
	 * @author : BangHN
	 * since : 10:07:10 AM
	 */
	public String getVersionDB(){
		if(StringUtil.isNullOrEmpty(dbVersion) || "0.0.0".equals(dbVersion)){
			try {
				String roleShop = GlobalInfo.getInstance().getProfile().getUserData().getAddRoleShopUser();
				SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
				dbVersion = sharedPreferences.getString(roleShop + LoginView.VNM_VER_DB, "0.0.0");
			} catch (Exception e) {
			}
		}
		if(StringUtil.isNullOrEmpty(dbVersion)){
			dbVersion = "0.0.0";
		}
		return dbVersion;
	}



	/**
	 * @param versionName
	 *            the versionName to set
	 */
	public void setVersionApp(String versionName) {
		if (!StringUtil.isNullOrEmpty(versionName)) {
			int length = versionName.length();
			if (length == 3) {
				// 2.1 --> 2.10
				versionName += ".0";
			}
			// else if (length == 5){
			// // 2.1.1 --> 2.11
			// String lastChar = versionName.substring(length - 1, length);
			// versionName = versionName.substring(0,3) + lastChar;
			// }
		}
		this.softVersion = versionName;
		save();
	}


	/**
	 * set dbversion
	 * @author : BangHN
	 * since : 10:06:33 AM
	 */
	public void setVersionDB(String dbVersion){
		if(StringUtil.isNullOrEmpty(dbVersion)){
			dbVersion = "0.0.0";
		}
		this.dbVersion = dbVersion;
		save();
	}


	public void setMyCell(String MNC, String LAC, String cellId, String cellType) {
		this.myGPSInfo.MNC = MNC;
		this.myGPSInfo.LAC = LAC;
		this.myGPSInfo.cellId = cellId;
		this.myGPSInfo.cellType = cellType;
		save();
	}



	/**
	 * @return the myGPSInfo
	 */
	public GPSInfo getMyGPSInfo() {
		return myGPSInfo;
	}

	public void setMyGPSInfo(GPSInfo myGPSInfo) {
		this.myGPSInfo = myGPSInfo;
		save();
	}


	/**
	 *
	 * Luu thong in profile
	 * @author : DoanDM since : 9:15:31 AM
	 */
	public void save() {
		GlobalUtil.saveObject(this, VNM_PROFILE);
	}


	/**
	 * Clear profile
	 * @author : BangHN
	 * since : 1.0
	 */
	public void clearProfile(){
		actionLogVisitCustomer = null;
		userData.setLoginState(0);
		save();
	}
}
