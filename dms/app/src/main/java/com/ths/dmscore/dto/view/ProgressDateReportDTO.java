package com.ths.dmscore.dto.view;

import java.util.ArrayList;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

public class ProgressDateReportDTO {
	public int totalList;	
	public String totalofTotal;
	public double minScoreNotification;

	public ArrayList<ProgressDateReportItem> arrList;
	
	public ProgressDateReportDTO(){
		arrList = new ArrayList<ProgressDateReportDTO.ProgressDateReportItem>();		
	}

	
	/**
	 * pare data
	 * @author: hoanpd1
	 * @since: 17:34:50 27-03-2015
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void addItem(Cursor c, int sysCurrencyDivide){
		// chia nho don vi tinh
		ProgressDateReportItem item = new ProgressDateReportItem();		
		item.staffId = CursorUtil.getLong(c, "OBJECT_ID");
		item.staffCode = CursorUtil.getString(c, "OBJECT_CODE");
		item.staffName = CursorUtil.getString(c, "OBJECT_NAME");
		item.mobile = CursorUtil.getString(c, "MOBILE");
		
		item.orderPlan = CursorUtil.getInt(c, "DAY_CUST_ORDER_PLAN");
		item.orderDone = CursorUtil.getInt(c, "DAY_CUST_ORDER");
		
		item.amountPlan = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_PLAN");
		item.amountDone = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT");
		item.amountApproved = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_APPROVED");
		item.amountPending = CursorUtil.getDoubleUsingSysConfig(c, "DAY_AMOUNT_PENDING");
		if(item.amountPlan > item.amountDone){
			item.amountRemain = item.amountPlan - item.amountDone;
		}
		item.amountProgress = StringUtil.calPercentUsingRound(item.amountPlan, item.amountDone);
		
		item.quantityPlan = CursorUtil.getLong(c, "DAY_QUANTITY_PLAN");
		item.quantityDone = CursorUtil.getLong(c, "DAY_QUANTITY");
		item.quantityApproved = CursorUtil.getLong(c, "DAY_QUANTITY_APPROVED");
		item.quantityPending = CursorUtil.getLong(c, "DAY_QUANTITY_PENDING");
		if(item.quantityPlan > item.quantityDone){
			item.quantityRemain = item.quantityPlan - item.quantityDone;
		}
		item.quantityProgress = StringUtil.calPercentUsingRound(item.quantityPlan, item.quantityDone);
		item.score  = CursorUtil.getString(c, "SCORE");
		item.routePlan = CursorUtil.getDouble(c, "DAY_ROUTE_PLAN");

		arrList.add(item);
	}
	
	/**
	 * 
	 * Item bao cao cua nhan vien
	 * @author: hoanpd1
	 * @version: 1.0 
	 * @since:  09:49:55 28-03-2015
	 */
	public class ProgressDateReportItem{
		public long staffId;
		public String staffCode;
		public String staffName;		
		public String mobile;
		public int orderPlan;
		public int orderDone;
		public double amountPlan;
		public double amountDone;
		public double amountApproved;
		public double amountPending;
		public double amountRemain;
		
		public long quantityPlan;
		public long quantityDone;
		public long quantityApproved;
		public long quantityPending;
		public long quantityRemain;
		public String score;
		public double routePlan;
		public double amountProgress;
		public double quantityProgress;
		
		public ProgressDateReportItem(){	
			staffId = 0;
			staffCode = "";
			staffName = "";
			orderPlan = 0;
			orderDone = 0;
			amountPlan = 0.0;
			amountDone = 0.0;
			amountRemain = 0.0;
			amountApproved = 0.0;
			amountPending = 0.0;
			quantityPlan = 0;
			quantityDone = 0;
			quantityApproved = 0;
			quantityPending = 0;
			score  = "";
			routePlan = 0.0;
			amountProgress = 0.0;
			quantityProgress = 0.0;
		}
	}
}
