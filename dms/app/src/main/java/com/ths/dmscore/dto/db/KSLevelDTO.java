package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.lib.sqllite.db.KEYSHOP_TABLE;

/**
 * DTO cho table ks_level
 * @author: Tuanlt11
 * @version: 1.0
 * @since: 1.0
 */
public class KSLevelDTO extends AbstractTableDTO {

	// noi dung field
	private static final long serialVersionUID = 1L;
	public long ksLevelId;
	public String ksLevelCode = "";
	public long ksId;
	public String name;
	public long amount;
	public long quantity;
	public int convfact;
	public String productNum;
	public String productNumDone;

	@Override
	public Object clone(){
		// TODO Auto-generated method stub
		KSLevelDTO object = new KSLevelDTO();
		object.ksId = ksId;
		object.name = name;
		object.amount = amount;
		object.quantity = quantity;
		object.ksLevelId = ksLevelId;
		return object;
	}
	
	/**
	 * initDataFromCursor
	 * @author: yennth16
	 * @since: 11:05:38 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initDataFromCursor(Cursor c) {
		ksId = CursorUtil.getLong(c, KEYSHOP_TABLE.KS_ID);
		ksLevelCode = CursorUtil.getString(c, "KS_LEVEL_CODE");
		name = CursorUtil.getString(c, "NAME");
		amount = CursorUtil.getLong(c, "AMOUNT");
		quantity = CursorUtil.getLong(c, "QUANTITY");
		ksLevelId = CursorUtil.getLong(c, "KS_LEVEL_ID");
	}
	
	/**
	 * initDataFromCursor
	 * @author: yennth16
	 * @since: 11:05:38 13-07-2015
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initDataProductFromCursor(Cursor c) {
		ksLevelCode = CursorUtil.getString(c, "PRODUCT_CODE");
		name = CursorUtil.getString(c, "PRODUCT_NAME");
		amount = CursorUtil.getLong(c, "PRODUCT_NUM");
		quantity = CursorUtil.getLong(c, "PRODUCT_NUM_DONE");
		convfact = CursorUtil.getInt(c, "CONVFACT");
		productNum = StringUtil.numReport(amount, convfact);
		productNumDone = StringUtil.numReport(quantity, convfact);
	}
}
