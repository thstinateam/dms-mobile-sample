/**
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.PRODUCT_GROUP_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 * ProductGroupDTO.java
 * @author: dungnt19
 * @version: 1.0 
 * @since:  1.0
 */
public class ProductGroupDTO extends AbstractTableDTO {
	public static final int GROUP_TYPE_PRODUCT = 1;
	public static final int GROUP_TYPE_PROMOTION = 2;
	
	private static final long serialVersionUID = 7474255160980812051L;
	public long productGroupId;
	public String productGroupCode;
	public String productGroupName;
	public long promotionProgramId;
	public int groupType;
	public int minQuantity;
	public int maxQuantity;
	public long minAmount;
	public long maxAmount;
	public int multiple;
	public int recursive;
	public int orderNumber;
	public String createDate;
	public String createUser;
	public String updateDate;
	public String updateUser;
	
	/** Khoi tao du lieu nhom cua CTKM
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param c
	 */
	public void initFromCursor(Cursor c) {
		productGroupId = CursorUtil.getLong(c, PRODUCT_GROUP_TABLE.PRODUCT_GROUP_ID);
		productGroupCode = CursorUtil.getString(c, PRODUCT_GROUP_TABLE.PRODUCT_GROUP_CODE);
		productGroupName = CursorUtil.getString(c, PRODUCT_GROUP_TABLE.PRODUCT_GROUP_NAME);
		groupType = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.GROUP_TYPE);
		minQuantity = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.MIN_QUANTITY);
		maxQuantity = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.MAX_QUANTITY);
		minAmount = CursorUtil.getLong(c, PRODUCT_GROUP_TABLE.MIN_AMOUNT);
		maxAmount = CursorUtil.getLong(c, PRODUCT_GROUP_TABLE.MAX_AMOUNT);
		multiple = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.MULTIPLE);
		recursive = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.RECURSIVE);
		orderNumber = CursorUtil.getInt(c, PRODUCT_GROUP_TABLE.ORDER_NUMBER);
	}

}
