package com.ths.dmscore.view.listener;

import org.slf4j.Marker;

public interface OnEventMarkerListener {
	void onClickInMarker(Marker marker, Object userData);
	void onDoubleClickInMarker(Marker marker, Object userData);
	void onLongClickInMarker(Marker marker, Object userData);
}
