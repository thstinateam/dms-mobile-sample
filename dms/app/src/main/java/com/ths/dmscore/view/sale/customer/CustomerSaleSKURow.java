package com.ths.dmscore.view.sale.customer;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.ths.dmscore.dto.view.CustomerSaleSKUDTO;
import com.ths.dmscore.util.GlobalUtil;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.view.control.SpannableObject;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dms.R;

/**
 * Dong thong tin sku ban cua khach hang
 * @author : BangHN
 * since : 2:15:44 PM
 * version :
 */
public class CustomerSaleSKURow extends DMSTableRow implements OnClickListener{
	TextView tvSTT;//STT
	TextView tvProductCode;//ma san pham
	TextView tvProductName;//ten san pham
	TextView tvAmount;//doanh so

	public CustomerSaleSKURow(Context context) {
		super(context, R.layout.layout_customer_sale_sku_row, GlobalUtil.getInstance().getDPIScreen() - GlobalUtil.dip2Pixel(30));
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
	}
	public CustomerSaleSKURow(Context context, boolean isHeader) {
		super(context, R.layout.layout_customer_sale_sku_row,GlobalUtil.getInstance().getDPIScreen() - GlobalUtil.dip2Pixel(30));
		setOnClickListener(this);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvProductCode = (TextView) findViewById(R.id.tvProductCode);
		tvProductName = (TextView) findViewById(R.id.tvProductName);
		tvAmount = (TextView) findViewById(R.id.tvAmount);
	}


	public void render(CustomerSaleSKUDTO dto, int index, boolean requestShowQuantity){
		if(dto != null){
			tvSTT.setText("" + index);
			tvProductCode.setText(dto.productCode);
			tvProductName.setText(dto.productName);
			if (requestShowQuantity) {
				tvAmount.setText(StringUtil.formatNumber(dto.quantity));
			} else{
				tvAmount.setText(StringUtil.formatNumber(dto.amount));
			}
		}
	}

	public void renderTotal(double amount){
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT,tvProductCode,tvProductName);
		SpannableObject tmp = new SpannableObject();
		tmp.addSpan(StringUtil.formatNumber(amount),
				ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);
		tvAmount.setText(tmp.getSpan());

	}

	 /**
	 * render dong tong cho quantity
	 * @author: Tuanlt11
	 * @param quantity
	 * @return: void
	 * @throws:
	*/
	public void renderTotal(long quantity){
		showRowSum(StringUtil.getString(R.string.TEXT_TOTAL), tvSTT,tvProductCode,tvProductName);
		SpannableObject tmp = new SpannableObject();
		tmp.addSpan(StringUtil.formatNumber(quantity),
				ImageUtil.getColor(R.color.BLACK),
				android.graphics.Typeface.BOLD);
		tvAmount.setText(tmp.getSpan());
	}

	@Override
	public void onClick(View v) {

	}
}
