package com.ths.dmscore.dto.view;

import java.io.Serializable;
import java.util.ArrayList;

import com.ths.dmscore.dto.db.DebitDTO;
import com.ths.dmscore.dto.db.PayReceivedDTO;
import com.ths.dmscore.lib.json.me.JSONArray;

/**
 * DTO man hinh chi tiet cong no cua khach hang
 * 
 * @author: ThangNV31
 * @version: 1.0 
 */
public class CustomerDebitDetailDTO implements Serializable {
	private static final long serialVersionUID = -5904069391108384385L;
	public ArrayList<CustomerDebitDetailItem> arrList = new ArrayList<CustomerDebitDetailItem>();
	public boolean isAllUncheck = true;
	public DebitDTO debitDTO;
	public PayReceivedDTO currentPayReceivedDTO;

	public CustomerDebitDetailDTO() {
		currentPayReceivedDTO = new PayReceivedDTO();
		debitDTO = new DebitDTO();
	}
	
	public JSONArray generatePayDebtSql() {
		JSONArray listSql = new JSONArray();
		// update DEBIT table
		
		// yeu cau GP: vansale khong gui truc tiep debit len server nua!

//		listSql.put(debitDTO.generateUpdateForPayDebit());
		// insert PAY_RECEIVED table
		listSql.put(currentPayReceivedDTO.generateInsertForPayDebt());
		for (int i = 0; i < arrList.size(); i++) {
			if (arrList.get(i).isWouldPay) {
				// update DEBIT_DETAIL table
				listSql.put(arrList.get(i).debitDetailDTO.generateUpdateForPayDebit());
				// insert PAYMENT_DETAIL table
				listSql.put(arrList.get(i).currentPaymentDetailDTO.generateInsertForPayDebt());
			}
		}

		return listSql;
	}

	/**Tao phieu chi
	 * @author cuonglt3
	 * 
	 * @return
	 */
	public JSONArray generatePayDebtSqlReturn() {
		JSONArray listSql = new JSONArray();
		// update DEBIT table
		
		// yeu cau GP: vansale khong gui truc tiep debit len server nua!
		
//		listSql.put(debitDTO.generateUpdateForPayDebit());
		// insert PAY_RECEIVED table
		listSql.put(currentPayReceivedDTO.generateInsertForPayDebt());
		for (int i = 0; i < arrList.size(); i++) {
			if (arrList.get(i).isCheck) {
				// update DEBIT_DETAIL table
				listSql.put(arrList.get(i).debitDetailDTO.generateUpdateForPayDebit());
				// insert PAYMENT_DETAIL table
				listSql.put(arrList.get(i).currentPaymentDetailDTO.generateInsertForPayDebt());
			}
		}
		
		return listSql;
	}
}
