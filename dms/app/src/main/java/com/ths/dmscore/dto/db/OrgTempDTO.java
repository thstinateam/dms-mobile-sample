/**
 * Copyright 2015 THS. All rights reserved.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.lib.sqllite.db.STAFF_TABLE;
import com.ths.dmscore.util.CursorUtil;

/**
 * bang tam chua cac thong tin trong org access
 *
 * @author: dungdq3
 * @version: 1.0 
 * @since:  4:13:21 PM Mar 18, 2015
 */
public class OrgTempDTO extends AbstractTableDTO {

	private static final long serialVersionUID = -5516775602368549132L;
	
	public String staffID;
	public String shopID;

	public OrgTempDTO(TableType type) {
		super(type);
		// TODO Auto-generated constructor stub
		staffID = Constants.STR_BLANK;
		shopID = Constants.STR_BLANK;
	}

	public OrgTempDTO() {
		// TODO Auto-generated constructor stub
		staffID = Constants.STR_BLANK;
		shopID = Constants.STR_BLANK;
	}
	
	public void initFromCursor(Cursor c){
		staffID = CursorUtil.getString(c, STAFF_TABLE.STAFF_ID);
		shopID = CursorUtil.getString(c, STAFF_TABLE.SHOP_ID);
	}

}
