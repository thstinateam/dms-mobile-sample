/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;

import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.PriceDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.CursorUtil;

/**
 *  Luu gia ca san pham
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class PRICE_TABLE extends ABSTRACT_TABLE {
	// ma gia
	public static final String PRICE_ID = "PRICE_ID";
	// ma san pham
	public static final String PRODUCT_ID = "PRODUCT_ID";
	// hieu luc tu ngay
	public static final String FROM_DATE = "FROM_DATE";
	// hieu luc den ngay
	public static final String TO_DATE = "TO_DATE";
	// 1: hoat dong, 0: ngung
	public static final String STATUS = "STATUS";
	// nguoi tao
	public static final String CREATE_USER = "CREATE_USER";
	// nguoi cap nhat
	public static final String UPDATE_USER = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay cap nhat
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// gia
	public static final String PRICE = "PRICE";
	// gia chua vat
	public static final String PRICE_NOT_VAT = "PRICE_NOT_VAT";
	// gia
	public static final String PACKAGE_PRICE = "PACKAGE_PRICE";
	// gia chua vat
	public static final String PACKAGE_PRICE_NOT_VAT = "PACKAGE_PRICE_NOT_VAT";

	// vat
	public static final String VAT = "VAT";

	//loai khach hang ap dung gia
	public static final String CUSTOMER_TYPE_ID = "CUSTOMER_TYPE_ID";
	//kenh ap dung gia
	public static final String SHOP_CHANNEL = "SHOP_CHANNEL";
	//shop ap dung gia
	public static final String SHOP_ID = "SHOP_ID";

	private static final String TABLE_PRICE = "PRICE";

	public PRICE_TABLE(SQLiteDatabase mDB) {
		this.tableName = TABLE_PRICE;
		this.columns = new String[] {PRICE_ID, PRODUCT_ID ,STATUS,FROM_DATE,TO_DATE,CREATE_USER,UPDATE_USER,CREATE_DATE,
				UPDATE_DATE,PRICE,PRICE_NOT_VAT, PACKAGE_PRICE, PACKAGE_PRICE_NOT_VAT, VAT, CUSTOMER_TYPE_ID, SHOP_ID, TABLE_PRICE, SYN_STATE};
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((PriceDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(PriceDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		PriceDTO disDTO = (PriceDTO)dto;
		ContentValues value = initDataRow(disDTO);
		String[] params = { "" + disDTO.priceId };
		return update(value, PRICE_ID + " = ?", params);
	}

	/**
	 * Xoa 1 dong cua CSDL
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(PRICE_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		PriceDTO disDTO = (PriceDTO)dto;
		String[] params = { "" + disDTO.priceId };
		return delete(PRICE_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 * @author: DoanDM replaced
	 * @param id
	 * @return: DisplayPrdogrameLvDTO
	 * @throws:
	 */
	public PriceDTO getRowById(String id) {
		PriceDTO dto = null;
		Cursor c = null;
		try {
			String[]params = {id};
			c = query(
					PRICE_ID + " = ?" , params, null, null, null);
		} catch (Exception ex) {
			c = null;
		}
		if (c != null) {
			if (c.moveToFirst()) {
				dto = initLogDTOFromCursor(c);
			}
		}
		if (c != null) {
			c.close();
		}
		return dto;
	}

	private PriceDTO initLogDTOFromCursor(Cursor c) {
		PriceDTO dpDetailDTO = new PriceDTO();
		dpDetailDTO.priceId = (CursorUtil.getInt(c, PRICE_ID));
		dpDetailDTO.productId = (CursorUtil.getInt(c, PRODUCT_ID));
		dpDetailDTO.status = (CursorUtil.getInt(c, STATUS));
		dpDetailDTO.fromDate = (CursorUtil.getString(c, FROM_DATE));
		dpDetailDTO.toDate = (CursorUtil.getString(c, TO_DATE));
		dpDetailDTO.createUser = (CursorUtil.getString(c, CREATE_USER));

		dpDetailDTO.updateUser = (CursorUtil.getString(c, UPDATE_USER));
		dpDetailDTO.createDate = (CursorUtil.getString(c, CREATE_DATE));
		dpDetailDTO.updateDate = (CursorUtil.getString(c, UPDATE_DATE));
		dpDetailDTO.price = (CursorUtil.getFloat(c, PRICE));
		dpDetailDTO.packagePrice = (CursorUtil.getFloat(c, PACKAGE_PRICE));

		return dpDetailDTO;
	}

	/**
	 *
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: HieuNH
	 * @return
	 * @return: Vector<DisplayPrdogrameLvDTO>
	 * @throws:
	 */
	public Vector<PriceDTO> getAllRow() {
		Vector<PriceDTO> v = new Vector<PriceDTO>();
		Cursor c = null;
		try {
			c = query(null,
					null, null, null, null);
			if (c != null) {
				PriceDTO price;
				if (c.moveToFirst()) {
					do {
						price = initLogDTOFromCursor(c);
						v.addElement(price);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

	private ContentValues initDataRow(PriceDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(PRICE_ID, dto.priceId);
		editedValues.put(PRODUCT_ID, dto.productId);
		editedValues.put(CREATE_USER, dto.createUser);
		editedValues.put(UPDATE_USER, dto.updateUser);
		editedValues.put(FROM_DATE, dto.fromDate);
		editedValues.put(TO_DATE, dto.toDate);
		editedValues.put(STATUS, dto.status);

		editedValues.put(CREATE_DATE, dto.createDate);
		editedValues.put(UPDATE_DATE, dto.updateDate);
		editedValues.put(PRICE, dto.price);
		editedValues.put(PACKAGE_PRICE, dto.packagePrice);

		return editedValues;
	}

}
