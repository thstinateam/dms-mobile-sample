package com.ths.dmscore.dto.view;

import java.util.Vector;

public class CategoryCodeDTO {
	public Vector<String> categoryCode;
	public Vector<String> subCategoryCode;
	
	public CategoryCodeDTO(){
		categoryCode = new Vector<String>();
		subCategoryCode = new Vector<String>();
	}
}
