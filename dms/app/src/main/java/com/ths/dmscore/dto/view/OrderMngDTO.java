package com.ths.dmscore.dto.view;

import com.ths.dmscore.dto.db.CustomerDTO;
import com.ths.dmscore.dto.db.SaleOrderDTO;

public class OrderMngDTO {
	public CustomerDTO customer = new CustomerDTO();
	public SaleOrderDTO saleOrder = new SaleOrderDTO();
}
