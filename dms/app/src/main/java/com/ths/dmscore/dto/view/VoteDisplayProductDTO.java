/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.util.StringUtil;

/**
 * display product dto to vote
 *
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class VoteDisplayProductDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	public String groupCode;
	public String groupName;
	public String productCode;
	public String productName;
	public String numberProduct;
	public int voteNumber;
	public int productID;
	public boolean isGroup;
	public int convfact;

	public VoteDisplayProductDTO() {
		productCode = "";
		productName = "";
		numberProduct = "";
		voteNumber = -1;
		productID = -1;
		convfact = 0;
	}

	/**
	 *
	 * init vote display product
	 *
	 * @author: HaiTC3
	 * @param c
	 * @return: void
	 * @throws:
	 */
	public void initVoteDisplayProduct(Cursor c) {
		groupCode = CursorUtil.getString(c, "DISPLAY_DP_GROUP_CODE");
		groupCode = CursorUtil.getString(c, "DISPLAY_DP_GROUP_CODE");
		groupName = CursorUtil.getString(c, "DISPLAY_DP_GROUP_NAME");
		productCode = CursorUtil.getString(c, "PRODUCT_CODE");
		productName = CursorUtil.getString(c, "PRODUCT_NAME");
		convfact = CursorUtil.getInt(c, "CONVFACT");
		numberProduct = CursorUtil.getString(c, "QUANTITY");
		if (StringUtil.isNullOrEmpty(numberProduct)
				|| numberProduct.equals("null")) {
			numberProduct = "";
		}

		voteNumber = CursorUtil.getInt(c, "VOTENUMBER");
		productID = CursorUtil.getInt(c, "PRODUCT_ID", -1);

	}
}
