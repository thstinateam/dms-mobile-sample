/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.view;

import java.io.Serializable;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_LEVEL_TABLE;
import com.ths.dmscore.lib.sqllite.db.DISPLAY_PROGRAME_TABLE;
import com.ths.dmscore.util.CursorUtil;


/**
 *  vote display present product header view
 *  @author: HaiTC3
 *  @version: 1.0
 *  @since: 1.0
 */
public class DisplayPresentProductInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// display programe code
	public String displayProgramCode;
	// display programe name
	public String displayProgramName;
	// join level for display program of customer
	public String joinLevel;
	// description for display program
	public String descriptionDisplayPrograme;
	// display programe id
	public String displayProgrameID;
	
	// status voted?
	public boolean isVoted;
	
	
	public DisplayPresentProductInfo(){
		displayProgramCode = "";
		displayProgramName = "";
		joinLevel = "";
		descriptionDisplayPrograme = "";
		displayProgrameID = "";
		isVoted = false;
	}
	
	/**
	 * 
	*  init display present product info
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void initDisplayPresentProductInfo(Cursor c){
		displayProgramCode = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_CODE);
		displayProgramName = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_NAME);
		joinLevel = CursorUtil.getString(c, DISPLAY_PROGRAME_LEVEL_TABLE.LEVEL_CODE);
		descriptionDisplayPrograme = CursorUtil.getString(c, "DESCRIPTIONDISPLAYPROGRAME");
		displayProgrameID = CursorUtil.getString(c, DISPLAY_PROGRAME_TABLE.DISPLAY_PROGRAM_ID);
	}
}
