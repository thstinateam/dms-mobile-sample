/*
 * Copyright 2014 THS. All rights reserved.
 *  Use is subject to license terms.
 */

package com.ths.dmscore.view.sale.newcustomer;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ths.dmscore.dto.view.NewCustomerItem;
import com.ths.dmscore.view.control.table.DMSTableRow;
import com.ths.dmscore.view.listener.VinamilkTableListener;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

/**
 * Row for New Customer List
 * NewCustomerRow
 * @author: duongdt3
 * @since:  10:50:03 03/01/2014
 * @update: 10:50:03 03/01/2014
 */
public class CustomerCreatedRow extends DMSTableRow {

	private int ACTION_CUSTOMER_NAME = 0;

	private int ACTION_DELETE = 1;
	
	private NewCustomerItem info;
	// so thu tu
	private TextView tvSTT;
	// khach hang
	private TextView tvCustomerName;
	// dia chi
	private TextView tvAddress;
	// so dien thoai
	private TextView tvPhone;
	// trang thai
	private TextView tvStatus;
	//icon xoa
	private ImageView ivDelete;

	private CustomerCreatedRow(Context context) {
		super(context, R.layout.list_new_customer_row);
		tvSTT = (TextView) findViewById(R.id.tvSTT);
		tvCustomerName = (TextView) findViewById(R.id.tvCustomerName);
		tvCustomerName.setOnClickListener(this);
		tvAddress = (TextView) findViewById(R.id.tvAddress);
		tvPhone = (TextView) findViewById(R.id.tvPhone);
		tvStatus = (TextView) findViewById(R.id.tvStatus);
		ivDelete = (ImageView) findViewById(R.id.ivDelete);
		//set onclick event
	}
	
	public CustomerCreatedRow(Context context, VinamilkTableListener listener,
			int actionCustomerName, int actionDelete) {
		this(context);
		setListener(listener);
		this.ACTION_CUSTOMER_NAME = actionCustomerName;
		this.ACTION_DELETE = actionDelete;

	}

	/**
	 * render row view from data
	 * @author: duongdt3
	 * @param stt 
	 * @since:  10:50:03 03/01/2014
	 * @update: 10:50:03 03/01/2014
	 * @return: void
	 */	
	public void render(int stt, NewCustomerItem info){
		this.info = info;
		//render
		
		tvSTT.setText(String.valueOf(stt));
		tvCustomerName.setText(this.info.tvCustomerName);
		tvAddress.setText(this.info.tvAddress);
		tvPhone.setText(this.info.tvPhone);
		tvStatus.setText(this.info.tvStatus);
		
		//check action customer name
		if (this.info.isEdit) {
			ivDelete.setOnClickListener(this);
		}else{
			//neu ko duoc sua thi an nut xoa
			ivDelete.setImageDrawable(null);
		}
		
		//set mau theo trang thai
		if (this.info.trangThai == NewCustomerItem.STATE_CUSTOMER_ERROR) {
			int colorError = ImageUtil.getColor(R.color.RED);
			setBackgroundRowByColor(colorError);
		}else if (this.info.trangThai == NewCustomerItem.STATE_CUSTOMER_REFUSE) {
			int colorNotApproved = ImageUtil.getColor(R.color.OGRANGE);
			setBackgroundRowByColor(colorNotApproved);
		}
		
	}
	
	@Override
	public void onClick(View v) {
		//click on ten Khach Hang
		super.onClick(v);
		if (listener != null) {
    		if (v == tvCustomerName) {
    			listener.handleVinamilkTableRowEvent(ACTION_CUSTOMER_NAME, v, info);
    		}else if (v == ivDelete) {
    			listener.handleVinamilkTableRowEvent(ACTION_DELETE, v, info);
    		}
		}
	}
	
}
