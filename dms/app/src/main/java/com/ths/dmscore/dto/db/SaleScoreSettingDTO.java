/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.dto.db;

import android.database.Cursor;

import com.ths.dmscore.lib.sqllite.db.SALE_SCORE_SETTING;
import com.ths.dmscore.util.CursorUtil;

/**
 * thong tin diem cua NVBH
 * 
 * @author: HaiTC3
 * @version: 1.0
 * @since: 1.0
 */
public class SaleScoreSettingDTO extends AbstractTableDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// id bang
	public long ID;
	// ma khach hang
	public int fromPercentage;
	// ID CTTB
	public int toPercentage;
	// id cua NVBH
	public int score;
	// ngay tao
	public String updateDate;
	// state for object in DB
	public int synState;

	public SaleScoreSettingDTO() {
		super(TableType.SALE_SCORE_SETTING);
	}

	/**
	 * 
	*  init data from cursor
	*  @author: HaiTC3
	*  @param c
	*  @return: void
	*  @throws:
	 */
	public void initDataFromCursor(Cursor c) {
		ID = CursorUtil.getLong(c, SALE_SCORE_SETTING.ID);
		fromPercentage = CursorUtil.getInt(c, SALE_SCORE_SETTING.FROM_PERCENTAGE);
		toPercentage = CursorUtil.getInt(c, SALE_SCORE_SETTING.TO_PERCENTAGE);
		score = CursorUtil.getInt(c, SALE_SCORE_SETTING.SCORE);
		updateDate = CursorUtil.getString(c, SALE_SCORE_SETTING.UPDATE_DATE);
	}

}
