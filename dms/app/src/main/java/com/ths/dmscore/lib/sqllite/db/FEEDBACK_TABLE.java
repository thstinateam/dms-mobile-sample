/**
 * Copyright THS.
 *  Use is subject to license terms.
 */
package com.ths.dmscore.lib.sqllite.db;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import net.sqlcipher.database.SQLiteDatabase;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;

import com.ths.dmscore.constants.Constants;
import com.ths.dmscore.dto.db.AbstractTableDTO;
import com.ths.dmscore.dto.db.FeedBackTBHVDTO;
import com.ths.dmscore.dto.view.FollowProblemDTO;
import com.ths.dmscore.dto.view.FollowProblemItemDTO;
import com.ths.dmscore.dto.view.ProblemsFeedBackDTO;
import com.ths.dmscore.dto.view.SuperviorTrackAndFixProblemOfGSNPPViewDTO;
import com.ths.dmscore.dto.view.SupervisorProblemOfGSNPPDTO;
import com.ths.dmscore.dto.view.TBHVFollowProblemItemDTO;
import com.ths.dmscore.util.MyLog;
import com.ths.dmscore.util.StringUtil;
import com.ths.dmscore.constants.IntentConstants;
import com.ths.dmscore.dto.db.FeedBackDTO;
import com.ths.dmscore.dto.db.FeedBackDetailDTO;
import com.ths.dmscore.dto.view.DisplayProgrameItemDTO;
import com.ths.dmscore.dto.view.ListNoteInfoViewDTO;
import com.ths.dmscore.dto.view.NoteInfoDTO;
import com.ths.dmscore.dto.view.ReviewsObjectDTO;
import com.ths.dmscore.dto.view.ReviewsStaffViewDTO;
import com.ths.dmscore.util.CursorUtil;
import com.ths.dmscore.view.sale.customer.CustomerFeedBackDto;

/**
 * Luu thong tin phan anh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class FEEDBACK_TABLE extends ABSTRACT_TABLE {
	// id bang
	public static final String FEEDBACK_ID = "FEEDBACK_ID";
	// id NVBH
	public static final String STAFF_ID = "STAFF_ID";
	// id KH
	public static final String CUSTOMER_ID = "CUSTOMER_ID";
	// trang thai: 1: tiep nhan, 2: da xu ly
	public static final String STATUS = "STATUS";
	// SHOP_ID
	public static final String SHOP_ID = "SHOP_ID";
	// noi dung
	public static final String CONTENT = "CONTENT";
	// nguoi update
	public static final String USER_UPDATE = "UPDATE_USER";
	// ngay tao
	public static final String CREATE_DATE = "CREATE_DATE";
	// ngay update
	public static final String UPDATE_DATE = "UPDATE_DATE";
	// loai phan anh
	public static final String TYPE = "TYPE";
	// num return
	public static final String NUM_RETURN = "NUM_RETURN";
	// nguoi nhac nho
	public static final String REMIND_DATE = "REMIND_DATE";
	// ngay thuc hien
	public static final String DONE_DATE = "DONE_DATE";
	// parent staff id
	public static final String CREATE_USER_ID = "CREATE_USER_ID";;
	// is deleted
	public static final String IS_DELETED = "IS_DELETED";;
	// training plan detail id
	public static final String TRAINING_PLAN_DETAIL_ID = "TRAINING_PLAN_DETAIL_ID";

	public static final String FEEDBACK_TABLE = "FEEDBACK";

	public static final String DESCRIPTION = "DESCRIPTION";

	// list nhan vien duoc chon
	public static final String LIST_STAFF_ID = "LIST_STAFF";


	public FEEDBACK_TABLE(SQLiteDatabase mDB) {
		this.tableName = FEEDBACK_TABLE;
		this.columns = new String[] { FEEDBACK_ID, STAFF_ID, CUSTOMER_ID, STATUS, SHOP_ID, DONE_DATE, REMIND_DATE, CONTENT,
				NUM_RETURN, IS_DELETED, TRAINING_PLAN_DETAIL_ID, USER_UPDATE, CREATE_DATE, UPDATE_DATE, TYPE,
				CREATE_USER_ID, SYN_STATE };
		this.sqlGetCountQuerry += this.tableName + ";";
		this.sqlDelete += this.tableName + ";";
		this.mDB = mDB;

	}

	/**
	 * Them 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long insert(AbstractTableDTO dto) {
		ContentValues value = initDataRow((FeedBackDTO) dto);
		return insert(null, value);
	}

	/**
	 *
	 * them 1 dong xuong CSDL
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	public long insert(FeedBackDTO dto) {
		ContentValues value = initDataRow(dto);
		return insert(null, value);
	}

	public long gsnppInsert(FeedBackDTO dto) {
		ContentValues value = gsnppInitDataRow(dto);
		return insert(null, value);
	}

	/**
	 * Update 1 dong xuong db
	 *
	 * @author: TruongHN
	 * @param dto
	 * @return: long
	 * @throws:
	 */
	@Override
	public long update(AbstractTableDTO dto) {
		FeedBackDTO dtoFeedBack = (FeedBackDTO) dto;
		ContentValues value = initDataRow(dtoFeedBack);
		String[] params = { "" + dtoFeedBack.getFeedBackId() };
		return update(value, FEEDBACK_ID + " = ?", params);
	}

	/**
	 *
	 * cap nhat noi dung cho feedback
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateContentFeedBack(FeedBackDTO dto) {
		ContentValues value = initDateRowUpdateFeedBackContent(dto);
		String[] params = { String.valueOf(dto.getFeedBackId()) };
		return update(value, FEEDBACK_ID + " = ?", params);
	}

	/**
	 *
	 * update status feedback dto in DB at table FEEDBACK
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateFeedBackStatus(FeedBackDTO dto) {
		try {
			ContentValues value = initDateRowUpdateFeedBackStatus(dto);
			String[] params = { String.valueOf(dto.getFeedBackId()) };
			return update(value, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 *
	 * update feedback status from feedbackId, status, doneDate
	 *
	 * @param status
	 * @param doneDate
	 * @param feedbackId
	 * @return
	 * @return: long
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	public long updateFeedBackStatus(String status, String doneDate, String feedbackId) {
		try {
			ContentValues value = initdateUpdateFeedBackStatus(status, doneDate);
			String[] params = { feedbackId };
			return update(value, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Xoa 1 dong cua CSDL
	 *
	 * @author: TruongHN
	 * @param inheritId
	 * @return: int
	 * @throws:
	 */
	public int delete(String code) {
		String[] params = { code };
		return delete(FEEDBACK_ID + " = ?", params);
	}

	public long delete(AbstractTableDTO dto) {
		FeedBackDTO dtoFeedBack = (FeedBackDTO) dto;
		String[] params = { "" + dtoFeedBack.getFeedBackId() };
		return delete(FEEDBACK_ID + " = ?", params);
	}

	/**
	 * Lay 1 dong cua CSDL theo id
	 *
	 * @author: DoanDM replaced
	 * @param id
	 * @return: FeedBackDTO
	 * @throws:
	 */
	public FeedBackDTO getRowById(String id) {
		FeedBackDTO dto = null;
		Cursor c = null;
		try {
			String[] params = { id };
			c = query(FEEDBACK_ID + " = ?", params, null, null, null);
			if (c != null) {
				if (c.moveToFirst()) {
					dto = initDTOFromCursor(c);
				}
			}
		} catch (Exception ex) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dto;
	}

	private FeedBackDTO initDTOFromCursor(Cursor c) {
		FeedBackDTO dto = new FeedBackDTO();
		dto.setFeedBackId((CursorUtil.getLong(c, FEEDBACK_ID)));
		dto.setStaffId((CursorUtil.getInt(c, STAFF_ID)));
		dto.setCustomerId((CursorUtil.getString(c, CUSTOMER_ID)));
		dto.setStatus((CursorUtil.getInt(c, STATUS)));
		dto.setContent((CursorUtil.getString(c, CONTENT)));
		dto.setUserUpdate((CursorUtil.getString(c, USER_UPDATE)));
		dto.setRemindDate((CursorUtil.getString(c, REMIND_DATE)));
		dto.setDoneDate((CursorUtil.getString(c, DONE_DATE)));
		dto.setCreateDate((CursorUtil.getString(c, CREATE_DATE)));
		dto.setUpdateDate((CursorUtil.getString(c, UPDATE_DATE)));
		dto.setFeedbackType((CursorUtil.getInt(c, TYPE)));
		dto.setCreateUserId((CursorUtil.getLong(c, CREATE_USER_ID)));
		return dto;
	}

	/**
	 * lay tat ca cac dong cua CSDL
	 *
	 * @author: TruongHN
	 * @return: Vector<FeedBackDTO>
	 * @throws:
	 */
	public Vector<FeedBackDTO> getAllRow() {
		Vector<FeedBackDTO> v = new Vector<FeedBackDTO>();
		Cursor c = null;
		try {
			c = query(null, null, null, null, null);
			if (c != null) {
				FeedBackDTO dto;
				if (c.moveToFirst()) {
					do {
						dto = initDTOFromCursor(c);
						v.addElement(dto);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.e("getAllRow", e);
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return v;

	}

//	private ContentValues initDataRow(FeedBackDTO dto) {
//		ContentValues editedValues = new ContentValues();
//		editedValues.put(FEEDBACK_ID, dto.feedBackId);
//		editedValues.put(STAFF_ID, dto.staffId);
//		editedValues.put(CUSTOMER_ID, dto.customerId);
//		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.trainingPlanDetailId);
//		editedValues.put(STATUS, dto.status);
//		editedValues.put(CONTENT, dto.content);
//		editedValues.put(TYPE, dto.type);
//		editedValues.put(REMIND_DATE, dto.remindDate);
//		editedValues.put(DONE_DATE, dto.doneDate);
//		editedValues.put(NUM_RETURN, dto.numReturn);
//		editedValues.put(CREATE_USER_ID, dto.createUserId);
//		editedValues.put(CREATE_DATE, dto.createDate);
//		editedValues.put(USER_UPDATE, dto.userUpdate);
//		editedValues.put(UPDATE_DATE, dto.updateDate);
//		editedValues.put(IS_DELETED, dto.isDeleted);
//		return editedValues;
//	}
//
	private ContentValues initDataRow(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(FEEDBACK_ID, dto.getFeedBackId());
//		editedValues.put(STAFF_ID, dto.staffId);
//		editedValues.put(CUSTOMER_ID, dto.customerId);
//		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.trainingPlanDetailId);
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(CONTENT, dto.getContent());
		editedValues.put(TYPE, dto.getFeedbackType());
		editedValues.put(REMIND_DATE, dto.getRemindDate());
		editedValues.put(CREATE_USER_ID, dto.getCreateUserId());
		editedValues.put(CREATE_DATE, dto.getCreateDate());
		editedValues.put(IS_DELETED, dto.getIsDeleted());
		return editedValues;
	}

	private ContentValues gsnppInitDataRow(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(FEEDBACK_ID, dto.getFeedBackId());
		editedValues.put(STAFF_ID, dto.getStaffId());
		editedValues.put(CUSTOMER_ID, dto.getCustomerId());
		editedValues.put(TRAINING_PLAN_DETAIL_ID, dto.getTrainingPlanDetailId());
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(SHOP_ID, dto.getShopId());
		editedValues.put(CONTENT, dto.getContent());
		editedValues.put(TYPE, dto.getFeedbackType());
		editedValues.put(REMIND_DATE, dto.getRemindDate());
		editedValues.put(DONE_DATE, dto.getDoneDate());
		editedValues.put(NUM_RETURN, dto.getNumReturn());
		editedValues.put(CREATE_USER_ID, dto.getCreateUserId());
		editedValues.put(CREATE_DATE, dto.getCreateDate());
		editedValues.put(USER_UPDATE, dto.getUserUpdate());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());
//		editedValues.put(IS_DELETED, dto.isDeleted);
		return editedValues;
	}


	/**
	 *
	 * update feedback dayInOrder row at column "STATUS"
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDateRowUpdateFeedBackStatus(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, dto.getStatus());
		editedValues.put(DONE_DATE, dto.getDoneDate());
		return editedValues;
	}

	/**
	 *
	 * update feedback status from status and doneDate
	 *
	 * @param status
	 * @param doneDate
	 * @return
	 * @return: ContentValues
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 7, 2012
	 */
	private ContentValues initdateUpdateFeedBackStatus(String status, String doneDate) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(STATUS, status);
		editedValues.put(DONE_DATE, doneDate);
		return editedValues;
	}

	/**
	 *
	 * tao du lieu update content cho feedaback
	 *
	 * @author: HaiTC3
	 * @param dto
	 * @return
	 * @return: ContentValues
	 * @throws:
	 */
	private ContentValues initDateRowUpdateFeedBackContent(FeedBackDTO dto) {
		ContentValues editedValues = new ContentValues();
		editedValues.put(CONTENT, dto.getContent());
		editedValues.put(USER_UPDATE, dto.getUserUpdate());
		editedValues.put(UPDATE_DATE, dto.getUpdateDate());
		editedValues.put(REMIND_DATE, dto.getRemindDate());
		return editedValues;
	}

	public CustomerFeedBackDto getFeedBackList(String staffId, String customerId, String type, String arrType[],
			String status, String doneDate, int page, int isGetTotalPage) {
		CustomerFeedBackDto dto = null;
		Cursor c = null;
		Cursor c_totalRow = null;
		dto = new CustomerFeedBackDto();

		StringBuffer var1 = new StringBuffer();
		ArrayList<String> param = new ArrayList<String>();
		ArrayList<String> totalPageParam = new ArrayList<String>();
		var1.append("select FB.FEEDBACK_ID                       as FEEDBACK_ID, ");
		var1.append("       FB.STAFF_ID                          as STAFF_ID, ");
		var1.append("       FB.CUSTOMER_ID                       as CUSTOMER_ID, ");
		var1.append("       CT.CUSTOMER_CODE                     as CUSTOMER_CODE, ");
		var1.append("       CT.CUSTOMER_NAME                     as CUSTOMER_NAME, ");
		var1.append("       CT.STREET                     		 as STREET, ");
		var1.append("       CT.HOUSENUMBER                       as HOUSE_NUMBER, ");
		var1.append("       FB.STATUS                            as STATUS, ");
		var1.append("       FB.CONTENT                           as CONTENT, ");
		var1.append("       FB.TYPE                              as TYPE, ");
		var1.append("       FB.CREATE_USER_ID                    as CREATE_USER_ID, ");
		var1.append("       AP.DESCRIPTION                       as AP_PARAM_NAME, ");
		var1.append("       AP.CODE                       		 as AP_PARAM_CODE, ");
		var1.append("       FB.IS_DELETED                        as IS_DELETED, ");
		var1.append("       Strftime('%d/%m/%Y', FB.CREATE_DATE) as CREATE_DATE, ");
		var1.append("       Strftime('%d/%m/%Y', FB.REMIND_DATE) as REMIND_DATE, ");
		var1.append("       Strftime('%d/%m/%Y', FB.DONE_DATE)   as DONE_DATE, ");
		var1.append("       Strftime('%d/%m/%Y', FB.UPDATE_DATE) as UPDATE_DATE, ");
		var1.append("       Strftime('%d/%m/%Y', FB.USER_UPDATE) as USER_UPDATE ");
		var1.append("from   AP_PARAM AP, FEEDBACK FB left join CUSTOMER CT on FB.CUSTOMER_ID = CT.CUSTOMER_ID  ");
		var1.append("where  FB.STAFF_ID = ? ");
		param.add(staffId);
		totalPageParam.add(staffId);
		var1.append("       and (AP.CODE like 'FEEDBACK_TYPE_NVBH' or AP.CODE like 'FEEDBACK_TYPE_GSNPP')");
		var1.append("       and FB.TYPE = AP.VALUE");
		if (!StringUtil.isNullOrEmpty(type)) {
			var1.append(" AND AP.VALUE = ?");
			param.add(type);
			totalPageParam.add(type);
		}
		var1.append("       and AP.STATUS = 1 ");
		var1.append("       and FB.STATUS <> 0 ");
		var1.append("       and Date(FB.CREATE_DATE) <= Date('NOW', 'localtime') ");

		if (StringUtil.isNullOrEmpty(customerId)) {
			if (!StringUtil.isNullOrEmpty(status)) {
				var1.append("       and FB.STATUS = ? ");
				param.add(status);
				totalPageParam.add(status);
			}
			if (!StringUtil.isNullOrEmpty(doneDate) && doneDate.equals("0")) {
				var1.append("       and FB.DONE_DATE is null ");
			}

		} else {
			var1.append("       and FB.CUSTOMER_ID = ? ");
			param.add(customerId);
			totalPageParam.add(customerId);
		}

		var1.append("		order  by FB.STATUS asc, ");
		var1.append("          			FB.REMIND_DATE , ");
		var1.append("          			FB.DONE_DATE desc, ");
		var1.append("          			FB.CREATE_DATE desc, ");
		var1.append("          			CT.CUSTOMER_CODE, ");
		var1.append("          			CT.CUSTOMER_NAME ");

		// get count
		StringBuffer totalCount = new StringBuffer();
		if (isGetTotalPage == 1) {
			totalCount.append("select count(*) as TOTAL_ROW from (" + var1 + ")");
		}

		if (page > 0) {
			var1.append("       limit ? offset ?");
			param.add("" + Constants.NUM_ITEM_PER_PAGE);
			param.add("" + (page - 1) * Constants.NUM_ITEM_PER_PAGE);
		}

		try {
			c = rawQueries(var1.toString(), param);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FeedBackDTO item = new FeedBackDTO();
						item.initDataFromCursor(c);
						dto.arrItem.add(item);
					} while (c.moveToNext());
				}
			}

			if (isGetTotalPage == 1) {
				c_totalRow = rawQueries(totalCount.toString(), totalPageParam);
				if (c_totalRow != null) {
					if (c_totalRow.moveToFirst()) {
						dto.totalFeedBack = c_totalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (c != null) {
				c.close();
			}
			if (c_totalRow != null) {
				c_totalRow.close();
			}
		}

		return dto;
	}

	public long postFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				TABLE_ID tableId = new TABLE_ID(mDB);
				// long offValue = GlobalInfo.getInstance().getProfile()
				// .getUserData().id
				// * GlobalInfo.getInstance().getFactorDefault();
				// long maxFeedbackId = tableId
				// .getMaxIdFromTableName(FEED_BACK_TABLE) + 1;
				// dto.feedBackId = maxFeedbackId + offValue;
				// if (returnCode != -1) {
				// tableId.updateMaxId(FEED_BACK_TABLE, maxFeedbackId);
				// }
				dto.setFeedBackId(tableId.getMaxIdTime(FEEDBACK_TABLE));
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}
//
//	public long gsnppPostFeedBack(FeedBackDTO dto) {
//		long returnCode = -1;
//		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
//			try {
//				TABLE_ID tableId = new TABLE_ID(mDB);
//				// long offValue = GlobalInfo.getInstance().getProfile()
//				// .getUserData().id
//				// * GlobalInfo.getInstance().getFactorDefault();
//				// long maxFeedbackId = tableId
//				// .getMaxIdFromTableName(FEED_BACK_TABLE) + 1;
//				// dto.feedBackId = maxFeedbackId + offValue;
//				// if (returnCode != -1) {
//				// tableId.updateMaxId(FEED_BACK_TABLE, maxFeedbackId);
//				// }
//				dto.feedBackId = tableId.getMaxId(FEEDBACK_TABLE);
//				dto.arrFeedBackId.add(String.valueOf(dto.feedBackId));
//				returnCode = gsnppInsert(dto);
//			} catch (Exception e) {
//			}
//		}
//		return returnCode;
//	}

	/**
	 *
	 * update note info
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param shopId
	 * @param noteUpdate
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO updateNoteInfo(String staffId, String shopId, NoteInfoDTO noteUpdate) {
		ListNoteInfoViewDTO listNote = new ListNoteInfoViewDTO();
		this.updateFeedBackStatus(noteUpdate.feedBack);
		listNote = this.getListNote(staffId, shopId, " limit 0,2");
		return listNote;
	}

	/**
	 *
	 * lay danh sach ghi chu cua NVBH doi voi cac user
	 *
	 * @author: HaiTC3
	 * @param staff_id
	 * @param shop_id
	 * @return
	 * @return: ListNoteInfoViewDTO
	 * @throws:
	 */
	public ListNoteInfoViewDTO getListNote(String staff_id, String shop_id, String ext) {
		ListNoteInfoViewDTO DTO = new ListNoteInfoViewDTO();
		StringBuffer requestGetNoteList = new StringBuffer();
		requestGetNoteList.append("SELECT FB.feedback_id, ");
		requestGetNoteList.append("       FB.content, ");
		requestGetNoteList.append("       FB.remind_date, ");
		requestGetNoteList.append("       FB.done_date, ");
		requestGetNoteList.append("       FB.status, ");
		requestGetNoteList.append("       Strftime('%d/%m/%Y', FB.create_date) AS CREATE_DATE, ");
		requestGetNoteList.append("       FB.type                              AS TYPE, ");
		requestGetNoteList.append("       FB.create_user_id                    AS CREATE_USER_ID, ");
		requestGetNoteList.append("       CT.customer_id, ");
		requestGetNoteList.append("       CT.customer_code, ");
		requestGetNoteList.append("       CT.customer_name ");
		requestGetNoteList.append("FROM   feedback AS FB, ");
		requestGetNoteList.append("       customer AS CT ");
		requestGetNoteList.append("WHERE  FB.customer_id = CT.customer_id ");
		requestGetNoteList.append("       AND FB.staff_id = ? ");
		requestGetNoteList.append("       AND CT.shop_id = ? ");
		requestGetNoteList.append("       AND FB.is_deleted = 0 ");
		requestGetNoteList.append("       AND CT.STATUS = 1 ");
		requestGetNoteList.append("       AND FB.status = 1 ");
		requestGetNoteList.append("ORDER  BY FB.create_date DESC ");

		Cursor c = null;
		Cursor cTmp = null;
		String getCountNoteList = " SELECT COUNT(*) AS TOTAL_ROW FROM (" + requestGetNoteList.toString() + ") ";
		String[] params = new String[] { staff_id, shop_id };

		try {
			cTmp = rawQuery(getCountNoteList, params);
			int total = 0;
			if (cTmp != null) {
				cTmp.moveToFirst();
				total = cTmp.getInt(0);
				DTO.setNoteNumber(total);
			}

			c = rawQuery(requestGetNoteList.toString() + ext, params);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						NoteInfoDTO note = new NoteInfoDTO();
						note.initDateWithCursor(c);
						DTO.getListNote().add(note);
					} while (c.moveToNext());
				}
			}
		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			if (cTmp != null) {
				cTmp.close();
			}
			if (c != null) {
				c.close();
			}
		}
		return DTO;
	}

	public long updateDoneDateFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				ContentValues editedValues = new ContentValues();
				editedValues.put(DONE_DATE, dto.getDoneDate());
				editedValues.put(STATUS, dto.getStatus());
				String[] params = { "" + dto.getFeedBackId() };
				returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	public long updateDeleteFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				ContentValues editedValues = new ContentValues();
				editedValues.put(STATUS, dto.getStatus());
				if (dto.getUpdateDate() != null) {
					editedValues.put(UPDATE_DATE, dto.getUpdateDate());
				}
				if (dto.getUserUpdate() != null) {
					editedValues.put(USER_UPDATE, dto.getUserUpdate());
				}
				String[] params = { "" + dto.getFeedBackId() };
				returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

	/**
	 *
	 * Thuc hien cap nhat van de cua nvbh (thuc hien boi GSNPP)
	 *
	 * @author: ThanhNN8
	 * @param dto
	 * @return
	 * @return: long
	 * @throws:
	 */
	public long updateGSNPPFollowProblemDone(FollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			ContentValues editedValues = new ContentValues();
			editedValues.put(STATUS, dto.status);
			if (dto.status == FeedBackDTO.FEEDBACK_STATUS_CREATE) {// neu la yeu
																	// cau lam
																	// lai thi
																	// update
																	// lai so
																	// lan yeu
																	// cau
				editedValues.put(NUM_RETURN, dto.numReturn);
				editedValues.put(DONE_DATE, dto.doneDate);
			}
			String[] params = { "" + dto.feedBackId };
			returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 * HieuNH update van de cua TBHV
	 *
	 * @param dto
	 * @return
	 */
//	public long updateTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
//		long returnCode = -1;
//		try {
//			ContentValues editedValues = new ContentValues();
//			editedValues.put(STATUS, dto.status);
//			editedValues.put(USER_UPDATE, dto.userUpdate);
//			editedValues.put(UPDATE_DATE, dto.updateDate);
//			editedValues.put(DONE_DATE, dto.doneDate);
//			editedValues.put(IS_DELETED, dto.isDeleted);
//			String[] params = { "" + dto.id };
//			returnCode = update(editedValues, FEEDBACK_ID + " = ?", params);
//		} catch (Exception e) {
//		}
//		return returnCode;
//	}

	/**
	 * HieuNH delete van de cua TBHV
	 *
	 * @param dto
	 * @return
	 */
	public long deleteTBHVFollowProblemDone(TBHVFollowProblemItemDTO dto) {
		long returnCode = -1;
		try {
			String[] params = { "" + dto.id };
			returnCode = delete(FEEDBACK_ID + " = ?", params);
		} catch (Exception e) {
		}
		return returnCode;
	}

	/**
	 *
	 * danh sach theo doi khac phuc cua GSNPP
	 *
	 * @author: ThanhNN8
	 * @param data
	 * @return
	 * @return: FollowProblemDTO
	 * @throws Exception
	 * @throws:
	 */
	public FollowProblemDTO getListProblemOfSuperVisor(Bundle data) throws Exception {
		String extPage = data.getString(IntentConstants.INTENT_PAGE);
		// chua su dung id gsnpp
		int superStaffId = data.getInt(IntentConstants.INTENT_STAFF_ID);
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID_PARA);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String customerCode = data.getString(IntentConstants.INTENT_CUSTOMER_CODE);
		String status = data.getString(IntentConstants.INTENT_STATE);
		String typeProblem = data.getString(IntentConstants.INTENT_TYPE_PROBLEM);
		String fromDate = data.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
		String toDate = data.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);
		boolean checkPagging = data.getBoolean(IntentConstants.INTENT_CHECK_PAGGING, false);

		FollowProblemDTO result = new FollowProblemDTO();
		List<String> stringParams = new ArrayList<String>();
		STAFF_TABLE staff = new STAFF_TABLE(mDB);
		StringBuilder stringbuilder = new StringBuilder();
		String staffList = staff.getListStaffOfSupervisor(String.valueOf(superStaffId), shopId);
		stringbuilder.append("SELECT s.staff_id                           STAFF_ID, ");
		stringbuilder.append("       s.staff_code                         STAFF_CODE, ");
		stringbuilder.append("       s.name                         STAFF_NAME, ");
		stringbuilder.append("       c.customer_id                        CUSTOMER_ID, ");
		stringbuilder.append("       SUBSTR(c.customer_code, 1, 3)        CUSTOMER_CODE, ");
		stringbuilder.append("       c.customer_name                      CUSTOMER_NAME, ");
		stringbuilder.append("       ap.description                     AP_PARAM_NAME, ");
		stringbuilder.append("       fb.feedback_id                       FEEDBACK_ID, ");
		stringbuilder.append("       fb.content                           CONTENT, ");
		stringbuilder.append("       fb.num_return                        NUM_RETURN, ");
		stringbuilder.append("       fb.status                            STATUS, ");
		stringbuilder.append("       Strftime('%d/%m/%Y', fb.create_date) CREATE_DATE, ");
		stringbuilder.append("       Strftime('%d/%m/%Y', fb.done_date)   DONE_DATE, ");
		stringbuilder.append("       Strftime('%d/%m/%Y', fb.remind_date) REMIND_DATE ");
		stringbuilder.append("FROM   feedback fb LEFT JOIN staff s ON (fb.staff_id = s.staff_id AND s.shop_id = ? ) ");
		stringParams.add(shopId);
		stringbuilder.append("       LEFT JOIN customer c ON (fb.customer_id = c.customer_id), ");
		stringbuilder.append("       ap_param ap ");
		stringbuilder.append("WHERE  fb.type = ap.value ");

		stringbuilder.append("       AND ( ap.code LIKE 'FEEDBACK_TYPE_NVBH' ");
		stringbuilder.append("              OR ap.code LIKE 'FEEDBACK_TYPE_GSNPP' ) ");
		stringbuilder.append("       AND s.staff_id IN( ");
		stringbuilder.append(staffList);
		stringbuilder.append("       ) ");
		stringbuilder.append("       AND (c.customer_id is null or (c.customer_id is not null AND c.shop_id = ?)) ");
		stringParams.add(shopId);
		stringbuilder.append("       AND s.status = 1 ");
		if (!StringUtil.isNullOrEmpty(status)) {
			stringbuilder.append("       AND fb.status = ? ");
			stringParams.add(status);
		} else {
			stringbuilder.append("       AND fb.status <> 0 ");
		}
		if (!StringUtil.isNullOrEmpty(fromDate)) {
			stringbuilder.append("       AND Date(fb.create_date) >= Date(?) ");
			stringParams.add(fromDate);
		}
		if (!StringUtil.isNullOrEmpty(toDate)) {
			stringbuilder.append("       AND Date(fb.create_date) <= Date(?) ");
			stringParams.add(toDate);
		}
		if (!StringUtil.isNullOrEmpty(staffId)) {
			stringbuilder.append("       AND fb.staff_id = ? ");
			stringParams.add(staffId);
		}
		if (!StringUtil.isNullOrEmpty(customerCode)) {
			stringbuilder.append("       AND Lower(c.customer_code) LIKE ? ");
			stringParams.add("%" + customerCode.toLowerCase() + "%");
		}

		if (!StringUtil.isNullOrEmpty(typeProblem)) {
			stringbuilder.append("       AND fb.type = ? ");
			stringParams.add(typeProblem);
		}
		stringbuilder.append("ORDER  BY status, ");
		stringbuilder.append("          done_date, ");
		stringbuilder.append("          remind_date, ");
		stringbuilder.append("          create_date DESC, ");
		stringbuilder.append("          staff_name, ");
		stringbuilder.append("          customer_code, ");
		stringbuilder.append("          customer_name ");

		String requestGetFollowProblemList = stringbuilder.toString();
		Cursor c = null;
		String[] params = new String[stringParams.size()];
		for (int i = 0, length = stringParams.size(); i < length; i++) {
			params[i] = stringParams.get(i);
		}

		Cursor cTmp = null;
		String getCountFollowProblemList = " SELECT COUNT(*) AS TOTAL_ROW FROM (" + requestGetFollowProblemList + ") ";

		try {
			if (!checkPagging) {
				cTmp = rawQuery(getCountFollowProblemList, params);
				if (cTmp != null) {
					cTmp.moveToFirst();
					result.total = cTmp.getInt(0);
				}
			}
			c = rawQuery(requestGetFollowProblemList + extPage, params);
			if (c != null) {
				if (c.moveToFirst()) {
					List<FollowProblemItemDTO> listFollow = new ArrayList<FollowProblemItemDTO>();

					do {
						FollowProblemItemDTO note = new FollowProblemItemDTO();
						note.initDateWithCursor(c);
						listFollow.add(note);
					} while (c.moveToNext());
					result.list = listFollow;
				} else {
					result.list = new ArrayList<FollowProblemItemDTO>();
				}
			}
		} catch (Exception e) {
			return null;
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTmp != null) {
					cTmp.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	// /**
	// *
	// * lay combobox nvbh cho man hinh theo doi khac phuc
	// *
	// * @author: ThanhNN8
	// * @param
	// * @return
	// * @return: List<ComboBoxDisplayProgrameItemDTO>
	// * @throws:
	// */
	// public List<ComboBoxDisplayProgrameItemDTO>
	// getComboboxProblemNVBHOfSuperVisor() {
	// List<ComboBoxDisplayProgrameItemDTO> result = new
	// ArrayList<ComboBoxDisplayProgrameItemDTO>();
	// StringBuilder stringbuilder = new StringBuilder();
	// stringbuilder.append("SELECT DISTINCT ST.STAFF_CODE FROM FEEDBACK AS FB, CUSTOMER AS CT, STAFF AS ST, AP_PARAM AS AP");
	// stringbuilder.append(" WHERE FB.CUSTOMER_ID = CT.CUSTOMER_ID");
	// stringbuilder.append(" AND FB.TYPE IN (5, 6, 7)");
	// stringbuilder.append(" AND FB.IS_DELETED = 0");
	// stringbuilder.append(" AND FB.STAFF_ID = ST.STAFF_ID");
	// stringbuilder.append(" AND DATE(FB.CREATE_DATE) <= DATE('now','localtime')");
	// stringbuilder.append(" AND AP.VALUE = FB.TYPE AND AP.CODE = ?");
	// stringbuilder.append(" AND FB.CREATE_USER_ID = ?");
	// stringbuilder.append(" ORDER BY ST.STAFF_CODE DESC");
	// String requestGetFollowProblemList = stringbuilder.toString();
	// Cursor c = null;
	// String[] params = new String[] { "FEEDBACK_TYPE_NVBH",
	// GlobalInfo.getInstance().getProfile().getUserData().id + "" };
	//
	// try {
	// c = rawQuery(requestGetFollowProblemList, params);
	// if (c != null) {
	// if (c.moveToFirst()) {
	// do {
	// ComboBoxDisplayProgrameItemDTO comboboxNVBHDTO = new
	// ComboBoxDisplayProgrameItemDTO();
	// if (c.getColumnIndex("STAFF_CODE") >= 0) {
	// comboboxNVBHDTO.value = c.getString(c
	// .getColumnIndex("STAFF_CODE"));
	// comboboxNVBHDTO.name = c.getString(c
	// .getColumnIndex("STAFF_CODE"));
	// } else {
	// comboboxNVBHDTO.value = "";
	// comboboxNVBHDTO.name = "";
	// }
	// result.add(comboboxNVBHDTO);
	// } while (c.moveToNext());
	// }
	// }
	// } catch (Exception e) {
	// return null;
	// } finally {
	// if (c != null) {
	// c.close();
	// }
	// }
	// return result;
	// }

	/**
	 *
	 * lay combobox trang thai cho man hinh theo doi khac phuc
	 *
	 * @author: ThanhNN8
	 * @param
	 * @return
	 * @return: List<ComboBoxDisplayProgrameItemDTO>
	 * @throws:
	 */
	public List<DisplayProgrameItemDTO> getComboboxProblemStatusOfSuperVisor() {
		List<DisplayProgrameItemDTO> result = new ArrayList<DisplayProgrameItemDTO>();
		return result;
	}

	/**
	 *
	 * get list track and fix problem of gsnpp
	 *
	 * @param ext
	 * @return
	 * @return: ArrayList<SupervisorProblemOfGSNPPDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 6, 2012
	 */
	public SuperviorTrackAndFixProblemOfGSNPPViewDTO getListTrackAndFixProblemOfGSNPP(Bundle ext) {
		SuperviorTrackAndFixProblemOfGSNPPViewDTO result = new SuperviorTrackAndFixProblemOfGSNPPViewDTO();
		Cursor c = null;
		Cursor cTmp = null;
		try {
			String typeProblem = ext.getString(IntentConstants.INTENT_TYPE_PROBLEM_GSNPP);
			String staffId = ext.getString(IntentConstants.INTENT_STAFF_ID);
			// String createUserStaffId =
			// ext.getString(IntentConstants.INTENT_CREATE_USER_STAFF_ID);
			boolean isGetTotalItem = ext.getBoolean(IntentConstants.INTENT_IS_ALL);
			String page = ext.getString(IntentConstants.INTENT_PAGE);

			String fromDate = ext.getString(IntentConstants.INTENT_FIND_ORDER_FROM_DATE);
			String toDate = ext.getString(IntentConstants.INTENT_FIND_ORDER_TO_DATE);

			ArrayList<String> listparam = new ArrayList<String>();
			listparam.add(staffId);

			StringBuffer sqlQuery = new StringBuffer();
			sqlQuery.append("SELECT FB.feedback_id                       AS FEEDBACK_ID, ");
			sqlQuery.append("       FB.staff_id                          AS STAFF_ID, ");
			sqlQuery.append("       FB.customer_id                       AS CUSTOMER_ID, ");
			sqlQuery.append("       CT.customer_code                     AS CUSTOMER_CODE, ");
			sqlQuery.append("       CT.customer_name                     AS CUSTOMER_NAME, ");
			sqlQuery.append("       CT.street                            AS STREET, ");
			sqlQuery.append("       CT.housenumber                       AS HOUSE_NUMBER, ");
			sqlQuery.append("       FB.status                            AS STATUS, ");
			sqlQuery.append("       FB.content                           AS CONTENT, ");
			sqlQuery.append("       AP.DESCRIPTION                       AS DESCRIPTION, ");
			sqlQuery.append("       FB.status                            AS IS_DELETED, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.create_date) AS CREATE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.remind_date) AS REMIND_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.done_date)   AS DONE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.update_date) AS UPDATE_DATE, ");
			sqlQuery.append("       Strftime('%d/%m/%Y', FB.user_update) AS USER_UPDATE ");
			sqlQuery.append("FROM   ap_param AP, ");
			sqlQuery.append("       feedback FB ");
			sqlQuery.append("       LEFT JOIN customer CT ");
			sqlQuery.append("              ON FB.customer_id = CT.customer_id ");
			sqlQuery.append("WHERE  FB.status <> 0 ");
			sqlQuery.append("       AND  FB.staff_id = ?");
			sqlQuery.append("       AND  AP.code LIKE 'FEEDBACK_TYPE_TBHV' ");
			sqlQuery.append("       AND Date(FB.create_date) <= Date('NOW', 'localtime') ");
			sqlQuery.append("       AND FB.type = AP.value ");

			if (!StringUtil.isNullOrEmpty(fromDate)) {
				sqlQuery.append("       AND ifnull(dayInOrder(fb.remind_date) >= dayInOrder(?),1) ");
				listparam.add(fromDate);
			}
			if (!StringUtil.isNullOrEmpty(toDate)) {
				sqlQuery.append("       AND ifnull(dayInOrder(fb.remind_date) <= dayInOrder(?),1) ");
				listparam.add(toDate);
			}

			if (!StringUtil.isNullOrEmpty(typeProblem)) {
				sqlQuery.append("       AND fb.status = ? ");
				listparam.add(typeProblem);
			}

			sqlQuery.append("ORDER  BY FB.status ASC, ");
			sqlQuery.append("          FB.remind_date, ");
			sqlQuery.append("          FB.done_date DESC, ");
			sqlQuery.append("          FB.create_date DESC, ");
			sqlQuery.append("          CT.customer_code, ");
			sqlQuery.append("          CT.customer_name ");

			String getCountProblemList = " select count(*) as total_row from (" + sqlQuery.toString() + ") ";

			String params[] = listparam.toArray(new String[listparam.size()]);

			int total = 0;

			if (isGetTotalItem) {
				cTmp = rawQuery(getCountProblemList, params);
				if (cTmp != null) {
					cTmp.moveToFirst();
					total = cTmp.getInt(0);
					result.totalItem = total;
				}
			}

			c = this.rawQuery(sqlQuery.toString() + page, params);
			if (c.moveToFirst()) {
				do {
					SupervisorProblemOfGSNPPDTO item = new SupervisorProblemOfGSNPPDTO();
					item.initDataWithCursor(c);
					result.listProblemsOfGSNPP.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception e) {
			result = null;
			// TODO: handle exception
		} finally {
			try {
				if (cTmp != null) {
					cTmp.close();
				}
				if (c != null) {
					c.close();
				}
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get list feedback reviews of tbhv
	 *
	 * @param data
	 * @return
	 * @return: List<FeedBackTBHVDTO>
	 * @throws:
	 * @author: HaiTC3
	 * @date: Nov 8, 2012
	 */
	public List<FeedBackTBHVDTO> getListReviewsOfTBHV(Bundle data) {
		List<FeedBackTBHVDTO> listReviews = new ArrayList<FeedBackTBHVDTO>();
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String createUserStaffId = data.getString(IntentConstants.INTENT_CREATE_USER_STAFF_ID);

		StringBuffer sqlQuery = new StringBuffer();
		sqlQuery.append("SELECT FEEDBACK_ID, STAFF_ID, CUSTOMER_ID, STATUS, CONTENT, USER_UPDATE, CREATE_DATE, UPDATE_DATE, TYPE, IS_SEND,  Strftime('%d/%m/%Y',REMIND_DATE) REMIND_DATE, DONE_DATE, IS_DELETED, CREATE_USER_ID ");
		sqlQuery.append("FROM   feedback ");
		sqlQuery.append("WHERE  1 = 1 ");
		sqlQuery.append("       AND is_deleted = 0 ");
		sqlQuery.append("       AND staff_id = ? ");
		sqlQuery.append("       AND create_user_id = ? ");
		sqlQuery.append("       AND Date(create_date) = Date('now', 'localtime') ");

		Cursor c = null;
		String[] paramsList = new String[] { staffId, createUserStaffId };

		try {
			c = this.rawQuery(sqlQuery.toString(), paramsList);
			if (c != null && c.moveToFirst()) {
				do {
					FeedBackTBHVDTO item = new FeedBackTBHVDTO();
					item.feedBackBasic.initDataWithCursor(c);
					item.currentState = FeedBackTBHVDTO.STATE_NO_UPDATE;
					listReviews.add(item);
				} while (c.moveToNext());
			}
		} catch (Exception ex) {
			MyLog.w("",	 ex.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listReviews;
	}

	/**
	 *
	 * get reviews of staff
	 *
	 * @author: HaiTC3
	 * @param data
	 * @return
	 * @return: ReviewsStaffViewDTO
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ReviewsStaffViewDTO getReviewStaffView(Bundle data) {
		String staffId = data.getString(IntentConstants.INTENT_STAFF_ID);
		String customerId = data.getString(IntentConstants.INTENT_CUSTOMER_ID);
		String shopId = data.getString(IntentConstants.INTENT_SHOP_ID);
		String createUserId = data.getString(IntentConstants.INTENT_STAFF_OWNER_ID);
		String tpdId = data.getString(IntentConstants.INTENT_TRAINING_DETAIL_ID);
		ReviewsStaffViewDTO reviewsInfo = new ReviewsStaffViewDTO();
		reviewsInfo.feedBackSKU.feedBack = this.getFeedBackSKU(staffId, customerId, shopId, tpdId, createUserId);
		if (reviewsInfo.feedBackSKU.feedBack != null && reviewsInfo.feedBackSKU.feedBack.getFeedBackId() > 0) {
			String feedBackSKUId = String.valueOf(reviewsInfo.feedBackSKU.feedBack.getFeedBackId());
			reviewsInfo.listSKU = this.getListSKUDone(feedBackSKUId);
		}
		reviewsInfo.listReviewsObject = this.getListReviewsOfStaff(shopId, createUserId, staffId, customerId, tpdId);

		return reviewsInfo;
	}

	/**
	 *
	 * get feedback for list SKU
	 *
	 * @author: HaiTC3
	 * @param staffId
	 * @param customerId
	 * @param shopId
	 * @param tpdId
	 * @param createUserId
	 * @return
	 * @return: FeedBackDTO
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public FeedBackDTO getFeedBackSKU(String staffId, String customerId, String shopId, String tpdId,
			String createUserId) {
		FeedBackDTO feedBackSKU = new FeedBackDTO();
		StringBuffer requestGetListSKU = new StringBuffer();
		requestGetListSKU.append("SELECT f.*, ap.[DESCRIPTION] ");
		requestGetListSKU.append("FROM   feedback f, ap_param ap ");
		requestGetListSKU.append("WHERE  f.staff_id = ? ");
		requestGetListSKU.append("       AND f.customer_id = ? ");
		requestGetListSKU.append("       AND f.training_plan_detail_id = ? ");
		requestGetListSKU.append("       AND f.status = 1 ");
		requestGetListSKU.append("       AND f.type = 9 ");
		requestGetListSKU.append("       AND f.create_user_id = ? ");
		requestGetListSKU.append("       AND Date(f.create_date) = Date('now', 'localtime') ");
		requestGetListSKU.append("       AND ap.value = f.type ");

		String[] params = { staffId, customerId, tpdId, createUserId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(requestGetListSKU.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					feedBackSKU.initDataWithCursor(c);
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return feedBackSKU;
	}

	/**
	 *
	 * get list SKU done
	 *
	 * @author: HaiTC3
	 * @return
	 * @return: ArrayList<FeedBackDetailDTO>
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ArrayList<FeedBackDetailDTO> getListSKUDone(String feedbackId) {
		ArrayList<FeedBackDetailDTO> result = new ArrayList<FeedBackDetailDTO>();
		String requestGetListSKU = "select fbd.*, p.product_code PRODUCT_CODE from feedback_detail fbd, product p where fbd.product_id = p.product_id and p.status = 1 and feedback_id = ?";
		String[] params = { feedbackId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(requestGetListSKU.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						FeedBackDetailDTO feedBackSKU = new FeedBackDetailDTO();
						feedBackSKU.initWithCursor(c);
						result.add(feedBackSKU);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return result;
	}

	/**
	 *
	 * get list revies of staff
	 *
	 * @author: HaiTC3
	 * @param shop_id
	 * @param supperStaff_id
	 * @param staff_id
	 * @param customer_id
	 * @return
	 * @return: ArrayList<ReviewsObjectDTO>
	 * @throws:
	 * @since: Jan 29, 2013
	 */
	public ArrayList<ReviewsObjectDTO> getListReviewsOfStaff(String shopId, String createUserId, String staffId,
			String customerId, String tpdId) {
		ArrayList<ReviewsObjectDTO> listReviews = new ArrayList<ReviewsObjectDTO>();
		StringBuffer requestGetListReviews = new StringBuffer();
		requestGetListReviews.append("SELECT * ");
		requestGetListReviews.append("FROM   feedback ");
		requestGetListReviews.append("WHERE  staff_id = ? ");
		requestGetListReviews.append("       AND customer_id = ? ");
		requestGetListReviews.append("       AND training_plan_detail_id = ? ");
		requestGetListReviews.append("       AND status = 1 ");
		requestGetListReviews.append("       AND type in (6, 7, 8) ");
		requestGetListReviews.append("       AND create_user_id = ? ");
		requestGetListReviews.append("       AND Date(create_date) = Date('now', 'localtime') ");

		String[] params = { staffId, customerId, tpdId, createUserId };
		Cursor c = null;
		try {
			// get total row first
			c = rawQuery(requestGetListReviews.toString(), params);

			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ReviewsObjectDTO trainingResult = new ReviewsObjectDTO();
						trainingResult.parserDataFromCursor(c);
						listReviews.add(trainingResult);
					} while (c.moveToNext());
				}
			}

		} catch (Exception e) {
			MyLog.w("",	 e.toString());
		} finally {
			try {
				if (c != null) {
					c.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return listReviews;
	}

	/**
	 * Role NV: lay danh sach van de can thuc hien hien thi khi ket thuc ghe tham
	 * @author: yennth16
	 * @since: 14:34:04 26-05-2015
	 * @return: ProblemsFeedBackDTO
	 * @param staffId
	 * @param customerId
	 * @param page
	 * @return
	 */

	public ProblemsFeedBackDTO getListProblemsFeedback(int staffId, long customerId, int page) {

		ProblemsFeedBackDTO dto = new ProblemsFeedBackDTO();
		ArrayList<String> params = new ArrayList<String>();
		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT FBS.STAFF_ID                            as  STAFF_ID, ");
		varname1.append("       FBSC.CUSTOMER_ID                        as  CUSTOMER_ID, ");
		varname1.append("       FB.CONTENT                              as  CONTENT, ");
		varname1.append("       FB.TYPE                                 as  TYPE, ");
		varname1.append("       FBS.RESULT                              as 	RESULT , ");
		varname1.append("       AP.AP_PARAM_NAME                        as 	AP_PARAM_NAME, ");
		varname1.append("       Strftime('%d/%m/%Y',FB.CREATE_DATE)     as 	CREATE_DATE, ");
		varname1.append("       Strftime('%d/%m/%Y',FB.REMIND_DATE)     as 	REMIND_DATE, ");
		varname1.append("       Strftime('%d/%m/%Y',FBS.DONE_DATE)      as 	DONE_DATE ");
		varname1.append("FROM ");
		varname1.append("    AP_PARAM AP, ");
		varname1.append("    FEEDBACK FB, ");
		varname1.append("    FEEDBACK_STAFF FBS, ");
		varname1.append("    FEEDBACK_STAFF_CUSTOMER FBSC ");
		varname1.append("WHERE ");
		varname1.append("    1 = 1 ");
		varname1.append("    AND FB.[FEEDBACK_ID] = FBS.[FEEDBACK_ID] ");
		varname1.append("    AND FBS.[FEEDBACK_STAFF_ID] = FBSC.[FEEDBACK_STAFF_ID] ");
		varname1.append("    AND FBS.STAFF_ID = ? ");
		params.add(Constants.STR_BLANK + staffId);
		varname1.append("    AND AP.TYPE like 'FEEDBACK_TYPE' ");
		varname1.append("    AND FBSC.CUSTOMER_ID = ? ");
		params.add(Constants.STR_BLANK + customerId);
		varname1.append("    AND FB.TYPE = AP.AP_PARAM_CODE ");
		varname1.append("    AND FB.STATUS = 1 ");
		varname1.append("    AND FBS.RESULT = 0 ");
		varname1.append("    AND ap.status = 1 ");
		varname1.append("    AND Date(FB.CREATE_DATE) >= DATE('NOW','localtime','START OF MONTH','-2 MONTH') ");
		varname1.append("ORDER  BY ");
		varname1.append("    FBS.RESULT asc, ");
		varname1.append("    FB.REMIND_DATE , ");
		varname1.append("    FBS.DONE_DATE desc, ");
		varname1.append("    FB.CREATE_DATE desc");
//		StringBuffer sql = new StringBuffer();
//		sql.append("SELECT STAFF_ID, ");
//		sql.append("       CUSTOMER_ID, ");
//		sql.append("       CONTENT, ");
//		sql.append("       feedback.TYPE, ");
//		sql.append("       feedback.STATUS, ");
//		sql.append("       Strftime('%d/%m/%Y', CREATE_DATE) as CREATE_DATE, ");
//		sql.append("       Strftime('%d/%m/%Y', REMIND_DATE) as REMIND_DATE, ");
//		sql.append("       Strftime('%d/%m/%Y', DONE_DATE)   as DONE_DATE, ");
//		sql.append("       ap.AP_PARAM_NAME AS TYPENAME ");
//		sql.append("FROM   feedback, AP_PARAM ap ");
//		sql.append("WHERE  staff_id = ? ");
//		params.add(staffId + "");
//		sql.append("       AND customer_id = ? ");
//		params.add(customerId + "");
//		sql.append("       AND ap.status = 1 ");
//		sql.append("       AND feedback.shop_id = ? ");
//		params.add(shop_Id);
//		sql.append("       AND ap.TYPE IN ('FEEDBACK_TYPE_NVBH','FEEDBACK_TYPE_GSNPP','FEEDBACK_TYPE_TBHV') ");
//		sql.append("       AND ap.AP_PARAM_CODE = feedback.TYPE ");
//		sql.append("       AND feedback.status = 1 "); // 0: da xoa, 1: Tao moi, 2: Da thuc hien, 3: Da duyet
//		sql.append("       AND Date(CREATE_DATE) >= DATE('NOW','START OF MONTH','-2 MONTH', 'localtime') ");
//		sql.append("order by feedback.STATUS asc, ");
//		sql.append("         DONE_DATE, ");
//		sql.append("         REMIND_DATE , ");
//		sql.append("         CREATE_DATE desc ");

		StringBuffer totalPageSql = new StringBuffer();
		if (page > 0) {
			totalPageSql.append("select count(*) as TOTAL_ROW from (");
			totalPageSql.append(varname1);
			totalPageSql.append(") ");
			varname1.append(" limit " + Integer.toString(Constants.NUM_ITEM_PER_PAGE));
			varname1.append(" offset " + Integer.toString((page - 1) * Constants.NUM_ITEM_PER_PAGE));
		}

		Cursor c = null;
		Cursor cTotalRow = null;
		try {
			c = rawQueries(varname1.toString(), params);
			//LogFile.logToFile(var1.toString());
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						ProblemsFeedBackDTO.ProblemsFeedBackItem item = dto.initDataFromCursor(c);
						dto.listProblems.add(item);
					} while (c.moveToNext());
				}
			}
			if (page > 0 ) {
				cTotalRow = rawQueries(totalPageSql.toString(), params);
				if (cTotalRow != null) {
					if (cTotalRow.moveToFirst()) {
						dto.totalRows = cTotalRow.getInt(0);
					}
				}
			}
		} catch (Exception e) {
		} finally {
			try {
				if (c != null) {
					c.close();
				}
				if (cTotalRow != null) {
					cTotalRow.close();
				}
			} catch (Exception ex) {
			}
		}

		return dto;
	}

	public long gsnppPostFeedBack(FeedBackDTO dto) {
		long returnCode = -1;
		if (AbstractTableDTO.TableType.FEEDBACK_TABLE.equals(dto.getType())) {
			try {
				TABLE_ID tableId = new TABLE_ID(mDB);
				dto.setFeedBackId(tableId.getMaxIdTime(FEEDBACK_TABLE));
				dto.getArrFeedBackId().add(String.valueOf(dto.getFeedBackId()));
				returnCode = insert(dto);
			} catch (Exception e) {
			}
		}
		return returnCode;
	}

}
