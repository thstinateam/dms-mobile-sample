/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/
package com.ths.universalimageloader;

import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.ths.image.zoom.ImageViewTouch;
import com.ths.dmscore.util.ImageUtil;
import com.ths.dmscore.view.main.BaseFragment;
import com.ths.dms.R;
import com.ths.image.zoom.PagerAdapter;

/**
 * Adapter for gallery image
 * ImagePagerAdapter.java
 * @author: duongdt3
 * @version: 1.0
 * @since:  10:26:45 25 Jan 2014
 */
public class ImagePagerAdapter extends PagerAdapter {


	private List<String> images;
	private LayoutInflater inflater;
	private BaseFragment parent;
	private OnTouchListener touchListener = null;
	private OnPagerListener pagerListener = null;


	public ImagePagerAdapter(Activity activity, BaseFragment parent,  List<String> images) {
		inflater = activity.getLayoutInflater();
		this.images = images;
		this.parent = parent;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		container.removeView((View) object);
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public Object instantiateItem(ViewGroup view, int position) {
		View imageLayout = inflater.inflate(R.layout.layout_item_pager_image, view, false);
		final ImageViewTouch imageTouch = (ImageViewTouch) imageLayout.findViewById(R.id.image);
		imageTouch.setFragment(parent);

		if (touchListener == null) {
			 touchListener = getDefaultListener();
		}
		imageTouch.setOnTouchListener(touchListener);
		Bitmap bm = BitmapFactory.decodeResource(parent.getResources(), R.drawable.image_loading);
		imageTouch.setImageBitmapReset(bm, true);

		ImageUtil.loadImage(images.get(position), imageTouch, new SimpleImageLoadingListener() {

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				if (ImageUtil.loadingListener != null) {
					ImageUtil.loadingListener.onLoadingFailed(imageUri, view, failReason);
				}
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				imageTouch.setImageBitmapReset(loadedImage, true);
			}
		});

		if(((getCount() - 3) == position)&& pagerListener != null){
			pagerListener.onLoadMore();
		}

		view.addView(imageLayout, 0);
		return imageLayout;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

	@Override
	public void startUpdate(View container) {
	}

	@Override
	public void finishUpdate(View container) {
	}

	public void setOntouchListener(OnTouchListener listener){
		this.touchListener = listener;
	}

	public void setPagerListener(OnPagerListener listener){
		this.pagerListener = listener;
	}

	private OnTouchListener getDefaultListener(){
		return new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return false;
			}
		};
	}

	public interface OnPagerListener{
		public void onLoadMore();
	}
}
