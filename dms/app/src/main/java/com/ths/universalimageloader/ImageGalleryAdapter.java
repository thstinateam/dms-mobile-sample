/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/
package com.ths.universalimageloader;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ths.dmscore.util.ImageUtil;
import com.ths.dms.R;

/**
 * Adapter Gallery
 * ImageGalleryAdapter.java
 * @author: QuangVT1
 * @version: 1.0
 * @since:  1:53:32 PM Jan 18, 2014
 */
public class ImageGalleryAdapter extends BaseAdapter {
	// Vi tri cuoi bat dau load more
	public final int LAST_ITEM_LOAD_MORE = 3;
	// Context de layout
	protected Context mContext;
	// Danh sach link anh
	protected List<String> mImageUrls;
	// Listener
	private OnGalleryListener listener;

	/**
	 * Ham khoi tao File layout truyen ben ngoai vao
	 *
	 * @param context
	 * @param urls
	 * @param options
	 */
	public ImageGalleryAdapter(Context context, List<String> urls) {
		this.mContext = context;
		this.mImageUrls = urls;
	}

	@Override
	public int getCount() {
		int count = 0;
		if (mImageUrls != null) {
			count = mImageUrls.size();
		}
		return count;
	}

	@Override
	public Object getItem(int position) {
		Object item = null;
		if (mImageUrls != null && position >= 0 && position < getCount()) {
			item = mImageUrls.get(position);
		}
		return item;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ImageView imageItem = null;

		if (convertView == null) {
			LayoutInflater layoutInflater = getLayoutInflater();
			row = layoutInflater.inflate(R.layout.layout_thumbnail_item_view,
					null);
			imageItem = (ImageView) row.findViewById(R.id.thumbnail_item);
			row.setTag(imageItem);
		} else {
			imageItem = (ImageView) row.getTag();
		}

		final String url = (String) getItem(position);
		ImageUtil.loadImage(url, imageItem);

		// Su kien load more
		if ((position == getCount() - LAST_ITEM_LOAD_MORE) && listener != null) {
			listener.onLoadMore();
		}

		return row;
	}

	public void setGalleryListener(OnGalleryListener listener) {
		this.listener = listener;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public interface OnGalleryListener {
		public void onLoadMore();
	}
}

