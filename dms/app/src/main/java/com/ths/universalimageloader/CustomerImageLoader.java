/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/
package com.ths.universalimageloader;

import java.io.File;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;
import com.ths.dmscore.util.ImageUtil;


/**
 * Cache loader image for Customer image
 * CustomerImageLoader.java
 * @author: duongdt3
 * @version: 1.0
 * @since:  10:00:42 23 Jan 2014
 */
public class CustomerImageLoader extends ImageLoader {

    private volatile static CustomerImageLoader instance;

    /** Returns singleton class instance */
    public static CustomerImageLoader getInstance() {
        if (instance == null) {
            synchronized (CustomerImageLoader.class) {
                if (instance == null) {
                    instance = new CustomerImageLoader();
            		File imageCache = ExternalStorage.getImageCachePath();
            		//Configuration load image default
            		ImageLoaderConfiguration configProduct = new ImageLoaderConfiguration.Builder(GlobalInfo.getInstance().getAppContext())
            			// 5 luong 1 luc
        				.threadPoolSize(5)
                		.threadPriority(Thread.NORM_PRIORITY)
                		.denyCacheImageMultipleSizesInMemory()
                		.tasksProcessingOrder(QueueProcessingType.FIFO)
                		// cho phep ghi log, neu ung dung release thi ko cho ghi
                		//.writeDebugLogs()
                		// option hien thi mac dinh
                		.defaultDisplayImageOptions(ImageUtil.getImageDisplayOptionDefault())
                		// dung cach hash MD5 cho ten file, voi dia chi cache Product image
                		.discCache(new UnlimitedDiscCache(imageCache, imageCache, new Md5FileNameGenerator()))
                		.build();

            		// Initialize ProductImageLoader with configuration.
            		instance.init(configProduct);
                }
            }
        }
        return instance;
    }
}