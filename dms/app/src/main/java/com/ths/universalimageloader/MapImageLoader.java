/**
* Copyright 2014 THS. All rights reserved.
*  Use is subject to license terms.
*/
package com.ths.universalimageloader;

import java.io.File;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.ths.dmscore.global.GlobalInfo;
import com.ths.dmscore.lib.sqllite.download.ExternalStorage;


/**
 * Cache loader image for Product image
 * ProductImageLoader.java
 * @author: duongdt3
 * @version: 1.0
 * @since:  10:01:22 23 Jan 2014
 */
public class MapImageLoader extends ImageLoader {

    private volatile static MapImageLoader instance;

    /** Returns singleton class instance */
    public static MapImageLoader getInstance() {
        if (instance == null) {
            synchronized (MapImageLoader.class) {
                if (instance == null) {
                    instance = new MapImageLoader();
                    DisplayImageOptions mOptions;
                    // init ImageLoader display options
                    DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder();
                    builder
                    	.cacheInMemory(false)
                    	.cacheOnDisc(true);
                    mOptions = builder.build();
                    
            		File mapCache = ExternalStorage.getMapCacheFolder();
            		//Configuration load image default
            		ImageLoaderConfiguration configMap = new ImageLoaderConfiguration.Builder(GlobalInfo.getInstance().getAppContext())
                		// 6 luong 1 luc
            			.threadPoolSize(6)
                		.threadPriority(Thread.NORM_PRIORITY)
                		.denyCacheImageMultipleSizesInMemory()
                		.tasksProcessingOrder(QueueProcessingType.FIFO)
                		// dung cach hash MD5 cho ten file, voi dia chi cache map
                		.discCache(new UnlimitedDiscCache(mapCache, mapCache, new Md5FileNameGenerator()))
                		// cho phep ghi log, neu ung dung release thi ko cho ghi
                		//.writeDebugLogs()
                		.defaultDisplayImageOptions(mOptions)
                		.build();

            		// Initialize ProductImageLoader with configuration.
            		instance.init(configMap);
                }
            }
        }
        return instance;
    }
}