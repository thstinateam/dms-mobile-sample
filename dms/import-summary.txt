ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
/Volumes/HDD/Data/Viettel Working/VPKDQT/Project/IDP/Source/NEW_DMS_Core_1504071
                         -                                                      

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .DS_Store
* .jazzignore
* proguard/
* proguard/dump.txt
* proguard/mapping.txt
* proguard/seeds.txt
* release/
* release/idp.key
* release/key info.txt
* sonar-project.properties

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0
gcm.jar => com.google.android.gms:play-services:+
guava-r09.jar => com.google.guava:guava:18.0

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

google-play-services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* assets/ => app/src/main/assets/
* libs/CWAC-AdapterWrapper.jar => app/libs/CWAC-AdapterWrapper.jar
* libs/CWAC-Bus.jar => app/libs/CWAC-Bus.jar
* libs/CWAC-Task.jar => app/libs/CWAC-Task.jar
* libs/VTLZMA.jar => app/libs/VTLZMA.jar
* libs/VTMapAPI.jar => app/libs/VTMapAPI.jar
* libs/achartengine-1.1.0.jar => app/libs/achartengine-1.1.0.jar
* libs/commons-codec.jar => app/libs/commons-codec.jar
* libs/commons-httpclient-3.1.jar => app/libs/commons-httpclient-3.1.jar
* libs/jackson-annotations-2.0.1.jar => app/libs/jackson-annotations-2.0.1.jar
* libs/jackson-core-2.0.1.jar => app/libs/jackson-core-2.0.1.jar
* libs/jackson-databind-2.0.1.jar => app/libs/jackson-databind-2.0.1.jar
* libs/microlog4android-1.0.0.jar => app/libs/microlog4android-1.0.0.jar
* libs/sqlcipher.jar => app/libs/sqlcipher.jar
* libs/universal-image-loader-1.9.3.jar => app/libs/universal-image-loader-1.9.3.jar
* libs/zip4j_1.3.1.jar => app/libs/zip4j_1.3.1.jar
* proguard.cfg => app/proguard.cfg
* res/ => app/src/main/res/
* src/ => app/src/main/java/
* src/com/viettel/dmscore/lib/oAuth/model/OAuthConfig.java.bak => app/src/main/resources/com/viettel/dmscore/lib/oAuth/model/OAuthConfig.java.bak
* noimages.PNG => noimages.png

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
